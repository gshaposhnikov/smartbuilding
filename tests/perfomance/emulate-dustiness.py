import argparse
import json

import pika
from time import sleep

exchange_name = 'ru.rubezh.firesec.nt.alerts'

properties = pika.BasicProperties(
    delivery_mode=2,
    priority=0,
    content_type='application/json',
    content_encoding='utf-8',
    headers={'__TypeId__': 'ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues'})


def main(host, driver, device):
    credentials = pika.credentials.PlainCredentials(username='admin', password='password')
    parameters = pika.ConnectionParameters(host=host, credentials=credentials)

    with pika.BlockingConnection(parameters=parameters) as connection:
        channel = connection.channel()

        channel.exchange_declare(exchange=exchange_name,
                                 exchange_type='topic',
                                 durable=True)

        # показания датчика задымления - пила от 0.00 до 0.20

        i = 0
        while True:
            dustiness = i % 20
            smokiness = (i - 1) % 10
            message = {
                'driverId': f'rubezh2op3DriverProfile-{driver}',
                'deviceId': device,
                'deviceLocal': False,
                'controlDeviceId': None,
                'controlDeviceAddress': 0,
                'lineNo': 0,
                'address': 0,
                'monitorableValues': {
                    'dustiness': dustiness / 100,
                    'smokiness': smokiness / 10,
                },
            }

            channel.basic_publish(
                exchange=exchange_name,
                routing_key=f'info.DEVICE.monitoring.rubezh2op3DriverProfile-{driver}',
                body=json.dumps(message),
                properties=properties,
            )

            sleep(5)
            i += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', help='Имя сервера AMQP', type=str, default='firesec-perfomance')
    parser.add_argument('-d', help='ID драйвера', type=int, default=61616)
    parser.add_argument('-a', help='ID устройства', type=str, default='5c04f749f14c8ec2056803bd')

    args = parser.parse_args()

    main(args.s, args.d, args.a)
