import argparse

import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

group_data = {
    'name': 'users-group',
    'description': 'users-group',
    'builtin': False,
    'permissionIds': [
        'PROJECT_READ',
        'PROJECT_CREATE',
        'PROJECT_UPDATE',
        'PROJECT_REMOVE',
        'PROJECT_STATE_UPDATE',
        'USER_READ',
        'USER_CREATE',
        'USER_UPDATE',
        'USER_REMOVE',
        'USER_BUILTIN_UPDATE',
        'USER_SELF_UPDATE',
        'USER_RESTORE',
        'USER_GROUP_READ',
        'USER_GROUP_CREATE',
        'USER_GROUP_UPDATE',
        'USER_GROUP_REMOVE',
        'REGION_READ',
        'REGION_CREATE',
        'REGION_UPDATE',
        'REGION_REMOVE',
        'DEVICE_READ',
        'DEVICE_CREATE',
        'DEVICE_UPDATE',
        'DEVICE_REMOVE',
        'SCENARIO_READ',
        'SCENARIO_CREATE',
        'SCENARIO_UPDATE',
        'SCENARIO_REMOVE',
        'VIRTUAL_STATE_READ',
        'VIRTUAL_STATE_CREATE',
        'VIRTUAL_STATE_UPDATE',
        'VIRTUAL_STATE_REMOVE',
        'ADMINISTRATE']
}

user_data = {
    'name': 'user0',
    'workspaceId': None,
    'indicatorId': None,
    'permissionIds': [
        'PROJECT_READ',
        'PROJECT_CREATE',
        'PROJECT_UPDATE',
        'PROJECT_REMOVE',
        'PROJECT_STATE_UPDATE',
        'USER_READ',
        'USER_CREATE',
        'USER_UPDATE',
        'USER_REMOVE',
        'USER_BUILTIN_UPDATE',
        'USER_SELF_UPDATE',
        'USER_RESTORE',
        'USER_GROUP_READ',
        'USER_GROUP_CREATE',
        'USER_GROUP_UPDATE',
        'USER_GROUP_REMOVE',
        'REGION_READ',
        'REGION_CREATE',
        'REGION_UPDATE',
        'REGION_REMOVE',
        'DEVICE_READ',
        'DEVICE_CREATE',
        'DEVICE_UPDATE',
        'DEVICE_REMOVE',
        'SCENARIO_READ',
        'SCENARIO_CREATE',
        'SCENARIO_UPDATE',
        'SCENARIO_REMOVE',
        'VIRTUAL_STATE_READ',
        'VIRTUAL_STATE_CREATE',
        'VIRTUAL_STATE_UPDATE',
        'VIRTUAL_STATE_REMOVE',
        'ADMINISTRATE'],
    'groupPermissionIds': [],
    'userGroupIds': ['5b7a79ede4b075635647755c'],
    'fullName': 'user0',
    'email': 'user0@example.com',
    'phoneNo': '+70000000000',
    'password': '000000',
    'password2': '000000'}


def main(host, number_of_users):
    session = requests.session()
    session.verify = False

    response = session.post(f'{host}/login?username=admin&password=123456', json=None)
    if response.status_code != 200:
        print(f'Unexpected auth response: {response.status_code}')

        from sys import exit
        exit(1)

    for index in range(number_of_users):
        group_data['name'] = f'users-group{index}'
        group_data['description'] = f'users-group{index}'

        url = f'{host}/api/v1/user_groups'
        response = session.post(url, json=group_data)

    url = f'{host}/api/v1/user_groups/'
    response = session.get(url)
    group_ids = {x.get('name'): x.get('id') for x in response.json() if x.get('name', '').startswith('user')}

    for index in range(number_of_users):
        user_data['name'] = f'user{index}'
        user_data['fullName'] = f'user{index}'
        user_data['email'] = f'user{index}@example.com'
        user_data['phoneNo'] = f'+700000{index:05}'
        user_data['password'] = f'0{index:05}'
        user_data['password2'] = f'0{index:05}'
        user_data['userGroupIds'] = [group_ids.get(f'users-group{index}')]

        url = f'{host}/api/v1/users'
        response = session.post(url, json=user_data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', help='URL сервера', type=str, default='https://firesec-perfomance:8443')
    parser.add_argument('-n', help='Количество пользователей', type=int, default=5000)

    args = parser.parse_args()

    main(args.u, args.n)
