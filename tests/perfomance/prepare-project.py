import argparse
import urllib3
import requests

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

create_ms_1_data = {
    'deviceProfileId': 'amod1DeviceProfile',
    'lineNo': 1,
    'lineAddress': 0
}

create_2OP3_data = {
    'deviceProfileId': 'rubezh2op3DeviceProfile',
    'parentDeviceId': '5aec3bb0f14ca2a7e4a10e22',
    'lineNo': 1,
    'lineAddress': 1}

create_am_4_data = {
    'deviceProfileId': 'am4ContainerDeviceProfile',
    'parentDeviceId': '5aec400cf14ca2a7e4a10e31',
    'lineNo': 1,
    'lineAddress': 1
}

create_rm_4_data = {
    'deviceProfileId': 'rm4ContainerDeviceProfile',
    'parentDeviceId': '5aec400cf14ca2a7e4a10e31',
    'lineNo': 1,
    'lineAddress': 1
}

session = requests.session()
session.verify = False


def create_device(parent_device_id, line_number, line_address, data):
    if parent_device_id:
        data['parentDeviceId'] = parent_device_id

    if line_number:
        data['lineNo'] = line_number

    if line_address:
        data['lineAddress'] = line_address

    response = session.post(create_device_url, json=data)
    response_data = response.json()
    created_devices = response_data.get('createdDevices', [{}])
    device_ids = [x.get('id', None) for x in created_devices]
    print(response.status_code, device_ids, response_data)

    return device_ids


def main(count_ms_1=1, count_2op3=1, count_devices=5, count_scenarios=20):
    response = session.post('https://firesec-perfomance:8443/login?username=admin&password=123456', json=None)
    if response.status_code != 200:
        print(f'Unexpected response: {response.status_code}')
        from sys import exit
        exit(1)

    devices_ms_1 = []
    for address_ms_1 in range(1, count_ms_1 + 1):
        device_id_ms_1 = create_device(None, None, address_ms_1, create_ms_1_data)[0]
        if device_id_ms_1:
            devices_ms_1.append(device_id_ms_1)

    devices_2op3 = []
    for index, device_id_ms_1 in enumerate(devices_ms_1):
        for address_2op3 in range(1, count_2op3 + 1):
            device_id_2op3 = create_device(device_id_ms_1, 1, address_2op3, create_2OP3_data)[0]
            if device_id_2op3:
                plan_group_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/plan_groups/'
                plan_group_data = {
                    'name': f'Прибор {index + 1}.{address_2op3}'
                }
                response = session.post(plan_group_url, json=plan_group_data)
                response_data = response.json()
                plan_group_id = response_data.get('id')
                devices_2op3.append((device_id_2op3, plan_group_id))

    device_id_am_4_tech = None
    for index, (device_id_2op3, plan_group_id) in enumerate(devices_2op3):
        for device_address in range(1, count_devices + 1, 4):
            device_ids_am_4 = create_device(device_id_2op3, 1, device_address, create_am_4_data)[1:]
            device_ids_rm_4 = create_device(device_id_2op3, 2, device_address, create_rm_4_data)[1:]

            if device_id_am_4_tech is None:
                device_id_am_4_tech = device_ids_am_4[0]

                change_data = {}
                change_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                             f'devices/{device_id_am_4_tech}?device_profile_id=am4TechDeviceProfile'
                response = session.put(change_url, json=change_data)
                response_data = response.json()
                change_id = response_data.get('id')

            for device_id in device_ids_am_4[1:]:
                enable_device_data = {}
                enable_device_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                                    f'devices/{device_id}?disabled=false'
                response = session.put(enable_device_url, json=enable_device_data)
                response_data = response.json()
                enabled_device_id = response_data.get('id')

            for device_id in device_ids_rm_4[1:]:
                enable_device_data = {}
                enable_device_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                                    f'devices/{device_id}?disabled=false'
                response = session.put(enable_device_url, json=enable_device_data)
                response_data = response.json()
                enabled_device_id = response_data.get('id')


            plan_data = {
                'name': f'Устройство {index + 1}.1.{device_address}',
                'subsystem': 'GENERAL',
                'planGroupId': plan_group_id,
                'xSize': 174,
                'ySize': 100,
            }
            plan_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/plans/'
            response = session.post(plan_url, json=plan_data)
            response_data = response.json()
            plan_id = response_data.get('id')

            zone_data = {
                'index': 10 * index + device_address,
                'name': f'Пожарная {10 * index + device_address}',
                'subsystem': 'FIRE',
                'fireEventCount': 2
            }
            zone_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/region_views'
            response = session.post(zone_url, json=zone_data)
            response_data = response.json()
            zone_id = response_data.get('id')

            for _id in device_ids_am_4:
                if _id == device_id_am_4_tech:
                    continue
                put_device_to_zone_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/regions/{zone_id}/devices/{_id}'
                response = session.put(put_device_to_zone_url, json={})
                # response_data = response.json()

            put_zone_to_plan_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/region_views/{zone_id}?plan_layouts'
            put_zone_to_plan_data = [{
                'planId': plan_id,
                'rect': True,
                'points':
                    [{'x': 10, 'y': 10}, {'x': 164, 'y': 51}]
            }]
            response = session.put(put_zone_to_plan_url, json=put_zone_to_plan_data)
            # response_data = response.json()

            for i, _id in enumerate(device_ids_am_4):
                put_device_to_plan_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/devices/{_id}?plan_layouts'
                response = session.put(put_device_to_plan_url, json=[{
                    'planId': plan_id,
                    'coordinatePoint': {
                        'x': 20 + i * (26 + 10),
                        'y': 20,
                    }
                }])
                # response_data = response.json()

            for i, _id in enumerate(device_ids_rm_4):
                put_device_to_plan_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/devices/{_id}?plan_layouts'
                response = session.put(put_device_to_plan_url, json=[{
                    'planId': plan_id,
                    'coordinatePoint': {
                        'x': 20 + i * (26 + 10),
                        'y': 61,
                    }
                }])
                # response_data = response.json()

    scenarios = []
    for scenario_index in range(1, count_scenarios + 1):
        state_data = {
            'globalNo': scenario_index,
            'name': f'Состояние {scenario_index}',
            'stateCategoryId': 'Normal',
            'messageOn': f'Состояние {scenario_index} уст.',
            'messageOff': f'Состояние {scenario_index} снято',
            'deviceId': devices_2op3[0][0]
        }
        state_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/virtual_states'
        response = session.post(state_url, json=state_data)
        response_data = response.json()
        state_id = response_data.get('id')

        scenario_data = {
            'name': f'Управление {scenario_index}',
            'scenarioPurpose': 'EXEC_BY_INVOKE',
            'enabled': True,
            'scenarioType': 'UNASSIGNED',
            'controlDeviceId': devices_2op3[0][0],
        }
        scenario_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/scenarios'
        response = session.post(scenario_url, json=scenario_data)
        response_data = response.json()
        scenario_id = response_data.get('id')

        scenario_execute_data = {
            'blockType': 'EXECUTIVE',
            'name': 'Исполнение 1',
            'timeDelaySec': 0,
            'conditionType': 'NONE',
            'conditionCheckType': 'IS_ACTIVE',
            'actions': [{
                'entityType': 'VIRTUAL_STATE',
                'entityId': state_id,
                'actionTypeId': 'satVirtualStateSetActive'
            }]
        }
        scenario_execute_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                               f'scenarios/{scenario_id}/time_line_blocks'
        response = session.post(scenario_execute_url, json=scenario_execute_data)
        response_data = response.json()
        scenario_execute_id = response_data.get('id')

        scenarios.append(scenario_id)

    main_scenario_data = {
        'name': 'Основной',
        'scenarioPurpose': 'EXEC_BY_LOGIC',
        'enabled': True,
        'scenarioType': 'UNASSIGNED',
        'description': 'Основной'
    }
    main_scenario_url = 'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/scenarios'
    response = session.post(main_scenario_url, json=main_scenario_data)
    response_data = response.json()
    main_scenario_id = response_data.get('id')

    main_scenario_logic_data = {
        'logicType': 'AND',
        'subLogics': [],
        'entityType': 'DEVICE',
        'triggerTypeId': 'sttAmTOnOff',
        'entityIds': [device_id_am_4_tech]
    }
    main_scenario_logic_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                              f'scenarios/{main_scenario_id}?start_logic'
    response = session.put(main_scenario_logic_url, json=main_scenario_logic_data)
    response_data = response.json()
    main_scenario_logic_id = response_data.get('id')

    main_scenario_advanced_data = {
        'manualStartStopAllowed': False,
        'stopOnGlobalReset': False,
        'stopType': 'AUTO',
        'startLogicSentence': 'если выполняются все условия: (\n  '
                              'Сработка 1-ого датчика АМ-Т на АМ-4-1Т (устройство 1.1.1)\n)',
        'stopLogicSentence': None
    }
    main_scenario_advanced_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                                 f'scenarios/{main_scenario_id}?advanced_params'
    response = session.put(main_scenario_advanced_url, json=main_scenario_advanced_data)
    response_data = response.json()
    main_scenario_advanced_id = response_data.get('id')

    main_scenario_execute_data = {
        'blockType': 'EXECUTIVE',
        'name': 'Исполнение 1',
        'timeDelaySec': 0,
        'conditionType': 'NONE',
        'conditionCheckType': 'IS_ACTIVE',
        'actions': [{
            'entityType': 'SCENARIO',
            'entityId': scenario_id,
            'actionTypeId': 'satScenarioStart'
        } for scenario_id in scenarios]
    }
    main_scenario_execute_url = f'https://firesec-perfomance:8443/api/v1/projects/5c52fca29758a5ced4408965/' \
                                f'scenarios/{main_scenario_id}/time_line_blocks'
    response = session.post(main_scenario_execute_url, json=main_scenario_execute_data)
    response_data = response.json()
    main_scenario_execute_id = response_data.get('id')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', help='URL сервера', type=str, default='https://firesec-perfomance:8443')
    parser.add_argument('-i', help='ИД проекта', type=str, default='5c52fca29758a5ced4408965')

    parser.add_argument('-m', help='Количество устройств МС-1', type=int, default=1)
    parser.add_argument('-p', help='Количество приборов 2ОП3 на МС-1', type=int, default=10)
    parser.add_argument('-d', help='Количество устройств на линию прибора', type=int, default=250)
    parser.add_argument('-s', help='Количество сценариев, включающих виртуальные состояния', type=int, default=500)
    args = parser.parse_args()

    create_device_url = f'{args.u}/api/v1/projects/{args.i}/devices'

    main(args.m, args.p, args.d, args.s)
