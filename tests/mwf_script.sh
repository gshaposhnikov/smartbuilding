﻿#!/bin/bash

set -e

# 1. Перед запуском установить curl, jq
# 2. Указать URL (по умолчанию указан https://localhost:8443)
# 3. Для запуска в windows установить git bash

# Устройства и их адреса в проекте:
# МС-1, Рубеж-2ОП3 1, АМ-4 2.1-2.4, РМ-4 2.5-2.8

url="https://localhost:8443"
projectid="$(curl -sk -b cookie-jar -X POST ''$url'/api/v1/projects/' -H'content-type:application/json;charset=UTF-8' -d'{"name":"test","userId":"admin","version":"new_version"}' | jq -r '.id')"
msid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/devices -H'content-type:application/json;charset=UTF-8' -d'{"deviceProfileId":"amod1DeviceProfile","lineNo":1,"lineAddress":1,"count":1}' | jq '.createdDevices[0].id')"
rubezhid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/devices -H'content-type:application/json;charset=UTF-8' -d '{"deviceProfileId":"rubezh2op3DeviceProfile","parentDeviceId":'$msid',"lineNo":1,"lineAddress":1,"count":1}' | jq '.createdDevices[0].id')"
amcontid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/devices -H'content-type:application/json;charset=UTF-8' -d'{"deviceProfileId":"am4ContainerDeviceProfile","parentDeviceId":'$rubezhid',"lineNo":2,"lineAddress":1,"count":1}' | jq '.createdDevices[0].id')"
am1id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,1,2,1") .id')"
am2id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,1,2,2") .id')"
am3id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,1,2,3") .id')"
rmcontid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/devices -H'content-type:application/json;charset=UTF-8' -d '{"deviceProfileId":"rm4ContainerDeviceProfile","parentDeviceId":'$rubezhid',"lineNo":2,"lineAddress":5,"count":1}' | jq '.createdDevices[0].id')"
rm1id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,5,2,5") .id')"
rm2id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,5,2,6") .id')"
rm3id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,5,2,7") .id')"
rm4id="$(curl -sk -b cookie-jar -X GET $url/api/v1/projects/$projectid/devices/ | jq -r '.[] | select(.addressPath==",1,1,1,2,5,2,8") .id')"
fire1id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/region_views -H'content-type:application/json;charset=UTF-8' -d'{"index":"1","name":"fire1","subsystem":"FIRE","fireEventCount":"2"}' | jq -r .id)"
sec1id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/region_views -H'content-type:application/json;charset=UTF-8' -d'{"index":"2","name":"sec","subsystem":"SECURITY", "securityRegionType":"DEFAULT","silenceAlarm":"true"}' | jq -r .id)"
virtstate1id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/virtual_states -H'content-type:application/json;charset=UTF-8' -d'{"globalNo":1,"name":"State 1","stateCategoryId":"Normal","messageOn":"State 1 on.","messageOff":"State 1 off","deviceId":'$rubezhid'}' | jq -r '.id')"
virtstate2id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/virtual_states -H'content-type:application/json;charset=UTF-8' -d'{"globalNo":2,"name":"State 2","stateCategoryId":"Normal","messageOn":"State 2 on.","messageOff":"State 2 off","deviceId":'$rubezhid'}' | jq -r '.id')"
scen1id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/scenarios -H'content-type:application/json;charset=UTF-8' -d'{"name":"mwf","scenarioPurpose":"EXEC_BY_LOGIC","enabled":true,"scenarioType":"UNASSIGNED"}' | jq -r .id)"
plan1id="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/plans -H'content-type:application/json;charset=UTF-8' -d'{"name":"Plan 1","subsystem":"GENERAL","planGroupId":null,"xSize":500,"ySize":500}' | jq -r .id)"
indgroupid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/indicator_groups -H'content-type:application/json;charset=UTF-8' -d'{"name":"Group_panels"}' | jq .id)"
indpanelcontid="$(curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/indicator_panels -H'content-type:application/json;charset=UTF-8' -d'{"name":"Panel","colCount":3,"rowCount":3,"indicatorGroupId":'$indgroupid'}' | jq -r .id)"
indpanel1id="$(curl -sk -b cookie-jar -X PUT ''$url'/api/v1/projects/'$projectid'/indicator_panels/'$indpanelcontid'?row=0&col=0' -H'content-type:application/json;charset=UTF-8' -d'{"id":null,"name":"sec_zone","description":"","passwordNeeded":false,"rowNo":0,"colNo":0,"entityType":"REGION","entityIds":["'$sec1id'"],"styles":null}' | jq .id)"
indpanel2id="$(curl -sk -b cookie-jar -X PUT ''$url'/api/v1/projects/'$projectid'/indicator_panels/'$indpanelcontid'?row=0&col=1' -H'content-type:application/json;charset=UTF-8' -d'{"id":null,"name":"fire_zone","description":"","passwordNeeded":false,"rowNo":0,"colNo":1,"entityType":"REGION","entityIds":["'$fire1id'"],"styles":null}' | jq .id)"
indpanel3id="$(curl -sk -b cookie-jar -X PUT ''$url'/api/v1/projects/'$projectid'/indicator_panels/'$indpanelcontid'?row=0&col=2' -H'content-type:application/json;charset=UTF-8' -d'{"id":null,"name":"RM","description":"","passwordNeeded":false,"rowNo":0,"colNo":2,"entityType":"DEVICE","entityIds":["'$rm1id'"],"styles":null}' | jq .id)"
indpanel4id="$(curl -sk -b cookie-jar -X PUT ''$url'/api/v1/projects/'$projectid'/indicator_panels/'$indpanelcontid'?row=1&col=0' -H'content-type:application/json;charset=UTF-8' -d'{"id":null,"name":"scen","description":"","passwordNeeded":false,"rowNo":1,"colNo":0,"entityType":"SCENARIO","entityIds":["'$scen1id'"],"styles":null}' | jq .id)"


#Авторизация
curl -sk -c cookie-jar -X POST -d '' ''$url'/login?username=admin&password=123456'


#Девайсы
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$am2id?disabled=false #Активация устройтв
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$am3id?disabled=false
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$rm2id?disabled=false
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$rm3id?disabled=false
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$rm4id?disabled=false
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/devices/$am1id?device_profile_id=am4SecDeviceProfile #Смена профиля АМ
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$am2id?config -H'content-type:application/json;charset=UTF-8' -d'{"State1":"0"}' #Конфигурирование АМ
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$am3id?config -H'content-type:application/json;charset=UTF-8' -d'{"State1":"0"}'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$rm1id?properties -H'content-type:application/json;charset=UTF-8' -d'{"ManageAllowed":true}' #Конфигурирование РМ
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$rm2id?properties -H'content-type:application/json;charset=UTF-8' -d'{"ManageAllowed":true}'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$rm3id?properties -H'content-type:application/json;charset=UTF-8' -d'{"ManageAllowed":true}'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$rm4id?properties -H'content-type:application/json;charset=UTF-8' -d'{"ManageAllowed":true}'



#Зоны
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/regions/$fire1id/devices/$am2id #Добавление в зону
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/regions/$fire1id/devices/$am3id
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid/regions/$sec1id/devices/$am1id


#Виртуальные состояния
echo $virtstate1id
echo $virtstate2id


#Сценарии
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/scenarios/$scen1id?start_logic -H'content-type:application/json;charset=UTF-8' -d'{"logicType":"AND","subLogics":[],"entityType":"CONTROL_DEVICE","triggerTypeId":"sttWriteToControlDevice","entityIds":['$rubezhid']}'
curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/scenarios/$scen1id/time_line_blocks -H'content-type:application/json;charset=UTF-8' -d'{"blockType":"EXECUTIVE","name":"Block 3","timeDelaySec":10,"conditionType":"NONE","conditionCheckType":"IS_ACTIVE","actions":[{"entityType":"DEVICE","entityId":"'$rm4id'","actionTypeId":"satExecDeviceFlashingOn"}]}'
curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/scenarios/$scen1id/time_line_blocks -H'content-type:application/json;charset=UTF-8' -d'{"blockType":"EXECUTIVE","name":"Block 1","timeDelaySec":0,"conditionType":"NONE","conditionCheckType":"IS_ACTIVE","actions":[{"entityType":"REGION","entityId":"'$sec1id'","actionTypeId":"satSecRegionGuardOn"}]}'
curl -sk -b cookie-jar -X POST $url/api/v1/projects/$projectid/scenarios/$scen1id/time_line_blocks -H'content-type:application/json;charset=UTF-8' -d'{"blockType":"EXECUTIVE","name":"Block 2","timeDelaySec":5,"conditionType":"NONE","conditionCheckType":"IS_ACTIVE","actions":[{"entityType":"VIRTUAL_STATE","entityId":"'$virtstate1id'","actionTypeId":"satVirtualStateSetActive"}]}'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/scenarios/$scen1id?advanced_params -H'content-type:application/json;charset=UTF-8' -d'{"manualStartStopAllowed":true,"stopOnGlobalReset":false,"stopType":"AUTO","startLogicSentence":"","stopLogicSentence":null}'


#Планы

#Отрисовка зон
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/region_views/$sec1id?plan_layouts -H'content-type:application/json;charset=UTF-8' -d'[{"planId":"'$plan1id'","rect":true,"points":[{"x":20,"y":90},{"x":220,"y":290}]}]'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/region_views/$fire1id?plan_layouts -H'content-type:application/json;charset=UTF-8' -d'[{"planId":"'$plan1id'","rect":true,"points":[{"x":250,"y":90},{"x":450,"y":290}]}]'
#Отрисовка устройств
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$am1id?plan_layouts -H'content-type:application/json;charset=UTF-8' -d'[{"planId":"'$plan1id'","coordinatePoint":{"x":100,"y":190}}]'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$am2id?plan_layouts -H'content-type:application/json;charset=UTF-8' -d'[{"planId":"'$plan1id'","coordinatePoint":{"x":300,"y":190}}]'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/devices/$am3id?plan_layouts -H'content-type:application/json;charset=UTF-8' -d'[{"planId":"'$plan1id'","coordinatePoint":{"x":350,"y":190}}]'
#Подложка
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/plans/$plan1id -H'content-type:application/json;charset=UTF-8' -d'{"id":"'$plan1id'","name":"Plan 1","description":"","projectId":"'$projectid'","planGroupId":null,"subsystem":"GENERAL","xSize":500,"ySize":500,"backgrounds":[{"topLeftPoint":{"x":0,"y":0},"svgContent":"<svg width=\"123\" height=\"109\" xmlns=\"http://www.w3.org/2000/svg\" style=\"vector-effect: non-scaling-stroke;\" stroke=\"null\">\n <!-- Created with Method Draw - http://github.com/duopixel/Method-Draw/ -->\n <g stroke=\"null\">\n  <title stroke=\"null\">background</title>\n  <rect stroke=\"null\" fill=\"none\" id=\"canvas_background\" height=\"111\" width=\"125\" y=\"-1\" x=\"-1\"/>\n  <g style=\"vector-effect: non-scaling-stroke;\" stroke=\"null\" display=\"none\" overflow=\"visible\" y=\"0\" x=\"0\" height=\"100%\" width=\"100%\" id=\"canvasGrid\">\n   <rect fill=\"url(#gridpattern)\" stroke-width=\"0\" y=\"0\" x=\"0\" height=\"100%\" width=\"100%\"/>\n  </g>\n </g>\n <g stroke=\"null\">\n  <title stroke=\"null\">Layer 1</title>\n  <rect id=\"svg_3\" height=\"80\" width=\"90\" y=\"13\" x=\"9.5\" fill-opacity=\"null\" stroke-opacity=\"null\" stroke-width=\"3\" stroke=\"#000\" fill=\"none\"/>\n </g>\n</svg>"}],"generalStateCategoryView":{"id":"OffGuard","name":"Sec off","description":"","color":"#4444FF","fontColor":"#000000","iconName":"unlock.svg","regionFillAlpha":0.3,"stateCategory":{"id":"OffGuard","priority":15,"subsystem":"SECURITY","defaultCategory":false}}}'










#Активация
curl -sk -b cookie-jar -X PUT -d '' $url/api/v1/projects/$projectid?status=ACTIVE

sleep 5s

#Запись БД


curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"WRITE_ALL_CONTROL_DEVICES_DATABASE","projectId":"'$projectid'"}'



sleep 2m

#MWF
#Запуск пожара, и сброс пожара
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/active_devices/$am2id?config -H 'content-type:application/json;charset=UTF-8' -d'{"State1":"1"}'
sleep 2s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"WRITE_CONFIG_TO_DEVICE","projectId":"'$projectid'","deviceId":"'$am2id'"}'
curl -sk -b cookie-jar -X PUT '' $url/api/v1/projects/$projectid/active_devices/$am2id?config -H 'content-type:application/json;charset=UTF-8' -d'{"State1":"0"}'
sleep 2s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"WRITE_CONFIG_TO_DEVICE","projectId":"'$projectid'","deviceId":"'$am2id'"}'
sleep 3s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"RESET_STATE_ON_ALL_DEVICES","projectId":"'$projectid'","parameters":{"manualResetStateGroup":"FIRE"}}'
sleep 5s
#Отключение/включение устройства
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"DISABLE_DEVICE_POLLING_STATE","projectId":"'$projectid'","deviceId":"'$rm1id'","parameters":{"regionId":null}}'
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"ENABLE_DEVICE_POLLING_STATE","projectId":"'$projectid'","deviceId":"'$rm1id'","parameters":{"regionId":null}}'
#Запуск сценария MWF из ОЗ
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_SCENARIO_ACTION","projectId":"'$projectid'","parameters":{"scenarioNo":1,"actionId":"START"}}'
sleep 15s
#Сброс тревоги, вызваной сценарием
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"RESET_STATE_ON_ALL_DEVICES","projectId":"'$projectid'","parameters":{"manualResetStateGroup":"ALARM"}}'
#Остановка сценария в ОЗ
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_SCENARIO_ACTION","projectId":"'$projectid'","parameters":{"scenarioNo":1,"actionId":"STOP"}}'
#Чтение конфигурации с прибора
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"READ_CONFIG_FROM_DEVICE","projectId":"'$projectid'","deviceId":'$rubezhid'}'
#Управление устройствами из ОЗ
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_DEVICE_ACTION","projectId":"'$projectid'","deviceId":"'$rm1id'","parameters":{"actionId":"ManualMode"}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_DEVICE_ACTION","projectId":"'$projectid'","deviceId":"'$rm1id'","parameters":{"actionId":"AutoMode"}}'
#Выполнение действиий с индикаторами
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"ENABLE_INDICATOR_REGIONS_GUARD","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel1id',"colNo":0,"rowNo":0,"entityType":"REGION","entityIds":["'$sec1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"DISABLE_INDICATOR_REGIONS_GUARD","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel1id',"colNo":0,"rowNo":0,"entityType":"REGION","entityIds":["'$sec1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"DISABLE_INDICATOR_REGIONS_POLLING_STATE","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel2id',"colNo":1,"rowNo":0,"entityType":"REGION","entityIds":["'$fire1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"ENABLE_INDICATOR_REGIONS_POLLING_STATE","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel2id',"colNo":1,"rowNo":0,"entityType":"REGION","entityIds":["'$fire1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"DISABLE_INDICATOR_DEVICES_POLLING_STATE","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel3id',"colNo":2,"rowNo":0,"entityType":"DEVICE","entityIds":["'$rm1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"ENABLE_INDICATOR_DEVICES_POLLING_STATE","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel3id',"colNo":2,"rowNo":0,"entityType":"DEVICE","entityIds":["'$rm1id'"]}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_INDICATOR_SCENARIO_ACTION","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel4id',"colNo":0,"rowNo":1,"entityType":"SCENARIO","entityIds":["'$scen1id'"],"actionId":"BLOCK"}}'
sleep 5s
curl -sk -b cookie-jar -X POST $url/api/v1/issues -H'content-type:application/json;charset=UTF-8' -d'{"action":"PERFORM_INDICATOR_SCENARIO_ACTION","projectId":"'$projectid'","parameters":{"indicatorPanelId":'$indpanel4id',"colNo":0,"rowNo":1,"entityType":"SCENARIO","entityIds":["'$scen1id'"],"actionId":"UNBLOCK"}}'


#Запрос issues
sleep 10s
curl -sk -b cookie-jar -X GET $url/api/v1/issues | jq '.[] | {action, status}' > issues.json
#Запрос последних 50 событий журнала с фильтром по имени события, устройству, зоне, вирт.сост. (если такие есть)
curl -sk -b cookie-jar -X GET ''$url'/api/v1/events?size=80&page=0&sort=received,desc' | jq '.content[] | {event: .name, device: .deviceEventInfo.name, region: .regionEventInfo.name, virtualstate: .virtualStateEventInfo.name}' > events.json

#Проверка всех issue и необходимых событий
#Обязательные события и завершенные issue
finished="$(grep -o "FINISHED" issues.json | wc -l)"
fire1onevent="$(grep -o "Пожар-1" events.json | wc -l)"
fire1offevent="$(grep -o "Сброс пожара выполнен" events.json | wc -l)"
writeinevent="$(grep -o "Запись конфигурации в устройство" events.json | wc -l)"
userevent="$(grep -o "Сброс состояния: пожар" events.json | wc -l)"
deviceoffevent="$(grep -o "Устройство отключено" events.json | wc -l)"
deviceonevent="$(grep -o "Устройство задействовано" events.json | wc -l)"
scenonevent="$(grep -o "Сценарий включен" events.json | wc -l)"
zoneonsecevent="$(grep -o "Зона поставлена на охрану" events.json | wc -l)"
virtstateonevent="$(grep -o "State 1 on" events.json | wc -l)"
executiveblinkonevent="$(grep -o "Исполнительное устройство включено в режиме мерцания" events.json | wc -l)"
seczonealarmevent="$(grep -o "Тревога" events.json | wc -l)"
readconfigevent="$(grep -o "Чтение конфигурации с устройства" events.json | wc -l)"
scenoffevent="$(grep -o "Сценарий выключен" events.json | wc -l)"
virtstateoffevent="$(grep -o "State 1 off" events.json | wc -l)"
executiveoffevent="$(grep -o "Исполнительное устройство выключено" events.json | wc -l)"
resetalarmevent="$(grep -o "Сброс тревоги выполнен" events.json | wc -l)"
zoneoffsecevent="$(grep -o "Зона снята с охраны" events.json | wc -l)"
rmmanualmodeevent="$(grep -o "Ручной режим" events.json | wc -l)"
rmautomodeevent="$(grep -o "Автоматический режим" events.json | wc -l)"
indonsecevent="$(grep -o "Постановка зон индикатора на охрану" events.json | wc -l)"
indoffsecevent="$(grep -o "Снятие зон индикатора с охраны" events.json | wc -l)"
indoffdeviceregionevent="$(grep -o "Отключение всех датчиков зоны у индикатора" events.json | wc -l)"
indondeviceregionevent="$(grep -o "Опрос всех датчиков зон у индикатора" events.json | wc -l)"
indoffdeviceevent="$(grep -o "Отключение опроса всех устройств индикатора" events.json | wc -l)"
indondeviceevent="$(grep -o "Опрос всех устройств индикатора" events.json | wc -l)"
indblockscenevent="$(grep -o "Выполнение действия сценария у индикатора" events.json | wc -l)"
blockscenevent="$(grep -o "Сценарий заблокирован" events.json | wc -l)"
unblockscenevent="$(grep -o "Сценарий разблокирован" events.json | wc -l)"
okfinished="20"
okevents="1"

#Вывод MWF OK если все issue выполнены, в журнале есть все события, заданные выше/MWF FAILED если отсутствуют события или выполненные issue 
if [ "$finished" -ge "$okfinished" ] && [ "$fire1onevent" -ge "$okevents" ] && [ "$fire1offevent" -ge "$okevents" ] && [ "$writeinevent" -ge "$okevents" ] && [ "$userevent" -ge "$okevents" ] && [ "$deviceoffevent" -ge "$okevents" ] && [ "$deviceonevent" -ge "$okevents" ] && [ "$scenonevent" -ge "$okevents" ] && [ "$zoneonsecevent" -ge "$okevents" ] && [ "$virtstateonevent" -ge "$okevents" ] && [ "$executiveblinkonevent" -ge "$okevents" ] && [ "$seczonealarmevent" -ge "$okevents" ] && [ "$readconfigevent" -ge "$okevents" ] && [ "$scenoffevent" -ge "$okevents" ] && [ "$virtstateoffevent" -ge "$okevents" ] && [ "$executiveoffevent" -ge "$okevents" ] && [ "$resetalarmevent" -ge "$okevents" ] && [ "$zoneoffsecevent" -ge "$okevents" ] && [ "$rmmanualmodeevent" -ge "$okevents" ] && [ "$rmautomodeevent" -ge "$okevents" ] && [ "$indonsecevent" -ge "$okevents" ] && [ "$indoffsecevent" -ge "$okevents" ] && [ "$indoffdeviceregionevent" -ge "$okevents" ] && [ "$indondeviceregionevent" -ge "$okevents" ] && [ "$indoffdeviceevent" -ge "$okevents" ] && [ "$indondeviceevent" -ge "$okevents" ] && [ "$indblockscenevent" -ge "$okevents" ] && [ "$blockscenevent" -ge "$okevents" ] && [ "$unblockscenevent" -ge "$okevents" ]; then
    echo -en "\033[1;42m MWF OK \033[0m"
else 
    echo -en "\033[1;41m MWF FAILED \033[0m"
fi

