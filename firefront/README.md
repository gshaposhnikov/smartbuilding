Сборка клиента firesec
======================


Подготовка Ubuntu
-----------------

Установить NodeJS и зависимости

    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt-get install -y nodejs libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev build-essential g++

Установить менеджер пакетов yarn

    sudo npm install -g yarn

Перейти в каталог `firefront` и установить зависимости проекта

    yarn


Подготовка Windows
------------------

Скачать и установить [дистрибутив](https://nodejs.org/en/)

После установки запустить "Node.js command promt"

Установить менеджер пакетов yarn

    npm install -g yarn

Перейти в каталог `firefront` и установить зависимости проекта

    yarn


Сборка production-версии
------------------------

*Опционально*: для отключения авторизации нужно в файле `config/app.js` установить соответствующее значение для параметра `SECURITY_ON`

    var SECURITY_ON = false

Собрать проект

    yarn build

Запуск версии для разработчиков
-------------------------------

    yarn start
