/*********** Настройки API ***********/
var API_URL = null // {string} адрес API; null, чтобы обращаться к локальному серверу
var SECURITY_ON = true

var PROTOCOL = SECURITY_ON ? 'https' : 'http'; // при включенной авторизации переключиться на https

/*********** Настройки интерфейса ***********/

var NOTIFY_SUCCESS_DURATION = 2; // В секундах
var NOTIFY_ERROR_DURATION = 4;
var NOTIFY_LOADING_DURATION = 10;

/*********** Настройки Redux-devtools ***********/
var REDUX_DEVTOOLS_ON = false

/*********** Экспорт ***********/
module.exports = {
  protocol: PROTOCOL,
  apiUrl: API_URL,
  securityOn: SECURITY_ON,

  notifySuccessDuration: NOTIFY_SUCCESS_DURATION,
  notifyErrorDuration: NOTIFY_ERROR_DURATION,
  notifyLoadingDuration: NOTIFY_LOADING_DURATION,

  reduxDevtoolsOn: REDUX_DEVTOOLS_ON
}
