import React from 'react';
import 'pixi.js';
import styled from 'styled-components';

import addWheelListener from 'helpers/addWheelListener';
import { getDeviceStatus } from 'helpers/device.js';

const PlanViewerWrapper = styled.div`
  flex: 1;
`;

PlanViewerWrapper.defaultProps = {
  id: 'plan-viewer'
}
export default class PlanViewer extends React.Component {

  regionColors = {
    'lost': 'DDAAAA',
    'warning': 'FF7F27',
    'fire': 'FF0000',
    'normal': '0000FF'
  }

  init(){
    this.PIXI = window.PIXI;
    this.canvas = document.getElementById('plan-viewer');

    this.app = new this.PIXI.Application(
      1582, 508,
      {antialias: false, transparent: true, resolution: 1, backgroundColor: 0xFFFFFF}
    );

    this.PIXI.settings.SCALE_MODE = this.PIXI.SCALE_MODES.NEAREST;

    this.zoomLevel = 1;

    this.rootContainer = new this.PIXI.Container();
    this.rootContainer.x = (this.app.renderer.width - this.rootContainer.width) / 2;
    this.rootContainer.y = (this.app.renderer.height - this.rootContainer.height) / 2;

    this.canvas.appendChild(this.app.view);
    this.app.stage.addChild(this.rootContainer);
  }

  loadSprites(){
    const { PIXI } = this;
    this.textures = {
      "fireDetector": PIXI.Texture.fromImage('/img/device/device.svg'),
      "panelSimulator": PIXI.Texture.fromImage('/img/device/panel.svg'),

      "fail": PIXI.Texture.fromImage('/img/status/fail.svg'),
      "fire": PIXI.Texture.fromImage('/img/status/fire.svg'),
      "lost": PIXI.Texture.fromImage('/img/status/lost.svg'),
      "nd": PIXI.Texture.fromImage('/img/status/nd.svg'),
      "warning": PIXI.Texture.fromImage('/img/status/warning.svg')
    };
  }

  setupStage() {
    const { PIXI } = this;

    this.regions = [];
    this.devices = [];

    this.powerSystem = new PIXI.Container();

    this.initRegions();
    this.initDevices();
    this.rootContainer.addChild(this.powerSystem);
  }

  initRegions(){
    this.props.plan.regions.forEach(region => {
      let graphics = new this.PIXI.Graphics();

      graphics.xStart = Math.round(region.x);
      graphics.yStart = Math.round(region.y);
      graphics.xEnd = Math.round(region.xSize);
      graphics.yEnd = Math.round(region.ySize);

      const status = getDeviceStatus(region.fireStatus, region.deviceStatus);
      graphics.color = this.regionColors[status];

      this.regions.push(graphics);

      this.renderRegion(graphics);
      this.powerSystem.addChild(graphics);
    })
  }

  reRenderRegions(){
    this.regions.forEach(region => {
      this.renderRegion(region);
    })
  }

  renderRegion(region){

    region.clear();
    region.beginFill(`0x${region.color}`, 0.9);
    region.drawRect(
      region.xStart * this.zoomLevel,
      region.yStart * this.zoomLevel,
      region.xEnd * this.zoomLevel,
      region.yEnd * this.zoomLevel
    );
    region.endFill();
  }

  initDevices() {
    this.props.devices.forEach(device => {
      let status = getDeviceStatus(device.fireStatus, device.deviceStatus)
      let newDevice = new this.PIXI.Container();

      let newStatusSprite = new this.PIXI.Sprite(this.textures[status]);
      let newDeviceSprite = new this.PIXI.Sprite(this.textures[device.deviceType]);

      newDeviceSprite.anchor.set(0.5);
      newStatusSprite.anchor.set(0.5);

      newDevice.regionNo = device.regionNo;

      const parentRegionX =  Math.round(this.props.plan.regions[device.regionNo].x);
      const parentRegionY =  Math.round(this.props.plan.regions[device.regionNo].y);

      newDevice.defaultX = parentRegionX + device.x;
      newDevice.defaultY = parentRegionY + device.y;
      newDevice.x = newDevice.defaultX * this.zoomLevel;
      newDevice.y = newDevice.defaultY * this.zoomLevel;


      newStatusSprite.x -= 22;

      newDevice.addChild(newDeviceSprite);
      newDevice.addChild(newStatusSprite);

      this.devices.push(newDevice);
      this.powerSystem.addChild(newDevice);
    });
  }

  reRenderMarkersOnZoom(){
    this.devices.forEach(device => {
      device.x = device.defaultX * this.zoomLevel;
      device.y = device.defaultY * this.zoomLevel;
    })
  }
  addDragAndDrop(){
    let isDragging = false;
    let prevX, prevY;

    const interactionManager = new this.PIXI.interaction.InteractionManager(this.app);

    interactionManager.on("pointerdown", (e) => {
      let pos = e.data.global;
      prevX = pos.x; prevY = pos.y;
      isDragging = true;
    })

    interactionManager.on("pointermove", (e) => {
      if (!isDragging) {
        return;
      }
      var pos = e.data.global;
      var dx = pos.x - prevX;
      var dy = pos.y - prevY;

      this.rootContainer.position.x += dx;
      this.rootContainer.position.y += dy;
      prevX = pos.x; prevY = pos.y;
    })

    interactionManager.on("pointerup", (e) => {
      isDragging = false;
    })
  }

  addZoom(){
    addWheelListener(this.canvas, e => {
      this.zoom(e.layerX, e.layerY, e.deltaY < 0)
    })
  }

  zoom(cursorX, cursorY, isZoomIn){
    const zoomSpeed = 2;
    let dx = cursorX - this.rootContainer.x;
    let dy = cursorY - this.rootContainer.y;
    if(isZoomIn){
      this.zoomLevel *= zoomSpeed;
      this.reRenderRegions();
      this.reRenderMarkersOnZoom();
      this.rootContainer.x -= dx;
      this.rootContainer.y -= dy;
    }
    else {
      if(this.zoomLevel > 1) {
        this.zoomLevel /= zoomSpeed;
        this.reRenderRegions();
        this.reRenderMarkersOnZoom();
        dx /= zoomSpeed;
        dy /= zoomSpeed;
        this.rootContainer.x += dx;
        this.rootContainer.y += dy;
      }
    }
  }

  componentDidMount(){
    this.init();
    this.loadSprites();
    this.setupStage();
    this.addDragAndDrop();
    this.addZoom();
  }

  componentDidUpdate() {
    let rootOldX = this.rootContainer.x;
    let rootOldY = this.rootContainer.y;

    this.app.stage = new this.PIXI.Container();
    this.rootContainer = new this.PIXI.Container();

    this.rootContainer.x = rootOldX;
    this.rootContainer.y = rootOldY;

    this.app.stage.addChild(this.rootContainer);

    this.setupStage();
  }
  render() {
    return <PlanViewerWrapper />
  }
}
