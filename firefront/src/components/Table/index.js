import React, { Component } from 'react';
import { Table } from 'antd';
import EditableCell from './EditableCell';

import styled, { css } from 'styled-components';

const TableWrapper = styled.div`
  width: 100%;
  flex: 1;
  background: white;

  td {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  & .ant-table-wrapper {
    height: auto !important;
  }
  & .ant-table-middle .ant-table-thead > tr > th:not(.ant-table-selection-column),
  .ant-table-middle .ant-table-tbody > tr > td {
    max-width: 0px;
    padding: 4px 8px;
    border-right: 1px solid #e9e9e9;
  }
  & .ant-table-middle .ant-table-tbody > tr > td {
    padding: 0px 8px 1px 8px;
    border-right: 1px solid #e9e9e9;
  }
  & .ant-table-body {
    max-height: auto !important;
  }
  & .logTd {
    background: red;
  }
  & .hidden-expand {
    & .ant-table-row-expand-icon {
      display: none;
    }
  }
  & .ant-table-selection-column {
    display: none;
  }
  & .type-column {
    width: 300px;
    min-width: 300px;
  }

  & .editable-cell-input-wrapper .ant-input {
    width: 100px;
  }

  ${p =>
    p.className
      ? css`
          td {
            padding: 0 !important;
            height: 1px !important;
          }
        `
      : null} ${p => p.css};
`;

class TableComponent extends Component {
  render() {
    const { className, wrapperCss, tableStyle, ...restProps } = this.props;
    return (
      <TableWrapper css={wrapperCss} className={className}>
        <Table
          style={{ width: '100%', height: '100%', minHeight: '400px', ...tableStyle }}
          size="middle"
          pagination={false}
          {...restProps}
        />
      </TableWrapper>
    );
  }
}

const { Column } = Table;
export { Column, EditableCell };
export default TableComponent;
