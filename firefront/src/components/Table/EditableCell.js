import React, { Component } from 'react';
import { Input } from 'antd';
import styled from 'styled-components';

const EditableCellTextWrapper = styled.div`
  cursor: ${p => (p.editable ? 'pointer' : 'default')};
  color: ${p => (p.editable ? '#108ee9' : 'rgba(0, 0, 0, 0.65)')};
  display: inline-flex;
`;

/**
 * @param {string} this.props.temp - Значение, которое отображается только в режиме редактирования, если оно есть
 * @param {string} this.props.value - Значение, которое отображается в режиме просмотра
 * @param {boolean} this.props.editable - Возможность редактирования
 **/

export default class EditableCell extends Component {
  state = {
    editableValue: this.props.temp || this.props.value,
    editMode: false
  };
  componentWillReceiveProps(newProps) {
    this.setState({ editableValue: newProps.temp || newProps.value });
  }
  handleChange = e => {
    const editableValue = e.target.value;
    this.setState({ editableValue });
  };
  check = () => {
    this.setState({ editMode: false });
    if (this.props.onChange) {
      this.props.onChange(this.state.editableValue);
    }
  };
  edit = () => {
    this.setState({ editMode: true });
  };
  cancel = () => {
    const { value } = this.props;
    this.setState({
      editableValue: value,
      editMode: false
    });
  };
  render() {
    const { editable, value } = this.props;
    const { editableValue, editMode } = this.state;
    return (
      <div className="editable-cell">
        {editMode ? (
          <div className="editable-cell-input-wrapper">
            <Input
              ref={input => (this.nameinput = input)}
              value={editableValue}
              onChange={this.handleChange}
              onPressEnter={this.check}
              onBlur={this.cancel}
              autoFocus={true}
              size="small"
            />
          </div>
        ) : (
          <EditableCellTextWrapper
            editable={editable}
            className="editable-cell-text-wrapper"
            onClick={editable ? this.edit : ''}
          >
            {value || ' '}
          </EditableCellTextWrapper>
        )}
      </div>
    );
  }
}
