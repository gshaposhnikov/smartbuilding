import React from 'react';
import { connect } from 'react-redux';

class ConnectedRow extends React.Component {
  render = () => {
    const { children, className, node, onClick, onDoubleClick, item } = this.props;
    return (
      <div
        className={className}
        onClick={e => (onClick ? onClick(e, node) : null)}
        onDoubleClick={e => (onDoubleClick ? onDoubleClick(e, node) : null)}
      >
        {children(node, item)}
      </div>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  if (ownProps.getEntitiesHash) {
    const { getEntitiesHash, node } = ownProps;
    const entitiesHash = getEntitiesHash(state) || {};
    return {
      node: entitiesHash[node.id] || node,
      item: node
    };
  }

  return {};
};

export default connect(mapStateToProps)(ConnectedRow);
