import styled from 'styled-components';

export const TableWrapper = styled.div`
  .react-grid-Main {
    font-size: 12px !important;
  }
  .react-grid-HeaderCell {
    padding: 3px 8px !important;
    background: #f7f7f7 !important;
    font-weight: normal !important;
    border-color: #e9e9e9 !important;
  }
  .react-grid-Cell {
    border-color: #e9e9e9 !important;
  }
  .react-grid-Cell:hover {
    background: #ecf6fd !important;
  }
  .react-grid-HeaderCell .react-grid-checkbox-container {
    display: none;
  }

  /* Стили для редактируемых ячеек таблицы */
  .editable-cell {
    position: relative;
  }

  .editable-cell-input-wrapper,
  .editable-cell-text-wrapper {
    padding-right: 24px;
  }

  .editable-cell-text-wrapper {
    padding: 5px 24px 5px 5px;
  }

  .editable-cell-icon,
  .editable-cell-icon-check {
    position: absolute;
    right: 0;
    width: 20px;
    cursor: pointer;
  }

  .editable-cell-icon {
    line-height: 18px;
    display: none;
  }

  .editable-cell-icon-check {
    line-height: 28px;
  }

  .editable-cell:hover .editable-cell-icon {
    display: inline-block;
  }

  .editable-cell-icon:hover,
  .editable-cell-icon-check:hover {
    color: #108ee9;
  }

  .editable-add-btn {
    margin-bottom: 8px;
  }
`;
