import React, { Component } from 'react';

/**
 * Текстовое поле с тултипом
 * @param {string} text
 * @param {string} title
 */
export default class TextCell extends Component {
  render() {
    const { text, title, children } = this.props;
    return (
      <span className="text-cell" title={title || text || ''}>
        {text || children}
      </span>
    );
  }
}
