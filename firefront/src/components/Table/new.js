import React, { Component } from 'react';
import * as s from './styles';
import ReactDataGrid from 'react-data-grid';

import DefaultContextMenu from 'containers/ContextMenus/EmptyContextMenu';

import { isEmpty } from 'lodash';

export default class CustomTable extends Component {
  static defaultProps = {
    ...Component.defaultProps,
    data: [
      {
        id: '1',
        error: 'Ничего не найдено'
      }
    ],
    columns: [
      {
        key: 'error',
        name: 'Ошибка'
      }
    ],
    contextMenu: DefaultContextMenu,
    enableContextMenu: true
  };
  constructor(props) {
    super(props);

    let { data, columns, contextMenu } = this.props;

    columns = columns.map(e => {
      return { ...e, resizable: true };
    });

    this.state = {
      expanded: {},
      rows: data,
      columns: columns,
      ContextMenu: contextMenu,
      selectedIndexes: []
    };
  }

  onRowClick = (rowId, row) => {
    this.onRowsSelected([{ row: row, rowIdx: rowId }]);
  };

  getRowById = rowId => {
    return this.state.rows[rowId];
  };

  componentWillReceiveProps(nextProps) {
    let { data, currentItem } = nextProps;
    // Если нет используемого элемента, а данные поменялись, сбросить выделение строки таблицы
    this.setState(
      currentItem && isEmpty(currentItem)
        ? {
            selectedIndexes: [],
            rows: data
          }
        : {
            rows: data
          }
    );
  }

  getSubRowDetails = rowItem => {
    let isExpanded = this.state.expanded[rowItem.name] ? this.state.expanded[rowItem.name] : false;
    return {
      group: rowItem.children && rowItem.children.length > 0,
      expanded: isExpanded,
      children: rowItem.children,
      field: 'deviceProfileId',
      treeDepth: rowItem.treeDepth || 0,
      siblingIndex: rowItem.siblingIndex,
      numberSiblings: rowItem.numberSiblings
    };
  };

  onCellExpand = args => {
    let rows = this.state.rows.slice(0);
    let rowKey = args.rowData.name;
    let rowIndex = rows.indexOf(args.rowData);
    let subRows = args.expandArgs.children;

    let expanded = Object.assign({}, this.state.expanded);
    if (expanded && !expanded[rowKey]) {
      expanded[rowKey] = true;
      this.updateSubRowDetails(subRows, args.rowData.treeDepth);
      rows.splice(rowIndex + 1, 0, ...subRows);
    } else if (expanded[rowKey]) {
      expanded[rowKey] = false;
      rows.splice(rowIndex + 1, subRows.length);
    }

    this.setState({ expanded: expanded, rows: rows });
  };

  updateSubRowDetails(subRows, parentTreeDepth) {
    let treeDepth = parentTreeDepth || 0;
    subRows.forEach((sr, i) => {
      sr.treeDepth = treeDepth + 1;
      sr.siblingIndex = i;
      sr.numberSiblings = subRows.length;
    });
  }

  onRowsSelected = rows => {
    const { onSelection } = this.props;
    const currentRegion = rows[0].row;
    onSelection(currentRegion);
    this.setState({
      selectedIndexes: [rows[0].rowIdx]
    });
  };

  onRowsDeselected = rows => {
    const { onSelection } = this.props;
    onSelection({});
    this.setState({ selectedIndexes: [] });
  };

  render() {
    const { rows, columns, ContextMenu } = this.state;
    const { enableRowSelect, enableContextMenu, minHeight, ...rest } = this.props;
    return (
      <s.TableWrapper>
        <ReactDataGrid
          {...rest}
          enableCellSelect={false}
          contextMenu={enableContextMenu ? <ContextMenu data={rows} /> : null}
          columns={columns}
          rowGetter={this.getRowById}
          rowsCount={rows.length}
          rowHeight={25}
          minHeight={minHeight || 500}
          getSubRowDetails={this.getSubRowDetails}
          onCellExpand={this.onCellExpand}
          onRowClick={enableRowSelect ? this.onRowClick : rowId => {}}
          rowSelection={
            enableRowSelect
              ? {
                  showCheckbox: true,
                  onRowsSelected: this.onRowsSelected,
                  onRowsDeselected: this.onRowsDeselected,
                  selectBy: {
                    indexes: this.state.selectedIndexes
                  }
                }
              : null
          }
        />
      </s.TableWrapper>
    );
  }
}
