import React from 'react';
import { Table, Column } from 'react-virtualized';

export default class VirtualStateList extends React.Component {
  renderScenarioGlobalNoCell = record => {
    return <div>{record.globalNo}</div>;
  };

  renderVirtualStateNameCell = record => {
    return <div>{record.name}</div>;
  };

  getRowClassName = record => {
    if (!record) return '';
    const { rowSelections = [] } = this.props;
    if (rowSelections.includes(record.id)) return 'selected-row';
    return '';
  };

  render = () => {
    const {
      width,
      height,
      virtualStates = [],
      onRowDoubleClick = () => {},
      onRowClick = () => {}
    } = this.props;
    return (
      <Table
        width={width}
        height={height}
        rowHeight={26}
        headerHeight={16}
        rowGetter={({ index }) => virtualStates[index]}
        rowCount={virtualStates.length}
        onRowDoubleClick={onRowDoubleClick}
        onRowClick={onRowClick}
        rowClassName={({ index }) => this.getRowClassName(virtualStates[index])}
      >
        <Column
          label="Номер"
          dataKey="globalNo"
          key="globalNo"
          width={60}
          cellRenderer={({ rowData }) => this.renderScenarioGlobalNoCell(rowData)}
        />
        <Column
          label="Название"
          dataKey="name"
          key="name"
          width={width - 60}
          cellRenderer={({ rowData }) => this.renderVirtualStateNameCell(rowData)}
        />
      </Table>
    );
  };
}
