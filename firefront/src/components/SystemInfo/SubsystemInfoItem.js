import React from 'react';
import * as s from './styles';

import StatusImg from 'components/Device/StatusImg';
import SubsystemInfoItemCounter from './SubsystemInfoItemCounter';
import SubsystemInfoItemResetButton from './SubsystemInfoItemResetButton';

class SubsystemInfoItem extends React.Component {
  render() {
    const subsystemInfo = this.props.subsystemInfo;
    switch (subsystemInfo.subsystem) {
      case 'GENERAL':
        // TODO: Сделать подгрузку иконок категорий состояний и использовать их тут
        return (
          <s.SystemWrapper width="470px">
            <s.Lines>
              <s.LineInfo>
                <s.Info>
                  <StatusImg size="20px" name="run" />
                </s.Info>
                <s.Info>
                  <StatusImg size="20px" name="fail" />
                </s.Info>
                <s.Info>
                  <StatusImg size="20px" name="service" />
                </s.Info>
                <s.Info>
                  <StatusImg size="20px" name="disabled" />
                </s.Info>
                <s.Info>
                  <StatusImg size="20px" name="info" />
                </s.Info>
                {subsystemInfo.name ? (
                  <s.WideInfo statusColor={subsystemInfo.color}>{subsystemInfo.name}</s.WideInfo>
                ) : (
                  <s.WideInfo>Норма</s.WideInfo>
                )}
              </s.LineInfo>
              <s.LineInfo>
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Run} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Malfunction} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Service} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Ignore} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Info} />
                <SubsystemInfoItemResetButton
                  resetActions={subsystemInfo.resetActions}
                  project={this.props.project}
                />
              </s.LineInfo>
            </s.Lines>
          </s.SystemWrapper>
        );

      case 'FIRE':
        return (
          <s.SystemWrapper>
            <s.SystemImg>
              <StatusImg size="30px" name="fire" />
            </s.SystemImg>
            <s.Lines>
              <s.LineInfo>
                {subsystemInfo.name ? (
                  <s.WideInfo
                    statusColor={subsystemInfo.color}
                    statusFontColor={subsystemInfo.fontColor}
                  >
                    {subsystemInfo.name}
                  </s.WideInfo>
                ) : (
                  <s.WideInfo>Норма</s.WideInfo>
                )}
              </s.LineInfo>
              <s.LineInfo>
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Fire} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Fire1} />
                <SubsystemInfoItemResetButton
                  resetActions={subsystemInfo.resetActions}
                  project={this.props.project}
                />
              </s.LineInfo>
            </s.Lines>
          </s.SystemWrapper>
        );

      case 'SECURITY':
        return (
          <s.SystemWrapper>
            <s.SystemImg>
              <StatusImg size="30px" name="security" />
            </s.SystemImg>
            <s.Lines>
              <s.LineInfo>
                {subsystemInfo.name ? (
                  <s.WideInfo statusColor={subsystemInfo.color}>{subsystemInfo.name}</s.WideInfo>
                ) : (
                  <s.WideInfo>Норма</s.WideInfo>
                )}
              </s.LineInfo>
              <s.LineInfo>
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Alarm} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Warning} />
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.OnGuard} />
                <SubsystemInfoItemResetButton
                  resetActions={subsystemInfo.resetActions}
                  project={this.props.project}
                />
              </s.LineInfo>
            </s.Lines>
          </s.SystemWrapper>
        );

      case 'SKUD':
        return (
          <s.SystemWrapper opacity="0.3">
            <s.SystemImg>
              <StatusImg size="30px" name="skud" />
            </s.SystemImg>
            <s.Lines>
              <s.LineInfo>
                {subsystemInfo.name ? (
                  <s.WideInfo statusColor={subsystemInfo.color}>{subsystemInfo.name}</s.WideInfo>
                ) : (
                  <s.WideInfo>Норма</s.WideInfo>
                )}
              </s.LineInfo>
              <s.LineInfo>
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.Alarm} />
                <SubsystemInfoItemResetButton
                  resetActions={subsystemInfo.resetActions}
                  project={this.props.project}
                />
              </s.LineInfo>
            </s.Lines>
          </s.SystemWrapper>
        );

      case 'AUTO':
        return (
          <s.SystemWrapper opacity="0.3">
            <s.SystemImg>
              <StatusImg size="30px" name="auto" />
            </s.SystemImg>
            <s.Lines>
              <s.LineInfo>
                {subsystemInfo.name ? (
                  <s.WideInfo statusColor={subsystemInfo.color}>{subsystemInfo.name}</s.WideInfo>
                ) : (
                  <s.WideInfo>Норма</s.WideInfo>
                )}
              </s.LineInfo>
              <s.LineInfo>
                <SubsystemInfoItemCounter counter={subsystemInfo.counters.AutoOff} />
                <SubsystemInfoItemResetButton
                  resetActions={subsystemInfo.resetActions}
                  project={this.props.project}
                />
              </s.LineInfo>
            </s.Lines>
          </s.SystemWrapper>
        );

      default:
        return <div />;
    }
  }
}

export default SubsystemInfoItem;
