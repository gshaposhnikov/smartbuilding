import styled from 'styled-components';

export const SystemInfoWrapper = styled.div`
  display: flex;
`;

export const SystemWrapper = styled.div`
  display: flex;
  width: ${props => (props.width ? props.width : '320px')};
  border: 2px solid #ccc;
  opacity: ${props => props.opacity || '1'};
`;

export const Lines = styled.div`
  height: 100%;
  flex: 1;
`;
export const LineInfo = styled.div`
  display: flex;
  height: 50%;
`;

export const Info = styled.div`
  background-color: ${props =>
    props.statusColor ? props.statusColor : props.theme.colors.background};
  color: ${props => (props.statusFontColor ? props.statusFontColor : 'black')};
  border: 1px solid ${props => props.theme.colors.border};
  width: 63px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: bold;
`;

export const WideInfo = styled(Info)`
  flex: 1;
`;

export const SystemImg = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 50px;
`;
