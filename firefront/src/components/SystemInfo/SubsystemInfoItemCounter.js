import React from 'react';
import * as s from './styles';

class SystemInfoItemCounter extends React.Component {
  render() {
    if (this.props.counter) {
      return <s.Info statusColor={this.props.counter.color}>{this.props.counter.value}</s.Info>
    } else {
      return <s.Info>0</s.Info>
    }
  }
}

export default SystemInfoItemCounter;
