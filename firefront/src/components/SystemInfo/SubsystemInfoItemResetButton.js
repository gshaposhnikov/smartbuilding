import React from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';

import { resetDevicesStates } from 'actions/devices';

import { getCurrentUser } from 'helpers/user';

class SubsystemInfoItemResetButton extends React.Component {
  constructor(props) {
    super(props);
    this.selectValue = { value: 'TITLE', name: 'Сброс' };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = event => {
    const { currentUser } = this.props;
    const actionKey = event.target.value;
    if (currentUser && currentUser.filterTagsOn) {
      switch (actionKey) {
        case 'MALFUNCTIONS':
        case 'TESTS': {
          Modal.confirm({
            title: 'Подтверждение',
            content: 'Могут быть сброшены состояния устройств, которые Вам не видны. Продолжить?',
            onCancel: () => {},
            cancelText: 'Отмена',
            maskClosable: false,
            okText: 'Продолжить',
            onOk: () => {
              this.props.dispatch(resetDevicesStates(this.props.project.id, actionKey));
            }
          });
          break;
        }
        case 'SCENARIOS': {
          Modal.confirm({
            title: 'Подтверждение',
            content: 'Могут быть сброшены состояния сценариев, которые Вам не видны. Продолжить?',
            onCancel: () => {},
            cancelText: 'Отмена',
            maskClosable: false,
            okText: 'Продолжить',
            onOk: () => {
              this.props.dispatch(resetDevicesStates(this.props.project.id, actionKey));
            }
          });
          break;
        }
        default: {
          this.props.dispatch(resetDevicesStates(this.props.project.id, actionKey));
          break;
        }
      }
    } else {
      this.props.dispatch(resetDevicesStates(this.props.project.id, actionKey));
    }
  };

  render() {
    if (Object.keys(this.props.resetActions).length > 0) {
      let resetActionsList = Object.keys(this.props.resetActions).map(resetActionKey => (
        <option key={resetActionKey} value={resetActionKey}>
          {this.props.resetActions[resetActionKey]}
        </option>
      ));
      return (
        <select value={this.selectValue.value} onChange={this.handleChange}>
          <option disabled hidden value={this.selectValue.value}>
            {this.selectValue.name}
          </option>
          {resetActionsList}
        </select>
      );
    } else {
      return <div />;
    }
  }
}

const mapStateToProps = state => {
  return { currentUser: getCurrentUser(state) };
};

export default connect(mapStateToProps)(SubsystemInfoItemResetButton);
