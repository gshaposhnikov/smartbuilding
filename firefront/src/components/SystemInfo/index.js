import React from 'react';
import * as s from './styles';
import SubsystemInfoItem from './SubsystemInfoItem';

class SystemInfo extends React.Component {
  render() {
    return (
      <s.SystemInfoWrapper>
        {
            this.props.status.map((subsystemStatus, key) => {
                return <SubsystemInfoItem key={key}
                    subsystemInfo={subsystemStatus}
                    project={this.props.project}
                />
            })
        }

      </s.SystemInfoWrapper>
    )
  }
}

export default SystemInfo;
