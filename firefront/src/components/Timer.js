import React from 'react';
import styled from 'styled-components';

import Table, { Column } from 'components/Table';
import Card from 'components/Card';
import Draggable from 'components/Draggable';

const TableWrapper = styled.div`
  max-height: 217px;
  overflow: hidden;
`;
const COMPONENT_CLASS_NAME = 'op-timer';
const MAIN_WRAPPER_STYLES = {
  position: 'absolute',
  zIndex: 1000,
  top: '15%',
  left: '35%',
  width: 550
};
const CARD_STYLES = { maxHiehgt: 250, overflow: 'hidden' };
const TABLE_SCROLL = { y: 217 };

const Timer = ({ visibleTimers, timeResidue }) => (
  <div className={COMPONENT_CLASS_NAME} style={MAIN_WRAPPER_STYLES}>
    <Card
      title={
        <Draggable
          visible={true}
          title="Отсчет времени"
          componentClassName={COMPONENT_CLASS_NAME}
        />
      }
      style={CARD_STYLES}
    >
      <TableWrapper>
        <Table dataSource={visibleTimers} scroll={TABLE_SCROLL} rowKey={record => record.id}>
          <Column title="Название" dataIndex="name" />
          <Column
            title="Время"
            dataIndex="delay"
            render={(text, record) => {
              return (
                <span>{record.state === 'started' ? timeResidue[record.id] || text : text}</span>
              );
            }}
          />
        </Table>
      </TableWrapper>
    </Card>
  </div>
);

export default Timer;
