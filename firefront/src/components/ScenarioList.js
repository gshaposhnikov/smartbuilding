import React, { Component } from 'react';
import { Icon } from 'antd';
import { ContextMenuTrigger } from 'react-contextmenu';

import Table, { Column } from 'components/Table';

export default class ScenarioList extends Component {
  getRowClassName(record) {
    const { currentScenarioId } = this.props;
    if (record.id === currentScenarioId) return 'selected-row';
    return '';
  }

  renderNameCell(text, record) {
    const { contextMenuName, contextMenuEnabled, isActiveScenarios } = this.props;
    return contextMenuEnabled &&
      record.advancedParams &&
      record.advancedParams.manualStartStopAllowed ? (
      <ContextMenuTrigger
        holdToDisplay={-1}
        record={record}
        collect={props => props}
        id={contextMenuName}
      >
        <div
          style={{
            color: isActiveScenarios ? record.generalStateCategoryView.fontColor : record.fontColor
          }}
        >
          {text}
        </div>
      </ContextMenuTrigger>
    ) : (
      <div
        style={{
          color: isActiveScenarios ? record.generalStateCategoryView.fontColor : record.fontColor
        }}
      >
        {text}
      </div>
    );
  }

  render() {
    const { scenarios, onRowClick, scroll, isActiveScenarios } = this.props;
    return (
      <Table
        dataSource={scenarios}
        rowKey={record => record.id}
        onRowClick={onRowClick}
        scroll={scroll}
        rowClassName={this.getRowClassName.bind(this)}
      >
        <Column
          title="№"
          width={30}
          dataIndex="globalNo"
          key="globalNo"
          render={(text, record) => (
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Icon
                type="clock-circle"
                style={{
                  color: isActiveScenarios ? record.generalStateCategoryView.color : record.color
                }}
              />
              <div
                style={{
                  color: isActiveScenarios
                    ? record.generalStateCategoryView.fontColor
                    : record.fontColor
                }}
              >{`${text}`}</div>
            </div>
          )}
        />
        <Column
          title="Название"
          width={150}
          dataIndex="name"
          key="name"
          render={this.renderNameCell.bind(this)}
        />
        <Column title="Примечание" width={300} dataIndex="description" key="description" />
      </Table>
    );
  }
}
