import React, { PropTypes, Component } from 'react';
import styled from 'styled-components';
import { Tooltip } from 'antd';
import { NavLink } from 'react-router-dom'

import Icon from 'components/Icon';

const LinkWrapper = styled.div`
  & .active div{
    background-color: #FFEFBB;
  }
`;

const LinkBlock = styled.div`
  display: flex;
  width: 44px;
  height: 44px;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.isActive ? '#FFEFBB' : null};

  &:hover {
    background-color: #e6e6e6;
  }
`;

export default class SideNavItem extends Component {
  static propTypes = {
    icon: PropTypes.string,
    link: PropTypes.string,
    title: PropTypes.string
  }
  render() {
    const { icon, link, title, onClick } = this.props;
    return <LinkWrapper>
      <NavLink to={link} activeClassName="active" onClick={onClick}>
      <Tooltip placement="right" title={title}>
        <LinkBlock>
          <Icon name={icon} color="#5FA2DD" />
        </LinkBlock>
        </Tooltip>
      </NavLink>
    </LinkWrapper>
  }
}