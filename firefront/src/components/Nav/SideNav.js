import React, { PropTypes, Component } from 'react';
import styled from 'styled-components';

const SideNavWrapper = styled.div`
    width: 44px;
    background: #F6F6F6;
`;
const SideNavList = styled.div``;


export default class SideNav extends Component {
  static propTypes = {
    icon: PropTypes.func,
  }

  render() {
    return <SideNavWrapper>
      <SideNavList>
        {this.props.children}
      </SideNavList>
    </SideNavWrapper>
  }
}
