import styled from 'styled-components';

const Text = styled.div`
  padding: 10px 15px;
  font-size: 14px;
`;

export default Text;
