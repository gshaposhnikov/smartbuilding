import React, { Component } from 'react';

export default class CheckableText extends Component {
  constructor(props) {
    super(props);

    this.state = {
      background: 'white'
    };
  }

  onMouseEnter() {
    this.setState({
      ...this.state,
      background: 'AliceBlue'
    });
  }

  onMouseLeave() {
    this.setState({
      ...this.state,
      background: 'White'
    });
  }

  render() {
    const { children, checked, onClick } = this.props;
    const { background } = this.state;
    return (
      <span
        style={{ background: checked ? 'AliceBlue' : background, color: 'CornflowerBlue' }}
        onClick={onClick}
        onMouseEnter={this.onMouseEnter.bind(this)}
        onMouseLeave={this.onMouseLeave.bind(this)}
      >
        {children}
      </span>
    );
  }
}
