import React, { Component } from 'react';

import { TimePicker } from 'antd';
import moment from 'moment';

export const UNITS = {
  MILLISECONDS: 'm',
  SECONDS: 's',
  MINUTES: 'M',
  HOURS: 'H'
};

export default class TimePickerToForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accepableValue: null
    };
  }

  value2Acceptable(props) {
    const { input: { value }, units } = props;
    if (moment.isMoment(value)) {
      this.setState({ accepableValue: value });
    } else {
      let accepableValue = null;
      switch (units) {
        case UNITS.SECONDS:
          if (Number.isInteger(value) && value >= 0) accepableValue = moment.unix(value).utc();
          break;
        case UNITS.MINUTES:
          if (Number.isInteger(value) && value >= 0) accepableValue = moment.unix(value * 60).utc();
          break;
        case UNITS.HOURS:
          if (Number.isInteger(value) && value >= 0)
            accepableValue = moment.unix(value * 3600).utc();
          break;

        default:
          //UNITS.MILLISECONDS, String
          accepableValue = moment(value).utc();
          break;
      }
      if (accepableValue !== this.state.accepableValue) this.setState({ accepableValue });
    }
  }

  componentDidMount() {
    this.value2Acceptable(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.value2Acceptable(newProps);
  }

  onChange(momentValue) {
    const { input: { onChange }, units } = this.props;
    switch (units) {
      case UNITS.SECONDS:
        onChange(momentValue ? momentValue.unix() % (3600 * 24) : -1);
        break;
      case UNITS.MINUTES:
        onChange(momentValue ? Math.round((momentValue.unix() % (3600 * 24)) / 60) : -1);
        break;
      case UNITS.HOURS:
        onChange(momentValue ? Math.round((momentValue.unix() % (3600 * 24)) / 3600) : -1);
        break;
      case UNITS.MILLISECONDS:
        onChange(momentValue ? momentValue.valueOf() % (3600000 * 24) : -1);
        break;

      default:
        //String
        onChange(momentValue ? momentValue.toISOString() : null);
        break;
    }
  }

  render() {
    const { timeFormat, ...restProps } = this.props;
    const { accepableValue } = this.state;

    return (
      <TimePicker
        onChange={this.onChange.bind(this)}
        value={accepableValue}
        defaultOpenValue={moment(0).utc()}
        format={timeFormat || 'HH:mm:ss'}
        {...restProps}
      />
    );
  }
}
