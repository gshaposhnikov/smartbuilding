import React, { Component } from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class MultiSearchSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: [],
      filteredSource: []
    };
  }

  componentDidMount() {
    const { itemsSource, input: { value }, keyFieldName, valueFieldName } = this.props;
    this.setState({ filteredSource: itemsSource });
    if (value && Array.isArray(value)) {
      this.setState({
        value: value.map(key => ({
          key,
          label: itemsSource.find(item => item[keyFieldName ? keyFieldName : 'id'] === key)[
            valueFieldName ? valueFieldName : 'name'
          ]
        }))
      });
    }
  }

  onChange(value) {
    this.setState({ value }, () => {
      const { input } = this.props;
      if (input) {
        const { onChange } = input;
        if (onChange) {
          onChange(value ? value.map(keyAndLabel => keyAndLabel.key) : undefined);
        }
      }
    });
  }

  onSearch(searchText) {
    const { itemsSource, valueFieldName } = this.props;
    const filteredSource = itemsSource.filter(
      item =>
        item[valueFieldName ? valueFieldName : 'name'].match(
          new RegExp('^' + searchText + '.*', 'i')
        )
          ? true
          : false
    );
    this.setState({ filteredSource });
  }

  onBlur() {
    const { itemsSource } = this.props;
    this.setState({ filteredSource: itemsSource });
  }

  render() {
    const { itemsSource, valueFieldName, keyFieldName, ...restProps } = this.props;
    const { value, filteredSource } = this.state;

    if (itemsSource !== undefined) {
      return (
        <Select
          mode="multiple"
          labelInValue
          value={value}
          onChange={this.onChange.bind(this)}
          onSearch={this.onSearch.bind(this)}
          onBlur={this.onBlur.bind(this)}
          filterOption={false}
          notFoundContent="Введите первые буквы"
          {...restProps}
        >
          {filteredSource.map(item => {
            return (
              <Option key={keyFieldName ? item[keyFieldName] : item.id}>
                {valueFieldName ? item[valueFieldName] : item.name}
              </Option>
            );
          })}
        </Select>
      );
    } else {
      return <Select {...restProps} />;
    }
  }
}
