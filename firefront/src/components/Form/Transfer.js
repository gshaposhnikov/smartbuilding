import React, { Component } from 'react';
import styled from 'styled-components';
import { Transfer } from 'antd';

const TransferWrapper = styled.div`
  .ant-transfer-list-header-selected > span:first-child {
      display: none;
  }
`;

export default class TransferToForm extends Component {
  render() {
    const { input: { onChange, value }, dataSource, ...restProps } = this.props;
    return (
      <TransferWrapper>
        <Transfer
          onChange={onChange}
          dataSource={dataSource}
          targetKeys={value}
          {...restProps}
        />
      </TransferWrapper>
    );
  }
}
