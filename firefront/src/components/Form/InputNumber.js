import React, { Component } from 'react';
import { InputNumber } from 'antd';

export default class InputNumberToForm extends Component {
  render() {
    const { input: { onChange, value }, precision = 0, ...restProps } = this.props;
    return <InputNumber precision={precision} onChange={onChange} value={value} {...restProps} />;
  }
}
