import React, { Component } from 'react';

import { Input } from 'antd';

export default class InputToForm extends Component {
  render() {
    const { input: { onChange, value }, ...restProps } = this.props;
    return <Input onChange={onChange} value={value} {...restProps} />;
  }
}
