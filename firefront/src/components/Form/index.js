import React, { Component } from 'react';
import styled from 'styled-components';
import { Form } from 'antd';

import Input from './Input';

const FormWrapper = styled.div`
  .ant-form-item {
    margin-bottom: 10px;
  }
  .ant-btn-primary {
    margin-top: 20px;
    background-color: #5fa2dd;
    border-color: #5fa2dd;
  }
  ${p => p.css};
`;

export default class StyledForm extends Component {
  render() {
    const { children, css, ...restProps } = this.props;
    return (
      <FormWrapper css={css}>
        <Form {...restProps}>{children}</Form>
      </FormWrapper>
    );
  }
}

export const ItemLayoutWithLabel = {
  labelCol: {
    sm: { span: 8 }
  },
  wrapperCol: {
    sm: { span: 14 }
  }
};

export const ItemLayoutWithoutLabel = {
  wrapperCol: {
    sm: { offset: 8, span: 14 }
  }
};

export const FormItem = Form.Item;
export const FormCreate = Form.create;
export const FormInput = Input;
