import React, { Component } from 'react';

import { Select } from 'antd';

export default class SelectToForm extends Component {
  render() {
    const { input: { onChange }, children, ...restProps } = this.props;

    /* 
     * Костыль для инициализации выпадающего списка на форме
     * пустым массивом вместо пустой строки в режиме tags или multiple,
     * т.к. иначе происходит исключительная ситуация при отрисовке
     */
    const { mode } = this.props;
    var { input: { value } } = this.props;
    if (value === '' && (mode === 'tags' || mode === 'multiple')) {
      value = [];
    }

    return (
      <Select onChange={onChange} value={value} {...restProps}>
        {children}
      </Select>
    );
  }
}

export const { Option } = Select;
