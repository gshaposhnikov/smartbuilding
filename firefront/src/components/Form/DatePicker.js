import React, { Component } from 'react';

import { DatePicker } from 'antd';
import { timeFormat } from 'constants/settings';

import moment from 'moment';

export default class DatePickerToForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accepableValue: null
    };
  }

  componentDidMount() {
    const { input: { value } } = this.props;
    if (moment.isMoment(value)) {
      this.setState({ accepableValue: value });
    } else if (moment(value).isValid()) {
      this.setState({ accepableValue: moment(value) });
    }
  }

  componentWillReceiveProps(newProps) {
    const { input: { value } } = newProps;
    if (moment.isMoment(value)) {
      this.setState({ accepableValue: value });
    } else if (moment(value).isValid()) {
      this.setState({ accepableValue: moment(value) });
    } else {
      this.setState({ accepableValue: null });
    }
  }

  render() {
    const { input: { onChange }, dateTimeFormat, ...restProps } = this.props;
    const { accepableValue } = this.state;

    return (
      <DatePicker
        onChange={onChange}
        value={accepableValue}
        showTime
        format={dateTimeFormat || timeFormat }
        {...restProps}
      />
    );
  }
}
