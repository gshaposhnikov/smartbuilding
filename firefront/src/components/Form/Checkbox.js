import React, { Component } from 'react';

import { Checkbox } from 'antd';

export default class CheckboxToForm extends Component {
  render() {
    const { input: { onChange, value }, label, ...restProps } = this.props;
    return (
      <Checkbox onChange={onChange} checked={Boolean(value)} {...restProps}>
        {label}
      </Checkbox>
    );
  }
}
