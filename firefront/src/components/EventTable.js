import React from 'react';

import { Tooltip } from 'antd';

export const ColorRow = ({ stateCategoryId, className, ...restProps }) => {
  return <div className={`event-cell ${stateCategoryId} ${className}`} {...restProps} />;
};

export function renderDescriptionCell(text, record, selectedEventId) {
  const regionInfo = record.regionEventInfo;
  const deviceInfo = record.deviceEventInfo;
  const scenarioInfo = record.scenarioEventInfo;
  const employeeInfo = record.employeeEventInfo;
  const controlDeviceInfo = record.controlDeviceEventInfo;
  const controlDeviceStates = controlDeviceInfo ? controlDeviceInfo.states : null;
  if (regionInfo || deviceInfo || scenarioInfo || employeeInfo || controlDeviceStates) {
    return (
      <Tooltip
        title={() => (
          <div>
            {regionInfo ? (
              <div>
                <div>Информация о зоне:</div>
                <div>{regionInfo.description || regionInfo.name}</div>
                {regionInfo.states
                  ? regionInfo.states.map(regionState => <div>{regionState}</div>)
                  : null}
                <br />
              </div>
            ) : null}
            {deviceInfo ? (
              <div key={'tooltip-title'}>
                <div>Информация об устройстве:</div>
                <div key={'tooltip-title-description'}>{`${deviceInfo.description} ${
                  deviceInfo.address
                }`}</div>
                {deviceInfo.states
                  ? deviceInfo.states.map((deviceState, index) => (
                      <div key={index}>{deviceState}</div>
                    ))
                  : null}
                <br />
              </div>
            ) : null}
            {scenarioInfo ? (
              <div>
                <div>Информация о сценарии:</div>
                <div>{scenarioInfo.description || scenarioInfo.name}</div>
                {scenarioInfo.states
                  ? scenarioInfo.states.map(scenarioState => <div>{scenarioState}</div>)
                  : null}
                <br />
              </div>
            ) : null}
            {employeeInfo ? (
              <div>
                <div>Информация о сотруднике:</div>
                {`${employeeInfo.lastName} ${employeeInfo.firstName} ${employeeInfo.middleName}`}
                <br />
              </div>
            ) : null}
            {controlDeviceStates ? (
              <div key={'tooltip-title'}>
                <div>Состояния прибора:</div>
                {controlDeviceStates.map((deviceState, index) => (
                      <div key={index}>{deviceState}</div>
                    ))}
              </div>
            ) : null}
          </div>
        )}
        placement="leftTop"
      >
        <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
          {record.name}
        </ColorRow>
      </Tooltip>
    );
  } else {
    return (
      <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
        {record.name}
      </ColorRow>
    );
  }
}

export function renderAddressDeviceCell(text, record, selectedEventId) {
  const deviceInfo = record.deviceEventInfo;
  if (deviceInfo) {
    return (
      <Tooltip
        title={() => (
          <div key={'tooltip-title'}>
            <div key={'tooltip-title-description'}>{deviceInfo.description}</div>
            {deviceInfo.states
              ? deviceInfo.states.map((deviceState, index) => <div key={index}>{deviceState}</div>)
              : null}
          </div>
        )}
        placement="leftTop"
      >
        <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
          {`${record.deviceEventInfo.name}, ${record.deviceEventInfo.address}`}
        </ColorRow>
      </Tooltip>
    );
  } else {
    return <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId} />;
  }
}

export function renderRegionCell(text, record, selectedEventId) {
  const regionInfo = record.regionEventInfo;
  if (regionInfo) {
    return (
      <Tooltip
        title={() => (
          <div>
            <div>{regionInfo.description || regionInfo.name}</div>
            {regionInfo.states
              ? regionInfo.states.map(regionState => <div>{regionState}</div>)
              : null}
          </div>
        )}
        placement="leftTop"
      >
        <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
          {regionInfo.name}
        </ColorRow>
      </Tooltip>
    );
  } else {
    return <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId} />;
  }
}

export function renderScenarioCell(text, record, selectedEventId) {
  const scenarioInfo = record.scenarioEventInfo;
  if (scenarioInfo) {
    return (
      <Tooltip
        title={() => (
          <div>
            <div>{scenarioInfo.description || scenarioInfo.name}</div>
            {scenarioInfo.states
              ? scenarioInfo.states.map(scenarioState => <div>{scenarioState}</div>)
              : null}
          </div>
        )}
        placement="leftTop"
      >
        <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
          {record.scenarioEventInfo
            ? `${record.scenarioEventInfo.globalNo}. ${record.scenarioEventInfo.name}`
            : null}
        </ColorRow>
      </Tooltip>
    );
  } else {
    return (
      <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
        {record.scenarioEventInfo
          ? `${record.scenarioEventInfo.globalNo}. ${record.scenarioEventInfo.name}`
          : null}
      </ColorRow>
    );
  }
}

export function renderEmployeeCell(record, selectedEventId) {
  const employeeInfo = record.employeeEventInfo;
  if (employeeInfo) {
    return (
      <Tooltip
        title={`${employeeInfo.lastName} ${employeeInfo.firstName} ${employeeInfo.middleName}`}
        placement="leftTop"
      >
        <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
          {employeeInfo.name}
        </ColorRow>
      </Tooltip>
    );
  } else {
    return <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId} />;
  }
}
