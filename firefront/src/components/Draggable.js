import React from 'react';
import DragM from 'dragm';

export default class Draggable extends React.Component {
  updateTransform = transformStr => {
    this.dom.style.transform = transformStr;
  };
  componentDidMount() {
    const { componentClassName } = this.props;
    this.dom = document.getElementsByClassName(componentClassName)[0];
  }
  componentWillReceiveProps(newProps) {
    const { componentClassName } = newProps;
    if (!this.dom) this.dom = document.getElementsByClassName(componentClassName)[0];
  }
  render() {
    const { title } = this.props;
    return (
      <DragM updateTransform={this.updateTransform}>
        <div>{title}</div>
      </DragM>
    );
  }
}
