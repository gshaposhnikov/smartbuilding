import React from 'react';
import { Table, Column } from 'react-virtualized';

import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';

const DeviceList = ({ width, height, devices, onRowDoubleClick }) => (
  <Table
    width={width}
    height={height}
    rowHeight={26}
    headerHeight={16}
    rowGetter={({ index }) => devices[index]}
    rowCount={devices.length}
    onRowDoubleClick={onRowDoubleClick}
  >
    <Column
      label="Тип устройства"
      dataKey="globalNo"
      key="globalNo"
      width={width - 80}
      cellRenderer={({ rowData }) => <DeviceTypeTreeItem record={rowData} />}
    />
    <Column
      label="Адрес"
      dataKey="shortAddressPath"
      key="name"
      width={80}
      cellRenderer={({ rowData }) => <div>{rowData.shortAddressPath}</div>}
    />
  </Table>
);

export default DeviceList;
