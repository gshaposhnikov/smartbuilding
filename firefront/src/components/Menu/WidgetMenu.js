import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import ErrorTitle from 'components/Title/ErrorTitle';

/**
 * CRUD-меню c 3-мя кнопками
 */
export default class TripleCRUDMenu extends Component {
  static propTypes = {
    createButtonTitle: PropTypes.string,
    editButtonTitle: PropTypes.string,
    deleteButtonTitle: PropTypes.string,
    isCreateButtonActive: PropTypes.bool,
    isDeleteButtonActive: PropTypes.bool,
    isEditButtonActive: PropTypes.bool,
    onCreateButtonClick: PropTypes.func,
    onDeleteButtonClick: PropTypes.func,
    onEditButtonClick: PropTypes.func,
    errorMessage: PropTypes.string,
    title: PropTypes.string
  };

  static defaultProps = {
    createButtonTitle: 'Добавить',
    editButtonTitle: 'Изменить',
    deleteButtonTitle: 'Удалить'
  };

  render() {
    const {
      createButtonTitle,
      editButtonTitle,
      deleteButtonTitle,
      isCreateButtonActive,
      isDeleteButtonActive,
      isEditButtonActive,
      onCreateButtonClick,
      onDeleteButtonClick,
      onEditButtonClick,
      errorMessage,
      title,
      css,
      iconStyles,
      children
    } = this.props;

    return (
      <Menu isActive={isCreateButtonActive || isDeleteButtonActive || isEditButtonActive} css={css}>
        <ButtonGroup>
          {errorMessage ? (
            <ErrorTitle>{errorMessage}</ErrorTitle>
          ) : (
            (title && <Title>{title}</Title>) || children
          )}
        </ButtonGroup>
        <ButtonGroup>
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onCreateButtonClick}
            icon="file-add"
            isActive={isCreateButtonActive}
          >
            {createButtonTitle}
          </Button>
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onEditButtonClick}
            icon="edit"
            isActive={isEditButtonActive}
          >
            {editButtonTitle}
          </Button>
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onDeleteButtonClick}
            icon="delete"
            isActive={isDeleteButtonActive}
          >
            {deleteButtonTitle}
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}

/**
 * CRUD-меню c 2-мя кнопками
 */
export class DoubleCRUDMenu extends Component {
  static propTypes = {
    isAddActive: PropTypes.bool,
    onAdd: PropTypes.func,
    isDeleteActive: PropTypes.bool,
    onDelete: PropTypes.func
  };

  render() {
    const { isAddActive, onAdd, isDeleteActive, onDelete } = this.props;

    return (
      <Menu isActive={isAddActive || isDeleteActive}>
        <ButtonGroup />
        <ButtonGroup>
          <Button onClick={onAdd} icon="file-add" isActive={isAddActive}>
            Добавить
          </Button>
          <Button onClick={onDelete} icon="delete" isActive={isDeleteActive}>
            Удалить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
