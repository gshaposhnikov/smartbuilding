import styled, { css } from 'styled-components';

export const MenuWrapper = styled.div`
  display: flex;
  ${p => p.isVertical ? css`
      justify-content: center;
      flex-direction: column;
    ` :
    css`
      justify-content: space-between;
      align-items: center;
    `
  };

  background-color: ${p => p.isActive ? (p => p.color ? p.color : '#5FA2DD') : '#ccc'};
  transition: ease-out 0.2s;
  min-height: 35px;
  max-height: 35px;

  &:hover {
    background-color: ${p => p.color ? p.color : '#5FA2DD'};
  }

  ${p => p.css}
`;
