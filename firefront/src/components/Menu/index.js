import React, { Component } from 'react';
import * as s from './styles';

export default class Menu extends Component {
  render() {
    const { isActive, css, isVertical, color, ...restProps } = this.props;
    return (
      <s.MenuWrapper css={css || 'border-top: 2px solid lightgray;'} color={color} isActive={isActive} isVertical={isVertical} {...restProps}>
        {this.props.children}
      </s.MenuWrapper>
    )
  }
}