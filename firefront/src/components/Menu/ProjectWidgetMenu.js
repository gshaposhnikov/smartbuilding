import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import ErrorTitle from 'components/Title/ErrorTitle';

export default class WidgetMenu extends Component {
  static propTypes = {
    isExportButtonActive: PropTypes.bool,
    isImportButtonActive: PropTypes.bool,
    isAddButtonActive: PropTypes.bool,
    isDeleteButtonActive: PropTypes.bool,
    isEditButtonActive: PropTypes.bool,
    onAddButtonClick: PropTypes.func,
    onDeleteButtonClick: PropTypes.func,
    onEditButtonClick: PropTypes.func,
    onExportProjectButtonClick: PropTypes.func,
    onImportProjectButtonClick: PropTypes.func,
    errorMessage: PropTypes.string,
    title: PropTypes.string
  };
  render() {
    const {
      importProjectInProgress,
      isExportButtonActive,
      isImportButtonActive,
      onExportButtonClick,
      onImportButtonClick,
      isCreateButtonActive,
      isDeleteButtonActive,
      isEditButtonActive,
      onCreateButtonClick,
      onDeleteButtonClick,
      onEditButtonClick,
      errorMessage,
      title,
      css,
      iconStyles
    } = this.props;

    return (
      <Menu css={css}>
        <ButtonGroup>
          {errorMessage ? <ErrorTitle>{errorMessage}</ErrorTitle> : title && <Title>{title}</Title>}
        </ButtonGroup>
        <ButtonGroup>
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onExportButtonClick}
            icon="upload"
            isActive={isExportButtonActive}
          >
            Сохранить
          </Button>
          {importProjectInProgress ? (
            <Spin size="small">
              <Button
                css={css}
                iconStyles={iconStyles}
                onClick={onImportButtonClick}
                icon="download"
                isActive={isImportButtonActive}
              >
                Загрузить
              </Button>
            </Spin>
          ) : (
            <Button
              css={css}
              loading={!!importProjectInProgress}
              iconStyles={iconStyles}
              onClick={onImportButtonClick}
              icon="download"
              isActive={isImportButtonActive}
            >
              Загрузить
            </Button>
          )}
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onCreateButtonClick}
            icon="file-add"
            isActive={isCreateButtonActive}
          >
            Добавить
          </Button>
          <Button
            css={css}
            iconStyles={iconStyles}
            onClick={onEditButtonClick}
            icon="edit"
            isActive={isEditButtonActive}
          >
            Изменить
          </Button>
          <Button
            css={css}
            iconStyles={iconStyles}
            icon="delete"
            isActive={isDeleteButtonActive}
            onClick={onDeleteButtonClick}
          >
            Удалить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
