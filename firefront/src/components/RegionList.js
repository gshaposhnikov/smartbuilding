import React from 'react';
import { Table, Column } from 'react-virtualized';

export default class RegionList extends React.Component {
  renderRegionGlobalNoCell = record => {
    const { entities } = this.props;
    return <div>{entities ? record.index : record.region.index}</div>;
  };

  renderRegionNameCell = record => {
    return <div>{record.name}</div>;
  };

  getRowClassName = record => {
    const { disableCondition } = this.props;
    if (!record) return '';
    if (disableCondition && disableCondition(record)) return 'disabled-row';
    const { rowSelections = [] } = this.props;
    if (rowSelections.includes(record.id)) return 'selected-row';
    return '';
  };

  render = () => {
    const {
      width,
      height,
      regions = [],
      onRowDoubleClick = () => {},
      onRowClick = () => {}
    } = this.props;
    return (
      <Table
        width={width}
        height={height}
        rowHeight={26}
        headerHeight={16}
        rowGetter={({ index }) => regions[index]}
        rowCount={regions.length}
        onRowDoubleClick={onRowDoubleClick}
        onRowClick={onRowClick}
        rowClassName={({ index }) => this.getRowClassName(regions[index])}
      >
        <Column
          label="Номер"
          dataKey="region.index"
          key="index"
          width={60}
          cellRenderer={({ rowData }) => this.renderRegionGlobalNoCell(rowData)}
        />
        <Column
          label="Название"
          dataKey="name"
          key="name"
          width={width - 60}
          cellRenderer={({ rowData }) => this.renderRegionNameCell(rowData)}
        />
      </Table>
    );
  };
}
