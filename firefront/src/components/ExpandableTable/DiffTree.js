import React from 'react';
import Tree, { renderers } from 'react-virtualized-tree';
const { Expandable } = renderers;

import { Row, Cell } from './styles';

import SUBSYSTEMS from 'constants/subsystems';

const EXPANDED_ICON_CLASS_NAME = 'ant-table-row-expand-icon';
const EXPAND_ICONS_CLASS_NAMES = {
  expanded: `${EXPANDED_ICON_CLASS_NAME} ant-table-row-expanded`,
  collapsed: `${EXPANDED_ICON_CLASS_NAME} ant-table-row-collapsed`,
  lastChild: 'tree-node-expand-icon'
};
const CELL_CLASS_NAME = 'cell-content';
const IMG_STYLES = { marginRight: '6px' };

export default class CustomTree extends React.Component {
  constructor(props) {
    super();
    this.state = { nodes: [] };
  }

  componentDidMount() {
    const { nodes } = this.props;
    this.setState({ nodes });
  }

  componentWillReceiveProps(nextProps) {
    const { nodes } = nextProps;
    this.setState({ nodes });
  }

  handleChange = nodes => {
    this.setState({ nodes });
  };

  getCellContent = (col, node) => {
    switch (col.dataIndex) {
      case 'value': {
        if (node.cellValueRender) {
          return node.cellValueRender(node[col.dataIndex], node);
        } else {
          switch (`${node.id}`.toLowerCase()) {
            case 'subsystem': {
              return (
                <div className={CELL_CLASS_NAME} title={SUBSYSTEMS[node[col.dataIndex]].name}>
                  {SUBSYSTEMS[node[col.dataIndex]].name}
                </div>
              );
            }
            default: {
              let text = `${node[col.dataIndex]}`;
              switch (text) {
                case 'true': {
                  text = 'Да';
                  break;
                }
                case 'false': {
                  text = 'Нет';
                  break;
                }
                case 'undefined':
                case 'null': {
                  text = '';
                  break;
                }

                default: {
                  break;
                }
              }
              return (
                <div className={CELL_CLASS_NAME} title={text}>
                  {text}
                </div>
              );
            }
          }
        }
      }
      case 'description': {
        switch (node.itemType) {
          case 'CONDITION':
          case 'ACTION': {
            return (
              <span title={node.description} className={CELL_CLASS_NAME}>
                <img
                  width="16px"
                  height="16px"
                  style={IMG_STYLES}
                  src={`/public/img/device/${node.descriptionIconName}`}
                  alt=""
                />
                {node.description}
              </span>
            );
          }
          default: {
            return null;
          }
        }
      }
      default: {
        return (
          <div className={CELL_CLASS_NAME} title={node[col.dataIndex]}>
            {node[col.dataIndex]}
          </div>
        );
      }
    }
  };

  render() {
    const { nodes } = this.state;
    const { columns } = this.props;
    return (
      <Tree nodes={nodes || []} onChange={this.handleChange}>
        {({ node, ...rest }) => (
          <Row color={node.diffState ? node.diffState.color : '#fff'} key={node.id}>
            {rowData => {
              return columns.map((col, index) => (
                <Cell
                  key={index}
                  width={col.width || null}
                  steps={index === 0 ? node.deepness : null}
                >
                  {index === 0 ? (
                    <div className={CELL_CLASS_NAME} title={node.name}>
                      <Expandable
                        node={node}
                        {...rest}
                        iconsClassNameMap={EXPAND_ICONS_CLASS_NAMES}
                      >
                        {node.cellNameRender
                          ? node.cellNameRender(node[col.dataIndex], node)
                          : node[col.dataIndex]}
                      </Expandable>
                    </div>
                  ) : (
                    this.getCellContent(col, node)
                  )}
                </Cell>
              ));
            }}
          </Row>
        )}
      </Tree>
    );
  }
}
