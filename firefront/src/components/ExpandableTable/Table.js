import React from 'react';
import Tree from './Tree';
import { TableHeader, TableHeaderCell } from './styles';

const WRAPPER_DEFAULT_STYLES = { flex: 1, marginBottom: '25px' };

class Table extends React.Component {
  componentDidMount = () => {
    this.body = this.header.nextElementSibling.firstElementChild;
    this.body.addEventListener('scroll', this.syncHeaderScroll);
    this.setState({});
  };

  syncHeaderScroll = e => {
    if (this.header.scrollLeft !== e.currentTarget.scrollLeft) {
      this.header.scrollLeft = e.currentTarget.scrollLeft;
      this.setState({});
    }
  };

  componentWillUnmount = () => {
    this.body.removeEventListener('scroll', this.syncHeaderScroll);
  };

  render = () => {
    let styles = {};
    const { height, width, forceWidth } = this.props;
    if (height || width) {
      if (height) styles.height = `${height}px`;
      if (width) styles.width = `${width}px`;
      if (forceWidth) styles.width = '100%';
    } else styles = null;
    return (
      <div style={styles ? styles : WRAPPER_DEFAULT_STYLES}>
        <TableHeader
          className="expandable__table__header"
          getRef={node => {
            this.header = node;
          }}
        >
          {this.props.columns.map((col, index) => (
            <TableHeaderCell width={col.width} key={index}>
              {col.title}
            </TableHeaderCell>
          ))}
        </TableHeader>
        <Tree {...this.props} rowWidth={this.header ? this.header.scrollWidth : null} />
      </div>
    );
  };
}

export default Table;
