import React from 'react';
import DiffTree from './DiffTree';
import { TableHeader, TableHeaderCell } from './styles';

const WRAPPER_DEFAULT_STYLES = { flex: 1, marginBottom: '25px', height: 400 };
const COLUMNS = [
  {
    key: 'type',
    title: 'Наименование',
    dataIndex: 'name',
    width: 350
  },
  {
    key: 'type',
    title: 'Значение',
    dataIndex: 'value'
  }
];
const SCEANRIO_COLUMNS = [
  {
    key: 'type',
    title: 'Наименование',
    dataIndex: 'name',
    width: 250
  },
  {
    key: 'type',
    title: 'Значение',
    dataIndex: 'value',
    width: 250
  },
  {
    key: 'type',
    title: 'Описание',
    dataIndex: 'description'
  }
];
export default class Table extends React.Component {
  componentDidMount = () => {
    const { id } = this.props;
    this.addScrollListner(id);
  };

  addScrollListner = tableId => {
    const container = document.getElementById(tableId);
    this.tables = container.getElementsByClassName('ReactVirtualized__List');
    this.tables[0].addEventListener('scroll', () => {
      this.tables[1].scrollTop = this.tables[0].scrollTop;
    });
    this.tables[1].addEventListener('scroll', () => {
      this.tables[0].scrollTop = this.tables[1].scrollTop;
    });
  };

  componentWillUnmount = () => {
    this.tables[0].removeEventListener('scroll', () => {
      this.tables[1].scrollTop = this.tables[0].scrollTop;
    });
    this.tables[1].removeEventListener('scroll', () => {
      this.tables[0].scrollTop = this.tables[1].scrollTop;
    });
  };

  render = () => {
    const { nodesFromProject, nodesFromDevice, id } = this.props;
    const columns = id === 'scenariosComparison' ? SCEANRIO_COLUMNS : COLUMNS;
    return (
      <div id={id} className="expandable__table-diff">
        <div style={WRAPPER_DEFAULT_STYLES}>
          <TableHeader className="expandable__table__header">
            {columns.map((col, index) => (
              <TableHeaderCell width={col.width || 0} key={index}>
                {col.title}
              </TableHeaderCell>
            ))}
          </TableHeader>
          <DiffTree columns={columns} nodes={nodesFromProject || []} />
        </div>
        <div style={WRAPPER_DEFAULT_STYLES}>
          <TableHeader className="expandable__table__header">
            {columns.map((col, index) => (
              <TableHeaderCell width={col.width || 0} key={index}>
                {col.title}
              </TableHeaderCell>
            ))}
          </TableHeader>
          <DiffTree columns={columns} nodes={nodesFromDevice || []} />
        </div>
      </div>
    );
  };
}
