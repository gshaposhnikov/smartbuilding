import React from 'react';
import styled from 'styled-components';

import ConnectedRow from 'components/Table/ConnectedRow';

const getCellWidth = p => `flex: ${p.width ? `0 0 ${p.width}px` : '1 0 115px'};`;

const WrappedHeader = ({ getRef = () => null, ...restProps }) => (
  <div {...restProps} ref={getRef} />
);

const TableHeader = styled(WrappedHeader)`
  display: flex;
  height: 25px;
  overflow-y: scroll !important;
  background-color: #e1e1e1;
  overflow-x: hidden !important;
`;

const TableHeaderCell = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  height: 24px;
  padding: 0 8px;
  ${getCellWidth};
`;

const Row = styled(ConnectedRow)`
  display: flex;
  flex-flow: row nowrap;
  border-bottom: 1px solid lightgray;
  ${p => `background: ${p.color || '#fff'}`} !important;
  ${p => (p.rowWidth ? `min-width:${p.rowWidth}px` : '')};
`;

const Cell = styled.div`
  display: flex;
  flex-flow: row nowrap;
  ${getCellWidth};
  height: 24px;
  padding: 0 8px;
  align-items: center;
  border-left: 1px solid lightgray;
  overflow: hidden;
  & .cell-content {
    display: flex;
    ${p => (p.steps ? `margin-left: ${10 * p.steps}px;` : '')};
    flex-flow: row nowrap;
    flex: 1 0 1;
  }
`;

export { Row, Cell, TableHeader, TableHeaderCell };
