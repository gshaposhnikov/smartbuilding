import React from 'react';
import Tree, { renderers } from 'react-virtualized-tree';

import { Row, Cell } from './styles';

const { Expandable } = renderers;
// Список классов, при клике на которые блокируются иные действия
const IGNORABLE_CLASS_NAMES = {
  'ant-table-row-expand-icon': true,
  'ant-input': true
};
const EXPANDED_ICON_CLASS_NAME = 'ant-table-row-expand-icon';
const EXPAND_ICONS_CLASS_NAMES = {
  expanded: `${EXPANDED_ICON_CLASS_NAME} ant-table-row-expanded`,
  collapsed: `${EXPANDED_ICON_CLASS_NAME} ant-table-row-collapsed`,
  lastChild: 'tree-node-expand-icon'
};
const CELL_CLASS_NAME = 'cell-content';
const SELECTED_ROW_CLASS_NAME = 'tree-node-selected';

/**
 * Древовидная таблица.
 * Компонент дерева требует stateful-компонент,
 * поэтому библиотечные компоненты должны находиться
 * в функции render;
 *
 * @param {Array} columns - колонки таблицы
 * @param {Array} nodes - дерево с полями:
 *  id - обязательное поле, должно быть числом. Со строками вроде работает, но в библиотеке есть проверка scrollToId на число;
 *  state: { expanded: true | false } - необязательное поле, управляет раскрытием узла
 * @param {Number, undefined} initialExpandDeep - исходная глубина развернутости (если не задано, то по умолчанию 3)
 * @param {Number, String} rowSelection - идентификатор выделенного узла (node.id)
 * @param {Function} onRowClick - обработчик клика по строке, аргумент - соответствующий строке узел (node)
 */

// TODO: Сменить библиотеки для отрисовки деревьев, где обработка двойного клика не прерывается после выделения строки
export default class CustomTree extends React.Component {
  constructor(props) {
    super(props);
    this.state = { nodes: [] };
  }

  componentDidMount = () => {
    const { nodes } = this.props;
    const updatedNodes =
      nodes && nodes.length ? this.updateNodes(this.state.nodes, nodes, true) : [];
    this.setState({ nodes: updatedNodes });
    window.addEventListener('rerender-tree', this.rerender);
  };

  rerender = () => {
    const { nodes } = this.props;
    this.setState(prevState => ({
      nodes: this.updateNodes([], nodes || [], true)
    }));
  };

  componentWillReceiveProps = nextProps => {
    const { nodes, scrollTo, isEmpty } = nextProps;
    // FIX: МОМЕНТ ПЕРЕСОЗДАНИЯ НОД
    if (!isEmpty) {
      if (nextProps.scrollTo)
        if (nextProps.scrollTo.id !== null) this.setState({ scrollToId: nextProps.scrollTo.id });
      if (
        (nodes && nodes.length && nodes !== this.props.nodes) ||
        (nodes && nodes.length && scrollTo && scrollTo.state)
      ) {
        this.setState(prevState => ({
          nodes: this.updateNodes(
            prevState.nodes,
            nodes,
            !prevState.nodes.length,
            null,
            scrollTo ? scrollTo.expandedRows : []
          )
        }));
        if (scrollTo) {
          scrollTo.state = false;
        }
      }
    } else {
      this.setState({ nodes: [] });
    }
  };

  /**
   * Обновление элементов дерева
   * @param {Array|null} prev - Прошлое состояние дерева
   * @param {Array|null} next - Новое состояние дерева
   * @param {boolean|undefined} init - Момент инициализации (Нужен для раскрытия дерева до определеного уровня)
   * @param {Number|undefined|null} deep - Текущий уровень дерева
   * @param {Array|undefined} expandedRows - Список ключей дерева, которые нужно раскрыть
   **/
  updateNodes = (prev, next, init, deep = 0, expandedRows = []) => {
    const { initialExpandDeep = 3 } = this.props;
    const updatedNodes = next.map((node, index) => {
      if (init) {
        if (node.children && node.children.length && deep < initialExpandDeep)
          return {
            ...node,
            state: { expanded: true },
            children: this.updateNodes([], node.children, init, 1 + deep)
          };
        return node;
      } else {
        const updatedNode =
          prev[index] && prev[index].id === node.id
            ? prev[index]
            : prev.find(prevNode => prevNode.id === node.id);
        if (updatedNode) {
          if (node.children) {
            const newNode = {
              ...updatedNode,
              ...node,
              children: this.updateNodes(
                updatedNode.children || [],
                node.children,
                init,
                ++deep,
                expandedRows
              )
            };
            if (expandedRows.includes(newNode.id)) newNode.state = { expanded: true };
            return newNode;
          } else {
            const newNode = { ...updatedNode, ...node, children: [] };
            if (expandedRows.includes(newNode.id)) newNode.state = { expanded: true };
            return newNode;
          }
        }
        if (!updatedNode && expandedRows.includes(node.id)) {
          return {
            ...node,
            state: { expanded: true },
            children: this.updateNodes([], node.children || [], init, ++deep, expandedRows)
          };
        }
        return node;
      }
    });
    return updatedNodes;
  };

  handleChange = nodes => {
    this.setState({ nodes });
  };

  getCellContent = (col, node, treeItem) => {
    if (col.render) {
      return (
        <div className={CELL_CLASS_NAME}>
          <span className="ellipsis-content">
            {col.render(node[col.dataIndex], node, treeItem)}
          </span>
        </div>
      );
    } else
      return (
        <div className={CELL_CLASS_NAME}>
          <span className="ellipsis-content">{node[col.dataIndex]}</span>
        </div>
      );
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (this.props.rowWidth !== nextProps.rowWidth) return true;
    if (nextProps === this.props && nextState === this.state) return false;
    if (!nextProps && !nextState) return false;
    if (
      (!nextProps.nodes && !nextState.nodes) ||
      (!nextProps.nodes.length && !nextState.nodes.length && !nextProps.isEmpty)
    )
      return false;
    return true;
  };

  componentWillUnmount = () => {
    window.removeEventListener('rerender-tree', this.rerender);
  };

  render = () => {
    const { nodes, scrollToId } = this.state;
    const {
      columns,
      onRowClick,
      rowSelection,
      rowSelections,
      onRowDoubleClick,
      getEntitiesHash,
      rowWidth
    } = this.props;
    return (
      <Tree nodes={nodes} onChange={this.handleChange} scrollToId={scrollToId}>
        {({ node, ...rest }) => (
          <Row
            rowWidth={rowWidth}
            getEntitiesHash={getEntitiesHash}
            key={node.id}
            node={node}
            onClick={(e, rowData) => {
              if (rowSelections) {
                if (!IGNORABLE_CLASS_NAMES[e.target.classList[0]] && onRowClick)
                  onRowClick(rowData);
              } else {
                if (!IGNORABLE_CLASS_NAMES[e.target.classList[0]] && onRowClick)
                  onRowClick(rowData);
              }
            }}
            onDoubleClick={(e, rowData) => {
              if (rowSelections) {
                if (!IGNORABLE_CLASS_NAMES[e.target.classList[0]] && onRowDoubleClick)
                  onRowDoubleClick(rowData);
              } else {
                if (!IGNORABLE_CLASS_NAMES[e.target.classList[0]] && onRowDoubleClick)
                  onRowDoubleClick(rowData);
              }
            }}
            className={
              (rowSelection === node.id && !node.isRoot) ||
              (rowSelections && rowSelections.includes(node.id))
                ? SELECTED_ROW_CLASS_NAME
                : ''
            }
          >
            {(rowData, treeItem) => {
              return columns.map((col, index) => (
                <Cell
                  key={index}
                  width={col.width || null}
                  steps={index === 0 ? node.deepness : null}
                >
                  {index === 0 ? (
                    <div className={CELL_CLASS_NAME}>
                      <Expandable
                        node={node}
                        {...rest}
                        iconsClassNameMap={EXPAND_ICONS_CLASS_NAMES}
                      >
                        {col.render
                          ? col.render(rowData[col.dataIndex], rowData, treeItem)
                          : rowData[col.dataIndex]}
                      </Expandable>
                    </div>
                  ) : (
                    this.getCellContent(col, rowData, treeItem)
                  )}
                </Cell>
              ));
            }}
          </Row>
        )}
      </Tree>
    );
  };
}
