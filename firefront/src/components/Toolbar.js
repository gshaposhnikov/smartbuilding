import React from 'react';
import { Tooltip, Select } from 'antd';

import Button from 'components/Button';
import Menu from 'components/Menu';
import ButtonGroup from 'components/Button/ButtonGroup';

const { Option } = Select;
const textStyle = `margin: 0;`;
/* Размер иконки */
const ICON_SIZE = 22;
/* Стили для иконки активного инструмента */
const CHECKED_ICON_STYLES = { color: 'lightGrey' };
/* Стили для svg-иконок проекта */
const CUSTOM_ICON_STYLES = { width: ICON_SIZE, height: ICON_SIZE };
/* Стили для иконок antd */
const ANTD_ICON_STYLES = { fontSize: ICON_SIZE };

/**
 * TODO: Переделать поле action на строчную функцию
 *       и внутри родительского компонента вызывать ее через eval()
 */

/**
 * Описание набора инструментов
 *
 * tools: [
 *   [
 *     {
 *       key: <string>, - Уникальный ключ
 *       title: <string>, - Заголовок для тултипа
 *       icon: <string>, - Название иконки из списка в проекте или antd
 *       customIcon: <bool>, - Если true, то иконка берется из списка в проекте, иначе antd
 *       selectable: <bool>, - Если true, то может быть нажата, иначе работает как чекбокс
 *       type: <string (button|flag|options)>,
 *       action: <string>, - Название функции, которая будет вызвана внутри родительского компонента
 *       options: <array>, Список допустимых значений для Select,
 *       altTitle: <string>, Альтернативный текст для тултипа (если type === 'flag')
 *     },
 *     ...
 *   ],
 *   ...
 * ]
 */

/**
 * Набор инструментов, разбитый по группам
 *
 * @param {string} activeTool - Текущий инструмент
 * @param {func} onButtonClick - Смена текущего инструмента
 * @param {bool} isActive - Если меню активно
 * @param {boolean} selectedObject - Выбранный объект, с которым происходит взаимодействие
 * @param {array} isChecked - Список нажатых кнопок (type === 'flag')
 * @param {func} onChange - Вызывает функцию указаную в параметре action
 * @param {array} tools - Список инструментов (описан выше)
 */
class Toolbar extends React.Component {
  static isActiveButton = (tool, isActive, selectedObject, activeTool, toolsParams) => {
    if (toolsParams && toolsParams[tool.key]) {
      if (Object.prototype.hasOwnProperty.call(toolsParams[tool.key], 'disabled')) {
        return !toolsParams[tool.key].disabled && tool.key !== activeTool && isActive;
      }
    }
    return tool.type === 'button'
      ? !isActive || tool.key === 'delete'
        ? selectedObject
          ? true
          : false
        : tool.key !== activeTool
      : isActive;
  };

  divider = (
    <div
      style={{ width: 0, margin: '0px 4px', border: '1px solid white', height: 24, opacity: 0.6 }}
    />
  );

  renderTool = (tool, index) => {
    const {
      activeTool,
      onButtonClick,
      isActive,
      selectedObject,
      isChecked,
      onChange,
      toolsParams,
      ...restProps
    } = this.props;
    if (tool.key)
      return (
        <Tooltip
          key={tool.key}
          title={
            tool.type === 'flag'
              ? !isChecked.includes(tool.key)
                ? tool.title
                : tool.altTitle
              : tool.title
          }
        >
          {tool.type === 'button' || tool.type === 'flag' ? (
            <Button
              customIcon={tool.customIcon}
              iconStyles={
                tool.type === 'flag' && !isChecked.includes(tool.key)
                  ? tool.customIcon
                    ? { ...CHECKED_ICON_STYLES, ...CUSTOM_ICON_STYLES }
                    : { ...CHECKED_ICON_STYLES, ...ANTD_ICON_STYLES }
                  : tool.customIcon
                  ? CUSTOM_ICON_STYLES
                  : ANTD_ICON_STYLES
              }
              icon={tool.icon ? tool.icon : null}
              onClick={() =>
                onButtonClick({ key: tool.key, selectable: tool.selectable, type: tool.type })
              }
              className={tool.key === activeTool && isActive ? 'activeTool' : ''}
              isActive={this.constructor.isActiveButton(
                tool,
                isActive,
                selectedObject,
                activeTool,
                toolsParams
              )}
              css={textStyle}
            />
          ) : (
            <Select
              style={{ padding: '0 5px' }}
              size="small"
              defaultValue={`${restProps[tool.key] || ''}`}
              value={`${restProps[tool.key] || ''}`}
              onChange={value => onChange({ value, key: tool.key, action: tool.action })}
              disabled={!isActive}
            >
              {tool.options.map((option, index) => (
                <Option key={index} value={`${option.value}`}>
                  {`${option.text || option.value}`}
                </Option>
              ))}
            </Select>
          )}
        </Tooltip>
      );
    return this.divider;
  };

  renderToolsGroup = (group, index) => {
    return <ButtonGroup key={index}>{group.map(this.renderTool)}</ButtonGroup>;
  };

  render = () => {
    const { isActive, tools } = this.props;
    return (
      <Menu isActive={isActive} style={{ height: '30px' }}>
        {tools.map(this.renderToolsGroup)}
      </Menu>
    );
  };
}

export default Toolbar;
