import React from 'react';
import { Grid, AutoSizer } from 'react-virtualized';

const IndicatorPanel = ({
  selectedIndicatorPanel,
  panelsAutosize,
  renderIndicatorCell,
  getCellHeight,
  getCellWidth,
  regions,
  scenarios,
  devices
}) => (
  <AutoSizer>
    {({ width, height }) => {
      if (selectedIndicatorPanel) {
        const gridHeight = height - 35;
        return (
          <Grid
            cellRenderer={renderIndicatorCell}
            columnCount={selectedIndicatorPanel.colCount}
            rowCount={selectedIndicatorPanel.rowCount}
            height={gridHeight}
            width={width}
            indicatorPanel={selectedIndicatorPanel}
            regions={regions}
            scenarios={scenarios}
            devices={devices}
            rowHeight={
              selectedIndicatorPanel &&
              (panelsAutosize[selectedIndicatorPanel.id] ||
                typeof panelsAutosize[selectedIndicatorPanel.id] === 'undefined')
                ? gridHeight / selectedIndicatorPanel.rowCount
                : getCellHeight()
            }
            columnWidth={
              selectedIndicatorPanel &&
              (panelsAutosize[selectedIndicatorPanel.id] ||
                typeof panelsAutosize[selectedIndicatorPanel.id] === 'undefined')
                ? width / selectedIndicatorPanel.colCount
                : getCellWidth()
            }
            overscanColumnCount={5}
            overscanRowCount={5}
          />
        );
      }
      return null;
    }}
  </AutoSizer>
);

export default IndicatorPanel;
