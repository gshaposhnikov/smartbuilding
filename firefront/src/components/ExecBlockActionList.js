import React from 'react';
import { Table, Column } from 'react-virtualized';
import { Select } from 'antd';
const Option = Select.Option;

import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';

import { getDeviceProfile } from 'helpers/device';

export default class ActionList extends React.Component {
  renderObjectCell = record => {
    switch (record.entityType) {
      case 'DEVICE': {
        const { devices } = this.props;
        const device = devices[record.entityId];
        return (
          <DeviceTypeTreeItem
            record={{ ...device, name: `${device.name} (${device.middleAddressPath})` }}
          />
        );
      }
      case 'REGION': {
        const { regions } = this.props;
        const region = regions[record.entityId];
        return <div>{`${region.region.index}. ${region.name}`}</div>;
      }
      case 'SCENARIO': {
        const { scenarios } = this.props;
        const scenario = scenarios[record.entityId];
        return <div>{`${scenario.globalNo}. ${scenario.name}`}</div>;
      }
      case 'VIRTUAL_STATE': {
        const { virtualStates } = this.props;
        const virtualState = virtualStates[record.entityId];
        return <div>{`${virtualState.globalNo}. ${virtualState.name}`}</div>;
      }
      default:
        return <div />;
    }
  };
  renderActionCell = (record, index) => {
    const {
      scenarioPurpose,
      onChangeActionType,
      actionTypes = [],
      deviceProfileViewsHash,
      devices
    } = this.props;
    return (
      <Select
        style={{ width: 150 }}
        value={record.actionTypeId}
        onChange={value => onChangeActionType(value, index)}
      >
        {(scenarioPurpose === 'EXEC_BY_TACTICS'
          ? actionTypes
          : actionTypes.filter(actionType => {
              if (record.entityType === 'DEVICE') {
                const device = devices[record.entityId];
                const deviceProfile = getDeviceProfile(deviceProfileViewsHash, device);
                const isSupportedScenarioActionType = deviceProfile
                  ? deviceProfile.supportedScenarioActionIds.includes(actionType.id)
                  : false;
                return (
                  actionType.entityType === record.entityType &&
                  isSupportedScenarioActionType &&
                  (!actionType.scenarioPurpose || actionType.scenarioPurpose === scenarioPurpose)
                );
              } else
                return (
                  actionType.entityType === record.entityType &&
                  (!actionType.scenarioPurpose || actionType.scenarioPurpose === scenarioPurpose)
                );
            })
        ).map((scenarioPurpose, index) => (
          <Option key={index} value={scenarioPurpose.id}>
            {scenarioPurpose.name}
          </Option>
        ))}
      </Select>
    );
  };

  getRowClassName = rowIndex => {
    if (rowIndex === -1) return '';
    const { rowSelections = [] } = this.props;
    if (rowSelections.includes(rowIndex)) return 'selected-row';
    return '';
  };

  render = () => {
    const {
      width,
      height,
      actions = [],
      onRowClick = () => {},
      onRowDoubleClick = () => {}
    } = this.props;
    return (
      <Table
        onRowClick={onRowClick}
        onRowDoubleClick={onRowDoubleClick}
        width={width}
        height={height}
        rowHeight={32}
        headerHeight={16}
        rowGetter={({ index }) => actions[index]}
        rowCount={actions.length}
        rowClassName={({ index }) => this.getRowClassName(index)}
      >
        <Column
          label="Объект"
          dataKey="entityType"
          key="entityId"
          width={width / 2}
          cellRenderer={({ rowData }) => this.renderObjectCell(rowData)}
        />
        <Column
          label="Действие"
          dataKey="actionTypeId"
          key="actionTypeId"
          width={width / 2}
          cellRenderer={({ rowData, rowIndex }) => this.renderActionCell(rowData, rowIndex)}
        />
      </Table>
    );
  };
}
