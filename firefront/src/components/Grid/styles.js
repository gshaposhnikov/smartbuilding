import styled from "styled-components";

const StyledSplitPane = styled.div`

  display: flex;
  flex-grow: 1;

  & .SplitPane {
    flex-grow: 1 !important;
    position: static !important;
    height: auto !important;
  }
  & .Resizer {
      -moz-box-sizing: border-box;
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
      background: #000;
      -moz-background-clip: padding;
      -webkit-background-clip: padding;
      background-clip: padding-box;
  }

  & .Resizer.horizontal {
      border-top: 5px solid #ECECEC;
      border-bottom: 5px solid #ECECEC;
      cursor: row-resize;
      width: 100%;
  }

  & .Resizer.vertical {
      border-left: 4px solid #ECECEC;
      border-right: 4px solid #ECECEC;
      cursor: col-resize;
  }

  & .vertical section {
      width: 100%;
  }
`;

const StaticPane = styled.div`
  display: flex;
  ${props => props.minSize ? 'min-height:' + props.minSize + 'px' : null};
  border-bottom: 8px solid #ECECEC;
`;

export { StyledSplitPane, StaticPane };
