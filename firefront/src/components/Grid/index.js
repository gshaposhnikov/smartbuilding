import React, { Component, PropTypes } from 'react';
import styled from 'styled-components';

import wrapComponentWithContext from 'helpers/wrapComponentWithContext';

import GoldenLayout from "golden-layout";
import 'golden_layout_main';
import 'golden_layout_theme'

const GoldenLayoutWrapper = styled.div`
  flex: 1;
`;

export default class Grid extends Component {
  static propTypes = {
    config: PropTypes.object,
    components: PropTypes.arrayOf(PropTypes.object),
  }

  mainLayout = {};
  
  componentWillUpdate(){
    this.mainLayout.eventHub.emit('update');
  }

  componentWillUnmount(){
    this.mainLayout.destroy();
  }

  componentDidMount() {
    const { config, components, getLayout } = this.props;
    const wrapper = document.getElementById('goldenLayout');

    this.mainLayout = new GoldenLayout(config, wrapper);

    components.forEach(item => {
      this.mainLayout.registerComponent( item.name, wrapComponentWithContext(item.component, this.context.store, this.mainLayout.eventHub));
    });

    this.mainLayout.init();
    // Возвращаем проинициализированые компоненты
    if (getLayout) getLayout(this.mainLayout);
  }

  render() {
    return <GoldenLayoutWrapper id="goldenLayout">{this.props.children}</GoldenLayoutWrapper>;
  }
}

Grid.contextTypes = {
    store: React.PropTypes.object.isRequired
};