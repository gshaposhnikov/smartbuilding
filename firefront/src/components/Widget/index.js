import React, { PureComponent } from 'react';
import { ReactHeight } from 'react-height';

export default class Widget extends PureComponent {
  render() {
    const { children, ...restProps } = this.props;
    return (
      <ReactHeight
        style={{ display: 'flex', flex: 1, flexDirection: 'column', background: 'white' }}
        {...restProps}
      >
        {children}
      </ReactHeight>
    );
  }
}
