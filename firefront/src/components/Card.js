import React, { Component } from 'react';

import { Card } from 'antd';

export default class CustomCard extends Component {
  render() {
    const { children, ...restProps } = this.props;
    return <Card {...restProps}>{children}</Card>;
  }
}
