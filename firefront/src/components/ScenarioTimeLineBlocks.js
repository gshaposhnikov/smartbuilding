import React, { Component } from 'react';
import moment from 'moment';
import { Icon } from 'antd';

import Table, { Column } from 'components/Table';

export default class ScenarioTimeLineBlocks extends Component {
  renderTimeCell = (text, record) => {
    switch (record.itemType) {
      case 'ROOT': {
        return <Icon type={record.actionIconName} style={{ color: record.actionIconColor }} />;
      }
      case 'TIME': {
        return (
          <span>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px', fontSize: '14px' }}
            />
            {record.hiddenTime
              ? ''
              : moment
                  .utc(moment.duration(record.timeDelaySec, 's').asMilliseconds())
                  .format('HH:mm:ss')}
          </span>
        );
      }

      default: {
        return null;
      }
    }
  };

  renderActionCell = (text, record) => {
    switch (record.itemType) {
      case 'TIME':
      case 'ROOT': {
        return null;
      }
      case 'COMPUTER_ACTION':
      case 'TRACE':
      case 'NO_TRACE':
      case 'CONDITION':
      case 'IN': {
        return (
          <span style={{ fontWeight: 'bold', color: 'CornflowerBlue' }}>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px', fontSize: '14px' }}
            />
            {record.actionName ? `${record.actionName} ` : 'Без имени'}
          </span>
        );
      }

      default: {
        return (
          <span style={{ marginLeft: '16px' }}>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px' }}
            />
            {record.actionName}
          </span>
        );
      }
    }
  };

  renderDescriptionCell = (text, record) => {
    switch (record.itemType) {
      case 'COUNTDOWN':
      case 'SHOW_MESSAGE':
      case 'ROOT': {
        return <span>{record.description}</span>;
      }
      case 'NO_TRACE':
      case 'TRACE':
      case 'CONDITION':
      case 'ACTION': {
        return (
          <span>
            <img
              height="16px"
              width="16px"
              /* TODO: переделать иконки устройств в svg-формат */
              src={`/public/img/device/${record.descriptionIconName}`}
              alt=""
            />
            {` ${record.description}`}
          </span>
        );
      }
      default: {
        return null;
      }
    }
  };

  render() {
    const { dataSource, scroll, expandedRowKeys, rowClassName, onRowClick, onExpand } = this.props;
    return (
      <Table
        dataSource={dataSource}
        rowClassName={rowClassName}
        scroll={scroll}
        onRowClick={onRowClick}
        expandedRowKeys={expandedRowKeys}
        onExpand={onExpand}
      >
        <Column
          title="Задержка"
          width={120}
          dataIndex="timeDelaySec"
          key="timeDelaySec"
          render={this.renderTimeCell}
        />
        <Column
          title="Действие"
          width={250}
          dataIndex="actionName"
          key="actionName"
          render={this.renderActionCell}
        />
        <Column
          title="Описание"
          dataIndex="description"
          key="description"
          render={this.renderDescriptionCell}
        />
      </Table>
    );
  }
}
