import React, { Component, PropTypes } from 'react';
import { Progress, Icon } from 'antd';

const convertStatus = (status) => {
  switch (status) {
    case 'FAILED': return 'exception';
    case 'FINISHED': return 'success';
    default: return 'active';
  }
};

export default class ProgressBar extends Component {
  static propTypes = {
    status: PropTypes.string.isRequired,
    percent: PropTypes.number,
    width: PropTypes.number,
    format: PropTypes.func
  }

  formatter(status, width) {
    const iconStyle = (color) => {
      return { fontSize: width, color: color };
    }
    switch (status) {
      case 'CANCELED':
        return () => <Icon type="close-circle-o" style={iconStyle('gray')} />

      case 'PAUSE':
        return () => <Icon type="pause-circle-o" style={iconStyle('orange')} />

      case 'CREATED':
        return () => <Icon type="play-circle-o" style={iconStyle('green')} />

      case 'FAILED':
      case 'FINISHED':
      default:
        return null;
    }
  }

  render() {
    const { width, format, percent, status } = this.props;
    return <Progress type='circle' width={width || 30}
      format={format || this.formatter(status, width || 30)}
      status={convertStatus(status)}
      percent={percent || 0} />;
  }
}
