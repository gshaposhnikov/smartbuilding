import React, { PropTypes, Component } from 'react';
import * as icons from './icons';

export default class Icon extends Component {
  static propTypes = {
    name: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    color: PropTypes.string,
  }
  
  render() {
    let { name } = this.props;

    //React компоненты пишутся с большой буквы:
    name = name.charAt(0).toUpperCase() + name.slice(1);

    //Проверяем существует ли иконка:
    const FindedIcon = icons.hasOwnProperty(name) ? icons[name] : icons['Default'];

    return <FindedIcon {...this.props} />
  }
}