import React from 'react';
import { UserTimeWrapper, UserTimeElement } from './styles';
import { getCurrentTime } from 'helpers/time';

class UserTime extends React.Component {
  updateTime() {
    this.setState({
      currentTime: getCurrentTime()
    })
  }

  state = {
    currentTime: getCurrentTime()
  }

  componentDidMount(){
    this.interval = setInterval(() => this.updateTime(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <UserTimeWrapper>
        <UserTimeElement>{this.state.currentTime}</UserTimeElement>
      </UserTimeWrapper>
    )
  }
}

export default UserTime;
