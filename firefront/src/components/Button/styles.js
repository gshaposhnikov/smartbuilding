import styled, { css } from 'styled-components';

const activeStyle = css`
  cursor: pointer;
  &:hover {
    background: rgba(255, 255, 255, 0.1);
  }
`;

const noActiveStyle = css`
  cursor: not-allowed;
  opacity: 0.5;
`;

export const ButtonWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: ${p => (p.height ? `${p.height}px` : '35px')};
  padding: 0 10px;
  color: ${p => (p.fontColor ? p.fontColor : 'white')};
  font-weight: ${p => (p.fontWeight ? p.fontWeight : 'noraml')} !important;
  & i {
    font-size: ${p => (p.iconSize ? `${p.iconSize}px` : '18px')};
  }

  ${props => (props.isActive ? activeStyle : noActiveStyle)};
`;

export const Text = styled.div`
  color: white;
  ${p => p.css};
  ${p =>
    !p.iconPlacement || p.iconPlacement === 'left' ? 'margin-left: 10px;' : 'margin-right: 10px;'};
`;
