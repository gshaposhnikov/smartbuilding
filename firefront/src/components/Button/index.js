import React from 'react';
import { ButtonWrapper, Text } from './styles';

import { Icon } from 'antd';
import CustomIcon from 'components/Icon';
export default class CustomButton extends React.Component {
  static defaultProps = {
    isActive: true
  };

  render() {
    const {
      isActive,
      children,
      onClick,
      icon,
      customIcon,
      iconStyles = {},
      css,
      iconPlacement,
      ...restProps
    } = this.props;

    return (
      <ButtonWrapper isActive={isActive} onClick={isActive ? onClick : null} {...restProps}>
        {children && iconPlacement === 'right' ? (
          <Text css={css} iconPlacement={iconPlacement}>
            {children}
          </Text>
        ) : null}
        {customIcon ? (
          <CustomIcon name={icon} {...iconStyles} />
        ) : (
          <Icon type={icon} style={iconStyles} />
        )}
        {children && (iconPlacement === 'left' || !iconPlacement) ? (
          <Text css={css} iconPlacement={iconPlacement}>
            {children}
          </Text>
        ) : null}
      </ButtonWrapper>
    );
  }
}
