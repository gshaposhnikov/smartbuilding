import React, { Component } from 'react';
import styled from 'styled-components';

import { MenuItem } from 'react-contextmenu';

const PopupItemWrapper = styled.div`
  min-width: 200px;
  padding: 10px;
  font-size: 13px;
  &:hover {
    background-color: #ecf6fd;
    cursor: pointer;
  }
  .react-contextmenu-item {
    padding: 8px;
    cursor: pointer;
  }
`;

export default class PopupItem extends Component {
  render() {
    const { ...restProps } = this.props;
    return (
      <PopupItemWrapper className={this.props.disabled ? 'disabled' : null}>
        <MenuItem {...restProps}>{this.props.children}</MenuItem>
      </PopupItemWrapper>
    );
  }
}
