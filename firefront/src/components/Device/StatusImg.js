import React from 'react';

import styled from 'styled-components';

const StatusImgWrapper = styled.div`
  position: absolute;
  left: ${props => props.size ? `-${props.size}px` : '-23px'};
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: ${props => props.size ? `${props.size}px` : '23px'};
`;

class StatusImg extends React.Component {
  render() {
    let size = this.props.size ? this.props.size : 23;
    const { name } = this.props;

    return <StatusImgWrapper size={size}>
      <img width={size} height={size} src={`/public/img/status/${name}.svg`} alt={name} />
    </StatusImgWrapper>
  }
}

export default StatusImg;
