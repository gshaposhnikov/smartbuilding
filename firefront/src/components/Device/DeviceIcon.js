import React, { PropTypes, Component } from 'react';

export default class DeviceIcon extends Component {
  static propTypes = {
    name: PropTypes.string
  };

  render() {
    let { name, children } = this.props;

    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center'
        }}
      >
        <img
          width="16px"
          src={`/public/img/device/${name}`}
          alt={`/public/img/device/${name}`}
          style={{ marginRight: '8px' }}
        />
        {children}
      </div>
    );
  }
}
