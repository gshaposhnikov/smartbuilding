import React, { Component } from 'react';
import { connect } from 'react-redux';
import Device from './Device';
import DeviceImg from './DeviceImg';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Menu, Dropdown, Icon, Tooltip } from 'antd';
import PropTypes from 'prop-types';

class DeviceTypeTreeItem extends Component {
  render() {
    const {
      record,
      dropDownEnabled,
      dropDownItems,
      onDropDownSelect,
      contextMenuName,
      stateIconEnabled,
      onDragStart,
      draggable,
      showDisabledDevices,
      attachedIconEnabled,
      showDeviceSwitcher,
      onChangeDeviceSwitcher,
      treeItem
    } = this.props;
    let contextMenuEnabled = !record.contextMenuDisabled && this.props.contextMenuEnabled;
    const iconMedia = record.iconMedia.path;
    let content = null;
    const isActive = typeof record.isActive === 'boolean' ? record.isActive : true;
    const isEnabled =
      showDisabledDevices && typeof record.statePolling === 'boolean' ? record.statePolling : true;

    if (dropDownEnabled) {
      let menuItems = dropDownItems.map((dropDownItem, i) => (
        <Menu.Item key={i}> {dropDownItem} </Menu.Item>
      ));

      let menu = <Menu onClick={onDropDownSelect}>{menuItems}</Menu>;

      content = (
        <Dropdown overlay={menu} trigger={['click']}>
          <a className="ant-dropdown-link" href="#" draggable={false}>
            {record.name} <Icon type="down" />
          </a>
        </Dropdown>
      );
    } else {
      content = <span> {record.name} </span>;
    }

    if (isActive && contextMenuEnabled) {
      return (
        <Tooltip title={record.description || ''} mouseEnterDelay={0.4} placement="leftTop">
          <Device isActive={isActive && isEnabled}>
            <ContextMenuTrigger
              holdToDisplay={-1}
              record={record}
              treeItem={treeItem}
              collect={props => props}
              id={contextMenuName}
            >
              <DeviceImg
                size="15"
                generalStateCategoryView={stateIconEnabled ? record.generalStateCategoryView : null}
                name={iconMedia}
                record={record}
                onDragStart={onDragStart}
                draggable={draggable}
                attachedIconEnabled={attachedIconEnabled}
                planLayouts={attachedIconEnabled ? record.planLayouts : null}
                showDeviceSwitcher={showDeviceSwitcher}
                onChangeDeviceSwitcher={onChangeDeviceSwitcher}
              />
              {content}
            </ContextMenuTrigger>
          </Device>
        </Tooltip>
      );
    } else {
      return (
        <Tooltip title={record.description || ''} mouseEnterDelay={0.4} placement="leftTop">
          <Device isActive={isActive}>
            <DeviceImg
              size="15"
              generalStateCategoryView={stateIconEnabled ? record.generalStateCategoryView : null}
              name={iconMedia}
              record={record}
              onDragStart={onDragStart}
              draggable={draggable}
              planLayouts={attachedIconEnabled ? record.planLayouts : null}
              showDeviceSwitcher={showDeviceSwitcher}
              onChangeDeviceSwitcher={onChangeDeviceSwitcher}
            />
            {content}
          </Device>
        </Tooltip>
      );
    }
  }
}

DeviceTypeTreeItem.propTypes = {
  record: PropTypes.object.isRequired,
  dropDownEnabled: PropTypes.bool,
  dropDownItems: PropTypes.arrayOf(PropTypes.string),
  contextMenuEnabled: PropTypes.bool,
  contextMenuName: PropTypes.string
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  null,
  mapDispatchToProps
)(DeviceTypeTreeItem);
