import React from 'react';
import styled from 'styled-components';

import DeviceImg from './DeviceImg';
import StatusImg from './StatusImg';


const DeviceWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

class Device extends React.Component {

  getStatus(){
    const { fireStatus, deviceStatus, size } = this.props;

    if(fireStatus === 'normal' && deviceStatus === 'normal') {
      return <StatusImg size={size} />
    }
    else if(fireStatus === 'normal' || fireStatus === 'nd') {
      return <StatusImg size={size} name={deviceStatus}/>
    } else {
      return <StatusImg size={size} name={fireStatus}/>
    }
  }

  getDevice(){
    const { deviceType, size } = this.props;
    return <DeviceImg size={size} name={deviceType} />
  }

  render() {
    return <DeviceWrapper>
      {this.getStatus()}
      {this.getDevice()}
    </DeviceWrapper>
  }
}

export default Device;
