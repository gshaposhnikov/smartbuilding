import React from 'react';
import styled from 'styled-components';
import { Icon } from 'antd';

import Checkbox from 'components/Checkbox';

const DeviceImgWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  justify-content: flex-end;
  & .draggable {
    cursor: move;
  }
`;
const ATTACHABLE_DEVICE_CATEGORY = { SENSOR: true, CONTROL: true, EXECUTIVE: true, VIRTUAL_CONTAINER: true };

class DeviceImg extends React.Component {
  getStatus() {
    const { generalStateCategoryView } = this.props;

    if (generalStateCategoryView)
      return (
        <img
          width="15px"
          src={`/public/img/status/${generalStateCategoryView.iconName}`}
          alt={`${generalStateCategoryView.name}`}
        />
      );
    else return null;
  }

  getPlanAttachment() {
    const { attachedIconEnabled, planLayouts, record, onDragStart } = this.props;
    if (attachedIconEnabled) {
      const isAttachableDevice =
        typeof planLayouts !== 'undefined' &&
        record.id &&
        ATTACHABLE_DEVICE_CATEGORY[record.deviceCategory];
      return (
        <Icon
          type="select"
          style={{
            fontSize: '16px',
            margin: '0 5px',
            opacity: `${isAttachableDevice ? 1 : '0.3'}`,
            color: `${isAttachableDevice && planLayouts.length ? 'green' : 'grey'}`
          }}
          onClick={() => {
            if (isAttachableDevice) onDragStart();
          }}
        />
      );
    } else return null;
  }

  getIconContent() {
    const { name } = this.props;
    switch (name) {
      case 'usb.ico': {
        return <Icon type="usb" />;
      }

      case 'network.ico': {
        return <Icon type="cloud" />;
      }

      case 'computer.ico': {
        return <Icon type="desktop" />;
      }

      default:
        return (
          <img width="16px" draggable={false} src={`/public/img/device/${name}`} alt={`${name}`} />
        );
    }
  }

  getDeviceSwitcher() {
    const { record, onChangeDeviceSwitcher, showDeviceSwitcher } = this.props;
    if (!showDeviceSwitcher) return null;
    const { disabled, disableNotAllowed, id } = record;
    return (
      <Checkbox
        checked={!disabled}
        disabled={disableNotAllowed || !id}
        onChange={e => onChangeDeviceSwitcher(id, !e.target.checked)}
      />
    );
  }

  render() {
    let size = this.props.size ? this.props.size : 23;

    return (
      <DeviceImgWrapper size={size}>
        {this.getStatus()}
        {this.getDeviceSwitcher()}
        {this.getPlanAttachment()} {this.getIconContent()}
      </DeviceImgWrapper>
    );
  }
}

export default DeviceImg;
