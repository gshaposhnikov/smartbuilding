import React, { Component } from 'react';
import { chartsSettings } from 'constants/settings';
var Highcharts = require('highcharts/highstock');
Highcharts.setOptions(chartsSettings);

const CHART_STYLES = { height: '350px', width: '100%' };

/**
 * @param {array} this.props.data
 * @param {boolean} this.props.update
 * @param {string|null} this.props.title
 * @param {string} this.props.yKey
 * @param {string} this.props.xKey
 *
 * this.props.data = [
 *   {
 *     id: <string|number>,
 *     name: <string>,
 *     data: [
 *       {
 *         xkey: <string|number>
 *         ykey: <string|number>
 *       }
 *     ]
 *   }
 * ]
 */

export default class LineChart extends Component {
  constructor(props) {
    super(props);
    this.state = { chart: null };
    this.defaultSettings = {
      rangeSelector: {
        enabled: false
      },
      legend: {
        enabled: true
      },
      tooltip: {
        xDateFormat: '%d.%m.%Y %H:%M:%S'
      }
    };
  }

  componentDidMount() {
    const { title } = this.props;
    const chart = Highcharts.stockChart('lineChart', {
      title: { text: title || null },
      series: [{ data: [], name: 'Пусто' }],
      ...this.defaultSettings
    });
    this.setState({ chart });
  }

  componentWillReceiveProps(props) {
    const { data, update, title, xKey, yKey = 'value' } = props;
    if (data && update) {
      // TODO: Разобраться с перерисовкой графиков, не создавая новый
      const chart = Highcharts.stockChart('lineChart', {
        title: { text: title || null },
        series: [{ data: [] }],
        ...this.defaultSettings
      });
      let opposite = false;
      // Очистка графика
      chart.series.forEach(item => item.remove(true));

      // Формирует данные из объекта по ключам
      const series = [
        ...data.map(item => ({
          ...item,
          data: item.data.map(value =>
            xKey
              ? [
                  value[xKey],
                  value[yKey].includes('.') ? parseFloat(value[yKey]) : parseInt(value[yKey], 10)
                ]
              : value[yKey].includes('.')
              ? parseFloat(value[yKey])
              : parseInt(value[yKey], 10)
          )
        }))
      ];

      series.forEach((item, index) => {
        chart.addAxis({ id: item.id, title: { text: item.name }, opposite });
        chart.addSeries(
          {
            ...item,
            marker: {
              enabled: true,
              radius: 5
            },
            yAxis: item.id
          },
          true
        );
        opposite = !opposite;
      });
      this.setState({ chart });
    }
  }

  componentWillUnmount() {
    const { chart } = this.state;
    if (chart) chart.destroy();
  }

  render() {
    return <div id="lineChart" style={CHART_STYLES} />;
  }
}
