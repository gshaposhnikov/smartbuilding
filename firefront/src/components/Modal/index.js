import React, { Component } from 'react';

import { Modal } from 'antd';

export default class CustomModal extends Component {
  render() {
    const { onClose, children, title, isVisible, ...restProps } = this.props;
    if (isVisible) {
      return (
        <Modal title={title} visible={isVisible} onCancel={onClose} footer={null} {...restProps}>
          {children}
        </Modal>
      );
    } else {
      return null;
    }
  }
}
