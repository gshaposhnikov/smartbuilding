import styled from 'styled-components';

const ErrorTitle = styled.div`
  display: flex;
  align-items: center;
  padding: 0 15px;
  color: red;
  font-size: 0.9em;
`;

export default ErrorTitle;
