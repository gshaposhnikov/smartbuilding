import React from 'react';
import { Tooltip } from 'antd';
import styled from 'styled-components';

const Title = styled.div`
  padding: 0 15px;
  color: white;
  font-size: 14px;
  max-width: 500px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const TitleTooltip = ({ children }) => (
  <Tooltip title={children}>
    <Title>{children}</Title>
  </Tooltip>
);

export default TitleTooltip;
