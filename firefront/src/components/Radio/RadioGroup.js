import React from 'react';
import { Radio } from 'antd';

export default class RadioGroup extends React.Component {
  render() {
    const { onChange, children, ...restProps } = this.props;
    return (
      <Radio.Group onChange={onChange} {...restProps}>
        {children}
      </Radio.Group>
    );
  }
}
