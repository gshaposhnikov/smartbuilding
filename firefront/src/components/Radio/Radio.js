import React from 'react';
import styled from 'styled-components';
import { Radio } from 'antd';

export const RadioWrapper = styled.div`
  display: flex;
`;

export default class CustomRadio extends React.Component {
  static defaultProps = {
    isActive: true
  };

  render() {
    const { isActive, title, value, ...restProps } = this.props;

    return (
      <RadioWrapper isActive={isActive} {...restProps}>
        <Radio value={value}>{title}</Radio>
      </RadioWrapper>
    );
  }
}
