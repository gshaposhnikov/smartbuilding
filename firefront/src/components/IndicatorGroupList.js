import React from 'react';
import { Table, Column, AutoSizer } from 'react-virtualized';

const IndicatorGroupList = ({ indicatorGroups, onRowClick, topMargin = 0, rowSelection }) => (
  <AutoSizer>
    {({ width, height }) => (
      <Table
        width={width}
        height={height ? height - topMargin : 500}
        rowHeight={26}
        headerHeight={16}
        rowClassName={({ index }) =>
          index >= 0 && indicatorGroups[index].id === rowSelection ? 'selected-row ' : ''
        }
        rowGetter={({ index }) => indicatorGroups[index]}
        rowCount={indicatorGroups.length}
        onRowClick={onRowClick}
      >
        <Column
          label="Название"
          dataKey="name"
          key="name"
          width={width / 2}
          cellRenderer={({ rowData }) => <div>{rowData.name}</div>}
        />
        <Column
          label="Описание"
          dataKey="description"
          key="description"
          width={width / 2}
          cellRenderer={({ rowData }) => <div>{rowData.description}</div>}
        />
      </Table>
    )}
  </AutoSizer>
);

export default IndicatorGroupList;
