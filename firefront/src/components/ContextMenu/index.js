import React, { Component } from 'react';
import { Menu } from 'react-data-grid-addons';

const DefaultContextMenu = Menu.ContextMenu;

export default class ontextMenu extends Component{
  render() {
    return (
      <DefaultContextMenu>
        {this.props.children}
      </DefaultContextMenu>
    );
  }
};
