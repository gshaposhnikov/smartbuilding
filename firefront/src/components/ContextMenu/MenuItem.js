import React, { Component } from 'react';
import { Menu } from 'react-data-grid-addons';

const DefaultMenuItem = Menu.MenuItem;

export default class MenuItem extends Component{
  render() {
    return (
      <DefaultMenuItem onClick={this.props.onClick}>{this.props.children}</DefaultMenuItem>
    );
  }
};
