import React, { Component } from 'react';
import styled from 'styled-components';

const ListItemWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  margin-bottom: 5px;
`;

const Img = styled.div`
  margin-right: 5px;
`;

const EmptyImg = styled.div`
  display: block;
  width: 14px;
  height: 14px;

`;

export class ListItem extends Component {
  render() {
    const { img, name } = this.props;
    return <ListItemWrapper>
      <Img>{img ? <img width="14px" src={`/public/img/${img}`} alt="img"/> : <EmptyImg />}</Img>
      <span>{name}</span>
    </ListItemWrapper>;
  }
}

const List = styled.div`
  margin-bottom: 10px;
`;

export default List;