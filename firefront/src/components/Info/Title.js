import React from 'react';
import styled from 'styled-components';

const TitleStyles = styled.div`
  font-weight: bold;
  border-bottom: 1px solid gray;
  padding-bottom: 2px;
  margin-bottom: 5px;
`;

const Title = props => <TitleStyles {...props} />;

export default Title;
