import React from 'react';

import { Checkbox } from 'antd';
export default class CustomCheckbox extends React.Component {
  static defaultProps = {
    isActive: true,
    disabled: false
  };

  render() {
    const { isActive, value, disabled, ...restProps } = this.props;

    return isActive ? (
      <Checkbox
        checked={value === true || value === 'true' ? true : false}
        disabled={disabled}
        {...restProps}
      />
    ) : (
      <div>{value === true || value === 'true' ? 'Да' : 'Нет'}</div>
    );
  }
}
