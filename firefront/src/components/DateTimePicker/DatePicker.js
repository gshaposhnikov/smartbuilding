import React from 'react';
import { DatePicker } from 'antd';
export default class CustomDatePicker extends React.Component {
  render() {
    return <DatePicker format={"DD.MM.YYYY"} {...this.props} />;
  }
}
