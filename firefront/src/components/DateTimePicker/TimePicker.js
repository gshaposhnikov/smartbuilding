import React from 'react';
import { TimePicker } from 'antd';
export default class CustomTimePicker extends React.Component {
  render() {
    return <TimePicker format={'HH:mm:ss'} {...this.props} />;
  }
}
