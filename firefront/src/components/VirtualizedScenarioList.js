import React from 'react';
import { Table, Column } from 'react-virtualized';
import { Icon } from 'antd';

export default class ScenarioList extends React.Component {
  renderScenarioGlobalNoCell = record => {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <Icon
          type="clock-circle"
          style={{
            color: record.color
          }}
        />
        <div
          style={{
            color: record.fontColor
          }}
        >
          {record.globalNo}
        </div>
      </div>
    );
  };

  renderScenarioNameCell = record => {
    return (
      <div
        style={{
          color: record.fontColor
        }}
      >
        {record.name}
      </div>
    );
  };

  getRowClassName = record => {
    if (!record) return '';
    const { rowSelections = [] } = this.props;
    if (rowSelections.includes(record.id)) return 'selected-row';
    return '';
  };

  render = () => {
    const {
      width,
      height,
      scenarios = [],
      onRowDoubleClick = () => {},
      onRowClick = () => {}
    } = this.props;
    return (
      <Table
        width={width}
        height={height}
        rowHeight={26}
        headerHeight={16}
        rowGetter={({ index }) => scenarios[index]}
        rowCount={scenarios.length}
        onRowDoubleClick={onRowDoubleClick}
        onRowClick={onRowClick}
        rowClassName={({ index }) => this.getRowClassName(scenarios[index])}
      >
        <Column
          label="Номер"
          dataKey="globalNo"
          key="globalNo"
          width={60}
          cellRenderer={({ rowData }) => this.renderScenarioGlobalNoCell(rowData)}
        />
        <Column
          label="Название"
          dataKey="name"
          key="name"
          width={width - 60}
          cellRenderer={({ rowData }) => this.renderScenarioNameCell(rowData)}
        />
      </Table>
    );
  };
}
