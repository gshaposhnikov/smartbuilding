import React from 'react';
import Table from 'components/Table';

import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';

const defaultColumns = [
  {
    key: 'type',
    title: 'Тип устройства',
    dataIndex: 'type',
    stepper: true,
    render: (text, record) => <DeviceTypeTreeItem record={record} />
  },
  {
    key: 'shortAddressPath',
    title: 'Адрес',
    width: 100,
    dataIndex: 'shortAddressPath',
    render: (text, record) => <span>{text}</span>
  }
];
const DeviceList = ({ columns, devices = [], onRowClick, onRowDoubleClick, ...restProps }) => {
  return (
    <Table
      hideDefaultSelections={true}
      columns={columns ? columns : defaultColumns}
      dataSource={devices}
      rowKey="id"
      onRowClick={onRowClick}
      onRowDoubleClick={onRowDoubleClick}
      {...restProps}
    />
  );
};

export default DeviceList;
