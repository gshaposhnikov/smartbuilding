export function saveGroupIdToStorage(projectId, groupId) {
  let storage = localStorage.getItem('indicators');
  storage = storage ? JSON.parse(storage) : {};
  storage[projectId] = { groupId };
  storage = JSON.stringify(storage);
  localStorage.setItem('indicators', storage);
}

export function savePanelIdToStorage(projectId, groupId, panelId) {
  let storage = localStorage.getItem('indicators');
  storage = storage ? JSON.parse(storage) : {};
  storage[projectId] = { groupId, panelId };
  storage = JSON.stringify(storage);
  localStorage.setItem('indicators', storage);
}

export function removeIndicatorIdsFromStorageByProjectId(projectId) {
  let storage = localStorage.getItem('indicators');
  if (storage) {
    storage = JSON.parse(storage);
    if (storage[projectId]) {
      storage[projectId] = undefined;
      storage = JSON.stringify(storage);
      localStorage.setItem('indicators', storage);
    }
  }
}

export function removePanelIdFromStorageByProjectId(projectId) {
  let storage = localStorage.getItem('indicators');
  if (storage) {
    storage = JSON.parse(storage);
    if (storage[projectId]) {
      storage[projectId]['panelId'] = undefined;
      storage = JSON.stringify(storage);
      localStorage.setItem('indicators', storage);
    }
  }
}
