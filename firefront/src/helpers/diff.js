import React from 'react';
import { Icon } from 'antd';
import moment from 'moment';

import { sortDeviceTree } from 'helpers/device';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';

export const DIFF_STATES = {
  UNKNOWN: {
    id: 'UNKNOWN',
    name: 'Неизвестное',
    color: '#616161'
  },
  UNCHANGED: {
    id: 'UNCHANGED',
    name: 'Равны',
    color: '#fff'
  },
  UPDATED: {
    id: 'UPDATED',
    name: 'Отличаются',
    color: '#e57373'
  },
  CREATED: {
    id: 'CREATED',
    name: 'Отсутствует',
    color: '#7986cb',
    inverseDiffState: 'DELETED'
  },
  DELETED: {
    id: 'DELETED',
    name: 'Отсутствует в соседнем дереве',
    color: '#c5cae9',
    inverseDiffState: 'CREATED'
  },
  VARIED: {
    id: 'VARIED',
    name: 'Изменения в подузлах',
    color: '#fbc02d'
  }
};

// Создает дубликат JSON-элемента
const createJsonReplica = item => {
  return JSON.parse(JSON.stringify(item));
};

/**
 * Нахождение разницы
 *
 * Скрипт взят здесь: http://jsfiddle.net/sbgoran/kySNu/
 * и переписан в соответствии со стандартами es6
 */
class Diff {
  // Основная функция обхода данных
  map = (obj1, obj2) => {
    /**
     * TODO: Сделать нормальный выход из функции, если данные невозможно сравнить (Выход с ошибкой),
     *       а все вызовы этой функции обернуть в try-catch
     */
    if (this.isFunction(obj1) || this.isFunction(obj2)) return {}; // Выход из функции, если данные невозможно сравнить
    if (this.isValue(obj1) || this.isValue(obj2))
      return {
        type: this.compareValues(obj1, obj2),
        data: obj1 === undefined ? obj2 : obj1
      };

    const diff = {};

    for (const key in obj1) {
      if (this.isFunction(obj1[key])) continue;
      let value2 = undefined;
      if ('undefined' !== typeof obj2[key]) value2 = obj2[key];
      diff[key] = this.map(obj1[key], value2);
    }

    for (const key in obj2) {
      if (this.isFunction(obj2[key]) || 'undefined' !== typeof diff[key]) continue;
      diff[key] = this.map(undefined, obj2[key]);
    }

    return diff;
  };
  // Сравнение значений
  compareValues = (value1, value2) => {
    if (value1 === value2) return DIFF_STATES.UNCHANGED.id;
    if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime())
      return DIFF_STATES.UNCHANGED.id;
    if ('undefined' === typeof value1) return DIFF_STATES.CREATED.id;
    if ('undefined' === typeof value2) return DIFF_STATES.DELETED.id;

    return DIFF_STATES.UPDATED.id;
  };
  /**
   * Проверки типов
   */
  isFunction = obj => ({}.toString.apply(obj) === '[object Function]');
  isArray = obj => ({}.toString.apply(obj) === '[object Array]');
  isObject = obj => ({}.toString.apply(obj) === '[object Object]');
  isDate = obj => ({}.toString.apply(obj) === '[object Date]');
  isValue = obj => !this.isObject(obj) && !this.isArray(obj);
}

/**
 * Получение разницы списков зон
 * @param {Object[]} props - зоны (текущие, восстановленные)
 * @return {Object[]}
 */
export class RegionsDiff extends Diff {
  constructor({ fromProject, fromDevice, regionConstants }) {
    super();
    this.isEqual = true;
    this.fromProject = fromProject;
    this.fromDevice = fromDevice;
    this.regionConstants = regionConstants;
    this.ignorableFields = ['id', 'planLayouts', 'projectId', 'region', 'index'];
    this.fieldNames = {
      name: 'Название',
      description: 'Примечание',
      subsystem: 'Подсистема',
      autoRelock: 'Автоперевзятие',
      silenceAlarm: 'Тихая тревога',
      securityRegionType: 'Вид охранной зоны',
      inputOutputTimeout: 'Входная/выходная задержка',
      fireEventCount: 'Кол-во датчиков перехода'
    };
  }
  /**
   * Главная функция обработки данных
   *  - Задает всю логику обработки данных
   *  - Возвращает обработанные данные
   */
  get = () => {
    this.flatLists(); // Формируем "плоские" сущности и очищаем от мусора
    this.prepareToDiff(); // Подготавливаем данные для нахождения разницы
    this.findDiff(); // Находим разницу
    this.prepareToView(); // Подготавливаем отображение разницы
    return {
      fromProject: this.fromProjectView,
      fromDevice: this.fromDeviceView,
      isEqual: this.isEqual
    };
  };
  // Выносим необходимые параметы из подузлов и удаляем ненужные поля
  flatLists = () => {
    this.fromProject = this.flatList(this.fromProject);
    this.fromDevice = this.flatList(this.fromDevice);
  };
  flatList = regions => {
    return regions.map(regionView => {
      const newRegionView = {
        ...regionView,
        cellNameRender: (text, node) => `${regionView.name} (${regionView.region.index})`,
        fields: { ...regionView, ...regionView.region },
        state: { expanded: true }
      };
      // Удаляем поля, которые не нужно сравнивать
      this.ignorableFields.forEach(fieldName => {
        if (newRegionView.fields[fieldName]) {
          delete newRegionView.fields[fieldName];
        }
      });
      return newRegionView;
    });
  };
  // Подготовка данных к сравнению
  prepareToDiff = () => {
    this.mappedFromProject = this.getMappedList(this.fromProject);
    this.mappedFromDevice = this.getMappedList(this.fromDevice);
  };
  // Функция делает удобные данные для скрипта
  getMappedList = list => {
    const obj = {};
    list.forEach(item => (obj[item.region.index] = item.fields));
    return obj;
  };
  // Находит разницу
  findDiff = () => {
    this.diff = this.map(this.mappedFromProject, this.mappedFromDevice);
  };
  // Генерирует отображение поля
  getFieldView = (value, fieldName, diffState) => {
    return {
      name: this.fieldNames[fieldName] || fieldName,
      value,
      diffState,
      id: fieldName
    };
  };
  // Главная функция подготовки данных к отображению
  prepareToView = () => {
    this.fromProjectView = [];
    this.fromDeviceView = [];

    Object.keys(this.diff).forEach(itemIndex => {
      let projectItem = this.fromProject.find(entity => `${entity.region.index}` === itemIndex);
      let deviceItem = this.fromDevice.find(entity => `${entity.region.index}` === itemIndex);
      const mappedProjectItem = this.mappedFromProject[itemIndex];
      const mappedDeviceItem = this.mappedFromDevice[itemIndex];
      // Если не была найден соответствующая сущность в соседнем дереве
      if (!projectItem && deviceItem) projectItem = { ...deviceItem };
      else if (!deviceItem && projectItem) deviceItem = { ...projectItem };

      if (!projectItem.children) projectItem['children'] = [];
      if (!deviceItem.children) deviceItem['children'] = [];
      // Если элемент на входе уже имеет разницу (удалено/добавлено)
      if (this.diff[itemIndex].type && this.diff[itemIndex].data) {
        switch (this.diff[itemIndex].type) {
          case 'CREATED':
          case 'DELETED': {
            this.isEqual = false;
            const diffState = DIFF_STATES[this.diff[itemIndex].type];
            const inverseDiffState = diffState.inverseDiffState
              ? DIFF_STATES[diffState.inverseDiffState]
              : diffState;
            // Назначаем цвет
            projectItem.diffState = diffState;
            deviceItem.diffState = inverseDiffState;

            if (mappedProjectItem)
              Object.keys(mappedProjectItem).forEach(fieldName => {
                const value =
                  fieldName === 'securityRegionType'
                    ? this.regionConstants.securityTypes.find(
                        securityType => securityType.id === mappedProjectItem[fieldName]
                      ).name
                    : mappedProjectItem[fieldName];
                const fieldView = this.getFieldView(value, fieldName, diffState);

                projectItem.children.push(fieldView);
                if (!mappedDeviceItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, inverseDiffState);
                  deviceItem.children.push(inverseFieldView);
                }
              });

            if (mappedDeviceItem)
              Object.keys(mappedDeviceItem).forEach(fieldName => {
                const value =
                  fieldName === 'securityRegionType'
                    ? this.regionConstants.securityTypes.find(
                        securityType => securityType.id === mappedDeviceItem[fieldName]
                      ).name
                    : mappedDeviceItem[fieldName];
                const fieldView = this.getFieldView(value, fieldName, inverseDiffState);
                projectItem.children.push(fieldView);
                if (!mappedProjectItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, diffState);
                  projectItem.children.push(inverseFieldView);
                }
              });
            break;
          }
          default: {
            break;
          }
        }
      }
      // Если есть изменения в полях
      else {
        const diff = this.diff[itemIndex];
        Object.keys(diff).forEach(fieldName => {
          const fieldDiff = diff[fieldName];

          switch (fieldDiff.type) {
            case 'UPDATED':
            case 'UNCHANGED': {
              const diffState = DIFF_STATES[fieldDiff.type];
              const projectValue =
                fieldName === 'securityRegionType'
                  ? this.regionConstants.securityTypes.find(
                      securityType => securityType.id === mappedProjectItem[fieldName]
                    ).name
                  : mappedProjectItem[fieldName];
              const deviceValue =
                fieldName === 'securityRegionType'
                  ? this.regionConstants.securityTypes.find(
                      securityType => securityType.id === mappedDeviceItem[fieldName]
                    ).name
                  : mappedDeviceItem[fieldName];

              if (fieldDiff.type === 'UPDATED') {
                this.isEqual = false;
                projectItem.diffState = DIFF_STATES['VARIED'];
                deviceItem.diffState = DIFF_STATES['VARIED'];
              }
              projectItem.children.push(this.getFieldView(projectValue, fieldName, diffState));
              deviceItem.children.push(this.getFieldView(deviceValue, fieldName, diffState));
              break;
            }
            case 'DELETED':
            case 'CREATED': {
              this.isEqual = false;
              const diffState = DIFF_STATES[fieldDiff.type];
              const inverseDiffState = diffState.inverseDiffState
                ? DIFF_STATES[diffState.inverseDiffState]
                : diffState;

              if (mappedProjectItem[fieldName]) {
                const value =
                  fieldName === 'securityRegionType'
                    ? this.regionConstants.securityTypes.find(
                        securityType => securityType.id === mappedProjectItem[fieldName]
                      ).name
                    : mappedProjectItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedDeviceItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              if (mappedDeviceItem[fieldName]) {
                const value =
                  fieldName === 'securityRegionType'
                    ? this.regionConstants.securityTypes.find(
                        securityType => securityType.id === mappedDeviceItem[fieldName]
                      ).name
                    : mappedDeviceItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedProjectItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              break;
            }
            default:
              break;
          }
        });
      }

      this.fromProjectView.push(projectItem);
      this.fromDeviceView.push(deviceItem);
    });
  };
}

/**
 * Получение разницы списков устройств
 * @param {Object[]} props - устройства (текущие, восстановленные)
 * @return {Object[]}
 */
export class DevicesDiff extends Diff {
  constructor({ fromProject, fromDevice }) {
    super();
    this.isEqual = true;
    this.fromProject = fromProject;
    this.fromDevice = fromDevice;
    this.availableFields = {
      name: true,
      description: true,
      aggregatedPropertyViews: true,
      subsystem: true
    };
    this.fieldNames = {
      name: 'Название',
      description: 'Примечание',
      subsystem: 'Подсистема'
    };
  }
  /**
   * Главная функция обработки данных
   *  - Задает всю логику обработки данных
   *  - Возвращает обработанные данные
   */
  get = () => {
    this.getDevicesTrees(); // Формируем "плоские" сущности и очищаем от мусора
    this.mapTreeItemsAndFindDiff(); // Находим разницу
    this.sortDeviceTrees(); // Сортируем полученные деревья
    return {
      fromProject: this.fromProject,
      fromDevice: this.fromDevice,
      isEqual: this.isEqual
    };
  };
  // Вызов сортировки деревьев
  sortDeviceTrees = () => {
    sortDeviceTree(this.fromProject);
    sortDeviceTree(this.fromDevice);
  };
  // Получение дерева устройств
  getDevicesTrees = () => {
    this.fromProject = this.getDevicesTree(this.fromProject);
    this.fromDevice = this.getDevicesTree(this.fromDevice);
  };
  getDevicesTree = (listDevices, callback = item => item) => {
    const tree = {};
    let rootDevices = [];
    if (listDevices.length === 0) {
      return [];
    } else {
      for (let i = 0; i < listDevices.length; ++i) {
        const device = { ...listDevices[i] },
          parentId = device.parentDeviceId;
        device.key = device.id;

        if (tree[device.key]) {
          Object.assign(tree[device.key], device);
        } else tree[device.key] = device;

        if (device.deviceCategory === 'CONTROL') {
          rootDevices.push(callback(tree[device.key]));
        } else {
          if (tree[parentId] && !tree[parentId].children) {
            tree[parentId].children = [];
          }
          if (!tree[parentId]) {
            tree[parentId] = { children: [] };
          }
          tree[parentId].children.push(callback(tree[device.key]));
          // Записываем информацию о родителе
          tree[device.key].parent = tree[parentId];
        }
      }
      return rootDevices;
    }
  };
  // Функция вызова обхода деревьев
  mapTreeItemsAndFindDiff = () => {
    const fromProjectReplica = this.fromProject,
      fromDeviceReplica = this.fromDevice;

    this.mapTreeItems(this.fromProject, fromDeviceReplica);
    this.mapTreeItems(this.fromDevice, fromProjectReplica);
  };
  // Главная функция обхода дерева
  mapTreeItems = (mainList = [], minorList = [], mainItem, minorItem) => {
    mainList.forEach((treeItem, index) => {
      if (!treeItem.additional) {
        switch (treeItem.deviceCategory) {
          case 'CONTROL': {
            treeItem.cellNameRender = (text, node) => (
              <span>
                <DeviceTypeTreeItem
                  record={{ ...node, name: `${node.name} (${node.shortAddressPath})` }}
                />
              </span>
            );

            treeItem.state = { expanded: true };
            minorList[index].state = { expanded: true };

            this.mapTreeItems(
              treeItem.children,
              minorList[index].children,
              treeItem,
              minorList[index]
            );
            break;
          }
          case 'CONTAINER': {
            treeItem.cellNameRender = (text, node) => (
              <span>
                <DeviceTypeTreeItem
                  record={{ ...node, name: `${node.name} (${node.shortAddressPath})` }}
                />
              </span>
            );

            let minorTreeItem = minorList.find(item => {
              if (item.addressLevel !== treeItem.addressLevel) return false;
              if (
                item.addressPath
                  .split(',')
                  .slice(4)
                  .join(',') ===
                treeItem.addressPath
                  .split(',')
                  .slice(4)
                  .join(',')
              )
                return true;
              return false;
            });
            if (minorTreeItem) {
              // Если категории устройств не совпадают
              if (treeItem.deviceCategory !== minorTreeItem.deviceCategory) {
                treeItem.children = [];
                minorTreeItem.children = [];
                this.findDiffAndPrepareToView(treeItem, minorTreeItem);
              } else {
                this.mapTreeItems(treeItem.children, minorTreeItem.children);
              }
            }
            // Если не нашли соответствующий элемент в соседнем дереве
            else {
              // Проставляем статусы для родительских элементов
              this.setVariedStateForParents(treeItem.parent);
              this.setVariedStateForParents(minorItem);
              // Пересоздаем элемент дерева
              minorTreeItem = {
                ...treeItem,
                additional: true,
                children: [...treeItem.children],
                parent: minorItem
              };

              minorList.push(minorTreeItem);
              // Находим дифф для дочерних элементов
              treeItem.children.forEach((treeItemChild, index) => {
                minorTreeItem.children[index] = {
                  ...minorTreeItem.children[index],
                  additional: true,
                  parent: minorTreeItem
                };
                treeItemChild.children = [];
                minorTreeItem.children[index].children = [];

                this.findDiffAndPrepareToView(treeItemChild, minorTreeItem.children[index], true);
              });
            }
            break;
          }
          case 'SENSOR':
          case 'EXECUTIVE': {
            treeItem.cellNameRender = (text, node) => (
              <span>
                <DeviceTypeTreeItem
                  record={{ ...node, name: `${node.name} (${node.shortAddressPath})` }}
                />
              </span>
            );

            let minorTreeItem = minorList.find(item => {
              if (item.addressLevel !== treeItem.addressLevel) return false;
              if (
                item.addressPath
                  .split(',')
                  .slice(4)
                  .join(',') ===
                treeItem.addressPath
                  .split(',')
                  .slice(4)
                  .join(',')
              )
                return true;
              return false;
            });
            if (minorTreeItem) {
              treeItem.children = [];
              minorTreeItem.children = [];
              this.findDiffAndPrepareToView(treeItem, minorTreeItem);
            } else {
              minorTreeItem = { ...treeItem, parent: minorItem, additional: true, children: [] };
              minorList.push(minorTreeItem);
              treeItem.children = [];
              this.findDiffAndPrepareToView(treeItem, minorTreeItem, true);
            }
            break;
          }
          default:
            break;
        }
      }
    });
  };
  // Функция для генерациия удобных данных для скрипта (для работы со списком параметров)
  getMappedProperties = properties => {
    const mappedConfig = {};
    properties.forEach((property, index) => {
      if (property.id !== 'AlternateIcon') mappedConfig[index] = property.projectValue;
    });
    return mappedConfig;
  };
  // Функция генерациия удобных данных для скрипта (Поле: Параметры)
  getMappedParams = treeItem => {
    const mappedParams = {
      common: {},
      aggregated: {}
    };

    Object.keys(treeItem).forEach(fieldName => {
      if (this.availableFields[fieldName]) {
        if (fieldName === 'aggregatedPropertyViews')
          mappedParams.aggregated = this.getMappedProperties(treeItem[fieldName]);
        else {
          mappedParams.common[fieldName] = treeItem[fieldName];
        }
      }
    });
    return mappedParams;
  };
  // Функция для проставления статусов родительским элементам, если были какие-то изменения в подузлах
  setVariedStateForParents = item => {
    item.diffState = DIFF_STATES.VARIED;
    if (item.parent) this.setVariedStateForParents(item.parent);
  };
  // Функция для проставления статусов родительским элементам, если соседний элемент "добавленный"
  setDiffStateForAdditionalItems = (mainItem, diffState, minorItem) => {
    if (mainItem.additional || minorItem.additional) mainItem.diffState = diffState;
    if (
      (mainItem.parent && mainItem.parent.additional) ||
      (minorItem.parent && minorItem.parent.additional)
    )
      this.setDiffStateForAdditionalItems(mainItem.parent, diffState, minorItem.parent);
  };
  // Генерация отображения поля
  getFieldView = (name, value, id, diffState) => ({ name, value, id, diffState });
  // Главная функция нахождения разницы и подготовки к отображению
  findDiffAndPrepareToView = (mainTreeItem, minorTreeItem, additional) => {
    /**
     * Общие блоки для параметров устройства
     */
    const mainTreeItemParamsDiff = {
        id: 'mainTreeItemParamsDiff',
        parent: mainTreeItem,
        name: 'Параметры',
        children: [],
        additional
      },
      mainTreeItemConfigDiff = {
        id: 'mainTreeItemConfigDiff',
        parent: mainTreeItem,
        name: 'Конфигурация',
        children: [],
        additional
      },
      minorTreeItemParamsDiff = {
        id: 'minorTreeItemParamsDiff',
        parent: minorTreeItem,
        name: 'Параметры',
        children: [],
        additional
      },
      minorTreeItemConfigDiff = {
        id: 'minorTreeItemConfigDiff',
        parent: minorTreeItem,
        name: 'Конфигурация',
        children: [],
        additional
      },
      /**
       * Подготовка данных для скрипта
       */
      mappedMainTreeItemParams = this.getMappedParams(mainTreeItem),
      mappedMinorTreeItemParams = this.getMappedParams(minorTreeItem),
      mappedMainTreeItemConfig = this.getMappedProperties(mainTreeItem.configPropertyViews),
      mappedMinorTreeItemConfig = this.getMappedProperties(minorTreeItem.configPropertyViews),
      /**
       * Нахождение диффа
       */
      commonParamsDiff = this.map(
        mappedMainTreeItemParams.common,
        mappedMinorTreeItemParams.common
      ),
      aggregatedPropertiesDiff = this.map(
        mappedMainTreeItemParams.aggregated,
        mappedMinorTreeItemParams.aggregated
      ),
      configPropertiesDiff = this.map(mappedMainTreeItemConfig, mappedMinorTreeItemConfig);
    // Добавляем блоки отображения параметров в устройства
    mainTreeItem.children.push(mainTreeItemParamsDiff, mainTreeItemConfigDiff);
    minorTreeItem.children.push(minorTreeItemParamsDiff, minorTreeItemConfigDiff);
    /**
     * Генерация отображения полей
     */

    // Общие параметры
    Object.keys(commonParamsDiff).forEach(fieldName => {
      const diffState = additional
          ? DIFF_STATES.DELETED
          : DIFF_STATES[commonParamsDiff[fieldName].type],
        inverseDiffState = diffState.inverseDiffState
          ? DIFF_STATES[diffState.inverseDiffState]
          : diffState;

      if (
        diffState.id === DIFF_STATES.UPDATED.id ||
        (!additional &&
          (diffState.id === DIFF_STATES.DELETED.id || diffState.id === DIFF_STATES.CREATED.id))
      ) {
        this.setVariedStateForParents(mainTreeItemParamsDiff);
        this.setVariedStateForParents(minorTreeItemParamsDiff);
      }

      if (additional) {
        this.setDiffStateForAdditionalItems(
          mainTreeItemParamsDiff,
          diffState,
          minorTreeItemParamsDiff
        );
        this.setDiffStateForAdditionalItems(
          minorTreeItemParamsDiff,
          inverseDiffState,
          mainTreeItemParamsDiff
        );
      }

      mainTreeItemParamsDiff.children.push(
        this.getFieldView(
          this.fieldNames[fieldName] || fieldName,
          mappedMainTreeItemParams.common[fieldName],
          fieldName,
          diffState
        )
      );
      minorTreeItemParamsDiff.children.push(
        this.getFieldView(
          this.fieldNames[fieldName] || fieldName,
          mappedMinorTreeItemParams.common[fieldName],
          fieldName,
          inverseDiffState
        )
      );
    });
    // Прочие параметры
    Object.keys(aggregatedPropertiesDiff).forEach(fieldName => {
      const diffState = additional
          ? DIFF_STATES.DELETED
          : DIFF_STATES[aggregatedPropertiesDiff[fieldName].type],
        inverseDiffState = diffState.inverseDiffState
          ? DIFF_STATES[diffState.inverseDiffState]
          : diffState;

      if (
        diffState.id === DIFF_STATES.UPDATED.id ||
        (!additional &&
          (diffState.id === DIFF_STATES.DELETED.id || diffState.id === DIFF_STATES.CREATED.id))
      ) {
        this.isEqual = false;
        this.setVariedStateForParents(mainTreeItemParamsDiff);
        this.setVariedStateForParents(minorTreeItemParamsDiff);
      }

      if (additional) {
        this.isEqual = false;
        this.setDiffStateForAdditionalItems(
          mainTreeItemParamsDiff,
          diffState,
          minorTreeItemParamsDiff
        );
        this.setDiffStateForAdditionalItems(
          minorTreeItemParamsDiff,
          inverseDiffState,
          mainTreeItemParamsDiff
        );
      }

      mainTreeItemParamsDiff.children.push(
        this.getFieldView(
          mainTreeItem.aggregatedPropertyViews.hasOwnProperty(fieldName)
            ? mainTreeItem.aggregatedPropertyViews[fieldName].name
            : minorTreeItem.aggregatedPropertyViews[fieldName].name,
          mainTreeItem.aggregatedPropertyViews.hasOwnProperty(fieldName)
            ? mainTreeItem.aggregatedPropertyViews[fieldName].projectValue
            : minorTreeItem.aggregatedPropertyViews[fieldName].projectValue,
          fieldName,
          diffState
        )
      );
      minorTreeItemParamsDiff.children.push(
        this.getFieldView(
          minorTreeItem.aggregatedPropertyViews.hasOwnProperty(fieldName)
            ? minorTreeItem.aggregatedPropertyViews[fieldName].name
            : mainTreeItem.aggregatedPropertyViews[fieldName].name,
          minorTreeItem.aggregatedPropertyViews.hasOwnProperty(fieldName)
            ? minorTreeItem.aggregatedPropertyViews[fieldName].projectValue
            : mainTreeItem.aggregatedPropertyViews[fieldName].projectValue,
          fieldName,
          inverseDiffState
        )
      );
    });
    // Конфигурационные параметры
    Object.keys(configPropertiesDiff).forEach(fieldName => {
      const diffState = additional
          ? DIFF_STATES.DELETED
          : DIFF_STATES[configPropertiesDiff[fieldName].type],
        inverseDiffState = diffState.inverseDiffState
          ? DIFF_STATES[diffState.inverseDiffState]
          : diffState;

      if (
        diffState.id === DIFF_STATES.UPDATED.id ||
        (!additional &&
          (diffState.id === DIFF_STATES.DELETED.id || diffState.id === DIFF_STATES.CREATED.id))
      ) {
        this.setVariedStateForParents(mainTreeItemConfigDiff);
        this.setVariedStateForParents(minorTreeItemConfigDiff);
      }

      if (additional) {
        this.setDiffStateForAdditionalItems(
          mainTreeItemConfigDiff,
          diffState,
          minorTreeItemConfigDiff
        );
        this.setDiffStateForAdditionalItems(
          minorTreeItemConfigDiff,
          inverseDiffState,
          mainTreeItemConfigDiff
        );
      }

      mainTreeItemConfigDiff.children.push(
        this.getFieldView(
          mainTreeItem.configPropertyViews.hasOwnProperty(fieldName)
            ? mainTreeItem.configPropertyViews[fieldName].name
            : minorTreeItem.configPropertyViews[fieldName].name,
          mainTreeItem.configPropertyViews.hasOwnProperty(fieldName)
            ? mainTreeItem.configPropertyViews[fieldName].projectValue
            : minorTreeItem.configPropertyViews[fieldName].projectValue,
          fieldName,
          diffState
        )
      );
      minorTreeItemConfigDiff.children.push(
        this.getFieldView(
          minorTreeItem.configPropertyViews.hasOwnProperty(fieldName)
            ? minorTreeItem.configPropertyViews[fieldName].name
            : mainTreeItem.configPropertyViews[fieldName].name,
          minorTreeItem.configPropertyViews.hasOwnProperty(fieldName)
            ? minorTreeItem.configPropertyViews[fieldName].projectValue
            : mainTreeItem.configPropertyViews[fieldName].projectValue,
          fieldName,
          inverseDiffState
        )
      );
    });
  };
}

export class ScenariosDiff extends Diff {
  constructor({ fromProject, fromDevice, scenarioConstants }) {
    super();
    this.isEqual = true;
    this.scenarioConstants = scenarioConstants;
    this.fromProject = fromProject.map(item => ({ ...item, state: { expanded: true } }));
    this.fromDevice = fromDevice.map(item => ({ ...item, state: { expanded: true } }));
    this.fromProject = createJsonReplica(this.fromProject);
    this.fromDevice = createJsonReplica(this.fromDevice);

    this.availableFields = {
      name: true,
      description: true,
      stopType: true,
      startLogicSentence: true,
      stopLogicSentence: true,
      scenarioPurpose: true,
      enabled: true,
      scenarioType: true,
      stopOnGlobalReset: true,
      manualStartStopAllowed: true
    };
    this.fieldNames = {
      name: 'Название',
      description: 'Примечание',
      stopType: 'Тип остановки',
      startLogicSentence: 'Логика включения',
      stopLogicSentence: 'Логика выключения',
      scenarioPurpose: 'Назначение',
      enabled: 'Включен',
      scenarioType: 'Тип',
      stopOnGlobalReset: 'Выкл. при сбросе',
      manualStartStopAllowed: 'Управление в ОЗ'
    };
  }

  get = () => {
    this.prepareParamsToDiff();
    this.findParamsDiff();
    this.prepareParamsToView();
    this.prepareExecBlocksToDiff();

    return {
      fromProject: this.fromProjectView,
      fromDevice: this.fromDeviceView,
      isEqual: this.isEqual
    };
  };

  prepareExecBlocksToDiff = () => {
    this.fromProjectView = createJsonReplica(this.fromProjectView);
    this.fromDeviceView = createJsonReplica(this.fromDeviceView);

    Object.keys(this.paramsDiff).forEach(scenarioIndex => {
      let mainScenario = this.fromProject.find(item => `${item.globalNo}` === scenarioIndex);
      let minorScenario = this.fromDevice.find(item => `${item.globalNo}` === scenarioIndex);

      let mainTimeLineTree = mainScenario
        ? {
            id: 'mainexec' + scenarioIndex,
            name: 'Исполнительная часть',
            itemType: 'ROOT',
            children: mainScenario.timeLineBlocksTree.filter(
              item => item.itemType !== 'ROOT' && item.itemType !== 'OUT'
            )
          }
        : null;
      let minorTimeLineTree = minorScenario
        ? {
            id: 'minorexec' + scenarioIndex,
            name: 'Исполнительная часть',
            itemType: 'ROOT',
            children: minorScenario.timeLineBlocksTree.filter(
              item => item.itemType !== 'ROOT' && item.itemType !== 'OUT'
            )
          }
        : null;

      if (!mainTimeLineTree && minorTimeLineTree)
        mainTimeLineTree = createJsonReplica(minorTimeLineTree);
      if (mainTimeLineTree && !minorTimeLineTree)
        minorTimeLineTree = createJsonReplica(mainTimeLineTree);

      this.mapTreeAndFindDiff(mainTimeLineTree, minorTimeLineTree, !mainScenario, !minorScenario);

      mainTimeLineTree.children.sort((a, b) => {
        return a.timeDelaySec > b.timeDelaySec ? 1 : a.timeDelaySec < b.timeDelaySec ? -1 : 0;
      });
      minorTimeLineTree.children.sort((a, b) => {
        return a.timeDelaySec > b.timeDelaySec ? 1 : a.timeDelaySec < b.timeDelaySec ? -1 : 0;
      });
      // Добавляем исполнительную часть в отображение сценария проекта
      const projectScenario = this.fromProjectView.find(
        scenario => `${scenario.globalNo}` === scenarioIndex
      );
      projectScenario.children.push(mainTimeLineTree);
      // Проставляем статус, если необходимо
      if (!projectScenario.diffState && mainTimeLineTree.diffState)
        projectScenario.diffState = DIFF_STATES.VARIED;
      // Добавляем исполнительную часть в отображение сценария прибора
      const deviceSceanrio = this.fromDeviceView.find(
        scenario => `${scenario.globalNo}` === scenarioIndex
      );
      deviceSceanrio.children.push(minorTimeLineTree);
      // Проставляем статус, если необходимо
      if (!deviceSceanrio.diffState && minorTimeLineTree.diffState)
        deviceSceanrio.diffState = DIFF_STATES.VARIED;
    });
  };
  // Добавляем функции рендеринга для элементов дерева
  addRenderFunctions = (mainChild, minorChild) => {
    mainChild.cellNameRender = (text, record) => this.renderTimeCell(text, record);
    mainChild.cellValueRender = (text, record) => this.renderActionCell(text, record);
    minorChild.cellNameRender = (text, record) => this.renderTimeCell(text, record);
    minorChild.cellValueRender = (text, record) => this.renderActionCell(text, record);
  };
  // Проставляем статусы для родительских элементов, если есть изменения в подузлах
  setVariedStateForParents = (main, minor, mainChild, minorChild) => {
    if (
      mainChild.diffState &&
      (mainChild.diffState.id === DIFF_STATES.VARIED.id ||
        mainChild.diffState.id === DIFF_STATES.CREATED.id ||
        mainChild.diffState.id === DIFF_STATES.DELETED.id ||
        mainChild.diffState.id === DIFF_STATES.UPDATED.id)
    ) {
      main.diffState = DIFF_STATES.VARIED;
      minor.diffState = DIFF_STATES.VARIED;
    }
    if (
      minorChild.diffState &&
      (minorChild.diffState.id === DIFF_STATES.VARIED.id ||
        minorChild.diffState.id === DIFF_STATES.CREATED.id ||
        minorChild.diffState.id === DIFF_STATES.DELETED.id ||
        minorChild.diffState.id === DIFF_STATES.UPDATED.id)
    ) {
      main.diffState = DIFF_STATES.VARIED;
      minor.diffState = DIFF_STATES.VARIED;
    }
  };

  /**
   * Главная функция обхода исполнительной дерева и поиска разницы
   * @param {Object} main - основной элемент дерева
   * @param {Object} minor - смежный элемент дерева
   * @param {boolean|undefined} isMainAdded - является ли основной элемент добавленным (копией)
   * @param {boolean|undefined} isMinorAdded - является ли смежный элемент добавленным (копией)
   * @param {number|undefined} level - глубина дерева
   */
  mapTreeAndFindDiff = (main, minor, isMainAdded, isMinorAdded, level = 0) => {
    // Обход временных элементов дерева
    if (!level) {
      // Проставляем статусы
      if (isMainAdded) {
        this.isEqual = false;
        minor.diffState = DIFF_STATES.DELETED;
        main.diffState = DIFF_STATES.CREATED;
      } else if (isMinorAdded) {
        this.isEqual = false;
        minor.diffState = DIFF_STATES.CREATED;
        main.diffState = DIFF_STATES.DELETED;
      } else {
        if (main.children.length !== minor.children.length) {
          this.isEqual = false;
          main.diffState = DIFF_STATES.VARIED;
          minor.diffState = DIFF_STATES.VARIED;
        }
      }
      // Пробегаемся по подузлам основного элемента дерева
      main.children.forEach((mainChild, index) => {
        mainChild.id = `${level}.${mainChild.timeDelaySec}.${index}`;

        let minorChild = isMinorAdded
          ? minor.children[index]
          : minor.children.find(
              item =>
                item.timeDelaySec === mainChild.timeDelaySec &&
                item.itemType === mainChild.itemType &&
                !item.additional &&
                !item.diffState
            );

        if (minorChild) {
          minorChild.id = `${level}.${minorChild.timeDelaySec}.${index}`;
        } else {
          this.isEqual = false;
          minorChild = createJsonReplica(mainChild);
          minorChild.additional = true;
          minor.children.push(minorChild);
        }
        // Добавляем функции рендеринга
        this.addRenderFunctions(mainChild, minorChild);
        // Обходим подузлы
        this.mapTreeAndFindDiff(
          mainChild,
          minorChild,
          mainChild.additional || isMainAdded,
          minorChild.additional || isMinorAdded,
          level + 1
        );
        // Проверяем изменения в подузлах
        this.setVariedStateForParents(main, minor, mainChild, minorChild);
      });
      // Пробегаемся по подузлам смежного элемента дерева
      minor.children.forEach((minorChild, index) => {
        if (!minorChild.additional && !minorChild.diffState) {
          minorChild.id = `${level}.${minorChild.timeDelaySec}.${index}`;

          let mainChild = isMainAdded
            ? main.children[index]
            : main.children.find(
                item =>
                  item.timeDelaySec === minorChild.timeDelaySec &&
                  item.itemType === minorChild.itemType &&
                  !item.additional &&
                  !item.diffState
              );

          if (mainChild) {
            mainChild.id = `${level}.${mainChild.timeDelaySec}.${index}`;
          } else {
            this.isEqual = false;
            mainChild = createJsonReplica(minorChild);
            mainChild.additional = true;
            main.children.push(mainChild);
          }
          // Добавляем функции рендеринга
          this.addRenderFunctions(mainChild, minorChild);
          // Обходим подузлы
          this.mapTreeAndFindDiff(
            minorChild,
            mainChild,
            minorChild.additional || isMinorAdded,
            mainChild.additional || isMainAdded,
            level + 1
          );
          // Проверяем изменения в подузлах
          this.setVariedStateForParents(main, minor, mainChild, minorChild);
        }
      });
    }
    // Обход исполнительных элементов дерева
    else {
      // Удаляем корневые элементы
      if (main.children && main.children.length)
        main.children = main.children.filter(mainChild => mainChild.itemType !== 'OUT');
      if (minor.children && minor.children.length)
        minor.children = minor.children.filter(minorChild => minorChild.itemType !== 'OUT');
      // Проставляем статусы
      if (isMainAdded) {
        this.isEqual = false;
        minor.diffState = DIFF_STATES.DELETED;
        main.diffState = DIFF_STATES.CREATED;
      } else if (isMinorAdded) {
        this.isEqual = false;
        main.diffState = DIFF_STATES.DELETED;
        minor.diffState = DIFF_STATES.CREATED;
      } else {
        if (main.children.length !== minor.children.length) {
          this.isEqual = false;
          main.diffState = DIFF_STATES.VARIED;
          minor.diffState = DIFF_STATES.VARIED;
        }
        if (main.additional) {
          this.isEqual = false;
          main.diffState = DIFF_STATES.CREATED;
          minor.diffState = DIFF_STATES.DELETED;
        }
        if (minor.additional) {
          this.isEqual = false;
          main.diffState = DIFF_STATES.DELETED;
          minor.diffState = DIFF_STATES.CREATED;
        }
      }
      // Пробегаемся по подузлам основного элемента дерева
      if (main.children && main.children.length) {
        main.children.forEach((mainChild, index) => {
          if (!mainChild.additional && !mainChild.diffState) {
            mainChild.id = `${main.id}.${index}`;
            let minorChild = minor.children.find(
              item =>
                item.description === mainChild.description &&
                item.timeDelaySec === mainChild.timeDelaySec &&
                item.itemType === mainChild.itemType &&
                !item.additional &&
                !item.diffState
            );

            if (minorChild) {
              minorChild.id = `${minor.id}.${index}`;

              if (mainChild.actionName !== minorChild.actionName) {
                mainChild.diffState = DIFF_STATES.UPDATED;
                minorChild.diffState = DIFF_STATES.UPDATED;
                main.diffState = DIFF_STATES.VARIED;
                minor.diffState = DIFF_STATES.VARIED;
                this.isEqual = false;
              } else if (mainChild.description !== minorChild.description) {
                mainChild.diffState = DIFF_STATES.UPDATED;
                minorChild.diffState = DIFF_STATES.UPDATED;
                main.diffState = DIFF_STATES.VARIED;
                minor.diffState = DIFF_STATES.VARIED;
                this.isEqual = false;
              }
            } else {
              this.isEqual = false;
              minorChild = createJsonReplica(mainChild);
              minorChild.additional = true;
              minor.children.push(minorChild);
            }
            // Добавляем функции рендеринга
            this.addRenderFunctions(mainChild, minorChild);
            // Проверяем изменения в подузлах
            this.mapTreeAndFindDiff(
              mainChild,
              minorChild,
              mainChild.additional || isMainAdded,
              minorChild.additional || isMinorAdded,
              level + 1
            );
          }
        });
      }
      // Пробегаемся по подузлам смежного элемента дерева
      if (minor.children && minor.children.length) {
        minor.children.forEach((minorChild, index) => {
          if (!minorChild.additional && !minorChild.diffState) {
            minorChild.id = `${minor.id}.${index}`;

            let mainChild = main.children.find(
              item =>
                item.description === minorChild.description &&
                item.timeDelaySec === minorChild.timeDelaySec &&
                item.itemType === minorChild.itemType &&
                !item.additional &&
                !item.diffState
            );

            if (mainChild) {
              mainChild.id = `${main.id}.${index}`;

              if (mainChild.actionName !== minorChild.actionName) {
                mainChild.diffState = DIFF_STATES.UPDATED;
                minorChild.diffState = DIFF_STATES.UPDATED;
                main.diffState = DIFF_STATES.VARIED;
                minor.diffState = DIFF_STATES.VARIED;
                this.isEqual = false;
              } else if (mainChild.description !== minorChild.description) {
                mainChild.diffState = DIFF_STATES.UPDATED;
                minorChild.diffState = DIFF_STATES.UPDATED;
                main.diffState = DIFF_STATES.VARIED;
                minor.diffState = DIFF_STATES.VARIED;
                this.isEqual = false;
              }
            } else {
              this.isEqual = false;
              mainChild = createJsonReplica(minorChild);
              minorChild.additional = true;
              main.children.push(mainChild);
            }
            // Добавляем функции рендеринга
            this.addRenderFunctions(mainChild, minorChild);
            // Проверяем изменения в подузлах
            this.mapTreeAndFindDiff(
              minorChild,
              mainChild,
              minorChild.additional || isMinorAdded,
              mainChild.additional || isMainAdded,
              level + 1
            );
          }
        });
      }
    }
  };

  renderTimeCell = (text, record) => {
    switch (record.itemType) {
      case 'ROOT': {
        return <Icon type={record.actionIconName} style={{ color: record.actionIconColor }} />;
      }
      case 'TIME': {
        return (
          <span>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px', fontSize: '14px' }}
            />
            {record.hiddenTime
              ? ''
              : moment
                  .utc(moment.duration(record.timeDelaySec, 's').asMilliseconds())
                  .format('HH:mm:ss')}
          </span>
        );
      }

      default: {
        return null;
      }
    }
  };

  renderActionCell = (text, record) => {
    switch (record.itemType) {
      case 'TIME':
      case 'ROOT': {
        return null;
      }
      case 'TRACE':
      case 'NO_TRACE':
      case 'CONDITION':
      case 'IN': {
        return (
          <span>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px', fontSize: '14px' }}
            />
            {record.actionName ? `${record.actionName} ` : 'Без имени'}
          </span>
        );
      }

      default: {
        return (
          <span style={{ marginLeft: '16px' }}>
            <Icon
              type={record.actionIconName}
              style={{ color: record.actionIconColor, marginRight: '4px' }}
            />
            {record.actionName}
          </span>
        );
      }
    }
  };

  setAdditionalStateForChildren = item => {
    item.additional = true;
    if (item.children && item.children.length) {
      item.children.forEach(child => {
        this.setAdditionalStateForChildren(child);
      });
    }
  };

  prepareParamsToDiff = () => {
    this.mappedProjectParams = this.mapParams(this.fromProject);
    this.mappedDeviceParams = this.mapParams(this.fromDevice);
  };

  mapParams = list => {
    const obj = {};
    list.forEach(item => {
      obj[item.globalNo] = {};
      Object.keys(item.basicParams).forEach(fieldName => {
        if (this.availableFields[fieldName])
          obj[item.globalNo][fieldName] = item.basicParams[fieldName];
      });
      Object.keys(item.advancedParams).forEach(fieldName => {
        if (this.availableFields[fieldName])
          obj[item.globalNo][fieldName] = item.advancedParams[fieldName];
      });
    });
    return obj;
  };

  findParamsDiff = () => {
    this.paramsDiff = this.map(this.mappedProjectParams, this.mappedDeviceParams);
  };

  getFieldView = (value, fieldName, diffState) => {
    return {
      name: this.fieldNames[fieldName] || fieldName,
      value,
      diffState,
      id: fieldName
    };
  };

  getValueFromMappedItem = (mappedItem, fieldName) => {
    switch (fieldName) {
      case 'scenarioPurpose': {
        return this.scenarioConstants.dictionaries.purposes.find(
          item => item.id === mappedItem[fieldName]
        ).name;
      }
      case 'scenarioType': {
        return this.scenarioConstants.dictionaries.types.find(
          item => item.id === mappedItem[fieldName]
        ).name;
      }
      case 'stopType': {
        return this.scenarioConstants.dictionaries.stopTypes.find(
          item => item.id === mappedItem[fieldName]
        ).name;
      }
      default: {
        return mappedItem[fieldName];
      }
    }
  };

  prepareParamsToView = () => {
    this.fromProjectView = [];
    this.fromDeviceView = [];

    Object.keys(this.paramsDiff).forEach(itemIndex => {
      let projectItem = this.fromProject.find(entity => `${entity.globalNo}` === `${itemIndex}`);
      let deviceItem = this.fromDevice.find(entity => `${entity.globalNo}` === `${itemIndex}`);

      const mappedProjectItem = this.mappedProjectParams[itemIndex];
      const mappedDeviceItem = this.mappedDeviceParams[itemIndex];
      // Если не была найден смежная сущность в соседнем дереве
      if (!projectItem && deviceItem) projectItem = { ...deviceItem };
      else if (!deviceItem && projectItem) deviceItem = { ...projectItem };

      if (!projectItem.children) projectItem['children'] = [];
      if (!deviceItem.children) deviceItem['children'] = [];

      if (this.paramsDiff[itemIndex].type && this.paramsDiff[itemIndex].data) {
        switch (this.paramsDiff[itemIndex].type) {
          case 'CREATED':
          case 'DELETED': {
            this.isEqual = false;
            const diffState = DIFF_STATES[this.paramsDiff[itemIndex].type];
            const inverseDiffState = diffState.inverseDiffState
              ? DIFF_STATES[diffState.inverseDiffState]
              : diffState;
            // Назначаем цвет
            projectItem.diffState = diffState;
            deviceItem.diffState = inverseDiffState;
            if (mappedProjectItem) {
              Object.keys(mappedProjectItem).forEach(fieldName => {
                const value = this.getValueFromMappedItem(mappedProjectItem);
                const fieldView = this.getFieldView(value, fieldName, diffState);
                projectItem.children.push(fieldView);
                if (!mappedDeviceItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, inverseDiffState);
                  deviceItem.children.push(inverseFieldView);
                }
              });
            }

            if (mappedDeviceItem) {
              Object.keys(mappedDeviceItem).forEach(fieldName => {
                const value = this.getValueFromMappedItem(mappedDeviceItem);
                const fieldView = this.getFieldView(value, fieldName, inverseDiffState);
                deviceItem.children.push(fieldView);
                if (!mappedProjectItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, diffState);
                  projectItem.children.push(inverseFieldView);
                }
              });
            }
            break;
          }
          default: {
            break;
          }
        }
      } else {
        const diff = this.paramsDiff[itemIndex];

        Object.keys(diff).forEach(fieldName => {
          const fieldDiff = diff[fieldName];

          switch (fieldDiff.type) {
            case 'UPDATED':
            case 'UNCHANGED': {
              const diffState = DIFF_STATES[fieldDiff.type];
              let projectValue = '';
              let deviceValue = '';
              switch (fieldName) {
                case 'scenarioPurpose': {
                  projectValue = this.scenarioConstants.dictionaries.purposes.find(
                    item => item.id === mappedProjectItem[fieldName]
                  ).name;
                  deviceValue = this.scenarioConstants.dictionaries.purposes.find(
                    item => item.id === mappedDeviceItem[fieldName]
                  ).name;
                  break;
                }
                case 'scenarioType': {
                  projectValue = this.scenarioConstants.dictionaries.types.find(
                    item => item.id === mappedProjectItem[fieldName]
                  ).name;
                  deviceValue = this.scenarioConstants.dictionaries.types.find(
                    item => item.id === mappedDeviceItem[fieldName]
                  ).name;
                  break;
                }
                case 'stopType': {
                  projectValue = this.scenarioConstants.dictionaries.stopTypes.find(
                    item => item.id === mappedProjectItem[fieldName]
                  ).name;
                  deviceValue = this.scenarioConstants.dictionaries.stopTypes.find(
                    item => item.id === mappedDeviceItem[fieldName]
                  ).name;
                  break;
                }
                default: {
                  projectValue = mappedProjectItem[fieldName];
                  deviceValue = mappedDeviceItem[fieldName];
                  break;
                }
              }

              if (fieldDiff.type === 'UPDATED') {
                this.isEqual = false;
                projectItem.diffState = DIFF_STATES['VARIED'];
                deviceItem.diffState = DIFF_STATES['VARIED'];
              }
              projectItem.children.push(this.getFieldView(projectValue, fieldName, diffState));
              deviceItem.children.push(this.getFieldView(deviceValue, fieldName, diffState));
              break;
            }
            case 'DELETED':
            case 'CREATED': {
              this.isEqual = false;
              const diffState = DIFF_STATES[fieldDiff.type];
              const inverseDiffState = diffState.inverseDiffState
                ? DIFF_STATES[diffState.inverseDiffState]
                : diffState;

              if (mappedProjectItem[fieldName]) {
                const value = mappedProjectItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedDeviceItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              if (mappedDeviceItem[fieldName]) {
                const value = mappedDeviceItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedProjectItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              break;
            }
            default:
              break;
          }
        });
      }
      this.fromProjectView.push(projectItem);
      this.fromDeviceView.push(deviceItem);
    });
  };
}

export class VirtualStatesDiff extends Diff {
  constructor({ fromProject, fromDevice, currentDevice, recoveredDevice, stateCategoryViewsHash }) {
    super();
    this.isEqual = true;
    this.fromProject = fromProject.map(item => ({ ...item, state: { expanded: true } }));
    this.fromDevice = fromDevice.map(item => ({ ...item, state: { expanded: true } }));
    this.currentDevice = currentDevice;
    this.recoveredDevice = recoveredDevice;
    this.stateCategoryViewsHash = stateCategoryViewsHash;

    this.availableFields = {
      name: true,
      stateCategoryId: true,
      description: true,
      messageOff: true,
      messageOn: true
    };
    this.fieldNames = {
      name: 'Название',
      description: 'Примечание',
      device: 'Прибор',
      stateCategoryId: 'Класс состояния',
      messageOn: 'Сообщение при установке',
      messageOff: 'Сообщение при снятии'
    };
  }

  get = () => {
    this.prepareToDiff();
    this.findDiff();
    this.prepareToView();
    return {
      fromProject: this.fromProjectView,
      fromDevice: this.fromDeviceView,
      isEqual: this.isEqual
    };
  };

  prepareToDiff = () => {
    this.mappedFromProject = this.getMappedParams(this.fromProject);
    this.mappedFromDevice = this.getMappedParams(this.fromDevice);
  };

  getMappedParams = list => {
    const obj = {};
    list.forEach(item => {
      obj[item.globalNo] = {};
      Object.keys(item).forEach(fieldName => {
        if (this.availableFields[fieldName]) {
          obj[item.globalNo][fieldName] = item[fieldName];
        }
      });
    });
    return obj;
  };

  findDiff = () => {
    this.diff = this.map(this.mappedFromProject, this.mappedFromDevice);
  };

  getFieldView = (value, fieldName, diffState) => {
    return {
      name: this.fieldNames[fieldName] || fieldName,
      value,
      diffState,
      id: fieldName
    };
  };

  // Главная функция подготовки данных к отображению
  prepareToView = () => {
    this.fromProjectView = [];
    this.fromDeviceView = [];

    Object.keys(this.diff).forEach(itemIndex => {
      // Поиск смежных сущностей
      let projectItem = this.fromProject.find(entity => `${entity.globalNo}` === `${itemIndex}`);
      let deviceItem = this.fromDevice.find(entity => `${entity.globalNo}` === `${itemIndex}`);
      const mappedProjectItem = this.mappedFromProject[itemIndex];
      const mappedDeviceItem = this.mappedFromDevice[itemIndex];
      // Если не была найден смежная сущность в соседнем дереве
      if (!projectItem && deviceItem) projectItem = { ...deviceItem };
      else if (!deviceItem && projectItem) deviceItem = { ...projectItem };

      if (!projectItem.children) projectItem['children'] = [];
      if (!deviceItem.children) deviceItem['children'] = [];
      // Если элемент на входе уже имеет разницу (удалено/добавлено)
      if (this.diff[itemIndex].type && this.diff[itemIndex].data) {
        switch (this.diff[itemIndex].type) {
          case 'CREATED':
          case 'DELETED': {
            this.isEqual = false;
            const diffState = DIFF_STATES[this.diff[itemIndex].type];
            const inverseDiffState = diffState.inverseDiffState
              ? DIFF_STATES[diffState.inverseDiffState]
              : diffState;
            // Назначаем цвет
            projectItem.diffState = diffState;
            deviceItem.diffState = inverseDiffState;
            if (mappedProjectItem) {
              const controlDeviceField = this.getFieldView(
                this.currentDevice.name,
                'device',
                diffState
              );
              controlDeviceField.cellValueRender = () => (
                <DeviceTypeTreeItem
                  record={{
                    ...this.currentDevice,
                    name: `${this.currentDevice.name} (${this.currentDevice.shortAddressPath})`
                  }}
                />
              );
              projectItem.children.push(controlDeviceField);
              if (!mappedDeviceItem) {
                deviceItem.children.push({ ...controlDeviceField, diffState: inverseDiffState });
              }

              Object.keys(mappedProjectItem).forEach(fieldName => {
                const value =
                  fieldName === 'stateCategoryId'
                    ? this.stateCategoryViewsHash[mappedProjectItem[fieldName]].name
                    : mappedProjectItem[fieldName];
                const fieldView = this.getFieldView(value, fieldName, diffState);
                projectItem.children.push(fieldView);
                if (!mappedDeviceItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, inverseDiffState);
                  deviceItem.children.push(inverseFieldView);
                }
              });
            }

            if (mappedDeviceItem) {
              const controlDeviceField = this.getFieldView(
                this.recoveredDevice.name,
                'device',
                inverseDiffState
              );
              controlDeviceField.cellValueRender = () => (
                <DeviceTypeTreeItem
                  record={{
                    ...this.recoveredDevice,
                    name: `${this.recoveredDevice.name} (${this.recoveredDevice.shortAddressPath})`
                  }}
                />
              );
              deviceItem.children.push(controlDeviceField);
              if (!mappedProjectItem) {
                projectItem.children.push({ ...controlDeviceField, diffState });
              }

              Object.keys(mappedDeviceItem).forEach(fieldName => {
                const value =
                  fieldName === 'stateCategoryId'
                    ? this.stateCategoryViewsHash[mappedDeviceItem[fieldName]].name
                    : mappedDeviceItem[fieldName];
                const fieldView = this.getFieldView(value, fieldName, inverseDiffState);
                deviceItem.children.push(fieldView);
                if (!mappedProjectItem) {
                  const inverseFieldView = this.getFieldView(value, fieldName, diffState);
                  projectItem.children.push(inverseFieldView);
                }
              });
            }
            break;
          }
          default: {
            break;
          }
        }
      }
      // Если есть изменения в полях
      else {
        const diff = this.diff[itemIndex];
        /**
         * Добавляем поле с прибором
         */
        const controlDeviceDiff =
          this.currentDevice.shortAddressPath !== this.recoveredDevice.shortAddressPath
            ? DIFF_STATES.UPDATED
            : DIFF_STATES.UNCHANGED;
        const currentDeviceField = this.getFieldView(
          this.currentDevice.name,
          'device',
          controlDeviceDiff
        );
        const recoveredDeviceField = this.getFieldView(
          this.recoveredDevice.name,
          'device',
          controlDeviceDiff
        );

        if (controlDeviceDiff.id === 'UPDATED') {
          this.isEqual = false;
          projectItem.diffState = DIFF_STATES['VARIED'];
          deviceItem.diffState = DIFF_STATES['VARIED'];
        }

        currentDeviceField.cellValueRender = () => (
          <DeviceTypeTreeItem
            record={{
              ...this.currentDevice,
              name: `${this.currentDevice.name} (${this.currentDevice.shortAddressPath})`
            }}
          />
        );
        recoveredDeviceField.cellValueRender = () => (
          <DeviceTypeTreeItem
            record={{
              ...this.currentDevice,
              name: `${this.currentDevice.name} (${this.currentDevice.shortAddressPath})`
            }}
          />
        );
        projectItem.children.push(currentDeviceField);
        deviceItem.children.push(recoveredDeviceField);
        /**
         * Добавляем отображение остальных полей
         */
        Object.keys(diff).forEach(fieldName => {
          const fieldDiff = diff[fieldName];

          switch (fieldDiff.type) {
            case 'UPDATED':
            case 'UNCHANGED': {
              const diffState = DIFF_STATES[fieldDiff.type];
              const projectValue =
                fieldName === 'stateCategoryId'
                  ? this.stateCategoryViewsHash[mappedProjectItem[fieldName]].name
                  : mappedProjectItem[fieldName];
              const deviceValue =
                fieldName === 'stateCategoryId'
                  ? this.stateCategoryViewsHash[mappedDeviceItem[fieldName]].name
                  : mappedDeviceItem[fieldName];

              if (fieldDiff.type === 'UPDATED') {
                this.isEqual = false;
                projectItem.diffState = DIFF_STATES['VARIED'];
                deviceItem.diffState = DIFF_STATES['VARIED'];
              }
              projectItem.children.push(this.getFieldView(projectValue, fieldName, diffState));
              deviceItem.children.push(this.getFieldView(deviceValue, fieldName, diffState));
              break;
            }
            case 'DELETED':
            case 'CREATED': {
              this.isEqual = false;
              const diffState = DIFF_STATES[fieldDiff.type];
              const inverseDiffState = diffState.inverseDiffState
                ? DIFF_STATES[diffState.inverseDiffState]
                : diffState;

              if (mappedProjectItem[fieldName]) {
                const value =
                  fieldName === 'stateCategoryId'
                    ? this.stateCategoryViewsHash[mappedProjectItem[fieldName]].name
                    : mappedProjectItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedDeviceItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              if (mappedDeviceItem[fieldName]) {
                const value =
                  fieldName === 'stateCategoryId'
                    ? this.stateCategoryViewsHash[mappedDeviceItem[fieldName]].name
                    : mappedDeviceItem[fieldName];
                projectItem.children.push(this.getFieldView(value, fieldName, diffState));
                if (!mappedProjectItem[fieldName])
                  deviceItem.children.push(this.getFieldView(value, fieldName, inverseDiffState));
              }

              break;
            }
            default:
              break;
          }
        });
      }

      this.fromProjectView.push(projectItem);
      this.fromDeviceView.push(deviceItem);
    });
  };
}
