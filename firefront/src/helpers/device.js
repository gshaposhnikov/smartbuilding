import { createSelector } from 'reselect';
import { isEmpty } from 'lodash';

import { getCurrentProjectDeviceList } from './currentProject';

const MAX_LINE_NO = 250;
const VIRTUAL_CONTAINER_LINE_NO = 256;

export function getDeviceStatus(fireStatus, deviceStatus) {
  if (fireStatus === 'normal' && deviceStatus === 'normal') {
    return 'normal';
  } else if (fireStatus === 'normal' || fireStatus === 'nd') {
    return deviceStatus;
  } else {
    return fireStatus;
  }
}

/**
 * Отфильтровать спсок устройств, оставив родителей для дальнейшего формирования частичного дерева устройств
 * @param {Array} devices список устройств
 * @param {Function} filterCallback callback-функция для фильтрацииустройств
 * @returns {Array} отфильтрованный список устройств и их родителей
 */
export function filterDevicesLeavingParents(devices, filterCallback) {
  const filteredDevices = devices.filter(filterCallback);
  if (filteredDevices) {
    let paths = [],
      filteredDevicesWithParents = [];
    // Получаем пути устройств в текущей зоне
    filteredDevices.forEach(device => {
      if (device.addressPath) {
        const addressPath = device.addressPath.split(',');
        let path = '';
        for (let i = 0; i + 1 < addressPath.length; i += 2) {
          if (i > 0) path += ',' + addressPath[i];
          path += ',' + addressPath[i + 1];
          if (path !== device.AddressPath) paths.push(path);
        }
      }
    });
    // Получаем устройства зоны из полного дерева
    devices.forEach(device => {
      if (paths.includes(device.addressPath)) filteredDevicesWithParents.push(device);
    });
    return filteredDevicesWithParents;
  } else {
    return [];
  }
}

/**
 * Сворачивание списка устройств в дерево устройств
 *
 * @export
 * @param {Object[]} listDevices список устройств
 * @param {Function} [callback=item => item] callback для модификации устройств
 * @returns {Object[]} Массив деревьев устройств, для случая, если root устройств больше, чем одного.
 */
export function getDevicesTree(listDevices, callback = item => item) {
  const tree = {};
  let rootDevices = [];
  if (listDevices.length === 0) {
    return [];
  } else {
    for (let i = 0; i < listDevices.length; ++i) {
      const device = { ...listDevices[i] },
        parentId = device.parentDeviceId,
        virtualContainerId = device.virtualContainerId;
      device.key = device.id;

      if (tree[device.key]) {
        Object.assign(tree[device.key], device);
      } else tree[device.key] = device;

      if (!parentId) {
        rootDevices.push(callback(tree[device.key]));
      } else {
        if (tree[parentId] && !tree[parentId].children) {
          tree[parentId].children = [];
        }
        if (!tree[parentId]) {
          tree[parentId] = { children: [] };
        }
        tree[parentId].children.push(callback(tree[device.key]));

        if (virtualContainerId) {
          if (tree[virtualContainerId] && !tree[virtualContainerId].children) {
            tree[virtualContainerId].children = [];
          }
          if (!tree[virtualContainerId]) {
            tree[virtualContainerId] = { children: [] };
          }
          tree[virtualContainerId].children.push(callback(tree[device.key]));
        }
      }
    }
    sortDeviceTree(rootDevices);
    return rootDevices;
  }
}

/**
 * Возвращает развернутое дерево
 * @param {Array} devices
 * @return {Array}
 */
export function getFlatTree(tree, list = [], parentIds = []) {
  const deviceList = list;
  tree.forEach(node => {
    const treeNode = { ...node };
    deviceList.push(treeNode);
    treeNode['parentIds'] = parentIds;
    if (node.children) {
      getFlatTree(node.children, deviceList, [...parentIds, node.id]);
    }
  });
  return deviceList;
}

/**
 * Строит поддерево устройств,
 * Нужно для новых устройств
 * @param {Array} devices
 */
function getDeviceSubtree(devices) {
  if (!devices.length) return [];
  const temp = {};
  const subtree = [];
  devices.forEach(device => {
    const parentId = device.parentDeviceId;
    if (!temp[parentId]) {
      devices.find(item => {
        if (item.id === parentId) {
          const newDevice = { ...item, children: [] };
          temp[item.id] = newDevice;
          subtree.push(newDevice);
          return true;
        }
        return false;
      });
      if (temp[parentId]) {
        temp[parentId].children = [...temp[parentId].children, { ...device }];
      } else {
        const newDevice = { ...device, children: [] };
        temp[device.id] = newDevice;
        subtree.push(newDevice);
      }
    } else {
      temp[parentId].children = [...temp[parentId].children, { ...device }];
    }
  });
  return subtree;
}

/**
 * Сортировка дерева устройств по адресному пути
 * @param {Array} rootDevices Дерево устройств
 * @param {Boolean} recursive Рекурсивная сортировка
 */
export function sortDeviceTree(rootDevices, recursive = true) {
  rootDevices.sort((dev1, dev2) => {
    const dev1AddressNumbers = dev1.addressPath.split(',');
    virtualContainerLining(dev1, dev1AddressNumbers);
    const dev2AddressNumbers = dev2.addressPath.split(',');
    virtualContainerLining(dev2, dev2AddressNumbers);
    if (dev1AddressNumbers.length !== dev2AddressNumbers.length)
      return dev1AddressNumbers.length - dev2AddressNumbers.length;

    if (dev1AddressNumbers.length === 0) return 0;

    const dev1Address = dev1AddressNumbers[dev1AddressNumbers.length - 1];
    const dev2Address = dev2AddressNumbers[dev2AddressNumbers.length - 1];
    if (dev1AddressNumbers.length === 1) return dev1Address - dev2Address;

    var dev1LineNo = dev1AddressNumbers[dev1AddressNumbers.length - 2];
    var dev2LineNo = dev2AddressNumbers[dev2AddressNumbers.length - 2];

    if (dev1LineNo === dev2LineNo) return dev1Address - dev2Address;

    if (dev1LineNo >= MAX_LINE_NO) dev1LineNo -= 2 * MAX_LINE_NO;
    if (dev2LineNo >= MAX_LINE_NO) dev2LineNo -= 2 * MAX_LINE_NO;

    return dev1LineNo - dev2LineNo;
  });

  if (recursive) {
    rootDevices.forEach(rootDevice => {
      if (rootDevice.children && rootDevice.children.length && rootDevice.children[0].addressPath)
        sortDeviceTree(rootDevice.children);
    });
  }
}

function virtualContainerLining(device, devAddressNumbers) {
  if (device.deviceCategory === 'VIRTUAL_CONTAINER') {
    devAddressNumbers[devAddressNumbers.length - 2] = VIRTUAL_CONTAINER_LINE_NO;
  }
}

export function getDevicesByIds(devices, idsList) {
  let devicesMap = {};

  devices.forEach(device => {
    device.key = device.id;
    devicesMap = {
      ...devicesMap,
      [device.id]: device
    };
  });

  return idsList.map(id => {
    return devicesMap[id];
  });
}

/**
 * Фильтрация списка устройств по определенной зоне
 *
 * @param {Object[]} devices
 * @param {string} regionId
 * @returns {Object[]} устройства, привязанные к определенной зоне
 */
export function getDevicesByRegionId(devices, regionId) {
  return devices.filter(device => {
    return device.regionId === regionId;
  });
}

/**
 * Фильтрация списка устройств по определенной СКУД-зоне,
 * c добавлением поля isActive: false и подсказки вход/выход
 *
 * @param {Object[]} devices
 * @param {Object} deviceProfileViewsHash
 * @param {string} regionId
 * @returns {Object[]} устройства, привязанные к определенной зоне
 */
export function getInactiveDevicesBySkudRegionId(devices, deviceProfileViewsHash, regionId) {
  const skudRegionDevices = [];
  if (deviceProfileViewsHash) {
    devices.forEach(device => {
      const deviceProfileView = deviceProfileViewsHash[device.deviceProfileId];
      if (deviceProfileView && deviceProfileView.deviceProfile.accessPoint) {
        const regionToIdProp = device.aggregatedPropertyViews.find(
          property => property.id === 'RegionToId'
        );
        const regionFromIdProp = device.aggregatedPropertyViews.find(
          property => property.id === 'RegionFromId'
        );
        if (regionToIdProp && regionToIdProp.projectValue === regionId) {
          skudRegionDevices.push({
            ...device,
            isActive: false,
            description: `${device.description} (вход)`
          });
        } else if (regionFromIdProp && regionFromIdProp.projectValue === regionId) {
          skudRegionDevices.push({
            ...device,
            isActive: false,
            description: `${device.description} (выход)`
          });
        }
      }
    });
  }
  return skudRegionDevices;
}

/**
 * Получение списка устройств, доступные для добавления к зонам:
 *
 * Если есть активная зона, возвращаем дерево устройств,
 * с добавленным каждому устройству флагом isActive, который показывает
 * можно ли подключить устройство к зоне
 *
 * @export
 * @param {Object[]} devices
 * @param {Object} region
 * @returns {Object[]} устройства, доступные для добавления
 */
export function getRegionsAttachableDevicesTree(devices, region) {
  if (!isEmpty(region)) {
    return getDevicesTree(devices, item => {
      item.isActive =
        item.attachableToRegion &&
        !item.disabled &&
        item.regionId === null &&
        region.subsystem === item.subsystem;
      return item;
    });
  } else {
    return [];
  }
}

/**
 * Получение списка устройств, доступные для добавления к зонам:
 *
 * Если есть активная зона, возвращаем дерево устройств,
 * с добавленным каждому устройству флагом isActive, который показывает
 * можно ли подключить устройство к зоне
 *
 * @export
 * @param {Object[]} devices
 * @param {Object} region
 * @returns {Object} устройства, доступные для добавления
 */
export function getRegionsAttachableDevices(deviceList, region) {
  if (!isEmpty(region)) {
    const devices = deviceList.map(item => {
      const device = { ...item };
      device.isActive =
        device.attachableToRegion &&
        !device.disabled &&
        device.regionId === null &&
        region.subsystem === device.subsystem;
      return device;
    });
    return { list: devices, tree: getDevicesTree(devices) };
  } else {
    return { list: [], tree: [] };
  }
}

/**
 * Кореневые устройства дерева
 */
export class DeviceTreeRoot {
  constructor() {
    this.id = 'computer';
    this.key = 'computer';
    this.name = 'Компьютер';
    this.iconMedia = { path: 'computer.ico' };
    this.editableAddress = false;
    this.isRoot = true;
    this.state = {
      expanded: true
    };
    this.contextMenuDisabled = true; // Отключение контекстного меню
    this.children = [
      {
        id: 'usb',
        key: 'usb',
        name: 'USB',
        iconMedia: { path: 'usb.ico' },
        outputConnectionType: 'USB',
        children: [],
        isRoot: true,
        editableAddress: false,
        state: {
          expanded: true
        }
      },
      {
        id: 'network',
        key: 'network',
        name: 'Ethernet',
        iconMedia: { path: 'network.ico' },
        outputConnectionType: 'ETHERNET',
        children: [],
        isRoot: true,
        editableAddress: false,
        state: {
          expanded: true
        }
      }
    ];
  }
}

/**
 * Получить начало USB-ветки
 * @param {Array} deviceTree Дерево устройств
 */
export function getUSBRoot(deviceTree) {
  return deviceTree[0].children[0];
}

/**
 * Получить все устройства с указанной категорией.
 * @param {object} store
 * @param {string} deviceCategory
 * @param {object} devices
 */
export function findDevicesByDeviceCategory(store, deviceCategory, devices) {
  if (!devices) {
    devices = getCurrentProjectDeviceList(store);
  }
  if (devices) {
    return devices.filter(device => device.deviceCategory === deviceCategory);
  } else {
    return [];
  }
}

/**
 * Получить все вкл./выкл. устройства
 * @param {array} devices
 * @param {bool} state
 **/
export const getDevicesByPollingState = createSelector(
  (devices = [], state) => devices.filter(device => device.statePolling === state),
  devices => devices
);

/**
 * Получить профиль устройства
 * @param {object} profiles
 * @param {object} device
 **/
export function getDeviceProfile(profilesHash, device) {
  const profileView =
    device && device.id && profilesHash ? profilesHash[device.deviceProfileId] : null;
  return profileView ? profileView.deviceProfile : null;
}

/**
 * Получить список устройств, у которых есть наблюдаемые параметры
 * @param {array} devices
 * @param {object} deviceProfileViewsHash
 */
export function getMonitorableDevices(devices, deviceProfileViewsHash) {
  const monitorableDevices = devices.filter(
    device =>
      deviceProfileViewsHash[device.deviceProfileId] &&
      deviceProfileViewsHash[device.deviceProfileId].deviceProfile.customMonitorableValues.length
  );
  return monitorableDevices;
}

/**
 * Получить список наблюдаемых параметров
 * @param {array} devices - Отсортированный список устройств по наличию наблюдаемых параметров
 * @param {object} deviceProfileViewsHash
 */
export function getMonitorableValues(devices, deviceProfileViewsHash) {
  const monitorableValues = {};
  devices.forEach(device => {
    deviceProfileViewsHash[device.deviceProfileId].deviceProfile.customMonitorableValues.forEach(
      value => {
        if (!monitorableValues[value.profile.id]) monitorableValues[value.profile.id] = 1;
      }
    );
  });
  return Object.keys(monitorableValues);
}

/**
 * Создает полное дерево устройств
 * @param {Array} devices - Список устройств
 * @return {Array} - Дерево устройств
 */
export function createDeviceTree(devices) {
  const deviceTree = [{ ...new DeviceTreeRoot() }];
  getUSBRoot(deviceTree).children = getDevicesTree(devices);
  return deviceTree;
}

/**
 * Функция добавления устройств в дерево
 * Обновляет дерево НА МЕСТЕ!
 * @param {Array} deviceTree - Дерево устройств
 * @param {Array} devices - Обновленное(-ые) устройство(-а)
 */
export function addDeviceTreeItems(deviceTree, devices) {
  if (devices.length) {
    if (devices.length > 1) {
      let subTree = getDeviceSubtree(devices);
      if (subTree.length === 0) subTree = getDevicesTree(devices);
      for (let device of subTree) addDeviceTreeItem(deviceTree, device);
    } else {
      addDeviceTreeItem(deviceTree, devices[0]);
    }
  }
}
function addDeviceTreeItem(deviceTree, device) {
  const parent = getDeviceTreeItem(
    getUSBRoot(deviceTree),
    device.addressPath
      .split(',')
      .slice(0, -1)
      .join(','),
    device.parentDeviceId
  );
  if (!parent.children) parent.children = [];
  parent.children = [...parent.children, device];
  sortDeviceTree(parent.children);
}

/**
 * Функция обновления нескольких элементов дерева устройств
 * Обновляет дерево НА МЕСТЕ!
 * @param {Array} deviceTree - Дерево устройств
 * @param {Array} devices - Обновленные устройства
 */
export function updateDeviceTreeItems(deviceTree, devices) {
  if (devices && devices.length) {
    const parentsByParentId = {};
    const parentsByDeviceId = {};
    devices.forEach(updatedDevice => {
      const parentId = updatedDevice.parentDeviceId;
      if (!parentId || !parentsByParentId[parentId]) {
        const parent = getDeviceTreeItem(
          getUSBRoot(deviceTree),
          updatedDevice.addressPath
            .split(',')
            .slice(0, -1)
            .join(','),
          updatedDevice.parentDeviceId
        );
        if (parentId) parentsByParentId[parentId] = parent;
        else parentsByDeviceId[updatedDevice.id] = parent;
      }
      if (parentId) {
        parentsByParentId[parentId].children = parentsByParentId[parentId].children.map(device => {
          if (device.id === updatedDevice.id) {
            updatedDevice = { ...updatedDevice, children: device.children };
            // Если устройство находится(-лось) в вирт. контейнере
            if (updatedDevice.virtualContainerId || device.virtualContainerId) {
              let virtualContainerParent;
              if (parent.addressLevel === 2) {
                virtualContainerParent = parent;
              } else {
                virtualContainerParent = getDeviceTreeItem(
                  getUSBRoot(deviceTree),
                  updatedDevice.addressPath
                    .split(',')
                    .slice(0, 1 - updatedDevice.addressLevel)
                    .join(',')
                );
              }
              // Находим вирт. контейнер
              const virtualContainer = virtualContainerParent.children.find(
                item =>
                  item.id === device.virtualContainerId ||
                  item.id === updatedDevice.virtualContainerId
              );
              if (virtualContainer) {
                // Если устройство до сих пор находится в вирт. контейнере
                if (device.virtualContainerId && updatedDevice.virtualContainerId) {
                  virtualContainer.children = virtualContainer.children.map(item =>
                    item.id === updatedDevice.id ? updatedDevice : item
                  );
                }
                // Если устройство удалено из вирт. контейнера
                else if (device.virtualContainerId && !updatedDevice.virtualContainerId) {
                  virtualContainer.children = virtualContainer.children.filter(
                    item => item.id !== updatedDevice.id
                  );
                }
                // Если устройство добавлено в вирт. контейнер
                else if (!device.virtualContainerId && updatedDevice.virtualContainerId) {
                  if (!virtualContainer.children) virtualContainer.children = [];
                  virtualContainer.children = [...virtualContainer.children, updatedDevice];
                }
              }
            }
            return updatedDevice;
          }
          return device;
        });
      } else {
        parentsByDeviceId[updatedDevice.id].children = parentsByDeviceId[
          updatedDevice.id
        ].children.map(device => {
          return device.id === updatedDevice.id
            ? { ...updatedDevice, children: device.children || [] }
            : device;
        });
      }
    });
    //
    for (let id in parentsByParentId) {
      if (parentsByParentId[id].children) sortDeviceTree(parentsByParentId[id].children, false);
    }
    for (let id in parentsByDeviceId) {
      if (parentsByDeviceId[id].children) sortDeviceTree(parentsByDeviceId[id].children, false);
    }
  }
}

/**
 * Функция обновления одного элемента дерева устройств
 * Обновляет дерево НА МЕСТЕ!
 * @param {Array} deviceTree - Дерево устройств
 * @param {Object} updatedDevice - Обновленное устройство
 */
export function updateOneDeviceTreeItem(deviceTree, updatedDevice) {
  if (updatedDevice) {
    const parent = getDeviceTreeItem(
      getUSBRoot(deviceTree),
      updatedDevice.addressPath
        .split(',')
        .slice(0, -1)
        .join(','),
      updatedDevice.parentDeviceId
    );
    let needResortTree = false;
    parent.children = parent.children.map(device => {
      if (device.id === updatedDevice.id) {
        updatedDevice = { ...updatedDevice, children: device.children };
        if (device.virtualContainerId) {
          let virtualContainerParent;
          if (parent.addressLevel === 2) {
            virtualContainerParent = parent;
          } else {
            virtualContainerParent = getDeviceTreeItem(
              getUSBRoot(deviceTree),
              updatedDevice.addressPath
                .split(',')
                .slice(0, 1 - updatedDevice.addressLevel)
                .join(',')
            );
          }

          const virtualContainer = virtualContainerParent.children.find(
            item => item.id === device.virtualContainerId
          );
          if (virtualContainer) {
            virtualContainer.children = virtualContainer.children.map(item =>
              item.id === updatedDevice.id ? updatedDevice : item
            );
          }
        }
        if (updatedDevice.addressPath !== device.addressPath) {
          needResortTree = true;
        }
        return updatedDevice;
      }
      return device;
    });
    if (needResortTree) {
      sortDeviceTree(parent.children, false);
    }
  }
}

/**
 * Удаление элемента дерева
 * Обновляет дерево НА МЕСТЕ!
 * @param {Array} deviceTree
 * @param {Object} removedDevice
 *
 */
export function deleteDeviceTreeItem(deviceTree, removedDevice) {
  const parent = getDeviceTreeItem(
    getUSBRoot(deviceTree),
    removedDevice.addressPath
      .split(',')
      .slice(0, -1)
      .join(','),
    removedDevice.parentDeviceId
  );
  parent.children = parent.children.filter(device => device.id !== removedDevice.id);
}

/**
 * Поиск элемента дерева устройств
 * @param {Array} deviceTree - Дерево устройств (адресных устройств)
 * @param {String} addressPath - адрес устройства
 * @param {String} parentId - id родительского устройства
 * @return {Object} - элемент дерева
 */
function getDeviceTreeItem(deviceTree, addressPath, parentId) {
  if (!addressPath || !deviceTree.children) return deviceTree;
  const index = deviceTree.children.findIndex(device => {
    return (
      device.addressPath ===
        addressPath
          .split(',')
          .slice(0, device.addressPath.split(',').length)
          .join(',') || device.id === parentId
    );
  });
  if (index === -1) {
    return deviceTree;
  }
  return addressPath.length
    ? getDeviceTreeItem(deviceTree.children[index], addressPath, parentId)
    : deviceTree.children[index];
}

export function getRegionalDeviceTreeItems(deviceList, regionId) {
  const parentIds = {},
    regionalDevices = {};
  let devices = deviceList.filter(device => {
    if (device.regionId === regionId) {
      const parentId = device.parentDeviceId;
      if (parentId && !regionalDevices[parentId]) parentIds[parentId] = true;
      regionalDevices[device.id] = device;
      return false;
    } else if (parentIds[device.id]) {
      const parentId = device.parentDeviceId;
      if (parentId && !regionalDevices[parentId]) parentIds[parentId] = true;
      regionalDevices[device.id] = device;
      delete parentIds[device.id];
      return false;
    }
    return true;
  });

  do {
    devices = devices.filter(device => {
      if (parentIds[device.id]) {
        const parentId = device.parentDeviceId;
        if (parentId && !regionalDevices[parentId]) parentIds[parentId] = true;
        regionalDevices[device.id] = device;
        delete parentIds[device.id];
        return false;
      }
      return true;
    });
  } while (Object.keys(parentIds).length);
  return Object.values(regionalDevices);
}

/**
 * Проверяет устройство на неисправность
 * @param {Object} device
 * @return {Object|undefined}
 */
export function checkMalfunction(device) {
  if (
    device.generalStateCategoryView &&
    (device.generalStateCategoryView.id === 'Malfunction' ||
      device.generalStateCategoryView.id === 'Service')
  ) {
    return device;
  }
}

export function getDeviceIdsGroupedByRegionId(devices = []) {
  const groupedDeviceId = {};
  devices.forEach(device => {
    if (device.regionId) {
      if (!groupedDeviceId[device.regionId]) groupedDeviceId[device.regionId] = [];
      groupedDeviceId[device.regionId].push(device.id);
    }
  });
  return groupedDeviceId;
}
