import { createSelector } from 'reselect';

import SUBSYSTEMS from 'constants/subsystems';

const HASH = 'hash',
  TREE = 'tree',
  GROUPS = 'groups',
  PANELS = 'panels';

/**
 * Получить текущий проект.
 * @param {String} state.currentProjectId
 * @param {Object{}} state.projects
 * @return {Object}
 */
export function getCurrentProject({ currentProjectId, projects }) {
  return currentProjectId ? projects[currentProjectId] || {} : {};
}

/**
 * Проверить, что текущий проект - активен.
 * @param {String} state.currentProjectId
 * @param {Object{}} state.projects
 * @return {bool|null}
 */
export function checkCurrentProjectIsActive({ currentProjectId, projects }) {
  return currentProjectId && projects[currentProjectId]
    ? projects[currentProjectId].status === 'ACTIVE'
    : null;
}

/**
 * Получить список устройств текущего проекта.
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectDeviceList = createSelector(
  getCurrentProjectDevicesHash,
  devicesHash => Object.values(devicesHash)
);

export function getCurrentProjectDevicesHash({ currentProjectId, devices }) {
  if (!devices[currentProjectId]) return {};
  return devices[currentProjectId][HASH];
}

/**
 * Получить дерево устройств текущего проекта.
 * @param {String} state.currentProjectId
 * @param {Object[]} state.devices
 * @return {Array}
 */
export function getCurrentProjectDeviceTree({ currentProjectId, devices }) {
  return currentProjectId ? (devices[currentProjectId] ? devices[currentProjectId][TREE] : []) : [];
}

/**
 * Получить зоны (без отображений) текущего проекта.
 * @param {String} state.currentProjectId
 * @param {Object[]} state.regions
 * @return {Array}
 */
export const getCurrentProjectRegions = createSelector(
  getCurrentProjectRegionsHash,
  regionsHash =>
    Object.values(regionsHash)
      .map(regionView => ({ ...regionView.region }))
      .sort((regionA, regionB) => regionA.index - regionB.index)
);

/**
 * Получить зоны (без отображений) текущего проекта сгруппированные по подсистемам
 * @param {Object} state
 * @return {Object}
 */
export const getCurrentProjectRegionsGroupedBySubsystem = createSelector(
  getCurrentProjectRegions,
  (regions = []) =>
    regions.reduce((hash, region) => {
      if (!hash[region.subsystem]) {
        hash[region.subsystem] = [];
      }
      hash[region.subsystem].push(region);
      return hash;
    }, {})
);

export function getCurrentProjectRegionsHash({ currentProjectId, regions }) {
  if (!currentProjectId || !regions[currentProjectId]) return {};
  return regions[currentProjectId];
}

/**
 * Получить отображения зон текущего проекта.
 * @param {String} state.currentProjectId
 * @param {Object[]} state.regions
 * @return {Array}
 */
export const getCurrentProjectRegionViews = createSelector(
  getCurrentProjectRegionsHash,
  regionsHash => Object.values(regionsHash)
);

/**
 * Получить зоны текущего проекта отфильтрованные по подсистеме
 * @param {Object} state - redux-store
 * @param {String} subsystem - Название подсистемы
 * @return {Array} - список зон
 */
export const getCurrentProjectRegionsBySubsystem = (state, subsystem) => {
  const currentProjectRegions = [...getCurrentProjectRegions(state)].sort(function(a, b) {
    return SUBSYSTEMS[a.subsystem].index - SUBSYSTEMS[b.subsystem].index;
  });
  if (!subsystem || subsystem === 'ALL') return currentProjectRegions;
  return currentProjectRegions.filter(region => region.subsystem === subsystem);
};

/**
 * Получить список помещений текущего проекта
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectPlans = createSelector(
  getCurrentProjectPlansHash,
  devicesHash => Object.values(devicesHash)
);

function getCurrentProjectPlansHash({ currentProjectId, plans }) {
  if (!plans[currentProjectId]) return {};
  return plans[currentProjectId];
}

/**
 * Получить группы планов помещений текущего проекта.
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectPlanGroups = createSelector(
  getCurrentProjectPlanGroupsHash,
  devicesHash => Object.values(devicesHash)
);

function getCurrentProjectPlanGroupsHash({ currentProjectId, planGroups }) {
  if (!planGroups[currentProjectId]) return {};
  return planGroups[currentProjectId];
}

/**
 * Получить сценарии текущего проекта
 * @param {String} state.currentProjectId
 * @param {Array[]} state.scenarios
 * @return {Array}
 */
export const getCurrentProjectScenarios = createSelector(
  getCurrentProjectScenariosHash,
  scenariosHash =>
    Object.values(scenariosHash).sort(
      (prevScenario, nextScenario) => prevScenario.index - nextScenario.index
    )
);

export function getCurrentProjectScenariosHash({ currentProjectId, scenarios }) {
  if (!currentProjectId || !scenarios[currentProjectId]) return {};
  return scenarios[currentProjectId];
}

/**
 * Получить список групп панелей индикаторов
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectIndicatorGroupsList = createSelector(
  getCurrentProjectIndicatorGroupsHash,
  groupsHash => Object.values(groupsHash)
);
/**
 * Получить хеш групп панелей индикаторов
 * @param {String} state
 * @return {Object}
 */
export function getCurrentProjectIndicatorGroupsHash({ currentProjectId, indicators }) {
  if (!indicators[GROUPS][currentProjectId]) return {};
  return indicators[GROUPS][currentProjectId];
}

/**
 * Получить список панелей индикаторов
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectIndicatorPanelsList = createSelector(
  getCurrentProjectIndicatorPanelsHash,
  groupsHash => Object.values(groupsHash)
);
/**
 * Получить хеш панелей индикаторов
 * @param {String} state
 * @return {Object}
 */
export function getCurrentProjectIndicatorPanelsHash({ currentProjectId, indicators }) {
  if (!indicators[PANELS][currentProjectId]) return {};
  return indicators[PANELS][currentProjectId];
}

export const getCurrentProjectIndicatorPanelsInSelectedGroup = createSelector(
  [
    getCurrentProjectIndicatorPanelsHash,
    ({ widgets: { selectedIndicatorGroupId } }) => selectedIndicatorGroupId
  ],
  (indicatorPanels, selectedIndicatorGroupId) =>
    Object.values(indicatorPanels).filter(
      indicatorPanel => indicatorPanel.indicatorGroupId === selectedIndicatorGroupId
    )
);

/**
 * Получить список виртуальных состояний
 * @param {String} state
 * @return {Array}
 */
export const getCurrentProjectVirtualStates = createSelector(
  getCurrentProjectVirtualStateHash,
  groupsHash => Object.values(groupsHash)
);

export function getCurrentProjectVirtualStateHash({ currentProjectId, virtualStates }) {
  if (!virtualStates[currentProjectId]) return {};
  return virtualStates[currentProjectId];
}

/**
 * Получить список ошибок импорта проекта
 * @param {String} state.currentProjectId
 * @param {Array} state.validateMessages
 * @return {Array}
 */
export function getCurrentProjectValidateMessages({ currentProjectId, validateMessages }) {
  return  validateMessages[currentProjectId] ?  validateMessages[currentProjectId] : [];
}
