import { securityOn } from '../../config/app';

/**
 * Получить список прав групп переданного пользователя
 * @param user пользователь
 * @param userGroups список (map<groupId>: <group>) групп пользователей
 * @return список прав групп переданного пользователя
 */
export function getUserGroupPermissions(user, userGroups) {
  let permissions = [];
  if (user && userGroups) {
    for (let groupId of user.userGroupIds) {
      if (userGroups[groupId]) permissions = [...permissions, ...userGroups[groupId].permissionIds];
    }
  }
  return permissions;
}

/**
 * Получить текущего пользователя
 * @param {Object} state
 * @return {Object|null}
 */
export function getCurrentUser({ currentUser }) {
  return currentUser;
}

/**
 * Узнать имеет ли пользователь доступ
 * @param {Object|null} user - пользователь
 * @param {String} role - роль
 * @return {boolean}
 */
export const hasRole = securityOn
  ? (user, role) => {
      if (!securityOn) return true;
      if (user) {
        const allPermissions = [...(user.groupPermissions || []), ...(user.permissionIds || [])];
        return allPermissions.includes(role);
      } else return false;
    }
  : () => true;
