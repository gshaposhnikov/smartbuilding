import moment from 'moment';
import { timeFormat } from 'constants/settings';

export function getDateFormat(date){
  return moment(date).format(timeFormat);
}

export function getDateTimestamp(timestamp){
  return moment(timestamp).format(timeFormat);
}

export function getCurrentTime(){
  return moment().format(timeFormat);
}
