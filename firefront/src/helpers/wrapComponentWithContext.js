import React from 'react';
import {Provider} from 'react-redux';
import ruRU from 'antd/lib/locale-provider/ru_RU';
import { LocaleProvider } from 'antd';
        

export default function wrapComponentWithStore(Component, store, eventHub) {
  class Wrapped extends React.Component {
    constructor(props){
      super(props);
      eventHub.on('update', this.updateComponent);
    }
    updateComponent = () => {
      this.forceUpdate();
    }
    render() {
      return (
        <Provider store={store}>
          <LocaleProvider locale={ruRU}>
            <Component {...this.props}/>
          </LocaleProvider>
        </Provider>
      );
    }
  }
  return Wrapped;
};
