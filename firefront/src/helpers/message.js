import { message as showMessage } from 'antd';
import { notifySuccessDuration, notifyErrorDuration, notifyLoadingDuration } from '../../config/app';

const message = (type, text, duration) => {
  if (!duration) {
    if (type === 'error') duration = notifyErrorDuration;
    if (type === 'loading') duration = notifyLoadingDuration;
    else duration = notifySuccessDuration;
  }

  showMessage[type](text, duration);
};

export default message;
