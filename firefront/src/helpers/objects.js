export default function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

// Преобразовывает объект в http строку параметров
export function objToQueryString(obj) {
  return (
    '?' +
    Object.keys(obj)
      .map(function(key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
      })
      .join('&')
  );
}

/**
* Получить значение объекта по строке
* @param {Object} o - Объект, по которому идет поиск
* @param {String} s - Путь к значению
* @return {any}
*/
export function objValueByStringPath(o, s) {
  s = s.replace(/\[(\w+)\]/g, '.$1');
  s = s.replace(/^\./, '');
  var a = s.split('.');
  for (var i = 0, n = a.length; i < n; ++i) {
    var k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
}
