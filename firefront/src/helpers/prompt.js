import { Modal } from 'antd';

/**
 * Подтверждение сохранения несохраненных изменений
 * @param {function} func - Функция сохранения
 * @param {any} args - Набор аргументов, которые будет переданны в функцию сохранения
 */
export const promptUnsavedChanges = (func, ...args) => {
  Modal.confirm({
    title: 'Есть несохраненные данные',
    content: 'Сохранить?',
    okText: 'Да',
    cancelText: 'Нет',
    onOk: () => {
      return func(...args);
    }
  });
};
