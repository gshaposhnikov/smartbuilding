/**
 * Генератор HTML-строки
 * @param {string} title - заголовок страницы
 * @param {string} styles - чистые css-стили
 */
export default class HTML {
  constructor(props) {
    this.props = props;
    this.htmlString = '';
    this.generateHTML();
    return this.htmlString;
  }
  // Основная функция создания HTML-строки
  generateHTML = () => {
    this.htmlString += '<!DOCTYPE html>';
    this.htmlString += '<head>';
    this.addMeta();
    this.addStyles();
    this.addTitle();
    this.addBody();
    this.htmlString += '</head>';
    this.htmlString += '</html>';
  };
  // Добавляет мета-теги
  addMeta = () => {
    this.htmlString += '<meta charset="utf-8">';
    this.htmlString += '<meta name="viewport" content="width=device-width, initial-scale=1">';
  };
  // Добавляет стили
  addStyles = () => {
    const { styles } = this.props;
    this.htmlString += '<style>';
    this.htmlString += 'html,body{margin:0;padding:0;}';
    this.htmlString += 'body{width:100%;height:100vh;color:#000;overflow:auto;}';
    this.htmlString +=
      '*{font-family:"Helvetica Neue For Number",-apple-system,BlinkMacSystemFont,' +
      '"Segoe UI",Roboto,"PingFang SC","Hiragino Sans GB","Microsoft YaHei","Helvetica Neue",' +
      'Helvetica, Arial, sans-serif;font-size:12px;}td,th{padding:4px;border:1px solid #fff}' +
      'thead tr:nth-child(1) th{background:lightgray;position: sticky;top: 0;z-index:10;}' +
      'table{width:100%;}';
    this.htmlString += styles || '';
    this.htmlString += '</style>';
  };
  // Добавляет заголовок страницы
  addTitle = () => {
    const { title } = this.props;
    this.htmlString += '<title>';
    this.htmlString += `FIRESEC: ${title || ''}`;
    this.htmlString += '</title>';
  };
  // Обертка для содержимого  страницы
  addBody = () => {
    this.htmlString += '<body>';
    this.generateContent();
    this.htmlString += '</body>';
  };
  // Получние содержимого страницы
  generateContent = () => {
    const { contentType } = this.props;
    switch (contentType) {
      case 'table': {
        this.generateTable();
        break;
      }
      default: {
        break;
      }
    }
  };
  // Получение таблицы
  generateTable = () => {
    this.htmlString += '<table cellspacing=0>';
    this.getTableHeader();
    this.getTableBody();
    this.htmlString += '</table>';
  };
  // Получение шапки таблицы
  getTableHeader = () => {
    const { columns } = this.props;
    this.htmlString += '<thead><tr>';
    columns.forEach(col => {
      this.htmlString += `<th style="width:${col.width || ''};">`;
      this.htmlString += col.title || '';
      this.htmlString += '</th>';
    });
    this.htmlString += '</tr></thead>';
  };
  // Получение тела таблицы
  getTableBody = () => {
    const { data, columns } = this.props;
    this.htmlString += '<tbody>';
    data.forEach(record => {
      this.htmlString += '<tr>';
      columns.forEach(col => {
        this.htmlString += `<td style="background:${record.color};color:${record.fontColor}">`;
        if (col.render) {
          this.htmlString += col.render(record);
        } else {
          this.htmlString += record[col.dataIndex] || '';
        }
        this.htmlString += '</td>';
      });
      this.htmlString += '</tr>';
    });
    this.htmlString += '</tbody>';
  };
}
