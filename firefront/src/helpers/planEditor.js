/**
 * Функция для сортировки объектов по слоям
 * Вызывается после полной отрисовки всех svg-объектов
 *
 * Слои должны идти в таком порядке
 * 1. Сетка
 * 2. Фоновое изображение
 * 3. Зоны
 * 4. Устройства
 * 5. Попап
 *
 * @param {Object} fabricCanvas
 * @return {Object} Сгруппированные объекты плана
 *
 */
function layerSorter(fabricCanvas) {
  if (fabricCanvas && fabricCanvas.getObjects()) {
    let layer = 0;
    const upper = object => {
      fabricCanvas.moveTo(object, layer);
      layer++;
    };
    const objects = fabricCanvas.getObjects();
    const grid = [],
      backgrounds = [],
      regions = [],
      devices = [];
    objects.forEach(object => {
      const { info } = object;
      if (info) {
        if (info.gridKey) grid.push(object);
        else if (info.backgroundKey) backgrounds.push(object);
        else if (info.regionKey) regions.push(object);
        else if (info.deviceKey) devices.push(object);
      }
    });
    grid.forEach(object => upper(object));
    backgrounds.forEach(object => upper(object));
    regions.forEach(object => upper(object));
    devices.forEach(object => upper(object));
    return {
      grid,
      backgrounds,
      regions,
      devices
    };
  }
}

/**
 * Удаляет одинаковые точки
 * @param {Array} points
 * @return {Array}
 */
function filterSamePoints(points) {
  return points.filter((point, index) =>
    index === 0 ? 1 : points[index - 1].x !== point.x || points[index - 1].y !== point.y
  );
}

/**
 * Изменяет возможность выделения объекта
 * @param {Object} fabricCanvas
 * @param {boolean} state
 */
function setObjectSelectable(fabricCanvas, state = false) {
  fabricCanvas.forEachObject(object => {
    if (object.info && object.info.gridKey !== 'grid' && !object.info.backgroundKey) {
      object.selectable = state;
      object.hoverCursor = object.selectable ? 'pointer' : 'default';
    }
  });
}

/**
 * Возвращает y-положение с учетом размеров плана и сетки
 * @param {Object} target
 * @param {Object} plan
 * @param {Object} gridState
 * @param {Object} gridSize
 * @return {number}
 */
function getObjTopPosition(target, plan, gridState, gridSize) {
  const { top, height, scaleY } = target;
  const minTop = 0,
    maxTop = plan.ySize - height * scaleY,
    newTop = gridState ? Math.round(top / gridSize) * gridSize : top;
  return top > minTop ? (top > maxTop ? maxTop : newTop > maxTop ? maxTop : newTop) : minTop;
}

/**
 * Возвращает x-положение с учетом размеров плана и сетки
 * @param {Object} target
 * @param {Object} plan
 * @param {Object} gridState
 * @param {Object} gridSize
 * @return {number}
 */
function getObjLeftPosition(target, plan, gridState, gridSize) {
  const { left, width, scaleX } = target;
  const minLeft = 0,
    maxLeft = plan.xSize - width * scaleX,
    newLeft = gridState ? Math.round(left / gridSize) * gridSize : left;
  return left > minLeft
    ? left > maxLeft
      ? maxLeft
      : newLeft > maxLeft
      ? maxLeft
      : newLeft
    : minLeft;
}

/**
 * Возвращает точки для прямоугольников
 * @param {Object} target
 * @param {Object} plan
 * @return {Array}
 */
function getRectPoints(target, plan) {
  const min = 0,
    maxX = plan.xSize,
    maxY = plan.ySize;
  const x1 = target.left < min ? min : target.left > maxX ? maxX : target.left,
    x2 = x1 + target.width * target.scaleX > maxX ? maxX : x1 + target.width * target.scaleX,
    y1 = target.top < min ? min : target.top > maxY ? maxY : target.top,
    y2 = y1 + target.height * target.scaleY > maxY ? maxY : y1 + target.height * target.scaleY;

  return [
    { x: x1, y: y1 },
    {
      x: x2,
      y: y2
    }
  ];
}

/**
 * Возвращает точки для многоугольников
 * @param {Object} target
 * @param {Object} fabric
 * @return {Array}
 */
function getPolygonPoints(target, fabric) {
  return target.points.map(point => ({
    x: point.x - (fabric.util.array.min(target.points, 'x') - target.left),
    y: point.y - (fabric.util.array.min(target.points, 'y') - target.top)
  }));
}

/**
 * Возвращает объекты отфильтрованные по подсистеме
 * @param {String} subsystem
 * @param {Array} objects
 * @return {Array}
 */
function filterObjectBySubsytem(subsystem, objects) {
  return objects.filter(object => object.info && object.info.subsystem === subsystem);
}

/**
 * Возвращает смежные объекты
 * @param {Object} target
 * @param {Array} objects
 * @return {Array}
 */
function getTargetIntersectionsWithObjects(target, objects) {
  return objects.filter(object => target.intersectsWithObject(object));
}

/**
 * Возвращает валидные x-координаты
 * @param {number} x
 * @param {number} maxX
 * @return {number}
 */
function getValidxPosition(x, maxX, grid) {
  if (grid) {
    if (x < 0) return 0;
    if (x > maxX) return maxX;
    return Math.round(x / grid) * grid;
  } else return x < 0 ? 0 : x > maxX ? maxX : x;
}

/**
 * Возвращает валидные y-координаты
 * @param {number} y
 * @param {number} maxY
 * @return {number}
 */
function getValidyPosition(y, maxY, grid) {
  if (grid) {
    if (y < 0) return 0;
    if (y > maxY) return maxY;
    return Math.round(y / grid) * grid;
  } else return y < 0 ? 0 : y > maxY ? maxY : y;
}

/**
 * Фильтрует объекты по ключу
 * @param {Array} objects
 * @param {String} key
 * @return {Array}
 */
function filterPlanObjectsbyKey(objects, key) {
  const keys = [];
  return objects.filter(object => {
    if (object.info && object.info[key] && !keys.includes(object.info[key])) {
      keys.push(object.info[key]);
      return true;
    } else return false;
  });
}

/**
 * Удаляет объекты плана
 * @param {Object} fabricCanvas
 * @param {Array} objects
 */
function removePlanObjects(fabricCanvas, objects) {
  objects.forEach(object => fabricCanvas.remove(object));
}

export {
  layerSorter,
  filterSamePoints,
  setObjectSelectable,
  getObjTopPosition,
  getObjLeftPosition,
  getRectPoints,
  getPolygonPoints,
  filterObjectBySubsytem,
  getTargetIntersectionsWithObjects,
  getValidxPosition,
  getValidyPosition,
  filterPlanObjectsbyKey,
  removePlanObjects
};
