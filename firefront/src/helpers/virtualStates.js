/**
 * Получить выбранное виртуальное состояние.
 *
 * @param project - текущий проект
 * @param selectedVirtualStateId - идентификатор выбранного вирт. состояния
 * @param projectVirtualStates - список вирт. состояний
 * @return виртуальное состояние, или null, если не выбран
 */
function getSelectedVirtualState(project, selectedVirtualStateId, projectVirtualStates) {
  let result = null;
  if (project && project.id && selectedVirtualStateId) {
    if (projectVirtualStates) {
      result = projectVirtualStates.find(virtualState => virtualState.id === selectedVirtualStateId);
    }
  }
  return result;
}

export { getSelectedVirtualState };
