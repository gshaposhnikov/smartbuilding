export function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export function initializeValues(entity) {
  const initialValues = {};
  Object.keys(entity).forEach(key => {
    initialValues[key] = { value: entity[key] };
  });
  return initialValues;
}

/**
 * Функция для проверки успешного выполнения операций
 * @param {boolean|null} state.inProgress[key]
 * @param {string|null} state.errors[key]
 *
 * см. reducers/inProgress.js
 */
export function checkCompletion(progress, error) {
  return progress === false && !error ? true : false;
}

/**
 * Функция для получения первой возникшей ошибки на форме
 * @param syncErrors {<field_name>: <error_message>} результат валидации
 * @return string текст ошибки или null, если ошибок нет
 */
export function getFirstError(syncErrors) {
  if (syncErrors && Object.keys(syncErrors).length > 0)
    return syncErrors[Object.keys(syncErrors)[0]];
  else return null;
}
