import fetch from 'isomorphic-fetch';
import { apiUrl, protocol } from '../../config/app';

const restApi = apiUrl ? `${protocol}://${apiUrl}` : '';

const fetchJson = (...args) => {
  const [url, ...otherArgs] = args;
  return fetch(restApi + '/api/v1' + url, {
    credentials: 'include',
  }, ...otherArgs).then(r => r.json());
}

export const fetchRequest = (...args) => {
  const [url, type = "POST", body = {}, ...otherArgs] = args;
  return fetch(restApi + '/api/v1' + url, {
    method: type,
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }, ...otherArgs).then(r => {return r.headers.get('content-type')
    ? r.json()
    : r.ok
      ? null
      : {error: r.status.toString() + ': ' + r.statusText}
  });
}

export const fetchLoginRequest = (...args) => {
  const [url, type = "POST", ...otherArgs] = args;
  return fetch(restApi + url, {
    method: type,
    credentials: 'include',
  }, ...otherArgs).then(response => {
    const isOk = response.status === 200;
    const error = response.status === 401 ? "Неверный логин и/или пароль" : "Авторизация не выполнена";
    return {status: isOk, error: error};
  });
}

export const fetchLogoutRequest = (...args) => {
  const [url, type = "GET", ...otherArgs] = args;
  return fetch(restApi + url, {
    method: type,
    credentials: 'include'
  }, ...otherArgs).then(response => {
    return response.status === 200;
  });
}

export default fetchJson;
