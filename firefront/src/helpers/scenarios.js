import { createSelector } from 'reselect';

import { getCurrentProjectScenariosHash } from 'helpers/currentProject';

/**
 * Добавить рекурсивно уникальные ключи во все элементы дерева или поддерева
 * @param {Object} item объект дерева
 * @param {string} parentKey ключ родительского элемента
 * @param {number} index номер элемент в массиве текущего уровня
 */
export function addKeys(item, parentKey, index) {
  if (!item) return;
  item.key = `${parentKey}.${index}`;
  if (item.children) {
    item.children.forEach((child, index) => addKeys(child, item.key, index));
  }
}

/**
 * Получить экземпляр сценария
 * @param {Object} state.currentProjectId id текущего проекта
 * @param {string} state.widgets.currentScenarioId идентификатор сценария
 * @param {Object[]} state.scenarios список сценариев всех проектов
 * @returns {Object| null} найденный экземпляр сценария или null
 */
export function getSelectedScenario({
  currentProjectId,
  widgets: { currentScenarioId },
  scenarios
}) {
  if (!currentScenarioId || !currentProjectId) return null;
  if (!scenarios[currentProjectId]) return null;
  if (!scenarios[currentProjectId][currentScenarioId]) return null;
  const scenario = { ...scenarios[currentProjectId][currentScenarioId] };
  scenario.timeLineBlocksTree.forEach((item, index) => addKeys(item, '', index));
  return scenario;
}

/**
 * Получить исполнительные сценарии
 * @param {String} projectId идентификатор проекта
 * @param {Object[]} projectScenarios список сценариев всех проектов
 * @returns {Object[]} список сценариев
 */
export const getExecutiveScenarios = createSelector(getCurrentProjectScenariosHash, scenariosHash =>
  Object.values(scenariosHash).filter(
    scenario => scenario.basicParams.enabled && scenario.scenarioPurpose === 'EXEC_BY_INVOKE'
  )
);
