import { createSelector } from 'reselect';

/**
 * Получить список проектов
 * @param {Object{}} state
 * @return {Array|null}
 */
export const getProjectList = createSelector(
  getProjectHash,
  projectHash => (projectHash ? Object.values(projectHash) : null)
);

function getProjectHash({ projects }) {
  if (!projects) return {};
  return projects;
}
