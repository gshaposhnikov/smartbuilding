import { put, call } from 'redux-saga/effects';
import fetchJson, { fetchRequest } from 'helpers/fetch';
import { isLoaded } from 'constants/actionTypes';

/* 
 * TODO: перенести сюда из sagas все проверки на пустой ответ и ответ, содержащий error,
 * или разобраться, почему в fetchJson и fetchRequest по этим ошибкам
 * не выбрасывается исключение
 */
const fetch = (...args) => call(fetchJson, ...args);
const request = (...args) => call(fetchRequest, ...args);
const dispatch = (type, payload) => put({ type, payload });
const dispatchSuccess = (type, payload) => dispatch(isLoaded(type, true), payload);
const dispatchFail = (type, payload) => dispatch(isLoaded(type, false), payload);

export { fetch, request, dispatch, dispatchSuccess, dispatchFail };
