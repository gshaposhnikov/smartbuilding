// Состояния (SRC:SMARTBUILDNIG)
export const STATE = {
  ON: 'on', // Вкл.
  OFF: 'off' // Откл.
};
export const STATE_NAME = {
  [STATE.ON]: 'Вкл.',
  [STATE.OFF]: 'Выкл.'
};
export const STATE_ACTION = {
  [STATE.ON]: STATE.OFF,
  [STATE.OFF]: STATE.ON
};
// Соответствие состояний FIRESEC и SMARTBUILDNIG
export const STATE_MAP = {
  Warning: STATE.ON,
  Unknown: STATE.OFF,
  Normal: STATE.OFF
};
