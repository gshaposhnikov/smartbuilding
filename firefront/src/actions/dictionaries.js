import {
  APPLICATION_INFO_LOAD
} from 'constants/actionTypes';
import { createAction } from 'redux-actions';

export const loadApplicationInfo = createAction(APPLICATION_INFO_LOAD);