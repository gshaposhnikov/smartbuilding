import {
  ISSUES_LOAD,
  ISSUES_DELETE_FINISHED,
  ISSUES_DELETE_ERROR,
  ISSUES_DELETE_IN_QUEUE,
  ISSUE_DELETE,
  ISSUE_PLAY,
  ISSUE_PAUSE
} from 'constants/actionTypes';
import { createAction } from 'redux-actions';

export const loadIssues = createAction(ISSUES_LOAD);
export const deleteFinishedIssues = createAction(ISSUES_DELETE_FINISHED);
export const deleteErrorIssues = createAction(ISSUES_DELETE_ERROR);
export const deleteAllIssuesInQueue = createAction(ISSUES_DELETE_IN_QUEUE);
export const deleteIssue = createAction(ISSUE_DELETE, issueId => ({ issueId }));
export const playIssue = createAction(ISSUE_PLAY, issueId => ({ issueId }));
export const pauseIssue = createAction(ISSUE_PAUSE, issueId => ({ issueId }));