import {
  STATE_CATEGORY_VIEWS_LOAD,
} from 'constants/actionTypes';

import { createAction } from 'redux-actions';

export const loadStateCategoryViews = createAction(STATE_CATEGORY_VIEWS_LOAD);
