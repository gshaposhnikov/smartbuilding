import { createAction } from 'redux-actions';

import { LICENSE_LOAD_PERMISSIONS } from 'constants/actionTypes';

export const loadLicensePermissions = createAction(LICENSE_LOAD_PERMISSIONS);
