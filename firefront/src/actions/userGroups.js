import {
  USER_GROUPS_LOAD,
  USER_GROUP_CREATE,
  USER_GROUP_UPDATE,
  USER_GROUP_DELETE,
  USER_GROUP_SELECT
} from 'constants/actionTypes';
import { createAction } from 'redux-actions';

export const loadUserGroups = createAction(USER_GROUPS_LOAD, userGroups => userGroups);
export const createUserGroup = createAction(USER_GROUP_CREATE, userGroup => userGroup);
export const updateUserGroup = createAction(USER_GROUP_UPDATE, (userGroupId, userGroup) => ({userGroupId, userGroup}));
export const deleteUserGroup = createAction(USER_GROUP_DELETE, userGroupId => userGroupId);
export const selectUserGroup = createAction(USER_GROUP_SELECT, userGroupId => userGroupId);
