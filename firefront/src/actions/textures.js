import { createAction } from 'redux-actions';
import { TEXTURES_ADD, TEXTURES_REMOVE, TEXTURES_CLEAR } from 'constants/actionTypes';

export const addTexture = createAction(TEXTURES_ADD, (textureId, texture) => ({
  textureId,
  texture
}));
export const removeTexture = createAction(TEXTURES_REMOVE, textureId => ({ textureId }));
export const clearTextures = createAction(TEXTURES_CLEAR);
