import {
  VIRTUAL_STATES_LOAD,
  VIRTUAL_STATE_CREATE,
  VIRTUAL_STATE_UPDATE,
  VIRTUAL_STATE_DELETE,
  VIRTUAL_STATE_SELECT
} from 'constants/actionTypes';

import { createAction } from 'redux-actions';

export const loadVirtualStates = createAction(VIRTUAL_STATES_LOAD, projectId => projectId);
export const createVirtualState = createAction(VIRTUAL_STATE_CREATE, (projectId, params) => ({
  projectId,
  params
}));
export const updateVirtualState = createAction(
  VIRTUAL_STATE_UPDATE,
  (projectId, virtualStateId, params) => ({ projectId, virtualStateId, params })
);
export const deleteVirtualState = createAction(
  VIRTUAL_STATE_DELETE,
  (projectId, virtualStateId) => ({ projectId, virtualStateId })
);

export const selectVirtualState = createAction(
  VIRTUAL_STATE_SELECT,
  virtualStateId => virtualStateId
);
