import {
  PLANS_LOAD_ACTIVE,
  PLAN_GROUPS_LOAD_ACTIVE,
  PLAN_GROUP_CREATE,
  PLAN_GROUP_UPDATE,
  PLAN_GROUP_DELETE,
  PLAN_CREATE,
  PLAN_UPDATE,
  PLAN_DELETE,
  PLANS_LOAD,
  PLAN_GROUPS_LOAD
} from 'constants/actionTypes';

import { createAction } from 'redux-actions';

export const loadPlans = createAction(PLANS_LOAD, projectId => ({ projectId }));
export const loadPlanGroups = createAction(PLAN_GROUPS_LOAD, projectId => ({ projectId }));
export const loadActivePlans = createAction(PLANS_LOAD_ACTIVE, projectId => ({ projectId }));
export const loadActivePlanGroups = createAction(PLAN_GROUPS_LOAD_ACTIVE, projectId => ({
  projectId
}));
export const createPlanGroup = createAction(PLAN_GROUP_CREATE, (planGroup, projectId) => ({
  planGroup,
  projectId
}));
export const updatePlanGroup = createAction(PLAN_GROUP_UPDATE, (planGroup, projectId) => ({
  planGroup,
  projectId
}));
export const deletePlanGroup = createAction(PLAN_GROUP_DELETE, (planGroupId, projectId) => ({
  planGroupId,
  projectId
}));
export const createPlan = createAction(PLAN_CREATE, (plan, projectId) => ({
  plan,
  projectId
}));
export const updatePlan = createAction(PLAN_UPDATE, (plan, projectId) => ({
  plan,
  projectId
}));
export const deletePlan = createAction(PLAN_DELETE, (planId, projectId) => ({
  planId,
  projectId
}));
