import {
  DEVICE_TREE_CHANGE_VIEW_MODE,
  REGION_VIEWER_UPDATE_CURRENT,
  ACTIVE_SCENARIOS_SET_FILTER,
  CURRENT_ENTITY_INFO_UPDATE,
  REGIONS_CHANGE_FILTER
} from 'constants/actionTypes';

import { createAction } from 'redux-actions';

export const deviceTreeChangeViewMode = createAction(DEVICE_TREE_CHANGE_VIEW_MODE, mode => mode);
export const regionViewerUpdateCurrent = createAction(
  REGION_VIEWER_UPDATE_CURRENT,
  currentRegion => currentRegion
);
export const changeActiveScenariosFilter = createAction(
  ACTIVE_SCENARIOS_SET_FILTER,
  filter => filter
);
export const updateCurrentEntityInfo = createAction(
  CURRENT_ENTITY_INFO_UPDATE,
  entityInfo => entityInfo
);
export const changeRegionsFilter = createAction(REGIONS_CHANGE_FILTER, filter => filter);
