import { createAction } from 'redux-actions';
import {
  SKUD_ENTITIES,
  getAddActionType,
  getUpdateActionType,
  getRemoveActionType,
  SKUD_TREE_ITEM_SELECT,
  WORK_SCHEDULES_TREE_ITEM_SELECT,
  WORK_SCHEDULE_DAY_ADD,
  WORK_SCHEDULE_DAY_UPDATE,
  WORK_SCHEDULE_DAY_REMOVE,
  ACCESS_GRANTED_TIME_ADD,
  ACCESS_GRANTED_TIME_UPDATE,
  ACCESS_GRANTED_TIME_REMOVE
} from 'constants/actionTypes';
import {
  SKUD_WRITE_DB_TO_CONTROL_DEVICES,
  ACCESS_KEY_START_READ_VALUE,
  ACCESS_KEY_STOP_READ_VALUE
} from 'constants/actionTypeModules/skud';

/* usage: skudActions.<entityType - from SKUD_ENTITIES>.<actionType - add | update | remove>(...args) */
export const skudMainActions = {};
Object.keys(SKUD_ENTITIES).forEach(entityName => {
  skudMainActions[entityName] = {};
  skudMainActions[entityName].add = createAction(
    getAddActionType(entityName),
    (projectId, entityData) => ({
      projectId,
      entityData
    })
  );
  skudMainActions[entityName].update = createAction(
    getUpdateActionType(entityName),
    (projectId, entityId, entityData) => ({ projectId, entityId, entityData })
  );
  skudMainActions[entityName].remove = createAction(
    getRemoveActionType(entityName),
    (projectId, entityId) => ({
      projectId,
      entityId
    })
  );
});

export const selectSkudTreeItem = createAction(SKUD_TREE_ITEM_SELECT, treeItem => treeItem);
export const selectWorkSchedulesTreeItem = createAction(
  WORK_SCHEDULES_TREE_ITEM_SELECT,
  treeItem => treeItem
);

export const addWorkScheduleDay = createAction(
  WORK_SCHEDULE_DAY_ADD,
  (projectId, workScheduleId, day) => ({ projectId, workScheduleId, day })
);

export const updateWorkScheduleDay = createAction(
  WORK_SCHEDULE_DAY_UPDATE,
  (projectId, workScheduleId, dayNo, day) => ({ projectId, workScheduleId, dayNo, day })
);

export const removeWorkScheduleDay = createAction(
  WORK_SCHEDULE_DAY_REMOVE,
  (projectId, workScheduleId, dayNo) => ({ projectId, workScheduleId, dayNo })
);

export const addAccessGrantedTime = createAction(
  ACCESS_GRANTED_TIME_ADD,
  (projectId, workScheduleId, dayNo, accessGrantedTime) => ({
    projectId,
    workScheduleId,
    dayNo,
    accessGrantedTime
  })
);

export const updateAccessGrantedTime = createAction(
  ACCESS_GRANTED_TIME_UPDATE,
  (projectId, workScheduleId, dayNo, timeNo, accessGrantedTime) => ({
    projectId,
    workScheduleId,
    dayNo,
    timeNo,
    accessGrantedTime
  })
);

export const removeAccessGrantedTime = createAction(
  ACCESS_GRANTED_TIME_REMOVE,
  (projectId, workScheduleId, dayNo, timeNo) => ({ projectId, workScheduleId, dayNo, timeNo })
);

export const writeSkudDBToControlDevices = createAction(
  SKUD_WRITE_DB_TO_CONTROL_DEVICES,
  (projectId, controlDeviceIds) => ({ projectId, controlDeviceIds })
);

export const startReadAccessKeyValue = createAction(
  ACCESS_KEY_START_READ_VALUE,
  (projectId, deviceId, accessKeyId) => ({ projectId, deviceId, accessKeyId })
);

export const stopReadAccessKeyValue = createAction(
  ACCESS_KEY_STOP_READ_VALUE,
  (projectId, deviceId) => ({ projectId, deviceId })
);
