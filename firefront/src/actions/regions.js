import {
  REGION_UPDATE_CURRENT,
  REGION_CREATE,
  REGION_NEW_DEVICE,
  REGION_DELETE_DEVICE,
  REGION_DELETE,
  REGION_UPDATE,
  REGIONS_LOAD_ACTIVE,
  REGION_CHANGE_GUARD_STATUS,
  REGION_CONSTANTS_LOAD,
  REGIONS_LOAD,
  REGION_UPDATE_PLAN_LAYOUTS,
  REGION_NEW_DEVICE_LIST,
  REGION_DELETE_DEVICE_LIST,
  REGION_CREATE_AND_CONNECT_DEVICE
} from 'constants/actionTypes';

import { createAction } from 'redux-actions';

// TODO: находить текущую зону по ID в объекте зон, вместо дублирования всего объекта
export const updateCurrentRegion = createAction(REGION_UPDATE_CURRENT, region => region);
export const createRegion = createAction(REGION_CREATE, region => region);
export const newDeviceRegion = createAction(REGION_NEW_DEVICE, connection => connection);
export const deleteRegion = createAction(REGION_DELETE, (currentProjectId, regionId) => ({
  projectId: currentProjectId,
  regionId: regionId
}));
export const deleteDeviceRegion = createAction(REGION_DELETE_DEVICE, connection => connection);
export const updateRegion = createAction(REGION_UPDATE, updatedRegion => updatedRegion);

export const loadActiveRegions = createAction(REGIONS_LOAD_ACTIVE, projectId => ({ projectId }));
export const changeRegionGuardStatus = createAction(
  REGION_CHANGE_GUARD_STATUS,
  (projectId, regionId, onGuard) => ({ projectId, regionId, onGuard })
);
export const loadRegionConstants = createAction(REGION_CONSTANTS_LOAD);
export const loadRegions = createAction(REGIONS_LOAD, projectId => ({ projectId }));
export const updateRegionPlanLayouts = createAction(
  REGION_UPDATE_PLAN_LAYOUTS,
  (projectId, regionId, planLayouts) => ({ projectId, regionId, planLayouts })
);
export const newDeviceListRegion = createAction(
  REGION_NEW_DEVICE_LIST,
  (projectId, regionId, deviceIds) => ({
    projectId,
    regionId,
    deviceIds
  })
);
export const deleteDeviceListRegion = createAction(
  REGION_DELETE_DEVICE_LIST,
  (projectId, regionId, deviceIds) => ({
    projectId,
    regionId,
    deviceIds
  })
);
export const createRegionAndConnectDevice = createAction(
  REGION_CREATE_AND_CONNECT_DEVICE,
  (projectId, region, deviceId) => ({ projectId, region, deviceId })
);
