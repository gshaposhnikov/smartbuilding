import {
  USERS_LOAD,
  USER_CREATE,
  USER_UPDATE,
  USER_DELETE,
  USER_SELECT
} from 'constants/actionTypes';
import { createAction } from 'redux-actions';

export const loadUsers = createAction(USERS_LOAD);
export const createUser = createAction(USER_CREATE, user => user);
export const updateUser = createAction(USER_UPDATE, (userId, user) => ({userId, user}));
export const deleteUser = createAction(USER_DELETE, (userId, active) => ({userId, active}));
export const selectUser = createAction(USER_SELECT, userId => userId);
