import fetchJson from 'helpers/fetch';

const fetchInitDataAsync = async () => {
  let plans, panels, devices = [], log, totalPages;

  plans = await fetchJson('/plans');

  plans = plans.content;
  
  const planId = plans[0].id;

  panels = await fetchJson(`/plans/${planId}/devices?page=0&size=6`);

  panels = panels.content;

  const promises = [];

  for (let panel of panels) {

    if(panel.nChildDevices > 0) {
      let panelDevices = await fetchJson(`/plans/${planId}/devices?parent=${panel.id}&page=0&size=512`).then(i => i.content);
      promises.push(panelDevices);
      panel.children = [];
    };
    panel.key = panel.id;
    devices.push(panel);
  }

  const panelDevices = await Promise.all(promises);

  panelDevices.forEach((content, key) => {
    content.forEach(device => {
      device.key = device.id;
      panels[key].children.push(device);
    })
  });

  log = await fetchJson('/log_records?size=10&page=0&sort=dateTime,desc');
  log = log.content;

  return {plan: plans[0], devices: devices, log:log};
};

const initData = async () => {
  const data = await fetchInitDataAsync();
  postMessage({type: 'init', data: data});
}

export default initData;