import appConfig from 'appConfig';
import Stomp from 'stompjs';

let stompClient = null;

function stompDisconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

export default function stompConnect() {
    
    let socket = new WebSocket(`ws://${appConfig.apiUrl}/api/v1/ws`);
    stompClient = Stomp.over(socket);
    stompClient.debug = null;

    stompClient.connect({}, frame => {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/api/v1/ws', response => {
            let data = JSON.parse(response.body);

            postMessage({type: `socket.${data.contentType}`, data: data});
        });
    });

    socket.onclose = function () {
        stompDisconnect();
    };
}