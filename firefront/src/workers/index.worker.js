import initData from './controllers/api';
import stompConnect from './controllers/sockets';

const startWorker = async () => {
  await initData();
  await stompConnect();
}

startWorker();