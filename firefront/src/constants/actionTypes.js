const isLoaded = (constant, type) => {
  if (type) return constant + '_SUCCEEDED';
  else return constant + '_FAILED';
};

import * as devices from './actionTypeModules/devices';
import * as deviceShapes from './actionTypeModules/deviceShapes';
import * as events from './actionTypeModules/events';
import * as indicators from './actionTypeModules/indicators';
import * as issues from './actionTypeModules/issues';
import * as logViews from './actionTypeModules/logViews';
import * as medias from './actionTypeModules/medias';
import * as plans from './actionTypeModules/plans';
import * as projects from './actionTypeModules/projects';
import * as regions from './actionTypeModules/regions';
import * as scenarios from './actionTypeModules/scenarios';
import * as users from './actionTypeModules/users';
import * as virtualStates from './actionTypeModules/virtualStates';
import * as skud from './actionTypeModules/skud';

import * as other from './actionTypeModules/other';

module.exports = {
  ...devices,
  ...deviceShapes,
  ...events,
  ...indicators,
  ...issues,
  ...logViews,
  ...medias,
  ...plans,
  ...projects,
  ...regions,
  ...scenarios,
  ...users,
  ...virtualStates,
  ...skud,
  ...other,
  isLoaded
};
