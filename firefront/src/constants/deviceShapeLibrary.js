export const ENTITY_TYPE = {
  DEVICE_PROFILE: 'DEVICE_PROFILE',
  DEVICE_SHAPE_LIBRARY: 'DEVICE_SHAPE_LIBRARY',
  DEVICE_SHAPE: 'DEVICE_SHAPE',
  NEW_DEVICE_SHAPE_LIBRARY: 'NEW_DEVICE_SHAPE_LIBRARY',
  NEW_DEVICE_SHAPE: 'NEW_DEVICE_SHAPE'
};

export const ANIMATION_TYPE = [
  { id: 'NONE', name: 'Нет анимации' },
  { id: 'BLINKING', name: 'Мерцание' },
  { id: 'RESIZING', name: 'Изменение размера' },
  { id: 'ROTATING', name: 'Вращение' }
];

export const ANIMATION_SPEED = {
  MIN: 500,
  MAX: 5000
};
