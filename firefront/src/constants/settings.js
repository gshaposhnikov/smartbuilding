const settings = {
  iconSizes: [
    {
      label: 'Очень маленький',
      value: 12
    },
    {
      label: 'Маленький',
      value: 16
    },
    {
      label: 'Нормальный',
      value: 23,
      active: true
    },
    {
      label: 'Большой',
      value: 25
    },
    {
      label: 'Очень большой',
      value: 30
    }
  ],
  locale: 'ru',
  timeFormat: 'DD.MM.YYYY HH:mm:ss',
  chartsSettings: {
    global: {
      useUTC: false
    },
    lang: {
      loading: 'Загрузка...',
      months: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
      ],
      weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
      shortMonths: [
        'Янв',
        'Фев',
        'Март',
        'Апр',
        'Май',
        'Июнь',
        'Июль',
        'Авг',
        'Сент',
        'Окт',
        'Нояб',
        'Дек'
      ],
      exportButtonTitle: 'Экспорт',
      printButtonTitle: 'Печать',
      rangeSelectorFrom: 'С',
      rangeSelectorTo: 'По',
      rangeSelectorZoom: 'Период',
      downloadPNG: 'Скачать PNG',
      downloadJPEG: 'Скачать JPEG',
      downloadPDF: 'Скачать PDF',
      downloadSVG: 'Скачать SVG',
      printChart: 'Напечатать график'
    }
  }
};

import moment from 'moment';
moment.locale(settings.locale);

export const iconSizes = settings.iconSizes;
export const timeFormat = settings.timeFormat;
export const chartsSettings = settings.chartsSettings;
