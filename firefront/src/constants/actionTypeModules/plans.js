/* Планы помещений и группы планов */

/**
 * Загрузить все планы помещений активного проекта.
 *
 * Payload: {
 *  projectId: <string>
 * }
 *
 * Store: state.activeProject.plans
 **/
export const PLANS_LOAD_ACTIVE = 'PLANS_LOAD_ACTIVE';
/**
 * Загрузить все группы помещений активного проекта.
 *
 * Payload: {
 *  projectId: <string>
 * }
 *
 * Store: state.activeProject.plan_groups
 **/
export const PLAN_GROUPS_LOAD_ACTIVE = 'PLAN_GROUPS_LOAD_ACTIVE';
/**
 * Загрузить все группы планов текущего проекта.
 *
 * Payload: {
 *  projectId: <string>
 * }
 *
 * REST-метод: GET /api/v1/projects/{projectId}/plans/
 *
 * Store: state.plan_groups
 **/
export const PLANS_LOAD = 'PLANS_LOAD';
/**
 * Загрузить все группы планов текущего проекта.
 *
 * Payload: {
 *  projectId: <string>
 * }
 *
 * REST-метод: GET /api/v1/projects/{projectId}/plan_groups/
 *
 * Store: state.plan_groups
 **/
export const PLAN_GROUPS_LOAD = 'PLAN_GROUPS_LOAD';
/**
 * Создание группы планов
 * REST-метод: POST /api/v1/projects/{projectId}/plan_groups/
 **/
export const PLAN_GROUP_CREATE = 'PLAN_GROUP_CREATE';
/**
 * Изменение параметров группы (только название и описание можно изменить)
 * REST-метод: PUT /api/v1/projects/{projectId}/plan_groups/<planGroupId>
 **/
export const PLAN_GROUP_UPDATE = 'PLAN_GROUP_UPDATE';
/**
 * Удаление группы планов
 * REST-метод: DELETE /api/v1/projects/{projectId}/plan_groups/<planGroupId>
 **/
export const PLAN_GROUP_DELETE = 'PLAN_GROUP_DELETE';
/**
 * Создание плана
 * REST-метод: POST /api/v1/projects/{projectId}/plans
 **/
export const PLAN_CREATE = 'PLAN_CREATE';
/**
 * Изменение плана
 * REST-метод: PUT /api/v1/projects/{projectId}/plans/{planId}
 **/
export const PLAN_UPDATE = 'PLAN_UPDATE';
/**
 * Удаление плана
 * REST-метод: DELETE /api/v1/projects/{projectId}/plans/{planId}
 **/
export const PLAN_DELETE = 'PLAN_DELETE';
/**
 * См. описание REST-методов по группам и планам
 * https://docs.google.com/document/d/19fDadv_CBaeP-RlnnSvsjpdx7BCqSeax4MlCHjeEW28/edit#heading=h.kphw4ohe6rcn
 **/
