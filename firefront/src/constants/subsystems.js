const SUBSYSTEMS = {
  GENERAL: {
    id: 'GENERAL',
    name: '',
    color: '#000',
    index: 0,
    default: true,
    isFilterTag: true
  },
  AUTO: {
    id: 'AUTO',
    name: 'Автоматика',
    color: '#FF0000',
    index: 4,
    default: false,
    isFilterTag: false
  },
  FIRE: {
    id: 'FIRE',
    name: 'Пожарная',
    color: '#33CC33',
    index: 1,
    default: false,
    isFilterTag: true
  },
  SECURITY: {
    id: 'SECURITY',
    name: '',
    color: '#CCCC00',
    index: 2,
    default: false,
    isFilterTag: true
  },
  SKUD: {
    id: 'SKUD',
    name: 'СКУД',
    color: '#0066FF',
    index: 3,
    default: false,
    isFilterTag: true
  }
};

export default SUBSYSTEMS;
