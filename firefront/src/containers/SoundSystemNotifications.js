import React from 'react';
import { connect } from 'react-redux';
import { Icon, notification } from 'antd';
import { isEmpty } from 'lodash';

import { base64ToBuffer } from 'helpers/audio';

const WRAPPER_STYLES = {
  display: 'flex',
  position: 'relative',
  width: 40,
  height: 35,
  justifyContent: 'center',
  alignItems: 'center',
  cursor: 'pointer'
};
const MUTED_ICON_STYLES = {
  position: 'absolute',
  fontSize: 22,
  color: 'red'
};

const SOUND_NOTIFICATION_MESSAGE_PARAMS = {
  key: 'sound_notification_message',
  message: 'Автовоспроизведение звуков',
  description: (
    <div>
      <div>{'Браузер блокирует оповещения.'}</div>
      <div>{'Закройте, чтобы разблокировать.'}</div>
    </div>
  ),
  duration: 0
};

class SoundSystemNotifications extends React.Component {
  audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  audioSource = null;
  muted = localStorage.getItem('muted') === 'true' ? true : false;
  playing = false;
  dataLoaded = false;
  inited = false;
  prevStateCategoryId = null;
  userGestureNotRecorded = true; // Отсутствие действий пользователя на странице

  componentDidMount = () => {
    window.addEventListener('click', this.recordUserGesture);
  };

  recordUserGesture = () => {
    window.removeEventListener('click', this.recordUserGesture);
    this.userGestureNotRecorded = false;
    notification.close(SOUND_NOTIFICATION_MESSAGE_PARAMS.key);
    if (!this.muted) {
      const stateCategoryId = this.findMaxStateCategory(this.props);
      const sound = this.getSound(
        stateCategoryId,
        this.props.sounds,
        this.props.soundNotifications
      );
      this.playSound(sound);
    }
  };

  componentWillReceiveProps = nextProps => {
    if (!this.inited) {
      if (
        !isEmpty(nextProps.soundNotifications) &&
        !isEmpty(nextProps.sounds) &&
        !isEmpty(nextProps.stateCategoryViews) &&
        nextProps.subsystems.length
      )
        this.dataLoaded = true;
    }
    if (this.props.subsystems !== nextProps.subsystems || this.dataLoaded !== this.inited) {
      const stateCategoryId = this.findMaxStateCategory(nextProps);
      if (!this.prevStateCategoryId || this.prevStateCategoryId !== stateCategoryId) {
        this.prevStateCategoryId = stateCategoryId;
        const sound = this.getSound(
          stateCategoryId,
          nextProps.sounds,
          nextProps.soundNotifications
        );
        this.playSound(sound);
      }
    }
    this.inited = this.dataLoaded;
  };

  findMaxStateCategory = ({ activeProject, subsystems, stateCategoryViews }) => {
    if (activeProject && subsystems.length && activeProject.status === 'ACTIVE') {
      let maxStateCategoryId, maxPriority;
      subsystems.forEach(({ counters }) => {
        Object.keys(counters).forEach(stateCategoryId => {
          const stateCategoryView = stateCategoryViews[stateCategoryId];
          if (
            stateCategoryView &&
            (typeof maxPriority === 'undefined' ||
              stateCategoryView.stateCategory.priority < maxPriority)
          ) {
            maxStateCategoryId = stateCategoryId;
            maxPriority = stateCategoryView.stateCategory.priority;
          }
        });
      });
      return maxStateCategoryId;
    }
    return null;
  };

  getSound = (stateCategoryId, sounds, soundNotifications) => {
    const soundNotification = soundNotifications[stateCategoryId];
    if (soundNotification && soundNotification.mediaId) {
      const sound = sounds[soundNotification.mediaId];
      if (sound && sound.content) {
        return { ...sound, loop: soundNotification.unstoppable ? true : false };
      }
    }
    return null;
  };

  playSound = sound => {
    if (sound) {
      this.stopSound();
      this.audioCtx.decodeAudioData(
        base64ToBuffer(sound.content),
        buffer => {
          this.stopSound(this.invalid);
          this.audioSource = this.audioCtx.createBufferSource();
          this.audioSource.buffer = buffer;
          this.audioSource.loop = sound.loop;
          this.audioSource.connect(this.audioCtx.destination);
          if (!this.muted && !this.invalid) {
            this.audioSource.start(0);
            this.playing = true;
            if (this.audioSource.context.state === 'suspended' && this.userGestureNotRecorded) {
              notification.warning(SOUND_NOTIFICATION_MESSAGE_PARAMS);
            }
          }
        },
        () => {
          this.stopSound();
        }
      );
    } else this.stopSound(true);
  };

  stopSound = invalid => {
    if (this.audioSource && this.playing) {
      this.audioSource.stop(0);
      this.playing = false;
    }
    this.invalid = invalid;
  };

  mute = () => {
    if (this.muted) {
      this.muted = false;
      localStorage.setItem('muted', false);
      const stateCategoryId = this.findMaxStateCategory(this.props);
      const sound = this.getSound(
        stateCategoryId,
        this.props.sounds,
        this.props.soundNotifications
      );
      this.playSound(sound);
    } else {
      this.muted = true;
      this.stopSound();
      localStorage.setItem('muted', true);
    }
    this.setState({});
  };

  render = () => {
    return (
      <div style={WRAPPER_STYLES} onClick={() => this.mute()}>
        <Icon
          type="sound"
          style={{ fontSize: 22, color: this.muted ? 'lightslategray' : '#fff' }}
        />
        {this.muted ? <Icon type="close" style={MUTED_ICON_STYLES} /> : null}
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    sounds: state.medias.sounds,
    soundNotifications: state.soundNotifications,
    activeProject: state.activeProject.activeProject,
    subsystems: state.activeProject.subsystems,
    stateCategoryViews: state.stateCategoryViewsHash
  };
};

export default connect(
  mapStateToProps,
  null
)(SoundSystemNotifications);
