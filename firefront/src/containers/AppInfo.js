import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

const AppWrapper = styled.div`
  display: flex;
  align-items: center;
  color: aqua;
`;
const AppElement = styled.div`
  margin-left: 6px;
  margin-right: 20px;
`;

class AppInfo extends React.Component {
  render() {
    return (
      <AppWrapper>
        <AppElement>Версия ПО: {this.props.appInfo.version}</AppElement>
      </AppWrapper>
    )
  }
}

const mapStateToProps = state => {
  return {
    appInfo: state.dictionaries.application
  };
};

export default connect(mapStateToProps)(AppInfo);
