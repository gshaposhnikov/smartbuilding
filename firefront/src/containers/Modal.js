import React, { Component } from 'react';

import { modalClose } from 'actions/modals';
import { connect } from 'react-redux';

import Modal from 'components/Modal';
import styled from 'styled-components';

const ModalContentWrapper = styled.div`
  .hidden-field {
    display: none !important;
  }
`;

class ModalRedux extends Component {
  render() {
    const { onModalClose, children, name, title, modals, onClose, ...restProps } = this.props;
    return (
      <Modal
        onClose={() => {
          onModalClose(name);
          if (onClose) onClose();
        }}
        title={title}
        isVisible={modals[name] || false}
        {...restProps}
      >
        <ModalContentWrapper className="ant-modal-wrapper">{children}</ModalContentWrapper>
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onModalClose: name => {
      dispatch(modalClose(name));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalRedux);
