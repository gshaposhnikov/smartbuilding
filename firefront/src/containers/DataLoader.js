import { Component } from 'react';
import { connect } from 'react-redux';

import { SOCKET_CLIENT_START } from 'constants/actionTypes';

import { loadLicensePermissions } from 'actions/license';
import { getDeviceProfileViews, getDeviceProfilesDictionary } from 'actions/devices';
import { loadRegionConstants } from 'actions/regions';
import { loadIcons, loadSvgs, loadSounds } from 'actions/medias';
import { projectsLoad, loadActiveProject } from 'actions/projects';
import { getMonitorableValueProfiles } from 'actions/monitorableValues';
import { loadApplicationInfo } from 'actions/dictionaries';
import { loadDeviceShapeLibraries, loadDeviceShapes } from 'actions/deviceShapeLibrary';
import { loadPermissions } from 'actions/login';
import { loadStateCategoryViews } from 'actions/stateCategoryViews';
import { loadLogViews } from 'actions/logViews';
import { loadSoundNotifications } from 'actions/soundNotifications';

/**
 * Компонент отвечающий за подгрузку данных на старте приложения
 * (После процедуры авторизации, если включена безопасность)
 */

class DataLoader extends Component {
  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch(loadRegionConstants());
    dispatch(loadLicensePermissions());
    dispatch(getDeviceProfilesDictionary());
    dispatch(loadApplicationInfo());
    dispatch(loadPermissions());
    dispatch(loadLogViews());
    dispatch(loadSounds());
    dispatch(loadIcons());
    dispatch(loadSvgs());
    dispatch(loadSoundNotifications());
    dispatch(getDeviceProfileViews());
    dispatch(getMonitorableValueProfiles());
    dispatch(loadStateCategoryViews());
    dispatch(loadDeviceShapeLibraries());
    dispatch(loadDeviceShapes());
    dispatch(projectsLoad());
    dispatch(loadActiveProject());
    dispatch({ type: SOCKET_CLIENT_START });
  };

  shouldComponentUpdate = () => {
    return false;
  };

  render() {
    return null;
  }
}

const mapStateToProps = dispatch => ({
  dispatch
});

export default connect(mapStateToProps)(DataLoader);
