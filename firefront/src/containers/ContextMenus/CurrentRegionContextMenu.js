import React, { Component } from 'react';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

class CurrentRegionContextMenu extends Component {
  onDisconnectDeviceList = () => {
    const { onDisconnectDeviceList, item } = this.props;
    onDisconnectDeviceList(item.record);
  };
  render() {
    const { isActive } = this.props;
    return (
      <Popup id="currentRegionContext">
        <PopupItem disabled={!isActive} onClick={this.onDisconnectDeviceList}>
          Удалить из зоны
        </PopupItem>
      </Popup>
    );
  }
}

CurrentRegionContextMenu = connectMenu('currentRegionContext')(CurrentRegionContextMenu);

export default CurrentRegionContextMenu;
