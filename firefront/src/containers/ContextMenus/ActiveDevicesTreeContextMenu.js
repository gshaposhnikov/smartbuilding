import React, { Component } from 'react';

import { connect } from 'react-redux';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

import { STATE_ACTION, STATE_NAME } from 'constants_new/states';
import { setDeviceState } from 'actions_new/device';

class ActiveDevicesTreeContextMenu extends Component {
  render() {
    const { dispatch } = this.props;
    const { item } = this.props;
    const currentDevice =
      item && item.record && item.record.id && !item.record.isRoot ? item.record : {};
    let deviceState;
    let deviceAction;
    if (currentDevice.activeStateViews) {
      deviceState = currentDevice.activeStateViews[0].id;
      deviceAction = STATE_ACTION[deviceState];
    }
    return (
      <Popup id="activeDeviceContext">
        <PopupItem
          onClick={() => {
            dispatch(setDeviceState(currentDevice.projectId, currentDevice.id, deviceAction));
          }}
        >
          {STATE_NAME[deviceAction]}
        </PopupItem>
      </Popup>
    );
  }
}

ActiveDevicesTreeContextMenu = connectMenu('activeDeviceContext')(ActiveDevicesTreeContextMenu);

export default connect(null)(ActiveDevicesTreeContextMenu);
