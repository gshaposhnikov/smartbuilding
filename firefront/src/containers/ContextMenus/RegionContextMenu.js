import React, { Component } from 'react';

import { connect } from 'react-redux';

import ContextMenu from 'components/ContextMenu';
import MenuItem from 'components/ContextMenu/MenuItem';

import { deleteProject } from 'actions/projects';
import { push } from 'react-router-redux';


class CustomContextMenu extends Component{

  deleteProject = () => {
    const { dispatch, rowIdx } = this.props;
    dispatch(deleteProject(rowIdx));
  }

  openProject = () => {
    const { dispatch, rowIdx, data } = this.props;
    dispatch(push(`/projects/${data[rowIdx].id}/plan`));
  }

  render() {

    return (
      <ContextMenu>
        <MenuItem onClick={this.openProject}>Открыть проект</MenuItem>
        <MenuItem onClick={this.deleteProject}>Удалить проект</MenuItem>
      </ContextMenu>
    );
  }
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch: dispatch }
};

export default connect(null, mapDispatchToProps)(CustomContextMenu);