import React from 'react';
import { connect } from 'react-redux';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

import {
  getCurrentProjectRegionsHash,
  getCurrentProjectDevicesHash,
  getCurrentProjectDeviceTree
} from 'helpers/currentProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const ATTACHABLE_DEVICE_CATEGORY_TO_PLAN = {
  SENSOR: true,
  CONTROL: true,
  EXECUTIVE: true,
  VIRTUAL_CONTAINER: true
};

const checkConnectionIsAvailableByConnectionType = connectionType => {
  if (!connectionType || connectionType === 'NONE') return false;
  return true;
};

const ProjectDevicesTreeContextMenu = ({
  item,
  isActiveProject,
  deviceProfileViewsHash,
  regionsHash,
  devicesHash,
  deviceTree,
  // Обработчики действий
  onConnectDevice,
  onUpdateDeviceLinks,
  onDeleteDevice,
  onSyncDeviceTime,
  onAttachToPlan,
  onReadEvents,
  onResetDevice,
  onUpdateDeviceFW,
  onDragDevice,
  onSetControlDevicePassword,
  onImportControlDeviceConfig
}) => {
  const device = item && item.record ? item.record : {};
  const treeItem = item && item.treeItem ? item.treeItem : null;
  const deviceRegion = device.regionId ? regionsHash[device.regionId] || {} : {};
  const regionPlanLayouts = deviceRegion.planLayouts || [];
  const isAttachableToPlan =
    !device.isRoot && device.id && ATTACHABLE_DEVICE_CATEGORY_TO_PLAN[device.deviceCategory];
  let parentDevice = device.parentDeviceId ? devicesHash[device.parentDeviceId] : {};
  const deviceProfileView = device.deviceProfileId
    ? deviceProfileViewsHash[device.deviceProfileId]
    : null;
  if (!parentDevice.id && deviceProfileView) {
    parentDevice =
      deviceTree[0].children.find(device =>
        deviceProfileView.deviceProfile.connectionTypes.includes(device.outputConnectionType)
      ) || {};
  }
  const mayHaveConnections =
    checkConnectionIsAvailableByConnectionType(device.outputConnectionType) ||
    checkConnectionIsAvailableByConnectionType(
      deviceProfileView && deviceProfileView.deviceProfile.outputConnectionType
    );
  const isAvailableConnectDeviceToParent =
    !isActiveProject && deviceProfileView
      ? parentDevice.deviceCategory === 'CONTAINER'
        ? false
        : true
      : false;

  return (
    <Popup id="newDeviceContext">
      {/** Добавление элементов дерева */}
      <PopupItem
        disabled={isActiveProject || !mayHaveConnections}
        onClick={() => onConnectDevice(device)}
      >
        Подключить
      </PopupItem>
      <PopupItem
        disabled={!isAvailableConnectDeviceToParent}
        onClick={() => onConnectDevice(parentDevice)}
      >
        Добавить
      </PopupItem>
      <PopupItem
        disabled={isActiveProject || device.deviceCategory !== 'VIRTUAL_CONTAINER'}
        onClick={() => onUpdateDeviceLinks(device)}
      >
        Присоеденить
      </PopupItem>
      <PopupItem
        disabled={isActiveProject || device.deviceCategory !== 'TRANSPORT'}
        onClick={() => onImportControlDeviceConfig(device)}
      >
        Импорт конфигурации прибора
      </PopupItem>
      {/** Работа с планами */}
      <PopupItem
        disabled={
          isActiveProject || !isAttachableToPlan || !device.regionId || !regionPlanLayouts.length
        }
        onClick={() => onAttachToPlan(device)}
      >
        Разместить на плане
      </PopupItem>
      <PopupItem
        disabled={isActiveProject || !isAttachableToPlan}
        onClick={() => onDragDevice(device)}
      >
        Переместить на план
      </PopupItem>
      {/** Работа с прибором */}
      <PopupItem
        disabled={
          !isActiveProject || !device.id || device.deviceCategory !== 'CONTROL' || device.isRoot
        }
        onClick={() => onSyncDeviceTime(device)}
      >
        Синхронизировать время
      </PopupItem>
      <PopupItem
        disabled={
          !isActiveProject || !item || !item.record || item.record.deviceCategory !== 'CONTROL'
        }
        onClick={() => onReadEvents(item ? item.record : {})}
      >
        Прочитать журнал событий
      </PopupItem>
      <PopupItem
        disabled={
          !isActiveProject || !device.id || device.isRoot || device.deviceCategory !== 'CONTROL'
        }
        onClick={() => onResetDevice(device)}
      >
        Перезагрузить прибор
      </PopupItem>
      <PopupItem
        disabled={
          !isActiveProject || !item || !item.record || item.record.deviceCategory !== 'CONTROL'
        }
        onClick={() => onUpdateDeviceFW(item ? item.record : {})}
      >
        Обновить прошивку
      </PopupItem>
      <PopupItem
        disabled={
          !isActiveProject ||
          !device.id ||
          device.isRoot ||
          !deviceProfileView ||
          !deviceProfileView.deviceProfile.deviceUsers ||
          !deviceProfileView.deviceProfile.deviceUsers.length
        }
        onClick={() => onSetControlDevicePassword(device)}
      >
        Задать пароль
      </PopupItem>
      {/** Удаление элемента дерева */}
      <PopupItem
        disabled={isActiveProject || device.isRoot}
        onClick={() => onDeleteDevice(device, treeItem)}
      >
        Удалить
      </PopupItem>
    </Popup>
  );
};

const mapStateToProps = state => ({
  deviceTree: getCurrentProjectDeviceTree(state),
  devicesHash: getCurrentProjectDevicesHash(state),
  regionsHash: getCurrentProjectRegionsHash(state),
  deviceProfileViewsHash: getDeviceProfileViewsHash(state)
});

export default connect(mapStateToProps)(
  connectMenu('newDeviceContext')(ProjectDevicesTreeContextMenu)
);
