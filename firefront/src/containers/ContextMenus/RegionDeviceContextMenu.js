import React, { Component } from 'react';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

class RegionDeviceContextMenu extends Component {
  onConnectDeviceList = () => {
    const { onConnectDeviceList, item } = this.props;
    onConnectDeviceList(item.record);
  };
  render() {
    const { isActiveProject } = this.props;
    return (
      <Popup id="regionDeviceContext">
        <PopupItem disabled={isActiveProject} onClick={this.onConnectDeviceList}>
          Добавить в зону
        </PopupItem>
      </Popup>
    );
  }
}

RegionDeviceContextMenu = connectMenu('regionDeviceContext')(RegionDeviceContextMenu);

export default RegionDeviceContextMenu;
