import React from 'react';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

class ScenarioListContextMenu extends React.Component {
  render() {
    const { scenarioActions = [], onPerformScenarioAction, item } = this.props,
      scenario = item && item.record ? item.record : {},
      disabledScenarioActions =
        scenario.generalStateCategoryView && scenario.generalStateCategoryView.id === 'Malfunction'
          ? true
          : false;

    return (
      <Popup id="scenarioListContext">
        {scenarioActions.map(action => (
          <PopupItem
            disabled={disabledScenarioActions}
            key={action.id}
            onClick={() => {
              onPerformScenarioAction(scenario.globalNo, action.id);
            }}
          >
            {action.name}
          </PopupItem>
        ))}
      </Popup>
    );
  }
}

export default connectMenu('scenarioListContext')(ScenarioListContextMenu);
