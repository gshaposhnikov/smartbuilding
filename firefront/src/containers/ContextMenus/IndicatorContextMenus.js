import React from 'react';

import Popup from 'components/Popup';
import PopupItem from 'components/Popup/PopupItem';

import connectMenu from 'containers/Popup';

const CONTEXT_MENU_NAMES = {
  DEVICE: 'devicesContextMenu',
  REGION: 'regionsContextMenu',
  SCENARIO: 'scenariosContextMenu'
};

const DeviceContextMenu = connectMenu(CONTEXT_MENU_NAMES.DEVICE)(
  ({ onChangeIndicatorDevicesPollingState, item }) => {
    return (
      <Popup id={CONTEXT_MENU_NAMES.DEVICE}>
        <PopupItem
          onClick={() =>
            onChangeIndicatorDevicesPollingState(
              item ? item.record : {},
              item ? item.hasDisabledDevices : false,
              item ? item.entityIds : []
            )
          }
        >
          {item && item.hasDisabledDevices ? 'Включить' : 'Отключить'}
        </PopupItem>
      </Popup>
    );
  }
);

const RegionContextMenu = connectMenu(CONTEXT_MENU_NAMES.REGION)(
  ({ onChangeIndicatorRegionsGuard, onChangeIndicatorRegionsPollingState, item }) => {
    return (
      <Popup id={CONTEXT_MENU_NAMES.REGION}>
        <PopupItem
          disabled={item && !item.hasSecurityRegions ? true : false}
          onClick={() =>
            onChangeIndicatorRegionsGuard(
              item ? item.record : {},
              item ? !item.hasGuardedRegions : false,
              item ? item.entityIds : []
            )
          }
        >
          {item && item.hasGuardedRegions ? 'Снять с охраны' : 'Поставить на охрану'}
        </PopupItem>
        <PopupItem
          onClick={() =>
            onChangeIndicatorRegionsPollingState(
              item ? item.record : {},
              item ? item.hasDisabledDevices : false,
              item ? item.entityIds : []
            )
          }
        >
          {item && item.hasDisabledDevices ? 'Включить все датчики' : 'Отключить все датчики'}
        </PopupItem>
      </Popup>
    );
  }
);

const ScenarioContextMenu = connectMenu(CONTEXT_MENU_NAMES.SCENARIO)(
  ({ actions = [], item, onPerformIndicatorScenarioAction }) => {
    return (
      <Popup id={CONTEXT_MENU_NAMES.SCENARIO}>
        {actions.map(action => (
          <PopupItem
            key={action.id}
            onClick={() => onPerformIndicatorScenarioAction(item ? item.record : {}, action.id)}
          >
            {action.name}
          </PopupItem>
        ))}
      </Popup>
    );
  }
);

export { DeviceContextMenu, RegionContextMenu, ScenarioContextMenu };
