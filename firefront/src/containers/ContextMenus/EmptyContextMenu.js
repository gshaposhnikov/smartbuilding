import React, { Component } from 'react';

import { connect } from 'react-redux';

import ContextMenu from 'components/ContextMenu';

class CustomContextMenu extends Component{
  
  render() {
    return (
      <ContextMenu></ContextMenu>
    );
  }
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch: dispatch }
};

export default connect(null, mapDispatchToProps)(CustomContextMenu);