import React, { Component } from 'react';

import { connect } from 'react-redux';

import ContextMenu from 'components/ContextMenu';
import MenuItem from 'components/ContextMenu/MenuItem';

import { push } from 'react-router-redux';


class CustomContextMenu extends Component{

  openProject = () => {
    const { dispatch, rowIdx, data } = this.props;
    dispatch(push(`/projects/${data[rowIdx].id}/plan`));
  }
 
  render() {

    return (
      <ContextMenu>
        <MenuItem onClick={this.openProject}>Открыть проект</MenuItem>
      </ContextMenu>
    );
  }
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch: dispatch }
};

export default connect(null, mapDispatchToProps)(CustomContextMenu);