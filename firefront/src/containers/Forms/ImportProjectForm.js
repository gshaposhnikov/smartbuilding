import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Input, Button, Icon, Upload } from 'antd';
const isJson = require('is-json');
const isXml = require('is-xml');

import Modal from 'containers/Modal';

import { initializeValues } from 'helpers/form';
import { getStringByteLength } from 'helpers/strings';

const MAX_FILE_SIZE = 52428800,
  FormItem = Form.Item,
  fileErrorMessages = {
    content: 'Cтруктура файла не соответствует формату',
    size: 'Размер файла не должен превышать 50 мб',
    format: 'Файл должен иметь расширение JSON или FSC',
    empty: 'Выберите файл'
  };

class ImportProjectForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object, // Генерируется Form.create
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = { fileList: [] };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
      this.setState({
        fileList: [],
        isValidFile: null
      });
    });
  };

  handleFileUpload(event) {
    const { file } = event;
    if (file.size > MAX_FILE_SIZE)
      // Проверка размер файла
      this.setFileData(false, 'size', []);
    else if (file.name.includes('.FSc') || file.name.includes('.json')) {
      // Проверка расширения
      const reader = new FileReader();
      reader.onload = data => {
        if (isJson(data.currentTarget.result))
          // Проверка структуры файла
          this.setFileData(
            true,
            '',
            [
              {
                uid: 1,
                name: file.name,
                status: 'done'
              }
            ],
            data.currentTarget.result,
            file.name,
            'NT' 
          );
          else if (isXml(data.currentTarget.result))
          // Проверка структуры файла
          this.setFileData(
            true,
            '',
            [
              {
                uid: 1,
                name: file.name,
                status: 'done'
              }
            ],
            data.currentTarget.result,
            file.name,
            'NG' 
          );
        else this.setFileData(false, 'content', [], '', file.name, file.type);
      };
      reader.readAsText(file, 'UTF-8');
    } else this.setFileData(false, 'format', [], '', file.name, file.type);
  }

  setFileData(isValid, err, fileList, content, nameOfFile, fireSecVersion) {
    const { form } = this.props;
    form.setFieldsValue({
      fileContent: {
        importedFile: content,
        version: fireSecVersion,
        fileName: nameOfFile
      }
    });
    this.setState({
      isValidFile: isValid,
      validFileMessage: fileErrorMessages[err] || null,
      fileList
    });
    form.validateFields(['fileContent'], { force: true });
  }

  handleFileValidate(rule, value, callback) {
    const { form } = this.props;
    const { isValidFile, validFileMessage } = this.state;
    const file = form.getFieldValue('fileContent');
    // Проверка валидности при загрузке
    if (isValidFile === false) callback([new Error(validFileMessage)]);
    else if (!file)
      // Проверка на пустоту
      callback([new Error(fileErrorMessages['empty'])]);
    else if (getStringByteLength(file) > MAX_FILE_SIZE)
      // Проверка размера файла
      callback([new Error(fileErrorMessages['size'])]);
    // Если нет ошибок, то возврщаем пустой ответ
    else callback();
  }

  render() {
    const { modalName, form } = this.props;
    const { fileList } = this.state;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 14 }
    };
    return (
      <Modal name={modalName} title={'Загрузить проект'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            label="Файл"
            {...formItemLayout}
            help={form.getFieldError('fileContent')}
            validateStatus={form.getFieldError('fileContent') ? 'error' : null}
          >
            {getFieldDecorator('fileContent', {
              rules: [
                { required: true, message: ' ' },
                { validator: this.handleFileValidate.bind(this), trigger: 'onChange' }
              ]
            })(<Input style={{ display: 'none' }} />)}
            <Upload
              customRequest={this.handleFileUpload.bind(this)}
              onRemove={() => this.setFileData(false, [], 'empty', '')}
              accept=".json, .FSc"
              id="uploader"
              fileList={fileList}
              multiple={false}
            >
              <Button>
                <Icon type="upload" /> {'Выберите файл'}
              </Button>
            </Upload>
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={false}>
              {'Загрузить'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

ImportProjectForm = Form.create({
  mapPropsToFields(props) {
    return initializeValues(props);
  }
})(ImportProjectForm);

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};

ImportProjectForm = connect(mapStateToProps)(ImportProjectForm);

export default ImportProjectForm;
