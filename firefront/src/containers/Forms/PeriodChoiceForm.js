import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { Form, Button } from 'antd';

import Modal from 'containers/Modal';
import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';
import RadioGroup from 'components/Radio/RadioGroup';
import Radio from 'components/Radio/Radio';
import DatePicker from 'components/DateTimePicker/DatePicker';
import TimePicker from 'components/DateTimePicker/TimePicker';

class PeriodChoiceForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object,
    modals: PropTypes.object,
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const { modalName, form, customValues, ranges } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const periodValue = getFieldValue('period') || customValues.period;
    const now = new Date();
    const yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
    return (
      <Modal name={modalName} title={'Выбор временного диапазона'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithoutLabel} required={true}>
            {getFieldDecorator('period', {
              rules: [{ required: true, message: 'Выберите диапазон' }],
              initialValue: customValues.period ? customValues.period : 'day'
            })(
              <RadioGroup>
                {Object.entries(ranges).map(range => (
                  <Radio key={range[0]} value={range[0]} title={range[1].title} />
                ))}
              </RadioGroup>
            )}
          </FormItem>
          {periodValue === 'any' ? (
            <div>
              <div style={{ display: 'flex' }}>
                <FormItem style={{ flex: 1 }} {...ItemLayoutWithLabel} label="От">
                  {getFieldDecorator('dateFrom', {
                    rules: [{ required: true, message: 'Выберите дату начала' }],
                    initialValue: customValues.dateFrom
                      ? moment(customValues.dateFrom, 'MM.DD.YYYY')
                      : moment(yesterday, 'MM.DD.YYYY')
                  })(<DatePicker allowClear={false} />)}
                </FormItem>
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('timeFrom', {
                    rules: [{ required: true, message: 'Выберите время начала' }],
                    initialValue: customValues.timeFrom
                      ? moment(customValues.timeFrom, 'HH:MM:ss')
                      : moment(yesterday, 'HH:MM:ss')
                  })(<TimePicker allowEmpty={false} />)}
                </FormItem>
              </div>
              <div style={{ display: 'flex' }}>
                <FormItem style={{ flex: 1 }} {...ItemLayoutWithLabel} label="До">
                  {getFieldDecorator('dateTo', {
                    rules: [{ required: true, message: 'Выберите дату окончания' }],
                    initialValue: customValues.dateTo
                      ? moment(customValues.dateTo, 'MM.DD.YYYY')
                      : moment(now, 'MM.DD.YYYY')
                  })(<DatePicker allowClear={false} />)}
                </FormItem>
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('timeTo', {
                    rules: [{ required: true, message: 'Выберите время окончания' }],
                    initialValue: customValues.timeTo
                      ? moment(customValues.timeTo, 'HH:MM:ss')
                      : moment(now, 'HH:MM:ss')
                  })(<TimePicker allowEmpty={false} />)}
                </FormItem>
              </div>
            </div>
          ) : null}
          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit">
              {'Готово'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

PeriodChoiceForm = FormCreate({
  mapPropsToFields(props) {
    return null;
  }
})(PeriodChoiceForm);

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};

PeriodChoiceForm = connect(mapStateToProps, null)(PeriodChoiceForm);

export default PeriodChoiceForm;
