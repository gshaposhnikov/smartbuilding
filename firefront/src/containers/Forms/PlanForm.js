import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import { Form, Input, Button, InputNumber } from 'antd';
import SubsystemSelect from 'containers/Selects/SubsystemSelect';
import PlanGroupsSelect from 'containers/Selects/PlanGroupsSelect';
import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import { initializeValues } from 'helpers/form';
import { getCurrentProjectPlanGroups, getCurrentProjectPlans } from 'helpers/currentProject';

class planForm extends Component {
  static propTypes = {
    plans: PropTypes.array,
    planGroups: PropTypes.array,
    modalName: PropTypes.string,
    isEdit: PropTypes.bool,
    form: PropTypes.object, // Генерируется Form.create
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const { modalName, form, isEdit, planGroups, plans, currentPlanGroupId } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal name={modalName} title={isEdit ? 'Редактирование плана' : 'Новый план'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Название">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Имя обязательно' }],
              initialValue: `План ${plans ? plans.length + 1 : 1}`
            })(<Input maxLength={20} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} required={true} label="Подсистема">
            {getFieldDecorator('subsystem', {
              rules: [{ required: true, message: 'Выберите подсистему' }],
              initialValue: 'GENERAL'
            })(
              <SubsystemSelect>
                <Option value="GENERAL">Общая</Option>
              </SubsystemSelect>
            )}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Группа планов">
            {getFieldDecorator('planGroupId', {
              initialValue: currentPlanGroupId === 'allPlans' ? null : currentPlanGroupId
            })(<PlanGroupsSelect groups={planGroups} onChange={this.handleChange} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Ширина">
            {getFieldDecorator('xSize', {
              rules: [{ required: true, message: 'Ширина обязательна' }],
              initialValue: 800
            })(<InputNumber precision={0} min={100} max={5000} step={10} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Высота">
            {getFieldDecorator('ySize', {
              rules: [{ required: true, message: 'Высота обязательна' }],
              initialValue: 600
            })(<InputNumber precision={0} min={100} max={5000} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Описание">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} maxLength={256} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit">
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

planForm = FormCreate({
  mapPropsToFields(props) {
    return props.isEdit ? initializeValues(props.currentPlan) : null;
  }
})(planForm);

const mapStateToProps = state => {
  return {
    modals: state.modals,
    planGroups: getCurrentProjectPlanGroups(state),
    plans: getCurrentProjectPlans(state)
  };
};

planForm = connect(mapStateToProps, null)(planForm);

export default planForm;
