import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'antd';

import { getCurrentProjectRegions } from 'helpers/currentProject';

import Modal from 'containers/Modal';
import RegionsSelect from 'containers/Selects/RegionsSelect';

import { FormItem, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

const REGION_SELECT_STYLES = {
  width: 150,
  maxWidth: 150
};

class UpdateDeviceRegionForm extends React.Component {
  static propTypes = {
    regions: PropTypes.array,
    modalName: PropTypes.string,
    onSubmit: PropTypes.func
  };

  state = {
    selectedRegionId: ''
  };

  resetSelectedRegion = () => {
    this.setState({ selectedRegionId: '' });
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { selectedRegionId } = this.state;
    onSubmit(selectedRegionId);
    this.resetSelectedRegion();
  };

  render = () => {
    const { modalName, regions, regionInfo, isEdit } = this.props;
    const { selectedRegionId } = this.state;
    return (
      <Modal
        name={modalName}
        title={isEdit ? 'Редактирование помещения' : 'Создание помещения'}
        onClose={this.resetSelectedRegion}
      >
        <FormItem {...ItemLayoutWithLabel} label="Выберите зону">
          <RegionsSelect
            value={isEdit ? selectedRegionId || regionInfo.regionId : selectedRegionId}
            style={REGION_SELECT_STYLES}
            regions={regions}
            onChange={regionId => this.setState({ selectedRegionId: regionId })}
          />
        </FormItem>
        <FormItem {...ItemLayoutWithoutLabel}>
          <Button
            type="primary"
            disabled={
              !selectedRegionId ||
              (isEdit && selectedRegionId && selectedRegionId === regionInfo.regionId)
            }
            onClick={this.handleSubmit}
          >
            {isEdit ? 'Изменить' : 'Создать'}
          </Button>
        </FormItem>
      </Modal>
    );
  };
}

const mapStateToProps = state => ({
  regions: getCurrentProjectRegions(state)
});

export default connect(mapStateToProps)(UpdateDeviceRegionForm);
