import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import Form, { FormItem, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';
import Input from 'components/Form/Input';
import InputNumber from 'components/Form/InputNumber';
import Select from 'containers/Selects';
import DeviceSelect from 'containers/Selects/DeviceSelect';

import { Button } from 'antd';
import { Field, reduxForm, initialize, reset } from 'redux-form';
import { getCurrentProjectVirtualStates, getCurrentProjectDeviceList } from 'helpers/currentProject';

const FORM_NAME = 'scenarioTraceBlock';

/**
 * Сброс значений формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};

/**
 * Модальная форма редактирования блока времени сценария.
 **/
class ScenarioTraceBlockForm extends Component {
  static propTypes = {
    /* Из компонента верхнего уровня */

    modalName: PropTypes.string,
    handleSubmit: PropTypes.func,
    /* Флаг редактирования существующего блока */
    isEdit: PropTypes.bool,

    /* Из redux-store */

    blockTypes: PropTypes.array,
    tracingEntityTypes: PropTypes.array,
    scenarioDevices: PropTypes.array,
    virtualStates: PropTypes.array,
    dispatch: PropTypes.func,

    /* Из redux-form через redux-store */

    /* Текущие занчения действий из redux-form  */
    currentValues: PropTypes.object,
    /* Объект для управления полями - массивами */
    array: PropTypes.object,
    /* Валидация значений */
    valid: PropTypes.bool,
    /* Признак что есть изменения */
    dirty: PropTypes.bool
  };

  render() {
    const {
      modalName,
      handleSubmit,
      isEdit,
      blockTypes,
      tracingEntityTypes,
      scenarioDevices,
      virtualStates,
      currentValues,
      valid,
      dirty,
      dispatch
    } = this.props;

    return (
      <Modal name={modalName} title="Блок слежения сценария">
        <Form onSubmit={handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Блок">
            <Field component={Select} name="blockType" itemsSource={blockTypes} disabled />
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Название блока">
            <Field component={Input} name="name" />
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Задержка, в секундах ">
            <Field component={InputNumber} name="tracingStartSec" min={0} step={10} />
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Тип" required={true}>
            <Field
              component={Select}
              name="tracingEntityType"
              itemsSource={tracingEntityTypes}
              onChange={() =>
                resetFormValues(dispatch, {
                  ...currentValues,
                  tracingEntityId: null
                })
              }
            />
          </FormItem>
          {currentValues.tracingEntityType === 'SENSOR_DEVICE' && (
            <FormItem {...ItemLayoutWithLabel} label="Устройство" required={true}>
              <Field component={DeviceSelect} name="tracingEntityId" devices={scenarioDevices} />
            </FormItem>
          )}
          {currentValues.tracingEntityType === 'VIRTUAL_STATE' && (
            <FormItem {...ItemLayoutWithLabel} label="Вирт. состояние" required={true}>
              <Field component={Select} name="tracingEntityId" itemsSource={virtualStates} />
            </FormItem>
          )}
          <FormItem {...ItemLayoutWithLabel} label="Период, в секундах ">
            <Field component={InputNumber} name="tracingPeriodSec" min={0} step={10} />
          </FormItem>

          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit" disabled={!valid || (isEdit && !dirty)}>
              {isEdit ? 'Изменить' : 'Добавить'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

const validate = values => {
  const errors = {};
  if (values) {
    if (!values.blockType) {
      errors.blockType = 'Не указан тип блока';
    }
    if (!values.tracingEntityType) {
      errors.tracingEntityType = 'Не выбран тип объекта слежения';
    }
    if (!values.tracingEntityId) {
      errors.tracingEntityId = 'Не выбран объект';
    }
    if (!values.tracingPeriodSec) {
      errors.tracingPeriodSec = 'Не указан период слежения';
    }
  }
  return errors;
};

ScenarioTraceBlockForm = reduxForm({
  form: FORM_NAME,
  validate
})(ScenarioTraceBlockForm);

const mapStateToProps = state => {
  return {
    /* Справочники */
    blockTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.timeLineBlockTypes
      : [],
    tracingEntityTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.tracingEntityTypes
      : [],

    /* Сущности */
    scenarioDevices: getCurrentProjectDeviceList(state).filter(
      device => device.acceptableAsScenarioCondition && !device.disabled
    ),
    virtualStates: getCurrentProjectVirtualStates(state),

    /* Текущие значения полей */
    currentValues: state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioTraceBlockForm);
