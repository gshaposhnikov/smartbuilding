import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import Form, { FormItem } from 'components/Form';

import Input from 'components/Form/Input';
import Select from 'containers/Selects';
import Checkbox from 'components/Form/Checkbox';
import DeviceSelect from 'containers/Selects/DeviceSelect';

import { Button } from 'antd';
import { Field, reduxForm, formValueSelector, initialize, reset } from 'redux-form';

export const FORM_NAME = 'scenarioBasicParams';
const required = value => (value ? undefined : 'Required');

/**
 * Модальная форма редактирования базовых параметров сценария.
 *
 * Параметры, которые должны передаваться от родительского компонента:
 * @prop modalName - глобальное название модального окна
 * @prop handleSubmit - обработчик кнопки ОК
 * @prop isEdit - создание или редактирование
 * @prop scenarioPurposes - список вариантов назначения сценариев
 * @prop scenarioTypes - список возможных типов сценариев
 * @prop controlDevices - список приборов (необходим для варианта назначения EXEC_BY_INVOKE)
 **/
class ScenarioBasicParamsForm extends Component {
  render() {
    const {
      modalName,
      handleSubmit,
      isEdit,
      scenarioPurposes,
      scenarioTypes,
      controlDevices,
      scenarioPurposeValue,
      dirty,
      valid
    } = this.props;
    return (
      <Modal name={modalName} title={isEdit ? 'Параметры сценария' : 'Параметры нового сценария'}>
        <Form onSubmit={handleSubmit}>
          <FormItem required label="Название сценария (передаваемое в прибор)">
            <Field required validate={[required]} component={Input} name="name" />
          </FormItem>

          <FormItem required={true} label="Назначение сценария">
            <Field
              required
              component={Select}
              validate={[required]}
              name="scenarioPurpose"
              itemsSource={scenarioPurposes}
              disabled={isEdit}
            />
          </FormItem>

          <FormItem>
            <Field component={Checkbox} name="enabled" label="Включен" />
          </FormItem>

          <FormItem required label="Тип сценария">
            <Field
              component={Select}
              name="scenarioType"
              validate={[required]}
              required
              itemsSource={scenarioTypes}
            />
          </FormItem>

          {scenarioPurposeValue === 'EXEC_BY_INVOKE' ? (
            <FormItem required label="Прибор">
              <Field
                component={DeviceSelect}
                required
                validate={[required]}
                name="controlDeviceId"
                devices={controlDevices}
              />
            </FormItem>
          ) : null}

          <FormItem label="Описание (примечание) сценария">
            <Field component={Input} type="textarea" rows={4} name="description" />
          </FormItem>

          <FormItem>
            <Button type="primary" htmlType="submit" disabled={isEdit ? !dirty || !valid : !valid}>
              {isEdit ? 'Изменить' : 'Добавить'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

ScenarioBasicParamsForm = reduxForm({
  form: FORM_NAME
})(ScenarioBasicParamsForm);

const mapStateToProps = state => {
  return {
    scenarioPurposeValue: formValueSelector(FORM_NAME)(state, 'scenarioPurpose')
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioBasicParamsForm);

/**
 * Сброс значение формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};
