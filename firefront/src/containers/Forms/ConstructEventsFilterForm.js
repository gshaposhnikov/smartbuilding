import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Row } from 'antd';
import { Field, FieldArray, reduxForm, initialize, reset } from 'redux-form';
import moment from 'moment';

import { loadEventTypeViews } from 'actions/events';
import { loadActiveRegions } from 'actions/regions';
import { loadScenarios } from 'actions/scenarios';
import { loadVirtualStates } from 'actions/virtualStates';
import { loadUsers } from 'actions/users';

import Text from 'components/Text';
import Form, { FormItem, FormInput } from 'components/Form';
import Table, { Column } from 'components/Table';
import DatePicker from 'components/Form/DatePicker';
import MultiSearchSelect from 'components/Form/MultiSearchSelect';

import Modal from 'containers/Modal';
import Select from 'containers/Selects';
import DeviceSelect from 'containers/Selects/DeviceSelect';

import { findDevicesByDeviceCategory } from 'helpers/device';
import {
  getActiveDeviceList,
  getActiveRegions,
  getActiveScenarios,
  getActiveVirtualStates
} from 'helpers/activeProject';
import { getActiveEmployeesHash } from 'helpers/skud';

const FORM_NAME = 'constuctEventsFilter';

/**
 * Сброс значений формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};

const filters = [
  {
    id: 'occurredAfter',
    name: 'Минимальное время прибора'
  },
  {
    id: 'occurredBefore',
    name: 'Максимальное время прибора'
  },
  {
    id: 'receivedAfter',
    name: 'Минимальное системное время'
  },
  {
    id: 'receivedBefore',
    name: 'Максимальное системное время'
  },
  {
    id: 'name',
    name: 'Описание'
  },
  {
    id: 'subsystems',
    name: 'Подсистема'
  },
  {
    id: 'eventTypeIds',
    name: 'Тип события'
  },
  {
    id: 'controlDeviceIds',
    name: 'Прибор'
  },
  {
    id: 'deviceIds',
    name: 'Адресное устройство'
  },
  {
    id: 'regionIds',
    name: 'Зона'
  },
  {
    id: 'virtualStateIds',
    name: 'Виртуальное состояние'
  },
  {
    id: 'scenarioIds',
    name: 'Сценарий'
  },
  {
    id: 'userIds',
    name: 'Пользователь'
  },
  {
    id: 'employeeIds',
    name: 'Сотрудник (СКУД)'
  },
  {
    id: 'logTypeIds',
    name: 'Журнал'
  }
];

const subsystems = [
  {
    id: 'GENERAL',
    name: 'Общая подсистема'
  },
  {
    id: 'FIRE',
    name: 'Пожарная подсистема'
  },
  {
    id: 'SECURITY',
    name: 'Охранная подсистема'
  },
  {
    id: 'SKUD',
    name: 'СКУД'
  },
  {
    id: 'AUTO',
    name: 'Автоматика'
  }
];

const filtersTable = ({
  fields,
  onDeleteRowClick,
  filterValues,
  onIdChange,
  eventTypeViews,
  controlDevices,
  addressDevices,
  logTypesMap,
  regions,
  virtualStates,
  scenarios,
  users,
  employees
}) => {
  let logTypes = [];
  for (let logTypesMapItem of Object.values(logTypesMap)) {
    logTypes = [...logTypes, ...logTypesMapItem];
  }

  return (
    <div>
      <Table
        dataSource={[...fields.getAll(), { id: '' }]}
        tableStyle={{ minHeight: '200px' }}
        rowKey="id"
      >
        <Column
          title="Поле"
          dataIndex="id"
          key="id"
          width={'18%'}
          render={(id, record, index) => (
            <Field
              component={Select}
              name={`filters[${index}].id`}
              itemsSource={filters.filter(
                filter =>
                  !filterValues.find(
                    (filterValue, fvIndex) => fvIndex !== index && filterValue.id === filter.id
                  )
              )}
              dropdownMatchSelectWidth={false}
              onChange={(event, newValue) => onIdChange(index, newValue)}
            />
          )}
        />
        <Column
          title="Значение"
          dataIndex="value"
          key="value"
          render={(value, record, index) => {
            if (filterValues[index]) {
              switch (filterValues[index].id) {
                case 'receivedAfter':
                case 'receivedBefore':
                case 'occurredAfter':
                case 'occurredBefore': {
                  return <Field component={DatePicker} name={`filters[${index}].value`} />;
                }
                case 'name': {
                  return <Field component={FormInput} name={`filters[${index}].value`} />;
                }
                case 'subsystems': {
                  return (
                    <Field
                      component={MultiSearchSelect}
                      name={`filters[${index}].value`}
                      itemsSource={subsystems}
                      mode="multiple"
                    />
                  );
                }
                case 'eventTypeIds': {
                  return (
                    <Field
                      component={MultiSearchSelect}
                      name={`filters[${index}].value`}
                      itemsSource={eventTypeViews}
                      allowClear
                      dropdownMatchSelectWidth={false}
                    />
                  );
                }
                case 'controlDeviceIds': {
                  return (
                    <Field
                      component={DeviceSelect}
                      name={`filters[${index}].value`}
                      devices={controlDevices}
                      mode="multiple"
                    />
                  );
                }
                case 'deviceIds': {
                  return (
                    <Field
                      component={DeviceSelect}
                      name={`filters[${index}].value`}
                      devices={addressDevices}
                      mode="multiple"
                    />
                  );
                }
                case 'regionIds': {
                  return (
                    <Field
                      component={Select}
                      name={`filters[${index}].value`}
                      itemsSource={regions}
                      mode="multiple"
                    />
                  );
                }
                case 'virtualStateIds': {
                  return (
                    <Field
                      component={Select}
                      name={`filters[${index}].value`}
                      itemsSource={virtualStates}
                      mode="multiple"
                    />
                  );
                }
                case 'scenarioIds': {
                  return (
                    <Field
                      component={Select}
                      name={`filters[${index}].value`}
                      itemsSource={scenarios}
                      mode="multiple"
                    />
                  );
                }
                case 'userIds': {
                  return (
                    <Field
                      component={Select}
                      name={`filters[${index}].value`}
                      itemsSource={users}
                      mode="multiple"
                    />
                  );
                }
                case 'employeeIds': {
                  return (
                    <Field
                      component={Select}
                      name={`filters[${index}].value`}
                      itemsSource={employees}
                      mode="multiple"
                    />
                  );
                }
                case 'logTypeIds': {
                  return (
                    <Field
                      component={MultiSearchSelect}
                      name={`filters[${index}].value`}
                      itemsSource={Object.values(logTypes)}
                      mode="multiple"
                    />
                  );
                }
                default: {
                  return <div />;
                }
              }
            } else {
              return <div />;
            }
          }}
        />
        <Column
          key="deleteButton"
          width={'50px'}
          render={(text, record, index) =>
            filterValues[index] ? (
              <Button icon="delete" onClick={() => onDeleteRowClick(index)} />
            ) : (
              <div />
            )
          }
        />
      </Table>
    </div>
  );
};

/* TODO: реализовать фильтрацию по пользователям и сценариям */
class ConstructEventsFilterForm extends Component {
  static propTypes = {
    /* Из компонента верхнего уровня */

    modalName: PropTypes.string,
    handleSubmit: PropTypes.func,

    /* Из redux-store */

    /* Отображения типов событий */
    eventTypeViews: PropTypes.array,
    /* Ошибка получения отображений типов событий от сервера */
    loadEventTypeViewsError: PropTypes.string,
    /* Приборы */
    controlDevices: PropTypes.array,
    /* Адресные устройства */
    addressDevices: PropTypes.array,
    /* Зоны */
    regions: PropTypes.array,
    /* Ошибка получения зон активного проекта */
    loadActiveRegionsError: PropTypes.string,
    /* Сущность активного проекта */
    project: PropTypes.object,
    /* Виртуальные состояния активного проекта */
    virtualStates: PropTypes.array,
    /* Ошибка получения виртуальных состояний проекта */
    loadVirtualStatesError: PropTypes.string,
    /* Сценарии активного проекта */
    scenarios: PropTypes.array,
    /* Пользователи */
    users: PropTypes.object,
    /* Ошибка получения списка пользователей */
    loadUsersError: PropTypes.string,

    /* Из redux-form через redux-store */

    /* Текущие занчения действий из redux-form  */
    currentValues: PropTypes.object,
    /* Объект для управления полями - массивами */
    array: PropTypes.object,
    /* Валидация значений */
    valid: PropTypes.bool,
    /* Признак что есть изменения */
    dirty: PropTypes.bool,
    dispatch: PropTypes.func
  };

  componentDidMount() {
    const {
      dispatch,
      eventTypeViews,
      loadEventTypeViewsError,
      regions,
      loadActiveRegionsError,
      project,
      virtualStates,
      loadVirtualStatesError,
      scenarios,
      loadScenariosError,
      users,
      loadUsersError
    } = this.props;

    if (!eventTypeViews && !loadEventTypeViewsError) {
      dispatch(loadEventTypeViews());
    }
    if (!regions && !loadActiveRegionsError && project && project.id) {
      dispatch(loadActiveRegions(project.id));
    }
    if (!virtualStates && !loadVirtualStatesError && project && project.id) {
      dispatch(loadVirtualStates(project.id));
    }
    if (!scenarios && !loadScenariosError && project && project.id) {
      dispatch(loadScenarios(project.id));
    }
    if (!users && !loadUsersError) {
      dispatch(loadUsers());
    }
  }

  componentWillReceiveProps(newProps) {
    const {
      dispatch,
      regions,
      loadActiveRegionsError,
      project,
      virtualStates,
      loadVirtualStatesError,
      scenarios,
      loadScenariosError
    } = this.props;

    if (!regions && !loadActiveRegionsError && project && project.id) {
      dispatch(loadActiveRegions(project.id));
    }
    if (!virtualStates && !loadVirtualStatesError && project && project.id) {
      dispatch(loadVirtualStates(project.id));
    }
    if (!scenarios && !loadScenariosError && project && project.id) {
      dispatch(loadScenarios(project.id));
    }
  }

  render() {
    const {
      modalName,
      handleSubmit,
      eventTypeViews,
      controlDevices,
      addressDevices,
      deviceProfileLogTypes,
      regions,
      virtualStates,
      scenarios,
      users,
      employees,
      currentValues,
      array,
      valid,
      dirty
    } = this.props;
    return (
      <Modal name={modalName} title="Конструктор фильтрации" width="50%">
        <Form onSubmit={handleSubmit}>
          <Field name="validator" component={Text} style={{ display: 'none' }} />
          <Row>
            <FieldArray
              name="filters"
              onDeleteRowClick={index => {
                array.remove('filters', index);
              }}
              component={filtersTable}
              filterValues={currentValues.filters}
              onIdChange={(index, newValue) => {
                array.splice('filters', index, 1, { id: newValue });
              }}
              eventTypeViews={eventTypeViews}
              controlDevices={controlDevices}
              addressDevices={addressDevices}
              logTypesMap={deviceProfileLogTypes}
              regions={regions}
              virtualStates={virtualStates}
              scenarios={scenarios}
              employees={employees}
              users={users ? Object.values(users) : []}
            />
          </Row>
          <Row>
            <FormItem>
              <Button type="primary" htmlType="submit" disabled={!valid || !dirty}>
                Применить
              </Button>
            </FormItem>
          </Row>
        </Form>
      </Modal>
    );
  }
}

const validate = values => {
  const errors = {};
  if (values && values.filters) {
    values.filters.forEach(filter => {
      if (
        !filter.value ||
        (moment.isMoment(filter.value) && !filter.value.isValid()) ||
        (Array.isArray(filter.value) && filter.value.length === 0)
      ) {
        errors.validator = 'Не указано значение';
      }
    });
  }
  return errors;
};

ConstructEventsFilterForm = reduxForm({
  form: FORM_NAME,
  validate
})(ConstructEventsFilterForm);

const mapStateToProps = state => {
  const activeDevices = getActiveDeviceList(state);
  const activeEmployeesHash = getActiveEmployeesHash(state);
  return {
    eventTypeViews: state.eventTypeViews,
    loadEventTypeViewsError: state.errors.loadEventTypeViews,
    controlDevices: findDevicesByDeviceCategory(state, 'CONTROL', activeDevices),
    addressDevices: [
      ...findDevicesByDeviceCategory(state, 'SENSOR', activeDevices),
      ...findDevicesByDeviceCategory(state, 'EXECUTIVE', activeDevices),
      ...findDevicesByDeviceCategory(state, 'TRANSPORT', activeDevices),
      ...findDevicesByDeviceCategory(state, 'SKUD', activeDevices)
    ],
    deviceProfileLogTypes: state.deviceProfileLogTypes,
    regions: getActiveRegions(state),
    loadActiveRegionsError: state.errors.loadActiveRegions,
    project: state.activeProject.project,
    virtualStates: getActiveVirtualStates(state),
    loadVirtualStatesError: state.errors.loadVirtualStates,
    scenarios: getActiveScenarios(state),
    loadScenariosError: state.errors.loadScenariosError,
    users: state.admin.users,
    employees: activeEmployeesHash
      ? Object.keys(activeEmployeesHash).map(key => activeEmployeesHash[key])
      : [],
    loadUsersError: state.errors.loadUsersError,
    /* Текущие значения полей */
    currentValues: state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {}
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConstructEventsFilterForm);
