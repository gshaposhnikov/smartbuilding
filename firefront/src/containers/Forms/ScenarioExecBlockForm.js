import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Row, Col, Tabs, Select as CustomSelect, Icon, Tooltip } from 'antd';
import { Field, reduxForm, initialize, reset, change } from 'redux-form';
import { AutoSizer } from 'react-virtualized';
import styled from 'styled-components';
const TabPane = Tabs.TabPane;

import Form, { FormItem, ItemLayoutWithLabel } from 'components/Form';
import ExpandableTable from 'components/ExpandableTable/Table';
import Input from 'components/Form/Input';
import InputNumber from 'components/Form/InputNumber';
import ScenarioList from 'components/VirtualizedScenarioList';
import VirtualStateList from 'components/VirtualizedVirtualStateList';
import RegionList from 'components/RegionList';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import ExecBlockActionList from 'components/ExecBlockActionList';

import Modal from 'containers/Modal';
import Select from 'containers/Selects';
import DeviceSelect from 'containers/Selects/DeviceSelect';

import {
  getCurrentProjectRegions,
  getCurrentProjectVirtualStates,
  getCurrentProjectDeviceList,
  getCurrentProjectDevicesHash,
  getCurrentProjectRegionsHash,
  getCurrentProjectScenariosHash,
  getCurrentProjectVirtualStateHash
} from 'helpers/currentProject';
import {
  getDevicesTree,
  filterDevicesLeavingParents,
  getFlatTree,
  getDeviceProfile
} from 'helpers/device';
import { getExecutiveScenarios } from 'helpers/scenarios';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  border: 1px solid lightgray;
  width: 100%;
  min-height: 315px;
`;
const TabsWrapper = styled.div`
  flex: 1;
  .ReactVirtualized__Table__headerRow {
    background-color: lightgray;
    font-weight: normal;
    font-size: 11px !important;
  }
  .ReactVirtualized__Table__headerColumn {
    display: flex;
    justify-content: center;
  }
  .ReactVirtualized__Table__Grid {
    background-color: white;
  }
  .expandable__table__header {
    background-color: lightgray;
  }
  .ReactVirtualized__Table__row {
    border-bottom: 1px solid lightgray;
  }
  .ReactVirtualized__Table__rowColumn {
    padding: 4px;
    line-height: 26px;
  }
  .ReactVirtualized__Table__row {
    margin: 0 !important;
    & > .ReactVirtualized__Table__rowColumn:not(:first-child) {
      border-left: 1px solid lightgray;
    }
  }
`;

const BUTTON_GROUP_STYLES = {
  display: 'flex',
  flexDirection: 'column',
  height: 315,
  justifyContent: 'center',
  alignItems: 'center'
};
const BUTTON_WRAPPER_STYLES = {
  margin: '10px 0'
};
const ICON_STYLES = { marginRight: '5px' };
const SELECT_STYLES = { width: 150 };
const ACTIONS_MENU_WRAPPER_STYLES = {
  flex: 1,
  height: 36,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center'
};
const FORM_NAME = 'scenarioExecBlock';
const SELECTED_IDS_LIST_NAMES = {
  DEVICE: 'selectedDevices',
  REGION: 'selectedRegions',
  SCENARIO: 'selectedScenarios',
  VIRTUAL_STATE: 'selectedVirtualStates'
};
/**
 * Сброс значений формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};

/**
 * Модальная форма редактирования блока времени сценария.
 **/
class ScenarioExecBlockForm extends Component {
  static propTypes = {
    /* Из компонента верхнего уровня */
    modalName: PropTypes.string,
    handleSubmit: PropTypes.func,
    /* Флаг редактирования существующего блока */
    isEdit: PropTypes.bool,
    /* Из redux-store */
    blockTypes: PropTypes.array,
    actionEntityTypes: PropTypes.array,
    actionTypes: PropTypes.array,
    conditionTypes: PropTypes.array,
    conditionCheckTypes: PropTypes.array,
    scenarioDevices: PropTypes.array,
    securityRegions: PropTypes.array,
    executiveScenarios: PropTypes.array,
    virtualStates: PropTypes.array,
    dispatch: PropTypes.func,
    /* Из redux-form через redux-store */
    /* Текущие занчения действий из redux-form  */
    currentValues: PropTypes.object,
    /* Объект для управления полями - массивами */
    array: PropTypes.object,
    /* Валидация значений */
    valid: PropTypes.bool,
    /* Признак что есть изменения */
    dirty: PropTypes.bool
  };
  state = {
    deviceTree: [],
    selectedDevices: [],
    selectedScenarios: [],
    selectedRegions: [],
    selectedVirtualStates: [],
    selectedActions: [],
    activeTabKey: 'DEVICE',
    activeActionTypeId: ''
  };
  deviceTreeColumns = [
    {
      key: 'type',
      title: 'Тип устройства',
      dataIndex: 'type',
      className: 'type-column',
      width: 250,
      render: (text, record) => this.deviceTypeColumnRender(text, record)
    },
    { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'shortAddressPath' },
    { key: 'regionName', title: 'Зона', dataIndex: 'regionName' }
  ];
  // Состояние ctrl
  ctrlKey = false;
  // Состояние shift
  shiftKey = false;
  // Id последнего выбранного устройства
  lastSelectedDeviceId = null;
  // Таймаут для сброса lastSelectedDeviceId
  lastSelectedDeviceIdCleaner = null;

  deviceTypeColumnRender = (text, record) => {
    return <DeviceTypeTreeItem record={record} />;
  };

  getDeviceConditionStates = deviceId => {
    const { deviceProfileViewsHash, scenarioDevices } = this.props;
    const device = scenarioDevices.find(device => device.id === deviceId);
    if (device) {
      const deviceProfileView = deviceProfileViewsHash[device.deviceProfileId];
      if (deviceProfileView && deviceProfileView.stateViews) {
        return [
          ...deviceProfileView.stateViews.filter(
            stateView => stateView.state.acceptableAsScenarioCondition
          )
        ];
      }
    }
    return [];
  };

  componentDidMount = () => {
    const { deviceList = [] } = this.props;
    if (deviceList.length) {
      const newDevicesTree = this.getExecutiveDeviceTree(deviceList);
      this.setState({
        deviceTree: newDevicesTree
      });
    }
    window.addEventListener('keydown', this.onKeyDown);
    window.addEventListener('keyup', this.onKeyUp);
  };

  componentWillUnmount() {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('keyup', this.onKeyUp);
  }

  onKeyDown = e => {
    if (e.ctrlKey) this.ctrlKey = true;
    if (e.shiftKey) this.shiftKey = true;
  };

  onKeyUp = e => {
    if (!e.ctrlKey && this.ctrlKey) this.ctrlKey = false;
    if (!e.shiftKey && this.shiftKey) this.shiftKey = false;
  };

  componentWillReceiveProps = nextProps => {
    const {
      currentValues,
      securityRegions,
      executiveScenarios,
      virtualStates,
      actionTypes = [],
      currentScenarioId,
      currentScenarioTLBlockNo
    } = this.props;
    const { activeActionTypeId } = this.state;
    if (
      currentScenarioId !== nextProps.currentScenarioId ||
      currentScenarioTLBlockNo !== nextProps.currentScenarioTLBlockNo
    ) {
      this.setState({
        selectedDevices: [],
        selectedScenarios: [],
        selectedRegions: [],
        selectedVirtualStates: [],
        selectedActions: [],
        activeTabKey: 'DEVICE',
        activeActionTypeId: ''
      });
    }
    if (this.props !== nextProps) {
      const actionDeviceIds =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.currentValues.actions
              .filter(action => action.entityType === 'DEVICE')
              .map(action => action.entityId)
          : [];
      const newDevicesTree = this.getExecutiveDeviceTree(
        nextProps.deviceList,
        nextProps.deviceProfileViewsHash,
        actionDeviceIds,
        nextProps.currentValues.forcedActionTypes
      );
      this.setState({
        deviceTree: newDevicesTree
      });
    }
    if (
      nextProps.securityRegions !== securityRegions ||
      currentValues !== nextProps.currentValues
    ) {
      const actionRegionIds =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.currentValues.actions
              .filter(action => action.entityType === 'REGION')
              .map(action => action.entityId)
          : [];
      const newRegionList =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.securityRegions.filter(region => !actionRegionIds.includes(region.id))
          : nextProps.securityRegions;
      this.setState({
        regionList: newRegionList
      });
    }
    if (
      nextProps.executiveScenarios !== executiveScenarios ||
      currentValues !== nextProps.currentValues
    ) {
      const actionScenarioIds =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.currentValues.actions
              .filter(action => action.entityType === 'SCENARIO')
              .map(action => action.entityId)
          : [];
      const newScenarioList =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.executiveScenarios.filter(
              scenario => !actionScenarioIds.includes(scenario.id)
            )
          : nextProps.executiveScenarios;
      this.setState({
        scenarioList: newScenarioList
      });
    }
    if (nextProps.virtualStates !== virtualStates || currentValues !== nextProps.currentValues) {
      const actionVirtualStateIds =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.currentValues.actions
              .filter(action => action.entityType === 'VIRTUAL_STATE')
              .map(action => action.entityId)
          : [];
      const newVirtualStateList =
        nextProps.currentValues.actions && nextProps.currentValues.actions.length
          ? nextProps.virtualStates.filter(scenario => !actionVirtualStateIds.includes(scenario.id))
          : nextProps.virtualStates;
      this.setState({
        virtualStateList: newVirtualStateList
      });
    }
    if (
      nextProps.actionTypes &&
      nextProps.actionTypes.length &&
      (actionTypes.length !== nextProps.actionTypes.length ||
        !activeActionTypeId ||
        (nextProps.currentValues.forcedActionTypes &&
          nextProps.currentValues.forcedActionTypes[0] &&
          nextProps.currentValues.forcedActionTypes[0] !== activeActionTypeId))
    ) {
      if (
        nextProps.currentValues.forcedActionTypes &&
        nextProps.currentValues.forcedActionTypes[0]
      ) {
        this.setState({ activeActionTypeId: nextProps.currentValues.forcedActionTypes[0].id });
      } else this.setState({ activeActionTypeId: nextProps.actionTypes[0].id });
    }
  };

  getExecutiveDeviceTree = (
    devices,
    deviceProfileViewsHash = {},
    actionDeviceIds = [],
    forcedActionTypes
  ) => {
    return getDevicesTree(
      filterDevicesLeavingParents(devices, device =>
        forcedActionTypes && forcedActionTypes[0] && forcedActionTypes[0].entityType === 'DEVICE'
          ? deviceProfileViewsHash[
              device.deviceProfileId
            ].deviceProfile.supportedScenarioActionIds.includes(forcedActionTypes[0].id) &&
            !device.disabled &&
            !actionDeviceIds.includes(device.id)
          : (device.deviceCategory === 'EXECUTIVE' ||
              device.deviceCategory === 'VIRTUAL_CONTAINER') &&
            !device.disabled &&
            !actionDeviceIds.includes(device.id)
      ),
      item => {
        item.isActive =
          !item.disabled &&
          (item.deviceCategory === 'EXECUTIVE' ||
            item.deviceCategory === 'VIRTUAL_CONTAINER' ||
            item.deviceCategory === 'CONTAINER');
        return item;
      }
    );
  };

  onSelectDevice = record => {
    const { selectedDevices, deviceTree } = this.state;
    if (record.isActive) {
      if (!this.ctrlKey && !this.shiftKey) {
        // Реализация обработки двойного клика в дереве
        if (record.id === this.lastSelectedDeviceId) {
          clearTimeout(this.lastSelectedDeviceIdCleaner);
          this.lastSelectedDeviceId = null;
          this.setState({ selectedDevices: [], selectedDevicesLastItem: null }, () => {
            this.onAddAction(record);
          });
        } else {
          clearTimeout(this.lastSelectedDeviceIdCleaner);
          this.lastSelectedDeviceId = record.id;
          this.lastSelectedDeviceIdCleaner = setTimeout(() => {
            this.lastSelectedDeviceId = null;
          }, 500);
          const deviceList = getFlatTree(deviceTree).filter(device => device.isActive);
          this.onRowListClick(record, 'selectedDevices', selectedDevices, deviceList);
        }
      } else {
        const deviceList = getFlatTree(deviceTree).filter(device => device.isActive);
        this.onRowListClick(record, 'selectedDevices', selectedDevices, deviceList);
      }
    }
  };

  onSelectRegion = record => {
    const { selectedRegions, regionList } = this.state;
    this.onRowListClick(record, 'selectedRegions', selectedRegions, regionList);
  };

  onSelectScenario = record => {
    const { selectedScenarios, scenarioList } = this.state;
    this.onRowListClick(record, 'selectedScenarios', selectedScenarios, scenarioList);
  };

  onSelectVirtualState = record => {
    const { selectedVirtualStates, virtualStateList } = this.state;
    this.onRowListClick(record, 'selectedVirtualStates', selectedVirtualStates, virtualStateList);
  };

  onSelectAction = (record, recordIndex) => {
    let { selectedActions, selectedActionsLastItem } = this.state;
    if (this.ctrlKey && !this.shiftKey) {
      if (selectedActions.includes(recordIndex))
        selectedActions = selectedActions.filter(key => key !== recordIndex);
      else selectedActions.push(recordIndex);
      this.setState({
        selectedActions: selectedActions,
        selectedActionsLastItem: selectedActions[selectedActions.length - 1] || null
      });
    } // Если зажат shift
    else if (
      this.shiftKey &&
      !this.ctrlKey &&
      selectedActions.length &&
      !isNaN(selectedActionsLastItem)
    ) {
      let start, end;
      const lastSelectedItemIndex = selectedActionsLastItem;
      const currentItemIndex = recordIndex;
      if (lastSelectedItemIndex > currentItemIndex) {
        start = currentItemIndex;
        end = lastSelectedItemIndex;
      } else {
        end = currentItemIndex;
        start = lastSelectedItemIndex;
      }
      const newSelectedIds = [];
      for (let i = start; i <= end; ++i) {
        newSelectedIds.push(i);
      }
      this.setState({ selectedActions: newSelectedIds });
    } // Если нет зажатых клавиш
    else if (!this.ctrlKey && !this.shiftKey) {
      if (selectedActions.length === 1 && selectedActions[0] === recordIndex) {
        this.setState({ selectedActions: [], selectedActionsLastItem: null });
      } else {
        this.setState({ selectedActions: [recordIndex], selectedActionsLastItem: recordIndex });
      }
    }
  };

  getAvailableActionTypeId = (entityType, entityId) => {
    const { actionTypes, devicesHash, deviceProfileViewsHash } = this.props;
    const { activeActionTypeId } = this.state;
    const availableActionTypes = actionTypes.filter(actionType => {
      if (entityType === 'DEVICE') {
        const device = devicesHash[entityId];
        const deviceProfile = getDeviceProfile(deviceProfileViewsHash, device);
        const isSupportedScenarioActionType = deviceProfile
          ? deviceProfile.supportedScenarioActionIds.includes(actionType.id)
          : false;
        return actionType.entityType === entityType && isSupportedScenarioActionType;
      } else return actionType.entityType === entityType;
    });
    const activeActionType = availableActionTypes.find(item => item.id === activeActionTypeId);
    if (activeActionType) {
      return activeActionType.id;
    } else return availableActionTypes[0].id;
  };

  onRowListClick = (record, listName, selectedIds, entitiesList) => {
    const { [listName + 'LastItem']: lastSelectedId } = this.state;
    // Если зажат ctrl
    if (this.ctrlKey && !this.shiftKey) {
      if (selectedIds.includes(record.id))
        selectedIds = selectedIds.filter(key => key !== record.id);
      else selectedIds.push(record.id);
      this.setState({
        [listName]: selectedIds,
        [listName + 'LastItem']: selectedIds[selectedIds.length - 1] || null
      });
    }
    // Если зажат shift
    else if (this.shiftKey && !this.ctrlKey && selectedIds.length && lastSelectedId) {
      let start, end;
      const lastSelectedItemIndex = entitiesList.findIndex(entity => entity.id === lastSelectedId);
      const currentItemIndex = entitiesList.findIndex(entity => entity.id === record.id);
      if (lastSelectedItemIndex > currentItemIndex) {
        start = currentItemIndex;
        end = lastSelectedItemIndex;
      } else {
        end = currentItemIndex;
        start = lastSelectedItemIndex;
      }
      const entitiesInRange = entitiesList.slice(start, end + 1);
      const newSelectedIds = entitiesInRange.map(entity => entity.id);
      this.setState({ [listName]: newSelectedIds });
    }
    // Если нет зажатых клавиш
    else if (!this.ctrlKey && !this.shiftKey) {
      if (selectedIds.length === 1 && selectedIds[0] === record.id) {
        this.setState({ [listName]: [], [listName + 'LastItem']: null });
      } else {
        this.setState({ [listName]: [record.id], [listName + 'LastItem']: record.id });
      }
    }
  };

  onAddActions = () => {
    const { dispatch, currentValues } = this.props;
    const { activeTabKey: entityType } = this.state;
    let listName = SELECTED_IDS_LIST_NAMES[entityType];
    if (listName) {
      const { [listName]: selectedIds } = this.state;
      const actions = currentValues.actions ? [...currentValues.actions] : [];
      if (entityType === 'DEVICE') {
        const { deviceTree } = this.state;
        const selectedIdsHash = selectedIds.reduce(
          (hash, entityId) => ({ ...hash, [entityId]: {} }),
          {}
        );
        getFlatTree(deviceTree).forEach(device => {
          if (selectedIdsHash[device.id]) {
            selectedIdsHash[device.id] = device;
            if (
              device.deviceCategory === 'CONTAINER' &&
              device.children &&
              device.children.length
            ) {
              device.children.forEach(child => (selectedIdsHash[child.id] = child));
              delete selectedIdsHash[device.id];
            }
          }
        });
        const entityIds = Object.keys(selectedIdsHash);
        entityIds.forEach(entityId => {
          const actionTypeId = this.getAvailableActionTypeId(entityType, entityId);
          const action = {
            entityId,
            entityType,
            actionTypeId
          };
          actions.push(action);
        });
      } else {
        selectedIds.forEach(entityId => {
          const actionTypeId = this.getAvailableActionTypeId(entityType, entityId);
          const action = {
            entityId,
            entityType,
            actionTypeId
          };
          actions.push(action);
        });
      }
      dispatch(change(FORM_NAME, `actions`, actions));
      this.setState({ [listName]: [] });
    }
  };

  onAddAction = record => {
    if (!this.ctrlKey && !this.shiftKey) {
      const { dispatch, currentValues } = this.props;
      const { activeTabKey: entityType } = this.state;
      let listName = SELECTED_IDS_LIST_NAMES[entityType];
      if (listName) {
        if (entityType === 'DEVICE') {
          if (record.isActive) {
            if (record.deviceCategory === 'CONTAINER') {
              if (record.children && record.children.length) {
                const actions = currentValues.actions ? [...currentValues.actions] : [];
                record.children.forEach(({ id: entityId }) => {
                  const actionTypeId = this.getAvailableActionTypeId(entityType, entityId);
                  const action = {
                    entityId,
                    entityType,
                    actionTypeId
                  };
                  actions.push(action);
                });
                dispatch(change(FORM_NAME, `actions`, actions));
                this.setState({ [listName]: [] });
                this.setState({ [listName]: [] });
              }
            } else {
              const actionTypeId = this.getAvailableActionTypeId(entityType, record.id);
              const actions = currentValues.actions ? [...currentValues.actions] : [];
              const action = {
                entityId: record.id,
                entityType,
                actionTypeId
              };
              actions.push(action);
              dispatch(change(FORM_NAME, `actions`, actions));
              this.setState({ [listName]: [] });
            }
          }
        } else {
          const actionTypeId = this.getAvailableActionTypeId(entityType, record.id);
          const actions = currentValues.actions ? [...currentValues.actions] : [];
          const action = {
            entityId: record.id,
            entityType,
            actionTypeId
          };
          actions.push(action);
          dispatch(change(FORM_NAME, `actions`, actions));
          this.setState({ [listName]: [] });
        }
      }
    }
  };

  onRemoveActions = () => {
    const { dispatch, currentValues } = this.props;
    const { selectedActions } = this.state;
    if (currentValues.actions && currentValues.actions.length && selectedActions.length) {
      const actions = currentValues.actions.filter(
        (action, index) => !selectedActions.includes(index)
      );
      dispatch(change(FORM_NAME, `actions`, actions));
      this.setState({ selectedActions: [] });
    }
  };

  onRemoveAction = record => {
    if (!this.ctrlKey && !this.shiftKey) {
      const { dispatch, currentValues } = this.props;
      if (currentValues.actions && currentValues.actions.length) {
        const actions = currentValues.actions.filter(action =>
          action.entityType === record.entityType && action.entityId === record.entityId
            ? false
            : true
        );
        dispatch(change(FORM_NAME, `actions`, actions));
        this.setState({ selectedActions: [] });
      }
    }
  };

  onChangeActionType = (value, index) => {
    const { dispatch } = this.props;
    dispatch(change(FORM_NAME, `actions[${index}].actionTypeId`, value));
  };

  onChangeActiveTab = activeTabKey => {
    this.setState({ activeTabKey });
  };

  onChangeActiveActionType = value => {
    this.setState({ activeActionTypeId: value });
  };

  onSetActionType = () => {
    const {
      actionTypes,
      currentValues,
      dispatch,
      devicesHash,
      deviceProfileViewsHash
    } = this.props;
    const { activeActionTypeId, selectedActions } = this.state;
    const activeActionType = actionTypes.find(actionType => actionType.id === activeActionTypeId);
    const actions = currentValues.actions.map((action, index) => {
      if (action.entityType === activeActionType.entityType && selectedActions.includes(index)) {
        const availableActionTypes = actionTypes.filter(actionType => {
          if (action.entityType === 'DEVICE') {
            const device = devicesHash[action.entityId];
            const deviceProfile = getDeviceProfile(deviceProfileViewsHash, device);
            const isSupportedScenarioActionType = deviceProfile
              ? deviceProfile.supportedScenarioActionIds.includes(actionType.id)
              : false;
            return actionType.entityType === action.entityType && isSupportedScenarioActionType;
          } else return actionType.entityType === action.entityType;
        });
        if (availableActionTypes.find(item => item.id === activeActionType.id)) {
          return { ...action, actionTypeId: activeActionType.id };
        } else return action;
      } else return action;
    });
    dispatch(change(FORM_NAME, `actions`, actions));
  };

  // TODO: Переделать дизейбл вкладок по scenarioPurpose и сделать по forcedEntityType
  render() {
    const {
      modalName,
      handleSubmit,
      isEdit,
      blockTypes,
      actionTypes,
      conditionTypes,
      conditionCheckTypes,
      scenarioDevices,
      virtualStates,
      currentValues,
      valid,
      dirty,
      dispatch,
      deviceProfileViewsHash,
      devicesHash,
      regionsHash,
      scenariosHash,
      virtualStatesHash
    } = this.props;
    const {
      deviceTree,
      regionList,
      scenarioList,
      virtualStateList,
      selectedDevices = [],
      selectedScenarios = [],
      selectedRegions = [],
      selectedVirtualStates = [],
      selectedActions = [],
      activeTabKey,
      activeActionTypeId
    } = this.state;

    return (
      <Modal name={modalName} title="Исполняемый блок сценария" width="90%">
        <Form onSubmit={handleSubmit}>
          <Row>
            <Col span={8}>
              <FormItem {...ItemLayoutWithLabel} label="Блок">
                <Field component={Select} name="blockType" itemsSource={blockTypes} disabled />
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Название блока">
                <Field component={Input} name="name" />
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Задержка, в секундах ">
                <Field
                  component={InputNumber}
                  name="timeDelaySec"
                  min={0}
                  disabled={currentValues.purpose === 'EXEC_BY_TACTICS'}
                />
              </FormItem>
              {/* TODO: дерево устройств для выбора */}
              <FormItem {...ItemLayoutWithLabel} label="Условие">
                <Field
                  component={Select}
                  name="conditionType"
                  itemsSource={conditionTypes}
                  onChange={() =>
                    resetFormValues(dispatch, {
                      ...currentValues,
                      conditionEntityId: null
                    })
                  }
                  disabled={currentValues.purpose === 'EXEC_BY_TACTICS'}
                />
              </FormItem>
              {currentValues.conditionType !== 'NONE' && (
                <FormItem {...ItemLayoutWithLabel} label="Тип проверки">
                  <Field
                    component={Select}
                    name="conditionCheckType"
                    itemsSource={conditionCheckTypes}
                  />
                </FormItem>
              )}
              {currentValues.conditionType === 'VIRTUAL_STATE' && (
                <FormItem {...ItemLayoutWithLabel} label="Состояние" required={true}>
                  <Field component={Select} name="conditionEntityId" itemsSource={virtualStates} />
                </FormItem>
              )}
              {currentValues.conditionType === 'DEVICE_STATE' && (
                <FormItem {...ItemLayoutWithLabel} label="Устройство" required={true}>
                  <Field
                    component={DeviceSelect}
                    name="conditionEntityId"
                    devices={scenarioDevices}
                  />
                </FormItem>
              )}
              {currentValues.conditionType === 'DEVICE_STATE' && (
                <FormItem {...ItemLayoutWithLabel} label="Состояние" required={true}>
                  <Field
                    component={Select}
                    name="conditionDeviceStateId"
                    itemsSource={this.getDeviceConditionStates(currentValues.conditionEntityId)}
                  />
                </FormItem>
              )}
            </Col>
            <Col span={8}>
              <TabsWrapper>
                <Tabs activeKey={activeTabKey} animated={false} onChange={this.onChangeActiveTab}>
                  <TabPane tab="Устройства" key="DEVICE">
                    <Wrapper>
                      {deviceTree.length ? (
                        <ExpandableTable
                          forceWidth
                          onRowClick={record => this.onSelectDevice(record)}
                          onRowDoubleClick={record => this.onAddAction(record)}
                          rowSelections={selectedDevices}
                          columns={this.deviceTreeColumns}
                          nodes={deviceTree}
                          height={288}
                        />
                      ) : null}
                    </Wrapper>
                  </TabPane>
                  <TabPane
                    tab="Зоны"
                    key="REGION"
                    disabled={currentValues.purpose === 'EXEC_BY_TACTICS'}
                  >
                    <Wrapper>
                      <AutoSizer>
                        {({ height, width }) => (
                          <RegionList
                            entities
                            onRowDoubleClick={({ rowData }) => this.onAddAction(rowData)}
                            onRowClick={({ rowData }) => this.onSelectRegion(rowData)}
                            rowSelections={selectedRegions}
                            height={height - 2}
                            width={width - 2}
                            regions={regionList}
                          />
                        )}
                      </AutoSizer>
                    </Wrapper>
                  </TabPane>
                  <TabPane
                    tab="Сценарии"
                    key="SCENARIO"
                    disabled={currentValues.purpose === 'EXEC_BY_TACTICS'}
                  >
                    <Wrapper>
                      <AutoSizer>
                        {({ height, width }) => (
                          <ScenarioList
                            onRowDoubleClick={({ rowData }) => this.onAddAction(rowData)}
                            onRowClick={({ rowData }) => this.onSelectScenario(rowData)}
                            rowSelections={selectedScenarios}
                            height={height - 2}
                            width={width - 2}
                            scenarios={scenarioList}
                          />
                        )}
                      </AutoSizer>
                    </Wrapper>
                  </TabPane>
                  <TabPane
                    tab="Вирт. сост."
                    key="VIRTUAL_STATE"
                    disabled={currentValues.purpose === 'EXEC_BY_TACTICS'}
                  >
                    <Wrapper>
                      <AutoSizer>
                        {({ height, width }) => (
                          <VirtualStateList
                            onRowDoubleClick={({ rowData }) => this.onAddAction(rowData)}
                            onRowClick={({ rowData }) => this.onSelectVirtualState(rowData)}
                            rowSelections={selectedVirtualStates}
                            height={height - 2}
                            width={width - 2}
                            virtualStates={virtualStateList}
                          />
                        )}
                      </AutoSizer>
                    </Wrapper>
                  </TabPane>
                </Tabs>
              </TabsWrapper>
            </Col>
            <Col span={1}>
              <div style={BUTTON_GROUP_STYLES}>
                <div style={BUTTON_WRAPPER_STYLES}>
                  <Button shape="circle" onClick={this.onRemoveActions} icon="left" />
                </div>
                <div style={BUTTON_WRAPPER_STYLES}>
                  <Button shape="circle" onClick={this.onAddActions} icon="right" />
                </div>
              </div>
            </Col>
            <Col span={7}>
              <TabsWrapper>
                <div style={ACTIONS_MENU_WRAPPER_STYLES}>
                  <div>
                    <Tooltip title="Выбранное действие автоматически применяется при добавлении объектов">
                      <span>
                        <Icon style={ICON_STYLES} type="info-circle-o" />
                      </span>
                    </Tooltip>
                    <CustomSelect
                      style={SELECT_STYLES}
                      value={activeActionTypeId}
                      onChange={this.onChangeActiveActionType}
                    >
                      {(currentValues.forcedActionTypes && currentValues.forcedActionTypes[0]
                        ? currentValues.forcedActionTypes
                        : actionTypes
                      ).map((actionType, index) => (
                        <CustomSelect.Option key={index} value={actionType.id}>
                          {actionType.name}
                        </CustomSelect.Option>
                      ))}
                    </CustomSelect>
                  </div>
                  <Button onClick={this.onSetActionType}>Применить к выдел.</Button>
                </div>
                <Wrapper>
                  <AutoSizer>
                    {({ height, width }) => (
                      <Field
                        component={ExecBlockActionList}
                        name="actions"
                        rowSelections={selectedActions}
                        onRowDoubleClick={({ rowData }) => this.onRemoveAction(rowData)}
                        onRowClick={({ rowData, index }) => this.onSelectAction(rowData, index)}
                        onChangeActionType={this.onChangeActionType}
                        deviceProfileViewsHash={deviceProfileViewsHash}
                        scenarioPurpose={currentValues.purpose}
                        actions={currentValues.actions}
                        devices={devicesHash}
                        regions={regionsHash}
                        scenarios={scenariosHash}
                        virtualStates={virtualStatesHash}
                        actionTypes={
                          currentValues.forcedActionTypes && currentValues.forcedActionTypes[0]
                            ? currentValues.forcedActionTypes
                            : actionTypes
                        }
                        height={height - 2}
                        width={width - 3}
                      />
                    )}
                  </AutoSizer>
                </Wrapper>
              </TabsWrapper>
            </Col>
          </Row>
          <Row>
            <FormItem>
              <Button type="primary" htmlType="submit" disabled={!valid || (isEdit && !dirty)}>
                {isEdit ? 'Изменить' : 'Добавить'}
              </Button>
            </FormItem>
          </Row>
        </Form>
      </Modal>
    );
  }
}

const validate = values => {
  const errors = {};
  if (values) {
    if (!values.conditionType) {
      errors.conditionType = 'Не выбрано условие';
    }
    if (!values.conditionCheckType) {
      errors.conditionCheckType = 'Не выбран тип проверки';
    }
    if (values.conditionType !== 'NONE' && !values.conditionEntityId) {
      errors.conditionEntityId = 'Не выбрано устройство или виртуальное состояние';
    }
    if (values.conditionType === 'DEVICE_STATE' && !values.conditionDeviceStateId) {
      errors.conditionDeviceStateId = 'Не выбрано состояние устройства';
    }
    if ((values.actions && !values.actions.length) || !values.actions) {
      errors.actions = 'Нет заданных действий для исполнительного блока';
    }
  }
  return errors;
};

ScenarioExecBlockForm = reduxForm({
  form: FORM_NAME,
  validate
})(ScenarioExecBlockForm);

const mapStateToProps = state => {
  const currentValues = state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {};
  return {
    /* Справочники */
    blockTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.timeLineBlockTypes
      : [],
    actionEntityTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.actionEntityTypes
      : [],
    actionTypes: state.scenarioConstants.actionTypes
      ? currentValues.purpose
        ? state.scenarioConstants.actionTypes
        : state.scenarioConstants.actionTypes.filter(
            actionType =>
              !actionType.scenarioPurpose || actionType.scenarioPurpose === currentValues.purpose
          )
      : [],
    conditionTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.conditionTypes
      : [],
    conditionCheckTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.conditionCheckTypes
      : [],
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),

    /* Сущности */
    devicesHash: getCurrentProjectDevicesHash(state) || {},
    regionsHash: getCurrentProjectRegionsHash(state) || {},
    scenariosHash: getCurrentProjectScenariosHash(state) || {},
    virtualStatesHash: getCurrentProjectVirtualStateHash(state) || {},
    deviceList: getCurrentProjectDeviceList(state),
    scenarioDevices: getCurrentProjectDeviceList(state).filter(
      device => device.acceptableAsScenarioCondition && !device.disabled
    ),
    securityRegions: getCurrentProjectRegions(state).filter(
      region => region.subsystem === 'SECURITY'
    ),
    executiveScenarios: getExecutiveScenarios(state),
    virtualStates: getCurrentProjectVirtualStates(state),

    currentScenarioId: state.widgets.currentScenarioId,
    currentScenarioTLBlockNo: state.widgets.currentScenarioTLBlockNo,
    /* Текущие значения полей */
    currentValues
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioExecBlockForm);
