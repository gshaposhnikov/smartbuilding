import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { selectScenarioSubLogic } from 'actions/scenarios';

import { isEmpty } from 'lodash';
import { findDevicesByDeviceCategory } from 'helpers/device';

import Form, { FormItem, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';
import Text from 'components/Text';

import Modal from 'containers/Modal';
import Select from 'containers/Selects';
import DeviceSelect from 'containers/Selects/DeviceSelect';

import { Button, Tree, Row, Col } from 'antd';
const TreeNode = Tree.TreeNode;

import {
  Field,
  reduxForm,
  initialize,
  reset,
  formValueSelector,
  clearFields,
  change
} from 'redux-form';
import {
  getCurrentProjectVirtualStates,
  getCurrentProjectDeviceList,
  getCurrentProjectRegions
} from 'helpers/currentProject';

const FORM_NAME = 'scenarioLogic';
const SCENARIO_SUBLOGIC_MAX_LEVEL = 15;

/**
 * Сброс значение формы
 * @param dispatch
 * @param values объект со значениями
 */
export function resetFormValues(dispatch, values) {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
}

/**
 * Получить префикс названия переменной по рекурсивному пути.
 * @param {array} recursionPath
 */
function getFieldNamePrefix(recursionPath) {
  let fieldNamePath = '';
  recursionPath.forEach(position => {
    fieldNamePath += `subLogics[${position}].`;
  });
  return fieldNamePath;
}

function renderEntitySelector(
  currentValues,
  controlDevices,
  devicesGroupedByTriggerTypes,
  regionsGroupedByTriggerTypes,
  virtualStates,
  fieldName,
  required
) {
  const { entityType, triggerTypeId } = currentValues;

  switch (entityType) {
    case 'DEVICE':
      return (
        <FormItem {...ItemLayoutWithLabel} label="Устройства" required={required}>
          <Field
            component={DeviceSelect}
            name={fieldName}
            devices={devicesGroupedByTriggerTypes[triggerTypeId]}
            mode="multiple"
          />
        </FormItem>
      );
    case 'CONTROL_DEVICE':
      return (
        <FormItem {...ItemLayoutWithLabel} label="Приборы" required={required}>
          <Field
            component={DeviceSelect}
            name={fieldName}
            devices={controlDevices}
            mode="multiple"
          />
        </FormItem>
      );
    case 'REGION':
      return (
        <FormItem {...ItemLayoutWithLabel} label="Зоны" required={required}>
          <Field
            component={Select}
            name={fieldName}
            itemsSource={regionsGroupedByTriggerTypes[triggerTypeId]}
            mode="multiple"
          />
        </FormItem>
      );
    case 'VIRTUAL_STATE': {
      return (
        <FormItem {...ItemLayoutWithLabel} label="Виртуальные состояния" required={required}>
          <Field component={Select} name={fieldName} itemsSource={virtualStates} mode="multiple" />
        </FormItem>
      );
    }
    default:
      return <div />;
  }
}

/**
 * Модальная форма редактирования логики запуска/остановки сценария.
 *
 * Параметры, которые должны передаваться от родительского компонента:
 * @prop modalName - глобальное название модального окна
 * @prop handleSubmit - обработчик кнопки ОК
 **/
class ScenarioLogicForm extends Component {
  static propTypes = {
    /* Из компонента верхнего уровня */
    modalName: PropTypes.string,
    handleSubmit: PropTypes.func,

    /* Из redux-store */
    logicTypes: PropTypes.array,
    triggerEntityTypes: PropTypes.array,
    triggerTypes: PropTypes.array,
    controlDevices: PropTypes.array,
    devicesGroupedByTriggerTypes: PropTypes.object,
    regionsGroupedByTriggerTypes: PropTypes.object,
    virtualStates: PropTypes.array,

    /* Из redux-form через redux-store */
    currentValues: PropTypes.object,
    valid: PropTypes.bool,
    dirty: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.state = {
      currentSubLogicPath: [],
      currentSubLogic: {},
      fieldNamePrefix: '',
      currentTriggerType: null
    };
  }

  getDictionaryEntityName(id, fieldName) {
    if (id) {
      const dicEntities = this.props[fieldName];
      const dicEntity = dicEntities.find(dicEntity => dicEntity.id === id);
      if (dicEntity) {
        return dicEntity.name;
      }
    }
    return 'не определено';
  }

  propsToState(props) {
    const { currentValues, currentSubLogicKey } = props;

    const currentSubLogicPath =
      currentSubLogicKey === 'root'
        ? []
        : currentSubLogicKey.split(',').map(str => parseInt(str, 10));

    let currentSubLogic = currentValues;
    currentSubLogicPath.forEach(subLogicNo => {
      if (currentSubLogic.subLogics && currentSubLogic.subLogics.length > subLogicNo) {
        currentSubLogic = currentSubLogic.subLogics[subLogicNo];
      } else {
        currentSubLogic = {};
      }
    });

    const fieldNamePrefix = getFieldNamePrefix(currentSubLogicPath);

    this.setState({
      ...this.state,
      currentSubLogicPath,
      currentSubLogic,
      fieldNamePrefix
    });
  }

  componentDidMount() {
    this.propsToState(this.props);
  }

  onSubLogicSelect(selectedKeys, info) {
    const { dispatch } = this.props;
    dispatch(
      selectScenarioSubLogic(selectedKeys && selectedKeys.length > 0 ? selectedKeys[0] : 'root')
    );
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onEntityTypeChanged = () => {
    const { dispatch } = this.props;
    const { fieldNamePrefix } = this.state;
    dispatch(
      clearFields(
        FORM_NAME,
        false,
        false,
        `${fieldNamePrefix}triggerTypeId`,
        `${fieldNamePrefix}entityIds`
      )
    );
  };

  onTriggerTypeChanged = (e, triggerTypeId) => {
    const { dispatch, triggerTypes } = this.props;
    const { fieldNamePrefix } = this.state;

    dispatch(clearFields(FORM_NAME, false, false, `${fieldNamePrefix}entityIds`));
    const currentTriggerType = triggerTypes.find(triggerType => triggerType.id === triggerTypeId);
    this.setState({ currentTriggerType });
    if (currentTriggerType && currentTriggerType.forcedScenarioLogicType)
      dispatch(
        change(FORM_NAME, `${fieldNamePrefix}logicType`, currentTriggerType.forcedScenarioLogicType)
      );
  };

  renderSubLogicsNavigator(subLogics, subLogicPath) {
    return [...subLogics, {}].map((subLogic, index) => (
      <TreeNode
        title={
          isEmpty(subLogic)
            ? `...`
            : `Логика "${this.getDictionaryEntityName(
                subLogic.logicType,
                'logicTypes'
              )}" (${this.getDictionaryEntityName(subLogic.triggerTypeId, 'triggerTypes')})`
        }
        key={[...subLogicPath, index].join()}
      >
        {subLogicPath.length + 1 < SCENARIO_SUBLOGIC_MAX_LEVEL
          ? !isEmpty(subLogic) &&
            (subLogic.subLogics
              ? this.renderSubLogicsNavigator(subLogic.subLogics, [...subLogicPath, index])
              : this.renderSubLogicsNavigator([], [...subLogicPath, index]))
          : null}
      </TreeNode>
    ));
  }

  render() {
    const {
      modalName,
      handleSubmit,
      scenarioPurpose,
      logicTypes,
      triggerEntityTypes,
      triggerTypes,
      currentValues,
      currentSubLogicKey,
      dispatch,
      controlDevices,
      devicesGroupedByTriggerTypes,
      regionsGroupedByTriggerTypes,
      virtualStates,
      valid,
      dirty
    } = this.props;

    const {
      currentSubLogicPath,
      currentSubLogic,
      fieldNamePrefix,
      currentTriggerType
    } = this.state;

    return (
      <Modal name={modalName} title="Логика сценария" width="50%">
        <Form onSubmit={handleSubmit}>
          <Row gutter={16}>
            <Col span={10}>
              <div style={{ height: '300px', overflow: 'auto', border: '1px inset' }}>
                <Tree
                  showLine
                  onSelect={this.onSubLogicSelect.bind(this)}
                  defaultExpandedKeys={['root']}
                >
                  <TreeNode
                    title={`Логика "${this.getDictionaryEntityName(
                      currentValues.logicType,
                      'logicTypes'
                    )}" (${this.getDictionaryEntityName(
                      currentValues.triggerTypeId,
                      'triggerTypes'
                    )})`}
                    key="root"
                  >
                    {scenarioPurpose !== 'EXEC_BY_TACTICS'
                      ? currentValues.subLogics
                        ? this.renderSubLogicsNavigator(currentValues.subLogics, [])
                        : this.renderSubLogicsNavigator([], [])
                      : null}
                  </TreeNode>
                </Tree>
              </div>
            </Col>
            <Col span={14}>
              <Row>
                <Text style={{ display: 'flex', justifyContent: 'center' }}>
                  {currentSubLogicKey === 'root'
                    ? `Логика "${this.getDictionaryEntityName(
                        currentValues.logicType,
                        'logicTypes'
                      )}" (корневая)`
                    : isEmpty(currentSubLogic)
                      ? `Определите логику (уровень ${
                          currentSubLogicPath.length
                        }, вложение ${currentSubLogicPath[currentSubLogicPath.length - 1] + 1})`
                      : `Логика "${this.getDictionaryEntityName(
                          currentSubLogic.logicType,
                          'logicTypes'
                        )}" (уровень ${currentSubLogicPath.length +
                          1}, вложение ${currentSubLogicPath[currentSubLogicPath.length - 1] + 1})`}
                </Text>
              </Row>
              <Row>
                <div style={{ padding: 10 }}>
                  <Field name="validator" component={Text} style={{ display: 'none' }} />
                  <FormItem {...ItemLayoutWithLabel} label="Логика" required={true}>
                    <Field
                      disabled={
                        !!(currentTriggerType && currentTriggerType.forcedScenarioLogicType)
                      }
                      component={Select}
                      name={`${fieldNamePrefix}logicType`}
                      itemsSource={logicTypes}
                    />
                  </FormItem>
                  <FormItem
                    {...ItemLayoutWithLabel}
                    label="Тип"
                    required={
                      currentSubLogic.subLogics && currentSubLogic.subLogics.length > 0
                        ? false
                        : true
                    }
                  >
                    <Field
                      component={Select}
                      name={`${fieldNamePrefix}entityType`}
                      itemsSource={triggerEntityTypes}
                      onChange={this.onEntityTypeChanged}
                      disabled={currentSubLogic && currentSubLogic.logicType ? false : true}
                    />
                  </FormItem>
                  <FormItem
                    {...ItemLayoutWithLabel}
                    label="Событие"
                    required={
                      currentSubLogic.subLogics && currentSubLogic.subLogics.length > 0
                        ? false
                        : true
                    }
                  >
                    <Field
                      component={Select}
                      name={`${fieldNamePrefix}triggerTypeId`}
                      itemsSource={triggerTypes.filter(
                        triggerType => triggerType.entityType === currentSubLogic.entityType
                      )}
                      onChange={this.onTriggerTypeChanged}
                      disabled={currentSubLogic && currentSubLogic.logicType ? false : true}
                    />
                  </FormItem>
                  {renderEntitySelector(
                    currentSubLogic,
                    controlDevices,
                    devicesGroupedByTriggerTypes,
                    regionsGroupedByTriggerTypes,
                    virtualStates,
                    `${fieldNamePrefix}entityIds`,
                    currentSubLogic.subLogics && currentSubLogic.subLogics.length > 0 ? false : true
                  )}
                  {currentSubLogicKey !== 'root' &&
                    !isEmpty(currentSubLogic) && (
                      <FormItem {...ItemLayoutWithoutLabel}>
                        <Button
                          icon="delete"
                          onClick={() => {
                            const fieldName = `${getFieldNamePrefix(
                              currentSubLogicPath.slice(0, -1)
                            )}subLogics`;
                            let parent = currentValues;
                            for (let i = 0; i < currentSubLogicPath.length - 1; ++i) {
                              parent = parent.subLogics[currentSubLogicPath[i]];
                            }
                            const lastIndex = currentSubLogicPath[currentSubLogicPath.length - 1];
                            dispatch(
                              change(FORM_NAME, fieldName, [
                                ...parent.subLogics.slice(0, lastIndex),
                                ...parent.subLogics.slice(lastIndex + 1)
                              ])
                            );
                          }}
                        />
                      </FormItem>
                    )}
                </div>
              </Row>
            </Col>
          </Row>
          <Row>
            <FormItem>
              <Button type="primary" htmlType="submit" disabled={!valid || !dirty}>
                {'Изменить'}
              </Button>
            </FormItem>
          </Row>
        </Form>
      </Modal>
    );
  }
}

const validate = values => {
  if (!values.logicType) {
    return { validator: 'Не выбран тип логики' };
  }
  if (!values.subLogics || values.subLogics.length === 0) {
    if (!values.entityType) {
      return { validator: 'Не выбран тип объекта' };
    }
    if (!values.triggerTypeId) {
      return { validator: 'Не выбрано событие' };
    }
    if (!values.entityIds || values.entityIds.length === 0) {
      return { validator: 'Не выбран ни один объект' };
    }
  } else {
    for (var i = 0; i < values.subLogics.length; ++i) {
      const subErrors = validate(values.subLogics[i]);
      if (subErrors.validator) {
        return subErrors;
      }
    }
  }
  return {};
};

ScenarioLogicForm = reduxForm({
  form: FORM_NAME,
  validate
})(ScenarioLogicForm);

function findDevicesGroupedByTriggerTypes(state) {
  const { scenarioConstants, deviceProfileViews } = state;
  const devices = getCurrentProjectDeviceList(state);
  const groupedDevices = {};
  if (scenarioConstants && scenarioConstants.triggerTypes) {
    scenarioConstants.triggerTypes.forEach(triggerType => {
      groupedDevices[triggerType.id] = [];
      if (devices && deviceProfileViews && triggerType.entityType === 'DEVICE') {
        devices.forEach(device => {
          const deviceProfileView = deviceProfileViews[device.deviceProfileId];
          if (
            deviceProfileView &&
            deviceProfileView.deviceProfile.supportedScenarioTriggerIds.includes(triggerType.id) &&
            !device.disabled
          ) {
            groupedDevices[triggerType.id].push({ ...device });
          }
        });
      }
    });
  }
  return groupedDevices;
}

function findRegionsGroupedByTriggerTypes(state) {
  const { scenarioConstants } = state;
  const regions = getCurrentProjectRegions(state);
  const groupedRegions = {};
  if (scenarioConstants && scenarioConstants.triggerTypes) {
    scenarioConstants.triggerTypes.forEach(triggerType => {
      if (regions && triggerType.entityType === 'REGION') {
        if (triggerType.subsystem === 'GENERAL') {
          groupedRegions[triggerType.id] = [...regions];
        } else {
          groupedRegions[triggerType.id] = [
            ...regions.filter(region => region.subsystem === triggerType.subsystem)
          ];
        }
      } else {
        groupedRegions[triggerType.id] = [];
      }
    });
  }
  return groupedRegions;
}

const getCurrentFormValues = formValueSelector(FORM_NAME);
const mapStateToProps = (state, ownProps) => {
  const { scenarioPurpose } = ownProps;
  const currentValues = state.form[FORM_NAME]
    ? getCurrentFormValues(
        state,
        'logicType',
        'entityType',
        'triggerTypeId',
        'entityIds',
        'subLogics'
      )
    : {};
  /* Триггеры только с назначением как у сценария */
  const triggerTypes = state.scenarioConstants.triggerTypes
    ? state.scenarioConstants.triggerTypes.filter(
        triggerType => triggerType.scenarioPurpose === scenarioPurpose
      )
    : [];
  /* Типы сущностей только те, который есть у допустимых триггеров */
  const triggerEntityTypes = state.scenarioConstants.dictionaries
    ? state.scenarioConstants.dictionaries.triggerEntityTypes.filter(entityType =>
        triggerTypes.find(triggerType => triggerType.entityType === entityType.id)
      )
    : [];

  /*
   * TODO: перенести фильтрацию в componentWillReceiveProps,
   * либо написать селекторы (https://github.com/reduxjs/reselect#accessing-react-props-in-selectors)
   */
  return {
    /* Справочники */
    logicTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.logicTypes
      : [],
    triggerEntityTypes,
    triggerTypes,

    /* Сущности */
    controlDevices: findDevicesByDeviceCategory(state, 'CONTROL').filter(
      device => !device.disabled
    ),
    devicesGroupedByTriggerTypes: findDevicesGroupedByTriggerTypes(state),
    regionsGroupedByTriggerTypes: findRegionsGroupedByTriggerTypes(state),
    virtualStates: getCurrentProjectVirtualStates(state),

    /* Текущие значения полей */
    currentValues,
    /* Выделенный уровень логики */
    currentSubLogicKey: state.widgets.currentSubLogicKey
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ScenarioLogicForm);
