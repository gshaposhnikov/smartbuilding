import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, Field, change, reset } from 'redux-form';
import styled from 'styled-components';
import { Button, Icon, InputNumber } from 'antd';
import { AutoSizer } from 'react-virtualized';

import Checkbox from 'components/Form/Checkbox';
import DeviceList from 'components/VirtualizedDeviceList';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import { EditableCell } from 'components/Table';
import { FormItem } from 'components/Form';
import Input from 'components/Form/Input';
import { Row, Column } from 'components/Layout';
import RegionList from 'components/RegionList';
import ScenarioList from 'components/VirtualizedScenarioList';
import Select, { Option } from 'components/Form/Select';
import Tree from 'components/ExpandableTable/Table';

import Modal from 'containers/Modal';

import {
  getCurrentProjectRegionViews,
  getCurrentProjectRegionsHash,
  getCurrentProjectScenarios,
  getCurrentProjectScenariosHash,
  getCurrentProjectDeviceList,
  getCurrentProjectDevicesHash
} from 'helpers/currentProject';
import { getDevicesTree } from 'helpers/device';
import message from 'helpers/message';

const ButtonGroup = Button.Group;
const FORM_NAME = 'updateIndicator';
const ENTITY_TYPES = {
  DEVICE: {
    id: 'DEVICE',
    name: 'Устройство'
  },
  REGION: {
    id: 'REGION',
    name: 'Зона'
  },
  SCENARIO: {
    id: 'SCENARIO',
    name: 'Сценарий'
  },
  NONE: {
    id: 'NONE',
    name: 'Без типа'
  }
};
const FONT_BUTTON_STYLES = { width: '2em', padding: 0 };
const FONT_SIZE_STYLES = { width: '4em' };
const COLUMN_STYLES = { padding: '0 8px' };
const WRAPPER_STYLES = { height: 300 };
const BUTTON_GROUP_STYLES = { marginRight: '4px' };
const AVAILABLE_DEVICE_CATEGORY = {
  EXECUTIVE: true,
  SENSOR: true,
  VIRTUAL_CONTAINER: true
};
const Wrapper = styled.div`
  flex: 1;
  .ReactVirtualized__Table {
    border: 1px solid lightgray;
  }
  .ReactVirtualized__Table__headerRow {
    background-color: lightgray;
    font-weight: normal;
    font-size: 11px !important;
  }
  .ReactVirtualized__Table__headerColumn {
    display: flex;
    justify-content: center;
    margin: 1px !important;
  }
  .ReactVirtualized__Table__Grid {
    background-color: white;
  }
  .ReactVirtualized__List {
    border-color: lightgray;
    border-style: solid;
    border-width: 0 1px 1px 1px;
  }
  .expandable__table__header {
    background-color: lightgray;
  }
  .ReactVirtualized__Table__row:first-child {
    border-top: 1px solid lightgray;
  }
  .ReactVirtualized__Table__rowColumn {
    line-height: 26px;
  }
  .ReactVirtualized__Table__row {
    margin: 0 !important;
    border-bottom: 1px solid lightgray;
    & > .ReactVirtualized__Table__rowColumn:nth-child(1) {
      border-right: 1px solid lightgray;
    }
  }
`;

class IndicatorForm extends React.Component {
  state = {
    deviceTree: []
  };
  columns = [
    {
      key: 'type',
      title: 'Тип устройства',
      dataIndex: 'type',
      width: 250,
      stepper: true,
      render: (text, record) => (
        <DeviceTypeTreeItem
          record={{ ...record, name: `${record.name} (${record.shortAddressPath})` }}
        />
      )
    }
  ];

  renderAddressCell(text, record) {
    return <EditableCell value={text} editable={false} />;
  }

  onSubmitForm = e => {
    e.preventDefault();
  };

  handleSubmit = e => {
    e.preventDefault();
    const { formValues, onSubmit, dispatch } = this.props;
    onSubmit(formValues);
    dispatch(reset(FORM_NAME));
  };

  // TODO: Разобраться с перерисовкой дерева, а не строить каждый раз заново
  componentWillReceiveProps = newProps => {
    const { formValues } = this.props;
    if (newProps.formValues && newProps.formValues.entityType) {
      if (newProps.formValues.entityType === 'DEVICE') {
        let deviceTree;
        if (!formValues) {
          deviceTree = getDevicesTree(this.getDeviceTreeItems(newProps));
        } else {
          if (
            (formValues.entityType === 'DEVICE' &&
              formValues.entityIds.length !== newProps.formValues.entityIds.length) ||
            formValues.entityType !== newProps.formValues.entityType
          ) {
            deviceTree = getDevicesTree(this.getDeviceTreeItems(newProps));
          }
        }
        if (deviceTree) {
          this.setState({ deviceTree });
        }
      }
    }
  };

  getDeviceTreeItems = props => {
    const { entitiesList, devices } = props;
    let paths = [],
      subtree = [];
    entitiesList.forEach(device => {
      if (device.addressPath) {
        const addressPath = device.addressPath.split(',');
        let path = '';
        for (let i = 0; i + 1 < addressPath.length; i += 2) {
          if (i > 0) path += ',' + addressPath[i];
          path += ',' + addressPath[i + 1];
          if (path !== device.AddressPath) paths.push(path);
        }
      }
    });
    devices.forEach(device => {
      if (paths.includes(device.addressPath)) subtree.push(device);
    });
    return subtree;
  };

  changeStyles = (styleName, value) => {
    const { formValues, dispatch } = this.props;
    if (formValues && formValues.styles) {
      const newStyles = { ...formValues.styles };
      if (newStyles[styleName]) {
        if (newStyles[styleName] === value) {
          delete newStyles[styleName];
        } else {
          newStyles[styleName] = value;
        }
      } else {
        newStyles[styleName] = value;
      }
      if (Object.keys(newStyles).length) {
        dispatch(change(FORM_NAME, 'styles', newStyles));
      } else {
        dispatch(change(FORM_NAME, 'styles', null));
      }
    } else {
      dispatch(change(FORM_NAME, 'styles', { [styleName]: value }));
    }
  };

  onChangeEntityType = value => {
    const { dispatch } = this.props;
    dispatch(change(FORM_NAME, 'entityType', value));
    dispatch(change(FORM_NAME, 'entityIds', [])); // Сбрасываем список сущностей при смене типа
  };

  onAddEntity = ({ rowData }) => {
    const { formValues, dispatch, entitiesHash } = this.props;
    let newEntityIds = formValues && formValues.entityIds ? formValues.entityIds : [];
    switch (formValues.entityType) {
      case 'SCENARIO': {
        newEntityIds = [rowData.id];
        break;
      }
      case 'DEVICE': {
        if (AVAILABLE_DEVICE_CATEGORY[rowData.deviceCategory]) {
          newEntityIds = [...newEntityIds, rowData.id];
        }
        break;
      }
      case 'REGION': {
        if (
          !formValues.entityIds ||
          !formValues.entityIds.length ||
          (formValues.entityIds &&
            formValues.entityIds.length &&
            rowData.region.subsystem === entitiesHash[formValues.entityIds[0]].region.subsystem)
        ) {
          newEntityIds = [...newEntityIds, rowData.id];
        } else {
          message('error', 'Нельзя добавлять зоны с различными подсистемами');
        }
        break;
      }
      default: {
        newEntityIds = [...newEntityIds, rowData.id];
        break;
      }
    }
    dispatch(change(FORM_NAME, 'entityIds', newEntityIds));
  };

  onRemoveEntity = ({ rowData }) => {
    const { formValues, dispatch } = this.props;
    const newEntityIds =
      formValues && formValues.entityIds
        ? formValues.entityIds.filter(entityId => entityId !== rowData.id)
        : [];
    dispatch(change(FORM_NAME, 'entityIds', newEntityIds));
  };

  render() {
    const { pristine, dispatch, entitiesList, entitiesHash, formValues } = this.props;
    const { deviceTree } = this.state;
    return (
      <Modal
        name={FORM_NAME}
        onClose={() => dispatch(reset(FORM_NAME))}
        title="Свойства индикатора"
        width="40%"
      >
        <Form onSubmit={this.onSubmitForm}>
          <Row>
            <Column style={COLUMN_STYLES}>
              <FormItem label="Название">
                <Field component={Input} name="name" />
              </FormItem>
            </Column>
            <Column style={COLUMN_STYLES}>
              <FormItem label="Подтверждение">
                <Field component={Checkbox} name="passwordNeeded" label={'Запрашивать пароль'} />
              </FormItem>
            </Column>
          </Row>
          <FormItem label="Описание">
            <Field
              component={Input}
              type={'textarea'}
              rows={2}
              maxLength={256}
              name="description"
            />
          </FormItem>
          <Row>
            <Column style={COLUMN_STYLES}>
              <FormItem label="Тип индикатора">
                <Field
                  component={Select}
                  name="entityType"
                  onChange={value => this.onChangeEntityType(value)}
                >
                  {Object.values(ENTITY_TYPES).map(entityType => (
                    <Option key={entityType.id} value={entityType.id}>
                      {entityType.name}
                    </Option>
                  ))}
                </Field>
              </FormItem>
            </Column>
            <Column style={COLUMN_STYLES}>
              <FormItem label="Стили">
                <ButtonGroup style={BUTTON_GROUP_STYLES}>
                  <Button
                    type={
                      formValues && formValues.styles && formValues.styles['fontWeight']
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('fontWeight', 'bold')}
                  >
                    <b>B</b>
                  </Button>
                  <Button
                    type={
                      formValues && formValues.styles && formValues.styles['fontStyle']
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('fontStyle', 'italic')}
                  >
                    <i>i</i>
                  </Button>
                  <Button
                    type={
                      formValues && formValues.styles && formValues.styles['textDecoration']
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('textDecoration', 'underline')}
                  >
                    <u>U</u>
                  </Button>
                </ButtonGroup>
                <ButtonGroup style={BUTTON_GROUP_STYLES}>
                  <Button
                    type={
                      formValues &&
                      formValues.styles &&
                      formValues.styles['textAlign'] &&
                      formValues.styles['textAlign'] === 'left'
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('textAlign', 'left')}
                  >
                    <Icon type="menu-fold" />
                  </Button>
                  <Button
                    type={
                      formValues &&
                      formValues.styles &&
                      formValues.styles['textAlign'] &&
                      formValues.styles['textAlign'] === 'center'
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('textAlign', 'center')}
                  >
                    <Icon type="bars" />
                  </Button>
                  <Button
                    type={
                      formValues &&
                      formValues.styles &&
                      formValues.styles['textAlign'] &&
                      formValues.styles['textAlign'] === 'right'
                        ? 'primary'
                        : 'default'
                    }
                    style={FONT_BUTTON_STYLES}
                    onClick={() => this.changeStyles('textAlign', 'right')}
                  >
                    <Icon type="menu-unfold" />
                  </Button>
                </ButtonGroup>
                <InputNumber
                  precision={0}
                  value={
                    formValues && formValues.styles && formValues.styles['fontSize']
                      ? formValues.styles['fontSize']
                      : 12
                  }
                  min={8}
                  max={36}
                  style={FONT_SIZE_STYLES}
                  onChange={value => this.changeStyles('fontSize', value)}
                />
              </FormItem>
            </Column>
          </Row>
          {formValues && formValues.entityType && formValues.entityType !== 'NONE' ? (
            <Wrapper style={WRAPPER_STYLES}>
              <AutoSizer>
                {({ height, width }) => {
                  const tableWidth = (width - 32) / 2;
                  return (
                    <Row>
                      <FormItem label="Добавленные">
                        <Column style={COLUMN_STYLES}>
                          {formValues.entityType === 'REGION' ? (
                            <RegionList
                              height={height - 40}
                              width={tableWidth}
                              regions={formValues.entityIds.map(entityId => entitiesHash[entityId])}
                              onRowDoubleClick={this.onRemoveEntity}
                            />
                          ) : null}
                          {formValues.entityType === 'SCENARIO' ? (
                            <ScenarioList
                              height={height - 40}
                              width={tableWidth}
                              scenarios={formValues.entityIds.map(
                                entityId => entitiesHash[entityId]
                              )}
                              onRowDoubleClick={this.onRemoveEntity}
                            />
                          ) : null}
                          {formValues.entityType === 'DEVICE' ? (
                            <DeviceList
                              height={height - 40}
                              width={tableWidth}
                              devices={formValues.entityIds.map(entityId => entitiesHash[entityId])}
                              onRowDoubleClick={this.onRemoveEntity}
                            />
                          ) : null}
                        </Column>
                      </FormItem>
                      <FormItem label="Все">
                        <Column style={COLUMN_STYLES}>
                          {formValues.entityType === 'REGION' ? (
                            <RegionList
                              disableCondition={record => {
                                if (formValues.entityIds && formValues.entityIds.length) {
                                  if (
                                    record.region.subsystem !==
                                    entitiesHash[formValues.entityIds[0]].region.subsystem
                                  )
                                    return true;
                                }
                                return false;
                              }}
                              height={height - 40}
                              width={tableWidth}
                              regions={entitiesList}
                              onRowDoubleClick={this.onAddEntity}
                            />
                          ) : null}
                          {formValues.entityType === 'SCENARIO' ? (
                            <ScenarioList
                              height={height - 40}
                              width={tableWidth}
                              scenarios={entitiesList}
                              onRowDoubleClick={this.onAddEntity}
                            />
                          ) : null}

                          {formValues.entityType === 'DEVICE' ? (
                            <Tree
                              columns={this.columns}
                              nodes={deviceTree}
                              height={height - 63}
                              isEmpty={!deviceTree || !deviceTree.length ? true : false}
                              width={tableWidth}
                              onRowDoubleClick={node => this.onAddEntity({ rowData: node })}
                            />
                          ) : null}
                        </Column>
                      </FormItem>
                    </Row>
                  );
                }}
              </AutoSizer>
            </Wrapper>
          ) : null}
          <Button type="primary" onClick={this.handleSubmit} disabled={pristine}>
            Сохранить
          </Button>
        </Form>
      </Modal>
    );
  }
}

IndicatorForm = reduxForm({ form: FORM_NAME, enableReinitialize: true })(IndicatorForm);

const mapStateToProps = state => {
  const { form } = state;
  let entitiesList = [];
  let entitiesHash = {};
  const devices = getCurrentProjectDeviceList(state);
  if (
    form[FORM_NAME] &&
    form[FORM_NAME].values &&
    form[FORM_NAME].values.entityType &&
    form[FORM_NAME].values.entityType !== 'NONE'
  ) {
    switch (form[FORM_NAME].values.entityType) {
      case 'REGION': {
        entitiesList = getCurrentProjectRegionViews(state).filter(
          region => !form[FORM_NAME].values.entityIds.includes(region.id)
        );
        entitiesHash = getCurrentProjectRegionsHash(state);
        break;
      }
      case 'SCENARIO': {
        entitiesList = getCurrentProjectScenarios(state).filter(
          scenario => !form[FORM_NAME].values.entityIds.includes(scenario.id)
        );
        entitiesHash = getCurrentProjectScenariosHash(state);
        break;
      }
      case 'DEVICE': {
        entitiesList = devices.filter(
          device =>
            !form[FORM_NAME].values.entityIds.includes(device.id) &&
            AVAILABLE_DEVICE_CATEGORY[device.deviceCategory] &&
            !device.disabled
        );
        entitiesHash = getCurrentProjectDevicesHash(state);
        break;
      }
      default: {
        break;
      }
    }
  }
  return {
    devices,
    entitiesList,
    entitiesHash,
    formValues: form[FORM_NAME] && form[FORM_NAME].values,
    initialValues: state.widgets.selectedIndicator
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorForm);
