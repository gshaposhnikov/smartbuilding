import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, Field, reset } from 'redux-form';
import { Button } from 'antd';

import Input from 'components/Form/Input';

const FORM_NAME = 'deviceNoteForm';

const DeviceNote = props => {
  const { onSubmit, dispatch, formValues, pristine, submitting } = props;
  const handleSubmit = e => {
    e.preventDefault();
    onSubmit(formValues);
    dispatch(reset(FORM_NAME));
  };

  return (
    <Form onSubmit={handleSubmit} style={{ display: 'flex' }}>
      <Field component={Input} name="message" rows={1} type="textarea" />
      <Button type="primary" htmlType="submit" disabled={pristine || submitting}>
        Сохранить
      </Button>
    </Form>
  );
};

const DeviceNoteForm = reduxForm({ form: FORM_NAME })(DeviceNote);

const mapStateToProps = state => {
  return {
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {}
  };
};
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceNoteForm);
