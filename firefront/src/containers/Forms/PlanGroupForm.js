import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import { Form, Input, Button } from 'antd';
import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import { initializeValues } from 'helpers/form';
import { getCurrentProjectPlanGroups } from 'helpers/currentProject';

class planGroupForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    isEdit: PropTypes.bool,
    form: PropTypes.object, // Генерируется Form.create
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func,
    planGroups: PropTypes.array
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const { modalName, form, isEdit, planGroups } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal name={modalName} title={isEdit ? 'Редактирование группы' : 'Новая группа'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Название">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Имя обязательно' }],
              initialValue: `Группа ${planGroups ? planGroups.length + 1 : 1}`
            })(<Input maxLength={20} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Описание">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} maxLength={256} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit">
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

planGroupForm = FormCreate({
  mapPropsToFields(props) {
    return props.isEdit ? initializeValues(props.currentPlanGroup) : null;
  }
})(planGroupForm);

const mapStateToProps = state => {
  return {
    modals: state.modals,
    planGroups: getCurrentProjectPlanGroups(state)
  };
};

planGroupForm = connect(mapStateToProps, null)(planGroupForm);

export default planGroupForm;
