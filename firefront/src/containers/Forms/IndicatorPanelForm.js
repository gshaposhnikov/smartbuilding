import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Input, Button, InputNumber } from 'antd';

import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import Modal from 'containers/Modal';

import { initializeValues } from 'helpers/form';

class IndicatorPanelForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object,
    modals: PropTypes.object,
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };

  checkRemovedCellsContent = () => {
    const { selectedIndicatorPanel, form } = this.props;
    const { getFieldValue } = form;
    if (!selectedIndicatorPanel || !selectedIndicatorPanel.indicators) return false;
    if (getFieldValue('rowCount') < selectedIndicatorPanel.rowCount) {
      for (let i = getFieldValue('rowCount'); i < selectedIndicatorPanel.rowCount; ++i) {
        for (let j = 0; j < selectedIndicatorPanel.colCount; ++j) {
          if (selectedIndicatorPanel.indicators[i][j].entityIds.length) {
            return true;
          }
        }
      }
    }
    if (getFieldValue('colCount') < selectedIndicatorPanel.colCount) {
      for (let i = 0; i < selectedIndicatorPanel.rowCount; ++i) {
        for (let j = getFieldValue('colCount'); j < selectedIndicatorPanel.colCount; ++j) {
          if (selectedIndicatorPanel.indicators[i][j].entityIds.length) {
            return true;
          }
        }
      }
    }

    return false;
  };

  render() {
    const { modalName, form, isEdit } = this.props;
    const { getFieldDecorator } = form;
    const hasDeletedCellsContent = isEdit ? this.checkRemovedCellsContent() : false;
    return (
      <Modal
        name={modalName}
        title={isEdit ? 'Редактирование панели индикаторов' : 'Новая панель индикаторов'}
      >
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Название">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Имя обязательно' }],
              initialValue: `Новая панель индикаторов`
            })(<Input maxLength={32} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Кол-во столбцов">
            {getFieldDecorator('colCount', {
              rules: [{ required: true, message: 'Обязательное поле' }],
              initialValue: 3
            })(<InputNumber precision={0} min={3} max={20} step={1} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Кол-во строк">
            {getFieldDecorator('rowCount', {
              rules: [{ required: true, message: 'Обязательное поле' }],
              initialValue: 3
            })(<InputNumber precision={0} min={3} max={100} step={1} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Описание">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} maxLength={256} />)}
          </FormItem>
          <FormItem
            {...ItemLayoutWithoutLabel}
            help={isEdit && hasDeletedCellsContent ? 'В удаляемых индикаторах есть объекты' : ''}
            validateStatus={isEdit && hasDeletedCellsContent ? 'error' : null}
          >
            <Button type="primary" htmlType="submit">
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

IndicatorPanelForm = FormCreate({
  mapPropsToFields(props) {
    return props.isEdit ? initializeValues(props.selectedIndicatorPanel || {}) : null;
  }
})(IndicatorPanelForm);

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;
  return {
    modals: state.modals,
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.indicators.panels[state.currentProjectId]
        ? state.indicators.panels[state.currentProjectId][selectedIndicatorPanelId]
        : null
  };
};

export default connect(
  mapStateToProps,
  null
)(IndicatorPanelForm);
