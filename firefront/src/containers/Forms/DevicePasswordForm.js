import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';
import { Form, Button, Input, Select } from 'antd';

import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

class DevicePasswordForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object,
    onSubmit: PropTypes.func,
    modals: PropTypes.object,
    deviceProfileViewsHash: PropTypes.object,
    currentDevice: PropTypes.object
  };
  state = {
    confirmDirty: false,
    userList: [],
    passwordDescription: '',
    pattern: ''
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const { deviceProfileViewsHash, currentDevice } = nextProps;

      const deviceProfileView =
        deviceProfileViewsHash && deviceProfileViewsHash[currentDevice.deviceProfileId]
          ? deviceProfileViewsHash[currentDevice.deviceProfileId]
          : {};
      const userList =
        deviceProfileView.id && deviceProfileView.deviceProfile.deviceUsers
          ? deviceProfileView.deviceProfile.deviceUsers.map(userId => ({
              value: userId,
              name: deviceProfileView.deviceUserNames[userId]
            }))
          : [];
      const passwordDescription = deviceProfileView.userPasswordDescription;
      const pattern = deviceProfileView.deviceProfile
        ? deviceProfileView.deviceProfile.userPasswordPattern
        : '';
      this.setState({ userList, passwordDescription, pattern });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  }

  validateToNextPassword(rule, value, callback) {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  validateToFirstPassword(rule, value, callback) {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Пароли должны совпадать');
    } else {
      callback();
    }
  }

  checkValid(rule, value, callback) {
    const { passwordDescription, pattern } = this.state;
    if (new RegExp(pattern).test(value)) callback();
    else callback(passwordDescription);
  }

  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  render() {
    const { modalName, form } = this.props;
    const { userList, passwordDescription } = this.state;
    const { getFieldDecorator } = form;
    return (
      <Modal name={modalName} title={'Задать пароль'}>
        <Form onSubmit={this.handleSubmit.bind(this)}>
          <FormItem {...ItemLayoutWithLabel} label="Пользователь">
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Выберите пользователя' }]
            })(
              <Select>
                {userList.map(user => {
                  return (
                    <Option value={user.value} key={user.value}>
                      {user.name}
                    </Option>
                  );
                })}
              </Select>
            )}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Пароль" help={passwordDescription}>
            {getFieldDecorator('password', {
              rules: [
                { validator: this.checkValid.bind(this) },
                { validator: this.validateToNextPassword.bind(this) }
              ],
              initialValue: ''
            })(<Input type="password" />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Подтверждение">
            {getFieldDecorator('confirm', {
              rules: [
                { validator: this.checkValid.bind(this) },
                { validator: this.validateToFirstPassword.bind(this) }
              ],
              initialValue: ''
            })(<Input type="password" onBlur={this.handleConfirmBlur.bind(this)} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit">
              {'Задать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

DevicePasswordForm = FormCreate({
  mapPropsToFields(props) {
    return null;
  }
})(DevicePasswordForm);

const mapStateToProps = state => {
  return {
    modals: state.modals,
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    currentDevice: state.widgets.currentDevice
  };
};

DevicePasswordForm = connect(
  mapStateToProps,
  null
)(DevicePasswordForm);

export default DevicePasswordForm;
