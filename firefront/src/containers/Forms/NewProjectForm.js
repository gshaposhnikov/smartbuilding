import React, { Component } from 'react';
import Modal from 'containers/Modal';

import Form, { FormItem } from 'components/Form';
import Input from 'components/Form/Input';

import { Button } from 'antd';
import { Field, reduxForm } from 'redux-form';

class NewProjectForm extends Component {
  render() {
    const { handleSubmit } = this.props;

    return (
      <Modal name="newProject" title="Новый проект">
        <Form onSubmit={handleSubmit}>
          <FormItem validateStatus={''} help={''} label="Название проекта">
            <Field component={Input} name="name" placeholder="Новый проект" />
          </FormItem>
          <FormItem validateStatus={''} help={''} label="Версия">
            <Field component={Input} rows={4} name="version" placeholder="Версия" />
          </FormItem>
          <FormItem validateStatus={''} help={''} label="Примечания">
            <Field
              component={Input}
              type="textarea"
              rows={4}
              name="description"
              placeholder="Примечания"
            />
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={false}>
              Создать новый проект
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default reduxForm({
  form: 'newProject'
})(NewProjectForm);
