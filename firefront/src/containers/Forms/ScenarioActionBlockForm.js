import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Row, Col, notification } from 'antd';
import { Field, FieldArray, reduxForm, initialize, reset } from 'redux-form';
import moment from 'moment';

import Input from 'components/Form/Input';
import Form, { FormItem, ItemLayoutWithLabel } from 'components/Form';
import InputNumber from 'components/Form/InputNumber';
import Timer from 'components/Timer';
import Table, { Column } from 'components/Table';

import Modal from 'containers/Modal';
import Select from 'containers/Selects';

const FORM_NAME = 'scenarioActionBlock';
const COMMANDS = [
  {
    id: 'SHOW_MESSAGE',
    name: 'Показать сообщение'
  },
  {
    id: 'COUNTDOWN',
    name: 'Обратный отсчет'
  }
];
const TABLE_STYLES = { minHeight: '200px' };
const required = value => (value ? undefined : 'Required');

/**
 * Сброс значений формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};

const actionsTable = ({ fields, onDeleteRowClick, actionValues, onTestActionClick }) => (
  <div>
    <Table
      dataSource={[...fields.getAll(), {}]}
      rowKey={(record, index) => index}
      tableStyle={TABLE_STYLES}
    >
      <Column
        title="Команда"
        dataIndex="command"
        key="command"
        width="17%"
        render={(command, record, index) => (
          <Field
            component={Select}
            name={`computerActions[${index}].command`}
            itemsSource={COMMANDS}
            dropdownMatchSelectWidth={false}
          />
        )}
      />
      <Column
        title="Заголовок"
        dataIndex="title"
        key="title"
        width="25%"
        render={(title, record, index) => {
          if (actionValues[index]) {
            return (
              <Field
                validate={actionValues[index].command !== 'SHOW_MESSAGE' ? [] : [required]}
                maxLength={50}
                disabled={actionValues[index].command !== 'SHOW_MESSAGE'}
                component={Input}
                name={`computerActions[${index}].title`}
              />
            );
          } else {
            return <div />;
          }
        }}
      />
      <Column
        title="Время"
        dataIndex="countdownTimeSec"
        key="countdownTimeSec"
        width={94}
        render={(title, record, index) => {
          if (actionValues[index]) {
            return (
              <Field
                disabled={actionValues[index].command !== 'COUNTDOWN'}
                min={0}
                component={InputNumber}
                name={`computerActions[${index}].countdownTimeSec`}
              />
            );
          } else {
            return <div />;
          }
        }}
      />
      <Column
        title="Сообщение"
        maxLength={200}
        dataIndex="message"
        key="message"
        render={(title, record, index) => {
          if (actionValues[index]) {
            return (
              <Field
                validate={[required]}
                component={Input}
                type="textarea"
                rows={1}
                name={`computerActions[${index}].message`}
              />
            );
          } else {
            return <div />;
          }
        }}
      />
      <Column
        key="test"
        width={46}
        render={(text, record, index) =>
          actionValues[index] ? (
            <Button
              disabled={
                actionValues[index].command === 'SHOW_MESSAGE'
                  ? !actionValues[index].title || !actionValues[index].message
                  : !actionValues[index].message
              }
              icon="play-circle"
              onClick={() => onTestActionClick(index)}
            />
          ) : (
            <div />
          )
        }
      />
      <Column
        key="delete"
        width={46}
        render={(text, record, index) =>
          actionValues[index] ? (
            <Button icon="delete" onClick={() => onDeleteRowClick(index)} />
          ) : (
            <div />
          )
        }
      />
    </Table>
  </div>
);

/**
 * Модальная форма редактирования блока времени сценария.
 */
class ScenarioActionBlockForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    handleSubmit: PropTypes.func,
    isEdit: PropTypes.bool,
    blockTypes: PropTypes.array,
    currentValues: PropTypes.object,
    array: PropTypes.object,
    valid: PropTypes.bool,
    dirty: PropTypes.bool
  };
  state = {
    visibleTimers: [],
    timeResidue: {}
  };
  interval = null;
  intervalEnabled = false;
  testStopped = true;

  testAction = action => {
    const { currentValues } = this.props;
    switch (action.command) {
      case 'SHOW_MESSAGE': {
        const args = {
          message: action.title || '',
          description: action.message || '',
          duration: 0
        };
        notification.open(args);
        break;
      }
      case 'COUNTDOWN': {
        this.testStopped = false;
        this.setState(
          {
            visibleTimers: [
              {
                id: action.command,
                name: `${currentValues.name}(${action.message})`,
                state: 'started',
                timeStart: moment().unix(),
                delay: action.countdownTimeSec
              }
            ],
            timeResidue: {}
          },
          () => {
            if (this.intervalEnabled) {
              clearInterval(this.interval);
            }
            this.intervalEnabled = true;
            this.interval = setInterval(() => this.updateTime(), 1000);
          }
        );
        break;
      }

      default:
        break;
    }
  };

  updateTime = () => {
    const { visibleTimers } = this.state;
    const timeResidue = {};
    const now = moment().unix();
    const newVisibleTimers = this.testStopped
      ? []
      : visibleTimers.filter(timer => {
          const { timeStart, delay, id } = timer;
          if (!delay || delay <= 0) return false;
          if (timer.state === 'started') {
            const residue = timeStart + delay - now;
            if (residue <= 0) return false;
            timeResidue[id] = residue;
          }
          return true;
        });
    if (!newVisibleTimers.length) {
      this.testStopped = false;
      this.intervalEnabled = false;
      clearInterval(this.interval);
    }
    this.setState({
      timeResidue,
      visibleTimers: newVisibleTimers
    });
  };

  stopTest = () => {
    this.testStopped = true;
    if (this.intervalEnabled) {
      this.intervalEnabled = false;
      clearInterval(this.interval);
    }
    this.setState({ visibleTimers: [], timeResidue: {} });
  };

  render() {
    const {
      modalName,
      handleSubmit,
      isEdit,
      blockTypes,
      valid,
      dirty,
      array,
      currentValues
    } = this.props;
    const { visibleTimers, timeResidue } = this.state;
    return (
      <Modal
        name={modalName}
        title="Действия на компьютере"
        width="70%"
        onClose={() => this.stopTest()}
      >
        {visibleTimers.length ? (
          <Timer visibleTimers={visibleTimers} timeResidue={timeResidue} />
        ) : null}

        <Form onSubmit={handleSubmit}>
          <Row>
            <Col span={8}>
              <FormItem {...ItemLayoutWithLabel} label="Блок">
                <Field component={Select} name="blockType" itemsSource={blockTypes} disabled />
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Название блока">
                <Field component={Input} name="name" />
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Задержка, в секундах ">
                <Field component={InputNumber} name="timeDelaySec" min={0} />
              </FormItem>
            </Col>
            <Col span={16}>
              <FieldArray
                name="computerActions"
                onDeleteRowClick={index => {
                  array.remove('computerActions', index);
                }}
                component={actionsTable}
                actionValues={currentValues.computerActions}
                onTestActionClick={index => this.testAction(currentValues.computerActions[index])}
              />
            </Col>
          </Row>
          <Button type="primary" htmlType="submit" disabled={!valid || (isEdit && !dirty)}>
            {isEdit ? 'Изменить' : 'Добавить'}
          </Button>
        </Form>
      </Modal>
    );
  }
}

ScenarioActionBlockForm = reduxForm({
  form: FORM_NAME
})(ScenarioActionBlockForm);

const mapStateToProps = state => {
  return {
    blockTypes: state.scenarioConstants.dictionaries
      ? state.scenarioConstants.dictionaries.timeLineBlockTypes
      : [],
    currentValues: state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioActionBlockForm);
