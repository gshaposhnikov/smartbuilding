import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form';

import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import Select from 'containers/Selects';
import Form, { FormItem, ItemLayoutWithLabel } from 'components/Form';

const FORM_STYLES = { padding: 24 };

class ScenarioParamsForm extends Component {
  /* TODO: добавить propTypes для всех входящих props */

  render() {
    const {
      scenarioConstants,
      currentValues,
      selectedScenario,
      flagNames,
      isActiveScenario
    } = this.props;
    return (
      <div>
        {selectedScenario && selectedScenario.scenarioPurpose === 'EXEC_BY_TACTICS' && (
          <Form style={FORM_STYLES}>
            <FormItem {...ItemLayoutWithLabel} label="Включение">
              <Field
                component={Input}
                type="textarea"
                rows={8}
                name="startLogicSentence"
                disabled
              />
            </FormItem>
          </Form>
        )}
        {selectedScenario && selectedScenario.scenarioPurpose === 'EXEC_BY_LOGIC' && (
          <Form style={FORM_STYLES}>
            <FormItem {...ItemLayoutWithLabel} label="Включение">
              <Field
                component={Input}
                type="textarea"
                rows={8}
                name="startLogicSentence"
                disabled
              />
            </FormItem>
            <FormItem {...ItemLayoutWithLabel} label="Тип остановки сценария" required={true}>
              <Field
                component={Select}
                name="stopType"
                itemsSource={
                  scenarioConstants.dictionaries ? scenarioConstants.dictionaries.stopTypes : []
                }
                disabled={isActiveScenario}
              />
            </FormItem>
            {currentValues.stopType === 'BY_LOGIC' && (
              <FormItem {...ItemLayoutWithLabel} label="Выключение">
                <Field
                  component={Input}
                  type="textarea"
                  rows={8}
                  name="stopLogicSentence"
                  disabled
                />
              </FormItem>
            )}
            <FormItem {...ItemLayoutWithLabel} label={flagNames.manualStartStopAllowedName}>
              <Field
                component={Checkbox}
                name="manualStartStopAllowed"
                disabled={isActiveScenario}
              />
            </FormItem>
            <FormItem {...ItemLayoutWithLabel} label={flagNames.stopOnGlobalResetName}>
              <Field component={Checkbox} name="stopOnGlobalReset" disabled={isActiveScenario} />
            </FormItem>
          </Form>
        )}
        {selectedScenario && selectedScenario.scenarioPurpose === 'EXEC_BY_INVOKE' && (
          <Form style={FORM_STYLES}>
            <FormItem {...ItemLayoutWithLabel} label="Тип остановки сценария" required={true}>
              <Field
                component={Select}
                name="stopType"
                itemsSource={
                  scenarioConstants.dictionaries
                    ? scenarioConstants.dictionaries.stopTypes.filter(
                        type => type.id !== 'BY_LOGIC'
                      )
                    : []
                }
                disabled={isActiveScenario}
              />
            </FormItem>
            <FormItem {...ItemLayoutWithLabel} label={flagNames.manualStartStopAllowedName}>
              <Field
                component={Checkbox}
                name="manualStartStopAllowed"
                disabled={isActiveScenario}
              />
            </FormItem>
          </Form>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    scenarioConstants: state.scenarioConstants
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioParamsForm);
