import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button, Icon, Upload } from 'antd';
const FormItem = Form.Item;

import Checkbox from 'components/Form/Checkbox';

import Modal from 'containers/Modal';

const MAX_FILE_SIZE = 15728640;
const INITIAL_STATE = {
  file: null,
  fileContent: null,
  fileValid: true,
  errorMessage: '',
  updateAllControlDeviceFw: false
};

class ChooseFirmwareForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    onSubmit: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = INITIAL_STATE;
  }

  handleSubmit = e => {
    e.preventDefault();

    this.setState(() => {
      this.props.onSubmit({ ...this.state });
      return INITIAL_STATE;
    });
  };

  /* Метод, которые подменяет AJAX-запрос сохранением файла в state */
  putFileToStateInsteadAJAXRequest(event) {
    const { file } = event;

    this.setState({ file: { name: file.name, type: file.type, size: file.size, uid: file.uid } });
    if (file.size > MAX_FILE_SIZE) {
      this.setState({ fileValid: false, errorMessage: 'Размер файла не должен превышать 15 мб' });
    } else if (file.name.substr(-4).toLowerCase() !== '.hxc') {
      this.setState({ fileValid: false, errorMessage: 'Тип файла должен быть .hxc' });
    } else {
      const reader = new FileReader();
      reader.onload = () => {
        this.setState({ fileValid: true, fileContent: btoa(reader.result) });
      };
      reader.onerror = () => {
        this.setState({ fileValid: false, errorMessage: 'Ошибка чтения файла' });
      };
      reader.readAsBinaryString(file);
    }
  }

  render() {
    const { modalName } = this.props;
    const { file, fileContent, fileValid, errorMessage, updateAllControlDeviceFw } = this.state;

    return (
      <Modal name={modalName} title={'Файл прошивки'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            label="Файл"
            help={fileValid ? (fileContent ? '' : 'Файл не выбран') : errorMessage}
            validateStatus={fileValid ? '' : 'error'}
          >
            <Upload
              customRequest={this.putFileToStateInsteadAJAXRequest.bind(this)}
              fileList={file ? [file] : []}
              multiple={false}
              accept=".hxc"
              onRemove={() => {
                this.setState({
                  file: null,
                  fileContent: null,
                  fileValid: true,
                  errorMessage: ''
                });
              }}
            >
              <Button>
                <Icon type="upload" /> {'Выберите файл'}
              </Button>
            </Upload>
          </FormItem>
          <FormItem>
            <Checkbox
              label="Обновить для всех приборов"
              checked={updateAllControlDeviceFw}
              input={{
                onChange: ({ target: { checked: value } }) =>
                  this.setState({ updateAllControlDeviceFw: !updateAllControlDeviceFw })
              }}
            />
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={!fileValid || !fileContent}>
              {'Отправить'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};

ChooseFirmwareForm = connect(mapStateToProps)(ChooseFirmwareForm);

export default ChooseFirmwareForm;
