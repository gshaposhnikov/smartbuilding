import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, InputNumber, Button, Input, Icon, Upload } from 'antd';
const isJson = require('is-json');

import { FormItem, FormCreate } from 'components/Form';

import Modal from 'containers/Modal';
import ProfileSelect from 'containers/Selects/ProfilesSelect';

import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const MAX_FILE_SIZE = 15728640;
let lastSelectedLine = 1;

class NewDeviceForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object, // Генерируется Form.create
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    handleSubmit: PropTypes.func,
    deviceProfileViewsHash: PropTypes.object,
    currentDevice: PropTypes.object,
    deviceGroups: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.state = {
      currentDeviceProfileView: {},
      connectableDeviceProfileGroups: [],
      fileList: []
    };
    this.formSubmitted = false;
    this.lastSelectedAddressByDeviceCategory = {};
  }

  handleSubmit = e => {
    const { form, isImport, onSubmit } = this.props;
    e.preventDefault();
    form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      if (isImport) {
        const { controlDeviceEntities, lineNo, lineAddress } = fieldsValue;
        const controlDevice = controlDeviceEntities.devices.find(
          device => device.deviceCategory === 'CONTROL'
        );
        if (controlDevice) {
          const { currentDevice } = this.props;
          controlDevice.parentDeviceId = currentDevice.id;
          controlDevice.lineNo = lineNo;
          controlDevice.lineAddress = lineAddress;
          onSubmit(controlDeviceEntities);
          this.setState({ fileList: [] });
        }
      } else {
        this.formSubmitted = true;
        onSubmit(fieldsValue);
      }
    });
  };

  setFileData = (errorMessage = '', fileList = []) => {
    this.setState({ importedConfigError: errorMessage, fileList });
  };

  handleFileUpload = event => {
    const { file } = event;
    if (file.size > MAX_FILE_SIZE) this.setFileData('Размер файла не должен превышать 15мб');
    else {
      const reader = new FileReader();
      reader.onload = data => {
        if (!isJson(data.currentTarget.result)) this.setFileData('Содержимое файла повреждено');
        else {
          const { form } = this.props;
          const controlDeviceEntities = JSON.parse(reader.result);
          if (
            controlDeviceEntities.devices &&
            controlDeviceEntities.devices.length &&
            controlDeviceEntities.devices.find(device => device.deviceCategory === 'CONTROL')
          ) {
            this.setFileData('', [
              {
                uid: 1,
                name: file.name,
                status: 'done'
              }
            ]);
            form.setFieldsValue({
              controlDeviceEntities
            });
          } else this.setFileData('Конфигурация не содержит прибор');
        }
      };
      reader.readAsText(file);
    }
  };

  clearFileList = () => {
    const { form } = this.props;
    this.setFileData('');
    form.setFieldsValue({
      controlDeviceEntities: null
    });
  };

  lineAddressSelector = (deviceProfileId, currentLineNo = 1) => {
    const { deviceProfileViewsHash, currentDevice, form } = this.props;
    const { getFieldValue, setFieldsValue } = form;
    const newDeviceProfileView = deviceProfileId
      ? deviceProfileViewsHash[deviceProfileId]
      : deviceProfileViewsHash[getFieldValue('deviceProfileId')];

    if (newDeviceProfileView && newDeviceProfileView.deviceProfile) {
      const newDeviceProfile = newDeviceProfileView.deviceProfile;
      const internalDevices = newDeviceProfile.internalDevices;
      const lineAddressRanges = currentDevice.lineAddressRanges;
      /* Определяем количество добавляемых устройств */
      const devicesCount =
        internalDevices.reduce((internalDevicesCount, item) => {
          if (item.addressType === 'GENERIC') {
            return internalDevicesCount + item.nDevices;
          }
          return internalDevicesCount;
        }, 0) || 1;

      /* Ищем диапазон свобоных адресов, чтобы хватило место добавляемому кол-ву устройств */
      if (lineAddressRanges) {
        const lastSelectedAddress =
          this.lastSelectedAddressByDeviceCategory[currentDevice.deviceCategory] || 0;
        let rangeFound = false;
        for (let range of lineAddressRanges[currentLineNo]) {
          if (
            !lastSelectedAddress ||
            (lastSelectedAddress && range.firstAddress > lastSelectedAddress)
          )
            if (range.addressCount >= devicesCount) {
              // Если необходимый диапазон был найден, то завершаем поиск
              setFieldsValue({
                lineAddress: range.firstAddress
              });
              rangeFound = true;
              break;
            }
        }
        /* Если не было найдено ни одного свободного после последнего заданного */
        if (!rangeFound && lastSelectedAddress) {
          for (let range of lineAddressRanges[currentLineNo]) {
            if (range.addressCount >= devicesCount) {
              // Если необходимый диапазон был найден, то завершаем поиск
              setFieldsValue({
                lineAddress: range.firstAddress
              });
              break;
            }
          }
        }
        /* Если у устройства есть дочерние устройства */
      } else if (currentDevice.children) {
        let address =
          this.lastSelectedAddressByDeviceCategory[currentDevice.deviceCategory || 'ROOT'] || 1;
        const checkAddress = childDevice => {
          if (childDevice.fullAddressPath) {
            let childDeviceAddress = parseInt(childDevice.fullAddressPath, 10);
            return childDeviceAddress === address;
          }
          return false;
        };
        for (; address < 255; ++address) {
          if (!currentDevice.children.find(checkAddress)) {
            break;
          }
        }
        setFieldsValue({
          lineAddress: address
        });
      }
    }
  };

  fetchData(currentDevice, deviceProfileViewsHash, deviceGroups) {
    if (
      currentDevice &&
      currentDevice.id &&
      deviceProfileViewsHash &&
      deviceGroups &&
      deviceGroups.length
    ) {
      const currentDeviceProfileView = deviceProfileViewsHash[currentDevice.deviceProfileId];
      if (currentDeviceProfileView) {
        const currentDeviceProfile = currentDeviceProfileView.deviceProfile;
        const connectableDeviceProfileViews = currentDeviceProfile.outputDeviceProfileIds
          ? currentDeviceProfile.outputDeviceProfileIds.map(
              outputDeviceProfileId => deviceProfileViewsHash[outputDeviceProfileId]
            )
          : [];
        const connectableDeviceProfileGroups = deviceGroups.map(deviceGroup => ({
          ...deviceGroup,
          children: connectableDeviceProfileViews.filter(
            profile => profile.deviceProfile.deviceGroup === deviceGroup.id
          )
        }));

        this.setState({
          currentDeviceProfileView,
          connectableDeviceProfileGroups
        });
      } else if (currentDevice.outputConnectionType) {
        const connectableDeviceProfileViews = Object.values(deviceProfileViewsHash).filter(
          deviceProfileView => {
            return deviceProfileView.deviceProfile.connectionTypes.includes(
              currentDevice.outputConnectionType
            );
          }
        );
        const connectableDeviceProfileGroups = deviceGroups.map(deviceGroup => ({
          ...deviceGroup,
          children: connectableDeviceProfileViews.filter(
            profile => profile.deviceProfile.deviceGroup === deviceGroup.id
          )
        }));
        this.setState({
          currentDeviceProfileView: {},
          connectableDeviceProfileGroups
        });
      } else this.setState({ currentDeviceProfileView: {}, connectableDeviceProfileGroups: [] });
    }
  }

  componentDidMount = () => {
    const { currentDevice, deviceProfileViewsHash, deviceGroups } = this.props;
    this.fetchData(currentDevice, deviceProfileViewsHash, deviceGroups);
  };

  componentWillReceiveProps = nextProps => {
    const { currentDevice, deviceProfileViewsHash, deviceGroups, modals, modalName } = this.props;
    if (
      this.lastDeviceProfileId !== nextProps.form.getFieldValue('deviceProfileId') ||
      currentDevice !== nextProps.currentDevice ||
      deviceProfileViewsHash !== nextProps.deviceProfileViewsHash ||
      deviceGroups !== nextProps.deviceGroups
    ) {
      this.fetchData(
        nextProps.currentDevice,
        nextProps.deviceProfileViewsHash,
        nextProps.deviceGroups
      );
    }
    /* Если форма была успешно закрыта, то запоминаем последний заданный адрес */
    if (
      modals[modalName] &&
      !nextProps.modals[nextProps.modalName] &&
      this.formSubmitted &&
      !nextProps.isImport
    ) {
      this.lastSelectedAddressByDeviceCategory = {
        ...this.lastSelectedAddressByDeviceCategory,
        [currentDevice.deviceCategory || 'ROOT']: nextProps.form.getFieldValue('lineAddress')
      };
      this.formSubmitted = false;
    }
    /* Сохраняем последний выбранный профиль */
    this.lastDeviceProfileId = nextProps.form.getFieldValue('deviceProfileId');
  };

  render() {
    const {
      currentDevice,
      form,
      modalName,
      isImport,
      importConfigInProgress,
      createDeviceInProgress
    } = this.props;
    const { getFieldDecorator, getFieldValue } = form;
    const {
      currentDeviceProfileView,
      connectableDeviceProfileGroups,
      importedConfigError,
      fileList
    } = this.state;
    const currentDeviceProfile = currentDeviceProfileView.deviceProfile || {};

    return (
      <Modal name={modalName} title={isImport ? 'Импорт конфигурации' : 'Подключение устройства'}>
        <Form onSubmit={this.handleSubmit}>
          {isImport ? (
            <FormItem
              label="Файл"
              help={importedConfigError ? importedConfigError : ''}
              validateStatus={importedConfigError ? 'error' : null}
            >
              {getFieldDecorator('controlDeviceEntities', {
                rules: [{ required: true, message: ' ' }]
              })(<Input className="row-hidden" />)}
              <Upload
                customRequest={this.handleFileUpload}
                onRemove={this.clearFileList}
                accept=".json"
                fileList={fileList}
                multiple={false}
              >
                <Button>
                  <Icon type="upload" /> Выберите файл
                </Button>
              </Upload>
            </FormItem>
          ) : (
            <FormItem label="Доступные устройства" help="Устройства, доступные для подключения">
              {getFieldDecorator('deviceProfileId', {
                rules: [{ required: true, message: 'Выберите тип устройства' }]
              })(
                <ProfileSelect
                  onSelect={deviceProfileId =>
                    this.lineAddressSelector(deviceProfileId, getFieldValue('lineNo'))
                  }
                  profileGroups={connectableDeviceProfileGroups}
                />
              )}
            </FormItem>
          )}
          <FormItem
            label={currentDeviceProfileView.lineNoAlias || 'Номер линии'}
            className={currentDevice.id ? '' : 'hidden-field'}
          >
            {getFieldDecorator('lineNo', {
              initialValue:
                currentDeviceProfile.lineCount && currentDeviceProfile.lineCount >= lastSelectedLine
                  ? lastSelectedLine
                  : 1
            })(
              <InputNumber
                disabled={
                  isImport
                    ? !getFieldValue('controlDeviceEntities')
                    : !getFieldValue('deviceProfileId')
                }
                precision={0}
                min={1}
                onChange={lineNo => {
                  lastSelectedLine = lineNo;
                  this.lineAddressSelector(null, lineNo);
                  delete this.lastSelectedAddressByDeviceCategory[currentDevice.deviceCategory];
                }}
                max={currentDeviceProfile.lineCount || 1}
              />
            )}
          </FormItem>
          <FormItem label={currentDeviceProfileView.addressAlias || 'Адрес'}>
            {getFieldDecorator('lineAddress', { initialValue: 1 })(
              <InputNumber
                precision={0}
                disabled={
                  isImport
                    ? !getFieldValue('controlDeviceEntities')
                    : !getFieldValue('deviceProfileId')
                }
                min={1}
                max={255}
              />
            )}
          </FormItem>
          {isImport ? null : (
            <FormItem label={currentDeviceProfileView.countAlias || 'Кол-во'}>
              {getFieldDecorator('count', { initialValue: 1 })(
                <InputNumber
                  precision={0}
                  disabled={getFieldValue('deviceProfileId') ? false : true}
                  min={1}
                  max={255}
                />
              )}
            </FormItem>
          )}

          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              loading={
                (isImport && importConfigInProgress) || createDeviceInProgress ? true : false
              }
              disabled={
                isImport
                  ? !!importedConfigError || !getFieldValue('controlDeviceEntities')
                  : !getFieldValue('deviceProfileId')
              }
            >
              {isImport ? 'Импорт' : 'Добавить'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

NewDeviceForm = FormCreate({})(NewDeviceForm);

const mapStateToProps = state => ({
  modals: state.modals,
  deviceProfileViewsHash: getDeviceProfileViewsHash(state),
  currentDevice: state.widgets.currentDevice,
  deviceGroups: state.dictionaries.deviceProfilesDictionary.deviceGroups || [],
  importConfigInProgress: state.inProgress.importConfig,
  createDeviceInProgress: state.inProgress.createDevice
});

export default connect(mapStateToProps)(NewDeviceForm);
