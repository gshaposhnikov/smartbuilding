import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Form, Button } from 'antd';

import { FormItem, FormCreate } from 'components/Form';
import ButtonGroup from 'components/Button/ButtonGroup';
import Text from 'components/Form';
import Modal from 'containers/Modal';
import RegionsSelect from 'containers/Selects/RegionsSelect';
import { getCurrentProjectRegionsHash } from 'helpers/currentProject';

const ContentWrapper = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    & .ant-select-selection {
      width: 200px;
    }
  `,
  ButtonWrapper = styled.div`
    margin: 5px;
  `;

class UpdateDeviceRegionForm extends Component {
  static propTypes = {
    regions: PropTypes.object
  };
  render() {
    const {
      modalName,
      actionType,
      regions,
      deviceData,
      form,
      /**
       * Возможные действия
       * с постфиксом :
       *    Confirm - Выполняет действие
       *    Reject - Отмена действия с последующим редактирование (по возможности) или частичное выполнение действия (по логической составляющей)
       *    Cancel - Отменить действие полностью (Отмена перемещений и выделения)
       */
      // Отвязка устройства от зоны
      onDeleteDeviceRegionConfirm,
      onDeleteDeviceRegionReject,
      onDeleteDeviceRegionCancel,
      // Cменить зону на плане
      onReplacePlanRegionConfirm,
      onReplacePlanRegionReject,
      onReplacePlanRegionCancel,
      // Сменить зону устройства
      onChangeDeviceRegionConfirm,
      onChangeDeviceRegionReject,
      onChangeDeviceRegionCancel,
      // Выбор зоны
      onSelectDeviceRegionConfirm,
      onSelectDeviceRegionCancel,
      // Размещение в зоне с другой подсистемой (без привязки к ней)
      onForceDropDeviceConfirm,
      onForceDropDeviceCancel
    } = this.props;

    const { getFieldDecorator, getFieldValue } = form;
    return (
      <Modal name={modalName} title="Подтвердить" maskClosable={false}>
        {deviceData && actionType === 'DELETE' && deviceData.device ? (
          <ContentWrapper>
            <Text>У устройства зона уже указана. Отвязать?</Text>
            <ButtonGroup>
              <ButtonWrapper>
                <Button type="primary" onClick={onDeleteDeviceRegionConfirm}>
                  Да
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onDeleteDeviceRegionReject}>
                  Нет
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onDeleteDeviceRegionCancel}>
                  Отмена
                </Button>
              </ButtonWrapper>
            </ButtonGroup>
          </ContentWrapper>
        ) : null}
        {deviceData && actionType === 'FORCE' && deviceData.device ? (
          <ContentWrapper>
            <Text>Вы пытаетесь разместить устройство в зоне с другой подсистемой. Разместить?</Text>
            <ButtonGroup>
              <ButtonWrapper>
                <Button type="primary" onClick={onForceDropDeviceConfirm}>
                  Да
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onForceDropDeviceCancel}>
                  Отмена
                </Button>
              </ButtonWrapper>
            </ButtonGroup>
          </ContentWrapper>
        ) : null}
        {deviceData &&
        actionType === 'REPLACE' &&
        deviceData.device &&
        deviceData.device.regionId &&
        deviceData.regionIds ? (
          <ContentWrapper>
            <Text>{`Вы пытаетесь поместить устройство в зону ${
              regions[deviceData.regionIds[0]].name
            }, к которой оно не принадлежит. Зона устройства - ${deviceData.device.regionName}.
            Можно заменить зону на плане.
            Заменить зону?`}</Text>
            <ButtonGroup>
              <ButtonWrapper>
                <Button
                  type="primary"
                  onClick={() => onReplacePlanRegionConfirm(deviceData.regionIds[0])}
                >
                  Да
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onReplacePlanRegionReject}>
                  Нет
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onReplacePlanRegionCancel}>
                  Отмена
                </Button>
              </ButtonWrapper>
            </ButtonGroup>
          </ContentWrapper>
        ) : null}
        {deviceData && actionType === 'CHANGE' && deviceData.device && deviceData.regionIds ? (
          <ContentWrapper>
            <Text>
              Вы пытаетесь поместить устройство в зону, к которой оно не принадлежит. Заменить зону
              устройства?
            </Text>
            <ButtonGroup>
              <ButtonWrapper>
                <Button
                  type="primary"
                  onClick={() => onChangeDeviceRegionConfirm(deviceData.regionIds[0])}
                >
                  Да
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onChangeDeviceRegionReject}>
                  Нет
                </Button>
              </ButtonWrapper>
              <ButtonWrapper>
                <Button type="primary" onClick={onChangeDeviceRegionCancel}>
                  Отмена
                </Button>
              </ButtonWrapper>
            </ButtonGroup>
          </ContentWrapper>
        ) : null}
        {deviceData && actionType === 'SELECT' && deviceData.regionIds ? (
          <Form>
            <ContentWrapper>
              <Text>
                {'В этом месте находятся сразу несколько доступных зон. ' +
                  'Выберите ту, с которой хотите работать.'}
              </Text>
              <FormItem label="Доступные зоны">
                {getFieldDecorator('region', {
                  rules: [{ required: true, message: 'Выберите зону' }]
                })(
                  <RegionsSelect
                    regions={Object.values(regions).filter(region =>
                      deviceData.regionIds.includes(region.id)
                    )}
                  />
                )}
              </FormItem>
              <ButtonGroup>
                <ButtonWrapper>
                  <Button
                    type="primary"
                    disabled={getFieldValue('region') ? false : true}
                    onClick={() => onSelectDeviceRegionConfirm(getFieldValue('region'))}
                  >
                    Выбрать
                  </Button>
                </ButtonWrapper>
                <ButtonWrapper>
                  <Button type="primary" onClick={onSelectDeviceRegionCancel}>
                    Отмена
                  </Button>
                </ButtonWrapper>
              </ButtonGroup>
            </ContentWrapper>
          </Form>
        ) : null}
      </Modal>
    );
  }
}

UpdateDeviceRegionForm = FormCreate({
  mapPropsToFields(props) {
    return null;
  }
})(UpdateDeviceRegionForm);

const mapStateToProps = state => {
  return {
    regions: getCurrentProjectRegionsHash(state)
  };
};

UpdateDeviceRegionForm = connect(
  mapStateToProps,
  null
)(UpdateDeviceRegionForm);

export default UpdateDeviceRegionForm;
