import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button, Checkbox } from 'antd';
const FormItem = Form.Item;

import { FormCreate, ItemLayoutWithLabel } from 'components/Form';

import Modal from 'containers/Modal';

class ChooseLogTypesForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    onSubmit: PropTypes.func
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };

  render() {
    const { modalName, logTypes, form } = this.props;
    const { getFieldDecorator } = form;

    let selected = false;
    let checkboxes = !logTypes ? null : logTypes.map(logType => {
      const checked = form.getFieldValue(logType.id);
      selected = checked || selected;
      return (
        <FormItem
          key={logType.id}
          label={logType.name}
          {...ItemLayoutWithLabel}
        >
          {getFieldDecorator(logType.id)(<Checkbox checked={checked} />)}
        </FormItem>
      )
    });

    return (
      <Modal name={modalName} title={'Выберите журналы'}>
        <Form onSubmit={this.handleSubmit}>
          {checkboxes}
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={!selected}>
              {'Прочитать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

ChooseLogTypesForm = FormCreate({
  mapPropsToFields(props) {
    return null;
  }
})(ChooseLogTypesForm);

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};

ChooseLogTypesForm = connect(mapStateToProps)(ChooseLogTypesForm);

export default ChooseLogTypesForm;
