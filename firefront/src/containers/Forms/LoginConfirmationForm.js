import React from 'react';
import { connect } from 'react-redux';
import { Form, Button, Input } from 'antd';
import PropTypes from 'prop-types';

import { FormItem, FormCreate, ItemLayoutWithLabel } from 'components/Form';

import Modal from 'containers/Modal';

import { checkLoginConfirmation } from 'actions/login';

const FORM_NAME = 'loginConfirmationForm',
  BUTTON_GROUP_STYLES = { display: 'flex', justifyContent: 'space-between' },
  MAX_CONFIRMATION_TIME_DIFF = 60 * 10 * 1000;

class LoginConfirmationForm extends React.Component {
  static propTypes = {
    form: PropTypes.object,
    currentUser: PropTypes.object, // Текущий пользователь
    lastLoginConfirmation: PropTypes.number, // <timestamp> - Время последнего подтверждения
    onSuccess: PropTypes.func, // Функция выполняемая после удачного подтверждения
    onCancel: PropTypes.func // Отмена
  };

  state = {
    needPerformOnSuccess: false
  };

  handleSubmit = e => {
    const { dispatch } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({ needPerformOnSuccess: true }, () =>
        dispatch(checkLoginConfirmation(fieldsValue))
      );
    });
  };

  componentWillReceiveProps = nextProps => {
    const { lastLoginConfirmation } = this.props;
    const { needPerformOnSuccess } = this.state;
    if (
      !nextProps.loginConfirmationError &&
      nextProps.lastLoginConfirmation &&
      nextProps.lastLoginConfirmation !== lastLoginConfirmation
    ) {
      if (
        new Date() - nextProps.lastLoginConfirmation <= MAX_CONFIRMATION_TIME_DIFF &&
        needPerformOnSuccess
      ) {
        this.setState({ needPerformOnSuccess: false }, () => nextProps.onSuccess());
      }
    }
  };

  render = () => {
    const {
      form: { getFieldDecorator },
      currentUser,
      onCancel,
      loginConfirmationError
    } = this.props;
    return (
      <Modal name={FORM_NAME} title="Подтверждение">
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Пользователь">
            {getFieldDecorator('username', {
              initialValue:
                currentUser && currentUser.id ? currentUser.fullName : 'Неизвестный пользователь'
            })(<Input disabled />)}
          </FormItem>
          <FormItem
            {...ItemLayoutWithLabel}
            label="Пароль"
            help={loginConfirmationError}
            validateStatus={loginConfirmationError ? 'error' : null}
          >
            {getFieldDecorator('password', {
              initialValue: ''
            })(<Input type="password" />)}
          </FormItem>
          <div style={BUTTON_GROUP_STYLES}>
            <Button onClick={onCancel}>Отмена</Button>
            <Button type="primary" htmlType="submit">
              Выполнить
            </Button>
          </div>
        </Form>
      </Modal>
    );
  };
}

LoginConfirmationForm = FormCreate({})(LoginConfirmationForm);

const mapStateToProps = state => ({
  currentUser: state.currentUser,
  lastLoginConfirmation: state.lastLoginConfirmation,
  loginConfirmationError: state.errors.checkLoginConfirmation
});

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginConfirmationForm);
