import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, Field, reset } from 'redux-form';
import { Button } from 'antd';

import { FormItem } from 'components/Form';
import Input from 'components/Form/Input';
import Select, { Option } from 'components/Form/Select';
import List from 'components/Info/List';
import Title from 'components/Info/Title';

const FORM_NAME = 'deviceActionsForm';
const TITLE_STYLES = { display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' };
const ACTION_WRAPPER_STYLES = { marginBottom: 15 };

const DeviceActions = props => {
  const { onSubmit, dispatch, formValues, disabledExecution, actions = [], submitting } = props;
  const handleSubmit = (e, actionId) => {
    e.preventDefault();
    onSubmit(actionId, formValues[actionId]);
    dispatch(reset(FORM_NAME));
  };
  if (!actions) return null;
  return (
    <Form onSubmit={handleSubmit}>
      {actions.map((action, index) => (
        <div key={index} style={ACTION_WRAPPER_STYLES}>
          <Title style={TITLE_STYLES} title={action.description}>
            {action.name}
            <Button
              type="primary"
              htmlType="submit"
              onClick={e => handleSubmit(e, action.id)}
              disabled={disabledExecution || submitting}
            >
              Выполнить
            </Button>
          </Title>
          {action.properties ? (
            <List>
              {action.properties.map(property => {
                const { possibleValues, id, name, defaultValue } = property;
                return (
                  <FormItem
                    labelCol={{
                      sm: { span: 4 }
                    }}
                    wrapperCol={{
                      sm: { span: 10 }
                    }}
                    key={id}
                    label={name}
                  >
                    {possibleValues ? (
                      <Field name={`${action.id}.${id}`} value={defaultValue} component={Select}>
                        {Object.keys(possibleValues).map(possibleValue => (
                          <Option key={possibleValue}>{possibleValues[possibleValue]}</Option>
                        ))}
                      </Field>
                    ) : (
                      <Field name={id} component={Input} />
                    )}
                  </FormItem>
                );
              })}
            </List>
          ) : null}
        </div>
      ))}
    </Form>
  );
};

const DeviceActionsForm = reduxForm({})(DeviceActions);

const mapStateToProps = (state, { actions }) => {
  const initialValues = {};
  if (actions)
    actions.forEach(({ id, properties }) => {
      if (properties) {
        properties.forEach(property => {
          if (property.defaultValue) {
            if (!initialValues[id]) initialValues[id] = {};
            initialValues[id][property.id] = property.defaultValue;
          }
        });
      }
    });
  return {
    form: FORM_NAME,
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {},
    initialValues
  };
};
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceActionsForm);
