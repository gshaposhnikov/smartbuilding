import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, Field, change, reset, getFormSyncErrors } from 'redux-form';
import { Button } from 'antd';

import DeviceList from 'components/DeviceList';
import Select from 'components/Form/Select';

import Modal from 'containers/Modal';

import { getCurrentDevice } from 'helpers/widgets';
import { getDeviceProfile } from 'helpers/device';
import { getCurrentProjectDeviceList } from 'helpers/currentProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const FORM_NAME = 'deviceLinks';
const LINKED_DEVICES_FIELD = 'linkedDeviceIds';

class DeviceLinks extends React.Component {
  state = {
    selectedAvailable: null,
    selectedLinked: null
  };

  handleSubmit = e => {
    const { linkedDeviceIds, onSubmit, dispatch } = this.props;

    e.preventDefault();
    onSubmit(linkedDeviceIds);
    dispatch(reset(FORM_NAME));
  };

  rowClassName(record) {
    const { selectedLinked, selectedAvailable } = this.state;

    if (record.id === selectedLinked || record.id === selectedAvailable) return 'selected-row';
    return '';
  }

  connectDevice(deviceId) {
    const { linkedDeviceIds, dispatch } = this.props;

    dispatch(change(FORM_NAME, LINKED_DEVICES_FIELD, [...linkedDeviceIds, deviceId]));
    this.setState({
      selectedAvailable: null,
      selectedLinked: null
    });
  }

  disconnectDevice(deviceId) {
    const { linkedDeviceIds, dispatch } = this.props;

    dispatch(
      change(
        FORM_NAME,
        LINKED_DEVICES_FIELD,
        linkedDeviceIds.filter(linkedDeviceId => linkedDeviceId !== deviceId)
      )
    );
    this.setState({
      selectedAvailable: null,
      selectedLinked: null
    });
  }

  render() {
    const {
      modalName,
      availableDevices,
      linkedDevices,
      submitting,
      pristine,
      valid,
      errors,
      dispatch
    } = this.props;
    const { selectedLinked, selectedAvailable } = this.state;

    return (
      <Modal
        name={modalName}
        onClose={() => dispatch(reset(FORM_NAME))}
        className="device-links-modal"
        title="Подключение устройств"
        width="40%"
      >
        <Form onSubmit={this.handleSubmit}>
          <div style={{ display: 'flex', height: 300 }}>
            <div style={{ width: 400 }}>
              <DeviceList
                devices={linkedDevices}
                style={{ minHeight: 250 }}
                scroll={{ y: 300 }}
                onRowClick={record => this.setState({ selectedLinked: record.id })}
                rowClassName={record => this.rowClassName(record)}
                onRowDoubleClick={record => this.disconnectDevice(record.id)}
              />
            </div>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Button
                type="primary"
                icon="left"
                shape="circle"
                style={{ marginBottom: 30, marginTop: 80 }}
                disabled={!selectedAvailable}
                onClick={() => this.connectDevice(selectedAvailable)}
              />
              <Button
                type="primary"
                icon="right"
                shape="circle"
                disabled={!selectedLinked}
                onClick={() => this.disconnectDevice(selectedLinked)}
              />
            </div>
            <div style={{ width: 400 }}>
              <DeviceList
                devices={availableDevices}
                style={{ minHeight: 250 }}
                scroll={{ y: 300 }}
                onRowClick={record => this.setState({ selectedAvailable: record.id })}
                rowClassName={record => this.rowClassName(record)}
                onRowDoubleClick={record => this.connectDevice(record.id)}
              />
            </div>
          </div>
          <div style={{ display: 'none' }}>
            <Field name={LINKED_DEVICES_FIELD} mode="multiple" component={Select} />
          </div>
          {Object.keys(errors).map(key => (
            <div key={key} style={{ color: 'red' }}>
              {errors[key]}
            </div>
          ))}
          <div>
            <Button type="primary" htmlType="submit" disabled={pristine || submitting || !valid}>
              Сохранить
            </Button>
          </div>
        </Form>
      </Modal>
    );
  }
}

const validate = (values, { deviceProfile, linkedDevices, deviceProfileViewsHash }) => {
  const errors = {};
  if (deviceProfile) {
    deviceProfile.internalDevices.forEach(internalDevice => {
      const linkedInternalDevices = linkedDevices.filter(linkedDevice =>
        internalDevice.availableDeviceProfileIds.includes(linkedDevice.deviceProfileId)
      );
      if (linkedInternalDevices.length > internalDevice.maxAttachableDevices) {
        const deviceProfileNames = internalDevice.availableDeviceProfileIds.map(
          profileId => deviceProfileViewsHash[profileId].name
        );
        let errorMessage = `Подключено больше ${
          internalDevice.maxAttachableDevices
        }: ${deviceProfileNames.join(' и(или) ')}`;
        errors[LINKED_DEVICES_FIELD] = errorMessage;
      }
      if (linkedInternalDevices.length < internalDevice.nMandatoryDevices) {
        const deviceProfileNames = internalDevice.availableDeviceProfileIds.map(
          profileId => deviceProfileViewsHash[profileId].name
        );
        let errorMessage = `Подключено меньше ${
          internalDevice.nMandatoryDevices
        }: ${deviceProfileNames.join(' и(или) ')}`;
        errors[LINKED_DEVICES_FIELD] = errorMessage;
      }
    });
  }
  return errors;
};

DeviceLinks = reduxForm({ form: FORM_NAME, enableReinitialize: true, validate })(DeviceLinks);

const getAvailableDeviceProfileIds = deviceProfile => {
  const availableDeviceProfileIds = {};

  if (deviceProfile && deviceProfile.internalDevices) {
    deviceProfile.internalDevices.forEach(internalDevice => {
      if (internalDevice.availableDeviceProfileIds) {
        internalDevice.availableDeviceProfileIds.forEach(
          profileId => (availableDeviceProfileIds[profileId] = true)
        );
      }
    });
  }
  return availableDeviceProfileIds;
};

const getAvailableDevices = (
  devices,
  currentDevice,
  availableDeviceProfileIds,
  linkedDeviceIds
) => {
  const parentIds = {};
  const unlinked = {};

  devices.forEach(device => {
    if (device.parentDeviceId === currentDevice.parentDeviceId) parentIds[device.id] = true;
    if (device.virtualContainerId === currentDevice.id && !linkedDeviceIds.includes(device.id))
      unlinked[device.id] = true;
  });
  return devices.filter(device => {
    if (
      ((parentIds[device.id] || parentIds[device.parentDeviceId]) &&
        availableDeviceProfileIds[device.deviceProfileId] &&
        !linkedDeviceIds.includes(device.id) &&
        !device.virtualContainerId) ||
      unlinked[device.id]
    )
      return true;
    return false;
  });
};

const mapStateToProps = state => {
  let currentDevice = getCurrentDevice(state);
  if (currentDevice && currentDevice.deviceCategory !== 'VIRTUAL_CONTAINER') currentDevice = null;
  const devices = getCurrentProjectDeviceList(state);
  const linkedDeviceIds =
    state.form[FORM_NAME] &&
    state.form[FORM_NAME].values &&
    state.form[FORM_NAME].values[LINKED_DEVICES_FIELD]
      ? state.form[FORM_NAME].values[LINKED_DEVICES_FIELD] || []
      : currentDevice && currentDevice.linkedDeviceIds
        ? currentDevice.linkedDeviceIds
        : [];
  const deviceProfileViewsHash = getDeviceProfileViewsHash(state);
  const deviceProfile = currentDevice
    ? getDeviceProfile(deviceProfileViewsHash, currentDevice)
    : null;

  return {
    availableDevices: currentDevice
      ? getAvailableDevices(
          devices,
          currentDevice,
          getAvailableDeviceProfileIds(deviceProfile),
          linkedDeviceIds
        )
      : [],
    currentDevice,
    deviceProfile,
    deviceProfileViewsHash,
    errors: getFormSyncErrors(FORM_NAME)(state),
    linkedDevices: currentDevice
      ? devices.filter(device => linkedDeviceIds.includes(device.id))
      : [],
    linkedDeviceIds,
    initialValues: {
      [LINKED_DEVICES_FIELD]:
        currentDevice && currentDevice.linkedDeviceIds ? currentDevice.linkedDeviceIds : []
    }
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceLinks);
