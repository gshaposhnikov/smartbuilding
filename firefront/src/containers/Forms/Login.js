import React, { Component } from 'react'
import { connect } from 'react-redux'

import Form, { FormItem, FormCreate } from 'components/Form';
import { Icon, Input, Button } from 'antd';
import { loginUser } from 'actions/login'

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class Login extends Component{
  componentDidMount() {
    // Отключаем кнопку при первой отрисовке
    this.props.form.validateFields();
  }

  handleSubmit = (e) => {
    /* отключить стандартное действие формы */
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (!err) {
        const creds = {
          username: values.userName,
          password: values.password
        };
        this.props.dispatch(loginUser(creds));
      }
    });
  }

  render() {
    const { errorMessage } = this.props;
    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
    const userNameError = isFieldTouched('userName') && getFieldError('userName');
    const passwordError = isFieldTouched('password') && getFieldError('password');
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem label="Имя пользователя" help='' validateStatus={(userNameError || errorMessage) ? 'error' : ''}>
          {getFieldDecorator('userName', {
            rules: [{ required: true }],
          })(
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} />
          )}
        </FormItem>
        <FormItem label="Пароль" help={errorMessage || ''} validateStatus={(passwordError || errorMessage) ? 'error' : ''}>
          {getFieldDecorator('password', {
            rules: [{ required: true }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" />
          )}
        </FormItem>
        <FormItem validateStatus={errorMessage ? 'error' : ''}>
          <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>
            Войти
          </Button>
        </FormItem>
      </Form>
    );
  }
};

Login = FormCreate({
  mapPropsToFields(props) {
    return null;
  }
})(Login);

const mapStateToProps = state => {
  return {
    errorMessage: state.errors.loginUser,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
