import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { Button } from 'antd';

import { FormItem } from 'components/Form';
import Checkbox from 'components/Form/Checkbox';

import Modal from 'containers/Modal';

const FORM_NAME = 'controlDeviceParams';
const BUTTON_GROUP_STYLES = { display: 'flex', justifyContent: 'space-between' };

class DBParams extends React.Component {
  handleSubmit = e => {
    const { onSubmit, formValues } = this.props;
    e.preventDefault();
    onSubmit(formValues);
  };

  render() {
    const { modalName, submitting, onCancel } = this.props;

    return (
      <Modal
        name={modalName}
        title={modalName === 'uploadAllDB' ? 'Запись базы во все приборы' : 'Запись базы в прибор'}
      >
        <form onSubmit={this.handleSubmit}>
          <FormItem label="Параметры записи">
            <div>
              <Field
                component={Checkbox}
                label="Записать восстановительную информацию"
                name="recoveryNecessary"
              />
            </div>
            <div>
              <Field
                component={Checkbox}
                label="Записать базу СКУД"
                name="accessControlNecessary"
              />
            </div>
          </FormItem>
          <div style={BUTTON_GROUP_STYLES}>
            <Button
              onClick={() => {
                onCancel(modalName);
              }}
            >
              Отмена
            </Button>
            <Button type="primary" htmlType="submit" disabled={submitting}>
              Записать
            </Button>
          </div>
        </form>
      </Modal>
    );
  }
}

DBParams = reduxForm({ form: FORM_NAME, enableReinitialize: true })(DBParams);

const mapStateToProps = state => ({
  formValues:
    state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {},
  initialValues: { recoveryNecessary: true, accessControlNecessary: true }
});

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(DBParams);
