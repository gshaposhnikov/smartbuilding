import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, Field, initialize, reset } from 'redux-form';
import Modal from 'containers/Modal';

import { Button } from 'antd';
import DeviceSelect from 'containers/Selects/DeviceSelect';
import { FormItem } from 'components/Form';

const FORM_NAME = 'chooseControlDevicesForm';

/**
 * Сброс значений формы
 * @param dispatch
 * @param values объект со значениями
 */
export const resetFormValues = (dispatch, values) => {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
};

var ChooseControlDevicesForm = ({
  onSubmit,
  controlDevices,
  modalName,
  dispatch,
  formValues
}) => {
  const handleSubmit = e => {
    e.preventDefault();
    onSubmit(formValues);
  };

  return (
    <Modal name={modalName} title="Выберите приборы для записи БД СКУД">
      <Form onSubmit={handleSubmit}>
        <FormItem label="Доступные приборы">
          <Field
            component={DeviceSelect}
            name="controlDeviceIds"
            devices={controlDevices}
            mode="multiple"
          />
        </FormItem>
        <FormItem>
            <Button type="primary" htmlType="submit" disabled={!formValues.controlDeviceIds || !formValues.controlDeviceIds.length}>
              Записать
            </Button>
        </FormItem>
      </Form>
    </Modal>
  );
};

ChooseControlDevicesForm = reduxForm({ form: FORM_NAME })(ChooseControlDevicesForm);

const mapStateToProps = state => {
  return {
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {}
  };
};
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ChooseControlDevicesForm);
