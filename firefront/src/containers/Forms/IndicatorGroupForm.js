import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Input, Button } from 'antd';

import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import Modal from 'containers/Modal';

import { initializeValues } from 'helpers/form';

class IndicatorGroupForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object,
    modals: PropTypes.object,
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const { modalName, form, isEdit } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Modal name={modalName} title={isEdit ? 'Редактирование группы' : 'Новая группа'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Название">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Имя обязательно' }],
              initialValue: `Новая группа панелей`
            })(<Input maxLength={32} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Описание">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} maxLength={256} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithoutLabel}>
            <Button type="primary" htmlType="submit">
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

IndicatorGroupForm = FormCreate({
  mapPropsToFields(props) {
    return props.isEdit ? initializeValues(props.selectedIndicatorGroup || {}) : null;
  }
})(IndicatorGroupForm);

const mapStateToProps = state => {
  const selectedIndicatorGroupId = state.widgets.selectedIndicatorGroupId;
  return {
    modals: state.modals,
    selectedIndicatorGroup:
      selectedIndicatorGroupId && state.indicators.groups[state.currentProjectId]
        ? state.indicators.groups[state.currentProjectId][selectedIndicatorGroupId]
        : null
  };
};

export default connect(
  mapStateToProps,
  null
)(IndicatorGroupForm);
