import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import { Form, InputNumber, Input, Button, Checkbox } from 'antd';
import { FormItem, FormCreate, ItemLayoutWithLabel, ItemLayoutWithoutLabel } from 'components/Form';

import SubsystemSelect from 'containers/Selects/SubsystemSelect';
import SecurityRegionTypeSelect from 'containers/Selects/SecurityRegionTypeSelect';

import { initializeValues } from 'helpers/form';
import { getCurrentProjectRegions, getCurrentProject } from 'helpers/currentProject';

class RegionForm extends Component {
  static propTypes = {
    project: PropTypes.object,
    regions: PropTypes.array,
    modalName: PropTypes.string,
    isEdit: PropTypes.bool,
    form: PropTypes.object, // Генерируется Form.create
    currentRegion: PropTypes.object,
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const {
      modalName,
      form,
      isEdit,
      securityTypes,
      regions = [],
      createRegionInProgress,
      updateRegionInProgress,
      createRegionAndConnectDeviceInProgress,
      forceSubsystem
    } = this.props;
    const { getFieldDecorator } = form;
    const subsystemSelectValue = form.getFieldValue('subsystem');
    const securityRegionTypeSelectValue = form.getFieldValue('securityRegionType');
    const silenceAlarmValue = form.getFieldValue('silenceAlarm');
    // Найдём первый свободный индекс зоны
    const newRegionIndex = regions.length
      ? regions.sort((a, b) => a.index - b.index)[regions.length - 1].index + 1
      : 1;
    return (
      <Modal name={modalName} title={isEdit ? 'Редактирование зоны' : 'Новая зона'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem {...ItemLayoutWithLabel} label="Номер зоны">
            {getFieldDecorator('index', {
              rules: [{ required: true, message: 'Номер зоны обязателен' }],
              initialValue: newRegionIndex
            })(<InputNumber precision={0} min={1} max={65535} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Наименование">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Имя обязательно' }],
              initialValue: `Новая зона ${newRegionIndex}`
            })(<Input maxLength={256} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} label="Примечание">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} maxLength={256} />)}
          </FormItem>
          <FormItem {...ItemLayoutWithLabel} required={true} label="Назначение зоны">
            {getFieldDecorator('subsystem', {
              rules: [{ required: true, message: 'Выберите назначение зоны' }],
              initialValue: forceSubsystem || ''
            })(<SubsystemSelect disabled={isEdit || !!forceSubsystem} />)}
          </FormItem>

          {/* Если подсистема пожарная, выводим следующие поля:*/
          subsystemSelectValue === 'FIRE' ? (
            <FormItem {...ItemLayoutWithLabel} label="Кол-во датчиков перехода в состояние Пожар-2">
              {getFieldDecorator('fireEventCount', {
                rules: [
                  { required: true, message: 'Минимальное количество датчиков перехода - 2' }
                ],
                initialValue: 2
              })(<InputNumber precision={0} min={2} />)}
            </FormItem>
          ) : null}
          {/* Если подсистема охранная, выводим следующие поля: */
          subsystemSelectValue === 'SECURITY' ? (
            <div>
              <FormItem {...ItemLayoutWithLabel} required={true} label="Вид охранной зоны">
                {getFieldDecorator('securityRegionType', {
                  rules: [{ required: true, message: 'Выберите вид охранной зоны' }],
                  initialValue: `DEFAULT`
                })(<SecurityRegionTypeSelect types={securityTypes} />)}
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Тихая тревога">
                {getFieldDecorator('silenceAlarm')(<Checkbox checked={silenceAlarmValue} />)}
              </FormItem>
              <FormItem {...ItemLayoutWithLabel} label="Автоперевзятие, сек">
                {getFieldDecorator('autoRelock')(<InputNumber precision={0} min={0} />)}
              </FormItem>
              {/* Если зона с задержкой входа/выхода, то выводим поле задержки */
              securityRegionTypeSelectValue === 'TIMEOUT' ? (
                <FormItem {...ItemLayoutWithLabel} label="Входная/выходная задержка, сек">
                  {getFieldDecorator('inputOutputTimeout')(<InputNumber precision={0} min={0} />)}
                </FormItem>
              ) : null}
            </div>
          ) : null}

          <FormItem {...ItemLayoutWithoutLabel}>
            <Button
              type="primary"
              htmlType="submit"
              loading={createRegionInProgress || updateRegionInProgress || createRegionAndConnectDeviceInProgress ? true : false}
            >
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

RegionForm = FormCreate({
  mapPropsToFields(props) {
    if (props.forceSubsystem) return initializeValues({ subsystem: props.forceSubsystem });
    return props.isEdit ? initializeValues(props.currentRegion) : null;
  }
})(RegionForm);

const mapStateToProps = state => {
  return {
    modals: state.modals,
    currentRegion: state.widgets.currentRegion,
    securityTypes: state.regionConstants.securityTypes,
    regions: getCurrentProjectRegions(state),
    project: getCurrentProject(state),
    createRegionInProgress: state.inProgress.createRegion,
    updateRegionInProgress: state.inProgress.updateRegion,
    createRegionAndConnectDeviceInProgress: state.inProgress.createRegionAndConnectDevice
  };
};

RegionForm = connect(
  mapStateToProps,
  null
)(RegionForm);

export default RegionForm;
