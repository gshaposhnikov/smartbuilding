import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'containers/Modal';

import { Form, Input, Button } from 'antd';
const FormItem = Form.Item;

import { initializeValues } from 'helpers/form';

class ProjectForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    isEdit: PropTypes.bool,
    form: PropTypes.object, // Генерируется Form.create
    selectedProject: PropTypes.object,
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
    });
  };
  render() {
    const { modalName, form, isEdit } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal name={modalName} title={isEdit ? 'Редактирование проекта' : 'Новый проект'}>
        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Название проекта">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Название проекта обязательно' }]
            })(<Input />)}
          </FormItem>
          <FormItem label="Версия">
            {getFieldDecorator('version', {
              rules: [{ required: true, message: 'Версия проекта обязательна' }]
            })(<Input rows={4}  />)}
          </FormItem>
          <FormItem label="Примечания">
            {getFieldDecorator('description')(<Input type="textarea" rows={4} />)}
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={false}>
              {isEdit ? 'Изменить' : 'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

ProjectForm = Form.create({
  mapPropsToFields(props) {
    return props.isEdit ? initializeValues(props.selectedProject) : null;
  }
})(ProjectForm);

const mapStateToProps = state => {
  return {
    modals: state.modals,
    selectedProject: state.widgets.projects.selected
  };
};

ProjectForm = connect(mapStateToProps, null)(ProjectForm);

export default ProjectForm;
