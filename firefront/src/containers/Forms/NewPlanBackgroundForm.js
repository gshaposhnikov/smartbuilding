import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Input, Button, Icon, Upload } from 'antd';
const isSvg = require('is-svg');
import { parse } from 'svg-parser';

import Modal from 'containers/Modal';

import { initializeValues } from 'helpers/form';
import { getStringByteLength } from 'helpers/strings';

const MAX_FILE_SIZE = 15728640,
  FormItem = Form.Item,
  FILE_ERRORS = {
    CONTENT: 'Cтруктура файла не соответствует формату',
    SIZE: 'Размер файла не должен превышать 15 мб',
    FORMAT: 'Файл должен иметь расширение SVG',
    EMPTY: 'Выберите файл',
    INVALID: 'Файл невалидный',
    SIZE_ATTR: 'Не удалось прочитать высоту и ширину SVG'
  },
  SVG_FIELD = 'svgContent',
  FORM_ITEM_LAYOUT = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 }
  },
  HIDDEN_FIELD_STYLES = { display: 'none' };

class BackgroundForm extends Component {
  static propTypes = {
    modalName: PropTypes.string,
    form: PropTypes.object, // Генерируется Form.create
    modals: PropTypes.object, // При закрытии модалки props будут прокидываться заново, чтобы значия обновлялись при каждом открытие модалки
    onSubmit: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = { fileList: [] };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, fieldsValue) => {
      if (err) {
        return;
      }
      this.props.onSubmit(fieldsValue);
      this.setState({
        fileList: [],
        isValidFile: null
      });
    });
  };

  handleFileUpload = event => {
    const { file } = event;
    // Проверка размер файла
    if (file.size > MAX_FILE_SIZE) this.setFileData(false, FILE_ERRORS.SIZE, []);
    // Проверка расширения
    else if (file.type.includes('svg')) {
      const reader = new FileReader();
      reader.onload = data => {
        const svgString = data.currentTarget.result.replace(/\r?\n|\r/g, ' ');
        // Проверка структуры файла
        if (isSvg(svgString)) {
          try {
            const svgObject = parse(reader.result);
            if (!svgObject) throw new Error(FILE_ERRORS.INVALID);
            if (
              !svgObject.attributes ||
              !svgObject.attributes.xmlns ||
              svgObject.attributes.xmlns !== 'http://www.w3.org/2000/svg'
            )
              throw new Error(FILE_ERRORS.INVALID);
            if (
              !svgObject.attributes ||
              !svgObject.attributes.width ||
              !svgObject.attributes.height
            )
              throw new Error(FILE_ERRORS.SIZE_ATTR);
          } catch (err) {
            this.setFileData(false, err.message, [], '');
            return false;
          }
          this.setFileData(
            true,
            '',
            [
              {
                uid: 1,
                name: file.name,
                status: 'done'
              }
            ],
            svgString
          );
        } else this.setFileData(false, FILE_ERRORS.CONTENT, [], '');
      };
      reader.readAsText(file);
    } else this.setFileData(false, FILE_ERRORS.FORMAT, [], '');
  };

  setFileData = (isValid, err, fileList, content) => {
    const { form } = this.props;
    form.setFieldsValue({
      [SVG_FIELD]: content
    });
    this.setState({
      isValidFile: isValid,
      validFileMessage: err || null,
      fileList
    });
    form.validateFields([SVG_FIELD], { force: true });
  };

  handleFileValidate = (rule, value, callback) => {
    const { form } = this.props;
    const { isValidFile, validFileMessage } = this.state;
    const file = form.getFieldValue(SVG_FIELD);
    // Проверка валидности при загрузке
    if (isValidFile === false) callback([new Error(validFileMessage)]);
    // Проверка на пустоту
    else if (!file) callback([new Error(FILE_ERRORS.EMPTY)]);
    // Проверка размера файла
    else if (getStringByteLength(file) > MAX_FILE_SIZE) callback([new Error(FILE_ERRORS.SIZE)]);
    // Если нет ошибок, то возврщаем пустой ответ
    else callback();
  };

  render() {
    const { modalName, form } = this.props;
    const { fileList } = this.state;
    const { getFieldDecorator } = form;
    return (
      <Modal name={modalName} title="Новое фоновое изображение">
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            label="Файл"
            {...FORM_ITEM_LAYOUT}
            help={form.getFieldError(SVG_FIELD)}
            validateStatus={form.getFieldError(SVG_FIELD) ? 'error' : null}
          >
            {getFieldDecorator(SVG_FIELD, {
              rules: [
                { required: true, message: ' ' },
                { validator: this.handleFileValidate, trigger: 'onChange' }
              ]
            })(<Input style={HIDDEN_FIELD_STYLES} />)}
            <Upload
              customRequest={this.handleFileUpload}
              onRemove={() => this.setFileData(false, [], FILE_ERRORS.EMPTY, '')}
              accept={'image/svg+xml'}
              id="uploader"
              fileList={fileList}
              multiple={false}
            >
              <Button>
                <Icon type="upload" /> {'Выберите файл'}
              </Button>
            </Upload>
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" disabled={false}>
              {'Создать'}
            </Button>
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

BackgroundForm = Form.create({
  mapPropsToFields(props) {
    return initializeValues(props);
  }
})(BackgroundForm);

const mapStateToProps = state => {
  return {
    modals: state.modals
  };
};

BackgroundForm = connect(mapStateToProps)(BackgroundForm);

export default BackgroundForm;
