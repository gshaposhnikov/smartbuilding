import React, { Component } from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class PlanEditorSelect extends Component {
  render() {
    const { options } = this.props;
    return (
      <Select {...this.props}>
        {options.map((option, index) => (
          <Option key={index} value={`${option.value}`}>
            {`${option.text || option.value}`}
          </Option>
        ))}
      </Select>
    );
  }
}
