import React, { Component } from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class PlanGroupsSelect extends Component {
  render() {
    const { groups, ...restProps } = this.props;

    return (
      <Select {...restProps}>
        {groups.map(group => {
          return (
            <Option value={group.id} key={group.id}>
              {group.name}
            </Option>
          );
        })}
      </Select>
    );
  }
}
