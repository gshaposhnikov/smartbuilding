import React, { Component } from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class SecurityRegionTypeSelect extends Component {
  render() {
    const { types, ...restProps } = this.props;

    return (
      <Select {...restProps}>
        {types.map(item => {
          return (
            <Option value={item.id} key={item.id}>
              {item.name}
            </Option>
          );
        })}
      </Select>
    );
  }
}
