import React, { Component } from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class SubsystemSelect extends Component {
  render() {
    const { children, ...restProps } = this.props;

    //TODO: Получать список возможных подсистем с сервера
    return (
      <Select {...restProps}>
        {children}
        <Option value="FIRE">Пожарная</Option>
        <Option value="SECURITY">Охранная</Option>
        <Option value="SKUD">СКУД</Option>
        <Option value="AUTO">Автоматика</Option>
      </Select>
    );
  }
}
