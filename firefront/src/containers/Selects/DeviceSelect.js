import React, { Component } from 'react';
import Select, { Option } from 'components/Form/Select';

const IMAGE_STYLES = { marginRight: '4px' };

export default class DeviceSelect extends Component {
  render() {
    const { devices, ...restProps } = this.props;

    if (devices !== undefined) {
      return (
        <Select {...restProps}>
          {devices.map(device => {
            const name = `${device.name} (${device.fullAddressPath})`;
            return (
              <Option value={device.id} key={device.id} title={name}>
                <img
                  style={IMAGE_STYLES}
                  width="16px"
                  /* TODO: переделать иконки устройств в svg-формат */
                  src={`/public/img/device/${device.iconMedia.path}`}
                  alt={`/public/img/device/${device.iconMedia.path}`}
                />
                {name}
              </Option>
            );
          })}
        </Select>
      );
    } else {
      return <Select {...restProps} />;
    }
  }
}
