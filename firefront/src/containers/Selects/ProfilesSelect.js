import React, { Component } from 'react';
import { Select } from 'antd';
const { Option, OptGroup } = Select;

import Device from 'components/Device/Device';
import DeviceImg from 'components/Device/DeviceImg';

export default class ProfilesSelect extends Component {
  render() {
    const { profileGroups = [], ...restProps } = this.props;
    return (
      <Select
        notFoundContent="Не найдено"
        showSearch
        optionFilterProp="title"
        filterOption={(input, option) =>
          option.props.title.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        {...restProps}
      >
        {profileGroups.map(group => (
          <OptGroup label={group.name} key={group.id}>
            {group.children.map(item => {
              const name = item.deviceProfile.id;
              const iconMedia = item.iconMedia ? item.iconMedia.path : 'null.ico';
              return (
                <Option value={name} key={name} title={item.name}>
                  <Device>
                    <DeviceImg size={15} name={iconMedia} />
                    {item.name}
                  </Device>
                </Option>
              );
            })}
          </OptGroup>
        ))}
      </Select>
    );
  }
}
