import React, { Component } from 'react';
import Select, { Option } from 'components/Form/Select';

/**
 * Выпадающий список с возможностью задать источник значений через props,
 * для использования на формах.
 *
 * Обязательные параметры, которые должны передаваться от родительского компонента:
 * @prop itemsSource - массив объектов для заполнения элементов списка
 *
 * Опциональные параметры, которые могут передаваться от родительского компонента:
 * @prop valueFieldName - название поля, из которого должно браться
 * отображаемое пользователю значение
 * @prop keyFieldName - название поля ключа
 * @prop titleFieldName - название поля, из которого должна браться
 * отображаемая пользователю всплывающая подсказка
 *
 * Также допустимо передавать параметры в соответствии с API
 * компонента Select библиотеки antd, кроме value, onChange и
 * defaultValue (они заполняются формой).
 **/
export default class CustomSelect extends Component {
  render() {
    const {
      itemsSource,
      valueFieldName,
      keyFieldName,
      titleFieldName,
      disabledCondition,
      ...restProps
    } = this.props;

    if (itemsSource !== undefined) {
      return (
        <Select {...restProps}>
          {itemsSource.map(item => {
            return (
              <Option
                value={keyFieldName ? item[keyFieldName] : item.id}
                key={keyFieldName ? item[keyFieldName] : item.id}
                title={titleFieldName ? item[titleFieldName] : item.description || item.name}
                disabled={disabledCondition ? disabledCondition(item) : false}
              >
                {valueFieldName ? item[valueFieldName] : item.name}
              </Option>
            );
          })}
        </Select>
      );
    } else {
      return <Select {...restProps} />;
    }
  }
}
