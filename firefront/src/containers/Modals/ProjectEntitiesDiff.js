import React from 'react';
import { connect } from 'react-redux';
import { Tabs, Icon, Button } from 'antd';
import { isEmpty } from 'lodash';
const TabPane = Tabs.TabPane;
import { saveAs as saveFile } from 'file-saver';

import DiffTable from 'components/ExpandableTable/DiffTable';

import { modalOpen } from 'actions/modals';
import { loadRegionConstants } from 'actions/regions';
import { loadScenarioConstants } from 'actions/scenarios';

import {
  DIFF_STATES,
  RegionsDiff,
  DevicesDiff,
  VirtualStatesDiff,
  ScenariosDiff
} from 'helpers/diff';
const diffStates = Object.values(DIFF_STATES);
import Modal from 'containers/Modal';

const TITLE_MAIN_WRAPPER_STYLES = { display: 'flex', justifyContent: 'space-around' };
const LEGEND_WRAPPER_STYLES = {
  display: 'flex',
  justifyContent: 'space-around',
  paddingTop: 8,
  marginTop: 8,
  borderTop: '1px solid lightgray'
};
const LEGEND_ITEM_STYLES = { display: 'flex' };
const BUTTON_STYLES = { position: 'absolute', right: 0 };
const TITLE_WRAPPER_STYLES = {
  position: 'relative',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flex: 1
};

class ProjectEntitiesDiff extends React.Component {
  state = {
    diff: {}
  };
  componentDidMount = () => {
    const { regionConstants, scenarioConstants, dispatch } = this.props;
    if (isEmpty(regionConstants)) {
      dispatch(loadRegionConstants());
    }
    if (isEmpty(scenarioConstants)) {
      dispatch(loadScenarioConstants());
    }
  };

  componentWillReceiveProps = nextProps => {
    const { modals, modalName } = this.props;
    if (
      !nextProps.modals[nextProps.modalName] &&
      !modals[modalName] &&
      !isEmpty(nextProps.regionConstants) &&
      nextProps.stateCategoryViewsHash &&
      !isEmpty(nextProps.scenarioConstants)
    ) {
      if (Object.keys(nextProps.comparisonEntities).length) {
        if (
          Object.keys(nextProps.comparisonEntities[Object.keys(nextProps.comparisonEntities)[0]])
            .length
        ) {
          nextProps.dispatch(modalOpen(nextProps.modalName));
        }
      }
    }
    if (
      modals[modalName] !== nextProps.modals[nextProps.modalName] &&
      nextProps.modals[nextProps.modalName] &&
      nextProps.comparisonEntities &&
      Object.keys(nextProps.comparisonEntities).length
    ) {
      const comparisonEntities =
        nextProps.comparisonEntities[Object.keys(nextProps.comparisonEntities)[0]];
      this.setState({
        comparisonEntities,
        diff: this.getDiff(
          comparisonEntities,
          nextProps.regionConstants,
          nextProps.stateCategoryViewsHash,
          nextProps.scenarioConstants
        )
      });
    }
  };

  getDiff = (
    {
      currentEntitiesView: {
        devices: currentDevices,
        regions: currentRegions,
        virtualStates: currentVirtualStates,
        scenarios: currentScenarios
      },
      recoveredEntitiesView: {
        devices: recoveredDevices,
        regions: recoveredRegions,
        virtualStates: recoveredVirtualStates,
        scenarios: recoveredScenarios
      }
    },
    regionConstants,
    stateCategoryViewsHash,
    scenarioConstants
  ) => {
    const diff = {};
    diff['virtualStates'] = this.getVirtualStatesDiff(
      currentVirtualStates,
      recoveredVirtualStates,
      currentDevices.find(device => device.deviceCategory === 'CONTROL'),
      recoveredDevices.find(device => device.deviceCategory === 'CONTROL'),
      stateCategoryViewsHash
    );
    diff['scenarios'] = this.getScenariosDiff(
      currentScenarios,
      recoveredScenarios,
      scenarioConstants
    );
    diff['devices'] = this.getDevicesDiff(currentDevices, recoveredDevices);
    diff['regions'] = this.getRegionsDiff(currentRegions, recoveredRegions, regionConstants);
    return diff;
  };

  getDevicesDiff = (fromProject, fromDevice) => {
    return new DevicesDiff({ fromProject, fromDevice }).get();
  };

  getRegionsDiff = (fromProject, fromDevice, regionConstants) => {
    return new RegionsDiff({ fromProject, fromDevice, regionConstants }).get();
  };

  getScenariosDiff = (fromProject, fromDevice, scenarioConstants) => {
    return new ScenariosDiff({ fromProject, fromDevice, scenarioConstants }).get();
  };

  getVirtualStatesDiff = (
    fromProject,
    fromDevice,
    currentDevice,
    recoveredDevice,
    stateCategoryViewsHash
  ) => {
    return new VirtualStatesDiff({
      fromProject,
      fromDevice,
      currentDevice,
      recoveredDevice,
      stateCategoryViewsHash
    }).get();
  };

  onSave = key => {
    const { comparisonEntities } = this.state;
    const blob = new Blob([JSON.stringify(comparisonEntities[key] || {})], {
      type: 'application/json'
    });
    saveFile(blob, `FS-CONFIGURATION.json`);
  };

  render() {
    const { modalName } = this.props;
    const { diff } = this.state;
    return (
      <Modal name={modalName} title={'Сравнение конфигураций'} width="90%">
        <div style={TITLE_MAIN_WRAPPER_STYLES}>
          <div style={TITLE_WRAPPER_STYLES}>
            <h3>Рабочая</h3>
            <Button onClick={() => this.onSave('currentEntities')} style={BUTTON_STYLES}>
              Сохранить
            </Button>
          </div>
          <div style={TITLE_WRAPPER_STYLES}>
            <h3>С прибора</h3>
            <Button onClick={() => this.onSave('recoveredEntities')} style={BUTTON_STYLES}>
              Сохранить
            </Button>
          </div>
        </div>
        <Tabs defaultActiveKey="devices">
          <TabPane
            tab={
              <div>
                {'Устройства '}
                {diff && diff.devices && !diff.devices.isEqual ? (
                  <Icon type="exclamation-circle" />
                ) : (
                  ''
                )}
              </div>
            }
            key="devices"
          >
            {diff && diff.devices ? (
              <DiffTable
                id="devicesComparison"
                nodesFromProject={diff.devices.fromProject}
                nodesFromDevice={diff.devices.fromDevice}
              />
            ) : null}
          </TabPane>
          <TabPane
            tab={
              <div>
                {'Зоны '}
                {diff && diff.regions && !diff.regions.isEqual ? (
                  <Icon type="exclamation-circle" />
                ) : (
                  ''
                )}
              </div>
            }
            key="regions"
          >
            {diff && diff.regions ? (
              <DiffTable
                id="regionsComparison"
                nodesFromProject={diff.regions.fromProject}
                nodesFromDevice={diff.regions.fromDevice}
              />
            ) : null}
          </TabPane>
          <TabPane
            tab={
              <div>
                {'Вирт. Состояния '}
                {diff && diff.virtualStates && !diff.virtualStates.isEqual ? (
                  <Icon type="exclamation-circle" />
                ) : (
                  ''
                )}
              </div>
            }
            key="virtualStates"
          >
            {diff && diff.virtualStates ? (
              <DiffTable
                id="virtualStatesComparison"
                nodesFromProject={diff.virtualStates.fromProject}
                nodesFromDevice={diff.virtualStates.fromDevice}
              />
            ) : null}
          </TabPane>
          <TabPane
            tab={
              <div>
                {'Сценарии '}
                {diff && diff.scenarios && !diff.scenarios.isEqual ? (
                  <Icon type="exclamation-circle" />
                ) : (
                  ''
                )}
              </div>
            }
            key="scenarios"
          >
            {diff && diff.scenarios ? (
              <DiffTable
                id="scenariosComparison"
                nodesFromProject={diff.scenarios.fromProject}
                nodesFromDevice={diff.scenarios.fromDevice}
              />
            ) : null}
          </TabPane>
        </Tabs>
        <div style={LEGEND_WRAPPER_STYLES}>
          {diffStates.map((state, index) => {
            return (
              <div key={index} style={LEGEND_ITEM_STYLES}>
                <span
                  style={{
                    background: state.color,
                    display: 'flex',
                    width: 16,
                    height: 16,
                    border: '1px solid black',
                    marginRight: 4
                  }}
                />
                <span>{state.name}</span>
              </div>
            );
          })}
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    comparisonEntities: state.activeProject.exportProjectComparisonData,
    scenarioConstants: state.scenarioConstants,
    regionConstants: state.regionConstants,
    stateCategoryViewsHash: state.stateCategoryViewsHash,
    modals: state.modals
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectEntitiesDiff);
