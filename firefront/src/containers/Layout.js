import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import appConfig from 'appConfig';
import styled, { ThemeProvider } from 'styled-components';
import theme from 'constants/theme';
import { connect } from 'react-redux';

import Header from 'containers/Header';
import OP from 'containers/Pages/OP/';
import ProjectsPage from 'containers/Pages/Project';
import LoginPage from 'containers/Pages/Login';
import ConfigPage from 'containers/Pages/Config';
import AdminPage from 'containers/Pages/Admin/AdminPage';
import DataLoader from 'containers/DataLoader';
import ScenarioComputerAction from 'containers/Widgets/ScenariosViewer/ScenarioComputerAction';

import 'antd/dist/antd.css';
import 'containers/globalStyles.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-tree/lib/main.css';
import SkudPage from './Pages/Skud/SkudPage';

const LayoutWrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

class Layout extends Component {
  redirectPath = window.location.pathname;

  render() {
    const { currentUser, draggableDeviceId, currentUserIsLoaded } = this.props;
    if (!currentUser && appConfig.securityOn) {
      return (
        <ThemeProvider theme={theme}>
          <LayoutWrapper
            onContextMenu={e => {
              e.preventDefault();
            }}
          >
            <ContentWrapper id="mainContent">
              <Route path="/login" component={LoginPage} />
              <Redirect from="/" to="/login" />
            </ContentWrapper>
          </LayoutWrapper>
        </ThemeProvider>
      );
    } else {
      return (
        <ThemeProvider theme={theme}>
          {!appConfig.securityOn || (appConfig.securityOn && currentUserIsLoaded && currentUser) ? (
            <LayoutWrapper
              onContextMenu={e => {
                e.preventDefault();
              }}
              className={draggableDeviceId ? 'dragged' : ' '}
            >
              <DataLoader />
              <Header {...this.props} />
              <ContentWrapper id="mainContent">
                <Route path="/projects" component={ProjectsPage} />
                <Route path="/op" component={OP} />
                <Route path="/config" component={ConfigPage} />
                <Route path="/skud" component={SkudPage} />
                <Route path="/admin" component={AdminPage} />
                <Route path="/" exact render={() => <Redirect from="/*" to="/projects" />} />
                <Route
                  path="/login"
                  exact
                  render={() => (
                    <Redirect
                      from="/*"
                      to={
                        this.redirectPath && this.redirectPath !== '/login'
                          ? this.redirectPath
                          : '/projects'
                      }
                    />
                  )}
                />
              </ContentWrapper>
              <ScenarioComputerAction />
            </LayoutWrapper>
          ) : null}
        </ThemeProvider>
      );
    }
  }
}

const checkCurrentUserIsLoaded = state => {
  return state.inProgress.requestCurrentUser === false || state.inProgress.requestLogin === false;
};

const mapStateToProps = state => {
  return {
    currentUserIsLoaded: checkCurrentUserIsLoaded(state),
    currentUser: state.currentUser.id ? state.currentUser : false,
    activeProject: state.activeProject.activeProject,
    draggableDeviceId: state.widgets.draggableDeviceId
  };
};

export default connect(mapStateToProps)(Layout);
