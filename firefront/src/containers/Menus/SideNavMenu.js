import React, { Component } from 'react';
import { connect } from 'react-redux';

import DefaultSideNav from 'components/Nav/SideNav';
import SideNavItem from 'components/Nav/SideNavItem';

class SideNav extends Component {
  render() {
    const { currentProjectId } = this.props;
    return <DefaultSideNav>
      <SideNavItem icon="network" link={`/projects/${currentProjectId}/plan/`} title={'План'} />
      <SideNavItem icon="connected" link={`/projects/${currentProjectId}/regions/`} title={'Зоны'}/>
      <SideNavItem icon="virtual" link={`/projects/${currentProjectId}/virtual/`} title={'Виртуальные состояния'}/>
      <SideNavItem icon="time" link={`/projects/${currentProjectId}/scenarios/`} title={'Сценарии'}/>
      <SideNavItem icon="cross" link={`/projects/${currentProjectId}/indicator/`} title={'Виртуальные индикаторы'}/>
    </DefaultSideNav>
  }
}

const mapStateToProps = state => {
  return {
    currentProjectId: state.currentProjectId
  }
}

export default connect(mapStateToProps)(SideNav);
