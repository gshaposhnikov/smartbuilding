import React, { Component } from 'react';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class UsersEditorMenu extends Component {
  render() {
    const {
      isAddActive,
      onAdd,
      isDeleteActive,
      onDelete,
      isRestoreActive,
      onRestore
    } = this.props;

    return (
      <Menu>
        <ButtonGroup />
        <ButtonGroup>
          <Button onClick={onAdd} icon="file-add" isActive={isAddActive}>
            Добавить
          </Button>
          {isRestoreActive ? (
            <Button onClick={onRestore} icon="unlock" isActive={isRestoreActive}>
              Восстановить
            </Button>
            ) : (
            <Button onClick={onDelete} icon="lock" isActive={isDeleteActive}>
              Заблокировать
            </Button>
          )}
        </ButtonGroup>
      </Menu>
    );
  }
}
