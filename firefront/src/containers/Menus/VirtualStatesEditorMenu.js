import React, { Component } from 'react';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class VirtualStatesEditorMenu extends Component {
  render() {
    const {
      isVirtualStateAddActive,
      onVirtualStateAdd,
      isVirtualStateDeleteActive,
      onVirtualStateDelete
    } = this.props;

    return (
      <Menu>
        <ButtonGroup />
        <ButtonGroup>
          <Button onClick={onVirtualStateAdd} icon="file-add" isActive={isVirtualStateAddActive}>
            Добавить
          </Button>
          <Button onClick={onVirtualStateDelete} icon="delete" isActive={isVirtualStateDeleteActive}>
            Удалить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
