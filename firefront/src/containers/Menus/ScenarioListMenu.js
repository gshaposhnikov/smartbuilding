import React from 'react';
import { Select } from 'antd';
const { Option } = Select;

import Title from 'components/Title';
import Menu from 'components/Menu';

const ScenarioListMenu = ({
  scenarioPurposes = [],
  scenariosFilter,
  onChangeScenariosFilter,
  ...restProps
}) => (
  <Menu isActive={true} {...restProps}>
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Title style={{ padding: ' 8px' }}>Фильтр:</Title>
      <Select style={{ width: 150 }} value={scenariosFilter} onChange={onChangeScenariosFilter}>
        <Option value="ALL">Все</Option>
        {scenarioPurposes.map((scenarioPurpose, index) => (
          <Option key={index} value={scenarioPurpose.id}>
            {scenarioPurpose.name}
          </Option>
        ))}
      </Select>
    </div>
  </Menu>
);

export default ScenarioListMenu;
