import React, { Component } from 'react';

import Menu from 'components/Menu';
import Title from 'components/Title';

import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class ScenarioAdvancedEditorMenu extends Component {
  render() {
    const {
      title,
      isSaveActive,
      onSaveClick,
      isEditStartLogicActive,
      onEditStartLogicClick,
      isEditStopLogicActive,
      onEditStopLogicClick
    } = this.props;

    return (
      <Menu>
        <ButtonGroup>
          <Title style={{ padding: '0 10px' }}>{title}</Title>
        </ButtonGroup>
        <ButtonGroup>
          <Button icon="edit" isActive={isEditStartLogicActive} onClick={onEditStartLogicClick}>
            Логика включения
          </Button>
          <Button icon="edit" isActive={isEditStopLogicActive} onClick={onEditStopLogicClick}>
            Логика отключения
          </Button>
          <Button icon="save" isActive={isSaveActive} onClick={onSaveClick}>
            Сохранить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
