import React from 'react';
import { connect } from 'react-redux';
import { InputNumber, Button } from 'antd';

import Menu from 'components/Menu';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import { ButtonWrapper } from 'components/Button/styles';

const INPUT_NUMBER_STYLES = {
  width: '5em',
  marginLeft: '5px'
};

class IndicatorPanelWidgetMenu extends React.Component {
  render = () => {
    const {
      isActive,
      selectedIndicatorPanel,
      onChangeWidth,
      onChangeHeight,
      onChangeAutosize,
      autosize,
      setHeightValue,
      setWidthValue,
      height,
      width
    } = this.props;
    return (
      <Menu isActive={isActive}>
        <ButtonGroup>
          <Title>
            {selectedIndicatorPanel
              ? selectedIndicatorPanel.name || 'Без имени'
              : 'Панель не выбрана'}
          </Title>
        </ButtonGroup>
        <ButtonGroup>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            <Button
              disabled={!selectedIndicatorPanel}
              onClick={onChangeAutosize}
              type={autosize ? 'default' : 'danger'}
              size="small"
            >
              Авторазмер
            </Button>
          </ButtonWrapper>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            Ширина:
            <InputNumber
              disabled={!selectedIndicatorPanel || autosize}
              onBlur={() => setWidthValue()}
              onChange={onChangeWidth}
              size="small"
              style={INPUT_NUMBER_STYLES}
              value={width || 40}
              precision={0}
              min={30}
              max={1000}
            />
          </ButtonWrapper>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            Высота:
            <InputNumber
              disabled={!selectedIndicatorPanel || autosize}
              onBlur={() => setHeightValue()}
              onChange={onChangeHeight}
              size="small"
              style={INPUT_NUMBER_STYLES}
              value={height || 40}
              precision={0}
              min={20}
              max={500}
            />
          </ButtonWrapper>
        </ButtonGroup>
      </Menu>
    );
  };
}

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;

  return {
    selectedIndicatorPanelId,
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.indicators.panels[state.currentProjectId]
        ? state.indicators.panels[state.currentProjectId][selectedIndicatorPanelId]
        : null
  };
};
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorPanelWidgetMenu);
