import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import Menu from 'components/Menu';
import Title from 'components/Title';

import ButtonGroup from 'components/Button/ButtonGroup';
import { ButtonWrapper } from 'components/Button/styles';
import { Switch, Popconfirm, Spin, Tooltip } from 'antd';

import { updateProjectStatus } from 'actions/projects';
import styled from 'styled-components';
import { notification } from 'antd';
import { checkCurrentProjectIsActive } from 'helpers/currentProject';
import {
  getActiveProject,
  getActiveDeviceList,
  getActiveRegions,
  getActiveScenarios,
  getActiveVirtualStates
} from 'helpers/activeProject';
import {
  getActiveEmployeesHash,
  getActiveAccessKeysHash,
  getActiveWorkSchedulesHash
} from 'helpers/skud';

const ACTIVATED_PROJECT_STATUS_COLORS = {
  DEFAULT: 'Gold',
  INACTIVE: 'Gold',
  ACTIVE: 'Lime',
  FAILURE: 'Red'
};
const ACTIVATED_PROJECT_TOOGLE_COLOR = 'Grey';
const ACTIVATED_PROJECT_TEXT_COLOR = 'Black';

const BUILD_PROJECT_STATUS_TITLE = 'Проект в разработке';

const ACTIVATED_PROJECT_STATUS_TITLES = {
  DEFAULT: 'Проект активирован, но не в работе',
  INACTIVE: 'Проект активирован, но не в работе',
  ACTIVE: 'Проект активирован и в работе',
  FAILURE: 'Проект активирован, но содержит ошибки'
};

const VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TYPE = {
  WARNING: 'warning',
  ERROR: 'error',
  DEFAULT: 'info'
};

const VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TITLE = {
  WARNING: 'Предупреждение',
  ERROR: 'Ошибка',
  DEFAULT: 'Информация'
};

const SwitchWrapper = styled.div`
  & .ant-switch-checked {
    ${props => (props.color === 'default' ? '' : `color: ${props.color}`)};
    ${props =>
      props.backgroundColor === 'default' ? '' : `background-color: ${props.backgroundColor}`};
  }
  & .ant-switch-inner {
    ${props => (props.color === 'default' ? '' : `color: ${props.color}`)};
    ${props =>
      props.backgroundColor === 'default' ? '' : `background-color: ${props.backgroundColor}`};
  }
  & .ant-switch {
    ${props => (props.color === 'default' ? '' : `color: ${props.color}`)};
    ${props =>
      props.backgroundColor === 'default' ? '' : `background-color: ${props.backgroundColor}`};
  }

  & .ant-switch:after {
    background-color: ${props => props.toggleColor};
    ${props => (props.toggleColor === 'default' ? '' : `background-color: ${props.toggleColor}`)};
  }
`;

class CurrentProjectMenu extends Component {
  doProjectStatusChange(checked) {
    const { currentProject, dispatch } = this.props;
    dispatch(updateProjectStatus(currentProject.id, checked ? 'ACTIVE' : 'BUILD'));
  }

  getValidateMessageEntityDescr(validateMessage, props) {
    const {
      activeDevices,
      activeRegions,
      activeScenarios,
      virtualStates,
      employeesHash,
      accessKeysHash,
      workSchedulesHash
    } = props;
    switch (validateMessage.entityType) {
      case 'DEVICE': {
        const device = activeDevices.find(device => device.id === validateMessage.entityId);
        if (device) {
          return `Устройство ${device.name} (${device.middleAddressPath})`;
        }
        break;
      }
      case 'REGION': {
        const region = activeRegions.find(region => region.id === validateMessage.entityId);
        if (region) {
          return `Зона: "${region.name}" (№${region.index})`;
        }
        break;
      }
      case 'SCENARIO': {
        const scenario = activeScenarios.find(scenario => scenario.id === validateMessage.entityId);
        if (scenario) {
          return `Сценарий: "${scenario.name}" (№${scenario.globalNo})`;
        }
        break;
      }
      case 'VIRTUAL_STATE': {
        const virtualState = virtualStates.find(
          virtualState => virtualState.id === validateMessage.entityId
        );
        if (virtualState) {
          return `Виртуальное состояние: "${virtualState.name}" (№${virtualState.globalNo})`;
        }
        break;
      }
      case 'EMPLOYEE':
        if (employeesHash) {
          const employee = employeesHash[validateMessage.entityId];
          if (employee) {
            return `Сотрудник: ${employee.name}`;
          }
        }
        return 'Неизвестный сотрудник';

      case 'ACCESS_KEY':
        if (accessKeysHash) {
          const accessKey = accessKeysHash[validateMessage.entityId];
          if (accessKey) {
            return accessKey.keyType === 'CARD'
              ? `Карточка доступа: ${accessKey.keyValue}`
              : `Пароль: ${accessKey.keyValue}`;
          }
        }
        return 'Неизвестный ключ доступа';

      case 'WORK_SCHEDULE':
        if (workSchedulesHash) {
          const workSchedule = workSchedulesHash[validateMessage.entityId];
          if (workSchedule) {
            return `График работ: ${workSchedule.name}`;
          }
        }
        return 'Неизвестный график работ';

      default:
        break;
    }
    return validateMessage.entityId;
  }

  showValidateMessages(props) {
    const { activeProject, currentProject } = props;
    if (activeProject && activeProject.validateMessages && currentProject.id === activeProject.id) {
      activeProject.validateMessages
        .sort((a, b) => a.level.localeCompare(b.level))
        .forEach(validateMessage => {
          const notifType =
            VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TYPE[validateMessage.level] ||
            VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TYPE.DEFAULT;
          notification[notifType]({
            message:
              VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TITLE[validateMessage.level] ||
              VALIDATE_MESSAGE_LEVEL_TO_NOTIF_TITLE.DEFAULT,
            description: `${this.getValidateMessageEntityDescr(validateMessage, props)}: ${
              validateMessage.description
            }`
          });
        });
    }
  }

  componentWillReceiveProps(newProps) {
    const { activeProject } = newProps;
    if (
      activeProject &&
      this.props.activeProject &&
      activeProject !== this.props.activeProject &&
      activeProject.validateMessages &&
      activeProject.validateMessages.length
    ) {
      this.showValidateMessages(newProps);
    }
  }

  render() {
    const {
      currentProject,
      activeProjectStatus,
      projectStatus,
      updateProjectStatusInProgress
    } = this.props;

    var isProjectActive = false;
    if (currentProject.id && currentProject.status === 'ACTIVE') {
      isProjectActive = true;
    }
    const projectName = currentProject.id
      ? currentProject.name + ' (версия: ' + currentProject.version + ')'
      : '';

    return (
      <Menu isActive={currentProject.id ? true : false}>
        <ButtonGroup>
          <Title>{projectName}</Title>
        </ButtonGroup>
        <ButtonGroup>
          <Title>{currentProject.description || ''}</Title>
        </ButtonGroup>
        <ButtonGroup>
          <ButtonWrapper isActive={true}>
            <Popconfirm
              placement="bottomRight"
              title="Изменить статус проекта?"
              onConfirm={() => {
                this.doProjectStatusChange(!isProjectActive);
              }}
              okText="Да"
              cancelText="Отмена"
            >
              <div>
                <Tooltip
                  title={
                    projectStatus === 'BUILD'
                      ? BUILD_PROJECT_STATUS_TITLE
                      : ACTIVATED_PROJECT_STATUS_TITLES[activeProjectStatus] ||
                        ACTIVATED_PROJECT_STATUS_TITLES.DEFAULT
                  }
                  placement="left"
                >
                  <SwitchWrapper
                    backgroundColor={
                      projectStatus === 'BUILD'
                        ? 'default'
                        : ACTIVATED_PROJECT_STATUS_COLORS[activeProjectStatus] ||
                          ACTIVATED_PROJECT_STATUS_COLORS.DEFAULT
                    }
                    color={projectStatus === 'BUILD' ? 'default' : ACTIVATED_PROJECT_TEXT_COLOR}
                    toggleColor={
                      projectStatus === 'BUILD' ? 'default' : ACTIVATED_PROJECT_TOOGLE_COLOR
                    }
                  >
                    <Switch
                      checked={isProjectActive}
                      checkedChildren={
                        updateProjectStatusInProgress || activeProjectStatus === 'INACTIVE' ? <Spin size="small" /> : <div>Актив.</div>
                      }
                      unCheckedChildren={
                        updateProjectStatusInProgress ? <Spin size="small" /> : <div>В разр.</div>
                      }
                      disabled={updateProjectStatusInProgress}
                    />
                  </SwitchWrapper>
                </Tooltip>
              </div>
            </Popconfirm>
          </ButtonWrapper>
        </ButtonGroup>
      </Menu>
    );
  }
}

const getProjectStatus = createSelector(
  checkCurrentProjectIsActive,
  ({ activeProject: { project } }) => project,
  (currentProjectIsActive, currentProject) =>
    currentProjectIsActive && currentProject ? currentProject.status : 'BUILD'
);

const mapStateToProps = createSelector(
  ({ activeProject: { activeProject } }) => (activeProject ? activeProject.status : 'BUILD'),
  getProjectStatus,
  getActiveProject,
  getActiveDeviceList,
  getActiveRegions,
  getActiveScenarios,
  getActiveVirtualStates,
  getActiveEmployeesHash,
  getActiveAccessKeysHash,
  getActiveWorkSchedulesHash,
  ({ inProgress: { updateProjectStatus } }) => updateProjectStatus,
  (
    activeProjectStatus,
    projectStatus,
    activeProject,
    activeDevices,
    activeRegions,
    activeScenarios,
    virtualStates,
    employeesHash,
    accessKeysHash,
    workSchedulesHash,
    updateProjectStatusInProgress
  ) => ({
    activeProjectStatus,
    projectStatus,
    activeProject,
    activeDevices,
    activeRegions,
    activeScenarios,
    virtualStates,
    employeesHash,
    accessKeysHash,
    workSchedulesHash,
    updateProjectStatusInProgress
  })
);

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentProjectMenu);
