import React, { Component } from 'react';
import { connect } from 'react-redux';

import Menu from 'components/Menu';
import Tabs, { Tab } from 'components/Tabs';
import ButtonGroup from 'components/Button/ButtonGroup';

import { deviceTreeChangeViewMode } from 'actions/widgets';

import { getViewMode } from 'helpers/widgets';

class DeviceTreeMenu extends Component {
  changeViewMode = mode => {
    if (mode !== this.props.viewMode) {
      this.props.dispatch(deviceTreeChangeViewMode(mode));
    }
  };
  render() {
    const { viewMode } = this.props;
    return (
      <Menu color="#e1e1e1" isActive={true}>
        <ButtonGroup>
          <Tabs>
            <Tab
              color="black"
              small
              active={viewMode === 'all' ? true : false}
              onClick={() => this.changeViewMode('all')}
            >
              Все
            </Tab>
            <Tab
              color="black"
              small
              active={viewMode === 'regions' ? true : false}
              onClick={() => this.changeViewMode('regions')}
            >
              По зонам
            </Tab>
          </Tabs>
        </ButtonGroup>
      </Menu>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {
    viewMode: getViewMode(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceTreeMenu);
