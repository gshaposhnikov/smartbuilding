import React, { Component } from 'react';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import ErrorTitle from 'components/Title/ErrorTitle';

export default class WidgetParamsEditorMenu extends Component {
  render() {
    const {
      isViewTitle,
      title,
      errorMessage,
      isSaveActive,
      onSaveAction,
      titlePlacement = 'left'
    } = this.props;

    return (
      <Menu isActive={isSaveActive}>
        {titlePlacement === 'left' ? (
          <ButtonGroup>
            {isViewTitle ? <Title>{title}</Title> : null}
            {isViewTitle && errorMessage ? <ErrorTitle>{errorMessage}</ErrorTitle> : null}
          </ButtonGroup>
        ) : null}
        <ButtonGroup>
          <Button onClick={onSaveAction} icon="save" isActive={isSaveActive}>
            Сохранить
          </Button>
        </ButtonGroup>
        {titlePlacement === 'right' ? (
          <ButtonGroup>
            {isViewTitle ? <Title>{title}</Title> : null}
            {isViewTitle && errorMessage ? <ErrorTitle>{errorMessage}</ErrorTitle> : null}
          </ButtonGroup>
        ) : null}
      </Menu>
    );
  }
}
