import React, { Component } from 'react';
import { Tooltip } from 'antd';

import Menu from 'components/Menu';
import Button from 'components/Button';
import Title from 'components/Title';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class MonitorableValuesMenu extends Component {
  render() {
    const { isActive, name, onButtonClick, info } = this.props;

    return (
      <Menu isActive={isActive}>
        <ButtonGroup>
          <Title style={{ padding: '0 10px' }}> Устройство: {name}</Title>
          <Tooltip title="Изменить временной диапазон">
            <Button icon="edit" isActive={isActive} onClick={onButtonClick} />
          </Tooltip>
        </ButtonGroup>
        <ButtonGroup>
          <Title style={{ padding: '0 10px' }}>{info}</Title>
        </ButtonGroup>
      </Menu>
    );
  }
}
