import React, { Component } from 'react';
import { connect } from 'react-redux';

import Menu from 'components/Menu';
import Title from 'components/Title';

import ButtonGroup from 'components/Button/ButtonGroup';

class ActiveDeviceDetailsMenu extends Component {
  render() {
    const { name } = this.props;
    return (
      <Menu isActive={true} style={{ height: '30px' }}>
        <ButtonGroup>
          <Title style={{ padding: '0 10px' }}> Устройство: {name}</Title>
        </ButtonGroup>
      </Menu>
    );
  }
}

export default connect(null, null)(ActiveDeviceDetailsMenu);
