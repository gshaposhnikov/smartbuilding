import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Menu from 'components/Menu';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';

export default class ScenarioViewMenu extends Component {
  static propTypes = {
    title: PropTypes.string
  };
  render() {
    const { title } = this.props;

    return (
      <Menu>
        <ButtonGroup>
          <Title>{title}</Title>
        </ButtonGroup>
      </Menu>
    );
  }
}
