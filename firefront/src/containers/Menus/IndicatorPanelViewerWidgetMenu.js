import React from 'react';
import { connect } from 'react-redux';
import { InputNumber, Button } from 'antd';

import Menu from 'components/Menu';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import { ButtonWrapper } from 'components/Button/styles';

const INPUT_NUMBER_STYLES = {
  width: '5em',
  marginLeft: '5px'
};

class IndicatorPanelWidgetMenu extends React.Component {
  render = () => {
    const {
      selectedIndicatorPanel,
      onChangeWidth,
      onChangeHeight,
      onChangeAutosize,
      autosize,
      height,
      width
    } = this.props;
    return (
      <Menu isActive={true}>
        <ButtonGroup>
          <Title>
            {selectedIndicatorPanel
              ? selectedIndicatorPanel.name || 'Без имени'
              : 'Панель не выбрана'}
          </Title>
        </ButtonGroup>
        <ButtonGroup>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            <Button
              disabled={!selectedIndicatorPanel}
              onClick={onChangeAutosize}
              type={autosize ? 'default' : 'danger'}
              size="small"
            >
              Авторазмер
            </Button>
          </ButtonWrapper>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            Ширина:{' '}
            <InputNumber
              disabled={!selectedIndicatorPanel || autosize}
              onChange={onChangeWidth}
              size="small"
              style={INPUT_NUMBER_STYLES}
              value={width || 40}
              min={20}
              max={500}
              precision={0}
            />
          </ButtonWrapper>
          <ButtonWrapper isActive={!!selectedIndicatorPanel}>
            Высота:
            <InputNumber
              disabled={!selectedIndicatorPanel || autosize}
              onChange={onChangeHeight}
              size="small"
              style={INPUT_NUMBER_STYLES}
              value={height || 40}
              min={20}
              max={500}
              precision={0}
            />
          </ButtonWrapper>
        </ButtonGroup>
      </Menu>
    );
  };
}

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;

  return {
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.activeProject.indicators.panels
        ? state.activeProject.indicators.panels[selectedIndicatorPanelId]
        : null
  };
};
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorPanelWidgetMenu);
