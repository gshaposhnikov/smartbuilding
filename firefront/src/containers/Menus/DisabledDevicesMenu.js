import React, { Component } from 'react';

import Menu from 'components/Menu';

import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class DisabledDevicesMenu extends Component {
  render() {
    const { isActive, onEnableDevice } = this.props;

    return (
      <Menu isActive={isActive}>
        <ButtonGroup>
          <Button
            icon="double-right"
            iconPlacement={'right'}
            isActive={isActive}
            onClick={onEnableDevice}
          >
            Удалить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
