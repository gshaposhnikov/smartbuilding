import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';
import ErrorTitle from 'components/Title/ErrorTitle';

import { Tooltip } from 'antd';

export default class ScenarioTimeLineMenu extends Component {
  static propTypes = {
    isAddActionButtonActive: PropTypes.bool,
    isAddExecButtonActive: PropTypes.bool,
    isAddTraceButtonActive: PropTypes.bool,
    isAddEnclosedButtonActive: PropTypes.bool,
    isEditButtonActive: PropTypes.bool,
    isDeleteButtonActive: PropTypes.bool,
    onAddActionBlockButtonClick: PropTypes.func,
    onAddExecBlockButtonClick: PropTypes.func,
    onAddTraceBlockButtonClick: PropTypes.func,
    onAddEnclosedBlockButtonClick: PropTypes.func,
    onEditButtonClick: PropTypes.func,
    onDeleteButtonClick: PropTypes.func,
    errorMessage: PropTypes.string,
    title: PropTypes.string
  };
  render() {
    const {
      isAddActionButtonActive,
      isAddExecButtonActive,
      isAddTraceButtonActive,
      isEditButtonActive,
      isDeleteButtonActive,
      onAddActionBlockButtonClick,
      onAddExecBlockButtonClick,
      onAddTraceBlockButtonClick,
      onEditButtonClick,
      onDeleteButtonClick,
      errorMessage,
      title
    } = this.props;

    return (
      <Menu>
        <ButtonGroup>
          {errorMessage ? <ErrorTitle>{errorMessage}</ErrorTitle> : title && <Title>{title}</Title>}
        </ButtonGroup>
        <ButtonGroup>
          <Tooltip title="Добавить действие на компьютере">
            <Button
              onClick={onAddActionBlockButtonClick}
              icon="plus-square-o"
              isActive={isAddActionButtonActive}
            >
              Действие
            </Button>
          </Tooltip>
          <Tooltip title="Добавить исполнительный блок">
            <Button
              onClick={onAddExecBlockButtonClick}
              icon="file-add"
              isActive={isAddExecButtonActive}
            >
              Исполнение
            </Button>
          </Tooltip>
          <Tooltip title="Добавить блок слежения">
            <Button
              onClick={onAddTraceBlockButtonClick}
              icon="folder-add"
              isActive={isAddTraceButtonActive}
            >
              Cлежение
            </Button>
          </Tooltip>
          <Tooltip title="Изменить блок">
            <Button onClick={onEditButtonClick} icon="edit" isActive={isEditButtonActive}>
              Изменить
            </Button>
          </Tooltip>
          <Tooltip title="Удалить блок">
            <Button onClick={onDeleteButtonClick} icon="delete" isActive={isDeleteButtonActive}>
              Удалить
            </Button>
          </Tooltip>
        </ButtonGroup>
      </Menu>
    );
  }
}
