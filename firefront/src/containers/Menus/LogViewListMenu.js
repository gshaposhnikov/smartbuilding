import React from 'react';
import Menu from 'components/Menu';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

const LogViewListMenu = ({
  isAddActive,
  onAdd,
  isDeleteActive,
  onDelete,
  isToUpActive,
  onToUp,
  isToDownActive,
  onToDown
}) => (
  <Menu isActive={isAddActive}>
    <ButtonGroup>
      <Button onClick={onToUp} icon="caret-up" isActive={isToUpActive}>
        Вверх
      </Button>
      <Button onClick={onToDown} icon="caret-down" isActive={isToDownActive}>
        Вниз
      </Button>
      <Button onClick={onAdd} icon="file-add" isActive={isAddActive}>
        Добавить
      </Button>
      <Button onClick={onDelete} icon="delete" isActive={isDeleteActive}>
        Удалить
      </Button>
    </ButtonGroup>
  </Menu>
);

export default LogViewListMenu;
