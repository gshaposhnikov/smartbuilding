import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Badge } from 'antd';
import { isDirty } from 'redux-form';

import { modalOpen, modalClose } from 'actions/modals';

import MenuWrapper from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import { getActiveDeviceList, getProject } from 'helpers/activeProject';
import ChooseControlDevicesForm, { resetFormValues } from '../Forms/ChooseControlDevicesForm';
import { writeSkudDBToControlDevices } from 'actions/skud';

import { FORM_NAME as SKUD_TREE_ITEM_FORM_NAME } from 'containers/Widgets/Skud/SkudTreeItemParams';
import { FORM_NAME as WORK_SCHEDULE_FORM_NAME } from 'containers/Widgets/Skud/WorkScheduleParams';

const UploadContentStyle = {
  width: 120,
  display: 'flex',
  displayDirection: 'row',
  justifyContent: 'space-between'
};

const CHOOSE_CONTROLS_MODAL_NAME = 'CHOOSE_CONTROLS_MODAL_NAME';

const SkudMenu = ({
  activeProject,
  activeProjectTitle,
  activeControlDevices,
  activeControlDevicesWithSkudDBOutOfDate,
  activeControlDevicesWithSkudDBOutOfDateIds,
  uploadDBIsActive,
  dispatch
}) => {
  return (
    <MenuWrapper isActive={true}>
      <ButtonGroup>
        <Title>{activeProjectTitle}</Title>
      </ButtonGroup>

      <ButtonGroup>
        <ChooseControlDevicesForm
          onSubmit={({ controlDeviceIds }) => {
            dispatch(writeSkudDBToControlDevices(activeProject.id, controlDeviceIds));
            dispatch(modalClose(CHOOSE_CONTROLS_MODAL_NAME));
          }}
          controlDevices={activeControlDevices}
          modalName={CHOOSE_CONTROLS_MODAL_NAME}
        />
        {/* TODO: раскомментировать, когда будут реализованы новые пункты */
        /* <Dropdown
          overlay={
            <Menu
              onClick={({ key }) => {
                switch (key) {
                  case '2':
                    resetFormValues(dispatch, {
                      controlDeviceIds: activeControlDevicesWithSkudDBOutOfDateIds
                    });
                    dispatch(modalOpen(CHOOSE_CONTROLS_MODAL_NAME));
                    break;
                  default:
                    break;
                }
              }}
            >
              <Menu.Item key="1" disabled>
                Автосохранение
              </Menu.Item>
              <Menu.Item key="2">Целиком</Menu.Item>
              <Menu.Item key="3" disabled>
                Целиком по USB
              </Menu.Item>
            </Menu>
          }
        >
          <Button icon="upload" isActive={true}>
            <div style={UploadContentStyle}>
              <Badge count={activeControlDevicesWithSkudDBOutOfDate.length}> </Badge>
              Запись в приборы
            </div>
          </Button>
        </Dropdown> */}
        <Button
          icon="upload"
          isActive={uploadDBIsActive}
          onClick={() => {
            resetFormValues(dispatch, {
              controlDeviceIds: activeControlDevicesWithSkudDBOutOfDateIds
            });
            dispatch(modalOpen(CHOOSE_CONTROLS_MODAL_NAME));
          }}
        >
          <div style={UploadContentStyle}>
            <Badge count={activeControlDevicesWithSkudDBOutOfDate.length}> </Badge>
            Запись в приборы
          </div>
        </Button>
      </ButtonGroup>
    </MenuWrapper>
  );
};

const getActiveProjectTitle = createSelector(
  getProject,
  activeProject => (activeProject ? `${activeProject.name} (версия: ${activeProject.version})` : '')
);

const getActiveControlDevices = createSelector(
  getActiveDeviceList,
  activeDevices =>
    activeDevices
      ? activeDevices.filter(activeDevice => activeDevice.deviceCategory === 'CONTROL')
      : []
);

const getActiveControlDevicesWithSkudDBOutOfDate = createSelector(
  getActiveControlDevices,
  activeControlDevices =>
    activeControlDevices
      ? activeControlDevices.filter(
          activeDevice =>
            activeDevice.activeStateViews &&
            activeDevice.activeStateViews.find(stateView => stateView.id === 'skudDBIsOutOfDate')
        )
      : []
);

const getActiveControlDevicesWithSkudDBOutOfDateIds = createSelector(
  getActiveControlDevicesWithSkudDBOutOfDate,
  activeControlDevicesWithSkudDBOutOfDate =>
    activeControlDevicesWithSkudDBOutOfDate.map(device => device.id)
);

const getUploadDBIsActive = createSelector(
  state => !isDirty(SKUD_TREE_ITEM_FORM_NAME)(state) && !isDirty(WORK_SCHEDULE_FORM_NAME)(state),
  uploadDBIsActive => uploadDBIsActive
);

const mapStateToProps = createSelector(
  [
    getProject,
    getActiveProjectTitle,
    getActiveControlDevices,
    getActiveControlDevicesWithSkudDBOutOfDate,
    getActiveControlDevicesWithSkudDBOutOfDateIds,
    getUploadDBIsActive
  ],
  (
    activeProject,
    activeProjectTitle,
    activeControlDevices,
    activeControlDevicesWithSkudDBOutOfDate,
    activeControlDevicesWithSkudDBOutOfDateIds,
    uploadDBIsActive
  ) => ({
    activeProject,
    activeProjectTitle,
    activeControlDevices,
    activeControlDevicesWithSkudDBOutOfDate,
    activeControlDevicesWithSkudDBOutOfDateIds,
    uploadDBIsActive
  })
);

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SkudMenu);
