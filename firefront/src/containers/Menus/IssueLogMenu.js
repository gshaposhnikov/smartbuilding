import React from 'react';

import Menu from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

const TITLE_STYLES = { padding: '0 10px' };

const IssueLogMenu = ({
  title,
  isPauseActive,
  onPauseClick,
  isPlayActive,
  onPlayClick,
  isDeleteActive,
  onDeleteClick,
  isDeleteAllActive,
  onDeleteAllClick,
  isDeleteFinishedActive,
  onDeleteFinishedClick,
  isDeleteErrorActive,
  onDeleteErrorClick
}) => (
  <Menu isActive={true}>
    <ButtonGroup>
      <Title style={TITLE_STYLES}>{title}</Title>
    </ButtonGroup>
    <ButtonGroup>
      <Button icon="caret-right" isActive={isPlayActive} onClick={onPlayClick}>
        Выполнить
      </Button>
      <Button icon="pause" isActive={isPauseActive} onClick={onPauseClick}>
        Пауза
      </Button>
      <Button icon="delete" isActive={isDeleteActive} onClick={onDeleteClick}>
        Удалить
      </Button>
      <Button icon="exclamation-circle-o" isActive={isDeleteAllActive} onClick={onDeleteAllClick}>
        Очистить очередь
      </Button>
      <Button
        icon="check-circle-o"
        isActive={isDeleteFinishedActive}
        onClick={onDeleteFinishedClick}
      >
        Удалить выполненые
      </Button>
      <Button icon="close-circle-o" isActive={isDeleteErrorActive} onClick={onDeleteErrorClick}>
        Удалить с ошибками
      </Button>
    </ButtonGroup>
  </Menu>
);

export default IssueLogMenu;
