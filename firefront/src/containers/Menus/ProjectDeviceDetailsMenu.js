import React from 'react';
import { Tooltip } from 'antd';

import Menu from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

const MENU_STYLES = { height: '30px' };
const TITLE_STYLES = { padding: '0 10px' };

const ProjectDeviceDetails = ({
  name,
  activeTab,
  isSaveActive,
  onSaveClick,
  isSyncActive,
  onSyncClick,
  isDownloadActive,
  onDownloadClick,
  isUploadActive,
  onUploadClick,
  isSetDefaultsActive,
  onSetDefaultsClick
}) => (
  <Menu isActive={true} style={MENU_STYLES}>
    <ButtonGroup>
      <Title style={TITLE_STYLES}> Устройство: {name}</Title>
    </ButtonGroup>
    {activeTab === 'config' ? (
      <ButtonGroup>
        <Tooltip title="Считать с устройства">
          <Button icon="download" isActive={isDownloadActive} onClick={onDownloadClick} />
        </Tooltip>
        <Tooltip title="Синхронизировать">
          <Button icon="sync" isActive={isSyncActive} onClick={onSyncClick} />
        </Tooltip>
        <Tooltip title="Записать на устройство">
          <Button icon="upload" isActive={isUploadActive} onClick={onUploadClick} />
        </Tooltip>
      </ButtonGroup>
    ) : null}
    {activeTab === 'config' || activeTab === 'properties' ? (
      <ButtonGroup>
        <Tooltip title="По умолчанию">
          <Button icon="rollback" isActive={isSetDefaultsActive} onClick={onSetDefaultsClick} />
        </Tooltip>
        <Tooltip title="Сохранить">
          <Button icon="save" isActive={isSaveActive} onClick={onSaveClick} />
        </Tooltip>
      </ButtonGroup>
    ) : null}
  </Menu>
);

export default ProjectDeviceDetails;
