import React, { Component } from 'react';

import Menu from 'components/Menu';

import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

export default class EnabledDevicesMenu extends Component {
  render() {
    const { isActive, onDisableDevice } = this.props;

    return (
      <Menu isActive={isActive}>
        <ButtonGroup>
          <Button icon="double-left" isActive={isActive} onClick={onDisableDevice}>
            Добавить
          </Button>
        </ButtonGroup>
      </Menu>
    );
  }
}
