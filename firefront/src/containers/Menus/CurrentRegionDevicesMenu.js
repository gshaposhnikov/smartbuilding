import React from 'react';

import Menu from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';

const CurrentRegionDevicesMenu = ({
  region,
  isDisconnectDeviceListActive,
  onDisconnectDeviceList
}) => (
  <Menu isActive={region.id ? true : false}>
    <Title>{`${region.id ? `Устройства зоны: ${region.name}` : 'Выберите зону'}`}</Title>
    <Button
      icon="double-right"
      iconStyles={{ transform: 'rotate(90deg)' }}
      isActive={isDisconnectDeviceListActive}
      onClick={onDisconnectDeviceList}
    >
      Удалить из зоны
    </Button>
  </Menu>
);

export default CurrentRegionDevicesMenu;
