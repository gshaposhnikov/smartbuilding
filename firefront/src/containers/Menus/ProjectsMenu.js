import React, { Component } from 'react';

import { connect } from 'react-redux';

import { modalOpen } from 'actions/modals';

import Menu from 'components/Menu';

import IconButton from 'components/Button/antd';
import ButtonGroup from 'components/Button/ButtonGroup';

class ProjectsMenu extends Component {
  render() {
    const { onModalOpen } = this.props;
    return (
      <Menu>
        <ButtonGroup>
          <IconButton onClick={() => onModalOpen('newProject')} icon="plus" />
        </ButtonGroup>
      </Menu>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onModalOpen: (name) => { dispatch(modalOpen(name)) }
  }
}

export default connect(null, mapDispatchToProps)(ProjectsMenu);