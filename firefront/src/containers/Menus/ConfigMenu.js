import React from 'react';
import { Menu as DropdownMenu, Dropdown } from 'antd';

import Menu from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

const TITLE_WRAPPER_STYLES = { width: '200px' };
const TITLE_STYLES = { padding: '0 10px' };

const ConfigMenu = ({
  title,
  isSetDefaultsActive,
  onSetDefaultsClick,
  isDownloadActive,
  onDownloadClick,
  isUploadActive,
  onUploadClick,
  isSaveActive,
  onSaveClick,
  isControlDevice,
  onUploadDBClick,
  onUploadAllDBsClick,
  onExportControlDeviceConfig,
  onSyncControlDeviceTime
}) => (
  <Menu>
    <ButtonGroup>
      <div style={TITLE_WRAPPER_STYLES}>
        <Title style={TITLE_STYLES}>{title}</Title>
      </div>
      <Dropdown
        overlay={
          <DropdownMenu
            onClick={({ item }) => {
              if (item && item.props && item.props.action) item.props.action();
            }}
          >
            <DropdownMenu.Item disabled={!isControlDevice} action={() => onUploadDBClick()}>
              Записать в текущий
            </DropdownMenu.Item>
            <DropdownMenu.Item action={() => onUploadAllDBsClick()}>
              Записать во все
            </DropdownMenu.Item>
            <DropdownMenu.Item
              disabled={!isControlDevice}
              action={() => onExportControlDeviceConfig()}
            >
              Экспорт
            </DropdownMenu.Item>
          </DropdownMenu>
        }
        placement="bottomLeft"
      >
        <Button icon="database" title="БД прибора">
          БД прибора
        </Button>
      </Dropdown>
      <Button
        icon="clock-circle-o"
        title="Синхронизировать время прибора"
        isActive={isControlDevice}
        onClick={onSyncControlDeviceTime}
      >
        Cинх. время
      </Button>
    </ButtonGroup>
    <ButtonGroup>
      <Button icon="rollback" isActive={isSetDefaultsActive} onClick={onSetDefaultsClick}>
        По умолчанию
      </Button>
      <Button icon="download" isActive={isDownloadActive} onClick={onDownloadClick}>
        Считать
      </Button>
      <Button icon="upload" isActive={isUploadActive} onClick={onUploadClick}>
        Записать
      </Button>
      <Button icon="save" isActive={isSaveActive} onClick={onSaveClick}>
        Сохранить
      </Button>
    </ButtonGroup>
  </Menu>
);

export default ConfigMenu;
