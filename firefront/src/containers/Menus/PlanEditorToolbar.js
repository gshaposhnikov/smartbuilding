import React from 'react';

import Toolbar from 'components/Toolbar';

export const DEFAULT_SELECT_TOOL = {
  key: 'select',
  title: 'Выделение',
  icon: 'cursor',
  customIcon: true,
  selectable: true,
  type: 'button'
};

const tools = [
  [
    DEFAULT_SELECT_TOOL,
    {
      key: 'rect',
      title: 'Создание прямоугольной зоны',
      icon: 'rect',
      customIcon: true,
      selectable: true,
      type: 'button'
    },
    {
      key: 'polygon',
      title: 'Создание многоугольной зоны',
      icon: 'polygon',
      customIcon: true,
      selectable: true,
      type: 'button'
    },
    {
      key: 'grid',
      title: 'Закрепить за сеткой',
      altTitle: 'Открепить от сетки',
      icon: 'grid',
      customIcon: true,
      selectable: false,
      type: 'flag'
    },
    {
      key: 'gridSize',
      title: 'Размер сетки',
      type: 'options',
      action: 'changeGridSize',
      options: [{ value: 10 }, { value: 20 }, { value: 30 }, { value: 50 }, { value: 100 }]
    },
    {
      key: 'delete',
      title: 'Удалить выбранный элемент плана',
      icon: 'delete',
      customIcon: false,
      selectable: false,
      type: 'button'
    },
    {},
    {
      key: 'background',
      title: 'Добавить фоновое изображение',
      icon: 'gallery',
      customIcon: true,
      selectable: false,
      type: 'button'
    },
    {
      key: 'moveBackground',
      title: 'Переместить фоновое изображение',
      icon: 'movePicture',
      customIcon: true,
      selectable: true,
      type: 'button'
    },
    {
      key: 'deleteBackground',
      title: 'Удалить фоновое изображение',
      icon: 'emptyPicture',
      customIcon: true,
      selectable: false,
      type: 'button'
    }
  ]
];

const PlanEditorToolbar = props => <Toolbar tools={tools} {...props} />;

export default PlanEditorToolbar;
