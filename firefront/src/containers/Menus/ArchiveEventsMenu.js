import React from 'react';

import Menu from 'components/Menu';
import Title from 'components/Title';

import Button from 'components/Button';
import ButtonGroup from 'components/Button/ButtonGroup';

const TITLE_STYLES = { padding: '0 10px' };

const ArchiveEventsMenu = ({
  title,
  isSaveEventTableActive,
  onSaveEventTable,
  isConstructFiltersActive,
  onConstructFiltersClick
}) => (
  <Menu>
    <ButtonGroup>
      <Title style={TITLE_STYLES}>{title}</Title>
    </ButtonGroup>
    <ButtonGroup>
      <Button icon="download" isActive={isSaveEventTableActive} onClick={onSaveEventTable}>
        Скачать
      </Button>
      <Button icon="filter" isActive={isConstructFiltersActive} onClick={onConstructFiltersClick}>
        Фильтр
      </Button>
    </ButtonGroup>
  </Menu>
);

export default ArchiveEventsMenu;
