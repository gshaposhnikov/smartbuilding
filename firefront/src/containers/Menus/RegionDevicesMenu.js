import React from 'react';

import Menu from 'components/Menu';
import Title from 'components/Title';
import Button from 'components/Button';

const RegionDevicesMenu = ({ region, isConnectDeviceListActive, onConnectDeviceList }) => (
  <Menu isActive={region.id ? true : false}>
    <Title>Доступные устройства для добавления в зону</Title>
    <Button
      icon="double-left"
      iconStyles={{ transform: 'rotate(90deg)' }}
      isActive={isConnectDeviceListActive}
      onClick={onConnectDeviceList}
    >
      Добавить в зону
    </Button>
  </Menu>
);

export default RegionDevicesMenu;
