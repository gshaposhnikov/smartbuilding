import React, { Component } from 'react';

import DefaultSideNav from 'components/Nav/SideNav';
import SideNavItem from 'components/Nav/SideNavItem';

export default class SkudNav extends Component {
  render() {
    return (
      <DefaultSideNav>
        <SideNavItem icon="team" link="/skud/employees/" title="Картотека" />
        <SideNavItem icon="time" link="/skud/work_schedules/" title="Графики работы" />
        <SideNavItem icon="book" link="/skud/events_archive/" title="Архив событий" />
      </DefaultSideNav>
    );
  }
}
