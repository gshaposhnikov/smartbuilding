import React, { Component } from 'react';
import styled from 'styled-components';

import Menu from 'components/Menu';
import { NavLink } from 'react-router-dom';

import Tabs, { Tab } from 'components/Tabs';

export const HeaderLeft = styled.div`
  display: flex;
`;

export const HeaderRight = styled.div`
  display: flex;
  align-items: center;
`;

export const HeaderWrapper = styled.div`
  height: 35px;
`;

export default class Header extends Component {
  render() {
    return (
      <HeaderWrapper>
        <Menu isActive={true}>
          <HeaderLeft>
            <Tabs light>
              <NavLink to="/op/plan/" activeClassName="active">
                <Tab>План</Tab>
              </NavLink>
              <NavLink to="/op/log/" activeClassName="active">
                <Tab>Архив событий</Tab>
              </NavLink>
              <NavLink to="/op/disabled_devices/" activeClassName="active">
                <Tab>Отключенные устройства</Tab>
              </NavLink>
              <NavLink to="/op/device_parameters/" activeClassName="active">
                <Tab>Параметры устройств</Tab>
              </NavLink>
              <NavLink to="/op/scenarios/" activeClassName="active">
                <Tab>Сценарии</Tab>
              </NavLink>
              <NavLink to="/op/indicator/" activeClassName="active">
                <Tab>Индикатор</Tab>
              </NavLink>
            </Tabs>
          </HeaderLeft>
          <HeaderRight />
        </Menu>
      </HeaderWrapper>
    );
  }
}
