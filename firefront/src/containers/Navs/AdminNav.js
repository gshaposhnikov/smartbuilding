import React, { Component } from 'react';

import DefaultSideNav from 'components/Nav/SideNav';
import SideNavItem from 'components/Nav/SideNavItem';

export default class AdminNav extends Component {
  render() {
    return (
      <DefaultSideNav>
        <SideNavItem icon="user" link={`/admin/users/`} title={'Пользователи'} />
        <SideNavItem icon="team" link={`/admin/usergroups/`} title={'Группы пользователей'} />
        {/*
          TODO: Реализовать справочник прав
          <SideNavItem icon="access" link={`/admin/access/`} title={'Права'} />
        */}
        {/*
          TODO: Реализовать интерфейсов
          <SideNavItem icon="layouts" link={`/admin/interfaces/`} title={'Интерфейсы'} />
        */}
        {/*
          TODO: Реализовать справочник серверов
          <SideNavItem icon="servers" link={`/admin/servers/`} title={'Сервера'} />
        */}
        <SideNavItem
          icon="Gallery"
          link={`/admin/device_shape_library/`}
          title={'Библиотека шейпов устройств'}
        />
        <SideNavItem icon="book" link={`/admin/log_views/`} title={'Журналы'} />
        <SideNavItem icon="tone" link={`/admin/sounds/`} title={'Звуки'} />
        <SideNavItem
          icon="notification"
          link={`/admin/sound_notifications/`}
          title={'Звуковые оповещения'}
        />
      </DefaultSideNav>
    );
  }
}
