import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Tabs, { Tab } from 'components/Tabs';

import { includesInUserFilterTags } from 'helpers/filtration';

export default class HeaderNav extends Component {
  render() {
    const { activeProject, currentUser } = this.props;
    const isAvailableActiveProject =
      activeProject && activeProject.status === 'ACTIVE' ? true : false;
    let viewAdmin = !currentUser ? true : false;
    let viewSkud = !currentUser ? true : false;
    let isAvailableSkudPage = !currentUser ? true : false;
    if (currentUser) {
      let allPermissions = [
        ...(currentUser.groupPermissions || []),
        ...(currentUser.permissionIds || [])
      ];
      viewAdmin = allPermissions.includes('ADMINISTRATE');
      viewSkud = allPermissions.includes('SKUD_READ');
      isAvailableSkudPage =
        isAvailableActiveProject && includesInUserFilterTags(currentUser, 'SKUD');
    }
    return (
      <Tabs>
        <NavLink to="/projects/" exact activeClassName="active">
          <Tab>Проекты</Tab>
        </NavLink>
        <NavLink
          isActive={(match, location) =>
            location.pathname.includes(`/projects/${activeProject && activeProject.id}`)
          }
          to={`/projects/${activeProject && activeProject.id}/plan`}
          disabled={!activeProject}
          activeClassName="active"
        >
          <Tab className={activeProject ? '' : 'disabled'}>Активный проект</Tab>
        </NavLink>
        <NavLink
          isActive={(match, location) => location.pathname.includes('/op/')}
          to="/op/plan"
          disabled={!isAvailableActiveProject}
          activeClassName="active"
        >
          <Tab className={isAvailableActiveProject ? '' : 'disabled'}>Оперативная задача</Tab>
        </NavLink>
        <NavLink strict to="/config/" disabled={!isAvailableActiveProject} activeClassName="active">
          <Tab className={isAvailableActiveProject ? '' : 'disabled'}>Конфигуратор устройств</Tab>
        </NavLink>
        {viewSkud && (
          <NavLink
            isActive={(match, location) => location.pathname.includes('/skud/')}
            to="/skud/employees"
            disabled={!isAvailableSkudPage}
            activeClassName="active"
          >
            <Tab className={isAvailableSkudPage ? '' : 'disabled'}>СКУД</Tab>
          </NavLink>
        )}
        {viewAdmin && (
          <NavLink
            isActive={(match, location) => location.pathname.includes('/admin/')}
            to="/admin/users/"
            activeClassName="active"
          >
            <Tab>Администрирование</Tab>
          </NavLink>
        )}
      </Tabs>
    );
  }
}
