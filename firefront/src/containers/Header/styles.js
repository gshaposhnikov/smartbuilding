import styled from 'styled-components';

export const HeaderLeft = styled.div`
  display: flex;
`;

export const HeaderRight = styled.div`
  display: flex;
  align-items: center;
`;

export const HeaderWrapper = styled.div`
  height: 34px;
`;
