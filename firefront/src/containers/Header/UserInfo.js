import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Popconfirm } from 'antd';

import Icon from 'components/Icon';

import { logoutUser } from 'actions/login';

const Wrapper = styled.div`
  padding: 0 10px;
  display: flex;
  height: 35px;
  justify-content: center;
  align-items: center;
`;

const PopupText = styled.div`
  margin: 0 10px;
  color: white;
  font-size: 13px;
`;

const LogoutButton = styled.div`
  margin: 0 10px;
  color: white;
  font-size: 13px;
  border-width: 4px;
  border-style: inset;
  border-color: rgba(37, 55, 75, 0.18);
  cursor: pointer;
`;

class UserInfo extends Component {
  onLogout = () => {
    const { dispatch } = this.props;
    dispatch(logoutUser());
  };
  render() {
    return (
      <Wrapper>
        <div>
          <Icon name="user" color="#fff" />
        </div>
        <PopupText>{this.props.currentUserName && this.props.currentUserName}</PopupText>
        <Popconfirm
          placement="bottomRight"
          title="Выйти?"
          onConfirm={this.onLogout}
          okText="Да"
          cancelText="Отмена"
        >
          <LogoutButton>Выход</LogoutButton>
        </Popconfirm>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUserName: state.currentUser ? state.currentUser.fullName : 'unknown'
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfo);
