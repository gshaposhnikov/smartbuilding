import React, { Component } from 'react';

import Menu from 'components/Menu';

import UserInfo from './UserInfo';

import UserTime from 'components/UserTime';
import HeaderNav from 'containers/Navs/HeaderNav';
import AppInfo from 'containers/AppInfo';
import SoundSystemNotifications from 'containers/SoundSystemNotifications';

import * as s from './styles';

export default class Header extends Component {
  render() {
    return (
      <s.HeaderWrapper>
        <Menu isActive={true} color="#25374B">
          <s.HeaderLeft>
            <HeaderNav {...this.props} />
          </s.HeaderLeft>
          <s.HeaderRight>
            <AppInfo {...this.props.appInfo} />
            <UserTime />
            <SoundSystemNotifications />
            <UserInfo />
          </s.HeaderRight>
        </Menu>
      </s.HeaderWrapper>
    );
  }
}
