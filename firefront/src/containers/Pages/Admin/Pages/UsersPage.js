import React, { Component } from 'react';

import Grid from 'components/Grid';

import UserList from 'containers/Widgets/UsersWidget/UserList';
import UserInfoEditor from 'containers/Widgets/UsersWidget/UserInfoEditor';

class UsersPage extends Component {
  render() {
    const components = [
      { name: 'UserList', component: UserList },
      { name: 'UserInfoEditor', component: UserInfoEditor }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Пользователи',
              component: 'UserList',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'UserInfoEditor'
            },
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default UsersPage;
