import React, { Component } from 'react';

import Grid from 'components/Grid';

import LogViewsList from 'containers/Widgets/LogViews/LogViewsList';
import LogViewsParams from 'containers/Widgets/LogViews/LogViewsParams';

class LogViewsPage extends Component {
  render() {
    const components = [
      { name: 'LogViewsList', component: LogViewsList },
      { name: 'LogViewsParams', component: LogViewsParams }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Журналы',
              component: 'LogViewsList',
              width: 50
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'LogViewsParams'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default LogViewsPage;
