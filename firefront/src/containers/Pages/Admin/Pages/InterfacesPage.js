import React, { Component } from 'react';

import Grid from 'components/Grid';

import InterfaceList from 'containers/Widgets/InterfacesWidget/InterfaceList';
import InterfaceInfo from 'containers/Widgets/InterfacesWidget/InterfaceInfo';

class InterfacesPage extends Component {
  render() {
    const components = [
      { name: 'InterfaceList', component: InterfaceList },
      { name: 'InterfaceInfo', component: InterfaceInfo }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Интерфейсы',
              component: 'InterfaceList',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Общие',
              component: 'InterfaceInfo'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default InterfacesPage;
