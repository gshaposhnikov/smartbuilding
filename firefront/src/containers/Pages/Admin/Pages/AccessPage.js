import React, { Component } from 'react';

import Grid from 'components/Grid';

import AccessList from 'containers/Widgets/AccessWidget/AccessList';
import AccessInfo from 'containers/Widgets/AccessWidget/AccessInfo';

class AccessPage extends Component {
  render() {
    const components = [
      { name: 'AccessList', component: AccessList },
      { name: 'AccessInfo', component: AccessInfo }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Права',
              component: 'AccessList',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Общие',
              component: 'AccessInfo'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default AccessPage;
