import React, { Component } from 'react';

import Grid from 'components/Grid';

import SoundList from 'containers/Widgets/Sounds/SoundList';
import SoundParams from 'containers/Widgets/Sounds/SoundParams';

class SoundsPage extends Component {
  render() {
    const components = [
      { name: 'SoundList', component: SoundList },
      { name: 'SoundParams', component: SoundParams }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Звуки',
              component: 'SoundList',
              width: 50
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'SoundParams'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default SoundsPage;
