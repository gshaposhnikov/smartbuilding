import React, { Component } from 'react';

import Grid from 'components/Grid';

import UserGroupList from 'containers/Widgets/UserGroupsWidget/UserGroupList';
import UserGroupInfoEditor from 'containers/Widgets/UserGroupsWidget/UserGroupInfoEditor';

class UserGroupsPage extends Component {
  render() {
    const components = [
      { name: 'UserGroupList', component: UserGroupList },
      { name: 'UserGroupInfoEditor', component: UserGroupInfoEditor }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Группы пользователей',
              component: 'UserGroupList',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'UserGroupInfoEditor'
            },
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default UserGroupsPage;
