import React, { Component } from 'react';

import Grid from 'components/Grid';

import NotificationList from 'containers/Widgets/Sounds/NotificationList';
import NotificationParams from 'containers/Widgets/Sounds/NotificationParams';

class SoundsPage extends Component {
  render() {
    const components = [
      { name: 'NotificationList', component: NotificationList },
      { name: 'NotificationParams', component: NotificationParams }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Оповещения',
              component: 'NotificationList',
              width: 50
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'NotificationParams'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default SoundsPage;
