import React, { Component } from 'react';

import Grid from 'components/Grid';

import DeviceShapeTree from '../../../Widgets/DeviceShapeLibrary/DeviceShapeTree';
import DeviceShapeLibraryParams from '../../../Widgets/DeviceShapeLibrary/DeviceShapeLibraryParams';

class UserGroupsPage extends Component {
  render() {
    const components = [
      { name: 'DeviceShapeTree', component: DeviceShapeTree },
      { name: 'DeviceShapeParams', component: DeviceShapeLibraryParams }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Библиотека шейпов',
              component: 'DeviceShapeTree',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Параметры',
              component: 'DeviceShapeParams'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default UserGroupsPage;
