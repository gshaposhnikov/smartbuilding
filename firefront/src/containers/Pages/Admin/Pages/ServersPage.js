import React, { Component } from 'react';

import Grid from 'components/Grid';

import ServerList from 'containers/Widgets/ServersWidget/ServerList';
import ServerInfo from 'containers/Widgets/ServersWidget/ServerInfo';

class ServersPage extends Component {
  render() {
    const components = [
      { name: 'ServerList', component: ServerList },
      { name: 'ServerInfo', component: ServerInfo }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Сервера',
              component: 'ServerList',
              width: 45
            },
            {
              type: 'react-component',
              title: 'Общие',
              component: 'ServerInfo'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

export default ServersPage;
