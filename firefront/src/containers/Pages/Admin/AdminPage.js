import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';

import AdminNav from 'containers/Navs/AdminNav';

import UsersPage from './Pages/UsersPage';
import UserGroupsPage from './Pages/UserGroupsPage';
import InterfacesPage from './Pages/InterfacesPage';
import AccessPage from './Pages/AccessPage';
import ServersPage from './Pages/ServersPage';
import DeviceShapeLibPage from './Pages/DeviceShapeLibPage';
import LogViewsPage from './Pages/LogViewsPage';
import SoundsPage from './Pages/SoundsPage';
import SoundNotificationsPage from './Pages/SoundNotificationsPage';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex: 1;
`;

export default class AdminPage extends Component {
  render() {
    return (
      <PageWrapper>
        <AdminNav match={this.props.match} />
        <Route path="/admin/users" component={UsersPage} />
        <Route path="/admin/usergroups" component={UserGroupsPage} />
        <Route path="/admin/access" component={AccessPage} />
        <Route path="/admin/interfaces" component={InterfacesPage} />
        <Route path="/admin/device_shape_library" component={DeviceShapeLibPage} />
        <Route path="/admin/servers" component={ServersPage} />
        <Route path="/admin/log_views" component={LogViewsPage} />
        <Route path="/admin/sounds" component={SoundsPage} />
        <Route path="/admin/sound_notifications" component={SoundNotificationsPage} />
      </PageWrapper>
    );
  }
}
