import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loadActiveProject } from 'actions/projects';
import { loadActiveDevices } from 'actions/devices';

import Grid from 'components/Grid';

import ConfigDeviceTree from 'containers/Widgets/DevicesTreeWidget/ConfigDevicesTree';
import ConfigWidget from 'containers/Widgets/ConfigWidget';
import IssueLog from 'containers/Widgets/IssueLog';

import { getActiveDeviceList } from 'helpers/activeProject';

class ConfigPage extends Component {
  fetchActiveData(props) {
    const { dispatch } = props;
    const { project, devices } = props;
    const {
      loadActiveProjectError,
      loadActiveDevicesError,
      loadActiveProjectInProgress,
      loadActiveDevicesInProgress
    } = props;

    if (!project && !loadActiveProjectError && !loadActiveProjectInProgress) {
      dispatch(loadActiveProject());
    }
    if (project && !devices && !loadActiveDevicesError && !loadActiveDevicesInProgress) {
      dispatch(loadActiveDevices(project.id));
    }
  }

  componentDidMount() {
    this.fetchActiveData(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.fetchActiveData(newProps);
  }

  render() {
    const components = [
      { name: 'ConfigDeviceTree', component: ConfigDeviceTree },
      { name: 'ConfigWidget', component: ConfigWidget },
      { name: 'IssueLog', component: IssueLog }
    ];

    const config = {
      content: [
        {
          type: 'column',
          content: [
            {
              type: 'row',
              content: [
                {
                  title: 'Дерево устройств',
                  type: 'react-component',
                  component: 'ConfigDeviceTree',
                  width: 33
                },
                {
                  title: 'Конфигуратор устройства',
                  type: 'react-component',
                  component: 'ConfigWidget'
                }
              ]
            },
            {
              title: 'Очередь сервера',
              type: 'react-component',
              component: 'IssueLog',
              height: 30
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}

const mapStateToProps = state => {
  return {
    project: state.activeProject.project,
    devices: getActiveDeviceList(state),

    loadActiveProjectError: state.errors.loadActiveProject,
    loadActiveDevicesError: state.errors.loadActiveDevices,

    loadActiveProjectInProgress: state.inProgress.loadActiveProject,
    loadActiveDevicesInProgress: state.inProgress.loadActiveDevices
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigPage);
