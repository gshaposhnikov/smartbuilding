import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';

import SideNav from 'containers/Menus/SideNavMenu';

import PlanPage from './Pages/PlanPage';
import RegionsPage from './Pages/RegionsPage';
import ScenariosEditorPage from './Pages/ScenariosEditorPage';
import VirtualPage from './Pages/VirtualPage';
import IndicatorPage from './Pages/IndicatorPage';

import CurrentProjectWidget from 'containers/Widgets/CurrentProjectWidget';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex: 1;
`;

export default class CurrentProject extends Component {
  render() {
    const currentProjectId = this.props.match.params.project;
    return (
      <PageWrapper>
        <CurrentProjectWidget currentProjectId={currentProjectId} />
        <ContentWrapper>
          <SideNav match={this.props.match} />
          <Route path="/projects/:project/plan" component={PlanPage} />
          <Route path="/projects/:project/regions" component={RegionsPage} />
          <Route path="/projects/:project/virtual" component={VirtualPage} />
          <Route path="/projects/:project/scenarios" component={ScenariosEditorPage} />
          <Route path="/projects/:project/indicator" component={IndicatorPage} />
        </ContentWrapper>
      </PageWrapper>
    );
  }
}
