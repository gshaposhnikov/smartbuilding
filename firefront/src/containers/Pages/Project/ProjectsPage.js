import React, { Component } from 'react';
import Grid from 'components/Grid';
import ProjectsWidget from 'containers/Widgets/ProjectsWidget';
import IssueLog from 'containers/Widgets/IssueLog';

export default class Projects extends Component {

  render(){

    const components = [
      { name: 'ProjectsWidget', component: ProjectsWidget },
      { name: 'IssueLog', component: IssueLog }
    ];

    const config = ({
      content: [{
        type: 'column',
        content: [{
            type: 'react-component',
            title: 'Проекты',
            component: 'ProjectsWidget'
          },{
            type: 'react-component',
            title: 'Очередь сервера',
            component: 'IssueLog',
            height: 30
        }]
      }]
    })

    return <Grid config={config} components={components} />
  }
}