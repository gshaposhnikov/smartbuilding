import React, { Component } from 'react';

import Grid from 'components/Grid';

import VirtualStatesEditor from 'containers/Widgets/VirtualStatesEditor';
import VirtualStateParamsEditor from 'containers/Widgets/VirtualStatesEditor/VirtualStateParamsEditor';

export default class Virtual extends Component {

  render() {
    const components = [
      { name: 'VirtualStatesEditor', component: VirtualStatesEditor },
      { name: 'VirtualStateParamsEditor', component: VirtualStateParamsEditor }
    ];

    const config = {
      content: [{
        type: 'row',
        content: [
          {
            title: 'Виртуальные состояния',
            type: 'react-component',
            component: 'VirtualStatesEditor',
            width: 40
          },
          {
            type: 'column',
            content: [
              {
                title: 'Параметры',
                type: 'react-component',
                component: 'VirtualStateParamsEditor'
              }
            ]
          }
        ]
      }]
    };
    return <Grid config={config} components={components} />;
  }
}
