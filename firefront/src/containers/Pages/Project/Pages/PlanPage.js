import React, { Component } from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';

import Grid from 'components/Grid';
import DevicesTreeWidget from 'containers/Widgets/DevicesTreeWidget/ProjectDevicesTree';
import PlanEditorWidget from 'containers/Widgets/PlanEditor/PlanEditorWidget';
import IssueLog from 'containers/Widgets/IssueLog';
import ProjectValidateMessages from 'containers/Widgets/ProjectValidateMessages';


const PlanWrapper = styled.div`
  width: 100%;
  display: flex;
  flex: 1;
`;

class Plan extends Component {
 
  render() {
    const components = [
      { name: 'DevicesTreeWidget', component: DevicesTreeWidget },
      { name: 'PlanEditorWidget', component: PlanEditorWidget },
      { name: 'IssueLog', component: IssueLog },
      { name: 'ProjectValidateMessages', component: ProjectValidateMessages }
    ];
    const config = {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'react-component',
              title: 'Устройства',
              component: 'DevicesTreeWidget',
              width: 30
            },

            {
              type: 'column',
              content: [
                {
                  type: 'react-component',
                  title: 'Редактор планов',
                  component: 'PlanEditorWidget'
                },
                {
                  type: 'stack',
                  height: 31,
                  content: [
                    {
                      title: 'Ошибки проекта',
                      type: 'react-component',
                      component: 'ProjectValidateMessages'
                    },
                    {
                      title: 'Очередь сервера',
                      type: 'react-component',
                      component: 'IssueLog'
                    },
                    {
                      title: 'Ошибки импорта проекта',
                      type: 'react-component',
                      component: 'ProjectValidateMessages',
                      props: { isCurrentProjectImported : true}
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
    return (
      <PlanWrapper>
        <Grid config={config} components={components} />
      </PlanWrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    log: state.page.log
  };
};

export default connect(mapStateToProps)(Plan);
