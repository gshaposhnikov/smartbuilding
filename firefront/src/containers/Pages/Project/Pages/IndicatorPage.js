import React from 'react';

import Grid from 'components/Grid';

import GroupList from 'containers/Widgets/IndicatorsEditor/GroupList';
import PanelList from 'containers/Widgets/IndicatorsEditor/PanelList';
import IndicatorPanel from 'containers/Widgets/IndicatorsEditor/IndicatorPanel';

const components = [
  { name: 'GroupList', component: GroupList },
  { name: 'PanelList', component: PanelList },
  { name: 'IndicatorPanel', component: IndicatorPanel }
];
const config = {
  content: [
    {
      type: 'row',
      content: [
        {
          type: 'column',
          width: 35,
          content: [
            {
              title: 'Группы',
              type: 'react-component',
              component: 'GroupList',
              height: 38
            },
            {
              title: 'Панели',
              type: 'react-component',
              component: 'PanelList'
            }
          ]
        },
        {
          type: 'column',
          content: [
            {
              title: 'Панель',
              type: 'react-component',
              component: 'IndicatorPanel'
            }
          ]
        }
      ]
    }
  ]
};

const IndicatorPage = () => <Grid config={config} components={components} />;
export default IndicatorPage;
