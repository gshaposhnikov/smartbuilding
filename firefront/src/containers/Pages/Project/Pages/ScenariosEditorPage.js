import React, { Component } from 'react';

import Grid from 'components/Grid';

import ScenariosEditor from 'containers/Widgets/ScenariosEditor';
import AdvancedParams from 'containers/Widgets/ScenariosEditor/AdvancedParams';
import TimeLineBlocks from 'containers/Widgets/ScenariosEditor/TimeLineBlocks';

export default class ScenariosEditorPage extends Component {
  render() {
    const components = [
      { name: 'ScenariosEditor', component: ScenariosEditor },
      { name: 'AdvancedParams', component: AdvancedParams },
      { name: 'TimeLineBlocks', component: TimeLineBlocks }
    ];

    const config = {
      content: [{
        type: 'row',
        content: [
          {
            type: 'column',
            width: 35,
            content: [
              {
                title: 'Сценарии проекта',
                type: 'react-component',
                component: 'ScenariosEditor',
                height: 38
              },
              {
                title: 'Параметры',
                type: 'react-component',
                component: 'AdvancedParams'
              }
            ]
          },
          {
            type: 'column',
            content: [
              {
                title: 'Блоки времени',
                type: 'react-component',
                component: 'TimeLineBlocks'
              }
            ]
          }
        ]
      }]
    };

    return <Grid config={config} components={components} />;
  }
}
