import React, { Component } from 'react';

import Grid from 'components/Grid';

import RegionEditor from 'containers/Widgets/RegionEditorWidget/RegionEditor';
import RegionDevices from 'containers/Widgets/RegionEditorWidget/RegionDevices';

export default class Page extends Component {
  components = [
    { name: 'RegionEditor', component: RegionEditor },
    { name: 'RegionDevices', component: RegionDevices }
  ];
  config = {
    content: [
      {
        type: 'row',
        content: [
          {
            type: 'react-component',
            title: 'Зоны',
            component: 'RegionEditor'
          },
          {
            title: 'Текущие устройства',
            type: 'react-component',
            component: 'RegionDevices'
          }
        ]
      }
    ]
  };
  render() {
    return <Grid config={this.config} components={this.components} />;
  }
}
