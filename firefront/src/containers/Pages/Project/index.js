import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';

import ProjectsPage from './ProjectsPage';
import ProjectPage from './ProjectPage';

const ProjectsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
`;

export default class ProjectsMainPage extends Component {

  render() {
    return (
      <ProjectsWrapper>
        <Route exact path="/projects/" component={ProjectsPage} />
        <Route path="/projects/:project/" component={ProjectPage} />
      </ProjectsWrapper>
    )
  }
}