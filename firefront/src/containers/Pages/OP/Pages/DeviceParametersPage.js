import React, { Component } from 'react';

import Grid from 'components/Grid';

import DeviceList from 'containers/Widgets/MonitorableValuesWidget/DeviceList';
import History from 'containers/Widgets/MonitorableValuesWidget/History';

export default class DeviceParametersPage extends Component {
  render() {
    const components = [
      { name: 'DeviceList', component: DeviceList },
      { name: 'History', component: History }
    ];

    const config = {
      content: [
        {
          type: 'column',
          content: [
            {
              title: 'Дерево устройств',
              type: 'react-component',
              component: 'DeviceList'
            },
            {
              title: 'Графики',
              type: 'react-component',
              component: 'History'
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}
