import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Icon, Tooltip } from 'antd';
import { createSelector } from 'reselect';
import { difference } from 'lodash/array';

import Grid from 'components/Grid';
import DevicesTreeWidget from 'containers/Widgets/DevicesTreeWidget/ActiveDevicesTree';
import PlanViewerWidget from 'containers/Widgets/PlanViewer/PlanViewerWidget';
import LogWidget from 'containers/Widgets/LogWidget';
import IssueLog from 'containers/Widgets/IssueLog';

const TREE_ID = 'activeTree',
  PLAN_VIEWER_ID = 'planViewer',
  LOG_WIDGET_ID = 'logWidget',
  ROOT_LOG = 'all',
  BUTTON_STYLES = {
    position: 'absolute',
    top: '50%',
    left: 0,
    padding: '5px',
    borderRadius: '0 50% 50%  0',
    cursor: 'pointer',
    zIndex: 1000
  };

class PlanPage extends Component {
  state = {
    treeEnabled: true
  };

  components = [
    { name: 'DevicesTreeWidget', component: DevicesTreeWidget },
    { name: 'PlanViewerWidget', component: PlanViewerWidget },
    { name: 'LogWidget', component: LogWidget },
    { name: 'IssueLog', component: IssueLog }
  ];

  config = {
    content: [
      {
        type: 'row',
        content: [
          {
            id: TREE_ID,
            title: 'Дерево устройств',
            type: 'react-component',
            component: 'DevicesTreeWidget',
            width: 35
          },
          {
            type: 'column',
            content: [
              {
                id: PLAN_VIEWER_ID,
                title: 'Планы помещений',
                type: 'react-component',
                component: 'PlanViewerWidget',
                height: 66
              },
              {
                id: LOG_WIDGET_ID,
                type: 'stack',
                height: 34,
                content: [
                  {
                    title: 'Очередь сервера',
                    type: 'react-component',
                    component: 'IssueLog'
                  },
                  {
                    id: ROOT_LOG,
                    title: 'Журнал событий',
                    type: 'react-component',
                    component: 'LogWidget',
                    isActive: true,
                    props: { logViewId: ROOT_LOG, logView: {} }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  };
  layout = {};

  // Функция для скрытия/отображения дерева устройств
  hideShowTree() {
    const { treeEnabled, treeComponent } = this.state;
    // Проверяем, что компоненты были проинициализированы
    if (this.layout.root) {
      // Если дерево отображается
      if (treeEnabled) {
        // Ищем дерево
        const tree = this.layout.root.contentItems[0].getItemsById(TREE_ID)[0];
        // Если дерево найдено
        if (tree) {
          // Удаляем дерево
          tree.parent.removeChild(tree, true);
          this.setState({ treeEnabled: false, treeComponent: tree });
        }
      }
      // Если дерево скрыто
      else {
        // Проверяем, что элемент дерева сохранился в state
        if (treeComponent) {
          // Старый глобальный слой
          const oldLayoutState = this.layout.root.contentItems[0],
            // Создаем новый глобальный слой
            newLayoutState = this.layout.createContentItem({
              type: 'row',
              content: []
            }),
            // Обертка для видимых компонентов
            childLayoutWrapper = this.layout.createContentItem({
              type: 'column',
              content: []
            }),
            // Обертка для дерева
            treeLayoutWrapper = this.layout.createContentItem({
              type: 'stack',
              content: []
            });
          // Отмечаем, что новый слой уже проинициализирован
          newLayoutState.isInitialised = true;
          // Оборачиваем дерево
          treeLayoutWrapper.addChild(treeComponent);
          // Добавляем дерево на глобальный слой
          newLayoutState.addChild(treeLayoutWrapper);
          // Оборачиваем видимые компонентов
          oldLayoutState.contentItems.forEach(child => childLayoutWrapper.addChild(child));
          // Добавляем видимые компоненты на глобальный слой
          newLayoutState.addChild(childLayoutWrapper);
          // Заменяем старый глобальный слой на новый
          this.layout.root.replaceChild(oldLayoutState, newLayoutState);
          // Выставляем дереву размеры в соответствии с изначальными конфигурациями
          treeComponent.container.setSize(
            parseInt(this.layout.width / 100, 10) * treeComponent.config.width,
            this.layout.height
          );
          // Ищем компонент с планами
          const planViewer = newLayoutState.parent.contentItems[0].getItemsById(PLAN_VIEWER_ID)[0];
          // Если есть компонент с планами
          if (planViewer)
            // Подгоняем высоту планов в соответствии с изначальными конфигурациями
            planViewer.container.setSize(
              this.layout.width - treeComponent.container.width,
              parseInt(this.layout.height / 100, 10) * planViewer.config.height
            );
          this.setState({ treeEnabled: true });
          window.dispatchEvent(new Event('rerender-tree'));
        }
      }
    }
  }

  componentDidMount = () => {
    if (this.layout.root) {
      const container = this.layout.root.getItemsById(LOG_WIDGET_ID)[0];
      if (container) {
        const { activeLogViews } = this.props;
        if (activeLogViews.length) {
          this.updatedLogViews(this.props);
        } else {
          container.setActiveContentItem(container.getItemsById(ROOT_LOG)[0]);
        }
      }
    }
  };

  componentWillReceiveProps = newProps => {
    this.updatedLogViews(this.props, newProps);
  };

  updatedLogViews = (prevProps, nextProps) => {
    if (!nextProps) {
      if (this.layout.root) {
        const { activeLogViews } = prevProps;
        this.initLogViews(activeLogViews);
      }
    } else {
      if (this.layout.root) {
        const { activeLogViews: prevLogViews } = prevProps;
        const { activeLogViews: nextLogViews } = nextProps;
        if (
          prevLogViews.length !== nextLogViews.length ||
          difference(prevLogViews, nextLogViews).length
        ) {
          const container = this.layout.root.getItemsById(LOG_WIDGET_ID)[0];
          for (let index = container.contentItems.length - 1; index > 0; --index) {
            if (
              container.contentItems[index] &&
              container.contentItems[index].config.id &&
              container.contentItems[index].config.id !== ROOT_LOG
            )
              container.contentItems[index].remove();
          }
          this.initLogViews(nextLogViews);
        }
      }
    }
  };

  initLogViews = logViews => {
    const container = this.layout.root.getItemsById(LOG_WIDGET_ID)[0];
    if (container) {
      logViews.forEach((logView, index) => {
        const logViewConfig = this.getLogViewConfig(logView);
        container.addChild(logViewConfig);
      });
      container.setActiveContentItem(container.getItemsById(ROOT_LOG)[0]);
    }
  };

  getLogViewConfig = logView => {
    return {
      title: logView.name,
      id: logView.id,
      type: 'react-component',
      component: 'LogWidget',
      props: { logViewId: logView.id, logView }
    };
  };

  render() {
    const { treeEnabled } = this.state;
    return (
      <Grid
        config={this.config}
        getLayout={layout => {
          this.layout = layout;
          /**
           * Добавляет триггер на инициализацию Golden Layout
           * после сработки которого рендерятся представления
           */
          this.layout.on('initialised', () => this.updatedLogViews(this.props));
        }}
        components={this.components}
      >
        <Tooltip placement="right" title={`${treeEnabled ? 'Скрыть' : 'Показать'} дерево`}>
          <Button type="primary" style={BUTTON_STYLES} onClick={() => this.hideShowTree()}>
            <Icon type={`${treeEnabled ? 'left' : 'right'}`} />
          </Button>
        </Tooltip>
      </Grid>
    );
  }
}

const getActiveLogViews = createSelector(
  logViews => logViews,
  logViews => logViews.filter(logView => logView.active && !logView.isRoot)
);

const mapStateToProps = state => {
  return {
    activeLogViews: getActiveLogViews(state.activeProject.logEvents.logViews)
  };
};

export default connect(mapStateToProps)(PlanPage);
