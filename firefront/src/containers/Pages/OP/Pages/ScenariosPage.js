import React, { Component } from 'react';

import Grid from 'components/Grid';

import ScenarioList from 'containers/Widgets/ScenariosViewer/ScenarioList';
import ScenarioParams from 'containers/Widgets/ScenariosViewer/ScenarioParams';
import ScenarioView from 'containers/Widgets/ScenariosViewer/ScenarioView';

export default class ScenariosEditorPage extends Component {
  render() {
    const components = [
      { name: 'ScenarioList', component: ScenarioList },
      { name: 'ScenarioParams', component: ScenarioParams },
      { name: 'ScenarioView', component: ScenarioView }
    ];

    const config = {
      content: [
        {
          type: 'row',
          content: [
            {
              type: 'column',
              width: 35,
              content: [
                {
                  title: 'Список сценариев',
                  type: 'react-component',
                  component: 'ScenarioList',
                  height: 38
                },
                {
                  title: 'Параметры',
                  type: 'react-component',
                  component: 'ScenarioParams'
                }
              ]
            },
            {
              type: 'column',
              content: [
                {
                  title: 'Сценарий',
                  type: 'react-component',
                  component: 'ScenarioView'
                }
              ]
            }
          ]
        }
      ]
    };

    return <Grid config={config} components={components} />;
  }
}
