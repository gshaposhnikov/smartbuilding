import React, { Component } from 'react';

import Grid from 'components/Grid';

import ArchiveEventsWidget from 'containers/Widgets/ArchiveEventsWidget';

export default class LogPage extends Component {
  render() {
    const components = [{ name: 'ArchiveEventsWidget', component: ArchiveEventsWidget }];

    const config = {
      content: [
        {
          type: 'row',
          content: [
            {
              title: 'Архив событий',
              type: 'react-component',
              component: 'ArchiveEventsWidget',
              width: 50
            }
          ]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}
