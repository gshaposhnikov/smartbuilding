import React, { Component } from 'react';

import Grid from 'components/Grid';

import DisabledDevicesWidget from 'containers/Widgets/DisabledDevicesWidget/DisabledDevicesWidget';
import EnabledDevicesTree from 'containers/Widgets/DevicesTreeWidget/EnabledDevicesTree';
import IssueLog from 'containers/Widgets/IssueLog';

export default class DisabledDevicesPage extends Component {
  render() {
    const components = [
      { name: 'DisabledDevicesWidget', component: DisabledDevicesWidget },
      { name: 'EnabledDevicesTree', component: EnabledDevicesTree },
      { name: 'IssueLog', component: IssueLog }
    ];

    const config = {
      content: [
        {
          type: 'column',
          content: [{
            type: 'row',
            content: [
              {
                title: 'Отключенные устройства',
                type: 'react-component',
                component: 'DisabledDevicesWidget',
                width: 1
              },
              {
                title: 'Все устройства',
                type: 'react-component',
                component: 'EnabledDevicesTree',
                width: 2
              }
            ]
          },{
            title: 'Очередь сервера',
            type: 'react-component',
            component: 'IssueLog',
            height: 34
          }]
        }
      ]
    };
    return <Grid config={config} components={components} />;
  }
}
