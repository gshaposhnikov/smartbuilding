import React, { Component } from 'react';
import styled from 'styled-components';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import OpNav from 'containers/Navs/OpNav';

import OpSystemInfoWidget from 'containers/Widgets/OpSystemInfoWidget';

import PlanPage from './Pages/PlanPage';
import LogPage from './Pages/LogPage';
import DisabledDevicesPage from './Pages/DisabledDevicesPage';
import DeviceParametersPage from './Pages/DeviceParametersPage';
import ScenariosPage from './Pages/ScenariosPage';
import IndicatorPage from './Pages/IndicatorPage';

import { loadActiveProject } from 'actions/projects';

import { getProject, getTimers } from 'helpers/activeProject';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;

class Page extends Component {
  fetchActiveData(props) {
    const { dispatch } = props;
    const { project } = props;
    const { loadActiveProjectError, loadActiveProjectInProgress } = props;

    if (!project && !loadActiveProjectError && !loadActiveProjectInProgress) {
      dispatch(loadActiveProject());
    }
  }

  componentDidMount() {
    this.fetchActiveData(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.fetchActiveData(newProps);
  }

  render() {
    const { project } = this.props;

    return (
      <PageWrapper>
        <OpSystemInfoWidget project={project} />
        <OpNav />
        <Route path="/op/plan/" component={PlanPage} />
        <Route path="/op/log/" component={LogPage} />
        <Route path="/op/disabled_devices/" component={DisabledDevicesPage} />
        <Route path="/op/device_parameters/" component={DeviceParametersPage} />
        <Route path="/op/scenarios/" component={ScenariosPage} />
        <Route path="/op/indicator/" component={IndicatorPage} />
      </PageWrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: getProject(state),
    timers: getTimers(state),

    loadActiveProjectError: state.errors.loadActiveProject,
    loadActiveProjectInProgress: state.inProgress.loadActiveProject
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);
