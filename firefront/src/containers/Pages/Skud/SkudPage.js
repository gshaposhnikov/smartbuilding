import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import styled from 'styled-components';

import SkudNav from 'containers/Navs/SkudNav';

import EmployeesPage from './Pages/EmployeesPage';
import WorkSchedulesPage from './Pages/WorkSchedulesPage';
import LogPage from './Pages/LogPage';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const ContentWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex: 1;
`;

export default class SkudPage extends Component {
  render() {
    return (
      <PageWrapper>
        <ContentWrapper>
          <SkudNav match={this.props.match} />
          <Route path="/skud/employees" component={EmployeesPage} />
          <Route path="/skud/work_schedules" component={WorkSchedulesPage} />
          <Route path="/skud/events_archive" component={LogPage} />
        </ContentWrapper>
      </PageWrapper>
    );
  }
}
