import React, { Component } from 'react';
import styled from 'styled-components';

import { SKUD_LOG, SKUD_LOG_COLUMNS } from 'constants/skud';

import Grid from 'components/Grid';

import SkudTree from 'containers/Widgets/Skud/SkudTree';
import SkudTreeItemParams from 'containers/Widgets/Skud/SkudTreeItemParams';
import LogWidget from 'containers/Widgets/LogWidget';
import IssueLog from 'containers/Widgets/IssueLog';
import ProjectValidateMessages from 'containers/Widgets/ProjectValidateMessages';
import SkudMenu from '../../../Menus/SkudMenu';

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const components = [
  { name: 'SkudTree', component: SkudTree },
  { name: 'SkudTreeItemParams', component: SkudTreeItemParams },
  { name: 'LogWidget', component: LogWidget },
  { name: 'IssueLog', component: IssueLog },
  { name: 'ProjectValidateMessages', component: ProjectValidateMessages }
];

const config = {
  content: [
    {
      type: 'column',
      content: [
        {
          type: 'row',
          content: [
            {
              title: 'Картотека',
              type: 'react-component',
              component: 'SkudTree'
            },
            {
              title: 'Параметры',
              type: 'react-component',
              component: 'SkudTreeItemParams'
            }
          ]
        },
        {
          type: 'stack',
          height: 31,
          content: [
            {
              id: SKUD_LOG.id,
              title: SKUD_LOG.name,
              type: 'react-component',
              component: 'LogWidget',
              isActive: true,
              props: {
                logViewId: SKUD_LOG.id,
                logView: SKUD_LOG,
                columns: SKUD_LOG_COLUMNS
              }
            },
            {
              title: 'Очередь сервера',
              type: 'react-component',
              component: 'IssueLog'
            },
            {
              title: 'Ошибки проекта',
              type: 'react-component',
              component: 'ProjectValidateMessages',
              props : {
                isActive: true
              }
            }
          ]
        }
      ]
    }
  ]
};

export default class EmployeesPage extends Component {
  render() {
    return (
      <PageWrapper>
        <SkudMenu />
        <Grid config={config} components={components} />
      </PageWrapper>
    );
  }
}
