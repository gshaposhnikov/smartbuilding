import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { LocaleProvider } from 'antd';
import { ConnectedRouter } from 'react-router-redux';
import ruRU from 'antd/lib/locale-provider/ru_RU';

import appConfig from 'appConfig';
import createHistory from 'history/createBrowserHistory';

import { loadCurrentUserInfo } from 'actions/login';

import configureStore from 'store/configureStore';
import { createOnStorage } from 'middlewares/storageMiddleware';

import Layout from './Layout';

const history = createHistory();

const store = configureStore(history);
const onStorage = createOnStorage(store);
window.addEventListener('storage', onStorage);

class App extends Component {
  componentDidMount() {
    if (appConfig.securityOn) {
      store.dispatch(loadCurrentUserInfo());
    }
  }

  render() {
    return (
      <Provider store={store}>
        <LocaleProvider locale={ruRU}>
          <ConnectedRouter history={history}>
            <Route path="/" component={Layout} />
          </ConnectedRouter>
        </LocaleProvider>
      </Provider>
    );
  }
}

export default App;
