import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { Spin, Alert, Modal } from 'antd';

import ScenarioList from 'components/ScenarioList';
import Text from 'components/Text';
import TripleCRUDMenu from 'components/Menu/WidgetMenu';
import Widget from 'components/Widget';

import ScenarioBasicParamsForm, { resetFormValues } from 'containers/Forms/ScenarioBasicParamsForm';

import { modalOpen, modalClose } from 'actions/modals';
import {
  loadScenarioConstants,
  loadScenarios,
  createScenario,
  updateScenarioBasics,
  deleteScenario,
  selectScenario
} from 'actions/scenarios';

import {
  checkCurrentProjectIsActive,
  getCurrentProject,
  getCurrentProjectScenarios
} from 'helpers/currentProject';
import { getSelectedScenario } from 'helpers/scenarios';
import { findDevicesByDeviceCategory } from 'helpers/device';
import { getCurrentUser, hasRole } from 'helpers/user';

/**
 * TODO: ПЕРЕДЕЛАТЬ С НУЛЯ СТРАНИЦУ РЕДАКТОРА СЦЕНАРИЕВ (и все контейнеры относящиеся к ней):
 *       -  Отказаться от state и множественной обработки данных в componentWillReceiveProps(propsToState)
 *          т.к. сейчас происходит много вычислений при обновлении или смене текущего сценария
 *       -  По возможности разбить компоненты на более мелкие
 *       -  Вынести всю логику обработки данных из рендера
 *       -  Использовать reselect для данных в mapStateToProps
 *       -  Использовать компоненты с виртуальным скроллом для таблиц и select-ов
 */

/**
 * Редактор сценариев.
 *
 * Компонент ответственен за изначальное получение сценариев текущего редактируемого проекта, однако рассчитывает, что сам проект, устройства проекта, профили устройств запрашиваются другими компонентами.
 */
class ScenarioEditor extends Component {
  constructor(props) {
    super(props);
    this.noData = (
      <Spin>
        <Text>Нет данных</Text>
      </Spin>
    );
    this.accessDenied = <Alert message="Недостаточно прав" type="error" />;
    this.state = {
      scenariosRequested: false,
      scenarios: null,
      isActive: false,
      selectedScenario: null,
      errorMessage: null,
      height: 500
    };
  }

  /**
   * Получить сценарии текущего проекта или запросить их.
   *
   * @param {object} props - свойства компонента
   * @param {boolean} requestIfAbsent - запрашивать ли сценарии в случае их отсутствия
   * @return {scenariosRequested: boolean, scenarios: array} флаг - отправлен ли запрос, и массив сценариев (может быть null)
   */
  getScenariosOrSendRequest(props, requestIfAbsent) {
    const { currentProject, scenarios, dispatch } = props;

    if (currentProject.id) {
      if (scenarios) {
        return {
          scenariosRequested: true,
          scenarios
        };
      } else if (requestIfAbsent) {
        dispatch(loadScenarios(currentProject.id));
        return { scenariosRequested: true, scenarios: null };
      }
    }
    return { scenariosRequested: false, scenarios: null };
  }

  propsToState(props) {
    const { loadCurrentProjectError, loadScenarioConstantsError, loadScenariosError } = props;

    if (loadCurrentProjectError) {
      this.setState({
        isActive: false,
        errorMessage: `Ошибка загрузки проекта: ${loadCurrentProjectError}`
      });
    } else if (loadScenarioConstantsError) {
      this.setState({
        isActive: false,
        errorMessage: `Ошибка загрузки справочных данных: ${loadScenarioConstantsError}`
      });
    } else if (loadScenariosError) {
      this.setState({
        isActive: false,
        errorMessage: `Ошибка загрузки сценариев проекта: ${loadScenariosError}`
      });
    } else {
      const { selectedScenario } = props;
      const { scenariosRequested } = this.state;

      this.setState({
        ...this.getScenariosOrSendRequest(props, !scenariosRequested),
        isActive: true,
        selectedScenario,
        errorMessage: null
      });
    }
  }

  componentDidMount() {
    const { scenarioConstants, dispatch } = this.props;

    if (isEmpty(scenarioConstants)) {
      dispatch(loadScenarioConstants());
    }

    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onScenarioAdd = () => {
    const { dispatch } = this.props;
    const { scenarios } = this.state;
    resetFormValues(dispatch, {
      name: `Сценарий ${scenarios.length + 1}`,
      scenarioPurpose: 'EXEC_BY_LOGIC',
      enabled: true,
      scenarioType: 'UNASSIGNED'
    });
    dispatch(modalOpen('addScenario'));
  };

  onScenarioAddSubmit = values => {
    const scenarioBasicParams = values;
    const { currentProject, dispatch } = this.props;
    dispatch(createScenario(currentProject.id, scenarioBasicParams));
    dispatch(modalClose('addScenario'));
  };

  onScenarioEdit = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    resetFormValues(dispatch, selectedScenario.basicParams);
    dispatch(modalOpen('editScenario'));
  };

  onScenarioEditSubmit = values => {
    const scenarioBasicParams = values;
    const { currentProject, dispatch } = this.props;
    const { selectedScenario } = this.state;
    dispatch(updateScenarioBasics(currentProject.id, selectedScenario.id, scenarioBasicParams));
    dispatch(modalClose('editScenario'));
  };

  onScenarioDelete = () => {
    const { currentProject, dispatch } = this.props;
    const { selectedScenario } = this.state;
    Modal.confirm({
      title: 'Удаление сценария',
      content: (
        <div>
          Удалить сценарий{' '}
          <b>
            {selectedScenario.globalNo}. {selectedScenario.name}
          </b>
          ?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteScenario(currentProject.id, selectedScenario.id));
      }
    });
  };

  onScenarioSelect(record) {
    const { dispatch } = this.props;
    const scenario = record;
    dispatch(selectScenario(scenario.id));
  }

  render() {
    const { scenarioConstants, controlDevices, isActiveProject, сurrentUser } = this.props;
    const { scenarios, isActive, selectedScenario, errorMessage, height } = this.state;

    if (!hasRole(сurrentUser, 'SCENARIO_READ')) return this.accessDenied;

    let data = this.noData;

    if (scenarios) {
      data = (
        <ScenarioList
          scenarios={scenarios}
          scroll={{ y: height - 56 }}
          currentScenarioId={selectedScenario ? selectedScenario.id : null}
          onRowClick={record => this.onScenarioSelect(record)}
        />
      );
    }

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ScenarioBasicParamsForm
          modalName="addScenario"
          isEdit={false}
          onSubmit={this.onScenarioAddSubmit}
          scenarioPurposes={
            scenarioConstants && scenarioConstants.dictionaries
              ? scenarioConstants.dictionaries.purposes
              : []
          }
          scenarioTypes={
            scenarioConstants && scenarioConstants.dictionaries
              ? scenarioConstants.dictionaries.types
              : []
          }
          controlDevices={controlDevices}
        />
        <ScenarioBasicParamsForm
          modalName="editScenario"
          isEdit={true}
          onSubmit={this.onScenarioEditSubmit}
          scenarioPurposes={
            scenarioConstants && scenarioConstants.dictionaries
              ? scenarioConstants.dictionaries.purposes
              : []
          }
          scenarioTypes={
            scenarioConstants && scenarioConstants.dictionaries
              ? scenarioConstants.dictionaries.types
              : []
          }
          controlDevices={controlDevices}
        />
        <TripleCRUDMenu
          isCreateButtonActive={
            isActive && !isActiveProject && hasRole(сurrentUser, 'SCENARIO_CREATE')
          }
          onCreateButtonClick={this.onScenarioAdd}
          isEditButtonActive={
            isActive &&
            selectedScenario &&
            !isActiveProject &&
            hasRole(сurrentUser, 'SCENARIO_UPDATE')
              ? true
              : false
          }
          onEditButtonClick={this.onScenarioEdit}
          isDeleteButtonActive={
            isActive &&
            selectedScenario &&
            !isActiveProject &&
            hasRole(сurrentUser, 'SCENARIO_REMOVE')
              ? true
              : false
          }
          onDeleteButtonClick={this.onScenarioDelete}
          errorMessage={errorMessage}
        />
        {data}
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    scenarioConstants: state.scenarioConstants,
    controlDevices: findDevicesByDeviceCategory(state, 'CONTROL').filter(
      device => !device.disabled
    ),
    scenarios: getCurrentProjectScenarios(state),
    selectedScenario: getSelectedScenario(state),
    currentProject: getCurrentProject(state),
    сurrentUser: getCurrentUser(state),

    loadCurrentProjectError: state.errors.loadCurrentProject,
    loadScenarioConstantsError: state.errors.loadScenarioConstants,
    loadScenariosError: state.errors.loadScenarios
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioEditor);
