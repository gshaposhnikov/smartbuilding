import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';
import { reduxForm, initialize, reset } from 'redux-form';

import {
  updateScenarioAdvanced,
  updateScenarioStartLogic,
  updateScenarioStopLogic,
  selectScenarioSubLogic
} from 'actions/scenarios';
import { modalOpen, modalClose } from 'actions/modals';

import ScenarioAdvancedEditorMenu from 'containers/Menus/ScenarioAdvancedEditorMenu';
import ScenarioLogicForm, {
  resetFormValues as resetLogicFormValues
} from 'containers/Forms/ScenarioLogicForm';
import ScenarioParamsForm from 'containers/Forms/ScenarioParamsForm';

import { checkCurrentProjectIsActive, getCurrentProject } from 'helpers/currentProject';
import { getSelectedScenario } from 'helpers/scenarios';
import { getCurrentUser, hasRole } from 'helpers/user';

const FORM_NAME = 'scenarioAdvancedParams';
const DEFAULT_LOGIC_TYPE = 'OR';
const DEFAULT_ENTITY_TYPE = 'REGION';
const START_LOGIC_PURPOSES = {
  EXEC_BY_LOGIC: true,
  EXEC_BY_TACTICS: true
};
const STOP_LOGIC_PURPOSES = {
  EXEC_BY_LOGIC: true
};

class ScenarioAdvancedParamsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seletedScenario: null,
      flagNames: this.getFlagNames(props)
    };
  }

  getFlagNames(props) {
    const { scenarioConstants } = props;
    if (scenarioConstants && scenarioConstants.dictionaries) {
      try {
        const manualStartStopAllowedName = scenarioConstants.dictionaries.flags.find(
          flag => flag.id === 'manualStartStopAllowed'
        ).name;
        const stopOnGlobalResetName = scenarioConstants.dictionaries.flags.find(
          flag => flag.id === 'stopOnGlobalReset'
        ).name;
        return { manualStartStopAllowedName, stopOnGlobalResetName };
      } catch (error) {
        return {
          manualStartStopAllowedName: 'Управление в ОЗ',
          stopOnGlobalResetName: 'Выключение при сбросе'
        };
      }
    } else {
      return {
        manualStartStopAllowedName: 'Управление в ОЗ',
        stopOnGlobalResetName: 'Выключение при сбросе'
      };
    }
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      flagNames: this.getFlagNames(this.props)
    });
  }

  componentWillReceiveProps(newProps) {
    const { dispatch, selectedScenario: newSelectedScenario } = newProps;
    const { selectedScenario } = this.state;
    if (!isEqual(selectedScenario, newSelectedScenario)) {
      if (newSelectedScenario) {
        dispatch(initialize(FORM_NAME, newSelectedScenario.advancedParams));
      } else {
        dispatch(initialize(FORM_NAME, {}));
      }
      dispatch(reset(FORM_NAME));
      this.setState({
        ...this.state,
        selectedScenario: newSelectedScenario
      });
    }
  }

  onSaveClick = () => {
    const { dispatch, currentProject, selectedScenario, currentValues } = this.props;
    dispatch(updateScenarioAdvanced(currentProject.id, selectedScenario.id, currentValues));
  };

  getAddedEntityTypeIntoLogic(logic) {
    const { scenarioConstants } = this.props;
    const startLogicTriggerType = scenarioConstants.triggerTypes.find(
      triggerType => triggerType.id === logic.triggerTypeId
    );
    const subLogics = [];
    if (logic.subLogics) {
      logic.subLogics.forEach(subLogic => {
        subLogics.push(this.getAddedEntityTypeIntoLogic(subLogic));
      });
    }
    return {
      ...logic,
      logicType: logic.logicType || DEFAULT_LOGIC_TYPE,
      entityType: startLogicTriggerType ? startLogicTriggerType.entityType : DEFAULT_ENTITY_TYPE,
      subLogics,
    };
  }

  onEditStartLogicClick = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    if (selectedScenario.startLogic) {
      resetLogicFormValues(dispatch, this.getAddedEntityTypeIntoLogic(selectedScenario.startLogic));
    } else {
      resetLogicFormValues(dispatch, {logicType: DEFAULT_LOGIC_TYPE, entityType: DEFAULT_ENTITY_TYPE});
    }
    dispatch(selectScenarioSubLogic('root'));
    dispatch(modalOpen('editScenarioStartLogic'));
  };

  onEditStartLogicSubmit = values => {
    const { dispatch, currentProject, selectedScenario } = this.props;
    dispatch(updateScenarioStartLogic(currentProject.id, selectedScenario.id, values));
    dispatch(modalClose('editScenarioStartLogic'));
  };

  onEditStopLogicClick = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    if (selectedScenario.stopLogic) {
      resetLogicFormValues(dispatch, this.getAddedEntityTypeIntoLogic(selectedScenario.stopLogic));
    } else {
      resetLogicFormValues(dispatch, {logicType: DEFAULT_LOGIC_TYPE, entityType: DEFAULT_ENTITY_TYPE});
    }
    dispatch(selectScenarioSubLogic('root'));
    dispatch(modalOpen('editScenarioStopLogic'));
  };

  onEditStopLogicSubmit = values => {
    const { dispatch, currentProject, selectedScenario } = this.props;
    dispatch(updateScenarioStopLogic(currentProject.id, selectedScenario.id, values));
    dispatch(modalClose('editScenarioStopLogic'));
  };

  render() {
    const { selectedScenario, flagNames } = this.state;
    const { valid, dirty, isActiveProject, currentValues, сurrentUser } = this.props;

    return (
      <div>
        <ScenarioAdvancedEditorMenu
          title={
            selectedScenario
              ? `${selectedScenario.name} (№ ${selectedScenario.globalNo})`
              : 'Не выбран сценарий'
          }
          isSaveActive={Boolean(
            selectedScenario &&
              valid &&
              dirty &&
              !isActiveProject &&
              hasRole(сurrentUser, 'SCENARIO_UPDATE')
          )}
          onSaveClick={this.onSaveClick}
          isEditStartLogicActive={Boolean(
            selectedScenario &&
              START_LOGIC_PURPOSES[selectedScenario.scenarioPurpose] &&
              !dirty &&
              !isActiveProject &&
              hasRole(сurrentUser, 'SCENARIO_UPDATE')
          )}
          onEditStartLogicClick={this.onEditStartLogicClick}
          isEditStopLogicActive={Boolean(
            selectedScenario &&
              STOP_LOGIC_PURPOSES[selectedScenario.scenarioPurpose] &&
              selectedScenario.advancedParams.stopType === 'BY_LOGIC' &&
              !dirty &&
              !isActiveProject &&
              hasRole(сurrentUser, 'SCENARIO_UPDATE')
          )}
          onEditStopLogicClick={this.onEditStopLogicClick}
        />
        <ScenarioLogicForm
          modalName="editScenarioStartLogic"
          onSubmit={this.onEditStartLogicSubmit}
          scenarioPurpose={selectedScenario ? selectedScenario.scenarioPurpose : null}
        />
        <ScenarioLogicForm
          modalName="editScenarioStopLogic"
          onSubmit={this.onEditStopLogicSubmit}
          scenarioPurpose={selectedScenario ? selectedScenario.scenarioPurpose : null}
        />
        <ScenarioParamsForm
          selectedScenario={selectedScenario}
          flagNames={flagNames}
          currentValues={currentValues}
        />
      </div>
    );
  }
}

const validate = values => {
  if (!values.stopType) {
    return { stopType: 'Не выбран тип остановки сценария' };
  } else {
    return {};
  }
};

ScenarioAdvancedParamsForm = reduxForm({
  form: FORM_NAME,
  validate
})(ScenarioAdvancedParamsForm);

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    сurrentUser: getCurrentUser(state),
    /* Справочники */
    scenarioConstants: state.scenarioConstants,

    /* Выделенные сущности */
    currentProject: getCurrentProject(state),
    selectedScenario: getSelectedScenario(state),

    /* Текущие значения полей */
    currentValues: state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(ScenarioAdvancedParamsForm);
