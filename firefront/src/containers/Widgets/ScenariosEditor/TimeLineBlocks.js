import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal } from 'antd';
/**
 * Компоненты
 */
import ScenarioTimeLineBlocks from 'components/ScenarioTimeLineBlocks';
import Widget from 'components/Widget';
/**
 * Контейнеры
 */
import ScenarioTimeLineMenu from 'containers/Menus/ScenarioTimeLineMenu';
import ScenarioExecBlockForm, {
  resetFormValues as resetExecFormValues
} from 'containers/Forms/ScenarioExecBlockForm';
import ScenarioActionBlockForm, {
  resetFormValues as resetActionFormValues
} from 'containers/Forms/ScenarioActionBlockForm';
import ScenarioTraceBlockForm, {
  resetFormValues as resetTraceFormValues
} from 'containers/Forms/ScenarioTraceBlockForm';
/**
 * Экшены
 */
import { modalOpen, modalClose } from 'actions/modals';
import {
  addScenarioTimeLineBlock,
  updateScenarioTimeLineBlock,
  removeScenarioTimeLineBlock,
  selectScenarioTimeLineBlock
} from 'actions/scenarios';
/**
 * Хелперы
 */
import { checkCurrentProjectIsActive, getCurrentProject } from 'helpers/currentProject';
import { getSelectedScenario } from 'helpers/scenarios';
import { getCurrentUser, hasRole } from 'helpers/user';
/**
 * Константы
 */
const MODAL_NAMES = {
  ADD: {
    EXECUTIVE: 'addExecBlock',
    TRACING: 'addTraceBlock',
    COMPUTER_ACTION: 'addActionBlock'
  },
  EDIT: {
    EXECUTIVE: 'editExecBlock',
    TRACING: 'editTraceBlock',
    COMPUTER_ACTION: 'editActionBlock'
  }
};
const EXECUTIVE = 'EXECUTIVE';
const TRACING = 'TRACING';
const COMPUTER_ACTION = 'COMPUTER_ACTION';
/**
 * Редактор блоков времени сценариев.
 */
class TimeLineBlocksEditor extends Component {
  static propTypes = {
    scenarioConstants: PropTypes.object,
    currentProject: PropTypes.object,
    isActiveProject: PropTypes.bool,
    selectedScenario: PropTypes.object
  };

  state = {
    seletedScenario: null,
    selectedTimeLineBlock: null,
    height: 500,
    expandedRowKeys: []
  };

  propsToState(props) {
    const { currentScenarioTLBlockNo, selectedScenario } = props;

    if (
      (!this.props.selectedScenario && selectedScenario) ||
      (this.props.selectedScenario &&
        selectedScenario &&
        this.props.selectedScenario.id !== selectedScenario.id)
    ) {
      if (selectedScenario) {
        const expandedRowKeys = [];
        for (let i = 0; i < selectedScenario.timeLineBlocksTree.length - 1; i++) {
          expandedRowKeys.push('.' + i);
          if (selectedScenario.timeLineBlocksTree[i].children) {
            for (let j = 0; j < selectedScenario.timeLineBlocksTree[i].children.length - 1; j++) {
              expandedRowKeys.push('.' + i + '.' + j);
            }
          }
        }
        this.setState({ expandedRowKeys });
      }
    }
    const selectedTimeLineBlock =
      selectedScenario && currentScenarioTLBlockNo >= 0
        ? selectedScenario.timeLineBlocks[currentScenarioTLBlockNo]
        : null;
    this.setState({
      selectedScenario,
      selectedTimeLineBlock
    });
  }

  componentDidMount() {
    this.propsToState(this.props);
  }
  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  newTimeLineBlockNameGenerator = (blocks = [], name = 'Новый блок#1', indexCopy = 0) => {
    const newName = name.split('#').join(' ');
    const blockWithIdentityName = blocks.find(block => block.name === newName);
    if (blockWithIdentityName) {
      return this.newTimeLineBlockNameGenerator(
        blocks,
        `${name.split('#')[0]}#${indexCopy}`,
        indexCopy + 1
      );
    } else {
      return newName;
    }
  };

  getLatestTimeSec() {
    const { selectedScenario } = this.state;
    let latestTime = 0;
    if (selectedScenario) {
      selectedScenario.timeLineBlocks.forEach(block => {
        switch (block.blockType) {
          case COMPUTER_ACTION:
          case EXECUTIVE: {
            if (latestTime < block.timeDelaySec) {
              latestTime = block.timeDelaySec;
            }
            break;
          }
          case TRACING: {
            if (latestTime < block.tracingStartSec + block.tracingPeriodSec) {
              latestTime = block.tracingStartSec + block.tracingPeriodSec;
            }
            break;
          }
          default: {
            break;
          }
        }
      });
    }
    return latestTime;
  }
  /**
   * Добавление исполнительного блока
   */
  onExecBlockAdd = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    const execBlocks = selectedScenario.timeLineBlocks.filter(
      block => block.blockType === EXECUTIVE
    );
    const newExecBlockName = this.newTimeLineBlockNameGenerator(
      execBlocks,
      `Исполнение#${execBlocks.length + 1}`,
      execBlocks.length + 1
    );
    const initialValues = {
      blockType: EXECUTIVE,
      name: newExecBlockName,
      timeDelaySec: this.getLatestTimeSec(),
      conditionType: 'NONE',
      conditionCheckType: 'IS_ACTIVE',
      actions: []
    };
    resetExecFormValues(dispatch, initialValues);
    dispatch(modalOpen(MODAL_NAMES.ADD.EXECUTIVE));
  };
  onExecBlockAddSubmit = values => {
    const { currentProject, selectedScenario, dispatch } = this.props;
    dispatch(addScenarioTimeLineBlock(currentProject.id, selectedScenario.id, values));
    dispatch(modalClose(MODAL_NAMES.ADD.EXECUTIVE));
  };
  /**
   * Добавление блока слежения
   */
  onTraceBlockAdd = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    const traceBlocks = selectedScenario.timeLineBlocks.filter(
      block => block.blockType === TRACING
    );
    const newTraceBlockName = this.newTimeLineBlockNameGenerator(
      traceBlocks,
      `Слежение#${traceBlocks.length + 1}`,
      traceBlocks.length + 1
    );
    resetTraceFormValues(dispatch, {
      blockType: TRACING,
      name: newTraceBlockName,
      tracingStartSec: this.getLatestTimeSec(),
      tracingEntityType: 'SENSOR_DEVICE',
      tracingPeriodSec: 10
    });
    dispatch(modalOpen(MODAL_NAMES.ADD.TRACING));
  };
  onTraceBlockAddSubmit = values => {
    const { currentProject, selectedScenario, dispatch } = this.props;
    dispatch(addScenarioTimeLineBlock(currentProject.id, selectedScenario.id, values));
    dispatch(modalClose(MODAL_NAMES.ADD.TRACING));
  };
  /**
   * Добавление блока действия на компьютере
   */
  onActionBlockAdd = () => {
    const { dispatch } = this.props;
    const { selectedScenario } = this.state;
    const actionBlocks = selectedScenario.timeLineBlocks.filter(
      block => block.blockType === COMPUTER_ACTION
    );
    const newActionBlockName = this.newTimeLineBlockNameGenerator(
      actionBlocks,
      `Действие#${actionBlocks.length + 1}`,
      actionBlocks.length + 1
    );
    resetActionFormValues(dispatch, {
      blockType: COMPUTER_ACTION,
      name: newActionBlockName,
      timeDelaySec: this.getLatestTimeSec(),
      computerActions: []
    });
    dispatch(modalOpen(MODAL_NAMES.ADD.COMPUTER_ACTION));
  };
  onActionBlockAddSubmit = values => {
    const { currentProject, selectedScenario, dispatch } = this.props;
    dispatch(addScenarioTimeLineBlock(currentProject.id, selectedScenario.id, values));
    dispatch(modalClose(MODAL_NAMES.ADD.COMPUTER_ACTION));
  };
  /**
   * Обновление блока сценария
   */
  onBlockEdit = () => {
    const { dispatch, selectedScenario, scenarioConstants } = this.props;
    const { selectedTimeLineBlock } = this.state;
    const initialValues = {
      ...selectedTimeLineBlock,
      purpose: selectedScenario.scenarioPurpose,
      forcedActionTypes:
        selectedScenario.startLogic && selectedScenario.startLogic.triggerTypeId // Специальные действия для тактического сценария
          ? scenarioConstants.actionTypes.filter(
              actionType =>
                actionType.id ===
                scenarioConstants.triggerTypes.find(
                  triggerType => triggerType.id === selectedScenario.startLogic.triggerTypeId
                ).forcedScenarioActionTypeId
            )
          : null
    };
    switch (selectedTimeLineBlock.blockType) {
      case EXECUTIVE: {
        const { scenarioConstants } = this.props;
        initialValues.actions = selectedTimeLineBlock.actions.map(action => ({
          ...action,
          entityType: scenarioConstants.actionTypes.find(
            actionType => actionType.id === action.actionTypeId
          ).entityType
        }));
        resetExecFormValues(dispatch, initialValues);
        break;
      }
      case TRACING: {
        resetTraceFormValues(dispatch, initialValues);
        break;
      }
      case COMPUTER_ACTION: {
        resetActionFormValues(dispatch, initialValues);
        break;
      }
      default:
        break;
    }
    dispatch(modalOpen(MODAL_NAMES.EDIT[selectedTimeLineBlock.blockType]));
  };
  onBlockEditSubmit = values => {
    const { currentProject, selectedScenario, currentScenarioTLBlockNo, dispatch } = this.props;
    dispatch(
      updateScenarioTimeLineBlock(
        currentProject.id,
        selectedScenario.id,
        currentScenarioTLBlockNo,
        values
      )
    );
    dispatch(modalClose(MODAL_NAMES.EDIT[values.blockType]));
  };
  /**
   * Удаление блока сценария
   */
  onBlockRemove = () => {
    const { currentProject, selectedScenario, currentScenarioTLBlockNo, dispatch } = this.props;
    const { selectedTimeLineBlock } = this.state;
    if (
      (selectedTimeLineBlock.actions && selectedTimeLineBlock.actions.length) ||
      (selectedTimeLineBlock.computerActions && selectedTimeLineBlock.computerActions.length) ||
      selectedTimeLineBlock.blockType === 'TRACING'
    ) {
      let blockTypeName = 'временной блок';
      switch (selectedTimeLineBlock.blockType) {
        case 'EXECUTIVE': {
          blockTypeName = 'исполнительный блок';
          break;
        }
        case 'TRACING': {
          blockTypeName = 'блок слежения';
          break;
        }
        case 'COMPUTER_ACTION': {
          blockTypeName = 'блок действий на компьютере';
          break;
        }
        default:
          break;
      }
      Modal.confirm({
        title: 'Удаление блока сценария',
        content: (
          <div>
            Удалить {blockTypeName} <b>{selectedTimeLineBlock.name}</b> из сценария{' '}
            <b>
              {selectedScenario.globalNo}. {selectedScenario.name}
            </b>
            ?
          </div>
        ),
        okText: 'Удалить',
        onCancel: () => {},
        cancelText: 'Отмена',
        maskClosable: false,
        onOk: () => {
          dispatch(
            removeScenarioTimeLineBlock(
              currentProject.id,
              selectedScenario.id,
              currentScenarioTLBlockNo
            )
          );
        }
      });
    } else {
      dispatch(
        removeScenarioTimeLineBlock(
          currentProject.id,
          selectedScenario.id,
          currentScenarioTLBlockNo
        )
      );
    }
  };
  /**
   * Выбор блока сценария
   */
  onBlockSelect = record => {
    if (record.itemType !== 'TIME' && record.itemType !== 'ROOT') {
      const { dispatch, currentScenarioTLBlockNo } = this.props;
      dispatch(
        selectScenarioTimeLineBlock(record.number === currentScenarioTLBlockNo ? -1 : record.number)
      );
    }
  };

  getRowClassName = record => {
    const { currentScenarioTLBlockNo } = this.props;
    let classNames = '';
    if (!record.children || !record.children.length) classNames += 'hidden-expand ';
    switch (record.itemType) {
      case 'OUT': {
        classNames += 'row-hidden ';
        break;
      }
      case 'TIME': {
        classNames += 'row-highlighted ';
        break;
      }
      case 'COMPUTER_ACTION':
      case 'TRACE':
      case 'NO_TRACE':
      case 'CONDITION':
      case 'IN': {
        if (currentScenarioTLBlockNo === record.number) classNames += 'selected-row ';
        break;
      }

      default:
        break;
    }
    return classNames;
  };

  onRowExpand = (expand, record) => {
    let { expandedRowKeys } = this.state;
    if (expand) expandedRowKeys.push(record.key);
    else expandedRowKeys = expandedRowKeys.filter(key => key !== record.key);
    this.setState({
      expandedRowKeys
    });
  };

  render() {
    const { isActiveProject, currentUser } = this.props;
    const { selectedScenario, selectedTimeLineBlock, height, expandedRowKeys } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ScenarioActionBlockForm
          modalName={MODAL_NAMES.ADD.COMPUTER_ACTION}
          onSubmit={this.onActionBlockAddSubmit}
          isEdit={false}
        />
        <ScenarioActionBlockForm
          modalName={MODAL_NAMES.EDIT.COMPUTER_ACTION}
          onSubmit={this.onBlockEditSubmit}
          isEdit={true}
        />
        <ScenarioExecBlockForm
          modalName={MODAL_NAMES.ADD.EXECUTIVE}
          onSubmit={this.onExecBlockAddSubmit}
          isEdit={false}
        />
        <ScenarioExecBlockForm
          modalName={MODAL_NAMES.EDIT.EXECUTIVE}
          onSubmit={this.onBlockEditSubmit}
          isEdit={true}
        />
        <ScenarioTraceBlockForm
          modalName={MODAL_NAMES.ADD.TRACING}
          onSubmit={this.onTraceBlockAddSubmit}
          isEdit={false}
        />
        <ScenarioTraceBlockForm
          modalName={MODAL_NAMES.EDIT.TRACING}
          onSubmit={this.onBlockEditSubmit}
          isEdit={true}
        />
        <ScenarioTimeLineMenu
          title={
            selectedScenario
              ? `${selectedScenario.name} (№ ${selectedScenario.globalNo})`
              : 'Не выбран сценарий'
          }
          isAddActionButtonActive={
            selectedScenario &&
            hasRole(currentUser, 'SCENARIO_UPDATE') &&
            selectedScenario.scenarioPurpose !== 'EXEC_BY_TACTICS' &&
            !selectedTimeLineBlock &&
            !isActiveProject
              ? true
              : false
          }
          isAddExecButtonActive={
            selectedScenario &&
            hasRole(currentUser, 'SCENARIO_UPDATE') &&
            selectedScenario.scenarioPurpose !== 'EXEC_BY_TACTICS' &&
            !selectedTimeLineBlock &&
            !isActiveProject
              ? true
              : false
          }
          isAddTraceButtonActive={
            selectedScenario &&
            hasRole(currentUser, 'SCENARIO_UPDATE') &&
            selectedScenario.scenarioPurpose !== 'EXEC_BY_TACTICS' &&
            !selectedTimeLineBlock &&
            !isActiveProject
              ? true
              : false
          }
          onAddActionBlockButtonClick={this.onActionBlockAdd}
          onAddExecBlockButtonClick={this.onExecBlockAdd}
          onAddTraceBlockButtonClick={this.onTraceBlockAdd}
          isEditButtonActive={
            selectedScenario &&
            hasRole(currentUser, 'SCENARIO_UPDATE') &&
            selectedTimeLineBlock &&
            !isActiveProject
              ? true
              : false
          }
          onEditButtonClick={this.onBlockEdit}
          isDeleteButtonActive={
            selectedScenario &&
            hasRole(currentUser, 'SCENARIO_UPDATE') &&
            selectedScenario.scenarioPurpose !== 'EXEC_BY_TACTICS' &&
            selectedTimeLineBlock &&
            !isActiveProject
              ? true
              : false
          }
          onDeleteButtonClick={this.onBlockRemove}
        />
        <ScenarioTimeLineBlocks
          dataSource={
            selectedScenario && selectedScenario.timeLineBlocksTree
              ? selectedScenario.timeLineBlocksTree
              : []
          }
          rowClassName={this.getRowClassName}
          scroll={{ y: height - 62 }}
          onRowClick={this.onBlockSelect}
          expandedRowKeys={expandedRowKeys}
          onExpand={this.onRowExpand}
        />
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    scenarioConstants: state.scenarioConstants,
    selectedScenario: getSelectedScenario(state),
    currentProject: getCurrentProject(state),
    currentScenarioTLBlockNo: state.widgets.currentScenarioTLBlockNo,
    currentUser: getCurrentUser(state)
  };
};

export default connect(mapStateToProps)(TimeLineBlocksEditor);
