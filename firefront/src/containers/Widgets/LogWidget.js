import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import { Table, Column, AutoSizer } from 'react-virtualized';
import styled from 'styled-components';
import moment from 'moment';

import { loadLogEvents } from 'actions/events';

import Text from 'components/Text';
import {
  ColorRow,
  renderAddressDeviceCell,
  renderRegionCell,
  renderScenarioCell,
  renderEmployeeCell,
  renderDescriptionCell
} from 'components/EventTable';

import { getDateTimestamp } from 'helpers/time';
import { getCurrentUser } from 'helpers/user';
import { includesInUserFilterTags } from 'helpers/filtration';

import { SKUD_LOG } from 'constants/skud';

const LogWrapper = styled.div`
  flex: 1;
  .ReactVirtualized__Table__headerRow {
    background-color: lightgray;
    font-weight: normal;
    font-size: 11px !important;
  }
  .ReactVirtualized__Table__headerColumn {
    display: flex;
    justify-content: center;
    margin: 1px !important;
  }
  .ReactVirtualized__Table__Grid {
    background-color: white;
  }
  .ReactVirtualized__Table__rowColumn {
    margin: 1px !important;
  }
  .ReactVirtualized__Table__row {
    margin: 0 !important;
  }
`;

export const LOG_VIEW_COLUMNS = {
  INDEX: Symbol('INDEX'),
  OCCURRED: Symbol('OCCURRED'),
  RECEIVED: Symbol('RECEIVED'),
  DESCRIPTION: Symbol('DESCRIPTION'),
  OBJECT: Symbol('OBJECT'),
  CONTROL_DEVICE: Symbol('CONTROL_DEVICE'),
  REGION: Symbol('REGION'),
  USER: Symbol('USER'),
  EMPLOYEE: Symbol('EMPLOYEE')
};

/*
 * TODO: несколько окон журналов (сколько настроено в проекте),
 * а также получение настроек каждого журнала с сервера.
 */
class LogWidget extends Component {
  static propTypes = {
    /* Из redux-store */

    /* События */
    events: PropTypes.array,
    /* Ошибка получения событий журнала от сервера */
    loadLogEventsError: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      height: 500,
      scroll: 0
    };
  }

  columnRenderers = {
    [LOG_VIEW_COLUMNS.INDEX]: width => (
      <Column
        label="#"
        width={45}
        dataKey="occurred"
        key="index"
        cellRenderer={({ rowIndex, rowData }) => {
          return <ColorRow stateCategoryId={rowData.stateCategoryId}>{rowIndex + 1}</ColorRow>;
        }}
      />
    ),

    [LOG_VIEW_COLUMNS.OCCURRED]: width => (
      <Column
        label="Время прибора"
        width={width / 9}
        dataKey="occurred"
        key="occurred"
        cellRenderer={({ rowData }) => (
          <ColorRow
            stateCategoryId={rowData.stateCategoryId}
            title={rowData.occurred ? getDateTimestamp(rowData.occurred) : ''}
          >
            {rowData.occurred ? getDateTimestamp(rowData.occurred) : ''}
          </ColorRow>
        )}
      />
    ),

    [LOG_VIEW_COLUMNS.RECEIVED]: width => (
      <Column
        label="Системное время"
        width={width / 9}
        dataKey="received"
        key="received"
        cellRenderer={({ rowData }) => (
          <ColorRow
            stateCategoryId={rowData.stateCategoryId}
            title={rowData.received ? getDateTimestamp(rowData.received) : ''}
          >
            {rowData.received ? getDateTimestamp(rowData.received) : ''}
          </ColorRow>
        )}
      />
    ),

    [LOG_VIEW_COLUMNS.DESCRIPTION]: width => (
      <Column
        label="Описание"
        width={width / 3.9}
        dataKey="name"
        key="name"
        cellRenderer={({ rowData }) => renderDescriptionCell('', rowData)}
      />
    ),

    [LOG_VIEW_COLUMNS.OBJECT]: width => (
      <Column // Объединяет колонки: Адресное устройство, Виртуальное состояние, Сценарий
        label="Объект"
        dataKey="deviceEventInfo"
        key="deviceEventInfo"
        width={width / 10}
        cellRenderer={({ rowData }) => {
          if (rowData.deviceEventInfo) {
            return renderAddressDeviceCell('', rowData);
          } else if (rowData.virtualStateEventInfo) {
            return (
              <ColorRow
                stateCategoryId={rowData.stateCategoryId}
                title={rowData.virtualStateEventInfo.name}
              >
                {rowData.virtualStateEventInfo.name}
              </ColorRow>
            );
          } else if (rowData.scenarioEventInfo) {
            return renderScenarioCell('', rowData);
          } else {
            return <ColorRow stateCategoryId={rowData.stateCategoryId}>{null}</ColorRow>;
          }
        }}
      />
    ),

    [LOG_VIEW_COLUMNS.CONTROL_DEVICE]: width => (
      <Column
        label="Прибор"
        dataKey="controlDeviceEventInfo"
        width={width / 7}
        key="controlDeviceEventInfo"
        cellRenderer={({ rowData }) => (
          <ColorRow
            stateCategoryId={rowData.stateCategoryId}
            title={
              rowData.controlDeviceEventInfo
                ? `${rowData.controlDeviceEventInfo.name}, ${
                    rowData.controlDeviceEventInfo.address
                  }`
                : null
            }
          >
            {rowData.controlDeviceEventInfo
              ? `${rowData.controlDeviceEventInfo.name}, ${rowData.controlDeviceEventInfo.address}`
              : null}
          </ColorRow>
        )}
      />
    ),

    [LOG_VIEW_COLUMNS.REGION]: width => (
      <Column
        label="Зона"
        width={width / 10}
        dataKey="name"
        key="regionEventInfo"
        cellRenderer={({ rowData }) => renderRegionCell('', rowData)}
      />
    ),

    [LOG_VIEW_COLUMNS.USER]: width => (
      <Column
        label="Пользователь"
        dataKey="userEventInfo"
        key="userEventInfo"
        width={width / 7}
        cellRenderer={({ rowData }) => (
          <ColorRow
            stateCategoryId={rowData.stateCategoryId}
            title={rowData.userEventInfo ? `${rowData.userEventInfo.name}` : null}
          >
            {rowData.userEventInfo ? `${rowData.userEventInfo.name}` : null}
          </ColorRow>
        )}
      />
    ),

    [LOG_VIEW_COLUMNS.EMPLOYEE]: width => (
      <Column
        label="Сотрудник"
        width={width / 7}
        dataKey="employee"
        key="employeeEventInfo"
        cellRenderer={({ rowData }) => renderEmployeeCell(rowData)}
      />
    )
  };

  componentDidMount() {
    const { events, loadLogEventsError } = this.props;
    if (!events && !loadLogEventsError) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { dispatch, logViewId, logView } = this.props;
    const filters = {};
    if (logView) {
      const { subsystems, stateCategoryIds, nLastDays } = logView;
      if (subsystems && subsystems.length) {
        filters['subsystems'] = subsystems;
      }
      if (stateCategoryIds && stateCategoryIds.length) {
        filters['stateCategoryIds'] = stateCategoryIds;
      }
      if (nLastDays) {
        filters['receivedAfter'] = encodeURIComponent(
          moment()
            .subtract(nLastDays, 'd')
            .format('YYYY-MM-DDTHH:mm:ss.SSSZZ')
        );
      }
    }
    dispatch(loadLogEvents(logViewId, logView.recordCount, null, null, filters));
  };

  shouldComponentUpdate(nextProps) {
    if (nextProps.events && nextProps.events.length) {
      if (!this.props.events || !this.props.events.length) return true;
      if (this.props.events) {
        if (nextProps.events.length !== this.props.events.length) return true;
        if (nextProps.events[0].id !== this.props.events[0].id) return true;
      }
    }
    return false;
  }

  defaultColumns = [
    LOG_VIEW_COLUMNS.INDEX,
    LOG_VIEW_COLUMNS.OCCURRED,
    LOG_VIEW_COLUMNS.RECEIVED,
    LOG_VIEW_COLUMNS.DESCRIPTION,
    LOG_VIEW_COLUMNS.OBJECT,
    LOG_VIEW_COLUMNS.CONTROL_DEVICE,
    LOG_VIEW_COLUMNS.REGION,
    LOG_VIEW_COLUMNS.USER
  ];

  render() {
    const { events, columns = this.defaultColumns } = this.props;
    const { scroll } = this.state;
    if (events && events.length) {
      return (
        <LogWrapper>
          <AutoSizer>
            {({ height, width }) => {
              return (
                <Table
                  width={width}
                  height={height || 300}
                  headerHeight={16}
                  rowCount={events.length}
                  rowHeight={26}
                  rowGetter={({ index }) => events[index]}
                  onScroll={({ scrollTop }) => this.setState({ scroll: scrollTop })}
                  scrollTop={scroll}
                >
                  {columns.map(column => this.columnRenderers[column](width))}
                </Table>
              );
            }}
          </AutoSizer>
        </LogWrapper>
      );
    } else
      return (
        <Spin>
          <Text>Нет данных</Text>
        </Spin>
      );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { logViewId } = ownProps;
  const currentUser = getCurrentUser(state);
  let events = state.activeProject.logEvents.events[logViewId];
  if (SKUD_LOG.id === logViewId) {
    if (!includesInUserFilterTags(currentUser, 'SKUD')) {
      events = [];
    }
  }
  return {
    events,
    loadLogEventsError: state.errors.loadLogEvents
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogWidget);
