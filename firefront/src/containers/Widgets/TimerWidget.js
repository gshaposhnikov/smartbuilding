import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import Timer from 'components/Timer';

import { getActiveDevicesHash, getTimers } from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const UPDATE_INTERVAL = 1000;
const TIMER_STATES = {
  POSTPONED: 'paused',
  RESUMED: 'started',
  SCHEDULED: 'started'
};

class TimerWidget extends Component {
  constructor(props) {
    super(props);
    this.getVisibleTimers(props);
    this.state = {
      timeResidue: {}, // Остатки времени
      visibleTimers: [] // Видимые таймеры устройств
    };
  }

  componentWillReceiveProps(newProps) {
    this.getVisibleTimers(newProps);
  }

  updateTime() {
    const { visibleTimers } = this.state;
    const timeResidue = {};
    const now = moment().unix();
    // Фильтруем список устройств по остатку времени
    const newVisibleTimers = visibleTimers.filter(timer => {
      const { timeStart, delay, id } = timer;

      if (!delay || delay <= 0) return false;

      // Если таймер запущен, то обновляем время
      if (timer.state === 'started') {
        const residue = timeStart + delay - now;
        if (residue <= 0) return false;
        timeResidue[id] = residue;
      } else if (timer.state === 'paused') {
        timeResidue[id] = delay;
      } else {
        return false;
      }
      return true;
    });
    // Если нет видимых таймеров, то сбрасываем обновление времени
    if (!newVisibleTimers.length) {
      this.intervalEnabled = false;
      clearInterval(this.interval);
    }
    this.setState({
      timeResidue,
      visibleTimers: newVisibleTimers
    });
  }

  getVisibleTimers(props) {
    const {
      timers: { devices: deviceTimers, computerActions },
      devices
    } = props;
    const visibleTimers = [];
    const now = moment().unix();
    // Пробегаем по таймерам устройств
    for (let id in deviceTimers) {
      if (deviceTimers[id]) {
        const { timeStart, event, delay } = deviceTimers[id];
        // Находим нужно устройство в списке
        const device = devices[id];
        // Считаем сколько секунд осталось
        const residue = timeStart + delay - now;
        // Если отсчет не был закончен, то записываем
        if (residue > 0 || (TIMER_STATES[event] === 'paused' && (delay || delay > 0)))
          visibleTimers.push({
            id,
            name: device ? `${device.name} (${device.fullAddressPath})` : '',
            state: TIMER_STATES[event],
            timeStart,
            delay
          });
      }
    }
    for (let uniqId in computerActions) {
      if (computerActions[uniqId]) {
        const { timeStart, delay, name, message } = computerActions[uniqId];
        const residue = timeStart + delay - now;
        // Если отсчет не был закончен, то записываем
        if (residue > 0)
          visibleTimers.push({
            id: uniqId,
            name: `${name}(${message})`,
            state: TIMER_STATES.RESUMED,
            timeStart,
            delay
          });
      }
    }
    this.setState({ visibleTimers });
    // Если обновление времени не запущено и есть устройства, то запускаем
    if (!this.intervalEnabled && visibleTimers.length) {
      this.intervalEnabled = true;
      this.interval = setInterval(() => this.updateTime(), UPDATE_INTERVAL);
    }
    // Если запущено обновление времени, а устройств нету, то останавливаем
    if (this.intervalEnabled && !visibleTimers.length) {
      this.intervalEnabled = false;
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    this.intervalEnabled = false;
    clearInterval(this.interval);
  }

  render() {
    const { visibleTimers, timeResidue } = this.state;
    if (visibleTimers.length) {
      return <Timer visibleTimers={visibleTimers} timeResidue={timeResidue} />;
    } else return null;
  }
}

const mapStateToProps = state => {
  return {
    devices: getActiveDevicesHash(state),
    timers: getTimers(state),
    deviceProfileViewsHash: getDeviceProfileViewsHash(state)
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimerWidget);
