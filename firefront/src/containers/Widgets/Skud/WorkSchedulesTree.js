import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Modal } from 'antd';

import Tree from 'components/ExpandableTable/Tree';
import { TableHeader, TableHeaderCell } from 'components/ExpandableTable/styles';
import { DoubleCRUDMenu } from 'components/Menu/WidgetMenu';
import { getActiveProjectId } from 'helpers/activeProject';
import {
  selectWorkSchedulesTreeItem,
  skudMainActions,
  removeWorkScheduleDay,
  removeAccessGrantedTime
} from 'actions/skud';
import { createSelector } from 'reselect';
import { createTreeItem } from 'helpers/tree';
import { getCurrentUser, hasRole } from 'helpers/user';
import { ENTITY_TYPE, WORK_SCHEDULE_TYPES_HASH } from 'constants/skud';
import {
  getActiveWorkSchedulesHash,
  createWorkScheduleDayIdAndTreeItem,
  createAccessGrantedTimeIdAndTreeItem
} from 'helpers/skud';
import { includesInUserFilterTags } from 'helpers/filtration';

const WorkSchedulesTreeWrapper = styled.div`
  flex: 1;
  margin-bottom: 25px;
  background-color: white;
`;

class WorkSchedulesTree extends Component {
  static propTypes = {
    projectId: PropTypes.string,
    workSchedules: PropTypes.object,
    selectedTreeItem: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.columns = [
      {
        key: 'name',
        title: 'Название',
        dataIndex: 'entity',
        width: 280,
        stepper: true,
        render: (entity, node) => this.nameColumnRender(entity, node)
      },
      {
        key: 'description',
        title: 'Описание',
        dataIndex: 'entity',
        render: (entity, node) => this.descriptionColumnRender(entity, node)
      }
    ];
    this.state = {
      workScheduleTreeItemsHash: {},
      daysFolderTreeItemsHash: {},
      dayTreeItemsHash: {},
      timesFolderTreeItemsHash: {},
      timeTreeItemsHash: {},
      workSchedulesTree: [],
      isAddActive: false,
      isDeleteActive: false,
      treeInitialized: false
    };
  }

  nameColumnRender(entity, node) {
    return entity.name;
  }

  descriptionColumnRender(entity, node) {
    switch (node.entityType) {
      case ENTITY_TYPE.WORK_SCHEDULE:
        return WORK_SCHEDULE_TYPES_HASH[entity.workScheduleType].name;
      case ENTITY_TYPE.WORK_SCHEDULE_DAY:
        return entity.workday ? 'Рабочий день' : 'Выходной день';
      default:
        return entity.description;
    }
  }

  getWorkSchedulesTree(workSchedules, selectedTreeItem) {
    if (workSchedules) {
      const { treeInitialized } = this.state;
      const workScheduleTreeItemsHash = {};
      const daysFolderTreeItemsHash = {};
      const dayTreeItemsHash = {};
      const timesFolderTreeItemsHash = {};
      const timeTreeItemsHash = {};
      const rootTreeItem = createTreeItem(
        treeInitialized,
        ENTITY_TYPE.ROOT,
        'ROOT',
        { name: 'Предприятие' },
        true
      );
      const workSchedulesTree = [rootTreeItem];

      /* Графики работы */
      Object.keys(workSchedules).forEach(workScheduleId => {
        const workScheduleTreeItem = createTreeItem(
          treeInitialized,
          ENTITY_TYPE.WORK_SCHEDULE,
          workScheduleId,
          workSchedules[workScheduleId],
          selectedTreeItem && selectedTreeItem.entity.workScheduleId === workScheduleId
        );
        workScheduleTreeItemsHash[workScheduleId] = workScheduleTreeItem;
        rootTreeItem.children.push(workScheduleTreeItem);

        /* Дни */
        const daysFolderTreeItem = createTreeItem(
          treeInitialized,
          ENTITY_TYPE.DAYS_FOLDER,
          workScheduleId,
          {
            name: 'Дни',
            workScheduleId
          },
          true
        );
        daysFolderTreeItemsHash[workScheduleId] = daysFolderTreeItem;
        workScheduleTreeItem.children.push(daysFolderTreeItem);
        workSchedules[workScheduleId].days.forEach((day, dayNo) => {
          const [dayId, dayTreeItem] = createWorkScheduleDayIdAndTreeItem(
            workScheduleId,
            dayNo,
            day
          );
          dayTreeItem.state.expanded = selectedTreeItem && selectedTreeItem.entity.dayId === dayId;
          dayTreeItemsHash[dayId] = dayTreeItem;
          daysFolderTreeItem.children.push(dayTreeItem);

          /* Интервалы прохода */
          const timesFolderTreeItem = createTreeItem(
            treeInitialized,
            ENTITY_TYPE.GRANTED_TIMES_FOLDER,
            dayId,
            {
              name: 'Интервалы прохода',
              workScheduleId,
              dayId,
              dayNo
            },
            true
          );
          timesFolderTreeItemsHash[dayId] = timesFolderTreeItem;
          dayTreeItem.children.push(timesFolderTreeItem);
          day.accessGrantedTimes.forEach((accessGrantedTime, timeNo) => {
            const [timeId, timeTreeItem] = createAccessGrantedTimeIdAndTreeItem(
              workScheduleId,
              dayNo,
              dayId,
              timeNo,
              accessGrantedTime
            );
            timeTreeItemsHash[timeId] = timeTreeItem;
            timesFolderTreeItem.children.push(timeTreeItem);
          });
        });
      });

      this.setState({
        workScheduleTreeItemsHash,
        daysFolderTreeItemsHash,
        dayTreeItemsHash,
        timesFolderTreeItemsHash,
        timeTreeItemsHash,
        workSchedulesTree,
        treeInitialized: true
      });
    }
  }

  componentDidMount() {
    const { workSchedules, currentUser } = this.props;
    if (includesInUserFilterTags(currentUser, 'SKUD')) this.getWorkSchedulesTree(workSchedules);
  }

  componentWillReceiveProps(newProps) {
    const { workSchedules, selectedTreeItem, currentUser } = newProps;
    if (includesInUserFilterTags(currentUser, 'SKUD')) {
      if (workSchedules !== this.props.workSchedules) {
        this.getWorkSchedulesTree(workSchedules, selectedTreeItem);
      }
      if (selectedTreeItem !== this.props.selectedTreeItem) {
        const { isAddActive, isDeleteActive } = this.state;
        let isNowAddActive = false;
        let isNowDeleteActive = false;
        if (selectedTreeItem) {
          switch (selectedTreeItem.entityType) {
            case ENTITY_TYPE.GRANTED_TIMES_FOLDER:
            case ENTITY_TYPE.ACCESS_GRANTED_TIME:
            case ENTITY_TYPE.WORK_SCHEDULE:
            case ENTITY_TYPE.ROOT:
              isNowAddActive = true;
              break;
            case ENTITY_TYPE.DAYS_FOLDER:
            case ENTITY_TYPE.WORK_SCHEDULE_DAY: {
              const { workScheduleTreeItemsHash } = this.state;
              isNowAddActive =
                workScheduleTreeItemsHash[selectedTreeItem.entity.workScheduleId].entity
                  .workScheduleType === 'OTHER';
              break;
            }

            default:
              break;
          }

          switch (selectedTreeItem.entityType) {
            case ENTITY_TYPE.ACCESS_GRANTED_TIME:
            case ENTITY_TYPE.WORK_SCHEDULE:
              isNowDeleteActive = true;
              break;
            case ENTITY_TYPE.WORK_SCHEDULE_DAY: {
              const { workScheduleTreeItemsHash } = this.state;
              isNowDeleteActive =
                workScheduleTreeItemsHash[selectedTreeItem.entity.workScheduleId].entity
                  .workScheduleType === 'OTHER';
              break;
            }

            default:
              break;
          }
        }
        if (isNowAddActive !== isAddActive) this.setState({ isAddActive: isNowAddActive });
        if (isNowDeleteActive !== isDeleteActive)
          this.setState({ isDeleteActive: isNowDeleteActive });
      }
    }
  }

  onRowClick(treeItem) {
    const { dispatch } = this.props;
    dispatch(selectWorkSchedulesTreeItem(treeItem));
  }

  onAddClick() {
    const { selectedTreeItem, dispatch, workSchedules } = this.props;
    if (selectedTreeItem) {
      let newSelectedTreeItem;
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.WORK_SCHEDULE:
        case ENTITY_TYPE.ROOT: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_WORK_SCHEDULE,
            new: true,
            entity: {
              workScheduleType: 'WEEK',
              name: 'График работ #' + (Object.keys(workSchedules || {}).length + 1),
              activeFrom: new Date()
            }
          };
          break;
        }

        case ENTITY_TYPE.DAYS_FOLDER:
        case ENTITY_TYPE.WORK_SCHEDULE_DAY: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_WORK_SCHEDULE_DAY,
            new: true,
            entity: {
              workScheduleId: selectedTreeItem.entity.workScheduleId,
              workday: true,
              workStartTime: 480, // 8:00
              workStopTime: 1020, // 17:00
              lunchStartTime: 720, // 12:00
              lunchStopTime: 780 // 13:00
            }
          };
          break;
        }

        case ENTITY_TYPE.GRANTED_TIMES_FOLDER:
        case ENTITY_TYPE.ACCESS_GRANTED_TIME: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_ACCESS_GRANTED_TIME,
            new: true,
            entity: {
              workScheduleId: selectedTreeItem.entity.workScheduleId,
              dayId: selectedTreeItem.entity.dayId,
              dayNo: selectedTreeItem.entity.dayNo
            }
          };
          break;
        }

        default:
          break;
      }
      if (newSelectedTreeItem) {
        dispatch(selectWorkSchedulesTreeItem(newSelectedTreeItem));
      }
    }
  }

  onDeleteClick() {
    const { projectId, selectedTreeItem, dispatch } = this.props;
    if (selectedTreeItem) {
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.WORK_SCHEDULE: {
          Modal.confirm({
            title: 'Удаление графика работ',
            content: (
              <div>
                Удалить график работ <b>{selectedTreeItem.entity.name}</b>?
              </div>
            ),
            okText: 'Удалить',
            onCancel: () => {},
            cancelText: 'Отмена',
            maskClosable: false,
            onOk: () => {
              dispatch(skudMainActions.workSchedules.remove(projectId, selectedTreeItem.entity.id));
            }
          });
          break;
        }
        case ENTITY_TYPE.WORK_SCHEDULE_DAY:
          dispatch(
            removeWorkScheduleDay(
              projectId,
              selectedTreeItem.entity.workScheduleId,
              selectedTreeItem.entity.dayNo
            )
          );
          break;

        case ENTITY_TYPE.ACCESS_GRANTED_TIME:
          dispatch(
            removeAccessGrantedTime(
              projectId,
              selectedTreeItem.entity.workScheduleId,
              selectedTreeItem.entity.dayNo,
              selectedTreeItem.entity.timeNo
            )
          );
          break;

        default:
          break;
      }
    }
  }

  render() {
    const { selectedTreeItem, currentUser } = this.props;
    const { workSchedulesTree, isAddActive, isDeleteActive } = this.state;
    /* TODO: разобраться с высотой */
    return (
      <WorkSchedulesTreeWrapper>
        <DoubleCRUDMenu
          isAddActive={
            isAddActive &&
            hasRole(currentUser, 'SKUD_CREATE') &&
            includesInUserFilterTags(currentUser, 'SKUD')
          }
          onAdd={this.onAddClick.bind(this)}
          isDeleteActive={
            isDeleteActive &&
            hasRole(currentUser, 'SKUD_REMOVE') &&
            includesInUserFilterTags(currentUser, 'SKUD')
          }
          onDelete={this.onDeleteClick.bind(this)}
        />
        <TableHeader className="expandable__table__header">
          {this.columns.map((col, index) => (
            <TableHeaderCell width={col.width || 0} key={index}>
              {col.title}
            </TableHeaderCell>
          ))}
        </TableHeader>
        <div className="expandable__table-with-menu">
          <Tree
            rowSelection={selectedTreeItem ? selectedTreeItem.id : null}
            columns={this.columns}
            nodes={workSchedulesTree}
            onRowClick={this.onRowClick.bind(this)}
            initialExpandDeep={0}
          />
        </div>
      </WorkSchedulesTreeWrapper>
    );
  }
}

const mapStateToProps = createSelector(
  [
    getActiveProjectId,
    getActiveWorkSchedulesHash,
    ({
      widgets: {
        workSchedulesTree: { selectedTreeItem }
      }
    }) => selectedTreeItem,
    getCurrentUser
  ],
  (projectId, workSchedules, selectedTreeItem, currentUser) => ({
    projectId,
    workSchedules,
    selectedTreeItem,
    currentUser
  })
);

export default connect(mapStateToProps)(WorkSchedulesTree);
