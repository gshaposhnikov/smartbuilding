import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { Spin, Button } from 'antd';

import { getActiveDeviceList, getActiveProjectId } from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { filterDevicesLeavingParents, getDeviceProfile, getDevicesTree } from 'helpers/device';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import { DeviceTreeWrapper } from 'components/Layout';
import Table, { Column } from 'components/Table';
import { startReadAccessKeyValue, stopReadAccessKeyValue } from 'actions/skud';

const getAccessKeyReadersWithParents = createSelector(
  getActiveDeviceList,
  getDeviceProfileViewsHash,
  (devices, deviceProfileViewsHash) =>
    devices && deviceProfileViewsHash
      ? filterDevicesLeavingParents(devices, device => {
          const deviceProfile = getDeviceProfile(deviceProfileViewsHash, device);
          return deviceProfile && deviceProfile.accessKeyReader;
        })
      : []
);

const getAccessKeyReadersTree = createSelector(
  getAccessKeyReadersWithParents,
  getDeviceProfileViewsHash,
  (accessKeyReadersWithParents, deviceProfileViewsHash) =>
    accessKeyReadersWithParents && deviceProfileViewsHash
      ? getDevicesTree(accessKeyReadersWithParents, device => {
          const deviceProfile = getDeviceProfile(deviceProfileViewsHash, device);
          device.isActive =
            deviceProfile &&
            deviceProfile.accessKeyReader &&
            device.generalStateCategoryView &&
            device.generalStateCategoryView.id !== 'Malfunction';
          return device;
        })
      : []
);

const getReadAccessKeyValueInfo = ({
  widgets: {
    skudTree: { readAccessKeyValueInfo }
  }
}) => readAccessKeyValueInfo;

const mapStateToProps = createSelector(
  getActiveProjectId, //
  getAccessKeyReadersTree, //
  getReadAccessKeyValueInfo, //
  (projectId, accessKeyReadersTree, readAccessKeyValueInfo) => ({
    projectId,
    accessKeyReadersTree,
    readAccessKeyValueInfo
  })
);

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

class ChooseAccessKeyReader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentDevice: {}
    };
  }

  componentWillReceiveProps(newProps) {
    const { accessKeyReadersTree } = newProps;
    if (accessKeyReadersTree !== this.props.accessKeyReadersTree) {
      this.setState({ currentDevice: {} });
    }
  }

  rowClassName(record) {
    const { currentDevice } = this.state;
    return currentDevice.id === record.key ? 'selected-row' : '';
  }

  onRowClick(device) {
    const { currentDevice } = this.state;
    if (!currentDevice.id && !device.isActive) return;
    if (currentDevice.id === device.id) return;
    this.setState({
      currentDevice: device.isActive ? device : {}
    });
  }

  startAccessKeyRead(device) {
    const { projectId, accessKeyId, dispatch } = this.props;
    dispatch(startReadAccessKeyValue(projectId, device.id, accessKeyId));
  }

  stopAccessKeyRead() {
    const { currentDevice } = this.state;
    const { projectId, dispatch } = this.props;
    dispatch(stopReadAccessKeyValue(projectId, currentDevice.id));
  }

  onRowDoubleClick(device) {
    if (device.isActive) this.startAccessKeyRead(device);
  }

  render() {
    const { accessKeyReadersTree, readAccessKeyValueInfo } = this.props;

    return (
      <DeviceTreeWrapper>
        <Spin
          spinning={readAccessKeyValueInfo === 'WAITING'}
          onClick={() => this.stopAccessKeyRead()}
        >
          <Table
            hideDefaultSelections={true}
            defaultExpandAllRows={true}
            dataSource={accessKeyReadersTree}
            rowClassName={this.rowClassName.bind(this)}
            onRowClick={this.onRowClick.bind(this)}
            onRowDoubleClick={this.onRowDoubleClick.bind(this)}
          >
            <Column
              key="type"
              title="Устройство"
              dataIndex="type"
              width={200}
              render={(text, device) => <DeviceTypeTreeItem record={device} stateIconEnabled />}
            />
            <Column key="shortAddressPath" title="Адрес" width={80} dataIndex="shortAddressPath" />
            <Column key="regionName" title="Зона" width={80} dataIndex="regionName" />
            <Column
              key="startRead"
              width={50}
              render={(text, device) => (
                <div>
                  {device.isActive ? (
                    <Button
                      size="small"
                      icon="to-top"
                      onClick={() => {
                        this.onRowClick(device);
                        this.onRowDoubleClick(device);
                      }}
                    />
                  ) : null}
                </div>
              )}
            />
            <Column />
          </Table>
        </Spin>
      </DeviceTreeWrapper>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseAccessKeyReader);
