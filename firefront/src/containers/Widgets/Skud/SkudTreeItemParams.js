import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Field, reduxForm, initialize, reset, formValueSelector, change } from 'redux-form';

import { Collapse } from 'antd';
const Panel = Collapse.Panel;

import { WrapperWithLabel } from 'components/Layout';
import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import Select from 'containers/Selects';
import Form, { FormItem } from 'components/Form';
import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';
import { WhiteFormWrapper, ScrollableContainerWithoutHeader } from 'components/Layout';
import { ENTITY_TYPE } from 'constants/skud';
import DatePicker from 'components/Form/DatePicker';
import { getActiveProjectId } from 'helpers/activeProject';
import { skudMainActions } from 'actions/skud';
import DeviceSelect from 'containers/Selects/DeviceSelect';
import {
  getActiveAccessPointDevices,
  getActiveSkudRegions,
  getActiveWorkSchedulesHash,
  getActiveEmployeesHash,
  getActiveAccessKeysHash
} from 'helpers/skud';
import { getCurrentUser, hasRole } from 'helpers/user';
import { promptUnsavedChanges } from 'helpers/prompt';
import ChooseAccessKeyReader from './ChooseAccessKeyReader';

export const FORM_NAME = 'SkudTreeItemParamsForm';

const dateFormat = 'DD.MM.YYYY';
const MIN_KEY_VALUE_LENGTH = 4;
const MAX_KEY_VALUE_LENGTH = 12;

/**
 * Сброс значение формы
 * @param dispatch
 * @param values объект со значениями
 */
export function resetFormParams(dispatch, values) {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
}

class SkudTreeItemParamsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: 'Выберите элемент картотеки',
      displayParams: false,
      displayEmployeeParams: false,
      displayAccessKeyParams: false
    };
  }

  componentWillReceiveProps = nextProps => {
    const {
      selectedTreeItem,
      readAccessKeyValueInfo,
      currentValues,
      dirty,
      valid,
      employees,
      accessKeys
    } = this.props;
    if (nextProps.selectedTreeItem !== selectedTreeItem) {
      if (dirty && valid && selectedTreeItem && nextProps.selectedTreeItem) {
        if (
          (selectedTreeItem.id &&
            nextProps.selectedTreeItem.id &&
            nextProps.selectedTreeItem.id !== selectedTreeItem.id) ||
          (selectedTreeItem.id && nextProps.selectedTreeItem.new) ||
          (!selectedTreeItem.id &&
            selectedTreeItem.new &&
            (Object.keys(employees).length === Object.keys(nextProps.employees).length &&
              Object.keys(accessKeys).length === Object.keys(nextProps.accessKeys).length))
        ) {
          promptUnsavedChanges(this.onSave, selectedTreeItem, currentValues);
        }
      }
      if (nextProps.selectedTreeItem) {
        resetFormParams(nextProps.dispatch, nextProps.selectedTreeItem.entity);
        switch (nextProps.selectedTreeItem.entityType) {
          case ENTITY_TYPE.ROOT:
            this.setState({
              title: 'Выберите элемент картотеки или добавьте сотрудника',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.CARDS_FOLDER:
            this.setState({
              title: 'Выберите элемент картотеки или добавьте карточку доступа',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.PASSW_FOLDER:
            this.setState({
              title: 'Выберите элемент картотеки или добавьте пароль',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.EMPLOYEE:
            this.setState({
              title: `Сотрудник ${nextProps.selectedTreeItem.entity.name || '" "'}`,
              displayParams: true,
              displayEmployeeParams: true,
              displayAccessKeyParams: false
            });
            break;
          case ENTITY_TYPE.NEW_EMPLOYEE:
            this.setState({
              title: 'Новый сотрудник',
              displayParams: true,
              displayEmployeeParams: true,
              displayAccessKeyParams: false
            });
            break;
          case ENTITY_TYPE.CARD:
            this.setState({
              title: `Карточка доступа ${nextProps.selectedTreeItem.entity.keyValue || '" "'}`,
              displayParams: true,
              displayEmployeeParams: false,
              displayAccessKeyParams: true
            });
            break;
          case ENTITY_TYPE.NEW_CARD:
            this.setState({
              title: 'Новая карточка доступа',
              displayParams: true,
              displayEmployeeParams: false,
              displayAccessKeyParams: true
            });
            break;
          case ENTITY_TYPE.PASSWORD:
            this.setState({
              title: `Пароль  ${nextProps.selectedTreeItem.entity.keyValue || '" "'}`,
              displayParams: true,
              displayEmployeeParams: false,
              displayAccessKeyParams: true
            });
            break;
          case ENTITY_TYPE.NEW_PASSWORD:
            this.setState({
              title: 'Новый пароль',
              displayParams: true,
              displayEmployeeParams: false,
              displayAccessKeyParams: true
            });
            break;
          default:
            this.setState({ displayParams: false });
            break;
        }
      } else {
        this.setState({ title: 'Выберите элемент картотеки', displayParams: false });
      }
    } else if (nextProps.readAccessKeyValueInfo !== readAccessKeyValueInfo) {
      if (nextProps.readAccessKeyValueInfo instanceof Object) {
        if (
          (nextProps.selectedTreeItem.entityType === 'NEW_CARD' &&
            nextProps.readAccessKeyValueInfo.accessKeyId === 'NEW_CARD') ||
          (nextProps.selectedTreeItem.entityType === 'CARD' &&
            nextProps.selectedTreeItem.entity.id === nextProps.readAccessKeyValueInfo.accessKeyId)
        ) {
          nextProps.dispatch(
            change(FORM_NAME, 'keyValue', nextProps.readAccessKeyValueInfo.accessKeyValue)
          );
        }
      }
    }
  };

  componentWillUnmount = () => {
    const { valid, dirty, selectedTreeItem, currentValues } = this.props;
    if (dirty && valid && selectedTreeItem) {
      promptUnsavedChanges(this.onSave, selectedTreeItem, currentValues);
    }
  };

  doAddEmployee = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.employees.add(projectId, values));
  };

  doUpdateEmployee = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.employees.update(projectId, treeItem.entity.id, values));
  };

  doAddAccessKey = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.accessKeys.add(projectId, values));
  };

  doUpdateAccesskey = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.accessKeys.update(projectId, treeItem.entity.id, values));
  };

  onSave = (oldTreeItem, oldValues) => {
    const { selectedTreeItem, currentValues } = this.props;
    const treeItem = oldTreeItem.entityType ? oldTreeItem : selectedTreeItem;
    const values = oldTreeItem.entityType ? oldValues : currentValues;
    if (treeItem) {
      switch (treeItem.entityType) {
        case ENTITY_TYPE.NEW_EMPLOYEE:
          this.doAddEmployee(treeItem, values);
          break;
        case ENTITY_TYPE.EMPLOYEE:
          this.doUpdateEmployee(treeItem, values);
          break;
        case ENTITY_TYPE.NEW_CARD:
        case ENTITY_TYPE.NEW_PASSWORD:
          this.doAddAccessKey(treeItem, values);
          break;
        case ENTITY_TYPE.CARD:
        case ENTITY_TYPE.PASSWORD:
          this.doUpdateAccesskey(treeItem, values);
          break;
        default:
          break;
      }
    }
  };

  render() {
    const {
      selectedTreeItem,
      accessPointDevices,
      skudRegions,
      workSchedules,
      valid,
      dirty,
      currentUser,
      selectedAccessKeyId
    } = this.props;
    const { title, displayParams, displayEmployeeParams, displayAccessKeyParams } = this.state;

    return (
      <WhiteFormWrapper>
        <WidgetParamsEditorMenu
          titlePlacement="right"
          isViewTitle={true}
          title={title}
          isSaveActive={
            valid &&
            selectedTreeItem &&
            (selectedTreeItem.new
              ? (selectedTreeItem.entityType === ENTITY_TYPE.NEW_EMPLOYEE ||
                selectedTreeItem.entityType === ENTITY_TYPE.NEW_CARD ||
                selectedTreeItem.entityType === ENTITY_TYPE.NEW_PASSWORD) &&
              hasRole(currentUser, 'SKUD_CREATE')
              : dirty && hasRole(currentUser, 'SKUD_UPDATE'))
          }
          onSaveAction={this.onSave}
        />
        <ScrollableContainerWithoutHeader headerHeight="36">
          <Form
            style={{
              display: displayParams ? 'inline' : 'none'
            }}
          >
            <div style={{ display: displayEmployeeParams ? 'inline' : 'none' }}>
              <WrapperWithLabel label="Общее">
                <FormItem label="Фамилия" required>
                  <Field component={Input} name="lastName" />
                </FormItem>
                <FormItem label="Имя" required>
                  <Field component={Input} name="firstName" />
                </FormItem>
                <FormItem label="Отчество" required>
                  <Field component={Input} name="middleName" />
                </FormItem>
              </WrapperWithLabel>
            </div>
            <div style={{ display: displayAccessKeyParams ? 'inline' : 'none' }}>
              <WrapperWithLabel label="Время действия">
                <Field component={Input} name="keyType" className="row-hidden" />
                <Field component={Input} name="employeeId" className="row-hidden" />
                <FormItem label="Код" required>
                  <Field component={Input} name="keyValue" maxLength={MAX_KEY_VALUE_LENGTH} />
                </FormItem>
                <div
                  style={{
                    display:
                      selectedTreeItem &&
                        (selectedTreeItem.entityType === 'CARD' ||
                          selectedTreeItem.entityType === 'NEW_CARD')
                        ? 'inline'
                        : 'none'
                  }}
                >
                  <Collapse>
                    <Panel header="Считать код с ключа" key="1">
                      <ChooseAccessKeyReader accessKeyId={selectedAccessKeyId} />
                    </Panel>
                  </Collapse>
                </div>
                <FormItem label="Дата начала действия">
                  <Field
                    component={DatePicker}
                    name="activeFrom"
                    showTime={false}
                    dateTimeFormat={dateFormat}
                  />
                </FormItem>
                <FormItem label="Дата окончания действия">
                  <Field
                    component={DatePicker}
                    name="activeTo"
                    showTime={false}
                    dateTimeFormat={dateFormat}
                  />
                </FormItem>
              </WrapperWithLabel>
              <WrapperWithLabel label="Параметры">
                <FormItem>
                  <Field component={Checkbox} name="locked" label="Заблокировать" />
                </FormItem>
                <FormItem>
                  <Field component={Checkbox} name="ignoreAPBOn" label="Игнорировать АПБ" />
                </FormItem>
                <FormItem>
                  <Field
                    component={Checkbox}
                    name="confirmationKeyOn"
                    label="Подтверждающий ключ"
                  />
                </FormItem>
                <FormItem>
                  <Field component={Checkbox} name="advancedCodeOff" label="Без доп. кода" />
                </FormItem>
                <FormItem>
                  <Field component={Checkbox} name="disarmForcedOn" label="По принуждению" />
                </FormItem>
              </WrapperWithLabel>
            </div>
            <WrapperWithLabel label="Доступ и учет времени">
              <FormItem label="Точки доступа">
                <Field
                  component={DeviceSelect}
                  name="accessMap.accessPointDeviceIds"
                  devices={accessPointDevices}
                  mode="multiple"
                />
              </FormItem>
              <FormItem label="Рабочие зоны">
                <Field
                  component={Select}
                  name={'accessMap.workRegionIds'}
                  itemsSource={skudRegions}
                  mode="multiple"
                />
              </FormItem>
              <FormItem label="График работы">
                <Field
                  component={Select}
                  name={'accessMap.workScheduleId'}
                  itemsSource={workSchedules}
                />
              </FormItem>
            </WrapperWithLabel>
          </Form>
        </ScrollableContainerWithoutHeader>
      </WhiteFormWrapper>
    );
  }
}

const validate = values => {
  if (!values.employeeId) {
    if (!values.firstName) {
      return { firstName: 'Не задано имя сотрудника' };
    }
    if (!values.middleName) {
      return { middleName: 'Не задано отчество сотрудника' };
    }
    if (!values.lastName) {
      return { lastName: 'Не задана фамилия сотрудника' };
    }
  } else {
    if (values.keyValue) {
      if (values.keyValue.length < MIN_KEY_VALUE_LENGTH)
        return { keyValue: `Значение ключа меньше ${MIN_KEY_VALUE_LENGTH} символов` };
      if (values.keyValue.length > MAX_KEY_VALUE_LENGTH)
        return { keyValue: `Значение ключа больше ${MAX_KEY_VALUE_LENGTH} символов` };
      const regexp = /^[0-9A-Fa-f]*$/g;
      if (!regexp.test(values.keyValue))
        return {
          keyValue: 'Значение ключа должно содержать цифры и символы шестнадцатиричной арифметики'
        };
    }
  }
  return {};
};

SkudTreeItemParamsForm = reduxForm({
  form: FORM_NAME,
  validate
})(SkudTreeItemParamsForm);

const selector = formValueSelector(FORM_NAME);

const getSelectedTreeItem = ({
  widgets: {
    skudTree: { selectedTreeItem }
  }
}) => selectedTreeItem;

const getWorkSchedules = createSelector(
  getActiveWorkSchedulesHash,
  workSchedulesHash => {
    let workSchedules = [];
    if (workSchedulesHash) {
      workSchedules = Object.keys(workSchedulesHash).map(key => workSchedulesHash[key]);
    }
    workSchedules.push({ name: 'Всегда', id: '' });
    return workSchedules;
  }
);

const getSelectedAccessKeyId = createSelector(
  getSelectedTreeItem,
  selectedTreeItem => {
    if (!selectedTreeItem) return null;
    switch (selectedTreeItem.entityType) {
      case 'NEW_CARD':
        return 'NEW_CARD';
      case 'CARD':
        return selectedTreeItem.entity.id;
      default:
        return null;
    }
  }
);

const getReadAccessKeyValueInfo = ({
  widgets: {
    skudTree: { readAccessKeyValueInfo }
  }
}) => readAccessKeyValueInfo;

const mapStateToProps = createSelector(
  [
    getActiveProjectId,
    getSelectedTreeItem,
    getActiveAccessPointDevices,
    getActiveSkudRegions,
    getWorkSchedules,
    state =>
      state.form[FORM_NAME]
        ? selector(
          state,
          'firstName',
          'middleName',
          'lastName',
          'employeeId',
          'keyType',
          'keyValue',
          'activeFrom',
          'activeTo',
          'locked',
          'ignoreAPBOn',
          'confirmationKeyOn',
          'advancedCodeOff',
          'disarmForcedOn',
          'accessMap'
        )
        : {},
    getCurrentUser,
    getSelectedAccessKeyId,
    getReadAccessKeyValueInfo,
    getActiveEmployeesHash,
    getActiveAccessKeysHash
  ],
  (
    projectId,
    selectedTreeItem,
    accessPointDevices,
    skudRegions,
    workSchedules,
    currentValues,
    currentUser,
    selectedAccessKeyId,
    readAccessKeyValueInfo,
    employees,
    accessKeys
  ) => ({
    projectId,
    selectedTreeItem,
    accessPointDevices,
    skudRegions,
    workSchedules,
    currentValues,
    currentUser,
    selectedAccessKeyId,
    readAccessKeyValueInfo,
    employees,
    accessKeys
  })
);

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SkudTreeItemParamsForm);
