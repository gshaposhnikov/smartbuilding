import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Field, reduxForm, initialize, reset, formValueSelector } from 'redux-form';

import { WrapperWithLabel } from 'components/Layout';
import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import Select from 'containers/Selects';
import Form, { FormItem } from 'components/Form';
import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';
import { WhiteFormWrapper, ScrollableContainerWithoutHeader } from 'components/Layout';
import { ENTITY_TYPE, WORK_SCHEDULE_TYPES } from 'constants/skud';
import DatePicker from 'components/Form/DatePicker';
import TimePicker, { UNITS } from 'components/Form/TimePicker';
import { getActiveProjectId } from 'helpers/activeProject';
import { getCurrentUser, hasRole } from 'helpers/user';
import { promptUnsavedChanges } from 'helpers/prompt';
import {
  skudMainActions,
  addWorkScheduleDay,
  updateWorkScheduleDay,
  addAccessGrantedTime,
  updateAccessGrantedTime
} from 'actions/skud';

export const FORM_NAME = 'WorkScheduleParamsForm';

const dateFormat = 'DD.MM.YYYY';

/**
 * Сброс значение формы
 * @param dispatch
 * @param values объект со значениями
 */
export function resetFormParams(dispatch, values) {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
}

class WorkScheduleParamsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      displayParams: false,
      displayWorkScheduleParams: false,
      displayDayParams: false,
      displayTimeParams: false
    };
  }

  componentWillReceiveProps = nextProps => {
    const { selectedTreeItem, dirty, valid, currentValues } = this.props;
    if (nextProps.selectedTreeItem !== selectedTreeItem) {
      if (dirty && valid && nextProps.selectedTreeItem && selectedTreeItem) {
        if (
          // Смена сущности (проверка по id)
          Boolean(
            selectedTreeItem.id &&
            nextProps.selectedTreeItem.id &&
            selectedTreeItem.id !== nextProps.selectedTreeItem.id
          ) ||
          // Смена сущности (проверка по id рабочего графика)
          Boolean(
            !selectedTreeItem.id &&
            !nextProps.selectedTreeItem.new &&
            selectedTreeItem.new &&
            selectedTreeItem.entity &&
            nextProps.selectedTreeItem.entity &&
            (selectedTreeItem.entity.workScheduleId !==
              nextProps.selectedTreeItem.entity.workScheduleId ||
              false)
          ) ||
          // Смена сущности (при создании новой сущности)
          Boolean(selectedTreeItem.id && nextProps.selectedTreeItem.new && !selectedTreeItem.new) ||
          // Смена сущности (при переходе на папки)
          Boolean(
            !selectedTreeItem.id &&
            !nextProps.selectedTreeItem.new &&
            selectedTreeItem.new &&
            ((!selectedTreeItem.entity && nextProps.selectedTreeItem.entity) ||
              (!selectedTreeItem.entity && !nextProps.selectedTreeItem.entity))
          )
        ) {
          promptUnsavedChanges(this.onSave, selectedTreeItem, currentValues);
        }
      }
      if (nextProps.selectedTreeItem) {
        resetFormParams(nextProps.dispatch, nextProps.selectedTreeItem.entity);
        switch (nextProps.selectedTreeItem.entityType) {
          case ENTITY_TYPE.ROOT:
            this.setState({
              title: 'Выберите график работ или добавьте новый',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.WORK_SCHEDULE:
            this.setState({
              title: `График работ ${nextProps.selectedTreeItem.entity.name}`,
              displayParams: true,
              displayWorkScheduleParams: true,
              displayDayParams: false,
              displayTimeParams: false
            });
            break;
          case ENTITY_TYPE.NEW_WORK_SCHEDULE:
            this.setState({
              title: 'Новый график работ',
              displayParams: true,
              displayWorkScheduleParams: true,
              displayDayParams: false,
              displayTimeParams: false
            });
            break;
          case ENTITY_TYPE.DAYS_FOLDER:
            this.setState({
              title: 'Выберите день или создайте новый',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.WORK_SCHEDULE_DAY:
            this.setState({
              title: `День №${nextProps.selectedTreeItem.entity.name} графика работ`,
              displayParams: true,
              displayWorkScheduleParams: false,
              displayDayParams: true,
              displayTimeParams: false
            });
            break;
          case ENTITY_TYPE.NEW_WORK_SCHEDULE_DAY:
            this.setState({
              title: 'Новый день графика работ',
              displayParams: true,
              displayWorkScheduleParams: false,
              displayDayParams: true,
              displayTimeParams: false
            });
            break;
          case ENTITY_TYPE.GRANTED_TIMES_FOLDER:
            this.setState({
              title: 'Выберите интервал прохода или добавьте новый',
              displayParams: false
            });
            break;
          case ENTITY_TYPE.ACCESS_GRANTED_TIME:
            this.setState({
              title: `Интервал прохода ${nextProps.selectedTreeItem.entity.name} графика работ`,
              displayParams: true,
              displayWorkScheduleParams: false,
              displayDayParams: false,
              displayTimeParams: true
            });
            break;
          case ENTITY_TYPE.NEW_ACCESS_GRANTED_TIME:
            this.setState({
              title: `Новый интервал прохода графика работ`,
              displayParams: true,
              displayWorkScheduleParams: false,
              displayDayParams: false,
              displayTimeParams: true
            });
            break;

          default:
            this.setState({ displayParams: false });
            break;
        }
      } else {
        this.setState({ title: 'Выберите элемент картотеки', displayParams: false });
      }
    }
  };

  componentWillUnmount = () => {
    const { valid, dirty, selectedTreeItem, currentValues } = this.props;
    if (dirty && valid && selectedTreeItem) {
      promptUnsavedChanges(this.onSave, selectedTreeItem, currentValues);
    }
  };

  doAddWorkSchedule = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.workSchedules.add(projectId, values));
  };

  doUpdateWorkSchedule = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(skudMainActions.workSchedules.update(projectId, treeItem.entity.id, values));
  };

  doAddWorkScheduleDay = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(addWorkScheduleDay(projectId, treeItem.entity.workScheduleId, values));
  };

  doUpdateWorkScheduleDay = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(
      updateWorkScheduleDay(
        projectId,
        treeItem.entity.workScheduleId,
        treeItem.entity.dayNo,
        values
      )
    );
  };

  doAddAccessGrantedTime = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(
      addAccessGrantedTime(projectId, treeItem.entity.workScheduleId, treeItem.entity.dayNo, values)
    );
  };

  doUpdateAccessGrantedTime = (treeItem, values) => {
    const { dispatch, projectId } = this.props;
    dispatch(
      updateAccessGrantedTime(
        projectId,
        treeItem.entity.workScheduleId,
        treeItem.entity.dayNo,
        treeItem.entity.timeNo,
        values
      )
    );
  };

  onSave = (oldTreeItem, oldValues) => {
    const { selectedTreeItem, currentValues } = this.props;
    const treeItem = oldTreeItem.entityType ? oldTreeItem : selectedTreeItem;
    const values = oldTreeItem.entityType ? oldValues : currentValues;
    if (treeItem) {
      switch (treeItem.entityType) {
        case ENTITY_TYPE.NEW_WORK_SCHEDULE:
          this.doAddWorkSchedule(treeItem, values);
          break;
        case ENTITY_TYPE.WORK_SCHEDULE:
          this.doUpdateWorkSchedule(treeItem, values);
          break;
        case ENTITY_TYPE.NEW_WORK_SCHEDULE_DAY:
          this.doAddWorkScheduleDay(treeItem, values);
          break;
        case ENTITY_TYPE.WORK_SCHEDULE_DAY:
          this.doUpdateWorkScheduleDay(treeItem, values);
          break;
        case ENTITY_TYPE.NEW_ACCESS_GRANTED_TIME:
          this.doAddAccessGrantedTime(treeItem, values);
          break;
        case ENTITY_TYPE.ACCESS_GRANTED_TIME:
          this.doUpdateAccessGrantedTime(treeItem, values);
          break;

        default:
          break;
      }
    }
  };

  render() {
    const { selectedTreeItem, valid, dirty, currentValues, currentUser } = this.props;
    const {
      title,
      displayParams,
      displayWorkScheduleParams,
      displayDayParams,
      displayTimeParams
    } = this.state;

    return (
      <WhiteFormWrapper>
        <WidgetParamsEditorMenu
          titlePlacement="right"
          isViewTitle={true}
          title={title}
          isSaveActive={
            valid &&
            selectedTreeItem &&
            (selectedTreeItem.new
              ? (selectedTreeItem.entityType === ENTITY_TYPE.NEW_WORK_SCHEDULE ||
                selectedTreeItem.entityType === ENTITY_TYPE.NEW_WORK_SCHEDULE_DAY ||
                selectedTreeItem.entityType === ENTITY_TYPE.NEW_ACCESS_GRANTED_TIME) &&
              hasRole(currentUser, 'SKUD_CREATE')
              : dirty && hasRole(currentUser, 'SKUD_UPDATE'))
          }
          onSaveAction={this.onSave}
        />
        <ScrollableContainerWithoutHeader headerHeight="36">
          <Form
            style={{
              display: displayParams ? 'inline' : 'none'
            }}
          >
            <div style={{ display: displayWorkScheduleParams ? 'inline' : 'none' }}>
              <WrapperWithLabel label="Общее">
                <FormItem label="Название" required>
                  <Field component={Input} name="name" />
                </FormItem>
                <FormItem label="Описание">
                  <Field component={Input} type="textarea" rows={4} name="description" />
                </FormItem>
              </WrapperWithLabel>
              <WrapperWithLabel label="Тип и начало действия">
                <FormItem label="Дата начала действия">
                  <Field
                    component={DatePicker}
                    name="activeFrom"
                    showTime={false}
                    dateTimeFormat={dateFormat}
                  />
                </FormItem>
                <FormItem label="Тип графика">
                  <Field
                    component={Select}
                    name="workScheduleType"
                    itemsSource={WORK_SCHEDULE_TYPES}
                  />
                </FormItem>
              </WrapperWithLabel>
            </div>
            <div style={{ display: displayDayParams ? 'inline' : 'none' }}>
              <WrapperWithLabel>
                <FormItem>
                  <Field component={Checkbox} name="workday" label="Рабочий" />
                </FormItem>
              </WrapperWithLabel>
              <WrapperWithLabel label="Рабочее время">
                <FormItem label="Время начала" required={currentValues.workday}>
                  <Field
                    component={TimePicker}
                    name="workStartTime"
                    disabled={!currentValues.workday}
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
                <FormItem label="Время окончания" required={currentValues.workday}>
                  <Field
                    component={TimePicker}
                    name="workStopTime"
                    disabled={!currentValues.workday}
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
              </WrapperWithLabel>
              <WrapperWithLabel label="Время перерыва">
                <FormItem label="Начало перерыва">
                  <Field
                    component={TimePicker}
                    name="lunchStartTime"
                    disabled={!currentValues.workday}
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
                <FormItem label="Окончание перерыва">
                  <Field
                    component={TimePicker}
                    name="lunchStopTime"
                    disabled={!currentValues.workday}
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
              </WrapperWithLabel>
            </div>
            <div style={{ display: displayTimeParams ? 'inline' : 'none' }}>
              <WrapperWithLabel label="Интервал прохода">
                <FormItem label="С" required>
                  <Field
                    component={TimePicker}
                    name="from"
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
                <FormItem label="По" required>
                  <Field
                    component={TimePicker}
                    name="to"
                    units={UNITS.MINUTES}
                    timeFormat="HH:mm"
                  />
                </FormItem>
              </WrapperWithLabel>
            </div>

            {/* спрятанные поля нужны, чтобы проверять их наличие при валидации,
              чтобы определить тип редактируемой сущности */}
            <Field component={Input} name="workScheduleId" className="row-hidden" />
            <Field component={Input} name="dayId" className="row-hidden" />
          </Form>
        </ScrollableContainerWithoutHeader>
      </WhiteFormWrapper>
    );
  }
}

const validate = values => {
  if (!values.workScheduleId) {
    /* проверяем параметры графика */
    if (!values.name) {
      return { name: 'Не задано название графика' };
    }
    if (!values.workScheduleType) {
      return { workScheduleType: 'Не указан тип графика' };
    }
  } else if (!values.dayId) {
    /* проверяем параметры дня */
    if (values.workday) {
      if (isNaN(values.workStartTime) || values.workStartTime < 0) {
        return { workStartTime: 'Не задано время начала рабочего дня' };
      }
      if (
        isNaN(values.workStopTime) ||
        values.workStopTime < 0 ||
        values.workStopTime <= values.workStartTime
      ) {
        return { workStopTime: 'Не задано время окончания рабочего дня' };
      }
    }
  } else {
    /* проверяем параметры интервала прохода */
    if (isNaN(values.from) || values.from < 0) {
      return { from: 'Не задано время начала разрешенного прохода' };
    }
    if (isNaN(values.to) || values.to < 0 || values.to <= values.from) {
      return { to: 'Не задано время окончания разрешенного прохода' };
    }
  }
  return {};
};

WorkScheduleParamsForm = reduxForm({
  form: FORM_NAME,
  validate
})(WorkScheduleParamsForm);

const selector = formValueSelector(FORM_NAME);
const mapStateToProps = createSelector(
  [
    getActiveProjectId,
    ({
      widgets: {
        workSchedulesTree: { selectedTreeItem }
      }
    }) => selectedTreeItem,
    state =>
      state.form[FORM_NAME]
        ? selector(
          state,
          'name',
          'description',
          'activeFrom',
          'workScheduleType',
          'workday',
          'workStartTime',
          'workStopTime',
          'lunchStartTime',
          'lunchStopTime',
          'from',
          'to'
        )
        : {},
    getCurrentUser
  ],
  (projectId, selectedTreeItem, currentValues, currentUser) => ({
    projectId,
    selectedTreeItem,
    currentValues,
    currentUser
  })
);

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkScheduleParamsForm);
