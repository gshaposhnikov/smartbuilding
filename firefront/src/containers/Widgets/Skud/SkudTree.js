import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { Modal } from 'antd';

import Tree from 'components/ExpandableTable/Tree';
import { TableHeader, TableHeaderCell } from 'components/ExpandableTable/styles';
import { DoubleCRUDMenu } from 'components/Menu/WidgetMenu';
import { ENTITY_TYPE } from 'constants/skud';
import { selectSkudTreeItem, skudMainActions } from 'actions/skud';
import { getActiveEmployeesHash, getActiveAccessKeysHash } from 'helpers/skud';

import { getCurrentUser, hasRole } from 'helpers/user';
import { createTreeItem } from 'helpers/tree';
import { getActiveProjectId } from 'helpers/activeProject';
import { includesInUserFilterTags } from 'helpers/filtration';

const SkudTreeWrapper = styled.div`
  flex: 1;
  margin-bottom: 25px;
  background-color: white;
`;

class SkudTree extends Component {
  static propTypes = {
    projectId: PropTypes.string,
    employees: PropTypes.object,
    accessKeys: PropTypes.object,
    selectedTreeItem: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'name',
        title: 'Название',
        dataIndex: 'entity',
        width: 250,
        stepper: true,
        render: (entity, node) => this.nameColumnRender(entity, node)
      },
      {
        key: 'description',
        title: 'Описание',
        dataIndex: 'entity',
        render: (entity, node) => this.descriptionColumnRender(entity, node)
      }
    ];
    this.state = {
      employeeTreeItemsHash: {},
      cardFolderTreeItemsHash: {},
      passwFolderTreeItemsHash: {},
      accessKeyTreeItemsHash: {},
      skudTree: [],
      treeInitialized: false
    };
  }

  nameColumnRender(entity, node) {
    return entity.name || entity.keyValue;
  }

  descriptionColumnRender(entity, node) {
    return entity.description;
  }

  /* TODO: убрать пересоздание дерева при изменении элементов и сделать сохранение развернутости */
  getSkudTree(employees, accessKeys, selectedTreeItem) {
    if (employees && accessKeys) {
      const { treeInitialized } = this.state;
      const employeeTreeItemsHash = {};
      const cardFolderTreeItemsHash = {};
      const passwFolderTreeItemsHash = {};
      const accessKeyTreeItemsHash = {};
      const rootTreeItem = createTreeItem(
        treeInitialized,
        ENTITY_TYPE.ROOT,
        'ROOT',
        { name: 'Предприятие' },
        true
      );
      const skudTree = [rootTreeItem];
      /* Сотрудники */
      Object.keys(employees).forEach(employeeId => {
        const employeeTreeItem = createTreeItem(
          treeInitialized,
          ENTITY_TYPE.EMPLOYEE,
          employeeId,
          employees[employeeId],
          selectedTreeItem && selectedTreeItem.entity.employeeId === employeeId
        );
        const cardFolderTreeItem = createTreeItem(
          treeInitialized,
          ENTITY_TYPE.CARDS_FOLDER,
          employeeId,
          {
            name: 'Карточки',
            employeeId
          },
          true
        );
        const passwFolderTreeItem = createTreeItem(
          treeInitialized,
          ENTITY_TYPE.PASSW_FOLDER,
          employeeId,
          {
            name: 'Пароли',
            employeeId
          },
          true
        );
        employeeTreeItemsHash[employeeId] = employeeTreeItem;
        cardFolderTreeItemsHash[employeeId] = cardFolderTreeItem;
        passwFolderTreeItemsHash[employeeId] = passwFolderTreeItem;
        employeeTreeItem.children.push(cardFolderTreeItem);
        employeeTreeItem.children.push(passwFolderTreeItem);
        rootTreeItem.children.push(employeeTreeItem);
      });
      /* Ключи доступа (карты и пароли) */
      Object.keys(accessKeys).forEach(accessKeyId => {
        const accessKey = accessKeys[accessKeyId];
        const accessKeyTreeItem = createTreeItem(
          treeInitialized,
          accessKey.keyType,
          accessKeyId,
          accessKey
        );
        accessKeyTreeItemsHash[accessKeyId] = accessKeyTreeItem;
        if (accessKey.keyType === ENTITY_TYPE.CARD) {
          cardFolderTreeItemsHash[accessKey.employeeId].children.push(accessKeyTreeItem);
        } else {
          passwFolderTreeItemsHash[accessKey.employeeId].children.push(accessKeyTreeItem);
        }
      });

      this.setState({
        employeeTreeItemsHash,
        cardFolderTreeItemsHash,
        passwFolderTreeItemsHash,
        accessKeyTreeItemsHash,
        skudTree,
        treeInitialized: true
      });
    }
  }

  componentDidMount() {
    const { employees, accessKeys, currentUser } = this.props;
    if (includesInUserFilterTags(currentUser, 'SKUD')) this.getSkudTree(employees, accessKeys);
  }

  componentWillReceiveProps(nextProps) {
    const { employees, accessKeys, selectedTreeItem, currentUser } = nextProps;
    if (includesInUserFilterTags(currentUser, 'SKUD'))
      if (employees !== this.props.employees || accessKeys !== this.props.accessKeys) {
        this.getSkudTree(employees, accessKeys, selectedTreeItem);
      }
  }

  onRowClick(treeItem) {
    const { dispatch } = this.props;
    dispatch(selectSkudTreeItem(treeItem));
  }

  onAddClick() {
    const { selectedTreeItem, dispatch } = this.props;
    if (selectedTreeItem) {
      let newSelectedTreeItem;
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.EMPLOYEE:
        case ENTITY_TYPE.ROOT: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_EMPLOYEE,
            new: true,
            entity: {}
          };
          break;
        }

        case ENTITY_TYPE.CARDS_FOLDER:
        case ENTITY_TYPE.CARD: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_CARD,
            new: true,
            entity: {
              keyType: ENTITY_TYPE.CARD,
              employeeId: selectedTreeItem.entity.employeeId
            }
          };
          break;
        }

        case ENTITY_TYPE.PASSW_FOLDER:
        case ENTITY_TYPE.PASSWORD: {
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_PASSWORD,
            new: true,
            entity: {
              keyType: ENTITY_TYPE.PASSWORD,
              employeeId: selectedTreeItem.entity.employeeId
            }
          };
          break;
        }

        default:
          break;
      }
      if (newSelectedTreeItem) {
        dispatch(selectSkudTreeItem(newSelectedTreeItem));
      }
    }
  }

  onDeleteClick() {
    const { projectId, selectedTreeItem, dispatch, employees } = this.props;
    if (selectedTreeItem) {
      const { entity: treeItemEntity } = selectedTreeItem;
      const modalParams = {
        okText: 'Удалить',
        onCancel: () => {},
        cancelText: 'Отмена',
        maskClosable: false
      };
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.EMPLOYEE: {
          modalParams['title'] = 'Удаление сотрудника';
          modalParams['content'] = (
            <div>
              Удалить сотрудника <b>{treeItemEntity.name}</b>?
            </div>
          );
          modalParams['onOk'] = () => {
            dispatch(skudMainActions.employees.remove(projectId, treeItemEntity.id));
          };
          break;
        }

        case ENTITY_TYPE.CARD: {
          modalParams['title'] = 'Удаление ключа доступа';
          modalParams['content'] = (
            <div>
              Удалить карточку{treeItemEntity.keyValue ? `(код: ${treeItemEntity.keyValue}) ` : ' '}
              сотрудника <b>{employees[treeItemEntity.employeeId].name}</b>?
            </div>
          );
          modalParams['onOk'] = () => {
            dispatch(skudMainActions.accessKeys.remove(projectId, treeItemEntity.id));
          };
          break;
        }
        case ENTITY_TYPE.PASSWORD: {
          modalParams['title'] = 'Удаление ключа доступа';
          modalParams['content'] = (
            <div>
              Удалить пароль{treeItemEntity.keyValue ? `(код: ${treeItemEntity.keyValue}) ` : ' '}
              сотрудника <b>{employees[treeItemEntity.employeeId].name}</b>?
            </div>
          );
          modalParams['onOk'] = () => {
            dispatch(skudMainActions.accessKeys.remove(projectId, treeItemEntity.id));
          };
          break;
        }
        default:
          break;
      }
      if (modalParams.onOk) Modal.confirm(modalParams);
    }
  }

  render() {
    const { selectedTreeItem, currentUser } = this.props;
    const { skudTree } = this.state;
    return (
      <SkudTreeWrapper>
        <DoubleCRUDMenu
          isAddActive={
            includesInUserFilterTags(currentUser, 'SKUD') &&
            hasRole(currentUser, 'SKUD_CREATE') &&
            selectedTreeItem &&
            selectedTreeItem.entityType !== ENTITY_TYPE.NEW_CARD &&
            selectedTreeItem.entityType !== ENTITY_TYPE.NEW_EMPLOYEE &&
            selectedTreeItem.entityType !== ENTITY_TYPE.NEW_PASSWORD
          }
          onAdd={this.onAddClick.bind(this)}
          isDeleteActive={
            includesInUserFilterTags(currentUser, 'SKUD') &&
            hasRole(currentUser, 'SKUD_REMOVE') &&
            selectedTreeItem &&
            (selectedTreeItem.entityType === ENTITY_TYPE.EMPLOYEE ||
              selectedTreeItem.entityType === ENTITY_TYPE.CARD ||
              selectedTreeItem.entityType === ENTITY_TYPE.PASSWORD)
          }
          onDelete={this.onDeleteClick.bind(this)}
        />
        <TableHeader>
          {this.columns.map((col, index) => (
            <TableHeaderCell width={col.width || 0} key={index}>
              {col.title}
            </TableHeaderCell>
          ))}
        </TableHeader>
        <div className="expandable__table-with-menu">
          <Tree
            rowSelection={selectedTreeItem ? selectedTreeItem.id : null}
            columns={this.columns}
            nodes={skudTree}
            onRowClick={this.onRowClick.bind(this)}
            initialExpandDeep={0}
          />
        </div>
      </SkudTreeWrapper>
    );
  }
}

const mapStateToProps = createSelector(
  [
    getActiveProjectId,
    getActiveEmployeesHash,
    getActiveAccessKeysHash,
    ({
      widgets: {
        skudTree: { selectedTreeItem }
      }
    }) => selectedTreeItem,
    getCurrentUser
  ],
  (projectId, employees, accessKeys, selectedTreeItem, currentUser) => ({
    projectId,
    employees,
    accessKeys,
    selectedTreeItem,
    currentUser
  })
);

export default connect(mapStateToProps)(SkudTree);
