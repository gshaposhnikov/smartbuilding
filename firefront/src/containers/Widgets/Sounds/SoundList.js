import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';

import Table, { Column } from 'components/Table';
import { DoubleCRUDMenu } from 'components/Menu/WidgetMenu';
import Widget from 'components/Widget';

import { selectSound, deleteMedia } from 'actions/medias';

class SoundList extends Component {
  state = {
    height: 500
  };

  getRowClassName = record => {
    const { selectedSound } = this.props;
    if (selectedSound) {
      if (record.id === selectedSound.id) return 'selected-row';
    }
    return '';
  };

  onRowClick = record => {
    const { dispatch } = this.props;
    dispatch(selectSound(record));
  };

  onAddClick = () => {
    const { dispatch } = this.props;
    dispatch(selectSound({ mediaType: 'SOUND' }));
  };

  onDeleteClick = () => {
    const { dispatch, selectedSound } = this.props;
    Modal.confirm({
      title: 'Удаление звука',
      content: (
        <div>
          Удалить звук <b>{selectedSound.name}</b>?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteMedia(selectedSound.id));
      }
    });
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(selectSound(null));
  };

  render = () => {
    const { sounds, selectedSound } = this.props;
    const { height } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <DoubleCRUDMenu
          isAddActive={true}
          onAdd={this.onAddClick}
          isDeleteActive={selectedSound && selectedSound.id ? true : false}
          onDelete={this.onDeleteClick}
        />
        <Table
          dataSource={sounds}
          rowKey="id"
          scroll={{ y: height - 61 }}
          onRowClick={this.onRowClick}
          rowClassName={this.getRowClassName}
        >
          <Column title="Название" dataIndex="name" key="id" />
          <Column title="Описание" dataIndex="description" />
        </Table>
      </Widget>
    );
  };
}

const mapStateToProps = state => {
  return {
    sounds: Object.values(state.medias.sounds),
    selectedSound: state.widgets.selectedSound
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SoundList);
