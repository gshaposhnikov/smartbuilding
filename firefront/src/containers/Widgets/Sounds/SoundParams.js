import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, reset, change } from 'redux-form';
import { Button, Icon, Upload } from 'antd';

import Input from 'components/Form/Input';
import { FormItem } from 'components/Form';
import {
  WrapperWithoutPadding as WidgetWrapper,
  WrapperWithPadding as FormWrapper,
  Row
} from 'components/Layout';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';

import { addMedia, updateMedia } from 'actions/medias';

import { bufferToBase64, base64ToBuffer } from 'helpers/audio';

const FORM_NAME = 'soundForm';
const required = value => (value ? undefined : 'Required');
const MAX_FILE_SIZE = 15728640;
const FILE_ERRORS = {
  SIZE: 'Максимальный размер файла 15мб',
  INVALID: 'Неудалось декодировать'
};

class SoundParams extends Component {
  state = {
    fileList: [],
    isPlay: false,
    decodedSoundId: null
  };
  audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  audioSource = null;
  audioBuffer = null;

  handleSubmit = e => {
    e.preventDefault();
    const { selectedSound } = this.props;
    if (selectedSound) {
      if (selectedSound.id) {
        this.updateSound();
      } else {
        this.addSound();
      }
    }
  };

  handleRequest = ({ file }) => {
    const { dispatch } = this.props;
    this.playStopSound(true);
    if (file.size > MAX_FILE_SIZE) {
      dispatch(change(FORM_NAME, 'content', null));
      this.setState({ fileList: [], errorMessage: FILE_ERRORS.SIZE });
    } else {
      const reader = new FileReader();
      reader.onload = data => {
        const base64String = bufferToBase64(reader.result);
        const audioFromString = base64ToBuffer(base64String);

        this.audioCtx.decodeAudioData(
          audioFromString,
          buffer => {
            this.audioBuffer = buffer;

            this.setState({
              fileList: [{ uid: 1, name: file.name, status: 'done' }],
              errorMessage: null
            });
            dispatch(change(FORM_NAME, 'content', base64String));
          },
          e => {
            this.setState({ fileList: [], errorMessage: FILE_ERRORS.INVALID, content: '' });
            dispatch(change(FORM_NAME, 'content', null));
          }
        );
      };
      reader.readAsArrayBuffer(file);
    }
  };

  addSound = () => {
    const { formValues, dispatch } = this.props;
    const newSound = {
      ...formValues
    };
    dispatch(addMedia(newSound));
  };

  updateSound = () => {
    const { formValues, dispatch, selectedSound } = this.props;
    const updatedSound = { ...selectedSound, ...formValues };
    dispatch(updateMedia(updatedSound));
  };

  componentWillReceiveProps = newProps => {
    if (newProps.selectedSound) {
      const { selectedSound, dispatch } = this.props;
      if (
        (selectedSound && selectedSound.id !== newProps.selectedSound.id) ||
        (!selectedSound && newProps.selectedSound)
      ) {
        dispatch(reset(FORM_NAME));
        if (newProps.selectedSound.id && newProps.selectedSound.content) {
          this.audioCtx.decodeAudioData(
            base64ToBuffer(newProps.selectedSound.content),
            buffer => {
              this.audioBuffer = buffer;
            },
            null
          );
        }
        this.playStopSound(true);
        this.setState({ fileList: [] });
      }
    }
  };

  playStopSound = isPlay => {
    if (isPlay) {
      if (this.audioSource) {
        this.audioSource.stop(0);
      }
    } else {
      this.audioSource = this.audioCtx.createBufferSource();
      this.audioSource.buffer = this.audioBuffer;
      this.audioSource.loop = false;
      this.audioSource.connect(this.audioCtx.destination);
      this.audioSource.start(0);
      this.audioSource.addEventListener('ended', () => {
        this.playStopSound(true);
      });
    }
    this.setState({ isPlay: !isPlay });
  };

  onRemoveFile = () => {
    const { dispatch } = this.props;
    const { isPlay } = this.state;
    this.setState({ fileList: [] });
    dispatch(change(FORM_NAME, 'content', ''));
    if (isPlay) this.playStopSound(true);
  };

  componentWillUnmount = () => {
    this.playStopSound(true);
  };

  render() {
    const {
      selectedSound,
      pristine,
      valid,
      formValues: { content }
    } = this.props;
    const { fileList, errorMessage, isPlay } = this.state;
    return (
      <WidgetWrapper>
        <WidgetParamsEditorMenu
          isViewTitle={true}
          title={selectedSound ? selectedSound.name || 'Новый звук' : 'Не выбран звук'}
          isSaveActive={selectedSound && !pristine && valid && content ? true : false}
          onSaveAction={this.handleSubmit}
        />
        {selectedSound ? (
          <form>
            <Row>
              <FormWrapper>
                <FormItem label="Название" required>
                  <Field component={Input} name="name" validate={[required]} />
                </FormItem>
                <FormItem label="Описание">
                  <Field
                    component={Input}
                    name="description"
                    type="textarea"
                    rows={3}
                    maxLength={256}
                  />
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    icon={isPlay ? 'pause' : 'caret-right'}
                    disabled={!content}
                    onClick={() => this.playStopSound(isPlay)}
                  >
                    {isPlay ? 'Остановить' : 'Прослушать'}
                  </Button>
                </FormItem>
                <FormItem
                  label="Файл"
                  required
                  help={errorMessage ? errorMessage : ''}
                  validateStatus={errorMessage ? 'error' : ''}
                >
                  <Field
                    component={Upload}
                    name="сontent"
                    customRequest={this.handleRequest}
                    fileList={fileList}
                    accept="audio/*"
                    multiple={false}
                    onRemove={this.onRemoveFile}
                  >
                    <Button>
                      <Icon type="upload" /> {'Выбрать файл'}
                    </Button>
                  </Field>
                </FormItem>
              </FormWrapper>
            </Row>
          </form>
        ) : null}
      </WidgetWrapper>
    );
  }
}

SoundParams = reduxForm({ form: FORM_NAME, enableReinitialize: true })(SoundParams);

const mapStateToProps = state => {
  return {
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {},
    selectedSound: state.widgets.selectedSound,
    initialValues: state.widgets.selectedSound || {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(SoundParams);
