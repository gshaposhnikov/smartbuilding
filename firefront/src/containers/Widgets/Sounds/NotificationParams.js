import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, reset } from 'redux-form';
import { Button } from 'antd';

import { FormItem } from 'components/Form';
import {
  WrapperWithoutPadding as WidgetWrapper,
  WrapperWithPadding as FormWrapper,
  Row
} from 'components/Layout';
import Checkbox from 'components/Form/Checkbox';
import Select, { Option } from 'components/Form/Select';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';

import { addSoundNotification, updateSoundNotification } from 'actions/soundNotifications';

import { base64ToBuffer } from 'helpers/audio';

const FORM_NAME = 'soundNotificationsForm';

class NotificationParams extends Component {
  state = {
    fileList: [],
    isPlay: false,
    decodedSoundId: null
  };
  audioCtx = new (window.AudioContext || window.webkitAudioContext)();
  audioSource = null;
  audioBuffer = null;

  handleSubmit = e => {
    e.preventDefault();
    const { stateCategory, currentSoundNotification } = this.props;
    if (stateCategory) {
      if (currentSoundNotification) {
        this.updateSoundNotification();
      } else {
        this.addSoundNotification();
      }
    }
  };

  addSoundNotification = () => {
    const { formValues, dispatch } = this.props;
    dispatch(addSoundNotification({ ...formValues }));
  };

  updateSoundNotification = () => {
    const { formValues, currentSoundNotification, dispatch } = this.props;
    const updatedSoundNotification = { ...currentSoundNotification, ...formValues };
    dispatch(updateSoundNotification(updatedSoundNotification));
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.stateCategory) {
      const { stateCategory, dispatch, currentSoundNotification, formValues } = this.props;
      if (
        (stateCategory && stateCategory.id !== nextProps.stateCategory.id) ||
        (!stateCategory && nextProps.stateCategory) ||
        (!currentSoundNotification && nextProps.currentSoundNotification)
      ) {
        dispatch(reset(FORM_NAME));
        this.playStopSound(true);
      }
      if (
        (!formValues.mediaId && nextProps.formValues.mediaId) ||
        formValues.mediaId !== nextProps.formValues.mediaId
      ) {
        this.playStopSound(true);
        if (nextProps.soundsHash[nextProps.formValues.mediaId])
          this.audioCtx.decodeAudioData(
            base64ToBuffer(nextProps.soundsHash[nextProps.formValues.mediaId].content),
            buffer => {
              this.audioBuffer = buffer;
            },
            null
          );
      }
    }
  };

  playStopSound = isPlay => {
    if (isPlay) {
      if (this.audioSource) {
        this.audioSource.stop(0);
        this.audioSource.removeEventListener('ended', () => {
          this.playStopSound(true);
        });
      }
    } else {
      const {
        formValues: { unstoppable }
      } = this.props;
      this.audioSource = this.audioCtx.createBufferSource();
      this.audioSource.buffer = this.audioBuffer;
      this.audioSource.loop = unstoppable ? true : false;
      this.audioSource.connect(this.audioCtx.destination);
      this.audioSource.start(0);
      this.audioSource.addEventListener('ended', () => {
        this.playStopSound(true);
      });
    }
    this.setState({ isPlay: !isPlay });
  };

  componentWillUnmount = () => {
    this.playStopSound(true);
  };

  render() {
    const {
      stateCategory,
      pristine,
      valid,
      soundsList,
      formValues: { mediaId }
    } = this.props;
    const { isPlay } = this.state;

    return (
      <WidgetWrapper>
        <WidgetParamsEditorMenu
          isViewTitle={true}
          title={stateCategory ? stateCategory.name : 'Выберите оповещение'}
          isSaveActive={stateCategory && !pristine && valid ? true : false}
          onSaveAction={this.handleSubmit}
        />
        {stateCategory ? (
          <form>
            <Row>
              <FormWrapper>
                <FormItem label="Звук оповещения">
                  <Field name="mediaId" component={Select}>
                    <Option value="">Без звука</Option>
                    {soundsList.map(sound => (
                      <Option key={sound.id} value={sound.id}>
                        {sound.name}
                      </Option>
                    ))}
                  </Field>
                </FormItem>
                <FormItem label="Воспроизведение">
                  <Field
                    name="unstoppable"
                    component={Checkbox}
                    label="Воспроизводить непрерывно"
                  />
                </FormItem>
                <FormItem>
                  <Button
                    type="primary"
                    icon={isPlay ? 'pause' : 'caret-right'}
                    disabled={!mediaId}
                    onClick={() => this.playStopSound(isPlay)}
                  >
                    {isPlay ? 'Остановить' : 'Прослушать'}
                  </Button>
                </FormItem>
              </FormWrapper>
            </Row>
          </form>
        ) : null}
      </WidgetWrapper>
    );
  }
}

NotificationParams = reduxForm({ form: FORM_NAME, enableReinitialize: true })(NotificationParams);

const mapStateToProps = state => {
  const selectedSoundNotificationId = state.widgets.selectedSoundNotificationId;
  const currentSoundNotification = state.soundNotifications[selectedSoundNotificationId];
  return {
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {},
    stateCategory: state.stateCategoryViewsHash[selectedSoundNotificationId],
    currentSoundNotification,
    soundsHash: state.medias.sounds,
    soundsList: Object.values(state.medias.sounds),
    initialValues: selectedSoundNotificationId
      ? currentSoundNotification || {
          stateCategoryId: selectedSoundNotificationId,
          unstoppable: true
        }
      : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(NotificationParams);
