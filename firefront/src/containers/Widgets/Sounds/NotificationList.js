import React, { Component } from 'react';
import { connect } from 'react-redux';

import Table, { Column } from 'components/Table';
import Widget from 'components/Widget';

import { selectSoundNotification } from 'actions/soundNotifications';

class SoundList extends Component {
  state = {
    height: 500
  };

  getRowClassName = record => {
    const { selectedSoundNotificationId } = this.props;
    if (selectedSoundNotificationId) {
      if (record.id === selectedSoundNotificationId) return 'selected-row';
    }
    return '';
  };

  onRowClick = record => {
    const { dispatch } = this.props;
    dispatch(selectSoundNotification(record.id));
  };

  renderSoundCell = (text, record) => {
    const { soundNotifications, sounds } = this.props;
    return soundNotifications[record.id] &&
      soundNotifications[record.id].mediaId &&
      sounds[soundNotifications[record.id].mediaId]
      ? sounds[soundNotifications[record.id].mediaId].name
      : null;
  };

  renderLoopCell = (text, record) => {
    const { soundNotifications } = this.props;
    if (soundNotifications[record.id]) {
      return soundNotifications[record.id].unstoppable ? 'Да' : 'Нет';
    } else return 'Нет';
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(selectSoundNotification(null));
  };

  render = () => {
    const { stateCategories } = this.props;
    const { height } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <Table
          dataSource={stateCategories}
          rowKey="id"
          scroll={{ y: height - 61 }}
          onRowClick={this.onRowClick}
          rowClassName={this.getRowClassName}
        >
          <Column title="Состояние системы" dataIndex="name" />
          <Column title="Звук" dataIndex="sound" render={this.renderSoundCell} />
          <Column title="Непрерывно" dataIndex="loop" render={this.renderLoopCell} />
        </Table>
      </Widget>
    );
  };
}

const mapStateToProps = state => {
  return {
    sounds: state.medias.sounds,
    stateCategories: Object.values(state.stateCategoryViewsHash),
    soundNotifications: state.soundNotifications,
    selectedSoundNotificationId: state.widgets.selectedSoundNotificationId
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SoundList);
