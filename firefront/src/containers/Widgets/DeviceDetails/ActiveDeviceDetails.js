import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Tabs, Tooltip, Icon } from 'antd';
import PropTypes from 'prop-types';

import List, { ListItem } from 'components/Info/List';
import Table, { Column } from 'components/Table';
import Title from 'components/Info/Title';
import TextCell from 'components/Table/TextCell';

import ActiveDeviceDetailsMenu from 'containers/Menus/ActiveDeviceDetailsMenu';
import DeviceNoteForm from 'containers/Forms/DeviceNoteForm';
import DeviceActionsForm from 'containers/Forms/DeviceActionsForm';

import { addDeviceNote, performDeviceAction } from 'actions/devices';

import { getActiveDeviceList, getActiveProjectId, getActiveScenarios } from 'helpers/activeProject';
import { getCurrentDevice } from 'helpers/widgets';
import { getDateTimestamp } from 'helpers/time';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const TabPane = Tabs.TabPane,
  Wrapper = styled.div`
    height: 300px;
    background-color: #fff;
    margin-top: 25px;
    & .ant-tabs-nav-container {
      width: 36px;
    }
    & .ant-tabs-tab {
      padding: 5px !important;
      width: 24px;
    }
    & .ant-tabs-content {
      padding: 0 !important;
    }
    & .ant-table-row {
      height: 30px;
    }
    & .hidden-expand {
      & .ant-table-row-expand-icon,
      .indent-level-1 {
        display: none;
      }
    }
  `,
  TablesWrapper = styled.div`
    display: flex;
    flex: 1;
  `,
  Content = styled.div`
    padding: 0 8px 0;
    height: 233px;
    overflow: auto;
  `,
  ICON_STYLES = { fontSize: '24px' },
  LIST_STYLES = { padding: '8px 8px 0 8px' },
  DEVICE_CATEGORIES = {
    SENSOR: 'Датчик',
    EXECUTIVE: 'Исполнительное устройство',
    CONTROL: 'Управляющее устройство',
    TRANSPORT: 'Транспортное устройство',
    VIRTUAL_CONTAINER: 'Составное устройство'
  },
  DEVICE_SUBSYSTEMS = {
    GENERAL: 'общего назначения',
    FIRE: 'пожарного назначения',
    SECURITY: 'охранного назначения',
    SKUD: '',
    AUTO: 'автоматика'
  },
  DEVICE_CATEGORIES_HAS_CONTROL = {
    EXECUTIVE: true,
    VIRTUAL_CONTAINER: true,
    SENSOR: true
  },
  EXECUTIVE_DEVICE_CATEGORIES = {
    EXECUTIVE: true,
    VIRTUAL_CONTAINER: true
  };

class ProjectDeviceDetails extends Component {
  static propTypes = {
    currentDevice: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.panes = [
      {
        name: 'Основные',
        key: 'common',
        icon: 'bars',
        content: this.renderCommonPane
      },
      {
        name: 'Состояние',
        key: 'state',
        icon: 'exception',
        content: this.renderStatePane
      },
      {
        name: 'Параметры',
        key: 'config',
        icon: 'tool',
        content: this.renderConfigPane
      },
      {
        name: 'Блокнот',
        key: 'note',
        icon: 'file-text',
        content: this.renderNotePane
      },
      {
        name: 'Управление',
        key: 'control',
        icon: 'setting',
        content: this.renderControlPane
      }
    ];
    this.state = {
      activeTab: 'state',
      disabledExecution: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    const { currentDevice } = this.props;
    let { activeTab } = this.state;
    if (
      currentDevice.id !== nextProps.currentDevice.id ||
      currentDevice.statePolling !== nextProps.currentDevice.statePolling
    ) {
      const controlAllowedProperty =
        nextProps.currentDevice &&
        nextProps.currentDevice.statePolling &&
        nextProps.currentDevice.aggregatedPropertyViews &&
        DEVICE_CATEGORIES_HAS_CONTROL[nextProps.currentDevice.deviceCategory]
          ? nextProps.currentDevice.aggregatedPropertyViews.find(
              property => property.id === 'ManageAllowed'
            )
          : null;
      const controlAllowed = controlAllowedProperty
        ? controlAllowedProperty.projectValue === 'true'
        : false;
      if (activeTab === 'control' && !controlAllowed) activeTab = 'state';
      this.setState({ activeTab, controlAllowed });
    }
  }

  getDeviceParents(childDeviceId, children) {
    const { devices } = this.props;
    const childDevice = devices.find(device => device.id === childDeviceId);
    const parentDevice = childDevice.parentDeviceId
      ? devices.find(device => device.id === childDevice.parentDeviceId)
      : null;

    return `${
      parentDevice
        ? `${this.getDeviceParents(
            parentDevice.id,
            `${parentDevice.name} ${children ? `\\ ${children}` : ''}`
          )}`
        : children || ''
    }`;
  }

  updateActiveTab = activeTab => {
    this.setState({ activeTab });
  };

  onAddDeviceNote = ({ message }) => {
    const { dispatch, currentDevice, activeProjectId } = this.props;
    dispatch(addDeviceNote(activeProjectId, currentDevice.id, message));
  };

  onPerformDeviceAction = (actionId, actionParameters) => {
    const { dispatch, currentDevice, activeProjectId } = this.props;
    this.setState({ disabledExecution: { [currentDevice.id]: true } }, () =>
      setTimeout(() => this.setState({ disabledExecution: { [currentDevice.id]: false } }), 5000)
    );
    dispatch(performDeviceAction(activeProjectId, currentDevice.id, actionId, actionParameters));
  };

  renderCommonPane = () => {
    const { currentDevice, scenarios } = this.props;
    const { id, deviceCategory, subsystem, parentDeviceId } = currentDevice;

    return (
      <Content>
        <Title>Тип устройства:</Title>
        <List>
          <ListItem
            name={`
                ${DEVICE_CATEGORIES[deviceCategory] || 'Неизвестный тип устройства'},
                ${DEVICE_SUBSYSTEMS[subsystem] || 'Неизвестная подсистема'}
              `}
          />
        </List>
        {parentDeviceId ? (
          <div>
            <Title>Подключено к:</Title>
            <List>
              <ListItem name={this.getDeviceParents(id)} />
            </List>
          </div>
        ) : null}
        {EXECUTIVE_DEVICE_CATEGORIES[deviceCategory] ? (
          <div>
            <Title>Условия работы:</Title>
            <List>
              {scenarios.map(scenario =>
                scenario.deviceIds.includes(id) ? (
                  <ListItem key={scenario.id} name={`${scenario.name} (${scenario.globalNo})`} />
                ) : null
              )}
            </List>
          </div>
        ) : null}
        {currentDevice.attachableToRegion ? (
          <div>
            <Title>Зона:</Title>
            <List>
              <ListItem
                key="region"
                name={currentDevice.regionId ? currentDevice.regionName : 'Зона не указана'}
              />
            </List>
          </div>
        ) : null}
      </Content>
    );
  };

  renderStatePane = () => {
    const { currentDevice } = this.props;
    const { activeStateViews, generalStateCategoryView, monitorableValueViews } = currentDevice;

    return (
      <Content>
        <Title>Главный статус</Title>
        <List>
          <ListItem
            img={`status/${generalStateCategoryView.iconName}`}
            name={generalStateCategoryView.name}
          />
        </List>
        <Title>Дополнительные статусы</Title>
        <List>
          {activeStateViews.map((status, index) => {
            return <ListItem img={`status/${status.iconName}`} name={status.name} key={index} />;
          })}
        </List>
        {monitorableValueViews.length === 0 ? null : (
          <div>
            <Title>Наблюдаемые параметры</Title>
            <List>
              {monitorableValueViews.map((value, index) => {
                return (
                  <ListItem name={`${value.name} : ${value.value} ${value.unit}`} key={index} />
                );
              })}
            </List>
          </div>
        )}
      </Content>
    );
  };

  renderConfigPane = () => {
    const { currentDevice } = this.props;
    const { configPropertyViews } = currentDevice;
    return (
      <TablesWrapper>
        <Table
          dataSource={configPropertyViews}
          scroll={{ y: 206 }}
          rowClassName={record => (record.type !== 'section' ? 'hidden-expand' : null)}
          rowKey={record => record.id}
        >
          <Column
            title="Параметр"
            dataIndex="name"
            render={(text, record) => {
              if (record.type === 'section')
                return {
                  children: <TextCell text={text} />,
                  props: { colSpan: 3 }
                };
              else return <TextCell text={text} />;
            }}
          />
          <Column
            title="В системе"
            dataIndex="projectValue"
            render={(text, record) => {
              if (record.type === 'section') {
                return {
                  children: text,
                  props: { colSpan: 0 }
                };
              } else if (record.type === 'date') {
                const seconds = text && text !== '' ? Number(text) : null;
                const date = seconds ? getDateTimestamp(seconds) : text;
                return <span>{date}</span>;
              } else if (record.possibleValues) {
                return <div>{record.possibleValues[text]}</div>;
              } else if (record.type === 'integer') {
                return <TextCell text={text} />;
              }
            }}
          />
          <Column
            title="В устройстве"
            dataIndex="activeValue"
            render={(text, record) => {
              if (record.type === 'section')
                return {
                  children: text,
                  props: { colSpan: 0 }
                };
              else if (record.type === 'date') {
                const seconds = text && text !== '' ? Number(text) : null;
                const date = seconds ? getDateTimestamp(seconds) : text;
                return <span>{date}</span>;
              } else
                return (
                  <TextCell text={record.possibleValues ? record.possibleValues[text] : text} />
                );
            }}
          />
        </Table>
      </TablesWrapper>
    );
  };

  renderNotePane = () => {
    const { currentDevice } = this.props;
    const { notes } = currentDevice;
    return (
      <div>
        <DeviceNoteForm onSubmit={this.onAddDeviceNote} />
        <Content style={{ height: 205 }}>
          {notes.map((note, index) => (
            <div key={index}>
              <Title>{`${note.userName} (${getDateTimestamp(note.createDateTime)})`}</Title>
              <List>
                <ListItem name={note.message} />
              </List>
            </div>
          ))}
        </Content>
      </div>
    );
  };

  renderControlPane = () => {
    const { currentDevice, license, deviceProfileViewsHash } = this.props;
    const { disabledExecution } = this.state;
    let licenseControlAllowed = false;
    const deviceProfile = deviceProfileViewsHash[currentDevice.deviceProfileId]
      ? deviceProfileViewsHash[currentDevice.deviceProfileId].deviceProfile
      : null;
    if (deviceProfile) {
      if (deviceProfile.firefightingDevice) {
        if (license && license.control && license.control.firefightingEnabled) {
          licenseControlAllowed = true;
        }
      } else {
        if (license && license.control && license.control.engineeringEnabled) {
          licenseControlAllowed = true;
        }
      }
    }
    return (
      <Content>
        {licenseControlAllowed ? (
          <DeviceActionsForm
            actions={currentDevice.aggregatedSupportedActions}
            disabledExecution={disabledExecution[currentDevice.id]}
            onSubmit={this.onPerformDeviceAction}
          />
        ) : null}
      </Content>
    );
  };

  render() {
    const { activeTab, controlAllowed } = this.state;
    const { currentDevice } = this.props;

    return (
      <Wrapper
        id="ACTIVE_DEVICE_DETAILS"
        onScroll={() => {
          const focusedSelector = document
            .getElementById('ACTIVE_DEVICE_DETAILS')
            .querySelector(':focus');
          if (focusedSelector) focusedSelector.blur();
        }}
      >
        <ActiveDeviceDetailsMenu
          name={
            currentDevice && currentDevice.id
              ? currentDevice.name + ' (' + currentDevice.fullAddressPath + ')'
              : 'Не выбрано'
          }
        />
        {currentDevice.id && !currentDevice.isRoot ? (
          <Tabs
            tabPosition="left"
            size="small"
            activeKey={activeTab}
            onTabClick={this.updateActiveTab}
            defaultActiveKey={activeTab}
          >
            {this.panes.map(pane => (
              <TabPane
                tab={
                  <Tooltip placement="rightTop" title={pane.name}>
                    <Icon style={ICON_STYLES} type={pane.icon} />
                  </Tooltip>
                }
                key={pane.key}
                disabled={pane.key === 'control' ? !controlAllowed : false}
              >
                {pane.content
                  ? [
                      <List style={LIST_STYLES} key="deviceIcon">
                        <ListItem
                          img={`device/${currentDevice.iconMedia.path}`}
                          name={currentDevice.name}
                        />
                      </List>,
                      <pane.content key={pane.key} />
                    ]
                  : null}
              </TabPane>
            ))}
          </Tabs>
        ) : null}
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  activeProjectId: getActiveProjectId(state),
  currentDevice: getCurrentDevice(state),
  devices: getActiveDeviceList(state),
  scenarios: getActiveScenarios(state),
  deviceProfileViewsHash: getDeviceProfileViewsHash(state),
  license: state.license
});

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDeviceDetails);
