import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Tabs, Tooltip, Icon, Select, Input, InputNumber } from 'antd';
import PropTypes from 'prop-types';

import ProjectDeviceDetailsMenu from 'containers/Menus/ProjectDeviceDetailsMenu';
import List, { ListItem } from 'components/Info/List';
import Table, { Column } from 'components/Table';
import Checkbox from 'components/Checkbox';
import DeviceIcon from 'components/Device/DeviceIcon';
import Title from 'components/Info/Title';
import TextCell from 'components/Table/TextCell';
import RegionsSelect from 'containers/Selects/RegionsSelect';

import {
  updateCurrentDevice,
  setDeviceConfig,
  setActiveDeviceConfig,
  setDeviceProperties,
  uploadDeviceConfig,
  downloadDeviceConfig,
  setDeviceShapeLibrary
} from 'actions/devices';

import {
  checkCurrentProjectIsActive,
  getCurrentProjectDeviceList,
  getCurrentProjectPlans,
  getCurrentProjectRegions,
  getCurrentProjectRegionsHash
} from 'helpers/currentProject';
import { getDeviceProfile } from 'helpers/device';
import { getCurrentDevice } from 'helpers/widgets';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { getDateTimestamp } from 'helpers/time';

const TabPane = Tabs.TabPane;
const Option = Select.Option;
const Wrapper = styled.div`
  height: 276px;
  background-color: #fff;
  margin-top: 25px;
  & .ant-tabs-nav-container {
    width: 36px;
  }
  & .ant-tabs-tab {
    padding: 5px !important;
    width: 24px;
  }
  & .ant-tabs-content {
    padding: 0 !important;
  }
  & .ant-select {
    width: 100%;
  }
  & .ant-input-number {
    width: 100%;
  }
  & .ant-table-row {
    height: 30px;
  }
  & .hidden-expand {
    & .ant-table-row-expand-icon,
    .indent-level-1 {
      display: none;
    }
  }
`;
const ConfigWrapper = styled.div`
  flex: 1;
`;
const TablesWrapper = styled.div`
  display: flex;
  flex: 1;
`;
const Content = styled.div`
  padding: 8px;
  height: 244px;
  overflow: auto;
`;

const writableDeviceCategories = ['SENSOR', 'EXECUTIVE'],
  unreadableDeviceCategories = ['CONTAINER', 'TRANSPORT'];
const ATTACHABLE_DEVICE_CATEGORY = {
  SENSOR: true,
  CONTROL: true,
  EXECUTIVE: true,
  VIRTUAL_CONTAINER: true
};

class ProjectDeviceDetails extends Component {
  static propTypes = {
    currentDevice: PropTypes.object,
    devices: PropTypes.array,
    plans: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.panes = [
      {
        name: 'Общее',
        key: 'common',
        icon: 'bars',
        content: () => this.renderCommonPane()
      },
      {
        name: 'Прочие',
        key: 'properties',
        icon: 'appstore-o',
        content: () => this.renderPropertiesPane()
      },
      {
        name: 'Конфигурирование',
        key: 'config',
        icon: 'tool',
        content: () => this.renderConfigPane()
      }
    ];
    this.state = {
      activeTab: 'common',
      deviceProperties: {
        id: '',
        properties: []
      },
      deviceConfig: { id: '', properties: [] },
      isConfigChanged: false,
      isPropertiesChanged: false,
      isShapeLibraryChanged: false,
      configSaving: true,
      propertiesSaving: true,
      shapeLibraryId: ''
    };
  }

  componentWillReceiveProps({
    currentDevice,
    setDevicePropertiesError,
    setDeviceConfigError,
    deviceProfileViewsHash
  }) {
    if (!currentDevice.isRoot) {
      const { configSaving, propertiesSaving } = this.state;
      let {
        deviceProperties,
        deviceConfig,
        isPropertiesChanged,
        isConfigChanged,
        shapeLibraryId,
        isShapeLibraryChanged
      } = this.state;

      if (deviceConfig.id && deviceConfig.id === currentDevice.id) {
        deviceConfig.properties = currentDevice.configPropertyViews.map((prop, index) => ({
          ...deviceConfig.properties[index],
          ...prop,
          projectValue:
            configSaving && !setDeviceConfigError
              ? prop.projectValue
              : deviceConfig.properties[index].projectValue
        }));
        isConfigChanged = !configSaving || setDeviceConfigError ? true : false;
      } else {
        deviceConfig = {
          id: currentDevice.id,
          properties: currentDevice.configPropertyViews
            ? currentDevice.configPropertyViews.map(prop => ({ ...prop }))
            : []
        };
        isConfigChanged = false;
      }

      if (deviceProperties.id && deviceProperties.id === currentDevice.id) {
        deviceProperties.properties = currentDevice.aggregatedPropertyViews.map((prop, index) => ({
          ...deviceProperties.properties[index],
          ...prop,
          projectValue:
            propertiesSaving && !setDevicePropertiesError
              ? prop.projectValue
              : deviceProperties.properties[index].projectValue
        }));
        isPropertiesChanged = !propertiesSaving || setDevicePropertiesError ? true : false;
      } else {
        deviceProperties = {
          id: currentDevice.id,
          properties: currentDevice.aggregatedPropertyViews
            ? currentDevice.aggregatedPropertyViews.map(prop => ({ ...prop }))
            : []
        };
        isPropertiesChanged = false;
      }
      // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
      if (
        (!this.props.currentDevice && currentDevice) ||
        (this.props.currentDevice &&
          currentDevice &&
          currentDevice.id !== this.props.currentDevice.id) ||
        (this.props.currentDevice &&
          currentDevice &&
          currentDevice.deviceShapeLibraryId !== this.props.currentDevice.deviceShapeLibraryId) ||
        isShapeLibraryChanged
      ) {
        shapeLibraryId = currentDevice.deviceShapeLibraryId;
        isShapeLibraryChanged = false;
      }
      const deviceProfile = getDeviceProfile(deviceProfileViewsHash, currentDevice);

      this.setState({
        deviceProfile,
        deviceConfig,
        shapeLibraryId,
        deviceProperties,
        isConfigChanged,
        isShapeLibraryChanged,
        isPropertiesChanged
      });
    }
  }

  renderCommonPane() {
    const { currentDevice, plans } = this.props;
    const { id, name, deviceProfileId, iconMedia, parentDeviceId, planLayouts } = currentDevice;

    return (
      <Content>
        <List>
          <ListItem img={id ? `device/${iconMedia.path.slice(0, -4)}.ico` : null} name={name} />
        </List>
        {parentDeviceId ? (
          <span>
            <Title>Подключено к:</Title>
            <List>
              <ListItem name={this.getDeviceParents(id)} />
            </List>
          </span>
        ) : null}
        {deviceProfileId && ATTACHABLE_DEVICE_CATEGORY[currentDevice.deviceCategory] ? (
          <span>
            <Title>Расположено:</Title>
            <List>
              {planLayouts && planLayouts.length ? (
                planLayouts.map((layout, index) => (
                  <ListItem
                    key={index}
                    name={plans.find(plan => plan.id === layout.planId)['name']}
                  />
                ))
              ) : (
                <ListItem name={'Отсутствует на плане'} />
              )}
            </List>
          </span>
        ) : null}
        {currentDevice.attachableToRegion ? (
          <span>
            <Title>Зона:</Title>
            <List>
              <ListItem
                key="region"
                name={currentDevice.regionId ? currentDevice.regionName : 'Зона не указана'}
              />
            </List>
          </span>
        ) : null}
      </Content>
    );
  }

  renderPropertiesPane() {
    const { isActiveProject, iconsHash, icons, currentDevice, deviceShapeLibraries } = this.props;
    const { deviceProperties, shapeLibraryId } = this.state;
    const { deviceShapeLibraryId, deviceProfileId } = currentDevice;
    const propertiesData = deviceProperties;
    let hasShapeLibrary = false;
    if (Object.keys(deviceShapeLibraries).length) {
      hasShapeLibrary = true;
    }

    return (
      <ConfigWrapper>
        <TablesWrapper>
          {propertiesData.properties.length ? (
            <Table
              dataSource={
                hasShapeLibrary
                  ? [
                      // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
                      ...propertiesData.properties,
                      {
                        id: 'deviceShapeLibraryId',
                        name: 'Библиотека шейпов',
                        defaultValue: `${deviceProfileId}_default_shlib`,
                        possibleValues: deviceShapeLibraries,
                        projectValue:
                          shapeLibraryId ||
                          deviceShapeLibraryId ||
                          `${deviceProfileId}_default_shlib`
                      }
                    ]
                  : propertiesData.properties
              }
              scroll={{ y: 243 }}
              rowKey={record => record.id}
              showHeader={false}
            >
              <Column title="Параметр" dataIndex="name" />
              <Column
                title="В системе"
                dataIndex="projectValue"
                render={(text, record, index) => {
                  if (record.possibleValues) {
                    if (isActiveProject) {
                      return <span>{record.possibleValues[text]}</span>;
                    } else {
                      const options = [];
                      const keys = Object.keys(record.possibleValues);
                      keys.forEach(key => {
                        options.push(
                          <Option value={key} key={key}>
                            {record.possibleValues[key]}
                          </Option>
                        );
                      });
                      return (
                        <Select
                          value={text}
                          onSelect={value => this.onPropertyChange(value, index)}
                        >
                          {options}
                        </Select>
                      );
                    }
                  } else if (record.type === 'integer') {
                    const min = parseInt(record.min, 10) || 0;
                    const max = parseInt(record.max, 10);
                    return isActiveProject || record.readOnly ? (
                      <span>{text}</span>
                    ) : (
                      <InputNumber
                        precision={0}
                        value={parseInt(text, 10)}
                        min={min}
                        max={max}
                        onChange={value => this.onPropertyChange(value || min, index)}
                      />
                    );
                  } else {
                    switch (record.type) {
                      case 'icon': {
                        if (isActiveProject) {
                          return (
                            <DeviceIcon name={iconsHash[text].path}>
                              {iconsHash[text].name}
                            </DeviceIcon>
                          );
                        } else {
                          const options = [];
                          icons.forEach(icon =>
                            options.push(
                              <Option value={icon.id} key={icon.id} title={icon.name}>
                                <DeviceIcon name={icon.path}>{icon.name}</DeviceIcon>
                              </Option>
                            )
                          );
                          return (
                            <Select
                              notFoundContent={'Не найдено'}
                              showSearch={true}
                              value={text}
                              onSelect={value => this.onPropertyChange(value, index)}
                              optionFilterProp={'title'}
                              filterOption={(input, option) =>
                                option.props.title.toLowerCase().indexOf(input.toLowerCase()) >=
                                  0 ||
                                option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
                              }
                            >
                              {options}
                            </Select>
                          );
                        }
                      }

                      case 'text': {
                        return isActiveProject || record.readOnly ? (
                          <span>{text}</span>
                        ) : (
                          <Input
                            value={text}
                            onChange={event => this.onPropertyChange(event.target.value, index)}
                          />
                        );
                      }

                      case 'flag': {
                        return (
                          <Checkbox
                            value={text}
                            isActive={!isActiveProject}
                            onChange={event => this.onPropertyChange(event.target.checked, index)}
                          />
                        );
                      }

                      case 'entityId': {
                        switch (record.entityType) {
                          case 'REGION': {
                            const { regions } = this.props;
                            return (
                              <RegionsSelect
                                value={text}
                                regions={[
                                  { id: null, name: 'Не назначено' },
                                  ...(record.entitySubsystem
                                    ? regions.filter(
                                        region => region.subsystem === record.entitySubsystem
                                      )
                                    : regions)
                                ]}
                                onSelect={value => this.onPropertyChange(value, index)}
                              />
                            );
                          }
                          default: {
                            /* TODO: реализовать для других типов сущностей, если понадобится */
                            return <span>{text}</span>;
                          }
                        }
                      }

                      default: {
                        return <span>{text}</span>;
                      }
                    }
                  }
                }}
              />
            </Table>
          ) : null}
        </TablesWrapper>
      </ConfigWrapper>
    );
  }

  getDeviceParents(childDeviceId, children) {
    const { devices } = this.props;
    const childDevice = devices.find(device => device.id === childDeviceId);
    const parentDevice = childDevice.parentDeviceId
      ? devices.find(device => device.id === childDevice.parentDeviceId)
      : null;

    return `${
      parentDevice
        ? `${this.getDeviceParents(
            parentDevice.id,
            `${parentDevice.name} ${children ? `\\ ${children}` : ''}`
          )}`
        : children || ''
    }`;
  }

  renderConfigPane() {
    const { isActiveProject } = this.props;
    const { deviceConfig } = this.state;
    const deviceConfigView = { ...deviceConfig };
    const sections = {};
    deviceConfigView.properties = deviceConfigView.properties.filter((prop, index) => {
      prop.key = index;
      if (prop.type === 'section') {
        prop['children'] = [];
        sections[prop.id] = prop;
        return true;
      }
      if (prop.section) {
        sections[prop.section].children.push(prop);
        return false;
      }
      return true;
    });
    return (
      <ConfigWrapper>
        <TablesWrapper>
          <Table
            dataSource={deviceConfigView.properties}
            scroll={{ y: 217 }}
            rowClassName={record => (record.type !== 'section' ? 'hidden-expand' : null)}
            rowKey={record => record.id}
          >
            <Column
              title="Параметр"
              dataIndex="name"
              render={(text, record) => {
                if (record.type === 'section')
                  return {
                    children: <TextCell text={text} />,
                    props: { colSpan: isActiveProject ? 3 : 2 }
                  };
                else return <TextCell text={text} />;
              }}
            />
            <Column
              title="В системе"
              dataIndex="projectValue"
              render={(text, record) => {
                if (record.type === 'section') {
                  return {
                    children: text,
                    props: { colSpan: 0 }
                  };
                } else if (record.possibleValues) {
                  const options = [];
                  const keys = Object.keys(record.possibleValues);
                  keys.forEach(key => {
                    options.push(
                      <Option value={key} key={key + record.key}>
                        {record.possibleValues[key]}
                      </Option>
                    );
                  });
                  return (
                    <Select
                      value={text}
                      onSelect={value => this.onConfigPropertyChange(value, record.key)}
                    >
                      {options}
                    </Select>
                  );
                } else if (record.type === 'integer') {
                  const min = parseInt(record.min, 10) || 0;
                  const max = parseInt(record.max, 10);
                  return (
                    <InputNumber
                      precision={0}
                      value={parseInt(text, 10)}
                      min={min}
                      max={max}
                      onChange={value => this.onConfigPropertyChange(value || min, record.key)}
                    />
                  );
                } else if (record.type === 'float') {
                  const min = parseFloat(record.min, 10);
                  const max = parseFloat(record.max, 10);
                  return (
                    <InputNumber
                      value={parseFloat(text, 10)}
                      min={min}
                      precision={2}
                      max={max}
                      step={0.01}
                      onChange={value => this.onConfigPropertyChange(value || min, record.key)}
                    />
                  );
                } else if (record.type === 'text' && !record.readOnly) {
                  return (
                    <Input
                      value={text}
                      onChange={event =>
                        this.onConfigPropertyChange(event.target.value, record.key)
                      }
                    />
                  );
                } else if (record.type === 'date') {
                  const seconds = text && text !== '' ? Number(text) : null;
                  const date = seconds ? getDateTimestamp(seconds) : text;
                  return <span>{date}</span>;
                } else return <TextCell text={text} />;
              }}
            />
            {isActiveProject ? (
              <Column
                title="В устройстве"
                dataIndex="activeValue"
                render={(text, record) => {
                  if (record.type === 'section')
                    return {
                      children: text,
                      props: { colSpan: 0 }
                    };
                  else if (record.type === 'date') {
                    const seconds = text && text !== '' ? Number(text) : null;
                    const date = seconds ? getDateTimestamp(seconds) : text;
                    return <span>{date}</span>;
                  } else
                    return (
                      <TextCell text={record.possibleValues ? record.possibleValues[text] : text} />
                    );
                }}
              />
            ) : null}
          </Table>
        </TablesWrapper>
      </ConfigWrapper>
    );
  }

  onPropertyChange(value, index) {
    const { deviceProperties } = this.state;
    if (deviceProperties.properties[index]) {
      deviceProperties.properties[index].projectValue = value;
      deviceProperties.properties[index].isChanged = true;

      this.setState({
        deviceProperties,
        isPropertiesChanged: true,
        propertiesSaving: false
      });
    }
    // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
    else {
      this.setState({ shapeLibraryId: value, isShapeLibraryChanged: true });
    }
  }

  onConfigPropertyChange(value, index) {
    const { deviceConfig } = this.state;

    deviceConfig.properties[index].projectValue = value;
    deviceConfig.properties[index].isChanged = true;

    this.setState({
      deviceConfig,
      isConfigChanged: true,
      configSaving: false
    });
  }

  onDownloadClick() {
    const { deviceConfig } = this.state;
    const { dispatch, projectId, isActiveProject } = this.props;
    if (isActiveProject) dispatch(downloadDeviceConfig(projectId, deviceConfig.id));
  }

  onUploadClick() {
    const { deviceConfig } = this.state;
    const { dispatch, projectId, isActiveProject } = this.props;
    if (isActiveProject) dispatch(uploadDeviceConfig(projectId, deviceConfig.id));
  }

  onSyncClick() {
    const { deviceConfig } = this.state;
    const { dispatch, projectId, isActiveProject } = this.props;
    const changedProperties = {};

    deviceConfig.properties.forEach(property => {
      if (property.activeValue !== property.projectValue) {
        changedProperties[property.id] = property.activeValue;
      }
    });

    if (isActiveProject)
      dispatch(setActiveDeviceConfig(projectId, deviceConfig.id, changedProperties));
  }

  // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
  onSetDeviceShapeLibrary = deviceShapeLibraryId => {
    const {
      dispatch,
      projectId,
      currentDevice: { id: deviceId }
    } = this.props;
    dispatch(setDeviceShapeLibrary(projectId, deviceId, deviceShapeLibraryId));
  };

  onSetDefaultsClick() {
    const { deviceConfig, deviceProperties, activeTab, shapeLibraryId } = this.state;
    const { isActiveProject, currentDevice } = this.props;
    if (activeTab === 'config') {
      let isConfigChanged = false;
      deviceConfig.properties.forEach(property => {
        if (!property.readOnly && property.projectValue !== property.defaultValue) {
          property.projectValue = property.defaultValue;
          property.isChanged = true;
          isConfigChanged = true;
        }
      });

      if (isConfigChanged) {
        this.setState({
          deviceConfig,
          isConfigChanged
        });
      }
    } else if (activeTab === 'properties' && !isActiveProject) {
      // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
      if (
        (currentDevice.deviceShapeLibraryId &&
          currentDevice.deviceShapeLibraryId !==
            `${currentDevice.deviceProfileId}_default_shlib`) ||
        (shapeLibraryId !== '' &&
          shapeLibraryId !== `${currentDevice.deviceProfileId}_default_shlib`)
      ) {
        this.setState({
          shapeLibraryId: `${currentDevice.deviceProfileId}_default_shlib`,
          isShapeLibraryChanged: true
        });
      }

      let isPropertiesChanged = false;
      deviceProperties.properties.forEach(property => {
        if (!property.readOnly && property.projectValue !== property.defaultValue) {
          property.projectValue = property.defaultValue;
          property.isChanged = true;
          isPropertiesChanged = true;
        }
      });

      if (isPropertiesChanged) {
        this.setState({
          deviceProperties,
          isPropertiesChanged
        });
      }
    }
  }

  onSaveClick() {
    const {
      deviceConfig,
      deviceProperties,
      activeTab,
      isShapeLibraryChanged,
      isPropertiesChanged,
      shapeLibraryId
    } = this.state;
    const { dispatch, projectId, isActiveProject } = this.props;
    if (activeTab === 'config') {
      const changedProperties = {};
      deviceConfig.properties.forEach(property => {
        if (property.isChanged) {
          changedProperties[property.id] = property.projectValue;
        }
      });
      this.setState({ configSaving: true });
      if (isActiveProject)
        dispatch(setActiveDeviceConfig(projectId, deviceConfig.id, changedProperties));
      else dispatch(setDeviceConfig(projectId, deviceConfig.id, changedProperties));
    } else if (activeTab === 'properties' && !isActiveProject) {
      const changedProperties = {};
      deviceProperties.properties.forEach(property => {
        if (property.isChanged) {
          changedProperties[property.id] = property.projectValue;
        }
      });
      this.setState({ propertiesSaving: true });
      // TODO: Удалить, когда библиотека шейпов будет назначаться через параметры
      if (isShapeLibraryChanged) this.onSetDeviceShapeLibrary(shapeLibraryId);
      if (isPropertiesChanged)
        dispatch(setDeviceProperties(projectId, deviceProperties.id, changedProperties));
    }
  }

  updateActiveTab(activeTab) {
    this.setState({ activeTab });
  }

  componentWillUnmount() {
    this.props.dispatch(updateCurrentDevice({}));
  }

  render() {
    const {
      deviceConfig,
      deviceProperties,
      isConfigChanged,
      activeTab,
      isPropertiesChanged,
      deviceProfile,
      isShapeLibraryChanged
    } = this.state;
    const { isActiveProject, currentDevice } = this.props;
    if (currentDevice.id && !currentDevice.isRoot) {
      const propertiesWasRead = deviceConfig.properties.find(
        prop => prop.id === 'LastReadDateTime'
      );
      const configurableDevice =
        deviceConfig && deviceConfig.properties && deviceConfig.properties.length ? true : false;
      const hasDeviceProperties =
        deviceProperties && deviceProperties.properties && deviceProperties.properties.length
          ? true
          : false;

      let deviceCategory = 'UNKNOWN';
      if (deviceProfile) deviceCategory = deviceProfile.deviceCategory;
      const isEditableConfigProperties = isActiveProject && activeTab === 'config';
      const writableDeviceAddress = currentDevice.addressType === 'GENERIC';

      return (
        <Wrapper
          id="PROJECT_DEVICE_DETAILS"
          onScroll={() => {
            const focusedSelector = document
              .getElementById('PROJECT_DEVICE_DETAILS')
              .querySelector(':focus');
            if (focusedSelector) focusedSelector.blur();
          }}
        >
          <ProjectDeviceDetailsMenu
            name={`${currentDevice.name} (${currentDevice.fullAddressPath})`}
            activeTab={activeTab}
            isDownloadActive={
              configurableDevice &&
              isEditableConfigProperties &&
              !unreadableDeviceCategories.includes(deviceCategory) &&
              writableDeviceAddress
            }
            onDownloadClick={this.onDownloadClick.bind(this)}
            isUploadActive={
              configurableDevice &&
              !isConfigChanged &&
              isEditableConfigProperties &&
              writableDeviceCategories.includes(deviceCategory) &&
              writableDeviceAddress
            }
            isSyncActive={
              configurableDevice &&
              !isConfigChanged &&
              isEditableConfigProperties &&
              propertiesWasRead &&
              propertiesWasRead.activeValue &&
              !unreadableDeviceCategories.includes(deviceCategory)
            }
            onSyncClick={this.onSyncClick.bind(this)}
            onUploadClick={this.onUploadClick.bind(this)}
            isSetDefaultsActive={
              (configurableDevice && activeTab === 'config') ||
              (hasDeviceProperties && activeTab === 'properties' && !isActiveProject)
            }
            onSetDefaultsClick={this.onSetDefaultsClick.bind(this)}
            isSaveActive={
              (configurableDevice && isConfigChanged && activeTab === 'config') ||
              (hasDeviceProperties &&
                (isPropertiesChanged || isShapeLibraryChanged) &&
                activeTab === 'properties')
            }
            onSaveClick={this.onSaveClick.bind(this)}
          />
          <Tabs
            tabPosition="left"
            size="small"
            onTabClick={this.updateActiveTab.bind(this)}
            defaultActiveKey={activeTab}
          >
            {this.panes.map(pane => (
              <TabPane
                tab={
                  <Tooltip placement="rightTop" title={pane.name}>
                    <Icon style={{ fontSize: '24px' }} type={pane.icon} />
                  </Tooltip>
                }
                key={pane.key}
              >
                {pane.content ? pane.content() : null}
              </TabPane>
            ))}
          </Tabs>
        </Wrapper>
      );
    } else {
      return (
        <Wrapper>
          <ProjectDeviceDetailsMenu
            name="не выбрано"
            isSyncActive={false}
            isDownloadActive={false}
            isUploadActive={false}
            isSaveActive={false}
            isSetDefaultsActive={false}
          />
        </Wrapper>
      );
    }
  }
}

const mapStateToProps = state => {
  const currentDevice = getCurrentDevice(state);
  const deviceShapeLibraries = {};
  Object.keys(state.deviceShapeLibrary.deviceShapeLibrariesHash).forEach(deviceShapeLibraryId => {
    const deviceShapeLibrary =
      state.deviceShapeLibrary.deviceShapeLibrariesHash[deviceShapeLibraryId];
    if (deviceShapeLibrary.deviceProfileId === currentDevice.deviceProfileId) {
      deviceShapeLibraries[deviceShapeLibraryId] = deviceShapeLibrary.name;
    }
  });
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    projectId: state.currentProjectId,
    currentDevice,
    devices: getCurrentProjectDeviceList(state),
    regionsHash: getCurrentProjectRegionsHash(state),
    regions: getCurrentProjectRegions(state),
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    plans: getCurrentProjectPlans(state),
    deviceShapeLibraries,
    icons: state.medias.icons,
    iconsHash: state.medias.iconsHash,
    setDeviceConfigError: state.errors.setDeviceConfig,
    setDevicePropertiesError: state.errors.setDeviceProperties
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDeviceDetails);
