import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Icon } from 'antd';
import { AutoSizer } from 'react-virtualized';

import Table from 'components/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';

import {
  getActiveDeviceList,
  getActiveProject,
  getActiveRegions,
  getActiveScenarios,
  getActiveVirtualStates
} from 'helpers/activeProject';
import {
  getActiveEmployeesHash,
  getActiveAccessKeysHash,
  getActiveWorkSchedulesHash
} from 'helpers/skud';
import {
  getCurrentProjectDeviceList,
  getCurrentProjectRegions,
  getCurrentProjectScenarios,
  getCurrentProjectVirtualStates,
  getCurrentProject
} from 'helpers/currentProject';

class ProjectValidateMessages extends Component {
  static propTypes = {
    activeProject: PropTypes.object,
    activeDevices: PropTypes.array,
    activeRegions: PropTypes.array,
    activeScenarios: PropTypes.array,
    virtualStates: PropTypes.array,
    employeesHash: PropTypes.object,
    accessKeysHash: PropTypes.object,
    workSchedulesHash: PropTypes.object
  };
  columns = [
    {
      title: '',
      key: 'level',
      dataIndex: 'level',
      width: '3%',
      render: text => (
        <Icon
          type={text === 'WARNING' ? 'exclamation-circle-o' : 'close-circle-o'}
          style={{ color: text === 'WARNING' ? 'orange' : 'red' }}
        />
      )
    },
    {
      title: 'Объект',
      key: 'entity',
      width: '20%',
      render: (text, record) => <div> {this.getValidateMessageEntityDescr(record)} </div>
    },
    {
      title: 'Ошибка',
      key: 'description',
      dataIndex: 'description'
    }
  ];

  getValidateMessageEntityDescr(validateMessage) {
    const {
      devices,
      regions,
      scenarios,
      virtualStates,
      employeesHash,
      accessKeysHash,
      workSchedulesHash
    } = this.props;

    switch (validateMessage.entityType) {
      case 'DEVICE': {
        const device = devices.find(device => device.id === validateMessage.entityId);
        if (device) {
          return (
            <div>
              Устройство: <DeviceTypeTreeItem record={device} />
              {', ' + device.middleAddressPath}
            </div>
          );
        }
        break;
      }
      case 'REGION': {
        const region = regions.find(region => region.id === validateMessage.entityId);
        if (region) {
          return `Зона: "${region.name}" (№${region.index})`;
        }
        break;
      }
      case 'SCENARIO': {
        const scenario = scenarios.find(scenario => scenario.id === validateMessage.entityId);
        if (scenario) {
          return `Сценарий: "${scenario.name}" (№${scenario.globalNo})`;
        }
        break;
      }
      case 'VIRTUAL_STATE': {
        const virtualState = virtualStates.find(
          virtualState => virtualState.id === validateMessage.entityId
        );
        if (virtualState) {
          return `Виртуальное состояние: "${virtualState.name}" (№${virtualState.globalNo})`;
        }
        break;
      }
      case 'EMPLOYEE':
        if (employeesHash) {
          const employee = employeesHash[validateMessage.entityId];
          if (employee) {
            return `Сотрудник: ${employee.name}`;
          }
        }
        return 'Неизвестный сотрудник';

      case 'ACCESS_KEY':
        if (accessKeysHash) {
          const accessKey = accessKeysHash[validateMessage.entityId];
          if (accessKey) {
            return accessKey.keyType === 'CARD'
              ? `Карточка доступа: ${accessKey.keyValue}`
              : `Пароль: ${accessKey.keyValue}`;
          }
        }
        return 'Неизвестный ключ доступа';

      case 'WORK_SCHEDULE':
        if (workSchedulesHash) {
          const workSchedule = workSchedulesHash[validateMessage.entityId];
          if (workSchedule) {
            return `График работ: ${workSchedule.name}`;
          }
        }
        return 'Неизвестный график работ';

      default:
        break;
    }
    return validateMessage.entityId;
  }

  render() {
    const { activeProject, currentProject, isActive, isCurrentProjectImported } = this.props;

    return (
      <AutoSizer>
        {({ height, width }) => (
          <div style={{ height, width: width - 2, background: 'white' }}>
            <Table
              dataSource={
                isCurrentProjectImported
                  ? currentProject && currentProject.importValidateMessages
                    ? currentProject.importValidateMessages.sort((a, b) =>
                        a.level.localeCompare(b.level)
                      )
                    : []
                  : activeProject && (activeProject.id === currentProject.id || isActive)
                  ? activeProject.validateMessages.sort((a, b) => a.level.localeCompare(b.level))
                  : []
              }
              rowKey={(record, index) => index}
              columns={this.columns}
              scroll={{ y: height - 26 }}
              height={height}
            />
          </div>
        )}
      </AutoSizer>
    );
  }
}

const activeProjectData = createSelector(
  [
    getActiveProject,
    getActiveDeviceList,
    getActiveRegions,
    getActiveScenarios,
    getActiveVirtualStates,
    getActiveEmployeesHash,
    getActiveAccessKeysHash,
    getActiveWorkSchedulesHash,
    getCurrentProject
  ],
  (
    activeProject,
    devices,
    regions,
    scenarios,
    virtualStates,
    employeesHash,
    accessKeysHash,
    workSchedulesHash,
    currentProject
  ) => ({
    activeProject,
    devices,
    regions,
    scenarios,
    virtualStates,
    employeesHash,
    accessKeysHash,
    workSchedulesHash,
    currentProject
  })
);

const currentProjectData = createSelector(
  [
    getCurrentProjectDeviceList,
    getCurrentProjectRegions,
    getCurrentProjectScenarios,
    getCurrentProjectVirtualStates,
    getCurrentProject
  ],
  (devices, regions, scenarios, virtualStates, currentProject) => ({
    devices,
    regions,
    scenarios,
    virtualStates,
    currentProject
  })
);

const mapStateToProps = (state, ownProps) => {
  const { isCurrentProjectImported } = ownProps;
  if (isCurrentProjectImported) {
    return currentProjectData;
  } else {
    return activeProjectData;
  }
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectValidateMessages);
