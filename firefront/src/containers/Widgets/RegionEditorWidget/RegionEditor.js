import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { Select, Modal } from 'antd';
const { Option } = Select;
import { Table, Column, AutoSizer } from 'react-virtualized';

import SUBSYSTEMS from 'constants/subsystems';

import TripleCRUDMenu from 'components/Menu/WidgetMenu';
import Widget from 'components/Widget';

import SubsystemSelect from 'containers/Selects/SubsystemSelect';
import RegionForm from 'containers/Forms/RegionForm';

import { modalOpen, modalClose } from 'actions/modals';
import { updateCurrentRegion, createRegion, updateRegion, deleteRegion } from 'actions/regions';
import { changeRegionsFilter } from 'actions/widgets';

import {
  checkCurrentProjectIsActive,
  getCurrentProjectRegionsBySubsystem
} from 'helpers/currentProject';
import { checkCompletion } from 'helpers/form';

const regionModals = {
  create: 'newRegion',
  update: 'updateRegion'
};
const SELECT_STYLES = { marginLeft: 8, width: 120 };

class RegionWidget extends Component {
  state = {
    height: 500,
    isActive: false
  };
  columns = [
    { key: 'index', name: 'Номер', width: 100 },
    { key: 'name', name: 'Название' },
    { key: 'subsystem', name: 'Подсистема' },
    { key: 'description', name: 'Примечание' }
  ];

  propsToState(props) {
    const { loadRegionConstantsError } = props;

    if (loadRegionConstantsError) {
      this.setState({
        isActive: false,
        errorMessage: `Ошибка загрузки справочных данных: ${loadRegionConstantsError}`
      });
    } else {
      this.setState({
        isActive: true
      });
    }
  }

  componentDidMount() {
    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    const { onModalClose, modals, createRegionCompleted, updateRegionCompleted } = newProps;
    if (modals[regionModals.create] && createRegionCompleted) onModalClose(regionModals.create);
    if (modals[regionModals.update] && updateRegionCompleted) onModalClose(regionModals.update);
    this.propsToState(newProps);
  }

  openRegionForm = name => {
    this.props.onModalOpen(name);
  };

  createNewRegion = values => {
    const { currentProjectId, onCreateRegion, onUpdateCurrentRegion } = this.props;
    const newRegion = {
      newRegion: values,
      projectId: currentProjectId
    };

    onCreateRegion(newRegion);
    onUpdateCurrentRegion({});
  };

  updateRegion = values => {
    const { currentProjectId, onUpdateRegion, onUpdateCurrentRegion, currentRegion } = this.props;

    const updatedRegion = {
      updatedRegion: {
        ...currentRegion,
        ...values
      },
      projectId: currentProjectId
    };

    onUpdateRegion(updatedRegion);
    onUpdateCurrentRegion({});
  };

  updateCurrentRegion = currentRegion => {
    this.props.onUpdateCurrentRegion(currentRegion);
  };

  deleteRegion = () => {
    const { onDeleteRegion, currentProjectId, currentRegion } = this.props;
    Modal.confirm({
      title: 'Удаление зоны',
      content: (
        <div>
          Удалить зону{' '}
          <b>
            {currentRegion.index}. {currentRegion.name}
          </b>
          ?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        onDeleteRegion(currentProjectId, currentRegion.id);
      }
    });
  };

  componentWillUnmount() {
    this.updateCurrentRegion({});
  }

  onChangeRegionsFilter = value => {
    const { dispatch, regionsFilter } = this.props;
    if (regionsFilter !== value) dispatch(changeRegionsFilter(value));
  };

  render() {
    const { regions = [], currentRegion, isActiveProject, regionsFilter = 'ALL' } = this.props;
    const { isActive } = this.state;
    return (
      <Widget onHeightReady={() => null}>
        <RegionForm modalName={regionModals.create} onSubmit={this.createNewRegion} />
        <RegionForm modalName={regionModals.update} isEdit={true} onSubmit={this.updateRegion} />
        <TripleCRUDMenu
          isCreateButtonActive={isActive && !isActiveProject}
          isDeleteButtonActive={isActive && !isEmpty(currentRegion) && !isActiveProject}
          isEditButtonActive={isActive && !isEmpty(currentRegion) && !isActiveProject}
          onCreateButtonClick={() => this.openRegionForm(regionModals.create)}
          onDeleteButtonClick={this.deleteRegion}
          onEditButtonClick={() => this.openRegionForm(regionModals.update)}
        >
          <SubsystemSelect
            style={SELECT_STYLES}
            value={regionsFilter}
            onChange={this.onChangeRegionsFilter}
          >
            <Option value="ALL">Все</Option>
          </SubsystemSelect>
        </TripleCRUDMenu>
        <AutoSizer>
          {({ width: tableWidth, height }) => {
            const width = tableWidth - 60;
            return (
              <Table
                className="table__list"
                width={tableWidth}
                height={height - 35}
                rowHeight={26}
                headerHeight={16}
                rowGetter={({ index }) => regions[index]}
                rowCount={regions.length}
                rowClassName={({ index }) => {
                  const rowData = regions[index];
                  return rowData && currentRegion.id === rowData.id ? 'selected-row' : '';
                }}
                onRowClick={({ rowData }) => this.updateCurrentRegion(rowData)}
              >
                <Column
                  label="Номер"
                  dataKey="index"
                  key="index"
                  width={60}
                  cellRenderer={({ rowData }) => <div>{rowData.index}</div>}
                />
                <Column
                  label="Название"
                  dataKey="name"
                  key="name"
                  width={width / 3}
                  cellRenderer={({ rowData }) => <div>{rowData.name}</div>}
                />
                <Column
                  label="Подсистема"
                  dataKey="subsystem"
                  key="subsystem"
                  width={width / 3}
                  cellRenderer={({ rowData }) => <div>{SUBSYSTEMS[rowData.subsystem].name}</div>}
                />
                <Column
                  label="Примечение"
                  dataKey="description"
                  key="description"
                  width={width / 3}
                  cellRenderer={({ rowData }) => <div>{rowData.description}</div>}
                />
              </Table>
            );
          }}
        </AutoSizer>
      </Widget>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    onUpdateCurrentRegion: bindActionCreators(updateCurrentRegion, dispatch),
    onModalOpen: bindActionCreators(modalOpen, dispatch),
    onModalClose: bindActionCreators(modalClose, dispatch),
    onDeleteRegion: bindActionCreators(deleteRegion, dispatch),
    onCreateRegion: bindActionCreators(createRegion, dispatch),
    onUpdateRegion: bindActionCreators(updateRegion, dispatch)
  };
};

const mapStateToProps = state => {
  const regionsFilter = state.widgets.regionsFilter;
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    regionConstants: state.regionConstants,
    regions: getCurrentProjectRegionsBySubsystem(state, regionsFilter),
    currentProjectId: state.currentProjectId,
    currentRegion: state.widgets.currentRegion,
    modals: state.modals,
    regionsFilter,

    loadRegionConstantsError: state.errors.loadRegionConstants,
    createRegionCompleted: checkCompletion(
      state.inProgress.createRegion,
      state.errors.createRegion
    ),
    updateRegionCompleted: checkCompletion(state.inProgress.updateRegion, state.errors.updateRegion)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegionWidget);
