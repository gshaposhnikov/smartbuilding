import React, { Component } from 'react';
import { connect } from 'react-redux';

import Table, { Column } from 'components/Table';
import RegionDeviceContextMenu from 'containers/ContextMenus/RegionDeviceContextMenu';
import CurrentRegionContextMenu from 'containers/ContextMenus/CurrentRegionContextMenu';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Widget from 'components/Widget';
import ExpandableTable from 'components/ExpandableTable/Table';

import CurrentRegionDevicesMenu from 'containers/Menus/CurrentRegionDevicesMenu';
import RegionDevicesMenu from 'containers/Menus/RegionDevicesMenu';

import {
  newDeviceRegion,
  deleteDeviceRegion,
  newDeviceListRegion,
  deleteDeviceListRegion
} from 'actions/regions';

import {
  getDevicesByRegionId,
  getInactiveDevicesBySkudRegionId,
  filterDevicesLeavingParents,
  getDevicesTree,
  getFlatTree
} from 'helpers/device';
import {
  checkCurrentProjectIsActive,
  getCurrentProjectDeviceList,
  getCurrentProject,
  getCurrentProjectRegionViews
} from 'helpers/currentProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const SKUD_SUBSYSTEM = 'SKUD';

class RegionDevicesWidget extends Component {
  constructor(props) {
    super(props);
    // Состояние ctrl
    this.ctrlKey = false;
    // Состояние shift
    this.shiftKey = false;

    this.lastSelectedTreeItem = null;
    this.lastSelectedListItem = null;

    this.currentTable = null;
    this.state = {
      selectedTreeRows: [],
      selectedListRows: [],
      deviceTree: [],
      attachableDeviceList: [],
      height: 500
    };
    this.deviceTreeColumns = [
      {
        key: 'type',
        title: 'Тип устройства',
        dataIndex: 'type',
        className: 'type-column',
        width: 300,
        render: (text, record) => (
          <DeviceTypeTreeItem
            contextMenuEnabled={true}
            contextMenuName="regionDeviceContext"
            record={record}
          />
        )
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'shortAddressPath' }
    ];
  }

  componentDidMount = () => {
    window.addEventListener('keydown', this.onKeyDown);
    window.addEventListener('keyup', this.onKeyUp);
  };

  componentWillReceiveProps = nextProps => {
    const { currentRegion, regionDeviceList } = this.props;
    const { attachableDeviceList: prevAttachableDeviceList } = this.state;
    if (
      regionDeviceList.length !== nextProps.regionDeviceList.length ||
      currentRegion.id !== nextProps.currentRegion.id
    ) {
      const deviceTree = this.getRegionAttachableDeviceTree(
        nextProps.deviceList,
        nextProps.currentRegion,
        nextProps.regionDeviceList
      );
      const attachableDeviceList = getFlatTree(deviceTree);
      if (
        currentRegion.id !== nextProps.currentRegion.id ||
        (prevAttachableDeviceList.length && attachableDeviceList.length)
      ) {
        this.setState({ selectedListRows: [], selectedTreeRows: [] });
      }
      this.setState({
        deviceTree,
        attachableDeviceList
      });
    }
  };

  getRegionAttachableDeviceTree = (devices, region, regionalDevices = []) => {
    const regionalDeviceIds = regionalDevices.reduce(
      (deviceIds, device) => [...deviceIds, device.id],
      []
    );
    const deviceTree = getDevicesTree(
      filterDevicesLeavingParents(
        devices,
        device =>
          device.attachableToRegion &&
          !device.disabled &&
          !device.regionId &&
          region.subsystem === device.subsystem &&
          !regionalDeviceIds.includes(device.id)
      ),
      item => {
        item.isActive = item.attachableToRegion;
        return item;
      }
    );
    return deviceTree;
  };

  // Добавление одного устройства
  connectDevice = device => {
    const { isActiveProject } = this.props;
    if (!isActiveProject && device.isActive) {
      const newConnection = {
        projectId: this.props.currentProject.id,
        regionId: this.props.currentRegion.id,
        deviceId: device.id
      };
      this.setState({ selectedListRows: [], selectedTreeRows: [] }, () => {
        this.props.dispatch(newDeviceRegion(newConnection));
      });
    }
  };

  // Добавление списка устройств
  connectDeviceList = device => {
    const { isActiveProject, currentProject, currentRegion } = this.props;
    const { selectedTreeRows } = this.state;
    if (!isActiveProject) {
      if (selectedTreeRows.length) {
        this.props.dispatch(
          newDeviceListRegion(currentProject.id, currentRegion.id, selectedTreeRows)
        );
      } else {
        this.connectDevice(device);
      }
      this.setState({ selectedListRows: [], selectedTreeRows: [] });
    }
  };

  // Удаление одного устройства
  disconnectDevice = device => {
    const { isActiveProject, currentRegion } = this.props;
    if (!isActiveProject && currentRegion.subsystem !== SKUD_SUBSYSTEM) {
      const deleteConnection = {
        projectId: this.props.currentProject.id,
        regionId: this.props.currentRegion.id,
        deviceId: device.id
      };
      this.props.dispatch(deleteDeviceRegion(deleteConnection));
    }
  };

  // Удаление списка устройств
  disconnectDeviceList = device => {
    const { isActiveProject, currentProject, currentRegion } = this.props;
    const { selectedListRows } = this.state;
    if (!isActiveProject) {
      if (selectedListRows.length) {
        this.props.dispatch(
          deleteDeviceListRegion(currentProject.id, currentRegion.id, selectedListRows)
        );
      } else {
        this.disconnectDevice(device);
      }
    }
  };

  onKeyDown = e => {
    if (e.ctrlKey) this.ctrlKey = true;
    if (e.shiftKey) this.shiftKey = true;
    if (e.ctrlKey && !e.shiftKey && e.keyCode === 65 && this.currentTable) {
      if (this.currentTable === 'tree') {
        const { attachableDeviceList: list } = this.state;
        const selectedTreeRows = [];
        list.forEach(item => {
          if (item.isActive) selectedTreeRows.push(item.id);
        });
        this.setState({ selectedTreeRows });
      } else if (this.currentTable === 'list') {
        const { regionDeviceList } = this.props;
        const selectedListRows = [];
        regionDeviceList.forEach(item => {
          selectedListRows.push(item.id);
        });
        this.setState({ selectedListRows });
      }
    }
  };

  onKeyUp = e => {
    if (!e.ctrlKey && this.ctrlKey) this.ctrlKey = false;
    if (!e.shiftKey && this.shiftKey) this.shiftKey = false;
  };

  onTreeRowClick = record => {
    let { selectedTreeRows } = this.state;
    // Если зажат ctrl
    if (this.ctrlKey && !this.shiftKey && record.isActive) {
      if (selectedTreeRows.includes(record.id))
        selectedTreeRows = selectedTreeRows.filter(key => key !== record.id);
      else selectedTreeRows.push(record.id);
      this.setState({ selectedTreeRows });
    }
    // Если зажат shift
    else if (
      this.shiftKey &&
      !this.ctrlKey &&
      selectedTreeRows.length &&
      this.lastSelectedTreeItem
    ) {
      const { attachableDeviceList: list } = this.state;
      let start, end;
      const lastSelectedItemIndex = list.findIndex(
        node => node.id === this.lastSelectedTreeItem.id
      );
      const currentItemIndex = list.findIndex(node => node.id === record.id);
      if (lastSelectedItemIndex > currentItemIndex) {
        start = currentItemIndex;
        end = lastSelectedItemIndex;
      } else {
        end = currentItemIndex;
        start = lastSelectedItemIndex;
      }
      // Получаем список выделенных устройств
      const devicesInRange = list.slice(start, end + 1);
      // Фильтруем по возможности добавления в зону
      const availableDevicesInRange = devicesInRange.filter(device => device.isActive);
      selectedTreeRows = availableDevicesInRange.map(device => device.id);
      this.setState({ selectedTreeRows });
    }
    // Если нет зажатых клавиш
    else if (!this.ctrlKey && !this.shiftKey && record.isActive) {
      let { selectedTreeRows } = this.state;
      if (record.id === this.lastSelectedTreeItemId) {
        clearTimeout(this.lastSelectedTreeItemIdCleaner);
        this.lastSelectedTreeItemId = null;
        this.setState({ selectedTreeRows: [] }, () => {
          this.connectDevice(record);
        });
      } else {
        clearTimeout(this.lastSelectedTreeItemIdCleaner);
        this.lastSelectedTreeItemId = record.id + '';
        this.lastSelectedTreeItemIdCleaner = setTimeout(() => {
          this.lastSelectedTreeItemId = null;
        }, 500);
        if (selectedTreeRows.length === 1 && selectedTreeRows[0] === record.id) {
          this.lastSelectedTreeItem = null;
          selectedTreeRows = [];
        } else {
          this.lastSelectedTreeItem = record;
          selectedTreeRows = [record.id];
        }
        this.setState({ selectedTreeRows });
      }
    }
  };

  onListRowClick = record => {
    let { selectedListRows } = this.state;
    // Если зажат ctrl
    if (this.ctrlKey && !this.shiftKey) {
      if (selectedListRows.includes(record.id))
        selectedListRows = selectedListRows.filter(key => key !== record.id);
      else selectedListRows.push(record.id);
      this.setState({ selectedListRows });
    }
    // Если зажат shift
    else if (
      this.shiftKey &&
      !this.ctrlKey &&
      selectedListRows.length &&
      this.lastSelectedListItem
    ) {
      const { regionDeviceList } = this.props;
      let start, end;
      const lastSelectedItemIndex = regionDeviceList.findIndex(
        node => node.id === this.lastSelectedListItem.id
      );
      const currentItemIndex = regionDeviceList.findIndex(node => node.id === record.id);
      if (lastSelectedItemIndex > currentItemIndex) {
        start = currentItemIndex;
        end = lastSelectedItemIndex;
      } else {
        end = currentItemIndex;
        start = lastSelectedItemIndex;
      }
      // Получаем список выделенных устройств
      const devicesInRange = regionDeviceList.slice(start, end + 1);
      selectedListRows = devicesInRange.map(device => device.id);
      this.setState({ selectedListRows });
    }
    // Если нет зажатых клавиш
    else if (!this.ctrlKey && !this.shiftKey) {
      let { selectedListRows } = this.state;
      if (selectedListRows.length === 1 && selectedListRows[0] === record.id) {
        this.lastSelectedListItem = null;
        selectedListRows = [];
      } else {
        this.lastSelectedListItem = record;
        selectedListRows = [record.id];
      }
      this.setState({ selectedListRows });
    }
  };

  getTreeRowClassName = record => {
    const { selectedTreeRows } = this.state;
    if (selectedTreeRows.includes(record.id)) return 'selected-row';
    return '';
  };

  getListRowClassName = record => {
    const { selectedListRows } = this.state;
    if (selectedListRows.includes(record.id)) return 'selected-row';
    return '';
  };

  updateCurrentTable = id => {
    this.currentTable = id;
  };

  componentWillUnmount = () => {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('keyup', this.onKeyUp);
  };

  render() {
    const { isActiveProject, regionDeviceList, currentRegion } = this.props;
    const { height, selectedTreeRows, selectedListRows, deviceTree } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <CurrentRegionDevicesMenu
          region={currentRegion}
          onDisconnectDeviceList={this.disconnectDeviceList}
          isDisconnectDeviceListActive={
            selectedListRows.length &&
            !isActiveProject &&
            currentRegion.subsystem !== SKUD_SUBSYSTEM
          }
        />
        <CurrentRegionContextMenu
          isActive={!isActiveProject}
          onDisconnectDeviceList={this.disconnectDeviceList}
        />
        <div onMouseOver={() => this.updateCurrentTable('list')}>
          <Table
            scroll={{ y: height / 2 - 76 }}
            style={{ minHeight: height / 2 - 50 }}
            dataSource={regionDeviceList}
            rowKey={record => record.id}
            onRowDoubleClick={this.disconnectDevice}
            onRowClick={this.onListRowClick}
            rowClassName={this.getListRowClassName}
          >
            <Column
              title="Тип устройства"
              width={300}
              dataIndex="type"
              key="type"
              render={(text, record) => (
                <DeviceTypeTreeItem
                  contextMenuEnabled={true}
                  contextMenuName="currentRegionContext"
                  record={record}
                />
              )}
            />
            <Column
              title="Адрес"
              width={300}
              dataIndex="middleAddressPath"
              key="middleAddressPath"
            />
          </Table>
        </div>
        <RegionDevicesMenu
          region={currentRegion}
          onConnectDeviceList={this.connectDeviceList}
          isConnectDeviceListActive={selectedTreeRows.length && !isActiveProject}
        />
        <RegionDeviceContextMenu
          isActiveProject={isActiveProject}
          onConnectDeviceList={this.connectDeviceList}
        />
        <div onMouseOver={() => this.updateCurrentTable('tree')}>
          {deviceTree.length ? (
            <ExpandableTable
              forceWidth
              onRowClick={this.onTreeRowClick}
              onRowDoubleClick={this.connectDevice}
              rowSelections={selectedTreeRows}
              columns={this.deviceTreeColumns}
              nodes={deviceTree}
              height={height / 2 - 46}
            />
          ) : null}
        </div>
      </Widget>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  const currentRegion = state.widgets.currentRegion;
  const deviceList = getCurrentProjectDeviceList(state);
  return {
    deviceList,
    currentProject: getCurrentProject(state),
    currentRegion,
    isActiveProject: checkCurrentProjectIsActive(state),
    regionDeviceList:
      currentRegion.subsystem !== SKUD_SUBSYSTEM
        ? getDevicesByRegionId(deviceList, currentRegion.id)
        : getInactiveDevicesBySkudRegionId(
            deviceList,
            getDeviceProfileViewsHash(state),
            currentRegion.id
          ),
    regions: getCurrentProjectRegionViews(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegionDevicesWidget);
