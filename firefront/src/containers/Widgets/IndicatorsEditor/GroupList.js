import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';

import { VirtualizedWrapper as Wrapper } from 'components/Layout';
import IndicatorGroupList from 'components/IndicatorGroupList';
import TripleCRUDMenu from 'components/Menu/WidgetMenu';

import IndicatorGroupForm from 'containers/Forms/IndicatorGroupForm';

import { modalOpen, modalClose } from 'actions/modals';
import {
  addIndicatorGroup,
  updateIndicatorGroup,
  deleteIndicatorGroup,
  resetSelectedIndicator,
  selectIndicatorGroup,
  selectIndicatorGroupAndPanel
} from 'actions/indicators';

import {
  checkCurrentProjectIsActive,
  getCurrentProjectIndicatorGroupsList,
  getCurrentProjectIndicatorGroupsHash,
  getCurrentProjectIndicatorPanelsHash
} from 'helpers/currentProject';
import { saveGroupIdToStorage } from 'helpers/indicators';

const MODAL_NAMES = {
  GROUP: {
    ADD: 'addIndicatorGroup',
    UPDATE: 'updateIndicatorGroup'
  }
};

class IndicatorListWidget extends Component {
  componentDidMount = () => {
    const { indicatorGroups, selectedIndicatorGroupId } = this.props;
    if (indicatorGroups.length && !selectedIndicatorGroupId)
      this.initSelectedGroupAndPanel(this.props);
  };

  componentWillReceiveProps = nextProps => {
    const { indicatorGroups, selectedIndicatorGroupId } = this.props;
    if (
      !indicatorGroups.length &&
      nextProps.indicatorGroups.length &&
      !selectedIndicatorGroupId &&
      !nextProps.selectedIndicatorGroupId
    )
      this.initSelectedGroupAndPanel(nextProps);
  };

  initSelectedGroupAndPanel = props => {
    const { currentProjectId, dispatch, indicatorGroupsHash, indicatorPanelsHash } = props;
    let storage = localStorage.getItem('indicators');
    if (storage) {
      storage = JSON.parse(storage);
      if (storage[currentProjectId]) {
        const groupId = storage[currentProjectId].groupId;
        const panelId = storage[currentProjectId].panelId;
        if (groupId && indicatorGroupsHash[groupId]) {
          if (panelId && indicatorPanelsHash[panelId]) {
            dispatch(selectIndicatorGroupAndPanel(groupId, panelId));
          } else {
            dispatch(selectIndicatorGroup(groupId));
          }
        }
      }
    }
  };

  openModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalOpen(modalName));
  };

  closeModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalClose(modalName));
  };

  onRowClick = ({ rowData: { id: groupId } }) => {
    const { dispatch, currentProjectId } = this.props;
    saveGroupIdToStorage(currentProjectId, groupId);
    dispatch(selectIndicatorGroup(groupId));
  };

  addIndicatorGroup = (values, modalName) => {
    const { dispatch, currentProjectId } = this.props;
    dispatch(addIndicatorGroup(currentProjectId, values));
    this.closeModal(modalName);
  };

  updateIndicatorGroup = (values, modalName) => {
    const { dispatch, currentProjectId, selectedIndicatorGroupId } = this.props;
    dispatch(updateIndicatorGroup(currentProjectId, selectedIndicatorGroupId, values));
    this.closeModal(modalName);
  };

  deleteIndicatorGroup = () => {
    const {
      dispatch,
      currentProjectId,
      selectedIndicatorGroupId,
      indicatorGroups,
      indicatorGroupsHash
    } = this.props;
    if (selectedIndicatorGroupId) {
      Modal.confirm({
        title: 'Удаление группы панелей вирт. индикаторов',
        content: (
          <div>
            Удалить группу панелей <b>{indicatorGroupsHash[selectedIndicatorGroupId].name}</b>?
          </div>
        ),
        okText: 'Удалить',
        onCancel: () => {},
        cancelText: 'Отмена',
        maskClosable: false,
        onOk: () => {
          let nextIndicatorGroupId = null;
          if (indicatorGroups.length > 1) {
            nextIndicatorGroupId = indicatorGroups.filter(
              indicatorGroup => indicatorGroup.id !== selectedIndicatorGroupId
            )[0].id;
          }
          dispatch(
            deleteIndicatorGroup(currentProjectId, selectedIndicatorGroupId, nextIndicatorGroupId)
          );
        }
      });
    }
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(resetSelectedIndicator());
  };

  render = () => {
    const { indicatorGroups, isActiveProject, selectedIndicatorGroupId } = this.props;
    return (
      <Wrapper>
        <IndicatorGroupForm
          modalName={MODAL_NAMES.GROUP.ADD}
          onSubmit={values => this.addIndicatorGroup(values, MODAL_NAMES.GROUP.ADD)}
        />
        <IndicatorGroupForm
          isEdit
          modalName={MODAL_NAMES.GROUP.UPDATE}
          onSubmit={values => this.updateIndicatorGroup(values, MODAL_NAMES.GROUP.UPDATE)}
        />
        <TripleCRUDMenu
          isActive={!isActiveProject}
          isCreateButtonActive={!isActiveProject}
          onCreateButtonClick={() => this.openModal(MODAL_NAMES.GROUP.ADD)}
          isEditButtonActive={!isActiveProject && selectedIndicatorGroupId}
          onEditButtonClick={() => this.openModal(MODAL_NAMES.GROUP.UPDATE)}
          isDeleteButtonActive={!isActiveProject && selectedIndicatorGroupId}
          onDeleteButtonClick={this.deleteIndicatorGroup}
        />
        <IndicatorGroupList
          topMargin={35}
          indicatorGroups={indicatorGroups}
          rowSelection={selectedIndicatorGroupId}
          onRowClick={this.onRowClick}
        />
      </Wrapper>
    );
  };
}

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    currentProjectId: state.currentProjectId,
    selectedIndicatorGroupId: state.widgets.selectedIndicatorGroupId,
    indicatorGroups: getCurrentProjectIndicatorGroupsList(state),
    indicatorGroupsHash: getCurrentProjectIndicatorGroupsHash(state),
    indicatorPanelsHash: getCurrentProjectIndicatorPanelsHash(state)
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorListWidget);
