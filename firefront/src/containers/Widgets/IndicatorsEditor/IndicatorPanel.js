import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import IndicatorPanel from 'components/IndicatorPanel';

import IndicatorPanelEditorWidgetMenu from 'containers/Menus/IndicatorPanelEditorWidgetMenu';
import IndicatorForm from 'containers/Forms/IndicatorForm';

import { selectIndicator, updateIndicator } from 'actions/indicators';
import { modalOpen, modalClose } from 'actions/modals';

import {
  getCurrentProjectRegionsHash,
  getCurrentProjectScenariosHash,
  getCurrentProjectDevicesHash,
  checkCurrentProjectIsActive
} from 'helpers/currentProject';

const Wrapper = styled.div`
  flex: 1;
  background-color: white;
  .indicator-cell {
    border-right: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
const MIN_CELL_HEIGHT = 20;
const MAX_CELL_HEIGHT = 500;
const MIN_CELL_WIDTH = 30;
const MAX_CELL_WIDTH = 1000;
const PANELS_SIZE = 'panelsSize';
const PANELS_AUTOSIZE = 'panelsAutosize';
const MODAL_NAMES = {
  PANEL: {
    UPDATE: 'updateIndicatorPanel'
  },
  INDICATOR: {
    UPDATE: 'updateIndicator'
  }
};

class IndicatorPanelWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panelsSizeValue: localStorage.getItem(PANELS_SIZE)
        ? JSON.parse(localStorage.getItem(PANELS_SIZE))
        : {},
      [PANELS_SIZE]: localStorage.getItem(PANELS_SIZE)
        ? JSON.parse(localStorage.getItem(PANELS_SIZE))
        : {},
      [PANELS_AUTOSIZE]: localStorage.getItem(PANELS_AUTOSIZE)
        ? JSON.parse(localStorage.getItem(PANELS_AUTOSIZE))
        : {}
    };
  }

  openModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalOpen(modalName));
  };

  closeModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalClose(modalName));
  };

  renderIndicatorCell = ({ columnIndex, rowIndex, key, style }) => {
    const {
      selectedIndicatorPanel: { indicators },
      regions,
      devices,
      scenarios,
      isActiveProject
    } = this.props;
    const indicator = indicators[rowIndex][columnIndex];
    let cellName = indicator.name;
    if (!cellName && indicator.entityType !== 'NONE' && !cellName && indicator.entityIds.length) {
      switch (indicator.entityType) {
        case 'REGION': {
          cellName = `${indicator.entityIds.map(entityId => regions[entityId].name)}`;
          break;
        }
        case 'SCENARIO': {
          cellName = `${indicator.entityIds.map(entityId => scenarios[entityId].name)}`;
          break;
        }
        case 'DEVICE': {
          cellName = `${indicator.entityIds.map(entityId => devices[entityId].name)}`;
          break;
        }
        default: {
          break;
        }
      }
    }
    const styles = indicator.styles ? { ...style, ...indicator.styles } : style;
    return (
      <div
        key={key}
        className="indicator-cell"
        style={styles}
        title={indicator.description || cellName}
        onDoubleClick={() => (!isActiveProject ? this.onIndicatorClick(indicator) : null)}
      >
        {cellName}
      </div>
    );
  };

  onIndicatorClick = indicator => {
    const { dispatch } = this.props;
    dispatch(selectIndicator(indicator));
    this.openModal(MODAL_NAMES.INDICATOR.UPDATE);
  };

  onChangeAutosize = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsAutosize } = this.state;
    if (
      !panelsAutosize[selectedIndicatorPanel.id] &&
      typeof panelsAutosize[selectedIndicatorPanel.id] !== 'undefined'
    ) {
      panelsAutosize[selectedIndicatorPanel.id] = true;
    } else {
      panelsAutosize[selectedIndicatorPanel.id] = false;
    }
    localStorage.setItem(PANELS_AUTOSIZE, JSON.stringify(panelsAutosize));
    this.setState({ panelsAutosize });
  };

  onChangeCellsWidth = value => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!panelsSizeValue[selectedIndicatorPanel.id])
      panelsSizeValue[selectedIndicatorPanel.id] = {};
    panelsSizeValue[selectedIndicatorPanel.id].width = value;
    this.setState({ panelsSizeValue });
  };

  onChangeCellsHeight = value => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!panelsSizeValue[selectedIndicatorPanel.id])
      panelsSizeValue[selectedIndicatorPanel.id] = {};
    panelsSizeValue[selectedIndicatorPanel.id].height = value;
    this.setState({ panelsSizeValue });
  };

  setCellsHeight = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!panelsSizeValue[selectedIndicatorPanel.id])
      panelsSizeValue[selectedIndicatorPanel.id] = {};
    const value = panelsSizeValue[selectedIndicatorPanel.id].height;
    const newValue = value
      ? value < MIN_CELL_HEIGHT
        ? MIN_CELL_HEIGHT
        : value > MAX_CELL_HEIGHT
        ? MAX_CELL_HEIGHT
        : value
      : MIN_CELL_HEIGHT;
    panelsSizeValue[selectedIndicatorPanel.id].height = newValue;
    const newPanelsSizeString = JSON.stringify(panelsSizeValue);
    localStorage.setItem(PANELS_SIZE, newPanelsSizeString);
    this.setState({ panelsSize: JSON.parse(newPanelsSizeString) });
  };

  setCellsWidth = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!panelsSizeValue[selectedIndicatorPanel.id])
      panelsSizeValue[selectedIndicatorPanel.id] = {};
    const value = panelsSizeValue[selectedIndicatorPanel.id].width;
    const newValue = value
      ? value < MIN_CELL_WIDTH
        ? MIN_CELL_WIDTH
        : value > MAX_CELL_WIDTH
        ? MAX_CELL_WIDTH
        : value
      : MIN_CELL_WIDTH;
    panelsSizeValue[selectedIndicatorPanel.id].width = newValue;
    const newPanelsSizeString = JSON.stringify(panelsSizeValue);
    localStorage.setItem(PANELS_SIZE, newPanelsSizeString);
    this.setState({ panelsSize: JSON.parse(newPanelsSizeString) });
  };

  updateIndicator = (values, modalName) => {
    const { dispatch, currentProjectId, selectedIndicatorPanel } = this.props;
    dispatch(updateIndicator(currentProjectId, selectedIndicatorPanel.id, values));
    this.closeModal(modalName);
  };

  getCellHeight = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_HEIGHT;
    if (!panelsSize[selectedIndicatorPanel.id]) return MIN_CELL_HEIGHT;
    if (!panelsSize[selectedIndicatorPanel.id].height) return MIN_CELL_HEIGHT;
    return panelsSize[selectedIndicatorPanel.id].height;
  };

  getCellHeightValue = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_HEIGHT;
    if (!panelsSizeValue[selectedIndicatorPanel.id]) return MIN_CELL_HEIGHT;
    if (!panelsSizeValue[selectedIndicatorPanel.id].height) return MIN_CELL_HEIGHT;
    return panelsSizeValue[selectedIndicatorPanel.id].height;
  };

  getCellWidth = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_WIDTH;
    if (!panelsSize[selectedIndicatorPanel.id]) return MIN_CELL_WIDTH;
    if (!panelsSize[selectedIndicatorPanel.id].width) return MIN_CELL_WIDTH;
    return panelsSize[selectedIndicatorPanel.id].width;
  };

  getCellWidthValue = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSizeValue } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_WIDTH;
    if (!panelsSizeValue[selectedIndicatorPanel.id]) return MIN_CELL_WIDTH;
    if (!panelsSizeValue[selectedIndicatorPanel.id].width) return MIN_CELL_WIDTH;
    return panelsSizeValue[selectedIndicatorPanel.id].width;
  };

  render = () => {
    const { selectedIndicatorPanel, isActiveProject, regions, scenarios, devices } = this.props;
    const { panelsAutosize } = this.state;
    return (
      <Wrapper>
        <IndicatorForm
          onSubmit={values => this.updateIndicator(values, MODAL_NAMES.INDICATOR.UPDATE)}
        />
        <IndicatorPanelEditorWidgetMenu
          isActive={!isActiveProject}
          onChangeWidth={this.onChangeCellsWidth}
          onChangeHeight={this.onChangeCellsHeight}
          onChangeAutosize={this.onChangeAutosize}
          autosize={
            selectedIndicatorPanel &&
            (panelsAutosize[selectedIndicatorPanel.id] ||
              typeof panelsAutosize[selectedIndicatorPanel.id] === 'undefined')
              ? true
              : false
          }
          setHeightValue={this.setCellsHeight}
          setWidthValue={this.setCellsWidth}
          height={this.getCellHeightValue()}
          width={this.getCellWidthValue()}
        />
        <IndicatorPanel
          regions={regions}
          scenarios={scenarios}
          devices={devices}
          selectedIndicatorPanel={selectedIndicatorPanel}
          panelsAutosize={panelsAutosize}
          renderIndicatorCell={this.renderIndicatorCell}
          getCellHeight={this.getCellHeight}
          getCellWidth={this.getCellWidth}
        />
      </Wrapper>
    );
  };
}

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;

  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    regions: getCurrentProjectRegionsHash(state),
    scenarios: getCurrentProjectScenariosHash(state),
    devices: getCurrentProjectDevicesHash(state),
    currentProjectId: state.currentProjectId,
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.indicators.panels[state.currentProjectId]
        ? state.indicators.panels[state.currentProjectId][selectedIndicatorPanelId]
        : null
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorPanelWidget);
