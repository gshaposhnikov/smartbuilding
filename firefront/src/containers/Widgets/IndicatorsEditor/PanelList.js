import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';

import { VirtualizedWrapper as Wrapper } from 'components/Layout';
import IndicatorPanelList from 'components/IndicatorPanelList';
import TripleCRUDMenu from 'components/Menu/WidgetMenu';

import IndicatorPanelForm from 'containers/Forms/IndicatorPanelForm';

import { modalOpen, modalClose } from 'actions/modals';
import {
  updateIndicatorPanel,
  addIndicatorPanel,
  selectIndicatorPanel,
  resetSelectedIndicator,
  deleteIndicatorPanel
} from 'actions/indicators';

import {
  getCurrentProjectIndicatorPanelsInSelectedGroup,
  checkCurrentProjectIsActive
} from 'helpers/currentProject';
import { savePanelIdToStorage } from 'helpers/indicators';

const MODAL_NAMES = {
  PANEL: {
    ADD: 'addIndicatorPanel',
    UPDATE: 'updateIndicatorPanel'
  }
};

class IndicatorListWidget extends Component {
  openModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalOpen(modalName));
  };

  closeModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalClose(modalName));
  };

  onRowClick = ({ rowData: { id: panelId } }) => {
    const { dispatch, currentProjectId, selectedIndicatorGroupId } = this.props;
    savePanelIdToStorage(currentProjectId, selectedIndicatorGroupId, panelId);
    dispatch(selectIndicatorPanel(panelId));
  };

  addIndicatorPanel = (values, modalName) => {
    const { dispatch, currentProjectId, selectedIndicatorGroupId } = this.props;
    dispatch(
      addIndicatorPanel(currentProjectId, { ...values, indicatorGroupId: selectedIndicatorGroupId })
    );
    this.closeModal(modalName);
  };

  updateIndicatorPanel = (values, modalName) => {
    const { dispatch, currentProjectId, selectedIndicatorPanel } = this.props;
    dispatch(updateIndicatorPanel(currentProjectId, { ...selectedIndicatorPanel, ...values }));
    this.closeModal(modalName);
  };

  deleteIndicatorPanel = () => {
    const { selectedIndicatorPanel, currentProjectId, dispatch } = this.props;
    Modal.confirm({
      title: 'Удаление панели вирт. индикаторов',
      content: (
        <div>
          Удалить панель вирт. индикаторов <b>{selectedIndicatorPanel.name}</b>?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteIndicatorPanel(currentProjectId, selectedIndicatorPanel.id));
      }
    });
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(resetSelectedIndicator());
  };

  render = () => {
    const {
      indicatorPanels,
      isActiveProject,
      selectedIndicatorPanelId,
      selectedIndicatorGroupId
    } = this.props;
    return (
      <Wrapper>
        <IndicatorPanelForm
          modalName={MODAL_NAMES.PANEL.ADD}
          onSubmit={values => this.addIndicatorPanel(values, MODAL_NAMES.PANEL.ADD)}
        />
        <IndicatorPanelForm
          isEdit
          modalName={MODAL_NAMES.PANEL.UPDATE}
          onSubmit={values => this.updateIndicatorPanel(values, MODAL_NAMES.PANEL.UPDATE)}
        />
        <TripleCRUDMenu
          isCreateButtonActive={!isActiveProject && selectedIndicatorGroupId}
          onCreateButtonClick={() => this.openModal(MODAL_NAMES.PANEL.ADD)}
          isEditButtonActive={!isActiveProject && selectedIndicatorPanelId}
          onEditButtonClick={() => this.openModal(MODAL_NAMES.PANEL.UPDATE)}
          isDeleteButtonActive={!isActiveProject && selectedIndicatorPanelId}
          onDeleteButtonClick={this.deleteIndicatorPanel}
        />
        <IndicatorPanelList
          topMargin={35}
          indicatorPanels={indicatorPanels}
          rowSelection={selectedIndicatorPanelId}
          onRowClick={this.onRowClick}
        />
      </Wrapper>
    );
  };
}

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    currentProjectId: state.currentProjectId,
    selectedIndicatorGroupId: state.widgets.selectedIndicatorGroupId,
    indicatorPanels: getCurrentProjectIndicatorPanelsInSelectedGroup(state),
    selectedIndicatorPanelId,
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.indicators.panels[state.currentProjectId]
        ? state.indicators.panels[state.currentProjectId][selectedIndicatorPanelId]
        : null
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndicatorListWidget);
