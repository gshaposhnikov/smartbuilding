import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { createSelector } from 'reselect';

import { selectScenario, performScenarioAction } from 'actions/scenarios';
import { changeActiveScenariosFilter } from 'actions/widgets';

import Text from 'components/Text';
import Widget from 'components/Widget';
import ScenarioList from 'components/ScenarioList';

import ScenarioListContextMenu from 'containers/ContextMenus/ScenarioListContextMenu';
import ScenarioListMenu from 'containers/Menus/ScenarioListMenu';

import { getActiveScenariosByFilter } from 'helpers/activeProject';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

class ScenarioListView extends Component {
  state = {
    height: 500
  };

  onRowClick = record => {
    const { dispatch } = this.props;
    dispatch(selectScenario(record.id));
  };

  onChangeScenariosFilter = value => {
    const { dispatch, scenariosFilter } = this.props;
    if (scenariosFilter !== value) dispatch(changeActiveScenariosFilter(value));
  };

  onPerformScenarioAction = (scenarioNo, actionId) => {
    const { dispatch, activeProjectId } = this.props;
    dispatch(performScenarioAction(activeProjectId, scenarioNo, actionId));
  };

  render() {
    const {
      scenarios,
      currentScenarioId,
      scenarioDictionaries: { purposes, manageActions },
      scenariosFilter,
      license
    } = this.props;
    const { height } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ScenarioListContextMenu
          scenarioActions={manageActions}
          onPerformScenarioAction={this.onPerformScenarioAction}
        />
        {scenarios ? (
          [
            <ScenarioListMenu
              key="scenarioMenu"
              scenarioPurposes={purposes}
              scenariosFilter={scenariosFilter}
              onChangeScenariosFilter={this.onChangeScenariosFilter}
            />,
            <ScenarioList
              key="scenarioList"
              isActiveScenarios
              scenarios={scenarios}
              scroll={{ y: height - 86 }}
              onRowClick={this.onRowClick}
              currentScenarioId={currentScenarioId}
              contextMenuEnabled={
                license && license.control && license.control.engineeringEnabled ? true : false
              }
              contextMenuName="scenarioListContext"
            />
          ]
        ) : (
          <Spin>
            <Text>Нет данных</Text>
          </Spin>
        )}
      </Widget>
    );
  }
}

const getFilteredScenarios = createSelector(
  getCurrentUser,
  getActiveScenariosByFilter,
  filterEntities
);

const mapStateToProps = state => {
  const scenariosFilter = state.widgets.activeScenariosFilter;
  return {
    activeProjectId: state.activeProject.project ? state.activeProject.project.id : null,
    scenarios: getFilteredScenarios(state),
    currentScenarioId: state.widgets.currentScenarioId,
    scenarioDictionaries: state.scenarioConstants.dictionaries || {},
    scenariosFilter,
    license: state.license
  };
};

export default connect(mapStateToProps)(ScenarioListView);
