import { Component } from 'react';
import { connect } from 'react-redux';
import { notification } from 'antd';

import { startScenarioTimer } from 'actions/scenarios';

class ScenarioComputerAction extends Component {
  componentWillReceiveProps(nextProps) {
    const { computerAction } = this.props;
    if (nextProps.computerAction && computerAction !== nextProps.computerAction) {
      this.performComputerAction(nextProps.computerAction);
    }
  }

  performComputerAction = computerAction => {
    setTimeout(() => {
      switch (computerAction.command) {
        case 'SHOW_MESSAGE': {
          const { message: notificationMessage, title } = computerAction;
          const args = {
            message: title,
            description: notificationMessage,
            duration: 0
          };
          notification.open(args);
          break;
        }
        case 'COUNTDOWN': {
          const { dispatch } = this.props;
          dispatch(startScenarioTimer(computerAction));
          break;
        }
        default:
          break;
      }
    }, computerAction.timeDelaySec * 1000);
  };

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    computerAction: state.activeProject.scenarioComputerAction
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioComputerAction);
