import React, { Component } from 'react';
import { connect } from 'react-redux';

import Widget from 'components/Widget';
import ScenarioTimeLineBlocks from 'components/ScenarioTimeLineBlocks';

import ScenarioViewMenu from 'containers/Menus/ScenarioViewMenu';

import { addKeys } from 'helpers/scenarios';
import {
  getActiveScenariosHash,
  getActiveDevicesHash,
  getActiveRegionsHash
} from 'helpers/activeProject';
import { availableByFilterTags } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

class ScenarioView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seletedScenario: null,
      height: 500,
      expandedRowKeys: []
    };
  }

  propsToState = props => {
    const { currentScenarioId, scenarios } = props;
    let selectedScenario = null;
    if (scenarios && currentScenarioId && currentScenarioId !== this.props.currentScenarioId) {
      const newSelectedScenario = scenarios[currentScenarioId];
      if (newSelectedScenario && newSelectedScenario.timeLineBlocksTree) {
        newSelectedScenario.timeLineBlocksTree.forEach((rootItem, index) =>
          addKeys(rootItem, '', index)
        );
        selectedScenario = newSelectedScenario;
      }
    }
    if (selectedScenario || !currentScenarioId) {
      this.setState({ selectedScenario });
    }
    if (
      (!this.props.currentScenarioId && currentScenarioId) ||
      (this.props.currentScenarioId &&
        currentScenarioId &&
        this.props.currentScenarioId !== currentScenarioId)
    ) {
      if (selectedScenario) {
        const expandedRowKeys = [];
        for (let i = 0; i < selectedScenario.timeLineBlocksTree.length - 1; i++) {
          expandedRowKeys.push('.' + i);
          if (selectedScenario.timeLineBlocksTree[i].children) {
            for (let j = 0; j < selectedScenario.timeLineBlocksTree[i].children.length - 1; j++) {
              expandedRowKeys.push('.' + i + '.' + j);
            }
          }
        }
        this.setState({ expandedRowKeys });
      }
    }
  };

  componentDidMount = () => {
    this.propsToState(this.props);
  };

  componentWillReceiveProps = nextProps => {
    this.propsToState(nextProps);
  };

  getRowClassName = record => {
    let classNames = '';
    if (!record.children || !record.children.length) classNames += 'hidden-expand ';
    switch (record.itemType) {
      case 'OUT': {
        classNames += 'row-hidden ';
        break;
      }
      case 'TIME': {
        classNames += 'row-highlighted ';
        break;
      }
      default:
        break;
    }
    switch (record.entityType) {
      case 'DEVICE': {
        const { devices, currentUser } = this.props;
        const device = devices[record.entityId];
        if (device && !availableByFilterTags(currentUser, device)) classNames += 'row-unavailable ';
        break;
      }
      case 'REGION': {
        const { regions, currentUser } = this.props;
        const region = regions[record.entityId];
        if (region && !availableByFilterTags(currentUser, region)) classNames += 'row-unavailable ';
        break;
      }
      case 'SCENARIO': {
        const { scenarios, currentUser } = this.props;
        const scenario = scenarios[record.entityId];
        if (scenario && !availableByFilterTags(currentUser, scenario))
          classNames += 'row-unavailable ';
        break;
      }
      default:
        break;
    }
    return classNames;
  };

  onRowExpand = (expand, record) => {
    let { expandedRowKeys } = this.state;
    if (expand) expandedRowKeys.push(record.key);
    else expandedRowKeys = expandedRowKeys.filter(key => key !== record.key);
    this.setState({
      expandedRowKeys
    });
  };

  render = () => {
    const { expandedRowKeys, height, selectedScenario } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ScenarioViewMenu title={selectedScenario ? selectedScenario.name : 'Не выбран сценарий'} />
        <ScenarioTimeLineBlocks
          dataSource={
            selectedScenario && selectedScenario.timeLineBlocksTree
              ? selectedScenario.timeLineBlocksTree
              : []
          }
          rowClassName={this.getRowClassName}
          scroll={{ y: height - 62 }}
          expandedRowKeys={expandedRowKeys}
          onExpand={this.onRowExpand}
        />
      </Widget>
    );
  };
}

const mapStateToProps = state => {
  return {
    currentUser: getCurrentUser(state),
    scenarios: getActiveScenariosHash(state),
    devices: getActiveDevicesHash(state),
    regions: getActiveRegionsHash(state),
    currentScenarioId: state.widgets.currentScenarioId
  };
};

export default connect(mapStateToProps)(ScenarioView);
