import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEqual, isEmpty } from 'lodash';
import { reduxForm, initialize, reset } from 'redux-form';

import { loadScenarioConstants } from 'actions/scenarios';

import ScenarioParamsForm from 'containers/Forms/ScenarioParamsForm';
import Widget from 'components/Widget';

import { getActiveScenarios } from 'helpers/activeProject';

export const FORM_NAME = 'scenarioParams';

class ScenarioParams extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seletedScenario: null,
      flagNames: this.getFlagNames(props)
    };
  }

  getFlagNames(props) {
    const { scenarioConstants } = props;
    if (scenarioConstants && scenarioConstants.dictionaries) {
      try {
        const manualStartStopAllowedName = scenarioConstants.dictionaries.flags.find(
          flag => flag.id === 'manualStartStopAllowed'
        ).name;
        const stopOnGlobalResetName = scenarioConstants.dictionaries.flags.find(
          flag => flag.id === 'stopOnGlobalReset'
        ).name;
        return { manualStartStopAllowedName, stopOnGlobalResetName };
      } catch (error) {
        return {
          manualStartStopAllowedName: 'Управление в ОЗ',
          stopOnGlobalResetName: 'Выключение при сбросе'
        };
      }
    } else {
      return {
        manualStartStopAllowedName: 'Управление в ОЗ',
        stopOnGlobalResetName: 'Выключение при сбросе'
      };
    }
  }

  componentDidMount() {
    const { scenarioConstants, dispatch } = this.props;
    if (isEmpty(scenarioConstants)) {
      dispatch(loadScenarioConstants());
    }
    this.setState({
      ...this.state,
      flagNames: this.getFlagNames(this.props)
    });
  }

  componentWillReceiveProps(newProps) {
    const { currentScenarioId, scenarios, dispatch } = newProps;
    const { selectedScenario } = this.state;

    const newSelectedScenario =
      scenarios && currentScenarioId
        ? scenarios.find(scenario => scenario.id === currentScenarioId)
        : null;

    if (!isEqual(selectedScenario, newSelectedScenario)) {
      if (newSelectedScenario) {
        dispatch(initialize(FORM_NAME, newSelectedScenario.advancedParams));
      } else {
        dispatch(initialize(FORM_NAME, {}));
      }
      dispatch(reset(FORM_NAME));
      this.setState({
        ...this.state,
        selectedScenario: newSelectedScenario
      });
    }
  }

  render() {
    const { selectedScenario, flagNames } = this.state;
    const { currentValues } = this.props;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ScenarioParamsForm
          isActiveScenario
          selectedScenario={selectedScenario}
          flagNames={flagNames}
          currentValues={currentValues}
        />;
      </Widget>
    );
  }
}

ScenarioParams = reduxForm({
  form: FORM_NAME
})(ScenarioParams);

const mapStateToProps = state => {
  return {
    scenarioConstants: state.scenarioConstants,
    scenarios: getActiveScenarios(state),
    currentScenarioId: state.widgets.currentScenarioId,
    /* Текущие значения полей */
    currentValues: state.form[FORM_NAME] ? state.form[FORM_NAME].values || {} : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScenarioParams);
