import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Select, Input, InputNumber } from 'antd';
const Option = Select.Option;

import {
  setActiveDeviceConfig,
  uploadDeviceConfig,
  downloadDeviceConfig,
  uploadDeviceControlDatabase,
  updateCurrentDevice,
  uploadAllDeviceControlDatabases,
  exportControlDeviceConfig,
  setDeviceTime
} from 'actions/devices';
import { modalOpen, modalClose } from 'actions/modals';

import Table, { Column } from 'components/Table';
import TextCell from 'components/Table/TextCell';
import Widget from 'components/Widget';

import ConfigMenu from 'containers/Menus/ConfigMenu';
import ContolDeviceDBParams from 'containers/Forms/ControlDeviceDBParamsForm';
import ProjectEntitiesDiff from 'containers/Modals/ProjectEntitiesDiff';

import { getActiveProjectId } from 'helpers/activeProject';
import { getCurrentDevice } from 'helpers/widgets';
import { getDateTimestamp } from 'helpers/time';

const ConfigWrapper = styled.div`
  flex: 1;
  background-color: #fff;
  & .ant-select {
    width: 100%;
  }
  & .ant-input-number {
    width: 100%;
  }
  & .ant-table-row {
    height: 30px;
  }
  & .hidden-expand {
    & .ant-table-row-expand-icon,
    .indent-level-1 {
      display: none;
    }
  }
`;
const TablesWrapper = styled.div`
  display: flex;
  flex: 1;
`;
const WRITABLE_DEVICE_CATEGORY = ['SENSOR', 'EXECUTIVE'],
  UREADABLE_DEVICE_CATEGORY = ['CONTAINER', 'TRANSPORT'],
  SECTION_TYPE = 'section';
const MODAL_NAMES = {
  UPLOAD_DB: {
    ONE: 'uploadDB',
    ALL: 'uploadAllDB'
  },
  DIFF: 'projectEntitiesDiff'
};

class ConfigWidget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deviceConfig: { id: '', properties: [] },
      isConfigChanged: false,
      configSaving: true
    };
  }

  componentWillReceiveProps(newProps) {
    const { currentDevice, setDeviceConfigError } = newProps;
    let { deviceConfig, isConfigChanged, configSaving } = this.state;
    if (deviceConfig.id && deviceConfig.id === currentDevice.id) {
      deviceConfig.properties = currentDevice.configPropertyViews.map((prop, index) => ({
        ...deviceConfig.properties[index],
        ...prop,
        projectValue:
          configSaving && !setDeviceConfigError
            ? prop.projectValue
            : deviceConfig.properties[index].projectValue
      }));
      isConfigChanged = !configSaving || setDeviceConfigError ? true : false;
    } else {
      deviceConfig = {
        id: currentDevice.id,
        properties: currentDevice.configPropertyViews
          ? currentDevice.configPropertyViews.map(prop => ({ ...prop }))
          : []
      };
      isConfigChanged = false;
    }

    this.setState({
      deviceConfig,
      isConfigChanged
    });
  }

  onPropertyChange = (value, index) => {
    const deviceConfig = { ...this.state.deviceConfig };

    deviceConfig.properties[index].projectValue = value;
    deviceConfig.properties[index].isChanged = true;

    this.setState({
      ...this.state,
      deviceConfig,
      isConfigChanged: true,
      configSaving: false,
      height: 500
    });
  };

  onSetDefaultsClick = () => {
    const deviceConfig = { ...this.state.deviceConfig };
    let isConfigChanged = false;
    deviceConfig.properties.forEach(property => {
      if (property.projectValue !== property.defaultValue) {
        property.projectValue = property.defaultValue;
        property.isChanged = true;
        isConfigChanged = true;
      }
    });

    if (isConfigChanged) {
      this.setState({
        ...this.state,
        deviceConfig,
        isConfigChanged
      });
    }
  };

  onDownloadClick = () => {
    const { deviceConfig } = this.state;
    const { dispatch, projectId } = this.props;

    dispatch(downloadDeviceConfig(projectId, deviceConfig.id));
  };

  onUploadClick = () => {
    const { deviceConfig } = this.state;
    const { dispatch, projectId } = this.props;

    dispatch(uploadDeviceConfig(projectId, deviceConfig.id));
  };

  onUploadDBClick = params => {
    const { dispatch, projectId, currentDevice } = this.props;
    dispatch(uploadDeviceControlDatabase(projectId, currentDevice.id, params));
    this.closeModal(MODAL_NAMES.UPLOAD_DB.ONE);
  };

  onUploadAllDBsClick = params => {
    const { dispatch, projectId } = this.props;
    dispatch(uploadAllDeviceControlDatabases(projectId, params));
    this.closeModal(MODAL_NAMES.UPLOAD_DB.ALL);
  };

  onUploadDBCancel = modalName => {
    this.closeModal(modalName);
  };

  onSaveClick = () => {
    const { deviceConfig } = this.state;
    const { dispatch, projectId } = this.props;
    const changedProperties = {};

    deviceConfig.properties.forEach(property => {
      if (property.isChanged) {
        changedProperties[property.id] = property.projectValue;
      }
    });

    dispatch(setActiveDeviceConfig(projectId, deviceConfig.id, changedProperties));
    this.setState({ configSaving: true });
  };

  exportControlDeviceConfig = () => {
    const { dispatch, projectId, currentDevice } = this.props;
    dispatch(exportControlDeviceConfig(projectId, currentDevice.id));
  };

  // Синхронизировать время прибора
  syncDeviceTime = () => {
    const { projectId, currentDevice, dispatch } = this.props;
    dispatch(setDeviceTime(projectId, currentDevice.id));
  };

  componentWillUnmount() {
    this.props.dispatch(updateCurrentDevice({}));
  }

  openModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalOpen(modalName));
  };

  closeModal = modalName => {
    const { dispatch } = this.props;
    dispatch(modalClose(modalName));
  };

  render() {
    const { currentDevice } = this.props;
    const { deviceConfig, isConfigChanged, height } = this.state;

    const deviceConfigView = { ...deviceConfig };
    const sections = {};
    deviceConfigView.properties = deviceConfigView.properties.filter((prop, index) => {
      prop.key = index;
      if (prop.type === SECTION_TYPE) {
        prop['children'] = [];
        sections[prop.id] = prop;
        return true;
      }
      if (prop.section) {
        sections[prop.section].children.push(prop);
        return false;
      }
      return true;
    });

    let configurableDevice = false;
    if (deviceConfig && deviceConfig.properties && deviceConfig.properties.length > 0)
      configurableDevice = true;

    const writableDeviceCategory = WRITABLE_DEVICE_CATEGORY.includes(currentDevice.deviceCategory);
    const writableDeviceAddress = currentDevice.addressType === 'GENERIC';
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ConfigWrapper
          id="CONFIG_WIDGET"
          onScroll={() => {
            const focusedSelector = document
              .getElementById('CONFIG_WIDGET')
              .querySelector(':focus');
            if (focusedSelector) focusedSelector.blur();
          }}
        >
          <ContolDeviceDBParams
            modalName={MODAL_NAMES.UPLOAD_DB.ONE}
            onSubmit={this.onUploadDBClick}
            onCancel={this.onUploadDBCancel}
          />
          <ContolDeviceDBParams
            modalName={MODAL_NAMES.UPLOAD_DB.ALL}
            onSubmit={this.onUploadAllDBsClick}
            onCancel={this.onUploadDBCancel}
          />
          <ProjectEntitiesDiff modalName={MODAL_NAMES.DIFF} />
          <ConfigMenu
            title={
              currentDevice.id
                ? `${currentDevice.name} (${currentDevice.fullAddressPath})`
                : 'Устройство не выбрано'
            }
            isSetDefaultsActive={configurableDevice && writableDeviceCategory}
            onSetDefaultsClick={this.onSetDefaultsClick}
            isDownloadActive={
              configurableDevice &&
              !UREADABLE_DEVICE_CATEGORY.includes(currentDevice.deviceCategory) &&
              writableDeviceAddress
            }
            onDownloadClick={this.onDownloadClick}
            isUploadActive={
              configurableDevice &&
              !isConfigChanged &&
              writableDeviceCategory &&
              writableDeviceAddress
            }
            onUploadClick={this.onUploadClick}
            isSaveActive={configurableDevice && isConfigChanged}
            onSaveClick={this.onSaveClick}
            isControlDevice={currentDevice.deviceCategory === 'CONTROL'}
            onUploadDBClick={() => this.openModal(MODAL_NAMES.UPLOAD_DB.ONE)}
            onUploadAllDBsClick={() => this.openModal(MODAL_NAMES.UPLOAD_DB.ALL)}
            onExportControlDeviceConfig={this.exportControlDeviceConfig}
            onSyncControlDeviceTime={this.syncDeviceTime}
          />
          <TablesWrapper>
            <Table
              dataSource={deviceConfigView.properties}
              scroll={{ y: height - 62 }}
              rowKey={record => record.id}
              rowClassName={record => (record.type !== SECTION_TYPE ? 'hidden-expand' : null)}
            >
              <Column
                title="Параметр"
                dataIndex="name"
                width={300}
                render={(text, record) => {
                  if (record.type === SECTION_TYPE)
                    return { children: <TextCell text={text} />, props: { colSpan: 4 } };
                  else return <TextCell text={text} />;
                }}
              />
              <Column
                title="По умолчанию"
                dataIndex="defaultValue"
                width={100}
                render={(text, record) => {
                  if (record.type === 'section') {
                    return {
                      children: text,
                      props: { colSpan: 0 }
                    };
                  } else
                    return (
                      <TextCell text={record.possibleValues ? record.possibleValues[text] : text} />
                    );
                }}
              />
              <Column
                title="В системе"
                dataIndex="projectValue"
                width={100}
                render={(text, record) => {
                  if (record.type === 'section') {
                    return {
                      children: text,
                      props: { colSpan: 0 }
                    };
                  } else if (record.possibleValues) {
                    const options = [];
                    const keys = Object.keys(record.possibleValues);
                    keys.forEach(key => {
                      options.push(
                        <Option value={key} key={key}>
                          {record.possibleValues[key]}
                        </Option>
                      );
                    });
                    return (
                      <Select
                        value={text}
                        onSelect={value => this.onPropertyChange(value, record.key)}
                      >
                        {options}
                      </Select>
                    );
                  } else if (record.type === 'integer') {
                    const min = parseInt(record.min, 10) || 0;
                    const max = parseInt(record.max, 10);
                    return (
                      <InputNumber
                        precision={0}
                        value={parseInt(text, 10)}
                        min={min}
                        max={max}
                        onChange={value => this.onPropertyChange(value || min, record.key)}
                      />
                    );
                  } else if (record.type === 'float') {
                    const min = parseFloat(record.min, 10);
                    const max = parseFloat(record.max, 10);
                    return (
                      <InputNumber
                        precision={2}
                        value={parseFloat(text, 10)}
                        min={min}
                        max={max}
                        step={0.01}
                        onChange={value => this.onPropertyChange(value || min, record.key)}
                      />
                    );
                  } else if (record.type === 'text' && !record.readOnly) {
                    return (
                      <Input
                        value={text}
                        onChange={value => this.onPropertyChange(value, record.key)}
                      />
                    );
                  } else if (record.type === 'date') {
                      const seconds = text && text !== '' ? Number(text) : null;
                      const date = seconds ? getDateTimestamp(seconds) : text;
                      return (
                        <span>{date}</span>
                      );
                  } else return <TextCell text={text} />;
                }}
              />
              <Column
                title="В устройстве"
                dataIndex="activeValue"
                width={100}
                render={(text, record) => {
                  if (record.type === 'section') {
                    return {
                      children: text,
                      props: { colSpan: 0 }
                    };
                  } else if (record.type === 'date') {
                      const seconds = text && text !== '' ? Number(text) : null;
                      const date = seconds ? getDateTimestamp(seconds) : text;
                      return (
                        <span>{date}</span>
                      );
                  } else
                    return (
                      <TextCell text={record.possibleValues ? record.possibleValues[text] : text} />
                    );
                }}
              />
            </Table>
          </TablesWrapper>
        </ConfigWrapper>
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    projectId: getActiveProjectId(state),
    currentDevice: getCurrentDevice(state)
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfigWidget);
