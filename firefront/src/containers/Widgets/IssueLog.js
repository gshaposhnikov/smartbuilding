import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { AutoSizer } from 'react-virtualized';
import { createSelector } from 'reselect';

import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Progress from 'components/Progress';
import Table from 'components/Table';

import IssueLogMenu from 'containers/Menus/IssueLogMenu';

import {
  loadIssues,
  deleteFinishedIssues,
  deleteErrorIssues,
  deleteAllIssuesInQueue,
  deleteIssue,
  playIssue,
  pauseIssue
} from 'actions/issues';
import { loadUsers } from 'actions/users';
import { setArchiveEventsFilters, setArchiveEventsSorter } from 'actions/events';

import {
  getActiveDevicesHash,
  getIssuesList,
  getIssuesHash,
  getActiveIndicatorPanelsHash
} from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

class IssueLog extends React.Component {
  state = {
    currentIssueId: null,
    isClearCompletedActive: false,
    isClearQueryActive: false,
    isClearFailedActive: false
  };
  columns = [
    {
      title: 'Добавлено',
      key: 'createDateTime',
      render: (text, record) => this.renderStatusCell(text, record)
    },
    {
      title: 'Запущено',
      dataIndex: 'startDateTime',
      key: 'startDateTime',
      render: text => this.renderTimeCell(text)
    },
    {
      title: 'Завершено',
      dataIndex: 'finishDateTime',
      key: 'finishDateTime',
      render: text => this.renderTimeCell(text)
    },
    {
      title: 'Наименование',
      dataIndex: 'action',
      key: 'action',
      render: (text, record) => this.renderActionCell(record)
    },
    {
      title: 'Прибор/устройство',
      dataIndex: 'deviceId',
      key: 'deviceId',
      render: text => this.renderDeviceCell(text)
    },
    {
      title: 'Пользователь',
      dataIndex: 'userId',
      key: 'userId',
      render: text => this.renderUserCell(text)
    },
    {
      title: 'Действия',
      key: 'actions',
      render: (text, record) => this.renderEventActionsCell(record)
    }
  ];

  renderStatusCell = (text, record) => {
    return (
      <div>
        <Progress status={record.status} percent={+record.progress.toFixed(0)} />{' '}
        {moment(record.createDateTime).format('HH:mm:ss DD.MM.YYYY')}
      </div>
    );
  };

  renderUserCell = userId => {
    const { users } = this.props;
    if (!userId || !users) return null;
    const user = users[userId];
    return user ? user.fullName : null;
  };

  renderDeviceCell = deviceId => {
    const { activeDevices } = this.props;
    const device = activeDevices[deviceId];
    return device ? (
      <div>
        <DeviceTypeTreeItem record={device} />
        {', ' + device.middleAddressPath}
      </div>
    ) : (
      <div />
    );
  };

  /* TODO: Убрать хардкод - получать названия в самой задаче */
  renderActionCell = record => {
    switch (record.action) {
      case 'ENABLE_INDICATOR_REGIONS_GUARD':
      case 'DISABLE_INDICATOR_REGIONS_GUARD':
      case 'ENABLE_INDICATOR_REGIONS_POLLING_STATE':
      case 'DISABLE_INDICATOR_REGIONS_POLLING_STATE':
      case 'ENABLE_INDICATOR_DEVICES_POLLING_STATE':
      case 'DISABLE_INDICATOR_DEVICES_POLLING_STATE':
      case 'PERFORM_INDICATOR_SCENARIO_ACTION': {
        if (record.parameters && record.parameters.indicatorPanelId) {
          const { indicatorPanels } = this.props;
          const indicatorPanel = indicatorPanels[record.parameters.indicatorPanelId];
          if (indicatorPanel) {
            const text = `Действие с индикатором: ${indicatorPanel.name}(${record.parameters.rowNo +
              1},${record.parameters.colNo + 1})`;
            return <span title={text}>{text}</span>;
          }
        }
        return `Действие с индикатором`;
      }
      case 'WRITE_ALL_CONTROL_DEVICES_DATABASE':
      case 'WRITE_CONTROL_DEVICE_DATABASE':
        return 'Запись БД в прибор';
      case 'WRITE_SKUD_DATABASE':
        return 'Запись БД СКУД в прибор';
      case 'RESET_STATE_ON_ALL_DEVICES':
      case 'RESET_STATE_ON_DEVICE': {
        let result = 'Сброс состояния';
        switch (record.parameters.manualResetStateGroup) {
          case 'ALARM':
            return result + ': тревога';
          case 'FIRE':
            return result + ': пожар';
          case 'TESTS':
            return result + ': тесты';
          case 'SCENARIOS':
            return result + ': сценарии';
          case 'MALFUNCTIONS':
            return result + ': неисправности';
          case 'LOCK':
            return result + ': блокировка';
          default:
            return result;
        }
      }
      case 'READ_CONFIG_FROM_DEVICE':
        return 'Чтение параметров с устройства';
      case 'WRITE_CONFIG_TO_DEVICE':
        return 'Запись параметров в устройство';
      case 'ENABLE_DEVICE_POLLING_STATE':
        return 'Включение устройства';
      case 'DISABLE_DEVICE_POLLING_STATE':
        return 'Отключение устройства';
      case 'ENABLE_REGION_GUARD':
        return 'Постановка зоны на охрану';
      case 'DISABLE_REGION_GUARD':
        return 'Снятие зоны с охраны';
      case 'SET_CONTROL_DEVICE_TIME':
        return 'Установка времени с ПК';
      case 'SET_CONTROL_DEVICE_USER_PASSWORD':
        return 'Установка пароля с ПК';
      case 'READ_CONTROL_DEVICE_DATABASE':
        return 'Чтение конфигурации прибора';
      case 'RESET_CONTROL_DEVICE':
        return 'Перезагрузка прибора';
      case 'UPDATE_ALL_CONTROL_DEVICE_FIRMWARE':
      case 'UPDATE_CONTROL_DEVICE_FIRMWARE':
        return 'Обновление прошивки прибора';
      case 'READ_EVENTS':
        return 'Чтение событий прибора';
      case 'PERFORM_DEVICE_ACTION':
        return 'Выполнение действия устройства';
      case 'PERFORM_SCENARIO_ACTION':
        return 'Выполнение действия над сценарием';
      case 'START_READ_ACCESS_KEY':
        return 'Чтение ключа доступа';
      case 'STOP_READ_ACCESS_KEY':
        return 'Отмена чтения ключа доступа';
      case 'SET_DEVICE_STATE':
        return 'Включение/выключение устройства';
      case 'SET_REGION_STATE':
        return 'Включение/выключение зоны';
      case 'SET_APARTMENT_STATE':
        return 'Включение/выключение апартаментов';
      case 'SET_DEVICE_MONITORABLE_VALUE':
        return 'Изменение параметра устройства';
      default:
        return record.action;
    }
  };

  renderTimeCell = value => {
    if (!value) return null;
    const time = moment(value);
    return time.unix() !== 0 ? time.format('HH:mm:ss DD.MM.YYYY') : null;
  };

  renderEventActionsCell = record => {
    const { dispatch } = this.props;
    switch (record.action) {
      case 'READ_EVENTS':
        const { logIds } = record.parameters;
        return (
          <a
            disabled={record.status !== 'FINISHED'}
            onClick={() => {
              dispatch(
                setArchiveEventsFilters([
                  {
                    id: 'controlDeviceIds',
                    value: [record.deviceId]
                  },
                  {
                    id: 'logTypeIds',
                    value: logIds
                  }
                ])
              );
              dispatch(setArchiveEventsSorter('occurred', 'descend'));
              dispatch(push('/op/log'));
            }}
          >
            Отчёт
          </a>
        );
      default:
        return null;
    }
  };

  getRowClassName = record => {
    const { currentIssueId } = this.state;
    return (
      (currentIssueId === record.id ? 'selected-row' : '') +
      (record.statusMessage ? '' : ' hidden-expand')
    );
  };

  onRowClick = record => {
    this.setState({ currentIssueId: record.id });
  };

  onPause = () => {
    const { dispatch } = this.props;
    const { currentIssueId } = this.state;
    dispatch(pauseIssue(currentIssueId));
  };

  onPlay = () => {
    const { dispatch } = this.props;
    const { currentIssueId } = this.state;
    dispatch(playIssue(currentIssueId));
  };

  onDelete = () => {
    const { dispatch } = this.props;
    const { currentIssueId } = this.state;
    dispatch(deleteIssue(currentIssueId));
  };

  onClearQuery = () => {
    const { dispatch } = this.props;
    dispatch(deleteAllIssuesInQueue());
  };

  onDeleteFinished = () => {
    const { dispatch } = this.props;
    dispatch(deleteFinishedIssues());
  };

  onDeleteErrors = () => {
    const { dispatch } = this.props;
    dispatch(deleteErrorIssues());
  };

  componentDidMount = () => {
    const { dispatch, issuesHash, loadIssuesError, users, loadUsersError } = this.props;
    if (!issuesHash && !loadIssuesError) {
      dispatch(loadIssues());
    }
    if (!users && !loadUsersError) {
      dispatch(loadUsers());
    }
  };

  componentWillReceiveProps = nextProps => {
    const { issuesHash, issuesList } = nextProps;
    const { currentIssueId } = this.state;
    if (currentIssueId) {
      if (issuesHash && issuesHash[currentIssueId]) {
        if (this.props.issuesHash !== issuesHash) {
        }
      } else {
        this.setState({ currentIssueId: null });
      }
    }
    if (issuesList && issuesHash === this.props.issuesHash) {
      let isClearCompletedActive, isClearQueryActive, isClearFailedActive;
      issuesList.forEach(issue => {
        switch (issue.status) {
          case 'CREATED':
          case 'PAUSED': {
            isClearQueryActive = true;
            break;
          }
          case 'FAILED':
          case 'CANCELED': {
            isClearFailedActive = true;
            break;
          }
          case 'FINISHED': {
            isClearCompletedActive = true;
            break;
          }
          default: {
            break;
          }
        }
      });
      this.setState({ isClearCompletedActive, isClearQueryActive, isClearFailedActive });
    }
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    const { issuesList, issuesHash } = this.props;
    const { currentIssueId } = this.props;
    if (currentIssueId !== nextState.currentIssueId) return true;
    if (!nextProps.issuesHash) return false;
    if (!nextProps.issuesList.length && !issuesList.length) return false;
    if (nextProps.issuesList === issuesList && nextProps.issuesHash === issuesHash) return false;
    return true;
  };

  render = () => {
    const { issuesHash, issuesList } = this.props;
    const {
      currentIssueId,
      isClearFailedActive,
      isClearQueryActive,
      isClearCompletedActive
    } = this.state;
    const currentIssue =
      currentIssueId && issuesHash && issuesHash[currentIssueId]
        ? issuesHash[currentIssueId]
        : null;
    return (
      <AutoSizer>
        {({ height, width }) => (
          <div style={{ height, width: width - 2, background: 'white' }}>
            <IssueLogMenu
              // Пауза
              isPauseActive={currentIssue && currentIssue.status === 'CREATED'}
              onPauseClick={this.onPause}
              // Выполнить
              isPlayActive={currentIssue && currentIssue.status === 'PAUSE'}
              onPlayClick={this.onPlay}
              // Удалить
              isDeleteActive={currentIssue && currentIssue.status !== 'IN_PROGRESS'}
              onDeleteClick={this.onDelete}
              // Очистить очередь
              isDeleteAllActive={issuesList.length && isClearQueryActive ? true : false}
              onDeleteAllClick={this.onClearQuery}
              // Удалить выполненные
              isDeleteFinishedActive={issuesList.length && isClearCompletedActive ? true : false}
              onDeleteFinishedClick={this.onDeleteFinished}
              // Удалить с ошибками
              isDeleteErrorActive={issuesList.length && isClearFailedActive ? true : false}
              onDeleteErrorClick={this.onDeleteErrors}
            />
            <Table
              dataSource={issuesList}
              rowKey={record => record.id}
              expandedRowRender={record => <span>{record.statusMessage}</span>}
              columns={this.columns}
              rowClassName={this.getRowClassName}
              onRowClick={this.onRowClick}
              scroll={{ y: height - 63 }}
              height={height}
            />
          </div>
        )}
      </AutoSizer>
    );
  };
}

const getFilteredIssues = createSelector(
  getCurrentUser,
  getIssuesList,
  filterEntities
);
const mapStateToProps = state => ({
  activeDevices: getActiveDevicesHash(state) || {},
  deviceProfiles: getDeviceProfileViewsHash(state),
  indicatorPanels: getActiveIndicatorPanelsHash(state),
  issuesList: getFilteredIssues(state),
  issuesHash: getIssuesHash(state),
  users: state.admin.users,
  loadIssuesError: state.errors.loadIssues,
  loadUsersError: state.errors.loadUsersError
});

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IssueLog);
