import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Spin } from 'antd';

import ExpandableTable from 'components/ExpandableTable/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Text from 'components/Text';

import { updateCurrentDevice } from 'actions/devices';

import {
  getActiveProjectId,
  getActiveDeviceTree,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { getCurrentDevice } from 'helpers/widgets';
import { getCurrentUser } from 'helpers/user';
import { filterEntities } from 'helpers/filtration';

const DeviceTreeWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

class ConfigDevicesTree extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'type',
        title: 'Устройство',
        dataIndex: 'type',
        width: 250,
        render: (text, record) => this.deviceTypeColumnRender(text, record)
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'shortAddressPath' }
    ];
    this.state = {
      deviceTree: null
    };
  }

  deviceTypeColumnRender(text, record) {
    return <DeviceTypeTreeItem record={record} stateIconEnabled={false} />;
  }

  onRowClick(device) {
    this.props.dispatch(updateCurrentDevice(device.isRoot ? {} : device));
  }

  filterDeviceTree = (currentUser, deviceTree = []) => {
    return filterEntities(currentUser, deviceTree).map(device => {
      if (device.children && device.children.length) {
        return {
          ...device,
          children: this.filterDeviceTree(currentUser, device.children)
        };
      } else return device;
    });
  };

  componentDidMount = () => {
    const { deviceTree, currentUser } = this.props;
    if (!this.state.deviceTree && deviceTree && deviceTree.length) {
      const filteredDeviceTree = this.filterDeviceTree(currentUser, deviceTree);
      this.setState({ deviceTree: filteredDeviceTree });
    }
  };

  componentWillReceiveProps = nextProps => {
    if (
      (!this.state.deviceTree && nextProps.deviceTree && nextProps.deviceTree.length) ||
      (this.state.deviceTree && !nextProps.deviceTree)
    ) {
      const filteredDeviceTree = this.filterDeviceTree(nextProps.currentUser, nextProps.deviceTree);
      this.setState({ deviceTree: filteredDeviceTree });
    }
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (
      (!this.state.deviceTree && nextState.deviceTree) ||
      this.state.deviceTree !== nextState.deviceTree
    ) {
      return true;
    }
    if (nextProps.currentDevice.id !== this.props.currentDevice.id) {
      return true;
    }
    return false;
  };

  render() {
    const { deviceTree } = this.state;
    const { currentDevice } = this.props;

    if (deviceTree) {
      return (
        <DeviceTreeWrapper>
          <ExpandableTable
            getEntitiesHash={getActiveDevicesHash}
            columns={this.columns}
            nodes={deviceTree}
            rowSelection={currentDevice.id}
            onRowClick={this.onRowClick.bind(this)}
          />
        </DeviceTreeWrapper>
      );
    } else {
      return (
        <Spin>
          <Text>Нет данных</Text>
        </Spin>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    currentUser: getCurrentUser(state),
    currentDevice: getCurrentDevice(state),
    deviceTree: getActiveDeviceTree(state),
    projectId: getActiveProjectId(state)
  };
};

export default connect(mapStateToProps)(ConfigDevicesTree);
