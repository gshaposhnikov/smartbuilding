import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { createSelector } from 'reselect';

import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Text from 'components/Text';
import Widget from 'components/Widget';
import ExpandableTable from 'components/ExpandableTable/Table';

import EnabledDevicesMenu from 'containers/Menus/EnabledDevicesMenu';

import {
  getDevicesTree,
  getDeviceProfile,
  filterDevicesLeavingParents,
  getDevicesByPollingState
} from 'helpers/device';
import {
  getActiveProjectId,
  getActiveDeviceList,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { filterEntities, availableByFilterTags } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

import { setDevicePollingState } from 'actions/devices';

class EnabledDevicesTree extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'type',
        title: 'Устройство',
        dataIndex: 'type',
        className: 'type-column',
        width: 255,
        render: this.deviceTypeColumnRender
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'shortAddressPath' },
      { key: 'regionName', title: 'Зона', dataIndex: 'regionName' }
      /* TODO: колонка с примечанием */
    ];
    this.state = {
      height: 500,
      currentDeviceId: null,
      deviceTree: []
    };
  }

  componentDidMount = () => {
    const { deviceProfiles, deviceList } = this.props;
    if (deviceList && deviceList.length && Object.keys(deviceProfiles).length) {
      const deviceTree = getDevicesTree(this.getDevicesTreeByPollingState(this.props), item => {
        item.isActive = item.statePolling && getDeviceProfile(deviceProfiles, item).ignorable;
        return item;
      });
      this.setState({ deviceTree });
    }
  };

  componentWillReceiveProps = nextProps => {
    const { disabledDeviceList } = this.props;
    const { deviceTree } = this.state;
    if (
      nextProps.deviceList &&
      nextProps.deviceList.length &&
      Object.keys(nextProps.deviceProfiles).length
    ) {
      if (disabledDeviceList.length !== nextProps.disabledDeviceList.length || !deviceTree.length) {
        let { currentDeviceId } = this.state;
        const newDeviceTree = getDevicesTree(this.getDevicesTreeByPollingState(nextProps), item => {
          item.isActive =
            item.statePolling && getDeviceProfile(nextProps.deviceProfiles, item).ignorable;
          return item;
        });
        if (currentDeviceId) {
          const currentDevice = nextProps.devicesHash[currentDeviceId];
          if (currentDevice && !nextProps.devicesHash[currentDeviceId].statePolling) {
            currentDeviceId = null;
          }
        }
        this.setState({ deviceTree: newDeviceTree, currentDeviceId });
      }
    }
  };

  deviceTypeColumnRender = (text, record) => (
    <DeviceTypeTreeItem record={record} stateIconEnabled={false} />
  );

  getDevicesTreeByPollingState = props => {
    const { deviceList, deviceProfiles, currentUser } = props;
    return filterDevicesLeavingParents(deviceList, device => {
      const deviceProfile = getDeviceProfile(deviceProfiles, device);
      return (
        device.statePolling &&
        deviceProfile &&
        deviceProfile.ignorable &&
        availableByFilterTags(currentUser, device)
      );
    });
  };

  getRowClassName = record => {
    const { currentDeviceId } = this.state;
    return currentDeviceId === record.id ? 'selected-row' : '';
  };

  onRowClick = record => {
    const { currentDeviceId } = this.state;
    if (!record.isActive) return;
    if (currentDeviceId === record.id) return;
    this.setState({
      currentDeviceId: record.id
    });
  };

  disableDevice = deviceId => {
    const { projectId, dispatch } = this.props;
    dispatch(setDevicePollingState(projectId, deviceId, null, false));
    this.setState({ currentDeviceId: null });
  };

  onRowDoubleClick = record => {
    if (record.isActive) this.disableDevice(record.id);
  };

  onDisableClick = () => {
    const { currentDeviceId } = this.state;
    if (currentDeviceId) this.disableDevice(currentDeviceId);
  };

  shouldComponentUpdate = (nextProps, nextState) => nextState !== this.state;

  render() {
    const { deviceTree, currentDeviceId, height } = this.state;
    const { deviceList } = this.props;
    if (deviceList.length) {
      return (
        <Widget onHeightReady={height => this.setState({ height })}>
          <EnabledDevicesMenu isActive={!!currentDeviceId} onDisableDevice={this.onDisableClick} />
          {deviceTree.length ? (
            <ExpandableTable
              forceWidth
              onRowClick={this.onRowClick}
              onRowDoubleClick={this.onRowDoubleClick}
              rowSelection={currentDeviceId}
              columns={this.columns}
              nodes={deviceTree}
              height={height - 61}
            />
          ) : null}
        </Widget>
      );
    } else {
      return (
        <Spin>
          <Text>Нет данных</Text>
        </Spin>
      );
    }
  }
}

const getDisabledDevices = createSelector(
  getActiveDeviceList,
  activeDeviceList => getDevicesByPollingState(activeDeviceList || [], false)
);

const getFilteredDeviceList = createSelector(
  getCurrentUser,
  getDisabledDevices,
  filterEntities
);

const mapStateToProps = state => ({
  currentUser: getCurrentUser(state),
  devicesHash: getActiveDevicesHash(state) || {},
  deviceList: getActiveDeviceList(state) || [],
  disabledDeviceList: getFilteredDeviceList(state),
  projectId: getActiveProjectId(state),
  deviceProfiles: getDeviceProfileViewsHash(state)
});

export default connect(mapStateToProps)(EnabledDevicesTree);
