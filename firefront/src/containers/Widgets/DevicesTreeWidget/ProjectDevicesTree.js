import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Menu, Dropdown, Icon, Spin, Modal } from 'antd';
import PropTypes from 'prop-types';
import { isEqual } from 'lodash';

import { EditableCell } from 'components/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Text from 'components/Text';
import Widget from 'components/Widget';
import ExpandableTable from 'components/ExpandableTable/Table';

import NewDeviceForm from 'containers/Forms/NewDeviceForm';
import ChooseLogTypesForm from 'containers/Forms/ChooseLogTypesForm';
import ChooseFirmwareForm from 'containers/Forms/ChooseFirmwareForm';
import ProjectDevicesTreeContextMenu from 'containers/ContextMenus/ProjectDevicesTreeContextMenu';
import ProjectDeviceDetails from 'containers/Widgets/DeviceDetails/ProjectDeviceDetails';
import DevicePasswordForm from 'containers/Forms/DevicePasswordForm';
import DeviceLinksForm from 'containers/Forms/DeviceLinksForm';
import RegionForm from 'containers/Forms/RegionForm';
// Хелперы
import { checkCompletion } from 'helpers/form';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
// Экшены
import { modalOpen, modalClose } from 'actions/modals';
import {
  importControlDeviceConfig,
  updateCurrentDevice,
  updateDeviceAddressPath,
  updateDeviceType,
  createDevice,
  deleteDevice,
  setDeviceTime,
  resetDevice,
  updateDraggableDevice,
  setDeviceDisabled,
  updateDeviceFw,
  readEvents,
  setDeviceControlPassword,
  updateDeviceLinks,
  updateDeviceSubsystem
} from 'actions/devices';
import {
  checkCurrentProjectIsActive,
  getCurrentProjectDeviceTree,
  getCurrentProjectRegionsHash,
  getCurrentProjectRegionsGroupedBySubsystem,
  getCurrentProjectDevicesHash
} from 'helpers/currentProject';
import { updateDevicePlanLayouts } from 'actions/devices';
import { newDeviceRegion, deleteDeviceRegion, createRegionAndConnectDevice } from 'actions/regions';

import SUBSYSTEMS from 'constants/subsystems';

const SUBSYSTEM_TAGS = Object.values(SUBSYSTEMS).filter(subsystem => subsystem.isFilterTag);
const MODAL_NAMES = {
  IMPORT_CONFIG: 'importConfig',
  CREATE_DEVICE: 'createDevice',
  UPDATE_FW: 'updateFW',
  READ_EVENT_LOG: 'readEventLog',
  DEVICE_PASSWORD: 'devicePassword',
  DEVICE_LINKS: 'deviceLinks',
  CREATE_REGION: 'createRegion'
};
const ATTACHABLE_DEVICE_CATEGORY = {
  SENSOR: true,
  CONTROL: true,
  EXECUTIVE: true,
  VIRTUAL_CONTAINER: true
};

class ProjectDevicesTree extends Component {
  static propTypes = {
    regionsHash: PropTypes.object,
    deviceTree: PropTypes.array,
    currentDevice: PropTypes.object,
    currentProjectId: PropTypes.string,
    isActiveProject: PropTypes.bool,
    deviceProfileViewsHash: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'type',
        title: 'Тип устройства',
        dataIndex: 'type',
        width: 255,
        stepper: true,
        render: this.deviceTypeColumnRender
      },
      {
        key: 'shortAddressPath',
        title: 'Адрес',
        dataIndex: 'shortAddressPath',
        render: this.renderAddressCell
      },
      {
        key: 'regionName',
        title: 'Зона',
        dataIndex: 'regionName',
        render: this.renderRegionCell
      },
      {
        key: 'subsystem',
        title: 'Подсистема',
        dataIndex: 'subsystem',
        render: this.renderSubsystemCell
      }
      /* TODO: колонка с примечанием */
    ];
    this.state = {
      height: 500,
      selectedRowKeys: null
    };
    window.addEventListener('keydown', this.onKeyDown, true);
  }

  componentDidMount = () => {
    const { deviceTree } = this.props;
    if (!this.state.deviceTree && deviceTree && deviceTree.length) {
      this.setState({ deviceTree });
    }
  };

  componentWillReceiveProps = nextProps => {
    const {
      modals,
      devicesHash,
      createDeviceCompleted,
      importControlDeviceConfigCompleted,
      createRegionAndConnectDeviceCompleted,
      dispatch,
      currentProjectId,
      isActiveProject
    } = nextProps;
    if (
      currentProjectId !== this.props.currentProjectId ||
      isActiveProject !== this.props.isActiveProject ||
      (!this.state.deviceTree && nextProps.deviceTree && nextProps.deviceTree.length) ||
      Object.keys(devicesHash).length !== Object.keys(this.props.devicesHash).length ||
      !isEqual(nextProps.currentDevice, this.props.currentDevice)
    ) {
      this.setState({ deviceTree: nextProps.deviceTree });
    }

    if (modals[MODAL_NAMES.CREATE_DEVICE] && createDeviceCompleted)
      dispatch(modalClose(MODAL_NAMES.CREATE_DEVICE));
    if (modals[MODAL_NAMES.IMPORT_CONFIG] && importControlDeviceConfigCompleted)
      dispatch(modalClose(MODAL_NAMES.IMPORT_CONFIG));
    if (modals[MODAL_NAMES.CREATE_REGION] && createRegionAndConnectDeviceCompleted)
      dispatch(modalClose(MODAL_NAMES.CREATE_REGION));
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (
      (!this.state.deviceTree && nextState.deviceTree) ||
      this.state.deviceTree !== nextState.deviceTree
    )
      return true;
    if (!isEqual(nextProps.currentDevice, this.props.currentDevice)) return true;
    if (this.state.height !== nextState.height) return true;
    if (this.props.deviceProfileViewsHash !== nextProps.deviceProfileViewsHash) return true;

    return false;
  };

  // Подключить устройство
  onCreateDevice = device => {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    dispatch(modalOpen(MODAL_NAMES.CREATE_DEVICE));
  };

  createDevice = values => {
    const { currentProjectId, currentDevice } = this.props;
    const device = {
      deviceProfileId: values.deviceProfileId,
      parentDeviceId: currentDevice.isRoot ? null : currentDevice.id,
      lineNo: values.lineNo,
      lineAddress: values.lineAddress,
      count: values.count
    };
    this.props.dispatch(createDevice(currentProjectId, device));
  };

  createRegionAndConnectDevice = values => {
    const { currentProjectId, currentDevice, dispatch } = this.props;
    dispatch(createRegionAndConnectDevice(currentProjectId, values, currentDevice.id));
  };

  openDeviceLinksDialog = device => {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    dispatch(modalOpen(MODAL_NAMES.DEVICE_LINKS));
  };

  updateDeviceLinks = deviceLinks => {
    const { currentProjectId, currentDevice, dispatch } = this.props;
    dispatch(updateDeviceLinks(currentProjectId, currentDevice.id, deviceLinks));
    dispatch(modalClose(MODAL_NAMES.DEVICE_LINKS));
  };

  // Удалить устройство
  deleteDevice = (device, treeItem) => {
    const { currentProjectId } = this.props;
    if (
      treeItem.deviceCategory !== 'VIRTUAL_CONTAINER' &&
      treeItem.children &&
      treeItem.children.length
    ) {
      Modal.confirm({
        title: 'Удаление устройства',
        content: (
          <div>
            <div>
              Удалить устройство{' '}
              <b>
                {device.name} ({device.shortAddressPath})
              </b>{' '}
              ?
            </div>
            <div>(Также будут удалены дочерние устройства)</div>
          </div>
        ),
        okText: 'Удалить',
        onCancel: () => {},
        cancelText: 'Отмена',
        maskClosable: false,
        onOk: () => {
          this.props.dispatch(deleteDevice(currentProjectId, device.id));
        }
      });
    } else {
      this.props.dispatch(deleteDevice(currentProjectId, device.id));
    }
  };

  // Синхронизировать время прибора
  syncDeviceTime = device => {
    const { currentProjectId, dispatch } = this.props;
    dispatch(setDeviceTime(currentProjectId, device.id));
  };

  resetDevice = device => {
    const { currentProjectId, dispatch } = this.props;
    dispatch(resetDevice(currentProjectId, device.id));
  };

  openUpdateDeviceFwDialog = device => {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    dispatch(modalOpen(MODAL_NAMES.UPDATE_FW));
  };

  doUpdateDeviceFW = values => {
    const { fileContent, updateAllControlDeviceFw } = values;
    const { dispatch, currentProjectId, currentDevice } = this.props;
    dispatch(
      updateDeviceFw(
        currentProjectId,
        updateAllControlDeviceFw ? null : currentDevice.id,
        fileContent
      )
    );
    dispatch(modalClose(MODAL_NAMES.UPDATE_FW));
  };

  readEventsFromLog = device => {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    dispatch(modalOpen(MODAL_NAMES.READ_EVENT_LOG));
  };

  doReadEvents = values => {
    const selectedMap = values;
    const { dispatch, currentProjectId, currentDevice } = this.props;

    const logTypes = [];
    for (let logName in selectedMap) {
      if (selectedMap[logName] === true) logTypes.push(logName);
    }
    dispatch(readEvents(currentProjectId, currentDevice.id, logTypes));
    dispatch(modalClose(MODAL_NAMES.READ_EVENT_LOG));
  };

  // Разместить устройство на плане
  attachDeviceToPlan = device => {
    const { currentProjectId, regionsHash, dispatch } = this.props;
    const regionView = regionsHash[device.regionId];
    if (regionView && regionView.planLayouts.length) {
      const regionPlanLayout = regionView.planLayouts[regionView.planLayouts.length - 1];
      let minX, minY;
      regionPlanLayout.points.forEach(point => {
        if (typeof minX === 'undefined' || minX > point.x) minX = point.x;
        if (typeof minY === 'undefined' || minY > point.y) minY = point.y;
      });
      const newPlanLayout = {
        planId: regionPlanLayout.planId,
        coordinatePoint: { x: minX, y: minY }
      };
      const newPlanLayouts = [...device.planLayouts, newPlanLayout];
      let event = new CustomEvent('DEVICE_UPDATE_PLAN_LAYOUTS', {
        detail: { planId: regionPlanLayout.planId }
      });
      document.dispatchEvent(event);
      dispatch(updateDevicePlanLayouts(currentProjectId, device.id, newPlanLayouts));
    }
  };

  // Перетащить устройство на план
  dragDeviceToPlan = device => {
    const { dispatch, draggableDeviceId } = this.props;
    if (device || draggableDeviceId) {
      const newDraggableDeviceId =
        device && draggableDeviceId !== device.id && !device.isRoot ? device.id : null;
      dispatch(updateDraggableDevice(newDraggableDeviceId));
    }
  };

  // Задать пароль
  setControlDevicePassword = values => {
    const { dispatch, currentProjectId, currentDevice } = this.props;
    const { username, password } = values;
    dispatch(setDeviceControlPassword(currentProjectId, currentDevice.id, username, password));
    dispatch(modalClose(MODAL_NAMES.DEVICE_PASSWORD));
  };

  onSetControlDevicePassword = device => {
    const { dispatch } = this.props;
    dispatch(modalOpen(MODAL_NAMES.DEVICE_PASSWORD));
    dispatch(updateCurrentDevice(device));
  };

  onDeviceTypeSelect = (deviceTreeItem, item) => {
    const { currentProjectId } = this.props;
    const deviceId = deviceTreeItem.id;
    const newDeviceProfileId = deviceTreeItem.acceptableDeviceProfiles[item.key].deviceProfileId;
    this.props.dispatch(updateDeviceType(currentProjectId, deviceId, newDeviceProfileId));
  };

  updateAddressPath = (newDeviceAddressPath, deviceId) => {
    const { currentProjectId } = this.props;
    this.props.dispatch(updateDeviceAddressPath(currentProjectId, deviceId, newDeviceAddressPath));
  };

  renderAddressCell = (text, device, treeItem) => {
    const { isActiveProject } = this.props;
    let { editableAddress } = device;
    let shortAddressPath;
    if (device.deviceCategory === 'CONTAINER') {
      const deviceChildren = device.children ? device.children : treeItem.children;
      if (deviceChildren && deviceChildren.length) {
        shortAddressPath = deviceChildren[0].shortAddressPath;
      } else {
        editableAddress = false;
      }
    }
    return (
      <EditableCell
        onChange={text => this.updateAddressPath(text, device.id)}
        value={text}
        editable={editableAddress && !isActiveProject}
        temp={shortAddressPath}
      />
    );
  };

  renderRegionCell = (
    text,
    { id: deviceId, regionId, attachableToRegion, subsystem, disabled }
  ) => {
    const { regionsGroupedBySubsystem = {}, isActiveProject, dispatch } = this.props;
    if (attachableToRegion) {
      if (isActiveProject || disabled) return text;
      const regionsByDeviceSubsystem = regionsGroupedBySubsystem[subsystem] || [];
      return (
        <Dropdown
          overlay={
            <Menu
              onClick={({ key }) => {
                if (key === 'NEW') {
                  dispatch(modalOpen(MODAL_NAMES.CREATE_REGION));
                } else if (key === 'NONE') {
                  if (regionId) this.disconnectDeviceFromRegion(deviceId, regionId);
                } else if (key !== regionId) {
                  this.connectDeviceToRegion(deviceId, key);
                }
              }}
            >
              <Menu.Item key="NONE">Нет</Menu.Item>
              <Menu.Item key="NEW">Создать новую...</Menu.Item>
              <Menu.Divider />
              {regionsByDeviceSubsystem.map((region, i) => (
                <Menu.Item className="ellipsis-content" title={region.name} key={region.id}>
                  {`${region.index}. ${region.name}`}
                </Menu.Item>
              ))}
            </Menu>
          }
          trigger={['click']}
        >
          <a className="ant-dropdown-link ellipsis-content" href="#" draggable={false}>
            <Icon type="down" /> {text || 'Добавить'}
          </a>
        </Dropdown>
      );
    }
    return null;
  };

  renderSubsystemCell = (text, { id: deviceId, subsystem, customSubsystem, deviceProfileId }) => {
    const { isActiveProject, deviceProfileViewsHash, dispatch, currentProjectId } = this.props;
    if (subsystem) {
      const deviceSubsystem = customSubsystem || subsystem;
      const deviceSubsystemName = SUBSYSTEMS[deviceSubsystem].name;
      if (!isActiveProject && deviceProfileViewsHash) {
        const deviceProfileView = deviceProfileViewsHash[deviceProfileId];
        if (
          deviceProfileView &&
          deviceProfileView.deviceProfile.subsystem === 'GENERAL' &&
          !deviceProfileView.deviceProfile.internalDevices.length &&
          deviceProfileView.deviceProfile.outputConnectionType === 'NONE'
        ) {
          return (
            <Dropdown
              overlay={
                <Menu
                  onClick={({ key }) => {
                    const newCustomSubsystem = key === 'GENERAL' ? '' : key;
                    dispatch(updateDeviceSubsystem(currentProjectId, deviceId, newCustomSubsystem));
                  }}
                >
                  {SUBSYSTEM_TAGS.map(subsystemTag => (
                    <Menu.Item key={subsystemTag.id} disabled={subsystemTag.id === deviceSubsystem}>
                      {subsystemTag.name}
                    </Menu.Item>
                  ))}
                </Menu>
              }
              trigger={['click']}
            >
              <a className="ant-dropdown-link ellipsis-content" href="#" draggable={false}>
                {deviceSubsystemName} <Icon type="down" />
              </a>
            </Dropdown>
          );
        }
      }
      return deviceSubsystemName;
    }
    return null;
  };

  // Привязать устройство к зоне
  connectDeviceToRegion = (deviceId, regionId) => {
    const { currentProjectId } = this.props;
    const newConnection = {
      projectId: currentProjectId,
      regionId,
      deviceId
    };
    this.props.dispatch(newDeviceRegion(newConnection));
  };
  // Отвзяать устройство от зоны
  disconnectDeviceFromRegion = (deviceId, regionId) => {
    const { currentProjectId } = this.props;
    const deleteConnection = {
      projectId: currentProjectId,
      regionId,
      deviceId
    };
    this.props.dispatch(deleteDeviceRegion(deleteConnection));
  };

  // Вкл./откл. устройства в проекте
  onChangeDeviceSwitcher = (deviceId, disabled) => {
    const { currentProjectId, dispatch } = this.props;
    dispatch(setDeviceDisabled(currentProjectId, deviceId, disabled));
  };

  onImportControlDeviceConfig = device => {
    const { dispatch } = this.props;
    dispatch(modalOpen(MODAL_NAMES.IMPORT_CONFIG));
    dispatch(updateCurrentDevice(device));
  };

  importControlDeviceConfig = controlDeviceEntities => {
    const { currentProjectId, dispatch } = this.props;
    dispatch(importControlDeviceConfig(currentProjectId, controlDeviceEntities));
  };

  deviceTypeColumnRender = (text, record, treeItem) => {
    const { isActiveProject } = this.props;
    const draggable =
      record.deviceProfileId &&
      ATTACHABLE_DEVICE_CATEGORY[record.deviceCategory] &&
      !isActiveProject;
    if (
      record.acceptableDeviceProfiles &&
      record.acceptableDeviceProfiles.length > 1 &&
      !isActiveProject
    ) {
      const dropDownItems = record.acceptableDeviceProfiles.map(
        deviceProfileItem => deviceProfileItem.deviceProfileName
      );
      return (
        <DeviceTypeTreeItem
          dropDownEnabled={true}
          dropDownItems={dropDownItems}
          onDropDownSelect={index => this.onDeviceTypeSelect(record, index)}
          contextMenuEnabled={true}
          contextMenuName="newDeviceContext"
          record={record}
          treeItem={treeItem}
          onDragStart={() => this.dragDeviceToPlan(record)}
          draggable={draggable}
          stateIconEnabled={false}
          attachedIconEnabled={!isActiveProject}
          showDeviceSwitcher={!isActiveProject && record.canBeDisabled}
          onChangeDeviceSwitcher={this.onChangeDeviceSwitcher}
        />
      );
    } else {
      return (
        <DeviceTypeTreeItem
          contextMenuEnabled={true}
          contextMenuName="newDeviceContext"
          treeItem={treeItem}
          record={record}
          onDragStart={() => this.dragDeviceToPlan(record)}
          draggable={draggable}
          stateIconEnabled={false}
          attachedIconEnabled={!isActiveProject}
          showDeviceSwitcher={!isActiveProject && record.canBeDisabled}
          onChangeDeviceSwitcher={this.onChangeDeviceSwitcher}
        />
      );
    }
  };

  onRowClick = device => {
    this.props.dispatch(updateCurrentDevice(device.isRoot ? {} : device));
  };

  onKeyDown = e => {
    if (e.keyCode === 27) {
      this.dragDeviceToPlan();
    }
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    window.removeEventListener('keydown', this.onKeyDown, true);
    dispatch(updateDraggableDevice(null));
  };

  render = () => {
    const { height, deviceTree } = this.state;
    const {
      currentProjectId,
      currentDevice,
      isActiveProject,
      loadCurrentProjectInProgress,
      deviceProfileViewsHash
    } = this.props;
    return currentProjectId && deviceTree && !loadCurrentProjectInProgress ? (
      <Widget
        className="project-devices-tree-widget"
        onHeightReady={height => this.setState({ height })}
      >
        <RegionForm
          modalName={MODAL_NAMES.CREATE_REGION}
          forceSubsystem={currentDevice.id ? currentDevice.subsystem : ''}
          onSubmit={this.createRegionAndConnectDevice}
        />
        <NewDeviceForm modalName={MODAL_NAMES.CREATE_DEVICE} onSubmit={this.createDevice} />
        <NewDeviceForm
          modalName={MODAL_NAMES.IMPORT_CONFIG}
          isImport
          onSubmit={this.importControlDeviceConfig}
        />
        <ChooseFirmwareForm onSubmit={this.doUpdateDeviceFW} modalName={MODAL_NAMES.UPDATE_FW} />
        <ChooseLogTypesForm
          onSubmit={this.doReadEvents}
          modalName={MODAL_NAMES.READ_EVENT_LOG}
          logTypes={
            currentDevice.id &&
            currentDevice.deviceProfileId &&
            deviceProfileViewsHash[currentDevice.deviceProfileId]
              ? deviceProfileViewsHash[currentDevice.deviceProfileId].deviceProfile.logTypes
              : null
          }
        />
        <DevicePasswordForm
          modalName={MODAL_NAMES.DEVICE_PASSWORD}
          onSubmit={this.setControlDevicePassword}
        />
        <DeviceLinksForm modalName={MODAL_NAMES.DEVICE_LINKS} onSubmit={this.updateDeviceLinks} />
        <ProjectDevicesTreeContextMenu
          onConnectDevice={this.onCreateDevice}
          onImportControlDeviceConfig={this.onImportControlDeviceConfig}
          onDeleteDevice={this.deleteDevice}
          onSyncDeviceTime={this.syncDeviceTime}
          onResetDevice={this.resetDevice}
          onUpdateDeviceFW={this.openUpdateDeviceFwDialog}
          onAttachToPlan={this.attachDeviceToPlan}
          onReadEvents={this.readEventsFromLog}
          onDragDevice={this.dragDeviceToPlan}
          onUpdateDeviceLinks={this.openDeviceLinksDialog}
          onSetControlDevicePassword={this.onSetControlDevicePassword}
          isActiveProject={isActiveProject}
        />
        <ExpandableTable
          getEntitiesHash={getCurrentProjectDevicesHash}
          rowSelection={currentDevice.id}
          columns={this.columns}
          nodes={deviceTree}
          height={height - 301}
          onRowClick={this.onRowClick}
        />
        <ProjectDeviceDetails />
      </Widget>
    ) : (
      <Spin>
        <Text>Нет данных</Text>
      </Spin>
    );
  };
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    currentProjectId: state.currentProjectId,
    currentDevice: state.widgets.currentDevice,
    devicesHash: getCurrentProjectDevicesHash(state),
    deviceTree: getCurrentProjectDeviceTree(state),
    regionsHash: getCurrentProjectRegionsHash(state),
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    modals: state.modals,
    draggableDeviceId: state.widgets.draggableDeviceId,
    regionsGroupedBySubsystem: getCurrentProjectRegionsGroupedBySubsystem(state),

    loadCurrentProjectInProgress: state.inProgress.loadCurrentProject,
    createDeviceCompleted: checkCompletion(
      state.inProgress.createDevice,
      state.errors.createDevice
    ),
    importControlDeviceConfigCompleted: checkCompletion(
      state.inProgress.importConfig,
      state.errors.importConfig
    ),
    createRegionAndConnectDeviceCompleted: checkCompletion(
      state.inProgress.createRegionAndConnectDevice,
      state.errors.createRegionAndConnectDevice
    )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDevicesTree);
