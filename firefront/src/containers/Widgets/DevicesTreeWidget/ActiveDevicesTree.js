import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { push } from 'react-router-redux';
import { AutoSizer } from 'react-virtualized';

import ExpandableTable from 'components/ExpandableTable/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import ActiveDevicesTreeContextMenu from 'containers/ContextMenus/ActiveDevicesTreeContextMenu';
import ChooseLogTypesForm from 'containers/Forms/ChooseLogTypesForm';
import Text from 'components/Text';
import Widget from 'components/Widget';

import ActiveDeviceDetails from 'containers/Widgets/DeviceDetails/ActiveDeviceDetails';
import DeviceTreeMenu from 'containers/Menus/DeviceTreeMenu';
import RegionViewerWidget from 'containers/Widgets/RegionViewerWidget';

import { modalOpen, modalClose } from 'actions/modals';
import { updateCurrentDevice, setDevicePollingState, readEvents } from 'actions/devices';
import { setArchiveEventsFilters } from 'actions/events';

import { getCurrentUser } from 'helpers/user';
import { getFlatTree, checkMalfunction } from 'helpers/device';
import {
  getActiveRegions,
  getActiveProjectId,
  getActiveDeviceTree,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { getCurrentDevice, getViewMode, getCurrentRegion } from 'helpers/widgets';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { filterEntities } from 'helpers/filtration';

const deviceModals = {
  readEventsModalName: 'readEvents'
};

const DEVICE_TREE_WRAPPER_STYLES = { display: 'flex', flex: 1, width: '100%' };

class ActiveDevicesTree extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'type',
        title: 'Тип устройства',
        dataIndex: 'type',
        className: 'type-column',
        width: 250,
        render: (text, record) => this.deviceTypeColumnRender(text, record)
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'shortAddressPath' },
      { key: 'regionName', title: 'Зона', dataIndex: 'regionName' }
      /* TODO: колонка с примечанием */
    ];
    this.scrollTo = { state: false, expandedRows: [], id: null };
    this.state = {
      height: 500
    };
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice({}));
  }

  onRowClick(record) {
    this.props.dispatch(updateCurrentDevice(record.isRoot ? {} : record));
  }

  deviceTypeColumnRender(text, record) {
    return (
      <DeviceTypeTreeItem
        contextMenuEnabled={Boolean(record.activeStateViews && record.activeStateViews.length)}
        contextMenuName="activeDeviceContext"
        record={record}
        showDisabledDevices={true}
      />
    );
  }

  enableDevice(device) {
    const { projectId, dispatch } = this.props;
    if (device.id) dispatch(setDevicePollingState(projectId, device.id, null, true));
  }

  disableDevice(device) {
    const { projectId, dispatch } = this.props;
    if (device.id) dispatch(setDevicePollingState(projectId, device.id, null, false));
  }

  showEvents(device) {
    const { dispatch } = this.props;
    dispatch(
      setArchiveEventsFilters([
        {
          id: device.deviceCategory === 'CONTROL' ? 'controlDeviceIds' : 'deviceIds',
          value: [device.id]
        }
      ])
    );
    dispatch(push('/op/log'));
  }

  readEventsFromLog(device) {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    dispatch(modalOpen(deviceModals.readEventsModalName));
  }

  doReadEvents(values) {
    const selectedMap = values;
    const { dispatch, projectId, currentDevice } = this.props;

    const logTypes = [];
    for (let logName in selectedMap) {
      if (selectedMap[logName] === true) logTypes.push(logName);
    }
    dispatch(readEvents(projectId, currentDevice.id, logTypes));
    dispatch(modalClose(deviceModals.readEventsModalName));
  }

  goToPrevMalfunction(record) {
    const { deviceTree } = this.props;
    const deviceList = getFlatTree(deviceTree);
    const currentDeviceIndex = deviceList.findIndex(device => device.id === record.id);
    for (let i = currentDeviceIndex - 1; i > 0; i--) {
      const prevMalfunction = checkMalfunction(deviceList[i]);
      if (prevMalfunction) {
        this.goToMalfunction(prevMalfunction);
        return;
      }
    }
    for (let j = deviceList.length - 1; j > currentDeviceIndex; j--) {
      const prevMalfunction = checkMalfunction(deviceList[j]);
      if (prevMalfunction) {
        this.goToMalfunction(prevMalfunction);
        return;
      }
    }
  }

  goToNextMalfunction(record) {
    const { deviceTree } = this.props;
    const deviceList = getFlatTree(deviceTree);
    const currentDeviceIndex = deviceList.findIndex(device => device.id === record.id);
    for (let i = currentDeviceIndex + 1; i < deviceList.length - 1; i++) {
      const nextMalfunction = checkMalfunction(deviceList[i]);
      if (nextMalfunction) {
        this.goToMalfunction(nextMalfunction);
        return;
      }
    }
    for (let j = 0; j < currentDeviceIndex; j++) {
      const nextMalfunction = checkMalfunction(deviceList[j]);
      if (nextMalfunction) {
        this.goToMalfunction(nextMalfunction);
        return;
      }
    }
  }

  goToMalfunction(device) {
    const { dispatch } = this.props;
    dispatch(updateCurrentDevice(device));
    /* TODO: перенести scrollTo в state */
    this.scrollTo.state = true;
    this.scrollTo.id = device.id;
    this.scrollTo.expandedRows = device.parentIds || [];
  }

  filterDeviceTree = (currentUser, deviceTree = []) => {
    return filterEntities(currentUser, deviceTree).map(device => {
      if (device.children && device.children.length) {
        return {
          ...device,
          children: this.filterDeviceTree(currentUser, device.children)
        };
      } else return device;
    });
  };

  componentDidMount = () => {
    const { deviceTree, currentUser } = this.props;
    if (!this.state.deviceTree && deviceTree && deviceTree.length) {
      const filteredDeviceTree = this.filterDeviceTree(currentUser, deviceTree);
      this.setState({ deviceTree: filteredDeviceTree });
    }
  };

  componentWillReceiveProps = nextProps => {
    const { viewMode, currentRegion } = this.props;
    if (
      (!this.state.deviceTree && nextProps.deviceTree && nextProps.deviceTree.length) ||
      viewMode !== nextProps.viewMode ||
      currentRegion.id !== nextProps.currentRegion.id ||
      (this.state.deviceTree && !nextProps.deviceTree)
    ) {
      if (!nextProps.deviceTree) {
        this.setState({ deviceTree: null });
      } else {
        const filteredDeviceTree = this.filterDeviceTree(
          nextProps.currentUser,
          nextProps.deviceTree
        );
        this.setState({ deviceTree: filteredDeviceTree });
      }
    }
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (
      (!this.state.deviceTree && nextState.deviceTree) ||
      this.state.deviceTree !== nextState.deviceTree
    ) {
      return true;
    }
    if (nextProps.currentDevice.id !== this.props.currentDevice.id) {
      return true;
    }
    if (this.state.height !== nextState.height) {
      return true;
    }
    return false;
  };

  render() {
    const { deviceTree } = this.state;
    const { deviceProfileViewsHash, currentDevice } = this.props;
    if (deviceTree) {
      return (
        <Widget
          onHeightReady={height => {
            this.setState({ height });
          }}
        >
          <DeviceTreeMenu />
          <ChooseLogTypesForm
            onSubmit={this.doReadEvents.bind(this)}
            modalName={deviceModals.readEventsModalName}
            logTypes={
              currentDevice.id
                ? deviceProfileViewsHash[currentDevice.deviceProfileId].deviceProfile.logTypes
                : null
            }
          />
          <ActiveDevicesTreeContextMenu
            deviceProfiles={deviceProfileViewsHash}
            onEnableDevice={this.enableDevice.bind(this)}
            onDisableDevice={this.disableDevice.bind(this)}
            onShowEvents={this.showEvents.bind(this)}
            onReadEvents={this.readEventsFromLog.bind(this)}
            goToPrevMalfunction={this.goToPrevMalfunction.bind(this)}
            goToNextMalfunction={this.goToNextMalfunction.bind(this)}
          />
          <RegionViewerWidget />
          <div style={DEVICE_TREE_WRAPPER_STYLES}>
            <AutoSizer>
              {({ height, width }) => (
                <div style={{ minHeight: height, width }}>
                  <ExpandableTable
                    getEntitiesHash={getActiveDevicesHash}
                    rowSelection={currentDevice.id}
                    columns={this.columns}
                    nodes={deviceTree}
                    height={height}
                    onRowClick={this.onRowClick.bind(this)}
                    scrollTo={this.scrollTo}
                  />
                </div>
              )}
            </AutoSizer>
          </div>
          <ActiveDeviceDetails />
        </Widget>
      );
    } else {
      return (
        <Spin>
          <Text>Нет данных</Text>
        </Spin>
      );
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  /* TODO: перенести все сущности ОЗ в activeProject */
  return {
    viewMode: getViewMode(state),
    currentRegion: getCurrentRegion(state),
    regions: getActiveRegions(state),
    projectId: getActiveProjectId(state),
    deviceTree: getActiveDeviceTree(state),
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    currentDevice: getCurrentDevice(state),
    currentUser: getCurrentUser(state)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActiveDevicesTree);
