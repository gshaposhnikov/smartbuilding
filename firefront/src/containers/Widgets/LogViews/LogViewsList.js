import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Modal } from 'antd';

import Table, { Column } from 'components/Table';
import Widget from 'components/Widget';
import WidgetMenu from 'containers/Menus/LogViewListMenu';

import { selectLogView, deleteLogView, updateLogView } from 'actions/logViews';

import subsystemsHash from 'constants/subsystems';

class LogViewsList extends Component {
  state = {
    height: 500
  };
  emptyNameCell = <div>Без названия</div>;
  rootLogView = <div>Все события</div>;

  renderNameCell = text => {
    if (!text) return this.emptyNameCell;
    return <div>{text}</div>;
  };

  renderDescriptionCell = (
    text,
    { recordCount, nLastDays, stateCategoryIds, subsystems, isRoot }
  ) => {
    if (isRoot) return this.rootLogView;
    const { stateCategoryViewsHash } = this.props;
    return (
      <div>{`${recordCount} записей; ${nLastDays} дней; ${subsystems.map(
        subsystemId => subsystemsHash[subsystemId].name
      )}; ${stateCategoryIds.map(stateCategoryId =>
        stateCategoryViewsHash[stateCategoryId]
          ? stateCategoryViewsHash[stateCategoryId].name
          : null
      )}`}</div>
    );
  };

  getRowClassName = record => {
    const { selectedLogView } = this.props;
    if (selectedLogView) {
      if (record.id === selectedLogView.id) return 'selected-row';
    }
    if (!record.active && !record.isRoot) return 'disabled-row';
    return '';
  };

  onRowClick = record => {
    if (!record.isRoot) {
      const { dispatch } = this.props;
      dispatch(selectLogView(record));
    }
  };

  onAddClick = () => {
    const { dispatch } = this.props;
    dispatch(
      selectLogView({
        nLastDays: 0,
        recordCount: 1,
        active: true
      })
    );
  };

  onToUpClick = () => {
    const { dispatch, selectedLogView } = this.props;
    dispatch(updateLogView({ ...selectedLogView, position: selectedLogView.position - 1 }));
  };

  onToDownClick = () => {
    const { dispatch, selectedLogView } = this.props;
    dispatch(updateLogView({ ...selectedLogView, position: selectedLogView.position + 1 }));
  };

  onDeleteClick = () => {
    const { dispatch, selectedLogView } = this.props;
    Modal.confirm({
      title: 'Удаление журнала',
      content: (
        <div>
          Удалить журнал <b>{selectedLogView.name}</b>?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteLogView(selectedLogView.id));
      }
    });
  };

  render = () => {
    const { logViews, selectedLogView } = this.props;
    const { height } = this.state;
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <WidgetMenu
          isAddActive={true}
          onAdd={this.onAddClick}
          isDeleteActive={selectedLogView && selectedLogView.id ? true : false}
          onDelete={this.onDeleteClick}
          isToUpActive={
            selectedLogView && selectedLogView.id && selectedLogView.position > 0 ? true : false
          }
          onToUp={this.onToUpClick}
          isToDownActive={
            selectedLogView && selectedLogView.id && selectedLogView.position <= logViews.length - 3
              ? true
              : false
          }
          onToDown={this.onToDownClick}
        />
        <Table
          dataSource={logViews}
          rowKey="id"
          scroll={{ y: height - 61 }}
          onRowClick={this.onRowClick}
          rowClassName={this.getRowClassName}
        >
          <Column title="Название" dataIndex="name" key="id" render={this.renderNameCell} />
          <Column title="Описание" dataIndex="name" render={this.renderDescriptionCell} />
        </Table>
      </Widget>
    );
  };
}

const getLogViews = createSelector(
  logViews => logViews,
  logViews => logViews.filter(logView => !logView.isRoot)
);

const mapStateToProps = state => {
  return {
    logViews: getLogViews(state.activeProject.logEvents.logViews),
    selectedLogView: state.widgets.selectedLogView,
    stateCategoryViewsHash: state.stateCategoryViewsHash
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogViewsList);
