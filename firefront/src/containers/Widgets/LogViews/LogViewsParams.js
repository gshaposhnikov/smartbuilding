import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, change, reset } from 'redux-form';

import Checkbox from 'components/Form/Checkbox';
import Input from 'components/Form/Input';
import { FormItem } from 'components/Form';
import InputNumber from 'components/Form/InputNumber';
import {
  WrapperWithoutPadding as WidgetWrapper,
  WrapperWithPadding as FormWrapper,
  Row,
  Column
} from 'components/Layout';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';

import { addLogView, updateLogView } from 'actions/logViews';

import subsystemsHash from 'constants/subsystems';

const SUBSYSTEMS = Object.values(subsystemsHash);
const FORM_NAME = 'logViewParams';
const required = value => value ? undefined : 'Required'

class LogViewParams extends Component {
  handleSubmit = e => {
    e.preventDefault();
    const { selectedLogView } = this.props;
    if (selectedLogView) {
      if (selectedLogView.id) {
        this.updateLogView();
      } else {
        this.addLogView();
      }
    }
  };

  addLogView = () => {
    const { formValues, dispatch } = this.props;
    const newLogView = {
      ...formValues
    };
    dispatch(addLogView(newLogView));
  };

  updateLogView = () => {
    const { formValues, dispatch, selectedLogView } = this.props;
    const updatedLogView = { ...selectedLogView, ...formValues };
    dispatch(updateLogView(updatedLogView));
  };

  componentWillReceiveProps = newProps => {
    if (newProps.selectedLogView) {
      const { selectedLogView, dispatch } = this.props;
      if (selectedLogView && selectedLogView.id !== newProps.selectedLogView.id) {
        dispatch(reset(FORM_NAME));
      }
    }
  };

  onCheckboxChange = (groupName, fieldName, value) => {
    const { dispatch, formValues } = this.props;
    const groupValues = formValues && formValues[groupName] ? formValues[groupName] : [];
    const newValues = groupValues
      ? value
        ? [...groupValues, fieldName]
        : groupValues.filter(id => id !== fieldName)
      : [fieldName];
    dispatch(change(FORM_NAME, groupName, newValues));
  };

  render() {
    const { stateCategoryViews, formValues, selectedLogView, pristine, valid } = this.props;
    return (
      <WidgetWrapper>
        <WidgetParamsEditorMenu
          isViewTitle={true}
          title={selectedLogView ? selectedLogView.name || 'Новый журнал' : 'Не выбран журнал'}
          isSaveActive={selectedLogView && !pristine && valid}
          onSaveAction={this.handleSubmit}
        />
        {selectedLogView ? (
          <form>
            <Row>
              <FormWrapper>
                <Column>
                  <FormItem label="Название" required>
                    <Field component={Input} name="name" validate={[required]} />
                  </FormItem>
                  <FormItem label="Классы состояний">
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                      {stateCategoryViews.map((stateCategoryView, index) => (
                        <Checkbox
                          key={stateCategoryView.id}
                          checked={
                            formValues && formValues['stateCategoryIds']
                              ? formValues['stateCategoryIds'].includes(stateCategoryView.id)
                              : false
                          }
                          label={stateCategoryView.name}
                          input={{
                            onChange: ({ target: { checked: value } }) =>
                              this.onCheckboxChange('stateCategoryIds', stateCategoryView.id, value)
                          }}
                        />
                      ))}
                    </div>
                  </FormItem>
                </Column>
              </FormWrapper>
              <FormWrapper>
                <Column>
                  <FormItem label={'Отображение в ОЗ'}>
                    <Field component={Checkbox} label={'Включено'} name="active" />
                  </FormItem>
                  <FormItem label="Подсистемы">
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                      {SUBSYSTEMS.map((subsystem, index) => (
                        <Checkbox
                          key={subsystem.id}
                          checked={
                            formValues && formValues['subsystems']
                              ? formValues['subsystems'].includes(subsystem.id)
                              : false
                          }
                          label={subsystem.name}
                          input={{
                            onChange: ({ target: { checked: value } }) =>
                              this.onCheckboxChange('subsystems', subsystem.id, value)
                          }}
                        />
                      ))}
                    </div>
                  </FormItem>
                  <FormItem label={'Кол-во записей'}>
                    <Field component={InputNumber} name="recordCount" min={1} max={1000} />
                  </FormItem>
                  <FormItem label={'Не позднее'}>
                    <Field component={InputNumber} name="nLastDays" min={0} />
                    дней
                  </FormItem>
                </Column>
              </FormWrapper>
            </Row>
          </form>
        ) : null}
      </WidgetWrapper>
    );
  }
}

LogViewParams = reduxForm({ form: FORM_NAME, enableReinitialize: true })(LogViewParams);

const mapStateToProps = state => {
  return {
    formValues:
      state.form[FORM_NAME] && state.form[FORM_NAME].values ? state.form[FORM_NAME].values : {},
    selectedLogView: state.widgets.selectedLogView,
    stateCategoryViews: Object.values(state.stateCategoryViewsHash),
    initialValues: state.widgets.selectedLogView || {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogViewParams);
