import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import Table from 'components/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Widget from 'components/Widget';

import DisabledDevicesMenu from 'containers/Menus/DisabledDevicesMenu';

import { getDevicesByPollingState } from 'helpers/device';
import {
  getActiveProjectId,
  getActiveDeviceList,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

import { setDevicePollingState } from 'actions/devices';

class DisabledDevicesWidget extends Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        key: 'type',
        title: 'Тип',
        dataIndex: 'type',
        render: (text, record) => this.deviceTypeColumnRender(text, record)
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'middleAddressPath' }
    ];
    this.state = {
      currentDeviceId: null,
      height: 500
    };
  }

  componentWillReceiveProps = nextProps => {
    const { currentDeviceId } = this.state;
    const { devicesHash } = nextProps;
    if (currentDeviceId) {
      if (devicesHash[currentDeviceId]) {
        if (devicesHash[currentDeviceId].statePolling !== false) {
          this.setState({ currentDeviceId: null });
        }
      } else {
        this.setState({ currentDeviceId: null });
      }
    }
  };

  deviceTypeColumnRender = (text, record) => {
    return <DeviceTypeTreeItem record={record} stateIconEnabled={false} />;
  };

  rowClassName = record => {
    const { currentDeviceId } = this.state;
    return currentDeviceId === record.id ? 'selected-row' : '';
  };

  onRowClick = record => {
    this.setState({ currentDeviceId: record.id });
  };

  enableDevice = deviceId => {
    const { projectId, dispatch } = this.props;
    dispatch(setDevicePollingState(projectId, deviceId, null, true));
    this.setState({ currentDeviceId: null });
  };

  onRowDoubleClick = record => {
    this.enableDevice(record.id);
  };

  onEnableClick = () => {
    const { currentDeviceId } = this.state;
    if (currentDeviceId) this.enableDevice(currentDeviceId);
  };

  render() {
    const { currentDeviceId, height } = this.state;
    const { disabledDeviceList } = this.props;

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <DisabledDevicesMenu isActive={!!currentDeviceId} onEnableDevice={this.onEnableClick} />
        {disabledDeviceList.length ? (
          <Table
            hideDefaultSelections={true}
            columns={this.columns}
            dataSource={disabledDeviceList}
            rowKey={record => record.id || record.key}
            rowClassName={this.rowClassName}
            onRowClick={this.onRowClick}
            onRowDoubleClick={this.onRowDoubleClick}
            scroll={{ y: height - 61 }}
          />
        ) : null}
      </Widget>
    );
  }
}

const getDisabledDevices = createSelector(
  getActiveDeviceList,
  activeDeviceList => getDevicesByPollingState(activeDeviceList || [], false)
);

const getFilteredDeviceList = createSelector(
  getCurrentUser,
  getDisabledDevices,
  filterEntities
);

const mapStateToProps = state => ({
  devicesHash: getActiveDevicesHash(state) || {},
  disabledDeviceList: getFilteredDeviceList(state),
  projectId: getActiveProjectId(state)
});

export default connect(mapStateToProps)(DisabledDevicesWidget);
