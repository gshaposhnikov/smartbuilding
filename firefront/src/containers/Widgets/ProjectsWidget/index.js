import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { isEmpty } from 'lodash';
import { Modal, Icon } from 'antd';
import styled from 'styled-components';
import { push } from 'react-router-redux';

import Table from 'components/Table/new';
import ProjectForm from 'containers/Forms/ProjectForm';
import ProjectContextMenu from 'containers/ContextMenus/ProjectContextMenu';
import ImportProjectForm from 'containers/Forms/ImportProjectForm';

import ProjectWidgetMenu from 'components/Menu/ProjectWidgetMenu';
import Widget from 'components/Widget';

import {
  projectsLoad,
  createProject,
  updateProject,
  deleteProject,
  selectProject,
  importProject,
  projectExport
} from 'actions/projects';

import { loadUsers } from 'actions/users';
import { modalOpen, modalClose } from 'actions/modals';

import { checkCompletion } from 'helpers/form';
import { getProjectList } from 'helpers/projects';
import { getCurrentUser, hasRole } from 'helpers/user';

const BUILD_PROJECT_STATUS = {
  DEFAULT: { title: 'В разработке', iconType: 'link', iconStyle: { color: '#5FA2DD' } },
  BUILD: { title: 'В разработке', iconType: 'link', iconStyle: { fontSize: '10px', color: '#5FA2DD' } },
  ACTIVE: { title: 'Активирован', iconType: 'link', iconStyle: { color: '#5FA2DD' } }
};

const ACTIVATED_PROJECT_STATUS = {
  DEFAULT: { title: 'Не работает', iconType: 'close-circle-o', iconStyle: { color: 'Orange' } },
  INACTIVE: { title: 'Не работает', iconType: 'close-circle-o', iconStyle: { color: 'Orange' } },
  ACTIVE: { title: 'В работе', iconType: 'check-circle-o', iconStyle: { color: 'Green' } },
  FAILURE: { title: 'Содержит ошибки', iconType: 'close-circle-o', iconStyle: { color: 'Red' } }
};

const PROJECT_STATUS_STYLE = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center'
};

const TableWrapper = styled.div`
  .react-grid-Row {
    font-size: 14px;
  }
`;

const projectModals = {
  import: 'importProject',
  create: 'newProject',
  update: 'updateProject'
};

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
    this.columns = [
      { key: 'name', name: 'Название проекта', width: 300 },
      { key: 'status', name: 'Статус', width: 160, formatter: this.statusFormatter.bind(this) },
      { key: 'version', name: 'Версия', width: 100 },
      { key: 'createDateTime', name: 'Дата', width: 150, formatter: this.dateFormatter.bind(this) },
      { key: 'userId', name: 'Автор', width: 150, formatter: this.usersFormatter.bind(this) },
      { key: 'description', name: 'Примечание' }
    ];
  }

  componentWillMount() {
    const { onLoadUsers, onProjectsLoad, users, projects } = this.props;
    if (!users || Object.keys(users).length === 0) {
      onLoadUsers();
    }
    if (!projects || projects.length === 0) {
      onProjectsLoad();
    }
  }

  componentWillReceiveProps(newProps) {
    const { onModalClose, modals, createProjectCompleted, updateProjectCompleted } = newProps;
    if (modals[projectModals.create] && createProjectCompleted) {
      onModalClose(projectModals.create);
    }
    if (modals[projectModals.update] && updateProjectCompleted) {
      onModalClose(projectModals.update);
    }
  }

  statusFormatter({ value }) {
    const { activeProjectStatus } = this.props;

    const projectStatusInfo =
      value === 'ACTIVE' && activeProjectStatus
        ? ACTIVATED_PROJECT_STATUS[activeProjectStatus] || ACTIVATED_PROJECT_STATUS.DEFAULT
        : BUILD_PROJECT_STATUS[value] || BUILD_PROJECT_STATUS.DEFAULT;

    return (
      <span style={PROJECT_STATUS_STYLE}>
        {projectStatusInfo.title}
        <Icon type={projectStatusInfo.iconType} style={projectStatusInfo.iconStyle} />
      </span>
    );
  }

  dateFormatter({ value }) {
    return <span>{moment(value).format('HH:mm:ss DD.MM.YYYY')}</span>;
  }

  usersFormatter({ value }) {
    const { users } = this.props;
    let userName = users ? users[value] : null;
    return userName ? <span>{userName.fullName}</span> : <span />;
  }

  openForm(name) {
    this.props.onModalOpen(name);
  }

  createNewProject = values => {
    const { onProjectCreate, onProjectSelect, currentUser } = this.props;
    const newProject = {
      name: '',
      description: '',
      userId: currentUser.id,
      version: ''
    };
    onProjectCreate({ ...newProject, ...values });
    onProjectSelect({});
  };

  projectExport = () => {
    const { onProjectExport, selectedProject } = this.props;
    onProjectExport(selectedProject.id);
  };

  importProject = values => {
    const { onModalClose, onImportProject } = this.props;
    const {
      fileContent: { importedFile, version, fileName }
    } = values;
    onImportProject(fileName, version, importedFile);
    onModalClose(projectModals.importProject);
  };

  updateProject = values => {
    const { onProjectUpdate, onProjectSelect, selectedProject } = this.props;
    const updatedProject = {
      ...selectedProject,
      ...values
    };

    onProjectUpdate(updatedProject);
    onProjectSelect({});
  };

  selectProject = selectedProject => {
    this.props.onProjectSelect(selectedProject);
  };

  deleteProject = () => {
    const { onDeleteProject, selectedProject } = this.props;
    Modal.confirm({
      title: 'Удаление проекта',
      content: (
        <div>
          Удалить проект{' '}
          <b>
            {selectedProject.name} ({selectedProject.version})
          </b>
          ?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        onDeleteProject(selectedProject.id);
      }
    });
  };

  openProject = (rowIdx, data) => {
    const { dispatch } = this.props;
    dispatch(push(`/projects/${data.id}/plan`));
  }

  render() {
    const {
      projects,
      selectedProject,
      isActiveProject,
      importProjectInProgress,
      currentUser
    } = this.props;
    const { height } = this.state;

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <ImportProjectForm modalName={projectModals.importProject} onSubmit={this.importProject} />
        <ProjectForm modalName={projectModals.create} onSubmit={this.createNewProject} />
        <ProjectForm modalName={projectModals.update} isEdit={true} onSubmit={this.updateProject} />
        <ProjectWidgetMenu
          importProjectInProgress={importProjectInProgress}
          isCreateButtonActive={true}
          isExportButtonActive={!isEmpty(selectedProject) && hasRole(currentUser, 'PROJECT_EXPORT')}
          isImportButtonActive={hasRole(currentUser, 'PROJECT_IMPORT')}
          isDeleteButtonActive={!isEmpty(selectedProject) && !isActiveProject}
          isEditButtonActive={!isEmpty(selectedProject) && !isActiveProject}
          onCreateButtonClick={() => this.openForm(projectModals.create)}
          onDeleteButtonClick={this.deleteProject}
          onEditButtonClick={() => this.openForm(projectModals.update)}
          onExportButtonClick={this.projectExport}
          onImportButtonClick={() => this.openForm(projectModals.importProject)}
        />
        <TableWrapper>
          <Table
            columns={this.columns}
            data={projects}
            enableRowSelect={true}
            contextMenu={ProjectContextMenu}
            onSelection={this.selectProject}
            currentItem={selectedProject}
            minHeight={height - 35}
            onRowDoubleClick={this.openProject}
          />
        </TableWrapper>
      </Widget>
    );
    /* 
     * TOFIX: если меняется только статус активного проекта (projects не меняется), то таблица не перерисовывается.
     * Поэтому активированные проект остается с желтым значком, пока не обновишь страницу.
     */
  }
}

const mapStateToProps = state => {
  return {
    isActiveProject:
      !isEmpty(state.widgets.projects.selected) &&
      state.widgets.projects.selected.status === 'ACTIVE',
    projects: getProjectList(state),
    selectedProject: state.widgets.projects.selected,
    currentUser: getCurrentUser(state),
    users: state.admin.users,
    activeProjectStatus: state.activeProject.activeProject
      ? state.activeProject.activeProject.status
      : null,
    modals: state.modals,

    createProjectCompleted: checkCompletion(
      state.inProgress.createProject,
      state.errors.createProject
    ),
    updateProjectCompleted: checkCompletion(
      state.inProgress.updateProject,
      state.errors.updateProject
    ),
    importProjectInProgress: state.inProgress.importProject
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    onDeleteProject: bindActionCreators(deleteProject, dispatch),
    onModalOpen: bindActionCreators(modalOpen, dispatch),
    onModalClose: bindActionCreators(modalClose, dispatch),
    onProjectSelect: bindActionCreators(selectProject, dispatch),
    onProjectCreate: bindActionCreators(createProject, dispatch),
    onProjectUpdate: bindActionCreators(updateProject, dispatch),
    onLoadUsers: bindActionCreators(loadUsers, dispatch),
    onProjectsLoad: bindActionCreators(projectsLoad, dispatch),
    onImportProject: bindActionCreators(importProject, dispatch),
    onProjectExport: bindActionCreators(projectExport, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
