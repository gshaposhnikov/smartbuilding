import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fabric } from 'fabric';
import { isEqual } from 'lodash';
import { Modal } from 'antd';

import { modalOpen, modalClose } from 'actions/modals';
import { updateRegionPlanLayouts, deleteDeviceRegion, newDeviceRegion } from 'actions/regions';
import { updateDevicePlanLayouts, updateDraggableDevice } from 'actions/devices';
import { updatePlan } from 'actions/plans';

import PlanEditorToolbar, { DEFAULT_SELECT_TOOL } from 'containers/Menus/PlanEditorToolbar';
import PlanRegionForm from 'containers/Forms/PlanRegionForm';
import NewPlanBackgroundForm from 'containers/Forms/NewPlanBackgroundForm';
import UpdateDeviceRegionForm from 'containers/Forms/UpdateDeviceRegionForm';

import subsystems from 'constants/subsystems';
import {
  getCurrentProject,
  getCurrentProjectPlans,
  getCurrentProjectPlanGroups,
  getCurrentProjectRegionViews,
  getCurrentProjectDeviceList,
  getCurrentProjectRegionsHash,
  getCurrentProjectDevicesHash
} from 'helpers/currentProject';
import {
  layerSorter,
  filterSamePoints,
  setObjectSelectable,
  getObjTopPosition,
  getObjLeftPosition,
  getRectPoints,
  getPolygonPoints,
  getTargetIntersectionsWithObjects,
  getValidxPosition,
  getValidyPosition,
  removePlanObjects
} from 'helpers/planEditor';

const { Canvas, Text, Point, Rect, Polygon, Group, Line } = fabric;
const defaultFields = {
  originX: 'left',
  originY: 'top',
  flipX: false,
  flipY: false,
  angle: 0,
  cornerSize: 8,
  borderColor: '#000',
  cornerColor: '#000',
  opacity: 0.6,
  scaleX: 1,
  scaleY: 1,
  clipTo: null,
  strokeDashArray: null,
  strokeLineCap: 'butt',
  strokeLineJoin: 'miter',
  transformMatrix: null,
  globalCompositeOperation: 'source-over',
  visible: true,
  fillRule: 'nonzero',
  shadow: null,
  startAngle: 0,
  endAngle: 0,
  strokeMiterLimit: 10
};
const rectFields = {
  type: 'rect',
  radius: 100,
  _controlsVisibility: {
    mr: true,
    mb: true
  },
  ...defaultFields
};
const polygonFields = {
  type: 'polygon',
  skewX: 0,
  skewY: 0,
  _controlsVisibility: {}, // Редактирование многоугольников пока убрал
  ...defaultFields
};

const ACTIVE_PROJECT = 'ACTIVE';
const CURSOR_STYLES = {
  COPY: 'copy',
  POINTER: 'pointer',
  DEFAULT: 'default',
  MOVE: 'move'
};
const GRID_KEY = 'grid';
const TOOL_KEYS = {
  RECT: 'rect',
  SELECT: 'select',
  POLYGON: 'polygon'
};
const POPUP_STYLES = {
  position: 'fixed',
  display: 'none',
  background: 'white',
  zIndex: 9999,
  padding: '4px 8px',
  borderRadius: '6px',
  border: '2px solid #5FA2DD',
  fontFamily: 'Helvetica, Arial, sans-serif',
  fontSize: '14px',
  color: 'black',
  whiteSpace: 'nowrap'
};

class PlanEditor extends Component {
  constructor(props) {
    super(props);
    const isActiveProject = props.project.status === ACTIVE_PROJECT;
    this.state = {
      zoom: 1,
      grid: false,
      gridSize: 20,
      isChecked: [],
      activeTool: 'select',
      isActiveProject,
      draggableDeviceData: {},
      selectedRegionInfo: {}
    };
    this.planRegions = [];
    this.planDevices = [];
    this.resetRectData(true, true);
    this.resetPolygonData(true, true);
    this.screen = {
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    };
  }

  componentDidMount() {
    const fabricCanvas = new Canvas();
    fabricCanvas.altActionKey = 'none';
    const { currentPlan, project } = this.props;
    this.bindActions(fabricCanvas); // Вызывается единожды и принимает первичное состояние canvas
    const isActiveProject = project.status === ACTIVE_PROJECT;
    this.setState({
      fabricCanvas,
      plan: currentPlan,
      isActiveProject
    });
    fabricCanvas.selection = false;
    this.init(this.props, fabricCanvas);
    window.addEventListener('keydown', this.onKeyDown, true);
  }

  componentWillReceiveProps = nextProps => {
    const { project, draggableDeviceId } = nextProps;
    const { fabricCanvas } = this.state;
    let { activeTool } = this.state;
    const isActiveProject = project.status === ACTIVE_PROJECT;
    if (isActiveProject) {
      activeTool = TOOL_KEYS.SELECT;
      this.resetRectData(true, true, fabricCanvas);
      this.resetPolygonData(true, true, fabricCanvas);
      fabricCanvas.selection = false;
      fabricCanvas.discardActiveObject();
      fabricCanvas.hoverCursor = CURSOR_STYLES.DEFAULT;
    }
    if (draggableDeviceId) fabricCanvas.hoverCursor = CURSOR_STYLES.COPY;
    fabricCanvas.getObjects().forEach(object => {
      if (object.info && object.info.gridKey !== GRID_KEY && !object.info.backgroundKey) {
        object.selectable = isActiveProject || draggableDeviceId ? false : true;
        object.hoverCursor = isActiveProject
          ? CURSOR_STYLES.DEFAULT
          : draggableDeviceId
          ? CURSOR_STYLES.COPY
          : CURSOR_STYLES.POINTER;
      }
      if (object.info && object.info.gridKey === GRID_KEY)
        object.hoverCursor =
          isActiveProject || !draggableDeviceId ? CURSOR_STYLES.DEFAULT : CURSOR_STYLES.COPY;
    });
    this.setState({ isActiveProject, activeTool });
    this.init(nextProps, fabricCanvas);
  };

  openForm = name => {
    this.props.modalOpen(name);
  };

  closeForm = name => {
    this.props.modalClose(name);
  };

  // Инициализирует события вызываемые FabricJS
  bindActions = fabricCanvas => {
    // TODO: Добавить закрепление за сеткой при создании объектов и их модификации
    fabricCanvas.on({
      'object:moving': this.objMoving,
      'object:modified': this.objModified,
      'mouse:down': this.mouseDownEvent,
      'mouse:move': this.mouseMoveEvent,
      'mouse:over': this.mouseOverEvent,
      'mouse:out': this.mouseOutEvent,
      'mouse:dblclick': this.onMouseDbClick,
      'selection:created': options => this.createSelection(options.target),
      'selection:updated': options => this.createSelection(options.target),
      'selection:cleared': () => {
        const { activeTool } = this.state;
        if (activeTool === 'moveBackground') {
          const background = fabricCanvas
            .getObjects()
            .find(object => object.info && object.info.backgroundKey);
          if (background) {
            background.set({ selectable: false, hoverCursor: CURSOR_STYLES.DEFAULT });
          }
          this.setActiveTool(DEFAULT_SELECT_TOOL);
        }
        this.setState({ selectedObject: null });
      }
    });
    fabric.util.addListener(window, 'dblclick', options => this.stopPolygonDrawing(options));
    fabric.util.addListener(window, 'contextmenu', options => this.onRightClick(options));
    fabric.util.addListener(window, 'click', options => {
      // Сброс выделенного элемента, при клике вне плана
      if (options.target.tagName !== 'CANVAS') {
        fabricCanvas.discardActiveObject();
        fabricCanvas.renderAll();
      }
    });
  };

  onMouseDbClick = options => {
    const { isActiveProject } = this.state;
    if (
      !isActiveProject &&
      options.target.info &&
      options.target.info.regionKey &&
      options.target.selectable
    ) {
      this.setState({ selectedRegionInfo: options.target.info });
      this.openForm('updatePlanRegion');
    }
  };

  onUpdatePlanRegionLayout = regionId => {
    const { selectedRegionInfo } = this.state;
    const { regionsHash, project, doUpdateRegionPlanLayouts } = this.props;
    const selectedLayoutIndex = parseInt(selectedRegionInfo.regionKey.split('.')[1], 10);
    const extendedPlanLayouts = [
      ...(regionsHash[regionId].planLayouts || []),
      {
        planId: selectedRegionInfo.planId,
        points: selectedRegionInfo.points,
        rect: selectedRegionInfo.rect
      }
    ];
    const reducedPlanLayouts = regionsHash[selectedRegionInfo.regionId].planLayouts.filter(
      (layout, index) => index !== selectedLayoutIndex
    );
    doUpdateRegionPlanLayouts(project.id, regionId, extendedPlanLayouts);
    doUpdateRegionPlanLayouts(project.id, selectedRegionInfo.regionId, reducedPlanLayouts);
    this.closeForm('updatePlanRegion');
    this.setState({ selectedRegionInfo: {} });
  };

  onRightClick = options => {
    if (this.polygonLines.length) {
      this.stopPolygonDrawing(options);
    }
    if (this.rectLines.length) {
      const { fabricCanvas } = this.state;
      this.resetRectData(true, true, fabricCanvas);
    }
  };

  onKeyDown = e => {
    switch (e.keyCode) {
      /* Esc */
      case 27: {
        const { fabricCanvas, activeTool } = this.state;
        // Отмена рисования зоны
        if (this.polygonLines.length || this.rectLines.length) {
          this.resetPolygonData(true, true, fabricCanvas);
          this.resetRectData(true, true, fabricCanvas);
        }
        // Сброс выделения
        if (activeTool === TOOL_KEYS.SELECT) {
          fabricCanvas.discardActiveObject();
          fabricCanvas.renderAll();
        }
        break;
      }
      /* Delete, Del */
      case 46:
      case 110: {
        const { modals } = this.props;
        let deleteAvailable = true;
        Object.keys(modals).forEach(modalName => {
          if (modals[modalName]) deleteAvailable = false;
        });
        if (deleteAvailable) this.deletePlanObject();
        break;
      }

      default:
        break;
    }
  };

  createSelection = target => {
    this.setState({ selectedObject: { position: { x: target.left, y: target.top }, target } });
  };

  resetPolygonData = (lines, points, fabricCanvas) => {
    if (fabricCanvas) removePlanObjects(fabricCanvas, this.polygonLines);
    if (lines) this.polygonLines = [];
    if (points) this.polygonPoints = [];
  };

  resetRectData = (lines, points, fabricCanvas) => {
    if (fabricCanvas) removePlanObjects(fabricCanvas, this.rectLines);
    if (lines) this.rectLines = [];
    if (points) this.rectPoints = [];
  };

  createDeviceClone = pos => {
    const { fabricCanvas } = this.state;
    this.removeDeviceClone();
    this.invisibleObject = new Rect({
      top: pos.y,
      left: pos.x,
      width: 1,
      height: 1,
      opacity: 0
    });
    fabricCanvas.add(this.invisibleObject);
    this.invisibleObject.selectable = false;
    this.invisibleObject.setCoords();
  };

  removeDeviceClone = () => {
    const { fabricCanvas } = this.state;
    if (this.invisibleObject) fabricCanvas.remove(this.invisibleObject);
  };

  mouseOverEvent = options => {
    if (this.popupEl) {
      this.popupDelay = setTimeout(() => this.showPopup(options), 600);
    }
  };

  mouseOutEvent = () => {
    if (this.popupEl) {
      clearInterval(this.popupDelay);
      this.hidePopup();
      this.popupDelay = null;
    }
  };

  checkEditable = object => {
    const { isActiveProject, activeTool } = this.state;
    object.selectable = isActiveProject ? false : activeTool === TOOL_KEYS.SELECT;
    object.hoverCursor = isActiveProject
      ? CURSOR_STYLES.DEFAULT
      : activeTool === TOOL_KEYS.SELECT
      ? CURSOR_STYLES.POINTER
      : CURSOR_STYLES.DEFAULT;
  };

  _getPointer = (event, grid) => {
    const { fabricCanvas, gridSize } = this.state;
    const { currentPlan } = this.props;
    const _mouse = fabricCanvas.getPointer(event);
    return {
      x: getValidxPosition(_mouse.x, currentPlan.xSize, grid ? gridSize : null),
      y: getValidyPosition(_mouse.y, currentPlan.ySize, grid ? gridSize : null)
    };
  };

  mouseMoveEvent = options => {
    const { activeTool, fabricCanvas } = this.state;
    const { e } = options;
    const _mouse = this._getPointer(e);
    this._mouseX = _mouse.x;
    this._mouseY = _mouse.y;
    switch (activeTool) {
      case TOOL_KEYS.POLYGON: {
        if (this.polygonLines.length) {
          const { grid, gridSize } = this.state;
          const { currentPlan } = this.props;
          const x2 = grid ? getValidxPosition(_mouse.x, currentPlan.xSize, gridSize) : _mouse.x;
          const y2 = grid ? getValidxPosition(_mouse.y, currentPlan.xSize, gridSize) : _mouse.y;
          const updatedLine = this.polygonLines[this.polygonLines.length - 1];
          updatedLine.set({ x2, y2 }).setCoords();
          fabricCanvas.renderAll();
        }
        break;
      }
      case TOOL_KEYS.RECT: {
        if (this.rectLines.length) {
          const { grid, gridSize } = this.state;
          const { currentPlan } = this.props;
          const _x = grid ? getValidxPosition(_mouse.x, currentPlan.xSize, gridSize) : _mouse.x;
          const _y = grid ? getValidxPosition(_mouse.y, currentPlan.xSize, gridSize) : _mouse.y;
          this.rectLines.forEach((line, index) => {
            switch (index) {
              case 0: {
                line.set({ x2: _x }).setCoords();
                break;
              }
              case 1: {
                line
                  .set({
                    x1: _x,
                    x2: _x,
                    y2: _y
                  })
                  .setCoords();
                break;
              }
              case 2: {
                line
                  .set({
                    x2: _x,
                    y1: _y,
                    y2: _y
                  })
                  .setCoords();
                break;
              }
              case 3: {
                line
                  .set({
                    y2: _y
                  })
                  .setCoords();
                break;
              }
              default:
                break;
            }
          });
          fabricCanvas.renderAll();
        }
        break;
      }
      case TOOL_KEYS.SELECT: {
        if (this.popupEl) {
          const xPos = e.clientX + 20;
          const popupWidth = this.popupEl.offsetWidth;
          // Проверяем выходит ли popup за пределы экрана, если да, то отображаем его слева от курсора
          if (xPos + popupWidth > this.screen.width) {
            this.popupEl.style.left = e.clientX - popupWidth - 20 + 'px';
          } else {
            this.popupEl.style.left = e.clientX + 20 + 'px';
          }
          this.popupEl.style.top = e.clientY + 20 + 'px';
        }
        break;
      }
      default:
        break;
    }
  };

  getPlanObjectsAndSortByLayer = fabricCanvas => {
    const planObjects = layerSorter(fabricCanvas);
    this.planRegions = planObjects.regions;
    this.planDevices = planObjects.devices;
  };

  stopRectDrawing = () => {
    const { activeTool, fabricCanvas, isActiveProject } = this.state;
    const { draggableDeviceId } = this.props;
    if (this.rectPoints.length === 2) this.openForm('createPlanRegion');
    this.resetRectData(false, false, fabricCanvas);
    if (activeTool === TOOL_KEYS.RECT) this.setState({ activeTool: TOOL_KEYS.SELECT });
    if (!isActiveProject && !draggableDeviceId) setObjectSelectable(fabricCanvas, true);
  };

  stopPolygonDrawing = options => {
    const { activeTool, fabricCanvas, isActiveProject } = this.state;
    const { draggableDeviceId } = this.props;
    if (this.polygonLines.length) this.openForm('createPlanRegion');
    this.resetPolygonData(false, false, fabricCanvas);
    if (activeTool === TOOL_KEYS.POLYGON) this.setState({ activeTool: TOOL_KEYS.SELECT });
    if (!isActiveProject && !draggableDeviceId) setObjectSelectable(fabricCanvas, true);
  };

  mouseDownEvent = options => {
    const { activeTool, fabricCanvas, grid } = this.state;
    const { draggableDeviceId, devicesHash } = this.props;
    const _mouse = this._getPointer(options.e, grid);
    const _x = _mouse.x;
    const _y = _mouse.y;
    switch (activeTool) {
      case TOOL_KEYS.POLYGON: {
        const line = new Line([_x, _y, _x, _y], {
          strokeWidth: 1,
          selectable: false,
          stroke: 'red'
        });
        line.hoverCursor = CURSOR_STYLES.DEFAULT;
        this.polygonPoints = [...this.polygonPoints, new Point(_x, _y)];
        this.polygonLines = [...this.polygonLines, line];
        fabricCanvas.add(line);
        break;
      }
      case TOOL_KEYS.RECT: {
        if (this.rectLines.length) {
          this.rectPoints = [...this.rectPoints, new Point(_x, _y)];
          this.stopRectDrawing();
        } else {
          for (var i = 0; i < 4; i++) {
            const line = new Line([_x, _y, _x, _y], {
              strokeWidth: 1,
              selectable: false,
              stroke: 'red'
            });
            line.hoverCursor = CURSOR_STYLES.DEFAULT;
            this.rectLines.push(line);
            fabricCanvas.add(line);
          }
          this.rectPoints = [new Point(_x, _y)];
        }
        break;
      }
      case TOOL_KEYS.SELECT: {
        if (draggableDeviceId) {
          // Ищем устройство в списке
          const draggableDevice = devicesHash[draggableDeviceId];
          // Если устройство найдено и его можно привязать к зоне
          if (draggableDevice && draggableDevice.attachableToRegion) {
            this.createDeviceClone({ x: _x, y: _y });
            this.checkNewPlanDeviceIntersections(draggableDevice, options);
          }
          // Если устройство найдено и нельзя привязать к зоне
          else if (draggableDevice && !draggableDevice.attachableToRegion) {
            // Добавляем устройство на план
            this.createPlanDevice(options);
          }
        }
        break;
      }
      default:
        break;
    }
  };

  getPlanRegionsForAttachDevice = (planRegions, planDevices, device) => {
    const devices = planDevices.filter(
      planDevice => planDevice.info && planDevice.info.attachableToRegion
    );
    // Фильтруем зоны плана
    return planRegions.filter(
      region =>
        // Если совпадает подсистема
        (region.info && region.info.subsystem === device.subsytem) ||
        // Или не имеет на себе устройств
        !getTargetIntersectionsWithObjects(region, devices).length
    );
  };

  updateDeviceRegion = (device, regionId) => {
    const newConnection = {
      projectId: device.projectId,
      regionId,
      deviceId: device.id
    };
    this.props.dispatch(newDeviceRegion(newConnection));
  };

  deleteDeviceRegion = (device, regionId) => {
    const deleteConnection = {
      projectId: device.projectId,
      regionId: device.regionId,
      deviceId: device.id
    };
    this.props.dispatch(deleteDeviceRegion(deleteConnection));
  };

  /**
   * Отвязка устройства от зоны
   */

  deleteDeviceRegionConfirm = () => {
    const {
      draggableDeviceData: { device, options, target }
    } = this.state;
    if (target) {
      this.updatePlanDevice(target);
    } else {
      this.createPlanDevice(options);
    }
    this.deleteDeviceRegion(device);
    this.closeForm('deleteDeviceRegion');
    this.removeDeviceClone();
  };

  deleteDeviceRegionReject = () => {
    const {
      draggableDeviceData: { target, options }
    } = this.state;
    if (target) {
      this.updatePlanDevice(target);
    } else {
      this.createPlanDevice(options);
    }
    this.closeForm('deleteDeviceRegion');
    this.removeDeviceClone();
  };

  deleteDeviceRegionCancel = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) {
      this.resetPlanDeviceModifications(target);
    } else {
      const { doUpdateDraggableDevice } = this.props;
      doUpdateDraggableDevice(null);
    }
    this.closeForm('deleteDeviceRegion');
  };

  /**
   * Заменить зону на плане
   */

  replacePlanRegionConfirm = regionId => {
    const {
      draggableDeviceData: { device, options, availableRegions, target }
    } = this.state;
    const { currentPlan, project, doUpdateRegionPlanLayouts, regionsHash } = this.props;
    const planRegion = availableRegions.filter(region => region.info.regionId === regionId)[0];
    if (planRegion) {
      const regionView = regionsHash[device.regionId];
      const newLayout = {
        planId: currentPlan.id,
        rect: planRegion.type === 'rect' ? true : false,
        points: planRegion.info.points
      };
      const planLayouts = [...regionView.planLayouts, newLayout];
      doUpdateRegionPlanLayouts(project.id, device.regionId, planLayouts);
      this.deletePlanRegion(planRegion);
      if (target) this.updatePlanDevice(target);
      else this.createPlanDevice(options);
      this.closeForm('replacePlanRegion');
    }
    this.removeDeviceClone();
  };

  replacePlanRegionReject = () => {
    const {
      draggableDeviceData: { target, options }
    } = this.state;
    if (target) {
      this.updatePlanDevice(target);
    } else {
      this.createPlanDevice(options);
    }
    this.closeForm('replacePlanRegion');
    this.removeDeviceClone();
  };

  replacePlanRegionCancel = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) {
      this.resetPlanDeviceModifications(target);
    } else {
      const { doUpdateDraggableDevice } = this.props;
      doUpdateDraggableDevice(null);
    }
    this.closeForm('replacePlanRegion');
    this.removeDeviceClone();
  };

  /**
   * Изменение зоны устройства
   */
  changeDeviceRegionConfirm = regionId => {
    const {
      draggableDeviceData: { device, options, target }
    } = this.state;
    if (target) this.updatePlanDevice(target);
    else this.createPlanDevice(options);
    this.updateDeviceRegion(device, regionId);
    this.closeForm('changeDeviceRegion');
    this.removeDeviceClone();
  };

  changeDeviceRegionReject = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) this.resetPlanDeviceModifications(target);
    this.closeForm('changeDeviceRegion');
    this.removeDeviceClone();
  };

  changeDeviceRegionCancel = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) {
      this.resetPlanDeviceModifications(target);
    } else {
      const { doUpdateDraggableDevice } = this.props;
      doUpdateDraggableDevice(null);
    }
    this.closeForm('changeDeviceRegion');
    this.removeDeviceClone();
  };

  /**
   * Выбор зоны
   */
  selectDeviceRegionConfirm = regionId => {
    const {
      draggableDeviceData: { options, device, availableRegions, target }
    } = this.state;
    const selectedRegion = availableRegions.find(region => region.info.regionId === regionId);
    if (selectedRegion) {
      if (device.regionId) {
        if (selectedRegion.info.subsytem === device.subsystem) {
          this.setState({
            draggableDeviceData: {
              device,
              regionIds: [selectedRegion.info.regionId],
              options,
              target
            }
          });
          this.openForm('changeDeviceRegion');
        } else {
          this.setState({
            draggableDeviceData: {
              device,
              regionIds: [selectedRegion.info.regionId],
              options,
              availableRegions,
              target
            }
          });
          this.openForm('replacePlanRegion');
        }
      } else {
        if (selectedRegion.info.subsytem === device.subsytem) {
          if (target) this.updatePlanDevice(target);
          else this.createPlanDevice(options);
          this.updateDeviceRegion(device, regionId);
        }
      }
    }
    this.closeForm('selectDeviceRegion');
  };

  selectDeviceRegionCancel = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) {
      this.resetPlanDeviceModifications(target);
    }
    this.closeForm('selectDeviceRegion');
  };

  /**
   * Расположить поверх зоны с другой подсистемой
   */

  forceDropDeviceConfirm = regionId => {
    const {
      draggableDeviceData: { options, target }
    } = this.state;
    if (target) {
      this.updatePlanDevice(target);
    } else {
      this.createPlanDevice(options);
    }
    this.closeForm('forceDropDevice');
  };

  forceDropDeviceCancel = () => {
    const {
      draggableDeviceData: { target }
    } = this.state;
    if (target) {
      this.resetPlanDeviceModifications(target);
    } else {
      const { doUpdateDraggableDevice } = this.props;
      doUpdateDraggableDevice(null);
    }
    this.closeForm('forceDropDevice');
  };

  // Метод обновления объекта
  objModified = options => {
    const { target } = options;
    if (target.info.regionKey) {
      const { currentPlan } = this.props;

      this.updatePlanRegion(
        target.type === TOOL_KEYS.RECT
          ? getRectPoints(target, currentPlan)
          : getPolygonPoints(target, fabric),
        target
      );
    } else if (target.info.deviceKey) {
      if (target.info.attachableToRegion) {
        const { devicesHash } = this.props;
        const draggableDevice = devicesHash[target.info.deviceId];
        this.createDeviceClone({ x: target.left, y: target.top });
        this.checkPlanDeviceIntersections(draggableDevice, options);
      } else this.updatePlanDevice(target);
    } else if (target.info.backgroundKey) this.updatePlanBackground(target);
  };

  resetPlanDeviceModifications = target => {
    const {
      selectedObject: { position }
    } = this.state;
    target.set({
      top: position.y,
      left: position.x
    });
    target.setCoords();
  };

  // TODO: Объеденить функции определения зоны для перемещения и размещения устройства
  checkPlanDeviceIntersections = (device, options) => {
    const { target } = options;
    // Получаем зоны, с которыми устройство пересекается в текущем месте
    const regionsIntersectionsWithDevice = getTargetIntersectionsWithObjects(
      this.invisibleObject,
      this.planRegions
    );
    const intersectionsCount = regionsIntersectionsWithDevice.length;

    // Если устройство добавляется в пустое место плана
    if (!intersectionsCount) {
      // Если привязано к зоне, то спрашиваем про отвязку
      if (device.regionId) {
        this.setState({
          draggableDeviceData: {
            device,
            target
          }
        });
        this.openForm('deleteDeviceRegion');
      }
      // Если не привязано к зоне, то добавляем его на план
      else this.updatePlanDevice(target);
    }
    // Если устройство добавляется в место, где есть зона(ы)
    else {
      // Если привязано к зоне
      if (device.regionId) {
        // Ищем зону, к которой привязано устройство
        const hasIntersectionsWithDeviceRegion = regionsIntersectionsWithDevice.find(
          planRegion => planRegion.info.regionId === device.regionId
        );
        // Если есть зона, которой привязано устройство
        if (hasIntersectionsWithDeviceRegion) {
          // Размещаем устройство на плане
          this.updatePlanDevice(target);
        }
        // Если нет зоны, к которой привязано устройство
        else {
          // Ищем зоны, к которым можно привязать устройство
          const availableRegionBySubsystemIds = [];
          const availableRegionForReplaceIds = [];
          const availableRegions = regionsIntersectionsWithDevice.filter(region => {
            if (region.info.subsystem === device.subsystem) {
              if (!availableRegionBySubsystemIds.includes(region.info.regionId))
                availableRegionBySubsystemIds.push(region.info.regionId);
              return true;
            } else if (
              !getTargetIntersectionsWithObjects(
                region,
                this.planDevices.filter(
                  planDevice => planDevice.info.regionId === region.info.regionId
                )
              ).length
            ) {
              if (!availableRegionForReplaceIds.includes(region.info.regionId))
                availableRegionForReplaceIds.push(region.info.regionId);
              return true;
            } else return false;
          });
          const availableRegionsCount =
            availableRegionBySubsystemIds.length + availableRegionForReplaceIds.length;

          // Если нет доступных зон
          if (!availableRegionsCount) {
            this.setState(
              {
                draggableDeviceData: {
                  device,
                  target
                }
              },
              () => {
                this.openForm('forceDropDevice');
              }
            );
          }
          // Если есть одна доступная зона для привязки
          else if (availableRegionsCount === 1) {
            // Если подсистема совпадает
            if (availableRegionBySubsystemIds.includes(availableRegions[0].info.regionId)) {
              this.setState({
                draggableDeviceData: {
                  device,
                  regionIds: availableRegionBySubsystemIds,
                  target
                }
              });
              this.openForm('changeDeviceRegion');
            }
            // Если не совпадат
            else {
              this.setState({
                draggableDeviceData: {
                  device,
                  regionIds: availableRegionForReplaceIds,
                  availableRegions,
                  target
                }
              });
              this.openForm('replacePlanRegion');
            }
          }
          // Если больше одной
          else if (availableRegionsCount > 1) {
            this.setState({
              draggableDeviceData: {
                device,
                regionIds: availableRegionForReplaceIds.concat(availableRegionBySubsystemIds),
                availableRegions,
                target
              }
            });
            this.openForm('selectDeviceRegion');
          }
        }
      }
      // Если не привязано к зоне
      else {
        // Ищем зоны, к которым можно привязать устройство
        const availableRegionIds = [];
        const availableRegions = regionsIntersectionsWithDevice.filter(region => {
          if (region.info.subsystem === device.subsystem) {
            if (!availableRegionIds.includes(region.info.regionId))
              availableRegionIds.push(region.info.regionId);
            return true;
          } else return false;
        });
        const availableRegionsCount = availableRegionIds.length;
        // Если нет доступных зон
        if (availableRegionsCount === 0) {
          this.setState(
            {
              draggableDeviceData: {
                device,
                target
              }
            },
            () => {
              this.openForm('forceDropDevice');
            }
          );
        }
        // Если есть одна доступная зона для привязки
        else if (availableRegionsCount === 1) {
          this.updatePlanDevice(target);
          this.updateDeviceRegion(device, availableRegions[0].info.regionId);
        }
        // Если есть одна доступная зона для привязки
        else if (availableRegionsCount > 1) {
          this.setState({
            draggableDeviceData: {
              device,
              regionIds: availableRegionIds,
              availableRegions,
              target
            }
          });
          this.openForm('selectDeviceRegion');
        }
      }
    }
  };

  checkNewPlanDeviceIntersections = (device, options) => {
    // Получаем зоны, с которыми устройство пересекается в текущем месте
    const regionsIntersectionsWithDevice = getTargetIntersectionsWithObjects(
      this.invisibleObject,
      this.planRegions
    );
    const intersectionsCount = regionsIntersectionsWithDevice.length;

    // Если устройство добавляется в пустое место плана
    if (!intersectionsCount) {
      // Если привязано к зоне, то спрашиваем про отвязку
      if (device.regionId) {
        this.setState({
          draggableDeviceData: {
            device,
            options
          }
        });
        this.openForm('deleteDeviceRegion');
      }
      // Если не привязано к зоне, то добавляем его на план
      else this.createPlanDevice(options);
    }
    // Если устройство добавляется в место, где есть зона(ы)
    else {
      // Если привязано к зоне
      if (device.regionId) {
        // Ищем зону, к которой привязано устройство
        const hasIntersectionsWithDeviceRegion = regionsIntersectionsWithDevice.find(
          planRegion => planRegion.info.regionId === device.regionId
        );
        // Если есть зона, которой привязано устройство
        if (hasIntersectionsWithDeviceRegion) {
          // Размещаем устройство на плане
          this.createPlanDevice(options);
        }
        // Если нет зоны, к которой привязано устройство
        else {
          // Ищем зоны, к которым можно привязать устройство
          const availableRegionBySubsystemIds = [];
          const availableRegionForReplaceIds = [];
          const availableRegions = regionsIntersectionsWithDevice.filter(region => {
            if (region.info.subsystem === device.subsystem) {
              if (!availableRegionBySubsystemIds.includes(region.info.regionId))
                availableRegionBySubsystemIds.push(region.info.regionId);
              return true;
            } else if (
              !getTargetIntersectionsWithObjects(
                region,
                this.planDevices.filter(
                  planDevice => planDevice.info.regionId === region.info.regionId
                )
              ).length
            ) {
              if (!availableRegionForReplaceIds.includes(region.info.regionId))
                availableRegionForReplaceIds.push(region.info.regionId);
              return true;
            } else return false;
          });
          const availableRegionsCount =
            availableRegionBySubsystemIds.length + availableRegionForReplaceIds.length;
          // Если нет доступных зон
          if (!availableRegionsCount) {
            this.setState(
              {
                draggableDeviceData: {
                  device,
                  options
                }
              },
              () => {
                this.openForm('forceDropDevice');
              }
            );
          }
          // Если есть одна доступная зона для привязки
          else if (availableRegionsCount === 1) {
            // Если подсистема совпадает
            if (availableRegionBySubsystemIds.includes(availableRegions[0].info.regionId)) {
              this.setState({
                draggableDeviceData: {
                  device,
                  regionIds: availableRegionBySubsystemIds,
                  options
                }
              });
              this.openForm('changeDeviceRegion');
            }
            // Если не совпадат
            else if (availableRegionForReplaceIds.includes(availableRegions[0].info.regionId)) {
              this.setState({
                draggableDeviceData: {
                  device,
                  regionIds: availableRegionForReplaceIds,
                  availableRegions,
                  options
                }
              });
              this.openForm('replacePlanRegion');
            } else {
              this.setState(
                {
                  draggableDeviceData: {
                    device,
                    options
                  }
                },
                () => {
                  this.openForm('forceDropDevice');
                }
              );
            }
          }
          // Если больше одной
          else if (availableRegionsCount > 1) {
            this.setState({
              draggableDeviceData: {
                device,
                regionIds: availableRegionForReplaceIds.concat(availableRegionBySubsystemIds),
                availableRegions,
                options
              }
            });
            this.openForm('selectDeviceRegion');
          }
        }
      }
      // Если не привязано к зоне
      else {
        // Ищем зоны, к которым можно привязать устройство
        const availableRegionIds = [];
        const availableRegions = regionsIntersectionsWithDevice.filter(region => {
          if (region.info.subsystem === device.subsystem) {
            if (!availableRegionIds.includes(region.info.regionId))
              availableRegionIds.push(region.info.regionId);
            return true;
          } else return false;
        });
        const availableRegionsCount = availableRegionIds.length;
        // Если нет доступных зон
        if (availableRegionsCount === 0) {
          this.setState(
            {
              draggableDeviceData: {
                device,
                options
              }
            },
            () => {
              this.openForm('forceDropDevice');
            }
          );
        }
        // Если есть одна доступная зона для привязки
        else if (availableRegionsCount === 1) {
          this.createPlanDevice(options);
          this.updateDeviceRegion(device, availableRegions[0].info.regionId);
        }
        // Если есть одна доступная зона для привязки
        else if (availableRegionsCount > 1) {
          this.setState({
            draggableDeviceData: {
              device,
              regionIds: availableRegionIds,
              availableRegions,
              options
            }
          });
          this.openForm('selectDeviceRegion');
        }
      }
    }
  };

  objMoving = options => {
    const { target } = options;
    if (target.info) {
      const { grid, gridSize } = this.state;
      const { currentPlan } = this.props;
      target.set({
        top: getObjTopPosition(target, currentPlan, grid, gridSize),
        left: getObjLeftPosition(target, currentPlan, grid, gridSize)
      });
    }
  };

  setActiveTool = tool => {
    const { key, selectable, type } = tool;
    const { activeTool, fabricCanvas, isChecked, isActiveProject } = this.state;
    fabricCanvas.selection = false;
    switch (type) {
      case 'flag': {
        this.setState({
          isChecked: isChecked.includes(key)
            ? isChecked.filter(item => item !== key)
            : [...isChecked, key],
          [key]: !this.state[key]
        });
        return;
      }
      case 'button': {
        if (!selectable) this.toolActions(key);
        else {
          this.setState({ activeTool: key });
          if (!isActiveProject) setObjectSelectable(fabricCanvas, key === 'select');
          this.resetPolygonData(true, true, fabricCanvas);
          this.resetRectData(true, true, fabricCanvas);
          switch (activeTool) {
            case TOOL_KEYS.POLYGON: {
              if (this.polygonLines.length) {
                this.stopPolygonDrawing();
              }
              break;
            }
            case TOOL_KEYS.RECT: {
              if (this.rectLines.length) {
                this.stopRectDrawing();
              }
              break;
            }
            case 'moveBackground': {
              const background = fabricCanvas
                .getObjects()
                .find(object => object.info && object.info.backgroundKey);
              if (background) {
                background.set({ selectable: false, hoverCursor: CURSOR_STYLES.DEFAULT });
              }
              break;
            }

            default:
              break;
          }
          switch (key) {
            case 'moveBackground': {
              const background = fabricCanvas
                .getObjects()
                .find(object => object.info && object.info.backgroundKey);
              if (background) {
                // Таймаут для обхода сброса выделения в this.bindActions
                setTimeout(() => {
                  background.set({ selectable: true, hoverCursor: CURSOR_STYLES.MOVE });
                  background.selectable = true;
                  background.hoverCursor = 'move';
                  fabricCanvas.setActiveObject(background);
                  fabricCanvas.renderAll();
                }, 50);
              }
              break;
            }
            default:
              break;
          }
        }
        return;
      }
      default:
        return;
    }
  };

  toolActions = key => {
    switch (key) {
      case 'delete': {
        this.deletePlanObject();
        break;
      }
      case 'background': {
        this.openForm('newBackground');
        break;
      }
      case 'deleteBackground': {
        const { fabricCanvas } = this.state;
        const background = fabricCanvas
          .getObjects()
          .find(object => object.info && object.info.backgroundKey);
        if (background) {
          fabricCanvas.setActiveObject(background);
          this.deletePlanObject();
        }
        break;
      }
      default:
        break;
    }
  };

  changeSelect = option => {
    this[option.action](option.value);
  };

  /**
   * Инициализация
   **/

  init = (props, fabricCanvas) => {
    const { currentPlanId, regions, devices, currentPlan } = props;
    const { plan } = this.state;
    const gridSize = localStorage[currentPlanId] || this.state.gridSize;
    if (fabricCanvas && currentPlan) {
      if (
        !plan ||
        currentPlanId !== plan.id ||
        plan.xSize !== currentPlan.xSize ||
        plan.ySize !== currentPlan.ySize
      ) {
        this.setState({
          plan: currentPlan,
          gridSize
        });
        this.resetRectData(fabricCanvas, true, true, true);
        this.resetPolygonData(fabricCanvas, true, true, true);
        fabricCanvas.initialize(this.editor, {
          height: currentPlan.ySize + 1,
          width: currentPlan.xSize + 1,
          backgroundColor: '#fff'
        });
        fabricCanvas.discardActiveObject();
        this.drawGrid(fabricCanvas, currentPlan, gridSize);
      }
      if (currentPlan.backgrounds)
        this.drawBackgrounds(fabricCanvas, currentPlan.backgrounds, currentPlan);
      if (regions) this.drawRegions(fabricCanvas, regions, currentPlan);
      if (devices) this.drawDevices(fabricCanvas, devices, currentPlan);
      fabricCanvas.renderAll();
    }
  };

  /**
   * Функции отрисовки сетки
   **/

  changeGridSize = value => {
    const { currentPlanId } = this.props;
    const { fabricCanvas, plan } = this.state;
    const oldGrid = fabricCanvas.getObjects().find(object => object.info && object.info.gridKey);
    if (oldGrid) fabricCanvas.remove(oldGrid);
    const gridSize = parseInt(value, 10);
    localStorage.setItem(currentPlanId, gridSize);
    this.setState({ gridSize });
    this.drawGrid(fabricCanvas, plan, gridSize);
  };

  drawGrid = (fabricCanvas, currentPlan, gridSizeValue) => {
    const { xSize, ySize } = currentPlan;
    const gridSize = parseInt(gridSizeValue, 10);
    const gridLines = [];
    for (let x = 0; x <= xSize; x += gridSize)
      gridLines.push(
        new Line([x, 0, x, ySize], {
          stroke: 'black',
          strokeWidth: 1,
          selectable: false,
          hasControls: false
        })
      );
    for (let y = 0; y <= ySize; y += gridSize)
      gridLines.push(
        new Line([0, y, xSize, y], {
          stroke: 'black',
          strokeWidth: 1,
          selectable: false,
          hasControls: false
        })
      );
    const grid = new Group(gridLines, {
      info: { gridKey: 'grid' },
      hasControls: false,
      hoverCursor: 'default',
      opacity: 0.4
    });
    grid.selectable = false;
    fabricCanvas.add(grid);
    this.getPlanObjectsAndSortByLayer(fabricCanvas);
  };

  /**
   * Функции отрисовки и отображения popup-а
   **/

  getPopupText = info => {
    let text;
    if (info.regionKey) text = `${info.regionIndex}. ${info.name}`;
    else if (info.deviceKey) text = `${info.name} (${info.addressPath})`;
    return text;
  };

  showPopup = options => {
    const { activeTool } = this.state;
    const { target } = options;
    if (this.popupEl && target && target.info && activeTool === TOOL_KEYS.SELECT) {
      const popupText = this.getPopupText(target.info);
      if (!popupText) return;
      this.popupEl.innerText = popupText;
      this.popupEl.style.display = 'block';
      const xPos = Number.parseInt(this.popupEl.style.left.split('px')[0], 10);
      const popupWIdth = this.popupEl.offsetWidth;
      // Проверяем выходит ли popup за пределы экрана, если да, то отображаем его слева от курсора
      if (xPos + popupWIdth > this.screen.width) {
        this.popupEl.style.left = xPos - 40 - popupWIdth + 'px';
      }
    }
  };

  hidePopup = options => {
    if (this.popupEl) {
      this.popupEl.style.display = 'none';
    }
  };

  /**
   * Функции отрисовки подложек
   **/

  drawBackgrounds = (fabricCanvas, backgrounds, currentPlan) => {
    const { backgroundsGroup } = this.state;
    if (backgrounds.length) {
      const newBackground = backgrounds[0];
      newBackground.info = { backgroundKey: currentPlan.id, svgContent: newBackground.svgContent };
      const oldBackground = fabricCanvas
        .getObjects()
        .find(object => object.info && object.info.backgroundKey);
      if (!oldBackground || backgroundsGroup.svgContent !== newBackground.svgContent) {
        this.renderBackgrounds(fabricCanvas, newBackground, currentPlan);
      } else if (
        backgroundsGroup.svgContent === newBackground.svgContent ||
        !isEqual(backgroundsGroup.topLeftPoint, newBackground.topLeftPoint)
      ) {
        this.updateBackground(newBackground, currentPlan);
      }
    } else {
      this.clearBackgrounds(fabricCanvas);
    }
  };

  clearBackgrounds = fabricCanvas => {
    const backgrounds = fabricCanvas
      .getObjects()
      .filter(object => object.info && object.info.backgroundKey);
    if (backgrounds) backgrounds.forEach(background => fabricCanvas.remove(background));
    this.setState({ backgroundsGroup: null });
  };

  renderBackgrounds = (fabricCanvas, background, currentPlan) => {
    this.drawBackground(fabricCanvas, background, true, currentPlan);
    this.setState({ backgroundsGroup: background });
  };

  drawBackground = (fabricCanvas, background, layering, currentPlan) => {
    const { topLeftPoint, svgContent, info } = background;
    const containerData = {
      left: topLeftPoint.x,
      top: topLeftPoint.y,
      info: info,
      hasControls: false
    };
    if (svgContent) {
      fabric.loadSVGFromString(svgContent, (objects, options) => {
        // Проверка на дубликат, если слой не успел очиститья или еще отрисовывается старый
        const backgrounds = fabricCanvas
          .getObjects()
          .filter(object => object.info && object.info.backgroundKey);
        backgrounds.forEach(item => {
          if (item.info.backgroundKey !== info.backgroundKey) fabricCanvas.remove(item);
        });
        const duplicate = backgrounds.find(
          background => background.info.backgroundKey === info.backgroundKey
        );
        if (duplicate) {
          if (duplicate.info.svgContent !== info.svgContent) fabricCanvas.remove(duplicate);
          else return;
        }

        const renderer = [];
        const picture = fabric.util.groupSVGElements(objects, options);
        if (picture._objects) {
          renderer.push(...picture._objects);
        } else {
          renderer.push(picture);
        }
        if (renderer) {
          const container = new Group(renderer, containerData);
          const scaleX = currentPlan.xSize / container.width;
          const scaleY = currentPlan.ySize / container.height;
          const scale = scaleX > scaleY ? scaleY : scaleX;
          container.set({
            scaleX: scale,
            scaleY: scale,
            selectable: false,
            hoverCursor: CURSOR_STYLES.DEFAULT
          });
          fabricCanvas.add(container);
          this.getPlanObjectsAndSortByLayer(fabricCanvas);
        }
      });
    }
  };

  updateBackground = (newBackground, currentPlan) => {
    const { topLeftPoint } = newBackground;
    const { fabricCanvas } = this.state;
    const updatedBackground = fabricCanvas
      .getObjects()
      .find(object => object.info && object.info.backgroundKey);
    if (updatedBackground) {
      const scaleX = currentPlan.xSize / updatedBackground.width;
      const scaleY = currentPlan.ySize / updatedBackground.height;
      const scale = scaleX > scaleY ? scaleY : scaleX;
      updatedBackground.set({
        top: topLeftPoint.y,
        left: topLeftPoint.x,
        scaleX: scale,
        scaleY: scale
      });
    }
  };

  /**
   * Функции отрисовки зон
   **/

  getPlanRegions = (regions, currentPlanId) => {
    const newRegionsGroup = [];
    regions.forEach((regionView, regionIndex) => {
      const { region } = regionView;
      regionView.planLayouts.forEach((layout, layoutIndex) => {
        if (layout.planId === currentPlanId)
          newRegionsGroup.push({
            ...layout,
            info: {
              ...layout,
              regionId: regionView.id,
              subsystem: region.subsystem,
              projectId: region.projectId,
              name: region.name,
              regionIndex: region.index,
              regionKey: `${regionIndex}.${layoutIndex}`
            }
          });
      });
    });
    return newRegionsGroup;
  };

  drawRegions = (fabricCanvas, regions, currentPlan) => {
    const newRegionsGroup = this.getPlanRegions(regions, currentPlan.id);
    const regionsGroup = fabricCanvas
      .getObjects()
      .filter(object => object.info && object.info.regionKey);
    if (!regionsGroup.length || regionsGroup.length !== newRegionsGroup.length) {
      regionsGroup.forEach(region => fabricCanvas.remove(region));
      this.renderRegions(fabricCanvas, newRegionsGroup);
    } else if (regionsGroup.length === newRegionsGroup.length) {
      this.updateRegions(newRegionsGroup);
    }
    this.getPlanObjectsAndSortByLayer(fabricCanvas);
  };

  renderRegions = (fabricCanvas, regionsGroup) => {
    regionsGroup.forEach(region => {
      const renderer = this.drawRegion(region);
      renderer.set({ info: region.info });
      this.checkEditable(renderer);
      fabricCanvas.add(renderer);
    });
    this.setState({ regionsGroup });
  };

  drawRegion = region => {
    const { points, rect } = region;
    if (rect) {
      return new Rect({
        left: fabric.util.array.min(points, 'x'),
        top: fabric.util.array.min(points, 'y'),
        width: Math.abs(points[1].x - points[0].x),
        height: Math.abs(points[1].y - points[0].y),
        fill: subsystems[region.info.subsystem].color,
        ...rectFields
      });
    } else {
      return new Polygon([...points], {
        left: fabric.util.array.min(points, 'x'),
        top: fabric.util.array.min(points, 'y'),
        fill: subsystems[region.info.subsystem].color,
        ...polygonFields
      });
    }
  };

  updateRegions = newRegionsGroup => {
    const { regionsGroup } = this.state;
    newRegionsGroup.forEach(newRegion => {
      const updatedRegion = regionsGroup.find(
        region => region.info.regionKey === newRegion.info.regionKey
      );
      if (updatedRegion && !isEqual(newRegion, updatedRegion)) {
        this.updateRegion(newRegion);
      }
    });
  };

  updateRegion = newRegion => {
    const { rect, points, info } = newRegion;
    const { fabricCanvas } = this.state;
    const updatedRegion = fabricCanvas
      .getObjects()
      .find(object => object.info && object.info.regionKey === newRegion.info.regionKey);
    if (updatedRegion) {
      updatedRegion.set({
        left: fabric.util.array.min(points, 'x'),
        top: fabric.util.array.min(points, 'y'),
        info
      });
      if (rect)
        updatedRegion.set({
          scaleX: 1,
          scaleY: 1,
          width: Math.abs(points[1].x - points[0].x),
          height: Math.abs(points[1].y - points[0].y)
        });
    }
  };

  /**
   * Функции отрисовки устройств
   **/

  getPlanDevices = (devices, currentPlanId) => {
    let newDevicesGroup = [];
    devices.forEach((deviceView, deviceIndex) => {
      deviceView.planLayouts.forEach((layout, layoutIndex) => {
        if (layout.planId === currentPlanId)
          newDevicesGroup.push({
            ...layout,
            info: {
              planId: layout.planId,
              name: deviceView.name,
              deviceKey: `${deviceView.id}.${layoutIndex}.${deviceIndex}`,
              deviceId: deviceView.id,
              addressPath: deviceView.shortAddressPath,
              attachableToRegion: deviceView.attachableToRegion,
              regionId: deviceView.regionId,
              profileId: deviceView.deviceProfileId
            },
            texture: deviceView.textureMedia
          });
      });
    });
    return newDevicesGroup;
  };

  drawDevices = (fabricCanvas, devices, currentPlan) => {
    const newDevicesGroup = this.getPlanDevices(devices, currentPlan.id);
    const devicesGroup = fabricCanvas
      .getObjects()
      .filter(object => object.info && object.info.deviceKey);
    if (!devicesGroup.length || devicesGroup.length !== newDevicesGroup.length) {
      devicesGroup.forEach(device => fabricCanvas.remove(device));
      this.renderDevices(fabricCanvas, newDevicesGroup, currentPlan);
    } else if (devicesGroup.length === newDevicesGroup.length) {
      this.updateDevices(fabricCanvas, newDevicesGroup, currentPlan);
    }
  };

  renderDevices = (fabricCanvas, devicesGroup, currentPlan) => {
    devicesGroup.forEach((device, index) => {
      this.drawDevice(fabricCanvas, device, currentPlan, devicesGroup.length - 1 === index);
    });
    this.setState({ devicesGroup });
  };

  drawDevice = (fabricCanvas, device, currentPlan, layering) => {
    const { coordinatePoint, texture, info } = device;
    const shape = new Rect({
      width: 48,
      stroke: 'black',
      strokeWidth: 2,
      fill: 'white',
      height: 48
    });
    const containerData = {
      left: coordinatePoint.x,
      top: coordinatePoint.y,
      scaleX: 0.5,
      scaleY: 0.5,
      info: info,
      hasControls: false
    };
    if (texture && texture.mediaType === 'SVG_CONTENT') {
      fabric.loadSVGFromString(texture.content, (objects, options) => {
        // Проверка на дубликат, если слой не успел очиститья или еще отрисовывается старый
        const devices = fabricCanvas
          .getObjects()
          .filter(object => object.info && object.info.deviceKey);
        devices.forEach(object => {
          if (object.info.planId !== currentPlan.id) fabricCanvas.remove(object);
        });
        const duplicate = devices.find(object => object.info.deviceKey === info.deviceKey);
        if (duplicate) return;

        const renderer = [];
        const picture = fabric.util.groupSVGElements(objects, options);
        if (picture._objects) {
          shape.set({ top: -25, left: -25 });
          picture.forEachObject(object => object.set({ stroke: 'black', zIndex: 2 }));
          renderer.push(shape, ...picture._objects);
        } else {
          picture.set({
            fill: 'black',
            stroke: 'black'
          });
          renderer.push(shape, picture);
        }
        if (renderer.length) {
          const container = new Group(renderer, containerData);
          this.checkEditable(container);
          fabricCanvas.add(container);
          if (layering) this.getPlanObjectsAndSortByLayer(fabricCanvas);
        }
      });
    } else {
      const renderer = new Text(info.name.substring(0, 2), {
        fontFamily: 'Helvetica, Arial, sans-serif',
        fontSize: '24',
        fill: 'black',
        left: 8,
        top: 12
      });
      const container = new Group([shape, renderer], containerData);
      this.checkEditable(container);
      fabricCanvas.add(container);
      if (layering) this.getPlanObjectsAndSortByLayer(fabricCanvas);
    }
  };

  updateDevices = (fabricCanvas, newDevicesGroup, currentPlan) => {
    const { devicesGroup } = this.state;
    newDevicesGroup.forEach(newDevice => {
      const updatedDevice = devicesGroup.find(
        device => device.info.deviceKey === newDevice.info.deviceKey
      );
      if (updatedDevice && !isEqual(newDevice, updatedDevice)) {
        if (newDevice.info.profileId === updatedDevice.info.profileId) {
          this.updateDevice(newDevice);
        } else {
          const deviceObject = fabricCanvas
            .getObjects()
            .find(object => object.info && object.info.deviceKey === newDevice.info.deviceKey);
          fabricCanvas.remove(deviceObject);
          this.drawDevice(fabricCanvas, newDevice, currentPlan);
        }
      }
    });
    this.setState({ devicesGroup: newDevicesGroup });
    this.getPlanObjectsAndSortByLayer(fabricCanvas);
  };

  updateDevice = newDevice => {
    const { coordinatePoint, info } = newDevice;
    const { fabricCanvas } = this.state;
    const updatedDevice = fabricCanvas
      .getObjects()
      .find(object => object.info.deviceKey === newDevice.info.deviceKey);
    if (updatedDevice) updatedDevice.set({ top: coordinatePoint.y, left: coordinatePoint.x, info });
  };

  /**
   * Функции создания новых объектов плана
   **/

  createPlanBackground = values => {
    const { modalClose, doUpdatePlan, currentPlan, project } = this.props;
    const { svgContent } = values;
    const newBackground = {
      topLeftPoint: {
        x: 0,
        y: 0
      },
      svgContent
    };
    currentPlan.backgrounds = [];
    currentPlan.backgrounds.push(newBackground);
    doUpdatePlan(currentPlan, project.id);
    modalClose('newBackground');
  };

  createPlanDevice = options => {
    const {
      draggableDeviceId,
      doUpdateDraggableDevice,
      doUpdateDevicePlanLayouts,
      project,
      currentPlanId,
      devicesHash
    } = this.props;
    const { fabricCanvas, grid } = this.state;
    fabricCanvas.discardActiveObject();

    const _mouse = this._getPointer(options.e, grid);
    const newLayout = {
      planId: currentPlanId,
      coordinatePoint: { x: _mouse.x, y: _mouse.y }
    };

    const updatedDeviceView = devicesHash[draggableDeviceId];
    updatedDeviceView.planLayouts.push(newLayout);
    doUpdateDraggableDevice(null);
    this.setState({ draggableDeviceData: {} });
    doUpdateDevicePlanLayouts(project.id, draggableDeviceId, updatedDeviceView.planLayouts);
  };

  createPlanRegion = regionId => {
    const { doUpdateRegionPlanLayouts, modalClose, currentPlan, regionsHash, project } = this.props;
    const regionView = regionsHash[regionId];
    const newLayout = {
      planId: currentPlan.id,
      rect: this.rectPoints.length ? true : false,
      points: this.rectPoints.length ? this.rectPoints : filterSamePoints(this.polygonPoints)
    };
    const planLayouts = [...regionView.planLayouts, newLayout];
    doUpdateRegionPlanLayouts(project.id, regionView.id, planLayouts);
    modalClose('createPlanRegion');
    this.resetRectData(true, true);
    this.resetPolygonData(true, true);
  };

  /**
   * Функции обновления объектов плана
   **/

  updatePlanBackground = target => {
    const { doUpdatePlan, currentPlan, project } = this.props;
    const updatedBackground = currentPlan.backgrounds[0];
    updatedBackground.topLeftPoint = { x: target.left, y: target.top };
    doUpdatePlan(currentPlan, project.id);
  };

  updatePlanRegion = (points, target) => {
    const { doUpdateRegionPlanLayouts, modalClose, currentPlan, regions, project } = this.props;
    const uniqKeys = target.info.regionKey.split('.');
    const regionView = regions[parseInt(uniqKeys[0], 10)];
    regionView.planLayouts[uniqKeys[1]] = {
      planId: currentPlan.id,
      rect: target.type === 'rect',
      points: points
    };
    doUpdateRegionPlanLayouts(project.id, regionView.id, regionView.planLayouts);
    modalClose('createPlanRegion');
  };

  updatePlanDevice = target => {
    const { selectedObject } = this.state;
    const { doUpdateDevicePlanLayouts, currentPlan, devicesHash, project } = this.props;
    const uniqKeys = target.info.deviceKey.split('.');
    const deviceView = devicesHash[uniqKeys[0]];
    deviceView.planLayouts[parseInt(uniqKeys[1], 10)] = {
      planId: currentPlan.id,
      coordinatePoint: { x: target.left, y: target.top }
    };

    this.setState({
      draggableDeviceData: {},
      selectedObject: { ...selectedObject, position: { x: target.left, y: target.top } }
    });
    doUpdateDevicePlanLayouts(project.id, deviceView.id, deviceView.planLayouts);
  };

  /**
   * Функции удаления объектов плана
   **/

  deletePlanObject = () => {
    const { fabricCanvas } = this.state;
    const planObject = fabricCanvas ? fabricCanvas.getActiveObject() : null;
    if (planObject && planObject.info && !planObject.isMoving) {
      fabricCanvas.discardActiveObject();
      const { info } = planObject;
      const modalParams = {
        okText: 'Удалить',
        onCancel: () => {},
        cancelText: 'Отмена',
        maskClosable: false
      };
      if (planObject.info.regionKey) {
        modalParams['title'] = 'Удаление зоны с плана';
        modalParams['content'] = (
          <span>
            Удалить зону{' '}
            <b>
              {info.regionIndex}. {info.name}
            </b>{' '}
            с плана?
          </span>
        );
        modalParams['onOk'] = () => {
          this.deletePlanRegion(planObject);
        };
      } else if (planObject.info.deviceKey) {
        modalParams['title'] = 'Удаление устройства с плана';
        modalParams['content'] = (
          <span>
            Удалить устройство{' '}
            <b>
              {info.name}
              {info.addressPath ? `(${info.addressPath})` : ''}
            </b>{' '}
            с плана?
          </span>
        );
        modalParams['onOk'] = () => {
          this.deletePlanDevice(planObject);
        };
      } else if (planObject.info.backgroundKey) {
        modalParams['title'] = 'Удаление фонового изображения';
        modalParams['content'] = `Удалить фоновое изображение плана?`;
        modalParams['onOk'] = () => {
          this.deletePlanBackground(planObject);
        };
      }
      if (modalParams['onOk']) {
        Modal.confirm(modalParams);
      }
    }
  };

  deletePlanRegion = region => {
    const { doUpdateRegionPlanLayouts, regions, project } = this.props;
    const uniqKeys = region.info.regionKey.split('.');
    const regionView = regions[parseInt(uniqKeys[0], 10)];
    const planLayouts = [
      ...regionView.planLayouts.filter((layout, index) => index !== parseInt(uniqKeys[1], 10))
    ];
    doUpdateRegionPlanLayouts(project.id, regionView.id, planLayouts);
  };

  deletePlanDevice = target => {
    const { doUpdateDevicePlanLayouts, devicesHash, project } = this.props;
    const uniqKeys = target.info.deviceKey.split('.');
    const deviceView = devicesHash[target.info.deviceId];
    const planLayouts = [
      ...deviceView.planLayouts.filter((layout, index) => index !== parseInt(uniqKeys[1], 10))
    ];
    doUpdateDevicePlanLayouts(project.id, target.info.deviceId, planLayouts);
  };

  deletePlanBackground = background => {
    const { doUpdatePlan, currentPlan, project } = this.props;
    currentPlan.backgrounds = [];
    doUpdatePlan(currentPlan, project.id);
  };

  render() {
    const { currentPlan } = this.props;
    const {
      activeTool,
      fabricCanvas,
      isActiveProject,
      isChecked,
      draggableDeviceData,
      gridSize,
      selectedRegionInfo
    } = this.state;
    const selectedObject = fabricCanvas ? fabricCanvas.getActiveObject() : null;
    return (
      <div
        style={{
          height: 'calc(100% - 70px)'
        }}
      >
        <PlanEditorToolbar
          onButtonClick={activeTool => this.setActiveTool(activeTool)}
          onChange={option => this.changeSelect(option)}
          isChecked={isChecked}
          isActive={!isActiveProject}
          activeTool={activeTool}
          selectedObject={selectedObject}
          gridSize={gridSize}
          toolsParams={{
            moveBackground: {
              disabled: !Boolean(currentPlan.backgrounds && currentPlan.backgrounds.length)
            },
            deleteBackground: {
              disabled: !Boolean(currentPlan.backgrounds && currentPlan.backgrounds.length)
            }
          }}
        />
        <NewPlanBackgroundForm modalName="newBackground" onSubmit={this.createPlanBackground} />
        {/* Расположить поверх зоны с другой подсистемой */}
        <UpdateDeviceRegionForm
          modalName="forceDropDevice"
          actionType="FORCE"
          deviceData={draggableDeviceData}
          onForceDropDeviceConfirm={this.forceDropDeviceConfirm}
          onForceDropDeviceCancel={this.forceDropDeviceCancel}
        />
        {/* Отвязать устройство */}
        <UpdateDeviceRegionForm
          modalName="deleteDeviceRegion"
          actionType="DELETE"
          deviceData={draggableDeviceData}
          onDeleteDeviceRegionConfirm={this.deleteDeviceRegionConfirm}
          onDeleteDeviceRegionReject={this.deleteDeviceRegionReject}
          onDeleteDeviceRegionCancel={this.deleteDeviceRegionCancel}
        />
        {/* Заменить зону на плане */}
        <UpdateDeviceRegionForm
          actionType="REPLACE"
          deviceData={draggableDeviceData}
          modalName="replacePlanRegion"
          onReplacePlanRegionConfirm={this.replacePlanRegionConfirm}
          onReplacePlanRegionReject={this.replacePlanRegionReject}
          onReplacePlanRegionCancel={this.replacePlanRegionCancel}
        />
        {/* Перепривязать устройство к другой зоне */}
        <UpdateDeviceRegionForm
          actionType="CHANGE"
          modalName="changeDeviceRegion"
          deviceData={draggableDeviceData}
          onChangeDeviceRegionConfirm={this.changeDeviceRegionConfirm}
          onChangeDeviceRegionReject={this.changeDeviceRegionReject}
          onChangeDeviceRegionCancel={this.changeDeviceRegionCancel}
        />
        {/* Выбор зоны для привязки */}
        <UpdateDeviceRegionForm
          actionType="SELECT"
          modalName="selectDeviceRegion"
          deviceData={draggableDeviceData}
          onSelectDeviceRegionConfirm={this.selectDeviceRegionConfirm}
          onSelectDeviceRegionCancel={this.selectDeviceRegionCancel}
        />
        <PlanRegionForm modalName="createPlanRegion" onSubmit={this.createPlanRegion} />
        <PlanRegionForm
          regionInfo={selectedRegionInfo}
          modalName="updatePlanRegion"
          isEdit
          onSubmit={this.onUpdatePlanRegionLayout}
        />
        {/* Popup-элемент */}
        <div
          style={POPUP_STYLES}
          ref={node => {
            if (node) this.popupEl = node;
          }}
        />
        <div
          ref={node => {
            if (node) this.pane = node;
          }}
          style={{
            display: this.pane
              ? this.pane.offsetWidth < currentPlan.xSize
                ? 'block'
                : 'flex'
              : 'flex',
            justifyContent: 'center',
            background: 'lightGrey',
            height: 'calc(100% - 37px)',
            overflow: 'auto',
            padding: '3px'
          }}
        >
          <div
            style={{
              height: currentPlan.ySize + 6,
              width: currentPlan.xSize + 6,
              padding: '1px',
              border: '2px outset',
              overflow: 'hidden'
            }}
          >
            <canvas ref={node => (this.editor = node)} id="c" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: getCurrentProject(state),
    plans: getCurrentProjectPlans(state),
    planGroups: getCurrentProjectPlanGroups(state),
    regions: getCurrentProjectRegionViews(state),
    draggableDeviceId: state.widgets.draggableDeviceId,
    devices: getCurrentProjectDeviceList(state),
    devicesHash: getCurrentProjectDevicesHash(state),
    regionsHash: getCurrentProjectRegionsHash(state),
    modals: state.modals
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    modalOpen: bindActionCreators(modalOpen, dispatch),
    modalClose: bindActionCreators(modalClose, dispatch),
    doUpdatePlan: bindActionCreators(updatePlan, dispatch),
    doUpdateRegionPlanLayouts: bindActionCreators(updateRegionPlanLayouts, dispatch),
    doUpdateDevicePlanLayouts: bindActionCreators(updateDevicePlanLayouts, dispatch),
    doUpdateDraggableDevice: bindActionCreators(updateDraggableDevice, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanEditor);
