import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { isEmpty } from 'lodash';
import { Spin, Tabs, Modal } from 'antd';
const TabPane = Tabs.TabPane;

import {
  createPlanGroup,
  updatePlanGroup,
  deletePlanGroup,
  createPlan,
  updatePlan,
  deletePlan
} from 'actions/plans';
import { modalOpen, modalClose } from 'actions/modals';

import Text from 'components/Text';
import TripleCRUDMenu from 'components/Menu/WidgetMenu';

import PlanGroupForm from 'containers/Forms/PlanGroupForm';
import PlanForm from 'containers/Forms/PlanForm';
import PlanEditor from './PlanEditor';

import {
  getCurrentProjectPlans,
  getCurrentProjectPlanGroups,
  checkCurrentProjectIsActive
} from 'helpers/currentProject';

const PlanEditorWrapper = styled.div`
  height: 100%;
  background-color: white;
  .ant-tabs-bar {
    height: 36px;
    margin: 0;
  }
`;
const MODAL_NAMES = {
  PLAN_GROUP: {
    ADD: 'addPlanGroupForm',
    EDIT: 'editPlanGroupForm'
  },
  PLAN: {
    ADD: 'addPlanForm',
    EDIT: 'editPlanForm'
  }
};
const MENU_PARAMS = {
  css: `
    background-color: white !important;
    color: #1890FF !important;
  `,
  iconStyles: { color: '#1890FF' }
};

class PlanEditorWidget extends Component {
  static propTypes = {
    plans: PropTypes.array, // Планы помещений текущего проекта
    planGroups: PropTypes.array // Группы планов помещений текущего проекта
  };
  constructor(props) {
    super(props);
    this.state = {
      activePlan: {}, // Сущность активного плана
      activePlanGroupId: 'allPlans', // Задается при первичной загрузке
      activePlanGroup: {}, // Сущность активной группы планов
      activePlanIdByGroupId: {}, // Идентификатор активного плана для группы
      panes: [] // Вкладки групп с планами
    };
  }
  // Переключатель на план, на котором было размещено устройство
  onUpdatePlanLayouts = event => {
    const { planId } = event.detail;
    this.updateActivePlan(planId);
  };
  // Прокидывание данных в state компонента
  propsToState = (props, needRerender) => {
    const { planGroups, plans } = props;
    if (plans && planGroups) {
      let { activePlanGroupId, activePlanIdByGroupId, activePlan, activePlanGroup } = this.state;
      if (needRerender) {
        activePlanGroupId = 'allPlans';
        activePlanIdByGroupId = {};
        activePlan = {};
        activePlanGroup = {};
      }
      const panes = [
        ...planGroups.map(group => {
          if (activePlanGroup && activePlanGroup.id && activePlanGroup.id === group.id) {
            this.setState({ activePlanGroup: group });
          }
          return {
            key: group.id,
            name: group.name,
            description: group.description || group.name,
            subPanes: plans
              .filter(plan => plan.planGroupId === group.id)
              .map(plan => ({
                key: plan.id,
                name: plan.name,
                description: plan.description || plan.name
              }))
          };
        }),
        {
          key: 'allPlans',
          name: 'Все планы',
          description: 'Все планы помещений без группировки',
          subPanes: plans.map(plan => {
            if (activePlan && activePlan.id && activePlan.id === plan.id) {
              this.setState({ activePlan: plan });
            }
            return {
              key: plan.id,
              name: plan.name,
              description: plan.name || plan.name
            };
          })
        }
      ];
      if (!activePlanIdByGroupId[activePlanGroupId]) {
        let activePane = null;
        if (activePlanGroupId === 'allPlans') activePane = panes[panes.length - 1];
        else activePane = panes.find(pane => pane.key === activePlanGroupId);
        if (activePane) {
          const newActiveSubPane = activePane.subPanes[0];
          if (newActiveSubPane) {
            activePlanIdByGroupId[activePlanGroupId] = newActiveSubPane.key;
            const newActivePlan = plans.find(plan => plan.id === newActiveSubPane.key);
            this.setState({
              activePlanIdByGroupId,
              activePlan: newActivePlan
            });
          }
        }
      }
      this.setState({
        panes
      });
    }
  };
  /**
   * Методы для работы с формами
   */
  // Открыть форму
  openForm = name => {
    const { doModalOpen } = this.props;
    doModalOpen(name);
  };
  // Закрыть форму
  closeForm = name => {
    const { doModalClose } = this.props;
    doModalClose(name);
  };
  /**
   * Методы для работы с группами планов
   */
  // Добавление новой группы
  addPlanGroup = values => {
    const { doCreatePlanGroup, projectId } = this.props;
    const newPlanGroup = { ...values };
    doCreatePlanGroup(newPlanGroup, projectId);
    this.closeForm(MODAL_NAMES.PLAN_GROUP.ADD);
  };
  // Обновление группы
  updatePlanGroup = values => {
    const { doUpdatePlanGroup, projectId } = this.props;
    const { activePlanGroup } = this.state;
    const updatedPlanGroup = { ...activePlanGroup, ...values };
    doUpdatePlanGroup(updatedPlanGroup, projectId);
    this.closeForm(MODAL_NAMES.PLAN_GROUP.EDIT);
  };
  // Удаление группы
  deletePlanGroup = () => {
    const { doDeletePlanGroup, projectId, plans, planGroups, isActiveProject } = this.props;
    const { activePlanGroupId, panes, activePlanIdByGroupId } = this.state;
    if (!isActiveProject) {
      // Ищем новую активную группу
      const newActivePane = panes.filter(pane => pane.key !== activePlanGroupId)[0];
      const newActivePlanGroup =
        planGroups.find(planGroup => planGroup.id === newActivePane.key) || {};
      this.setState(
        {
          activePlanGroupId: newActivePane.key,
          activePlanGroup: newActivePlanGroup
        },
        () => {
          doDeletePlanGroup(activePlanGroupId, projectId);
        }
      );
      if (activePlanIdByGroupId[newActivePane.key]) {
        const newActivePlan =
          plans.find(plan => plan.id === activePlanIdByGroupId[newActivePane.key]) || {};
        this.setState({ activePlan: newActivePlan });
      } else {
        const newActiveSubPane = newActivePane.subPanes[0];
        const newActivePlan = newActiveSubPane
          ? plans.find(plan => plan.id === newActiveSubPane.key) || {}
          : {};
        activePlanIdByGroupId[newActivePane.key] = newActiveSubPane ? newActiveSubPane.key : '';
        this.setState({
          activePlanIdByGroupId,
          activePlan: newActivePlan
        });
      }
    }
  };
  onDeletePlanGroup = () => {
    const { activePlanGroupId, panes } = this.state;
    const activePane = panes.find(pane => pane.key === activePlanGroupId);
    Modal.confirm({
      title: 'Удаление группы планов',
      content: (
        <span>
          Удалить группу планов <b>{activePane.name}</b>?
          <div>(Планы, находящиеся в группе, НЕ будут удалены)</div>
        </span>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        this.deletePlanGroup();
      }
    });
  };
  // Обновление активной группы
  updateActivePlanGroup = planGroupKey => {
    const { plans, planGroups } = this.props;
    const { panes, activePlanIdByGroupId } = this.state;
    const newActivePane = panes.find(pane => pane.key === planGroupKey);
    const newActivePlanGroup =
      planGroupKey === 'allPlans'
        ? {}
        : planGroups.find(planGroup => planGroup.id === planGroupKey) || {};
    this.setState({ activePlanGroupId: newActivePane.key, activePlanGroup: newActivePlanGroup });
    if (activePlanIdByGroupId[newActivePane.key]) {
      const newActivePlan =
        plans.find(plan => plan.id === activePlanIdByGroupId[newActivePane.key]) || {};
      this.setState({ activePlan: newActivePlan });
    } else {
      const newActiveSubPane = newActivePane.subPanes[0];
      const newActivePlan = newActiveSubPane
        ? plans.find(plan => plan.id === newActiveSubPane.key) || {}
        : {};
      activePlanIdByGroupId[newActivePane.key] = newActiveSubPane ? newActiveSubPane.key : '';
      this.setState({
        activePlanIdByGroupId,
        activePlan: newActivePlan
      });
    }
  };
  /**
   * Методы для работы с планами
   */
  // Добавление нового плана
  addPlan = values => {
    const { doCreatePlan, projectId } = this.props;
    const newPlan = { ...values };
    doCreatePlan(newPlan, projectId);
    this.closeForm(MODAL_NAMES.PLAN.ADD);
  };
  // Обновление плана
  updatePlan = values => {
    const { doUpdatePlan, projectId } = this.props;
    const { activePlan } = this.state;
    const updatePlan = { ...activePlan, ...values };
    doUpdatePlan(updatePlan, projectId);
    this.closeForm(MODAL_NAMES.PLAN.EDIT);
  };
  // Удаление плана
  deletePlan = () => {
    const { doDeletePlan, projectId, plans, isActiveProject } = this.props;
    const { activePlanIdByGroupId, activePlanGroupId, panes } = this.state;
    if (!isActiveProject) {
      const activePane = panes.find(pane => pane.key === activePlanGroupId);
      const newActiveSubPane = activePane.subPanes.filter(
        subPane => subPane.key !== activePlanIdByGroupId[activePlanGroupId]
      )[0];
      const newActivePlan = newActiveSubPane
        ? plans.find(plan => plan.id === newActiveSubPane.key) || {}
        : {};
      this.setState(
        {
          activePlanIdByGroupId: {
            ...activePlanIdByGroupId,
            [activePlanGroupId]: newActiveSubPane ? newActiveSubPane.key : ''
          },
          activePlan: newActivePlan
        },
        () => {
          doDeletePlan(activePlanIdByGroupId[activePlanGroupId], projectId);
        }
      );
    }
  };
  onDeletePlan = () => {
    const { activePlanIdByGroupId, activePlanGroupId, panes } = this.state;
    const activePane = panes.find(pane => pane.key === activePlanGroupId);
    const activeSubPane = activePane.subPanes.find(
      subPane => subPane.key === activePlanIdByGroupId[activePlanGroupId]
    );
    Modal.confirm({
      title: 'Удаление плана',
      content: (
        <span>
          Удалить план <b>{activeSubPane.name}</b>?
        </span>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        this.deletePlan();
      }
    });
  };
  // Обновление активного плана
  updateActivePlan = planKey => {
    const { plans } = this.props;
    const { activePlanGroupId, activePlanIdByGroupId, panes } = this.state;
    const activePane = panes.find(pane => pane.key === activePlanGroupId);
    const newActiveSubPane = activePane.subPanes.find(subPane => subPane.key === planKey);
    const newActivePlan = plans.find(plan => plan.id === planKey) || {};
    if (newActiveSubPane) {
      this.setState({
        activePlanIdByGroupId: {
          ...activePlanIdByGroupId,
          [activePlanGroupId]: planKey
        },
        activePlan: newActivePlan
      });
    } else {
      this.setState({
        activePlanIdByGroupId: {
          ...activePlanIdByGroupId,
          allPlans: newActivePlan.id
        },
        activePlan: newActivePlan,
        activePlanGroupId: 'allPlans',
        activePlanGroup: {}
      });
    }
  };

  /**
   * Обновление состояния компонента
   */
  componentDidMount = () => {
    document.addEventListener('DEVICE_UPDATE_PLAN_LAYOUTS', this.onUpdatePlanLayouts);
    this.propsToState(this.props);
  };

  componentWillUnmount = () => {
    document.removeEventListener('DEVICE_UPDATE_PLAN_LAYOUTS', this.onUpdatePlanLayouts);
  };

  componentWillReceiveProps = nextProps => {
    let needRerender = false;
    if (this.props.projectId && nextProps.projectId && this.props.projectId !== nextProps.projectId)
      needRerender = true;
    this.propsToState(nextProps, needRerender);
  };

  render() {
    const { isActiveProject } = this.props;
    const {
      panes,
      activePlan,
      activePlanGroup,
      activePlanGroupId,
      activePlanIdByGroupId
    } = this.state;

    return (
      <PlanEditorWrapper>
        {/** Модальные окна */}
        {!isActiveProject ? (
          <div>
            <PlanGroupForm modalName={MODAL_NAMES.PLAN_GROUP.ADD} onSubmit={this.addPlanGroup} />
            <PlanForm
              modalName={MODAL_NAMES.PLAN.ADD}
              currentPlanGroupId={activePlanGroupId}
              onSubmit={this.addPlan}
            />
            {activePlanGroup ? (
              <PlanGroupForm
                currentPlanGroup={activePlanGroup}
                modalName={MODAL_NAMES.PLAN_GROUP.EDIT}
                isEdit={true}
                onSubmit={this.updatePlanGroup}
              />
            ) : null}
            {activePlan ? (
              <PlanForm
                currentPlan={activePlan}
                modalName={MODAL_NAMES.PLAN.EDIT}
                isEdit={true}
                onSubmit={this.updatePlan}
              />
            ) : null}
          </div>
        ) : null}
        {/** Вкладки с планами */}
        {panes.length ? (
          <Tabs
            activeKey={activePlanGroupId}
            onTabClick={this.updateActivePlanGroup}
            tabBarExtraContent={
              <TripleCRUDMenu
                createButtonTitle="Добавить группу"
                editButtonTitle="Изменить группу"
                deleteButtonTitle="Удалить группу"
                isCreateButtonActive={!isActiveProject}
                isDeleteButtonActive={
                  isEmpty(activePlanGroup) || isActiveProject
                    ? false
                    : activePlanGroup.key === 'allPlans'
                    ? false
                    : true
                }
                isEditButtonActive={
                  isEmpty(activePlanGroup) || isActiveProject
                    ? false
                    : activePlanGroup.key === 'allPlans'
                    ? false
                    : true
                }
                onCreateButtonClick={() => this.openForm(MODAL_NAMES.PLAN_GROUP.ADD)}
                onDeleteButtonClick={() => this.onDeletePlanGroup(activePlanGroup)}
                onEditButtonClick={() => this.openForm(MODAL_NAMES.PLAN_GROUP.EDIT)}
                {...MENU_PARAMS}
              />
            }
          >
            {panes.map(pane => (
              <TabPane
                key={pane.key}
                tab={<span title={pane.description}>{pane.name}</span>}
                forceRender
              >
                <Tabs
                  activeKey={activePlanIdByGroupId[activePlanGroupId]}
                  onTabClick={this.updateActivePlan}
                  tabBarExtraContent={
                    <TripleCRUDMenu
                      createButtonTitle="Добавить план"
                      editButtonTitle="Изменить план"
                      deleteButtonTitle="Удалить план"
                      isCreateButtonActive={!isActiveProject}
                      isDeleteButtonActive={!isEmpty(activePlan) && !isActiveProject}
                      isEditButtonActive={!isEmpty(activePlan) && !isActiveProject}
                      onCreateButtonClick={() => this.openForm(MODAL_NAMES.PLAN.ADD)}
                      onDeleteButtonClick={() => this.onDeletePlan(activePlan)}
                      onEditButtonClick={() => this.openForm(MODAL_NAMES.PLAN.EDIT)}
                      {...MENU_PARAMS}
                    />
                  }
                  size="large"
                >
                  {pane.subPanes.map(subPane => (
                    <TabPane
                      key={subPane.key}
                      tab={<span title={subPane.description}>{subPane.name}</span>}
                      forceRender
                    />
                  ))}
                </Tabs>
              </TabPane>
            ))}
          </Tabs>
        ) : null}
        {/** Отображение плана */}
        {activePlanIdByGroupId[activePlanGroupId] && activePlan && activePlan.id ? (
          <PlanEditor
            currentPlanId={activePlanIdByGroupId[activePlanGroupId]}
            currentPlan={activePlan}
          />
        ) : (
          <Spin>
            <Text>Нет данных</Text>
          </Spin>
        )}
      </PlanEditorWrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    projectId: state.currentProjectId,
    plans: getCurrentProjectPlans(state),
    planGroups: getCurrentProjectPlanGroups(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch,
    doDeletePlanGroup: bindActionCreators(deletePlanGroup, dispatch),
    doCreatePlanGroup: bindActionCreators(createPlanGroup, dispatch),
    doUpdatePlanGroup: bindActionCreators(updatePlanGroup, dispatch),
    doDeletePlan: bindActionCreators(deletePlan, dispatch),
    doCreatePlan: bindActionCreators(createPlan, dispatch),
    doUpdatePlan: bindActionCreators(updatePlan, dispatch),
    doModalOpen: bindActionCreators(modalOpen, dispatch),
    doModalClose: bindActionCreators(modalClose, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanEditorWidget);
