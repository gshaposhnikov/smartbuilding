import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';

import { DoubleCRUDMenu } from 'components/Menu/WidgetMenu';

import Widget from 'components/Widget';
import Table from 'components/Table';
import Text from 'components/Text';

import { selectUserGroup, deleteUserGroup, loadUserGroups } from 'actions/userGroups';

class UserGroupList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUserGroup: null,
      height: 500
    };
  }

  propsToState(props) {
    const { selectedUserGroupId, userGroups } = props;
    const newState = { ...this.state };
    if (userGroups) {
      newState.selectedUserGroup = userGroups[selectedUserGroupId];
      this.setState(newState);
    }
  }

  componentDidMount() {
    const { userGroups, loadUserGroupsError, dispatch } = this.props;
    if (!userGroups && !loadUserGroupsError) {
      dispatch(loadUserGroups());
    }
    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onUserGroupAdd() {
    const { dispatch } = this.props;
    dispatch(selectUserGroup(null));
  }

  onUserGroupDelete() {
    const { dispatch } = this.props;
    const { selectedUserGroup } = this.state;
    Modal.confirm({
      title: 'Удаление группы пользователей',
      content: (
        <div>
          Удалить группу пользователей <b>{selectedUserGroup.name}</b>?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteUserGroup(selectedUserGroup.id));
      }
    });
  }

  onUserGroupSelect(userGroup) {
    const { dispatch } = this.props;
    dispatch(selectUserGroup(userGroup.id));
  }

  rowClassName(record) {
    const { selectedUserGroup } = this.state;
    return selectedUserGroup && selectedUserGroup.id === record.id ? 'selected-row' : '';
  }

  render() {
    const { userGroups } = this.props;
    const { selectedUserGroup } = this.state;

    const columns = [
      { key: 'name', dataIndex: 'name', title: 'Наименование' },
      { key: 'description', dataIndex: 'description', title: 'Описание' }
    ];
    const rowSelection = {
      selectedRowKeys: selectedUserGroup ? selectedUserGroup.id : null,
      type: 'radio'
    };

    let userGroupIds = [];
    let userGroupsToView = [];
    if (userGroups) {
      userGroupIds = Object.keys(userGroups);
      userGroupsToView = userGroupIds.map(userGroupId => userGroups[userGroupId]);
    }

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <DoubleCRUDMenu
          isAddActive={true}
          onAdd={this.onUserGroupAdd.bind(this)}
          isDeleteActive={selectedUserGroup ? true : false}
          onDelete={this.onUserGroupDelete.bind(this)}
        />
        {userGroups && userGroupIds.length > 0 ? (
          <Table
            columns={columns}
            dataSource={userGroupsToView}
            rowKey={record => record.id}
            onRowClick={this.onUserGroupSelect.bind(this)}
            rowClassName={this.rowClassName.bind(this)}
            rowSelection={rowSelection}
          />
        ) : (
          <Text>Нет данных</Text>
        )}
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    userGroups: state.admin.userGroups,
    selectedUserGroupId: state.widgets.selectedUserGroupId,
    loadUserGroupsError: state.errors.loadUserGroupsError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserGroupList);
