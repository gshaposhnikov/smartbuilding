import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;
import { isEqual } from 'lodash';

import Form from 'components/Form';
import Widget from 'components/Widget';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';

import UserGroupInfo from 'containers/Widgets/UserGroupsWidget/UserGroupInfo';
import UserGroupAccess from 'containers/Widgets/UserGroupsWidget/UserGroupAccess';

import { createUserGroup, updateUserGroup, loadUserGroups } from 'actions/userGroups';
import { loadPermissions } from 'actions/login';
import { getFirstError } from 'helpers/form';

import { reduxForm, initialize, reset, getFormSyncErrors, getFormValues } from 'redux-form';
export const FORM_NAME = 'userGroupEditor';

class UserGroupInfoEditor extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedUserGroup: null,
      selectedTab: 'UserGroupInfo',
      height: 200
    };
  }

  fillAndGetUserGroup(props, selectedUserGroup) {
    const {
      userGroups,
      selectedUserGroupId,
      dispatch
    } = props;

    let newUserGroup = selectedUserGroupId ? userGroups[selectedUserGroupId] : null;
    if (!isEqual(selectedUserGroup, newUserGroup)) {
      if (!newUserGroup && selectedUserGroupId) {
        // создание новой группы пользователей
        newUserGroup = {
          name: '',
          description: '',
          permissionIds: []
        };
        if (!isEqual(selectedUserGroup, newUserGroup)) {
          dispatch(reset(FORM_NAME));
          dispatch(initialize(FORM_NAME, newUserGroup));
        }
      } else {
        // редактирование
        dispatch(initialize(FORM_NAME, newUserGroup));
      }
    }
    return newUserGroup;
  }

  propsToState(props) {
    const newState = { ...this.state };
    newState.selectedUserGroup = this.fillAndGetUserGroup(props, this.state.selectedUserGroup);
    this.setState(newState);
  }

  componentDidMount() {
    const {
      userGroups,
      permissions,
      loadUserGroupsError,
      loadPermissionsError,
      dispatch
    } = this.props;
    if (!userGroups && !loadUserGroupsError) {
      dispatch(loadUserGroups());
    }
    if (!permissions && !loadPermissionsError) {
      dispatch(loadPermissions());
    }
    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onTabClick(tabId) {
    this.setState({ ...this.state, selectedTab: tabId });
  }

  onUserGroupSave(values) {
    const { dispatch, userGroupValues } = this.props;
    const { selectedUserGroup } = this.state;
    if (!selectedUserGroup.id) {
      dispatch(createUserGroup(userGroupValues));
    } else {
      dispatch(updateUserGroup(selectedUserGroup.id, userGroupValues));
    }
  }

  render() {
    const { selectedUserGroup } = this.state;
    const {
      validateStatuses,
      valid,
      dirty,
      userGroupValues,
      permissions
    } = this.props;
    const components = [
      { name: 'UserGroupInfo', title: 'Общие', component:
        <UserGroupInfo selectedUserGroup={userGroupValues} validateStatuses={validateStatuses} /> },
      { name: 'UserGroupAccess', title: 'Права', component:
        <UserGroupAccess selectedUserGroup={userGroupValues} permissions={permissions} /> }
    ];
    const isEdit = selectedUserGroup && selectedUserGroup.id;
    const title = isEdit
      ? selectedUserGroup.name
      : 'Создать новую группу пользователей';
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <WidgetParamsEditorMenu
          isViewTitle={selectedUserGroup}
          title={title}
          errorMessage={getFirstError(validateStatuses)}
          isSaveActive={selectedUserGroup && valid && dirty}
          onSaveAction={this.onUserGroupSave.bind(this)}
        />
        <Form>
          <Tabs
            onTabClick={this.onTabClick.bind(this)}
          >
            {components.map(component => (
              <TabPane
                key={component.name}
                tab={component.title}
                forceRendrer={true}
              >
                {component.component}
              </TabPane>
            ))}
          </Tabs>
        </Form>
      </Widget>
    );
  }
}

const validate = values => {
  let result = {};

  if (!values.name) {
    result.name = 'Не задано название';
  } else if (values.name.length > 30) {
    result.name = 'Название должно быть не больше 30 символов';
  }

  if (values.description && values.description.length > 100) {
    result.description = 'Описание должно быть не больше 100 символов';
  }

  return result;
};

UserGroupInfoEditor = reduxForm({
  form: FORM_NAME,
  validate
})(UserGroupInfoEditor);

const mapStateToProps = state => {
  return {
    userGroups: state.admin.userGroups,
    permissions: state.admin.permissions,
    selectedUserGroupId: state.widgets.selectedUserGroupId,
    userGroupValues: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {},
    loadUserGroupsError: state.errors.loadUserGroupsError,
    loadPermissionsError: state.errors.loadPermissionsError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserGroupInfoEditor);
