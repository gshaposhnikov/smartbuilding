import React, { Component } from 'react';
import { Field } from 'redux-form';

import Widget from 'components/Widget';
import Transfer from 'components/Form/Transfer';

export default class UserGroupAccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
  }

  render() {
    const { selectedUserGroup, permissions } = this.props;
    const allPermissions = !permissions ? [] : permissions.permissions.map(perm => (
      {key: perm.id, title: perm.name}
    ));
    return (
      <Widget onHeightReady={height => this.setState({ height })}
        children={selectedUserGroup ? (<span>
          <Field component={Transfer} name="permissionIds"
            dataSource={allPermissions}
            showSearch
            filterOption={(input, item) => item.title.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            render={item => item.title}
            titles={['Доступные', 'Выбранные']}
            listStyle={{
              width: '45%',
              height: 500
            }}
          />
        </span>) : ''}
      />
    );
  }
}
