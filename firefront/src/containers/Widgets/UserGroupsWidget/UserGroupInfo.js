import React, { Component } from 'react';
import { Field } from 'redux-form';

import { FormItem } from 'components/Form';
import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import Widget from 'components/Widget';

export default class UserGroupInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
  }

  render() {
    const { selectedUserGroup, validateStatuses } = this.props;
    return (
      <Widget onHeightReady={height => this.setState({ height })}
        children={selectedUserGroup ? (<span>
          <FormItem
            label="Название"
            validateStatus={validateStatuses.name ? 'error' : ''}
          >
            <Field component={Input} name="name" />
          </FormItem>

          <FormItem
            label="Описание"
            validateStatus={validateStatuses.description ? 'error' : ''}
          >
            <Field component={Input} name="description" />
          </FormItem>

          <FormItem>
            <Field component={Checkbox} name="builtin" label="Встроенная група пользователей" />
          </FormItem>
        </span>) : ''}
      />
    );
  }
}
