import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { createSelector } from 'reselect';

import Table from 'components/Table';
import DeviceTypeTreeItem from 'components/Device/DeviceTypeTreeItem';
import Menu from 'components/Menu';
import ButtonGroup from 'components/Button/ButtonGroup';
import Title from 'components/Title';

import { updateCurrentDevice } from 'actions/devices';

import { getMonitorableDevices, getMonitorableValues } from 'helpers/device';
import { getDateFormat } from 'helpers/time';
import {
  getActiveProjectId,
  getActiveDeviceList,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

const DeviceTreeWrapper = styled.div`
  height: 100%;
  background-color: white;
  display: flex;
  flex-direction: column;
`;

class MonitorableDevicesWidget extends Component {
  constructor(props) {
    super(props);
    const { devicesList, deviceProfileViewsHash } = props;
    const columns = [
      {
        key: 'type',
        title: 'Тип',
        dataIndex: 'type',
        render: this.deviceTypeColumnRender
      },
      { key: 'shortAddressPath', title: 'Адрес', dataIndex: 'middleAddressPath' },
      { key: 'regionName', title: 'Зона', dataIndex: 'regionName' }
    ];

    const monitorableDevices = getMonitorableDevices(devicesList, deviceProfileViewsHash);
    const monitorableValues = getMonitorableValues(monitorableDevices, deviceProfileViewsHash);
    const monitorableDeviceViews = this.getMonitorableDeviceViews(
      monitorableDevices,
      monitorableValues
    );
    const monitorableValueColumns = this.getMonitorableValueColumns(monitorableValues);
    this.state = {
      currentDevice: {},
      monitorableDeviceViews,
      monitorableValueColumns,
      selectedRowKeys: null,
      columns
    };
  }

  componentWillReceiveProps = nextProps => {
    const { devicesList, devicesHash, deviceProfileViewsHash } = nextProps;

    let { currentDevice } = this.state;
    // Ищем устройство в списке
    currentDevice = currentDevice.id ? { ...devicesHash[currentDevice.id] } : {};

    const monitorableDevices = getMonitorableDevices(devicesList, deviceProfileViewsHash);
    const monitorableValues = getMonitorableValues(monitorableDevices, deviceProfileViewsHash);
    const monitorableDeviceViews = this.getMonitorableDeviceViews(
      monitorableDevices,
      monitorableValues
    );
    const monitorableValueColumns = this.getMonitorableValueColumns(monitorableValues);
    this.setState({
      currentDevice,
      monitorableDeviceViews,
      monitorableValueColumns
    });
  };

  getMonitorableDeviceViews = (devices, values) => {
    const monitorableDeviceViews = devices.map(device =>
      Object.assign(
        {
          ...this.getDeviceMonitorableValues(device.monitorableValueViews)
        },
        device
      )
    );
    return monitorableDeviceViews;
  };

  getMonitorableValueColumns = values => {
    const { monitorableValueProfiles } = this.props;
    return values.map(value => ({
      key: value,
      title: monitorableValueProfiles[value].name,
      dataIndex: value,
      render: this.monitorableValueCellRender
    }));
  };

  getDeviceMonitorableValues = valueViews => {
    const deviceValues = {};
    valueViews.forEach(
      valueView => (deviceValues[valueView.id] = `${valueView.value} ${valueView.unit}`)
    );
    return deviceValues;
  };

  deviceTypeColumnRender = (text, record) => (
    <DeviceTypeTreeItem record={record} stateIconEnabled={false} />
  );

  monitorableValueCellRender = (text, record) => <div>{text ? text : '-'}</div>;

  rowClassName = record => {
    const { currentDevice } = this.state;
    return currentDevice.id === record.id ? 'selected-row' : '';
  };

  onRowClick = device => {
    this.setState({ currentDevice: device });
  };

  onRowDoubleClick = record => {
    const { devicesHash, dispatch } = this.props;
    const currentDevice = devicesHash[record.id];
    dispatch(updateCurrentDevice(currentDevice || {}));
  };

  componentWillUnmount = () => {
    this.props.dispatch(updateCurrentDevice({}));
  };

  render = () => {
    const { monitorableDeviceViews, monitorableValueColumns, currentDevice, columns } = this.state;
    const rowSelection = {
      selectedRowKeys: currentDevice.id,
      type: 'radio'
    };

    const dates = monitorableDeviceViews.map(device => device.monitorableValuesTimestamp),
      maxDate = dates.length ? Math.max.apply(null, dates) : null,
      dataRelevance = maxDate && isFinite(maxDate) ? getDateFormat(maxDate) : '';
    return (
      <DeviceTreeWrapper>
        <Menu isActive={true}>
          <ButtonGroup>
            <Title style={{ padding: '0 10px' }}>{`Данные актуальны на: ${dataRelevance}`}</Title>
          </ButtonGroup>
        </Menu>
        {monitorableDeviceViews.length ? (
          <Table
            hideDefaultSelections={true}
            columns={[...columns, ...monitorableValueColumns]}
            dataSource={monitorableDeviceViews}
            rowKey={record => record.id || record.key}
            rowClassName={this.rowClassName}
            onRowClick={this.onRowClick}
            rowSelection={rowSelection}
            onRowDoubleClick={this.onRowDoubleClick}
          />
        ) : null}
      </DeviceTreeWrapper>
    );
  };
}

const getFilteredDeviceList = createSelector(
  getCurrentUser,
  getActiveDeviceList,
  (currentUser, devicesList) => filterEntities(currentUser, devicesList || [])
);

const mapStateToProps = state => {
  return {
    devicesList: getFilteredDeviceList(state),
    devicesHash: getActiveDevicesHash(state) || {},
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    projectId: getActiveProjectId(state),
    monitorableValueProfiles: state.monitorableValueProfiles
  };
};

export default connect(mapStateToProps)(MonitorableDevicesWidget);
