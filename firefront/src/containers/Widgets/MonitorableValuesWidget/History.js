import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';
import moment from 'moment';
import {
  getMonitorableValuesHistory,
  resetMonitorableValuesHistory
} from 'actions/monitorableValues';
import MonitorableValuesMenu from 'containers/Menus/MonitorableValuesMenu';
import LineChart from 'components/Chart/LineChart';
import PeriodChoiceForm from 'containers/Forms/PeriodChoiceForm';
import { modalOpen, modalClose } from 'actions/modals';

const ranges = {
  day: { title: 'Последние 24 часа' },
  hour: { title: 'Последний час' },
  all: { title: 'Весь период' },
  any: { title: 'Указанный период' }
};

class HistoryMonitorableValuesWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [], // Исходные данные, для пустой таблицы
      deviceId: '', // Id текущего устройства
      rerender: false, // Необходимость перерисовки графика
      queryParams: {}, // Параметы фильтрации запроса
      deviceHistoryCount: 0, // Количество записей в истории
      formParams: {}
    };
  }

  componentDidMount() {
    this.fetchData(this.props);
  }

  componentWillReceiveProps(props) {
    this.fetchData(props);
  }

  fetchData(props) {
    const {
      currentDevice,
      deviceHistory,
      projectId,
      getdeviceHistoryInProgress,
      getdeviceHistoryError
    } = props;
    if (
      projectId &&
      currentDevice.id &&
      !deviceHistory &&
      !getdeviceHistoryInProgress &&
      !getdeviceHistoryError
    ) {
      this.getHistory({ period: 'day' }, props);
    }
    this.dataFormatter(props);
  }

  getHistory = (formParams, props) => {
    const { projectId, currentDevice, dispatch } = props ? props : this.props;
    const queryParams = {};
    const now = new Date();
    this.setState({ formParams }, () => {
      switch (formParams.period) {
        case 'hour':
          queryParams['from'] = now.getTime() - 1000 * 60 * 60;
          break;
        case 'all':
          break;
        case 'any':
          queryParams['from'] = moment(
            `${formParams.dateFrom.format('DD.MM.YYYY')} ${formParams.timeFrom.format('HH:mm:ss')}`,
            'DD.MM.YYYY HH:mm:ss'
          );
          queryParams['to'] = moment(
            `${formParams.dateTo.format('DD.MM.YYYY')} ${formParams.timeTo.format('HH:mm:ss')}`,
            'DD.MM.YYYY HH:mm:ss'
          );
          break;
        default:
          queryParams['from'] = now.getTime() - 1000 * 60 * 60 * 24;
          break;
      }

      if (queryParams['from'])
        queryParams['from'] =
          moment(queryParams['from'])
            .toISOString()
            .split('Z')[0] + '+0000';

      if (queryParams['to'])
        queryParams['to'] =
          moment(queryParams['to'])
            .toISOString()
            .split('Z')[0] + '+0000';

      queryParams['size'] = 2000;
      dispatch(getMonitorableValuesHistory(projectId, currentDevice.id, queryParams));
      dispatch(modalClose('period'));
    });
  };

  dataFormatter(props) {
    const { deviceHistory, monitorableValueProfiles, currentDevice } = props;
    const { deviceId, queryParams, deviceHistoryCount } = this.state;
    const chartData = [];
    let rerender = false;

    if (
      deviceHistory &&
      (deviceId !== currentDevice.id ||
        !isEqual(
          queryParams,
          deviceHistory.queryParams || deviceHistory.content.length !== deviceHistoryCount
        ))
    ) {
      const values = {};
      rerender = true;
      deviceHistory.content.forEach(item => {
        if (values[item.profileId]) values[item.profileId] = [...values[item.profileId], item];
        else {
          values[item.profileId] = [item];
        }
      });
      // Собираем все параметы, для отрисовки на графике
      Object.keys(values).forEach(key =>
        chartData.push({ id: key, name: monitorableValueProfiles[key].name, data: values[key] })
      );

      this.setState({
        chartData,
        deviceHistoryCount: deviceHistory.content.length,
        rerender,
        queryParams: deviceHistory.queryParams,
        deviceId: currentDevice.id
      });
    }
    this.setState({ rerender });
  }

  openModal = () => {
    this.props.dispatch(modalOpen('period'));
  };

  componentWillUnmount = () => {
    this.props.dispatch(resetMonitorableValuesHistory());
  };

  render() {
    const { currentDevice } = this.props;
    const { chartData, rerender, formParams } = this.state;
    return (
      <div>
        <PeriodChoiceForm
          modalName={'period'}
          customValues={formParams}
          ranges={ranges}
          onSubmit={this.getHistory}
        />
        <MonitorableValuesMenu
          name={
            currentDevice.id ? `${currentDevice.name} (${currentDevice.middleAddressPath})` : ''
          }
          isActive={currentDevice.id ? true : false}
          onButtonClick={this.openModal}
          info={`Данные ${
            ranges[formParams.period]
              ? `за ${ranges[formParams.period].title.toLowerCase()}`
              : 'отсутствуют'
          }`}
        />
        <LineChart data={chartData} update={rerender} xKey="received" yKey="value" />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {
    projectId: state.activeProject.project ? state.activeProject.project.id : null,
    currentDevice: state.widgets.currentDevice,
    deviceHistory: state.activeProject.monitorableValuesHistory[state.widgets.currentDevice.id]
      ? state.activeProject.monitorableValuesHistory[state.widgets.currentDevice.id]
      : null,
    monitorableValueProfiles: state.monitorableValueProfiles,

    getdeviceHistoryInProgress: state.inProgress.getMonitorableValuesHistory,
    getdeviceHistoryError: state.errors.getMonitorableValuesHistory
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryMonitorableValuesWidget);
