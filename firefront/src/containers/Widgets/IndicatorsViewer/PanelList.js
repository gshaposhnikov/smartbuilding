import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { VirtualizedWrapper as Wrapper } from 'components/Layout';
import IndicatorPanelList from 'components/IndicatorPanelList';

import { selectIndicatorPanel, resetSelectedIndicator } from 'actions/indicators';

import { getActiveIndicatorPanelsInSelectedGroup, getActiveProjectId } from 'helpers/activeProject';
import { savePanelIdToStorage } from 'helpers/indicators';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

class IndicatorListWidget extends Component {
  onRowClick = ({ rowData: { id: panelId } }) => {
    const { dispatch, projectId, selectedIndicatorGroupId } = this.props;
    savePanelIdToStorage(projectId, selectedIndicatorGroupId, panelId);
    dispatch(selectIndicatorPanel(panelId));
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(resetSelectedIndicator());
  };

  render = () => {
    const { indicatorPanels, selectedIndicatorPanelId } = this.props;
    return (
      <Wrapper>
        <IndicatorPanelList
          indicatorPanels={indicatorPanels}
          rowSelection={selectedIndicatorPanelId}
          onRowClick={this.onRowClick}
        />
      </Wrapper>
    );
  };
}

const getFilteredIndicatorPanels = createSelector(
  getCurrentUser,
  getActiveIndicatorPanelsInSelectedGroup,
  filterEntities
);

const mapStateToProps = state => {
  return {
    projectId: getActiveProjectId(state),
    selectedIndicatorPanelId: state.widgets.selectedIndicatorPanelId,
    selectedIndicatorGroupId: state.widgets.selectedIndicatorGroupId,
    indicatorPanels: getFilteredIndicatorPanels(state)
  };
};

export default connect(mapStateToProps)(IndicatorListWidget);
