import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { ContextMenuTrigger } from 'react-contextmenu';
import { isEmpty } from 'lodash';

import IndicatorPanel from 'components/IndicatorPanel';

import IndicatorPanelViewerWidgetMenu from 'containers/Menus/IndicatorPanelViewerWidgetMenu';
import {
  DeviceContextMenu,
  RegionContextMenu,
  ScenarioContextMenu
} from 'containers/ContextMenus/IndicatorContextMenus';
import LoginConfirmationForm from 'containers/Forms/LoginConfirmationForm';

import { modalOpen, modalClose } from 'actions/modals';
import { loadScenarioConstants } from 'actions/scenarios';
import {
  changeIndicatorDevicesPollingState,
  changeIndicatorRegionsPollingState,
  changeIndicatorRegionsGuard,
  performIndicatorScenarioAction
} from 'actions/indicators';
import { updateLoginConfirmation } from 'actions/login';

import {
  getActiveRegionsHash,
  getActiveScenariosHash,
  getActiveDevicesHash,
  getActiveProjectId
} from 'helpers/activeProject';
import { getDeviceIdsGroupedByRegionId } from 'helpers/device';
import { availableByFilterTags } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

const Wrapper = styled.div`
  flex: 1;
  background-color: white;
  .indicator-cell {
    border-right: 1px solid lightgray;
    border-bottom: 1px solid lightgray;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
const MIN_CELL_SIZE = 40,
  MAX_CELL_SIZE = 500,
  PANELS_SIZE = 'panelsSize',
  PANELS_AUTOSIZE = 'panelsAutosize',
  CONTEXT_MENU_NAMES = {
    DEVICE: 'devicesContextMenu',
    REGION: 'regionsContextMenu',
    SCENARIO: 'scenariosContextMenu'
  },
  MODAL_NAMES = {
    CONFIRM: 'loginConfirmationForm'
  },
  MAX_CONFIRMATION_TIME_DIFF = 60 * 10 * 1000;

class IndicatorPanelWidget extends Component {
  state = {
    [PANELS_SIZE]: localStorage.getItem(PANELS_SIZE)
      ? JSON.parse(localStorage.getItem(PANELS_SIZE))
      : {},
    [PANELS_AUTOSIZE]: localStorage.getItem(PANELS_AUTOSIZE)
      ? JSON.parse(localStorage.getItem(PANELS_AUTOSIZE))
      : {}
  };

  componentDidMount = () => {
    const { scenarioConstants, dispatch } = this.props;
    if (isEmpty(scenarioConstants)) {
      dispatch(loadScenarioConstants());
    }
  };

  renderIndicatorCell = ({ columnIndex, rowIndex, key, style }) => {
    const {
      selectedIndicatorPanel: { indicators },
      regions,
      devices,
      scenarios,
      regionalDeviceIds,
      license,
      currentUser
    } = this.props;
    const indicator = indicators[rowIndex][columnIndex];
    const styles = indicator.styles ? { ...style, ...indicator.styles } : style;
    const { entityIds, name, entityType } = indicator;
    if (availableByFilterTags(currentUser, indicator))
      switch (entityType) {
        case 'REGION': {
          let altName = '',
            backgroundColor = '',
            color = '',
            maxPriority,
            hasGuardedRegions,
            hasSecurityRegions,
            hasDisabledDevices,
            availableEntityIds = [];
          if (entityIds && entityIds.length) {
            availableEntityIds = entityIds.filter(entityId => {
              const region = regions[entityId];
              if (region) {
                return availableByFilterTags(currentUser, region);
              }
              return false;
            });
            altName = `${availableEntityIds.map(entityId => {
              const region = regions[entityId];
              if (region) {
                if (
                  isNaN(maxPriority) ||
                  maxPriority > region.generalStateCategoryView.stateCategory.priority
                ) {
                  maxPriority = region.generalStateCategoryView.stateCategory.priority;
                  backgroundColor = region.generalStateCategoryView.color;
                  color = region.generalStateCategoryView.fontColor;
                }
                if (region.subsystem === 'SECURITY') {
                  hasSecurityRegions = true;
                  if (region.onGuard) hasGuardedRegions = true;
                }
                if (regionalDeviceIds[entityId] && regionalDeviceIds[entityId].length) {
                  regionalDeviceIds[entityId].forEach(deviceId => {
                    if (!devices[deviceId].statePolling) {
                      hasDisabledDevices = true;
                    }
                  });
                }
                return region.name;
              }
              return null;
            })}`;
          }
          return (
            <ContextMenuTrigger
              holdToDisplay={-1}
              key={key}
              entityIds={availableEntityIds}
              record={indicator}
              hasDisabledDevices={hasDisabledDevices}
              hasGuardedRegions={hasGuardedRegions}
              hasSecurityRegions={hasSecurityRegions}
              collect={props => props}
              id={CONTEXT_MENU_NAMES[entityType]}
            >
              <div
                key={key}
                className="indicator-cell"
                style={{ ...styles, backgroundColor, color }}
                title={indicator.description || name || altName}
              >
                {name || altName}
              </div>
            </ContextMenuTrigger>
          );
        }
        case 'SCENARIO': {
          let altName = '',
            backgroundColor = '',
            color = '',
            maxPriority;
          if (entityIds && entityIds.length) {
            altName = `${entityIds
              .filter(entityId => {
                const scenario = scenarios[entityId];
                if (scenario) {
                  return availableByFilterTags(currentUser, scenario);
                }
                return false;
              })
              .map(entityId => {
                const scenario = scenarios[entityId];
                if (scenario) {
                  if (
                    isNaN(maxPriority) ||
                    maxPriority > scenario.generalStateCategoryView.stateCategory.priority
                  ) {
                    maxPriority = scenario.generalStateCategoryView.stateCategory.priority;
                    backgroundColor = scenario.generalStateCategoryView.color;
                    color = scenario.generalStateCategoryView.fontColor;
                  }
                  return scenario.name;
                }
                return null;
              })}`;
          }
          return license && license.control && license.control.engineeringEnabled ? (
            <ContextMenuTrigger
              holdToDisplay={-1}
              key={key}
              record={indicator}
              collect={props => props}
              id={CONTEXT_MENU_NAMES[entityType]}
            >
              <div
                key={key}
                className="indicator-cell"
                style={{ ...styles, backgroundColor, color }}
                title={indicator.description || name || altName}
              >
                {name || altName}
              </div>
            </ContextMenuTrigger>
          ) : (
            <div
              key={key}
              className="indicator-cell"
              style={{ ...styles, backgroundColor, color }}
              title={indicator.description || name || altName}
            >
              {name || altName}
            </div>
          );
        }
        case 'DEVICE': {
          let altName = '',
            backgroundColor = '',
            color = '',
            maxPriority,
            hasDisabledDevices,
            availableEntityIds = [];
          if (entityIds && entityIds.length) {
            availableEntityIds = entityIds.filter(entityId => {
              const device = devices[entityId];
              if (device) {
                return availableByFilterTags(currentUser, device);
              }
              return false;
            });
            altName = `${availableEntityIds.map(entityId => {
              const device = devices[entityId];
              if (device) {
                if (
                  isNaN(maxPriority) ||
                  maxPriority > device.generalStateCategoryView.stateCategory.priority
                ) {
                  maxPriority = device.generalStateCategoryView.stateCategory.priority;
                  backgroundColor = device.generalStateCategoryView.color;
                  color = device.generalStateCategoryView.fontColor;
                }
                if (!device.statePolling) {
                  hasDisabledDevices = true;
                }
                return device.name;
              }
              return null;
            })}`;
          }
          return (
            <ContextMenuTrigger
              holdToDisplay={-1}
              key={key}
              entityIds={availableEntityIds}
              record={indicator}
              hasDisabledDevices={hasDisabledDevices}
              collect={props => props}
              id={CONTEXT_MENU_NAMES[entityType]}
            >
              <div
                key={key}
                className="indicator-cell"
                style={{ ...styles, backgroundColor, color }}
                title={indicator.description || name || altName}
              >
                {name || altName}
              </div>
            </ContextMenuTrigger>
          );
        }
        default: {
          return (
            <div
              key={key}
              className="indicator-cell"
              style={styles}
              title={indicator.description || name}
            >
              {name}
            </div>
          );
        }
      }
    else {
      return <div key={key} className="indicator-cell" style={styles} />;
    }
  };

  onChangeAutosize = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsAutosize } = this.state;
    if (
      !panelsAutosize[selectedIndicatorPanel.id] &&
      typeof panelsAutosize[selectedIndicatorPanel.id] !== 'undefined'
    ) {
      panelsAutosize[selectedIndicatorPanel.id] = true;
    } else {
      panelsAutosize[selectedIndicatorPanel.id] = false;
    }
    localStorage.setItem(PANELS_AUTOSIZE, JSON.stringify(panelsAutosize));
    this.setState({ panelsAutosize });
  };

  onChangeCellsWidth = value => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!panelsSize[selectedIndicatorPanel.id]) panelsSize[selectedIndicatorPanel.id] = {};
    const newValue = value
      ? value < MIN_CELL_SIZE
        ? MIN_CELL_SIZE
        : value > MAX_CELL_SIZE
        ? MAX_CELL_SIZE
        : value
      : MIN_CELL_SIZE;
    panelsSize[selectedIndicatorPanel.id].width = newValue;
    localStorage.setItem(PANELS_SIZE, JSON.stringify(panelsSize));
    this.setState({ panelsSize });
  };

  onChangeCellsHeight = value => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!panelsSize[selectedIndicatorPanel.id]) panelsSize[selectedIndicatorPanel.id] = {};
    const newValue = value
      ? value < MIN_CELL_SIZE
        ? MIN_CELL_SIZE
        : value > MAX_CELL_SIZE
        ? MAX_CELL_SIZE
        : value
      : MIN_CELL_SIZE;
    panelsSize[selectedIndicatorPanel.id].height = newValue;
    localStorage.setItem(PANELS_SIZE, JSON.stringify(panelsSize));
    this.setState({ panelsSize });
  };

  getCellHeight = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_SIZE;
    if (!panelsSize[selectedIndicatorPanel.id]) return MIN_CELL_SIZE;
    if (!panelsSize[selectedIndicatorPanel.id].height) return MIN_CELL_SIZE;
    return panelsSize[selectedIndicatorPanel.id].height;
  };

  getCellWidth = () => {
    const { selectedIndicatorPanel } = this.props;
    const { panelsSize } = this.state;
    if (!selectedIndicatorPanel) return MIN_CELL_SIZE;
    if (!panelsSize[selectedIndicatorPanel.id]) return MIN_CELL_SIZE;
    if (!panelsSize[selectedIndicatorPanel.id].width) return MIN_CELL_SIZE;
    return panelsSize[selectedIndicatorPanel.id].width;
  };

  takeIndicatorAction = () => {
    const { dispatch, selectedIndicatorPanel, projectId } = this.props;
    const { indicatorAction, indicator } = this.state;
    if (indicatorAction && indicator) {
      switch (indicator.entityType) {
        case 'SCENARIO': {
          const { actionId } = this.state;
          dispatch(indicatorAction(projectId, selectedIndicatorPanel.id, indicator, actionId));
          break;
        }
        case 'DEVICE': {
          const { state } = this.state;
          dispatch(indicatorAction(projectId, selectedIndicatorPanel.id, indicator, state));
          break;
        }
        case 'REGION': {
          const { state } = this.state;
          dispatch(indicatorAction(projectId, selectedIndicatorPanel.id, indicator, state));
          break;
        }
        default:
          break;
      }
    }
    dispatch(modalClose(MODAL_NAMES.CONFIRM));
  };

  cancelIndicatorAction = () => {
    const { dispatch } = this.props;
    dispatch(modalClose(MODAL_NAMES.CONFIRM));
  };

  changeIndicatorDevicesPollingState = (indicator, state, entityIds = []) => {
    const { dispatch, lastLoginConfirmation, projectId, selectedIndicatorPanel } = this.props;
    if (
      !indicator.passwordNeeded ||
      (lastLoginConfirmation && new Date() - lastLoginConfirmation <= MAX_CONFIRMATION_TIME_DIFF)
    ) {
      dispatch(
        changeIndicatorDevicesPollingState(
          projectId,
          selectedIndicatorPanel.id,
          { ...indicator, entityIds },
          state
        )
      );
      if (indicator.passwordNeeded) dispatch(updateLoginConfirmation());
    } else {
      this.setState({
        indicatorAction: changeIndicatorDevicesPollingState,
        indicator: { ...indicator, entityIds },
        state
      });
      dispatch(modalOpen(MODAL_NAMES.CONFIRM));
    }
  };

  changeIndicatorRegionsGuard = (indicator, state, entityIds = []) => {
    const { dispatch, lastLoginConfirmation, projectId, selectedIndicatorPanel } = this.props;
    if (
      !indicator.passwordNeeded ||
      (lastLoginConfirmation && new Date() - lastLoginConfirmation <= MAX_CONFIRMATION_TIME_DIFF)
    ) {
      dispatch(
        changeIndicatorRegionsGuard(
          projectId,
          selectedIndicatorPanel.id,
          { ...indicator, entityIds },
          state
        )
      );
      if (indicator.passwordNeeded) dispatch(updateLoginConfirmation());
    } else {
      this.setState({
        indicatorAction: changeIndicatorRegionsGuard,
        indicator: { ...indicator, entityIds },
        state
      });
      dispatch(modalOpen(MODAL_NAMES.CONFIRM));
    }
  };

  changeIndicatorRegionsPollingState = (indicator, state, entityIds = []) => {
    const { dispatch, lastLoginConfirmation, projectId, selectedIndicatorPanel } = this.props;
    if (
      !indicator.passwordNeeded ||
      (lastLoginConfirmation && new Date() - lastLoginConfirmation <= MAX_CONFIRMATION_TIME_DIFF)
    ) {
      dispatch(
        changeIndicatorRegionsPollingState(
          projectId,
          selectedIndicatorPanel.id,
          { ...indicator, entityIds },
          state
        )
      );
      if (indicator.passwordNeeded) dispatch(updateLoginConfirmation());
    } else {
      this.setState({
        indicatorAction: changeIndicatorRegionsPollingState,
        indicator: { ...indicator, entityIds },
        state
      });
      dispatch(modalOpen(MODAL_NAMES.CONFIRM));
    }
  };

  performIndicatorScenarioAction = (indicator, actionId) => {
    const { dispatch, lastLoginConfirmation, projectId, selectedIndicatorPanel } = this.props;
    if (
      !indicator.passwordNeeded ||
      (lastLoginConfirmation && new Date() - lastLoginConfirmation <= MAX_CONFIRMATION_TIME_DIFF)
    ) {
      dispatch(
        performIndicatorScenarioAction(projectId, selectedIndicatorPanel.id, indicator, actionId)
      );
      if (indicator.passwordNeeded) dispatch(updateLoginConfirmation());
    } else {
      this.setState({ indicatorAction: performIndicatorScenarioAction, indicator, actionId });
      dispatch(modalOpen(MODAL_NAMES.CONFIRM));
    }
  };

  render = () => {
    const {
      selectedIndicatorPanel,
      regions,
      scenarios,
      devices,
      scenarioDictionaries
    } = this.props;
    const { panelsAutosize } = this.state;
    return (
      <Wrapper>
        <LoginConfirmationForm
          onSuccess={this.takeIndicatorAction}
          onCancel={this.cancelIndicatorAction}
        />
        <DeviceContextMenu
          onChangeIndicatorDevicesPollingState={this.changeIndicatorDevicesPollingState}
        />
        <RegionContextMenu
          onChangeIndicatorRegionsGuard={this.changeIndicatorRegionsGuard}
          onChangeIndicatorRegionsPollingState={this.changeIndicatorRegionsPollingState}
        />
        <ScenarioContextMenu
          actions={scenarioDictionaries.manageActions}
          onPerformIndicatorScenarioAction={this.performIndicatorScenarioAction}
        />
        <IndicatorPanelViewerWidgetMenu
          onChangeWidth={this.onChangeCellsWidth}
          onChangeHeight={this.onChangeCellsHeight}
          onChangeAutosize={this.onChangeAutosize}
          autosize={selectedIndicatorPanel ? panelsAutosize[selectedIndicatorPanel.id] : false}
          height={this.getCellHeight()}
          width={this.getCellWidth()}
        />
        <IndicatorPanel
          regions={regions}
          scenarios={scenarios}
          devices={devices}
          selectedIndicatorPanel={selectedIndicatorPanel}
          panelsAutosize={panelsAutosize}
          renderIndicatorCell={this.renderIndicatorCell}
          getCellHeight={this.getCellHeight}
          getCellWidth={this.getCellWidth}
        />
      </Wrapper>
    );
  };
}

const mapStateToProps = state => {
  const selectedIndicatorPanelId = state.widgets.selectedIndicatorPanelId;
  const devices = getActiveDevicesHash(state);
  return {
    projectId: getActiveProjectId(state),
    regions: getActiveRegionsHash(state),
    scenarios: getActiveScenariosHash(state),
    devices,
    license: state.license,
    scenarioDictionaries: state.scenarioConstants.dictionaries || {},
    regionalDeviceIds: getDeviceIdsGroupedByRegionId(Object.values(devices || {})),
    lastLoginConfirmation: state.lastLoginConfirmation,
    selectedIndicatorPanel:
      selectedIndicatorPanelId && state.activeProject.indicators.panels
        ? state.activeProject.indicators.panels[selectedIndicatorPanelId]
        : null,
    currentUser: getCurrentUser(state)
  };
};

export default connect(mapStateToProps)(IndicatorPanelWidget);
