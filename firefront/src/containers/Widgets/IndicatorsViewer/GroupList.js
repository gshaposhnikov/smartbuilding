import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { VirtualizedWrapper as Wrapper } from 'components/Layout';
import IndicatorGroupList from 'components/IndicatorGroupList';

import {
  selectIndicatorGroup,
  resetSelectedIndicator,
  selectIndicatorGroupAndPanel
} from 'actions/indicators';

import {
  getActiveIndicatorGroupsList,
  getActiveProjectId,
  getActiveIndicatorGroupsHash,
  getActiveIndicatorPanelsHash
} from 'helpers/activeProject';
import { saveGroupIdToStorage } from 'helpers/indicators';
import { filterEntities } from 'helpers/filtration';
import { getCurrentUser } from 'helpers/user';

class GroupListWidget extends Component {
  componentDidMount = () => {
    const { indicatorGroups, selectedIndicatorGroupId } = this.props;
    if (indicatorGroups.length && !selectedIndicatorGroupId)
      this.initSelectedGroupAndPanel(this.props);
  };

  componentWillReceiveProps = nextProps => {
    const { indicatorGroups, selectedIndicatorGroupId } = this.props;
    if (
      !indicatorGroups.length &&
      nextProps.indicatorGroups.length &&
      !selectedIndicatorGroupId &&
      !nextProps.selectedIndicatorGroupId
    )
      this.initSelectedGroupAndPanel(nextProps);
  };

  initSelectedGroupAndPanel = props => {
    const { projectId, dispatch, indicatorGroupsHash, indicatorPanelsHash } = props;
    let storage = localStorage.getItem('indicators');
    if (projectId && storage) {
      storage = JSON.parse(storage);
      if (storage[projectId]) {
        const groupId = storage[projectId].groupId;
        const panelId = storage[projectId].panelId;
        if (groupId && indicatorGroupsHash[groupId]) {
          if (panelId && indicatorPanelsHash[panelId]) {
            dispatch(selectIndicatorGroupAndPanel(groupId, panelId));
          } else {
            dispatch(selectIndicatorGroup(groupId));
          }
        }
      }
    }
  };

  onRowClick = ({ rowData: { id: groupId } }) => {
    const { dispatch, projectId } = this.props;
    saveGroupIdToStorage(projectId, groupId);
    dispatch(selectIndicatorGroup(groupId));
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    dispatch(resetSelectedIndicator());
  };

  render = () => {
    const { indicatorGroups, selectedIndicatorGroupId } = this.props;
    return (
      <Wrapper>
        <IndicatorGroupList
          indicatorGroups={indicatorGroups}
          rowSelection={selectedIndicatorGroupId}
          onRowClick={this.onRowClick}
        />
      </Wrapper>
    );
  };
}

const getFilteredIndicatorGroups = createSelector(
  getCurrentUser,
  getActiveIndicatorGroupsList,
  filterEntities
);

const mapStateToProps = state => {
  return {
    projectId: getActiveProjectId(state),
    selectedIndicatorGroupId: state.widgets.selectedIndicatorGroupId,
    indicatorGroups: getFilteredIndicatorGroups(state),
    indicatorGroupsHash: getActiveIndicatorGroupsHash(state),
    indicatorPanelsHash: getActiveIndicatorPanelsHash(state)
  };
};

export default connect(mapStateToProps)(GroupListWidget);
