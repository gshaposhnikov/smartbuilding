import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { getFormSyncErrors, getFormValues } from 'redux-form';

import { FormItem } from 'components/Form';
import Widget from 'components/Widget';
import Transfer from 'components/Form/Transfer';

import { getUserGroupPermissions } from 'helpers/user';

import { FORM_NAME } from './UserInfoEditor';

class UserAccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500,
      allPermissions: []
    };
  }

  componentDidMount = () => {
    const { selectedUser, userGroups, permissions } = this.props;
    this.mapPermissionsToState(selectedUser, userGroups, permissions);
  };

  componentWillReceiveProps = nextProps => {
    const { selectedUser, userGroups, permissions } = nextProps;
    if (
      selectedUser !== this.props.selectedUser ||
      userGroups !== this.props.userGroups ||
      permissions !== this.props.permissions
    )
      this.mapPermissionsToState(selectedUser, userGroups, permissions);
  };

  mapPermissionsToState = (selectedUser, userGroups, permissions) => {
    let allPermissions = [];
    let groupPermissionIds = [];
    if (selectedUser && userGroups && permissions) {
      groupPermissionIds = getUserGroupPermissions(selectedUser, userGroups);
      allPermissions = permissions.permissions.map(perm => ({
        key: perm.id,
        title: perm.name,
        description: perm.description,
        inGroups: groupPermissionIds.includes(perm.id)
      }));
    }
    this.setState({ allPermissions });
  };

  renderItem = item => {
    return {
      label: (
        <span>
          {item.inGroups ? (
            <i>
              <u>{item.title}</u>
            </i>
          ) : (
            item.title
          )}
        </span>
      ),
      value: `${item.description}${item.inGroups ? '\nУстановлено в группе' : ''}`
    };
  };

  render() {
    const { selectedUser } = this.props;
    const { allPermissions } = this.state;

    return (
      <Widget
        onHeightReady={height => this.setState({ height })}
        children={
          selectedUser ? (
            <FormItem
              label={
                <span>
                  Персональные права (права групп{' '}
                  <i>
                    <u>выделены</u>
                  </i>
                  )
                </span>
              }
            >
              <Field
                component={Transfer}
                name="permissionIds"
                dataSource={allPermissions}
                showSearch
                filterOption={(input, item) =>
                  item.title.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                render={this.renderItem}
                titles={['Доступные', 'Выбранные']}
                listStyle={{
                  width: '45%',
                  height: 500
                }}
              />
            </FormItem>
          ) : (
            ''
          )
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    userGroups: state.admin.userGroups,
    permissions: state.admin.permissions,
    selectedUser: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {}
  };
};

export default connect(
  mapStateToProps,
  null
)(UserAccess);
