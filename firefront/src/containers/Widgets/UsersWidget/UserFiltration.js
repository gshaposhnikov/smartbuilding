import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form';
import { getFormSyncErrors, getFormValues } from 'redux-form';

import SUBSYSTEMS from 'constants/subsystems';
import { FORM_NAME } from './UserInfoEditor';

import { FormItem } from 'components/Form';
import Checkbox from 'components/Form/Checkbox';
import Widget from 'components/Widget';

import Select from 'containers/Selects';

const SUBSYSTEM_TAGS = Object.values(SUBSYSTEMS).filter(subsystem => subsystem.isFilterTag);

class UserFiltration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
  }

  componentWillReceiveProps(newProps) {}

  render() {
    const { selectedUser } = this.props;

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        {selectedUser ? (
          <div>
            <FormItem>
              <Field component={Checkbox} name="filterTagsOn" label="Фильтрация включена" />
            </FormItem>
            <FormItem>
              <Field
                component={Select}
                name="filterTags.tags"
                itemsSource={SUBSYSTEM_TAGS}
                disabledCondition={item => !!item.default}
                label="Подсистемы"
                mode="multiple"
              />
            </FormItem>
          </div>
        ) : null}
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUser: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {}
  };
};

export default connect(
  mapStateToProps,
  null
)(UserFiltration);
