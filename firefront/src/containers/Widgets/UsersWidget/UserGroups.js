import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { getFormValues } from 'redux-form';

import Widget from 'components/Widget';
import Transfer from 'components/Form/Transfer';

import { FORM_NAME } from './UserInfoEditor';

class UserGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500,
      userGroups: []
    };
  }

  componentDidMount = () => {
    const { userGroupsHash } = this.props;
    this.mapUserGroupsToState(userGroupsHash);
  };

  componentWillReceiveProps = nextProps => {
    const { userGroupsHash, selectedUser } = nextProps;
    if (userGroupsHash !== this.props.userGroupsHash || selectedUser !== this.props.selectedUser)
      this.mapUserGroupsToState(userGroupsHash);
  };

  mapUserGroupsToState = userGroupsHash => {
    const userGroups = Object.keys(userGroupsHash).map(id => ({
      key: id,
      title: userGroupsHash[id].name,
      description: userGroupsHash[id].description
    }));
    this.setState({ userGroups });
  };

  renderItem = item => {
    return {
      label: item.title,
      value: item.description
    };
  };

  render() {
    const { selectedUser } = this.props;
    const { height, userGroups } = this.state;
    return (
      <Widget
        onHeightReady={height => this.setState({ height })}
        children={
          selectedUser ? (
            <Field
              component={Transfer}
              name="userGroupIds"
              dataSource={userGroups}
              titles={['Доступные', 'Выбранные']}
              render={this.renderItem}
              listStyle={{
                width: '45%',
                height: height
              }}
            />
          ) : (
            ''
          )
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    userGroupsHash: state.admin.userGroups || {},
    selectedUser: getFormValues(FORM_NAME)(state)
  };
};

export default connect(
  mapStateToProps,
  null
)(UserGroups);
