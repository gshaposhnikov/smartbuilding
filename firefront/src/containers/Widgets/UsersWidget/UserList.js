import React, { Component } from 'react';
import { connect } from 'react-redux';

import UsersEditorMenu from 'containers/Menus/UsersEditorMenu';

import Widget from 'components/Widget';
import Table from 'components/Table';
import Text from 'components/Text';

import { selectUser, deleteUser, loadUsers } from 'actions/users';

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUser: null,
      height: 200
    };
    this.columns = [
      { key: 'name', dataIndex: 'name', title: 'Имя' },
      { key: 'fullName', dataIndex: 'fullName', title: 'Полное имя' },
      { key: 'indicatorId', title: 'Индикатор', render: () => <span>По умолчанию</span> },
      { key: 'workspaceId', title: 'Интерфейс', render: () => <span>По умолчанию</span> }
    ];
  }

  propsToState(props) {
    const { selectedUserId, users } = props;
    const newState = this.state;
    if (users) {
      newState.selectedUser = users[selectedUserId];
      this.setState(newState);
    }
  }

  componentDidMount() {
    const { users, loadUsersError, dispatch } = this.props;
    if (!users && !loadUsersError) {
      dispatch(loadUsers());
    }
    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onUserAdd() {
    const { dispatch } = this.props;
    dispatch(selectUser(null));
  }

  onUserDelete() {
    const { dispatch } = this.props;
    const { selectedUser } = this.state;
    dispatch(deleteUser(selectedUser.id, false));
  }

  onUserRestore() {
    const { dispatch } = this.props;
    const { selectedUser } = this.state;
    dispatch(deleteUser(selectedUser.id, true));
  }

  onUserSelect(user) {
    const { dispatch } = this.props;
    dispatch(selectUser(user.id));
  }

  rowClassName(record) {
    const { selectedUser } = this.state;
    return (
      (selectedUser && selectedUser.id === record.id ? 'selected-row' : '') +
      (record.active === false ? ' disabled-row' : '')
    );
  }

  render() {
    const { users, currentUser } = this.props;
    const { selectedUser } = this.state;

    const rowSelection = {
      selectedRowKeys: selectedUser ? selectedUser.id : null,
      type: 'radio'
    };

    const usersToView = users ? Object.keys(users).map(userId => users[userId]) : [];
    const notCurrentUser = selectedUser && selectedUser.id !== currentUser.id;

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <UsersEditorMenu
          isAddActive={true}
          onAdd={this.onUserAdd.bind(this)}
          isDeleteActive={notCurrentUser && selectedUser.active === true ? true : false}
          onDelete={this.onUserDelete.bind(this)}
          isRestoreActive={notCurrentUser && selectedUser.active === false ? true : false}
          onRestore={this.onUserRestore.bind(this)}
        />
        {users && Object.keys(users).length > 0 ? (
          <Table
            columns={this.columns}
            dataSource={usersToView}
            rowKey={record => record.id}
            onRowClick={this.onUserSelect.bind(this)}
            rowClassName={this.rowClassName.bind(this)}
            rowSelection={rowSelection}
          />
        ) : (
          <Text>Нет данных</Text>
        )}
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.admin.users,
    currentUser: state.currentUser,
    selectedUserId: state.widgets.selectedUserId,
    loadUsersError: state.errors.loadUsersError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
