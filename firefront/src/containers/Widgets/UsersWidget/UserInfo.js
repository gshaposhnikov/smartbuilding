import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import { getFormSyncErrors, getFormValues } from 'redux-form';

import { FormItem } from 'components/Form';
import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import Widget from 'components/Widget';

import { FORM_NAME } from './UserInfoEditor';

class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 200
    };
  }

  render() {
    const { selectedUser, validateStatuses } = this.props;
    return (
      <Widget
        onHeightReady={height => this.setState({ height })}
        children={
          selectedUser
            ? ((
                <div>
                  <FormItem label="Имя" validateStatus={validateStatuses.name ? 'error' : ''}>
                    <Field component={Input} name="name" />
                  </FormItem>

                  <FormItem
                    label="Полное имя"
                    validateStatus={validateStatuses.fullName ? 'error' : ''}
                  >
                    <Field component={Input} name="fullName" />
                  </FormItem>

                  <FormItem label="Email" validateStatus={validateStatuses.email ? 'error' : ''}>
                    <Field component={Input} name="email" type="text" />
                  </FormItem>

                  <FormItem
                    label="Номер телефона"
                    validateStatus={validateStatuses.phoneNo ? 'error' : ''}
                  >
                    <Field component={Input} name="phoneNo" />
                  </FormItem>

                  <FormItem
                    label={selectedUser.id ? 'Изменить пароль' : 'Пароль'}
                    validateStatus={validateStatuses.password ? 'error' : ''}
                  >
                    <Field component={Input} name="password" type="password" />
                  </FormItem>

                  <FormItem
                    label="Пароль (подтверждение)"
                    validateStatus={validateStatuses.password2 ? 'error' : ''}
                  >
                    <Field component={Input} name="password2" type="password" />
                  </FormItem>

                  <FormItem>
                    <Field component={Checkbox} name="builtin" label="Встроенный пользователь" />
                  </FormItem>
                </div>
              ): '')
            : ''
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUser: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {}
  };
};

export default connect(
  mapStateToProps,
  null
)(UserInfo);
