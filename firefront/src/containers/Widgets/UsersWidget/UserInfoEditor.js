import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;
import { isEqual } from 'lodash';
import { reduxForm, initialize, reset, getFormSyncErrors, getFormValues } from 'redux-form';

import Form from 'components/Form';
import Widget from 'components/Widget';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';

import UserInfo from 'containers/Widgets/UsersWidget/UserInfo';
import UserGroups from 'containers/Widgets/UsersWidget/UserGroups';
import UserAccess from 'containers/Widgets/UsersWidget/UserAccess';
// TODO: Реализовать справочник интерфейсов
// import UserInterfaces from 'containers/Widgets/UsersWidget/UserInterfaces';
import UserFiltration from 'containers/Widgets/UsersWidget/UserFiltration';

import { createUser, updateUser } from 'actions/users';
import { loadUserGroups } from 'actions/userGroups';
import { getFirstError } from 'helpers/form';

export const FORM_NAME = 'userEditor';
const TAB_PANE_STYLES = { padding: '8px 14px' };

class UserInfoEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUser: null,
      selectedTab: 'UserInfo',
      height: 200
    };
    this.components = [
      {
        name: 'UserInfo',
        title: 'Общие',
        component: <UserInfo />
      },
      {
        name: 'UserGroups',
        title: 'Группы',
        component: <UserGroups />
      },
      {
        name: 'UserAccess',
        title: 'Права',
        component: <UserAccess />
      },
      // TODO: Реализовать справочник интерфейсов
      // {
      //   name: 'UserInterfaces',
      //   title: 'Внешний вид',
      //   component: <UserInterfaces />
      // },
      {
        name: 'UserFiltration',
        title: 'Фильтрация',
        component: <UserFiltration />
      }
    ];
  }

  fillAndGetUser = (props, selectedUser) => {
    const { users, selectedUserId, dispatch } = props;

    let newUser = selectedUserId ? users[selectedUserId] : null;
    if (!isEqual(selectedUser, newUser)) {
      if (selectedUserId) {
        if (newUser) {
          // редактирование
          if (!newUser.workspaceId) newUser.workspaceId = 'default';
          if (!newUser.indicatorId) newUser.indicatorId = 'default';
          dispatch(initialize(FORM_NAME, newUser));
        } else {
          // создание нового пользователя
          newUser = {
            name: '',
            workspaceId: 'default',
            indicatorId: 'default',
            permissionIds: [],
            groupPermissionIds: [],
            userGroupIds: [],
            filterTagsOn: true
          };
          if (!isEqual(selectedUser, newUser)) {
            dispatch(reset(FORM_NAME));
            dispatch(initialize(FORM_NAME, newUser));
          }
        }
      }
    }
    return newUser;
  };

  propsToState = props => {
    let { selectedUser } = this.state;
    const { users } = props;
    if (users) {
      selectedUser = this.fillAndGetUser(props, this.state.selectedUser);
    }
    this.setState({ selectedUser });
  };

  componentDidMount = () => {
    const { userGroups, loadUserGroupsError, dispatch } = this.props;
    if (!userGroups && !loadUserGroupsError) {
      dispatch(loadUserGroups());
    }
    this.propsToState(this.props);
  };

  componentWillReceiveProps = nextProps => {
    this.propsToState(nextProps);
  };

  onTabClick = tabId => {
    this.setState({ selectedTab: tabId });
  };

  onUserSave = values => {
    const { dispatch, userValues } = this.props;
    const { selectedUser } = this.state;
    if (userValues.workspaceId === 'default') userValues.workspaceId = null;
    if (userValues.indicatorId === 'default') userValues.indicatorId = null;
    if (!selectedUser.id) {
      dispatch(createUser(userValues));
    } else {
      if (userValues.password === '') userValues.password = null;
      dispatch(updateUser(selectedUser.id, userValues));
    }
  };

  render() {
    const { selectedUser } = this.state;
    const { validateStatuses, valid, dirty } = this.props;
    const isEdit = selectedUser && selectedUser.id;
    const title = isEdit
      ? selectedUser.fullName
        ? selectedUser.fullName
        : selectedUser.name
      : 'Создать нового пользователя';
    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <WidgetParamsEditorMenu
          isViewTitle={selectedUser}
          title={title}
          errorMessage={getFirstError(validateStatuses)}
          isSaveActive={selectedUser && valid && dirty}
          onSaveAction={this.onUserSave}
        />
        <Form>
          <Tabs onTabClick={this.onTabClick}>
            {this.components.map(component => (
              <TabPane
                style={TAB_PANE_STYLES}
                key={component.name}
                tab={component.title}
                forceRendrer={true}
              >
                {component.component}
              </TabPane>
            ))}
          </Tabs>
        </Form>
      </Widget>
    );
  }
}

const validate = values => {
  let result = {};

  if (!values.name) {
    result.name = 'Не задано имя';
  } else if (values.name.length > 30) {
    result.name = 'Имя должно быть не больше 30 символов';
  }

  if (!values.fullName) {
    result.fullName = 'Не задано полное имя';
  } else if (values.fullName.length > 100) {
    result.fullName = 'Полное имя должно быть не больше 100 символов';
  }

  if (!values.email) {
    result.email = 'Не задан email';
  } else if (values.email.length > 50) {
    result.email = 'Email должен быть не больше 50 символов';
  } else if (!/^[\w]+(\.[\w]+)*@[\w]+(\.[\w]+)*(\.[a-z]{2,})$/i.test(values.email)) {
    result.email = 'Неверный формат email';
  }

  if (!values.phoneNo) {
    result.phoneNo = 'Не задан номер телефона';
  } else if (`${values.phoneNo}`.length !== 12) {
    result.phoneNo = 'Номер телефона должен быть 12 символов';
  } else if (!/^\+[0-9]{11}$/i.test(values.phoneNo)) {
    result.phoneNo = 'Неверный формат номера телефона';
  }

  if (values.password) {
    if (values.password.length < 6) {
      result.password = 'Длинна пароля меньше 6 символов';
    } else if (values.password && !isEqual(values.password, values.password2)) {
      result.password2 = 'Пароли не совпадают';
    }
  }

  if (!values.id) {
    // создание
    if (!values.password) {
      result.password = 'Не задан пароль';
    }
  }

  return result;
};

UserInfoEditor = reduxForm({
  form: FORM_NAME,
  validate
})(UserInfoEditor);

const mapStateToProps = state => {
  return {
    users: state.admin.users,
    userGroups: state.admin.userGroups,
    selectedUserId: state.widgets.selectedUserId,
    userValues: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {},
    loadUserGroupsError: state.errors.loadUserGroupsError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserInfoEditor);
