import React, { Component } from 'react';
import { Field } from 'redux-form';
import { getFormSyncErrors, getFormValues } from 'redux-form';
import { connect } from 'react-redux';

import { FormItem } from 'components/Form';
import Widget from 'components/Widget';
import Select from 'containers/Selects';

import { FORM_NAME } from './UserInfoEditor';

class UserInterfaces extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
  }

  render() {
    const { selectedUser, validateStatuses } = this.props;
    const itemsSource = [{ id: 'default', name: 'По умолчанию' }];
    return (
      <Widget
        onHeightReady={height => this.setState({ height })}
        children={
          selectedUser ? (
            <div>
              <FormItem
                label="Интерфейс"
                validateStatus={validateStatuses.workspace ? 'error' : ''}
              >
                <Field component={Select} name="workspaceId" itemsSource={itemsSource} disabled />
              </FormItem>

              <FormItem
                label="Индикатор"
                validateStatus={validateStatuses.indicator ? 'error' : ''}
              >
                <Field component={Select} name="indicatorId" itemsSource={itemsSource} disabled />
              </FormItem>
            </div>
          ) : (
            ''
          )
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUser: getFormValues(FORM_NAME)(state),
    validateStatuses: getFormSyncErrors(FORM_NAME)(state) || {}
  };
};

export default connect(
  mapStateToProps,
  null
)(UserInterfaces);
