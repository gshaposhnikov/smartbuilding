import React, { Component } from 'react';
import { connect } from 'react-redux';

import CurrentProjectMenu from 'containers/Menus/CurrentProjectMenu';
import { loadCurrentProject, setCurrentProject } from 'actions/projects';

class CurrentProjectWidget extends Component {
  componentWillMount() {
    this.setCurrentProjectAndLoadEntities(this.props);
  }

  componentWillReceiveProps = nextProps => {
    this.setCurrentProjectAndLoadEntities(nextProps);
  };

  setCurrentProjectAndLoadEntities = props => {
    const { dispatch, currentProjectId, reduxCurrentProjectId, projects } = props;
    if (currentProjectId !== reduxCurrentProjectId) {
      dispatch(setCurrentProject(currentProjectId));
      if (!projects[currentProjectId] || !projects[currentProjectId].entitiesLoaded) {
        dispatch(loadCurrentProject(currentProjectId));
      }
    }
  };

  render() {
    const { projects, currentProjectId } = this.props;

    return <CurrentProjectMenu currentProject={projects[currentProjectId] || {}} />;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {
    reduxCurrentProjectId: state.currentProjectId,
    projects: state.projects
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentProjectWidget);
