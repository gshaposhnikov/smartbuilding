import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { saveAs as saveFile } from 'file-saver';

import {
  loadArchiveEvents,
  loadAvailableEvents,
  cleanArchiveEvents,
  setArchiveEventsFilters,
  setSelectedEvent
} from 'actions/events';
import { modalOpen, modalClose } from 'actions/modals';

import Table, { Column } from 'components/Table';
import Widget from 'components/Widget';
import {
  ColorRow,
  renderAddressDeviceCell,
  renderRegionCell,
  renderScenarioCell,
  renderEmployeeCell,
  renderDescriptionCell
} from 'components/EventTable';

import ConstructEventsFilterForm, {
  resetFormValues
} from 'containers/Forms/ConstructEventsFilterForm';
import ArchiveEventsMenu from 'containers/Menus/ArchiveEventsMenu';

import { getDateTimestamp } from 'helpers/time';
import HTML from 'helpers/HTML';

const PAGE_SIZE = 50;
const FOOTER_HEIGHT = 45;

class ArchiveEventsWidget extends Component {
  static propTypes = {
    /* Из redux-store */

    /* События */
    events: PropTypes.array,
    /* Сколько всего событий (с учетом текущего фильтра) */
    totalEventsCount: PropTypes.number,
    /* Идентификатор вывбранного события */
    selectedEventId: PropTypes.string,
    /* Ошибка получения событий от сервера */
    loadAvailableEventsError: PropTypes.string,
    /* Параметры фильтрации */
    filters: PropTypes.array
  };
  constructor(props) {
    super(props);
    this.state = {
      height: 500,
      isLoading: false,
      sorter: {
        field: 'received',
        order: 'descend'
      },
      pagination: {
        defaultPageSize: PAGE_SIZE,
        pageSizeOptions: ['50', '100', '200', '400'],
        defaultCurrent: 1,
        total: 0,
        showSizeChanger: true
      }
    };
  }

  getFiltersObject = filters => {
    const filtersObject = {};
    filters.forEach(filter => {
      if (moment.isMoment(filter.value)) {
        filtersObject[filter.id] = encodeURIComponent(
          filter.value.format('YYYY-MM-DDTHH:mm:ss.SSSZZ')
        );
      } else if (Array.isArray(filter.value)) {
        filtersObject[filter.id] = filter.value.join();
      } else {
        filtersObject[filter.id] = filter.value;
      }
    });
    return filtersObject;
  };

  getSorterString = () => {
    const { sorter } = this.state;
    if (sorter.field) {
      return `${sorter.field},${sorter.order === 'descend' ? 'desc' : 'asc'}`;
    } else {
      return '';
    }
  };

  loadCurrentPageEvents = (filters, requestCountForced = false) => {
    this.setState({ isLoading: true }, () => {
      const { dispatch } = this.props;
      const { pagination } = this.state;
      dispatch(
        loadArchiveEvents(
          pagination.pageSize || PAGE_SIZE,
          pagination.current ? pagination.current - 1 : 0,
          this.getSorterString(),
          this.getFiltersObject(filters),
          requestCountForced
        )
      );
    });
  };

  componentDidMount = () => {
    const { dispatch, defaultFilters, filters } = this.props;
    if (defaultFilters) {
      dispatch(setArchiveEventsFilters(defaultFilters));
      this.loadCurrentPageEvents(defaultFilters);
    } else {
      this.loadCurrentPageEvents(filters);
    }
    window.addEventListener('keydown', this.onKeyDown);
  };

  componentWillUnmount = () => {
    window.removeEventListener('keydown', this.onKeyDown);
    const { dispatch } = this.props;
    dispatch(cleanArchiveEvents());
  };

  componentWillReceiveProps = nextProps => {
    const {
      events,
      totalEventsCount,
      selectedEventId,
      filters,
      loadAvailableEventsError
    } = nextProps;
    const { dispatch } = this.props;
    const { needSaving, pagination } = this.state;

    if (events !== this.props.events) {
      if (events) {
        this.setState({
          isLoading: false,
          needSaving: false
        });
        if (needSaving) {
          this.saveEventTable(nextProps);
        }
      } else {
        this.loadCurrentPageEvents(filters, filters !== this.props.filters);
      }
    }

    if (loadAvailableEventsError && needSaving) {
      this.setState({
        isLoading: false,
        needSaving: false
      });
    }

    if (totalEventsCount !== pagination.total)
      this.setState({ pagination: { ...pagination, total: totalEventsCount } });

    if (events && events.length && !this.props.events && !selectedEventId)
      dispatch(setSelectedEvent(events[0].id));
  };

  onEventsFilterClick = () => {
    const { dispatch, filters } = this.props;
    resetFormValues(dispatch, { filters });
    dispatch(modalOpen('constructEventsFilter'));
  };

  onEventsFilterSubmit = values => {
    const { dispatch } = this.props;
    dispatch(setArchiveEventsFilters(values.filters));
    dispatch(modalClose('constructEventsFilter'));
  };

  onTableChange = (newPagination, newFilters, newSorter) => {
    const { dispatch } = this.props;
    const { sorter, pagination } = this.state;
    if (sorter.field !== newSorter.field || sorter.order !== newSorter.order) {
      this.setState({ sorter: newSorter, pagination: { ...pagination, current: 0 } }, () =>
        dispatch(cleanArchiveEvents())
      );
    } else if (pagination.current !== newPagination.current) {
      this.setState({ pagination: newPagination }, () => dispatch(cleanArchiveEvents()));
    } else if (pagination.pageSize !== newPagination.pageSize)
      this.setState({ pagination: { ...newPagination, current: 0 } }, () =>
        dispatch(cleanArchiveEvents())
      );
  };

  onRowClick = record => {
    const { dispatch, selectedEventId } = this.props;
    if (this.table.classList[0] !== 'ant-table-body')
      this.table = this.table.getElementsByClassName('ant-table-body')[0];
    dispatch(setSelectedEvent(record.id === selectedEventId ? null : record.id));
  };

  onKeyDown = e => {
    const { events, selectedEventId, dispatch } = this.props;
    if (selectedEventId && this.table) {
      switch (e.keyCode) {
        // Клавиша вверх
        case 38: {
          e.preventDefault();
          const prevEventIndex = events.findIndex(event => event.id === selectedEventId) - 1;
          if (prevEventIndex < 0) break;
          dispatch(setSelectedEvent(events[prevEventIndex].id));
          if (this.table.classList[0] !== 'ant-table-body')
            this.table = this.table.getElementsByClassName('ant-table-body')[0];
          if (prevEventIndex * 26 - this.table.scrollTop <= 0)
            this.table.scrollTop = prevEventIndex * 26;
          break;
        }
        // Клавиша вниз
        case 40: {
          e.preventDefault();
          const nextEventIndex = events.findIndex(event => event.id === selectedEventId) + 1;
          if (nextEventIndex >= events.length) break;
          dispatch(setSelectedEvent(events[nextEventIndex].id));
          if (this.table.classList[0] !== 'ant-table-body')
            this.table = this.table.getElementsByClassName('ant-table-body')[0];
          if ((nextEventIndex + 1) * 26 - this.table.scrollTop >= this.table.clientHeight)
            this.table.scrollTop = (nextEventIndex + 1) * 26 - this.table.clientHeight;
          break;
        }
        default:
          break;
      }
    }
  };

  getAvailableEvents = () => {
    const { dispatch, events, totalEventsCount, filters } = this.props;
    if (events && events.length && totalEventsCount === events.length) {
      this.saveEventTable(this.props);
    } else {
      this.setState(
        {
          isLoading: true,
          needSaving: true
        },
        dispatch(loadAvailableEvents(this.getSorterString(), this.getFiltersObject(filters)))
      );
    }
  };

  saveEventTable = props => {
    const { events } = props;
    const columns = [
      {
        title: 'Время прибора',
        width: '10%',
        dataIndex: 'occurred',
        render: record => (record.occurred ? getDateTimestamp(record.occurred) : '')
      },
      {
        title: 'Системное время',
        width: '10%',
        dataIndex: 'received',
        render: record => (record.occurred ? getDateTimestamp(record.occurred) : '')
      },
      {
        title: 'Описание',
        dataIndex: 'name'
      },
      {
        title: 'Прибор',
        width: '10%',
        dataIndex: 'controlDeviceEventInfo.address',
        render: record =>
          record.controlDeviceEventInfo && record.controlDeviceEventInfo.name !== ''
            ? `${record.controlDeviceEventInfo.name}, ${record.controlDeviceEventInfo.address}`
            : ''
      },
      {
        title: 'Адресное устройство',
        width: '10%',
        dataIndex: 'deviceEventInfo.address',
        render: record =>
          record.deviceEventInfo
            ? `${record.deviceEventInfo.name}, ${record.deviceEventInfo.address}`
            : ''
      },
      {
        title: 'Зона',
        width: '10%',
        dataIndex: 'regionEventInfo.name',
        render: record => (record.regionEventInfo ? `${record.regionEventInfo.name}` : '')
      },
      {
        title: 'Вирт. состояние',
        width: '10%',
        dataIndex: 'virtualStateEventInfo.name',
        render: record => (record.virtualStateEventInfo ? record.virtualStateEventInfo.name : '')
      },
      {
        title: 'Сценарий',
        width: '10%',
        dataIndex: 'scenarioEventInfo.name',
        render: record => (record.scenarioEventInfo ? record.scenarioEventInfo.name : '')
      },
      {
        title: 'Пользователь',
        width: '10%',
        dataIndex: 'userEventInfo.name',
        render: record => (record.userEventInfo ? record.userEventInfo.name : '')
      }
    ];
    const html = new HTML({
      title: 'Архив событий',
      contentType: 'table',
      columns: columns,
      data: events || []
    }).htmlString;
    const blob = new Blob([html], { type: 'text/plain' });
    saveFile(blob, `FS-ARCHIVE.html`);
  };

  render() {
    const { events, selectedEventId } = this.props;
    const { height, isLoading, sorter, pagination } = this.state;
    return (
      <Widget
        onHeightReady={height => {
          this.setState({ height });
        }}
      >
        <ConstructEventsFilterForm
          modalName="constructEventsFilter"
          onSubmit={this.onEventsFilterSubmit}
        />
        <ArchiveEventsMenu
          isSaveEventTableActive={events && !isLoading}
          onSaveEventTable={this.getAvailableEvents}
          isConstructFiltersActive={true}
          onConstructFiltersClick={this.onEventsFilterClick}
        />
        <div ref={node => (this.table = !this.table ? node : this.table)}>
          <Table
            dataSource={events || []}
            className="logTable"
            rowKey="id"
            scroll={{ y: height - (26 + FOOTER_HEIGHT * 2) }}
            pagination={pagination}
            loading={isLoading}
            onChange={this.onTableChange}
            onRowClick={this.onRowClick}
            rowClassName={record => (record.id === selectedEventId ? 'selected-row' : '')}
          >
            <Column
              title="Время прибора"
              width="10%"
              dataIndex="occurred"
              key="occurred"
              render={(text, record) => (
                <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
                  {record.occurred ? getDateTimestamp(record.occurred) : ''}
                </ColorRow>
              )}
              sorter={true}
              sortOrder={sorter.field === 'occurred' ? sorter.order : false}
            />
            <Column
              title="Системное время"
              width="10%"
              dataIndex="received"
              key="received"
              render={(text, record) => (
                <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
                  {record.received ? getDateTimestamp(record.received) : ''}
                </ColorRow>
              )}
              sorter={true}
              sortOrder={sorter.field === 'received' ? sorter.order : false}
            />
            <Column
              title="Описание"
              dataIndex="name"
              key="name"
              render={(text, record) => renderDescriptionCell(text, record, selectedEventId)}
              sorter={true}
              sortOrder={sorter.field === 'name' ? sorter.order : false}
            />
            <Column
              title="Прибор"
              width="10%"
              dataIndex="controlDeviceEventInfo.address"
              key="controlDeviceEventInfo"
              render={(text, record) => (
                <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
                  {record.controlDeviceEventInfo && record.controlDeviceEventInfo.name !== ''
                    ? `${record.controlDeviceEventInfo.name},
                  ${record.controlDeviceEventInfo.address}`
                    : null}
                </ColorRow>
              )}
              sorter={true}
              sortOrder={sorter.field === 'controlDeviceEventInfo.address' ? sorter.order : false}
            />
            <Column
              title="Адресное устройство"
              width="10%"
              dataIndex="deviceEventInfo.address"
              key="deviceEventInfo"
              render={(text, record) => renderAddressDeviceCell(text, record, selectedEventId)}
              sorter={true}
              sortOrder={sorter.field === 'deviceEventInfo.address' ? sorter.order : false}
            />
            <Column
              title="Зона"
              width="10%"
              dataIndex="regionEventInfo.name"
              key="regionEventInfo"
              render={(text, record) => renderRegionCell(text, record, selectedEventId)}
              sorter={true}
              sortOrder={sorter.field === 'regionEventInfo.name' ? sorter.order : false}
            />
            <Column
              title="Вирт. состояние"
              width="10%"
              dataIndex="virtualStateEventInfo.name"
              key="virtualStateEventInfo"
              render={(text, record) => (
                <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
                  {record.virtualStateEventInfo ? `${record.virtualStateEventInfo.name}` : null}
                </ColorRow>
              )}
              sorter={true}
              sortOrder={sorter.field === 'virtualStateEventInfo.name' ? sorter.order : false}
            />
            <Column
              title="Сценарий"
              width="10%"
              dataIndex="scenarioEventInfo.name"
              key="scenarioEventInfo"
              render={(text, record) => renderScenarioCell(text, record, selectedEventId)}
              sorter={true}
              sortOrder={sorter.field === 'scenarioEventInfo.name' ? sorter.order : false}
            />
            <Column
              title="Пользователь"
              width="10%"
              dataIndex="userEventInfo.name"
              key="userEventInfo"
              render={(text, record) => (
                <ColorRow stateCategoryId={record.id !== selectedEventId && record.stateCategoryId}>
                  {record.userEventInfo ? `${record.userEventInfo.name}` : null}
                </ColorRow>
              )}
              sorter={true}
              sortOrder={sorter.field === 'userEventInfo.name' ? sorter.order : false}
            />
            <Column
              title="Сотрудник (СКУД)"
              width="10%"
              dataIndex="employeeEventInfo.name"
              key="employeeEventInfo"
              render={(text, record) => renderEmployeeCell(record, selectedEventId)}
              sorter={true}
              sortOrder={sorter.field === 'employeeEventInfo.name' ? sorter.order : false}
            />
          </Table>
        </div>
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    events: state.activeProject.archiveEvents.events,
    totalEventsCount: state.activeProject.archiveEvents.totalEventsCount,
    selectedEventId: state.activeProject.archiveEvents.selectedEventId,
    loadAvailableEventsError: state.errors.loadAvailableEvents,
    filters: state.widgets.archiveEventsWidget.filters
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArchiveEventsWidget);
