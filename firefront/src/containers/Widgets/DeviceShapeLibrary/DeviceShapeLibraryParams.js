import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, initialize, reset, formValueSelector } from 'redux-form';
import SVGInline from 'react-svg-inline';
import { Button, Icon, Upload } from 'antd';
import isSvg from 'is-svg';
import { parse } from 'svg-parser';
import styled from 'styled-components';

import Input from 'components/Form/Input';
import Checkbox from 'components/Form/Checkbox';
import InputNumber from 'components/Form/InputNumber';
import Select from 'containers/Selects';
import Form, { FormItem } from 'components/Form';

import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';
import { ENTITY_TYPE, ANIMATION_TYPE, ANIMATION_SPEED } from 'constants/deviceShapeLibrary';
import {
  addDeviceShapeLibrary,
  updateDeviceShapeLibrary,
  addDeviceShape,
  updateDeviceShape
} from 'actions/deviceShapeLibrary';
import { WhiteFormWrapper, ScrollableContainerWithoutHeader } from 'components/Layout';

import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const FORM_NAME = 'deviceShapeLibraryParamsForm';
const MAX_SVG_FILE_SIZE = 15728640;
const PREVIEW_WRAPPER_STYLES = {
  width: 100,
  height: 100,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'lightgray',
  borderRadius: '25px'
};
const EMPTY_ENTITY = {
  title: 'Выберите библиотеку или шейп',
  customSvgFile: null,
  customSvgFileValid: true,
  customSvgFileErrorMessage: ''
};

const Shape = styled.div`
  background: ${p => p.backgroundColor};
  text-align: center;
  border-color: ${p => p.borderColor};
  border-style: solid;
  animation-name: ${p => p.animationType};
  animation-duration: ${p => p.animationDuration}ms;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  & svg * {
    fill: ${p => p.borderColor};
    stroke: ${p => p.borderColor};
  }
`;

/*
 * TODO: вызов этого метода приводит к наростающим тормозам при навигации по библиотеки шейпов.
 * Возможно надо уйти совсем от redux-form
 */

/**
 * Сброс значение формы
 * @param dispatch
 * @param values объект со значениями
 */
export function resetDeviceShapeLibraryParams(dispatch, values) {
  dispatch(initialize(FORM_NAME, values));
  dispatch(reset(FORM_NAME));
}

class DeviceShapeLibraryParamsForm extends Component {
  state = EMPTY_ENTITY;

  componentWillReceiveProps(newProps) {
    const { selectedTreeItem } = newProps;
    if (selectedTreeItem !== this.props.selectedTreeItem) {
      if (selectedTreeItem) {
        resetDeviceShapeLibraryParams(newProps.dispatch, selectedTreeItem.entity);

        const { deviceProfileViewsHash, deviceShapeLibrariesHash } = newProps;

        switch (selectedTreeItem.entityType) {
          case ENTITY_TYPE.DEVICE_SHAPE_LIBRARY:
            if (deviceProfileViewsHash[selectedTreeItem.entity.deviceProfileId])
              this.setState({
                title: `Библиотека шейпов устройства ${
                  deviceProfileViewsHash[selectedTreeItem.entity.deviceProfileId].name
                }: "${selectedTreeItem.entity.name}"`
              });
            break;
          case ENTITY_TYPE.DEVICE_SHAPE:
            if (deviceShapeLibrariesHash[selectedTreeItem.entity.deviceShapeLibraryId])
              this.setState({
                title: `Шейп устройства ${
                  deviceProfileViewsHash[
                    deviceShapeLibrariesHash[selectedTreeItem.entity.deviceShapeLibraryId]
                      .deviceProfileId
                  ].name
                }: "${selectedTreeItem.entity.name}"`
              });
            break;
          case ENTITY_TYPE.NEW_DEVICE_SHAPE_LIBRARY:
            this.setState({
              title: `Новая библиотека шейпов устройства ${
                deviceProfileViewsHash[selectedTreeItem.entity.deviceProfileId].name
              }`
            });
            break;
          case ENTITY_TYPE.NEW_DEVICE_SHAPE:
            this.setState({
              title: `Новый шейп устройства ${
                deviceProfileViewsHash[
                  deviceShapeLibrariesHash[selectedTreeItem.entity.deviceShapeLibraryId]
                    .deviceProfileId
                ].name
              } библиотеки "${
                deviceShapeLibrariesHash[selectedTreeItem.entity.deviceShapeLibraryId].name
              }"`
            });
            break;
          default:
            break;
        }
      } else {
        this.setState(EMPTY_ENTITY);
      }
    }
  }

  doAddDeviceShapeLibrary() {
    const { dispatch, currentValues } = this.props;
    dispatch(addDeviceShapeLibrary(currentValues));
  }

  doUpdateDeviceShapeLibrary(deviceShapeLibraryId) {
    const { dispatch, currentValues } = this.props;
    dispatch(updateDeviceShapeLibrary(deviceShapeLibraryId, currentValues));
  }

  doAddDeviceShape() {
    const { dispatch, currentValues, stateCategoryViewsHash } = this.props;
    dispatch(
      addDeviceShape({
        ...currentValues,
        name: currentValues.stateCategoryId
          ? stateCategoryViewsHash[currentValues.stateCategoryId].name
          : 'Базовый шейп'
      })
    );
    this.setState({
      customSvgFile: null,
      customSvgFileValid: true,
      customSvgFileErrorMessage: ''
    });
  }

  doUpdateDeviceShape(deviceShapeId) {
    const { dispatch, currentValues, stateCategoryViewsHash } = this.props;
    dispatch(
      updateDeviceShape(deviceShapeId, {
        ...currentValues,
        name: currentValues.stateCategoryId
          ? stateCategoryViewsHash[currentValues.stateCategoryId].name
          : 'Базовый шейп'
      })
    );
    this.setState({
      customSvgFile: null,
      customSvgFileValid: true,
      customSvgFileErrorMessage: ''
    });
  }

  onSave() {
    const { selectedTreeItem } = this.props;
    if (selectedTreeItem) {
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.NEW_DEVICE_SHAPE_LIBRARY:
          this.doAddDeviceShapeLibrary();
          break;
        case ENTITY_TYPE.DEVICE_SHAPE_LIBRARY:
          this.doUpdateDeviceShapeLibrary(selectedTreeItem.entity.id);
          break;
        case ENTITY_TYPE.NEW_DEVICE_SHAPE:
          this.doAddDeviceShape();
          break;
        case ENTITY_TYPE.DEVICE_SHAPE:
          this.doUpdateDeviceShape(selectedTreeItem.entity.id);
          break;
        default:
          break;
      }
    }
  }

  /* Метод, которые подменяет AJAX-запрос сохранением файла в state */
  putFileToStateInsteadAJAXRequest(event) {
    const { file } = event;

    this.setState({
      customSvgFile: { name: file.name, type: file.type, size: file.size, uid: file.uid }
    });
    if (file.size > MAX_SVG_FILE_SIZE) {
      this.setState({
        customSvgFileValid: false,
        customSvgFileErrorMessage: 'Размер файла не должен превышать 15 мб'
      });
    } else {
      const reader = new FileReader();
      reader.onload = () => {
        if (!isSvg(reader.result)) {
          this.setState({
            customSvgFileValid: false,
            customSvgFileErrorMessage: 'Файл должен содержать SVG'
          });
          return;
        }

        let svgObject;
        try {
          svgObject = parse(reader.result);
          if (!svgObject) throw new Error('Файл невалидный');
          if (
            !svgObject.attributes ||
            !svgObject.attributes.xmlns ||
            svgObject.attributes.xmlns !== 'http://www.w3.org/2000/svg'
          )
            throw new Error('Файл невалидный');
          if (!svgObject.attributes || !svgObject.attributes.width || !svgObject.attributes.height)
            throw new Error('Не удалось прочитать высоту и ширину SVG');
        } catch (error) {
          this.setState({
            customSvgFileValid: false,
            customSvgFileErrorMessage: error.message
          });
          return;
        }

        if (
          parseInt(svgObject.attributes.width, 10) !== 50 ||
          parseInt(svgObject.attributes.height, 10) !== 50
        ) {
          this.setState({
            customSvgFileValid: false,
            customSvgFileErrorMessage: 'SVG-изображение должно иметь размеры 50x50'
          });
          return;
        }
        const { change } = this.props;
        change('svgContent', reader.result.replace(/\r?\n|\r/g, ' '));
        this.setState({ customSvgFileValid: true });
      };
      reader.onerror = () => {};
      reader.readAsText(file);
    }
  }

  renderSvg(currentValues) {
    const { svgsHash, stateCategoryViewsHash } = this.props;
    if (currentValues.builtinMediaId) {
      if (svgsHash[currentValues.builtinMediaId]) {
        return (
          <Shape
            animationType={currentValues.animationType}
            animationDuration={currentValues.animationSpeed}
            borderColor={
              stateCategoryViewsHash[currentValues.stateCategoryId]
                ? stateCategoryViewsHash[currentValues.stateCategoryId].fontColor
                : '#000'
            }
            backgroundColor={
              stateCategoryViewsHash[currentValues.stateCategoryId]
                ? stateCategoryViewsHash[currentValues.stateCategoryId].color
                : 'silver'
            }
          >
            <SVGInline svg={svgsHash[currentValues.builtinMediaId].content} />
          </Shape>
        );
      }
    } else if (currentValues.svgContent) {
      return (
        <Shape
          animationType={currentValues.animationType}
          animationDuration={currentValues.animationSpeed}
          borderColor={
            stateCategoryViewsHash[currentValues.stateCategoryId]
              ? stateCategoryViewsHash[currentValues.stateCategoryId].fontColor
              : '#000'
          }
          backgroundColor={
            stateCategoryViewsHash[currentValues.stateCategoryId]
              ? stateCategoryViewsHash[currentValues.stateCategoryId].color
              : 'silver'
          }
        >
          <SVGInline svg={currentValues.svgContent} />
        </Shape>
      );
    }
    return <div />;
  }

  render() {
    const {
      selectedTreeItem,
      stateCategoryViewsHash,
      svgsHash,
      currentValues,
      valid,
      dirty
    } = this.props;

    const { title, customSvgFile, customSvgFileValid, customSvgFileErrorMessage } = this.state;

    /* Видимость полей библиотеки шейпов */

    const displayShapeLibraryParams =
      selectedTreeItem &&
      (selectedTreeItem.entityType === ENTITY_TYPE.NEW_DEVICE_SHAPE_LIBRARY ||
        selectedTreeItem.entityType === ENTITY_TYPE.DEVICE_SHAPE_LIBRARY);

    /* Видимость полей шейпов */

    const displayShapeParams =
      selectedTreeItem &&
      (selectedTreeItem.entityType === ENTITY_TYPE.NEW_DEVICE_SHAPE ||
        selectedTreeItem.entityType === ENTITY_TYPE.DEVICE_SHAPE);

    return (
      <WhiteFormWrapper>
        <WidgetParamsEditorMenu
          isViewTitle={true}
          title={title}
          isSaveActive={
            valid &&
            selectedTreeItem &&
            (selectedTreeItem.entityType === ENTITY_TYPE.NEW_DEVICE_SHAPE_LIBRARY ||
              selectedTreeItem.entityType === ENTITY_TYPE.NEW_DEVICE_SHAPE ||
              dirty)
          }
          onSaveAction={this.onSave.bind(this)}
        />
        <ScrollableContainerWithoutHeader headerHeight="36">
          <Form
            css={'padding: 0 24px;'}
            style={{
              display: selectedTreeItem ? 'inline' : 'none'
            }}
          >
            <div style={{ display: displayShapeLibraryParams ? 'inline' : 'none' }}>
              <FormItem label="Название" required>
                <Field component={Input} name="name" />
              </FormItem>
              <FormItem>
                <Field component={Checkbox} name="builtin" disabled label="Встроенная библиотека" />
              </FormItem>
            </div>
            <div style={{ display: displayShapeParams ? 'inline' : 'none' }}>
              <FormItem label="Класс состояния" required>
                <Field
                  component={Select}
                  name="stateCategoryId"
                  itemsSource={
                    selectedTreeItem && selectedTreeItem.entity.stateCategoryId === ''
                      ? [{ id: '', name: 'Общий' }]
                      : [
                          ...Object.keys(stateCategoryViewsHash).map(
                            key => stateCategoryViewsHash[key]
                          )
                        ]
                  }
                  disabled={
                    selectedTreeItem && selectedTreeItem.entityType !== ENTITY_TYPE.NEW_DEVICE_SHAPE
                  }
                />
              </FormItem>
              <FormItem label="Изображение" required>
                <Field
                  component={Select}
                  name="builtinMediaId"
                  itemsSource={[
                    { id: '', name: 'Пользовательское изображение' },
                    ...Object.keys(svgsHash).map(key => svgsHash[key])
                  ]}
                />
              </FormItem>
              <FormItem style={{ display: 'flex', justifyContent: 'center' }}>
                Предпросмотр:
                <div style={PREVIEW_WRAPPER_STYLES}>{this.renderSvg(currentValues)}</div>
              </FormItem>
              <FormItem
                style={{ display: !currentValues.builtinMediaId ? 'inline' : 'none' }}
                required
                help={
                  customSvgFileValid
                    ? customSvgFile
                      ? ''
                      : 'Файл не выбран'
                    : customSvgFileErrorMessage
                }
                validateStatus={customSvgFileValid ? '' : 'error'}
              >
                <Field
                  component={Upload}
                  name="svgContent"
                  customRequest={this.putFileToStateInsteadAJAXRequest.bind(this)}
                  fileList={customSvgFile ? [customSvgFile] : []}
                  accept={'image/svg+xml'}
                  multiple={false}
                  onRemove={() => {
                    this.setState({
                      customSvgFile: null,
                      customSvgFileValid: true,
                      customSvgFileErrorMessage: ''
                    });
                  }}
                >
                  <Button>
                    <Icon type="upload" /> {'Выбрать файл'}
                  </Button>
                </Field>
              </FormItem>
              <FormItem label="Тип анимации">
                <Field component={Select} name="animationType" itemsSource={ANIMATION_TYPE} />
              </FormItem>
              <FormItem label="Интервал анимации, мс">
                <Field
                  component={InputNumber}
                  name="animationSpeed"
                  min={ANIMATION_SPEED.MIN}
                  max={ANIMATION_SPEED.MAX}
                  disabled={currentValues.animationType === 'NONE'}
                />
              </FormItem>
            </div>
            <FormItem label="Описание">
              <Field component={Input} type="textarea" rows={4} name="description" />
            </FormItem>
          </Form>
        </ScrollableContainerWithoutHeader>
      </WhiteFormWrapper>
    );
  }
}

const validate = values => {
  if (values.deviceShapeLibraryId) {
    if (!values.builtinMediaId && !values.svgContent) {
      return { builtinMediaId: 'Не задано изображение' };
    }
  } else {
    if (!values.name) {
      return { name: 'Не задано название библиотеки' };
    }
  }
  return {};
};

DeviceShapeLibraryParamsForm = reduxForm({
  form: FORM_NAME,
  validate
})(DeviceShapeLibraryParamsForm);

const selector = formValueSelector(FORM_NAME);
const mapStateToProps = state => {
  return {
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    deviceShapeLibrariesHash: state.deviceShapeLibrary.deviceShapeLibrariesHash,
    selectedTreeItem: state.widgets.deviceShapeLibrary.selectedTreeItem,
    stateCategoryViewsHash: state.stateCategoryViewsHash,
    svgsHash: state.medias.svgsHash,
    /* Текущие значения полей */
    currentValues: state.form[FORM_NAME]
      ? selector(
          state,
          'name',
          'deviceProfileId',
          'description',
          'deviceShapeLibraryId',
          'stateCategoryId',
          'builtinMediaId',
          'svgContent',
          'animationType',
          'animationSpeed'
        )
      : {}
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceShapeLibraryParamsForm);
