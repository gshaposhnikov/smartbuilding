import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { Modal } from 'antd';

import Table from 'components/ExpandableTable/Table';
import { DoubleCRUDMenu } from 'components/Menu/WidgetMenu';
import {
  selectTreeItem,
  removeDeviceShapeLibrary,
  removeDeviceShape
} from 'actions/deviceShapeLibrary';
import { ENTITY_TYPE } from 'constants/deviceShapeLibrary';
import {
  createDeviceProfileTreeItem,
  createDeviceShapeLibraryTreeItem,
  createDeviceShapeTreeItem
} from 'helpers/deviceShapeLibrary';
import message from 'helpers/message';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';

const MAIN_WRAPPER_STYLES = { flex: 1, marginBottom: '25px' };

class DeviceShapeLibraryTree extends Component {
  static propTypes = {
    deviceProfileViewsHash: PropTypes.object,
    deviceShapeLibrariesHash: PropTypes.object,
    deviceShapesHash: PropTypes.object,
    stateCategoryViews: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.tableHeight = 500;
    this.columns = [
      {
        key: 'name',
        title: 'Название',
        dataIndex: 'entity',
        width: 250,
        stepper: true,
        render: (entity, node) => this.nameColumnRender(entity, node)
      },
      {
        key: 'description',
        title: 'Описание',
        dataIndex: 'entity',
        render: (entity, node) => this.descriptionColumnRender(entity, node)
      }
    ];
    this.state = {
      profileTreeItemsHash: {},
      libraryTreeItemsHash: {},
      libraryTree: [],
      scrollTreeTo: { state: false, expandedRows: [], id: null }
    };
  }

  getDeviceShapeLibraryTree(deviceProfileViewsHash, deviceShapeLibrariesHash, deviceShapesHash) {
    if (!isEmpty(deviceProfileViewsHash) && deviceShapeLibrariesHash && deviceShapesHash) {
      const { selectedTreeItem } = this.props;
      const profileTreeItemsHash = {};
      const libraryTreeItemsHash = {};
      const libraryTree = [];
      /* Верхний уровень - профили устройств */
      Object.keys(deviceProfileViewsHash).forEach(deviceProfileId => {
        const deviceProfileViewTreeItem = createDeviceProfileTreeItem(
          deviceProfileId,
          deviceProfileViewsHash[deviceProfileId]
        );
        profileTreeItemsHash[deviceProfileId] = deviceProfileViewTreeItem;
        libraryTree.push(deviceProfileViewTreeItem);
      });
      /* Средний уровень - библиотеки шейпов */
      Object.keys(deviceShapeLibrariesHash).forEach(deviceShapeLibraryId => {
        const libraryTreeItem = createDeviceShapeLibraryTreeItem(
          deviceShapeLibraryId,
          deviceShapeLibrariesHash[deviceShapeLibraryId]
        );
        if (selectedTreeItem && libraryTreeItem.id === selectedTreeItem.id) {
          libraryTreeItem.state = { expanded: true };
        }
        libraryTreeItemsHash[deviceShapeLibraryId] = libraryTreeItem;
        profileTreeItemsHash[libraryTreeItem.entity.deviceProfileId].children.push(libraryTreeItem);
      });
      /* Последний уровень - шейпы */
      Object.keys(deviceShapesHash).forEach(deviceShapeId => {
        const shapeTreeItem = createDeviceShapeTreeItem(
          deviceShapeId,
          deviceShapesHash[deviceShapeId]
        );
        if (libraryTreeItemsHash[shapeTreeItem.entity.deviceShapeLibraryId]) {
          if (selectedTreeItem && shapeTreeItem.id === selectedTreeItem.id) {
            shapeTreeItem.state = { expanded: true };
          }
          libraryTreeItemsHash[shapeTreeItem.entity.deviceShapeLibraryId].children.push(
            shapeTreeItem
          );
        }
      });

      this.setState({ profileTreeItemsHash, libraryTreeItemsHash, libraryTree });
    }
  }

  componentDidMount() {
    const { deviceProfileViewsHash, deviceShapeLibrariesHash, deviceShapesHash } = this.props;
    this.getDeviceShapeLibraryTree(
      deviceProfileViewsHash,
      deviceShapeLibrariesHash,
      deviceShapesHash
    );
  }

  componentWillReceiveProps(newProps) {
    const { deviceProfileViewsHash, deviceShapeLibrariesHash, deviceShapesHash } = newProps;
    if (
      deviceProfileViewsHash !== this.props.deviceProfileViewsHash ||
      deviceShapeLibrariesHash !== this.props.deviceShapeLibrariesHash ||
      deviceShapesHash !== this.props.deviceShapesHash
    ) {
      this.getDeviceShapeLibraryTree(
        deviceProfileViewsHash,
        deviceShapeLibrariesHash,
        deviceShapesHash
      );
    }

    /*
     * TODO: при изменении selectedTreeItem выставлять scrollTo, чтобы выделенный элемент раскрывался.
     * СЕйчас это убрано, т.к. после применения scrollTo дерево всегда отпрыгивает к элементу с идентификатором
     * со scrollTo если работаешь с элементами, находящимися далеко от него
     */
  }

  nameColumnRender(entity, node) {
    // return <Cell fontStyle={entity.builtin ? 'italic' : 'normal'}>{entity.name}</Cell>;
    // const Cell = styled.div`
    //   & .cell-content {
    //     font-style: ${p => p.fontStyle};
    //   }
    // `;
    // TODO сделать курсив для встроенных библиотек и базовых шейпов
    return entity.name;
  }

  descriptionColumnRender(entity, node) {
    return entity.description;
  }

  onRowClick(treeItem) {
    if (treeItem.entityType !== ENTITY_TYPE.DEVICE_PROFILE) {
      const { dispatch } = this.props;
      dispatch(selectTreeItem(treeItem));
      this.setState({ scrollTreeTo: { state: false, expandedRows: [], id: null } });
    }
  }

  getUnusedStateCategoryViews(deviceShapeLibraryId) {
    const { libraryTreeItemsHash } = this.state;
    const library = libraryTreeItemsHash[deviceShapeLibraryId];
    if (library) {
      const treeItemsOfLibrary = library.children;
      const { stateCategoryViews } = this.props;
      const unusedstateCategoryViews = stateCategoryViews.filter(stateCategoryView => {
        return !treeItemsOfLibrary.find(
          treeItem => treeItem.entity.stateCategoryId === stateCategoryView.id
        );
      });
      return unusedstateCategoryViews;
    }
    return null;
  }

  onAddClick() {
    const { selectedTreeItem, dispatch } = this.props;
    if (selectedTreeItem) {
      var newSelectedTreeItem;
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.DEVICE_SHAPE: {
          const unusedStateCategoryViews = this.getUnusedStateCategoryViews(
            selectedTreeItem.entity.deviceShapeLibraryId
          );
          if (!unusedStateCategoryViews || unusedStateCategoryViews.length === 0) {
            message('error', 'Библиотека шейпов заполнена');
            break;
          }

          const { deviceShapeLibrariesHash, deviceProfileViewsHash } = this.props;
          const deviceShapeLibrary =
            deviceShapeLibrariesHash[selectedTreeItem.entity.deviceShapeLibraryId];
          const deviceProfileView = deviceProfileViewsHash[deviceShapeLibrary.deviceProfileId];

          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_DEVICE_SHAPE,
            stateCategoryViews: unusedStateCategoryViews,
            entity: {
              deviceShapeLibraryId: deviceShapeLibrary.id,
              stateCategoryId: unusedStateCategoryViews[0].id,
              builtinMediaId: deviceProfileView ? deviceProfileView.textureMediaId : null,
              animationType: 'NONE',
              animationSpeed: 1000
            }
          };
          break;
        }

        case ENTITY_TYPE.DEVICE_SHAPE_LIBRARY: {
          const { profileTreeItemsHash } = this.state;
          newSelectedTreeItem = {
            entityType: ENTITY_TYPE.NEW_DEVICE_SHAPE_LIBRARY,
            entity: {
              deviceProfileId: selectedTreeItem.entity.deviceProfileId,
              name: `Библиотека шейпов ${
                profileTreeItemsHash[selectedTreeItem.entity.deviceProfileId].children.length
              }`
            }
          };
          break;
        }

        default:
          break;
      }
      if (newSelectedTreeItem) {
        dispatch(selectTreeItem(newSelectedTreeItem));
      }
    }
  }

  onDeleteClick() {
    const {
      selectedTreeItem,
      dispatch,
      deviceProfileViewsHash,
      deviceShapeLibrariesHash
    } = this.props;
    if (selectedTreeItem) {
      const { entity: treeItemEntity } = selectedTreeItem;
      switch (selectedTreeItem.entityType) {
        case ENTITY_TYPE.DEVICE_SHAPE_LIBRARY: {
          Modal.confirm({
            title: 'Удаление библиотеки шейпов',
            content: (
              <div>
                Удалить бибиотеку шейпов <b>{treeItemEntity.name}</b> для{' '}
                <b>{deviceProfileViewsHash[treeItemEntity.deviceProfileId].name}</b>?
              </div>
            ),
            okText: 'Удалить',
            onCancel: () => {},
            cancelText: 'Отмена',
            maskClosable: false,
            onOk: () => {
              dispatch(removeDeviceShapeLibrary(selectedTreeItem.entity.id));
            }
          });
          break;
        }
        case ENTITY_TYPE.DEVICE_SHAPE: {
          const deviceShapeLibrary = deviceShapeLibrariesHash[treeItemEntity.deviceShapeLibraryId];
          Modal.confirm({
            title: 'Удаление шейпа',
            content: (
              <div>
                Удалить шейп <b>{treeItemEntity.name}</b> из библиотеки шейпов{' '}
                <b>{deviceShapeLibrary.name}</b> для{' '}
                <b>{deviceProfileViewsHash[deviceShapeLibrary.deviceProfileId].name}</b>?
              </div>
            ),
            okText: 'Удалить',
            onCancel: () => {},
            cancelText: 'Отмена',
            maskClosable: false,
            onOk: () => {
              dispatch(removeDeviceShape(selectedTreeItem.entity.id));
            }
          });
          break;
        }
        default:
          break;
      }
    }
  }

  render() {
    const { selectedTreeItem } = this.props;
    const { libraryTree, scrollTreeTo } = this.state;
    return (
      <div
        style={MAIN_WRAPPER_STYLES}
        ref={node => {
          if (node) this.tableHeight = node.offsetHeight - 37;
        }}
      >
        <DoubleCRUDMenu
          isAddActive={
            selectedTreeItem && selectedTreeItem.entityType !== ENTITY_TYPE.DEVICE_PROFILE
          }
          onAdd={this.onAddClick.bind(this)}
          isDeleteActive={
            selectedTreeItem &&
            ((selectedTreeItem.entityType === ENTITY_TYPE.DEVICE_SHAPE_LIBRARY &&
              !selectedTreeItem.entity.builtin) ||
              (selectedTreeItem.entityType === ENTITY_TYPE.DEVICE_SHAPE &&
                selectedTreeItem.entity.stateCategoryId.length > 0))
          }
          onDelete={this.onDeleteClick.bind(this)}
        />
        <Table
          height={this.tableHeight}
          rowSelection={selectedTreeItem ? selectedTreeItem.id : null}
          columns={this.columns}
          nodes={libraryTree}
          onRowClick={this.onRowClick.bind(this)}
          initialExpandDeep={0}
          scrollTo={scrollTreeTo}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    deviceShapeLibrariesHash: state.deviceShapeLibrary.deviceShapeLibrariesHash,
    deviceShapesHash: state.deviceShapeLibrary.deviceShapesHash,
    stateCategoryViews: Object.keys(state.stateCategoryViewsHash).map(
      stateCategoryId => state.stateCategoryViewsHash[stateCategoryId]
    ),
    selectedTreeItem: state.widgets.deviceShapeLibrary.selectedTreeItem
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceShapeLibraryTree);
