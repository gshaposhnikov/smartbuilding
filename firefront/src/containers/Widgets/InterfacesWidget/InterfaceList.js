import React, { Component } from 'react';
import { connect } from 'react-redux';

import Widget from 'components/Widget';

class InterfacesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 500
    };
  }

  componentWillReceiveProps(newProps) {}

  render() {
    return <Widget onHeightReady={height => this.setState({ height })}>{'В разработке'}</Widget>;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  };
};

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(InterfacesList);
