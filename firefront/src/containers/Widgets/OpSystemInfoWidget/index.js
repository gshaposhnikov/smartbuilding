import React from 'react';
import styled from 'styled-components';
import { Spin } from 'antd';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import SystemInfo from 'components/SystemInfo';
import Text from 'components/Text';

import { getCurrentUser } from 'helpers/user';
import { filterEntities } from 'helpers/filtration';

const SystemInfoWrapper = styled.div`
  min-height: 80px;
  display: flex;
`;

const OpSystemInfoBlock = ({ project, subsystems }) => (
  <SystemInfoWrapper>
    {project && project.id && subsystems && subsystems.length > 0 ? (
      <SystemInfo status={subsystems} project={project} />
    ) : (
      <Spin>
        <Text>Нет данных</Text>
      </Spin>
    )}
  </SystemInfoWrapper>
);

const getSubsystems = state => state.activeProject.subsystems;
const getFilteredSubsystems = createSelector(
  getCurrentUser,
  getSubsystems,
  filterEntities
);

const mapStateToProps = state => ({
  subsystems: getFilteredSubsystems(state)
});

export default connect(
  mapStateToProps,
  null
)(OpSystemInfoBlock);
