import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import { isEqual, isEmpty } from 'lodash';
import {
  Sprite,
  Texture,
  Container,
  Application,
  Rectangle,
  Polygon,
  Graphics,
  Point
} from 'pixi.js';

import { ANIMATION_SPEED } from 'constants/deviceShapeLibrary';

import { updateCurrentDevice } from 'actions/devices';
import { addTexture, clearTextures } from 'actions/textures';
import { updateCurrentEntityInfo } from 'actions/widgets';

import {
  getTextures,
  getActivePlansHash,
  getActiveRegionsHash,
  getActiveDevicesHash
} from 'helpers/activeProject';
import { getDeviceProfileViewsHash } from 'helpers/deviceProfileViews';
import { getCurrentUser } from 'helpers/user';
import { availableByFilterTags } from 'helpers/filtration';

import PlanViewerPopup from './PlanViewerPopup';

const PIXI = window.PIXI;
PIXI.utils.skipHello();
const Loader = PIXI.loader;
/*
 * Важно: импорт pixi-layers должен быть после объявления константы PIXI,
 * т.к. внутри pixi-layers происходит работа с ней.
 */
import 'pixi-layers';

const HIGHLIGHT_COLOR = '0x5FA2DD';
const DRAWABLE_DEVICE_CATEGORY = {
  CONTROL: true,
  SENSOR: true,
  VIRTUAL_CONTAINER: true,
  EXECUTIVE: true
};
const ANIMATION = {
  RATIO: {
    ROTATING: 120,
    RESIZING: 30,
    BLINKING: 30
  },
  LIST_NAMES: {
    ROTATING: 'rotatedDevices',
    RESIZING: 'resizedDevices',
    BLINKING: 'blinkedDevices'
  },
  STEPS: {
    ROTATING: {
      MIN: 0,
      MAX: 1
    },
    RESIZING: {
      MIN: 0.7,
      MAX: 1.3
    },
    BLINKING: {
      MIN: 0.3,
      MAX: 1
    }
  },
  SPEED: ANIMATION_SPEED
};

const MONITORED_VALUES = {
  AVTEMP: true,
  AVLUX: true,
  AVHUM: true
};

class PlanRenderer extends React.Component {
  static propTypes = {
    /* Из redux-store */
    plan: PropTypes.object, // Текущий план
    regionsHash: PropTypes.object, // Хеш зон
    devicesHash: PropTypes.object // Хеш устройств
  };

  constructor(props) {
    super(props);
    this.blinkedDevices = new Map();
    this.rotatedDevices = new Map();
    this.resizedDevices = new Map();
    this.animationSteps = new Map();
    this.devicesLayout = null;
    this.devicesGroup = null;
    this.regionsLayout = null;
    this.regionsGroup = null;
    this.backgroundsLayout = null;
    this.backgroundsGroup = null;
    this.canvas = null;
    this.pixiApp = null;
    this.mainLayout = null;
    this.originBackgroundSize = [];
    this.defaultZoom = 1.0;
    this.state = {
      isReady: false
    };
    this.screen = {
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    };
  }

  /** MAIN FUNCTION */
  createPixiApp = (plan, planZoom) => {
    if (plan) {
      const { canvasId } = this.props;
      PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
      const canvas = document.getElementById(canvasId),
        pixiApp = new Application({
          width: plan.xSize * planZoom,
          height: plan.ySize * planZoom,
          forceCanvas: true, // Подгоняет pixi под текущий размер canvas
          backgroundColor: 0xffffff
        });
      const backgroundsGroup = new PIXI.display.Group(-1, false);
      const regionsGroup = new PIXI.display.Group(0, false);
      const devicesGroup = new PIXI.display.Group(1, false);
      pixiApp.stage = new PIXI.display.Stage();
      pixiApp.stage.addChild(new PIXI.display.Layer(backgroundsGroup));
      pixiApp.stage.addChild(new PIXI.display.Layer(regionsGroup));
      pixiApp.stage.addChild(new PIXI.display.Layer(devicesGroup));
      const mainLayout = new Graphics();
      mainLayout.parentGroup = backgroundsGroup;
      pixiApp.stage.addChild(mainLayout);
      canvas.appendChild(pixiApp.view);
      pixiApp.render();
      this.canvas = canvas;
      this.pixiApp = pixiApp;
      this.backgroundsGroup = backgroundsGroup;
      this.regionsGroup = regionsGroup;
      this.devicesGroup = devicesGroup;
      this.mainLayout = mainLayout;
    }
  };

  /**
   * HELPERS AND UTILS
   */

  isCurrentEntityCheck = (entityType, entityId) => {
    const { currentEntityInfo } = this.props;
    if (!currentEntityInfo) return false;
    if (currentEntityInfo.entityType === entityType && currentEntityInfo.entityId === entityId)
      return true;
    return false;
  };

  highlightObject = (target, highlight) => {
    target.serviceData.highlight = highlight;
    target.clear();
    if (target.serviceData.regionId) this.drawRegion(target);
    else if (target.serviceData.deviceId) this.drawDevice(target);
  };

  onRegionClick = options => {
    const { dispatch } = this.props;
    const { serviceData } = options.target;
    dispatch(
      updateCurrentEntityInfo({
        entityType: 'REGION',
        name: serviceData.entity.name,
        entityId: serviceData.entity.id,
        subsystem: serviceData.entity.subsystem,
        onGuard: serviceData.entity.onGuard,
        state: serviceData.stateCategoryView
      })
    );
    this.showPopup(options);
  };

  onDeviceClick = options => {
    const { devicesHash, dispatch } = this.props;
    const currentDevice = devicesHash[options.target.serviceData.entity.id] || {};
    const { generalStateCategoryView = {} } = currentDevice;
    dispatch(
      updateCurrentEntityInfo({
        entityType: 'DEVICE',
        name: currentDevice.name,
        regionName: currentDevice.regionName,
        regionId: currentDevice.regionId,
        state: generalStateCategoryView,
        entityId: options.target.serviceData.entity.id,
        deviceProfileId: currentDevice.deviceProfileId,
        deviceCategory: currentDevice.deviceCategory,
        statePolling: currentDevice.statePolling
      })
    );
    dispatch(updateCurrentDevice(currentDevice));
    this.showPopup(options);
  };

  showPopup = options => {
    const {
      target,
      data: { originalEvent }
    } = options;
    if (this.popupEl && target && target.serviceData) {
      this.popupEl.style.display = 'block';
      this.popupEl.style.left = originalEvent.layerX + 'px';
      this.popupEl.style.top = originalEvent.layerY + 'px';
      const boundedPopup = this.popupEl.getBoundingClientRect();
      const xPos = boundedPopup.x;
      const popupWIdth = boundedPopup.width;
      // Проверяем выходит ли popup за пределы экрана, если да, то отображаем его слева от курсора
      if (xPos + popupWIdth > this.screen.width) {
        this.popupEl.style.left = this.popupEl.style.left.split('px')[0] - popupWIdth + 'px';
      }
    }
  };

  hidePopup = () => {
    if (this.popupEl) {
      const { dispatch } = this.props;
      this.popupEl.style.display = 'none';
      dispatch(updateCurrentEntityInfo(null));
    }
  };

  onObjectOver = options => {
    this.highlightObject(options.currentTarget, true);
  };

  onMouseMove = options => {};

  onObjectOut = options => {
    this.highlightObject(options.currentTarget, false);
  };

  getParentDeviceInfo = (childDevice, parentInfo) => {
    const { devicesHash } = this.props;
    let info = parentInfo ? parentInfo : '';
    const parentDevice = childDevice.parentDeviceId
      ? devicesHash[childDevice.parentDeviceId]
      : null;
    if (parentDevice) {
      info += `\n${parentDevice.name} - ${
        parentDevice.generalStateCategoryView.name
      } ${this.getParentDeviceInfo(parentDevice, info)}`;
    }
    return info;
  };

  destroyDeviceTextures = () => {
    const { textures } = this.props;
    if (textures)
      for (let key in textures) {
        if (textures[key]) {
          textures[key].destroy(true);
          delete Loader.resources[textures[key]];
        }
      }
  };

  getBase64FromSvgString = svgString => {
    const domElement = new DOMParser().parseFromString(svgString, 'image/svg+xml');
    const serializedSvg = new XMLSerializer().serializeToString(domElement);
    const base64 = window.btoa(unescape(encodeURIComponent(serializedSvg)));
    return base64;
  };

  /**
   * ANIMATION
   */

  updateBlinkedDevices = () => {
    for (const [deviceId, layouts] of this.blinkedDevices) {
      for (const [layoutNo, renderer] of layouts) {
        const animationStep = this.animationSteps.get(deviceId).get(layoutNo);
        if (animationStep.direction) animationStep.tick += animationStep.speed;
        else animationStep.tick -= animationStep.speed;
        if (animationStep.tick >= animationStep.max) animationStep.direction = 0;
        else if (animationStep.tick <= animationStep.min) animationStep.direction = 1;
        renderer.alpha = animationStep.tick;
      }
    }
  };

  updateRotatedDevices = () => {
    for (const [deviceId, layouts] of this.rotatedDevices) {
      for (const [layoutNo, renderer] of layouts) {
        const animationStep = this.animationSteps.get(deviceId).get(layoutNo);
        if (animationStep.direction) animationStep.tick += animationStep.speed;
        else animationStep.tick -= animationStep.speed;
        renderer.rotation = animationStep.tick;
      }
    }
  };

  updateResizedDevices = () => {
    for (const [deviceId, layouts] of this.resizedDevices) {
      for (const [layoutNo, renderer] of layouts) {
        const animationStep = this.animationSteps.get(deviceId).get(layoutNo);
        if (animationStep.direction) animationStep.tick += animationStep.speed;
        else animationStep.tick -= animationStep.speed;
        if (animationStep.tick >= animationStep.max) animationStep.direction = 0;
        else if (animationStep.tick <= animationStep.min) animationStep.direction = 1;
        renderer.scale.set(animationStep.tick);
      }
    }
  };

  startDeviceAnimations = () => {
    if (this.blinkedDevices.size) this.updateBlinkedDevices();
    if (this.rotatedDevices.size) this.updateRotatedDevices();
    if (this.resizedDevices.size) this.updateResizedDevices();
    if (this.blinkedDevices.size || this.rotatedDevices.size || this.resizedDevices.size)
      this.animationFrameId = requestAnimationFrame(this.startDeviceAnimations);
  };

  removeDeviceAnimation = renderer => {
    const { deviceId, layoutNo, animationType } = renderer.serviceData;
    const listName = ANIMATION.LIST_NAMES[animationType];
    if (this[listName]) {
      if (this[listName].get(deviceId) && this[listName].get(deviceId).get(layoutNo))
        this[listName].get(deviceId).delete(layoutNo);
      if (this.animationSteps.get(deviceId) && this.animationSteps.get(deviceId).get(layoutNo))
        this.animationSteps.get(deviceId).delete(layoutNo);
    }
    renderer.alpha = 1.0;
    renderer.rotation = 0;
    renderer.scale.set(1.0);
    renderer.serviceData.animationType = null;
  };

  addDeviceAnimation = (renderer, media) => {
    const {
      serviceData: { deviceId, layoutNo }
    } = renderer;
    const listName = ANIMATION.LIST_NAMES[media.animationType];
    if (this[listName]) {
      if (!this[listName].get(deviceId)) this[listName].set(deviceId, new Map());
      this[listName].get(deviceId).set(layoutNo, renderer);
      if (!this.animationSteps.get(deviceId)) this.animationSteps.set(deviceId, new Map());
      this.animationSteps.get(deviceId).set(layoutNo, this.getAnimationParams(media));
    }
  };

  getAnimationParams = media => ({
    tick: 1, // Текущее значение
    min: ANIMATION.STEPS[media.animationType].MIN, // Значение, после которого должно поменяться direction
    max: ANIMATION.STEPS[media.animationType].MAX, // Значение, после которого должно поменяться direction
    direction: 0, // Направление анимации (0|1)
    speed: media.animationSpeed // Скорость анимации
      ? ANIMATION.RATIO[media.animationType] / media.animationSpeed
      : ANIMATION.RATIO[media.animationType] / ANIMATION.SPEED.MIN
  });

  destroyDeviceAnimations = () => {
    if (this.animationFrameId) cancelAnimationFrame(this.animationFrameId);
    this.animationFrameId = null;
    this.blinkedDevices.clear();
    this.rotatedDevices.clear();
    this.resizedDevices.clear();
    this.animationSteps.clear();
  };

  /**
   * DRAW BACKGROUNDS
   */

  clearOldBackgroundLayout = () => {
    if (this.oldBackgroundLayout) {
      this.oldBackgroundLayout.destroy({ children: true });
      this.mainLayout.removeChild(this.oldBackgroundLayout);
      this.oldBackgroundLayout = null;
    }
  };

  getBackgroundScale = (plan, background, planZoom) => {
    if (!this.originBackgroundSize[plan.id])
      this.originBackgroundSize[plan.id] = {
        height: background.height,
        width: background.width
      };
    const scaleX = (plan.xSize * planZoom) / this.originBackgroundSize[plan.id].width;
    const scaleY = (plan.ySize * planZoom) / this.originBackgroundSize[plan.id].height;
    const scale = scaleX > scaleY ? scaleY : scaleX;
    return scale;
  };

  drawBackground = props => {
    const { plan, planZoom } = props;
    const { topLeftPoint } = plan.backgrounds[0];
    const zoom = planZoom || this.defaultZoom;
    this.clearOldBackgroundLayout();
    const texture = Loader.resources[plan.id].texture;
    const picture = new Sprite(texture);
    texture.baseTexture.sourceScale = this.getBackgroundScale(plan, picture, zoom);
    const img = new Image();
    img.src = texture.baseTexture.imageUrl;
    texture.baseTexture.loadSource(img);
    picture.setTransform(topLeftPoint.x, topLeftPoint.y);
    if (this.backgroundsLayout) this.backgroundsLayout.addChild(picture);
  };

  drawBackgrounds = (props, forceRerender) => {
    const { plan } = props;
    if (plan && plan.backgrounds && plan.backgrounds.length) {
      if (!this.backgroundsLayout || forceRerender) {
        const { id: planId } = plan;
        if (forceRerender) this.oldBackgroundLayout = this.backgroundsLayout;
        const newBackgroundsLayout = new Container();
        newBackgroundsLayout.parentGroup = this.backgroundsGroup;
        this.mainLayout.addChild(newBackgroundsLayout);
        this.backgroundsLayout = newBackgroundsLayout;
        if (Loader.resources[planId] && Loader.resources[planId].texture)
          this.drawBackground(props);
        else {
          try {
            Loader.add(
              planId,
              'data:image/svg+xml;base64,' +
                this.getBase64FromSvgString(plan.backgrounds[0].svgContent)
            ).load(() => this.drawBackground(props));
          } catch (e) {
            // Если Loader уже запущен
            if (Loader.loading) {
              // Если подложка уже загружен или загружается, то повторяем попытку отрисовки, по завершению работы Loader-а
              if (Loader.resources[planId]) {
                Loader.onComplete.add(() => {
                  this.drawBackgrounds(props, forceRerender);
                });
              }
            }
            // Если иная ошибка, то возвращаем ее
            return new Error(e);
          }
        }
      }
    }
  };

  clearBackgrounds = () => {
    if (this.backgroundsLayout) {
      this.backgroundsLayout.destroy({ children: true });
      this.mainLayout.removeChild(this.backgroundsLayout);
      this.backgroundsLayout = null;
    }
  };

  /**
   * DRAW REGIONS
   */

  drawRegion = renderer => {
    const { shape, stateCategoryView, highlight, isCurrent } = renderer.serviceData;
    renderer
      .beginFill(`0x${stateCategoryView.color.substring(1)}`, stateCategoryView.regionFillAlpha)
      .lineStyle(
        2,
        highlight || isCurrent ? HIGHLIGHT_COLOR : `0x${stateCategoryView.fontColor.substring(1)}`
      )
      .drawShape(shape);
    renderer.endFill();
  };

  getRegionShape = layout => {
    const { planZoom } = this.props;
    const zoom = planZoom || this.defaultZoom;
    if (layout.rect) {
      // Меняем местами, если x0 больше, чем x1
      if (layout.points[0].x > layout.points[1].x) {
        [layout.points[0].x, layout.points[1].x] = [layout.points[1].x, layout.points[0].x];
      }
      // Меняем местами, если y0 больше, чем y1
      if (layout.points[0].y > layout.points[1].y) {
        [layout.points[0].y, layout.points[1].y] = [layout.points[1].y, layout.points[0].y];
      }
      return new Rectangle(
        layout.points[0].x * zoom,
        layout.points[0].y * zoom,
        layout.points[1].x * zoom - layout.points[0].x * zoom,
        layout.points[1].y * zoom - layout.points[0].y * zoom
      );
    }
    return new Polygon([
      ...layout.points.map(point => new Point(point.x * zoom, point.y * zoom)),
      new Point(layout.points[0].x * zoom, layout.points[0].y * zoom)
    ]);
  };

  drawRegions = props => {
    const { plan, regionsHash } = props;
    const regions = Object.values(regionsHash);
    if (plan && regions.length) {
      if (this.regionsLayout) {
        this.regionsLayout.children.forEach(renderer => {
          const { regionId, stateCategoryView } = renderer.serviceData;
          const updatedRegion = regionsHash[regionId];
          const isCurrentEntity = this.isCurrentEntityCheck('REGION', regionId);
          if (
            (updatedRegion &&
              !isEqual(stateCategoryView, updatedRegion.generalStateCategoryView)) ||
            (isCurrentEntity && !renderer.serviceData.isCurrent) ||
            (!isCurrentEntity && renderer.serviceData.isCurrent)
          ) {
            renderer.serviceData.isCurrent = isCurrentEntity;
            renderer.serviceData.stateCategoryView = updatedRegion.generalStateCategoryView;
            renderer.clear();
            this.drawRegion(renderer);
          }
        });
      } else {
        const { currentUser } = props;
        const newRegionsLayout = new Container();
        newRegionsLayout.parentGroup = this.regionsGroup;
        regions.forEach(region => {
          if (availableByFilterTags(currentUser, region))
            region.planLayouts
              .filter(planLayout => planLayout.planId === plan.id)
              .forEach(layout => {
                const renderer = new Graphics();
                renderer.serviceData = {
                  shape: this.getRegionShape(layout),
                  regionId: region.id,
                  stateCategoryView: region.generalStateCategoryView,
                  highlight: false,
                  isCurrent: this.isCurrentEntityCheck('REGION', region.id),
                  entity: region
                };
                renderer.interactive = true;
                renderer.cursor = 'pointer';
                renderer.on('pointerdown', this.onRegionClick);
                this.drawRegion(renderer);
                newRegionsLayout.addChild(renderer);
              });
        });
        this.mainLayout.addChild(newRegionsLayout);
        this.regionsLayout = newRegionsLayout;
      }
    }
  };

  clearRegions = () => {
    if (this.regionsLayout) {
      this.regionsLayout.destroy({ children: true });
      this.mainLayout.removeChild(this.regionsLayout);
      this.regionsLayout = null;
    }
  };

  /**
   * DRAW DEVICES
   */

  getDevicePicture = (device, layout) => {
    const { generalStateCategoryView } = device;
    const { deviceShapesHash, svgsHash } = this.props;
    const deviceShapeLibraryId =
      device.deviceShapeLibraryId || `${device.deviceProfileId}_default_shlib`;
    let textureMedia = null;
    const deviceShapeLibrary =
      deviceShapesHash[deviceShapeLibraryId] ||
      deviceShapesHash[`${device.deviceProfileId}_default_shlib`];
    const deviceShape = deviceShapeLibrary
      ? deviceShapeLibrary[generalStateCategoryView.id] || deviceShapeLibrary['']
      : null;
    if (deviceShape && deviceShape.builtinMediaId && svgsHash[deviceShape.builtinMediaId]) {
      textureMedia = svgsHash[deviceShape.builtinMediaId];
      const { animationSpeed, animationType } = deviceShape;
      textureMedia = { ...textureMedia, animationSpeed, animationType };
    } else if (deviceShape && deviceShape.svgContent) {
      textureMedia = deviceShape;
    }
    if (textureMedia) {
      const { textures, deviceSize, dispatch } = this.props;
      let texture = null;
      if (textures[textureMedia.id]) {
        if (textures[textureMedia.id] && textures[textureMedia.id].valid) {
          texture = textures[textureMedia.id];
        }
      }
      if (!texture) {
        if (textureMedia.svgContent) {
          texture = Texture.fromImage(
            'data:image/svg+xml;base64,' + this.getBase64FromSvgString(textureMedia.svgContent),
            true,
            PIXI.SCALE_MODES.LINEAR,
            deviceSize
          );
        } else if (textureMedia.mediaType === 'SVG_CONTENT') {
          texture = Texture.fromImage(
            'data:image/svg+xml;base64,' + this.getBase64FromSvgString(textureMedia.content),
            true,
            PIXI.SCALE_MODES.LINEAR,
            deviceSize
          );
        }
        if (texture) {
          dispatch(addTexture(textureMedia.id, texture));
        }
      }
      if (texture) {
        const sprite = new Sprite(texture);
        const spritePosition = -(50 * deviceSize) / 2;
        sprite.position.set(spritePosition, spritePosition);
        return { sprite, media: textureMedia };
      }
    }
    /* Если с текстурой не получилось - нарисуем первые 2 буквы из названия  */
    const { deviceSize } = this.props;
    const text = new PIXI.Text(device.name.substring(0, 2), {
      fontFamily: 'Helvetica, Arial, sans-serif',
      fontSize: 24 * deviceSize,
      fill: 0xffffff
    });
    const textPosition = -(50 * deviceSize) / 2 + 12 * deviceSize;
    text.position.set(textPosition - 2, textPosition);
    return { sprite: text };
  };

  drawDevice = renderer => {
    const { deviceSize, devicesHash } = this.props;
    const { stateCategoryView, highlight, isCurrent, entity } = renderer.serviceData;
    // Отображение текущих параметров
    if (devicesHash[entity.id].monitorableValueViews) {
      const tempValue = devicesHash[entity.id].monitorableValueViews.find(
        item => MONITORED_VALUES[item.id]
      );
      if (tempValue) {
        const monValue = new PIXI.Text(
          `${tempValue.value ? parseInt(tempValue.value, 10) : 0} ${tempValue.unit}`,
          {
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontSize: 10,
            fill: 0xffffff
          }
        );
        const currentDeviceSize = (50 * deviceSize) / 2;
        const textSize = monValue.getBounds();
        monValue.position.set(
          -textSize.width / 2,
          currentDeviceSize / 2 + textSize.height * deviceSize + 2
        );
        renderer.addChild(monValue);
      }
    }
    renderer
      .beginFill(`0x${stateCategoryView.color.substring(1)}`)
      .lineStyle(
        2,
        highlight || isCurrent ? HIGHLIGHT_COLOR : `0x${stateCategoryView.fontColor.substring(1)}`
      )
      .drawRect(
        -((50 * deviceSize) / 2),
        -((50 * deviceSize) / 2),
        50 * deviceSize,
        50 * deviceSize
      )
      .endFill();
    renderer.children.forEach(
      sprite => (sprite.tint = `0x${stateCategoryView.fontColor.substring(1)}`)
    );
  };

  updateDeviceTexture = (renderer, updatedDevice, layout) => {
    if (renderer.children && renderer.children.length > 1) {
      renderer.removeChildAt(1);
    }
    renderer.removeChildAt(0);
    const { sprite, media } = this.getDevicePicture(updatedDevice, layout);
    renderer.addChild(sprite);
    if (media && media.animationType && media.animationType !== 'NONE') {
      renderer.serviceData['animationType'] = media.animationType;
      this.addDeviceAnimation(renderer, media);
    }
  };

  drawDevices = props => {
    const { plan, planZoom, deviceSize, devicesHash, deviceShapesHash, svgsHash } = props;
    const devices = Object.values(devicesHash);
    if (plan && devices.length && !isEmpty(deviceShapesHash) && !isEmpty(svgsHash)) {
      if (this.devicesLayout) {
        this.devicesLayout.children.forEach(renderer => {
          const {
            deviceId,
            layout,
            animationType,
            stateCategoryView,
            monitorableValues
          } = renderer.serviceData;
          const updatedDevice = devicesHash[deviceId];
          const isCurrentEntity = this.isCurrentEntityCheck('DEVICE', deviceId);
          if (
            (updatedDevice &&
              (!isEqual(stateCategoryView, updatedDevice.generalStateCategoryView) ||
                !isEqual(monitorableValues, updatedDevice.monitorableValueViews))) ||
            (isCurrentEntity && !renderer.serviceData.isCurrent) ||
            (!isCurrentEntity && renderer.serviceData.isCurrent)
          ) {
            if (animationType) this.removeDeviceAnimation(renderer);
            renderer.serviceData.isCurrent = isCurrentEntity;
            renderer.serviceData.stateCategoryView = updatedDevice.generalStateCategoryView;
            renderer.serviceData.monitorableValues = updatedDevice.monitorableValueViews;
            this.updateDeviceTexture(renderer, updatedDevice, layout);
            renderer.clear();
            this.drawDevice(renderer);
          }
        });
      } else {
        const { currentUser } = props;
        const newDevicesLayout = new Container();
        newDevicesLayout.parentGroup = this.devicesGroup;
        devices.forEach(device => {
          if (availableByFilterTags(currentUser, device))
            if (DRAWABLE_DEVICE_CATEGORY[device.deviceCategory] && device.planLayouts.length) {
              device.planLayouts
                .filter(planLayout => planLayout.planId === plan.id)
                .forEach((layout, layoutNo) => {
                  const renderer = new Graphics();
                  const { sprite, media } = this.getDevicePicture(device, layout);
                  const zoom = planZoom || this.defaultZoom;
                  renderer.x = layout.coordinatePoint.x * zoom + (50 * deviceSize) / 2;
                  renderer.y = layout.coordinatePoint.y * zoom + (50 * deviceSize) / 2;
                  renderer.serviceData = {
                    deviceId: device.id,
                    layoutNo, // Нужен для уникальности отображения устройства
                    stateCategoryView: device.generalStateCategoryView,
                    monitorableValues: device.monitorableValueViews,
                    layout,
                    highlight: false,
                    isCurrent: this.isCurrentEntityCheck('DEVICE', device.id),
                    entity: device
                  };
                  renderer.interactive = true;
                  renderer.cursor = 'pointer';
                  renderer.on('pointerdown', this.onDeviceClick);
                  renderer.addChild(sprite);
                  this.drawDevice(renderer);
                  if (media && media.animationType && media.animationType !== 'NONE') {
                    renderer.serviceData['animationType'] = media.animationType;
                    this.addDeviceAnimation(renderer, media);
                  }
                  newDevicesLayout.addChild(renderer);
                });
            }
        });
        this.mainLayout.addChild(newDevicesLayout);
        this.devicesLayout = newDevicesLayout;
        if (
          (this.blinkedDevices.size || this.rotatedDevices.size || this.resizedDevices.size) &&
          !this.animationFrameId
        )
          this.startDeviceAnimations();
      }
    }
  };

  clearDevices = () => {
    if (this.devicesLayout) {
      this.devicesLayout.destroy({ children: true });
      this.mainLayout.removeChild(this.devicesLayout);
      this.devicesLayout = null;
    }
  };

  getPlanZoom = (canvasId, plan) => {
    const canvas = document.getElementById(canvasId);
    if (canvas) {
      const container = canvas.parentElement;
      const scaleX = container.offsetWidth / (plan.xSize + 12);
      const scaleY = container.offsetHeight / (plan.ySize + 12);
      return scaleX > scaleY ? scaleY : scaleX;
    }
  };

  componentDidMount = () => {
    const { plan, planZoom, canvasId } = this.props;
    this.defaultZoom = this.getPlanZoom(canvasId, plan) || 1.0;
    this.createPixiApp(plan, planZoom || this.defaultZoom);
    this.drawBackgrounds(this.props);
    this.drawRegions(this.props);
    this.drawDevices(this.props);
    this.setState({ isReady: true });
  };

  componentDidUpdate = prevProps => {
    const { isReady } = this.state;
    if (isReady) {
      const { planZoom, planId, plan, deviceSize, canvasId } = this.props;
      let needForceRerender = false;
      if (planId !== prevProps.planId) {
        this.defaultZoom = this.getPlanZoom(canvasId, plan) || 1.0;
        // Обновляем стейт, чтобы перерисовать контейнер для canvas
        this.setState({});
      }
      if (planZoom !== prevProps.planZoom || planId !== prevProps.planId) {
        needForceRerender = true;
        const zoom = planZoom || this.defaultZoom;
        this.pixiApp.view.width = plan.xSize * zoom;
        this.pixiApp.view.height = plan.ySize * zoom;
        this.destroyDeviceAnimations();
        this.clearDevices();
        this.clearRegions();
        if (planId !== prevProps.planId) this.clearBackgrounds();
      }
      if (deviceSize !== prevProps.deviceSize) {
        this.destroyDeviceAnimations();
        this.clearDevices();
        this.destroyDeviceTextures();
      }
      this.drawBackgrounds(this.props, needForceRerender);
      this.drawRegions(this.props);
      this.drawDevices(this.props);
    }
  };

  componentWillUnmount = () => {
    const { dispatch } = this.props;
    this.destroyDeviceAnimations();
    this.clearBackgrounds();
    this.clearRegions();
    this.clearDevices();
    dispatch(clearTextures());
    PIXI.utils.destroyTextureCache();
    Object.keys(Loader.resources).forEach(texture => {
      if (Loader.resources[texture]) delete Loader.resources[texture];
    });
    this.mainLayout.destroy({ children: true });
    this.pixiApp.destroy();
  };

  render() {
    const { plan, planZoom, canvasId, currentEntityInfo } = this.props;
    const zoom = planZoom || this.defaultZoom;
    return (
      <div
        style={{
          display: this.pane
            ? this.pane.offsetWidth < plan.xSize * zoom
              ? 'block'
              : 'flex'
            : 'flex',
          justifyContent: 'center',
          background: 'lightGrey',
          height: 'calc(100% - 72px)',
          overflow: 'auto',
          padding: '3px'
        }}
      >
        <div
          style={{
            height: plan.ySize * zoom + 6,
            width: plan.xSize * zoom + 6,
            padding: '1px',
            border: '2px outset',
            position: 'relative'
          }}
          id={canvasId}
        >
          <PlanViewerPopup
            hidePopup={this.hidePopup}
            entityInfo={currentEntityInfo}
            getNode={node => {
              if (node) this.popupEl = node;
            }}
          />
        </div>
      </div>
    );
  }
}

const getDeviceShapesGroupedByLibraryAndStateCategory = createSelector(
  ({ deviceShapeLibrary: { deviceShapesHash } }) => deviceShapesHash,
  deviceShapesHash =>
    Object.values(deviceShapesHash).reduce((groupedShapes, shape) => {
      if (!groupedShapes[shape.deviceShapeLibraryId]) {
        groupedShapes[shape.deviceShapeLibraryId] = {};
      }
      groupedShapes[shape.deviceShapeLibraryId][shape.stateCategoryId] = shape;
      return groupedShapes;
    }, {})
);

const getCurrentEntityInfo = state => state.widgets.currentEntityInfo;

const mapStateToProps = (state, ownProps) => {
  const { planId } = ownProps;
  const plansHash = getActivePlansHash(state);
  return {
    plan: plansHash[planId],
    deviceProfileViewsHash: getDeviceProfileViewsHash(state),
    deviceShapesHash: getDeviceShapesGroupedByLibraryAndStateCategory(state),
    textures: getTextures(state),
    svgsHash: state.medias.svgsHash,
    regionsHash: getActiveRegionsHash(state),
    devicesHash: getActiveDevicesHash(state) || {},
    currentUser: getCurrentUser(state),
    currentEntityInfo: getCurrentEntityInfo(state)
  };
};

const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanRenderer);
