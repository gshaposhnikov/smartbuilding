import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button } from 'antd';
const ButtonGroup = Button.Group;

import { STATE_ACTION, STATE_NAME, STATE_MAP, STATE } from 'constants_new/states';

import { setDeviceState } from 'actions_new/device';
import { setRegionState } from 'actions_new/region';
import { setApartmentState } from 'actions_new/apartment';

import { getActiveRegionsHash } from 'helpers/activeProject';

class Popup extends React.Component {
  defaultProps = {
    getNode: () => null,
    setEntityState: () => null
  };

  componentDidMount = () => {
    window.addEventListener('click', this.onUserClick);
  };

  componentWillUnmount = () => {
    window.removeEventListener('click', this.onUserClick);
  };

  onUserClick = e => {
    const { hidePopup } = this.props;
    const canvasContainer = document.getElementById('planViewer');
    if (e.target.tagName === 'CANVAS' || !canvasContainer.contains(e.target)) {
      hidePopup();
    }
  };

  setEntityState = (entityState, entityType) => {
    const { entityInfo, dispatch, project } = this.props;
    const type = entityType || entityInfo.entityType;
    if (type) {
      switch (type) {
        case 'DEVICE': {
          dispatch(setDeviceState(project.id, entityInfo.entityId, entityState));
          break;
        }
        case 'REGION': {
          let regionId;
          if (entityType === 'REGION') regionId = entityInfo.entityId;
          else if (regionId) regionId = entityInfo.regionId;
          dispatch(setRegionState(project.id, regionId, entityState));
          break;
        }
        case 'APARTMENT': {
          dispatch(setApartmentState(project.id, entityState));
          break;
        }
        default:
          return;
      }
    }
  };

  renderHeader = () => {
    const { entityInfo } = this.props;
    switch (entityInfo.entityType) {
      case 'DEVICE': {
        let stateName = '';
        if (entityInfo.state) {
          const currentState = STATE_MAP[entityInfo.state.id];
          stateName = STATE_NAME[currentState];
        }
        return [
          <tr key="name">
            <td colSpan="2">
              <b>{entityInfo.name}</b>
            </td>
          </tr>,
          <tr key="info">
            <td>
              <b>Зона</b>: {entityInfo.regionName || '-'}
            </td>
            <td>
              <b>Состояние</b>: {stateName || '-'}
            </td>
          </tr>
        ];
      }
      case 'REGION': {
        return (
          <tr key="name">
            <td colSpan="2">
              <b>{entityInfo.name}</b>
            </td>
          </tr>
        );
      }
      default:
        return null;
    }
  };

  renderSettings = () => {
    return;
  };

  renderActions = () => {
    const {
      entityInfo: { entityType, state, regionId },
      regions,
      apartmentState
    } = this.props;
    if (state) {
      const apartmentAction = STATE_ACTION[apartmentState];
      const deviceState = STATE_MAP[state.id];
      const deviceAction = STATE_ACTION[deviceState];
      let regionState;
      let regionAction;
      if (entityType === 'REGION') {
        regionState = STATE_MAP[state.id];
      } else if (regionId) {
        const region = regions[regionId];
        regionState = STATE_MAP[region.generalStateCategoryView.id];
      }
      regionAction = STATE_ACTION[regionState];
      return (
        <tr className="actions">
          <td colSpan="2">
            <ButtonGroup>
              {entityType === 'DEVICE' && (
                <Button type="primary" onClick={() => this.setEntityState(deviceAction)}>
                  {STATE_NAME[deviceAction]}
                </Button>
              )}
              <Button type="primary" onClick={() => this.setEntityState(regionAction, 'REGION')}>
                {STATE_NAME[regionAction]} все
                <br /> в зоне
              </Button>
              <Button
                type="primary"
                onClick={() => this.setEntityState(apartmentAction, 'APARTMENT')}
              >
                {STATE_NAME[apartmentAction]} все <br />в апартаментах
              </Button>
            </ButtonGroup>
          </td>
        </tr>
      );
    }
    return null;
  };

  render = () => {
    const { getNode, className, entityInfo } = this.props;
    return (
      <div className={className} ref={getNode}>
        {entityInfo && (
          <table>
            <tbody>
              {this.renderHeader()}
              {this.renderSettings()}
              {this.renderActions()}
            </tbody>
          </table>
        )}
      </div>
    );
  };
}

const PopupWrapper = styled(Popup)`
  position: absolute;
  display: none;
  background: white;
  z-index: 9999;
  border-radius: 4px;
  border: 2px solid #0e77ca;
  font-family: 'Helvetica, Arial, sans-serif';
  font-size: 14px;
  color: black;
  tr:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
  td {
    padding: 4px;
    white-space: nowrap;
  }
  .actions {
    td {
      padding: 0;
    }
    .ant-btn {
      border-radius: 0;
      height: auto;
      padding-top: 2px;
      padding-bottom: 2px;
      flex: 1;
    }
    .ant-btn-group {
      display: flex;
    }
  }
`;

const mapStateToProps = state => {
  const regions = getActiveRegionsHash(state);
  let apartmentState = STATE.OFF;
  const hasActiveRegion =
    Object.values(regions || {}).findIndex(
      region => STATE_MAP[region.generalStateCategoryView.id] === STATE.ON
    ) > -1;
  if (hasActiveRegion) {
    apartmentState = STATE.ON;
  }
  return {
    regions,
    apartmentState,
    project: state.activeProject.project
  };
};

export default connect(mapStateToProps)(PopupWrapper);
