import React from 'react';
import { connect } from 'react-redux';

import { getActivePlansHash } from 'helpers/activeProject';

const PlanTabTitle = ({ plan, stateCategory }) => {
  return (
    <div
      style={{
        padding: '2px 4px',
        borderRadius: '7%',
        backgroundColor: stateCategory ? stateCategory.color : null,
        color: stateCategory ? stateCategory.fontColor : null
      }}
      title={plan.description || plan.name}
    >
      {plan.name}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { planId } = ownProps;
  const plansHash = getActivePlansHash(state);
  return {
    plan: plansHash && plansHash[planId] ? plansHash[planId] : {}
  };
};

export default connect(mapStateToProps)(PlanTabTitle);
