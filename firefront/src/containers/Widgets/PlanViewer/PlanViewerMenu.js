import React from 'react';
import { Tooltip, Select } from 'antd';
import { connect } from 'react-redux';

import Button from 'components/Button';

import { updateCurrentEntityInfo } from 'actions/widgets';

const { Option } = Select,
  WRAPPER_STYLES = {
    display: 'flex',
    alignItems: 'center'
  },
  BUTTON_HEIGHT = 32,
  ICON_SIZE = 22,
  DEVICES_ZOOM_PARAMS = {
    key: 'devicesZoom',
    options: [
      { value: 0.2, text: 'Маленький' },
      { value: 0.4, text: 'Средний' },
      { value: 0.6, text: 'Большой' }
    ]
  },
  SELECT_STYLES = { padding: '0 5px', width: '102px', height: 30 },
  ICON_COLOR = '#5FA2DD',
  CUSTOM_ICON_STYLES = { color: ICON_COLOR };

class PlanViewerMenu extends React.Component {
  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(updateCurrentEntityInfo({}));
  }

  render = () => {
    const {
      priorityMode,
      onChangePriorityMode,
      deviceSize,
      onChangeDeviceSize,
      disabledZoomIn,
      disabledZoomOut,
      onPlanZoomIn,
      onPlanZoomOut,
      onPlanZoomFit
    } = this.props;
    return (
      <div style={WRAPPER_STYLES}>
        <Tooltip title="Увеличить масштаб" placement="topRight">
          <Button
            height={BUTTON_HEIGHT}
            icon="zoomIn"
            customIcon={true}
            iconStyles={CUSTOM_ICON_STYLES}
            onClick={onPlanZoomIn}
            isActive={!disabledZoomIn}
          />
        </Tooltip>
        <Tooltip title="Уменьшить масштаб" placement="topRight">
          <Button
            height={BUTTON_HEIGHT}
            icon="zoomOut"
            customIcon={true}
            iconStyles={CUSTOM_ICON_STYLES}
            onClick={onPlanZoomOut}
            isActive={!disabledZoomOut}
          />
        </Tooltip>
        <Tooltip title="Под размер окна" placement="topRight">
          <Button
            height={BUTTON_HEIGHT}
            icon="scan"
            fontColor={ICON_COLOR}
            fontWeight="bold"
            iconSize={ICON_SIZE}
            onClick={onPlanZoomFit}
          />
        </Tooltip>
        <Select
          style={SELECT_STYLES}
          defaultValue={`${deviceSize || ''}`}
          value={`${deviceSize || ''}`}
          onChange={onChangeDeviceSize}
        >
          {DEVICES_ZOOM_PARAMS.options.map((option, index) => (
            <Option key={index} value={`${option.value}`}>
              {option.text || option.value}
            </Option>
          ))}
        </Select>
        <Tooltip title="Активация плана с наиболее тревожным состоянием" placement="topLeft">
          <Button
            height={BUTTON_HEIGHT}
            icon="safety"
            fontColor={priorityMode ? 'green' : 'red'}
            fontWeight="bold"
            iconSize={ICON_SIZE}
            onClick={onChangePriorityMode}
          />
        </Tooltip>
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    project: state.activeProject.project
  };
};
const mapDispatchToProps = dispatch => ({
  dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanViewerMenu);
