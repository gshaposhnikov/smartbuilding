import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
/** Подписка на изменение сущности. Если сущность обновилась, то вызывает callback-функцию обновленения планов */
class EntityConnector extends React.Component {
  componentWillReceiveProps = nextProps => {
    const { entity } = this.props;
    if (entity && entity !== nextProps.entity) {
      const { updateStatesByEntity } = nextProps;
      updateStatesByEntity(entity);
    }
  };
  render = () => null;
}

const getEntityId = (state, { entityId }) => entityId;
const getEntitiesHash = (state, { getEntitiesHashFunc }) => getEntitiesHashFunc(state);

const mapStateToProps = createSelector(
  getEntitiesHash,
  getEntityId,
  (entitiesHash, entityId) => ({ entity: entitiesHash ? entitiesHash[entityId] : null })
);

export default connect(mapStateToProps)(EntityConnector);
