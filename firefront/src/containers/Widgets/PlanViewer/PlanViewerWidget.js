import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Tabs, Alert } from 'antd';
import styled from 'styled-components';
const TabPane = Tabs.TabPane;

import PlanTabTitle from './PlanTabTitle';
import PlanRenderer from './PlanRenderer';
import PlanViewerMenu from './PlanViewerMenu';
import EntityConnector from './EntityConnector';

import {
  getActivePlans,
  getActivePlanGroups,
  getActivePlansHash,
  getActiveDevicesHash,
  getActiveRegionsHash
} from 'helpers/activeProject';
import { getCurrentUser } from 'helpers/user';
import { filterEntities } from 'helpers/filtration';

const GroupedPlansWrapper = styled.div`
  height: 100%;
`;
const TABS_WRAPPER_STYLES = { background: 'white', height: '100%' };
const PRIORITY_MODE_VIEWER_DELAY = 5000;
const STORAGE_NAMES = {
  PLAN_ZOOM: 'planZoom',
  DEVICE_SIZE: 'deviceSize',
  PRIORITY_MODE: 'priorityMode'
};

class PlanViewerWidget extends Component {
  static propTypes = {
    /* Из redux-store*/
    project: PropTypes.object, // Активный проект
    plans: PropTypes.array, // Список планов активного проект
    plansHash: PropTypes.object, // Хеш планов активного проектов
    planGroups: PropTypes.array, // Список групп планов активного проекта
    currentUser: PropTypes.object, // Текущий пользователь
    devicesHash: PropTypes.object, // Хеш устройств активного проекта
    regionsHash: PropTypes.object, // Хеш зон активного проект
    /* Из родительского компонента */
    canvasId: PropTypes.string // Идентификатор canvas-контейнер для рендера планов
  };

  constructor(props) {
    super(props);
    this.state = {
      panes: [],
      activePlanGroupId: null,
      activePlanId: null,
      priorityMode: localStorage.getItem(STORAGE_NAMES.PRIORITY_MODE)
        ? localStorage.getItem(STORAGE_NAMES.PRIORITY_MODE) === 'true'
        : false,
      deviceSize: localStorage.getItem(STORAGE_NAMES.DEVICE_SIZE)
        ? Number.parseFloat(localStorage.getItem(STORAGE_NAMES.DEVICE_SIZE))
        : 0.4,
      planZoom: localStorage.getItem(STORAGE_NAMES.PLAN_ZOOM)
        ? JSON.parse(localStorage.getItem(STORAGE_NAMES.PLAN_ZOOM))
        : {},
      plansDeviceIds: [],
      plansRegionIds: [],
      entitiesIdsGroupedByPlanId: {},
      connectorsInitialized: false
    };
  }
  // Получение сущности плана
  getSubPane = plan => ({ key: plan.id });
  // Инициализация планов
  setPanes = (planGroups = [], plans = []) => {
    let { activePlanGroupId, activePlanId } = this.state;
    let panes = [];
    if (plans.length) {
      const allPlans = {
        key: 'allPlans',
        name: 'Все планы',
        description: 'Все планы помещений без группировки',
        subPanes: plans.map(this.getSubPane)
      };
      panes.push(allPlans);
      planGroups.forEach(planGroup => {
        panes.push({
          key: planGroup.id,
          name: planGroup.name,
          description: planGroup.description || planGroup.name,
          subPanes: plans.filter(plan => plan.planGroupId === planGroup.id).map(this.getSubPane)
        });
      });
      panes = panes.filter(pane => pane.subPanes && pane.subPanes.length);
      const activePlanGroup = panes[0];
      activePlanGroupId = activePlanGroup.key;
      const activePlan = activePlanGroup.subPanes[0];
      activePlanId = activePlan.key;
      this.setState({ activePlanGroupId, activePlanId });
    } else this.setState({ activePlanGroupId: null, activePlanId: null });
    this.setState({ panes });
  };
  // Смена активной группы
  setActivePlanGroup = planGroupId => {
    const { activePlanGroupId, panes } = this.state;
    if (activePlanGroupId !== planGroupId) {
      const planId = panes.find(pane => pane.key === planGroupId).subPanes[0].key;
      this.setState({ activePlanGroupId: planGroupId, activePlanId: planId });
    }
  };
  // Смена активного плана
  setActivePlan = planId => {
    const { activePlanId } = this.state;
    if (activePlanId !== planId) {
      this.setState({ activePlanId: planId });
    }
  };
  // Вкл./выкл. режима автоматического переключения на план с наиболее тревожным состоянием
  togglePriorityMode = () => {
    let { priorityMode } = this.state;
    priorityMode = !priorityMode;
    localStorage.setItem(STORAGE_NAMES.PRIORITY_MODE, priorityMode);
    if (priorityMode) this.startPriorityModeViewer();
    else this.stopPriorityModeViewer();
    this.setState({ priorityMode });
  };
  // Сделать активным план с наиболее тревожным состоянием
  setActivePlanWithMaxPriority = () => {
    const { entitiesIdsGroupedByPlanId } = this.state;
    const planIds = Object.keys(entitiesIdsGroupedByPlanId);
    if (planIds.length) {
      const { activePlanId, activePlanGroupId } = this.state;
      let maxPriority, planIdWithMaxPriority;
      planIds.forEach(planId => {
        const planGeneralStateCategoryView =
          entitiesIdsGroupedByPlanId[planId].generalStateCategoryView;
        if (
          planGeneralStateCategoryView &&
          (isNaN(maxPriority) ||
            planGeneralStateCategoryView.stateCategory.priority < maxPriority ||
            !planIdWithMaxPriority)
        ) {
          maxPriority = planGeneralStateCategoryView.stateCategory.priority;
          planIdWithMaxPriority = planId;
        }
      });
      if (planIdWithMaxPriority && activePlanId !== planIdWithMaxPriority) {
        const activePlanGeneralStateCategoryView =
          entitiesIdsGroupedByPlanId[activePlanId].generalStateCategoryView;
        if (
          activePlanGeneralStateCategoryView.stateCategory &&
          activePlanGeneralStateCategoryView.stateCategory.priority !== maxPriority
        ) {
          const { plansHash } = this.props;
          const newActivePlanGroupId = plansHash[planIdWithMaxPriority].planGroupId || 'allPlans';
          this.setState({
            activePlanGroupId:
              activePlanGroupId === 'allPlans' ? activePlanGroupId : newActivePlanGroupId,
            activePlanId: planIdWithMaxPriority
          });
        }
      }
    }
  };
  // Включение режима автоматического переключения на план с наиболее тревожным состоянием
  startPriorityModeViewer = () => {
    this.setActivePlanWithMaxPriority();
    this.priorityModeViwer = setInterval(
      this.setActivePlanWithMaxPriority,
      PRIORITY_MODE_VIEWER_DELAY
    );
  };
  // Выключение режима автоматического переключения на план с наиболее тревожным состоянием
  stopPriorityModeViewer = () => {
    clearInterval(this.priorityModeViwer);
  };
  // Изменить размер устройств на планах
  changeDeviceSize = value => {
    const deviceSize = Number.parseFloat(value);
    localStorage.setItem(STORAGE_NAMES.DEVICE_SIZE, deviceSize);
    this.setState({ deviceSize });
  };
  // Масштабирование: под размер окна
  zoomFit = () => {
    const { activePlanId: planId, planZoom } = this.state;
    const {
      canvasId,
      plansHash: { [planId]: plan }
    } = this.props;
    if (plan) {
      if (!planZoom[planId]) planZoom[planId] = 1.0;
      const canvasContainer = document.getElementById(canvasId).parentElement;
      const scaleX = canvasContainer.offsetWidth / (plan.xSize + 12);
      const scaleY = canvasContainer.offsetHeight / (plan.ySize + 12);
      planZoom[planId] = scaleX > scaleY ? scaleY : scaleX;
      localStorage.setItem(STORAGE_NAMES.PLAN_ZOOM, JSON.stringify(planZoom));
      this.setState({ planZoom });
    }
  };
  // Масштабирование: увеличить
  zoomIn = () => {
    const { activePlanId: planId, planZoom } = this.state;
    if (!planZoom[planId]) planZoom[planId] = 1.0;
    planZoom[planId] += 0.2;
    planZoom[planId] = Number.parseFloat(planZoom[planId].toFixed(1));
    localStorage.setItem(STORAGE_NAMES.PLAN_ZOOM, JSON.stringify(planZoom));
    this.setState({ planZoom });
  };
  // Масштабирование: уменьшить
  zoomOut = () => {
    const { activePlanId: planId, planZoom } = this.state;
    if (!planZoom[planId]) planZoom[planId] = 1.0;
    planZoom[planId] -= 0.2;
    planZoom[planId] = Number.parseFloat(planZoom[planId].toFixed(1));
    localStorage.setItem(STORAGE_NAMES.PLAN_ZOOM, JSON.stringify(planZoom));
    this.setState({ planZoom });
  };
  /**
   * Сбор и запись в state идентификаторов сущностей для коннекторов
   * @param {Object} devicesHash
   * @param {Object} regionsHash
   * @param {Object} currentUser
   */
  initConnectors = (devicesHash, regionsHash, currentUser) => {
    // Получаем отфильтрованные списки устройств и зон
    const [devices, regions] = filterEntities(
      currentUser,
      Object.values(devicesHash).filter(({ planLayouts }) => planLayouts && planLayouts.length),
      Object.values(regionsHash).filter(({ planLayouts }) => planLayouts && planLayouts.length)
    );
    const plansDeviceIds = [];
    const plansRegionIds = [];
    const entitiesIdsGroupedByPlanId = {};
    // Собираем id устройств
    devices.forEach(({ id, planLayouts }) => {
      plansDeviceIds.push(id);
      planLayouts.forEach(({ planId }) => {
        if (!entitiesIdsGroupedByPlanId[planId])
          entitiesIdsGroupedByPlanId[planId] = {
            deviceIds: [],
            regionIds: [],
            generalStateCategoryView: null
          };
        entitiesIdsGroupedByPlanId[planId].deviceIds.push(id);
      });
    });
    // Собираем id зон
    regions.forEach(({ id, planLayouts }) => {
      plansRegionIds.push(id);
      planLayouts.forEach(({ planId }) => {
        if (!entitiesIdsGroupedByPlanId[planId])
          entitiesIdsGroupedByPlanId[planId] = {
            deviceIds: [],
            regionIds: [],
            generalStateCategoryView: null
          };
        entitiesIdsGroupedByPlanId[planId].regionIds.push(id);
      });
    });
    this.setState(
      {
        plansDeviceIds,
        plansRegionIds,
        entitiesIdsGroupedByPlanId,
        connectorsInitialized: true
      },
      () => {
        this.updatePlanStates(Object.keys(entitiesIdsGroupedByPlanId));
      }
    );
  };
  // Обновление классов состояний для планов
  updatePlanStates = planIds => {
    const { devicesHash, regionsHash } = this.props;
    const { entitiesIdsGroupedByPlanId } = this.state;
    planIds.forEach(planId => {
      const devices = entitiesIdsGroupedByPlanId[planId].deviceIds.map(
        deviceId => devicesHash[deviceId]
      );
      const regions = entitiesIdsGroupedByPlanId[planId].regionIds.map(
        regionId => regionsHash[regionId]
      );
      // Ищем класс состояния среди устройств с максимальным приоритетом
      const maxStateCategoryByDevices = this.findStateCategoryWithMaxPriority(devices);
      // Ищем класс состояния среди зон с максимальным приоритетом
      const maxStateCategoryByRegions = this.findStateCategoryWithMaxPriority(regions);
      // Выставляем класс состояния с максимальным приоритетом
      if (maxStateCategoryByDevices && maxStateCategoryByRegions) {
        entitiesIdsGroupedByPlanId[planId].generalStateCategoryView =
          maxStateCategoryByDevices.stateCategory.priority >=
          maxStateCategoryByRegions.stateCategory.priority
            ? maxStateCategoryByRegions
            : maxStateCategoryByDevices;
      } else if (maxStateCategoryByDevices) {
        entitiesIdsGroupedByPlanId[planId].generalStateCategoryView = maxStateCategoryByDevices;
      } else if (maxStateCategoryByRegions) {
        entitiesIdsGroupedByPlanId[planId].generalStateCategoryView = maxStateCategoryByRegions;
      } else {
        entitiesIdsGroupedByPlanId[planId].generalStateCategoryView = null;
      }
    });
    this.setState({ entitiesIdsGroupedByPlanId });
  };

  // Обновление классов состояний для планов на которой размещена сущность
  updateStatesByEntity = item => {
    const planIds = Object.keys(
      item.planLayouts.reduce((planIdsHash, planLayout) => {
        planIdsHash[planLayout.planId] = true;
        return planIdsHash;
      }, {})
    );
    this.updatePlanStates(planIds);
  };
  // Поиск класса состояния с максимальным приоритетом в списке
  findStateCategoryWithMaxPriority = entities => {
    let maxPriority, stateCategoryWithMaxPriority;
    entities.forEach(entity => {
      if (
        isNaN(maxPriority) ||
        entity.generalStateCategoryView.stateCategory.priority < maxPriority ||
        !stateCategoryWithMaxPriority
      ) {
        maxPriority = entity.generalStateCategoryView.stateCategory.priority;
        stateCategoryWithMaxPriority = entity.generalStateCategoryView;
      }
    });
    return stateCategoryWithMaxPriority;
  };

  componentDidMount = () => {
    const { planGroups, plans, currentUser, devicesHash, regionsHash } = this.props;
    if (planGroups.length || plans.length)
      this.setPanes(planGroups, filterEntities(currentUser, plans));
    if (devicesHash && regionsHash) this.initConnectors(devicesHash, regionsHash, currentUser);
  };

  componentWillReceiveProps = nextProps => {
    const { planGroups: prevPlanGroups, plans: prevPlans } = this.props;
    const { planGroups, plans, currentUser, devicesHash, regionsHash } = nextProps;
    const { connectorsInitialized } = this.state;
    if (
      (!prevPlanGroups.length && planGroups.length) ||
      (prevPlanGroups.length && !planGroups.length) ||
      (!prevPlans.length && plans.length) ||
      (prevPlans.length && !plans.length)
    ) {
      this.setPanes(planGroups, filterEntities(currentUser, plans));
    }
    if (!connectorsInitialized) {
      if (devicesHash && regionsHash) this.initConnectors(devicesHash, regionsHash, currentUser);
    }
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    if (this.state !== nextState) return true;
    return false;
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { connectorsInitialized, priorityMode } = this.state;
    if (connectorsInitialized && !prevState.connectorsInitialized) {
      if (priorityMode) this.startPriorityModeViewer();
    }
  };

  componentWillUnmount = () => {
    this.stopPriorityModeViewer();
  };

  render() {
    const {
      panes,
      activePlanGroupId,
      activePlanId,
      priorityMode,
      deviceSize,
      planZoom,
      plansDeviceIds,
      plansRegionIds,
      entitiesIdsGroupedByPlanId
    } = this.state;
    const { canvasId } = this.props;
    return panes.length ? (
      <GroupedPlansWrapper>
        <div style={TABS_WRAPPER_STYLES}>
          <Tabs
            type="card"
            activeKey={activePlanGroupId}
            onTabClick={this.setActivePlanGroup}
            tabBarExtraContent={
              <PlanViewerMenu
                priorityMode={priorityMode}
                onChangePriorityMode={this.togglePriorityMode}
                deviceSize={deviceSize}
                onChangeDeviceSize={this.changeDeviceSize}
                onPlanZoomFit={this.zoomFit}
                onPlanZoomIn={this.zoomIn}
                onPlanZoomOut={this.zoomOut}
                disabledZoomIn={
                  planZoom[activePlanId] && planZoom[activePlanId] >= 3.0 ? true : false
                }
                disabledZoomOut={
                  planZoom[activePlanId] && planZoom[activePlanId] <= 0.2 ? true : false
                }
              />
            }
          >
            {panes.map(pane => (
              <TabPane
                key={pane.key}
                tab={<div title={pane.description || pane.name}>{pane.name}</div>}
              >
                <Tabs activeKey={activePlanId} onTabClick={this.setActivePlan}>
                  {pane.subPanes.map(subPane => (
                    <TabPane
                      key={subPane.key}
                      tab={
                        <PlanTabTitle
                          planId={subPane.key}
                          stateCategory={
                            entitiesIdsGroupedByPlanId[subPane.key]
                              ? entitiesIdsGroupedByPlanId[subPane.key].generalStateCategoryView
                              : null
                          }
                        />
                      }
                    />
                  ))}
                </Tabs>
              </TabPane>
            ))}
          </Tabs>
          {activePlanId ? (
            <PlanRenderer
              canvasId={canvasId}
              planId={activePlanId}
              deviceSize={deviceSize}
              planZoom={planZoom[activePlanId]}
            />
          ) : null}
        </div>
        {/* TODO: Вынести коннекторы в PlanRenderer, и добавить callback для обновления отображения своей сущности на плане*/}
        {plansDeviceIds.map(deviceId => (
          <EntityConnector
            getEntitiesHashFunc={getActiveDevicesHash}
            key={deviceId}
            entityId={deviceId}
            updateStatesByEntity={this.updateStatesByEntity}
          />
        ))}
        {plansRegionIds.map(regionId => (
          <EntityConnector
            getEntitiesHashFunc={getActiveRegionsHash}
            key={regionId}
            entityId={regionId}
            updateStatesByEntity={this.updateStatesByEntity}
          />
        ))}
      </GroupedPlansWrapper>
    ) : (
      <Alert message="Планы помещений отсутствуют" type="warning" />
    );
  }
}

PlanViewerWidget.defaultProps = {
  canvasId: `planViewer`
};

const mapStateToProps = state => ({
  project: state.activeProject.project,
  plans: getActivePlans(state),
  plansHash: getActivePlansHash(state),
  planGroups: getActivePlanGroups(state),
  currentUser: getCurrentUser(state),
  devicesHash: getActiveDevicesHash(state),
  regionsHash: getActiveRegionsHash(state)
});

export default connect(mapStateToProps)(PlanViewerWidget);
