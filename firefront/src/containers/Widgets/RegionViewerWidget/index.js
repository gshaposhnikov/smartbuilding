import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { createSelector } from 'reselect';

import { regionViewerUpdateCurrent } from 'actions/widgets';
import { changeRegionGuardStatus } from 'actions/regions';
import { setDevicePollingState } from 'actions/devices';
import { setArchiveEventsFilters } from 'actions/events';

import ContextMenuTrigger from 'react-contextmenu/modules/ContextMenuTrigger';
import ContextMenu from 'react-contextmenu/modules/ContextMenu';
import MenuItem from 'react-contextmenu/modules/MenuItem';

import Text from 'components/Text';

import {
  getActiveProjectId,
  getActiveDeviceListWithRegionId,
  getActiveRegions
} from 'helpers/activeProject';
import { getViewMode, getCurrentRegion } from 'helpers/widgets';
import { getCurrentUser } from 'helpers/user';
import { filterEntities } from 'helpers/filtration';

const Wrapper = styled.div`
  height: 77px;
  background: #f4f4f4;
`;

const Regions = styled.div`
  background: white;
  display: flex;
  overflow-y: auto;
  height: 50px;
  flex-wrap: wrap;
`;
const Region = styled.div`
  height: 20px;
  width: 40px;
  background: ${p => (p.color ? p.color : 'black')};
  display: flex;
  align-items: center;
  color: ${p => (p.fontColor ? p.fontColor : 'white')};
  font-weight: ${p => (p.active ? 'bold' : 'normal')};
  justify-content: center;
  border: ${p => (p.active ? '2px solid black' : '1px solid white')};
  cursor: pointer;
`;
const CurrentRegion = styled.div`
  padding: 0px 5px;
`;
const RegionWithContext = styled.div`
  .react-contextmenu {
    background-color: white;
    box-shadow: 0 1px 6px black;
    min-width: 150px;
    padding: 7px;
    color: #49a9ee;
    border-radius: 4px;
    z-index: 2000;
  }
  .react-contextmenu-item {
    padding: 4px;
  }
  .react-contextmenu-item:hover {
    background-color: rgba(0, 255, 255, 0.1);
    cursor: pointer;
  }
  .react-contextmenu-item:not(:first-child) {
    border-top: 1px solid lightgray;
  }
`;

class RegionViewerWidget extends Component {
  state = {
    hasDisabledDevices: {}
  };

  componentWillReceiveProps = nextProps => {
    const { devices } = nextProps;
    if (devices) {
      const hasDisabledDevices = {};
      devices.forEach(device => {
        if (!device.statePolling) {
          hasDisabledDevices[device.regionId] = true;
        }
      });
      this.setState({ hasDisabledDevices });
    }
  };

  updateCurrentRegion = currentRegion => {
    this.props.dispatch(regionViewerUpdateCurrent(currentRegion));
  };

  changeGuardStatus = (e, region) => {
    this.props.dispatch(changeRegionGuardStatus(this.props.projectId, region.id, !region.onGuard));
  };

  disableAllSensor = (e, data) => {
    const { region, state } = data;
    this.props.dispatch(setDevicePollingState(this.props.projectId, null, region.id, state));
  };

  showEvents = (e, region) => {
    const { dispatch } = this.props;
    dispatch(setArchiveEventsFilters([{ id: 'regionIds', value: [region.id] }]));
    dispatch(push('/op/log'));
  };

  componentWillUnmount = () => {
    this.props.dispatch(regionViewerUpdateCurrent({}));
  };

  render = () => {
    const { viewMode, regions, currentRegion } = this.props;
    const { hasDisabledDevices } = this.state;

    if (viewMode === 'regions') {
      return (
        <Wrapper>
          <Regions>
            {regions ? (
              regions.map(region => {
                const { color, fontColor } = region.generalStateCategoryView;
                return (
                  <RegionWithContext key={region.id}>
                    <ContextMenuTrigger id={region.id} holdToDisplay={1000}>
                      <Region
                        color={color}
                        fontColor={fontColor}
                        active={currentRegion.index === region.index ? true : false}
                        onClick={() => this.updateCurrentRegion(region)}
                      >
                        {region.index}
                      </Region>
                    </ContextMenuTrigger>
                    <ContextMenu id={region.id} class="regionContextMenu">
                      {region.subsystem === 'SECURITY' ? (
                        <MenuItem onClick={this.changeGuardStatus} data={region}>
                          {region.onGuard === true ? 'Снять с охраны' : 'Поставить на охрану'}
                        </MenuItem>
                      ) : null}
                      <MenuItem
                        onClick={this.disableAllSensor}
                        data={{ region, state: hasDisabledDevices[region.id] }}
                      >
                        {hasDisabledDevices[region.id]
                          ? 'Включить все датчики зоны'
                          : 'Отключить все датчики зоны'}
                      </MenuItem>
                      <MenuItem onClick={this.showEvents} data={region}>
                        {'Показать события в архиве'}
                      </MenuItem>
                    </ContextMenu>
                  </RegionWithContext>
                );
              })
            ) : (
              <Text>Зон не найдено</Text>
            )}
          </Regions>

          <CurrentRegion>{currentRegion.name ? currentRegion.name : null}</CurrentRegion>
        </Wrapper>
      );
    } else return null;
  };
}

const getFilteredRegions = createSelector(
  getCurrentUser,
  getActiveRegions,
  filterEntities
);

const mapStateToProps = state => {
  return {
    // TODO: Добавить прокидывание viewMode и currentRegion
    projectId: getActiveProjectId(state),
    viewMode: getViewMode(state),
    currentRegion: getCurrentRegion(state),
    devices: getActiveDeviceListWithRegionId(state),
    regions: getFilteredRegions(state)
  };
};

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegionViewerWidget);
