import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isEmpty, isEqual } from 'lodash';

import Form, { FormItem } from 'components/Form';
import InputNumber from 'components/Form/InputNumber';
import Input from 'components/Form/Input';
import Select from 'containers/Selects';
import DeviceSelect from 'containers/Selects/DeviceSelect';
import WidgetParamsEditorMenu from 'containers/Menus/WidgetParamsEditorMenu';
import { findDevicesByDeviceCategory } from 'helpers/device';
import { getSelectedVirtualState } from 'helpers/virtualStates';

import { createVirtualState, updateVirtualState } from 'actions/virtualStates';

import { Field, reduxForm, initialize, reset, getFormValues, getFormSyncErrors } from 'redux-form';
import {
  checkCurrentProjectIsActive,
  getCurrentProjectDeviceList,
  getCurrentProject,
  getCurrentProjectVirtualStates
} from 'helpers/currentProject';

export const FORM_NAME = 'virtualStateParamsEditor';

/**
 * Редактор параметров виртуальных состояний.
 */
class VirtualStateParamsEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedVirtualState: null,
      errorMessage: null
    };
  }

  /**
   * Получить новый свободный номер для виртуального состояния.
   *
   * @param projectVirtualStates - список вирт. состояний
   * @return свободный номер
   */
  getNewGlobalNo(projectVirtualStates) {
    let lastGlobalNo = 0;
    if (projectVirtualStates && projectVirtualStates.length) {
      lastGlobalNo = projectVirtualStates[projectVirtualStates.length - 1].globalNo;
    }
    return lastGlobalNo + 1;
  }

  /**
   * Получить класс состояния по умолчанию.
   *
   * @param stateCategories - список классов состояний
   * @return класс состояния, или null, если нет
   */
  getDefaultStateCategory(stateCategories) {
    if (!isEmpty(stateCategories))
      return stateCategories.find(record => {
        return record.stateCategory.defaultCategory;
      });
    else return null;
  }

  fillAndGetVirtualState(props, controlDevices, selectedVirtualState) {
    const {
      currentProject,
      selectedVirtualStateId,
      projectVirtualStates,
      stateCategories,
      dispatch
    } = props;

    let newVirtualState = getSelectedVirtualState(
      currentProject,
      selectedVirtualStateId,
      projectVirtualStates
    );
    if (!isEqual(selectedVirtualState, newVirtualState)) {
      if (!newVirtualState && selectedVirtualStateId) {
        const newGlobalNo = this.getNewGlobalNo(projectVirtualStates);
        const defaultStateCategory = this.getDefaultStateCategory(stateCategories);
        newVirtualState = {
          globalNo: newGlobalNo,
          name: 'Состояние ' + newGlobalNo,
          stateCategoryId: defaultStateCategory ? defaultStateCategory.id : null,
          messageOn: 'Состояние ' + newGlobalNo + ' уст.',
          messageOff: 'Состояние ' + newGlobalNo + ' снято',
          deviceId: isEmpty(controlDevices) ? null : controlDevices[0].id
        };
        if (!isEqual(selectedVirtualState, newVirtualState)) {
          dispatch(reset(FORM_NAME));
          dispatch(initialize(FORM_NAME, newVirtualState));
        }
      } else {
        dispatch(initialize(FORM_NAME, newVirtualState));
      }
    }
    return newVirtualState;
  }

  componentDidMount() {
    const { controlDevices, selectedVirtualStateId } = this.props;

    let virtualState = null;
    if (selectedVirtualStateId) {
      const { selectedVirtualState } = this.state;
      virtualState = this.fillAndGetVirtualState(this.props, controlDevices, selectedVirtualState);
    }

    this.propsToState(this.props, virtualState);
  }

  componentWillReceiveProps(newProps) {
    const { selectedVirtualState } = this.state;
    const { controlDevices } = this.props;
    const newVirtualState = this.fillAndGetVirtualState(
      newProps,
      controlDevices,
      selectedVirtualState
    );
    this.propsToState(newProps, newVirtualState);
  }

  propsToState(props, selectedVirtualState) {
    const { loadCurrentProjectError, loadStateCategoryError, loadVirtualStatesError } = props;

    if (loadCurrentProjectError) {
      this.setState({
        errorMessage: `Ошибка загрузки проекта: ${loadCurrentProjectError}`
      });
    } else if (loadStateCategoryError) {
      this.setState({
        errorMessage: `Ошибка загрузки списка категорий состояний: ${loadStateCategoryError}`
      });
    } else if (loadVirtualStatesError) {
      this.setState({
        errorMessage: `Ошибка загрузки виртуальных состояний проекта: ${loadVirtualStatesError}`
      });
    } else {
      this.setState({ selectedVirtualState });
    }
  }

  onVirtualStateSave(values) {
    const { currentProject, dispatch, submit, virtualStateValues } = this.props;
    const { selectedVirtualState } = this.state;
    submit();
    if (!selectedVirtualState.id) {
      dispatch(createVirtualState(currentProject.id, virtualStateValues));
    } else {
      dispatch(updateVirtualState(currentProject.id, selectedVirtualState.id, virtualStateValues));
    }
  }

  render() {
    const { selectedVirtualState } = this.state;
    const {
      stateCategories,
      controlDevices,
      valid,
      dirty,
      validateStatuses,
      errorToView,
      isActiveProject
    } = this.props;

    const itemLayoutWithLabel = {
      labelCol: {
        sm: { span: 7 }
      },
      wrapperCol: {
        sm: { span: 15 }
      }
    };
    const isEdit = selectedVirtualState && selectedVirtualState.id;
    const title = isEdit
      ? selectedVirtualState.name + ' (№ ' + selectedVirtualState.globalNo + ')'
      : 'Создать новое состояние';
    return (
      <div>
        <WidgetParamsEditorMenu
          isViewTitle={selectedVirtualState}
          title={title}
          errorMessage={errorToView}
          isSaveActive={
            selectedVirtualState && valid && (isEdit ? dirty : true) && !isActiveProject
          }
          onSaveAction={this.onVirtualStateSave.bind(this)}
        />
        {selectedVirtualState && (
          <Form>
            <FormItem {...itemLayoutWithLabel} required={true} label="Номер">
              <Field component={InputNumber} name="globalNo" min={1} defaultValue={1} value={1} />
            </FormItem>

            <FormItem
              {...itemLayoutWithLabel}
              label="Название вирт. состояния"
              required={true}
              hasFeedback
              validateStatus={validateStatuses.name ? 'error' : ''}
            >
              <Field component={Input} name="name" />
            </FormItem>

            <FormItem {...itemLayoutWithLabel} required={true} label="Класс состояния">
              <Field
                component={Select}
                name="stateCategoryId"
                itemsSource={stateCategories}
                keyFieldName="id"
                valueFieldName="name"
              />
            </FormItem>

            <FormItem
              {...itemLayoutWithLabel}
              label="Прибор"
              required={true}
              hasFeedback
              validateStatus={validateStatuses.deviceId ? 'error' : ''}
            >
              <Field component={DeviceSelect} name="deviceId" devices={controlDevices} />
            </FormItem>

            <FormItem
              {...itemLayoutWithLabel}
              label="Сообщение при установке"
              required={true}
              hasFeedback
              validateStatus={validateStatuses.messageOn ? 'error' : ''}
            >
              <Field component={Input} name="messageOn" />
            </FormItem>

            <FormItem
              {...itemLayoutWithLabel}
              label="Сообщение при снятии"
              required={true}
              hasFeedback
              validateStatus={validateStatuses.messageOff ? 'error' : ''}
            >
              <Field component={Input} name="messageOff" />
            </FormItem>

            <FormItem {...itemLayoutWithLabel} label="Описание (примечание) вирт. состояния">
              <Field component={Input} type="textarea" rows={4} name="description" />
            </FormItem>
          </Form>
        )}
      </div>
    );
  }
}

const validate = values => {
  let result = {};

  if (!values.name) {
    result = { ...result, name: 'Не задано название' };
  } else if (values.name.length > 20) {
    result = { ...result, name: 'Название должно быть меньше 20 символов' };
  }

  if (!values.deviceId) {
    result = { ...result, deviceId: 'Не задано устройство' };
  }

  if (!values.messageOn) {
    result = { ...result, messageOn: 'Не задано сообщение включения' };
  } else if (values.messageOn.length > 20) {
    result = { ...result, messageOn: 'Сообщение включения должно быть меньше 20 символов' };
  }

  if (!values.messageOff) {
    result = { ...result, messageOff: 'Не задано сообщение выключения' };
  } else if (values.messageOff.length > 20) {
    result = { ...result, messageOff: 'Сообщение выключения должно быть меньше 20 символов' };
  }

  if (values.description && values.description.length > 569) {
    result = { ...result, description: 'Описание должно быть меньше 569 символов' };
  }

  return result;
};

const getFirstError = syncErrors => {
  if (syncErrors && Object.keys(syncErrors).length > 0) {
    return syncErrors[Object.keys(syncErrors)[0]];
  } else {
    return null;
  }
};

VirtualStateParamsEditor = reduxForm({
  form: FORM_NAME,
  validate
})(VirtualStateParamsEditor);

const mapStateToProps = state => {
  let syncErrors = getFormSyncErrors(FORM_NAME)(state);
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    devices: getCurrentProjectDeviceList(state),
    controlDevices: findDevicesByDeviceCategory(state, 'CONTROL'),
    stateCategories: Object.values(state.stateCategoryViewsHash),
    projectVirtualStates: getCurrentProjectVirtualStates(state),
    currentProject: getCurrentProject(state),
    loadCurrentProjectError: state.errors.loadCurrentProject,
    loadStateCategoryError: state.errors.loadStateCategory,
    selectedVirtualStateId: state.widgets.selectedVirtualStateId,
    virtualStateValues: getFormValues(FORM_NAME)(state),
    validateStatuses: syncErrors ? syncErrors : {},
    errorToView: getFirstError(syncErrors)
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VirtualStateParamsEditor);
