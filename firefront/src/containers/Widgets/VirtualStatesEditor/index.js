import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Checkbox, Spin, Modal } from 'antd';

import Widget from 'components/Widget';
import Table, { Column } from 'components/Table';
import Text from 'components/Text';
import Icon from 'components/Icon';

import VirtualStatesEditorMenu from 'containers/Menus/VirtualStatesEditorMenu';
import { getSelectedVirtualState } from 'helpers/virtualStates';

import { loadVirtualStates, deleteVirtualState, selectVirtualState } from 'actions/virtualStates';
import {
  getCurrentProjectDeviceList,
  getCurrentProject,
  checkCurrentProjectIsActive,
  getCurrentProjectVirtualStates
} from 'helpers/currentProject';

/**
 * Редактор виртуальных состояний.
 *
 * Компонент ответственен за изначальное получение виртуальных состояний
 * текущего редактируемого проекта, однако рассчитывает, что сам проект,
 * устройства проекта запрашиваются другими компонентами.
 */
class VirtualStatesEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      virtualStates: null,
      virtualStatesRequested: false,
      isActive: false,
      selectedVirtualState: null,
      height: 600
    };
  }

  /**
   * Получить виртуальные состояния текущего проекта или запросить их.
   *
   * @param {object} props - свойства компонента
   * @param {boolean} requestIfAbsent - запрашивать ли виртуальные состояния в случае их отсутствия
   * @return {virtualStatesRequested: boolean, virtualStates: array} флаг - отправлен ли запрос, и массив виртуальных состояний (может быть null)
   */
  getVirtualStatesOrSendRequest(props, requestIfAbsent) {
    const { currentProject, projectVirtualStates, dispatch } = props;
    if (currentProject.id) {
      if (projectVirtualStates) {
        return {
          virtualStatesRequested: true,
          virtualStates: [...projectVirtualStates]
        };
      } else if (requestIfAbsent) {
        dispatch(loadVirtualStates(currentProject.id));
        return { virtualStatesRequested: true, virtualStates: null };
      }
    }
    return { virtualStatesRequested: false, virtualStates: null };
  }

  getVirtualStateIcon(props, id) {
    const { stateCategories } = props;

    if (id && stateCategories) {
      const stateCategory = stateCategories.find(stateCategory => stateCategory.id === id);
      if (stateCategory) return stateCategory.iconName;
      else return null;
    }
    return null;
  }

  propsToState(props) {
    const { currentProject, selectedVirtualStateId, projectVirtualStates, stateCategories } = props;
    const { virtualStatesRequested } = this.state;
    const newState = {
      ...this.state,
      ...this.getVirtualStatesOrSendRequest(props, !virtualStatesRequested),
      isActive: true
    };

    const { virtualStates } = newState;
    if (virtualStates) {
      newState.selectedVirtualState = getSelectedVirtualState(
        currentProject,
        selectedVirtualStateId,
        projectVirtualStates,
        stateCategories,
        null
      );
    }

    this.setState(newState);
  }

  componentDidMount() {
    this.propsToState(this.props);
  }

  componentWillReceiveProps(newProps) {
    this.propsToState(newProps);
  }

  onVirtualStateAdd() {
    const { dispatch } = this.props;
    dispatch(selectVirtualState(null));
  }

  onVirtualStateDelete() {
    const { currentProject, dispatch } = this.props;
    const { selectedVirtualState } = this.state;
    Modal.confirm({
      title: 'Удаление виртуального состояния',
      content: (
        <div>
          Удалить вирт. состояние{' '}
          <b>
            {selectedVirtualState.globalNo}. {selectedVirtualState.name}
          </b>
          ?
        </div>
      ),
      okText: 'Удалить',
      onCancel: () => {},
      cancelText: 'Отмена',
      maskClosable: false,
      onOk: () => {
        dispatch(deleteVirtualState(currentProject.id, selectedVirtualState.id));
      }
    });
  }

  onVirtualStateSelect(record) {
    const { dispatch } = this.props;
    const virtualState = { ...record };
    dispatch(selectVirtualState(virtualState.id));
  }

  render() {
    const { isActiveProject } = this.props;
    const { virtualStates, isActive, selectedVirtualState, height } = this.state;

    let data = (
      <Spin>
        <Text>Нет данных</Text>
      </Spin>
    );

    if (virtualStates) {
      data = (
        <Table
          dataSource={virtualStates}
          scroll={{ y: height - 60 }}
          onRowClick={record => this.onVirtualStateSelect(record)}
        >
          <Column
            title=""
            width={10}
            dataIndex="id"
            key="radio"
            render={(text, record) => (
              <div style={{ display: 'flex', justifyContent: 'center' }}>
                <Checkbox
                  checked={selectedVirtualState && selectedVirtualState.id === text ? true : false}
                />
              </div>
            )}
          />
          <Column
            title="№"
            width={25}
            dataIndex="globalNo"
            key="globalNo"
            sortOrder="ascend"
            render={(text, record) => (
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Icon name={`${this.getVirtualStateIcon(record.stateCategoryId)}`} />
                <div>{`${text}`}</div>
              </div>
            )}
          />
          <Column title="Название" width={100} dataIndex="name" key="name" />
          <Column title="Примечание" width={200} dataIndex="description" key="description" />
        </Table>
      );
    }

    return (
      <Widget onHeightReady={height => this.setState({ height })}>
        <VirtualStatesEditorMenu
          isVirtualStateAddActive={isActive && !isActiveProject}
          onVirtualStateAdd={this.onVirtualStateAdd.bind(this)}
          isVirtualStateDeleteActive={
            isActive &&
            selectedVirtualState !== undefined &&
            selectedVirtualState !== null &&
            !isActiveProject
          }
          onVirtualStateDelete={this.onVirtualStateDelete.bind(this)}
        />
        {data}
      </Widget>
    );
  }
}

const mapStateToProps = state => {
  return {
    isActiveProject: checkCurrentProjectIsActive(state),
    devices: getCurrentProjectDeviceList(state),
    deviceProfileViews: state.deviceProfiles,
    stateCategories: Object.values(state.stateCategoryViewsHash),
    projectVirtualStates: getCurrentProjectVirtualStates(state),
    currentProject: getCurrentProject(state),
    selectedVirtualStateId: state.widgets.selectedVirtualStateId
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VirtualStatesEditor);
