import { takeEvery } from 'redux-saga/effects';

import { SET_DEVICE_STATE } from 'actions_new/device/types';

import message from 'helpers/message';
import { request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* setDeviceState({ type, payload }) {
  const { projectId, deviceId, stateId } = payload;
  const newIssue = {
    action: type,
    projectId: projectId,
    deviceId,
    parameters: {
      stateId
    }
  };
  try {
    const issue = yield request(`/issues`, 'POST', newIssue);
    yield dispatchSuccess(type, issue);
  } catch (e) {
    yield dispatchFail(type, e);
    message('error', e.message);
  }
}

export default function* device() {
  yield takeEvery(SET_DEVICE_STATE, setDeviceState);
}
