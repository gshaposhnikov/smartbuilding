import { takeEvery } from 'redux-saga/effects';

import { SET_REGION_STATE } from 'actions_new/region/types';

import message from 'helpers/message';
import { request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* setRegionState({ type, payload }) {
  const { projectId, regionId, stateId } = payload;
  const newIssue = {
    action: type,
    projectId: projectId,
    parameters: {
      regionId,
      stateId
    }
  };
  try {
    const issue = yield request(`/issues`, 'POST', newIssue);
    yield dispatchSuccess(type, issue);
  } catch (e) {
    yield dispatchFail(type, e);
    message('error', e.message);
  }
}

export default function* region() {
  yield takeEvery(SET_REGION_STATE, setRegionState);
}
