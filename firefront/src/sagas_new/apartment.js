import { takeEvery } from 'redux-saga/effects';

import { SET_APARTMENT_STATE } from 'actions_new/apartment/types';

import message from 'helpers/message';
import { request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* setApartmentState({ type, payload }) {
  const { projectId, stateId } = payload;
  const newIssue = {
    action: type,
    projectId: projectId,
    parameters: {
      stateId
    }
  };
  try {
    const issue = yield request(`/issues`, 'POST', newIssue);
    yield dispatchSuccess(type, issue);
  } catch (e) {
    yield dispatchFail(type, e);
    message('error', e.message);
  }
}

export default function* apartment() {
  yield takeEvery(SET_APARTMENT_STATE, setApartmentState);
}
