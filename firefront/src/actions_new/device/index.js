import { createAction } from 'redux-actions';

import { SET_DEVICE_STATE } from './types';

export const setDeviceState = createAction(SET_DEVICE_STATE, (projectId, deviceId, stateId) => ({
  projectId,
  deviceId,
  stateId
}));
