import { createAction } from 'redux-actions';

import { SET_REGION_STATE } from './types';

export const setRegionState = createAction(SET_REGION_STATE, (projectId, regionId, stateId) => ({
  projectId,
  regionId,
  stateId
}));
