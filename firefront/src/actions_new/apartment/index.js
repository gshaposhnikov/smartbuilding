import { createAction } from 'redux-actions';

import { SET_APARTMENT_STATE } from './types';

export const setApartmentState = createAction(SET_APARTMENT_STATE, (projectId, stateId) => ({
  projectId,
  stateId
}));
