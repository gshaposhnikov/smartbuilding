import {
  VIRTUAL_STATES_LOAD,
  VIRTUAL_STATE_CREATE,
  VIRTUAL_STATE_UPDATE,
  VIRTUAL_STATE_DELETE,
  isLoaded,
  PROJECT_DELETE,
  PROJECT_LOAD_CURRENT,
  PROJECT_LOAD_ACTIVE,
  PROJECT_UPDATE_STATUS,
  DEVICE_IMPORT_CONTOL_CONFIG
} from 'constants/actionTypes';

/**
 * state = virtualStates: {
 *    <projectId>: {
 *        <virtualStateId>: {
 *          id: <string>,
 *          ...
 *       }
 *    }
 * }
 * (см. метод GET api/v1/projects/<projectId>/virtual_states в тех. проекте)
 */
export default function virtualStates(state = {}, action) {
  switch (action.type) {
    case isLoaded(VIRTUAL_STATES_LOAD, true): {
      const { projectId, virtualStates } = action.payload;
      const newState = { ...state };
      if (!newState[projectId]) {
        newState[projectId] = {};
      }
      newState[projectId] = {};
      virtualStates.forEach(virtualState => {
        newState[projectId][virtualState.id] = virtualState;
      });
      return newState;
    }
    case isLoaded(DEVICE_IMPORT_CONTOL_CONFIG, true): {
      const {
        projectId,
        virtualStates: { created: createdVirtualStates }
      } = action.payload;
      if (!state[projectId] || !createdVirtualStates) {
        return state;
      }
      const newState = { ...state };
      newState[projectId] = { ...newState[projectId] };
      if (createdVirtualStates && createdVirtualStates.length) {
        createdVirtualStates.forEach(
          createdVirtualState => (newState[projectId][createdVirtualState.id] = createdVirtualState)
        );
      }
      return newState;
    }
    case isLoaded(VIRTUAL_STATE_CREATE, true): {
      const { projectId, newVirtualState } = action.payload;
      if (!state[projectId]) return state;
      const newState = { ...state };
      newState[projectId][newVirtualState.id] = newVirtualState;
      newState[projectId] = { ...newState[projectId] };
      return newState;
    }
    case isLoaded(VIRTUAL_STATE_DELETE, true): {
      const { virtualStateId, projectId } = action.payload;
      if (!state[projectId]) return state;
      if (!state[projectId][virtualStateId]) return state;
      const newState = { ...state };
      delete newState[projectId][virtualStateId];
      newState[projectId] = { ...newState[projectId] };
      return newState;
    }
    case isLoaded(VIRTUAL_STATE_UPDATE, true): {
      const { updatedVirtualState, projectId } = action.payload;
      if (!state[projectId]) return state;
      if (!state[projectId][updatedVirtualState.id]) return state;
      const newState = { ...state };
      newState[projectId][updatedVirtualState.id] = updatedVirtualState;
      newState[projectId] = { ...newState[projectId] };
      return newState;
    }
    case isLoaded(PROJECT_LOAD_CURRENT, true):
    case isLoaded(PROJECT_LOAD_ACTIVE, true):
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      const { project, virtualStates } = action.payload;
      const newState = { ...state };
      if (!newState[project.id]) {
        newState[project.id] = {};
      }
      newState[project.id] = {};
      virtualStates.forEach(virtualState => {
        newState[project.id][virtualState.id] = virtualState;
      });
      newState[project.id] = { ...newState[project.id] };
      return newState;
    }
    case isLoaded(PROJECT_DELETE, true): {
      const projectId = action.payload;
      if (!state[projectId]) return state;
      const newState = { ...state };
      delete newState[projectId];
      return newState;
    }
    default: {
      return state;
    }
  }
}
