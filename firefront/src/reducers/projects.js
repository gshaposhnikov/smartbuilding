import {
  PROJECT_CREATE,
  PROJECT_DELETE,
  PROJECTS_LOAD,
  PROJECT_UPDATE,
  PROJECT_UPDATE_STATUS,
  isLoaded,
  PROJECT_LOAD_ACTIVE,
  PROJECT_LOAD_CURRENT,
  PROJECT_IMPORT
} from 'constants/actionTypes';

import { removeIndicatorIdsFromStorageByProjectId } from 'helpers/indicators';

/*
 * state = projects: {
 *  <projectId>: {
 *      id: <string>,
 *      name: <string>,
 *      ...,
 *      entitiesLoaded: <boolean> - были ли загружены сущности проекта (устройств, зон и т.д.)
 *  },
 *  <projectId>: {
 *    ...
 *  },
 *  ...
 * }
 */
export default function projects(state = {}, action) {
  switch (action.type) {
    case isLoaded(PROJECTS_LOAD, true): {
      const projects = action.payload;
      const newState = {};
      projects.forEach(project => {
        newState[project.id] = { ...project, entitiesLoaded: false };
      });
      return newState;
    }
    case isLoaded(PROJECT_CREATE, true): {
      const createdProject = action.payload;
      const newState = { ...state };
      newState[createdProject.id] = { ...createdProject, entitiesLoaded: false };
      return newState;
    }
    case isLoaded(PROJECT_IMPORT, true): {
      const importedProject = action.payload;
      const newState = { ...state };
      newState[importedProject.id] = { ...importedProject, entitiesLoaded: false };
      return newState;
    }
    case isLoaded(PROJECT_UPDATE, true): {
      const updatedProject = action.payload;
      const newState = { ...state };
      newState[updatedProject.id] = {
        ...updatedProject,
        entitiesLoaded: newState[updatedProject.id].entitiesLoaded
      };
      return newState;
    }
    case isLoaded(PROJECT_UPDATE_STATUS, true):
    case isLoaded(PROJECT_LOAD_CURRENT, true):
    case isLoaded(PROJECT_LOAD_ACTIVE, true): {
      const { project: loadedProject } = action.payload;
      const newState = { ...state };
      newState[loadedProject.id] = { ...loadedProject, entitiesLoaded: true };
      return newState;
    }
    case isLoaded(PROJECT_DELETE, true): {
      const projectId = action.payload;
      const newState = { ...state };
      removeIndicatorIdsFromStorageByProjectId(projectId);
      delete newState[projectId];
      return newState;
    }

    default:
      return state;
  }
}
