import { DEVICE_GET_PROFILE_VIEWS, isLoaded } from 'constants/actionTypes';

export default function deviceProfileLogTypes(state = {}, action) {
  switch (action.type) {
    case isLoaded(DEVICE_GET_PROFILE_VIEWS, true): {
      const deviceProfileViews = action.payload;
      const deviceProfileLogTypes = {};
      deviceProfileViews.forEach(deviceProfileView => {
        if (deviceProfileView.deviceProfile.logTypes.length > 0)
          deviceProfileLogTypes[deviceProfileView.id] = deviceProfileView.deviceProfile.logTypes;
      });
      return deviceProfileLogTypes;
    }

    default:
      return state;
  }
}
