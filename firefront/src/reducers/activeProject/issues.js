import {
  ISSUES_LOAD,
  ISSUE_CREATE,
  ISSUE_DELETE,
  ISSUES_DELETE_FINISHED,
  ISSUES_DELETE_ERROR,
  ISSUES_DELETE_IN_QUEUE,
  ISSUE_STATUS_UPDATED,
  ISSUES_STATUS_UPDATED,
  isLoaded
} from 'constants/actionTypes';

/*
 * state = issues: {
 *   id: {id: <string>, ... (см. метод GET api/v1/issues в тех. проекте)},
 *   ...
 * }
 */
export default function issues(state = null, action) {
  switch (action.type) {
    case isLoaded(ISSUES_LOAD, true): {
      const newState = {};
      const loadedIssues = action.payload;
      loadedIssues.forEach(issue => {
        newState[issue.id] = issue;
      });
      return newState;
    }
    case isLoaded(ISSUE_CREATE, true): {
      const newIssues = action.payload;
      if (!state) return state;
      if (!newIssues.length) return state;
      const newState = { ...state };
      newIssues.forEach(issue => {
        newState[issue.id] = issue;
      });
      return newState;
    }

    case isLoaded(ISSUE_DELETE, true): {
      const deletedIssueId = action.payload;
      if (!state) return state;
      const newState = { ...state };
      delete newState[deletedIssueId];
      return newState;
    }

    case isLoaded(ISSUES_DELETE_IN_QUEUE, true):
    case isLoaded(ISSUES_DELETE_FINISHED, true):
    case isLoaded(ISSUES_DELETE_ERROR, true): {
      const deletedIssueIds = action.payload;
      if (!state) return state;
      if (!deletedIssueIds.length) return state;
      const newState = { ...state };
      deletedIssueIds.forEach(deletedIssueId => {
        delete newState[deletedIssueId];
      });
      return newState;
    }
    case ISSUE_STATUS_UPDATED: {
      const updatedIssue = action.payload;
      if (!state) return state;
      const newState = { ...state };
      newState[updatedIssue.id] = updatedIssue;
      return newState;
    }

    case ISSUES_STATUS_UPDATED: {
      const updatedIssues = action.payload;
      if (!state) return state;
      if (!updatedIssues.length) return state;
      const newState = { ...state };
      updatedIssues.forEach(issue => {
        newState[issue.id] = issue;
      });
      return newState;
    }

    default:
      return state;
  }
}
