import moment from 'moment';

import {
  TIMERS_UPDATE,
  SCENARIO_START_TIMER,
  isLoaded,
  PROJECT_UPDATE_STATUS
} from 'constants/actionTypes';

/**
 * timers: {
 *    devices: {
 *      [deviceId]: {
 *        deviceId: <string>, Идентификатор устройства
 *        regionId: <string>, Идентификатор зоны, к которой привязано устройство
 *        event: <string, SCHEDULED|STARTED|CANCELLED|POSTPONED|RESUMED>, Событие
 *        delay: <uint>, Время до начала тушения пожара
 *        timeStart: <uint> Время начала отсчета (время когда событие дошло до клиента)
 *      },
 *      ...
 *    },
 *    computerActions: {
 *      [index] : {
 *        countdownTimeSec: <uint>, Время обратного отсчета
 *        message: <string>, - Подсказка таймера
 *        name: <string>, - Название блока действия
 *        timeDelaySec: <uint>, Задержка до начала отсчета
 *        ...
 *      }
 *    }
 * }
 */
export default function timers(state = { devices: {}, computerActions: {} }, { type, payload }) {
  switch (type) {
    case TIMERS_UPDATE: {
      const newState = { ...state };
      const devices = newState.devices;
      const { deviceId, event } = payload;
      switch (event) {
        case 'SCHEDULED': {
          // Записываем время старта
          devices[deviceId] = { ...payload, timeStart: moment().unix() };
          break;
        }
        case 'CANCELLED':
        case 'STARTED': {
          // Если есть устройство в списке, то удаляем
          if (devices[deviceId]) delete devices[deviceId];
          break;
        }
        case 'RESUMED': {
          // Если есть устройство, то продалжаем отсчет
          if (devices[deviceId]) {
            devices[deviceId] = { ...devices[deviceId], event, timeStart: moment().unix() };
          }
          break;
        }
        case 'POSTPONED': {
          // Если есть устройство, то отсчитываем сколько секунд осталось
          if (devices[deviceId]) {
            const updatedDelay =
              devices[deviceId].timeStart + devices[deviceId].delay - moment().unix();
            // Если не осталось времени, то удаляем устройство из списка
            if (updatedDelay > 0) {
              devices[deviceId] = {
                ...devices[deviceId],
                event,
                delay: updatedDelay
              };
            } else {
              delete devices[deviceId];
            }
          }
          break;
        }
        default:
          break;
      }
      return newState;
    }
    case SCENARIO_START_TIMER: {
      const newState = { ...state };
      const computerActions = newState.computerActions;
      newState.computerActions[`comp${Object.keys(computerActions).length}`] = {
        ...payload,
        delay: payload.countdownTimeSec,
        timeStart: moment().unix()
      };

      return newState;
    }
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      return { devices: {}, computerActions: {} };
    }
    default:
      return state;
  }
}
