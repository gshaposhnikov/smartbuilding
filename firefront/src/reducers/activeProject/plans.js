import {
  PROJECT_LOAD_ACTIVE,
  PROJECT_UPDATE_STATUS,
  PLANS_LOAD_ACTIVE,
  PROJECT_LOAD_CURRENT,
  isLoaded
} from 'constants/actionTypes';

export default function plans(state = null, action) {
  switch (action.type) {
    case isLoaded(PLANS_LOAD_ACTIVE, true): {
      const plans = action.payload;
      const newState = {};
      plans.forEach(plan => {
        newState[plan.id] = plan;
      });
      return newState;
    }
    case isLoaded(PROJECT_LOAD_ACTIVE, true): {
      const { plans } = action.payload;
      const newState = {};
      plans.forEach(plan => {
        newState[plan.id] = plan;
      });
      return newState;
    }
    case isLoaded(PROJECT_LOAD_CURRENT, true): {
      const { project, plans } = action.payload;
      if (project.status === 'ACTIVE') {
        const newState = {};
        plans.forEach(plan => {
          newState[plan.id] = plan;
        });
        return newState;
      } else {
        return state;
      }
    }
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      const { project, plans } = action.payload;
      if (project.status === 'ACTIVE') {
        const newState = {};
        plans.forEach(plan => {
          newState[plan.id] = plan;
        });
        return newState;
      } else {
        return null;
      }
    }
    default:
      return state;
  }
}
