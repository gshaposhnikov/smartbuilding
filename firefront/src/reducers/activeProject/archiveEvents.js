import {
  EVENTS_LOAD_ARCHIVE,
  EVENTS_SET_ARCHIVE_FILTERS,
  EVENTS_CLEAN_ARCHIVE,
  EVENT_SET_SELECTED,
  EVENTS_LOAD_AVAILABLE,
  isLoaded
} from 'constants/actionTypes';

const INITIAL_STATE = {
  events: null,
  totalEventsCount: 0,
  selectedEventId: null
};

export default function archiveEvents(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case isLoaded(EVENTS_LOAD_ARCHIVE, true):
    case isLoaded(EVENTS_LOAD_AVAILABLE, true): {
      const { events, totalEventsCount } = payload;
      return {
        events,
        /* в action.payload eventsCount может отсутствовать */
        totalEventsCount: Number.isInteger(totalEventsCount)
          ? totalEventsCount
          : state.totalEventsCount,
        selectedEventId: state.selectedEventId
      };
    }
    case EVENTS_SET_ARCHIVE_FILTERS:
    case EVENTS_CLEAN_ARCHIVE: {
      return {
        ...state,
        events: null
      };
    }
    case EVENT_SET_SELECTED: {
      const { eventId } = payload;
      return {
        ...state,
        selectedEventId: eventId
      };
    }
    default:
      return state;
  }
}
