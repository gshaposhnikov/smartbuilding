import { dropRight } from 'lodash'; //Удаляет n последних элементов из массива

import { EVENTS_LOAD_LOG, UPDATED_ENTITIES, LOG_VIEWS_LOAD, isLoaded } from 'constants/actionTypes';
import { SKUD_LOG } from 'constants/skud';

const MAX_LOG_EVENTS = 1000;

const INITIAL_STATE = {
  logViews: [
    {
      id: 'all',
      name: 'Журнал событий',
      position: -999,
      active: true,
      recordCount: MAX_LOG_EVENTS,
      isRoot: true
    },
    SKUD_LOG
  ],
  events: {}
};

import { filterEntities } from 'helpers/filtration';

export default function logEvents(state = INITIAL_STATE, { type, payload, currentUser }) {
  switch (type) {
    case isLoaded(LOG_VIEWS_LOAD, true): {
      const { logViews } = payload;
      if (!logViews || !logViews.length) return state;
      const newState = { ...state };
      newState.logViews = [...newState.logViews, ...logViews];
      newState.logViews.sort(({ position: a }, { position: b }) => {
        if (a === b) return 0;
        else return a < b ? -1 : 1;
      });
      return newState;
    }
    case isLoaded(EVENTS_LOAD_LOG, true): {
      const { logViewId, events } = payload;
      const newState = { ...state };
      newState.events[logViewId] = events;
      return newState;
    }
    case UPDATED_ENTITIES: {
      const { logViews, events } = payload;
      if (logViews || events) {
        const newState = { ...state };
        let isUpdatedState = false;
        // Если были обновлены представления
        if (logViews) {
          const { created, updated, deletedIds } = logViews;
          if (deletedIds.length) {
            isUpdatedState = true;
            newState.logViews = newState.logViews.filter(logView => {
              const deleted = deletedIds.includes(logView.id);
              if (deleted && newState.events[logView.id]) delete newState.events[logView.id];
              return !deleted;
            });
          }
          if (created.length) {
            isUpdatedState = true;
            created.forEach(logView => newState.logViews.push(logView));
            newState.logViews = [...newState.logViews];
          }
          if (updated.length) {
            isUpdatedState = true;
            newState.logViews = newState.logViews.map(logView => {
              const updatedLogView = updated.find(item => item.id === logView.id);
              if (updatedLogView) {
                if (newState.events[logView.id]) delete newState.events[logView.id];
                return updatedLogView;
              }
              return logView;
            });
          }
          if (isUpdatedState) {
            newState.logViews.sort(({ position: a }, { position: b }) => {
              if (a === b) return 0;
              else return a < b ? -1 : 1;
            });
          }
        }
        // Если были обновлены события
        if (events && events.length) {
          isUpdatedState = true;
          let newEvents = events.reverse();
          // Фильтруем события по меткам фильтрации
          if (currentUser && currentUser.filterTagsOn)
            newEvents = filterEntities(currentUser, newEvents);
          // Делаем реверс, т.к. события отсортированны от старого к новому
          const activeLogViews = newState.logViews.filter(logView => logView.active); // Список активных журналов
          // Пробегаем все активные журналы и добавляем подходящие события в них
          activeLogViews.forEach(activeLogView => {
            // Фильтруем события
            const filteredEvents = activeLogView.isRoot
              ? [...newEvents] // В журнал попадают все события
              : newEvents.filter(event => {
                  const { subsystems, stateCategoryIds } = activeLogView;
                  const { subsystem, stateCategoryId } = event;
                  if (subsystems.length) {
                    if (!subsystem) return false;
                    if (subsystems.indexOf(subsystem) === -1) return false;
                  }
                  if (stateCategoryIds.length) {
                    if (!stateCategoryId) return false;
                    if (stateCategoryIds.indexOf(stateCategoryId) === -1) return false;
                  }
                  return true;
                });
            // Если есть подходящие события
            if (filteredEvents.length && newState.events[activeLogView.id]) {
              const maxRecordCount = activeLogView.recordCount || MAX_LOG_EVENTS; // Максимальное количество записей в журнале
              // Если кол-во новых событий больше, чем вмещает журнал
              if (filteredEvents.length > maxRecordCount) {
                newState.events[activeLogView.id] = dropRight(
                  filteredEvents,
                  filteredEvents.length - maxRecordCount
                );
              }
              // Если журнал переполнен
              else if (
                newState.events[activeLogView.id].length >
                maxRecordCount - filteredEvents.length
              ) {
                newState.events[activeLogView.id] = [
                  ...filteredEvents,
                  ...dropRight(
                    newState.events[activeLogView.id],
                    newState.events[activeLogView.id].length -
                      maxRecordCount +
                      filteredEvents.length
                  )
                ];
              } else {
                newState.events[activeLogView.id] = [
                  ...filteredEvents,
                  ...newState.events[activeLogView.id]
                ];
              }
            }
          });
        }
        return isUpdatedState ? newState : state;
      }
      return state;
    }

    default:
      return state;
  }
}
