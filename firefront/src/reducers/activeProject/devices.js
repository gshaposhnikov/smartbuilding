import {
  PROJECT_LOAD_ACTIVE,
  PROJECT_UPDATE_STATUS,
  DEVICES_LOAD_ACTIVE,
  ACTIVE_DEVICE_UPDATED,
  ACTIVE_DEVICES_UPDATED,
  ACTIVE_DEVICE_SET_CONFIG,
  PROJECT_LOAD_CURRENT,
  REGION_VIEWER_UPDATE_CURRENT,
  DEVICE_TREE_CHANGE_VIEW_MODE,
  DEVICE_ADD_NOTE,
  isLoaded
} from 'constants/actionTypes';

import {
  createDeviceTree,
  updateOneDeviceTreeItem,
  getRegionalDeviceTreeItems,
  updateDeviceTreeItems
} from 'helpers/device';

const HASH = 'hash',
  TREE = 'tree';

export default function devices(state = null, { type, payload }) {
  switch (type) {
    case isLoaded(DEVICES_LOAD_ACTIVE, true): {
      const devices = payload;
      const devicesHash = { [HASH]: {}, [TREE]: [] };
      devices.forEach(device => (devicesHash[HASH][device.id] = device));
      devicesHash[TREE] = createDeviceTree(devices);
      return devicesHash;
    }
    case isLoaded(DEVICE_ADD_NOTE, true):
    case isLoaded(ACTIVE_DEVICE_SET_CONFIG, true): {
      const { device: updatedDevice } = payload;
      if (!state) return state;
      const devicesHash = { ...state };

      if (devicesHash[HASH][updatedDevice.id]) devicesHash[HASH][updatedDevice.id] = updatedDevice;
      updateOneDeviceTreeItem(devicesHash[TREE], updatedDevice);

      devicesHash[HASH] = { ...devicesHash[HASH] };
      devicesHash[TREE] = [...devicesHash[TREE]];
      return devicesHash;
    }
    case ACTIVE_DEVICE_UPDATED: {
      const updatedDevice = payload;
      if (!state) return state;
      const devices = { ...state };

      if (devices[HASH][updatedDevice.id]) devices[HASH][updatedDevice.id] = updatedDevice;
      updateOneDeviceTreeItem(devices[TREE], updatedDevice);

      devices[HASH] = { ...devices[HASH] };
      devices[TREE] = [...devices[TREE]];
      return devices;
    }

    case ACTIVE_DEVICES_UPDATED: {
      const updatedDevices = payload;
      if (!state) return state;
      const devices = { ...state };

      updatedDevices.forEach(updatedDevice => {
        if (devices[HASH][updatedDevice.id]) devices[HASH][updatedDevice.id] = updatedDevice;
      });
      updateDeviceTreeItems(devices[TREE], updatedDevices);

      devices[HASH] = { ...devices[HASH] };
      devices[TREE] = [...devices[TREE]];
      return devices;
    }

    case isLoaded(PROJECT_LOAD_ACTIVE, true): {
      const { devices } = payload;
      const devicesHash = { [HASH]: {}, [TREE]: [] };
      devices.forEach(device => (devicesHash[HASH][device.id] = device));
      devicesHash[TREE] = createDeviceTree(devices);
      return devicesHash;
    }
    case isLoaded(PROJECT_LOAD_CURRENT, true): {
      const { project, devices } = payload;
      if (project.status !== 'ACTIVE') return state;
      const devicesHash = { [HASH]: {}, [TREE]: [] };
      devices.forEach(device => (devicesHash[HASH][device.id] = device));
      devicesHash[TREE] = createDeviceTree(devices);
      return devicesHash;
    }
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      const { project, devices } = payload;
      if (project.status !== 'ACTIVE') return null;
      const devicesHash = { [HASH]: {}, [TREE]: [] };
      devices.forEach(device => (devicesHash[HASH][device.id] = device));
      devicesHash[TREE] = createDeviceTree(devices);
      return devicesHash;
    }
    case REGION_VIEWER_UPDATE_CURRENT: {
      const region = payload;
      if (!state) return state;
      const devices = { ...state };
      if (region.id) {
        devices[TREE] = createDeviceTree(
          getRegionalDeviceTreeItems(Object.values(devices[HASH]), region.id)
        );
      } else {
        devices[TREE] = createDeviceTree(Object.values(devices[HASH]));
      }
      return devices;
    }
    case DEVICE_TREE_CHANGE_VIEW_MODE: {
      if (!state) return state;
      if (payload === 'all') {
        const devices = { ...state };
        devices[TREE] = createDeviceTree(Object.values(devices[HASH]));
        return devices;
      }
      return state;
    }
    default:
      return state;
  }
}
