import { combineReducers } from 'redux';

import { EVENTS_SET_ARCHIVE_FILTERS } from 'constants/actionTypes';

function filters(state = [], action) {
  switch (action.type) {
    case EVENTS_SET_ARCHIVE_FILTERS: {
      return [ ...action.payload ];
    }
    default:
      return state;
  }
}

export default combineReducers({
  filters
});
