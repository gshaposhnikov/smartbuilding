import {
  DEVICE_UPDATE_CURRENT,
  DEVICE_UPDATE_TYPE,
  DEVICE_UPDATE_ADDRESS_PATH,
  DEVICE_UPDATE_PLAN_LAYOUTS,
  PROJECT_UPDATE_STATUS,
  DEVICE_DELETE,
  ACTIVE_DEVICE_UPDATED,
  ACTIVE_DEVICES_UPDATED,
  ACTIVE_DEVICE_SET_CONFIG,
  DEVICE_SET_CONFIG,
  DEVICE_SET_PROPERTIES,
  DEVICE_SET_DISABLED,
  DEVICE_SET_SHAPE_LIBRARY,
  DEVICE_ADD_NOTE,
  REGION_VIEWER_UPDATE_CURRENT,
  PLAN_DELETE,
  REGION_CREATE_AND_CONNECT_DEVICE,
  REGION_NEW_DEVICE,
  REGION_DELETE_DEVICE,
  DEVICE_UPDATE_SUBSYSTEM,
  DEVICE_UPDATE_LINKS,
  isLoaded
} from 'constants/actionTypes';

export default function currentDevice(state = {}, { type, payload }) {
  switch (type) {
    case REGION_VIEWER_UPDATE_CURRENT: {
      if (!payload.id || !state.id) return state;
      if (state.regionId !== payload.id) return {};
      return state;
    }

    case ACTIVE_DEVICE_UPDATED: {
      const updatedDevice = payload;
      return state.id === updatedDevice.id ? updatedDevice : state;
    }

    case ACTIVE_DEVICES_UPDATED: {
      const updatedDevices = payload;
      const updatedCurrentDevice = updatedDevices.find(
        updatedDevice => updatedDevice.id === state.id
      );
      
      if (updatedCurrentDevice) return updatedCurrentDevice;
      else return state;
    }

    case isLoaded(PLAN_DELETE, true): {
      const { updatedDevices } = payload;
      if (!updatedDevices.length || !state.id) return state;
      const updatedCurrentDevice = updatedDevices.find(
        updatedDevice => updatedDevice.id === state.id
      );
      return updatedCurrentDevice ? updatedCurrentDevice : state;
    }
    case isLoaded(REGION_CREATE_AND_CONNECT_DEVICE, true):
    case isLoaded(REGION_NEW_DEVICE, true):
    case isLoaded(REGION_DELETE_DEVICE, true):
    case isLoaded(DEVICE_SET_SHAPE_LIBRARY, true):
    case isLoaded(DEVICE_ADD_NOTE, true):
    case isLoaded(DEVICE_SET_DISABLED, true):
    case isLoaded(ACTIVE_DEVICE_SET_CONFIG, true):
    case isLoaded(DEVICE_SET_CONFIG, true):
    case isLoaded(DEVICE_SET_PROPERTIES, true): {
      const { device: updatedDevice } = payload;
      return state.id === updatedDevice.id ? { ...updatedDevice } : state;
    }
    case isLoaded(DEVICE_UPDATE_LINKS, true):
    case isLoaded(DEVICE_UPDATE_SUBSYSTEM, true):
    case isLoaded(DEVICE_UPDATE_ADDRESS_PATH, true):
    case isLoaded(DEVICE_UPDATE_TYPE, true): {
      const { updatedDevices } = payload;
      const updatedDevice = state.id ? updatedDevices.find(device => device.id === state.id) : null;
      const newState = updatedDevice ? { ...updatedDevice } : state;
      return newState;
    }
    case isLoaded(DEVICE_UPDATE_PLAN_LAYOUTS, true): {
      const { device } = payload;
      return device.id === state.id ? { ...device } : state;
    }
    case isLoaded(DEVICE_DELETE, true): {
      let newState = state;
      if (newState.id) {
        const { removedDeviceIds, updatedDevices } = payload;
        if (removedDeviceIds && Array.isArray(removedDeviceIds) && removedDeviceIds.length > 0) {
          newState = removedDeviceIds.includes(newState.id) ? {} : newState;
        }
        if (
          updatedDevices &&
          Array.isArray(updatedDevices) &&
          updatedDevices.length > 0 &&
          newState.id
        ) {
          const updatedDevice = updatedDevices.find(device => device.id === newState.id);
          newState = updatedDevice ? updatedDevice : newState;
        }
      }
      return newState;
    }
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      return {};
    }
    case DEVICE_UPDATE_CURRENT: {
      return { ...payload };
    }
    default:
      return state;
  }
}
