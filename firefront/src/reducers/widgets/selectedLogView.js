import {
  LOG_VIEW_SELECT,
  LOG_VIEW_DELETE,
  UPDATED_ENTITIES,
  isLoaded
} from 'constants/actionTypes';

const selectedLogView = (state = null, { type, payload }) => {
  switch (type) {
    case LOG_VIEW_SELECT: {
      const { logView } = payload;
      if (!logView) return null;
      return { ...logView };
    }
    case isLoaded(LOG_VIEW_DELETE, true): {
      return null;
    }
    case UPDATED_ENTITIES: {
      const { logViews } = payload;
      if (!logViews || !state) return state;
      if (logViews.updated.length && state) {
        const updatedLogView = logViews.updated.find(logView => logView.id === state.id);
        if (updatedLogView) return updatedLogView;
        else return null;
      }
      if (logViews.created.length) {
        return logViews.created[0];
      }
      return state;
    }
    default: {
      return state;
    }
  }
};

export default selectedLogView;
