import { STATE_CATEGORY_VIEWS_LOAD, isLoaded } from 'constants/actionTypes';

export default function stateCategoryViewsHash(state = {}, action) {
  switch (action.type) {
    case isLoaded(STATE_CATEGORY_VIEWS_LOAD, true): {
      const stateCategoryViews = action.payload;
      const stateCategoryViewsHash = {};
      stateCategoryViews.forEach(stateCategoryView => {
        stateCategoryViewsHash[stateCategoryView.id] = { ...stateCategoryView };
      });
      return stateCategoryViewsHash;
    }

    default:
      return state;
  }
}
