import {
  DEVICES_LOAD,
  REGION_DELETE,
  isLoaded,
  DEVICE_CREATE,
  DEVICE_UPDATE_TYPE,
  DEVICE_DELETE,
  DEVICE_UPDATE_ADDRESS_PATH,
  DEVICE_UPDATE_PLAN_LAYOUTS,
  DEVICE_SET_PROPERTIES,
  DEVICE_SET_CONFIG,
  PROJECT_LOAD_CURRENT,
  PROJECT_LOAD_ACTIVE,
  PROJECT_UPDATE_STATUS,
  PROJECT_DELETE,
  ACTIVE_DEVICE_SET_CONFIG,
  ACTIVE_DEVICE_UPDATED,
  ACTIVE_DEVICES_UPDATED,
  DEVICE_SET_DISABLED,
  DEVICE_UPDATE_LINKS,
  DEVICE_ADD_NOTE,
  DEVICE_SET_SHAPE_LIBRARY,
  DEVICE_IMPORT_CONTOL_CONFIG,
  PLAN_DELETE,
  REGION_NEW_DEVICE,
  REGION_DELETE_DEVICE,
  REGION_CREATE_AND_CONNECT_DEVICE,
  DEVICE_UPDATE_SUBSYSTEM
} from 'constants/actionTypes';

import {
  createDeviceTree,
  addDeviceTreeItems,
  updateDeviceTreeItems,
  updateOneDeviceTreeItem,
  deleteDeviceTreeItem
} from 'helpers/device';

/**
 * state = devices: {
 *    <projectId>: {
 *      hash: {
 *        <deviceId>: {
 *          id: <string>,
 *          ...
 *          }
 *      },
 *      tree: [
 *        {
 *          id: <string>,
 *          children: <deviceList>,
 *          ...
 *        },
 *        ...
 *      ]
 *    }
 * }
 * (см. метод GET api/v1/projects/<projectId>/devices в тех. проекте)
 */
export default function devices(state = {}, action) {
  switch (action.type) {
    case isLoaded(DEVICES_LOAD, true): {
      const { projectId, devices } = action.payload;
      const devicesHash = { ...state };
      if (!devicesHash[projectId]) {
        devicesHash[projectId] = {};
      }
      devicesHash[projectId].hash = {};
      devices.forEach(device => (devicesHash[projectId].hash[device.id] = device));
      devicesHash[projectId].tree = createDeviceTree(devices);
      return devicesHash;
    }

    case ACTIVE_DEVICE_UPDATED: {
      const updatedDevice = action.payload;
      const projectId = updatedDevice.projectId;
      if (!state[projectId]) {
        return state;
      }
      const devicesByProjects = { ...state };
      const projectDevices = devicesByProjects[projectId];

      if (projectDevices.hash[updatedDevice.id]) {
        projectDevices.hash[updatedDevice.id] = updatedDevice;
      }
      updateOneDeviceTreeItem(projectDevices.tree, updatedDevice);

      projectDevices.hash = { ...devicesByProjects[projectId].hash };
      projectDevices.tree = [...devicesByProjects[projectId].tree];
      devicesByProjects[projectId] = { ...projectDevices };
      return devicesByProjects;
    }

    case ACTIVE_DEVICES_UPDATED: {
      const updatedDevices = action.payload;
      if (updatedDevices.length === 0) return state;
      /* Предполагаем, что обновления активных устройств сразу по нескольким проектам прийти не могут */
      const projectId = updatedDevices[0].projectId;
      if (!state[projectId]) return state;

      const devicesByProjects = { ...state };
      const projectDevices = devicesByProjects[projectId];

      updatedDevices.forEach(updatedDevice => {
        if (projectDevices.hash[updatedDevice.id]) {
          projectDevices.hash[updatedDevice.id] = updatedDevice;
        }
      });
      updateDeviceTreeItems(projectDevices.tree, updatedDevices);

      projectDevices.hash = { ...projectDevices.hash };
      projectDevices.tree = [...projectDevices.tree];
      devicesByProjects[projectId] = { ...projectDevices };
      return devicesByProjects;
    }

    case isLoaded(DEVICE_IMPORT_CONTOL_CONFIG, true): {
      const {
        projectId,
        devices: { created: createdDevices, updated: updatedDevices }
      } = action.payload;
      if (!state[projectId] || (!createdDevices && !updatedDevices)) {
        return state;
      }
      const devicesHash = { ...state };
      devicesHash[projectId].hash = { ...devicesHash[projectId].hash };
      if (updatedDevices && updatedDevices.length) {
        updatedDevices.forEach(updatedDevice => {
          if (devicesHash[projectId].hash[updatedDevice.id])
            devicesHash[projectId].hash[updatedDevice.id] = updatedDevice;
        });
      }
      if (createdDevices && createdDevices.length) {
        createdDevices.forEach(
          createdDevice => (devicesHash[projectId].hash[createdDevice.id] = createdDevice)
        );
      }
      // FIX: Вместо полного пересоздания дерева создавать элемент поддерева и добавлять его в основное дерево
      devicesHash[projectId].tree = createDeviceTree(Object.values(devicesHash[projectId].hash));
      return devicesHash;
    }
    case isLoaded(DEVICE_UPDATE_SUBSYSTEM, true):
    case isLoaded(DEVICE_UPDATE_LINKS, true):
    case isLoaded(DEVICE_UPDATE_ADDRESS_PATH, true):
    case isLoaded(DEVICE_UPDATE_TYPE, true):
    case isLoaded(DEVICE_CREATE, true): {
      const { projectId, createdDevices, updatedDevices } = action.payload;
      if (!state[projectId] || (!createdDevices.length && !updatedDevices.length)) {
        return state;
      }
      const devicesHash = { ...state };
      devicesHash[projectId].hash = { ...devicesHash[projectId].hash };
      devicesHash[projectId].tree = [...devicesHash[projectId].tree];

      if (updatedDevices && updatedDevices.length) {
        updatedDevices.forEach(updatedDevice => {
          if (devicesHash[projectId].hash[updatedDevice.id])
            devicesHash[projectId].hash[updatedDevice.id] = updatedDevice;
        });
        if (updatedDevices.length)
          updateDeviceTreeItems(devicesHash[projectId].tree, updatedDevices);
      }
      if (createdDevices && createdDevices.length) {
        createdDevices.forEach(
          createdDevice => (devicesHash[projectId].hash[createdDevice.id] = createdDevice)
        );
        if (createdDevices.length) addDeviceTreeItems(devicesHash[projectId].tree, createdDevices);
      }
      return devicesHash;
    }
    case isLoaded(PLAN_DELETE, true):
    case isLoaded(REGION_DELETE, true): {
      const { projectId, updatedDevices } = action.payload;
      if (!state[projectId] || !updatedDevices.length) {
        return state;
      }
      const devicesHash = { ...state };
      devicesHash[projectId].hash = { ...devicesHash[projectId].hash };
      devicesHash[projectId].tree = [...devicesHash[projectId].tree];

      if (updatedDevices && updatedDevices.length) {
        updatedDevices.forEach(updatedDevice => {
          if (devicesHash[projectId].hash[updatedDevice.id])
            devicesHash[projectId].hash[updatedDevice.id] = updatedDevice;
        });
        updateDeviceTreeItems(devicesHash[projectId].tree, updatedDevices);
      }
      return devicesHash;
    }
    case isLoaded(REGION_CREATE_AND_CONNECT_DEVICE, true):
    case isLoaded(REGION_NEW_DEVICE, true):
    case isLoaded(REGION_DELETE_DEVICE, true):
    case isLoaded(DEVICE_SET_SHAPE_LIBRARY, true):
    case isLoaded(DEVICE_ADD_NOTE, true):
    case isLoaded(DEVICE_SET_DISABLED, true):
    case isLoaded(ACTIVE_DEVICE_SET_CONFIG, true):
    case isLoaded(DEVICE_SET_CONFIG, true):
    case isLoaded(DEVICE_SET_PROPERTIES, true):
    case isLoaded(DEVICE_UPDATE_PLAN_LAYOUTS, true): {
      const { projectId, device: updatedDevice } = action.payload;
      if (!state[projectId]) {
        return state;
      }
      const devicesHash = { ...state };
      devicesHash[projectId].hash = { ...devicesHash[projectId].hash };
      devicesHash[projectId].tree = [...devicesHash[projectId].tree];

      if (devicesHash[projectId].hash[updatedDevice.id])
        devicesHash[projectId].hash[updatedDevice.id] = updatedDevice;
      updateOneDeviceTreeItem(devicesHash[projectId].tree, updatedDevice);

      return devicesHash;
    }
    case isLoaded(DEVICE_DELETE, true): {
      const { projectId, removedDeviceIds, updatedDevices } = action.payload;
      if (!state[projectId] || (!removedDeviceIds.length && !updatedDevices.length)) {
        return state;
      }
      const devicesHash = { ...state };
      devicesHash[projectId].hash = { ...devicesHash[projectId].hash };
      devicesHash[projectId].tree = [...devicesHash[projectId].tree];

      if (removedDeviceIds && removedDeviceIds.length) {
        removedDeviceIds.forEach(removedDeviceId => {
          deleteDeviceTreeItem(
            devicesHash[projectId].tree,
            devicesHash[projectId].hash[removedDeviceId]
          );
          delete devicesHash[projectId].hash[removedDeviceId];
        });
      }
      if (updatedDevices && updatedDevices.length) {
        updatedDevices.forEach(updatedDevice => {
          if (devicesHash[projectId].hash[updatedDevice.id])
            devicesHash[projectId].hash[updatedDevice.id] = updatedDevice;
        });
        updateDeviceTreeItems(devicesHash[projectId].tree, updatedDevices);
      }
      return devicesHash;
    }
    case isLoaded(PROJECT_LOAD_CURRENT, true):
    case isLoaded(PROJECT_LOAD_ACTIVE, true):
    case isLoaded(PROJECT_UPDATE_STATUS, true): {
      const { project, devices } = action.payload;
      const devicesHash = { ...state };
      if (!devicesHash[project.id]) {
        devicesHash[project.id] = {};
      }
      devicesHash[project.id].hash = {};
      devices.forEach(device => (devicesHash[project.id].hash[device.id] = device));
      devicesHash[project.id].tree = createDeviceTree(devices);
      return devicesHash;
    }
    case isLoaded(PROJECT_DELETE, true): {
      const projectId = action.payload;
      if (!state[projectId]) {
        return state;
      }
      const devicesHash = { ...state };
      delete devicesHash[projectId];
      return devicesHash;
    }
    default:
      return state;
  }
}
