import { combineReducers } from 'redux';

import users from './admin/users';
import userGroups from './admin/userGroups';
import permissions from './admin/permissions';

export default combineReducers({
  users,
  userGroups,
  permissions
});
