import { combineReducers } from 'redux';
import * as actionTypes from 'constants/actionTypes';

// Редюсер для отслеживания состояния выполнения

/**
 * Функция отслеживания состояния выполения операций,
 * которые могут вызываться многократно
 */
function progressReduxer(actionName) {
  return (state = null, action) => {
    switch (action.type) {
      case actionName: {
        return true;
      }
      case actionTypes.isLoaded(actionName, false):
      case actionTypes.isLoaded(actionName, true): {
        return false;
      }
      default:
        return state;
    }
  };
}

/**
 * Функция отслеживания состояния выполения операций,
 * состояние которых нужно для закрытия некоторых модальных окон
 */
function statusReduxer(actionName) {
  return (state = null, action) => {
    switch (action.type) {
      case actionName: {
        return true;
      }
      case actionTypes.MODAL_CLOSE:
      case actionTypes.isLoaded(actionName, false): {
        return null;
      }
      case actionTypes.isLoaded(actionName, true): {
        return false;
      }
      default:
        return state;
    }
  };
}

export default combineReducers({
  // CREATE
  createProject: statusReduxer(actionTypes.PROJECT_CREATE),
  createDevice: statusReduxer(actionTypes.DEVICE_CREATE),
  createRegion: statusReduxer(actionTypes.REGION_CREATE),
  createRegionAndConnectDevice: statusReduxer(actionTypes.REGION_CREATE_AND_CONNECT_DEVICE),
  // GET
  getMonitorableValuesHistory: progressReduxer(actionTypes.MONITORABLE_VALUES_GET_DEVICE_HISTORY),
  // LOAD
  loadActiveProject: progressReduxer(actionTypes.PROJECT_LOAD_ACTIVE),
  loadCurrentProject: progressReduxer(actionTypes.PROJECT_LOAD_CURRENT),
  loadActiveDevices: progressReduxer(actionTypes.DEVICES_LOAD_ACTIVE),
  loadDevies: progressReduxer(actionTypes.DEVICES_LOAD),
  loadPlans: progressReduxer(actionTypes.PLANS_LOAD),
  loadPlanGroups: progressReduxer(actionTypes.PLAN_GROUPS_LOAD),
  loadActivePlans: progressReduxer(actionTypes.PLANS_LOAD_ACTIVE),
  loadActivePlanGroups: progressReduxer(actionTypes.PLAN_GROUPS_LOAD_ACTIVE),
  loadRegions: progressReduxer(actionTypes.REGIONS_LOAD),
  // UPDATE
  updateProject: statusReduxer(actionTypes.PROJECT_UPDATE),
  updateProjectStatus: progressReduxer(actionTypes.PROJECT_UPDATE_STATUS),
  updateRegion: statusReduxer(actionTypes.REGION_UPDATE),
  // IMPORT
  importConfig: statusReduxer(actionTypes.DEVICE_IMPORT_CONTOL_CONFIG),
  importProject: progressReduxer(actionTypes.PROJECT_IMPORT),
  // REQUEST
  requestCurrentUser: progressReduxer(actionTypes.CURRENT_USER_REQUEST),
  requestLogin: progressReduxer(actionTypes.LOGIN_REQUEST)
});
