import { combineReducers } from 'redux';

import {
  REGION_UPDATE_CURRENT,
  REGION_DELETE,
  REGION_CREATE,
  SCENARIO_SELECT,
  SCENARIO_SELECT_TL_BLOCK,
  SCENARIO_SELECT_SUB_LOGIC,
  SCENARIO_DELETE,
  SCENARIO_REMOVE_TL_BLOCK,
  VIRTUAL_STATE_SELECT,
  VIRTUAL_STATE_DELETE,
  USER_SELECT,
  USER_DELETE,
  USER_GROUP_SELECT,
  USER_GROUP_DELETE,
  DEVICE_DRAGGABLE_UPDATE,
  ACTIVE_SCENARIOS_SET_FILTER,
  REGIONS_CHANGE_FILTER,
  isLoaded
} from 'constants/actionTypes';

import deviceTree from './widgets/deviceTree';
import projects from './widgets/projects';
import archiveEventsWidget from './widgets/archiveEventsWidget';
import deviceShapeLibrary from './widgets/deviceShapeLibrary';
import selectedLogView from './widgets/selectedLogView';
import selectedIndicatorGroupId from './widgets/selectedIndicatorGroupId';
import selectedIndicatorPanelId from './widgets/selectedIndicatorPanelId';
import currentDevice from './widgets/currentDevice';
import selectedIndicator from './widgets/selectedIndicator';
import currentEntityInfo from './widgets/currentEntityInfo';
import skudTree from './widgets/skudTree';
import workSchedulesTree from './widgets/workSchedulesTree';
import selectedSound from './widgets/selectedSound';
import selectedSoundNotificationId from './widgets/selectedSoundNotificationId';

function currentRegion(state = {}, { type, payload }) {
  switch (type) {
    case REGIONS_CHANGE_FILTER: {
      if (payload !== 'ALL' && state.subsystem && state.subsystem !== payload) {
        return {};
      }
      return state;
    }
    case isLoaded(REGION_DELETE, true): {
      return {};
    }
    case REGION_UPDATE_CURRENT: {
      return { ...payload };
    }
    case isLoaded(REGION_CREATE, true): {
      const { newRegion } = payload;
      return { ...newRegion.region };
    }
    default:
      return state;
  }
}

function regionsFilter(state = localStorage.getItem('regionsFilter') || 'ALL', { type, payload }) {
  switch (type) {
    case REGIONS_CHANGE_FILTER: {
      localStorage.setItem('regionsFilter', payload);
      return payload;
    }
    case isLoaded(REGION_CREATE, true): {
      const { newRegion } = payload;
      if (state !== 'ALL' && newRegion.region.subsystem !== state) {
        localStorage.setItem('regionsFilter', 'ALL');
        return 'ALL';
      }
      return state;
    }
    default: {
      return state;
    }
  }
}

function deviceByRegion(state = {}, action) {
  switch (action.type) {
    case isLoaded(REGION_UPDATE_CURRENT, true): {
      return { ...action.payload };
    }
    default:
      return state;
  }
}

function currentScenarioId(state = null, action) {
  switch (action.type) {
    case SCENARIO_SELECT: {
      const scenarioId = (' ' + action.payload).slice(1);
      return scenarioId;
    }
    case isLoaded(SCENARIO_DELETE, true): {
      /* Сбрасываем выделение при удалении сценария */
      return null;
    }
    case ACTIVE_SCENARIOS_SET_FILTER:
    case '@@router/LOCATION_CHANGE': {
      return null;
    }
    default:
      return state;
  }
}

function currentScenarioTLBlockNo(state = -1, action) {
  const timeLineBlockNo = action.payload;
  switch (action.type) {
    case SCENARIO_SELECT_TL_BLOCK: {
      return timeLineBlockNo;
    }
    case isLoaded(SCENARIO_REMOVE_TL_BLOCK, true):
    case isLoaded(SCENARIO_DELETE, true): {
      /* Сбрасываем выделение при удалении сценария или блока времени */
      return -1;
    }
    case SCENARIO_SELECT: {
      /* Сбрасываем выделение при переключение сценария */
      return -1;
    }
    case ACTIVE_SCENARIOS_SET_FILTER:
    case '@@router/LOCATION_CHANGE': {
      /* Сбрасываем выделение при смене странички */
      return -1;
    }

    default:
      return state;
  }
}

function currentSubLogicKey(state = 'root', action) {
  switch (action.type) {
    case SCENARIO_SELECT_SUB_LOGIC: {
      const subLogicKey = (' ' + action.payload).slice(1);
      return subLogicKey;
    }

    /* Сбрасываем выделение при смене странички, переключение сценария или удалении */
    case '@@router/LOCATION_CHANGE':
    case SCENARIO_SELECT:
    case isLoaded(SCENARIO_DELETE, true): {
      return 'root';
    }

    default:
      return state;
  }
}

function selectedVirtualStateId(state = null, action) {
  switch (action.type) {
    case VIRTUAL_STATE_SELECT: {
      const virtualStateId = (' ' + action.payload).slice(1);
      return virtualStateId;
    }
    case isLoaded(VIRTUAL_STATE_DELETE, true): {
      return null;
    }

    default:
      return state;
  }
}

function selectedUserId(state = null, action) {
  switch (action.type) {
    case USER_SELECT: {
      const userId = action.payload + '';
      return userId;
    }
    case isLoaded(USER_DELETE, true): {
      return null;
    }

    default:
      return state;
  }
}

function selectedUserGroupId(state = null, action) {
  switch (action.type) {
    case USER_GROUP_SELECT: {
      const userGroupId = action.payload + '';
      return userGroupId;
    }
    case isLoaded(USER_GROUP_DELETE, true): {
      return null;
    }
    default:
      return state;
  }
}

function draggableDeviceId(state = null, action) {
  switch (action.type) {
    case DEVICE_DRAGGABLE_UPDATE: {
      return action.payload;
    }
    default:
      return state;
  }
}

function activeScenariosFilter(state = 'ALL', { type, payload }) {
  switch (type) {
    case ACTIVE_SCENARIOS_SET_FILTER: {
      return payload;
    }
    default:
      return state;
  }
}

export default combineReducers({
  projects,
  currentDevice,
  deviceTree,
  currentRegion,
  regionsFilter,
  deviceByRegion,
  currentScenarioId,
  currentScenarioTLBlockNo,
  currentSubLogicKey,
  selectedVirtualStateId,
  archiveEventsWidget,
  selectedUserId,
  selectedUserGroupId,
  draggableDeviceId,
  deviceShapeLibrary,
  activeScenariosFilter,
  selectedLogView,
  currentEntityInfo,
  selectedIndicatorGroupId,
  selectedIndicatorPanelId,
  selectedIndicator,
  selectedSound,
  selectedSoundNotificationId,
  skudTree,
  workSchedulesTree
});
