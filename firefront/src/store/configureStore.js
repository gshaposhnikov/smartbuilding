import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import sagas from 'sagas';
import { routerMiddleware } from 'react-router-redux';

import { reduxDevtoolsOn } from '../../config/app';

import socketMiddleware from 'middlewares/socketMiddleware';
import { storageMiddleware } from 'middlewares/storageMiddleware';

import rootReducer from 'reducers';

let isDevToolsEnabled = false;

isDevToolsEnabled = reduxDevtoolsOn && typeof window !== 'undefined' && window.devToolsExtension;

export default function configureStore(history, initialState = {}) {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(
        routerMiddleware(history),
        socketMiddleware,
        sagaMiddleware,
        storageMiddleware()
      ),
      isDevToolsEnabled ? window.devToolsExtension() : f => f
    )
  );

  sagaMiddleware.run(sagas);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
