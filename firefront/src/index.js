import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import 'react-virtualized/styles.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
