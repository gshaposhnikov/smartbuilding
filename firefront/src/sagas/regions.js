import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  REGION_CREATE,
  REGION_NEW_DEVICE,
  REGION_DELETE_DEVICE,
  DEVICES_LOAD,
  REGIONS_LOAD,
  REGION_UPDATE,
  REGION_DELETE,
  REGIONS_LOAD_ACTIVE,
  REGION_CHANGE_GUARD_STATUS,
  REGION_CONSTANTS_LOAD,
  REGION_UPDATE_PLAN_LAYOUTS,
  REGION_NEW_DEVICE_LIST,
  REGION_DELETE_DEVICE_LIST,
  REGION_CREATE_AND_CONNECT_DEVICE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* createRegion(action) {
  const { projectId, newRegion } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/region_views`, 'POST', newRegion);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response && response.error) {
      throw new Error(response.error);
    }
    if (response && !response.region) {
      throw new Error('Сервер не вернул созданную зону');
    }

    yield dispatchSuccess(REGION_CREATE, { projectId, newRegion: response });
  } catch (error) {
    yield dispatchFail(REGION_CREATE, error.message);
    message('error', error.message);
  }
}

function* updateRegion(action) {
  const { projectId, updatedRegion } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/region_views/${updatedRegion.id}`,
      'PUT',
      updatedRegion
    );

    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(REGION_UPDATE, { projectId, updatedRegion: response });
  } catch (error) {
    yield dispatchFail(REGION_UPDATE, error.message);
    message('error', error.message);
  }
}

function* createRegionAndConnectDevice({ type, payload }) {
  const { projectId, region, deviceId } = payload;
  try {
    const createRegionResponse = yield request(
      `/projects/${projectId}/region_views`,
      'POST',
      region
    );
    if (!createRegionResponse) {
      throw new Error('Неизвестная ошибка');
    }
    if (createRegionResponse && createRegionResponse.error) {
      throw new Error(createRegionResponse.error);
    }
    const createdRegion = createRegionResponse;
    const updatedDevice = yield request(
      `/projects/${projectId}/regions/${createdRegion.id}/devices/${deviceId}`,
      'PUT'
    );
    yield dispatchSuccess(type, { projectId, newRegion: createdRegion, device: updatedDevice });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* connectNewDeviceToRegion({ type, payload }) {
  const { projectId, regionId, deviceId } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/regions/${regionId}/devices/${deviceId}`,
      'PUT'
    );
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(REGION_NEW_DEVICE, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* disconnectDeviceFromRegion({ type, payload }) {
  const { projectId, regionId, deviceId } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/regions/${regionId}/devices/${deviceId}`,
      'DELETE'
    );
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(REGION_DELETE_DEVICE, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* deleteRegion(action) {
  const { projectId, regionId } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/region_views/${regionId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(REGION_DELETE, {
      projectId,
      regionId,
      updatedScenarios: response.updatedScenarios,
      updatedDevices: response.updatedDevices,
      updatedIndicatorPanels: response.indicatorPanels
    });
  } catch (error) {
    yield dispatchFail(REGION_DELETE, error.message);
    message('error', error.message);
  }
}

function* loadActiveRegions(action) {
  const { projectId } = action.payload;
  try {
    const response = yield fetch(`/projects/${projectId}/active_regions/`);
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    if (!Array.isArray(response)) {
      throw new Error(`Получен неправильный ответ: ${response}`);
    }
    yield dispatchSuccess(REGIONS_LOAD_ACTIVE, response);
  } catch (error) {
    yield dispatchFail(REGIONS_LOAD_ACTIVE, error.message);
    message('error', error.message);
  }
}

function* changeRegionGuardStatus(action) {
  const { projectId, regionId, onGuard } = action.payload;
  const newIssue = {
    action: (onGuard === true ? 'ENABLE' : 'DISABLE') + '_REGION_GUARD',
    projectId,
    parameters: {
      regionId: regionId
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(REGION_CHANGE_GUARD_STATUS, null);
  } catch (error) {
    yield dispatchFail(REGION_CHANGE_GUARD_STATUS, error.message);
    message('error', error.message);
  }
}

function* loadRegionConstants(action) {
  try {
    const dictionaries = yield fetch(`/region_dictionaries`);
    if (!dictionaries) {
      throw new Error('Получен пустой ответ на запрос неизменяемых данных');
    } else if (dictionaries.error) {
      throw new Error(dictionaries.error);
    }

    yield dispatchSuccess(REGION_CONSTANTS_LOAD, dictionaries);
  } catch (error) {
    yield dispatchFail(REGION_CONSTANTS_LOAD, error.message);
  }
}
function* loadRegions(action) {
  const { projectId } = action.payload;
  try {
    const regionViews = yield fetch(`/projects/${projectId}/region_views`);
    if (!regionViews) {
      throw new Error('Получен пустой ответ');
    }
    if (regionViews.error) {
      throw new Error(regionViews.error);
    }
    if (!Array.isArray(regionViews)) {
      throw new Error('Полученный ответ не является массивом');
    }

    yield dispatchSuccess(REGIONS_LOAD, { projectId, regionViews });
  } catch (error) {
    yield dispatchFail(REGIONS_LOAD, error.message);
  }
}

function* updatePlanLayouts(action) {
  const { projectId, regionId, planLayouts } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/region_views/${regionId}?plan_layouts`,
      'PUT',
      planLayouts
    );

    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(REGION_UPDATE, { projectId, updatedRegion: response });
  } catch (error) {
    yield dispatchFail(REGION_UPDATE_PLAN_LAYOUTS, error.message);
    message('error', error.message);
  }
}

function* connectDeviceListToRegion({ type, payload }) {
  const { projectId, regionId, deviceIds } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/regions/${regionId}/devices`,
      'PUT',
      deviceIds
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, null);

    const devices = yield fetch(`/projects/${projectId}/devices/`);
    yield dispatchSuccess(DEVICES_LOAD, { projectId, devices });
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* disconnectDeviceListFromRegion({ type, payload }) {
  const { projectId, regionId, deviceIds } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/regions/${regionId}/devices`,
      'DELETE',
      deviceIds
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, null);

    const devices = yield fetch(`/projects/${projectId}/devices/`);
    yield dispatchSuccess(DEVICES_LOAD, { projectId, devices });
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

export default function* projects() {
  yield takeEvery(REGION_CREATE, createRegion);
  yield takeEvery(REGION_DELETE, deleteRegion);
  yield takeEvery(REGION_NEW_DEVICE, connectNewDeviceToRegion);
  yield takeEvery(REGION_DELETE_DEVICE, disconnectDeviceFromRegion);
  yield takeEvery(REGION_UPDATE, updateRegion);
  yield takeEvery(REGIONS_LOAD_ACTIVE, loadActiveRegions);
  yield takeEvery(REGION_CHANGE_GUARD_STATUS, changeRegionGuardStatus);
  yield takeEvery(REGION_CONSTANTS_LOAD, loadRegionConstants);
  yield takeEvery(REGIONS_LOAD, loadRegions);
  yield takeEvery(REGION_UPDATE_PLAN_LAYOUTS, updatePlanLayouts);
  yield takeEvery(REGION_NEW_DEVICE_LIST, connectDeviceListToRegion);
  yield takeEvery(REGION_DELETE_DEVICE_LIST, disconnectDeviceListFromRegion);
  yield takeEvery(REGION_CREATE_AND_CONNECT_DEVICE, createRegionAndConnectDevice);
}
