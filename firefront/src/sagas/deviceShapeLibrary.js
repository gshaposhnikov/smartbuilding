import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';
import {
  DEVICE_SHAPE_LIBRARIES_LOAD,
  DEVICE_SHAPE_LIBRARY_ADD,
  DEVICE_SHAPE_LIBRARY_UPDATE,
  DEVICE_SHAPE_LIBRARY_REMOVE,
  DEVICE_SHAPES_LOAD,
  DEVICE_SHAPE_ADD,
  DEVICE_SHAPE_UPDATE,
  DEVICE_SHAPE_REMOVE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadDeviceShapeLibraries() {
  try {
    const response = yield fetch('/device_shape_libraries');

    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(DEVICE_SHAPE_LIBRARIES_LOAD, response);
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_LIBRARIES_LOAD, error);
    message('error', error.message);
  }
}

function* addDeviceShapeLibrary(action) {
  try {
    const { deviceShapeLibrary } = action.payload;
    const response = yield request('/device_shape_libraries', 'POST', deviceShapeLibrary);

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_LIBRARY_ADD, error);
    message('error', error.message);
  }
}

function* updateDeviceShapeLibrary(action) {
  try {
    const { deviceShapeLibraryId, deviceShapeLibrary } = action.payload;
    const response = yield request(
      `/device_shape_libraries/${deviceShapeLibraryId}`,
      'PUT',
      deviceShapeLibrary
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_LIBRARY_UPDATE, error);
    message('error', error.message);
  }
}

function* removeDeviceShapeLibrary(action) {
  try {
    const { deviceShapeLibraryId } = action.payload;
    const response = yield request(`/device_shape_libraries/${deviceShapeLibraryId}`, 'DELETE');

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_LIBRARY_REMOVE, error);
    message('error', error.message);
  }
}

function* loadDeviceShapes() {
  try {
    const response = yield fetch('/device_shapes');

    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(DEVICE_SHAPES_LOAD, response);
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPES_LOAD, error);
    message('error', error.message);
  }
}

function* addDeviceShape(action) {
  try {
    const { deviceShape } = action.payload;
    const response = yield request('/device_shapes', 'POST', deviceShape);

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_ADD, error);
    message('error', error.message);
  }
}

function* updateDeviceShape(action) {
  try {
    const { deviceShapeId, deviceShape } = action.payload;
    const response = yield request(`/device_shapes/${deviceShapeId}`, 'PUT', deviceShape);

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_UPDATE, error);
    message('error', error.message);
  }
}

function* removeDeviceShape(action) {
  try {
    const { deviceShapeId } = action.payload;
    const response = yield request(`/device_shapes/${deviceShapeId}`, 'DELETE');

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(DEVICE_SHAPE_REMOVE, error);
    message('error', error.message);
  }
}

export default function* deviceShapeLibrary() {
  yield takeEvery(DEVICE_SHAPE_LIBRARIES_LOAD, loadDeviceShapeLibraries);
  yield takeEvery(DEVICE_SHAPE_LIBRARY_ADD, addDeviceShapeLibrary);
  yield takeEvery(DEVICE_SHAPE_LIBRARY_UPDATE, updateDeviceShapeLibrary);
  yield takeEvery(DEVICE_SHAPE_LIBRARY_REMOVE, removeDeviceShapeLibrary);
  yield takeEvery(DEVICE_SHAPES_LOAD, loadDeviceShapes);
  yield takeEvery(DEVICE_SHAPE_ADD, addDeviceShape);
  yield takeEvery(DEVICE_SHAPE_UPDATE, updateDeviceShape);
  yield takeEvery(DEVICE_SHAPE_REMOVE, removeDeviceShape);
}
