import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  PROJECTS_LOAD,
  PROJECT_CREATE,
  PROJECT_DELETE,
  PROJECT_UPDATE,
  PROJECT_LOAD_CURRENT,
  PROJECT_LOAD_ACTIVE,
  PROJECT_UPDATE_STATUS,
  PROJECT_SYNC_STATUS,
  PROJECT_IMPORT,
  PROJECT_EXPORT
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';
import fileSaver from 'file-saver';

function* loadProjects() {
  try {
    const response = yield fetch('/projects/');
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(PROJECTS_LOAD, response);
  } catch (error) {
    yield dispatchFail(PROJECTS_LOAD, error);
  }
}

function* createProject(action) {
  try {
    const newProject = yield request('/projects/', 'POST', action.payload);
    if (!newProject) {
      throw new Error('Неизвестная ошибка');
    }
    if (newProject.error) {
      throw new Error(newProject.error);
    }
    yield dispatchSuccess(PROJECT_CREATE, newProject);
    message('success', `Проект успешно создан`);
  } catch (error) {
    yield dispatchFail(PROJECT_CREATE, error);
    message('error', error.message);
  }
}

function* updateProject(action) {
  const updatedProject = action.payload;
  try {
    const response = yield request(`/projects/${updatedProject.id}`, 'PUT', updatedProject);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(PROJECT_UPDATE, updatedProject);
    message('success', `Проект успешно обновлен`);
  } catch (error) {
    yield dispatchFail(PROJECT_UPDATE, error.message);
    message('error', `${error.message}`);
  }
}

function* deleteProject(action) {
  try {
    const response = yield request(`/projects/${action.payload}`, 'DELETE');
    /*
     * В ответ на данный запрос должен приходить 200Ok с пустым телом,
     * либо ошибка и в теле строка описания.
     */
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(PROJECT_DELETE, action.payload);
    message('success', `Проект удален`);
  } catch (error) {
    yield dispatchFail(PROJECT_DELETE, error);
    message('error', error.message);
  }
}

function* loadActiveProject(action) {
  try {
    const response = yield fetch('/projects/active/');

    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(PROJECT_LOAD_ACTIVE, {
      ...response,
      activeProject: response.activeProject || {},
      subsystems: response.subsystems || [],
      indicatorGroups: response.indicatorPanelGroups,
      indicatorPanels: response.indicatorPanels
    });
  } catch (error) {
    yield dispatchFail(PROJECT_LOAD_ACTIVE, error.message);
  }
}

function* updateProjectStatus(action) {
  const { projectId, statusName } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}?status=${statusName}`, 'PUT');

    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(PROJECT_UPDATE_STATUS, {
      ...response,
      activeProject: response.activeProject || {},
      subsystems: response.subsystems || [],
      indicatorGroups: response.indicatorPanelGroups,
      indicatorPanels: response.indicatorPanels
    });
  } catch (error) {
    yield dispatchFail(PROJECT_UPDATE_STATUS, error.message);
    message('error', error.message);
  }
}

function* importProject({ type, payload }) {
  const { importedFile, version, fileName } = payload;
  try {
    let response = null;
    if (version === 'NT') {
      response = yield request(`/projects/importNt`, 'POST', importedFile);
    } else if (version === 'NG') {
      response = yield request(`/projects/importNg?project_name=${fileName}`, 'POST', importedFile);
    }
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, response);
    message('success', `Проект успешно импортирован`);
  } catch (error) {
    yield dispatchFail(type, error);
    message('error', error.message);
  }
}

function* projectExport(action) {
  const projectId = action.payload;
  try {
    const exportedProject = yield request(`/projects/${projectId}/export`, 'POST');
    if (!exportedProject) {
      throw new Error('Неизвестная ошибка');
    }
    if (exportedProject.error) {
      throw new Error(exportedProject.error);
    }
    let blob = new Blob([JSON.stringify(exportedProject)], {
      type: 'application/json;charset=utf-8'
    });
    fileSaver.saveAs(blob, exportedProject.project.name + '.json');
    yield dispatchSuccess(PROJECT_EXPORT, exportedProject);
    message('success', `Проект успешно экспортирован`);
  } catch (error) {
    yield dispatchFail(PROJECT_EXPORT, error);
    message('error', error.message);
  }
}

/**
 * Получение проекта
 * @param {string} actionType - actionType, с которым должна завершиться обработка запроса
 * @param {boolean|undefined} sync - Флаг означающий, что вызов был выполнен для синхронизации с другими вкладками браузера
 */
function loadProject(actionType, sync) {
  return function*({ type, payload }) {
    const projectId = payload;
    try {
      const response = yield fetch(`/projects/${projectId}/`);
      if (!response) {
        throw new Error('Неизвестная ошибка');
      }
      if (response.error) {
        throw new Error(response.error);
      }

      yield dispatchSuccess(actionType, {
        ...response,
        activeProject: response.activeProject || {},
        subsystems: response.subsystems || [],
        indicatorGroups: response.indicatorPanelGroups,
        indicatorPanels: response.indicatorPanels,
        sync
      });
    } catch (error) {
      yield dispatchFail(actionType, error.message);
    }
  };
}

export default function* projects() {
  yield takeEvery(PROJECTS_LOAD, loadProjects);
  yield takeEvery(PROJECT_CREATE, createProject);
  yield takeEvery(PROJECT_DELETE, deleteProject);
  yield takeEvery(PROJECT_UPDATE, updateProject);
  yield takeEvery(PROJECT_LOAD_CURRENT, loadProject(PROJECT_LOAD_CURRENT));
  yield takeEvery(PROJECT_LOAD_ACTIVE, loadActiveProject);
  yield takeEvery(PROJECT_UPDATE_STATUS, updateProjectStatus);
  yield takeEvery(PROJECT_IMPORT, importProject);
  yield takeEvery(PROJECT_EXPORT, projectExport);
  yield takeEvery(PROJECT_SYNC_STATUS, loadProject(PROJECT_UPDATE_STATUS, true));
}
