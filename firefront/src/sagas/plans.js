import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  PLANS_LOAD_ACTIVE,
  PLAN_GROUPS_LOAD_ACTIVE,
  PLAN_GROUP_CREATE,
  PLAN_GROUP_DELETE,
  PLAN_GROUP_UPDATE,
  PLAN_CREATE,
  PLAN_DELETE,
  PLAN_UPDATE,
  PLANS_LOAD,
  PLAN_GROUPS_LOAD
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadPlans(action) {
  const { projectId } = action.payload;
  try {
    const plans = yield fetch(`/projects/${projectId}/plans`);
    if (!plans) {
      throw new Error('Получен пустой ответ');
    }
    if (plans.error) {
      throw new Error(plans.error);
    }
    if (!Array.isArray(plans)) {
      throw new Error('Полученный ответ не является массивом');
    }

    yield dispatchSuccess(PLANS_LOAD, { projectId, plans });
  } catch (error) {
    yield dispatchFail(PLANS_LOAD, error.message);
  }
}

function* loadPlanGroups(action) {
  const { projectId } = action.payload;
  try {
    const planGroups = yield fetch(`/projects/${projectId}/plan_groups`);
    if (!planGroups) {
      throw new Error('Получен пустой ответ');
    }
    if (planGroups.error) {
      throw new Error(planGroups.error);
    }
    if (!Array.isArray(planGroups)) {
      throw new Error('Полученный ответ не является массивом');
    }

    yield dispatchSuccess(PLAN_GROUPS_LOAD, { projectId, planGroups });
  } catch (error) {
    yield dispatchFail(PLAN_GROUPS_LOAD, error.message);
  }
}

function* loadActivePlans(action) {
  const { projectId } = action.payload;
  try {
    const activePlans = yield fetch(`/projects/${projectId}/active_plans`);
    if (!activePlans) {
      throw new Error('Получен пустой ответ');
    }
    if (activePlans.error) {
      throw new Error(activePlans.error);
    }
    if (!Array.isArray(activePlans)) {
      throw new Error('Полученный ответ не является массивом');
    }

    yield dispatchSuccess(PLANS_LOAD_ACTIVE, activePlans);
  } catch (error) {
    yield dispatchFail(PLANS_LOAD_ACTIVE, error.message);
  }
}

function* loadActivePlanGroups(action) {
  const { projectId } = action.payload;
  try {
    const activePlanGroups = yield fetch(`/projects/${projectId}/plan_groups`);
    if (!activePlanGroups) {
      throw new Error('Получен пустой ответ');
    }
    if (activePlanGroups.error) {
      throw new Error(activePlanGroups.error);
    }
    if (!Array.isArray(activePlanGroups)) {
      throw new Error('Полученный ответ не является массивом');
    }

    yield dispatchSuccess(PLAN_GROUPS_LOAD_ACTIVE, activePlanGroups);
  } catch (error) {
    yield dispatchFail(PLAN_GROUPS_LOAD_ACTIVE, error.message);
  }
}

function* createPlanGroup(action) {
  const { planGroup, projectId } = action.payload;
  try {
    const newPlanGroup = yield request(`/projects/${projectId}/plan_groups/`, 'POST', planGroup);
    if (!newPlanGroup) {
      throw new Error('Неизвестная ошибка');
    }
    if (newPlanGroup && newPlanGroup.error) {
      throw new Error(newPlanGroup.error);
    }
    yield dispatchSuccess(PLAN_GROUP_CREATE, { newPlanGroup, projectId });
    message('success', `Группа успешно создана`);
  } catch (error) {
    yield dispatchFail(PLAN_GROUP_CREATE, error.message);
    message('error', error.message);
  }
}

function* updatePlanGroup(action) {
  const { planGroup, projectId } = action.payload;
  try {
    const updatedPlanGroup = yield request(
      `/projects/${projectId}/plan_groups/${planGroup.id}`,
      'PUT',
      planGroup
    );
    if (updatedPlanGroup && updatedPlanGroup.error) {
      throw new Error(updatedPlanGroup.error);
    }
    yield dispatchSuccess(PLAN_GROUP_UPDATE, { updatedPlanGroup, projectId });
    message('success', `Группа обновлена`);
  } catch (error) {
    yield dispatchFail(PLAN_GROUP_UPDATE, error.message);
    message('error', error.message);
  }
}

function* deletePlanGroup(action) {
  const { planGroupId, projectId } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/plan_groups/${planGroupId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(PLAN_GROUP_DELETE, {
      planGroupId,
      projectId,
      updatedPlans: response.plans
    });
    message('success', `Группа удалена`);
  } catch (error) {
    yield dispatchFail(PLAN_GROUP_DELETE, error.message);
    message('error', error.message);
  }
}

function* createPlan(action) {
  const { plan, projectId } = action.payload;
  try {
    const newPlan = yield request(`/projects/${projectId}/plans`, 'POST', plan);
    if (!newPlan) {
      throw new Error('Неизвестная ошибка');
    }
    if (newPlan && newPlan.error) {
      throw new Error(newPlan.error);
    }
    yield dispatchSuccess(PLAN_CREATE, { newPlan, projectId });
    message('success', `План успешно создан`);
  } catch (error) {
    yield dispatchFail(PLAN_CREATE, error.message);
    message('error', error.message);
  }
}

function* updatePlan(action) {
  const { plan, projectId } = action.payload;
  try {
    const updatedPlan = yield request(`/projects/${projectId}/plans/${plan.id}`, 'PUT', plan);
    if (updatedPlan && updatedPlan.error) {
      throw new Error(updatedPlan.error);
    }
    yield dispatchSuccess(PLAN_UPDATE, { updatedPlan, projectId });
    message('success', `План обновлен`);
  } catch (error) {
    yield dispatchFail(PLAN_UPDATE, error.message);
    message('error', error.message);
  }
}

function* deletePlan(action) {
  const { planId, projectId } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/plans/${planId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(PLAN_DELETE, {
      planId,
      projectId,
      updatedDevices: response.devices,
      updatedRegions: response.regions
    });
    message('success', `План удален`);
  } catch (error) {
    yield dispatchFail(PLAN_DELETE, error.message);
    message('error', error.message);
  }
}

export default function* plansSagas() {
  yield takeEvery(PLANS_LOAD, loadPlans);
  yield takeEvery(PLAN_GROUPS_LOAD, loadPlanGroups);
  yield takeEvery(PLANS_LOAD_ACTIVE, loadActivePlans);
  yield takeEvery(PLAN_GROUPS_LOAD_ACTIVE, loadActivePlanGroups);
  yield takeEvery(PLAN_GROUP_CREATE, createPlanGroup);
  yield takeEvery(PLAN_GROUP_UPDATE, updatePlanGroup);
  yield takeEvery(PLAN_GROUP_DELETE, deletePlanGroup);
  yield takeEvery(PLAN_CREATE, createPlan);
  yield takeEvery(PLAN_UPDATE, updatePlan);
  yield takeEvery(PLAN_DELETE, deletePlan);
}
