import { takeEvery } from 'redux-saga/effects';

import {
  INDICATOR_GROUP_ADD,
  INDICATOR_GROUP_DELETE,
  INDICATOR_GROUP_UPDATE,
  INDICATOR_PANEL_ADD,
  INDICATOR_PANEL_UPDATE,
  INDICATOR_PANEL_DELETE,
  INDICATOR_UPDATE,
  INDICATOR_CHANGE_DEVICES_POLLING_STATE,
  INDICATOR_CHANGE_REGIONS_POLLING_STATE,
  INDICATOR_CHANGE_REGIONS_GUARD,
  INDICATOR_PERFORM_SCENARIO_ACTION
} from 'constants/actionTypes';

import message from 'helpers/message';
import { request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* addIndicatorGroup({ type, payload }) {
  const { projectId, newIndicatorGroup } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_groups`,
      'POST',
      newIndicatorGroup
    );
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, newIndicatorGroup: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* deleteIndicatorGroup({ type, payload }) {
  const { projectId, indicatorGroupId, nextIndicatorGroupId } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_groups/${indicatorGroupId}`,
      'DELETE'
    );
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, {
      projectId,
      deletedIndicatorGroupId: indicatorGroupId,
      nextIndicatorGroupId
    });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* updateIndicatorGroup({ type, payload }) {
  const { projectId, indicatorGroupId, updatedIndicatorGroup } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_groups/${indicatorGroupId}`,
      'PUT',
      updatedIndicatorGroup
    );
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, updatedIndicatorGroup: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* addIndicatorPanel({ type, payload }) {
  const { projectId, newIndicatorPanel } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_panels`,
      'POST',
      newIndicatorPanel
    );
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, newIndicatorPanel: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* updateIndicatorPanel({ type, payload }) {
  const { projectId, updatedIndicatorPanel } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_panels/${updatedIndicatorPanel.id}`,
      'PUT',
      updatedIndicatorPanel
    );
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, updatedIndicatorPanel: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* updateIndicator({ type, payload }) {
  const { projectId, indicatorPanelId, updatedIndicator } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_panels/${indicatorPanelId}?row=${
        updatedIndicator.rowNo
      }&col=${updatedIndicator.colNo}`,
      'PUT',
      updatedIndicator
    );
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, updatedIndicatorPanel: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* deleteIndicatorPanel({ type, payload }) {
  const { projectId, indicatorPanelId } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/indicator_panels/${indicatorPanelId}`,
      'DELETE'
    );
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, deletedIndicatorPanelId: indicatorPanelId });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* changeIndicatorDevicesPollingState({ type, payload }) {
  const { projectId, indicatorPanelId, indicator, state } = payload;
  const { colNo, rowNo, entityType, entityIds } = indicator;
  const issue = {
    action: `${state ? 'ENABLE' : 'DISABLE'}_INDICATOR_DEVICES_POLLING_STATE`,
    projectId,
    parameters: {
      indicatorPanelId,
      colNo,
      rowNo,
      entityType,
      entityIds
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', issue);
    if (!response) throw new Error('Неизвестная ошибка');
    if (response.error) throw new Error(response.error);
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* changeIndicatorRegionsPollingState({ type, payload }) {
  const { projectId, indicatorPanelId, indicator, state } = payload;
  const { colNo, rowNo, entityType, entityIds } = indicator;
  const issue = {
    action: `${state ? 'ENABLE' : 'DISABLE'}_INDICATOR_REGIONS_POLLING_STATE`,
    projectId,
    parameters: {
      indicatorPanelId,
      colNo,
      rowNo,
      entityType,
      entityIds
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', issue);
    if (!response) throw new Error('Неизвестная ошибка');
    if (response.error) throw new Error(response.error);
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* changeIndicatorRegionsGuard({ type, payload }) {
  const { projectId, indicatorPanelId, indicator, state } = payload;
  const { colNo, rowNo, entityType, entityIds } = indicator;
  const issue = {
    action: `${state ? 'ENABLE' : 'DISABLE'}_INDICATOR_REGIONS_GUARD`,
    projectId,
    parameters: {
      indicatorPanelId,
      colNo,
      rowNo,
      entityType,
      entityIds
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', issue);
    if (!response) throw new Error('Неизвестная ошибка');
    if (response.error) throw new Error(response.error);
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* performIndicatorScenarioAction({ type, payload }) {
  const { projectId, indicatorPanelId, indicator, actionId } = payload;
  const { colNo, rowNo, entityType, entityIds } = indicator;
  const issue = {
    action: 'PERFORM_INDICATOR_SCENARIO_ACTION',
    projectId,
    parameters: {
      indicatorPanelId,
      colNo,
      rowNo,
      entityType,
      entityIds,
      actionId
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', issue);
    if (!response) throw new Error('Неизвестная ошибка');
    if (response.error) throw new Error(response.error);
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

export default function* indicatorsSaga() {
  yield takeEvery(INDICATOR_GROUP_ADD, addIndicatorGroup);
  yield takeEvery(INDICATOR_GROUP_DELETE, deleteIndicatorGroup);
  yield takeEvery(INDICATOR_GROUP_UPDATE, updateIndicatorGroup);
  yield takeEvery(INDICATOR_PANEL_ADD, addIndicatorPanel);
  yield takeEvery(INDICATOR_PANEL_UPDATE, updateIndicatorPanel);
  yield takeEvery(INDICATOR_PANEL_DELETE, deleteIndicatorPanel);
  yield takeEvery(INDICATOR_UPDATE, updateIndicator);
  yield takeEvery(INDICATOR_CHANGE_DEVICES_POLLING_STATE, changeIndicatorDevicesPollingState);
  yield takeEvery(INDICATOR_CHANGE_REGIONS_POLLING_STATE, changeIndicatorRegionsPollingState);
  yield takeEvery(INDICATOR_CHANGE_REGIONS_GUARD, changeIndicatorRegionsGuard);
  yield takeEvery(INDICATOR_PERFORM_SCENARIO_ACTION, performIndicatorScenarioAction);
}
