import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';
import {
  DEVICE_CREATE,
  DEVICE_UPDATE_TYPE,
  DEVICE_SET_CONFIG,
  DEVICE_DELETE,
  DEVICES_RESET_STATES,
  DEVICE_UPLOAD_CONFIG,
  DEVICE_DOWNLOAD_CONFIG,
  DEVICE_UPLOAD_CONTROL_DATABASE,
  DEVICE_GET_PROFILE_VIEWS,
  DEVICE_GET_PROFILES_DICTIONARY,
  DEVICE_UPDATE_ADDRESS_PATH,
  DEVICES_LOAD_ACTIVE,
  DEVICE_UPDATE_PLAN_LAYOUTS,
  DEVICES_LOAD,
  ACTIVE_DEVICE_SET_CONFIG,
  DEVICE_SET_PROPERTIES,
  DEVICE_SET_POLLING_STATE,
  DEVICE_SET_TIME,
  DEVICE_UPLOAD_ALL_CONTROL_DATABASES,
  DEVICE_RESET,
  DEVICE_SET_DISABLED,
  DEVICE_UPDATE_FW,
  DEVICE_READ_EVENTS,
  DEVICE_SET_CONTROL_PASSWORD,
  DEVICE_UPDATE_LINKS,
  DEVICE_ADD_NOTE,
  DEVICE_PERFORM_ACTION,
  DEVICE_EXPORT_CONTOL_CONFIG,
  DEVICE_IMPORT_CONTOL_CONFIG,
  DEVICE_SET_SHAPE_LIBRARY,
  DEVICE_UPDATE_SUBSYSTEM
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* getDeviceProfileViews({ type }) {
  try {
    const response = yield fetch('/device_profile_views');
    if (!response) {
      throw new Error('Неизвестная ошибка при получении профилей устройств');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    if (!Array.isArray(response)) {
      throw new Error('Неправильный формат профилей устройств - должен быть массив');
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
  }
}

function* getDeviceProfilesDictionary({ type }) {
  try {
    const response = yield fetch('/device_profile_views/device_profiles_dictionary');
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
  }
}

function* loadDevices(action) {
  const { projectId } = action.payload;
  try {
    const response = yield fetch(`/projects/${projectId}/devices/`);
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    if (!Array.isArray(response)) {
      throw new Error(`Получен неправильный ответ: ${response}`);
    }
    yield dispatchSuccess(DEVICES_LOAD, { projectId, devices: response });
  } catch (error) {
    yield dispatchFail(DEVICES_LOAD, error.message);
    message('error', error.message);
  }
}

function* createDevice(action) {
  const { projectId, device } = action.payload;
  try {
    /* добавляем новое устройство REST-запросом */
    const response = yield request(`/projects/${projectId}/devices`, 'POST', device);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_CREATE, response);
    message('success', 'Добавлено');
  } catch (error) {
    yield dispatchFail(DEVICE_CREATE, error.message);
    message('error', error.message);
  }
}

function* updateDeviceType(action) {
  const { projectId, deviceId, newDeviceProfileId } = action.payload;
  try {
    /* изменяем тип устройства REST-запросом */
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?device_profile_id=${newDeviceProfileId}`,
      'PUT'
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_UPDATE_TYPE, response);
  } catch (error) {
    yield dispatchFail(DEVICE_UPDATE_TYPE, error.message);
    message('error', error.message);
  }
}

function* updateDeviceAddressPath(action) {
  const { projectId, deviceId, newDeviceAddressPath } = action.payload;
  try {
    /* изменяем адрес устройства REST-запросом */
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?address=${newDeviceAddressPath}`,
      'PUT'
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_UPDATE_ADDRESS_PATH, response);
    message('success', 'Адрес обновлен');
  } catch (error) {
    yield dispatchFail(DEVICE_UPDATE_ADDRESS_PATH, error.message);
    message('error', error.message);
  }
}

function* updateDevicePlanLayouts(action) {
  const { projectId, deviceId, planLayouts } = action.payload;
  try {
    /* изменяем расположение устройства на планах REST-запросом */
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?plan_layouts`,
      'PUT',
      planLayouts
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_UPDATE_PLAN_LAYOUTS, { projectId, device: response });
    message('success', 'Расположение устройства изменено');
  } catch (error) {
    yield dispatchFail(DEVICE_UPDATE_PLAN_LAYOUTS, error.message);
    message('error', error.message);
  }
}

function* deleteDevice(action) {
  const { projectId, deviceId } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/devices/${deviceId}`, 'DELETE');
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_DELETE, response);
  } catch (error) {
    yield dispatchFail(DEVICE_DELETE, error.message);
    message('error', error.message);
  }
}

function* loadActiveDevices(action) {
  const { projectId } = action.payload;
  try {
    const response = yield fetch(`/projects/${projectId}/active_devices/`);
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    if (!Array.isArray(response)) {
      throw new Error(`Получен неправильный ответ: ${response}`);
    }
    yield dispatchSuccess(DEVICES_LOAD_ACTIVE, response);
  } catch (error) {
    yield dispatchFail(DEVICES_LOAD_ACTIVE, error.message);
    message('error', error.message);
  }
}

function* setDeviceConfig(action) {
  const { projectId, deviceId, deviceConfig } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?config`,
      'PUT',
      deviceConfig
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(DEVICE_SET_CONFIG, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(DEVICE_SET_CONFIG, error.message);
    message('error', error.message);
  }
}

function* setActiveDeviceConfig(action) {
  const { projectId, deviceId, deviceConfig } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/active_devices/${deviceId}?config`,
      'PUT',
      deviceConfig
    );
    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(ACTIVE_DEVICE_SET_CONFIG, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(ACTIVE_DEVICE_SET_CONFIG, error.message);
    message('error', error.message);
  }
}

function* deviceResetState(action) {
  const { resetAction, projectId } = action.payload;
  const newIssue = {
    action: 'RESET_STATE_ON_ALL_DEVICES',
    projectId: projectId,
    parameters: {
      manualResetStateGroup: resetAction
    }
  };
  try {
    const issue = yield request(`/issues`, 'POST', newIssue);
    yield dispatchSuccess(DEVICES_RESET_STATES, issue);
  } catch (error) {
    yield dispatchFail(DEVICES_RESET_STATES, error);
    message('error', error.message);
  }
}

function* uploadAllDeviceControlDatabases({ type, payload }) {
  const { projectId, params } = payload;
  const newIssue = {
    action: 'WRITE_ALL_CONTROL_DEVICES_DATABASE',
    projectId: projectId,
    parameters: params
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error);
    message('error', error.message);
  }
}

function getDeviceIssueSaga(actionType, actionId) {
  return function*(action) {
    const { projectId, deviceId } = action.payload;
    const newIssue = {
      action: actionId,
      projectId,
      deviceId
    };
    try {
      const response = yield request(`/issues`, 'POST', newIssue);

      if (response && response.error) throw new Error(response.error);
      yield dispatchSuccess(actionType, response);
    } catch (error) {
      yield dispatchFail(actionType, error.message);
      message('error', error.message);
    }
  };
}

function* setDevicePollingState(action) {
  const { projectId, deviceId, regionId, state } = action.payload;
  const newIssue = {
    action:
      (state === true ? 'ENABLE' : 'DISABLE') +
      `${deviceId ? '_DEVICE' : '_REGION_DEVICES'}` +
      '_POLLING_STATE',
    projectId,
    deviceId,
    parameters: {
      regionId
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);

    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(DEVICE_SET_POLLING_STATE, response);
  } catch (error) {
    yield dispatchFail(DEVICE_SET_POLLING_STATE, error.message);
    message('error', error.message);
  }
}

function* setDeviceProperties(action) {
  const { projectId, deviceId, deviceProperties } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?properties`,
      'PUT',
      deviceProperties
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(DEVICE_SET_PROPERTIES, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(DEVICE_SET_PROPERTIES, error.message);
    message('error', error.message);
  }
}

function* setDeviceDisabled({ type, payload }) {
  const { projectId, deviceId, disabled } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?disabled=${disabled}`,
      'PUT'
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(DEVICE_SET_DISABLED, { projectId, device: response });
  } catch (err) {
    yield dispatchFail(DEVICE_SET_DISABLED);
    message('error', err.message);
  }
}

function* updateDeviceFw(action) {
  const { projectId, deviceId, fwFile } = action.payload;
  const newIssue = {
    action: deviceId ? 'UPDATE_CONTROL_DEVICE_FIRMWARE' : 'UPDATE_ALL_CONTROL_DEVICE_FIRMWARE',
    projectId,
    deviceId,
    parameters: {
      fwFile
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);

    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(DEVICE_UPDATE_FW, response);
  } catch (error) {
    yield dispatchFail(DEVICE_UPDATE_FW, error.message);
    message('error', error.message);
  }
}

function* readEvents(action) {
  const { projectId, deviceId, logTypes } = action.payload;
  const newIssue = {
    action: 'READ_EVENTS',
    projectId,
    deviceId,
    parameters: {
      logIds: logTypes
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);

    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(DEVICE_READ_EVENTS, response);
  } catch (error) {
    yield dispatchFail(DEVICE_READ_EVENTS, error.message);
    message('error', error.message);
  }
}

function* setDeviceControlPassword({ type, payload }) {
  const { projectId, deviceId, username, password } = payload;
  const newIssue = {
    action: 'SET_CONTROL_DEVICE_USER_PASSWORD',
    projectId,
    deviceId,
    parameters: { username, password }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(DEVICE_SET_CONTROL_PASSWORD, response);
  } catch (err) {
    yield dispatchFail(DEVICE_SET_CONTROL_PASSWORD);
    message('error', err.message);
  }
}

function* updateDeviceLinks({ type, payload }) {
  const { projectId, deviceId, deviceLinks } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?links`,
      'PUT',
      deviceLinks
    );
    if (response && response.error) throw new Error(response.error);
    yield dispatchSuccess(type, { projectId, createdDevices: [], updatedDevices: response });
  } catch (err) {
    yield dispatchFail(type);
    message('error', err.message);
  }
}

function* addDeviceNote({ type, payload }) {
  const { projectId, deviceId, messageText } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/active_devices/${deviceId}?notepad`,
      'POST',
      { message: messageText }
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, device: response });
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* performDeviceAction({ type, payload }) {
  const { projectId, deviceId, actionId, actionParameters } = payload;
  const newIssue = {
    action: 'PERFORM_DEVICE_ACTION',
    projectId,
    deviceId,
    parameters: {
      actionId,
      actionParameters
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error);
    message('error', error.message);
  }
}

function* uploadDeviceControlDatabases({ type, payload }) {
  const { projectId, deviceId, params } = payload;
  const newIssue = {
    action: 'WRITE_CONTROL_DEVICE_DATABASE',
    projectId,
    deviceId,
    parameters: params
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* exportControlDeviceConfig({ type, payload }) {
  const { projectId, deviceId } = payload;
  const newIssue = {
    action: 'READ_CONTROL_DEVICE_DATABASE',
    projectId,
    deviceId
  };
  try {
    const response = yield request('/issues', 'POST', newIssue);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { issue: response[0] });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* importControlDeviceConfig({ type, payload }) {
  const { projectId, controlDeviceEntities } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/entities`,
      'POST',
      controlDeviceEntities
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, ...response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* setDeviceShapeLibrary({ type, payload }) {
  const { projectId, deviceId, deviceShapeLibraryId } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?shape_library=${deviceShapeLibraryId}`,
      'PUT'
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { projectId, device: response });
  } catch (err) {
    yield dispatchFail(type);
    message('error', err.message);
  }
}

function* updateDeviceSubsystem({ type, payload }) {
  const { projectId, deviceId, subsystem } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/devices/${deviceId}?custom_subsystem=${subsystem}`,
      'PUT'
    );
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

export default function* deviceSagas() {
  yield takeEvery(ACTIVE_DEVICE_SET_CONFIG, setActiveDeviceConfig);
  yield takeEvery(DEVICE_SET_PROPERTIES, setDeviceProperties);
  yield takeEvery(DEVICE_GET_PROFILE_VIEWS, getDeviceProfileViews);
  yield takeEvery(DEVICE_GET_PROFILES_DICTIONARY, getDeviceProfilesDictionary);
  yield takeEvery(DEVICE_CREATE, createDevice);
  yield takeEvery(DEVICE_UPDATE_TYPE, updateDeviceType);
  yield takeEvery(DEVICE_UPDATE_ADDRESS_PATH, updateDeviceAddressPath);
  yield takeEvery(DEVICE_UPDATE_PLAN_LAYOUTS, updateDevicePlanLayouts);
  yield takeEvery(DEVICE_DELETE, deleteDevice);
  yield takeEvery(DEVICES_LOAD, loadDevices);
  yield takeEvery(DEVICES_LOAD_ACTIVE, loadActiveDevices);
  yield takeEvery(DEVICE_SET_CONFIG, setDeviceConfig);

  yield takeEvery(DEVICES_RESET_STATES, deviceResetState);
  yield takeEvery(
    DEVICE_UPLOAD_CONFIG,
    getDeviceIssueSaga(DEVICE_UPLOAD_CONFIG, 'WRITE_CONFIG_TO_DEVICE')
  );
  yield takeEvery(
    DEVICE_DOWNLOAD_CONFIG,
    getDeviceIssueSaga(DEVICE_DOWNLOAD_CONFIG, 'READ_CONFIG_FROM_DEVICE')
  );
  yield takeEvery(DEVICE_SET_POLLING_STATE, setDevicePollingState);
  yield takeEvery(DEVICE_SET_TIME, getDeviceIssueSaga(DEVICE_SET_TIME, 'SET_CONTROL_DEVICE_TIME'));
  yield takeEvery(DEVICE_RESET, getDeviceIssueSaga(DEVICE_RESET, 'RESET_CONTROL_DEVICE'));
  yield takeEvery(DEVICE_UPLOAD_ALL_CONTROL_DATABASES, uploadAllDeviceControlDatabases);
  yield takeEvery(DEVICE_SET_DISABLED, setDeviceDisabled);
  yield takeEvery(DEVICE_UPDATE_FW, updateDeviceFw);
  yield takeEvery(DEVICE_READ_EVENTS, readEvents);
  yield takeEvery(DEVICE_SET_CONTROL_PASSWORD, setDeviceControlPassword);
  yield takeEvery(DEVICE_UPDATE_LINKS, updateDeviceLinks);
  yield takeEvery(DEVICE_ADD_NOTE, addDeviceNote);
  yield takeEvery(DEVICE_PERFORM_ACTION, performDeviceAction);
  yield takeEvery(DEVICE_UPLOAD_CONTROL_DATABASE, uploadDeviceControlDatabases);
  yield takeEvery(DEVICE_EXPORT_CONTOL_CONFIG, exportControlDeviceConfig);
  yield takeEvery(DEVICE_IMPORT_CONTOL_CONFIG, importControlDeviceConfig);
  yield takeEvery(DEVICE_SET_SHAPE_LIBRARY, setDeviceShapeLibrary);
  yield takeEvery(DEVICE_UPDATE_SUBSYSTEM, updateDeviceSubsystem);
}
