import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';
import {
  USERS_LOAD,
  USER_CREATE,
  USER_UPDATE,
  USER_DELETE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadUsers() {
  try {
    const users = yield fetch('/users/');
    if (!users) {
      throw new Error('Получен пустой ответ на запрос списка пользователей');
    }
    if (users.error) {
      throw new Error(users.error);
    }

    yield dispatchSuccess(USERS_LOAD, users);
  } catch (error) {
    yield dispatchFail(USERS_LOAD, error);
  }
}

function* createUser(action) {
  const user = action.payload;
  try {
    const response = yield request(`/users`, 'POST', user);
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(USER_CREATE, error.message);
    message('error', error.message);
  }
}

function* updateUser(action) {
  const { userId, user } = action.payload;
  try {
    const response = yield request(`/users/${userId}`, 'PUT', user);
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(USER_UPDATE, error.message);
    message('error', error.message);
  }
}

function* deleteUser(action) {
  const { userId, active } = action.payload;
  try {
    const response = yield request(`/users/${userId}?active=${active}`, 'PUT');
    if (response && response.error) {
      throw new Error(response.error);
    }

  } catch (error) {
    yield dispatchFail(USER_DELETE, error.message);
    message('error', error.message);
  }
}

export default function* users() {
  yield takeEvery(USERS_LOAD, loadUsers);
  yield takeEvery(USER_CREATE, createUser);
  yield takeEvery(USER_UPDATE, updateUser);
  yield takeEvery(USER_DELETE, deleteUser);
}