import { all } from 'redux-saga/effects';
import projects from './projects';
import devices from './devices';
import regions from './regions';
import scenarios from './scenarios';
import virtualStates from './virtualStates';
import events from './events';
import authorization from './authorization';
import users from './users';
import userGroups from './userGroups';
import plans from './plans';
import medias from './medias';
import monitorableValues from './monitorableValues';
import issues from './issues';
import dictionaries from './dictionaries';
import deviceShapeLibrary from './deviceShapeLibrary';
import stateCategoryViews from './stateCategoryViews';
import logViews from './logViews';
import indicators from './indicators';
import skud from './skud';
import soundNotifications from './soundNotifications';
import license from './license';
// New
import apartmentNew from 'sagas_new/apartment';
import deviceNew from 'sagas_new/device';
import regionNew from 'sagas_new/region';

export default function* rootSaga() {
  yield all([
    projects(),
    devices(),
    regions(),
    scenarios(),
    virtualStates(),
    events(),
    authorization(),
    users(),
    userGroups(),
    plans(),
    medias(),
    monitorableValues(),
    issues(),
    dictionaries(),
    deviceShapeLibrary(),
    stateCategoryViews(),
    logViews(),
    indicators(),
    skud(),
    license(),
    soundNotifications(),
    // New
    apartmentNew(),
    deviceNew(),
    regionNew()
  ]);
}
