import { takeEvery } from 'redux-saga/effects';

import { LICENSE_LOAD_PERMISSIONS } from 'constants/actionTypes';

import { fetch, dispatchSuccess, dispatchFail } from 'helpers/request';
import message from 'helpers/message';

function* loadLicensePermissions({ type }) {
  try {
    const response = yield fetch('/driver/license');
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

export default function* license() {
  yield takeEvery(LICENSE_LOAD_PERMISSIONS, loadLicensePermissions);
}
