import { takeEvery } from 'redux-saga/effects';

import {
  STATE_CATEGORY_VIEWS_LOAD,
} from 'constants/actionTypes';

import { fetch, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadStateCategories(action) {
  try {
    const response = yield fetch(`/state_categories`);
    if (!response) {
      throw new Error('Получен пустой ответ на запрос классов состояний');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(STATE_CATEGORY_VIEWS_LOAD, response );
  } catch (error) {
    yield dispatchFail(STATE_CATEGORY_VIEWS_LOAD, error.message);
  }
}


export default function* stateCategoryViews() {
  yield takeEvery(STATE_CATEGORY_VIEWS_LOAD, loadStateCategories);
}
