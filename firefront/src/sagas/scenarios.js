import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  SCENARIO_CONSTANTS_LOAD,
  SCENARIOS_LOAD,
  SCENARIO_CREATE,
  SCENARIO_UPDATE_BASIC_PARAMS,
  SCENARIO_UPDATE_ADVANCED_PARAMS,
  SCENARIO_DELETE,
  SCENARIO_ADD_TL_BLOCK,
  SCENARIO_UPDATE_TL_BLOCK,
  SCENARIO_REMOVE_TL_BLOCK,
  SCENARIO_UPDATE_START_LOGIC,
  SCENARIO_UPDATE_STOP_LOGIC,
  SCENARIO_PERFORM_ACTION
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadScenarioConstants(action) {
  try {
    const dictionaries = yield fetch(`/scenario_dictionaries`);
    const triggerTypes = yield fetch(`/scenario_trigger_types`);
    const actionTypes = yield fetch(`/scenario_action_types`);
    if (!dictionaries || !triggerTypes || !actionTypes) {
      throw new Error('Получен пустой ответ на запрос неизменяемых данных');
    }
    if (dictionaries.error) {
      throw new Error(dictionaries.error);
    }
    if (triggerTypes.error) {
      throw new Error(triggerTypes.error);
    }
    if (actionTypes.error) {
      throw new Error(actionTypes.error);
    }

    yield dispatchSuccess(SCENARIO_CONSTANTS_LOAD, { dictionaries, triggerTypes, actionTypes });
  } catch (error) {
    yield dispatchFail(SCENARIO_CONSTANTS_LOAD, error.message);
  }
}

function* loadScenarios(action) {
  const projectId = action.payload;
  try {
    const scenarios = yield fetch(`/projects/${projectId}/scenarios`);
    if (!scenarios) {
      throw new Error('Получен пустой ответ');
    }
    if (scenarios.error) {
      throw new Error(scenarios.error);
    }

    yield dispatchSuccess(SCENARIOS_LOAD, { projectId: projectId, scenarios: scenarios });
  } catch (error) {
    yield dispatchFail(SCENARIOS_LOAD, error.message);
    message('error', error.message);
  }
}

function* createScenario(action) {
  const { projectId, scenarioBasicParams } = action.payload;
  try {
    const scenario = yield request(`/projects/${projectId}/scenarios`, 'POST', scenarioBasicParams);
    if (!scenario) {
      throw new Error('Получен пустой ответ');
    }
    if (scenario.error) {
      throw new Error(scenario.error);
    }

    yield dispatchSuccess(SCENARIO_CREATE, { projectId: projectId, scenario: scenario });
    message('success', `Сценарий добавлен`);
  } catch (error) {
    yield dispatchFail(SCENARIO_CREATE, error.message);
    message('error', error.message);
  }
}

function* deleteScenario(action) {
  const { projectId, scenarioId } = action.payload;
  try {
    const response = yield request(`/projects/${projectId}/scenarios/${scenarioId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(SCENARIO_DELETE, {
      projectId: projectId,
      scenarioId: scenarioId,
      updatedIndicatorPanels: response.updatedIndicatorPanels
    });
    message('success', `Сценарий удален`);
  } catch (error) {
    yield dispatchFail(SCENARIO_DELETE, error.message);
    message('error', error.message);
  }
}

function getScenarioUpdateSaga(actionType, entityName, httpMethod, urlTail, successMessage) {
  return function*(action) {
    const { projectId, scenarioId } = action.payload;
    const entity = action.payload[entityName];
    try {
      const scenario = yield request(
        `/projects/${projectId}/scenarios/${scenarioId}${urlTail}`,
        httpMethod,
        entity
      );
      if (!scenario) {
        throw new Error('Получен пустой ответ');
      }
      if (scenario.error) {
        throw new Error(scenario.error);
      }

      yield dispatchSuccess(actionType, {
        projectId: projectId,
        scenario: scenario
      });
      message('success', successMessage);
    } catch (error) {
      yield dispatchFail(actionType, error.message);
      message('error', error.message);
    }
  };
}

function* updateScenarioTimeLineBlock(action) {
  const { projectId, scenarioId, timeLineBlockNo, timeLineBlock } = action.payload;
  try {
    const scenario = yield request(
      `/projects/${projectId}/scenarios/${scenarioId}/time_line_blocks/${timeLineBlockNo}`,
      'PUT',
      timeLineBlock
    );
    if (!scenario) {
      throw new Error('Получен пустой ответ');
    }
    if (scenario.error) {
      throw new Error(scenario.error);
    }

    yield dispatchSuccess(SCENARIO_UPDATE_TL_BLOCK, { projectId: projectId, scenario: scenario });
    message('success', `Блок изменен`);
  } catch (error) {
    yield dispatchFail(SCENARIO_UPDATE_TL_BLOCK, error.message);
    message('error', error.message);
  }
}

function* removeScenarioTimeLineBlock(action) {
  const { projectId, scenarioId, timeLineBlockNo } = action.payload;
  try {
    const scenario = yield request(
      `/projects/${projectId}/scenarios/${scenarioId}/time_line_blocks/${timeLineBlockNo}`,
      'DELETE'
    );
    if (!scenario) {
      throw new Error('Получен пустой ответ');
    }
    if (scenario.error) {
      throw new Error(scenario.error);
    }

    yield dispatchSuccess(SCENARIO_REMOVE_TL_BLOCK, { projectId: projectId, scenario: scenario });
    message('success', `Блок удален`);
  } catch (error) {
    yield dispatchFail(SCENARIO_REMOVE_TL_BLOCK, error.message);
    message('error', error.message);
  }
}

function* performScenarioAction({ type, payload }) {
  const { projectId, scenarioNo, actionId } = payload;
  const newIssue = {
    action: 'PERFORM_SCENARIO_ACTION',
    projectId,
    parameters: {
      scenarioNo,
      actionId
    }
  };
  try {
    const response = yield request(`/issues`, 'POST', newIssue);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error);
    message('error', error.message);
  }
}

export default function* scenariosSagas() {
  yield takeEvery(SCENARIO_CONSTANTS_LOAD, loadScenarioConstants);
  yield takeEvery(SCENARIOS_LOAD, loadScenarios);
  yield takeEvery(SCENARIO_CREATE, createScenario);
  yield takeEvery(SCENARIO_DELETE, deleteScenario);
  yield takeEvery(
    SCENARIO_UPDATE_BASIC_PARAMS,
    getScenarioUpdateSaga(
      SCENARIO_UPDATE_BASIC_PARAMS,
      'scenarioBasicParams',
      'PUT',
      '?basic_params',
      'Параметры сценария изменены'
    )
  );
  yield takeEvery(
    SCENARIO_UPDATE_ADVANCED_PARAMS,
    getScenarioUpdateSaga(
      SCENARIO_UPDATE_ADVANCED_PARAMS,
      'scenarioAdvancedParams',
      'PUT',
      '?advanced_params',
      'Параметры сценария изменены'
    )
  );
  yield takeEvery(
    SCENARIO_ADD_TL_BLOCK,
    getScenarioUpdateSaga(
      SCENARIO_ADD_TL_BLOCK,
      'timeLineBlock',
      'POST',
      '/time_line_blocks',
      'Блок добавлен в сценарий'
    )
  );
  yield takeEvery(SCENARIO_UPDATE_TL_BLOCK, updateScenarioTimeLineBlock);
  yield takeEvery(SCENARIO_REMOVE_TL_BLOCK, removeScenarioTimeLineBlock);
  yield takeEvery(
    SCENARIO_UPDATE_START_LOGIC,
    getScenarioUpdateSaga(
      SCENARIO_UPDATE_START_LOGIC,
      'logicBlock',
      'PUT',
      '?start_logic',
      'Логика изменена'
    )
  );
  yield takeEvery(
    SCENARIO_UPDATE_STOP_LOGIC,
    getScenarioUpdateSaga(
      SCENARIO_UPDATE_STOP_LOGIC,
      'logicBlock',
      'PUT',
      '?stop_logic',
      'Логика изменена'
    )
  );
  yield takeEvery(SCENARIO_PERFORM_ACTION, performScenarioAction);
}
