import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';
import {
  SKUD_ENTITIES,
  getAddActionType,
  getUpdateActionType,
  getRemoveActionType,
  WORK_SCHEDULE_DAY_ADD,
  WORK_SCHEDULE_DAY_UPDATE,
  WORK_SCHEDULE_DAY_REMOVE,
  ACCESS_GRANTED_TIME_ADD,
  ACCESS_GRANTED_TIME_UPDATE,
  ACCESS_GRANTED_TIME_REMOVE,
  SKUD_WRITE_DB_TO_CONTROL_DEVICES
} from 'constants/actionTypes';
import { request, dispatchFail, dispatchSuccess } from 'helpers/request';
import {
  ACCESS_KEY_START_READ_VALUE,
  ACCESS_KEY_STOP_READ_VALUE
} from 'constants/actionTypeModules/skud';

const SKUD_ENTITY_NAMES = Object.keys(SKUD_ENTITIES);

const addSkudEntity = (entityName, entityPath) => {
  return function*({ payload }) {
    const { projectId, entityData } = payload;
    try {
      const response = yield request(`/projects/${projectId}/${entityPath}`, 'POST', entityData);

      if (response && response.error) {
        throw new Error(response.error);
      }
    } catch (error) {
      yield dispatchFail(getAddActionType(entityName), error);
      message('error', error.message);
    }
  };
};

const updateSkudEntity = (entityName, entityPath) => {
  return function*({ payload }) {
    const { projectId, entityId, entityData } = payload;
    try {
      const response = yield request(
        `/projects/${projectId}/${entityPath}/${entityId}`,
        'PUT',
        entityData
      );

      if (response && response.error) {
        throw new Error(response.error);
      }
    } catch (error) {
      yield dispatchFail(getUpdateActionType(entityName), error);
      message('error', error.message);
    }
  };
};

const removeSkudEntity = (entityName, entityPath) => {
  return function*({ payload }) {
    const { projectId, entityId } = payload;
    try {
      const response = yield request(`/projects/${projectId}/${entityPath}/${entityId}`, 'DELETE');

      if (response && response.error) {
        throw new Error(response.error);
      }
    } catch (error) {
      yield dispatchFail(getRemoveActionType(entityName), error);
      message('error', error.message);
    }
  };
};

function* addWorkScheduleDay({ payload }) {
  const { projectId, workScheduleId, day } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${SKUD_ENTITIES.workSchedules.entityPath}/${workScheduleId}/days`,
      'POST',
      day
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(WORK_SCHEDULE_DAY_ADD, error);
    message('error', error.message);
  }
}

function* updateWorkScheduleDay({ payload }) {
  const { projectId, workScheduleId, dayNo, day } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${
        SKUD_ENTITIES.workSchedules.entityPath
      }/${workScheduleId}/days/${dayNo}`,
      'PUT',
      day
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(WORK_SCHEDULE_DAY_UPDATE, error);
    message('error', error.message);
  }
}

function* removeWorkScheduleDay({ payload }) {
  const { projectId, workScheduleId, dayNo } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${
        SKUD_ENTITIES.workSchedules.entityPath
      }/${workScheduleId}/days/${dayNo}`,
      'DELETE'
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(WORK_SCHEDULE_DAY_REMOVE, error);
    message('error', error.message);
  }
}

function* addAccessGrantedTime({ payload }) {
  const { projectId, workScheduleId, dayNo, accessGrantedTime } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${
        SKUD_ENTITIES.workSchedules.entityPath
      }/${workScheduleId}/days/${dayNo}/access_granted_times`,
      'POST',
      accessGrantedTime
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(ACCESS_GRANTED_TIME_ADD, error);
    message('error', error.message);
  }
}

function* updateAccessGrantedTime({ payload }) {
  const { projectId, workScheduleId, dayNo, timeNo, accessGrantedTime } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${
        SKUD_ENTITIES.workSchedules.entityPath
      }/${workScheduleId}/days/${dayNo}/access_granted_times/${timeNo}`,
      'PUT',
      accessGrantedTime
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(ACCESS_GRANTED_TIME_UPDATE, error);
    message('error', error.message);
  }
}

function* removeAccessGrantedTime({ payload }) {
  const { projectId, workScheduleId, dayNo, timeNo } = payload;
  try {
    const response = yield request(
      `/projects/${projectId}/${
        SKUD_ENTITIES.workSchedules.entityPath
      }/${workScheduleId}/days/${dayNo}/access_granted_times/${timeNo}`,
      'DELETE'
    );

    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(ACCESS_GRANTED_TIME_REMOVE, error);
    message('error', error.message);
  }
}

function* writeSkudDBToControlDevices({ payload: { projectId, controlDeviceIds } }) {
  const requestBody = {
    action: 'WRITE_SKUD_DATABASE',
    projectId,
    parameters: { controlDeviceIds }
  };
  try {
    const response = yield request('/issues', 'POST', requestBody);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }

    if (response.error) {
      throw new Error(response.error);
    }

    /* Если success - то ничего не делаем, созданные задачи придут по WS */
  } catch (error) {
    yield dispatchFail(SKUD_WRITE_DB_TO_CONTROL_DEVICES, error);
    message('error', error.message);
  }
}

function* startReadAccessKeyValue({ payload: { projectId, deviceId, accessKeyId } }) {
  const requestBody = {
    action: 'START_READ_ACCESS_KEY',
    projectId,
    deviceId,
    parameters: { accessKeyId }
  };
  try {
    const response = yield request('/issues', 'POST', requestBody);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }

    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(ACCESS_KEY_START_READ_VALUE, response);
  } catch (error) {
    yield dispatchFail(ACCESS_KEY_START_READ_VALUE, error);
    message('error', error.message);
  }
}

function* stopReadAccessKeyValue({ payload: { projectId, deviceId } }) {
  const requestBody = {
    action: 'STOP_READ_ACCESS_KEY',
    projectId,
    deviceId
  };
  try {
    const response = yield request('/issues', 'POST', requestBody);
    if (!response) {
      throw new Error('Неизвестная ошибка');
    }

    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(ACCESS_KEY_STOP_READ_VALUE, response);
  } catch (error) {
    yield dispatchFail(ACCESS_KEY_STOP_READ_VALUE, error);
    message('error', error.message);
  }
}

export default function* skud() {
  for (let i = 0; i < SKUD_ENTITY_NAMES.length; ++i) {
    const entityName = SKUD_ENTITY_NAMES[i];
    const { entityPath } = SKUD_ENTITIES[entityName];
    yield takeEvery(getAddActionType(entityName), addSkudEntity(entityName, entityPath));
    yield takeEvery(getUpdateActionType(entityName), updateSkudEntity(entityName, entityPath));
    yield takeEvery(getRemoveActionType(entityName), removeSkudEntity(entityName, entityPath));
  }
  yield takeEvery(WORK_SCHEDULE_DAY_ADD, addWorkScheduleDay);
  yield takeEvery(WORK_SCHEDULE_DAY_UPDATE, updateWorkScheduleDay);
  yield takeEvery(WORK_SCHEDULE_DAY_REMOVE, removeWorkScheduleDay);
  yield takeEvery(ACCESS_GRANTED_TIME_ADD, addAccessGrantedTime);
  yield takeEvery(ACCESS_GRANTED_TIME_UPDATE, updateAccessGrantedTime);
  yield takeEvery(ACCESS_GRANTED_TIME_REMOVE, removeAccessGrantedTime);
  yield takeEvery(SKUD_WRITE_DB_TO_CONTROL_DEVICES, writeSkudDBToControlDevices);
  yield takeEvery(ACCESS_KEY_START_READ_VALUE, startReadAccessKeyValue);
  yield takeEvery(ACCESS_KEY_STOP_READ_VALUE, stopReadAccessKeyValue);
}
