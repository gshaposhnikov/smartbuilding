import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';
import {
  ISSUES_LOAD,
  ISSUES_DELETE_FINISHED,
  ISSUES_DELETE_ERROR,
  ISSUES_DELETE_IN_QUEUE,
  ISSUE_DELETE,
  ISSUE_PLAY,
  ISSUE_PAUSE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadIssues() {
  try {
    let issues = yield fetch('/issues');
    if (!issues) {
      throw new Error('Получен пустой ответ на запрос списка задач');
    }
    if (issues.error) {
      throw new Error(issues.error);
    }

    yield dispatchSuccess(ISSUES_LOAD, issues);
  } catch (error) {
    yield dispatchFail(ISSUES_LOAD, error);
  }
}

function* deleteFinishedIssues() {
  try {
    const deletedIssueIds = yield request(`/issues?finished`, 'DELETE');
    if (!deletedIssueIds) {
      throw new Error('Получен пустой ответ на запрос удаления завершённых задач');
    }
    if (deletedIssueIds.error) {
      throw new Error(deletedIssueIds.error);
    }

    yield dispatchSuccess(ISSUES_DELETE_FINISHED, deletedIssueIds);
  } catch (error) {
    yield dispatchFail(ISSUES_DELETE_FINISHED, error);
    message('error', error.message);
  }
}

function* deleteErrorIssues() {
  try {
    const deletedIssueIds = yield request(`/issues?error`, 'DELETE');
    if (!deletedIssueIds) {
      throw new Error('Получен пустой ответ на запрос удаления завершённых с ошибками задач');
    }
    if (deletedIssueIds.error) {
      throw new Error(deletedIssueIds.error);
    }

    yield dispatchSuccess(ISSUES_DELETE_ERROR, deletedIssueIds);
  } catch (error) {
    yield dispatchFail(ISSUES_DELETE_ERROR, error);
    message('error', error.message);
  }
}

function* deleteAllIssuesInQueue() {
  try {
    const deletedIssueIds = yield request(`/issues?queue`, 'DELETE');
    if (!deletedIssueIds) {
      throw new Error('Получен пустой ответ на запрос очистки очереди задач');
    }
    if (deletedIssueIds.error) {
      throw new Error(deletedIssueIds.error);
    }

    yield dispatchSuccess(ISSUES_DELETE_IN_QUEUE, deletedIssueIds);
  } catch (error) {
    yield dispatchFail(ISSUES_DELETE_IN_QUEUE, error);
    message('error', error.message);
  }
}

function* deleteIssue(action) {
  const { issueId } = action.payload;
  try {
    const response = yield request(`/issues/${issueId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(ISSUE_DELETE, issueId);
  } catch (error) {
    yield dispatchFail(ISSUE_DELETE, error);
    message('error', error.message);
  }
}

function* playIssue(action) {
  const { issueId } = action.payload;
  try {
    const response = yield request(`/issues/${issueId}?play`, 'PUT');
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(ISSUE_PLAY, error);
    message('error', error.message);
  }
}

function* pauseIssue(action) {
  const { issueId } = action.payload;
  try {
    const response = yield request(`/issues/${issueId}?pause`, 'PUT');
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(ISSUE_PAUSE, error);
    message('error', error.message);
  }
}

export default function* issues() {
  yield takeEvery(ISSUES_LOAD, loadIssues);
  yield takeEvery(ISSUES_DELETE_FINISHED, deleteFinishedIssues);
  yield takeEvery(ISSUES_DELETE_ERROR, deleteErrorIssues);
  yield takeEvery(ISSUES_DELETE_IN_QUEUE, deleteAllIssuesInQueue);
  yield takeEvery(ISSUE_DELETE, deleteIssue);
  yield takeEvery(ISSUE_PLAY, playIssue);
  yield takeEvery(ISSUE_PAUSE, pauseIssue);
}