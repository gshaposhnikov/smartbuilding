import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  LOG_VIEWS_LOAD,
  LOG_VIEW_ADD,
  LOG_VIEW_UPDATE,
  LOG_VIEW_DELETE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadLogViews({ type }) {
  try {
    const response = yield fetch(`/log_views`);
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type, { logViews: response });
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* addLogView({ type, payload: logView }) {
  try {
    const response = yield request(`/log_views`, 'POST', logView);
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* updateLogView({ type, payload: updatedLogView }) {
  try {
    const response = yield request(`/log_views/${updatedLogView.id}`, 'PUT', updatedLogView);
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

function* deleteLogView({ type, payload: logViewId }) {
  try {
    const response = yield request(`/log_views/${logViewId}`, 'DELETE');
    if (response && response.error) {
      throw new Error(response.error);
    }
    yield dispatchSuccess(type);
  } catch (err) {
    yield dispatchFail(type, err.message);
    message('error', err.message);
  }
}

export default function* logViewsSaga() {
  yield takeEvery(LOG_VIEWS_LOAD, loadLogViews);
  yield takeEvery(LOG_VIEW_ADD, addLogView);
  yield takeEvery(LOG_VIEW_UPDATE, updateLogView);
  yield takeEvery(LOG_VIEW_DELETE, deleteLogView);
}
