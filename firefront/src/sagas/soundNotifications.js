import { takeEvery } from 'redux-saga/effects';

import {
  SOUND_NOTIFICATIONS_LOAD,
  SOUND_NOTIFICATION_ADD,
  SOUND_NOTIFICATION_UPDATE
} from 'constants/actionTypes';

import message from 'helpers/message';
import { fetch, dispatchSuccess, dispatchFail, request } from 'helpers/request';

function* loadSoundNotifications({ type, payload }) {
  try {
    const response = yield fetch(`/sound_notifications`);
    if (!response) {
      throw new Error('Получен пустой ответ');
    }
    if (response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(type, response);
  } catch (error) {
    yield dispatchFail(type, error.message);
  }
}

function* addSoundNotification({ type, payload }) {
  const newSoundNotification = { ...payload };
  try {
    const response = yield request(`/sound_notifications`, 'POST', newSoundNotification);
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

function* updateSoundNotification({ type, payload }) {
  const updatedSoundNotification = { ...payload };
  try {
    const response = yield request(
      `/sound_notifications/${updatedSoundNotification.id}`,
      'PUT',
      updatedSoundNotification
    );
    if (response && response.error) {
      throw new Error(response.error);
    }
  } catch (error) {
    yield dispatchFail(type, error.message);
    message('error', error.message);
  }
}

export default function* soundNotificationsSagas() {
  yield takeEvery(SOUND_NOTIFICATIONS_LOAD, loadSoundNotifications);
  yield takeEvery(SOUND_NOTIFICATION_ADD, addSoundNotification);
  yield takeEvery(SOUND_NOTIFICATION_UPDATE, updateSoundNotification);
}
