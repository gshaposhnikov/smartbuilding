import { takeEvery } from 'redux-saga/effects';
import message from 'helpers/message';

import {
  VIRTUAL_STATES_LOAD,
  VIRTUAL_STATE_CREATE,
  VIRTUAL_STATE_UPDATE,
  VIRTUAL_STATE_DELETE
} from 'constants/actionTypes';

import { fetch, request, dispatchSuccess, dispatchFail } from 'helpers/request';

function* loadVirtualStates(action) {
  const projectId = action.payload;
  try {
    const virtualStates = yield fetch(`/projects/${projectId}/virtual_states`);
    if (!virtualStates) {
      throw new Error('Получен пустой ответ');
    }
    if (virtualStates.error) {
      throw new Error(virtualStates.error);
    }

    yield dispatchSuccess(VIRTUAL_STATES_LOAD, {
      projectId: projectId,
      virtualStates: virtualStates
    });
  } catch (error) {
    yield dispatchFail(VIRTUAL_STATES_LOAD, error.message);
    message('error', error.message);
  }
}

function* createVirtualState(action) {
  const { projectId, params } = action.payload;
  try {
    const virtualState = yield request(`/projects/${projectId}/virtual_states`, 'POST', params);
    if (!virtualState) {
      throw new Error('Получен пустой ответ');
    }
    if (virtualState.error) {
      throw new Error(virtualState.error);
    }

    yield dispatchSuccess(VIRTUAL_STATE_CREATE, {
      projectId: projectId,
      newVirtualState: virtualState
    });
    message('success', `Состояние добавлено`);
  } catch (error) {
    yield dispatchFail(VIRTUAL_STATE_CREATE, error.message);
    message('error', error.message);
  }
}

function* updateVirtualState(action) {
  const { projectId, virtualStateId, params } = action.payload;
  try {
    const virtualState = yield request(
      `/projects/${projectId}/virtual_states/${virtualStateId}`,
      'PUT',
      params
    );
    if (!virtualState) {
      throw new Error('Получен пустой ответ');
    }
    if (virtualState.error) {
      throw new Error(virtualState.error);
    }

    yield dispatchSuccess(VIRTUAL_STATE_UPDATE, {
      projectId: projectId,
      updatedVirtualState: virtualState
    });
    message('success', `Параметры состояния изменены`);
  } catch (error) {
    yield dispatchFail(VIRTUAL_STATE_UPDATE, error.message);
    message('error', error.message);
  }
}

function* deleteVirtualState(action) {
  const { projectId, virtualStateId } = action.payload;
  try {
    const response = yield request(
      `/projects/${projectId}/virtual_states/${virtualStateId}`,
      'DELETE'
    );
    if (response && response.error) {
      throw new Error(response.error);
    }

    yield dispatchSuccess(VIRTUAL_STATE_DELETE, {
      projectId: projectId,
      virtualStateId: virtualStateId
    });
    message('success', `Состояние удалено`);
  } catch (error) {
    yield dispatchFail(VIRTUAL_STATE_DELETE, error.message);
    message('error', error.message);
  }
}

export default function* virtualStatesSagas() {
  yield takeEvery(VIRTUAL_STATES_LOAD, loadVirtualStates);
  yield takeEvery(VIRTUAL_STATE_CREATE, createVirtualState);
  yield takeEvery(VIRTUAL_STATE_UPDATE, updateVirtualState);
  yield takeEvery(VIRTUAL_STATE_DELETE, deleteVirtualState);
}
