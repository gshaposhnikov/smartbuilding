package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

public abstract class IssueTask extends Task{

    String issueId;

    IssueTask(ControllerStateMachineContext stateMachineContext, String issueId) {
        super(stateMachineContext);
        this.issueId = issueId;
    }

    public String getIssueId() {
        return issueId;
    }
}
