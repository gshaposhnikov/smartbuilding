package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public abstract class Task {

    protected ControllerStateMachineContext stateMachineContext;

    Task(ControllerStateMachineContext stateMachineContext){
        this.stateMachineContext = stateMachineContext;
    }

    public abstract State execute(long currentTimeMs);
}
