package ru.rubezh.firesec.nt.plugin.driver.transport;

import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.plugin.driver.client_endpoint.WebSocketClientEndpoint;

import java.io.IOException;

public class ControllerTransport<ByteBuffer> implements Transport {

    private ConnectHandler connectHandler;
    private ReceiveHandler receiveHandler;
    private SendHandler sendHandler;
    private SendReceiveFailedHandler sendReceiveFailedHandler;
    private String receivedData;
    private String dataToSend;
    private Boolean receivable = false;

    private int maxWaitSendTimeMs = 10000;

    private Logger logger;

    private long lastActivityTime;

    private WebSocketClientEndpoint wsClientEndpoint;
    private long sendRequestedTimeMs;

    public ControllerTransport(Logger logger, WebSocketClientEndpoint wsClientEndpoint){
        this.logger = logger;
        this.wsClientEndpoint = wsClientEndpoint;
        logger.warn("CONTROLLER TRANSPORT CREATED");
    }

    @Override
    public void connect(long currentTimeMs, ConnectHandler connectHandler, DisconnectHandler disconnectHandler) {
        logger.warn("TRANSPORT CONNECT");
        lastActivityTime = currentTimeMs;
        assert wsClientEndpoint.isConnected();
        assert this.connectHandler == null;

        this.connectHandler = connectHandler;
    }

    @Override
    public int send(long currentTimeMs, Object rawData, SendHandler sendHandler, SendReceiveFailedHandler failHandler) {
        logger.warn("TRANSPORT SEND. req time: " + currentTimeMs);

        this.dataToSend = (String)rawData;
        this.sendHandler = sendHandler;
        this.sendReceiveFailedHandler = failHandler;
        this.sendRequestedTimeMs = currentTimeMs;

        lastActivityTime = currentTimeMs;
        return 0;
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler receiveHandler,
                        SendReceiveFailedHandler failHandler) {
        logger.warn("TRANSPORT RECEIVE");
        receivable = true;
        lastActivityTime = currentTimeMs;

        this.receiveHandler = receiveHandler;
        this.sendReceiveFailedHandler = failHandler;
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler receiveHandler,
                        SendReceiveFailedHandler failHandler, SendReceiveFailedHandler timeoutHandler) {
        logger.warn("TRANSPORT RECEIVE");
        lastActivityTime = currentTimeMs;
    }

    @Override
    public void disconnect(long currentTimeMs) throws IOException {
        logger.warn("TRANSPORT DISCONNECT");
        lastActivityTime = currentTimeMs;
    }

    @Override
    public void process(long currentTimeMs){
        if (receivable && wsClientEndpoint.isConnected()) {
            receiveData();
        }
        receivable = false;
        lastActivityTime = currentTimeMs;

        processSend(currentTimeMs);
        processReceive(currentTimeMs);
    }

    private void processReceive(long currentTimeMs) {
        if (receiveHandler == null){
            return;
        }
        if (wsClientEndpoint != null && wsClientEndpoint.isConnected() && receivedData != null ) {
            String data = receivedData;
            ReceiveHandler<String> disposableReceiveHandler = receiveHandler;
            this.receivedData = null;
            this.receiveHandler = null;

            disposableReceiveHandler.onReceived(currentTimeMs, 0, data);
        } else {
            sendReceiveFailedHandler.onFailed(currentTimeMs, 0);
        }
    }

    private void receiveData(){
        String data = wsClientEndpoint.getCurrentData();
        if (data != null && !data.isEmpty()){
            receivedData = data;
        }
    }

    private void processSend(long currentTimeMs) {
        logger.error("send handler null: " + (sendHandler == null));
        if (sendHandler == null){
            return;
        }

        logger.error("send req time: " + sendRequestedTimeMs);
        logger.error("curr time: " + currentTimeMs);
        logger.error("DTS: " + dataToSend);
        if (wsClientEndpoint != null && wsClientEndpoint.isConnected()
                && (sendRequestedTimeMs + getMaxWaitSendTimeMs()) > currentTimeMs
                && sendData()) {
            SendHandler disposableSendHandler = sendHandler;
            this.sendHandler = null;
            this.dataToSend = null;

            disposableSendHandler.onSended(currentTimeMs, 0);
        } else {
            sendReceiveFailedHandler.onFailed(currentTimeMs, 0);
        }
    }

    private boolean sendData(){
        return wsClientEndpoint.sendMessage(dataToSend);
    }

    @Override
    public long getLastActivityTime() {
        return lastActivityTime;
    }

    public int getMaxWaitSendTimeMs() {
        return maxWaitSendTimeMs;
    }

    public String getDataToSend() {
        return dataToSend;
    }
}
