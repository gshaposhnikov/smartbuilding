package ru.rubezh.firesec.nt.plugin.driver.controller;

import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.amqp.message.alert.ActiveProjectValidateStatus;
import ru.rubezh.firesec.nt.amqp.message.alert.DriverRegisteredAlert;
import ru.rubezh.firesec.nt.amqp.message.response.CreateIssueDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;
import ru.rubezh.firesec.nt.plugin.driver.client_endpoint.WebSocketClientEndpoint;
import ru.rubezh.firesec.nt.plugin.driver.clock.MultiClientClock;
import ru.rubezh.firesec.nt.plugin.driver.transport.ControllerTransport;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.codec.WebSocketMessageCodec;

import java.util.*;

public class ControllerDriver extends Driver {

    private DeviceProfile controllerDeviceProfile;
    private WebSocketMessageCodec messageCodec;
    private WebSocketClientEndpoint wsClientEndpoint;
    private List<ControllerExternalRequestHandler> externalRequestHandlers = new ArrayList<>();
    /**
     * Карта обработчиков внешних запросов по идентификаторам ВСЕХ устройств
     * (сам обработчик один на конечный автомат, т.е. на прибор)
     */
    private Map<String, ControllerExternalRequestHandler> externalRequestHandlersByAnyDeviceId = new HashMap<>();

    /**
     * Карта обработчиков внешних запросов по идентификаторам зон (сам
     * обработчик один на конечный автомат, т.е. на прибор)
     */
    private Map<String, List<ControllerExternalRequestHandler>> externalRequestHandlersByRegionId = new HashMap<>();

    public ControllerDriver(Logger logger) {
        super(logger);
    }

    @Override
    protected boolean fillMapsAndValidateProjectAndFetchStateMachines(List<ActivationValidateMessage> validateMessages,
                                                                      List<ActivationValidateMessage> skudValidateMessages,
                                                                      List<StateMachine> stateMachines) {
        List<ActiveDevice> rootControllerDevices = getRootControllerDevices();
        allControlDevices.addAll(rootControllerDevices);

        /*
         * Формируем карту дочерних устройств по идентификаторам приборов, к
         * которым они подключены
         */
        devicesByControlDeviceIds = getDevicesByControlDeviceIds();
        /* И обратную карту id устройства -> прибор */
        for (String controlDeviceId : devicesByControlDeviceIds.keySet()) {
            for (ActiveDevice childDevice : devicesByControlDeviceIds.get(controlDeviceId)) {
                controlDevicesByChildDeviceIds.put(childDevice.getId(), allDevicesByIds.get(controlDeviceId));
            }
        }

        /*
         * Далее создадим КА всех приборов, подключенных непосредственно по USB
         */
        for (ActiveDevice controlDevice : rootControllerDevices) {
            /* создаем транспорт для подключения по WebSocket */
            Transport transport = new ControllerTransport(logger, wsClientEndpoint);
            /* создаем контекст конечного автомата */
            ControllerStateMachineContext controllerStateMachineContext = new ControllerStateMachineContext(logger,
                    driverProfile,
                    driverProfileViews,
                    controllerDeviceProfile,
                    supportedDeviceProfiles,
                    driverAlertSender,
                    transport,
                    new MultiClientClock(),
                    devicesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDevicesByChildDeviceIds,
                    allDevicesByIds,
                    controlDevice,
                    regionsByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    messageCodec);
            addExternalRequestHandler(controlDevice, controllerStateMachineContext);
            /* добавляем конечный автомат в общий список */
            stateMachines.add(new ControllerStateMachine(controllerStateMachineContext));
        }
        return true;
    }

    private void addExternalRequestHandler(ActiveDevice controlDevice,
                                           ControllerStateMachineContext stateMachineContext) {
        ControllerExternalRequestHandler externalRequestHandler = new ControllerExternalRequestHandler(
                stateMachineContext);
        externalRequestHandlers.add(externalRequestHandler);
        bindExternalRequestHandlerToDeviceId(controlDevice.getId(), externalRequestHandler);
        if (devicesByControlDeviceIds.containsKey(controlDevice.getId())) {
            for (ActiveDevice activeDevice : devicesByControlDeviceIds.get(controlDevice.getId()))
                bindExternalRequestHandlerToDeviceId(activeDevice.getId(), externalRequestHandler);
        }
        if (regionsByControlDeviceIds.containsKey(controlDevice.getId())) {
            for (ActiveRegion activeRegion : regionsByControlDeviceIds.get(controlDevice.getId()))
                bindExternalRequestHandlerToRegionId(activeRegion.getId(), externalRequestHandler);
        }
    }

    private void bindExternalRequestHandlerToDeviceId(String deviceId,
                                                      ControllerExternalRequestHandler externalRequestHandler) {
        externalRequestHandlersByAnyDeviceId.put(deviceId, externalRequestHandler);
    }

    private void bindExternalRequestHandlerToRegionId(String regionId,
                                                      ControllerExternalRequestHandler externalRequestHandler) {
        if (!externalRequestHandlersByRegionId.containsKey(regionId)) {
            externalRequestHandlersByRegionId.put(regionId, new ArrayList<>());
        }
        externalRequestHandlersByRegionId.get(regionId).add(externalRequestHandler);
    }

    /**
     * Получить список приборов, подключенных по USB напрямую.
     *
     * @return список приборов
     */
    private List<ActiveDevice> getRootControllerDevices() {
        List<ActiveDevice> rootControllerDevices = new ArrayList<>();
        for (ActiveDevice device : inServiceDevicesToSet) {
            if (device.getParentDeviceId() == null
                    && device.getDeviceProject().getDeviceProfileId().equals(controllerDeviceProfile.getId())) {
                rootControllerDevices.add(device);
            }
        }
        return rootControllerDevices;
    }

    /**
     * Получить списки устройств, сгруппированных по идентификаторам приборов, к
     * которым они подключены
     *
     * В результат не попадут устройства, подключенные к приборам не 2ОП3.
     *
     * @return карта идентификаторам приборов -> список устройств
     */
    private Map<String, List<ActiveDevice>> getDevicesByControlDeviceIds() {
        for (ActiveDevice device : inServiceDevicesToSet) {
            if (device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTROL) {
                List<ActiveDevice> childDevices = new ArrayList<>();
                /*
                 * Если прибор не данного драйвера (не контроллера), то список
                 * устройств - потомков должен остаться пустым
                 */
                if (device.getDeviceProject().getDeviceProfileId().equals(controllerDeviceProfile.getId())) {
                    fetchChildDevices(device, controllerDeviceProfile.getOutputConnectionType(), childDevices);
                    fetchChildDevices(device, ConnectionInterfaceType.INTERNAL_BUS, childDevices);
                }
                devicesByControlDeviceIds.put(device.getId(), childDevices);
            }
        }
        return devicesByControlDeviceIds;
    }

    /**
     * Получить список устройств - непосредственных предков
     *
     * @param device
     *            корневое устройство, чьи предки нужны
     * @return список устройств
     */
    private void fetchChildDevices(ActiveDevice device, ConnectionInterfaceType connectionInterfaceType, List<ActiveDevice> childDevices) {
        for (ActiveDevice childDevice: inServiceDevicesToSet) {
            String parentDeviceId = childDevice.getParentDeviceId();
            if (parentDeviceId != null && parentDeviceId.equals(device.getId())) {
                DeviceProfile deviceProfile = supportedDeviceProfiles.get(childDevice.getDeviceProject().getDeviceProfileId());
                if (deviceProfile != null && deviceProfile.getConnectionTypes().contains(connectionInterfaceType)) {
                    if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER)
                        fetchChildDevices(childDevice, connectionInterfaceType, childDevices);
                    childDevices.add(childDevice);
                }
            }
        }
    }

    public void setControllerDeviceProfile(DeviceProfile controllerDeviceProfile) {
        this.controllerDeviceProfile = controllerDeviceProfile;
    }

    @Override
    public void onStart(long currentTimeMs) {
        if (requestQueueName == null || alertsExchangeName == null || driverRequestHandler == null
                || driverAlertSender == null || rabbitTemplate == null || listenerContainer == null
                || driverId == null) {
            throw new IllegalStateException(
                    "Required params for Driver are: requestQueueName, alertsExchangeName, driverRequestHandler, " +
                            "driverAlertSender, rabbitTemplate, listenerContainer, driverId");
        } else {
            initializeRabbit();
            logger.info("Driver initialized. Id: {}. Request queue: {}. Alerts exchange: {}", driverId,
                    requestQueueName, alertsExchangeName);

            DriverRegisteredAlert alert = new DriverRegisteredAlert();
            alert.setDriverId(driverId);
            alert.setRequestQueue(requestQueueName);
            alert.setAlertsExchange(alertsExchangeName);
            driverAlertSender.send(alert);
        }
    }

    @Override
    public void process(long currentTimeMs) {
        if (needSetNewProject) {
            needSetNewProject = false;

            stopAllStateMachines(currentTimeMs);
            stateMachines.clear();

            initDevicesHash();
            allControlDevices = new ArrayList<>();
            devicesByControlDeviceIds = new HashMap<>();
            controlDevicesByChildDeviceIds = new HashMap<>();
            regionsByControlDeviceIds = new HashMap<>();
            controlDevicesByRegionIds = new HashMap<>();
            virtualStatesByControlDeviceIds = new HashMap<>();
            scenariosByControlDeviceIds = new HashMap<>();
            externalScenariosByControlDeviceIds = new HashMap<>();
            controlDeviceIdsByScenarioIds = new HashMap<>();
            externalExecutiveDevicesByControlDeviceIds = new HashMap<>();

            /*
             * Результат валидации проекта может зависеть как от наличия в нем
             * общих ошибок, так и от ошибок по запуску каждого отдельного
             * конечного автомата
             */
            validateStatus = new ActiveProjectValidateStatus();
            validateStatus.setProjectId(inServiceProjectIdToSet);
            commonValidateMessages = new ArrayList<>();
            skudValidateMessages = new ArrayList<>();

            boolean projectValid = fillMapsAndValidateProjectAndFetchStateMachines(commonValidateMessages,
                    skudValidateMessages, stateMachines);
            /*
             * Запускаем конечные автоматы с запоминанием общего результата
             */
            for (StateMachine stateMachine : stateMachines) {
                projectValid &= stateMachine.start(currentTimeMs, commonValidateMessages);
            }
            if (projectValid) {
                /* Запускаем имеющиеся задачи */
                for (Issue inServiceIssue : inServiceIssuesToSet)
                    startIssue(inServiceIssue);
            } else {
                /*
                 * Сразу останавливаем все конечные автоматы, если есть ошибка
                 * запуска хотя бы в одном или были общие ошибки валидации
                 * проекта
                 */
                stopAllStateMachines(currentTimeMs);
                stateMachines.clear();
            }
            validateStatus.setValidateSucceded(projectValid && stateMachines.size() > 0);

            validateStatus.setValidateMessages(new ArrayList<>(commonValidateMessages));
            validateStatus.addValidateMessages(skudValidateMessages);
            driverAlertSender.send(validateStatus);
        }
        /*
         * Удаляемые конечные автоматы должны выполнить еще одну итерацию, чтобы
         * завершить остановку
         */
        for (StateMachine stateMachine : stateMachinesToRemove)
            stateMachine.step(currentTimeMs);
        stateMachinesToRemove.clear();

        List<Long> lastSMActivityTimesMs = new ArrayList<>();
        for (StateMachine stateMachine : stateMachines) {
            stateMachine.step(currentTimeMs);
            lastSMActivityTimesMs.add(stateMachine.getLastActivityTime());
        }

        lastCurrentTimeMs = currentTimeMs;
        this.lastSMActivityTimesMs = lastSMActivityTimesMs;
    }

    @Override
    public CreateIssueDriverResponse createIssue(Issue issue) {
        CreateIssueDriverResponse response = new CreateIssueDriverResponse();
        Set<ControllerExternalRequestHandler> requestHandlers = new HashSet<>(externalRequestHandlers);
        if (!requestHandlers.isEmpty()){
            requestHandlers.forEach(externalRequestHandler -> externalRequestHandler.handleMessage(issue, response));
        } else {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
        }
        return response;
    }

    @Override
    public void startIssue(Issue issue) {
        ControllerExternalRequestHandler requestHandler = externalRequestHandlersByAnyDeviceId.get(issue.getDeviceId());
        requestHandler.startIssue(issue);
    }

    public WebSocketMessageCodec getMessageCodec() {
        return messageCodec;
    }

    public void setMessageCodec(WebSocketMessageCodec messageCodec) {
        this.messageCodec = messageCodec;
    }

    public WebSocketClientEndpoint getWsClientEndpoint() {
        return wsClientEndpoint;
    }

    public void setWsClientEndpoint(WebSocketClientEndpoint wsClientEndpoint) {
        this.wsClientEndpoint = wsClientEndpoint;
    }

    public void setExternalRequestHandlers(List<ControllerExternalRequestHandler> externalRequestHandlers) {
        this.externalRequestHandlers = externalRequestHandlers;
    }
}
