package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

public enum TimerNames {
    EXECUTE_TASK("executeTask");

    TimerNames(String name){
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }
}
