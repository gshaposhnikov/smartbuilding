package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

public class Idle extends AbstractHandler {

    @Override
    public State getCurrentState() {
        return State.IDLE;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().connect(currentTimeMs);
        stateMachineContext.getLogger().debug(getLogMessage());
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.EXECUTE_TASK.getName(),
                200, false);
        return State.WAITING_TASK;
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return null;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return null;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return null;
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return null;
    }

    @Override
    public String getLogMessage(){
        return "IDLE: on get logger";
    }
}
