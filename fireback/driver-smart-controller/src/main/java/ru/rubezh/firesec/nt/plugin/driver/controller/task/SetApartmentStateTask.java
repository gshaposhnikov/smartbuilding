package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public class SetApartmentStateTask extends SetValueTask {

    SetApartmentStateTask(ControllerStateMachineContext stateMachineContext, int stateValue, String issueId) {
        super(stateMachineContext, stateValue, issueId);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setApartmentState(currentTimeMs, value);
        sendIssueStatusProgress(issueId);
        return State.STATES_WAITING;
    }
}
