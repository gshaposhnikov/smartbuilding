package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

import java.util.List;

public abstract class AbstractHandler implements StateHandler{

    protected ControllerStateMachineContext stateMachineContext;

    @Override
    public StateHandler cloneBinded(ControllerStateMachineContext stateMachineContext) {
        Idle handler = new Idle();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    protected String getLogMessage() {
        StringBuilder message = new StringBuilder()
                .append("Controller (address path: ")
                .append(stateMachineContext.getControllerDevice().getAddressPath())
                .append(") wrong event for the state ")
                .append(getCurrentState().toString());
        return message.toString();
    }

    public State onStatesReceived(long currentTimeMs, SetEntityStates alert, List<SetActiveDeviceMonitorableValues> monitorableValuesAlert){
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    public State onStateSent(long currentTimeMs){
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }

    public State onExchangeFailure(long currentTimeMs){
        stateMachineContext.getLogger().trace(getLogMessage());
        return getCurrentState();
    }
}
