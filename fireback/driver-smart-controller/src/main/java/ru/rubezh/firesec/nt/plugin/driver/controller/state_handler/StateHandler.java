package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

import java.util.List;

public interface StateHandler {

    State getCurrentState();
    StateHandler cloneBinded(ControllerStateMachineContext stateMachineContext);
    boolean isConnected();

    State onConnectRequest(long currentTimeMs);

    State onConnected(long currentTimeMs);
    State onConnectFailure(long currentTimeMs);
    State onDisconnected(long currentTimeMs);
    State onTimer(long currentTimeMs, String timerName);

    State onExchangeFailure(long currentTimeMs);

    State onStatesReceived(long currentTimeMs, SetEntityStates alert,
                           List<SetActiveDeviceMonitorableValues> monitorableValuesAlert);

    State onStateSent(long currentTimeMs);
}
