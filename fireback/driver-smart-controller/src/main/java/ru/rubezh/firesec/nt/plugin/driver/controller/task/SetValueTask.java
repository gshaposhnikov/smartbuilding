package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.amqp.message.alert.IssueProgress;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public abstract class SetValueTask extends IssueTask {

    int value;

    SetValueTask(ControllerStateMachineContext stateMachineContext, int value, String issueId) {
        super(stateMachineContext, issueId);
        this.value = value;
    }

    void sendIssueStatusProgress(String issueId){
        IssueProgress alert = new IssueProgress();
        alert.setIssueId(issueId);
        alert.setStatus(IssueStatus.IN_PROGRESS);
        alert.setStatusMessage("");
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public State execute(long currentTimeMs) {
        return State.WAITING_TASK;
    }
}
