package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import ru.rubezh.firesec.nt.amqp.message.alert.IssueProgress;
import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.task.SetValueTask;

import java.util.List;

public class StatesWaiting extends AbstractHandler {

    @Override
    public State getCurrentState() {
        return State.STATES_WAITING;
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public StateHandler cloneBinded(ControllerStateMachineContext stateMachineContext) {
        StatesWaiting handler = new StatesWaiting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onStatesReceived(long currentTimeMs, SetEntityStates statesAlert,
                                  List<SetActiveDeviceMonitorableValues> monitorableValuesAlerts){
        stateMachineContext.getDriverAlertSender().send(statesAlert);
        for (SetActiveDeviceMonitorableValues alert: monitorableValuesAlerts) {
            stateMachineContext.getDriverAlertSender().send(alert);
        }
        return setTimerAndCompleteTask(currentTimeMs);
    }

    @Override
    public State onStateSent(long currentTimeMs){
        SetValueTask task = (SetValueTask) stateMachineContext.getTaskManager().getCurrentTask();
        stateMachineContext.getLogger().debug("SW: data successfully sent to transport");
        sendIssueStatusFinished(task.getIssueId());
        return setTimerAndCompleteTask(currentTimeMs);
    }

    @Override
    public State onExchangeFailure(long currentTimeMs) {
        stateMachineContext.getLogger().error("SW: error while receiving/sending a message to transport");
        return setTimerAndCompleteTask(currentTimeMs);
    }

    private State setTimerAndCompleteTask(long currentTimeMs){
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.EXECUTE_TASK.getName(),
                200, false);
        return stateMachineContext.getTaskManager().completeCurrentTask();
    }

    private void sendIssueStatusFinished(String issueId) {
        IssueProgress alert = new IssueProgress();
        alert.setIssueId(issueId);
        alert.setStatus(IssueStatus.FINISHED);
        alert.setStatusMessage("");
        stateMachineContext.getDriverAlertSender().send(alert);
    }

}
