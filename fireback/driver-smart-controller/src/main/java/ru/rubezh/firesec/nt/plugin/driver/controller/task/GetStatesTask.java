package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public class GetStatesTask extends Task {

    GetStatesTask(ControllerStateMachineContext stateMachineContext) {
        super(stateMachineContext);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getStates(currentTimeMs);
        return State.STATES_WAITING;
    }
}
