package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

import java.util.EnumMap;
import java.util.List;

public class StateRelay implements StateHandler {

    ControllerStateMachineContext stateMachineContext;

    State state = State.IDLE;
    private EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);

    private State setState(State state) {
        if (state != this.state) {
            stateMachineContext.getLogger().info("Controller (address path: {}) state transition: {} -> {}",
                    stateMachineContext.getControllerDevice().getAddressPath(),
                    this.state, state);
            this.state = state;
        }
        return this.state;
    }

    public StateRelay(ControllerStateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
        for (State state: State.values()) {
            if (state != State.NONE)
                handlersByState.put(state, State.getHandler(state).cloneBinded(stateMachineContext));
        }
    }

    @Override
    public State getCurrentState() {
        return state;
    }

    @Override
    public StateHandler cloneBinded(ControllerStateMachineContext stateMachineContext) {
        return new StateRelay(stateMachineContext);
    }

    @Override
    public boolean isConnected() {
        return handlersByState.get(state).isConnected();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().debug("SR: on connect request");
        return setState(handlersByState.get(state).onConnectRequest(currentTimeMs));
    }

    @Override
    public State onConnected(long currentTimeMs) {
        return null;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        return null;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        return null;
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().debug("SR: on timer");
        return setState(handlersByState.get(state).onTimer(currentTimeMs, timerName));
    }

    @Override
    public State onExchangeFailure(long currentTimeMs) {
        return null;
    }

    public State onStatesReceived(long currentTimeMs, SetEntityStates alert, List<SetActiveDeviceMonitorableValues> monitorableValuesAlert){
        stateMachineContext.getLogger().debug("SR: on sates received");
        return setState(handlersByState.get(state).onStatesReceived(currentTimeMs, alert, monitorableValuesAlert));
    }

    @Override
    public State onStateSent(long currentTimeMs) {
        stateMachineContext.getLogger().debug("SR: on sates received");
        return setState(handlersByState.get(state).onStateSent(currentTimeMs));
    }
}
