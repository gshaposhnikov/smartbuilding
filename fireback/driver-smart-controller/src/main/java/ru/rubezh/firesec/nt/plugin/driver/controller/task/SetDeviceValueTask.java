package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

public abstract class SetDeviceValueTask extends SetValueTask{

    int regionIndex;
    DeviceType deviceType;
    int deviceNo;
    boolean withPrefix;

    SetDeviceValueTask(ControllerStateMachineContext stateMachineContext, int value, String issueId, int regionIndex,
                       DeviceType deviceType, int deviceNo, boolean withPrefix) {
        super(stateMachineContext, value, issueId);
        this.regionIndex = regionIndex;
        this.deviceType = deviceType;
        this.deviceNo = deviceNo;
        this.withPrefix = withPrefix;
    }

    public int getRegionIndex() {
        return regionIndex;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public int getDeviceNo() {
        return deviceNo;
    }

    public boolean isWithPrefix() {
        return withPrefix;
    }
}
