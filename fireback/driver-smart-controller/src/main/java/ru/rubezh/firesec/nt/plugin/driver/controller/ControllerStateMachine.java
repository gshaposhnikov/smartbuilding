package ru.rubezh.firesec.nt.plugin.driver.controller;

import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;

import java.io.IOException;
import java.util.List;

public class ControllerStateMachine implements StateMachine {

    private ControllerStateMachineContext stateMachineContext;
    private boolean started;
    private long startTimeMs;

    public ControllerStateMachine(ControllerStateMachineContext controllerStateMachineContext) {
        this.stateMachineContext = controllerStateMachineContext;
    }

    @Override
    public boolean start(long currentTimeMs, List<ActivationValidateMessage> validateMessages) {
        stateMachineContext.getStateHandler().onConnectRequest(currentTimeMs);
        startTimeMs = currentTimeMs;
        started = true;
        return true;
    }

    @Override
    public void step(long currentTimeMs) {
        if (isStarted()){
            stateMachineContext.getClock().process(currentTimeMs);
            try {
                stateMachineContext.getTransport().process(currentTimeMs);
            } catch (IOException e) {
                stateMachineContext.getLogger().error("TRANSPORT IO EXCEPTION");
            }
        }
    }

    @Override
    public void stop(long currentTimeMs) {
        if (isStarted()) {
        }
    }

    @Override
    public long getLastActivityTime() {
        long lastActivityTimeMs = stateMachineContext.getTransport().getLastActivityTime();
        if (lastActivityTimeMs == 0)
            lastActivityTimeMs = startTimeMs;
        return lastActivityTimeMs;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}
