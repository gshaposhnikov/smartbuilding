package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

import java.util.LinkedList;
import java.util.Queue;

public class ControllerTaskManager {

    private ControllerStateMachineContext stateMachineContext;
    private State sourceState;
    private Queue<Task> taskQueue = new LinkedList<>();
    private Task currentTask;

    public ControllerTaskManager(ControllerStateMachineContext stateMachineContext, State sourceState) {
        this.stateMachineContext = stateMachineContext;
        this.sourceState = sourceState;
    }

    public State executeNextTask(long currentTimeMs){
        if (!taskQueue.isEmpty() && !Driver.isExclusiveMode()) {
            currentTask = taskQueue.remove();
            return currentTask.execute(currentTimeMs);
        } else {
            return sourceState;
        }
    }

    public State completeCurrentTask(){
        currentTask = null;
        return sourceState;
    }

    public void enqueueGetStates(){
        GetStatesTask task = new GetStatesTask(stateMachineContext);
        taskQueue.add(task);
    }

    public void enqueueSetDeviceStates(int regionIndex, DeviceType deviceType, int deviceNo, boolean withPrefix,
                                       int stateValue, String issueId) {
        SetDeviceStateTask task = new SetDeviceStateTask(stateMachineContext, stateValue, issueId,
        regionIndex, deviceType, deviceNo, withPrefix);
        taskQueue.add(task);
    }

    public void enqueueSetRegionStates(int regionIndex, int stateValue, String issueId){
        SetRegionStateTask task = new SetRegionStateTask(stateMachineContext, regionIndex, stateValue, issueId);
        taskQueue.add(task);
    }

    public void enqueueSetApartmentState(int stateValue, String issueId) {
        SetApartmentStateTask task = new SetApartmentStateTask(stateMachineContext, stateValue, issueId);
        taskQueue.add(task);
    }

    public void enqueueSetDeviceMonitorableValue(int regionIndex, DeviceType deviceType, int deviceNo,
                                                 boolean withPrefix, int paramValue, String issueId, String paramName) {
        SetDeviceMonitorableValueTask task = new SetDeviceMonitorableValueTask(stateMachineContext, paramValue, issueId,
                regionIndex, deviceType, deviceNo, withPrefix, paramName);
        taskQueue.add(task);
    }

    public ControllerStateMachineContext getStateMachineContext() {
        return stateMachineContext;
    }

    public void setStateMachineContext(ControllerStateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    public Task getCurrentTask() {
        return currentTask;
    }
}
