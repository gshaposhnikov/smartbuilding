package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import java.util.EnumMap;

public enum State {

    IDLE(new Idle()), CONNECTING(new Connecting()), STATES_WAITING(new StatesWaiting()),
    WAITING_TASK(new WaitingTask()), NONE(null);

    private final StateHandler handler;

    State(StateHandler handler) {
        this.handler = handler;
    }

    private static final EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);

    static {
        for (State state : State.values()) {
            handlersByState.put(state, state.handler);
        }
    }

    public static StateHandler getHandler(State state) {
        return handlersByState.get(state);
    }
}
