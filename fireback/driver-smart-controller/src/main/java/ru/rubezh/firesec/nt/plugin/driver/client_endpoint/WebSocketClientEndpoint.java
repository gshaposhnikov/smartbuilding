package ru.rubezh.firesec.nt.plugin.driver.client_endpoint;

import org.apache.logging.log4j.Logger;

import javax.annotation.PreDestroy;
import javax.websocket.*;
import java.io.IOException;
import java.net.URI;

@ClientEndpoint
public class WebSocketClientEndpoint {

    private Logger logger;
    private Session userSession = null;
    private String currentData = "";

    public WebSocketClientEndpoint(URI endpointURI, Logger logger) {
        try {
            this.logger = logger;
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Callback hook for Connection open events.
     *
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
        logger.warn("WS ENDPOINT: connection attempt" );
        this.userSession = userSession;
    }

    /**
     * Callback hook for Connection close events.
     *
     * @param userSession the userSession which is getting closed.
     * @param reason the reason for connection close
     */
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
        logger.warn("WS ENDPOINT: connection attempt, {}", reason.getReasonPhrase());
        try {
            userSession.close();
        } catch (IOException e) {
            logger.error("WS ENDPOINT: close unsuccessful", e);
        }
    }

    @PreDestroy
    public void disconnect(){
        CloseReason closeReason = new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE,
                "Bean is being destroyed");
        onClose(this.userSession, closeReason);
    }

    @OnMessage
    public void onMessage(String message){
        logger.debug("WS ENDPOINT: received message from server");
        currentData = message;
    }

    public String getCurrentData() {
        return currentData;
    }

    public boolean sendMessage(String data){
        try {
            logger.error("successful send");
            userSession.getBasicRemote().sendText(data);
        } catch (IOException e) {
            logger.error("WS ENDPOINT: error while sending message to server", e);
            return false;
        }
        return true;
    }

    public boolean isConnected(){
        return userSession.isOpen();
    }
}
