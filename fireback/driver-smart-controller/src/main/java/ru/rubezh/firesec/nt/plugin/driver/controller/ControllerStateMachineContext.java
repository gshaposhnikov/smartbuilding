package ru.rubezh.firesec.nt.plugin.driver.controller;

import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.clock.MultiClientClock;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.StateHandler;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.StateRelay;
import ru.rubezh.firesec.nt.plugin.driver.controller.task.ControllerTaskManager;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.codec.WebSocketMessageCodec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControllerStateMachineContext {

    private Logger logger;

    private Transport transport;

    private Clock clock;
    private DriverAlertSender driverAlertSender;
    private DriverProfile driverProfile;
    private List<DriverProfileView> driverProfileViews;
    private Map<String, DeviceProfileView> deviceProfileViewsByIds;
    private DeviceProfile controllerDeviceProfile;
    private Map<String, DeviceProfile> supportedDeviceProfiles;
    private int taskExecuteInterval = 200; // нельзя устанавливать другие интревалы меньше этого

    private StateHandler stateHandler;
    private ControllerTaskManager taskManager;
    private WebSocketMessageCodec messageCodec;

    /* модель данных */
    private ActiveDevice controllerDevice;
    private List<ActiveDevice> childDevices;
    private Map<String, ActiveDevice> controlDevicesByChildDeviceIds;
    private Map<String, ActiveDevice> allProjectDevicesByIds;
    private List<ActiveRegion> regions;

    private ControllerActionExecutor actionExecutor;

    public ControllerStateMachineContext(Logger logger,
                                         DriverProfile driverProfile,
                                         List<DriverProfileView> driverProfileViews,
                                         DeviceProfile controllerDeviceProfile,
                                         Map<String, DeviceProfile> supportedDeviceProfiles,
                                         DriverAlertSender driverAlertSender,
                                         Transport<ByteBuffer> transport,
                                         MultiClientClock multiClientClock,
                                         List<ActiveDevice> childDevices,
                                         Map<String, ActiveDevice> controlDevicesByChildDeviceIds,
                                         Map<String, ActiveDevice> allDevicesByIds,
                                         ActiveDevice controlDevice,
                                         List<ActiveRegion> activeRegions,
                                         WebSocketMessageCodec messageCodec) {
        actionExecutor = new ControllerActionExecutor(this);

        this.logger = logger;
        this.clock = multiClientClock;
        this.driverAlertSender = driverAlertSender;
        this.transport = transport;
        this.driverProfile = driverProfile;
        this.driverProfileViews = driverProfileViews;
        this.controllerDeviceProfile = controllerDeviceProfile;
        this.supportedDeviceProfiles = supportedDeviceProfiles;

        this.controllerDevice = controlDevice;
        this.childDevices = childDevices;
        this.controlDevicesByChildDeviceIds = controlDevicesByChildDeviceIds;
        this.allProjectDevicesByIds = allDevicesByIds;
        this.regions = activeRegions;

        deviceProfileViewsByIds = new HashMap<>();
        if (driverProfileViews != null && !driverProfileViews.isEmpty()) {
            // TODO: Выбор нужного отображения
            DriverProfileView driverProfileView = driverProfileViews.get(0);
            List<DeviceProfileView> deviceProfileViews = driverProfileView.getDeviceProfileViews();
            for (DeviceProfileView deviceProfileView : deviceProfileViews) {
                deviceProfileViewsByIds.put(deviceProfileView.getId(), deviceProfileView);
            }
        }

        stateHandler = new StateRelay(this);
        taskManager = new ControllerTaskManager(this, State.WAITING_TASK);
        this.messageCodec = messageCodec;
    }

    public Logger getLogger() {
        return logger;
    }

    public Transport getTransport() {
        return transport;
    }

    public Clock getClock() {
        return clock;
    }

    public DriverAlertSender getDriverAlertSender() {
        return driverAlertSender;
    }

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public List<DriverProfileView> getDriverProfileViews() {
        return driverProfileViews;
    }

    public Map<String, DeviceProfileView> getDeviceProfileViewsByIds() {
        return deviceProfileViewsByIds;
    }

    public DeviceProfile getControllerDeviceProfile() {
        return controllerDeviceProfile;
    }

    public Map<String, DeviceProfile> getSupportedDeviceProfiles() {
        return supportedDeviceProfiles;
    }

    public StateHandler getStateHandler() {
        return stateHandler;
    }

    public ActiveDevice getControllerDevice() {
        return controllerDevice;
    }

    public List<ActiveDevice> getChildDevices() {
        return childDevices;
    }

    public Map<String, ActiveDevice> getControlDevicesByChildDeviceIds() {
        return controlDevicesByChildDeviceIds;
    }

    public Map<String, ActiveDevice> getAllProjectDevicesByIds() {
        return allProjectDevicesByIds;
    }

    public ControllerActionExecutor getActionExecutor() {
        return actionExecutor;
    }

    public ControllerTaskManager getTaskManager() {
        return taskManager;
    }

    public void setTaskManager(ControllerTaskManager taskManager) {
        this.taskManager = taskManager;
    }

    public int getTaskExecuteInterval() {
        return taskExecuteInterval;
    }

    public WebSocketMessageCodec getMessageCodec() {
        return messageCodec;
    }

    public void setMessageCodec(WebSocketMessageCodec messageCodec) {
        this.messageCodec = messageCodec;
    }

    public List<ActiveRegion> getRegions() {
        return regions;
    }
}
