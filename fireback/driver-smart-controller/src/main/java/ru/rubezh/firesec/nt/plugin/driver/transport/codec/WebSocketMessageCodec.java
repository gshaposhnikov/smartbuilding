package ru.rubezh.firesec.nt.plugin.driver.transport.codec;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class WebSocketMessageCodec {

    private static final String ALL_LIGHT_CODE = "xx";
    private static final String STARTING_LETTER = "R";
    // TODO: State number should not be hardcoded. Different logic for filling "off" regions needed.
    private static final int OFF_STATE_NO = 0;

    private Logger logger;

    public WebSocketMessageCodec(Logger logger) {
        this.logger = logger;
    }

    public void decodeWSMessageAndSetAlerts(Object data, SetEntityStates statesAlert,
                                            List<SetActiveDeviceMonitorableValues> monitorableValuesAlerts){
        try {
            if (data instanceof String){
                HashMap<String, Number> parsedJson = new ObjectMapper().readValue((String)data, HashMap.class);
                // device tag starts with "R"
                for (String key: parsedJson.keySet()) {
                    if (!key.startsWith(STARTING_LETTER)){
                        continue;
                    }
                    //device string info consists of 3 parts: region no, entity code, device no;
                    String[] keyValues = key.split("_", 3);
                    //1. region number
                    String regionNo = keyValues[0].substring(2);

                    if (regionNo.equals(ALL_LIGHT_CODE)){
                        boolean alertAllOff = parsedJson.get(key).intValue() == OFF_STATE_NO;
                        statesAlert.setAllOff(alertAllOff);
                    } else {
                        //2. entity code
                        DeviceType deviceType = getDeviceTypeByTag(keyValues);

                        if (deviceType == DeviceType.ROOM_LIGHT){
                            if (keyValues.length < 3 && parsedJson.get(key).intValue() == OFF_STATE_NO) {
                                statesAlert.addOffRegionNo(Integer.parseInt(regionNo));
                            }
                        } else {
                            //3. device number
                            String deviceNo = getDeviceNoByTag(keyValues, deviceType);

                            //4. filling alerts
                            fillAlerts(regionNo, deviceType, deviceNo, key, statesAlert,
                                    monitorableValuesAlerts, parsedJson.get(key));
                        }
                    }
                }
            } else {
                logger.warn("MD: data is not a string");
            }
        } catch (IOException e) {
            logger.warn("MD: couldn't parse the JSON correctly", e);
        }
    }

    private DeviceType getDeviceTypeByTag(String[] keyValues){
        for (DeviceType availableType : DeviceType.values()) {
            if (availableType.getCode().equals(keyValues[1])) {
                if (availableType.getCode().equals(DeviceType.ROLLS.getCode())) {
                    if (keyValues[2].equals(DeviceType.ROLLS.getOnOffKeyword())) {
                        return DeviceType.ROLLS;
                    } else {
                        return DeviceType.LOCK;
                    }
                } else {
                    return availableType;
                }
            }
        }
        return DeviceType.NONE;
    }

    private String getDeviceNoByTag(String[] keyValues, DeviceType deviceType){
        if (keyValues.length > 2){
            String deviceInfo = keyValues[2];
            if (isInteger(deviceInfo)){
                return deviceInfo;
            }
        }
        String onOffKeyword = deviceType.getOnOffKeyword();
        if (onOffKeyword != null){
            for (int i = 1; i < keyValues.length; i++) {
                if (keyValues[i].contains(onOffKeyword)){
                    return "1";
                }
                for (String paramName: deviceType.getParameterNames()){
                    if (keyValues[i].contains(paramName)){
                        return "1";
                    }
                }
            }
        }
        return "";
    }

    private void fillAlerts(String regionNo, DeviceType deviceType, String deviceNo, String key, SetEntityStates statesAlert,
                       List<SetActiveDeviceMonitorableValues> monitorableValuesAlerts, Number value){
        if (!deviceNo.isEmpty()) {
            String parameter = deviceType.getParameterNames()
                    .stream()
                    .filter(key::contains)
                    .findFirst().orElse(null);
            if (parameter != null){
                boolean inserted = false;
                for (SetActiveDeviceMonitorableValues alert : monitorableValuesAlerts) {
                    if (alert.getRegionNo().equals(regionNo)
                            && alert.getDeviceType().equals(deviceType)
                            && alert.getDeviceNo().equals(deviceNo)) {
                        alert.getMonitorableValues()
                                .put(parameter, value.toString());
                        inserted = true;
                        break;
                    }
                }
                if (!inserted){
                    SetActiveDeviceMonitorableValues alert = new SetActiveDeviceMonitorableValues(regionNo,
                            deviceType, deviceNo);
                    alert.getMonitorableValues().put(parameter, value.toString());
                    monitorableValuesAlerts.add(alert);
                }
            } else {
                // TODO: hardcode should be deleted after adding logic to alert situation
                if (regionNo.equals("03") && deviceType == DeviceType.LIGHT && deviceNo.equals("01")
                        && value.intValue() == 1){
                    value = 5;
                }
                statesAlert.addDeviceState(regionNo, deviceType, deviceNo, value.intValue());
            }
        }
    }

    private static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    public String makeDeviceStateData(int regionIndex, DeviceType deviceType, boolean withPostfix, int deviceNo,
                                      int stateValue) {
        String result = "{ \"R10" + regionIndex + ("_") + deviceType.getCode() + "_";
        if (withPostfix){
            result += deviceType.getOnOffKeyword();
        } else {
            result += "0" + deviceNo;
        }
        result += "\": " + stateValue + " }";
        return result;
    }

    public String makeRegionStateData(int regionIndex, int stateValue) {
        return "{ \"R10" + regionIndex + "_L0\": " + stateValue + " }";
    }

    public String makeApartmentStateData(int stateValue) {
        return "{ \"R1xx_L0\": " + stateValue + " }";
    }

    public String makeDeviceMonitorableValueData(int regionIndex, DeviceType deviceType, boolean withPostfix,
                                                 int deviceNo, int paramValue, String paramName){
        String result = "{ \"R10" + regionIndex + ("_") + deviceType.getCode() + "_";
        if (withPostfix){
            result += paramName;
        } else {
            result += deviceNo + "_" + paramName;
        }
        result+= "\": " + paramValue + " }";
        return result;
    }
}
