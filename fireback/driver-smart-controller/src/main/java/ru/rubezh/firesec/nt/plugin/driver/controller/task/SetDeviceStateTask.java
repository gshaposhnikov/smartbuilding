package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public class SetDeviceStateTask extends SetDeviceValueTask {

    SetDeviceStateTask(ControllerStateMachineContext stateMachineContext, int value, String issueId,
                       int regionIndex, DeviceType deviceType, int deviceNo, boolean withPrefix) {
        super(stateMachineContext, value, issueId, regionIndex, deviceType, deviceNo, withPrefix);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setDeviceState(currentTimeMs, regionIndex, deviceType, withPrefix,
                deviceNo, value);
        sendIssueStatusProgress(issueId);
        return State.STATES_WAITING;
    }
}
