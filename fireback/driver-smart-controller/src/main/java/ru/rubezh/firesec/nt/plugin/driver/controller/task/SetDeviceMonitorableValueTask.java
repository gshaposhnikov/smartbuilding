package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public class SetDeviceMonitorableValueTask extends SetDeviceStateTask {

    private String paramName;

    SetDeviceMonitorableValueTask(ControllerStateMachineContext stateMachineContext, int value, String issueId,
                                  int regionIndex, DeviceType deviceType, int deviceNo, boolean withPrefix,
                                  String paramName) {
        super(stateMachineContext, value, issueId, regionIndex, deviceType, deviceNo, withPrefix);
        this.paramName = paramName;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setDeviceMonitorableValue(currentTimeMs, regionIndex, deviceType,
                withPrefix, deviceNo, value, paramName);
        sendIssueStatusProgress(issueId);
        return State.STATES_WAITING;
    }
}
