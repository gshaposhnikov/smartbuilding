package ru.rubezh.firesec.nt.plugin.driver.controller;

import ru.rubezh.firesec.nt.amqp.message.response.CreateIssueDriverResponse;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.domain.v1.Issue;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;

import java.util.Date;

public class ControllerExternalRequestHandler {

    private ControllerStateMachineContext stateMachineContext;

    public ControllerExternalRequestHandler(ControllerStateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    void handleMessage(Issue issue, CreateIssueDriverResponse response) {
        if (!stateMachineContext.getStateHandler().isConnected()) {
            Issue actualIssue = new Issue(issue);
            actualIssue.generateId();
            Date currentDate = new Date();
            actualIssue.setCreateDateTime(currentDate);
            actualIssue.setStartDateTime(currentDate);
            actualIssue.setFinishDateTime(currentDate);
            actualIssue.setStatus(IssueStatus.FAILED);
            actualIssue.setStatusMessage("Связь с устройством потеряна");
            actualIssue.setProgress(100);
            response.getActualIssues().add(actualIssue);
            return;
        }
        switch (issue.getAction()){
            case SET_DEVICE_STATE:
                setDeviceState(issue, response);
                break;
            case SET_REGION_STATE:
                setRegionState(issue, response);
                break;
            case SET_APARTMENT_STATE:
                setApartmentState(issue, response);
                break;
            case SET_DEVICE_MONITORABLE_VALUE:
                setDeviceMonitorableValue(issue, response);
                break;
            default:
                Issue actualIssue = new Issue(issue);
                actualIssue.generateId();
                Date currentDate = new Date();
                actualIssue.setCreateDateTime(currentDate);
                actualIssue.setStartDateTime(currentDate);
                actualIssue.setFinishDateTime(currentDate);
                actualIssue.setStatus(IssueStatus.FAILED);
                actualIssue.setStatusMessage("Задача не поддерживается: " + issue.getAction());
                actualIssue.setProgress(100);
                response.getActualIssues().add(actualIssue);
        }
    }

    void startIssue(Issue issue) {
        switch (issue.getAction()){
            case SET_DEVICE_STATE:
                startSetDeviceState(issue);
                break;
            case SET_REGION_STATE:
                startSetRegionState(issue);
            case SET_APARTMENT_STATE:
                startSetApartmentState(issue);
            case SET_DEVICE_MONITORABLE_VALUE:
                startSetDeviceMonitorableValue(issue);
            default:
                issue.setStatus(IssueStatus.FAILED);
                issue.setStatusMessage("Задача не поддерживается: " + issue.getAction());
        }
    }

    private Issue createIssueWithIdAndAddToResponse(Issue issue, CreateIssueDriverResponse response){
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        return actualIssue;
    }

    private void setDeviceState(Issue issue, CreateIssueDriverResponse response) {
        startSetDeviceState(createIssueWithIdAndAddToResponse(issue, response));
    }

    private void startSetDeviceState(Issue issue) {
        int regionIndex = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.regionIndex.toString()).toString());
        DeviceType deviceType = DeviceType.valueOf(issue.getParameters()
                .get(Issue.IssueParameterNames.deviceType.toString()).toString());
        int deviceNo = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.deviceNo.toString()).toString());
        boolean withPrefix = Boolean.parseBoolean(issue.getParameters()
                .get(Issue.IssueParameterNames.withPostFix.toString()).toString());
        int stateValue = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.stateNumber.toString()).toString());
        stateMachineContext.getTaskManager().enqueueSetDeviceStates(regionIndex, deviceType, deviceNo, withPrefix,
                stateValue, issue.getId());
    }

    private void setRegionState(Issue issue, CreateIssueDriverResponse response) {
        startSetRegionState(createIssueWithIdAndAddToResponse(issue, response));
    }

    private void startSetRegionState(Issue issue) {
        int regionIndex = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.regionIndex.toString()).toString());
        int stateValue = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.stateNumber.toString()).toString());
        stateMachineContext.getTaskManager().enqueueSetRegionStates(regionIndex, stateValue, issue.getId());
    }

    private void setApartmentState(Issue issue, CreateIssueDriverResponse response){
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        startSetApartmentState(actualIssue);
    }

    private void startSetApartmentState(Issue issue){
        int stateValue = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.stateNumber.toString()).toString());
        stateMachineContext.getTaskManager().enqueueSetApartmentState(stateValue, issue.getId());
    }

    private void setDeviceMonitorableValue(Issue issue, CreateIssueDriverResponse response){
        startSetDeviceMonitorableValue(createIssueWithIdAndAddToResponse(issue, response));
    }

    private void startSetDeviceMonitorableValue(Issue issue){
        int regionIndex = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.regionIndex.toString()).toString());
        DeviceType deviceType = DeviceType.valueOf(issue.getParameters()
                .get(Issue.IssueParameterNames.deviceType.toString()).toString());
        int deviceNo = Integer.parseInt(issue.getParameters()
                .get(Issue.IssueParameterNames.deviceNo.toString()).toString());
        boolean withPrefix = Boolean.parseBoolean(issue.getParameters()
                .get(Issue.IssueParameterNames.withPostFix.toString()).toString());
        String paramName = deviceType.getParameterNames().stream().filter(parameterName -> {
            for (String key : issue.getParameters().keySet()){
                if (key.equals(parameterName)){
                    return true;
                }
            }
            return false;
        }).findFirst().orElse("");
        int paramValue = (int) issue.getParameters().get(paramName);
        stateMachineContext.getTaskManager().enqueueSetDeviceMonitorableValue(regionIndex, deviceType, deviceNo,
                withPrefix, paramValue, issue.getId(), paramName);
    }

}
