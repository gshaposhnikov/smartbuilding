package ru.rubezh.firesec.nt.plugin.driver.controller.task;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.controller.state_handler.State;

public class SetRegionStateTask extends SetValueTask {

    private int regionIndex;

    SetRegionStateTask(ControllerStateMachineContext stateMachineContext, int regionIndex, int stateValue, String issueId) {
        super(stateMachineContext, stateValue, issueId);
        this.regionIndex = regionIndex;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setRegionState(currentTimeMs, regionIndex, value);
        sendIssueStatusProgress(issueId);
        return State.STATES_WAITING;
    }
}
