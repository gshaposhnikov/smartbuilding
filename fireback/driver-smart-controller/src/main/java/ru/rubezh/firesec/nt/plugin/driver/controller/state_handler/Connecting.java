package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

public class Connecting extends AbstractHandler {
    @Override
    public State getCurrentState() {
        return State.CONNECTING;
    }
}
