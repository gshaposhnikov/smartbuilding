package ru.rubezh.firesec.nt.plugin.driver.controller.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.controller.ControllerStateMachineContext;

public class WaitingTask extends AbstractHandler {

    protected ControllerStateMachineContext stateMachineContext;

    @Override
    public State getCurrentState() {
        return State.WAITING_TASK;
    }

    @Override
    public StateHandler cloneBinded(ControllerStateMachineContext stateMachineContext) {
        WaitingTask handler = new WaitingTask();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().debug("WT: on timer");
        stateMachineContext.getTaskManager().enqueueGetStates();
        if (timerName.equals(TimerNames.EXECUTE_TASK.getName())) {
            return stateMachineContext.getTaskManager().executeNextTask(currentTimeMs);
        } else {
            return getCurrentState();
        }
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        return null;
    }

    @Override
    public State onConnected(long currentTimeMs) {
        return null;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        return null;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        return null;
    }
}
