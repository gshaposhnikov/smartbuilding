package ru.rubezh.firesec.nt.plugin.driver.controller;

import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;

import java.util.ArrayList;
import java.util.List;

public class ControllerActionExecutor {

    private ControllerStateMachineContext stateMachineContext;

    ControllerActionExecutor(ControllerStateMachineContext controllerStateMachineContext) {
        this.stateMachineContext = controllerStateMachineContext;
    }

    public void connect(long currentTimeMs) {
        stateMachineContext.getTransport().connect(currentTimeMs, this::connected, this::disconnected);
    }

    private void connected(long currentTimeMs, boolean isSuccess) {
        if (isSuccess)
            stateMachineContext.getStateHandler().onConnected(currentTimeMs);
        else
            stateMachineContext.getStateHandler().onConnectFailure(currentTimeMs);
    }

    private void disconnected(long currentTimeMs) {
        stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
    }

    private void timerHandler(long currentTimeMs, String timerName) {
        stateMachineContext.getStateHandler().onTimer(currentTimeMs, timerName);
    }

    public void setTimer(long currentTimeMs, String name, int interval, boolean isRepeatable) {
        stateMachineContext.getClock().setTimer(name, currentTimeMs, interval,
                timerTimeMs -> timerHandler(timerTimeMs, name), isRepeatable);
    }

    public void getStates(long currentTimeMs) {
        stateMachineContext.getTransport().receive(currentTimeMs, 0, this::receivedData,
                this::receiveDataFailed);
    }

    private void receivedData(long currentTimeMs, int sequenceNo, Object data) {
        SetEntityStates statesAlert = new SetEntityStates();
        List<SetActiveDeviceMonitorableValues> monitorableValuesAlerts = new ArrayList<>();
        stateMachineContext.getMessageCodec().decodeWSMessageAndSetAlerts(data, statesAlert, monitorableValuesAlerts);
        stateMachineContext.getStateHandler().onStatesReceived(currentTimeMs, statesAlert, monitorableValuesAlerts);
    }

    private void receiveDataFailed(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getLogger().error("AE: could not receive data from transport");
        stateMachineContext.getStateHandler().onExchangeFailure(currentTimeMs);
    }

    public void setDeviceState(long currentTimeMs, int regionIndex, DeviceType deviceType, boolean withPostfix,
                               int deviceNo, int stateValue){
        String dataJson = stateMachineContext.getMessageCodec().makeDeviceStateData(regionIndex, deviceType, withPostfix,
                deviceNo, stateValue);
        stateMachineContext.getLogger().error(dataJson);
        stateMachineContext.getTransport().send(currentTimeMs, dataJson, this::sentData, this::sendDataFailed);
    }

    public void setRegionState(long currentTimeMs, int regionIndex, int stateValue){
        String dataJson = stateMachineContext.getMessageCodec().makeRegionStateData(regionIndex, stateValue);
        stateMachineContext.getTransport().send(currentTimeMs, dataJson, this::sentData, this::sendDataFailed);
    }

    public void setApartmentState(long currentTimeMs, int stateValue){
        String dataJson = stateMachineContext.getMessageCodec().makeApartmentStateData(stateValue);
        stateMachineContext.getTransport().send(currentTimeMs, dataJson, this::sentData, this::sendDataFailed);
    }

    public void setDeviceMonitorableValue(long currentTimeMs, int regionIndex, DeviceType deviceType, boolean withPostfix,
                                          int deviceNo, int paramValue, String paramName){
        String dataJson = stateMachineContext.getMessageCodec().makeDeviceMonitorableValueData(regionIndex,
                deviceType, withPostfix, deviceNo, paramValue, paramName);
        stateMachineContext.getTransport().send(currentTimeMs, dataJson, this::sentData, this::sendDataFailed);
    }

    private void sentData(long currentTimeMs, int sequenceNo){
        stateMachineContext.getStateHandler().onStateSent(currentTimeMs);
    }

    private void sendDataFailed(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getLogger().error("AE: could not send data to transport");
        stateMachineContext.getStateHandler().onExchangeFailure(currentTimeMs);
    }

}
