package ru.rubezh.firesec.nt.domain.v0;

import java.beans.Transient;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Document(collection = "devices.v0")
@TypeAlias(value = "Panel")
@JsonTypeName("Panel")
public class Panel extends Device {

    public enum PowerStatus {
        normal,
        redundancy,
        fail,
        failFixed,
        nd
    }

    public enum OutputStatus {
        normal,
        breaked,
        shortCircuit,
        highLoad,
        lowLoad,
        nd
    }

    private PowerStatus power1Status = PowerStatus.nd;
    private PowerStatus power2Status = PowerStatus.nd;
    private OutputStatus output1Status = OutputStatus.nd;
    private OutputStatus output2Status = OutputStatus.nd;

    @JsonIgnore private long fireLogIndex;

    @Override
    @JsonIgnore
    public DeviceType getDeviceType() {
        return DeviceType.panel;
    }

    @Override
    @JsonIgnore
    public String getModelName() {
        return new String("Рубеж 2ОП3");
    }
    
    @Override
    @JsonIgnore
    public AddressDeviceType getAddressDeviceType() {
        return AddressDeviceType.RUBEZH_2OPM;
    }
    
    public PowerStatus getPower1Status() {
        return power1Status;
    }

    public void setPower1Status(PowerStatus power1Status) {
        this.power1Status = power1Status;
    }

    public PowerStatus getPower2Status() {
        return power2Status;
    }

    public void setPower2Status(PowerStatus power2Status) {
        this.power2Status = power2Status;
    }

    public OutputStatus getOutput1Status() {
        return output1Status;
    }

    public void setOutput1Status(OutputStatus output1Status) {
        this.output1Status = output1Status;
    }

    public OutputStatus getOutput2Status() {
        return output2Status;
    }

    public void setOutput2Status(OutputStatus output2Status) {
        this.output2Status = output2Status;
    }

    public long getFireLogIndex() {
        return fireLogIndex;
    }

    public void setFireLogIndex(long fireLogIndex) {
        this.fireLogIndex = fireLogIndex;
    }

    @JsonIgnore
    @Transient
    public DeviceStatus getCalculatedDeviceStatus() {
        if (power1Status.equals(PowerStatus.nd) && power2Status.equals(PowerStatus.nd) && output1Status.equals(OutputStatus.nd) && output2Status.equals(OutputStatus.nd))
            return DeviceStatus.lost;
        else if ((power1Status.equals(PowerStatus.normal) || power1Status.equals(PowerStatus.failFixed))
                && (power2Status.equals(PowerStatus.normal) || power2Status.equals(PowerStatus.failFixed))
                && output1Status.equals(OutputStatus.normal)
                && output2Status.equals(OutputStatus.normal))
            return DeviceStatus.normal;
        else
            return DeviceStatus.fail;
    }
}
