package ru.rubezh.firesec.nt.domain.v0;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum AddressDeviceType {
    NONE(0x00),                 //0 - для указания отсутствия значения
    RUBEZH_2AM(0x01),           //1
    BUNS_01(0x02),              //2
    RUBEZH_BI(0x03),            //3
    RUBEZH_10AM(0x04),          //4
    RUBEZH_4A(0x05),            //5
    RUBEZH_2OP(0x06),           //6
    RUBEZH_PDU(0x07),           //7
    BUNS_02(0x08),              //8
    RUBEZH_PDUPT(0x09),         //9
    BLANK_PANEL(0x0A),          //10
    RUBEZH_BIU(0x0B),           //11
    RUBEZH_2OPM(0x14),          //20
    BII(0x15),                  //21
    KAU1(0x16),                 //22
    KAU2(0x17),                 //23
    KAUE(0x18),                 //24
    AM1(0x62),                  //98
    AM2(0x63),                  //99
    AM3(0x64),                  //100
    AM4(0x65),                  //101
    AM_TEL(0x66),               //102
    AM_KP(0x67),                //103
    AM_SH(0x68),                //104
    AM_ETH(0x97),               //151
    DATA_COLLECT_MODULE(0x98),  //152
    SONAR(0xC8),                //200
    AM_RADIO(0xC9),             //201
    AM_SONAR(0xCA),             //202
    IMITATOR(0xFF);             //255
    
    private final int value;
    private AddressDeviceType(int value) {
        this.value = value;
    }

    private static final Map<Integer, AddressDeviceType> typesByValue = new HashMap<>();
    static {
        for (AddressDeviceType type : AddressDeviceType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<AddressDeviceType, Integer> valuesByType = new EnumMap<>(AddressDeviceType.class);
    static {
        for (AddressDeviceType type : AddressDeviceType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static AddressDeviceType fromInteger(int value) {
        return typesByValue.get(value);
    }
    
    public static Integer toInteger(AddressDeviceType type) {
        return valuesByType.get(type);
    }
}
