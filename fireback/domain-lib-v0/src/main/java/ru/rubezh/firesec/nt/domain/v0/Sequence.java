package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sequences.v0")
public class Sequence {
    @Id private String id;
    private long sequence;

    public Sequence(String id, long sequence) {
        this.id = new String(id);
        this.sequence = sequence;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }
}
