package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "DeviceLostEventContext")
public class DeviceLostEventContext extends EventContext {

    Boolean isLost;

    public Boolean getIsLost() {
        return isLost;
    }

    public void setIsLost(Boolean isLost) {
        this.isLost = isLost;
    }

    @Override
    public boolean equals(Device device) {
        if (device.getDeviceStatus().equals(DeviceStatus.lost))
            return isLost;
        else
            return !isLost;
    }

    @Override
    public void applyTo(Device device) {
        if (isLost)
            device.setDeviceStatus(DeviceStatus.lost);
        else
            device.setDeviceStatus(DeviceStatus.normal);
    }

    @Override
    public EventCode getEventCode() {
        if (isLost)
            return EventCode.deviceLost;
        else
            return EventCode.deviceLostOk;
    }
}
