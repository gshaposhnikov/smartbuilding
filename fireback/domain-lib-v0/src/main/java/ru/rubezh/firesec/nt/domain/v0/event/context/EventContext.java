package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.Transient;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

abstract public class EventContext {

    abstract public boolean equals(Device device);
    abstract public void applyTo(Device device);

    @Transient
    abstract public EventCode getEventCode();
}
