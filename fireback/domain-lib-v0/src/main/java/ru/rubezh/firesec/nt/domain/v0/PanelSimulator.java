package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Document(collection = "devices.v0")
@TypeAlias(value = "PanelSimulator")
@JsonTypeName("PanelSimulator")
public class PanelSimulator extends Panel {
    private String ipAddress = "127.0.0.1";
    private int port = 8081;

    @Override
    @JsonIgnore
    public DeviceType getDeviceType() {
        return DeviceType.panelSimulator;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
