package ru.rubezh.firesec.nt.domain.v0;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum RS485Rate{
    S_9600(0),
    S_19200(1),
    S_38400(2),
    S_57600(3),
    S_115200(4);

    private final int value;
    private RS485Rate(int value) {
        this.value = value;
    }

    private static final Map<Integer, RS485Rate> typesByValue = new HashMap<>();
    static {
        for (RS485Rate type : RS485Rate.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<RS485Rate, Integer> valuesByType = new EnumMap<>(RS485Rate.class);
    static {
        for (RS485Rate type : RS485Rate.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static RS485Rate fromInteger(int value) {
        return typesByValue.get(value);
    }
    
    public static Integer toInteger(RS485Rate type) {
        return valuesByType.get(type);
    }
}
