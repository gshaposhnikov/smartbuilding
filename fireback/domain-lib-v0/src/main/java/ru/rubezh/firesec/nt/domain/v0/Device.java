package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Document(collection = "devices.v0")
@CompoundIndexes({
    @CompoundIndex(name = "planId_1_parentDeviceId_1", def = "{'planId' : 1, 'parentDeviceId': 1}")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = FireDetector.class, name = "FireDetector"),
    @JsonSubTypes.Type(value = Panel.class, name = "Panel"),
    @JsonSubTypes.Type(value = PanelSimulator.class, name = "PanelSimulator"),
    @JsonSubTypes.Type(value = USBRS485AdapterModule.class, name = "USBRS485AdapterModule"),
})
abstract public class Device {

    public enum DeviceType {
        panel,
        panelSimulator,
        fireDetector,
        adapterModule
    }

    @Id private String id;

    @JsonIgnore
    @Indexed
    private String planId = "";

    @JsonIgnore
    @Indexed
    private String parentDeviceId;
    @Transient private Integer parentAddress;
    @Transient private int nChildDevices; //NOTE: may be we should store this value to DB for better performance

    @Indexed
    private int regionNo;

    @Indexed
    private int lineNo;
    @Indexed
    private int address = 1;

    private double x;
    private double y;

    @Indexed
    private DeviceStatus deviceStatus = DeviceStatus.nd;
    @Indexed
    private FireStatus fireStatus = FireStatus.nd;

    abstract public DeviceType getDeviceType();
    abstract public String getModelName();
    @JsonIgnore
    abstract public AddressDeviceType getAddressDeviceType();
    
    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public FireStatus getFireStatus() {
        return fireStatus;
    }

    public void setFireStatus(FireStatus fireStatus) {
        this.fireStatus = fireStatus;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public int getRegionNo() {
        return regionNo;
    }

    public void setRegionNo(int regionNo) {
        this.regionNo = regionNo;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public Integer getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(Integer parentAddress) {
        this.parentAddress = parentAddress;
    }

    public int getnChildDevices() {
        return nChildDevices;
    }

    public void setnChildDevices(int nChildDevices) {
        this.nChildDevices = nChildDevices;
    }
}
