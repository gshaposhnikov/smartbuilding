package ru.rubezh.firesec.nt.domain.v0;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import ru.rubezh.firesec.nt.domain.v0.Region;

@Document(collection = "plans.v0")
@TypeAlias(value = "Plan.v0")
public class Plan {

    @Id private String id;

    private String name = "Plan name";
    private String description = "Plan description";
    private AdminStatus adminStatus = AdminStatus.off;
    private PlanOperStatus operStatus = new PlanOperStatus();
    private List<Region> regions = new ArrayList<>();

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public Region getRegion(Integer regionNo) {
        try {
            return regions.get(regionNo);
        }
        catch(IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public Boolean addRegion(Region region) {
        return regions.add(region);
    }

    public Boolean removeRegion(Region region) {
        return regions.remove(region);
    }

    public AdminStatus getAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus(AdminStatus adminStatus) {
        this.adminStatus = adminStatus;
    }

    public PlanOperStatus getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(PlanOperStatus operStatus) {
        this.operStatus = operStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
