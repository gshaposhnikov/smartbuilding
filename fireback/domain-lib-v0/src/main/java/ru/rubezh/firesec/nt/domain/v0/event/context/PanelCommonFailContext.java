package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "PanelCommonFailContext")
public class PanelCommonFailContext extends EventContext {

    private Boolean isFail;

    public Boolean getIsFail() {
        return isFail;
    }

    public void setIsFail(Boolean isFail) {
        this.isFail = isFail;
    }

    @Override
    public boolean equals(Device device) {
        if (device.getDeviceStatus().equals(DeviceStatus.fail))
            return isFail;
        else
            return !isFail;
    }

    @Override
    public void applyTo(Device device) {
        if (isFail)
            device.setDeviceStatus(DeviceStatus.fail);
        else
            device.setDeviceStatus(DeviceStatus.normal);
    }

    @Override
    public EventCode getEventCode() {
        if (isFail)
            return EventCode.panelCommonFail;
        else
            return EventCode.panelCommonOk;
    }
}
