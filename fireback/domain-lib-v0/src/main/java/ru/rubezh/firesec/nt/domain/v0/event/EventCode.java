package ru.rubezh.firesec.nt.domain.v0.event;

public enum EventCode {
    panelCommonFail,
    panelCommonOk,

    panelPowerNormal,
    panelPowerRedundancy,
    panelPowerFail,

    panelOutputOk,
    panelOutputBreaked,
    panelOutputShortCircuit,
    panelOutputHighLoad,
    panelOutputLowLoad,

    deviceLostOk,
    deviceLost,

    fireDetectorFailFixedOne,
    fireDetectorFailFixedAll,
    fireDetectorFailDetected,

    fire,
    fireWarning,

    userReset
}
