package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.TypeAlias;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@TypeAlias(value = "RectRegion")
@JsonTypeName("RectRegion")
public class RectRegion extends Region {

    private double x;
    private double y;
    private double xSize;
    private double ySize;

    @Override
    @JsonIgnore
    public RegionType getRegionType() {
        return RegionType.rect;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getxSize() {
        return xSize;
    }

    public void setxSize(double xSize) {
        this.xSize = xSize;
    }

    public double getySize() {
        return ySize;
    }

    public void setySize(double ySize) {
        this.ySize = ySize;
    }

}
