package ru.rubezh.firesec.nt.domain.v0;


import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import ru.rubezh.firesec.nt.domain.v0.Region;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;
import ru.rubezh.firesec.nt.domain.v0.event.EventDescription;
import ru.rubezh.firesec.nt.domain.v0.event.context.EventContext;

@Document(collection = "log_records.v0")
@TypeAlias(value = "LogRecord")
public class LogRecord {

    @JsonIgnore
    @Id String id;
    @Indexed long recordNo;

    public enum SourceType {
        panel,
        device
    }

    public enum EventType {
        panelCommonFail,
        panelPowerFail,
        panelOutputFail,
        deviceLost,
        fireDetectorFail,
        fire
    }

    @Indexed
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date dateTime = new Date();

    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date receivedDateTime = dateTime;

    private SourceType sourceType;
    @JsonIgnore private String sourceId = "";
    @Transient private Object source;
    @JsonIgnore private String sourceDeviceId = "";
    @Transient private Object sourceDevice;

    @Transient private Region region;

    @JsonUnwrapped
    @Transient private EventDescription eventDescription;

    private EventType eventType;
    private EventContext eventContext;

    private String userName = "";

    public long getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(long recordNo) {
        this.recordNo = recordNo;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Date getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(Date receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }

    public EventDescription getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(EventDescription eventDescription) {
        this.eventDescription = eventDescription;
    }

    public EventCode getEventCode() {
        return this.eventContext.getEventCode();
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceDeviceId() {
        return sourceDeviceId;
    }

    public void setSourceDeviceId(String sourceDeviceId) {
        this.sourceDeviceId = sourceDeviceId;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public EventContext getEventContext() {
        return eventContext;
    }

    public void setEventContext(EventContext eventContext) {
        this.eventContext = eventContext;
    }

    public String getId() {
        return id;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getSourceDevice() {
        return sourceDevice;
    }

    public void setSourceDevice(Object sourceDevice) {
        this.sourceDevice = sourceDevice;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
