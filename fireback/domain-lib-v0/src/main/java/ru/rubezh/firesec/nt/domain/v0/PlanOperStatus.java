package ru.rubezh.firesec.nt.domain.v0;


import com.fasterxml.jackson.annotation.JsonIgnore;

public class PlanOperStatus {
    private int nLostDevices;
    private int nFailDevices;
    private int nFireDevices;
    private int nNormalDevices;
    private int nNoDataDevices;
    private int nFireRegions;

    @JsonIgnore
    public DeviceStatus getDeviceStatus() {
        if (getnFailDevices() > 0)
            return DeviceStatus.fail;
        else if (getnLostDevices() > 0 || getnNoDataDevices() > 0)
            return DeviceStatus.lost;
        else
            return DeviceStatus.normal;
    }

    @JsonIgnore
    public FireStatus getFireStatus() {
        if (getnFireDevices() > 0)
            return FireStatus.fire;
        else
            return FireStatus.normal;
    }

    public int getnLostDevices() {
        return nLostDevices;
    }

    public void setnLostDevices(int nLostDevices) {
        this.nLostDevices = nLostDevices;
    }

    public int getnFailDevices() {
        return nFailDevices;
    }

    public void setnFailDevices(int nFailDevices) {
        this.nFailDevices = nFailDevices;
    }

    public int getnFireDevices() {
        return nFireDevices;
    }

    public void setnFireDevices(int nFireDevices) {
        this.nFireDevices = nFireDevices;
    }

    public int getnNormalDevices() {
        return nNormalDevices;
    }

    public void setnNormalDevices(int nNormalDevices) {
        this.nNormalDevices = nNormalDevices;
    }

    public int getnNoDataDevices() {
        return nNoDataDevices;
    }

    public void setnNoDataDevices(int nNoDataDevices) {
        this.nNoDataDevices = nNoDataDevices;
    }

    public int getnFireRegions() {
        return nFireRegions;
    }

    public void setnFireRegions(int nFireRegions) {
        this.nFireRegions = nFireRegions;
    }
}
