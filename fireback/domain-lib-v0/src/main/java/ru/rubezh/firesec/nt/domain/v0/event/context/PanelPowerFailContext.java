package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.Panel;
import ru.rubezh.firesec.nt.domain.v0.Panel.PowerStatus;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "PanelPowerFailContext")
public class PanelPowerFailContext extends EventContext {

    private Integer inputNo;
    private PowerStatus powerStatus;

    public Integer getInputNo() {
        return inputNo;
    }

    public void setInputNo(Integer inputNo) {
        this.inputNo = inputNo;
    }

    public PowerStatus getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(PowerStatus powerStatus) {
        this.powerStatus = powerStatus;
    }

    @Override
    public boolean equals(Device device) {
        if (device instanceof Panel) {
            Panel panel = (Panel) device;
            if (inputNo == 1)
                return panel.getPower1Status().equals(powerStatus);
            else if (inputNo == 2)
                return panel.getPower2Status().equals(powerStatus);
            else
                return true;//TODO: throw an exception
        }
        else
            return true;//TODO: throw an exception
    }

    @Override
    public void applyTo(Device device) {
        if (device instanceof Panel) {
            Panel panel = (Panel) device;
            if (inputNo == 1)
                panel.setPower1Status(powerStatus);
            else if (inputNo == 2)
                panel.setPower2Status(powerStatus);
            //TODO: else - throw an exception

            panel.setDeviceStatus(panel.getCalculatedDeviceStatus());
        }
        //TODO: else - throw an exception
    }

    @Override
    public EventCode getEventCode() {
        EventCode eventCode;
        switch (powerStatus) {
        case redundancy:
            eventCode = EventCode.panelPowerRedundancy;
            break;
        case failFixed:
        case normal:
            eventCode = EventCode.panelPowerNormal;
            break;
        case fail:
        default:
            eventCode = EventCode.panelPowerFail;
            break;
        }
        return eventCode;
    }
}
