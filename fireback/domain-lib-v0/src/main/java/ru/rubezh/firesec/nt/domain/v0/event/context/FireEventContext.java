package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "FireEventContext")
public class FireEventContext extends EventContext {

    private FireStatus fireStatus;
    private int temperatureValue;
    private boolean isTemperatureSensorActive;
    private boolean isGradientTemperatureSensorActive;
    private boolean isSmokeSensorActive;

    public FireStatus getFireStatus() {
        return fireStatus;
    }

    public void setFireStatus(FireStatus fireStatus) {
        this.fireStatus = fireStatus;
    }

    public Integer getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(Integer temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public Boolean getIsTemperatureSensorActive() {
        return isTemperatureSensorActive;
    }

    public void setIsTemperatureSensorActive(Boolean isTemperatureSensorActive) {
        this.isTemperatureSensorActive = isTemperatureSensorActive;
    }

    public Boolean getIsGradientTemperatureSensorActive() {
        return isGradientTemperatureSensorActive;
    }

    public void setIsGradientTemperatureSensorActive(Boolean isGradientTemperatureSensorActive) {
        this.isGradientTemperatureSensorActive = isGradientTemperatureSensorActive;
    }

    public Boolean getIsSmokeSensorActive() {
        return isSmokeSensorActive;
    }

    public void setIsSmokeSensorActive(Boolean isSmokeSensorActive) {
        this.isSmokeSensorActive = isSmokeSensorActive;
    }

    @Override
    public boolean equals(Device device) {
        boolean equal = fireStatus.equals(device.getFireStatus());

        if (equal && device instanceof FireDetector) {
            FireDetector fireDetector = (FireDetector) device;
            equal = fireDetector.getIsTemperatureSensorActive() == isTemperatureSensorActive
                    && fireDetector.getIsGradientTemperatureSensorActive() == isGradientTemperatureSensorActive
                    && fireDetector.getIsSmokeSensorActive() == isSmokeSensorActive
                    && fireDetector.getTemperatureValue() == temperatureValue;
        }

        return equal;
    }

    @Override
    public void applyTo(Device device) {
        device.setFireStatus(fireStatus);

        if (device instanceof FireDetector) {
            FireDetector fireDetector = (FireDetector) device;
            fireDetector.setIsTemperatureSensorActive(isTemperatureSensorActive);
            fireDetector.setIsGradientTemperatureSensorActive(isGradientTemperatureSensorActive);
            fireDetector.setIsSmokeSensorActive(isSmokeSensorActive);
            fireDetector.setTemperatureValue(temperatureValue);
        }
    }

    @Override
    public EventCode getEventCode() {
        EventCode eventCode;
        switch (fireStatus) {
        case normal:
            eventCode = EventCode.userReset;
            break;
        case warning:
            eventCode = EventCode.fireWarning;
            break;
        case fire:
        default:
            eventCode = EventCode.fire;
            break;
        }
        return eventCode;
    }
}
