package ru.rubezh.firesec.nt.domain.v0;

public enum FireStatus {
    normal,
    warning,
    fire,
    nd
}
