package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "FireDetectorFailEventContext")
public class FireDetectorFailEventContext extends EventContext {

    public enum FailStatus {
        detected,
        fixedOne,
        fixedAll
    }

    private FailStatus failStatus;
    private Boolean isTemperatureChannelFailed;
    private Boolean isOpticalChannelFailed;

    public FailStatus getFailStatus() {
        return failStatus;
    }

    public void setFailStatus(FailStatus failStatus) {
        this.failStatus = failStatus;
    }

    public Boolean getIsTemperatureChannelFailed() {
        return isTemperatureChannelFailed;
    }

    public void setIsTemperatureChannelFailed(Boolean isTemperatureChannelFailed) {
        this.isTemperatureChannelFailed = isTemperatureChannelFailed;
    }

    public Boolean getIsOpticalChannelFailed() {
        return isOpticalChannelFailed;
    }

    public void setIsOpticalChannelFailed(Boolean isOpticalChannelFailed) {
        this.isOpticalChannelFailed = isOpticalChannelFailed;
    }

    @Override
    public boolean equals(Device device) {
        boolean equal = false;
        if (failStatus.equals(FailStatus.detected) || failStatus.equals(FailStatus.fixedOne))
            equal = device.getDeviceStatus().equals(DeviceStatus.fail);
        else
            equal = !device.getDeviceStatus().equals(DeviceStatus.fail);

        if (equal && device instanceof FireDetector) {
            FireDetector fireDetector = (FireDetector) device;
            equal = fireDetector.getIsTemperatureChannelFailed() == isTemperatureChannelFailed
                    && fireDetector.getIsOpticalChannelFailed() == isOpticalChannelFailed;
        }

        return equal;
    }

    @Override
    public void applyTo(Device device) {
        if (failStatus == FailStatus.detected || failStatus == FailStatus.fixedOne)
            device.setDeviceStatus(DeviceStatus.fail);
        else
            device.setDeviceStatus(DeviceStatus.normal);

        if (device instanceof FireDetector) {
            FireDetector fireDetector = (FireDetector) device;
            fireDetector.setIsTemperatureChannelFailed(isTemperatureChannelFailed);
            fireDetector.setIsOpticalChannelFailed(isOpticalChannelFailed);
        }
    }

    @Override
    public EventCode getEventCode() {
        EventCode eventCode;
        switch (failStatus) {
        case detected:
            eventCode = EventCode.fireDetectorFailDetected;
            break;
        case fixedOne:
            eventCode = EventCode.fireDetectorFailFixedOne;
            break;
        case fixedAll:
        default:
            eventCode = EventCode.fireDetectorFailFixedAll;
            break;
        }
        return eventCode;
    }
}
