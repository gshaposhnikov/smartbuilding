package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.TypeAlias;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@TypeAlias(value = "Region.v0")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = RectRegion.class, name = "RectRegion"),
})
abstract public class Region {

    private String name = "Region name";
    private String description = "Region description";
    private DeviceStatus deviceStatus = DeviceStatus.nd;
    private FireStatus fireStatus = FireStatus.nd;

    abstract public RegionType getRegionType();

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public FireStatus getFireStatus() {
        return fireStatus;
    }

    public void setFireStatus(FireStatus fireStatus) {
        this.fireStatus = fireStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
