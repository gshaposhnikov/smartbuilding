package ru.rubezh.firesec.nt.domain.v0;

public enum DeviceStatus {
    normal,
    fail,
    lost,
    nd
}
