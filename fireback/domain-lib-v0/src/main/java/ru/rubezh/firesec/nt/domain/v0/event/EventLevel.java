package ru.rubezh.firesec.nt.domain.v0.event;

public enum EventLevel {
    info,
    warning,
    error,
    critical
}
