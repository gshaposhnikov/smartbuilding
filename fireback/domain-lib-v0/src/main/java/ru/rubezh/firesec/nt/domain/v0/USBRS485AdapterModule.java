package ru.rubezh.firesec.nt.domain.v0;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Document(collection = "devices.v0")
@TypeAlias(value = "USBRS485AdapterModule")
@JsonTypeName("USBRS485AdapterModule")
public class USBRS485AdapterModule extends Device {
    private static final AddressDeviceType[] ADDRESS_DEVICE_TYPES = {AddressDeviceType.AM1, AddressDeviceType.AM2, AddressDeviceType.AM3, AddressDeviceType.AM4};

    private String serialNumber;
    private RS485Rate rs485Rate = RS485Rate.S_57600;
    private int nLines = 1;
    private int[] addressesOnLines = {33, 34, 35, 36};

    public int getnLines() {
        return nLines;
    }

    /**
     * Изменить кол-во линий модуля сопряжения (по умолчанию 1)
     * @param nLines кол-во линий от 1 до 4
     */
    public void setnLines(int nLines) {
        assert nLines > 0 && nLines <= 4;
        this.nLines = nLines;
    }
    
    public int getAddressOnLine(int lineNo) {
        if (lineNo < nLines)
            return addressesOnLines[lineNo];
        else
            return 0;
    }
    
    public void setAddressOnLine(int lineNo, int address) {
        if (lineNo < nLines && address > 0)
            addressesOnLines[lineNo] = address;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public RS485Rate getRs485Rate() {
        return rs485Rate;
    }

    public void setRs485Rate(RS485Rate rs485Rate) {
        this.rs485Rate = rs485Rate;
    }

    @Override
    @JsonIgnore
    public DeviceType getDeviceType() {
        return DeviceType.adapterModule;
    }

    @Override
    @JsonIgnore
    public String getModelName() {
        return "Модуль сопряжения МС-" + Integer.toString(nLines);
    }

    @Override
    @JsonIgnore
    public AddressDeviceType getAddressDeviceType() {
        assert nLines > 0 && nLines <= 4;
        return ADDRESS_DEVICE_TYPES[nLines - 1];
    }
}
