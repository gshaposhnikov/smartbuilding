package ru.rubezh.firesec.nt.domain.v0.event.context;

import org.springframework.data.annotation.TypeAlias;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.Panel;
import ru.rubezh.firesec.nt.domain.v0.Panel.OutputStatus;
import ru.rubezh.firesec.nt.domain.v0.event.EventCode;

@TypeAlias(value = "PanelOutputFailContext")
public class PanelOutputFailContext extends EventContext {

    private int outputNo;
    private OutputStatus outputStatus;

    public Integer getOutputNo() {
        return outputNo;
    }

    public void setOutputNo(Integer outputNo) {
        this.outputNo = outputNo;
    }

    public OutputStatus getOutputStatus() {
        return outputStatus;
    }

    public void setOutputStatus(OutputStatus outputStatus) {
        this.outputStatus = outputStatus;
    }

    @Override
    public boolean equals(Device device) {
        if (device instanceof Panel) {
            Panel panel = (Panel) device;
            if (outputNo == 1)
                return panel.getOutput1Status().equals(outputStatus);
            else if (outputNo == 2)
                return panel.getOutput2Status().equals(outputStatus);
            else
                return true;//TODO: throw an exception
        }
        else
            return true;//TODO: throw an exception
    }

    @Override
    public void applyTo(Device device) {
        if (device instanceof Panel) {
            Panel panel = (Panel) device;
            if (outputNo == 1)
                panel.setOutput1Status(outputStatus);
            else if (outputNo == 2)
                panel.setOutput2Status(outputStatus);
            //TODO: else - throw an exception

            panel.setDeviceStatus(panel.getCalculatedDeviceStatus());
        }
        //TODO: else - throw an exception
    }

    @Override
    public EventCode getEventCode() {
        EventCode eventCode;
        switch (outputStatus) {
        case normal:
            eventCode = EventCode.panelOutputOk;
            break;
        case shortCircuit:
            eventCode = EventCode.panelOutputShortCircuit;
            break;
        case highLoad:
            eventCode = EventCode.panelOutputHighLoad;
            break;
        case lowLoad:
            eventCode = EventCode.panelOutputLowLoad;
            break;
        case breaked:
        default:
            eventCode = EventCode.panelOutputBreaked;
            break;
        }
        return eventCode;
    }
}
