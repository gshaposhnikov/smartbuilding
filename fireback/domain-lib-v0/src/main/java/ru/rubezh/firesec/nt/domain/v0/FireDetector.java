package ru.rubezh.firesec.nt.domain.v0;

import java.beans.Transient;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Document(collection = "devices.v0")
@TypeAlias(value = "FireDetector")
@JsonTypeName("FireDetector")
public class FireDetector extends Device {

    private boolean dataPresence;
    private boolean isTemperatureChannelFailed;
    private boolean isOpticalChannelFailed;
    private int temperatureValue;
    private boolean isTemperatureSensorActive;
    private boolean isGradientTemperatureSensorActive;
    private boolean isSmokeSensorActive;

    @Override
    @JsonIgnore
    public DeviceType getDeviceType() {
        return DeviceType.fireDetector;
    }

    @Override
    @JsonIgnore
    public String getModelName() {
        return "ИП-29";
    }

    @Override
    @JsonIgnore
    public AddressDeviceType getAddressDeviceType() {
        return AddressDeviceType.NONE;
    }
    
    public boolean getDataPresence() {
        return dataPresence;
    }

    public void setDataPresence(boolean dataPresence) {
        this.dataPresence = dataPresence;
    }

    public boolean getIsTemperatureChannelFailed() {
        return isTemperatureChannelFailed;
    }

    public void setIsTemperatureChannelFailed(boolean temperatureChannelFailure) {
        this.isTemperatureChannelFailed = temperatureChannelFailure;
    }

    public boolean getIsOpticalChannelFailed() {
        return isOpticalChannelFailed;
    }

    public void setIsOpticalChannelFailed(boolean opticalChannelFailure) {
        this.isOpticalChannelFailed = opticalChannelFailure;
    }

    public int getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(int temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public boolean getIsTemperatureSensorActive() {
        return isTemperatureSensorActive;
    }

    public void setIsTemperatureSensorActive(boolean temperatureEvent) {
        this.isTemperatureSensorActive = temperatureEvent;
    }

    public boolean getIsGradientTemperatureSensorActive() {
        return isGradientTemperatureSensorActive;
    }

    public void setIsGradientTemperatureSensorActive(boolean gradientTemperatureEvent) {
        this.isGradientTemperatureSensorActive = gradientTemperatureEvent;
    }

    public boolean getIsSmokeSensorActive() {
        return isSmokeSensorActive;
    }

    public void setIsSmokeSensorActive(boolean smokeEvent) {
        this.isSmokeSensorActive = smokeEvent;
    }

    @JsonIgnore
    @Transient
    public DeviceStatus getCalculatedDeviceStatus() {
        if (!dataPresence)
            return DeviceStatus.nd;
        else if (isTemperatureChannelFailed || isOpticalChannelFailed)
            return DeviceStatus.fail;
        else
            return DeviceStatus.normal;
    }
}
