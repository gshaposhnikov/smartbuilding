Инструкция по работе с backend-сервером Firesec (fireback)
==========================================================

Для Ubuntu 14.04 LTS x64
------------------------

### Установить GIT, maven

    sudo apt-get update
    sudo apt-get install git maven
    
### Установить doxygen, если планируется генерировать документацию:

Ожидается версия не ниже 1.8.15

    sudo apt-get install doxygen


Для Windows 10 x64
------------------

### Установить maven

Скачать и установить c [оф. сайта](https://maven.apache.org/download.cgi)

### Установить doxygen, если планируется генерировать документацию:

Скачать и установить [версию не ниже 1.8.15](http://www.doxygen.nl/download.html)


Общее для обеих систем
----------------------

### Проверить версию oracle java

Ожидается версия 1.8*

    java -version


Компиляция проекта
------------------

Склонировать проект из репозитория

    pushd ~
    git clone git@bitbucket.org:satellite-soft/firesec.git

или, если на bitbucket.org не установлен SSH-ключ
    
    git clone https://<user_name>@bitbucket.org/satellite-soft/firesec.git

Перейти в каталог с приложением

    cd ~/firesec/fireback
	
Выбрать версию релиза

    git checkout v<major>.<minor>

*Опционально*: для включения|отключения авторизации нужно в файле frontend-server/src/main/webapp/WEB-INF/web.xml установить
значение активного профиля withSecurity|withoutSecurity. Пример с авторизацией:

    <context-param>
        <param-name>spring.profiles.active</param-name>
        <param-value>withSecurity</param-value>
    </context-param>

Выполнить компиляцию

    mvn clean install

Вернуться в исходную директорию

    popd


Выполнение интеграционных авто-тестов и генерирование документации
------------------------------------------------------------------

    pushd ~/firesec/fireback
    mvn clean
    mvn -PDOCS_AND_TESTS install
    popd

Добавление нового модуля в проект
---------------------------------

Перейти в каталог с приложением

    pushd ~/firesec/fireback
    
Добавить в вершину списка <modules> в файле `pom.xml` запись с названием нового модуля
(здесь и далее вместо new-module-name - подставить название корректное нового модуля)
    
    <module>new-module-name</module>

Создать соответствующую папку для нового модуля и перейти в нее

    mkdir new-module-name
    cd new-module-name
    
Создать файл pom.xml примерно такого содержания

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    
        <modelVersion>4.0.0</modelVersion>
    
        <parent>
            <groupId>ru.rubezh.firesec.nt</groupId>
            <artifactId>fireback</artifactId>
            <version>unversioned</version>
        </parent>
    
        <artifactId>business-logic</artifactId>
        <version>${global.version}</version>
    
        <name>Имя нового модуля</name>
        <description>Описание нового модуля</description>
    
        <packaging>war</packaging>
        
        <dependencies>
    
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>javax.servlet-api</artifactId>
                <version>3.1.0</version>
            </dependency>
    
            <dependency>
                <groupId>ru.rubezh.firesec.nt</groupId>
                <artifactId>rest-lib-v0</artifactId>
                <version>${global.version}</version>
            </dependency>
    
            <dependency>
                <groupId>ru.rubezh.firesec.nt</groupId>
                <artifactId>library</artifactId>
                <version>${global.version}</version>
            </dependency>
    
        </dependencies>
    
        <build>
            <resources>
                <resource>
                    <directory>src/main/resources</directory>
                    <filtering>true</filtering>
                </resource>
            </resources>
            <finalName>new-module-name</finalName>
        </build>
    
    </project>

При добавлении зависимостей в новый модуль необходимо учитывать иерархию библиотек проекта:


            +--------+         
            | domain |         
            +--------+         
                 |           
                 v    
              +-----+         
              | dao |         
              +-----+         
                 |           
         -----------------
         |               |
         v               v
     +------+          +---------+
     | rest |          | service |
     +------+          +---------+
                            |           
                            v    
                       +--------+         
                       | plugin |         
                       +--------+         
                            |           
                            v    
                       +---------+         
                       | library |         
                       +---------+         


Далее необходимо создать папки для исходников:
    
    mkdir -p src/main/java/ru/firesec
    mkdir -p src/main/resources
    
Скопировать файл настроек журнала из модуля сервера бизнес-логики:

    cp ../business-logic/src/main/resources/log4j2.xml src/main/resources/
    
Использовать существующие модули в качестве шаблонов:

  - business-logic, если новый модуль - это WEB-сервер;
  - HID4Java-example, если новый модуль - исполняемое приложение;
  - dao-lib-v0, если новый модуль - библиотека.


Запуск примера драйвера HID-устройства
--------------------------------------

Перейти в каталог с примером

    cd ~/firesec/fireback/HID4Java-example

Выполнить (вместо * подставить версию)

    java -jar target/HID4Java-example-*.jar
    
Остановить приложение

    Ctrl+C


Запуск примера драйвера USB-транспорта
--------------------------------------

Перейти в каталог с примером

    cd ~/firesec/fireback/usbtransport-example

Выполнить (вместо * подставить версию)

    java -jar target/usbtransport-example-*.jar
    
Остановить приложение

    Ctrl+C
        
