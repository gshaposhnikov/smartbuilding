package ru.rubezh.firesec.nt.plugin.driver.panel.message;

abstract public class GetParameterQuery extends ParameterQuery {
    @Override
    public QueryType getQueryType() {
        return QueryType.getParameter;
    }
}
