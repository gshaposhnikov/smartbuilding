package ru.rubezh.firesec.nt.plugin.driver.panel.message;

import java.util.List;

public class DeviceStateRequest extends GetParameterQuery {

    private int lineNo;
    private List<Integer> addresses;

    @Override
    public ParameterType getParameterType() {
        return ParameterType.devicesState;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public List<Integer> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Integer> addresses) {
        this.addresses = addresses;
    }
}
