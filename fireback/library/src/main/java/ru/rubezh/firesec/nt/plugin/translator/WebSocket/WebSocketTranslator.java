package ru.rubezh.firesec.nt.plugin.translator.WebSocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.translator.AbstractTranslatorPlugin;
import ru.rubezh.firesec.nt.plugin.translator.WebSocket.WebSocketConfigurer;
import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.EventDescriptionRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.Region;
import ru.rubezh.firesec.nt.domain.v0.event.EventDescription;

public class WebSocketTranslator extends AbstractTranslatorPlugin {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    private EventDescriptionRepository eventDescriptionRepository;
    
    public WebSocketTranslator(Logger logger, PlanRepository planRepository, DeviceRepository deviceRepository,
            LogRecordRepository logRecordRepository, EventDescriptionRepository eventDescriptionRepository) {
        super(logger, planRepository, deviceRepository, logRecordRepository);
        this.logger = logger;
        this.deviceRepository = deviceRepository;
        this.eventDescriptionRepository = eventDescriptionRepository;
        this.planRepository = planRepository;
    }

    private void sendToWebSocket(String data) {
        simpMessagingTemplate.convertAndSend(WebSocketConfigurer.webSocketAddress, data);
    }

    @Override
    protected void onPlanUpdated(Plan plan, long currentTimeMs) {
        logger.trace("Plan updated (id: {})", plan.getId());
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(plan);
            json =  "{\"contentType\":\"plan\",\"content\":" + json + "}";
            sendToWebSocket(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onRegionUpdated(Region region, long currentTimeMs) {
        logger.trace("Region updated (name: {})", region.getName());
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(region);
            json =  "{\"contentType\":\"region\",\"content\":" + json + "}";
            sendToWebSocket(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onDeviceUpdated(Device device, long currentTimeMs) {
        logger.trace("Device updated (id: {})", device.getId());
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(device);
            json =  "{\"contentType\":\"device\",\"content\":" + json + "}";
            sendToWebSocket(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onLogRecordInserted(LogRecord logRecord, long currentTimeMs) {
        logger.trace("Log record updated (№: {})", logRecord.getRecordNo());
        try {
            Device panel = deviceRepository.findOne(logRecord.getSourceId());
            if (panel != null) {
                logRecord.setSource(panel);

                Plan plan = planRepository.findOne(panel.getPlanId());

                if (logRecord.getSourceType() == LogRecord.SourceType.device) {
                    Device device = deviceRepository.findOne(logRecord.getSourceDeviceId());
                    if (device != null) {
                        logRecord.setSourceDevice(device);
                        logRecord.setRegion(plan.getRegion(device.getRegionNo()));
                    }
                    else {
                        logger.error("Device (id: {}) not found!", logRecord.getSourceDeviceId());
                    }
                }
                else {
                    logRecord.setRegion(plan.getRegion(panel.getRegionNo()));
                }
            }
            else {
                logger.error("Panel (id: {}) not found!", logRecord.getSourceId());
            }

            EventDescription eventDescription = eventDescriptionRepository.findOneByEventCode(logRecord.getEventCode());
            logRecord.setEventDescription(eventDescription);

            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            String json = mapper.writeValueAsString(logRecord);
            json =  "{\"contentType\":\"event\",\"content\":" + json + "}";
            sendToWebSocket(json);
        }
        catch (Exception e) {
        }
    }
}
