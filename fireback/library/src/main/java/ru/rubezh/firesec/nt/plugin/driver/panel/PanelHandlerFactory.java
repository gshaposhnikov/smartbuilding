package ru.rubezh.firesec.nt.plugin.driver.panel;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.Plan;

public interface PanelHandlerFactory <R> {

    public PanelHandler<R> createPanelHandler(Plan plan, Device device);
}
