package ru.rubezh.firesec.nt.plugin.driver.panel;

import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;

public class PanelHandler<R> {
    public Transport<R> transport;
    public Clock clock;
    public StateMachine<R> stateMachine;
}
