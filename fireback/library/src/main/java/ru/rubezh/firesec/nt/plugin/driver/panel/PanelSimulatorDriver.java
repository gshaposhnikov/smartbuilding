package ru.rubezh.firesec.nt.plugin.driver.panel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.plugin.Plugin;

public class PanelSimulatorDriver<R> implements Plugin {

    private Logger logger;
    
    private PlanRepository planRepository;
    private DeviceRepository deviceRepository;

    private List<PanelHandler<R>> panelHandlers = new ArrayList<>();
    private PanelHandlerFactory<R> panelHandlerFactory;
    
    public PanelSimulatorDriver(Logger logger, PlanRepository planRepository, DeviceRepository deviceRepository,
            PanelHandlerFactory<R> panelHandlerFactory) {
        this.logger = logger;
        this.planRepository = planRepository;
        this.deviceRepository = deviceRepository;
        this.panelHandlerFactory = panelHandlerFactory;
    }

    @Override
    public void onStart(long currentTimeMs) {
        Plan plan = planRepository.findByAdminStatusIsOn();
        if (plan != null) {
            List<Device> devices = deviceRepository.findByPlanId(plan.getId());
            for (Device device: devices) {
                PanelHandler<R> panelHandler = panelHandlerFactory.createPanelHandler(plan, device);
                if (panelHandler != null) {
                    panelHandlers.add(panelHandler);
                    panelHandler.stateMachine.init(currentTimeMs);
                }
            }
        }
    }

    @Override
    public void onStop(long currentTimeMs) {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(long currentTimeMs) {
        for (PanelHandler<R> panelHandler: panelHandlers) {
            try {
                panelHandler.transport.process(currentTimeMs);
                panelHandler.clock.process(currentTimeMs);
            }
            catch (IOException e) {
                logger.error("IO Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
            }
        }
    }
}
