package ru.rubezh.firesec.nt.plugin.driver.panel.message;

public class EventRequest extends GetParameterQuery {

    private LogType logType;
    private long recordIndex;

    @Override
    public ParameterType getParameterType() {
        return ParameterType.event;
    }

    public LogType getLogType() {
        return logType;
    }

    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    public long getRecordIndex() {
        return recordIndex;
    }

    public void setRecordIndex(long recordIndex) {
        this.recordIndex = recordIndex;
    }

}
