package ru.rubezh.firesec.nt.plugin.driver.panel.message;

public enum ParameterType {
    fullPanelState,
    devicesState,
    event
}
