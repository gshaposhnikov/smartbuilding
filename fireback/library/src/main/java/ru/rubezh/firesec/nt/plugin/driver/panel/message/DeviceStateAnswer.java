package ru.rubezh.firesec.nt.plugin.driver.panel.message;

import java.util.List;

import javax.validation.constraints.NotNull;

import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;

public class DeviceStateAnswer extends GetParameterQuery {

    public class DeviceState {
        private Integer address;
        private DeviceStatus deviceStatus;
        private FireStatus fireStatus;
        private Boolean dataPresence;
        private Integer temperatureValue;
        private Boolean isTemperatureSensorActive;
        private Boolean isGradientTemperatureSensorActive;
        private Boolean isTemperatureChannelFailed;

        public Integer getAddress() {
            return address;
        }
        public void setAddress(Integer address) {
            this.address = address;
        }
        public DeviceStatus getDeviceStatus() {
            return deviceStatus;
        }
        public void setDeviceStatus(DeviceStatus deviceStatus) {
            this.deviceStatus = deviceStatus;
        }
        public FireStatus getFireStatus() {
            return fireStatus;
        }
        public void setFireStatus(FireStatus fireStatus) {
            this.fireStatus = fireStatus;
        }
        public Boolean getDataPresence() {
            return dataPresence;
        }
        public void setDataPresence(Boolean dataPresence) {
            this.dataPresence = dataPresence;
        }
        public Integer getTemperatureValue() {
            return temperatureValue;
        }
        public void setTemperatureValue(Integer temperatureValue) {
            this.temperatureValue = temperatureValue;
        }
        public Boolean getIsTemperatureSensorActive() {
            return isTemperatureSensorActive;
        }
        public void setIsTemperatureSensorActive(Boolean isTemperatureSensorActive) {
            this.isTemperatureSensorActive = isTemperatureSensorActive;
        }
        public Boolean getIsGradientTemperatureSensorActive() {
            return isGradientTemperatureSensorActive;
        }
        public void setIsGradientTemperatureSensorActive(Boolean isGradientTemperatureSensorActive) {
            this.isGradientTemperatureSensorActive = isGradientTemperatureSensorActive;
        }
        public Boolean getIsTemperatureChannelFailed() {
            return isTemperatureChannelFailed;
        }
        public void setIsTemperatureChannelFailed(Boolean isTemperatureChannelFailed) {
            this.isTemperatureChannelFailed = isTemperatureChannelFailed;
        }

        public boolean equals(@NotNull FireDetector fireDetector) {
            return fireDetector.getDeviceStatus() == getDeviceStatus() &&
                    fireDetector.getFireStatus() == getFireStatus() &&
                    fireDetector.getDataPresence() == getDataPresence() &&
                    fireDetector.getIsTemperatureSensorActive() == getIsTemperatureSensorActive() &&
                    fireDetector.getIsGradientTemperatureSensorActive() == getIsGradientTemperatureSensorActive() &&
                    fireDetector.getIsTemperatureChannelFailed() == getIsTemperatureChannelFailed() &&
                    fireDetector.getTemperatureValue() == getTemperatureValue();

        }

        public void applyTo(@NotNull FireDetector fireDetector) {
            fireDetector.setDeviceStatus(getDeviceStatus());
            fireDetector.setFireStatus(getFireStatus());
            fireDetector.setDataPresence(getDataPresence());
            fireDetector.setIsTemperatureSensorActive(getIsTemperatureSensorActive());
            fireDetector.setIsGradientTemperatureSensorActive(getIsGradientTemperatureSensorActive());
            fireDetector.setIsTemperatureChannelFailed(getIsTemperatureChannelFailed());
            fireDetector.setTemperatureValue(getTemperatureValue());
        }
    }

    Integer lineNo;
    List<DeviceState> devicesStates;

    @Override
    public ParameterType getParameterType() {
        return ParameterType.devicesState;
    }

    public Integer getLineNo() {
        return lineNo;
    }

    public void setLineNo(Integer lineNo) {
        this.lineNo = lineNo;
    }

    public List<DeviceState> getDevicesStates() {
        return devicesStates;
    }

    public void setDevicesStates(List<DeviceState> devicesStates) {
        this.devicesStates = devicesStates;
    }

}
