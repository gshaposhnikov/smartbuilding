package ru.rubezh.firesec.nt.plugin.translator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.Region;
import ru.rubezh.firesec.nt.plugin.Plugin;
import ru.rubezh.firesec.nt.service.StateUpdateSubscriber;

abstract public class AbstractTranslatorPlugin implements Plugin, StateUpdateSubscriber {

    protected Logger logger;

        /* repositories */
    protected PlanRepository planRepository;
    protected DeviceRepository deviceRepository;
    protected LogRecordRepository logRecordRepository;

    public AbstractTranslatorPlugin(Logger logger, PlanRepository planRepository, DeviceRepository deviceRepository,
            LogRecordRepository logRecordRepository) {
        this.logger = logger;
        this.planRepository = planRepository;
        this.deviceRepository = deviceRepository;
        this.logRecordRepository = logRecordRepository;
    }
    
    private class PlanIdAndRegionNo {
        public String planId;
        public int regionNo;

        public PlanIdAndRegionNo(String planId, int regionNo) {
            this.planId = planId;
            this.regionNo = regionNo;
        }
    }

    private Set<String> updatedPlanIds = new HashSet<>();
    private Set<PlanIdAndRegionNo> updatedRegionNos = new HashSet<>();
    private Set<String> updatedDeviceIds = new HashSet<>();
    private Set<String> insertedLogRecordsIds = new HashSet<>();
    private final int nMaxLogRecordsPerIteration = 100;

    @Override
    public synchronized void onPlanUpdated(String planId) {
        updatedPlanIds.add(planId);
    }

    @Override
    public synchronized void onRegionUpdated(String planId, int regionNo) {
        PlanIdAndRegionNo planIdAndRegionNo = new PlanIdAndRegionNo(planId, regionNo);
        updatedRegionNos.add(planIdAndRegionNo);
    }

    @Override
    public synchronized void onDeviceUpdated(String deviceId) {
        updatedDeviceIds.add(deviceId);
    }

    @Override
    public synchronized void onLogRecordInserted(String logRecordId) {
        insertedLogRecordsIds.add(logRecordId);
    }

    @Override
    public synchronized void onStart(long currentTimeMs) {
        //TODO: something
    }

    @Override
    public void onStop(long currentTimeMs) {
        //TODO: something
    }

    @Override
    public void process(long currentTimeMs) {
        processPlansUpdates(currentTimeMs);
        processRegionsUpdates(currentTimeMs);
        processDevicesUpdates(currentTimeMs);
        processLogRecordsUpdates(currentTimeMs);
    }

    private synchronized void processPlansUpdates(long currentTimeMs) {
        Iterator<String> updatedPlanIdIt = updatedPlanIds.iterator();
        if (updatedPlanIdIt.hasNext()) {
            String planId = updatedPlanIdIt.next();
            updatedPlanIdIt.remove();

            Plan plan = planRepository.findOne(planId);
            if (plan != null)
                onPlanUpdated(plan, currentTimeMs);
            else
                logger.error("Plan entity not found (id: {})", planId);
        }
    }

    private synchronized void processRegionsUpdates(long currentTimeMs) {
        Iterator<PlanIdAndRegionNo> updatedPlanIdAndRegionNoIt = updatedRegionNos.iterator();
        if (updatedPlanIdAndRegionNoIt.hasNext()) {
            PlanIdAndRegionNo planIdAndRegionNo = updatedPlanIdAndRegionNoIt.next();
            updatedPlanIdAndRegionNoIt.remove();

            Plan plan = planRepository.findOne(planIdAndRegionNo.planId);
            if (plan != null && planIdAndRegionNo.regionNo < plan.getRegions().size())
                onRegionUpdated(plan.getRegions().get(planIdAndRegionNo.regionNo), currentTimeMs);
            else
                logger.error("Region entity not found (plan id: {}, region number {})", planIdAndRegionNo.planId, planIdAndRegionNo.regionNo);
        }
    }

    private synchronized void processDevicesUpdates(long currentTimeMs) {
        Iterator<String> updatedDeviceIdIt = updatedDeviceIds.iterator();
        if (updatedDeviceIdIt.hasNext()) {
            String deviceId = updatedDeviceIdIt.next();
            updatedDeviceIdIt.remove();

            Device device = deviceRepository.findOne(deviceId);
            if (device != null)
                onDeviceUpdated(device, currentTimeMs);
            else
                logger.error("Device entity not found (id: {})", deviceId);
        }
    }

    private synchronized void processLogRecordsUpdates(long currentTimeMs) {
        int i = 0;
        for (Iterator<String> insertedLogRecordIdIt = insertedLogRecordsIds.iterator(); insertedLogRecordIdIt.hasNext() && i < nMaxLogRecordsPerIteration; ++i) {
            String logRecordId = insertedLogRecordIdIt.next();
            insertedLogRecordIdIt.remove();
            LogRecord logRecord = logRecordRepository.findOne(logRecordId);
            onLogRecordInserted(logRecord, currentTimeMs);
        }
        
        /* Если за итерацию пришло слишком много записей логов, то не уведомляем о них. Они доступны по REST-API */
        insertedLogRecordsIds.clear();
    }

    abstract protected void onPlanUpdated(Plan plan, long currentTimeMs);
    abstract protected void onRegionUpdated(Region region, long currentTimeMs);
    abstract protected void onDeviceUpdated(Device device, long currentTimeMs);
    abstract protected void onLogRecordInserted(LogRecord logRecord, long currentTimeMs);
}
