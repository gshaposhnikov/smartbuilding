package ru.rubezh.firesec.nt.plugin.driver.panel.message;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;
import ru.rubezh.firesec.nt.domain.v0.Device.DeviceType;
import ru.rubezh.firesec.nt.domain.v0.LogRecord.EventType;
import ru.rubezh.firesec.nt.domain.v0.Panel.OutputStatus;
import ru.rubezh.firesec.nt.domain.v0.Panel.PowerStatus;
import ru.rubezh.firesec.nt.domain.v0.event.context.DeviceLostEventContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.FireDetectorFailEventContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.FireEventContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.PanelCommonFailContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.PanelOutputFailContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.PanelPowerFailContext;
import ru.rubezh.firesec.nt.domain.v0.event.context.FireDetectorFailEventContext.FailStatus;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.DeviceStateAnswer.DeviceState;

public class BsonMessageCodec implements MessageCodec<BasicDBObject> {
    private enum PanelEventType {
        fail,
        powerFail,
        outputFail
    }

    private enum DeviceEventType {
        fire,
        lost,
        fail
    }

    private Logger logger;
    
    public BsonMessageCodec(Logger logger) {
        this.logger = logger;
    }

    @Override
    public BasicDBObject encodeFullPanelStateRequest(FullPanelStateRequest request) {
        BasicDBObject rawObject = new BasicDBObject();
        rawObject.put("cmd", request.getQueryType().toString());
        rawObject.put("parameter", request.getParameterType().toString());
        return rawObject;
    }

    @Override
    public BasicDBObject encodeDeviceStateRequest(DeviceStateRequest request) {
        BasicDBObject rawObject = new BasicDBObject();
        rawObject.put("cmd", request.getQueryType().toString());
        rawObject.put("parameter", request.getParameterType().toString());
        rawObject.put("lineNo", request.getLineNo());

        BasicDBList devices = new BasicDBList();
        Iterator<Integer> it = request.getAddresses().iterator();
        if (it.hasNext()) {
            Integer address = it.next();
            Integer startAddress = address;
            Integer prevAddress = address;
            int count = 1;
            while (it.hasNext()) {
                address = it.next();
                if ((address - prevAddress) == 1)
                    count ++;
                else {
                    BasicDBObject deviceAddressGroup = new BasicDBObject();
                    deviceAddressGroup.put("startAddress", startAddress);
                    deviceAddressGroup.put("count", count);
                    devices.add(deviceAddressGroup);

                    startAddress = address;
                    count = 1;
                }
                prevAddress = address;
            }
            BasicDBObject deviceAddressGroup = new BasicDBObject();
            deviceAddressGroup.put("startAddress", startAddress);
            deviceAddressGroup.put("count", count);
            devices.add(deviceAddressGroup);
        }
        rawObject.put("devices", devices);

        return rawObject;
    }

    @Override
    public BasicDBObject encodeEventRequest(EventRequest request) {
        BasicDBObject rawObject = new BasicDBObject();
        rawObject.put("cmd", request.getQueryType().toString());
        rawObject.put("parameter", request.getParameterType().toString());
        rawObject.put("logType", request.getLogType().toString());
        rawObject.put("recordIndex", request.getRecordIndex());
        return rawObject;
    }

    @Override
    public FullPanelStateAnswer decodeFullPanelStateAnswer(BasicDBObject rawAnswer) {
        try {
            FullPanelStateAnswer answer = new FullPanelStateAnswer();
            BasicDBObject panelState = (BasicDBObject) rawAnswer.get("fullPanelState");
            answer.setDeviceStatus(DeviceStatus.valueOf(panelState.getString("deviceStatus")));
            answer.setFireStatus(FireStatus.valueOf(panelState.getString("fireStatus")));
            answer.setFireLogIndex(panelState.getInt("fireLogIndex"));
            return answer;
        }
        catch (Exception e) {
            logger.warn("Decoding failed with exception (cause: {}, message: {}, rawAnswer: {})", e.getCause(), e.getMessage(), rawAnswer.toJson());
            return null;
        }
    }

    @Override
    public DeviceStateAnswer decodeDeviceStateAnswer(BasicDBObject rawAnswer) {
        try {
            DeviceStateAnswer answer = new DeviceStateAnswer();
            answer.setLineNo(rawAnswer.getInt("lineNo"));
            List<DeviceState> devicesStates = new ArrayList<>();
            BasicDBList rawDeviceStates = (BasicDBList) rawAnswer.get("devices");
            for (Iterator<Object> it = rawDeviceStates.iterator(); it.hasNext();) {
                BasicDBObject rawDeviceState = (BasicDBObject) it.next();
                DeviceState deviceState = answer.new DeviceState();
                deviceState.setAddress(rawDeviceState.getInt("address"));
                deviceState.setDataPresence(rawDeviceState.getBoolean("dataPresence"));
                deviceState.setDeviceStatus(DeviceStatus.valueOf(rawDeviceState.getString("deviceStatus")));
                deviceState.setFireStatus(FireStatus.valueOf(rawDeviceState.getString("fireStatus")));
                BasicDBObject rawFireDetector = (BasicDBObject) rawDeviceState.get("fireDetector");
                deviceState.setIsTemperatureSensorActive(rawFireDetector.getBoolean("temperatureEvent"));
                deviceState.setIsGradientTemperatureSensorActive(rawFireDetector.getBoolean("gradientTemperatureEvent"));
                deviceState.setIsTemperatureChannelFailed(rawFireDetector.getBoolean("temperatureChannelFailure"));
                deviceState.setTemperatureValue(rawFireDetector.getInt("temperatureValue"));
                devicesStates.add(deviceState);
            }
            answer.setDevicesStates(devicesStates);
            return answer;
        }
        catch (Exception e) {
            logger.warn("Decoding failed with exception (cause: {}, message: {}, rawAnswer: {})", e.getCause(), e.getMessage(), rawAnswer.toJson());
            return null;
        }
    }

    private void decodeFireDetectorEvent(BasicDBObject rawAnswer, EventAnswer answer) {
        BasicDBObject rawObjectContext = (BasicDBObject) rawAnswer.get("deviceEventContext");
        answer.setLineNo(rawObjectContext.getInt("lineNo"));
        answer.setAddress(rawObjectContext.getInt("address"));
        DeviceEventType eventType = DeviceEventType.valueOf(rawObjectContext.getString("eventType"));
        BasicDBObject rawEventContext;
        switch (eventType) {
        case fail:
            rawEventContext = (BasicDBObject) rawObjectContext.get("failEventContext");
            FireDetectorFailEventContext fireDetectorFailEventContext = new FireDetectorFailEventContext();
            fireDetectorFailEventContext.setFailStatus(FailStatus.valueOf(rawEventContext.getString("failStatus")));
            fireDetectorFailEventContext.setIsOpticalChannelFailed(rawEventContext.getBoolean("opticalChannelFailure"));
            fireDetectorFailEventContext.setIsTemperatureChannelFailed(rawEventContext.getBoolean("temperatureChannelFailure"));
            answer.setEventType(EventType.fireDetectorFail);
            answer.setEventContext(fireDetectorFailEventContext);
            break;
        case fire:
            rawEventContext = (BasicDBObject) rawObjectContext.get("fireEventContext");
            FireEventContext fireEventContext = new FireEventContext();
            fireEventContext.setFireStatus(FireStatus.valueOf(rawEventContext.getString("fireStatus")));
            fireEventContext.setIsGradientTemperatureSensorActive(rawEventContext.getBoolean("gradientTemperatureEvent"));
            fireEventContext.setIsSmokeSensorActive(rawEventContext.getBoolean("smokeEvent"));
            fireEventContext.setIsTemperatureSensorActive(rawEventContext.getBoolean("temperatureEvent"));
            fireEventContext.setTemperatureValue(rawEventContext.getInt("temperatureValue"));
            answer.setEventType(EventType.fire);
            answer.setEventContext(fireEventContext);
            break;
        case lost:
            rawEventContext = (BasicDBObject) rawObjectContext.get("lostEventContext");
            DeviceLostEventContext deviceLostEventContext = new DeviceLostEventContext();
            deviceLostEventContext.setIsLost(rawEventContext.getBoolean("lost"));
            answer.setEventType(EventType.deviceLost);
            answer.setEventContext(deviceLostEventContext);
            break;
        default:
            break;
        }
    }

    private void decodePanelContext(BasicDBObject rawAnswer, EventAnswer answer) {
        BasicDBObject rawObjectContext = (BasicDBObject) rawAnswer.get("panelEventContext");
        PanelEventType eventType = PanelEventType.valueOf(rawObjectContext.getString("eventType"));
        BasicDBObject rawEventContext;
        switch (eventType) {
        case fail:
            rawEventContext = (BasicDBObject) rawObjectContext.get("failEventContext");
            PanelCommonFailContext panelCommonFailContext = new PanelCommonFailContext();
            panelCommonFailContext.setIsFail(rawEventContext.getBoolean("fail"));
            answer.setEventType(EventType.panelCommonFail);
            answer.setEventContext(panelCommonFailContext);
            break;
        case outputFail:
            rawEventContext = (BasicDBObject) rawObjectContext.get("outputFailEventContext");
            PanelOutputFailContext panelOutputFailContext = new PanelOutputFailContext();
            panelOutputFailContext.setOutputStatus(OutputStatus.valueOf(rawEventContext.getString("outputStatus")));
            panelOutputFailContext.setOutputNo(rawEventContext.getInt("outputNo"));
            answer.setEventType(EventType.panelOutputFail);
            answer.setEventContext(panelOutputFailContext);
            break;
        case powerFail:
            rawEventContext = (BasicDBObject) rawObjectContext.get("powerFailEventContext");
            PanelPowerFailContext panelPowerFailContext = new PanelPowerFailContext();
            panelPowerFailContext.setPowerStatus(PowerStatus.valueOf(rawEventContext.getString("powerStatus")));
            panelPowerFailContext.setInputNo(rawEventContext.getInt("powerInput"));
            answer.setEventType(EventType.panelPowerFail);
            answer.setEventContext(panelPowerFailContext);
            break;
        default:
            break;
        }
    }

    @Override
    public EventAnswer decodeEventAnswer(BasicDBObject rawAnswer) {
        try {
            EventAnswer answer = new EventAnswer();
            answer.setDateTime(new Date((long)rawAnswer.getInt("eventTime") * 1000));
            answer.setLogType(LogType.valueOf(rawAnswer.getString("logType")));
            answer.setRecordIndex(rawAnswer.getLong("recordIndex"));
            DeviceType deviceType = DeviceType.valueOf(rawAnswer.getString("deviceType"));
            answer.setDeviceType(deviceType);
            if (deviceType == DeviceType.fireDetector)
                decodeFireDetectorEvent(rawAnswer, answer);
            else
                decodePanelContext(rawAnswer, answer);
            return answer;
        }
        catch (Exception e) {
            logger.warn("Decoding failed with exception (cause: {}, message: {}, rawAnswer: {})", e.getCause(), e.getMessage(), rawAnswer.toJson());
            return null;
        }
    }
}
