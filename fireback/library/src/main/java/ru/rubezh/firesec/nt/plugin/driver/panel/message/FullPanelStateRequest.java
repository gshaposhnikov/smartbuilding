package ru.rubezh.firesec.nt.plugin.driver.panel.message;

public class FullPanelStateRequest extends GetParameterQuery {
    @Override
    public ParameterType getParameterType() {
        return ParameterType.fullPanelState;
    }
}
