package ru.rubezh.firesec.nt.plugin.driver.panel.message;

import javax.validation.constraints.NotNull;

import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;
import ru.rubezh.firesec.nt.domain.v0.Panel;

public class FullPanelStateAnswer extends FullPanelStateRequest {

    private Integer fireLogIndex;
    private DeviceStatus deviceStatus;
    private FireStatus fireStatus;

    public Integer getFireLogIndex() {
        return fireLogIndex;
    }

    public void setFireLogIndex(Integer fireLogIndex) {
        this.fireLogIndex = fireLogIndex;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public FireStatus getFireStatus() {
        return fireStatus;
    }

    public void setFireStatus(FireStatus fireStatus) {
        this.fireStatus = fireStatus;
    }

    public boolean equals(@NotNull Panel panel) {
        return getDeviceStatus().equals(panel.getDeviceStatus()) && getFireStatus().equals(panel.getFireStatus());
    }

    public void applyTo(@NotNull Panel panel) {
        panel.setDeviceStatus(getDeviceStatus());
        panel.setFireStatus(getFireStatus());
    }
}
