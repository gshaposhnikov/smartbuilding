package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.InterruptedByTimeoutException;
import java.nio.channels.SocketChannel;
import java.nio.channels.UnresolvedAddressException;
import java.nio.channels.UnsupportedAddressTypeException;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBDecoder;
import com.mongodb.DBEncoder;
import com.mongodb.DefaultDBDecoder;
import com.mongodb.DefaultDBEncoder;

//http://www.java2s.com/Tutorials/Java/Java_Network/0070__Java_Network_Non-Blocking_Socket.htm
public class TcpBsonTransport implements Transport<BasicDBObject> {

    private Logger logger;

    private InetSocketAddress panelAddress;
    private SocketChannel channel;

    private int maxWaitTimeMs = 1000;

    private long startWaitTimeMs;

    private ConnectHandler connectHandler = null;
    private ReceiveHandler<BasicDBObject> receiveHandler = null;
    private SendHandler sendHandler = null;
    private SendReceiveFailedHandler sendFailedHandler = null;
    private SendReceiveFailedHandler receiveFailedHandler = null;

    private int sequenceNo = 0;
    private int receiveSequenceNo;

    private ByteBuffer outputBuffer;
    private ByteBuffer inputBuffer = ByteBuffer.allocate(2048);

    public TcpBsonTransport(Logger logger, String hostname, int port) throws IOException {
        this.logger = logger;
        panelAddress = new InetSocketAddress(hostname, port);
        channel = SocketChannel.open();
        channel.configureBlocking(false);
    }

    public void setMaxWaitTime(int maxWaitTimeMs) {
        this.maxWaitTimeMs = maxWaitTimeMs;
    }

    public void setInputBufferCapacity(int capacity) {
        inputBuffer = ByteBuffer.allocate(capacity);
    }

    @Override
    public void connect(long currentTimeMs, @NotNull ConnectHandler connectHandler) {
        try {
            channel.connect(panelAddress);
            startWaitTimeMs = currentTimeMs;
            this.connectHandler = connectHandler;
        }
        catch (UnresolvedAddressException e) {
            logger.error("Unresolved address Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
            connectHandler.onConnected(currentTimeMs, false);
        }
        catch (UnsupportedAddressTypeException e) {
            logger.error("Unsupported address Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
            connectHandler.onConnected(currentTimeMs, false);
        }
        catch (SecurityException e) {
            logger.error("Security Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
            connectHandler.onConnected(currentTimeMs, false);
        }
        catch (IOException e) {
            logger.error("IO Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
            connectHandler.onConnected(currentTimeMs, false);
        }
    }

    @Override
    public int send(long currentTimeMs, @NotNull BasicDBObject rawData, @NotNull SendHandler sendHandler, @NotNull SendReceiveFailedHandler failedHandler) {
        rawData.put("sequenceNo", ++sequenceNo);
        DBEncoder encoder = DefaultDBEncoder.FACTORY.create();
        OutputBuffer ob = new BasicOutputBuffer();
        encoder.writeObject(ob, rawData);
        outputBuffer = ByteBuffer.wrap(ob.toByteArray());
        startWaitTimeMs = currentTimeMs;
        this.sendHandler = sendHandler;
        this.sendFailedHandler = failedHandler;
        return sequenceNo;
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, @NotNull ReceiveHandler<BasicDBObject> receiveHandler, @NotNull SendReceiveFailedHandler failedHandler) {
        inputBuffer.clear();
        startWaitTimeMs = currentTimeMs;
        this.receiveHandler = receiveHandler;
        this.receiveFailedHandler = failedHandler;
        this.receiveSequenceNo = sequenceNo;
    }

    @Override
    public void disconnect(long currentTimeMs) throws IOException {
        resetChannel();
    }

    public void process(long currentTimeMs)  throws IOException {
        if (connectHandler != null)
            processConnect(currentTimeMs);
        else if (sendHandler != null)
            processSend(currentTimeMs);
        else if (receiveHandler != null)
            processReceive(currentTimeMs);
    }

    private void resetChannel() throws IOException {
        channel.close();
        channel = SocketChannel.open();
        channel.configureBlocking(false);
    }

    private void processConnect(long currentTimeMs)  throws IOException {
        if (channel.isConnected()) {
            logger.warn("Wrong transport state, already connected");
            connectHandler.onConnected(currentTimeMs, true);
            connectHandler = null;
        }
        else {
            if (channel.isConnectionPending()) {
                try {
                    if (channel.finishConnect()) {
                        connectHandler.onConnected(currentTimeMs, true);
                        connectHandler = null;
                    }
                    else if (startWaitTimeMs + maxWaitTimeMs <= currentTimeMs)
                        throw new InterruptedByTimeoutException();
                }
                catch (IOException e) {
                    connectHandler.onConnected(currentTimeMs, false);
                    connectHandler = null;
                    resetChannel();
                }
            }
            else {
                logger.warn("Wrong transport state, no connection pending");
                connectHandler.onConnected(currentTimeMs, false);
                connectHandler = null;
            }
        }
    }

    private void processSend(long currentTimeMs)  throws IOException {
        if (channel.isConnected()) {
            try {
                channel.write(outputBuffer);
                if (!outputBuffer.hasRemaining()) {
                    sendHandler.onSended(currentTimeMs, sequenceNo);
                    sendHandler = null;
                    sendFailedHandler = null;
                }
                else if (startWaitTimeMs + maxWaitTimeMs <= currentTimeMs)
                    throw new InterruptedByTimeoutException();
            } catch (IOException e) {
                sendFailedHandler.onFailed(currentTimeMs, sequenceNo);
                sendHandler = null;
                sendFailedHandler = null;
                disconnect(currentTimeMs);
            }
        }
        else {
            logger.warn("Wrong transport state, no connection");
            sendFailedHandler.onFailed(currentTimeMs, sequenceNo);
            sendHandler = null;
            sendFailedHandler = null;
        }
    }

    private void processReceive(long currentTimeMs)  throws IOException {
        if (channel.isConnected()) {
            try {
                if (channel.read(inputBuffer) > 0) {
                    DBDecoder decoder = DefaultDBDecoder.FACTORY.create();
                    BasicDBObject response = (BasicDBObject) decoder.decode(inputBuffer.array(), (DBCollection) null);
                    if (response != null && response.containsField("sequenceNo")) {
                        if (response.getInt("sequenceNo") == receiveSequenceNo)
                            receiveHandler.onReceived(currentTimeMs, receiveSequenceNo, response);
                        else {
                            //TODO: message buffering 
                            receiveFailedHandler.onFailed(currentTimeMs, receiveSequenceNo);
                            disconnect(currentTimeMs);
                        }
                        receiveHandler = null;
                        receiveFailedHandler = null;
                    }
                }
                else if (startWaitTimeMs + maxWaitTimeMs <= currentTimeMs)
                    throw new InterruptedByTimeoutException();
            } catch (IOException e) {
                receiveFailedHandler.onFailed(currentTimeMs, receiveSequenceNo);
                receiveHandler = null;
                receiveFailedHandler = null;
                disconnect(currentTimeMs);
            }
        }
        else {
            logger.warn("Wrong transport state, no connection");
            receiveFailedHandler.onFailed(currentTimeMs, receiveSequenceNo);
            receiveHandler = null;
            receiveFailedHandler = null;
        }
    }

}
