package ru.rubezh.firesec.nt.plugin.translator;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.Region;

public class TestTranslator extends AbstractTranslatorPlugin {
    
    public TestTranslator(Logger logger, PlanRepository planRepository, DeviceRepository deviceRepository,
            LogRecordRepository logRecordRepository) {
        super(logger, planRepository, deviceRepository, logRecordRepository);
    }

    @Override
    protected void onPlanUpdated(Plan plan, long currentTimeMs) {
        System.out.printf("Plan updated (at %tF %<tT):\n", currentTimeMs);
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(plan);
            System.out.println(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onRegionUpdated(Region region, long currentTimeMs) {
        System.out.printf("Region updated (at %tF %<tT):\n", currentTimeMs);
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(region);
            System.out.println(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onDeviceUpdated(Device device, long currentTimeMs) {
        System.out.printf("Device updated (at %tF %<tT):\n", currentTimeMs);
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(device);
            System.out.println(json);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onLogRecordInserted(LogRecord logRecord, long currentTimeMs) {
        System.out.printf("Log record inserted (at %tF %<tT):\n", currentTimeMs);
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(logRecord);
            System.out.println(json);
        }
        catch (Exception e) {
        }
    }
}
