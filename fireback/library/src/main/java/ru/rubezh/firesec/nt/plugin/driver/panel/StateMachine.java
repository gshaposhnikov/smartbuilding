package ru.rubezh.firesec.nt.plugin.driver.panel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Panel;
import ru.rubezh.firesec.nt.domain.v0.Device.DeviceType;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.DeviceStateAnswer;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.DeviceStateAnswer.DeviceState;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.DeviceStateRequest;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.EventAnswer;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.EventRequest;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.FullPanelStateAnswer;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.FullPanelStateRequest;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.LogType;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.MessageCodec;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;

//TODO: tracing
//TODO: start/stop methods. Save panel in stop method.
//TODO: setters for configuration and get config from DB in @Bean method
//TODO: constant handler for insuccess from transport and dynamic handlers only for success
public class StateMachine<R> {

    Transport<R> transport;
    Clock clock;

        /* beans */
    MessageCodec<R> messageCodec;

    DeviceRepository deviceRepository;
    LogRecordRepository logRecordRepository;
    Logger logger;

        /* timers and etc. */
    int reconnectInterval = 10000;
    int stateRequestInterval = 500;
    int deviceStateRequestFrequency = 4;

        /* model */
    Panel panel;
    List<FireDetector> fireDetectors;

        /* state */
    int nFullPanelStateRequests = 0;
    int panelFireLogIndex;
    int awaitingSequenceNo;
    Iterator<FireDetector> currentPollDetector;

        /* helpers */
    private void setHierarchyDeviceStatus(DeviceStatus deviceStatus) {
        panel.setDeviceStatus(deviceStatus);
        panel = deviceRepository.save(panel);
        List<Device> devices = deviceRepository.findByPlanIdAndParentDeviceId(panel.getPlanId(), panel.getId());
        for (Device device: devices)
            device.setDeviceStatus(deviceStatus);
        deviceRepository.save(devices);
    }

        /* hooks */
    public void onDisconnected() {
        if (panel.getDeviceStatus() != DeviceStatus.lost)
            setHierarchyDeviceStatus(DeviceStatus.lost);
    }

    public StateMachine(Transport<R> transport,
            Clock clock,
            MessageCodec<R> messageCodec,
            DeviceRepository deviceRepository,
            LogRecordRepository logRecordRepository,
            Logger logger,
            Panel panel,
            List<FireDetector> fireDetectors) {
        this.transport = transport;
        this.clock = clock;
        this.messageCodec = messageCodec;
        this.deviceRepository = deviceRepository;
        this.logRecordRepository = logRecordRepository;
        this.logger = logger;
        this.panel = panel;
        this.fireDetectors = fireDetectors;
        currentPollDetector = this.fireDetectors.iterator();
    }

    public void init(long currentTimeMs) {
        setHierarchyDeviceStatus(DeviceStatus.nd);
        toConnect(currentTimeMs);
    }

    public void toConnect(long currentTimeMs) {
        transport.connect(currentTimeMs, this::connected);
    }

    public void connected(long currentTimeMs, boolean isSuccess) {
        if (isSuccess)
            clock.setTimer(currentTimeMs, stateRequestInterval, this::toRequestFullPanelState);
        else {
            onDisconnected();
            clock.setTimer(currentTimeMs, reconnectInterval, this::toConnect);
        }
    }

    public void toRequestFullPanelState(long currentTimeMs) {
        FullPanelStateRequest request = new FullPanelStateRequest();
        R rawData = messageCodec.encodeFullPanelStateRequest(request);
        awaitingSequenceNo = transport.send(currentTimeMs, rawData, this::fullPanelStateRequestSended, this::sendReceiveFailed);
        nFullPanelStateRequests++;
    }

    public void fullPanelStateRequestSended(long currentTimeMs, int sequenceNo) {
        transport.receive(currentTimeMs, sequenceNo, this::fullPanelStateReceived, this::sendReceiveFailed);
    }

    public void sendReceiveFailed(long currentTimeMs, int sequenceNo) {
        onDisconnected();
        clock.setTimer(currentTimeMs, reconnectInterval, this::toConnect);
    }
    
    public void fullPanelStateReceived(long currentTimeMs, int sequnceNo, R rawData) {
        FullPanelStateAnswer answer = messageCodec.decodeFullPanelStateAnswer(rawData);
        if (answer != null) {
            if (!answer.equals(panel)) {
                answer.applyTo(panel);
                panel = deviceRepository.save(panel);
            }
            panelFireLogIndex = answer.getFireLogIndex();
        }
        if (panel.getFireLogIndex() < panelFireLogIndex) {
            EventRequest eventRequest = new EventRequest();
            eventRequest.setLogType(LogType.fire);
            eventRequest.setRecordIndex(panel.getFireLogIndex() + 1);
            R rawEventRequest = messageCodec.encodeEventRequest(eventRequest);
            awaitingSequenceNo = transport.send(currentTimeMs, rawEventRequest, this::eventRequestSended, this::sendReceiveFailed);
        }
        else {
            if (fireDetectors.size() > 0 && nFullPanelStateRequests >= deviceStateRequestFrequency) {
                nFullPanelStateRequests = 0;
                if (!currentPollDetector.hasNext())
                    currentPollDetector = fireDetectors.iterator();

                FireDetector fireDetector = currentPollDetector.next();

                DeviceStateRequest deviceStateRequest = new DeviceStateRequest();
                deviceStateRequest.setLineNo(fireDetector.getLineNo());
                List<Integer> addresses = new ArrayList<>();
                addresses.add(fireDetector.getAddress());
                deviceStateRequest.setAddresses(addresses);

                R rawDeviceStateRequest = messageCodec.encodeDeviceStateRequest(deviceStateRequest);
                awaitingSequenceNo = transport.send(currentTimeMs, rawDeviceStateRequest, this::deviceStateRequestSended, this::sendReceiveFailed);
            }
            else
                clock.setTimer(currentTimeMs, stateRequestInterval, this::toRequestFullPanelState);

        }
    }

    public void eventRequestSended(long currentTimeMs, int sequenceNo) {
        transport.receive(currentTimeMs, sequenceNo, this::eventReceived, this::sendReceiveFailed);
    }

    public void eventReceived(long currentTimeMs, int sequenceNo, R rawData) {
            EventAnswer answer = messageCodec.decodeEventAnswer(rawData);
            if (answer == null) {
                    /* consider no event with requested index, increment last one not to request it again */
                panel.setFireLogIndex(panel.getFireLogIndex() + 1);
            }
            else {
                panel.setFireLogIndex(answer.getRecordIndex());
                LogRecord logRecord = new LogRecord();
                logRecord.setSourceId(panel.getId());
                answer.applyTo(logRecord);
                if (answer.getDeviceType() == DeviceType.fireDetector) {
                    Device device = deviceRepository.findOneByParentDeviceIdAndLineNoAndAddress(panel.getId(), answer.getLineNo(), answer.getAddress());
                    if (device != null) {
                        logRecord.setSourceDeviceId(device.getId());
                            /* apply changes to the entity */
                        if (!logRecord.getEventContext().equals(device)) {
                            logRecord.getEventContext().applyTo(device);
                            deviceRepository.save(device);
                        }
                    }
                    else
                        logger.warn("Device not found (panel address: {}, line number: {}, device address: {})", panel.getAddress(), answer.getLineNo(), answer.getAddress());
                }
                else {
                        /* apply changes to the entity */
                    if (!logRecord.getEventContext().equals(panel)) {
                        logRecord.getEventContext().applyTo(panel);
                        panel = deviceRepository.save(panel);
                    }
                }
                logRecordRepository.insert(logRecord);
            }
            /*
             * Notice!
             * If only log index was modified in panel,
             * it won't be saved to repository till
             * bigger modifications or state machine stop.
             */

            if (panel.getFireLogIndex() < panelFireLogIndex) {
                EventRequest eventRequest = new EventRequest();
                eventRequest.setLogType(LogType.fire);
                eventRequest.setRecordIndex(panel.getFireLogIndex() + 1);
                R rawEventRequest = messageCodec.encodeEventRequest(eventRequest);
                awaitingSequenceNo = transport.send(currentTimeMs, rawEventRequest, this::eventRequestSended, this::sendReceiveFailed);
            }
            else
                clock.setTimer(currentTimeMs, stateRequestInterval, this::toRequestFullPanelState);
    }

    public void deviceStateRequestSended(long currentTimeMs, int sequenceNo) {
        transport.receive(currentTimeMs, sequenceNo, this::deviceStateReceived, this::sendReceiveFailed);
    }

    public void deviceStateReceived(long currentTimeMs, int sequenceNo, R rawData) {
        DeviceStateAnswer answer = messageCodec.decodeDeviceStateAnswer(rawData);
        if (answer != null) {
                for (DeviceState deviceState: answer.getDevicesStates()) {
                    try {
                        FireDetector fireDetector = (FireDetector) deviceRepository.findOneByParentDeviceIdAndLineNoAndAddress(panel.getId(),
                                answer.getLineNo(),
                                deviceState.getAddress());
                        if (!deviceState.equals(fireDetector)) {
                            deviceState.applyTo(fireDetector);
                            deviceRepository.save(fireDetector);
                        }
                    }
                    catch (Exception e) {
                        logger.warn("Device absence or has wrong type (parent panel address: {}, line number: {}, address: {})",
                                panel.getAddress(),
                                answer.getLineNo(),
                                deviceState.getAddress());
                    }
                }
        }
        clock.setTimer(currentTimeMs, stateRequestInterval, this::toRequestFullPanelState);
    }
}
