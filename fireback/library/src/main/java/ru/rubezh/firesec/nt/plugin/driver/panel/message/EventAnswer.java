package ru.rubezh.firesec.nt.plugin.driver.panel.message;

import java.util.Date;

import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Device.DeviceType;
import ru.rubezh.firesec.nt.domain.v0.LogRecord.EventType;
import ru.rubezh.firesec.nt.domain.v0.LogRecord.SourceType;
import ru.rubezh.firesec.nt.domain.v0.event.context.EventContext;

public class EventAnswer extends EventRequest {

    Date dateTime;
    DeviceType deviceType;
    Integer lineNo;
    Integer address;
    EventType eventType;
    EventContext eventContext;
    public Date getDateTime() {
        return dateTime;
    }
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    public DeviceType getDeviceType() {
        return deviceType;
    }
    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }
    public Integer getLineNo() {
        return lineNo;
    }
    public void setLineNo(Integer lineNo) {
        this.lineNo = lineNo;
    }
    public Integer getAddress() {
        return address;
    }
    public void setAddress(Integer address) {
        this.address = address;
    }
    public EventType getEventType() {
        return eventType;
    }
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    public EventContext getEventContext() {
        return eventContext;
    }
    public void setEventContext(EventContext eventContext) {
        this.eventContext = eventContext;
    }

    public void applyTo(LogRecord logRecord) {
        logRecord.setDateTime(getDateTime());

        switch (getDeviceType()) {
        case fireDetector:
            logRecord.setSourceType(SourceType.device);
            break;
        default: //panel or panelSimulator
            logRecord.setSourceType(SourceType.panel);
            break;
        }

        logRecord.setEventType(getEventType());
        logRecord.setEventContext(getEventContext());
        logRecord.setRecordNo(getRecordIndex());
    }

}
