package ru.rubezh.firesec.nt.plugin.driver.panel.message;

public interface MessageCodec<R> {
    public R encodeFullPanelStateRequest(FullPanelStateRequest request);
    public R encodeDeviceStateRequest(DeviceStateRequest request);
    public R encodeEventRequest(EventRequest request);

    public FullPanelStateAnswer decodeFullPanelStateAnswer(R rawData);
    public DeviceStateAnswer decodeDeviceStateAnswer(R rawData);
    public EventAnswer decodeEventAnswer(R rawData);
}
