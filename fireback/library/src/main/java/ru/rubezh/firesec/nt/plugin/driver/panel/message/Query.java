package ru.rubezh.firesec.nt.plugin.driver.panel.message;

abstract public class Query {
    abstract public QueryType getQueryType();
}
