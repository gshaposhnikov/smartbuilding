package ru.rubezh.firesec.nt.plugin.driver.panel.message;

abstract public class ParameterQuery extends Query {

    abstract public ParameterType getParameterType();
}
