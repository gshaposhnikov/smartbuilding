package ru.rubezh.firesec.nt.plugin.driver.panel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.mongodb.BasicDBObject;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.PanelSimulator;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.plugin.driver.clock.SingleClientClock;
import ru.rubezh.firesec.nt.plugin.driver.panel.message.MessageCodec;
import ru.rubezh.firesec.nt.plugin.driver.transport.TcpBsonTransport;

public class PanelSimulatorHandlerFactory implements PanelHandlerFactory<BasicDBObject> {

    private Logger logger;

    private DeviceRepository deviceRepository;
    private LogRecordRepository logRecordRepository;

    private MessageCodec<BasicDBObject> codecFactory;
    
    public PanelSimulatorHandlerFactory(Logger logger, DeviceRepository deviceRepository,
            LogRecordRepository logRecordRepository, MessageCodec<BasicDBObject> codecFactory) {
        this.logger = logger;
        this.deviceRepository = deviceRepository;
        this.logRecordRepository = logRecordRepository;
        this.codecFactory = codecFactory;
    }

    @Override
    public PanelHandler<BasicDBObject> createPanelHandler(Plan plan, Device device) {
        try {
            if (device instanceof PanelSimulator) {
                PanelSimulator panelSimulator = (PanelSimulator) device;
                PanelHandler<BasicDBObject> panelHandler = new PanelHandler<>();
                panelHandler.clock = new SingleClientClock();
                panelHandler.transport = new TcpBsonTransport(logger, panelSimulator.getIpAddress(), panelSimulator.getPort());

                    /* get fire detectors */
                List<Device> devicesOnPanel = deviceRepository.findByPlanIdAndParentDeviceId(plan.getId(), panelSimulator.getId());
                List<FireDetector> fireDetectorsOnPanel = new ArrayList<>();
                for (Device deviceOnPanel: devicesOnPanel) {
                    try {
                        fireDetectorsOnPanel.add((FireDetector) deviceOnPanel);
                    }
                    catch (Exception e) {
                        logger.warn("Device is not a fire detector (device id: {}, parent panel id: {})", deviceOnPanel.getId(), panelSimulator.getId());
                    }
                }

                    /* create state machine instance */
                panelHandler.stateMachine = new StateMachine<>(panelHandler.transport,
                        panelHandler.clock,
                        codecFactory,
                        deviceRepository,
                        logRecordRepository,
                        logger,
                        panelSimulator,
                        fireDetectorsOnPanel);

                return panelHandler;
            }
        }
        catch (IOException e) {
            logger.error("IO Exception (cause: {}, message: {})", e.getCause(), e.getMessage());
        }

        return null;
    }
}
