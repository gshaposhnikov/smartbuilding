package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.MonitorableValueProfileView;

import java.util.Collection;
import java.util.List;

@Repository
public interface MonitorableValueProfileViewRepository extends MongoRepository<MonitorableValueProfileView, String> {

    List<MonitorableValueProfileView> findByIdIn(Collection<String> strings);

}
