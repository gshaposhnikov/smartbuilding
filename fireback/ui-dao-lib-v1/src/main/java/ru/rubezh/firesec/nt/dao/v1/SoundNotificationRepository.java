package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rubezh.firesec.nt.domain.v1.SoundNotification;

import java.util.Date;
import java.util.List;

public interface SoundNotificationRepository extends MongoRepository<SoundNotification, String> {

    boolean existsByStateCategoryId(String stateCategoryId);

    List<SoundNotification> findByRemovedIsFalseAndCreateDateTimeAfter(Date from);

    List<SoundNotification> findByRemovedIsFalseAndUpdateDateTimeAfter(Date from);

    List<SoundNotification> findByRemovedIsTrue();

    void deleteByIdIn(List<String> removedMediaIds);

    boolean existsByMediaId(String mediaId);
}
