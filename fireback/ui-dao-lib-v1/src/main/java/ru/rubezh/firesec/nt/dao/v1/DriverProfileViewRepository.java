package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;

@Repository
public interface DriverProfileViewRepository extends MongoRepository<DriverProfileView, String> {

}
