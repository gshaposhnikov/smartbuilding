package ru.rubezh.firesec.nt.dao.v1;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedEventView;

@Repository
public interface DslAggregatedEventViewRepository extends MongoRepository<AggregatedEventView, String>,
        QueryDslPredicateExecutor<AggregatedEventView> {

    @Override
    Page<AggregatedEventView> findAll(Predicate predicate, Pageable pageable);
}
