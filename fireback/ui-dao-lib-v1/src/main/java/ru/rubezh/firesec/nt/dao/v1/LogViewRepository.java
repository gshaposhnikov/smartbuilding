package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.LogView;

import java.util.Date;
import java.util.List;

@Repository
public interface LogViewRepository extends MongoRepository<LogView, String> {

    List<LogView> findByRemovedIsFalseAndCreateDateTimeAfter(Date createDateTime);

    List<LogView> findByRemovedIsFalseAndUpdateDateTimeAfter(Date updateDateTime);

    List<LogView> findByRemovedIsTrue();

    LogView findOneByRemovedIsFalseAndId(String id);

    LogView findOneByRemovedIsFalseAndPosition(int position);

    void deleteByIdIn(List<String> ids);

    List<LogView> findByRemovedIsFalse();

    List<LogView> findByRemovedIsFalseAndPositionGreaterThan(int position);
}
