package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;
import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.Media;
import ru.rubezh.firesec.nt.domain.v1.MediaType;

@Repository
public interface MediaRepository extends MongoRepository<Media, String> {

    List<Media> findByMediaType(MediaType mediaType);

    boolean existsByContent(String content);

    List<Media> findByRemovedIsFalseAndCreateDateTimeAfter(Date from);

    List<Media> findByRemovedIsFalseAndUpdateDateTimeAfter(Date from);

    List<Media> findByRemovedIsTrue();

    List<Media> findByIdIn(Collection<String> ids);

    void deleteByIdIn(List<String> removedMediaIds);

    boolean existsByIdAndMediaType(String id, MediaType mediaType);
}
