package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DeviceShapeLibrary;

@Repository
public interface DeviceShapeLibraryRepository extends MongoRepository<DeviceShapeLibrary, String> {

    DeviceShapeLibrary findOneByRemovedIsFalseAndId(String id);

    boolean existsByDeviceProfileIdAndName(String deviceProfileId, String name);

    boolean existsByIdNotAndDeviceProfileIdAndName(String id, String deviceProfileId, String name);

    List<DeviceShapeLibrary> findByRemovedIsFalseAndCreatedAfter(Date created);

    List<DeviceShapeLibrary> findByRemovedIsFalseAndUpdatedAfter(Date updtated);

    List<DeviceShapeLibrary> findByRemovedIsTrue();

    void deleteByIdIn(List<String> ids);

    boolean existsByRemovedIsFalseAndIdAndDeviceProfileId(String shapeLibraryId, String deviceProfileId);
}
