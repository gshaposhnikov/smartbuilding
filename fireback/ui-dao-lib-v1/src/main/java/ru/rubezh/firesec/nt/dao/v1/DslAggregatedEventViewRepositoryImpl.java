package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.QueryDslMongoRepository;
import org.springframework.data.mongodb.repository.support.SpringDataMongodbQuery;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.QSort;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.repository.core.EntityInformation;
import org.springframework.util.Assert;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.mongodb.AbstractMongodbQuery;

import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedEventView;

public class DslAggregatedEventViewRepositoryImpl extends QueryDslMongoRepository<AggregatedEventView, String>
        implements DslAggregatedEventViewRepository {

    private final PathBuilder<AggregatedEventView> builder;
    private EntityInformation<AggregatedEventView, String> entityInformation;
    private MongoOperations mongoOperations;

    @Autowired
    public DslAggregatedEventViewRepositoryImpl(MongoRepositoryFactory factory, MongoOperations mongoOperations){
        this(factory.getEntityInformation(AggregatedEventView.class), mongoOperations, SimpleEntityPathResolver.INSTANCE);
    }

    public DslAggregatedEventViewRepositoryImpl(MongoEntityInformation<AggregatedEventView, String> entityInformation,
                                                MongoOperations mongoOperations, EntityPathResolver resolver) {
        super(entityInformation, mongoOperations, resolver);
        Assert.notNull(resolver, "EntityPathResolver must not be null!");

        EntityPath<AggregatedEventView> path = resolver.createPath(entityInformation.getJavaType());

        this.builder = new PathBuilder<>(path.getType(), path.getMetadata());
        this.entityInformation = entityInformation;
        this.mongoOperations = mongoOperations;
    }

    /**
     * Найти все события с фильтрацией по предикату и с учетом пагенации.
     * 
     * В объект пагенации не возвращается корректное общее кол-во событий, страниц, признака последней страницы, т.к.
     * для быстродействия из него убран запрос count.
     * 
     * @todo Вернуть стандартную реализацию и сделать отдельный метод по возврату первых n событий с учетом сортировки и
     *       предиката фильтрации (для журналов событий). В ВЕБ-клиенте убрать отдельный вызов REST-метода /events/count
     *       - брать общее кол-во из параметров пагенации, и также сделать получение событий для журнала новым методом.
     */
    @Override
    public Page<AggregatedEventView> findAll(Predicate predicate, Pageable pageable) {
        AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> query =
                createQueryFor(predicate);

        List<AggregatedEventView> result = applyPagination(query, pageable).fetch();

        return new PageImpl<>(result, pageable, result.size());
    }

    /* Все функции ниже взяты из QueryDslMongoRepository */

    private AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> createQueryFor(Predicate predicate) {
        return createQuery().where(predicate);
    }

    private AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> createQuery() {
        return new SpringDataMongodbQuery<>(mongoOperations, entityInformation.getJavaType());
    }

    private AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> applyPagination(
            AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> query, Pageable pageable) {

        if (pageable == null) {
            return query;
        }

        query = query.offset(pageable.getOffset()).limit(pageable.getPageSize());
        return applySorting(query, pageable.getSort());
    }

    private AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> applySorting(
            AbstractMongodbQuery<AggregatedEventView, SpringDataMongodbQuery<AggregatedEventView>> query, Sort sort) {

        if (sort == null) {
            return query;
        }

        // TODO: find better solution than instanceof check
        if (sort instanceof QSort) {

            List<OrderSpecifier<?>> orderSpecifiers = ((QSort) sort).getOrderSpecifiers();
            query.orderBy(orderSpecifiers.toArray(new OrderSpecifier<?>[orderSpecifiers.size()]));

            return query;
        }

        for (Sort.Order order : sort) {
            query.orderBy(toOrder(order));
        }

        return query;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private OrderSpecifier<?> toOrder(Sort.Order order) {

        Expression<Object> property = builder.get(order.getProperty());

        return new OrderSpecifier(
                order.isAscending() ? com.querydsl.core.types.Order.ASC : com.querydsl.core.types.Order.DESC, property);
    }
}
