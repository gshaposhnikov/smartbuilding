package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;

import java.util.List;

@Repository
public interface StateCategoryViewRepository extends MongoRepository<StateCategoryView, String> {

    List<StateCategoryView> findByFireStateCategoryIsTrue();

    StateCategoryView findByWarningStateCategoryIsTrue();

    StateCategoryView findByAlarmStateCategoryIsTrue();

    StateCategoryView findByAutoOffStateCategoryIsTrue();
}
