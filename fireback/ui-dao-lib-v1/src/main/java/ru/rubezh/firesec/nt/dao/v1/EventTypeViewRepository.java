package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.EventTypeView;

import java.util.List;

@Repository
public interface EventTypeViewRepository extends MongoRepository<EventTypeView, String> {

    public EventTypeView findById(String id);

    public List<EventTypeView> findAllByIdIn(List<String> ids);
}
