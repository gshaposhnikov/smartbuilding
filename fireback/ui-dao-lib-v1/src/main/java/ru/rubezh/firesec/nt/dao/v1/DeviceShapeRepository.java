package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DeviceShape;

@Repository
public interface DeviceShapeRepository extends MongoRepository<DeviceShape, String> {

    DeviceShape findOneByRemovedIsFalseAndDeviceShapeLibraryIdAndStateCategoryId(String deviceShapeLibraryId,
            String stateCategoryId);

    List<DeviceShape> findByDeviceShapeLibraryIdIn(List<String> deviceShapeLibraryIds);

    List<DeviceShape> findByRemovedIsFalseAndDeviceShapeLibraryIdInAndStateCategoryIdInOrderByDeviceShapeLibraryId(
            List<String> deviceShapeLibraryIds, List<String> stateCategoryIds);

    DeviceShape findOneByRemovedIsFalseAndId(String id);

    boolean existsByDeviceShapeLibraryIdAndStateCategoryId(String deviceShapeLibraryId, String stateCategoryId);

    List<DeviceShape> findByRemovedIsFalseAndCreatedAfter(Date created);

    List<DeviceShape> findByRemovedIsFalseAndUpdatedAfter(Date updated);

    List<DeviceShape> findByRemovedIsTrue();

    void deleteByIdIn(List<String> ids);

}
