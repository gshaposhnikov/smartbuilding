package ru.rubezh.firesec.nt.dao.v1;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.StateView;

@Repository
public interface StateViewRepository extends MongoRepository<StateView, String> {
    List<StateView> findByIdIn(Collection<String> ids);
}
