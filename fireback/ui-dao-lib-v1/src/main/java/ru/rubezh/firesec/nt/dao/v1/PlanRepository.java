package ru.rubezh.firesec.nt.dao.v1;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.Plan;

@Repository
public interface PlanRepository extends MongoRepository<Plan, String> {

    public Plan findByProjectIdAndId(String projectId, String id);

    public boolean existsByProjectIdAndId(String projectId, String id);

    public long deleteByProjectIdAndId(String projectId, String id);

    public long deleteByProjectId(String projectId);

    public List<Plan> findByProjectId(String projectId);

    public List<Plan> findByProjectIdAndPlanGroupId(String projectId, String planGroupId);

    public List<Plan> findByIdIn(Collection<String> planIds);
}
