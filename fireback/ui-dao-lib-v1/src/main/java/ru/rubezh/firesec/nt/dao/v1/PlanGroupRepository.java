package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.PlanGroup;

@Repository
public interface PlanGroupRepository extends MongoRepository<PlanGroup, String> {

    PlanGroup findByProjectIdAndId(String projectId, String id);

    List<PlanGroup> findByProjectId(String projectId);

    long deleteByProjectIdAndId(String projectId, String id);

    long deleteByProjectId(String projectId);

    List<PlanGroup> findByProjectIdAndIdIn(String projectId, Set<String> keySet);
}
