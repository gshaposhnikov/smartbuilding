package ru.rubezh.firesec.nt.dao.v1;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DeviceProfileView;
import ru.rubezh.firesec.nt.domain.v1.Language;

@Repository
public interface DeviceProfileViewRepository extends MongoRepository<DeviceProfileView, String> {

    List<DeviceProfileView> findByLanguage(Language language);

    DeviceProfileView findByLanguageAndId(Language language, String id);

    List<DeviceProfileView> findByLanguageAndIdIn(Language language, Collection<String> ids);

    List<DeviceProfileView> findByLanguageAndIdInOrderByIdAsc(Language language, Collection<String> ids);

}
