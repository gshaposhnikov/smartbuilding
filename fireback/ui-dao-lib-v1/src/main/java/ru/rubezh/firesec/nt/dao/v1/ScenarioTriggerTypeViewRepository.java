package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerTypeView;

import java.util.Collection;
import java.util.List;

@Repository
public interface ScenarioTriggerTypeViewRepository extends MongoRepository<ScenarioTriggerTypeView, String> {

    List<ScenarioTriggerTypeView> findAllByOrderByIdAsc();

    List<ScenarioTriggerTypeView> findByIdIn(Collection<String> ids);

}
