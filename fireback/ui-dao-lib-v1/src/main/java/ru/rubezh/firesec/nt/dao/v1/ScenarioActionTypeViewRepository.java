package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ScenarioActionTypeView;

import java.util.Collection;
import java.util.List;

@Repository
public interface ScenarioActionTypeViewRepository extends MongoRepository<ScenarioActionTypeView, String> {

    List<ScenarioActionTypeView> findAllByOrderByIdAsc();

    List<ScenarioActionTypeView> findByIdIn(Collection<String> ids);

}
