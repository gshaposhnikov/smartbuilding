package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.RegionView;

@Repository
public interface RegionViewRepository extends MongoRepository<RegionView, String> {

    public List<RegionView> findByProjectId(String projectId);

    public RegionView findByProjectIdAndId(String projectId, String id);

    public List<RegionView> findByProjectIdAndIdIn(String projectId, Iterable<String> regionIds);

    public long deleteByProjectIdAndId(String projectId, String id);

    public void deleteByProjectIdAndIdIn(String projectId, List<String> ids);

    public long deleteByProjectId(String projectId);

    public List<RegionView> findByProjectIdAndPlanLayouts_planId(String projectId, String planId);
}
