package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.DeviceView;

import java.util.List;

@Repository
public interface DeviceViewRepository extends MongoRepository<DeviceView, String> {

    public DeviceView findByDeviceId(String deviceId);

    public List<DeviceView> findByProjectId(String projectId);

    public List<DeviceView> findByProjectIdAndParentDeviceId(String projectId, String parentDeviceId);

    public List<DeviceView> findByProjectIdAndIdIn(String projectId, Iterable<String> deviceIds);

    public DeviceView findByProjectIdAndId(String projectId, String id);

    public List<DeviceView> findByProjectIdAndPlanLayouts_planId(String projectId, String planId);

    public void deleteByProjectId(String projectId);

    public int deleteByProjectIdAndIdIn(String projectId, List<String> deviceIds);

    public List<DeviceView> findAllByDeviceIdIn(List<String> deviceIds);

    List<DeviceView> findAllByDeviceShapeLibraryId(String deviceShapeLibraryId);
}
