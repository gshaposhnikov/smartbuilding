package ru.rubezh.firesec.nt.cs;

import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.amqp.message.request.PingRequest;
import ru.rubezh.firesec.nt.amqp.message.response.PingResponse;

import java.util.Comparator;

/**
 * Класс для работы с уже зарегистрированным в системе драйвером, для которого
 * известен профиль драйвера и который запускается и останавливается по
 * необходимости работы с устройствами определенного типа.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class DriverWorkerRegistered extends DriverWorker {

    /** Максимальное кол-во неотвеченных PING-запросов от драйвера, по достижении которого драйвер считается упавшим или
     * зависшим */
    static final private int MAX_DRIVER_UNANSWERED_PING_REQUESTS = 5;
    /** Максимальное время простоя драйвера, мс */
    private static final long MAX_DRIVER_IDLE_TIME = 1l * 60 * 1000;
    /** Максимальное время захвата эксклюзтвного режима одним КА, мс */
    private static final long MAX_DRIVER_EXCLUSIVE_MODE_PER_SM_TIME = 15l * 60 * 1000;
    /** Максимальный объём памяти драйвера, байт */
    private static final long MAX_DRIVER_MEMORY_USAGE = 800l << 20;

    /** Кол-во ожидаемых ответов на ping-запрос */
    private int nAwaitingPingResponses;

    /** Последний ответ на ping-запрос */
    private PingResponse lastPingResponse;

    /** Полученный, но не обработанный ответ на ping-запрос */
    private PingResponse receivedPingResponse;

    public DriverWorkerRegistered(Logger logger) {
        super(logger);
    }

    public synchronized boolean checkDriverAlive() {
        if (nAwaitingPingResponses >= MAX_DRIVER_UNANSWERED_PING_REQUESTS) {
            logger.error("Driver {}: waiting ping response timeout", getDriverId());
            return false;
        }

        if (receivedPingResponse != null) {
            if (lastPingResponse != null
                    && lastPingResponse.getCurrentTimeMs() == receivedPingResponse.getCurrentTimeMs()) {
                logger.error("Driver {}: current time didn't change!", getDriverId());
                return false;
            }
            lastPingResponse = receivedPingResponse;
            receivedPingResponse = null;
            
            if (!lastPingResponse.isExclusiveActivityMode()) {
                /*
                 * В обычном режиме проверяем самую давнюю метку времени
                 * активности одного из КА
                 */
                long lastActiveTime = lastPingResponse.getLastSMActivityTimes().stream()
                        .min(Comparator.comparing(v -> v))
                        .orElse(lastPingResponse.getCurrentTimeMs());
                if (lastActiveTime + MAX_DRIVER_IDLE_TIME <= lastPingResponse.getCurrentTimeMs()) {
                    logger.error("Driver {}: one of state machines is inactive too long",
                            getDriverId());
                    return false;
                }
            } else {
                /*
                 * В монопольном режиме проверяем время активации режима
                 */
                if (lastPingResponse.getLastExclusiveModeAcquireTimeMs()
                        + MAX_DRIVER_EXCLUSIVE_MODE_PER_SM_TIME < lastPingResponse
                                .getCurrentTimeMs()) {
                    logger.error("Driver {}: exclusive mode is too long", getDriverId());
                    return false;
                }
            }

            if (lastPingResponse.getMemoryUsage() >= MAX_DRIVER_MEMORY_USAGE) {
                logger.error("Driver {}: memory limit exceeded", getDriverId());
                return false;
            }
        }
        getLogger().trace("Sending PING to {}", getDriverId());
        PingRequest pingRequest = new PingRequest();
        pingRequest.setDriverId(getDriverId());
        sendRequest(pingRequest);
        ++nAwaitingPingResponses;
        return true;
    }

    public synchronized void onPingResponseReceived(PingResponse pingResponse) {
        receivedPingResponse = pingResponse;
        /*
         * Сбрасываем счетчик сразу в ноль, потому что достаточно одного ответа, чтобы определить, что драйвер -
         * живой
         */
        nAwaitingPingResponses = 0;
    }

    @Override
    public boolean startDriver(String directory) {
        nAwaitingPingResponses = 0;
        lastPingResponse = null;
        receivedPingResponse = null;
        return super.startDriver(directory);
    }

}
