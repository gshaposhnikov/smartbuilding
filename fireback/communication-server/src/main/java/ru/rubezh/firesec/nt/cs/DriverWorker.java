package ru.rubezh.firesec.nt.cs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;

import ru.rubezh.firesec.nt.amqp.listener.ResponseListener;
import ru.rubezh.firesec.nt.amqp.message.request.AbstractDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.ShutdownRequest;
import ru.rubezh.firesec.nt.amqp.sender.DriverRequestSender;

/**
 * Класс, инкапсулирующий в себе всю общую работу с запущенными экземплярами
 * драйвера. Отвечает за отправку сообщений, обработку ответов и управление
 * очередями драйвера.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public abstract class DriverWorker {
    /** Максимальное время ожидания остановки процесса драйвера */
    static final private long WAIT_FOR_DRIVER_PROCESS_TIMEOUT_MS = 1000;

    protected Logger logger;
    private RabbitAdmin rabbitAdmin;
    private RabbitTemplate rabbitTemplate;
    private DriverManager driverManager;

    /** Два поля, которые в принципе после будут не нужны */
    private String commandLine;
    private String driverProfileId;
    /** Идентификатор экземпляра драйвера. */
    private String driverId;
    /** Имя очереди для запросов к драйверу. */
    private String requestQueueName;
    /** Имя очереди для ответов от драйвера. */
    private String responseQueueName;
    /** Имя exchange'а для отправки оповещений от драйвера. */
    private String alertsExchangeName;

    /** Объект для получения ответов от драйвера. */
    private SimpleMessageListenerContainer listenerContainer;
    /** Очередь для запросов к драйверу. */
    private Queue requestQueue;
    /** Очереди для ответов от драйвера. */
    private Queue responseQueue;

    /** Класс для отправки запросов драйверам */
    private DriverRequestSender requestSender;
    /** Обработчик ответов от драйвера. */
    private ResponseListener messageListener;
    /** Запущенный процесс драйвера */
    private Process driverProcess;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getDriverProfileId() {
        return driverProfileId;
    }

    public void setDriverProfileId(String driverProfileId) {
        this.driverProfileId = driverProfileId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getRequestQueueName() {
        return requestQueueName;
    }

    public void setRequestQueueName(String requestQueueName) {
        this.requestQueueName = requestQueueName;
    }

    public String getResponseQueueName() {
        return responseQueueName;
    }

    public void setResponseQueueName(String responseQueueName) {
        this.responseQueueName = responseQueueName;
    }

    public String getAlertsExchangeName() {
        return alertsExchangeName;
    }

    public void setAlertsExchangeName(String alertsExchangeName) {
        this.alertsExchangeName = alertsExchangeName;
    }

    public RabbitAdmin getRabbitAdmin() {
        return rabbitAdmin;
    }

    public void setRabbitAdmin(RabbitAdmin rabbitAdmin) {
        this.rabbitAdmin = rabbitAdmin;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public ResponseListener getMessageListener() {
        return messageListener;
    }

    public void setMessageListener(ResponseListener messageListener) {
        this.messageListener = messageListener;
    }

    public DriverRequestSender getRequestSender() {
        return requestSender;
    }

    public void setRequestSender(DriverRequestSender requestSender) {
        this.requestSender = requestSender;
    }

    public DriverManager getDriverManager() {
        return driverManager;
    }

    public void setDriverManager(DriverManager driverManager) {
        this.driverManager = driverManager;
    }

    public Process getDriverProcess() {
        return driverProcess;
    }

    public void setDriverProcess(Process driverProcess) {
        this.driverProcess = driverProcess;
    }

    public DriverWorker(Logger logger) {
        this.logger = logger;
    }

    /**
     * Метод должен вызываться после установки всех необходимых полей класса для
     * создания и инициализации RMI.
     */
    public void initialize() {
        requestQueue = new Queue(requestQueueName, true, false, false);
        responseQueue = new Queue(responseQueueName, true, false, false);
        rabbitAdmin.declareQueue(requestQueue);
        rabbitAdmin.declareQueue(responseQueue);

        listenerContainer = new SimpleMessageListenerContainer(rabbitTemplate.getConnectionFactory());
        listenerContainer.setAutoStartup(true);
        listenerContainer.setQueues(responseQueue);
        listenerContainer.setMessageListener(
            new MessageListenerAdapter(messageListener, rabbitTemplate.getMessageConverter()));
        logger.info("DriverWorker {} initialized!", driverId);
    }

    /**
     * Общий метод для отправки произвольного запроса. Используется как обертка
     * над
     * {@link RabbitTemplate#convertAndSend(String, Object, MessagePostProcessor)},
     * так как запросы всегда нужно слать в одну и ту же очередь и использовать
     * тот же {@link MessagePostProcessor}.
     *
     * @param request
     *            Объект запроса для отправки.
     */
    protected void sendRequest(AbstractDriverRequest request) {
        requestSender.send(request);
    }

    public boolean startDriver(String directory) {
        List<String> args = new ArrayList<>(Arrays.asList(commandLine.split(" ")));
        args.add("--id");
        args.add(driverId);
        args.add("--request-queue");
        args.add(requestQueueName);
        args.add("--alerts-exchange");
        args.add(alertsExchangeName);
        ProcessBuilder pb = new ProcessBuilder(args);
        pb.inheritIO();
        pb.redirectErrorStream(true);
        pb.directory(new File(directory));
        logger.info("Starting {}", pb);
        try {
            driverProcess = pb.start();
            listenerContainer.start();
            return true;
        } catch (IOException e) {
            logger.error("Error starting driver: {}", e);
            return false;
        }
    }

    public void stopDriver() {
        assert (driverProcess != null);

        try {
            driverProcess.destroyForcibly().waitFor(WAIT_FOR_DRIVER_PROCESS_TIMEOUT_MS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            getLogger().warn("Waiting of stop driver process was interruped");
        }
        listenerContainer.stop();
    }

    /**
     * Отправить драйверу сообщение от остановке.
     */
    public void requestDriverToStop() {
        assert (driverProcess != null);
        assert (listenerContainer != null);

        logger.info("Sending SHUTDOWN to {}", driverId);
        ShutdownRequest shutdownRequest = new ShutdownRequest();
        shutdownRequest.setDriverId(driverId);
        sendRequest(shutdownRequest);

        listenerContainer.stop();
    }

}
