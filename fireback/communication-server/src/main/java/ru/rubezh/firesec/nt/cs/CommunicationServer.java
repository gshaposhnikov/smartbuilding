package ru.rubezh.firesec.nt.cs;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class CommunicationServer {
    private ClassPathXmlApplicationContext applicationContext;
    private CommunicationServerArgs applicationArgs;

    private String[] args;

    public CommunicationServer(String[] args) {
        this.args = args;
    }

    public void run() {
        try {
            applicationArgs = new CommunicationServerArgs(args);
        } catch (ParseException parseException) {
            System.out.println(parseException.getMessage());
            CommunicationServerArgs.printHelp();
            return;
        }

        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(CommunicationServerArgs.class)
                .addConstructorArgValue(applicationArgs).getBeanDefinition();
        beanFactory.registerBeanDefinition("applicationArgs", beanDefinition);
        GenericApplicationContext genericApplicationContext = new GenericApplicationContext(beanFactory);
        genericApplicationContext.refresh();

        applicationContext = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" },
                genericApplicationContext);
        applicationContext.registerShutdownHook();
    }

    public static void main(String[] args) {
        new CommunicationServer(args).run();
    }

}
