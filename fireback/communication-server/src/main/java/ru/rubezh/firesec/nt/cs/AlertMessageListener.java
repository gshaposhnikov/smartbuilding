package ru.rubezh.firesec.nt.cs;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.listener.DriverAlertListener;
import ru.rubezh.firesec.nt.amqp.message.alert.DriverRegisteredAlert;
import ru.rubezh.firesec.nt.cs.DriverManager;

/**
 * Обработчик оповещений от драйвера
 * @author Александр Горячкин
 */
public class AlertMessageListener extends DriverAlertListener {

    DriverManager driverManager;

    public AlertMessageListener(Logger logger, DriverManager driverManager) {
        super(logger);
        this.driverManager = driverManager;
    }

    void handleMessage(DriverRegisteredAlert message) {
        if (logger != null) {
            logger.debug("Driver register."
                    + " Id: {}, requests queue: {}, alerts exchange: {}",
                    message.getDriverId(),
                    message.getRequestQueue(),
                    message.getAlertsExchange());
        }

        driverManager.startDriverRegisterProcess(message.getDriverId());
    }

}
