package ru.rubezh.firesec.nt.cs;

import java.util.List;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.request.GetDriverProfileViewsRequest;
import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;
import ru.rubezh.firesec.nt.service.v1.DriverProfileViewService;

/**
 * Класс для работы с ещё не зарегистрированным в системе драйвером.
 * @author Александр Горячкин
 */
public class DriverWorkerUnregistered extends DriverWorker {

    private DriverProfileViewService driverProfileViewService;

    public DriverWorkerUnregistered(Logger logger) {
        super(logger);
    }

    public DriverProfileViewService getDriverProfileViewService() {
        return driverProfileViewService;
    }

    public void setDriverProfileViewService(DriverProfileViewService driverProfileViewService) {
        this.driverProfileViewService = driverProfileViewService;
    }

    /**
     * Отправить драйверу запрос профиля драйвера.
     */
    public void sendDriverProfileViewsRequest() {
        GetDriverProfileViewsRequest request = new GetDriverProfileViewsRequest();
        request.setDriverId(getDriverId());
        sendRequest(request);
    }

    /**
     * Запомнить профиль драйвера и остановить драйвер.
     * Вызывается при получении ответа на запрос профиля драйвера.
     *
     * @param views список представлений профиля драйвера
     */
    public void onDriverProfileViewsReceived(List<DriverProfileView> views) {
        requestDriverToStop();
        getDriverManager().removeUnregisteredDriverWorker(getDriverId());
        driverProfileViewService.add(views);
        if (views.size() > 0) {
            getDriverManager().instantiateRegisteredDriver(getCommandLine(), views.get(0).getDriverProfile().getId());
        }
        getLogger().info("Driver \"{}\" registered", views.get(0).getDriverProfile().getId());
    }

}
