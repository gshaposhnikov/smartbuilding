package ru.rubezh.firesec.nt.cs;

import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.QueueInfo;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionListener;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.web.client.HttpClientErrorException;
import ru.rubezh.firesec.nt.amqp.listener.ResponseListener;
import ru.rubezh.firesec.nt.amqp.sender.DriverMessagePostProcessor;
import ru.rubezh.firesec.nt.amqp.sender.DriverRequestSender;
import ru.rubezh.firesec.nt.dao.v1.DriverInstanceRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverInstance;
import ru.rubezh.firesec.nt.service.v1.DriverProfileViewService;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для управления всеми экземплярами драйвера. Имеет фабричные методы для
 * запуска драйвера, а также команды для проверки их состояния. Для работы с ним
 * требуется установить следующие поля:
 * <ul>
 * <li>{@link #logger}</li>
 * <li>{@link #driverNamingManager}</li>
 * <li>{@link #rabbitAdmin}</li>
 * <li>{@link #rabbitTemplate}</li>
 * <li>{@link #rabbitHttpClient}</li>
 * </ul>
 * После установки этих полей необходимо вызвать метод {@link #initialize()}. В
 * нем происходит удаление старых очередей с помощью {@link #rabbitHttpClient}.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */

public class DriverManager implements ConnectionListener, Trigger {

    /**
     * Папка с местоположением драйверов. По умолчанию - текущая папка.
     */
    private String driversDir = ".";

    /** Флаг инициализации бина */
    private Boolean initialized = false;

    /** Флаг подключения с RabbitMQ */
    private boolean rabbitConnectionCreated;

    private Logger logger;

    /**
     * Менеджер, использующийся для генерации идентификаторов экземпляров
     * драйверов и генерирования имен очередей
     */
    private DriverNamingManager driverNamingManager;
    /** Используется для создания очередей */
    private RabbitAdmin rabbitAdmin;
    /** Вспомогательный класс для отправки сообщений */
    private RabbitTemplate rabbitTemplate;
    /**
     * Клиент для RESP API RabbitMQ. Используется для получения списка очередей
     * RabbitMQ для их удаления
     */
    private Client rabbitHttpClient;
    private DriverProfileViewService driverProfileViewService;

    /**
     * Список текущих запущенных экземпляров драйвера с соответствующими им
     * worker'ами. Ключ - идентификатор драйвера (driverId)
     */
    private Map<String, DriverWorkerRegistered> workers = new HashMap<>();

    /**
     * Список текущих запущенных не зарегистрированных драйверов с соответствующими им
     * worker'ами. Ключ - идентификатор драйвера (driverId)
     */
    private Map<String, DriverWorkerUnregistered> unregisteredDriverWorkers = new HashMap<>();

    /**
     * Сервис для сохранения информации о запущенных экземплярах драйвера
     */
    @Autowired
    DriverInstanceRepository driverInstanceRepository;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public DriverNamingManager getDriverNamingManager() {
        return driverNamingManager;
    }

    public void setDriverNamingManager(DriverNamingManager driverNamingManager) {
        this.driverNamingManager = driverNamingManager;
    }

    public RabbitAdmin getRabbitAdmin() {
        return rabbitAdmin;
    }

    public void setRabbitAdmin(RabbitAdmin rabbitAdmin) {
        this.rabbitAdmin = rabbitAdmin;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public Client getRabbitHttpClient() {
        return rabbitHttpClient;
    }

    public void setRabbitHttpClient(Client rabbitHttpClient) {
        this.rabbitHttpClient = rabbitHttpClient;
    }

    public DriverProfileViewService getDriverProfileViewService() {
        return driverProfileViewService;
    }

    public void setDriverProfileViewService(DriverProfileViewService driverProfileViewService) {
        this.driverProfileViewService = driverProfileViewService;
    }

    public String getDriversDir() {
        return driversDir;
    }

    public void setDriversDir(String driversDir) {
        this.driversDir = driversDir;
    }

    public DriverManager(Logger logger) {
        this.logger = logger;
    }

    /**
     * Метод, который должен вызываться после установки всех обязательных для
     * работы менеджера полей
     */
    public void initialize() {
        removeUnusedQueues();

        /*
         * TODO: убрать удаление зарегистрированных драйверов, вместо этого
         * добавить версию в профиль драйвера и сверять ее при запуске: если
         * изменилась - запрашивать и сохранять новый. Версия профиля драйвера
         * должна отправляться драйвером при старте оповещением.
         */
        driverInstanceRepository.deleteAll();

        List<DriverInstance> driverInstances = driverInstanceRepository.findAll();
        if (!driverInstances.isEmpty()) {
            restoreRegisteredDriverInstances(driverInstances);
        } else {
            instantiateUnregisteredDriver(DriverInstance.DEFAULT_DRIVER_COMMAND_LINE);
        }
    }

    private void prepareDriverWorker(DriverWorker driverWorker, String cmd, String driverId, String requestQueueName,
            String responseQueueName, String alertsExchangeName, ResponseListener messageListener,
            DriverRequestSender requestSender) {
        driverWorker.setLogger(logger);
        driverWorker.setCommandLine(cmd);
        driverWorker.setDriverId(driverId);
        driverWorker.setRequestQueueName(requestQueueName);
        driverWorker.setResponseQueueName(responseQueueName);
        driverWorker.setAlertsExchangeName(alertsExchangeName);
        driverWorker.setRabbitAdmin(rabbitAdmin);
        driverWorker.setRabbitTemplate(rabbitTemplate);
        driverWorker.setMessageListener(messageListener);
        driverWorker.setRequestSender(requestSender);
        driverWorker.setDriverManager(this);
    }

    /**
     * Запуск экземпляра драйвера, который ещё не зарегистрирован в системе,
     * для его идентификации.
     *
     * @param cmd
     *            Команда, которая будет выполнена для запуска драйвера.
     */
    private void instantiateUnregisteredDriver(String cmd) {
        logger.debug("Run unregistered driver. Command line: \"{}\"", cmd);
        String driverId = driverNamingManager.generateDriverId("unknown");
        String requestQueueName = driverNamingManager.generateDriverRequestQueueName(driverId);
        String responseQueueName = driverNamingManager.generateDriverResponseQueueName(driverId);
        String alertsExchangeName = driverNamingManager.getAlertsExchangeName();
        DriverWorkerUnregistered driverWorker = new DriverWorkerUnregistered(logger);
        ResponseListener messageListener = new RMIMessageUnregisteredListener(logger, driverWorker);
        DriverMessagePostProcessor messagePostProcessor = new DriverMessagePostProcessor(responseQueueName);
        DriverRequestSender requestSender = new DriverRequestSender(logger, rabbitTemplate, requestQueueName, messagePostProcessor);

        /* TODO: сделать один метод в самом worker-е вместо 3-х */
        prepareDriverWorker(driverWorker, cmd, driverId, requestQueueName, responseQueueName, alertsExchangeName,
                messageListener, requestSender);
        driverWorker.setDriverProfileViewService(driverProfileViewService);
        driverWorker.initialize();

        if (driverWorker.startDriver(driversDir)) {
            synchronized (unregisteredDriverWorkers) {
                unregisteredDriverWorkers.put(driverId, driverWorker);
                /* TODO: Стоит в DriverManager всю работу с worker'ами делать через полиморфизм - например,
                 * пинговать все worker'ы, но конкретный таймаут спрашивать у них же, то же самое с обработкой
                 * ответов - у зарегистрированных и незарегистрированных драйверов просто свои классы обработчиков.
                 * Т.е. единственное место, где в DriverManager будет отдельная работа с registered и undegistered -
                 * - это фабричные методы для запуска драйвера. Соответственно, и список worker'ов будет общий.
                 */
            }
        }
        /* TODO: деинициализация воркера при неуспехе запуска драйвера */
    }

    /**
     * Запуск процесса регистрации нового драйвера.
     * Вызывается после получения запроса драйвера о регистрации.
     *
     * @param driverId
     *            Текущий идентификатор драйвера.
     */
    public void startDriverRegisterProcess(String driverId) {
        logger.debug("Run registration process driver \"{}\"", driverId);
        DriverWorkerUnregistered driverWorker;
        synchronized (unregisteredDriverWorkers) {
            driverWorker = unregisteredDriverWorkers.get(driverId);
        }
        if (driverWorker != null)
            driverWorker.sendDriverProfileViewsRequest();
        /*
         * TODO: сделать запрос профилей драйверов (и устройств) отдельным запросом, ибо бизнес-логика должна быть
         * работоспособной, даже если отображения не настроены.
         */
    }

    /**
     * Удалить отработавший драйвер из списка.
     * Вызывается после регистрации и остановки драйвера.
     *
     * @param driverId
     *            Идентификатор драйвера.
     */
    public void removeUnregisteredDriverWorker(String driverId) {
        synchronized (unregisteredDriverWorkers) {
            unregisteredDriverWorkers.remove(driverId);
        }
    }

    /**
     * Запуск экземпляра драйвера, который уже зарегистрирован в системе.
     *
     * @param cmd
     *            Команда, которая будет выполнена для запуска драйвера.
     * @param driverProfileId
     *            Идентификатор профиля драйвера.
     * @return Объект, который отвечает за работу с запущенным экземпляром
     *         драйвера.
     */
    public DriverWorkerRegistered instantiateRegisteredDriver(String cmd, String driverProfileId) {
        logger.info("Run registered driver (profileId=\"{}\"): {}", driverProfileId, cmd);
        DriverWorkerRegistered driverWorker = new DriverWorkerRegistered(logger);
        String driverId = driverNamingManager.generateDriverId(driverProfileId);
        String requestQueueName = driverNamingManager.generateDriverRequestQueueName(driverId);
        String responseQueueName = driverNamingManager.generateDriverResponseQueueName(driverId);
        String alertsExchangeName = driverNamingManager.getAlertsExchangeName();
        ResponseListener messageListener = new RMIMessageRegisteredListener(logger, driverWorker);
        DriverMessagePostProcessor messagePostProcessor = new DriverMessagePostProcessor(responseQueueName);
        DriverRequestSender requestSender = new DriverRequestSender(logger, rabbitTemplate, requestQueueName,
                messagePostProcessor);

        /* TODO: сделать один метод в самом воркере вместо 2-х */
        prepareDriverWorker(driverWorker, cmd, driverId, requestQueueName, responseQueueName, alertsExchangeName,
                messageListener, requestSender);
        driverWorker.initialize();

        if (driverWorker.startDriver(driversDir)) {
            synchronized (workers) {
                workers.put(driverId, driverWorker);
            }

            DriverInstance driverInstance = new DriverInstance(driverId, driverProfileId, requestQueueName, cmd);
            driverInstanceRepository.save(driverInstance);

            return driverWorker;
        } else {
            /* TODO: деинициализация воркера */
            return null;
        }
    }

    /**
     * Восстанавливает список запущенных зарегистрированных драйверов из БД
     */
    private void restoreRegisteredDriverInstances(List<DriverInstance> driverInstances) {
        for (DriverInstance driverInstance : driverInstances) {
            DriverWorkerRegistered driverWorker = new DriverWorkerRegistered(logger);
            driverNamingManager.addDriverId(driverInstance.getId());
            String responseQueueName = driverNamingManager.generateDriverResponseQueueName(driverInstance.getId());
            String alertsExchangeName = driverNamingManager.getAlertsExchangeName();
            ResponseListener messageListener = new RMIMessageRegisteredListener(logger, driverWorker);
            DriverMessagePostProcessor messagePostProcessor = new DriverMessagePostProcessor(responseQueueName);
            DriverRequestSender requestSender = new DriverRequestSender(logger, rabbitTemplate,
                    driverInstance.getRequestQueueName(), messagePostProcessor);

            prepareDriverWorker(driverWorker, driverInstance.getCommandLine(), driverInstance.getId(),
                    driverInstance.getRequestQueueName(), responseQueueName, alertsExchangeName, messageListener,
                    requestSender);
            driverWorker.initialize();
            if (driverWorker.startDriver(driversDir)) {
                synchronized (workers) {
                    workers.put(driverInstance.getId(), driverWorker);
                }
            }
            /* TODO: иначе - деинициализация воркера */
        }
    }

    /**
     * Удаление устаревших очередей. С помощью RabbitMQ HTTP API получает список
     * всех очередей, выбирает из них те, которые относятся к экземплярам
     * драйвера и удаляет из них те, про которые КС не знает.
     */
    private void removeUnusedQueues() {
        String prefix = "^" + Pattern.quote(driverNamingManager.getQueueCommonPrefix() + ".drv.");
        String suffix = "\\.[^\\.]*$";
        Pattern namePattern = Pattern.compile(prefix + "(.*)" + suffix);
        List<QueueInfo> queues = rabbitHttpClient.getQueues();
        for (QueueInfo queueInfo : queues) {
            String queueName = queueInfo.getName();
            Matcher nameMatcher = namePattern.matcher(queueName);
            if (nameMatcher.matches()) {
                String driverId = nameMatcher.group(1);
                if (!driverNamingManager.isDriverIdExists(driverId)) {
                    logger.info("Queue \"{}\" is found for unknown driverId \"{}\". Deleting.", queueName, driverId);
                    try {
                        rabbitHttpClient.deleteQueue(queueInfo.getVhost(), queueName);
                    } catch (HttpClientErrorException x){
                        logger.warn("Deletion of queue \"{}\" failed", queueName);
                    }
                }
            }
        }
    }

    /**
     * Проверить жизнеспособность всех запущенных драйверов и перезапустить
     * зависшие
     */
    public void checkDriversAlive() {
        logger.trace("Checking drivers timeout...");
        /* Скопируем воркеры в локальный список, чтобы избежать блокировки внутри блокировки */
        List<DriverWorkerRegistered> currentWorkers = new ArrayList<>();
        synchronized (workers) {
            currentWorkers.addAll(workers.values());
        }
        for (DriverWorkerRegistered worker : currentWorkers) {
            /* checkDriverAlive - блокирующая */
            if (!worker.checkDriverAlive()) {
                /* Сделаем остановку драйвера на случай, если он завис, а не упал */
                worker.stopDriver();
                /* Запускаем драйвер заново */
                worker.startDriver(driversDir);
            }
        }
    }

    public void checkRabbitConnection() {
        boolean toInitialize = false;
        synchronized (initialized) {
            if (rabbitConnectionCreated && !initialized) {
                initialized = true;
                toInitialize = true;
            }
        }
        if (toInitialize) {
            initialize();
        }
    }

    @Override
    public void onCreate(Connection connection) {
        rabbitConnectionCreated = true;
    }

    @Override
    public void onClose(Connection connection) {
        rabbitConnectionCreated = false;
    }

    /** Назначает время возможной инициализации, если она не была выполнена*/
    @Override
    public Date nextExecutionTime(TriggerContext triggerContext) {
        if (!initialized) {
            return new Date();
        } else {
            return null;
        }
    }
}
