package ru.rubezh.firesec.nt.cs;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CommunicationServerArgs {
    private String driversDir;

    private static final Options options = new Options();
    static {
        options.addRequiredOption("d", "drivers-dir", true, "Drivers directory");
    }

    public static void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("communication-server", options, true);
    }

    public CommunicationServerArgs (String[] args) throws ParseException {
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = commandLineParser.parse(options, args);
        if (commandLine.hasOption("drivers-dir"))
            driversDir = commandLine.getOptionValue("drivers-dir");
    }

    public CommunicationServerArgs (CommunicationServerArgs other) {
        this.driversDir = other.driversDir;
    }

    public String getDriversDir() {
        return driversDir;
    }
}
