package ru.rubezh.firesec.nt.cs;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.listener.ResponseListener;
import ru.rubezh.firesec.nt.amqp.message.response.GetDriverProfileViewsResponse;

/**
 * Обработчик ответов от не зарегистрированного драйвера
 * @author Александр Горячкин
 */
public class RMIMessageUnregisteredListener extends ResponseListener {

    DriverWorkerUnregistered driverWorker;

    public RMIMessageUnregisteredListener(@NotNull Logger logger, DriverWorkerUnregistered driverWorker) {
        super(logger);
        this.driverWorker = driverWorker;
    }

    void handleMessage(GetDriverProfileViewsResponse message) {
        try {
            logger.debug("Driver profile views response."
                    + " Id: {}, views count: {}",
                    message.getDriverId(),
                    message.getDriverProfileViews().size());

            driverWorker.onDriverProfileViewsReceived(message.getDriverProfileViews());
        }
        catch (Exception e) {
            logger.error("Exception: {}, stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

}
