package ru.rubezh.firesec.nt.cs;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Менеджер, отвечающий за генерацию идентификаторов драйверов и имен очередей.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class DriverNamingManager {
    private String queueCommonPrefix;
    private String requestQueueSuffix;
    private String responseQueueSuffix;
    private String alertsExchangeName;
    private int maxDriverIdx;

    private Set<String> usedDriverIds = new HashSet<>();
    private Random random = new Random();

    public String getQueueCommonPrefix() {
        return queueCommonPrefix;
    }

    public void setQueueCommonPrefix(String queueCommonPrefix) {
        this.queueCommonPrefix = queueCommonPrefix;
    }

    public String getRequestQueueSuffix() {
        return requestQueueSuffix;
    }

    public void setRequestQueueSuffix(String requestQueueSuffix) {
        this.requestQueueSuffix = requestQueueSuffix;
    }

    public String getResponseQueueSuffix() {
        return responseQueueSuffix;
    }

    public void setResponseQueueSuffix(String responseQueueSuffix) {
        this.responseQueueSuffix = responseQueueSuffix;
    }

    public String getAlertsExchangeName() {
        return alertsExchangeName;
    }

    public void setAlertsExchangeName(String alertsExchangeName) {
        this.alertsExchangeName = alertsExchangeName;
    }

    public int getMaxDriverIdx() {
        return maxDriverIdx;
    }

    public void setMaxDriverIdx(int maxDriverIdx) {
        this.maxDriverIdx = maxDriverIdx;
    }

    private String getRandomId(String driverProfileId) {
        int randIdx = random.nextInt(maxDriverIdx);
        return driverProfileId + "-" + randIdx;
    }
    
    public void addDriverId(String driverId) {
        usedDriverIds.add(driverId);
    }

    public boolean isDriverIdExists(String driverId) {
        return usedDriverIds.contains(driverId);
    }

    public String generateDriverId(String driverProfileId) {
        String driverId = null;
        while (driverId == null || isDriverIdExists(driverId)) {
            driverId = getRandomId(driverProfileId);
        }
        usedDriverIds.add(driverId);
        return driverId;
    }

    public String generateDriverRequestQueueName(String driverId) {
        return queueCommonPrefix + ".drv." + driverId + "." + requestQueueSuffix;
    }

    public String generateDriverResponseQueueName(String driverId) {
        return queueCommonPrefix + ".drv." + driverId + "." + responseQueueSuffix;
    }
}
