package ru.rubezh.firesec.nt.cs;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.listener.ResponseListener;
import ru.rubezh.firesec.nt.amqp.message.response.PingResponse;

/**
 * Обработчик ответов от зарегистрированного драйвера
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 * @author Александр Горячкин
 */
public class RMIMessageRegisteredListener extends ResponseListener {

    private DriverWorkerRegistered driverWorker;

    public RMIMessageRegisteredListener(@NotNull Logger logger, @NotNull DriverWorkerRegistered driverWorker) {
        super(logger);
        this.driverWorker = driverWorker;
    }

    void handleMessage(PingResponse pingResponse) {
        logger.trace("Driver {}: catch PING response from it", driverWorker.getDriverId());
        driverWorker.onPingResponseReceived(pingResponse);
    }

}
