package ru.rubezh.firesec.nt.plugin.driver;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.Plugin;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.clock.SingleClientClock;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;

public class RubezhUSBExampleDriver implements Plugin {

    private static final byte[] msg1 = {0x01, 0x02, 0x37};
    private static final byte[] msg2 = {0x01, 0x01, 0x03};
    private static final byte[] msg3 = {0x01, 0x02, 0x14, 0x14, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00};
    private static final byte[] msg4 = {0x01, 0x02, 0x03, 0x21, 0x03, 0x01, 0x21, 0x22,
            (byte)(0xFF & 0xFF), 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00};
    private static final byte[] msg5 = {0x01, 0x01, 0x36};
    
    Logger logger;
    Transport<ByteBuffer> transport;
    
    Clock clock = new SingleClientClock();

    
    
    public RubezhUSBExampleDriver(Logger logger, Transport<ByteBuffer> transport) {
        this.logger = logger;
        this.transport = transport;
    }

    @Override
    public void onStart(long currentTimeMs) {
        transport.connect(currentTimeMs, this::onConnected);
    }

    @Override
    public void onStop(long currentTimeMs) {
        try {
            transport.disconnect(currentTimeMs);
        }
        catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void process(long currentTimeMs) {
        try {
            transport.process(currentTimeMs);
            clock.process(currentTimeMs);
        }
        catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    void onConnected(long currentTimeMs, boolean isSuccess) {
        if (isSuccess) {
            transport.send(currentTimeMs, ByteBuffer.wrap(msg1), this::onSended, this::onSendReceiveFialed);
            transport.send(currentTimeMs, ByteBuffer.wrap(msg2), this::onSended, this::onSendReceiveFialed);
            transport.send(currentTimeMs, ByteBuffer.wrap(msg3), this::onSended, this::onSendReceiveFialed);
            transport.send(currentTimeMs, ByteBuffer.wrap(msg4), this::onSended, this::onSendReceiveFialed);
            transport.send(currentTimeMs, ByteBuffer.wrap(msg5), this::onSended, this::onSendReceiveFialed);
            
            clock.setTimer(currentTimeMs, 10000, this::periodicalSend);
        }
        else {
            logger.error("Failed");
            clock.setTimer(currentTimeMs, 5000, this::reconnect);
        }
    }
    
    void reconnect(long currentTimeMs) {
        transport.connect(currentTimeMs, this::onConnected);
    }
    
    void onSended(long currentTimeMs, int sequenceNo) {
        logger.trace("Request {} sended", sequenceNo);
        transport.receive(currentTimeMs, sequenceNo, this::onReceived, this::onSendReceiveFialed);
    }

    void onSendReceiveFialed(long currentTimeMs, int sequenceNo) {
        logger.error("Failed");
    }
    
    void onReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        String hexDump = "";
        while (rawData.hasRemaining())
            hexDump += Integer.toHexString(rawData.get() & 0xFF) + " ";
        logger.trace("Response for {} received: {}", sequenceNo, hexDump);
    }
    
    void periodicalSend(long currentTimeMs) {
        transport.send(currentTimeMs, ByteBuffer.wrap(msg5), this::onPeriodicalSended, this::onSendReceivePeriodicalFialed);
    }

    void onPeriodicalSended(long currentTimeMs, int sequenceNo) {
        logger.trace("Request {} sended", sequenceNo);
        transport.receive(currentTimeMs, sequenceNo, this::onPeriodicalReceived, this::onSendReceivePeriodicalFialed);
    }

    void onSendReceivePeriodicalFialed(long currentTimeMs, int sequenceNo) {
        logger.error("Failed");
        clock.setTimer(currentTimeMs, 5000, this::reconnect);
    }

    void onPeriodicalReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        String hexDump = "";
        while (rawData.hasRemaining())
            hexDump += Integer.toHexString(rawData.get() & 0xFF) + " ";
        logger.trace("Response for {} received: {}", sequenceNo, hexDump);
        clock.setTimer(currentTimeMs, 10000, this::periodicalSend);
    }
}
