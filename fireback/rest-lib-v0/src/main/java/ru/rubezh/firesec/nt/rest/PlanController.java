package ru.rubezh.firesec.nt.rest;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.RectRegion;
import ru.rubezh.firesec.nt.domain.v0.Region;
import ru.rubezh.firesec.nt.domain.v0.RegionType;

@RepositoryRestController
@CrossOrigin
public class PlanController {

    private final PlanRepository repository;

    @Autowired
    public PlanController(PlanRepository planRepository) {
        repository = planRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plans")
    public @ResponseBody ResponseEntity<?> getPlans(@RequestParam(name = "admin_status", required = false) String adminStatus, Pageable pageable) {
        if (adminStatus == null) {
            Page<Plan> plans = repository.findAll(pageable);
            return ResponseEntity.ok(plans);
        }
        else if (adminStatus.equals("on")) {
            Plan plan = repository.findByAdminStatusIsOn();
            if (plan != null)
                return ResponseEntity.ok(plan);
            else
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("Active plan not found");
        }
        else if (adminStatus.equals("off")) {
            Page<Plan> plans = repository.findByAdminStatusIsOff(pageable);
            return ResponseEntity.ok(plans);
        }
        else
            return ResponseEntity.badRequest().build();
    }


    @RequestMapping(method = RequestMethod.GET, value = "/plans/{planId}/regions")
    public @ResponseBody ResponseEntity<?> getPlanRegion(@PathVariable String planId,
            @RequestParam(name = "device_status", required = false) DeviceStatus deviceStatus,
            @RequestParam(name = "fire_status", required = false) FireStatus fireStatus,
            Pageable pageable) {
        Plan plan = repository.findOne(planId);
        if (plan == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");
        else {
            // TODO: in a case of problems because of quit a lot of regions - filter regions in a query, using MongoTamplate
            // http://stackoverflow.com/questions/34751845/spring-boot-data-and-mongodb-filter-subdocument-array-query
            List<Region> regions = plan.getRegions();
            if (regions == null || regions.size() == 0)
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("Regions not found on plan " + planId);
            if (deviceStatus != null) {
                for (Iterator<Region> it = regions.iterator(); it.hasNext();) {
                    if (!it.next().getDeviceStatus().equals(deviceStatus))
                        it.remove();
                }
            }
            if (fireStatus != null) {
                for (Iterator<Region> it = regions.iterator(); it.hasNext();) {
                    if (!it.next().getFireStatus().equals(fireStatus))
                        it.remove();
                }
            }

            int start = pageable.getOffset();
            if (start >= regions.size())
                return ResponseEntity.badRequest().build();

            int end = (start + pageable.getPageSize()) > regions.size() ? regions.size() : (start + pageable.getPageSize());
            Page<Region> regionsPage = new PageImpl<>(regions.subList(start, end), pageable, regions.size());
            return ResponseEntity.ok(regionsPage);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plans/{planId}/regions/{regionNo}")
    public @ResponseBody ResponseEntity<?> getPlanRegion(@PathVariable String planId, @PathVariable Integer regionNo) {
        Plan plan = repository.findOne(planId);
        if (plan == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");
        else {
            Region region = plan.getRegion(regionNo);
            if (region != null) {
                if (region.getRegionType() == RegionType.rect)
                    return ResponseEntity.ok((RectRegion)region);
                else
                    return ResponseEntity.unprocessableEntity().build();
            }
            else
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body("Region " + regionNo + " on plan " + planId + " not found");
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/plans/{planId}/regions/rect")
    public @ResponseBody ResponseEntity<?> addPlanRegion(@PathVariable String planId, @RequestBody RectRegion region) {
        Plan plan = repository.findOne(planId);
        if (plan == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");
        else {
            if (plan.addRegion(region)) {
                Plan newPlan = repository.save(plan);
                if (newPlan != null)
                    return ResponseEntity.ok(newPlan);
                else
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
            else
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
