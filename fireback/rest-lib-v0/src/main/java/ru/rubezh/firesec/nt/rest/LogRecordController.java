package ru.rubezh.firesec.nt.rest;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import ru.rubezh.firesec.nt.dao.EventDescriptionRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.LogRecord;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.event.EventDescription;

@RepositoryRestController
@CrossOrigin
public class LogRecordController {

    @Autowired
    private Logger logger;
    @Autowired
    private LogRecordRepository logRecordRepository;
    @Autowired
    @Qualifier("deviceRepository_v0")
    private DeviceRepository deviceRepository;
    @Autowired
    private EventDescriptionRepository eventDescriptionRepository;
    @Autowired
    private PlanRepository planRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/log_records")
    public @ResponseBody ResponseEntity<?> getLogRecords(
            @RequestParam(name = "date_from", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date dateTimeFrom,
            @RequestParam(name = "date_to", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date dateTimeTo,
            Pageable pageable
            ) {
        Page<LogRecord> logRecords;
        if (dateTimeFrom != null) {
            if (dateTimeTo != null)
                logRecords = logRecordRepository.findByDateTimeBetween(dateTimeFrom, dateTimeTo, pageable);
            else
                logRecords = logRecordRepository.findByDateTimeAfter(dateTimeFrom, pageable);
        }
        else
            logRecords = logRecordRepository.findAll(pageable);

        for (LogRecord logRecord : logRecords) {
            Device panel = deviceRepository.findOne(logRecord.getSourceId());
            if (panel != null) {
                logRecord.setSource(panel);

                Plan plan = planRepository.findOne(panel.getPlanId());

                if (logRecord.getSourceType() == LogRecord.SourceType.device) {
                    Device device = deviceRepository.findOne(logRecord.getSourceDeviceId());
                    if (device != null) {
                        logRecord.setSourceDevice(device);
                        logRecord.setRegion(plan.getRegion(device.getRegionNo()));
                    }
                    else {
                        logger.error("Device (id: {}) not found!", logRecord.getSourceDeviceId());
                    }
                }
                else {
                    logRecord.setRegion(plan.getRegion(panel.getRegionNo()));
                }
            }
            else {
                logger.error("Panel (id: {}) not found!", logRecord.getSourceId());
            }

            EventDescription eventDescription = eventDescriptionRepository.findOneByEventCode(logRecord.getEventCode());
            logRecord.setEventDescription(eventDescription);
        }

        return ResponseEntity.ok(logRecords);
    }
}
