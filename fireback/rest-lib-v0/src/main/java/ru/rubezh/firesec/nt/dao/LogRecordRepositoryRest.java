package ru.rubezh.firesec.nt.dao;

import ru.rubezh.firesec.nt.dao.LogRecordRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Данный интерфейс здесь объявлен для автогенерации REST-контроллера с путем к ресурсу log_records
 * 
 * В библиотеке dao к интерфейсу LogRecordRepository данная аннотация специально не прописана,
 * чтобы можно было подключать репозитории без подключения REST.
 *  
 * @author Антон Васильев
 *
 */
@RepositoryRestResource(collectionResourceRel = "log_records", path = "log_records")
public interface LogRecordRepositoryRest extends LogRecordRepository {

}
