package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.FireDetector;
import ru.rubezh.firesec.nt.domain.v0.Panel;
import ru.rubezh.firesec.nt.domain.v0.PanelSimulator;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.USBRS485AdapterModule;

@RepositoryRestController
@CrossOrigin
public class DeviceController {

    @Autowired
    @Qualifier("deviceRepository_v0")
    private DeviceRepository deviceRepository;
    @Autowired
    private PlanRepository planRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/plans/{planId}/devices")
    public @ResponseBody ResponseEntity<?> getPlanRegion(@PathVariable String planId,
            @RequestParam(name = "parent", required = false) String parentDeviceId,
            Pageable pageable) {

        if (planRepository.countById(planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        if (parentDeviceId != null && deviceRepository.countByIdAndPlanId(parentDeviceId, planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Parent device " + parentDeviceId + " on plan " + planId + " not found");

        Page<Device> devices = deviceRepository.findByPlanIdAndParentDeviceId(planId, parentDeviceId, pageable);
        return ResponseEntity.ok(devices);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/plans/{planId}/regions/{regionNo}/devices")
    public @ResponseBody ResponseEntity<?> getPlanRegion(@PathVariable String planId, @PathVariable Integer regionNo, Pageable pageable) {

        Plan plan = planRepository.findOne(planId);
        if (plan == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        if (regionNo >= plan.getRegions().size())
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Region " + regionNo + " on plan " + planId + " not found");

        Page<Device> devices = deviceRepository.findByPlanIdAndRegionNo(planId, regionNo, pageable);
        return ResponseEntity.ok(devices);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/plans/{planId}/devices/panels")
    public @ResponseBody ResponseEntity<?> addPanel(@PathVariable String planId,
            @RequestBody Panel panel) {

        if (planRepository.countById(planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        panel.setPlanId(planId);
        Panel savedPanel = deviceRepository.insert(panel);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedPanel);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/plans/{planId}/devices/fire_detectors")
    public @ResponseBody ResponseEntity<?> addFireDetector(@PathVariable String planId,
            @RequestBody FireDetector fireDetector) {

        if (planRepository.countById(planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        fireDetector.setPlanId(planId);
        FireDetector savedFireDetector = deviceRepository.insert(fireDetector);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedFireDetector);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/plans/{planId}/devices/panel_simulators")
    public @ResponseBody ResponseEntity<?> addPanelSimulator(@PathVariable String planId,
            @RequestBody PanelSimulator panelSimulator) {

        if (planRepository.countById(planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        panelSimulator.setPlanId(planId);
        PanelSimulator savedPanelSimulator = deviceRepository.insert(panelSimulator);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedPanelSimulator);
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/plans/{planId}/devices/adapter_modules")
    public @ResponseBody ResponseEntity<?> addAdapterModule(@PathVariable String planId,
            @RequestBody USBRS485AdapterModule adapterModule) {

        if (planRepository.countById(planId) == 0)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Plan " + planId + " not found");

        adapterModule.setPlanId(planId);
        USBRS485AdapterModule savedAdapterModule = deviceRepository.insert(adapterModule);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(savedAdapterModule);
    }
}
