package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
import org.hid4java.HidServices;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.clock.SingleClientClock;
import ru.rubezh.firesec.nt.plugin.driver.transport.RubezhUSBTransport;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.MessageCodec;

public class AMod1DriverImpl extends Driver {
    private HidServices hidServices;
    private MessageCodec messageCodec;

    private int vendorId = 0xffffc251;
    private int productId = 0x1303;

    private DeviceProfile rootDeviceProfile;

    public AMod1DriverImpl(@NotNull Logger logger, DeviceProfile rootDeviceProfile) {
        super(logger);
        this.rootDeviceProfile = rootDeviceProfile;
    }

    public HidServices getHidServices() {
        return hidServices;
    }

    public void setHidServices(HidServices hidServices) {
        this.hidServices = hidServices;
    }

    public MessageCodec getMessageCodec() {
        return messageCodec;
    }

    public void setMessageCodec(MessageCodec messageCodec) {
        this.messageCodec = messageCodec;
    }

    @Override
    protected List<StateMachine> fetchStateMachines() {
        List<StateMachine> stateMachines = new ArrayList<>();
        for (ActiveDevice device: inServiceDevicesToSet) {
            if (device.getParentDeviceId() == null
                    && device.getDeviceProject().getDeviceProfileId().equals(rootDeviceProfile.getId())) {
                Clock clock = new SingleClientClock();
                Transport<ByteBuffer> transport = new RubezhUSBTransport(logger, hidServices, vendorId, productId,
                        device.getDeviceProject().getDevicePropertyValues().get(DeviceProperties.USBSerialNumber.toString()),
                        rootDeviceProfile.getProductName());
                List<ActiveDevice> childDevices = new ArrayList<>();
                for (ActiveDevice childDevice: inServiceDevicesToSet) {
                    if (childDevice.getAddressPath().matches("^" + device.getAddressPath() + ",.*")
                            && supportedDeviceProfiles.get(childDevice.getDeviceProject().getDeviceProfileId()) != null)
                        childDevices.add(childDevice);
                }
                StateMachineContext stateMachineContext = new StateMachineContextImpl(logger, rootDeviceProfile,
                        supportedDeviceProfiles, driverAlertSender, transport, messageCodec, clock, childDevices,
                        device, 1, new StateMachineHooksImpl());
                stateMachines.add(new StateMachineImpl(stateMachineContext));
            }
        }
        return stateMachines;
    }
}
