package ru.rubezh.firesec.nt.service;

public interface StateUpdateSubscriber {
    public void onPlanUpdated(String planId);
    public void onRegionUpdated(String planId, int regionNo);
    public void onDeviceUpdated(String deviceId);
    public void onLogRecordInserted(String logRecordId);
}
