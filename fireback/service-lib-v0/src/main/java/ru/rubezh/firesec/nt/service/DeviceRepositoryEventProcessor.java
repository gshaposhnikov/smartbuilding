package ru.rubezh.firesec.nt.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.PlanOperStatus;
import ru.rubezh.firesec.nt.domain.v0.Region;

public class DeviceRepositoryEventProcessor extends AbstractMongoEventListener<Device> {

    @Autowired
    @Qualifier("deviceRepository_v0")
    private DeviceRepository deviceRepository;
    @Autowired
    private PlanRepository planRepository;

    private Plan activePlan;
    private Queue<String> updatedDevicesIds = new ConcurrentLinkedQueue<>();

    private Set<StateUpdateSubscriber> stateUpdateSubscribers = new HashSet<>();

    private boolean aggregateOperStatus(Plan plan, List<Integer> updatedRegionNos) {
        PlanOperStatus operStatus = new PlanOperStatus();
        operStatus.setnFailDevices(deviceRepository.countByPlanIdAndDeviceStatus(plan.getId(), DeviceStatus.fail));
        operStatus.setnFireDevices(deviceRepository.countByPlanIdAndFireStatus(plan.getId(), FireStatus.fire) + deviceRepository.countByPlanIdAndFireStatus(plan.getId(), FireStatus.warning));
        operStatus.setnLostDevices(deviceRepository.countByPlanIdAndDeviceStatus(plan.getId(), DeviceStatus.lost));
        operStatus.setnNoDataDevices(deviceRepository.countByPlanIdAndDeviceStatusOrFireStatus(plan.getId(), DeviceStatus.nd, FireStatus.nd));
        operStatus.setnNormalDevices(deviceRepository.countByPlanIdAndDeviceStatusAndFireStatus(plan.getId(), DeviceStatus.normal, FireStatus.normal));

        int nFireRegions = 0;
        int regionNo = 0;
        for (Region region: plan.getRegions()) {
            DeviceStatus newDeviceStatus;
            if (deviceRepository.countByPlanIdAndRegionNoAndDeviceStatus(plan.getId(), regionNo, DeviceStatus.fail) > 0)
                newDeviceStatus = DeviceStatus.fail;
            else if (deviceRepository.countByPlanIdAndRegionNoAndDeviceStatus(plan.getId(), regionNo, DeviceStatus.lost) > 0)
                newDeviceStatus = DeviceStatus.lost;
            else if (deviceRepository.countByPlanIdAndRegionNoAndDeviceStatus(plan.getId(), regionNo, DeviceStatus.nd) > 0)
                newDeviceStatus = DeviceStatus.nd;
            else
                newDeviceStatus = DeviceStatus.normal;

            FireStatus newFireStatus;
            if (deviceRepository.countByPlanIdAndRegionNoAndFireStatus(plan.getId(), regionNo, FireStatus.fire) > 0) {
                newFireStatus = FireStatus.fire;
                nFireRegions++;
            }
            else if (deviceRepository.countByPlanIdAndRegionNoAndFireStatus(plan.getId(), regionNo, FireStatus.warning) > 0)
                newFireStatus = FireStatus.warning;
            else if (deviceRepository.countByPlanIdAndRegionNoAndFireStatus(plan.getId(), regionNo, FireStatus.nd) > 0)
                newFireStatus = FireStatus.nd;
            else
                newFireStatus = FireStatus.normal;

            if (newDeviceStatus != region.getDeviceStatus() || newFireStatus != region.getFireStatus()) {
                updatedRegionNos.add(new Integer(regionNo));
                region.setDeviceStatus(newDeviceStatus);
                region.setFireStatus(newFireStatus);
            }
            regionNo++;
        }
        operStatus.setnFireRegions(nFireRegions);

        boolean isPlanUpdated = false;
        if (!plan.getOperStatus().equals(operStatus)) {
            isPlanUpdated = true;
            plan.setOperStatus(operStatus);
        }
        else
            isPlanUpdated = false;

        return isPlanUpdated;
    }

    public void process() {
        if (activePlan != null && !updatedDevicesIds.isEmpty()) {
            List<Integer> updatedRegionNos = new ArrayList<>();
            boolean isPlanUpdated = aggregateOperStatus(activePlan, updatedRegionNos);

                /* сохраняем до отправки уведомлений */
            if (isPlanUpdated || !updatedRegionNos.isEmpty())
                activePlan = planRepository.save(activePlan);

                /* отправляем уведомления */
            for (StateUpdateSubscriber subscriber: stateUpdateSubscribers) {
                if (isPlanUpdated)
                    subscriber.onPlanUpdated(activePlan.getId());
                for (Integer regionNo: updatedRegionNos) {
                    subscriber.onRegionUpdated(activePlan.getId(), regionNo);
                }
            }

            String deviceId;
            while ((deviceId = updatedDevicesIds.poll()) != null) {
                for (StateUpdateSubscriber subscriber: stateUpdateSubscribers)
                    subscriber.onDeviceUpdated(deviceId);
            }
        }
    }
    
    public void start() {
        activePlan = planRepository.findByAdminStatusIsOn();
        if (activePlan != null) {
            List<Integer> updatedRegionNos = new ArrayList<>();
            boolean isPlanUpdated = aggregateOperStatus(activePlan, updatedRegionNos);

            if (isPlanUpdated || !updatedRegionNos.isEmpty())
                activePlan = planRepository.save(activePlan);
        }
    }

    public void stop() {
        activePlan = null;
    }

    @Override
    public void onAfterSave(AfterSaveEvent<Device> event) {
        if (activePlan != null) {
            Device device = event.getSource();
            if (device.getPlanId().equals(activePlan.getId()) && !updatedDevicesIds.contains(device.getId()))
                updatedDevicesIds.add(device.getId());
        }
    }

    @Override
    public void onAfterConvert(AfterConvertEvent<Device> event) {
        if (activePlan != null) {
            Device device = event.getSource();
            /* calculate number of child devices for front-end needs */
            device.setnChildDevices(deviceRepository.countByPlanIdAndParentDeviceId(device.getPlanId(), device.getId()));
            if (device.getParentDeviceId() != null && !device.getParentDeviceId().isEmpty()) {
                Device parentDevice = deviceRepository.findOne(device.getParentDeviceId());
                if (parentDevice != null)
                    device.setParentAddress(parentDevice.getAddress());
            }
        }
    }

    public void addStateUpdateSubscriber(StateUpdateSubscriber subscriber) {
        stateUpdateSubscribers.add(subscriber);
    }

    public void removeStateUpdateSubscriber(StateUpdateSubscriber subscriber) {
        stateUpdateSubscribers.remove(subscriber);
    }

    public void setStateUpdateSubscribers(List<StateUpdateSubscriber> newStateUpdateSubscribers) {
        for (StateUpdateSubscriber subscriber: stateUpdateSubscribers)
            removeStateUpdateSubscriber(subscriber);
        for (StateUpdateSubscriber subscriber: newStateUpdateSubscribers)
            addStateUpdateSubscriber(subscriber);
    }
}
