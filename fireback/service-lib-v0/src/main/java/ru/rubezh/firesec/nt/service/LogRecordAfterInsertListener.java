package ru.rubezh.firesec.nt.service;

import java.util.HashSet;
import java.util.Set;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;

import ru.rubezh.firesec.nt.domain.v0.LogRecord;

public class LogRecordAfterInsertListener extends AbstractMongoEventListener<LogRecord> {

    private Set<StateUpdateSubscriber> stateUpdateSubscribers = new HashSet<>();

    @Override
    public void onAfterSave(AfterSaveEvent<LogRecord> event) {
        LogRecord logRecord = event.getSource();
        
            /* предполагаем, что любое сохранение - есть вставка новой записи */
        for (StateUpdateSubscriber subscriber: stateUpdateSubscribers)
            subscriber.onLogRecordInserted(logRecord.getId());
    }

    public void addStateUpdateSubscriber(StateUpdateSubscriber subscriber) {
        stateUpdateSubscribers.add(subscriber);
    }

    public void removeStateUpdateSubscriber(StateUpdateSubscriber subscriber) {
        stateUpdateSubscribers.remove(subscriber);
    }

    public void setStateUpdateSubscribers(List<StateUpdateSubscriber> newStateUpdateSubscribers) {
        for (StateUpdateSubscriber subscriber: stateUpdateSubscribers)
            removeStateUpdateSubscriber(subscriber);
        for (StateUpdateSubscriber subscriber: newStateUpdateSubscribers)
            addStateUpdateSubscriber(subscriber);
    }
}
