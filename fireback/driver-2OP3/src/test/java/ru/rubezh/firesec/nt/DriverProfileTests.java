package ru.rubezh.firesec.nt;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile.InternalDeviceItem;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {
        "classpath:stateCategories.xml",
        "classpath:driverProfile.xml"
        })
public class DriverProfileTests {

    @Resource(name = "rubezh2op3DriverProfile")
    DriverProfile rubezh2op3DriverProfile;
    
    @Autowired
    List<DeviceProfile> deviceProfiles;
    
    @Autowired
    List<DecodableState> decodableStates;

    @Test
    public void testR2OP3EventTypeIdsUniqueness() {
        Set<String> eventTypeIds = new HashSet<>();
        for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
            assertFalse("Event type identifier " + eventProfile.getMainEventType().getId() + " must be unique", eventTypeIds.contains(eventProfile.getMainEventType().getId()));
            eventTypeIds.add(eventProfile.getMainEventType().getId());
            
            for (EventType eventType: eventProfile.getEventSubTypes()) {
                assertFalse("Event type identifier " + eventType.getId() + "must be unique", eventTypeIds.contains(eventType.getId()));
                eventTypeIds.add(eventType.getId());
            }
        }
    }
    
    @Test
    public void testR2OP3MainEventTypeCodesUniqueness() {
        Set<Integer> mainEventTypeCodes = new HashSet<>();
        for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
            assertFalse("Code of event type " + eventProfile.getMainEventType().getId() + " must be unique", mainEventTypeCodes.contains(eventProfile.getMainEventType().getCode()));
            mainEventTypeCodes.add(eventProfile.getMainEventType().getCode());
        }
    }
    
    @Test
    public void testR2OP3DecodableEventStateSetIdsUniqueness() {
        Set<String> decodableEventStateSetIds = new HashSet<>();
        for (DecodableEventStateSet decodableEventStateSet: rubezh2op3DriverProfile.getDecodableEventStateSets()) {
            assertFalse("DecodableEventStateSet identifier " + decodableEventStateSet.getId() + " must be unique", decodableEventStateSetIds.contains(decodableEventStateSet.getId()));
            decodableEventStateSetIds.add(decodableEventStateSet.getId());
        }
    }
    
    @Test
    public void testR2OP3DeviceProfileIdsOfCustomEventDeviceStatesRelevancy() {
        for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
            // main type
            for (String deviceProfileId : eventProfile.getMainEventType().getDeviceDecodableStateSetIds().keySet()) {
                assertTrue("Entry key "
                        + deviceProfileId
                        + " of device decodable state set of event "
                        + eventProfile.getMainEventType().getId()
                        + " must be a relevant device profile id",
                        rubezh2op3DriverProfile.getDeviceProfileIds().contains(deviceProfileId));
            }
            // subtypes
            for (EventType eventType: eventProfile.getEventSubTypes()) {
                for (String deviceProfileId : eventType.getDeviceDecodableStateSetIds().keySet()) {
                    assertTrue("Entry key "
                            + deviceProfileId
                            + " of device decodable state set of event "
                            + eventType.getId()
                            + " must be a relevant device profile id",
                            rubezh2op3DriverProfile.getDeviceProfileIds().contains(deviceProfileId));
                }
            }
        }
    }

    @Test
    public void testR2OP3CustomEventDeviceStateIdsRelevancy() {
        for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
            // main type
            for (String deviceDecodableStateSetId : eventProfile.getMainEventType().getDeviceDecodableStateSetIds().values()) {
                DecodableEventStateSet eventStates = null;
                for (DecodableEventStateSet decodableEventStateSet: rubezh2op3DriverProfile.getDecodableEventStateSets()) {
                    if (decodableEventStateSet.getId().equals(deviceDecodableStateSetId)) {
                        eventStates = decodableEventStateSet;
                        break;
                    }
                }
                assertNotNull("Decodable event state set "
                        + deviceDecodableStateSetId
                        + " of event "
                        + eventProfile.getMainEventType().getId()
                        + " must be determined in driver profile",
                        eventStates);
            }
            // subtypes
            for (EventType eventType: eventProfile.getEventSubTypes()) {
                for (String deviceDecodableStateSetId : eventType.getDeviceDecodableStateSetIds().values()) {
                    DecodableEventStateSet eventStates = null;
                    for (DecodableEventStateSet decodableEventStateSet: rubezh2op3DriverProfile.getDecodableEventStateSets()) {
                        if (decodableEventStateSet.getId().equals(deviceDecodableStateSetId)) {
                            eventStates = decodableEventStateSet;
                            break;
                        }
                    }
                    assertNotNull("Decodable event state set "
                            + deviceDecodableStateSetId
                            + " of event "
                            + eventType.getId()
                            + " must be determined in driver profile",
                            eventStates);
                }
            }
        }
    }
    
    @Test
    public void testR2OP3DeviceProfilesAreInDriverProfiles() {
        List<String> deviceProfileIds = new ArrayList<>();
        for (DeviceProfile deviceProfile: deviceProfiles)
            deviceProfileIds.add(deviceProfile.getId());
        deviceProfileIds.remove("commonDeviceProfile");
        deviceProfileIds.remove("am4CommonDeviceProfile");
        deviceProfileIds.remove("rmCommonDeviceProfile");
        deviceProfileIds.remove("commonSuDeviceProfile");
        assertTrue("Driver profile must contain all ids of top-level device profile beans and vise versa",
                deviceProfileIds.containsAll(rubezh2op3DriverProfile.getDeviceProfileIds()) &&
                rubezh2op3DriverProfile.getDeviceProfileIds().containsAll(deviceProfileIds));
    }
    
    @Test
    public void testR2OP3InternalDeviceProfileIdsRelevancy() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER || deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL) {
                for (InternalDeviceItem internalDeviceItem: deviceProfile.getInternalDevices()) {
                    assertTrue("Every device profile id of internal devices of device profile "
                                + deviceProfile.getId()
                                + " must be a relevant device profile id",
                                rubezh2op3DriverProfile.getDeviceProfileIds().containsAll(internalDeviceItem.getAvailableDeviceProfileIds()));
                }
            }
        }
    }

    @Test
    public void testR2OP3InternalDevicesWithInternalAddressConnectionType() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER || deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL) {
                for (InternalDeviceItem internalDeviceItem: deviceProfile.getInternalDevices()) {
                    if (internalDeviceItem.getAddressType() != AddressType.GENERIC) {
                        for (String internalDeviceProfileId: internalDeviceItem.getAvailableDeviceProfileIds()) {
                            for (DeviceProfile internalDeviceProfile: deviceProfiles) {
                                if (internalDeviceProfile.getId().equals(internalDeviceProfileId))
                                    assertTrue("Internal device profile "
                                            + internalDeviceProfileId
                                            + " must contain INTERNAL_BUS connection type because of addressInternal is set for it in a profile "
                                            + deviceProfile.getId(),
                                            internalDeviceProfile.getConnectionTypes().contains(ConnectionInterfaceType.INTERNAL_BUS));
                            }
                        }
                    }
                }
            }
        }
    }

    @Test
    public void testR2OP3InternalDevicesWithNonInternalAddressConnectionType() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER || deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL) {
                for (InternalDeviceItem internalDeviceItem: deviceProfile.getInternalDevices()) {
                    if (internalDeviceItem.getAddressType() == AddressType.GENERIC) {
                        for (String internalDeviceProfileId: internalDeviceItem.getAvailableDeviceProfileIds()) {
                            for (DeviceProfile internalDeviceProfile: deviceProfiles) {
                                if (internalDeviceProfile.getId().equals(internalDeviceProfileId)) {
                                    boolean containsAny = false;
                                    for (ConnectionInterfaceType connectionInterfaceType: internalDeviceProfile.getConnectionTypes()) {
                                        if (deviceProfile.getConnectionTypes().contains(connectionInterfaceType)) {
                                            containsAny = true;
                                            break;
                                        }
                                    }
                                    assertTrue("Internal device profile "
                                            + internalDeviceProfileId
                                            + " must contain any of the connection types of profile "
                                            + deviceProfile.getId()
                                            + " because addressInternal is not set for it",
                                            containsAny);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Test
    public void testR2OP3TriggerIdsRelevancyInDeviceProfiles() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            for(String triggerId: deviceProfile.getSupportedScenarioTriggerIds()) {
                boolean triggerIdRelevant = false;
                for (ScenarioTriggerType triggerType: rubezh2op3DriverProfile.getScenarioTriggerTypes()) {
                    if (triggerType.getId().equals(triggerId)) {
                        triggerIdRelevant = true;
                        break;
                    }
                }
                assertTrue(triggerId + " must be a relevant ScenarioTriggerType identifier"
                        ,triggerIdRelevant);
            }
        }
    }
    
    @Test
    public void testR2OP3ControlAndTransportMandatoryFields() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL || deviceProfile.getDeviceCategory() == DeviceCategory.TRANSPORT) {
                assertTrue("Profile's " + deviceProfile.getId() + " line count must be greater than zero",
                        deviceProfile.getLineCount() > 0);
                assertTrue("Profile's " + deviceProfile.getId() + " line device count must be greater than zero",
                        deviceProfile.getLineDeviceCount() > 0);
                assertTrue("Profile's " + deviceProfile.getId() + " address range must be initialized",
                        deviceProfile.getLineAddressRanges() != null && !deviceProfile.getLineAddressRanges().isEmpty());
                List<Boolean> addressMap = new ArrayList<>(256);
                for (int i = 0; i < 256; ++i) {
                    addressMap.add(false);
                }
                for (AddressRange addressRange: deviceProfile.getLineAddressRanges()) {
                    assertTrue("All profile's " + deviceProfile.getId() + " address ranges must be inside range [1; 255]",
                            addressRange.getFirstAddress() >= 1 && (addressRange.getFirstAddress() + addressRange.getAddressCount()) <= 255);
                    for (int i = 0; i < addressRange.getAddressCount(); ++i) {
                        assertFalse("Address ranges of " + deviceProfile.getId() + " must not overlap each other",
                                addressMap.get(i + addressRange.getFirstAddress()));
                        addressMap.add(i + addressRange.getFirstAddress(), true);
                    }
                }
            }
        }
    }

    @Test
    public void testR2OP3DecodableMaskedStates() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            List<DecodableState> maskedStates = deviceProfile.getCustomMaskedStates();
            for (DecodableState maskedState : maskedStates)
                assertFalse("Decodable bit state \"" + maskedState.getId() +
                        "\" must not be in a masked states list " + deviceProfile.getId() + ".customMaskedStates",
                        (maskedState.getByteNo() < 0) && (maskedState.getBitNo() >= 0));
        }
    }

    @Test
    public void testR2OP3DecodableBitStates() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            List<DecodableState> bitStates = deviceProfile.getCustomBitStates();
            for (DecodableState bitState : bitStates)
                assertFalse("Decodable masked state \"" + bitState.getId() +
                        "\" must not be in a bit states list " + deviceProfile.getId() + ".customBitStates",
                        (bitState.getBitNo() < 0) && (bitState.getByteNo() >= 0));
        }
    }

    @Test
    public void testR2OP3DecodableRegionStates() {
            for (DecodableState decodableState : decodableStates)
            assertFalse(
                    "Decodable state \"" + decodableState.getId()
                            + "\" must have bit number, or byte number, or region bit number",
                    (decodableState.getByteNo() < 0) && (decodableState.getBitNo() < 0)
                            && (decodableState.getRegionBitNo() == null));
    }

    @Test
    public void testR2OP3DeviceIgnoreStateIds() {
        Set<String> decodableStateIds = new HashSet<>();
        for (DecodableState state: decodableStates) {
            decodableStateIds.add(state.getId());
        }
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.isIgnorable() && deviceProfile.getDeviceIgnoreStateId() != null) {
                assertTrue(
                        "Device profile \"" + deviceProfile.getId()
                                + "\" must have a relevant decodable state Id for the deviceIgnoreStateId property",
                        decodableStateIds.contains(deviceProfile.getDeviceIgnoreStateId()));
            }
        }
        
    }

    @Test
    public void testR2OP3ConfigPropertySectionLinks() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            Set<String> sections = new HashSet<>();
            Set<String> links = new HashSet<>();
            for (String propertyName: deviceProfile.getConfigProperties().keySet()) {
                DeviceConfigProperty property = deviceProfile.getConfigProperties().get(propertyName);
                if (property.getType() == PropertyType.SECTION) {
                    assertFalse("Section must be unique: " + deviceProfile.getId(),
                            sections.contains(propertyName));
                    assertTrue("Section must not point to other section: " + deviceProfile.getId(),
                            property.getSection() == null);
                    sections.add(propertyName);
                } else if (property.getSection() != null) {
                    links.add(property.getSection());
                }
            }
            assertTrue("Section links must point to existed sections: " + deviceProfile.getId(),
                    sections.containsAll(links));
            assertTrue("Sections must not be empty: " + deviceProfile.getId(),
                    links.containsAll(sections));
        }
    }

    @Test
    public void testR2OP3DevicePropertySectionLinks() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            Set<String> sections = new HashSet<>();
            Set<String> links = new HashSet<>();
            for (String propertyName: deviceProfile.getDeviceProperties().keySet()) {
                DeviceProperty property = deviceProfile.getDeviceProperties().get(propertyName);
                if (property.getType() == PropertyType.SECTION) {
                    assertFalse("Section must be unique",
                            sections.contains(propertyName));
                    assertTrue("Section must not point to other section",
                            property.getSection() == null);
                    sections.add(propertyName);
                } else if (property.getSection() != null) {
                    links.add(property.getSection());
                }
            }
            assertTrue("Section links must point to existed sections: " + deviceProfile.getId(),
                    sections.containsAll(links));
            assertTrue("Sections must not be empty: " + deviceProfile.getId(),
                    links.containsAll(sections));
        }
    }

    @Test
    public void testR2OP3FireExtinctionDeviceDelayProperties() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.isFireExtinctionDevice()) {
                assertFalse("Fire extinction delay properties map is null for device " + deviceProfile.getId(),
                        deviceProfile.getFireExtinctionDelayPropertiesMap() == null);
                assertFalse("Fire extinction output type value is null or empty for device " + deviceProfile.getId(),
                        deviceProfile.getFireExtinctionOutputTypeValue() == null ||
                        deviceProfile.getFireExtinctionOutputTypeValue().isEmpty());
            }
        }
    }

}
