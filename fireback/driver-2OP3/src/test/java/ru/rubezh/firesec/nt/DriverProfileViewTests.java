package ru.rubezh.firesec.nt;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ru.rubezh.firesec.nt.domain.v1.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {
        "classpath:stateCategories.xml",
        "classpath:driverProfile.xml",
        "classpath:driverProfileRussianView.xml"
        })
public class DriverProfileViewTests {

    @Resource(name = "rubezh2op3DriverProfile")
    private DriverProfile rubezh2op3DriverProfile;

    @Resource(name = "rubezh2op3DriverProfileRussianView")
    private DriverProfileView rubezh2op3DriverProfileView;
    
    @Autowired
    List<DeviceProfile> deviceProfiles;

    @Test
    public void testR2OP3EventTypeViewToEventTypeReferences() {
        for (EventTypeView eventTypeView: rubezh2op3DriverProfileView.getEventTypeViews()) {
            EventType eventTypeReferenced = null;
            for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
                if (eventProfile.getMainEventType().getId().equals(eventTypeView.getId())) {
                    eventTypeReferenced = eventProfile.getMainEventType();
                } else {
                    for (EventType eventType: eventProfile.getEventSubTypes()) {
                        if (eventType.getId().equals(eventTypeView.getId()))
                            eventTypeReferenced = eventType;
                    }
                }
                if (eventTypeReferenced != null)
                    break;
            }
            assertNotNull("Event type " + eventTypeView.getId() + " must present in some of event profiles of driver profile", eventTypeReferenced);
        }
    }

    @Test
    public void testR2OP3EventTypeToEventTypeViewReferences() {
        for (EventProfile eventProfile: rubezh2op3DriverProfile.getEventProfiles()) {
            EventType mainEventType = eventProfile.getMainEventType();
            assertNotNull("Event type view " + mainEventType.getId() + " must present in driver profile view",
                    findReferencedTypeView(mainEventType));
            for (EventType eventSubType: eventProfile.getEventSubTypes()) {
                assertNotNull("Event type view"  + eventSubType.getId() + " must present in driver profile view",
                        findReferencedTypeView(eventSubType));
            }
        }
    }
    
    @Test
    public void testR2OP3DeviceProfileViewExistsForEveryDeviceProfile() {
        for (DeviceProfile deviceProfile: deviceProfiles) {
            if (deviceProfile.getId().equals("commonDeviceProfile") ||
                    deviceProfile.getId().equals("am4CommonDeviceProfile") ||
                    deviceProfile.getId().equals("rmCommonDeviceProfile") ||
                    deviceProfile.getId().equals("commonSuDeviceProfile"))
                continue;
            boolean viewExists = false;
            for (DeviceProfileView deviceProfileView: rubezh2op3DriverProfileView.getDeviceProfileViews()) {
                if (deviceProfileView.getDeviceProfile() == deviceProfile) {
                    viewExists = true;
                    break;
                }
            }
            assertTrue("Device profile " + deviceProfile.getId() + " must have it's view", viewExists);
        }
    }
    
    @Test
    public void testR2OP3ScenarioTriggerTypeViewExistsForEveryScenarioTriggerType() {
        for (ScenarioTriggerType triggerType: rubezh2op3DriverProfile.getScenarioTriggerTypes()) {
            boolean viewExists = false;
            for (ScenarioTriggerTypeView triggerTypeView: rubezh2op3DriverProfileView.getScenarioTriggerTypeViews()) {
                if (triggerTypeView.getId().equals(triggerType.getId())) {
                    viewExists = true;
                    break;
                }
            }
            assertTrue("Scenario trigger type " + triggerType.getId() + " must have it's view", viewExists);
        }
    }

    @Test
    public void testR2OP3ScenarioActionTypeViewExistsForEveryScenarioActionType() {
        for (ScenarioActionType actionType: rubezh2op3DriverProfile.getScenarioActionTypes()) {
            boolean viewExists = false;
            for (ScenarioActionTypeView actionTypeView: rubezh2op3DriverProfileView.getScenarioActionTypeViews()) {
                if (actionTypeView.getId().equals(actionType.getId())) {
                    viewExists = true;
                    break;
                }
            }
            assertTrue("Scenario action type " + actionType.getId() + " must have it's view", viewExists);
        }
    }

    private EventTypeView findReferencedTypeView(EventType eventType){
        for (EventTypeView eventTypeView: rubezh2op3DriverProfileView.getEventTypeViews()) {
            if (eventTypeView.getId().equals(eventType.getId()))
                return eventTypeView;
        }
        return null;
    }

}

