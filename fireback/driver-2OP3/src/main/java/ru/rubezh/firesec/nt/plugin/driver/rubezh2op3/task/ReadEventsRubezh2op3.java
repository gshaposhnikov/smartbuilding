package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3ReadEventsState.EventDescriptor;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.TimerNames;

public class ReadEventsRubezh2op3 extends Task {

    private List<String> requiredLogIds;

    private int eventsCounter = 0;
    private int progress = 0;
    private int amount = 1;

    public ReadEventsRubezh2op3(String issueId, List<String> logIds) {
        super(1000, issueId);
        requiredLogIds = logIds;
    }

    @Override
    public State execute(long currentTimeMs) {
        assert stateMachineContext.getDeviceEventCounters() != null : "Счётчики событий не проинициализированы";
        assert stateMachineContext.getDeviceMaxEventCounters() != null : "Размеры журналов событий не проинициализированы";

        stateMachineContext.getReadEventsState().prepareFullRead(stateMachineContext.getDeviceEventCounters(),
                stateMachineContext.getDeviceMaxEventCounters(), requiredLogIds);

        EventDescriptor eventToRead = stateMachineContext.getReadEventsState().getCurrentEvent();
        if (eventToRead == null) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.START_NEXT_STAGE, 0, false);
            return State.WAITING_DEVICE_EVENTS_BY_USER;
        }
        amount = (int)stateMachineContext.getReadEventsState().countEventsToRead();

        stateMachineContext.getActionExecutor().getEvent(currentTimeMs, eventToRead.logCode, eventToRead.index);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId,
                IssueStatus.IN_PROGRESS, "");
        return State.WAITING_DEVICE_EVENTS_BY_USER;
    }

    public void eventHasBeenRead() {
        eventsCounter++;
    }

    public Double getProgressUpdate() {
        int newProgress = 10 * eventsCounter / amount;
        if (newProgress != progress) {
            progress = newProgress;
            return 10.0 * progress;
        }
        return null;
    }

}
