package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3CommonRS implements Rubezh2OP3Message {
    private Rubezh2OP3FunctionType functionType;
    private boolean isSuccess = true;
    private ExceptionType exceptionType = ExceptionType.NONE;

    public Rubezh2OP3CommonRS(Rubezh2OP3FunctionType functionType) {
        this.functionType = functionType;
    }

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return functionType;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

}
