package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile.MemoryType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.intelhex.DataListener;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.intelhex.IntelHexException;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.intelhex.IntelHexParser;

public class HXPParsingHandler extends DefaultHandler2 implements DataListener {

    private final static String HEX_PACKAGE_ELEMENT_QNAME = "hexPackage";
    private final static String HEX_FILE_ELEMENT_QNAME = "hexFile";
    private final static String SOURCE_FILE_NAME_ATTR_QNAME = "comment";
    private final static String REQUIRED_DEVICE_NAME_ATTR_QNAME = "requiredDevice";
    private final static String MEMORY_TYPE_ATTR_QNAME = "memoryType";
    private final static String LOW_ADDRESS_ATTR_QNAME = "addrLow";
    private final static String HIGH_ADDRESS_ATTR_QNAME = "addrHigh";
    private final static String VERSION_ATTR_QNAME = "version";
    private final static String CRC_ATTR_QNAME = "CRC";

    /* Служебное */
    private boolean hexFileStarted = false;
    private boolean hexFileEnded;
    private IntelHexParser intelHexParser;
    private StringBuilder intelHexLineCollector = new StringBuilder();
    private HexFile currentHexFile = new HexFile();
    Logger logger;


    /* Результат расшифровки */
    private String requiredDeviceName;
    private int version = 0x6363;
    List<HexFile> hexFiles = new ArrayList<>();

    public HXPParsingHandler(@NotNull Logger logger) {
        this.logger = logger;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals(HEX_PACKAGE_ELEMENT_QNAME)) {
            requiredDeviceName = attributes.getValue(REQUIRED_DEVICE_NAME_ATTR_QNAME);
            try {
                version = Integer.valueOf(attributes.getValue(VERSION_ATTR_QNAME));
            } catch (Exception e) {
                logger.warn("Coudn't parse HEX Packege version attribute");
            }
        }
        if (qName.equals(HEX_FILE_ELEMENT_QNAME)) {
            try {
                currentHexFile.setFileName(attributes.getValue(SOURCE_FILE_NAME_ATTR_QNAME));
                currentHexFile.setMemoryType(MemoryType.getType(attributes.getValue(MEMORY_TYPE_ATTR_QNAME)));
                currentHexFile.setLowAddress(Integer.valueOf(attributes.getValue(LOW_ADDRESS_ATTR_QNAME)));
                currentHexFile.setHighAddress(Integer.valueOf(attributes.getValue(HIGH_ADDRESS_ATTR_QNAME)));
                currentHexFile.setVersion(Integer.valueOf(attributes.getValue(VERSION_ATTR_QNAME)));
                currentHexFile.setCrc(Integer.valueOf(attributes.getValue(CRC_ATTR_QNAME)));
            }
            catch (Exception e) {
            }
            if (!currentHexFile.areAttributesValid()) {
                currentHexFile.setFailureMessage("Ошибка атрибутов файла");
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(HEX_FILE_ELEMENT_QNAME)) {
            if (hexFileStarted && !hexFileEnded) {
                currentHexFile.setFailureMessage("Не найден конец IntelHex");
            }
            currentHexFile.getContinuousData().flip();
            hexFiles.add(currentHexFile);
            currentHexFile = new HexFile();
            hexFileStarted = false;
        }
    }

    @Override
    public void startCDATA() throws SAXException {
        if (currentHexFile.areAttributesValid()) {
            hexFileStarted = true;
            hexFileEnded = false;
            currentHexFile.setContinuousData(
                    ByteBuffer.allocateDirect(currentHexFile.getHighAddress() - currentHexFile.getLowAddress() + 1));
            intelHexParser = new IntelHexParser();
            intelHexParser.setDataListener(this);
            intelHexLineCollector.setLength(0);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentHexFile.areAttributesValid() && hexFileStarted && !hexFileEnded) {
            for (int i = 0; i < length && currentHexFile.isDataValid() && hexFileStarted && !hexFileEnded; ++i) {
                char currentChar = ch[i];
                if (currentChar == '\n' || currentChar == '\r') {
                    if (intelHexLineCollector.length() > 0) {
                        try {
                            intelHexParser.parseLine(intelHexLineCollector.toString());
                            intelHexLineCollector.setLength(0);
                        } catch (IntelHexException e) {
                            currentHexFile.setDataValid(false);
                            currentHexFile.setFailureMessage("Ошибка чтения IntelHex");
                            hexFileStarted = false;
                            hexFileEnded = true;
                        }
                    }
                } else {
                    intelHexLineCollector.append(currentChar);
                }
            }
        }
    }

    @Override
    public void data(long address, byte[] data) {
        /* Предполагаем, что:
         *  - lowAddres и highAddress в XML-атрибутах соответствуют содержимому intelHex
         *  - hex-файл не имеет прогалов в адресации
         *  - все участки памяти в intelHex идут последовательно
         */

        currentHexFile.getContinuousData().put(data);
//        currentHexFile.getDataItems().add(currentHexFile.new DataItem(address, data));
    }

    @Override
    public void eof() {
        hexFileEnded = true;
    }

    public List<HexFile> getHexFiles() {
        return hexFiles;
    }

    public String getRequiredDeviceName() {
        return requiredDeviceName;
    }

    public int getVersion() {
        return version;
    }

}
