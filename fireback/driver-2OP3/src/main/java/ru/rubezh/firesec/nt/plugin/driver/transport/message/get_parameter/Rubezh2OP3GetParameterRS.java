package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3CommonRS;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3FunctionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public abstract class Rubezh2OP3GetParameterRS extends Rubezh2OP3CommonRS {
    private Rubezh2OP3ParameterType parameterType;

    public Rubezh2OP3GetParameterRS(Rubezh2OP3ParameterType parameterType) {
        super(Rubezh2OP3FunctionType.GET_PARAMETER);
        this.parameterType = parameterType;
    }

    public Rubezh2OP3ParameterType getParameterType() {
        return parameterType;
    }
}
