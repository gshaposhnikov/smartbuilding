package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.SetChildDeviceConfigTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class SettingChildDeviceConfig extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.SETTING_CHILD_DEVICE_CONFIG;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        SettingChildDeviceConfig handler = new SettingChildDeviceConfig();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onChildDeviceConfigSet(long currentTimeMs) {
        try {
            SetChildDeviceConfigTask task = (SetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigSetReceived(currentTimeMs,
                    task.getActiveDevice().getId(), task.getPropertyValues());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Error setting child device config: {}", exception);

        try {
            SetChildDeviceConfigTask task = (SetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigSetReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            SetChildDeviceConfigTask task = (SetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigSetReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
