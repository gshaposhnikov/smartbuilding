package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.function.Function;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter.*;

public interface Rubezh2OP3MessageCodec {
    public ByteBuffer encode(Rubezh2OP3GetEventCountersAndStatesRQ getEventCountersAndStateBytesRQ);
    public ByteBuffer encode(Rubezh2OP3GetMaxEventCountersRQ getMaxEventCountersRQ);
    public ByteBuffer encode(Rubezh2OP3GetControlDeviceInfoRQ getDeviceSerialRQ);
    public ByteBuffer encode(Rubezh2OP3GetDataBlockRQ getDataBlockRQ);
    public ByteBuffer encode(Rubezh2OP3GetLogEventRQ getLogEventRQ);
    public ByteBuffer encode(Rubezh2OP3GetRegionStatesRQ getRegionStatesRQ);
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceStatesRQ getChildDeviceStatesRQ);
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceConfigRQ getChildDeviceConfigRQ);
    public ByteBuffer encode(Rubezh2OP3SetChildDeviceConfigRQ setChildDeviceConfigRQ);
    public ByteBuffer encode(Rubezh2OP3SetStateDeviceResetRQ setFireStateDeviceResetRQ);
    public ByteBuffer encode(Rubezh2OP3InvokeUpdateSoftwareModeRQ setUpdateSoftwareModeRQ);
    public ByteBuffer encode(Rubezh2OP3CheckDeviceReadyRQ checkDeviceReadyRQ);
    public ByteBuffer encode(Rubezh2OP3ResetRQ resetRQ);
    public ByteBuffer encode(Rubezh2OP3EraseSectorsRQ eraseSectorsRQ);
    public ByteBuffer encode(Rubezh2OP3WriteMemoryRQ writeMemoryRQ);
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareErrorRQ getUpdateSoftwareErrorRQ);
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareStatusRQ getUpdateStatusRQ);
    public ByteBuffer encode(Rubezh2OP3GetFirmwareVersionRQ getBuildNumberRQ);
    public ByteBuffer encode(Rubezh2OP3GetDatabaseVersionRQ getDatabaseVersionRQ);
    public ByteBuffer encode(Rubezh2OP3SetSecurityRegionStatusRQ setSecurityRegionStatusRQ);
    public ByteBuffer encode(Rubezh2OP3SetDevicePollingStateRQ setDevicePollingStateRQ);
    public ByteBuffer encode(Rubezh2OP3SetCurrentTimeRQ setCurrentTimeRQ);
    public ByteBuffer encode(Rubezh2OP3SetUserPasswordRQ setUserPasswordRQ);
    public ByteBuffer encode(Rubezh2OP3SetFirmwareVersionRQ setFirmwareVersionRQ);
    public ByteBuffer encode(Rubezh2OP3PerformDeviceActionRQ performDeviceActionRQ);
    public ByteBuffer encode(Rubezh2OP3PerformScenarioActionRQ performScenarioActionRQ);
    public ByteBuffer encode(Rubezh2OP3SetWaitingAccessKeyStatusRQ waitingAccessKeyStatusRQ);
    public ByteBuffer encode(Rubezh2OP3GetAddressListRQ getAddressListRQ);
    public ByteBuffer encode(Rubezh2OP3SetAddressListRQ setAddressListRQ);

    public Rubezh2OP3CommonRS decodeGetEventCountersAndStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetMaxEventCountersRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetControlDeviceInfoRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetDataBlockRS(ByteBuffer byteBuffer, int length) throws BufferUnderflowException;

    public Rubezh2OP3CommonRS decodeGetLogEventRS(ByteBuffer byteBuffer,
            Function<Integer, String> getRegionIdByLocalNo) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetRegionStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetChildDeviceStatesRS(ByteBuffer byteBuffer, BitSet regionStateBits) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetResetStateDeviceRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeInvokeUpdateSoftwareModeRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeCheckDeviceReadyRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeResetRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeEraseSectorsRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeWriteMemoryRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareErrorRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetDatabaseVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetSecurityRegionStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetDevicePollingStateRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetCurrentTimeRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetDeviceUserPasswordRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodePerformDeviceActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodePerformScenarioActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetWaitingAccessKeyStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeGetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
    public Rubezh2OP3CommonRS decodeSetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException;

}
