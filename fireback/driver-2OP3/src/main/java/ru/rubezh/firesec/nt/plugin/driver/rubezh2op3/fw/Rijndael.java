package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw;

import java.util.Arrays;

/*
 *  Rijndael(AES) — симметричный алгоритм блочного шифрования.
 */
public class Rijndael {
    /** счетчик итераций расшифровок входящих данных. */
    private int counter = 0;
    /** массив расшифрованных данных. */
    private byte[] pOut;
    /** расширенный ключ. */
    private ExtendedKey extendedKey;
    /** размер ключа файла. */
    private int fKeySize;
    /** размер блока файла. */
    private int fBlockSize;
    private byte[] rcon = new byte[30];
    private int[] t1 = new int[256];
    private int[] t2 = new int[256];
    private int[] t3 = new int[256];
    private int[] t4 = new int[256];
    private int[] t5 = new int[256];
    private int[] t6 = new int[256];
    private int[] t7 = new int[256];
    private int[] t8 = new int[256];
    private int[] u1 = new int[256];
    private int[] u2 = new int[256];
    private int[] u3 = new int[256];
    private int[] u4 = new int[256];
    private int[] S = new int[256];
    private int[] Si = new int[256];
    private int[] alog = new int[256];
    private int[] log = new int[256];
    
    /** расширенный ключ. */
    public class ExtendedKey {
        int ROUNDS;
        int BC;
        int blocksize;
        int [][] Ke= new int[15][8];
        int [][] Kd = new int [15][8];
    }
    public int getfKeySize() {
        return fKeySize;
    }

    public int getfBlockSize() {
        return fBlockSize;
    }

    public byte[] getpOut() {
        return pOut;
    }
    /*
    * @param   fileKeySize  размер ключа файла.
    * @param   fileBlockSize  размер блока файла.
    * @param   fileLength  длина ключа шифрования.
    */
    public Rijndael(int fileKeySize, int fileBlockSize, int fileLength) {
        pOut = new byte[fileLength];
        fKeySize = fileKeySize;
        fBlockSize = fileBlockSize;
        init();
    }

    public int mul(int a, int b) {
        int result = 0;
        if ((a != 0) && (b != 0)) {
            result = alog[(log[a & 0xFF] + log[b & 0xFF]) % 255];
        } else {
            result = 0;
        }
        return result;
    }

    public int mul4(int a, byte[] array) {
        int result = 0;
        int a0 = 0;
        int a1 = 0;
        int a2 = 0;
        int a3 = 0;
        if (a == 0) {
            result = 0;
            return result;
        }
        a = log[a & 0xFF];
        if (array[0] != 0) {
            a0 = (alog[(a + log[array[0] & 0xFF]) % 255] & 0xFF);
        } else {
            a0 = 0;
        }
        if (array[1] != 0) {
            a1 = (alog[(a + log[array[1] & 0xFF]) % 255] & 0xFF);
        } else {
            a1 = 0;
        }
        if (array[2] != 0) {
            a2 = (alog[(a + log[array[2] & 0xFF]) % 255] & 0xFF);
        } else {
            a2 = 0;
        }
        if (array[3] != 0) {
            a3 = (alog[(a + log[array[3] & 0xFF]) % 255] & 0xFF);
        } else {
            a3 = 0;
        }
        result = (a0 << 24) | (a1 << 16) | (a2 << 8) | a3;

        return result;
    }
    /** иницилизация данных для 256 битных блоков */
    private void init() {
        byte[][] a = { { 1, 1, 1, 1, 1, 0, 0, 0 }, { 0, 1, 1, 1, 1, 1, 0, 0 }, { 0, 0, 1, 1, 1, 1, 1, 0 },
                { 0, 0, 0, 1, 1, 1, 1, 1 }, { 1, 0, 0, 0, 1, 1, 1, 1 }, { 1, 1, 0, 0, 0, 1, 1, 1 },
                { 1, 1, 1, 0, 0, 0, 1, 1 }, { 1, 1, 1, 1, 0, 0, 0, 1 } };
        int ROOT = 0x11B;
        int j, ss, r, tt;
        byte[] b = { 0, 1, 1, 0, 0, 0, 1, 1 };
        byte[][] box = new byte[256][8];
        byte[][] cox = new byte[256][8];
        byte[][] aa = new byte[4][8];
        byte[][] ig = { { 2, 1, 1, 3 }, { 3, 2, 1, 1 }, { 1, 3, 2, 1 }, { 1, 1, 3, 2 } };
        byte[][] g = { { 2, 1, 1, 3 }, { 3, 2, 1, 1 }, { 1, 3, 2, 1 }, { 1, 1, 3, 2 } };
        byte pivot;
        byte tmp;

        alog[0] = 1;
        for (int i = 1; i < 256; i++) {
            j = (alog[i - 1] << 1) ^ alog[i - 1];
            if ((j & 0x100) != 0) {
                j = j ^ ROOT;
            }
            alog[i] = j;
        }
        for (int i = 1; i < 256; i++) {
            log[alog[i]] = i;
        }
        box[1][7] = 1;

        for (int i = 2; i < 256; i++) {
            j = alog[255 - log[i]];
            for (int t = 0; t < 8; t++) {
                box[i][t] = (byte) ((j >> (7 - t)) & 0x01);
            }
        }
        for (int i = 0; i < 256; i++) {
            for (int t = 0; t < 8; t++) {
                cox[i][t] = b[t];
                for (j = 0; j < 8; j++) {
                    cox[i][t] = (byte) (cox[i][t] ^ (a[t][j] * box[i][j]));
                }
            }
        }
        for (int i = 0; i < 256; i++) {
            S[i] = cox[i][0] << 7;
            for (int t = 1; t < 8; t++) {
                S[i] = S[i] ^ (cox[i][t] << (7 - t));
            }
            Si[S[i] & 0xFF] = i;
        }
        for (int i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                aa[i][j] = g[i][j];
            }
            aa[i][i + 4] = 1;
        }
        for (int i = 0; i < 4; i++) {
            pivot = aa[i][i];
            if (pivot == 0) {
                tt = i + 1;
                while ((aa[tt][i] == 0) & (tt < 4)) {
                    tt++;
                }
                for (j = 0; j < 8; j++) {
                    tmp = aa[i][j];
                    aa[i][j] = a[tt][j];
                    aa[tt][j] = tmp;
                }
                pivot = aa[i][i];
            }
            for (j = 0; j < 8; j++) {
                if (aa[i][j] != 0) {
                    aa[i][j] = (byte) alog[(255 + log[aa[i][j] & 0xFF] - log[pivot & 0xFF]) % 255];
                }
            }
            for (int t = 0; t < 4; t++) {
                if (i != t) {
                    for (j = i + 1; j < 8; j++) {
                        aa[t][j] = (byte) (aa[t][j] ^ mul(aa[i][j], aa[t][i]));
                    }
                    aa[t][i] = 0;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (j = 0; j < 4; j++) {
                ig[i][j] = aa[i][j + 4];
            }
        }
        for (int t = 0; t < 256; t++) {
            ss = S[t];
            t1[t] = mul4(ss, g[0]);
            t2[t] = mul4(ss, g[1]);
            t3[t] = mul4(ss, g[2]);
            t4[t] = mul4(ss, g[3]);
            ss = Si[t];
            t5[t] = mul4(ss, ig[0]);
            t6[t] = mul4(ss, ig[1]);
            t7[t] = mul4(ss, ig[2]);
            t8[t] = mul4(ss, ig[3]);
            u1[t] = mul4(t, ig[0]);
            u2[t] = mul4(t, ig[1]);
            u3[t] = mul4(t, ig[2]);
            u4[t] = mul4(t, ig[3]);
        }
        rcon[0] = 1;
        r = 1;
        for (int t = 1; t < 30; t++) {
            r = mul(2, r);
            rcon[t] = (byte) r;
        }
    }
    /*
     * Получаем расширенный ключ.
     * @param   KEY  ключ шифрования.
     * @param   keyLength  длина ключа шифрования.
     */
    public ExtendedKey initialize(String KEY, int keyLength) {
        byte[] pKey = KEY.getBytes();
        byte[] keyData = new byte[256];
        if (pKey.length >= (fKeySize >> 3)) {
            keyData = Arrays.copyOf(pKey, pKey.length);
        } else {
            keyData = Arrays.copyOf(pKey, pKey.length >> 3);
        }
        return extendedKey = makeKey(keyData, fKeySize >> 3, fBlockSize >> 3);
    }
    /*
     * Алгоритм Rijndael берет ключ шифрования и выполняет операцию
     * расширения ключа, чтобы создать набор данных для раундового ключа (kd).
     * @param   key  ключ шифрования предславленый в массиве byte.
     * @param   dataLen  размер ключа файла.
     * @param   blocksize  размер блока файла.
     */
    public ExtendedKey makeKey(byte[] key, int dataLen, int blocksize) {

        ExtendedKey result = new ExtendedKey();
        int ROUNDS, BC, ROUND_KEY_COUNT, KC;
        int j, t, tt, rconpointer;
        int[] tk = new int[8];
        if (dataLen == 16) {
            ROUNDS = 6 + blocksize / 4;
        } else {
            if (dataLen == 24 && blocksize != 32) {
                ROUNDS = 12;
            } else {
                ROUNDS = 14;
            }
        }
        BC = blocksize / 4;
        ROUND_KEY_COUNT = (ROUNDS + 1) * BC;
        KC = dataLen / 4;
        result.ROUNDS = ROUNDS;
        result.BC = BC;
        result.blocksize = blocksize;
        j = 0;
        /*
         * склеиваем каждые четыри byte ключа в один int
         */
        for (int i = 0; i != KC; i++) {
            tk[i] = (((key[j]) << 24) | (key[j + 1] << 16) | (key[j + 2] << 8) | (key[j + 3]));
            j += 4;
        }
        t = 0;
        j = 0;
        
        while ((j < KC) && (t < ROUND_KEY_COUNT)) {
            result.Ke[t / BC][t % BC] = tk[j];
            result.Kd[ROUNDS - t / BC][t % BC] = tk[j];
            j++;
            t++;
        }
        rconpointer = 0;
        while (t < ROUND_KEY_COUNT) {
            tt = tk[KC - 1];
            tk[0] = tk[0] 
                    ^ ((S[(tt >> 16) & 0xFF] & 0xFF) << 24) 
                    ^ ((S[(tt >> 8) & 0xFF] & 0xFF) << 16)
                    ^ ((S[tt & 0xFF] & 0xFF) << 8) 
                    ^ ((S[(tt >> 24) & 0xFF] & 0xFF))
                    ^ ((rcon[rconpointer] & 0xFF) << 24);
            rconpointer++;
            if (KC != 8) {
                j = 0;
                for (int i = 1; i < KC; i++) {
                    tk[i] = tk[i] ^ tk[j];
                    j++;
                }
            } else {
                j = 0;
                for (int i = 1; i < KC / 2; i++) {
                    tk[i] = tk[i] ^ tk[j];
                    j++;
                }
                tt = tk[KC / 2 - 1];
                tk[KC / 2] = tk[KC / 2] ^ ((S[tt & 0xFF] & 0xFF)) ^ ((S[(tt >> 8) & 0xFF] & 0xFF) << 8)
                        ^ ((S[(tt >> 16) & 0xFF] & 0xFF) << 16) ^ ((S[(tt >> 24) & 0xFF] & 0xFF) << 24);
                j = KC / 2;
                for (int i = j + 1; i < KC; i++) {
                    tk[i] = tk[i] ^ tk[j];
                    j++;
                }
            }
            j = 0;
            while ((j < KC) && (t < ROUND_KEY_COUNT)) {
                result.Ke[t / BC][t % BC] = tk[j];
                result.Kd[ROUNDS - t / BC][t % BC] = tk[j];
                j++;
                t++;
            }
        }
        for (int r = 1; r < ROUNDS; r++) {
            for (j = 0; j < BC; j++) {
                tt = result.Kd[r][j];
                result.Kd[r][j] = u1[(tt >> 24) & 0xFF] ^ u2[(tt >> 16) & 0xFF] ^ u3[(tt >> 8) & 0xFF] ^ u4[tt & 0xFF];
            }
        }
        return result;
    }
    /*
     * расшифровка входящего массива
     * @param  pIn массив зашифрованных данных
     */
    public void decipher(byte[] pIn) {

        int BLOCK_SIZE = 16;
        int[][][] shifts = { { { 0, 0 }, { 1, 3 }, { 2, 2 }, { 3, 1 } }, { { 0, 0 }, { 1, 5 }, { 2, 4 }, { 3, 3 } },
                { { 0, 0 }, { 1, 7 }, { 3, 5 }, { 4, 4 } } };
        int MAX_BC = 8;
        int SC, i, j, r, s_1, s_2, s_3, tt;
        int[] a = new int[MAX_BC];
        int[] t = new int[MAX_BC];
        int InOffset = 0;

        if (extendedKey.blocksize == BLOCK_SIZE) {
            decryptDefBlockSize(pIn);
        }
        if (extendedKey.BC == 4) {
            SC = 0;
        } else if (extendedKey.BC == 6) {
            SC = 1;
        } else {
            SC = 2;
        }
        s_1 = shifts[SC][1][1];
        s_2 = shifts[SC][2][1];
        s_3 = shifts[SC][3][1];
        for (i = 0; i < extendedKey.BC; i++) {
            t[i] = (((pIn[InOffset]) << 24) | ((pIn[InOffset + 1] << 16) & 0xFFFFFF)
                    | ((pIn[InOffset + 2] << 8) & 0xFFFF) | ((pIn[InOffset + 3]) & 0xFF)) ^ extendedKey.Kd[0][i];
            InOffset += 4;
            ;
        }
        for (r = 1; r < extendedKey.ROUNDS; r++) {
            for (i = 0; i < extendedKey.BC; i++) {
                a[i] = (t5[((t[i] >> 24) & 0xFF)] ^ t6[(t[(i + s_1) % extendedKey.BC] >> 16) & 0xFF]
                        ^ t7[(t[(i + s_2) % extendedKey.BC] >> 8) & 0xFF] ^ t8[t[(i + s_3) % extendedKey.BC] & 0xFF])
                        ^ extendedKey.Kd[r][i];
            }
            for (int k = 0; k < 8; k++) {
                t[k] = a[k];
            }
        }
        j = 0;
        for (i = 0; i < extendedKey.BC; i++) {
            tt = extendedKey.Kd[extendedKey.ROUNDS][i];
            pOut[j + (counter * 32)] = (byte) ((Si[((t[i] >> 24) & 0xFF)] ^ (tt >> 24)) & 0xFF);
            j++;
            pOut[j + (counter * 32)] = (byte) ((Si[((t[(i + s_1) % extendedKey.BC] >> 16) & 0xFF)] ^ (tt >> 16)) & 0xFF);
            j++;
            pOut[j + (counter * 32)] = (byte) ((Si[((t[(i + s_2) % extendedKey.BC] >> 8) & 0xFF)] ^ (tt >> 8)) & 0xFF);
            j++;
            pOut[j + (counter * 32)] = (byte) ((Si[(t[(i + s_3) % extendedKey.BC] & 0xFF)] ^ tt) & 0xFF);
            j++;
        }
        counter++;
    }
    /*
     * расшифровка входящего массива, если fBlockSize равен 16.
     * @param  pIn массив зашифрованных данных
     */
    public void decryptDefBlockSize(byte[] pIn) {
        int t_0, t_1, t_2, t_3;
        int a0, a1, a2, a3, r, tt;
        int InOffset = 0;
        t_0 = (((pIn[InOffset + 0]) << 24) | (pIn[InOffset + 1] << 16) | (pIn[InOffset + 2] << 8) | (pIn[InOffset + 3]))
                ^ extendedKey.Kd[0][0];
        t_1 = (((pIn[InOffset + 4]) << 24) | (pIn[InOffset + 5] << 16) | (pIn[InOffset + 6] << 8) | (pIn[InOffset + 7]))
                ^ extendedKey.Kd[0][1];
        t_2 = (((pIn[InOffset + 8]) << 24) | (pIn[InOffset + 9] << 16) | (pIn[InOffset + 10] << 8)
                | (pIn[InOffset + 11])) ^ extendedKey.Kd[0][2];
        t_3 = (((pIn[InOffset + 12]) << 24) | (pIn[InOffset + 13] << 16) | (pIn[InOffset + 14] << 8)
                | (pIn[InOffset + 15])) ^ extendedKey.Kd[0][3];
        for (r = 1; r != extendedKey.ROUNDS - 1; r++) {
            a0 = (t1[t_0 >> 24 != 0 && true ? 1 : 0] ^ t2[t_3 >> 16 != 0 && true ? 1 : 0]
                    ^ t3[t_2 >> 8 != 0 && true ? 1 : 0] ^ t4[t_1 != 0 && true ? 1 : 0]) ^ extendedKey.Kd[r][0];
            a1 = (t1[t_1 >> 24 != 0 && true ? 1 : 0] ^ t2[t_0 >> 16 != 0 && true ? 1 : 0]
                    ^ t3[t_3 >> 8 != 0 && true ? 1 : 0] ^ t4[t_2 != 0 && true ? 1 : 0]) ^ extendedKey.Kd[r][1];
            a2 = (t1[t_2 >> 24 != 0 && true ? 1 : 0] ^ t2[t_1 >> 16 != 0 && true ? 1 : 0]
                    ^ t3[t_0 >> 8 != 0 && true ? 1 : 0] ^ t4[t_3 != 0 && true ? 1 : 0]) ^ extendedKey.Kd[r][2];
            a3 = (t1[t_3 >> 24 != 0 && true ? 1 : 0] ^ t2[t_2 >> 16 != 0 && true ? 1 : 0]
                    ^ t3[t_1 >> 8 != 0 && true ? 1 : 0] ^ t4[t_0 != 0 && true ? 1 : 0]) ^ extendedKey.Kd[r][3];
            t_0 = a0;
            t_1 = a1;
            t_2 = a2;
            t_3 = a3;
        }

        tt = extendedKey.Kd[extendedKey.ROUNDS][0];
        pOut[0 + (counter * 32)] = (byte) ((Si[t_0 >> 24 != 0 && true ? 1 : 0] ^ tt >> 24) != 0 && true ? 1 : 0);
        pOut[1 + (counter * 32)] = (byte) ((Si[t_3 >> 16 != 0 && true ? 1 : 0] ^ tt >> 16) != 0 && true ? 1 : 0);
        pOut[2 + (counter * 32)] = (byte) ((Si[t_2 >> 8 != 0 && true ? 1 : 0] ^ tt >> 8) != 0 && true ? 1 : 0);
        pOut[3 + (counter * 32)] = (byte) ((Si[t_1 != 0 && true ? 1 : 0] ^ tt) != 0 && true ? 1 : 0);
        tt = extendedKey.Kd[extendedKey.ROUNDS][1];
        pOut[4 + (counter * 32)] = (byte) ((Si[t_1 >> 24 != 0 && true ? 1 : 0] ^ tt >> 24) != 0 && true ? 1 : 0);
        pOut[5 + (counter * 32)] = (byte) ((Si[t_0 >> 16 != 0 && true ? 1 : 0] ^ tt >> 16) != 0 && true ? 1 : 0);
        pOut[6 + (counter * 32)] = (byte) ((Si[t_3 >> 8 != 0 && true ? 1 : 0] ^ tt >> 8) != 0 && true ? 1 : 0);
        pOut[7 + (counter * 32)] = (byte) ((Si[t_2 != 0 && true ? 1 : 0] ^ tt) != 0 && true ? 1 : 0);
        tt = extendedKey.Kd[extendedKey.ROUNDS][2];
        pOut[8 + (counter * 32)] = (byte) ((Si[t_2 >> 24 != 0 && true ? 1 : 0] ^ tt >> 24) != 0 && true ? 1 : 0);
        pOut[9 + (counter * 32)] = (byte) ((Si[t_1 >> 16 != 0 && true ? 1 : 0] ^ tt >> 16) != 0 && true ? 1 : 0);
        pOut[10 + (counter * 32)] = (byte) ((Si[t_0 >> 8 != 0 && true ? 1 : 0] ^ tt >> 8) != 0 && true ? 1 : 0);
        pOut[11 + (counter * 32)] = (byte) ((Si[t_3 != 0 && true ? 1 : 0] ^ tt) != 0 && true ? 1 : 0);
        tt = extendedKey.Kd[extendedKey.ROUNDS][3];
        pOut[12 + (counter * 32)] = (byte) ((Si[t_3 >> 24 != 0 && true ? 1 : 0] ^ tt >> 24) != 0 && true ? 1 : 0);
        pOut[13 + (counter * 32)] = (byte) ((Si[t_2 >> 16 != 0 && true ? 1 : 0] ^ tt >> 16) != 0 && true ? 1 : 0);
        pOut[14 + (counter * 32)] = (byte) ((Si[t_1 >> 8 != 0 && true ? 1 : 0] ^ tt >> 8) != 0 && true ? 1 : 0);
        pOut[15 + (counter * 32)] = (byte) ((Si[t_0 != 0 && true ? 1 : 0] ^ tt) != 0 && true ? 1 : 0);
        counter++;
    }

}
