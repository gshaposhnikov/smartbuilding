package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

/**
 * Абстрактный элемент таблицы списков точек доступа.
 * 
 * @author Антон Васильев
 *
 */
public abstract class AccessPointListTE implements SkudTableEntry {

    private static final int ACCESS_POINTS_NUMBER_SIZE = 2;
    private static final int LINE_SIZE = 1;
    private static final int ADDRESS_SIZE = 1;

    private static int idCounter = 1;

    /** Идентификатор владельца списка - (сотрудника, ключа доступа и т.п.) */
    protected String ownerId;
    /** Запомненный числовой идентификатор списка */
    private int id;
    /** Запомненный список устройств - точек доступа */
    private List<ActiveDevice> accessPointDevices;
    // TODO: внедрить мемоизацию
    // (https://dzone.com/articles/java-8-automatic-memoization), когда будем
    // реализовывать автосохранение изменений

    public static void resetIdCounter() {
        idCounter = 1;
    }

    public AccessPointListTE(String ownerId) {
        this.ownerId = ownerId;
        this.id = idCounter++;
    }

    protected abstract List<ActiveDevice> getAccessPointDevices(
            SkudDatabaseBuilder skudDatabaseBuilder);
    
    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        if (id > MAX_SKUD_ENTRY_ID) {
            skudDatabaseBuilder.addError(Code.ACCESS_POINT_LIST_ID_EXCEEDS_LIMIT, EntityType.CONTROL_DEVICE,
                    skudDatabaseBuilder.getStateMachineContext().getRubezh2op3Device().getId());
            return;
        }

        if (accessPointDevices == null)
            accessPointDevices = getAccessPointDevices(skudDatabaseBuilder);
        writeId(id, stream);
        stream.writeIntegerLittleEndian(accessPointDevices.size(), ACCESS_POINTS_NUMBER_SIZE);
        writeBitFlags(stream);
        for (ActiveDevice accessPointDevice : accessPointDevices) {
            stream.writeInteger(accessPointDevice.getDeviceProject().getLineNo() - 1, LINE_SIZE);
            stream.writeInteger(accessPointDevice.getDeviceProject().getLineAddress(),
                    ADDRESS_SIZE);
        }
    }

    public int getId() {
        return id;
    }

}
