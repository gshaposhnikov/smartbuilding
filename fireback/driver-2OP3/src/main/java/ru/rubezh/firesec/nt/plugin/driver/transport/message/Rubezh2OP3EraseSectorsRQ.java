package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3EraseSectorsRQ implements Rubezh2OP3Message {

    private int startSector;
    private int endSector;

    public Rubezh2OP3EraseSectorsRQ(int startSector, int endSector) {
        assert startSector >= 0x01;
        assert startSector <= endSector;
        this.startSector = startSector;
        this.endSector = endSector;
    }

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return Rubezh2OP3FunctionType.ERASE_SECTORS;
    }

    public int getStartSector() {
        return startSector;
    }

    public int getEndSector() {
        return endSector;
    }

}
