package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.GetChildDeviceStatesTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceStatesInfo;

public class WaitingChildDeviceStates extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_CHILD_DEVICE_STATES;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingChildDeviceStates handler = new WaitingChildDeviceStates();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onChildDeviceStatesReceived(long currentTimeMs, ChildDeviceStatesInfo childDeviceStatesInfo) {
        GetChildDeviceStatesTask task = null;
        try {
            task = (GetChildDeviceStatesTask)stateMachineContext.getTaskManager().getCurrentTask();
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Can't cast task object to appropriate sub-class");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
        if (childDeviceStatesInfo != null) {
            Device device = task.getActiveDevice().getDeviceProject();
            if (device.getDeviceProfileId().equals(childDeviceStatesInfo.getDeviceProfileId())
                    && device.getLineNo() == childDeviceStatesInfo.getLineNo()
                    && device.getLineAddress() == childDeviceStatesInfo.getAddress()) {
                stateMachineContext.getStateMachineHooks().onChildDeviceStatesReceived(
                        currentTimeMs, device.getId(), childDeviceStatesInfo.getActiveStates(),
                        childDeviceStatesInfo.getActiveValues(), childDeviceStatesInfo.getQos());
            } else {
                stateMachineContext.getLogger().warn("Got response for the wrong device");
            }
        } else {
            stateMachineContext.getLogger().warn("No such device");
        }

        ActiveDevice urgentDeviceToPoll = null;
        task.statesReceived();
        if (!task.completed())
            urgentDeviceToPoll = stateMachineContext.getChildDevicePollingState().getUrgentDeviceToPoll();
        if (urgentDeviceToPoll != null) {
            stateMachineContext.getActionExecutor().getChildDeviceStates(currentTimeMs,
                    urgentDeviceToPoll.getDeviceProject().getLineNo(),
                    urgentDeviceToPoll.getDeviceProject().getLineAddress(),
                    urgentDeviceToPoll.getDeviceProject().getRegionId());
            return getCurrentState();
        } else {
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getStateMachineHooks().onChildDeviceStatesReceiveFailed(currentTimeMs, exception);
        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

}
