package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.PerformDeviceActionTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class PerformingDeviceAction extends AbstractTaskState {

    private static final int ISSUE_DELAY = 5000;

    @Override
    public State getCurrentState() {
        return State.PERFORMING_DEVICE_ACTION;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        PerformingDeviceAction handler = new PerformingDeviceAction();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onDeviceActionPerformed(long currentTimeMs) {
        try {
            PerformDeviceActionTask task = (PerformDeviceActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDeviceActionPerformed(currentTimeMs,
                    task.getActiveDevice().getId(), task.getActionId());
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                    task.getIssueId(), 50);
            stateMachineContext.getActionExecutor().setIssueCloseTimer(currentTimeMs,
                    task.getIssueId(), ISSUE_DELAY);
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Error performing device action: {}", exception);

        try {
            PerformDeviceActionTask task = (PerformDeviceActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDeviceActionFailed(currentTimeMs,
                    task.getActiveDevice().getId(), task.getActionId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            PerformDeviceActionTask task = (PerformDeviceActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDeviceActionFailed(currentTimeMs,
                    task.getActiveDevice().getId(), task.getActionId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
