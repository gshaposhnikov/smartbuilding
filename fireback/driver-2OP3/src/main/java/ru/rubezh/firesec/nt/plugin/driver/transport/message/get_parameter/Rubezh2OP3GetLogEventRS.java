package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

import java.util.BitSet;

public class Rubezh2OP3GetLogEventRS extends Rubezh2OP3GetParameterRS {
    ControlDeviceEvent logEvent;
    String regionId;
    BitSet regionStateBits;

    public Rubezh2OP3GetLogEventRS() {
        super(Rubezh2OP3ParameterType.LOG_EVENT);
    }

    public ControlDeviceEvent getLogEvent() {
        return logEvent;
    }

    public void setLogEvent(ControlDeviceEvent deviceLogEvent) {
        this.logEvent = deviceLogEvent;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public BitSet getRegionStateBits() {
        return regionStateBits;
    }

    public void setRegionStateBits(BitSet regionStateBits) {
        this.regionStateBits = regionStateBits;
    }

}
