package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetDevicePollingStateRQ extends Rubezh2OP3SetParameterRQ {

    private int type;
    private int lineNo;
    private int address;
    private boolean pollingState;

    public Rubezh2OP3SetDevicePollingStateRQ(int type, int lineNo, int address, boolean pollingState) {
        super(Rubezh2OP3ParameterType.RESET_STATE);
        this.type = type;
        this.lineNo = lineNo;
        this.address = address;
        this.pollingState = pollingState;
    }

    public int getType() {
        return type;
    }

    public int getLineNo() {
        return lineNo;
    }

    public int getAddress() {
        return address;
    }

    public boolean isPollingState() {
        return pollingState;
    }
}
