package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3FunctionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3SetGetParameterRQ;

public abstract class Rubezh2OP3SetParameterRQ implements Rubezh2OP3SetGetParameterRQ {

    private Rubezh2OP3ParameterType parameterType;

    public Rubezh2OP3SetParameterRQ(Rubezh2OP3ParameterType parameterType) {
        this.parameterType = parameterType;
    }

    @Override
    public Rubezh2OP3ParameterType getParameterType() {
        return parameterType;
    }

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return Rubezh2OP3FunctionType.SET_PARAMETER;
    }

}
