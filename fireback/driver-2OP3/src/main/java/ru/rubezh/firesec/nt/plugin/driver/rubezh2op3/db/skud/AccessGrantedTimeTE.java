package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.AccessGrantedTime;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class AccessGrantedTimeTE implements SkudTableEntry {
    private static final int TIME_SIZE = 1;
    private static final int TIME_MINUTES_BIT_OFFSET = 3;
    private static final int TIME_HOURS_MASK = 0x1F;
    private static final int TIME_MINUTES_MASK = 0x07;

    /** Идентификатор рабочего графика */
    private String workScheduleId;
    /** Номер дня в графике работ */
    private int dayNo;
    /** Номер интервала разрешенного прохода в дне графика работ */
    private int accessGrantedTimeNo;

    public AccessGrantedTimeTE(String workScheduleId, int dayNo, int accessGrantedTimeNo) {
        this.workScheduleId = workScheduleId;
        this.dayNo = dayNo;
        this.accessGrantedTimeNo = accessGrantedTimeNo;
    }

    private int getFormatedTime(int dayMinutes) {
        assert dayMinutes >= 0;
        assert dayMinutes <= (24 * 60);

        /* Время пишем как в документе, только в Little-Endian (минуты в начале) */
        int formatedTime = (dayMinutes / 60) & TIME_HOURS_MASK;
        int minutes = dayMinutes % 60;

        if (minutes >= 0 && minutes < 8) {
            minutes = 0; // округлаем к 0 минутам
        } else if (minutes >= 8 && minutes < 23) {
            minutes = 1; // округляем к 15 минутам
        } else if (minutes >= 23 && minutes < 38) {
            minutes = 2; // округляем к 30 минутам
        } else if (minutes >= 38 && minutes < 53) {
            minutes = 3; // округляем к 45 минутам
        } else {
            minutes = 4; // округляем к 60 минутам
        }

        formatedTime <<= TIME_MINUTES_BIT_OFFSET;
        formatedTime |= (minutes & TIME_MINUTES_MASK);
        return formatedTime;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        WorkSchedule workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules().get(workScheduleId);
        assert workSchedule != null;
        assert dayNo < workSchedule.getDays().size();

        WorkScheduleDay day = workSchedule.getDays().get(dayNo);
        assert day != null;
        assert accessGrantedTimeNo < day.getAccessGrantedTimes().size();

        AccessGrantedTime grantedTime = day.getAccessGrantedTimes().get(accessGrantedTimeNo);
        assert grantedTime != null;

        writeId(grantedTime.getIndex(), stream);
        stream.writeInteger(getFormatedTime(grantedTime.getFrom()), TIME_SIZE);
        stream.writeInteger(getFormatedTime(grantedTime.getTo()), TIME_SIZE);
        writeBitFlags(stream);
    }

}
