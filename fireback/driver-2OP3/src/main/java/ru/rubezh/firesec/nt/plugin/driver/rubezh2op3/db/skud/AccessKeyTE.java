package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.util.Date;

import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey.KeyType;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class AccessKeyTE implements SkudTableEntry {

    private static final int KEY_VALUE_SIZE = 6;
    private static final char PASSWORD_EMPTY_SYMBOL_STUB = 0xF & 0xFF;
    private static final char CARD_EMPTY_SYMBOL_STUB = 0x0 & 0xFF;
    private static final int KEY_PERMISSIONS_SIZE = 1;
    private static final int KEY_PERMISSIONS_VALUE = 1; // пока только СКУД, без охранки и сценариев
    private static final int SECURITY_REGION_LIST_ID_STUB = 0xFFFF & 0xFFFF;
    private static final int SCENARIO_LIST_ID_STUB = 0xFFFF & 0xFFFF;
    private static final int REZERVED_FLAGS = 0xFF & 0xFF;
    private static final int REZERVED_FLAGS_SIZE = 1;
    private static final int WORK_SCHEDULE_ID_STUB = 0xFFFF & 0xFFFF;
    private static final int YEAR_SIZE = 1;
    private static final int MONTH_SIZE = 1;
    private static final int DATE_SIZE = 1;
    private static final int YEAR_MONTH_DATE_STUB = 0x9B0C1E & 0xFFFFFF;
    /*
     * TODO: писать KEY_PERMISSIONS в зависимости от наличия настроек СКУД, охранки,
     * автоматизации
     */
    /*
     * TODO: писать действительные идентификаторы списка охранных зон и списка
     * сценариев
     */

    private String accessKeyId;

    public AccessKeyTE(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        AccessKey accessKey = skudDatabaseBuilder.getStateMachineContext().getAccessKeys().get(accessKeyId);
        assert accessKey != null;

        Employee employee = skudDatabaseBuilder.getStateMachineContext().getEmployees().get(accessKey.getEmployeeId());
        assert employee != null;

        Integer accessPointListId = skudDatabaseBuilder.getAccessPointListTEIndexByAccessKeyIds()
                .get(accessKey.getId());
        assert accessPointListId != null;

        WorkSchedule workSchedule = null;
        if (accessKey.getAccessMap().getWorkScheduleId() != null
                && !accessKey.getAccessMap().getWorkScheduleId().isEmpty()) {
            workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules()
                    .get(accessKey.getAccessMap().getWorkScheduleId());
        }
        if (workSchedule == null && employee.getAccessMap().getWorkScheduleId() != null
                && !employee.getAccessMap().getWorkScheduleId().isEmpty()) {
            workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules()
                    .get(employee.getAccessMap().getWorkScheduleId());
        }

        writeKeyValue(accessKey.getKeyValue(),
                accessKey.getKeyType() == KeyType.PASSWORD ? PASSWORD_EMPTY_SYMBOL_STUB : CARD_EMPTY_SYMBOL_STUB,
                stream);
        writeId(accessKey.getIndex(), stream);
        stream.writeInteger(KEY_PERMISSIONS_VALUE, KEY_PERMISSIONS_SIZE);
        writeId(employee.getIndex(), stream);
        writeId(SECURITY_REGION_LIST_ID_STUB, stream);
        stream.writeInteger(REZERVED_FLAGS, REZERVED_FLAGS_SIZE);
        writeId(SCENARIO_LIST_ID_STUB, stream);
        writeId(accessPointListId, stream);
        writeId(workSchedule != null ? workSchedule.getIndex() : WORK_SCHEDULE_ID_STUB, stream);
        writeDate(accessKey.getActiveFrom(), stream);
        writeDate(accessKey.getActiveTo(), stream);
        
        int flags = 0;
        if (!accessKey.isLocked()) flags |=             0b00000001;
        if (!accessKey.isIgnoreAPBOn()) flags |=        0b00000010;
        if (!accessKey.isConfirmationKeyOn()) flags |=  0b00000100;
        if (!accessKey.isAdvancedCodeOff()) flags |=    0b00001000;
        if (!accessKey.isDisarmForcedOn()) flags |=     0b00010000;
        
        // Не используемые биты на всякий случай взведем, т.к. 0 - это как правило true в БД СКУД
        flags |=                                        0b11100000;
        stream.writeInteger(flags, BIT_FLAGS_SIZE);
    }

    private void writeKeyValue(String keyValue, char emptySymbolStub, DatabaseOutputStream stream) {
        StringBuilder appendedKeyValue = new StringBuilder(keyValue);
        while (appendedKeyValue.length() < KEY_VALUE_SIZE * 2)
            appendedKeyValue.append(emptySymbolStub);
        byte[] keyValueBytes = new byte[KEY_VALUE_SIZE];
        for (int i = 0; i < KEY_VALUE_SIZE; ++i) {
            char asciiSymbol = appendedKeyValue.charAt(i * 2);
            int symbolNumber = char2Int(asciiSymbol, emptySymbolStub);
            keyValueBytes[i] = (byte) ((symbolNumber << 4) & 0xF0);
            asciiSymbol = appendedKeyValue.charAt(i * 2 + 1);
            symbolNumber = char2Int(asciiSymbol, emptySymbolStub);
            keyValueBytes[i] |= (byte) (symbolNumber & 0xF);
        }
        stream.writeByteArray(keyValueBytes);
    }

    private int char2Int(char asciiSymbol, char emptySymbolStub) {
        if (asciiSymbol >= '0' && asciiSymbol <= '9')
            return asciiSymbol - '0';
        else if (asciiSymbol >= 'a' && asciiSymbol <= 'f')
            return asciiSymbol - 'a' + 10;
        else if (asciiSymbol >= 'A' && asciiSymbol <= 'F')
            return asciiSymbol - 'A' + 10;
        else
            return emptySymbolStub;
    }

    @SuppressWarnings("deprecation")
    private void writeDate(Date date, DatabaseOutputStream stream) {
        if (date != null) {
            /*
             * Нужно записать текущий год - 2000. Date.getYear() возвращает текущий год - 1900. Поэтому - вычитаем 100.
             */
            stream.writeInteger(date.getYear() - 100, YEAR_SIZE);
            /* Date.getMonth() возвращает номер месяца с 0, нам нужно записать номер с 1. */
            stream.writeInteger(date.getMonth() + 1, MONTH_SIZE);
            stream.writeInteger(date.getDate(), DATE_SIZE);
        } else {
            stream.writeInteger(YEAR_MONTH_DATE_STUB, YEAR_SIZE + MONTH_SIZE + DATE_SIZE);
        }
    }

}
