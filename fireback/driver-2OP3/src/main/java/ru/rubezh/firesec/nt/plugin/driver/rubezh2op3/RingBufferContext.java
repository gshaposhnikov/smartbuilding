package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

/** Контекст чтения/записи кольцевого буфера */
public class RingBufferContext {
    private final long size;
    private long readIndex;
    private long writeIndex;

    public RingBufferContext(long size) {
        assert size >= 0 && size < Long.MAX_VALUE : "Недопустимый размер буфера";
        this.size = size;
    }

    public void setIndexes(long readIndex, long writeIndex) {
        assert readIndex >= 0 && readIndex < size : "Недопустимый индекс чтения";
        assert writeIndex >= 0 && writeIndex < size : "Недопустимый индекс записи";
        this.readIndex = readIndex;
        this.writeIndex = writeIndex;
    }

    public void shiftWriteIndex(long writeIndex)  {
        assert writeIndex >= 0 && writeIndex < size : "Недопустимый индекс записи";
        boolean overlap = writeIndex < this.writeIndex && writeIndex > this.readIndex;
        this.writeIndex = writeIndex;
        if (overlap) {
            this.readIndex = writeIndex;
            incReadIndex();
        }
    }

    public void fixMaxReadItems(long maxItemsToRead) {
        assert maxItemsToRead >= 0 && maxItemsToRead < size : "Недопустимый размер окна чтения";
        if (itemsToRead() > maxItemsToRead) {
            readIndex = writeIndex;
            decReadIndex(maxItemsToRead);
        }
    }

    public Long getReadIndex() {
        if (nothingToRead())
            return null;
        return readIndex;
    }

    public Long getNextReadIndex() {
        if (nothingToRead())
            return null;
        incReadIndex();
        return getReadIndex();
    }

    public long itemsToRead() {
        return !inverted() ? (writeIndex - readIndex) : (size - readIndex + writeIndex);
    }

    public long getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "r" + readIndex + ", w" + writeIndex;
    }

    private boolean nothingToRead() {
        return writeIndex == readIndex;
    }

    private void incReadIndex() {
        readIndex++;
        if (readIndex >= size)
            readIndex = 0;
    }

    private void decReadIndex(long decrease) {
        readIndex -= decrease;
        if (readIndex < 0)
            readIndex += size;
    }

    private boolean inverted() {
        return readIndex > writeIndex;
    }
}
