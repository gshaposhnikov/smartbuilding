package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.amqp.message.request.DeleteTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.PauseTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.SetDriverDeviceConfigRequest;
import ru.rubezh.firesec.nt.amqp.message.response.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.IssueParams.WriteSkudDatabaseParams;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock.LogicType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.TracingEntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContextImpl;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineHooksImpl;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineImpl;
import ru.rubezh.firesec.nt.plugin.driver.clock.MultiClientClock;
import ru.rubezh.firesec.nt.plugin.driver.clock.SingleClientClock;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.ChildDeviceConfigCodec;
import ru.rubezh.firesec.nt.plugin.driver.transport.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.DestinationType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.MessageCodec;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.RSR3DestinationViaAdapterModuleDecorator;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.Rubezh2OP3MessageCodec;

public class Rubezh2OP3DriverImpl extends Driver {

    private HidDeviceScanner hidDeviceScanner;
    private Rubezh2OP3MessageCodec rubezh2op3MessageCodec;
    private Rubezh2OP3MessageCodec rubezh2op3OnUSBMessageCodec;
    private MessageCodec amodMessageCodec;
    private ChildDeviceConfigCodec childDeviceConfigCodec;

    private int vendorId = 0xffffc251;
    private int productId = 0x1303;

    private Map<String, DeviceProfile> amodDeviceProfiles;
    private DeviceProfile rubezh2op3DeviceProfile;

    /** Обработчики внешних запросов */
    private List<Rubezh2OP3ExternalRequestHandler> externalRequestHandlers = new ArrayList<>();

    /**
     * Карта обработчиков внешних запросов по идентификаторам ВСЕХ устройств
     * (сам обработчик один на конечный автомат, т.е. на прибор)
     */
    private Map<String, Rubezh2OP3ExternalRequestHandler> externalRequestHandlersByAnyDeviceId = new HashMap<>();

    /**
     * Карта обработчиков внешних запросов по идентификаторам зон (сам
     * обработчик один на конечный автомат, т.е. на прибор)
     */
    private Map<String, List<Rubezh2OP3ExternalRequestHandler>> externalRequestHandlersByRegionId = new HashMap<>();

    /**
     * Карта обработчиков внешних запросов по идентификаторам сценариев (сам
     * обработчик один на конечный автомат, т.е. на прибор)
     */
    private Map<String, List<Rubezh2OP3ExternalRequestHandler>> externalRequestHandlersByScenarioId = new HashMap<>();

    @Autowired
    public Rubezh2OP3DriverImpl(Logger logger) {
        super(logger);

        initScenarioCheckers();
    }

    public HidDeviceScanner getHidDeviceScanner() {
        return hidDeviceScanner;
    }

    public void setHidDeviceScanner(HidDeviceScanner hidDeviceScanner) {
        this.hidDeviceScanner = hidDeviceScanner;
    }

    public Rubezh2OP3MessageCodec getRubezh2op3MessageCodec() {
        return rubezh2op3MessageCodec;
    }

    public void setRubezh2op3MessageCodec(Rubezh2OP3MessageCodec rubezh2op3MessageCodec) {
        this.rubezh2op3MessageCodec = rubezh2op3MessageCodec;
    }

    public Rubezh2OP3MessageCodec getRubezh2op3OnUSBMessageCodec() {
        return rubezh2op3OnUSBMessageCodec;
    }

    public void setRubezh2op3OnUSBMessageCodec(Rubezh2OP3MessageCodec rubezh2op3OnUSBMessageCodec) {
        this.rubezh2op3OnUSBMessageCodec = rubezh2op3OnUSBMessageCodec;
    }

    public MessageCodec getAmodMessageCodec() {
        return amodMessageCodec;
    }

    public void setAmodMessageCodec(MessageCodec amodMessageCodec) {
        this.amodMessageCodec = amodMessageCodec;
    }

    public ChildDeviceConfigCodec getChildDeviceConfigCodec() {
        return childDeviceConfigCodec;
    }

    public void setChildDeviceConfigCodec(ChildDeviceConfigCodec childDeviceConfigCodec) {
        this.childDeviceConfigCodec = childDeviceConfigCodec;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Map<String, DeviceProfile> getAmodDeviceProfiles() {
        return amodDeviceProfiles;
    }

    public void setAmodDeviceProfiles(Map<String, DeviceProfile> amodDeviceProfiles) {
        this.amodDeviceProfiles = amodDeviceProfiles;
    }

    public DeviceProfile getRubezh2op3DeviceProfile() {
        return rubezh2op3DeviceProfile;
    }

    public void setRubezh2op3DeviceProfile(DeviceProfile rubezh2op3DeviceProfile) {
        this.rubezh2op3DeviceProfile = rubezh2op3DeviceProfile;
    }

    @Override
    protected void stopAllStateMachines(long currentTimeMs) {
        super.stopAllStateMachines(currentTimeMs);
        externalRequestHandlers.clear();
        externalRequestHandlersByAnyDeviceId.clear();
        externalRequestHandlersByRegionId.clear();
        externalRequestHandlersByScenarioId.clear();
    }

    /**
     * Связать обработчик внешних запросов с идентификатором устройства
     */
    public void bindExternalRequestHandlerToDeviceId(String deviceId,
            Rubezh2OP3ExternalRequestHandler externalRequestHandler) {
        externalRequestHandlersByAnyDeviceId.put(deviceId, externalRequestHandler);
    }

    /**
     * Связать обработчик внешних запросов с идентификатором зоны
     */
    public void bindExternalRequestHandlerToRegionId(String regionId,
            Rubezh2OP3ExternalRequestHandler externalRequestHandler) {
        if (!externalRequestHandlersByRegionId.containsKey(regionId)) {
            externalRequestHandlersByRegionId.put(regionId, new ArrayList<>());
        }
        externalRequestHandlersByRegionId.get(regionId).add(externalRequestHandler);
    }

    /**
     * Связать обработчик внешних запросов с идентификатором сценария
     */
    public void bindExternalRequestHandlerToScenarioId(String scenarioId,
                                                     Rubezh2OP3ExternalRequestHandler externalRequestHandler) {
        if (!externalRequestHandlersByScenarioId.containsKey(scenarioId)) {
            externalRequestHandlersByScenarioId.put(scenarioId, new ArrayList<>());
        }
        externalRequestHandlersByScenarioId.get(scenarioId).add(externalRequestHandler);
    }

    /**
     * Получить список устройств - непосредственных предков
     * 
     * @param device
     *            корневое устройство, чьи предки нужны
     * @return список устройств
     */
    private void fetchChildDevices(ActiveDevice device, ConnectionInterfaceType connectionInterfaceType, List<ActiveDevice> childDevices) {
        for (ActiveDevice childDevice: inServiceDevicesToSet) {
            String parentDeviceId = childDevice.getParentDeviceId();
            if (parentDeviceId != null && parentDeviceId.equals(device.getId())) {
                DeviceProfile deviceProfile = supportedDeviceProfiles.get(childDevice.getDeviceProject().getDeviceProfileId());
                if (deviceProfile != null && deviceProfile.getConnectionTypes().contains(connectionInterfaceType)) {
                    if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER)
                        fetchChildDevices(childDevice, connectionInterfaceType, childDevices);
                    childDevices.add(childDevice);
                }
            }
        }
    }

    /**
     * Получить список конечных автоматов для приборов Рубеж-2ОП3, подключенных
     * через МС
     */
    private void fetchChildRubezh2op3StateMachines(ActiveDevice amodDevice, StateMachineContext amodStateMachineContext,
            List<ActiveDevice> controlDevices, Set<String> engagedInScenarioActionsDeviceIds,
            List<StateMachine> stateMachines) {
        for (ActiveDevice controlDevice : controlDevices) {
            /* создаем транспорт для подключения через МС */
            RubezhAdapterModuleTransport transport =
                    new RubezhAdapterModuleTransport(amodStateMachineContext.getLineNo());
            transport.setStateMachineContext(amodStateMachineContext);
            /* создаем кодек сообщений */
            Rubezh2OP3MessageCodec amod2op3MessageCodec = new RSR3DestinationViaAdapterModuleDecorator(logger, amodMessageCodec,
                    rubezh2op3MessageCodec,
                    (controlDevice.getDeviceProject().getLineNo() == 1 ? DestinationType.LINE1 : DestinationType.LINE2),
                    controlDevice.getDeviceProject().getLineAddress());
            /* создаем контекст конечного автомата */
            Rubezh2OP3StateMachineContext rubezh2op3StateMachineContext = new Rubezh2OP3StateMachineContextImpl(logger,
                    driverProfile,
                    driverProfileViews,
                    rubezh2op3DeviceProfile,
                    supportedDeviceProfiles,
                    driverAlertSender,
                    transport,
                    amod2op3MessageCodec,
                    childDeviceConfigCodec,
                    new MultiClientClock(),
                    devicesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDevicesByChildDeviceIds,
                    externalExecutiveDevicesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    engagedInScenarioActionsDeviceIds,
                    allDevicesByIds,
                    regionsByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    scenariosByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    externalScenariosByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDeviceIdsByScenarioIds,
                    virtualStatesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDevice,
                    new Rubezh2OP3StateMachineHooksImpl(),
                    getLicenseReader());
            /* Добавляем обработчик внешних запросов к устройствам */
            addExternalRequestHandler(controlDevice, rubezh2op3StateMachineContext);
            /* добавляем конечный автомат в общий список */
            stateMachines.add(new Rubezh2OP3StateMachineImpl(rubezh2op3StateMachineContext));
        }
    }

    private void addExternalRequestHandler(ActiveDevice controlDevice,
            Rubezh2OP3StateMachineContext rubezh2op3StateMachineContext) {
        Rubezh2OP3ExternalRequestHandler externalRequestHandler = new Rubezh2OP3ExternalRequestHandlerImpl(
                rubezh2op3StateMachineContext);
        externalRequestHandlers.add(externalRequestHandler);
        bindExternalRequestHandlerToDeviceId(controlDevice.getId(), externalRequestHandler);
        if (devicesByControlDeviceIds.containsKey(controlDevice.getId())) {
            for (ActiveDevice activeDevice : devicesByControlDeviceIds.get(controlDevice.getId()))
                bindExternalRequestHandlerToDeviceId(activeDevice.getId(), externalRequestHandler);
        }
        if (regionsByControlDeviceIds.containsKey(controlDevice.getId())) {
            for (ActiveRegion activeRegion : regionsByControlDeviceIds.get(controlDevice.getId()))
                bindExternalRequestHandlerToRegionId(activeRegion.getId(), externalRequestHandler);
        }
        if (scenariosByControlDeviceIds.containsKey(controlDevice.getId())) {
            for (ActiveScenario activeScenario : scenariosByControlDeviceIds.get(controlDevice.getId()))
                bindExternalRequestHandlerToScenarioId(activeScenario.getId(), externalRequestHandler);
        }
    }

    private interface ScenarioChecker {
        boolean check(ActiveScenario scenario, List<ActivationValidateMessage> validateMessages);
    }

    private interface ScenarioOnControlDeviceChecker {
        boolean check(ActiveScenario scenario, String controlDeviceId, List<ActivationValidateMessage> validateMessages);
    }

    private interface ScenarioTLBlockChecker {
        boolean check(ActiveScenario scenario, ScenarioTimeLineBlock tlBlock, List<ActivationValidateMessage> validateMessages);
    }

    private interface ScenarioOnControlDeviceTLBlockChecker {
        boolean check(ActiveScenario scenario, ScenarioTimeLineBlock tlBlock, String scenarioControlDeviceId,
                Set<String> scenarioControlDeviceIds,
                List<ActivationValidateMessage> validateMessages);
    }

    private interface ScenarioActionChecker {
        boolean check(ActiveScenario scenario, Action action, List<ActivationValidateMessage> validateMessages);
    }

    private interface ScenarioOnControlDeviceActionChecker {
        boolean check(ActiveScenario scenario, Action action, String scenarioControlDeviceId,
                List<ActivationValidateMessage> validateMessages);
    }

    private List<ScenarioChecker> scenarioCheckers = new ArrayList<>();
    private List<ScenarioOnControlDeviceChecker> scenarioOnControlDeviceCheckers = new ArrayList<>();
    private List<ScenarioTLBlockChecker> scenarioTLBlockCheckers = new ArrayList<>();
    private List<ScenarioOnControlDeviceTLBlockChecker> scenarioOnControlDeviceTLBlockCheckers = new ArrayList<>();
    private List<ScenarioActionChecker> scenarioActionCheckers = new ArrayList<>();
    private List<ScenarioOnControlDeviceActionChecker> scenarioOnControlDeviceActionCheckers = new ArrayList<>();

    private boolean checkTacticsScenarioContainsSublogics(ActiveScenario scenario,
            List<ActivationValidateMessage> validateMessages) {
        ScenarioLogicBlock startLogic = scenario.getScenarioProject().getStartLogic();
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && !startLogic.getSubLogics().isEmpty()) {
            validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.TACTICS_SCENARIO_HAS_SUBLOGICS,
                    EntityType.SCENARIO, scenario.getId()));
            return false;
        }
        return true;
    }

    private boolean checkTacticsScenarioContainsStopLogic(ActiveScenario scenario,
            List<ActivationValidateMessage> validateMessages) {
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && scenario.getScenarioProject().getStopLogic() != null) {
            validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.STOP_LOGIC_IS_IGNORED, EntityType.SCENARIO,
                    scenario.getId()));
        }
        return true;
    }

    private boolean checkTacticsScenarioContainsMoreThenOneTLBlock(ActiveScenario scenario,
            List<ActivationValidateMessage> validateMessages) {
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && scenario.getScenarioProject().getTimeLineBlocks().size() > 1) {
            validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.TACTICS_SCENARIO_HAS_MULTIPLE_TLBLOCKS,
                    EntityType.SCENARIO, scenario.getId()));
        }
        return true;
    }

    private boolean checkTacticsScenarioExecutiveBlockContainsDelay(ActiveScenario scenario,
            ScenarioTimeLineBlock tlBlock,
            List<ActivationValidateMessage> validateMessages) {
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && tlBlock.getTimeDelaySec() != 0) {
            validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.EXECUTIVE_BLOCK_HAS_DELAY, EntityType.SCENARIO,
                    scenario.getId()));
        }
        return true;
    }

    private boolean checkTacticsScenarioContainsNonExecutiveBlock(ActiveScenario scenario,
            ScenarioTimeLineBlock tlBlock,
            List<ActivationValidateMessage> validateMessages) {
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && tlBlock.getBlockType() != BlockType.EXECUTIVE) {
            validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.TACTICS_SCENARIO_HAS_NONEXECUTIVE_BLOCKS,
                    EntityType.SCENARIO, scenario.getId()));
        }
        return true;
    }

    private boolean checkTacticsScenarioExecutiveBlockContainsCondition(ActiveScenario scenario,
            ScenarioTimeLineBlock tlBlock, List<ActivationValidateMessage> validateMessages) {
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && tlBlock.getBlockType() == BlockType.EXECUTIVE && tlBlock.getConditionType() != ConditionType.NONE) {
            validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.EXECUTIVE_BLOCK_HAS_CONDITION,
                    EntityType.SCENARIO, scenario.getId()));
        }
        return true;
    }

    private boolean checkScenarioTracingRefersNonLocalObject(ActiveScenario scenario, ScenarioTimeLineBlock tlBlock,
            String scenarioControlDeviceId, Set<String> scenarioControlDeviceIds,
            List<ActivationValidateMessage> validateMessages) {
        if (tlBlock.getBlockType() == BlockType.TRACING) {
            String tracingControlDeviceId = null;
            if (tlBlock.getTracingEntityType() == TracingEntityType.SENSOR_DEVICE
                    && controlDevicesByChildDeviceIds.containsKey(tlBlock.getTracingEntityId())) {
                tracingControlDeviceId = controlDevicesByChildDeviceIds.get(tlBlock.getTracingEntityId()).getId();
            }
            if (tlBlock.getTracingEntityType() == TracingEntityType.VIRTUAL_STATE
                    && allVirtualStatesByIds.containsKey(tlBlock.getTracingEntityId())) {
                tracingControlDeviceId = allVirtualStatesByIds.get(tlBlock.getTracingEntityId()).getDeviceId();
            }

            if (scenarioControlDeviceIds.size() > 1 || tracingControlDeviceId == null
                    || !tracingControlDeviceId.equals(scenarioControlDeviceId)) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_TRACING_REFERS_NONLOCAL_OBJECT,
                        EntityType.SCENARIO, scenario.getId()));
                return false;
            }
        }
        return true;
    }

    private boolean checkScenarionActionConditionRefersNonLocalObject(ActiveScenario scenario, ScenarioTimeLineBlock tlBlock,
            String scenarioControlDeviceId, Set<String> scenarioControlDeviceIds,
            List<ActivationValidateMessage> validateMessages) {
        if (tlBlock.getBlockType() == BlockType.EXECUTIVE && tlBlock.getConditionType() != ConditionType.NONE) {
            String conditionControlDeviceId = null;
            if (tlBlock.getConditionType() == ConditionType.DEVICE_STATE
                    && controlDevicesByChildDeviceIds.containsKey(tlBlock.getConditionEntityId())) {
                conditionControlDeviceId = controlDevicesByChildDeviceIds.get(tlBlock.getConditionEntityId()).getId();
            }
            if (tlBlock.getConditionType() == ConditionType.VIRTUAL_STATE
                    && allVirtualStatesByIds.containsKey(tlBlock.getConditionEntityId())) {
                conditionControlDeviceId = allVirtualStatesByIds.get(tlBlock.getConditionEntityId()).getDeviceId();
            }

            if (scenarioControlDeviceIds.size() > 1 || conditionControlDeviceId == null
                    || !conditionControlDeviceId.equals(scenarioControlDeviceId)) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR,
                        Code.SCENARIO_EXEC_CONDITION_REFERS_NONLOCAL_OBJECT, EntityType.SCENARIO, scenario.getId()));
                return false;
            }
        }
        return true;
    }

    private boolean checkScenarioActionRefersNonCommunicatingVirtualState(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {
        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.VIRTUAL_STATE)
            return true;
        VirtualState virtualState = allVirtualStatesByIds.get(action.getEntityId());
        if (virtualState == null)
            return true;
        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        ActiveDevice virtualStateControlDevice = allDevicesByIds.get(virtualState.getDeviceId());
        if (scenarioControlDevice == null || virtualStateControlDevice == null)
            return true;
        if (scenarioControlDevice == virtualStateControlDevice)
            return true;
        if (checkControlDevicesAreCommunicating(scenarioControlDevice, virtualStateControlDevice))
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONCOMMUNICATING_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersUnknownVirtualState(ActiveScenario scenario, Action action,
            List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.VIRTUAL_STATE)
            return true;

        if (allVirtualStatesByIds.get(action.getEntityId()) != null)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_UNKNOWN_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersUnknownScenario(ActiveScenario scenario, Action action,
            List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.SCENARIO)
            return true;

        Set<String> invokingScenarioControlDeviceIds = controlDeviceIdsByScenarioIds.get(action.getEntityId());
        if (!invokingScenarioControlDeviceIds.isEmpty() && allScenariosByIds.get(action.getEntityId()) != null)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_UNKNOWN_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersSharedScenario(ActiveScenario scenario, Action action,
            List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.SCENARIO)
            return true;

        Set<String> invokingScenarioControlDeviceIds = controlDeviceIdsByScenarioIds.get(action.getEntityId());

        if (invokingScenarioControlDeviceIds.size() <= 1)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_SHARED_SCENARIO,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersUnknownDevice(ActiveScenario scenario, Action action,
            List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.DEVICE)
            return true;

        ActiveDevice actionControlDevice = controlDevicesByChildDeviceIds.get(action.getEntityId());
        if (actionControlDevice != null)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_UNKNOWN_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersNonInvokableScenario(ActiveScenario scenario, Action action,
            List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.SCENARIO)
            return true;

        ActiveScenario actionScenario = allScenariosByIds.get(action.getEntityId());
        if (actionScenario == null)
            return true;

        if (actionScenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_INVOKE)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONINVOKABLE_SCENARIO,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersNonCommunicatingScenario(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.SCENARIO)
            return true;

        Set<String> invokingScenarioControlDeviceIds = controlDeviceIdsByScenarioIds.get(action.getEntityId());
        if (invokingScenarioControlDeviceIds == null)
            return true;

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        boolean valid = true;
        for (String invokingScenarioControlDeviceId : invokingScenarioControlDeviceIds) {
            if (!invokingScenarioControlDeviceId.equals(scenarioControlDeviceId)) {
                ActiveDevice invokingScenarioControlDevice = allDevicesByIds.get(invokingScenarioControlDeviceId);
                assert invokingScenarioControlDevice != null : "Список идентификаторов приборов по идентификаторам сценариев формируется в вызывающем коде, идентификаторы должны быть актуальными";

                if (!checkControlDevicesAreCommunicating(scenarioControlDevice, invokingScenarioControlDevice)) {
                    validateMessages
                            .add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONCOMMUNICATING_OBJECT,
                                    EntityType.SCENARIO, scenario.getId()));
                    valid = false;
                }
            }
        }
        return valid;
    }

    private boolean checkScenarioActionRefersNonCommunicatingRegion(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.REGION)
            return true;

        List<ActiveDevice> regionControlDevices = controlDevicesByRegionIds.get(action.getEntityId());
        if (regionControlDevices == null)
            return true;

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        boolean valid = true;
        for (ActiveDevice regionControlDevice : regionControlDevices) {
            if (scenarioControlDevice != regionControlDevice
                    && !checkControlDevicesAreCommunicating(scenarioControlDevice, regionControlDevice)) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR,
                        Code.SCENARIO_ACTION_REFERS_NONCOMMUNICATING_OBJECT, EntityType.SCENARIO, scenario.getId()));
                valid = false;
            }
        }
        return valid;
    }

    private boolean checkScenarioActionRefersNonLocalRegion(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.REGION)
            return true;

        List<ActiveDevice> regionControlDevices = controlDevicesByRegionIds.get(action.getEntityId());
        if (regionControlDevices != null && regionControlDevices.size() == 1
                && regionControlDevices.get(0).getId().equals(scenarioControlDeviceId))
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONLOCAL_REGION,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkTacticsScenarioActionRefersNonLocalObject(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        if (scenario.getScenarioProject().getScenarioPurpose() != ScenarioPurpose.EXEC_BY_TACTICS)
            return true;

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.DEVICE)
            return true;

        String actionControlDeviceId = null;
        switch (actionType.getEntityType()) {
        case DEVICE:
        ActiveDevice actionControlDevice = controlDevicesByChildDeviceIds.get(action.getEntityId());
            if (actionControlDevice != null) {
                actionControlDeviceId = actionControlDevice.getId();
            }
            break;

        case REGION:
            List<ActiveDevice> regionControlDevices = controlDevicesByRegionIds.get(action.getEntityId());
            if (regionControlDevices != null && regionControlDevices.size() == 1) {
                actionControlDeviceId = regionControlDevices.get(0).getId();
            }
            break;

        case VIRTUAL_STATE:
            VirtualState virtualState = allVirtualStatesByIds.get(action.getEntityId());
            if (virtualState != null) {
                actionControlDeviceId = virtualState.getDeviceId();
            }
            break;

        case SCENARIO:
            Set<String> scenarioControlDeviceIds = controlDeviceIdsByScenarioIds.get(action.getEntityId());
            if (scenarioControlDeviceIds != null && scenarioControlDeviceIds.size() == 1) {
                actionControlDeviceId = scenarioControlDeviceIds.iterator().next();
            }
            break;

        default:
            break;
        }

        if (actionControlDeviceId != null && scenarioControlDeviceId.equals(actionControlDeviceId))
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.TACTICS_SCENARIO_ACTION_REFERS_NONLOCAL_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersNonCommunicatingDevice(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.DEVICE)
            return true;

        ActiveDevice actionControlDevice = controlDevicesByChildDeviceIds.get(action.getEntityId());
        if (actionControlDevice == null)
            return true;

        if (scenarioControlDeviceId.equals(actionControlDevice.getId()))
            return true;

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        if (checkControlDevicesAreCommunicating(scenarioControlDevice, actionControlDevice))
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONCOMMUNICATING_OBJECT,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    private boolean checkScenarioActionRefersNonLocalNonExecutiveDevice(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, List<ActivationValidateMessage> validateMessages) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.DEVICE)
            return true;

        ActiveDevice actionControlDevice = controlDevicesByChildDeviceIds.get(action.getEntityId());
        if (actionControlDevice == null)
            return true;

        if (scenarioControlDeviceId.equals(actionControlDevice.getId()))
            return true;

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        if (!checkControlDevicesAreCommunicating(scenarioControlDevice, actionControlDevice))
            return true;

        ActiveDevice actionDevice = allDevicesByIds.get(action.getEntityId());
        assert actionDevice != null : "Если прибор у устройства найден, то и само устройство должно быть найдено";

        if (actionDevice.getDeviceProject().getDeviceCategory() == DeviceCategory.EXECUTIVE)
            return true;

        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.SCENARIO_ACTION_REFERS_NONLOCAL_NONEXEC_DEVICE,
                EntityType.SCENARIO, scenario.getId()));
        return false;
    }

    void initScenarioCheckers() {
        scenarioCheckers.add(this::checkTacticsScenarioContainsSublogics);
        scenarioCheckers.add(this::checkTacticsScenarioContainsStopLogic);
        scenarioCheckers.add(this::checkTacticsScenarioContainsMoreThenOneTLBlock);

        scenarioTLBlockCheckers.add(this::checkTacticsScenarioContainsNonExecutiveBlock);
        scenarioTLBlockCheckers.add(this::checkTacticsScenarioExecutiveBlockContainsDelay);
        scenarioTLBlockCheckers.add(this::checkTacticsScenarioExecutiveBlockContainsCondition);

        scenarioActionCheckers.add(this::checkScenarioActionRefersUnknownVirtualState);
        scenarioActionCheckers.add(this::checkScenarioActionRefersUnknownScenario);
        scenarioActionCheckers.add(this::checkScenarioActionRefersSharedScenario);
        scenarioActionCheckers.add(this::checkScenarioActionRefersUnknownDevice);
        scenarioActionCheckers.add(this::checkScenarioActionRefersNonInvokableScenario);

        scenarioOnControlDeviceTLBlockCheckers.add(this::checkScenarioTracingRefersNonLocalObject);
        scenarioOnControlDeviceTLBlockCheckers.add(this::checkScenarionActionConditionRefersNonLocalObject);

        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonCommunicatingVirtualState);
        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonCommunicatingScenario);
        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonLocalRegion);
        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonCommunicatingRegion);
        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonCommunicatingDevice);
        scenarioOnControlDeviceActionCheckers.add(this::checkScenarioActionRefersNonLocalNonExecutiveDevice);
        scenarioOnControlDeviceActionCheckers.add(this::checkTacticsScenarioActionRefersNonLocalObject);
    }

    /**
     * Найти ошибки и получить нужные наборы сущностей с необходимыми
     * группировками.
     * 
     * @param engagedInScenarioActionsDeviceIds [OUT] глобальное на весь проект множество идентификаторов устройств,
     *  задействованных в действиях исполнительных блоков сценариев
     *  
     * @param validateMessages [OUT] сообщения валидации (дополняемый список)
     * 
     * @return true если не было нйдено критичных ошибок, иначе false
     */
    private boolean validateAndGetScenariosByControlDevices(Set<String> engagedInScenarioActionsDeviceIds,
            List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;

        /*
         * Получим для каждого сценария множество приборов (идентификаторов), к
         * которым он относится по логике запуска/остановки
         */
        valid &= getControlDeviceIdsByScenarioIds(inServiceScenariosToSet, validateMessages);

        for (ActiveScenario scenario : inServiceScenariosToSet) {
            /* Поиск ошибок сценариев без привязки к приборам */
            for (ScenarioChecker scenarioChecker : scenarioCheckers) {
                valid &= scenarioChecker.check(scenario, validateMessages);
            }

            /* Поиск ошибок временных блоков сценариев без привязки к приборам */
            for (ScenarioTimeLineBlock tlBlock : scenario.getScenarioProject().getTimeLineBlocks()) {
                for (ScenarioTLBlockChecker scenarioTLBlockChecker : scenarioTLBlockCheckers) {
                    valid &= scenarioTLBlockChecker.check(scenario, tlBlock, validateMessages);
                }
                /* Поиск ошибок действий исполнительных блоков сценариев */
                if (tlBlock.getBlockType() == BlockType.EXECUTIVE) {
                    for (Action action : tlBlock.getActions()) {
                        for (ScenarioActionChecker scenarioActionChecker : scenarioActionCheckers) {
                            valid &= scenarioActionChecker.check(scenario, action, validateMessages);
                        }
                    }
                }
            }

            Set<String> scenarioControlDeviceIds = controlDeviceIdsByScenarioIds.get(scenario.getId());
            for (String scenarioControlDeviceId : scenarioControlDeviceIds) {
                if (!scenariosByControlDeviceIds.containsKey(scenarioControlDeviceId)) {
                    scenariosByControlDeviceIds.put(scenarioControlDeviceId, new ArrayList<>());
                }
                scenariosByControlDeviceIds.get(scenarioControlDeviceId).add(scenario);

                Set<String> externalExecutiveDeviceIds = new HashSet<>();
                Set<String> externalScenarioIds = new HashSet<>();

                /* Поиск ошибок сценариев в контексте принадлежности их приборам */
                for (ScenarioOnControlDeviceChecker scenarioOnControlDeviceChecker : scenarioOnControlDeviceCheckers) {
                    valid &= scenarioOnControlDeviceChecker.check(scenario, scenarioControlDeviceId, validateMessages);
                }

                for (ScenarioTimeLineBlock tlBlock : scenario.getScenarioProject().getTimeLineBlocks()) {
                    /* Поиск ошибок временных блоков сценариев в контексте принадлежности их приборам */
                    for (ScenarioOnControlDeviceTLBlockChecker scenarioOnControlDeviceTLBlockChecker : scenarioOnControlDeviceTLBlockCheckers) {
                        valid &= scenarioOnControlDeviceTLBlockChecker.check(scenario, tlBlock, scenarioControlDeviceId,
                                scenarioControlDeviceIds, validateMessages);

                    }
                    if (tlBlock.getBlockType() == BlockType.EXECUTIVE) {
                        for (Action action : tlBlock.getActions()) {

                            rememberEngagedDevice(scenario, action, scenarioControlDeviceId,
                                    engagedInScenarioActionsDeviceIds, externalExecutiveDeviceIds);
                            rememberEngagedScenario(scenario, action, scenarioControlDeviceId, externalScenarioIds);
                            rememberEngagedVirtualState(scenario, action, scenarioControlDeviceId);

                            /*
                             * Поиск ошибок действий исполнительных блоков сценариев в контексте принадлежности приборам
                             */
                            for (ScenarioOnControlDeviceActionChecker scenarioOnControlDeviceActionChecker : scenarioOnControlDeviceActionCheckers) {
                                valid &= scenarioOnControlDeviceActionChecker.check(scenario, action,
                                        scenarioControlDeviceId, validateMessages);
                            }
                        }
                    }
                }

                /* запоминаем внешние устройства для прибора */
                rememberControlDeviceExternalExecutives(scenarioControlDeviceId, externalExecutiveDeviceIds);
                /* запоминаем внешние сценарии для прибора */
                rememberControlDeviceExternalScenarios(scenarioControlDeviceId, externalScenarioIds);
            }
        }
        return valid;
    }

    private void rememberEngagedDevice(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId, Set<String> engagedInScenarioActionsDeviceIds,
            Set<String> externalExecutiveDeviceIds) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.DEVICE)
            return;

        ActiveDevice actionDevice = allDevicesByIds.get(action.getEntityId());
        if (actionDevice == null)
            return;

        if (actionDevice.getDeviceProject().getDeviceCategory() != DeviceCategory.EXECUTIVE)
            return;

        ActiveDevice actionControlDevice = controlDevicesByChildDeviceIds.get(action.getEntityId());
        if (actionControlDevice == null)
            return;

        if (scenarioControlDeviceId.equals(actionControlDevice.getId())) {
            engagedInScenarioActionsDeviceIds.add(action.getEntityId());
            return;
        }

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        if (checkControlDevicesAreCommunicating(scenarioControlDevice, actionControlDevice)) {
            externalExecutiveDeviceIds.add(actionDevice.getId());
            engagedInScenarioActionsDeviceIds.add(actionDevice.getId());
        }
    }

    private void rememberEngagedScenario(ActiveScenario scenario, Action action, String scenarioControlDeviceId,
            Set<String> externalScenarioIds) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.SCENARIO)
            return;

        /* Ссылка на неисполнительные сценарии не поддерживается */
        ActiveScenario invokingScenario = allScenariosByIds.get(action.getEntityId());
        if (invokingScenario.getScenarioProject().getScenarioPurpose() != ScenarioPurpose.EXEC_BY_INVOKE)
            return;

        String invokingScenarioControlDeviceId = invokingScenario.getScenarioProject().getControlDeviceId();
        if (invokingScenarioControlDeviceId == null || invokingScenarioControlDeviceId.equals(scenarioControlDeviceId))
            return;

        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        assert scenarioControlDevice != null : "Проверка, что ссылки на объекты в логиках сценария корректны, должна быть в вызывающем коде";

        ActiveDevice invokingScenarioControlDevice = allDevicesByIds.get(invokingScenarioControlDeviceId);
        assert invokingScenarioControlDevice != null : "Список идентификаторов приборов по идентификаторам сценариев формируется в вызывающем коде, идентификаторы должны быть актуальными";

        if (checkControlDevicesAreCommunicating(scenarioControlDevice, invokingScenarioControlDevice)) {
            /* запоминаем идентификатор сценария, на который ссылается действие, как внешний */
            externalScenarioIds.add(invokingScenario.getId());
        }
    }

    private void rememberEngagedVirtualState(ActiveScenario scenario, Action action,
            String scenarioControlDeviceId) {

        ScenarioActionType actionType = actionTypesByIds.get(action.getActionTypeId());
        if (actionType == null || actionType.getEntityType() != ActionEntityType.VIRTUAL_STATE)
            return;
        VirtualState virtualState = allVirtualStatesByIds.get(action.getEntityId());
        if (virtualState == null)
            return;
        ActiveDevice scenarioControlDevice = allDevicesByIds.get(scenarioControlDeviceId);
        ActiveDevice virtualStateControlDevice = allDevicesByIds.get(virtualState.getDeviceId());
        if (scenarioControlDevice == null || virtualStateControlDevice == null)
            return;
        if (scenarioControlDevice == virtualStateControlDevice)
            return;
        if (!checkControlDevicesAreCommunicating(scenarioControlDevice, virtualStateControlDevice))
            return;

        if (!virtualStatesByControlDeviceIds.containsKey(scenarioControlDevice.getId())) {
            virtualStatesByControlDeviceIds.put(scenarioControlDevice.getId(), new ArrayList<>());
        }
        virtualStatesByControlDeviceIds.get(scenarioControlDevice.getId()).add(virtualState);
    }

    private void rememberControlDeviceExternalScenarios(String scenarioControlDeviceId,
            Set<String> externalScenarioIds) {
        if (!externalScenariosByControlDeviceIds.containsKey(scenarioControlDeviceId)) {
            externalScenariosByControlDeviceIds.put(scenarioControlDeviceId, new ArrayList<>());
        }
        List<ActiveScenario> externalScenarios = externalScenariosByControlDeviceIds
                .get(scenarioControlDeviceId);
        for (String externalScenarioId : externalScenarioIds) {
            externalScenarios.add(allScenariosByIds.get(externalScenarioId));
        }
    }

    private void rememberControlDeviceExternalExecutives(String scenarioControlDeviceId,
            Set<String> externalExecutiveDeviceIds) {
        if (!externalExecutiveDevicesByControlDeviceIds.containsKey(scenarioControlDeviceId)) {
            externalExecutiveDevicesByControlDeviceIds.put(scenarioControlDeviceId, new ArrayList<>());
        }
        List<ActiveDevice> externalExecutiveDevices = externalExecutiveDevicesByControlDeviceIds
                .get(scenarioControlDeviceId);
        for (String externalExecutiveDeviceId : externalExecutiveDeviceIds) {
            externalExecutiveDevices.add(allDevicesByIds.get(externalExecutiveDeviceId));
        }
    }

    private class BooleanKeeper {
        private boolean value = false;

        public boolean getValue() {
            return value;
        }

        public void setValue(boolean value) {
            this.value = value;
        }

    }

    private boolean getControlDeviceIdsByScenarioIds(List<ActiveScenario> allScenarios,
            List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (ActiveScenario scenario : allScenarios) {
            Set<String> controlDeviceIds = new HashSet<>();
            BooleanKeeper hasAndLogic = new BooleanKeeper();

            if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_LOGIC
                    || scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS) {
                valid &= getControlDeviceIdsFromScenarioStartLogic(scenario, controlDeviceIds,
                        hasAndLogic, validateMessages);

                if (scenario.getScenarioProject().getStopType() == StopType.BY_LOGIC) {
                    valid &= getControlDeviceIdsFromScenarioStopLogic(scenario, controlDeviceIds, hasAndLogic,
                            validateMessages);
                }
                if (controlDeviceIds.size() > 1) {
                    Iterator<String> controlDeviceIdsIt = controlDeviceIds.iterator();
                    String firstControlDeviceId = controlDeviceIdsIt.next();
                    ActiveDevice firstControlDevice = allDevicesByIds.get(firstControlDeviceId);
                    while (controlDeviceIdsIt.hasNext()) {
                        ActiveDevice otherControlDevice = allDevicesByIds.get(controlDeviceIdsIt.next());
                        if (!checkControlDevicesAreCommunicating(firstControlDevice, otherControlDevice)) {
                            validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.NONCOMMUNICATING_SCENARIO_SHARE,
                                    EntityType.SCENARIO, scenario.getId()));
                            valid = false;
                        }
                    }
                    if (hasAndLogic.getValue()) {
                        validateMessages.add(new ActivationValidateMessage(Level.ERROR,
                                Code.SHARED_BY_AND_SCENARIOS_NOT_SUPPORTED_YET, EntityType.SCENARIO, scenario.getId()));
                        valid = false;
                    }

                    if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS) {
                        validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.TACTICS_SHARED_SCENARIO,
                                EntityType.SCENARIO, scenario.getId()));
                    valid = false;
                }
                }
            } else {
                controlDeviceIds.add(scenario.getScenarioProject().getControlDeviceId());
            }
            controlDeviceIdsByScenarioIds.put(scenario.getId(), controlDeviceIds);
        }
        return valid;
    }

    private boolean getControlDeviceIdsFromScenarioStopLogic(ActiveScenario scenario, Set<String> controlDeviceIds,
            BooleanKeeper hasAndLogic, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        ScenarioLogicBlock stopLogic = scenario.getScenarioProject().getStopLogic();

        if (stopLogic == null) {
            ActivationValidateMessage message = new ActivationValidateMessage();
            message.entityType = EntityType.SCENARIO;
            message.entityId = scenario.getId();
            message.code = Code.SCENARIO_STOP_LOGIC_ABCENT;
            message.description = Code.SCENARIO_STOP_LOGIC_ABCENT.description;
            message.level = Level.ERROR;
            validateMessages.add(message);
            return false;
        }

        ScenarioTriggerType triggerType = triggerTypesByIds.get(stopLogic.getTriggerTypeId());

        if (checkLogicIsEmpty(stopLogic)) {
            ActivationValidateMessage message = new ActivationValidateMessage();
            message.entityType = EntityType.SCENARIO;
            message.entityId = scenario.getId();
            message.code = Code.SCENARIO_LOGIC_BROKEN;
            message.description = Code.SCENARIO_LOGIC_BROKEN.description;
            message.level = Level.ERROR;
            validateMessages.add(message);
            return false;
        }

        if (stopLogic.getLogicType() != LogicType.OR) {
            hasAndLogic.setValue(true);
        }

        if (triggerType != null) {
            switch (triggerType.getEntityType()) {
            case CONTROL_DEVICE: {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.SCENARIO;
                message.entityId = scenario.getId();
                message.code = Code.CONTROL_DEVICE_IN_STOP_SCENARIO_LOGIC;
                message.description = Code.CONTROL_DEVICE_IN_STOP_SCENARIO_LOGIC.description;
                message.level = Level.ERROR;
                validateMessages.add(message);
                valid = false;

                /*
                 * Несмотря на ошибку заполним набор идентификаторов приборов для
                 * проверки самих идентификаторов и чтобы не выдавались ошибки о
                 * пустой логике
                 */
                getControlDeviceIdsFromLogicControlDeviceReferences(scenario, stopLogic, controlDeviceIds,
                        validateMessages);

                break;
            }
            case DEVICE:
                valid &= getControlDeviceIdsFromLogicDeviceReferences(scenario, stopLogic,
                        controlDeviceIds, validateMessages);
                break;
            case REGION:
                valid &= getControlDeviceIdsFromLogicRegionReferences(scenario, stopLogic, controlDeviceIds,
                        validateMessages);
                break;
            case VIRTUAL_STATE:
                valid &= getControlDeviceIdsFromLogicVirtualStateReferences(scenario, stopLogic, controlDeviceIds,
                        validateMessages);
                break;
            }
        }

        if (!stopLogic.getSubLogics().isEmpty()) {
            valid &= getControlDeviceIdsFromScenarioSublogic(scenario, stopLogic.getSubLogics(),
                    controlDeviceIds, hasAndLogic, validateMessages);
        }
        return valid;
    }

    private boolean getControlDeviceIdsFromLogicControlDeviceReferences(ActiveScenario scenario,
            ScenarioLogicBlock logicBlock, Set<String> controlDeviceIds, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (String controlDeviceId : logicBlock.getEntityIds()) {
            ActiveDevice controlDevice = allDevicesByIds.get(controlDeviceId);
            if (controlDevice != null && controlDevice.getDeviceProject().getDeviceProfileId()
                    .equals(rubezh2op3DeviceProfile.getId())) {
                controlDeviceIds.add(controlDeviceId);
            } else {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.SCENARIO;
                message.entityId = scenario.getId();
                message.code = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT;
                message.description = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT.description;
                validateMessages.add(message);
                message.level = Level.ERROR;
                valid = false;
            }
        }
        return valid;
    }

    private boolean getControlDeviceIdsFromLogicVirtualStateReferences(ActiveScenario scenario,
            ScenarioLogicBlock logicBlock, Set<String> controlDeviceIds, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (String virtualStateId : logicBlock.getEntityIds()) {
            VirtualState virtualState = allVirtualStatesByIds.get(virtualStateId);
            ActiveDevice controlDevice = virtualState != null ? allDevicesByIds.get(virtualState.getDeviceId()) : null;
            if (virtualState == null || controlDevice == null) {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.SCENARIO;
                message.entityId = scenario.getId();
                message.code = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT;
                message.description = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT.description;
                message.level = Level.ERROR;
                validateMessages.add(message);
                valid = false;
            } else {
                controlDeviceIds.add(allVirtualStatesByIds.get(virtualStateId).getDeviceId());
            }
        }
        return valid;
    }

    private boolean getControlDeviceIdsFromLogicRegionReferences(ActiveScenario scenario, ScenarioLogicBlock logicBlock,
            Set<String> controlDeviceIds, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (String regionId : logicBlock.getEntityIds()) {
            List<ActiveDevice> regionControlDevices = controlDevicesByRegionIds.get(regionId);
            if (regionControlDevices == null || regionControlDevices.isEmpty()) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.EMPTY_REGION_IN_SCENARIO_LOGIC,
                        EntityType.SCENARIO, scenario.getId()));
                valid = false;
            } else {
                for (ActiveDevice controlDevice : regionControlDevices) {
                    controlDeviceIds.add(controlDevice.getId());
                }
            }
        }
        return valid;
    }

    private boolean getControlDeviceIdsFromLogicDeviceReferences(ActiveScenario scenario, ScenarioLogicBlock logicBlock,
            Set<String> controlDeviceIds, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (String deviceId : logicBlock.getEntityIds()) {
            ActiveDevice controlDevice = controlDevicesByChildDeviceIds.get(deviceId);
            if (controlDevice == null) {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.SCENARIO;
                message.entityId = scenario.getId();
                message.code = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT;
                message.description = Code.SCENARIO_LOGIC_REFERS_UNKNOWN_OBJECT.description;
                message.level = Level.ERROR;
                validateMessages.add(message);
                valid = false;
            } else {
                controlDeviceIds.add(controlDevice.getId());
            }
        }
        return valid;
    }

    private boolean checkLogicIsEmpty(ScenarioLogicBlock logic) {
        if (logic == null)
            return true;
        // пустое условие означает "скобки"
        if (logic.getTriggerTypeId() == null || logic.getTriggerTypeId().isEmpty()) {
            // "скобки" не должны быть пустыми
            if (logic.getSubLogics().isEmpty())
                return true;
            else
                for (ScenarioLogicBlock sublogic : logic.getSubLogics())
                    if (checkLogicIsEmpty(sublogic))
                        return true;
        }
        return false;
    }

    private boolean getControlDeviceIdsFromScenarioStartLogic(ActiveScenario scenario, Set<String> controlDeviceIds,
            BooleanKeeper hasAndLogic, List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        ScenarioLogicBlock startLogic = scenario.getScenarioProject().getStartLogic();

        if (startLogic == null) {
            ActivationValidateMessage message = new ActivationValidateMessage();
            message.entityType = EntityType.SCENARIO;
            message.entityId = scenario.getId();
            message.code = Code.SCENARIO_START_LOGIC_ABCENT;
            message.description = Code.SCENARIO_START_LOGIC_ABCENT.description;
            message.level = Level.ERROR;
            validateMessages.add(message);
            return false;
        }

        ScenarioTriggerType triggerType = triggerTypesByIds.get(startLogic.getTriggerTypeId());

        if (checkLogicIsEmpty(startLogic)) {
            ActivationValidateMessage message = new ActivationValidateMessage();
            message.entityType = EntityType.SCENARIO;
            message.entityId = scenario.getId();
            message.code = Code.SCENARIO_LOGIC_BROKEN;
            message.description = Code.SCENARIO_LOGIC_BROKEN.description;
            message.level = Level.ERROR;
            validateMessages.add(message);
            return false;
        }

        if (startLogic.getLogicType() != LogicType.OR) {
            hasAndLogic.setValue(true);
        }

        if (triggerType != null) {
            switch (triggerType.getEntityType()) {
            case CONTROL_DEVICE:
                if (!startLogic.getSubLogics().isEmpty()) {
                    ActivationValidateMessage message = new ActivationValidateMessage();
                    message.entityType = EntityType.SCENARIO;
                    message.entityId = scenario.getId();
                    message.code = Code.SUBLOGIC_WITH_CONTROL_DEVICE_SCENARIO_LOGIC;
                    message.description = Code.SUBLOGIC_WITH_CONTROL_DEVICE_SCENARIO_LOGIC.description;
                    message.level = Level.ERROR;
                    validateMessages.add(message);
                    valid = false;
                }
                valid &= getControlDeviceIdsFromLogicControlDeviceReferences(scenario, startLogic, controlDeviceIds,
                        validateMessages);
                break;
            case DEVICE:
                valid &= getControlDeviceIdsFromLogicDeviceReferences(scenario, startLogic,
                        controlDeviceIds, validateMessages);
                break;
            case REGION:
                valid &= getControlDeviceIdsFromLogicRegionReferences(scenario, startLogic, controlDeviceIds,
                        validateMessages);
                break;
            case VIRTUAL_STATE:
                valid &= getControlDeviceIdsFromLogicVirtualStateReferences(scenario, startLogic, controlDeviceIds,
                        validateMessages);
                break;
            }
        }

        if (!startLogic.getSubLogics().isEmpty()) {
            valid &= getControlDeviceIdsFromScenarioSublogic(scenario, startLogic.getSubLogics(), controlDeviceIds,
                    hasAndLogic, validateMessages);
        }
        return valid;
    }

    /**
     * Рекурсивное заполнение множества идентификаторов приборов, к которым
     * относится сценарий, по вложенным уровням логики
     */
    private boolean getControlDeviceIdsFromScenarioSublogic(ActiveScenario scenario, List<ScenarioLogicBlock> sublogics,
            Set<String> controlDeviceIds, BooleanKeeper hasAndLogic, List<ActivationValidateMessage> validateMessages) {
        boolean valid = true;
        for (int i = 0; i < sublogics.size(); ++i) {
            ScenarioLogicBlock sublogic = sublogics.get(i);
            ScenarioTriggerType triggerType = triggerTypesByIds.get(sublogic.getTriggerTypeId());

            if (sublogic.getLogicType() != LogicType.OR) {
                hasAndLogic.setValue(true);
            }

            if (triggerType != null) {
                switch (triggerType.getEntityType()) {
                case CONTROL_DEVICE: {
                    ActivationValidateMessage message = new ActivationValidateMessage();
                    message.entityType = EntityType.SCENARIO;
                    message.entityId = scenario.getId();
                    message.code = Code.NONTOP_CONTROL_DEVICE_SCENARIO_LOGIC;
                    message.description = Code.NONTOP_CONTROL_DEVICE_SCENARIO_LOGIC.description;
                    message.level = Level.ERROR;
                    validateMessages.add(message);
                    valid = false;

                    /*
                     * Несмотря на ошибку заполним набор идентификаторов приборов
                     * для проверки самих идентификаторов и чтобы не выдавались
                     * ошибки о пустой логике
                     */
                    getControlDeviceIdsFromLogicControlDeviceReferences(scenario, sublogic, controlDeviceIds,
                            validateMessages);

                    break;
                }
                case DEVICE:
                    valid &= getControlDeviceIdsFromLogicDeviceReferences(scenario, sublogic,
                            controlDeviceIds, validateMessages);
                    break;
                case REGION:
                    valid &= getControlDeviceIdsFromLogicRegionReferences(scenario, sublogic, controlDeviceIds,
                            validateMessages);
                    break;
                case VIRTUAL_STATE:
                    valid &= getControlDeviceIdsFromLogicVirtualStateReferences(scenario, sublogic, controlDeviceIds,
                            validateMessages);
                    break;
                }
            }

            if (!sublogic.getSubLogics().isEmpty()) {
                valid &= getControlDeviceIdsFromScenarioSublogic(scenario, sublogic.getSubLogics(), controlDeviceIds,
                        hasAndLogic, validateMessages);
            }
        }
        return valid;
    }

    /**
     * Сгруппировать зоны по приборам, которым они принадлежат, с валидацией
     * связей между приборами и зонами
     * 
     * @param validateMessages
     *            сообщения валидации
     * @return
     */
    private boolean validateAndGetRegionsByControlDevices(List<ActivationValidateMessage> validateMessages) {
        
        boolean valid = true;

        /* Множество общих зон */
        Set<String> sharedRegionIds = new HashSet<>();

        for (ActiveDevice controlDevice : allControlDevices) {
            Map<String, ActiveRegion> controlDeviceRegionsById = new HashMap<>();
            if (devicesByControlDeviceIds.containsKey(controlDevice.getId())) {
                /*
                 * Найдем все зоны прибора по наличию ссылок на них у
                 * подключенных к нему устройств
                 */
                for (ActiveDevice childDevice : devicesByControlDeviceIds.get(controlDevice.getId())) {
                    List<String> regionIds = new ArrayList<>();
                    regionIds.add(childDevice.getRegionId());
                    regionIds.add(childDevice.getDeviceProject().getDevicePropertyValues()
                            .get(DeviceProperties.RegionFromId.toString()));
                    regionIds.add(childDevice.getDeviceProject().getDevicePropertyValues()
                            .get(DeviceProperties.RegionToId.toString()));
                    for (String regionId : regionIds) {
                        if (regionId != null && !regionId.isEmpty()
                                && allRegionsByIds.containsKey(regionId)
                                && !controlDeviceRegionsById.containsKey(regionId)) {
                            controlDeviceRegionsById.put(regionId, allRegionsByIds.get(regionId));
                        }
                    }
                }
                /*
                 * Запомним прибор в карте приборов по идентификаторам зон, а
                 * также общие зоны
                 */
                for (String regionId : controlDeviceRegionsById.keySet()) {
                    if (controlDevicesByRegionIds.containsKey(regionId)) {
                        controlDevicesByRegionIds.get(regionId).add(controlDevice);
                        sharedRegionIds.add(regionId);
                    } else {
                        List<ActiveDevice> regionControlDevices = new ArrayList<>();
                        regionControlDevices.add(controlDevice);
                        controlDevicesByRegionIds.put(regionId, regionControlDevices);
                    }
                }
            }
            regionsByControlDeviceIds.put(controlDevice.getId(), new ArrayList<>(controlDeviceRegionsById.values()));
        }

        checkEmptyRegions(validateMessages);

        valid &= validateSharedRegions(sharedRegionIds, validateMessages);

        return valid;
    }

    /**
     * Валидация общих зон
     * 
     * @param sharedRegionIds
     *            идентификаторы общих зон
     * @param validateMessages
     *            список сообщений валидации для сохранения в нем новых
     *            сообщений
     * @return true - если валидация успешная, false - если есть ошибки
     */
    private boolean validateSharedRegions(Set<String> sharedRegionIds, List<ActivationValidateMessage> validateMessages) {
        boolean valid = true;
        for (String sharedRegionId : sharedRegionIds) {
            Subsystem sharedRegionSubsystem = allRegionsByIds.get(sharedRegionId).getRegionProject().getSubsystem();
            if (sharedRegionSubsystem == Subsystem.SKUD)
                continue; /* СКУД-зоны могут быть общими без каких-либо ограничений */

            if (sharedRegionSubsystem != Subsystem.FIRE) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.NONFIRE_SHARED_REGION, EntityType.REGION,
                        sharedRegionId));
                valid = false;
            } else {
                validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.FIRE_REGION_IS_SHARED, EntityType.REGION,
                        sharedRegionId));

                /* Проверяем, что все приборы - сообщающиеся */
                valid &= checkSharedRegionCommunicating(sharedRegionId,
                        controlDevicesByRegionIds.get(sharedRegionId),
                        validateMessages);
            }
        }
        return valid;
    }

    /**
     * Проверка что все приборы общей зоны сообщаются друг с другом
     * 
     * @param sharedRegionId
     *            идентификатор общей зоны
     * @param sharedRegionControlDevices
     *            приборы общей зоны
     * @param validateMessages
     *            список сообщений валидации для сохранения в нем новых
     *            сообщений
     * @return true, если все приборы сообщаются, иначе false
     */
    private boolean checkSharedRegionCommunicating(String sharedRegionId, List<ActiveDevice> sharedRegionControlDevices,
            List<ActivationValidateMessage> validateMessages) {
        boolean communicating = true;
        ActiveDevice firstControlDevice = sharedRegionControlDevices.get(0);
        for (int i = 1; i < sharedRegionControlDevices.size() && communicating; ++i) {
            ActiveDevice anotherControlDevice = sharedRegionControlDevices.get(i);
            if (!checkControlDevicesAreCommunicating(firstControlDevice, anotherControlDevice)) {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.REGION;
                message.entityId = sharedRegionId;
                message.code = Code.NON_COMMUNICATING_REGION_SHARE;
                message.description = Code.NON_COMMUNICATING_REGION_SHARE.description;
                message.level = Level.ERROR;
                validateMessages.add(message);
                communicating = false;
            }
        }
        return communicating;
    }

    private boolean checkControlDevicesAreCommunicating(ActiveDevice firstControlDevice,
            ActiveDevice secondControlDevice) {
        /*
         * Приборы сообщаются, если имеют родителя и только одного и подключены
         * на одну его линию
         */
        if (firstControlDevice.getParentDeviceId() == null || firstControlDevice.getParentDeviceId().isEmpty()
                || !firstControlDevice.getParentDeviceId().equals(secondControlDevice.getParentDeviceId())
                || firstControlDevice.getDeviceProject().getLineNo() != secondControlDevice.getDeviceProject()
                        .getLineNo()) {

            return false;
        } else {
            return true;
        }
    }

    /**
     * Проверка наличия общих зон с заполнением сообщений валидации
     * 
     * @param validateMessages
     *            список сообщений валидации для сохранения в нем новых
     *            сообщений
     * @return true если есть пустые зоны, иначе false
     */
    private boolean checkEmptyRegions(List<ActivationValidateMessage> validateMessages) {
        boolean emptyRegionsPresent = false;
        for (String regionId : allRegionsByIds.keySet()) {
            if (!controlDevicesByRegionIds.containsKey(regionId)) {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.REGION;
                message.entityId = regionId;
                message.code = Code.REGION_IS_EMPTY;
                message.description = Code.REGION_IS_EMPTY.description;
                validateMessages.add(message);
                message.level = Level.WARNING;
                emptyRegionsPresent = true;
            }
        }
        return emptyRegionsPresent;
    }

    /**
     * Получить списки устройств, сгруппированных по идентификаторам приборов, к
     * которым они подключены
     * 
     * В результат не попадут устройства, подключенные к приборам не 2ОП3.
     * 
     * @return карта идентификаторам приборов -> список устройств
     */
    private Map<String, List<ActiveDevice>> getDevicesByControlDeviceIds() {
        for (ActiveDevice device : inServiceDevicesToSet) {
            if (device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTROL) {
                List<ActiveDevice> childDevices = new ArrayList<>();
                /*
                 * Если прибор не данного драйвера (не 2ОП3), то список
                 * устройств - потомков должен остаться пустым
                 */
                if (device.getDeviceProject().getDeviceProfileId().equals(rubezh2op3DeviceProfile.getId())) {
                    fetchChildDevices(device, rubezh2op3DeviceProfile.getOutputConnectionType(), childDevices);
                    fetchChildDevices(device, ConnectionInterfaceType.INTERNAL_BUS, childDevices);
                }
                devicesByControlDeviceIds.put(device.getId(), childDevices);
            }
        }
        return devicesByControlDeviceIds;
    }

    /**
     * Получить списки приборов, сгруппированных по идентификаторам модулей
     * сопряжения, к которым они подключены.
     * 
     * @return карта идентификатор модуля сопряжения -> список приборов
     */
    private Map<String, List<ActiveDevice>> getRubezh2OP3DevicesByAmodDeviceIds() {
        Map<String, List<ActiveDevice>> rubezh2OP3DevicesByAmodDeviceIds = new HashMap<>();
        for (ActiveDevice device : inServiceDevicesToSet) {
            if (device.getParentDeviceId() == null
                    && amodDeviceProfiles.keySet().contains(device.getDeviceProject().getDeviceProfileId())) {
                List<ActiveDevice> childDevices = new ArrayList<>();
                fetchChildDevices(device, rubezh2op3DeviceProfile.getOutputConnectionType(), childDevices);
                fetchChildDevices(device, ConnectionInterfaceType.INTERNAL_BUS, childDevices);
                rubezh2OP3DevicesByAmodDeviceIds.put(device.getId(), childDevices);
            }
        }
        return rubezh2OP3DevicesByAmodDeviceIds;
    }

    /**
     * Получить список приборов 2ОП3, подключенных по USB напрямую.
     * 
     * @return список приборов 2ОП3
     */
    private List<ActiveDevice> getRootRubezh2OP3Devices() {
        List<ActiveDevice> rootRubezh2OP3Devices = new ArrayList<>();
        for (ActiveDevice device : inServiceDevicesToSet) {
            if (device.getParentDeviceId() == null
                    && device.getDeviceProject().getDeviceProfileId().equals(rubezh2op3DeviceProfile.getId())) {
                rootRubezh2OP3Devices.add(device);
            }
        }
        return rootRubezh2OP3Devices;
    }

    private boolean validateAndGetVirtualStatesByControlDevices(List<ActivationValidateMessage> validateMessages) {

        boolean valid = true;
        for (VirtualState virtualState : inServiceVirtualStatesToSet) {
            String controlDeviceId = virtualState.getDeviceId();
            ActiveDevice controlDevice = controlDeviceId != null ? allDevicesByIds.get(controlDeviceId) : null;
            if (controlDevice != null
                    && controlDevice.getDeviceProject().getDeviceProfileId().equals(rubezh2op3DeviceProfile.getId())) {
                if (!virtualStatesByControlDeviceIds.containsKey(controlDeviceId)) {
                    virtualStatesByControlDeviceIds.put(controlDeviceId, new ArrayList<>());
                }
                virtualStatesByControlDeviceIds.get(controlDeviceId).add(virtualState);
            } else {
                ActivationValidateMessage message = new ActivationValidateMessage();
                message.entityType = EntityType.VIRTUAL_STATE;
                message.entityId = virtualState.getId();
                message.code = Code.VIRTUAL_STATE_HAS_NO_CONTROL_DEVICE;
                message.description = Code.VIRTUAL_STATE_HAS_NO_CONTROL_DEVICE.description;
                validateMessages.add(message);
                message.level = Level.ERROR;
                valid = false;
            }
        }
        return valid;
    }

    @Override
    protected boolean fillMapsAndValidateProjectAndFetchStateMachines(List<ActivationValidateMessage> validateMessages,
                                                                      List<ActivationValidateMessage> skudValidateMessages,
                                                                      List<StateMachine> stateMachines) {

        boolean projectValid = true;

        /* Находим приборы, подключенные через МС */
        Map<String, List<ActiveDevice>> rubezh2OP3DevicesByAmodDeviceIds = getRubezh2OP3DevicesByAmodDeviceIds();
        /* Находим приборы, подключенные через USB */
        List<ActiveDevice> rootRubezh2OP3Devices = getRootRubezh2OP3Devices();
        /* Формируем полный набор приборов */
        allControlDevices.addAll(rootRubezh2OP3Devices);
        for (List<ActiveDevice> rubezh2OP3Devices : rubezh2OP3DevicesByAmodDeviceIds.values()) {
            allControlDevices.addAll(rubezh2OP3Devices);
        }

        /*
         * Формируем карту дочерних устройств по идентификаторам приборов, к
         * которым они подключены
         */
        devicesByControlDeviceIds = getDevicesByControlDeviceIds();
        /* И обратную карту id устройства -> прибор */
        for (String controlDeviceId : devicesByControlDeviceIds.keySet()) {
            for (ActiveDevice childDevice : devicesByControlDeviceIds.get(controlDeviceId)) {
                controlDevicesByChildDeviceIds.put(childDevice.getId(), allDevicesByIds.get(controlDeviceId));
            }
        }

        /*
         * Валидируем зоны и получаем наборы зон по принадлежности их приборам
         * (а также обратную карту - ноборы приборов по вхождению в них зон)
         */
        projectValid &= validateAndGetRegionsByControlDevices(validateMessages);

        /*
         * Валидируем виртуальные состояния и получаем наборы виртуальных
         * состояний по принадлежности их приборам
         */
        projectValid &= validateAndGetVirtualStatesByControlDevices(validateMessages);

        /*
         * Валидируем сценарии и получаем необходимые наборы сущностей с
         * необходимыми группировками
         */
        Set<String> engagedInScenarioActionsDeviceIds = new HashSet<>();
        projectValid &= validateAndGetScenariosByControlDevices(engagedInScenarioActionsDeviceIds, validateMessages);

        /*
         * Валидируем обьекты СКУД и размещаем их по приборам
         */
        WriteSkudDatabaseParams writeSkudDatabaseParams = new WriteSkudDatabaseParams(allControlDevices.stream()
                .map(ActiveDevice::getId).collect(Collectors.toList()),
                inServiceEmployeesToSet, inServiceAccessKeysToSet, inServiceWorkSchedulesToSet);
        projectValid &= validateSkudEntitiesAndSplitByControlDevices(writeSkudDatabaseParams, skudValidateMessages);

        /* Сначала создадим КА всех МС и приборов, подключенных через МС */
        for (String amodDeviceId : rubezh2OP3DevicesByAmodDeviceIds.keySet()) {
            ActiveDevice amodDevice = allDevicesByIds.get(amodDeviceId);
            DeviceProfile amodDeviceProfile =
                    amodDeviceProfiles.get(amodDevice.getDeviceProject().getDeviceProfileId());
            /* создаем первичный транспорт для подключения по USB */
            Transport<ByteBuffer> primaryTransport = new RubezhIdUSBTransport(logger,
                    hidDeviceScanner, amodMessageCodec, vendorId, productId, amodDevice.getDeviceProject()
                            .getDevicePropertyValues().get(DeviceProperties.USBSerialNumber.toString()),
                    amodDeviceProfile.getProductName());
            /* для каждой линии создаём контекст конечного автомата */
            Map<Integer, List<ActiveDevice>> controlDevicesByLineNo = rubezh2OP3DevicesByAmodDeviceIds.get(amodDeviceId)
                    .stream().collect(Collectors.groupingBy(d -> d.getDeviceProject().getLineNo()));
            if (controlDevicesByLineNo.size() > 1)
                primaryTransport = new MuxTransport<ByteBuffer>(primaryTransport);
            for (int lineNo = 1; lineNo <= amodDeviceProfile.getLineCount(); lineNo++) {
                if (!controlDevicesByLineNo.containsKey(lineNo))
                    continue;
                StateMachineContext amodStateMachineContext = new StateMachineContextImpl(logger, amodDeviceProfile,
                        supportedDeviceProfiles, driverAlertSender, primaryTransport,
                        amodMessageCodec, new SingleClientClock(), controlDevicesByLineNo.get(lineNo), amodDevice,
                        lineNo, new StateMachineHooksImpl());
                /* добавляем конечный автомат в общий список */
                stateMachines.add(new StateMachineImpl(amodStateMachineContext));
                /*
                 * Cоздаем и запоминаем конечные автоматы приборов, подключенных к
                 * данной линии МС
                 */
                fetchChildRubezh2op3StateMachines(amodDevice,
                        amodStateMachineContext,
                        controlDevicesByLineNo.get(lineNo),
                        engagedInScenarioActionsDeviceIds,
                        stateMachines);
            }
        }

        /*
         * Далее создадим КА всех приборов, подключенных непосредственно по USB
         */
        for (ActiveDevice controlDevice : rootRubezh2OP3Devices) {
            /* создаем транспорт для подключения по USB */
            Transport<ByteBuffer> transport = new RubezhNonIdUSBTransport(logger,
                    hidDeviceScanner, vendorId, productId, controlDevice.getDeviceProject().getDevicePropertyValues()
                            .get(DeviceProperties.USBSerialNumber.toString()),
                    rubezh2op3DeviceProfile.getProductName());
            /* создаем контекст конечного автомата */
            Rubezh2OP3StateMachineContext rubezh2op3StateMachineContext = new Rubezh2OP3StateMachineContextImpl(logger,
                    driverProfile,
                    driverProfileViews,
                    rubezh2op3DeviceProfile,
                    supportedDeviceProfiles,
                    driverAlertSender,
                    transport,
                    rubezh2op3OnUSBMessageCodec,
                    childDeviceConfigCodec,
                    new MultiClientClock(),
                    devicesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDevicesByChildDeviceIds,
                    externalExecutiveDevicesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    engagedInScenarioActionsDeviceIds,
                    allDevicesByIds,
                    regionsByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    scenariosByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    externalScenariosByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDeviceIdsByScenarioIds,
                    virtualStatesByControlDeviceIds.getOrDefault(controlDevice.getId(), new ArrayList<>()),
                    controlDevice,
                    new Rubezh2OP3StateMachineHooksImpl(),
                    getLicenseReader());
            addExternalRequestHandler(controlDevice, rubezh2op3StateMachineContext);
            /* добавляем конечный автомат в общий список */
            stateMachines.add(new Rubezh2OP3StateMachineImpl(rubezh2op3StateMachineContext));
        }

        return projectValid;
    }

    @Override
    public CreateIssueDriverResponse createIssue(Issue issue) {
        CreateIssueDriverResponse response = new CreateIssueDriverResponse();
        Set<Rubezh2OP3ExternalRequestHandler> requestHandlers = new HashSet<>();
        switch (issue.getAction().getEntityType()) {
        case DEVICE:
        case CONTROL_DEVICE:
            if (externalRequestHandlersByAnyDeviceId.containsKey(issue.getDeviceId())) {
                requestHandlers.add(externalRequestHandlersByAnyDeviceId.get(issue.getDeviceId()));
            }
            break;
        case REGION: {
            String regionId = issue.getParameters().getOrDefault("regionId", null).toString();
            if (regionId != null && externalRequestHandlersByRegionId.containsKey(regionId)) {
                requestHandlers.addAll(externalRequestHandlersByRegionId.get(regionId));
            }
            break;
        }
        case SCENARIO:
            Object scenarioNo = issue.getParameters().getOrDefault("scenarioNo", null);
            if (scenarioNo instanceof Integer){
                ActiveScenario scenario = allScenariosByIds.values()
                        .stream()
                        .filter(activeScenario -> activeScenario.getGlobalNo() == (int)scenarioNo).findFirst()
                            .orElse(null);
                if (scenario != null){
                    requestHandlers.addAll(externalRequestHandlersByScenarioId.get(scenario.getId()));
                }
            }
            break;
        case VIRTUAL_INDICATOR: {
            if (!issue.getParameters().containsKey("entityType") || !issue.getParameters().containsKey("entityIds"))
                break;
            EntityType entityType = EntityType.valueOf(issue.getParameters().get("entityType").toString());
            List<String> entityIds = new ArrayList<>();
            try {
                for (Object object: (List<?>)issue.getParameters().get("entityIds")) {
                    entityIds.add(String.valueOf(object));
                }
            } catch (ClassCastException ex){
                break;
            }
            if (!entityIds.isEmpty()) {
                if (entityType == EntityType.REGION) {
                    for (String regionId : entityIds) {
                        if (externalRequestHandlersByRegionId.containsKey(regionId)) {
                            requestHandlers.addAll(externalRequestHandlersByRegionId.get(regionId));
                        }
                    }
                    break;
                } else if (entityType == EntityType.DEVICE) {
                    for (String deviceId : entityIds) {
                        if (externalRequestHandlersByAnyDeviceId.containsKey(deviceId)) {
                            requestHandlers.add(externalRequestHandlersByAnyDeviceId.get(deviceId));
                        }
                    }
                    break;
                } else {
                    for (String scenarioId : entityIds) {
                        if (externalRequestHandlersByScenarioId.containsKey(scenarioId)){
                            requestHandlers.addAll(externalRequestHandlersByScenarioId.get(scenarioId));
                        }
                    }
                }
            }
            break;
        }
        default:
            switch (issue.getAction()) {
            case WRITE_SKUD_DATABASE:
                WriteSkudDatabaseParams writeSkudDatabaseParams = issue.getInjectedParameters().writeSkudDatabaseParams;

                if (writeSkudDatabaseParams == null) {
                    response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
                    response.setErrorString("Отсутствуют данные СКУД");
                    return response;
                }

                skudValidateMessages = new ArrayList<>();
                boolean skudValid = validateSkudEntitiesAndSplitByControlDevices(writeSkudDatabaseParams, skudValidateMessages);
                for (String controlDeviceId : writeSkudDatabaseParams.getControlDeviceIds()) {
                    if (externalRequestHandlersByAnyDeviceId.containsKey(controlDeviceId)) {
                        Rubezh2OP3ExternalRequestHandler requestHandler = externalRequestHandlersByAnyDeviceId
                                .get(controlDeviceId);

                        /* Отфильтруем карту "идентификатор сотрудника -> ключи доступа" для прибора */
                        Map<String, List<AccessKey>> controlDeviceAccessKeysByEmployeeIds = new HashMap<>();
                        List<Employee> controlDeviceEmployees = employeesByControlDeviceIds.get(controlDeviceId);
                        if (controlDeviceEmployees != null) {
                            Set<String> controlDeviceEmployeeIds = controlDeviceEmployees.stream()
                                    .map(employee -> employee.getId()).collect(Collectors.toSet());
                            Set<String> controlDeviceAccessKeyIds = accessKeysByControlDeviceIds.get(controlDeviceId)
                                    .stream().map(accessKey -> accessKey.getId()).collect(Collectors.toSet());
                            for (String employeeId : accessKeysByEmployeeIds.keySet()) {
                                if (controlDeviceEmployeeIds.contains(employeeId)) {
                                    List<AccessKey> controlDeviceAccessKeys = new ArrayList<>();
                                    for (AccessKey accessKey : accessKeysByEmployeeIds.get(employeeId)) {
                                        if (controlDeviceAccessKeyIds.contains(accessKey.getId())) {
                                            controlDeviceAccessKeys.add(accessKey);
                                        }
                                    }
                                    controlDeviceAccessKeysByEmployeeIds.put(employeeId, controlDeviceAccessKeys);
                                }
                            }
                        }
                        /* Обновляем БД СКУД у конечных автоматов */
                        if (requestHandler.updateVarSkudDatabase(
                                employeesByControlDeviceIds.getOrDefault(controlDeviceId, new ArrayList<>()),
                                accessKeysByControlDeviceIds.getOrDefault(controlDeviceId, new ArrayList<>()),
                                workSchedulesByControlDeviceIds.getOrDefault(controlDeviceId, new ArrayList<>()),
                                controlDeviceAccessKeysByEmployeeIds, skudValidateMessages) && skudValid) {
                            /* Добавляем обработчик, если обновление БД СКУД прошло без ощибок */
                            requestHandlers.add(requestHandler);
                        }
                    }
                }
                /* Повторно отправляем статус валидации проекта, актуализировав сообщения валидации СКУД */
                validateStatus.setValidateMessages(new ArrayList<>(commonValidateMessages));
                validateStatus.addValidateMessages(skudValidateMessages);
                driverAlertSender.send(validateStatus);

                if (!skudValid) {
                    response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
                    response.setErrorString("База данных СКУД не корректна");
                    return response;
                }

                break;
            default:
                requestHandlers.addAll(externalRequestHandlers);
                break;
            }
            break;
        }
        if (!requestHandlers.isEmpty()) {
            for (Rubezh2OP3ExternalRequestHandler requestHandler : requestHandlers) {
                requestHandler.handleMessage(issue, response);
            }
        } else {
            response.setError(ErrorType.UNKNOWN_TARGET);
            response.setErrorString("Адресат не найден");
        }
        return response;
    }

    private boolean validateSkudEntitiesAndSplitByControlDevices(WriteSkudDatabaseParams writeSkudDatabaseParams,
                                                                 List<ActivationValidateMessage> validateMessages) {
        /* Запоминаем новые наборы сущностей СКУД */
        initEmployeesHash(writeSkudDatabaseParams.getEmployees());
        initAccessKeysHash(writeSkudDatabaseParams.getAccessKeys());
        initWorkSchedulesHash(writeSkudDatabaseParams.getWorkSchedules());

        validateMessages.clear();

        Map<String, Set<String>> controlDeviceIdsByAccessKeyIds = new HashMap<>();
        Map<String, List<AccessKey>> accessKeysByEmployeeIds = new HashMap<>();
        Map<String, List<AccessKey>> accessKeysByWorkScheduleIds = new HashMap<>();
        Map<String, List<AccessKey>> accessKeysByControlDeviceIds = new HashMap<>();
        if (!validateAndGetAccessKeysByControlDevices(allEmployeesByIds, allAccessKeysByIds, allWorkSchedulesByIds,
                controlDevicesByChildDeviceIds, controlDeviceIdsByAccessKeyIds, accessKeysByEmployeeIds,
                accessKeysByWorkScheduleIds, accessKeysByControlDeviceIds, validateMessages))
            return false;
        this.accessKeysByControlDeviceIds = accessKeysByControlDeviceIds;
        this.accessKeysByEmployeeIds = accessKeysByEmployeeIds;

        Map<String, List<Employee>> employeesByControlDeviceIds = new HashMap<>();
        if (!validateAndGetEmployeesByControlDevices(allEmployeesByIds, controlDeviceIdsByAccessKeyIds,
                accessKeysByEmployeeIds, employeesByControlDeviceIds, validateMessages))
            return false;
        this.employeesByControlDeviceIds = employeesByControlDeviceIds;

        Map<String, List<WorkSchedule>> workSchedulesByControlDeviceIds = new HashMap<>();
        if (!validateAndGetWorkSchedulesByControlDevices(allWorkSchedulesByIds, controlDeviceIdsByAccessKeyIds,
                accessKeysByWorkScheduleIds, workSchedulesByControlDeviceIds, validateMessages))
            return false;
        this.workSchedulesByControlDeviceIds = workSchedulesByControlDeviceIds;

        return true;
    }

    private void initEmployeesHash(List<Employee> employees) {
        allEmployeesByIds = new HashMap<>();
        for (Employee employee : employees) {
            allEmployeesByIds.put(employee.getId(), employee);
        }
    }

    private void initAccessKeysHash(List<AccessKey> accessKeys) {
        allAccessKeysByIds = new HashMap<>();
        for (AccessKey accessKey : accessKeys) {
            allAccessKeysByIds.put(accessKey.getId(), accessKey);
        }
    }

    private void initWorkSchedulesHash(List<WorkSchedule> workSchedules) {
        allWorkSchedulesByIds = new HashMap<>();
        for (WorkSchedule workSchedule : workSchedules) {
            allWorkSchedulesByIds.put(workSchedule.getId(), workSchedule);
        }
    }

    private boolean validateAndGetAccessKeysByControlDevices(Map<String, Employee> employeesByIds, // IN
            Map<String, AccessKey> accessKeysByIds, // IN
            Map<String, WorkSchedule> workSchedulesByIds, //IN
            Map<String, ActiveDevice> controlDevicesByChildDeviceIds, // IN
            Map<String, Set<String>> controlDeviceIdsByAccessKeyIds, // OUT
            Map<String, List<AccessKey>> accessKeysByEmployeeIds, // OUT
            Map<String, List<AccessKey>> accessKeysByWorkScheduleIds, // OUT
            Map<String, List<AccessKey>> accessKeysByControlDeviceIds, // OUT
            List<ActivationValidateMessage> validateMessages// OUT
    ) {
        assert employeesByIds != null;
        assert accessKeysByIds != null;
        assert workSchedulesByIds != null;
        assert controlDevicesByChildDeviceIds != null;
        assert controlDeviceIdsByAccessKeyIds != null;
        assert accessKeysByEmployeeIds != null;
        assert accessKeysByWorkScheduleIds != null;
        assert accessKeysByControlDeviceIds != null;
        assert validateMessages != null;

        boolean valid = true;
        for (AccessKey accessKey : accessKeysByIds.values()) {
            WorkSchedule workSchedule = null;
            /* Если ключу указан свой график работ - запомним его */
            if (accessKey.getAccessMap().getWorkScheduleId() != null && !accessKey.getAccessMap().getWorkScheduleId().isEmpty()) {
                workSchedule = workSchedulesByIds.get(accessKey.getAccessMap().getWorkScheduleId());
            }
            /* Сформируем множество точек прохода, доступных ключу */
            Set<String> mergedAccessPointIds = new HashSet<>();
            mergedAccessPointIds.addAll(accessKey.getAccessMap().getAccessPointDeviceIds());
            if (accessKey.getEmployeeId() == null || accessKey.getEmployeeId().isEmpty()
                    || !employeesByIds.containsKey(accessKey.getEmployeeId())) {
                validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.ACCESS_KEY_WITHOUT_EMPLOYEE,
                        EntityType.ACCESS_KEY, accessKey.getId()));
                valid = false;
            } else {
                /* Заполним карту идентификатор сотрудника -> ключи доступа */
                Employee employee = employeesByIds.get(accessKey.getEmployeeId());
                if (accessKeysByEmployeeIds.get(employee.getId()) == null) {
                    accessKeysByEmployeeIds.put(employee.getId(), new ArrayList<>());
                }
                accessKeysByEmployeeIds.get(employee.getId()).add(accessKey);
                /* Дополним набор точек прохода ключа точками сотрудника */
                mergedAccessPointIds.addAll(employee.getAccessMap().getAccessPointDeviceIds());
                /* Если у ключа не прописан собственный график работ, то запомним график работ сотрудника */
                if (workSchedule == null && employee.getAccessMap().getWorkScheduleId() != null
                        && !employee.getAccessMap().getWorkScheduleId().isEmpty()) {
                    workSchedule = workSchedulesByIds.get(employee.getAccessMap().getWorkScheduleId());
                }
            }
            /*
             * По множеству устройств - точек доступа - сформируем множество приборов, которым должен быть известен
             * ключ.
             */
            Set<String> controlDeviceIds = new HashSet<>();
            for (String accessPointId : mergedAccessPointIds) {
                ActiveDevice controlDevice = controlDevicesByChildDeviceIds.get(accessPointId);
                if (controlDevice == null) {
                    validateMessages.add(new ActivationValidateMessage(Level.ERROR, Code.WRONG_ACCESS_POINT_REFERENCE,
                            EntityType.ACCESS_KEY, accessKey.getId()));
                    valid = false;
                } else {
                    controlDeviceIds.add(controlDevice.getId());
                }
            }
            if (!controlDeviceIds.isEmpty()) {
                /* Формируем карту идентификатор ключа -> идентификаторы приборов */
                controlDeviceIdsByAccessKeyIds.put(accessKey.getId(), controlDeviceIds);
                /* Формируем обратную карту: идентификатор прибора -> ключи доступа */
                for (String controlDeviceId : controlDeviceIds) {
                    if (accessKeysByControlDeviceIds.get(controlDeviceId) == null) {
                        accessKeysByControlDeviceIds.put(controlDeviceId, new ArrayList<>());
                    }
                    accessKeysByControlDeviceIds.get(controlDeviceId).add(accessKey);
                }
            } else {
                validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.ACCESS_KEY_WITHOUT_ANY_ACCESS_POINT,
                        EntityType.ACCESS_KEY, accessKey.getId()));
            }
            if (workSchedule != null) {
                /* Формирем карту идентификатор рабочего графика -> ключ доступа */
                if (accessKeysByWorkScheduleIds.get(workSchedule.getId()) == null) {
                    accessKeysByWorkScheduleIds.put(workSchedule.getId(), new ArrayList<>());
                }
                accessKeysByWorkScheduleIds.get(workSchedule.getId()).add(accessKey);
            } else {
                validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.ACCESS_KEY_WITHOUT_WORK_SCHEDULE,
                        EntityType.ACCESS_KEY, accessKey.getId()));
            }
        }

        return valid;
    }

    private boolean validateAndGetEmployeesByControlDevices(Map<String, Employee> employeesByIds, // IN
            Map<String, Set<String>> controlDeviceIdsByAccessKeyIds, // IN
            Map<String, List<AccessKey>> accessKeysByEmployeeIds, // IN
            Map<String, List<Employee>> employeesByControlDeviceIds, // OUT
            List<ActivationValidateMessage> validateMessages// OUT
    ) {
        assert employeesByIds != null;
        assert controlDeviceIdsByAccessKeyIds != null;
        assert accessKeysByEmployeeIds != null;
        assert employeesByControlDeviceIds != null;
        assert validateMessages != null;

        boolean valid = true;
        for (Employee employee : employeesByIds.values()) {
            List<AccessKey> employeeAccessKeys = accessKeysByEmployeeIds.get(employee.getId());
            if (employeeAccessKeys == null || employeeAccessKeys.isEmpty()) {
                validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.EMPLOYEE_WITHOUT_ACCESS_KEYS,
                        EntityType.EMPLOYEE, employee.getId()));
            } else {
                Set<String> controlDeviceIds = new HashSet<>();
                for (AccessKey accessKey : employeeAccessKeys) {
                    Set<String> accessKeyControlDeviceIds = controlDeviceIdsByAccessKeyIds.get(accessKey.getId());
                    if (accessKeyControlDeviceIds != null) {
                        controlDeviceIds.addAll(accessKeyControlDeviceIds);
                    }
                }
                if (controlDeviceIds.isEmpty()) {
                    validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.EMPLOYEE_WITHOUT_ANY_ACCESS_POINT,
                            EntityType.EMPLOYEE, employee.getId()));
                } else {
                    /* Формируем обратную карту: идентификатор прибора -> сотрудники */
                    for (String controlDeviceId : controlDeviceIds) {
                        if (employeesByControlDeviceIds.get(controlDeviceId) == null) {
                            employeesByControlDeviceIds.put(controlDeviceId, new ArrayList<>());
                        }
                        employeesByControlDeviceIds.get(controlDeviceId).add(employee);
                    }
                }
            }
        }
        return valid;
    }

    private boolean validateAndGetWorkSchedulesByControlDevices(Map<String, WorkSchedule> workSchedulesByIds, // IN
            Map<String, Set<String>> controlDeviceIdsByAccessKeyIds, // IN
            Map<String, List<AccessKey>> accessKeysByWorkScheduleIds, // IN
            Map<String, List<WorkSchedule>> workSchedulesByControlDeviceIds, // OUT
            List<ActivationValidateMessage> validateMessages// OUT
    ) {
        assert workSchedulesByIds != null;
        assert controlDeviceIdsByAccessKeyIds != null;
        assert accessKeysByWorkScheduleIds != null;
        assert workSchedulesByControlDeviceIds != null;
        assert validateMessages != null;

        boolean valid = true;
        for (WorkSchedule workSchedule : workSchedulesByIds.values()) {
            List<AccessKey> workScheduleAccessKeys = accessKeysByWorkScheduleIds.get(workSchedule.getId());
            if (workScheduleAccessKeys == null || workScheduleAccessKeys.isEmpty()) {
                validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.WORK_SCHEDULE_NOT_USED,
                        EntityType.WORK_SCHEDULE, workSchedule.getId()));
            } else {
                Set<String> controlDeviceIds = new HashSet<>();
                for (AccessKey accessKey : workScheduleAccessKeys) {
                    Set<String> accessKeyControlDeviceIds = controlDeviceIdsByAccessKeyIds.get(accessKey.getId());
                    if (accessKeyControlDeviceIds != null) {
                        controlDeviceIds.addAll(accessKeyControlDeviceIds);
                    }
                }
                if (controlDeviceIds.isEmpty()) {
                    validateMessages.add(new ActivationValidateMessage(Level.WARNING, Code.WORK_SCHEDULE_WITHOUT_ANY_ACCESS_POINT,
                            EntityType.WORK_SCHEDULE, workSchedule.getId()));
                } else {
                    /* Формируем обратную карту: идентификатор прибора -> рабочие графики */
                    for (String controlDeviceId : controlDeviceIds) {
                        if (workSchedulesByControlDeviceIds.get(controlDeviceId) == null) {
                            workSchedulesByControlDeviceIds.put(controlDeviceId, new ArrayList<>());
                        }
                        workSchedulesByControlDeviceIds.get(controlDeviceId).add(workSchedule);
                    }
                }
            }
        }
        return valid;
    }

    @Override
    public void startIssue(Issue issue) {
        Rubezh2OP3ExternalRequestHandler requestHandler = externalRequestHandlersByAnyDeviceId.get(issue.getDeviceId());
        requestHandler.startIssue(issue);
    }

    @Override
    public SetDriverDeviceConfigResponse setDeviceConfig(SetDriverDeviceConfigRequest request) {
        String deviceId = request.getDeviceId();
        if (externalRequestHandlersByAnyDeviceId.containsKey(deviceId)) {
            return externalRequestHandlersByAnyDeviceId.get(deviceId).handleMessage(request);
        } else {
            SetDriverDeviceConfigResponse response = new SetDriverDeviceConfigResponse();
            response.setError(ErrorType.UNKNOWN_DEVICE);
            response.setErrorString("Устройство не найдено: " + deviceId);
            return response;
        }
    }

    @Override
    public DeleteTaskDriverResponse deleteTask(DeleteTaskDriverRequest request) {
        DeleteTaskDriverResponse subResponse, response = new DeleteTaskDriverResponse();
        for (Rubezh2OP3ExternalRequestHandler externalRequestHandler : externalRequestHandlers) {
            subResponse = externalRequestHandler.handleMessage(request);
            response.getDeletedIssueIds().addAll(subResponse.getDeletedIssueIds());
            if (subResponse.getError() != ErrorType.OK) {
                response.setError(subResponse.getError());
                response.setErrorString(subResponse.getErrorString());
            }
        }
        return response;
    }

    @Override
    public PauseTaskDriverResponse pauseTask(PauseTaskDriverRequest request) {
        PauseTaskDriverResponse subResponse, response = new PauseTaskDriverResponse();
        for (Rubezh2OP3ExternalRequestHandler externalRequestHandler : externalRequestHandlers) {
            subResponse = externalRequestHandler.handleMessage(request);
            if (subResponse.getError() != ErrorType.OK) {
                response.setError(subResponse.getError());
                response.setErrorString(subResponse.getErrorString());
            }
        }
        return response;
    }

}
