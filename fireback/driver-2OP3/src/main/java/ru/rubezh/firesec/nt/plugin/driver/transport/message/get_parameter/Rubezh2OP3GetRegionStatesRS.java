package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.BitSet;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetRegionStatesRS extends Rubezh2OP3GetParameterRS {
    int regionNo;
    BitSet regionStateBits;

    public Rubezh2OP3GetRegionStatesRS() {
        super(Rubezh2OP3ParameterType.REGION_STATES);
    }

    public int getRegionNo() {
        return regionNo;
    }

    public void setRegionNo(int regionNo) {
        this.regionNo = regionNo;
    }

    public BitSet getRegionStateBits() {
        return regionStateBits;
    }

    public void setRegionStateBits(BitSet regionStateBits) {
        this.regionStateBits = regionStateBits;
    }

}
