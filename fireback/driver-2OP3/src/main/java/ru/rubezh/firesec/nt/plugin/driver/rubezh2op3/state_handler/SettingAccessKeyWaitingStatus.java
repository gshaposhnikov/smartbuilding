package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.SetWaitingAccessKeyStatusTask;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.Task;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class SettingAccessKeyWaitingStatus extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.SETTING_ACCESS_KEY_WAITING_STATUS;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        SettingAccessKeyWaitingStatus handler = new SettingAccessKeyWaitingStatus();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onWaitingAccessKeyStatusSet(long currentTimeMs) {
        try {
            SetWaitingAccessKeyStatusTask task = (SetWaitingAccessKeyStatusTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            if (task.isWaitingAccessKey()) {
                stateMachineContext.setWaitingAccessKeyId(task.getAccessKeyId());
            } else {
                stateMachineContext.setWaitingAccessKeyId(null);
            }
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                    IssueStatus.FINISHED, "");
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Error setting access key waiting status: {}", exception);

        Task task = stateMachineContext.getTaskManager().getCurrentTask();
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        Task task = stateMachineContext.getTaskManager().getCurrentTask();
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                IssueStatus.FAILED, "Потеря связи");

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
