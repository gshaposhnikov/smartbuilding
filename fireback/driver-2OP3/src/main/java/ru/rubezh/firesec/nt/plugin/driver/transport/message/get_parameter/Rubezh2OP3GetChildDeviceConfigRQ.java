package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetChildDeviceConfigRQ extends Rubezh2OP3GetParameterRQ {

    private int lineNo;
    private int address;

    public Rubezh2OP3GetChildDeviceConfigRQ(int lineNo, int address) {
        super(Rubezh2OP3ParameterType.CHILD_DEVICE_CONFIG);
        this.lineNo = lineNo;
        this.address = address;
    }

    public int getLineNo() {
        return lineNo;
    }

    public int getAddress() {
        return address;
    }

}
