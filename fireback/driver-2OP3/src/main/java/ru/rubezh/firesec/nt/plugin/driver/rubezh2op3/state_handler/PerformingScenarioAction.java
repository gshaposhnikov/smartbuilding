package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.PerformScenarioActionTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class PerformingScenarioAction extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.PERFORMING_SCENARIO_ACTION;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        PerformingScenarioAction handler = new PerformingScenarioAction();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onScenarioActionPerformed(long currentTimeMs) {
        try {
            PerformScenarioActionTask task = (PerformScenarioActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onScenarioActionPerformed(currentTimeMs,
                    task.getActiveScenario().getId(), task.getManageAction());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Error performing device action: {}", exception);

        try {
            PerformScenarioActionTask task = (PerformScenarioActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onScenarioActionFailed(currentTimeMs,
                    task.getActiveScenario().getId(), task.getManageAction());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            PerformScenarioActionTask task = (PerformScenarioActionTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onScenarioActionFailed(currentTimeMs,
                    task.getActiveScenario().getId(), task.getManageAction());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
