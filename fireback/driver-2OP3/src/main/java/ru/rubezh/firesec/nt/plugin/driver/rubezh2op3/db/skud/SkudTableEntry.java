package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

/**
 * Интерфейс для элемента какой-либо таблицы БД СКУД прибора.
 * 
 * @author Антон Васильев
 *
 */
public interface SkudTableEntry {
    public static final int SKUD_ENTRY_ID_SIZE = 2;
    public static final int MAX_SKUD_ENTRY_ID = 65534;
    public static final int BIT_FLAGS_SIZE = 1;
    public static final int DEFAULT_FLAGS_VALUE = 0xFF & 0xFF;
    
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream);

    default public void writeId(int id, DatabaseOutputStream stream) {
        stream.writeIntegerLittleEndian(id, SKUD_ENTRY_ID_SIZE);
    }

    default public void writeBitFlags(DatabaseOutputStream stream) {
        stream.writeInteger(DEFAULT_FLAGS_VALUE, BIT_FLAGS_SIZE);
    }
}
