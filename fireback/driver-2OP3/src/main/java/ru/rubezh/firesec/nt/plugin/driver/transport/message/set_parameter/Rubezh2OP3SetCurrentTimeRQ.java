package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import java.util.Date;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetCurrentTimeRQ extends Rubezh2OP3SetParameterRQ {

    private Date dateTime;

    public Rubezh2OP3SetCurrentTimeRQ(Date dateTime_) {
        super(Rubezh2OP3ParameterType.CURRENT_TIME);
        this.dateTime = dateTime_;
    }

    public Date getDateTime() {
        return dateTime;
    }

}
