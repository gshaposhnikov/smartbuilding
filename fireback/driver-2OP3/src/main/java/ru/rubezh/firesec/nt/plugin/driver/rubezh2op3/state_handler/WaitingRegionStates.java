package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.GetRegionStatesTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class WaitingRegionStates extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_REGION_STATES;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingRegionStates handler = new WaitingRegionStates();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates) {
        if (regionStates != null) {
            stateMachineContext.setRegionStateBits(regionId, regionStates);
            stateMachineContext.getStateMachineHooks().onRegionStatesReceived(currentTimeMs, regionId, regionStates);
        } else {
            try {
                GetRegionStatesTask currentTask = (GetRegionStatesTask) stateMachineContext.getTaskManager()
                        .getCurrentTask();
                stateMachineContext.getLogger().warn("Empty answer for region states request (region number: {})",
                        currentTask.getActiveRegion().getIndex());
            } catch (Exception e) {
                stateMachineContext.getLogger().error(e);
            }
        }
        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getStateMachineHooks().onRegionStatesReceiveFailed(currentTimeMs, exception);
        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

}
