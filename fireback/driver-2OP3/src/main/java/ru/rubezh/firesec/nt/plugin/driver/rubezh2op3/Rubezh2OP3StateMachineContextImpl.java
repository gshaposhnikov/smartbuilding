package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.nio.ByteBuffer;
import java.util.*;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseReader;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.ChildDeviceConfigCodec;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud.SkudDatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.StateHandler;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.StateRelay;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.Rubezh2OP3MessageCodec;

public class Rubezh2OP3StateMachineContextImpl implements Rubezh2OP3StateMachineContext {
    private Logger logger;

    private Transport<ByteBuffer> transport;
    private Rubezh2OP3MessageCodec messageCodec;
    private Clock clock;
    private StateHandler stateHandler;
    private Rubezh2OP3ActionExecutor actionExecutor;
    private Rubezh2OP3StateMachineHooks stateMachineHooks;
    private DriverAlertSender driverAlertSender;
    private Rubezh2OP3TaskManager taskManager;

    /* конфигурация */
    private DriverProfile driverProfile;
    private List<DriverProfileView> driverProfileViews;
    private Map<String, DeviceProfileView> deviceProfileViewsByIds;
    private DeviceProfile rubezh2op3DeviceProfile;
    private Map<String, DeviceProfile> supportedDeviceProfiles;
    private int reconnectInterval = 10000;
    private int resetTimeout = 10000;
    private int getDeviceConfigurationRetryInterval = 5000;
    private int checkEventsInterval = 1000;
    private int checkRegionHeadersInterval = 10000;
    private int checkRegionStatesInterval = 2000;
    private int checkChildDeviceStatesInterval = 5000;
    private int setTimeInterval = 12 * 60 * 60 * 1000;
    private int taskExecuteInterval = 200; // нельзя устанавливать другие интревалы меньше этого
    private int updateSoftwareModeRetry = 100;
    private int startWritingRetryTO = 100;
    private Rubezh2OP3DeviceInfo controlDeviceInfo;
    private Map<String, Long> deviceMaxEventCounters;
    private Map<String, Long> deviceEventCounters;
    private Rubezh2OP3ReadEventsState fetchingEventsState;
    private Rubezh2OP3ReadEventsState readEventsState;
    private DatabaseBuilder databaseBuilder;
    private SkudDatabaseBuilder skudDatabaseBuilder;
    private ChildDeviceConfigCodec childDeviceConfigCodec;
    private int resetProgress = 0;
    /** Максимальное кол-во накапливаемых событий */
    private int batchEventsCount = 30;
    /** Максимальное время накопления событий, мс */
    public final static int EVENTS_SEND_TIMEOUT = 1000;
    /** Размер пакетов, которыми пишется информация в прибор */
    private final static int WRITE_BLOCK_SIZE = 256;
    /** Интервал опроса режима прибора */
    private final static int MODE_CHECKING_INTERVAL = 5000;

    /* модель данных */
    private ActiveDevice rubezh2op3Device;
    private List<ActiveDevice> childDevices;
    private Map<String, ActiveDevice> controlDevicesByChildDeviceIds;
    private List<ActiveDevice> externalExecutiveChildDevices;
    private Set<String> engagedInScenarioActionsDeviceIds;
    private Map<String, ActiveDevice> allProjectDevicesByIds;
    private List<ActiveRegion> regions;
    private List<ActiveScenario> scenarios;
    private List<ActiveScenario> externalScenarios;
    private Map<String, Set<String>> controlDeviceIdsByScenarioIds;
    private List<VirtualState> virtualStates;
    private Map<String, BitSet> regionStateBitsMap = new HashMap<>();

    /* модель данных - СКУД */
    private Map<String, Employee> employeesByIds;
    private Map<String, AccessKey> accessKeysByIds;
    private Map<String, WorkSchedule> workSchedulesByIds;
    private Map<String, List<AccessKey>> accessKeysByEmployeeIds;

    /* вспомогательные данные СКУД */
    private String waitingAccessKeyId;

    /* ротация проверки состояния зон */
    private int nextRegionIndexToCheckStates = 0;

    /* ротация проверки состояния устройств */
    private Rubezh2OP3ChildDevicePollingState childDevicePollingState;

    /* текущее время ожидания завершения долговременной операции */
    private int longOperationWaitingTimeMs;

    /* состояние обновления БД */
    private Rubezh2OP3UpdatingDatabaseState updatingDatabaseState;

    /* состояние обновления ПО */
    private Rubezh2OP3UpdatingSoftwareState updatingSoftwareState;

    /* Буфер событий перед отправкой */
    private List<ControlDeviceEvent> events = new ArrayList<>();

    /* Считыватель лицензий */
    private LicenseReader licenseReader;

    /* Текущий режим прибора */
    public Rubezh2OP3UpdateSoftwareStatusType rubezh2op3DeviceMode;

    /* Состояние прибора "База отсутствует" */
    private boolean badSignature;

    public Rubezh2OP3StateMachineContextImpl(Logger logger,
            DriverProfile driverProfile,
            List<DriverProfileView> driverProfileViews,
            DeviceProfile rubezh2op3DeviceProfile,
            Map<String, DeviceProfile> supportedDeviceProfiles,
            DriverAlertSender driverAlertSender,
            Transport<ByteBuffer> transport,
            Rubezh2OP3MessageCodec messageCodec,
            ChildDeviceConfigCodec childDeviceConfigCodec,
            Clock clock,
            List<ActiveDevice> childDevices,
            Map<String, ActiveDevice> controlDevicesByChildDeviceIds,
            List<ActiveDevice> externalExecutiveChildDevices,
            Set<String> engagedInScenarioActionsDeviceIds,
            Map<String, ActiveDevice> allProjectDevicesByIds,
            List<ActiveRegion> regions,
            List<ActiveScenario> scenarios,
            List<ActiveScenario> externalScenarios,
            Map<String, Set<String>> controlDeviceIdsByScenarioIds,
            List<VirtualState> virtualStates,
            ActiveDevice rubezh2op3Device,
            Rubezh2OP3StateMachineHooks stateMachineHooks,
            LicenseReader licenseReader) {
        this.logger = logger;
        this.driverProfile = driverProfile;
        this.driverProfileViews = driverProfileViews;
        this.rubezh2op3DeviceProfile = rubezh2op3DeviceProfile;
        this.supportedDeviceProfiles = supportedDeviceProfiles;
        this.driverAlertSender = driverAlertSender;
        this.transport = transport;
        this.messageCodec = messageCodec;
        this.childDeviceConfigCodec = childDeviceConfigCodec;
        this.clock = clock;
        this.childDevices = childDevices;
        this.controlDevicesByChildDeviceIds = controlDevicesByChildDeviceIds;
        this.externalExecutiveChildDevices = externalExecutiveChildDevices;
        this.engagedInScenarioActionsDeviceIds = engagedInScenarioActionsDeviceIds;
        this.allProjectDevicesByIds = allProjectDevicesByIds;
        this.regions = regions;
        this.scenarios = scenarios;
        this.externalScenarios = externalScenarios;
        this.controlDeviceIdsByScenarioIds = controlDeviceIdsByScenarioIds;
        this.virtualStates = virtualStates;
        this.rubezh2op3Device = rubezh2op3Device;
        this.stateMachineHooks = stateMachineHooks;
        this.licenseReader = licenseReader;
        stateMachineHooks.setStateMachineContext(this);

        actionExecutor = new Rubezh2OP3ActionExecutorImpl(this);
        stateHandler = new StateRelay(this);
        taskManager = new Rubezh2OP3TaskManager(this, State.WAITING_TASK);
        fetchingEventsState = new Rubezh2OP3ReadEventsState(rubezh2op3DeviceProfile.getLogTypes());
        readEventsState = new Rubezh2OP3ReadEventsState(rubezh2op3DeviceProfile.getLogTypes());
        databaseBuilder = new DatabaseBuilder(this);
        skudDatabaseBuilder = new SkudDatabaseBuilder(this);
        childDevicePollingState = new Rubezh2OP3ChildDevicePollingState(childDevices);
        updatingDatabaseState = new Rubezh2OP3UpdatingDatabaseState();
        updatingSoftwareState = new Rubezh2OP3UpdatingSoftwareState();

        deviceProfileViewsByIds = new HashMap<>();
        if (driverProfileViews != null && !driverProfileViews.isEmpty()) {
            // TODO: Выбор нужного отображения
            DriverProfileView driverProfileView = driverProfileViews.get(0);
            List<DeviceProfileView> deviceProfileViews = driverProfileView.getDeviceProfileViews();
            for (DeviceProfileView deviceProfileView : deviceProfileViews) {
                deviceProfileViewsByIds.put(deviceProfileView.getId(), deviceProfileView);
            }
        }

        badSignature = rubezh2op3Device.getActiveStates().contains(rubezh2op3DeviceProfile.getBadSignatureStateId());
        deviceMaxEventCounters = getDefaultDeviceMaxEventCounters();
    }

    public void setReconnectInterval(int reconnectInterval) {
        this.reconnectInterval = reconnectInterval;
    }

    public void setResetTimeout(int resetTimeout) {
        this.resetTimeout = resetTimeout;
    }

    public void setGetDeviceConfigurationRetryInterval(int getDeviceConfigurationRetryInterval) {
        this.getDeviceConfigurationRetryInterval = getDeviceConfigurationRetryInterval;
    }

    public void setCheckEventsInterval(int checkEventsInterval) {
        this.checkEventsInterval = checkEventsInterval;
    }

    public void setCheckRegionHeadersInterval(int checkRegionHeadersInterval) {
        this.checkRegionHeadersInterval = checkRegionHeadersInterval;
    }

    public void setCheckRegionStatesInterval(int checkRegionStatesInterval) {
        this.checkRegionStatesInterval = checkRegionStatesInterval;
    }

    public void setCheckChildDeviceStatesInterval(int checkChildDeviceStatesInterval) {
        this.checkChildDeviceStatesInterval = checkChildDeviceStatesInterval;
    }

    public void setSetTimeInterval(int setTimeInterval) {
        this.setTimeInterval = setTimeInterval;
    }

    public void setTaskExecuteInterval(int taskExecuteInterval) {
        this.taskExecuteInterval = taskExecuteInterval;
    }

    public void setBatchEventsCount(int batchEventsCount) {
        this.batchEventsCount = batchEventsCount;
    }

    @Override
    public StateHandler getStateHandler() {
        return stateHandler;
    }

    @Override
    public Rubezh2OP3ActionExecutor getActionExecutor() {
        return actionExecutor;
    }

    @Override
    public DriverAlertSender getDriverAlertSender() {
        return driverAlertSender;
    }

    @Override
    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    @Override
    public List<DriverProfileView> getDriverProfileViews() {
        return driverProfileViews;
    }

    @Override
    public Map<String, DeviceProfileView> getDeviceProfileViewsByIds() {
        return deviceProfileViewsByIds;
    }

    @Override
    public DeviceProfile getRubezh2op3DeviceProfile() {
        return rubezh2op3DeviceProfile;
    }

    public void setAdapterModuleDeviceProfile(DeviceProfile adapterModuleDeviceProfile) {
        this.rubezh2op3DeviceProfile = adapterModuleDeviceProfile;
    }

    @Override
    public ActiveDevice getRubezh2op3Device() {
        return rubezh2op3Device;
    }

    @Override
    public void setRubezh2op3Device(ActiveDevice adapterModuleDevice) {
        this.rubezh2op3Device = adapterModuleDevice;
    }

    @Override
    public List<ActiveDevice> getChildDevices() {
        return childDevices;
    }

    @Override
    public ActiveDevice updateChildDeviceConfig(String deviceId, Map<String, String> configValues) {
        for (ActiveDevice activeDevice : childDevices) {
            if (activeDevice.getId().equals(deviceId)) {
                activeDevice.getDeviceProject().setProjectConfigValues(configValues);
                return activeDevice;
            }
        }
        return null;
    }

    @Override
    public List<ActiveRegion> getRegions() {
        return regions;
    }

    @Override
    public List<ActiveScenario> getScenarios() {
        return scenarios;
    }

    @Override
    public List<ActiveScenario> getExternalScenarios() {
        return externalScenarios;
    }

    @Override
    public Map<String, Set<String>> getControlDeviceIdsByScenarioIds() {
        return controlDeviceIdsByScenarioIds;
    }

    @Override
    public List<VirtualState> getVirtualStates() {
        return virtualStates;
    }

    @Override
    public Map<String, ActiveDevice> getControlDevicesByChildDeviceIds() {
        return controlDevicesByChildDeviceIds;
    }

    @Override
    public List<ActiveDevice> getExternalExecutiveChildDevices() {
        return externalExecutiveChildDevices;
    }

    @Override
    public Set<String> getEngagedInScenarioActionsDeviceIds() {
        return engagedInScenarioActionsDeviceIds;
    }

    @Override
    public Map<String, ActiveDevice> getAllProjectDevicesByIds() {
        return allProjectDevicesByIds;
    }

    @Override
    public Transport<ByteBuffer> getTransport() {
        return transport;
    }

    @Override
    public Clock getClock() {
        return clock;
    }

    @Override
    public Rubezh2OP3MessageCodec getMessageCodec() {
        return messageCodec;
    }

    @Override
    public Rubezh2OP3StateMachineHooks getStateMachineHooks() {
        return stateMachineHooks;
    }

    @Override
    public void setStateMachineHooks(Rubezh2OP3StateMachineHooks stateMachineHooks) {
        this.stateMachineHooks = stateMachineHooks;
        stateMachineHooks.setStateMachineContext(this);
    }

    @Override
    public DatabaseBuilder getDatabaseBuilder() {
        return databaseBuilder;
    }

    @Override
    public ChildDeviceConfigCodec getChildDeviceConfigCodec() {
        return childDeviceConfigCodec;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public int getReconnectInterval() {
        return reconnectInterval;
    }

    @Override
    public int getResetTimeout() {
        return resetTimeout;
    }

    @Override
    public int getGetDeviceConfigurationRetryInterval() {
        return getDeviceConfigurationRetryInterval;
    }

    @Override
    public int getCheckEventsInterval() {
        return checkEventsInterval;
    }

    @Override
    public int getCheckRegionHeadersInterval() {
        return checkRegionHeadersInterval;
    }

    @Override
    public int getCheckRegionStatesInterval() {
        return checkRegionStatesInterval;
    }

    @Override
    public int getCheckChildDeviceStatesInterval() {
        return checkChildDeviceStatesInterval;
    }

    @Override
    public int getSetTimeInterval() {
        return setTimeInterval;
    }

    @Override
    public int getTaskExecuteInterval() {
        return taskExecuteInterval;
    }

    @Override
    public int getUpdateSoftwareModeRetry() {
        return updateSoftwareModeRetry;
    }

    @Override
    public int getStartWritingRetryTO() {
        return startWritingRetryTO;
    }

    @Override
    public int getModeCheckingInterval() {
        return MODE_CHECKING_INTERVAL;
    }

    @Override
    public Map<String, DeviceProfile> getSupportedDeviceProfiles() {
        return supportedDeviceProfiles;
    }

    @Override
    public Rubezh2OP3TaskManager getTaskManager() {
        return taskManager;
    }

    @Override
    public Rubezh2OP3ReadEventsState getFetchingEventsState() {
        return fetchingEventsState;
    }

    @Override
    public void setFetchingEventsState(Rubezh2OP3ReadEventsState fetchingEventsState) {
        this.fetchingEventsState = fetchingEventsState;
    }

    @Override
    public Rubezh2OP3ReadEventsState getReadEventsState() {
        return readEventsState;
    }

    @Override
    public ActiveRegion getNextActiveRegionToCheckStates() {
        ActiveRegion activeRegion = null;
        int regionsCount = regions.size();
        if (regionsCount != 0) {
            int index = nextRegionIndexToCheckStates % regionsCount;
            activeRegion = regions.get(index);
            index = (index + 1) % regionsCount;
            nextRegionIndexToCheckStates= index;
        }
        return activeRegion;
    }

    @Override
    public Rubezh2OP3ChildDevicePollingState getChildDevicePollingState() {
        return childDevicePollingState;
    }

    @Override
    public void setControlDeviceInfo(Rubezh2OP3DeviceInfo controlDeviceInfo) {
        this.controlDeviceInfo = controlDeviceInfo;
    }

    @Override
    public Rubezh2OP3DeviceInfo getControlDeviceInfo() {
        return controlDeviceInfo;
    }

    @Override
    public void setDeviceMaxEventCounters(Map<String, Long> deviceMaxEventCounters) {
        this.deviceMaxEventCounters = deviceMaxEventCounters;
    }

    @Override
    public Map<String, Long> getDeviceMaxEventCounters() {
        return deviceMaxEventCounters;
    }

    private Map<String, Long> getDefaultDeviceMaxEventCounters() {
        List<DeviceProfile.LogTypeItem> logTypes = rubezh2op3DeviceProfile.getLogTypes();
        Map<String, Long> defaultDeviceMaxEventCounters = new HashMap<>();
        for (DeviceProfile.LogTypeItem logType : logTypes) {
            defaultDeviceMaxEventCounters.put(logType.getId(), (long) logType.getSize());
        }
        return defaultDeviceMaxEventCounters;
    }

    @Override
    public void setDeviceEventCounters(Map<String, Long> deviceEventCounters) {
        this.deviceEventCounters = deviceEventCounters;
    }

    @Override
    public Map<String, Long> getDeviceEventCounters() {
        return deviceEventCounters;
    }

    @Override
    public void setLongOperationWaitingTimeMs(int waitingTimeMs) {
        this.longOperationWaitingTimeMs = waitingTimeMs;
    }

    @Override
    public int getLongOperationWaitingTimeMs() {
        return longOperationWaitingTimeMs;
    }

    @Override
    public Rubezh2OP3UpdatingDatabaseState getUpdatingDatabaseState() {
        return updatingDatabaseState;
    }

    @Override
    public Rubezh2OP3UpdatingSoftwareState getUpdatingSoftwareState() {
        return updatingSoftwareState;
    }

    @Override
    public void resetUpdatingSoftwareState() {
        updatingSoftwareState = new Rubezh2OP3UpdatingSoftwareState();
    }

    @Override
    public BitSet getRegionStateBits(String regionId) {
        if (regionStateBitsMap.containsKey(regionId)) {
            return regionStateBitsMap.get(regionId);
        }
        return new BitSet();
    }

    @Override
    public void setRegionStateBits(String regionId, BitSet regionStateBits) {
        regionStateBitsMap.put(regionId, regionStateBits);
    }


    @Override
    public boolean checkEventsReadyToSend() {
        return events.size() >= getBatchEventsCount();
    }

    @Override
    public int getBatchEventsCount() {
        return batchEventsCount;
    }

    @Override
    public int getEventsSendTimeOut() {
        return EVENTS_SEND_TIMEOUT;
    }

    @Override
    public int getWriteBlockSize() {
        return WRITE_BLOCK_SIZE;
    }

    @Override
    public List<ControlDeviceEvent> getEvents() {
        return events;
    }

    @Override
    public void setEvents(List<ControlDeviceEvent> events) {
        this.events = events;
    }

    @Override
    public void addEvent(ControlDeviceEvent event) {
        events.add(event);
    }

    @Override
    public int getResetProgress() {
        return resetProgress;
    }

    @Override
    public void setResetProgress(int resetProgress){
        this.resetProgress = resetProgress;
    }

    @Override
    public LicenseReader getLicenseReader() {
        return licenseReader;
    }

    @Override
    public Rubezh2OP3UpdateSoftwareStatusType getRubezh2op3DeviceMode() {
        return rubezh2op3DeviceMode;
    }

    @Override
    public void setRubezh2op3DeviceMode(Rubezh2OP3UpdateSoftwareStatusType mode) {
        if (rubezh2op3DeviceMode != mode) {
            logger.info("Rubezh2OP3 (address path: {}) mode: {}", getRubezh2op3Device().getAddressPath(),
                    mode != null ? mode.toString() : "Unrecognized");
            rubezh2op3DeviceMode = mode;
        }
    }

    @Override
    public boolean isBadSignature() {
        return badSignature;
    }

    @Override
    public void setBadSignature(boolean badSignature) {
        this.badSignature = badSignature;
    }

    @Override
    public SkudDatabaseBuilder getSkudDatabaseBuilder() {
        return skudDatabaseBuilder;
    }

    @Override
    public void updateVarSkudDatabase(List<Employee> employees, List<AccessKey> accessKeys,
            List<WorkSchedule> workSchedules, Map<String, List<AccessKey>> accessKeysByEmployeeIds) {
        employeesByIds = new HashMap<>();
        if (employees != null) {
            for (Employee employee : employees) {
                employeesByIds.put(employee.getId(), employee);
            }
        }

        accessKeysByIds = new HashMap<>();
        if (accessKeys != null) {
            for (AccessKey accessKey : accessKeys) {
                accessKeysByIds.put(accessKey.getId(), accessKey);
            }
        }

        workSchedulesByIds = new HashMap<>();
        if (workSchedules != null) {
            for (WorkSchedule workSchedule : workSchedules) {
                workSchedulesByIds.put(workSchedule.getId(), workSchedule);
            }
        }

        this.accessKeysByEmployeeIds = accessKeysByEmployeeIds;
    }

    @Override
    public Map<String, Employee> getEmployees() {
        return employeesByIds;
    }

    @Override
    public Map<String, AccessKey> getAccessKeys() {
        return accessKeysByIds;
    }

    @Override
    public Map<String, WorkSchedule> getWorkSchedules() {
        return workSchedulesByIds;
    }

    @Override
    public Map<String, List<AccessKey>> getAccessKeysByEmployeeIds() {
        return accessKeysByEmployeeIds;
    }

    @Override
    public String getWaitingAccessKeyId() {
        return waitingAccessKeyId;
    }

    @Override
    public void setWaitingAccessKeyId(String waitingAccessKeyId) {
        this.waitingAccessKeyId = waitingAccessKeyId;
    }

}
