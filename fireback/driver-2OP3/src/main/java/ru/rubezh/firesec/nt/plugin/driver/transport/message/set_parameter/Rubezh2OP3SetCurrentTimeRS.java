package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetCurrentTimeRS extends Rubezh2OP3SetParameterRS {

    public Rubezh2OP3SetCurrentTimeRS() {
        super(Rubezh2OP3ParameterType.CURRENT_TIME);
    }

}
