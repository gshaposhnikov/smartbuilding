package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

public class RegionHeader {
    int no;
    String title;
    int entryLength;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEntryLength() {
        return entryLength;
    }

    public void setEntryLength(int entryLength) {
        this.entryLength = entryLength;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{Region no: ");
        stringBuilder.append(no);
        stringBuilder.append(", title: \"");
        stringBuilder.append(title.trim());
        stringBuilder.append("\", entryLength: ");
        stringBuilder.append(entryLength);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

}
