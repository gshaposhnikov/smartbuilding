package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.SetRubezh2op3TimeTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class WaitingSetRubezh2op3Time extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_SET_RUBEZH2OP3_TIME;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingSetRubezh2op3Time handler = new WaitingSetRubezh2op3Time();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onDeviceTimeSet(long currentTimeMs) {
        try {
            SetRubezh2op3TimeTask task =
                    (SetRubezh2op3TimeTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onRubezh2op3TimeSet(currentTimeMs,
                    stateMachineContext.getRubezh2op3Device().getId());
            if (task.getIssueId() != null)
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                        task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        try {
            SetRubezh2op3TimeTask task =
                    (SetRubezh2op3TimeTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onRubezh2op3TimeSetFailed(currentTimeMs);
            if (task.getIssueId() != null)
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                        task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            SetRubezh2op3TimeTask task =
                    (SetRubezh2op3TimeTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onRubezh2op3TimeSetFailed(currentTimeMs);
            if (task.getIssueId() != null)
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                        task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
