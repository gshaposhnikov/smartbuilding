package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.ResetDeviceStateTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class WaitingResetDeviceState extends AbstractTaskState {
    @Override
    public State getCurrentState() { return State.WAITING_RESET_STATE_DEVICE; }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingResetDeviceState handler = new WaitingResetDeviceState();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onResetStateDeviceReceived(long currentTimeMs) {
        try {
            ResetDeviceStateTask task = (ResetDeviceStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onStateReset(currentTimeMs,
                    task.getActiveDevice().getId(), task.getTargetGroup());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        try {
            ResetDeviceStateTask task = (ResetDeviceStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onStateResetFailed(currentTimeMs,
                    task.getActiveDevice().getId(), task.getTargetGroup());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            ResetDeviceStateTask task = (ResetDeviceStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onStateResetFailed(currentTimeMs,
                    task.getActiveDevice().getId(), task.getTargetGroup());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
