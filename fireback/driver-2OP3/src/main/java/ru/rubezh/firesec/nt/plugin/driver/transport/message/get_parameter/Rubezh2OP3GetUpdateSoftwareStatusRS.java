package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public class Rubezh2OP3GetUpdateSoftwareStatusRS extends Rubezh2OP3GetParameterRS {

    private Rubezh2OP3UpdateSoftwareStatusType status;

    public Rubezh2OP3GetUpdateSoftwareStatusRS() {
        super(Rubezh2OP3ParameterType.UPDATE_SOFTWARE_STATUS);
    }

    public Rubezh2OP3UpdateSoftwareStatusType getStatus() {
        return status;
    }

    public void setStatus(Rubezh2OP3UpdateSoftwareStatusType status) {
        this.status = status;
    }

}
