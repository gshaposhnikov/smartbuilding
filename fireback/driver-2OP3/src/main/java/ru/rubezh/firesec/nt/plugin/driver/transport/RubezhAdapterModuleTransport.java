package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.io.IOException;
import java.nio.ByteBuffer;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineHooks;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;


/**
 * Транспортная сущность для взаимодействия конечного автомата драйвера с устройством, подключенным к системе через модуль сопряжения.
 *
 * Этот класс:
 *  - все запросы на отправку/прием сообщений перенаправляет как есть транспорту конечного автомата модуля сопряжения;
 *  - запросы на подключение/отключение направляют самому конечному автомату модуля сопряжения;
 *  - ответы о результатах подключения/отключения получает, подменив собой "прицеп" (StateMachineHooks) конечного автомата модуля сопряжения;
 *  - все вызовы от конечного автомата модуля сопряжения к "прицепу" транслирует подмененному "прицепу";
 *  - изначально настраивается номером линии модуля сопряжения. Для обслуживания приборов на разных линиях необходимо создавать;
 *      по экземпляру такого класса на каждую линию, выстраивая их в цепочку.
 *
 * Типовое использование:
 *  1. Создать транспорт для конечного автомата модуля сопряжения (в базовом случае RubezhUSBTransport);
 *  2. Создать контекст конечного автомата модуля сопряжения, указав ему ранее созданный транспорт;
 *  3. Создать экземпляр данного класса, указав ему номер линии RS-485 (нумерация начинается с 0) модуля сопряжения;
 *  4. Указать экземпляру данного класса ранее созданный контекст конечного автомата модуля сопряжения;
 *  5. Создать контекст конечного автомата устройства, подключенного через модуль сопряжения, указав экземпляр данного класса.
 *
 * @author Антон Васильев
 *
 */
public class RubezhAdapterModuleTransport implements Transport<ByteBuffer>, StateMachineHooks {

    private StateMachineHooks primaryStateMachineHooks;
    private StateMachineContext amodStateMachineContext;
    int lineNo;

    private ConnectHandler connectHandler;
    private DisconnectHandler disconnectHandler;
    boolean connected = false;

    /* Время последней полезной операции */
    private long lastActivityTime;

    public RubezhAdapterModuleTransport(int lineNo) {
        super();
        this.lineNo = lineNo;
    }

    @Override
    public StateMachineContext getStateMachineContext() {
        return amodStateMachineContext;
    }

    @Override
    public void setStateMachineContext(StateMachineContext stateMachineContext) {
        if (amodStateMachineContext != null)
            amodStateMachineContext.setStateMachineHooks(primaryStateMachineHooks);

        amodStateMachineContext = stateMachineContext;
        primaryStateMachineHooks = amodStateMachineContext.getStateMachineHooks();
        amodStateMachineContext.setStateMachineHooks(this);
    }

    @Override
    public void connect(long currentTimeMs, ConnectHandler connectHandler, DisconnectHandler disconnectHandler) {
        lastActivityTime = currentTimeMs;
        this.connectHandler = connectHandler;
        this.disconnectHandler = disconnectHandler;
        if (!connected)
            amodStateMachineContext.getStateHandler().onConnectRequest(currentTimeMs);
    }

    @Override
    public int send(long currentTimeMs, ByteBuffer rawData, SendHandler sendHandler,
            SendReceiveFailedHandler failHandler) {
        lastActivityTime = currentTimeMs;
        return amodStateMachineContext.getTransport().send(currentTimeMs, rawData, sendHandler, failHandler);
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failHandler) {
        lastActivityTime = currentTimeMs;
        amodStateMachineContext.getTransport().receive(currentTimeMs, sequenceNo, receiveHandler, failHandler);
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failHandler, SendReceiveFailedHandler timeoutHandler) {
        lastActivityTime = currentTimeMs;
        amodStateMachineContext.getTransport().receive(currentTimeMs, sequenceNo, receiveHandler, failHandler,
                timeoutHandler);
    }

    @Override
    public void disconnect(long currentTimeMs) throws IOException {
        lastActivityTime = currentTimeMs;
    }

    @Override
    public void process(long currentTimeMs) throws IOException {
        if (connected && connectHandler != null) {
            ConnectHandler connectHandler = this.connectHandler;
            this.connectHandler = null;
            connectHandler.onConnected(currentTimeMs, true);
        }
    }

    @Override
    public long getLastActivityTime() {
        return lastActivityTime;
    }

    @Override
    public void onConnected(long currentTimeMs) {
        primaryStateMachineHooks.onConnected(currentTimeMs);
    }

    @Override
    public void onDisconnected(long currentTimeMs) {
        primaryStateMachineHooks.onDisconnected(currentTimeMs);
        connected = false;
        if (disconnectHandler != null) {
            DisconnectHandler disconnectHandler = this.disconnectHandler;
            this.connectHandler = null;
            this.disconnectHandler = null;
            disconnectHandler.onDisconnected(currentTimeMs);
        }
    }

    @Override
    public void onConnectFailure(long currentTimeMs) {
        primaryStateMachineHooks.onConnectFailure(currentTimeMs);
        connected = false;
        if (connectHandler != null) {
            ConnectHandler connectHandler = this.connectHandler;
            this.connectHandler = null;
            this.disconnectHandler = null;
            connectHandler.onConnected(currentTimeMs, false);
        }
    }

    @Override
    public void onRS485Configured(long currentTimeMs, int lineNo) {
        primaryStateMachineHooks.onRS485Configured(currentTimeMs, lineNo);
        if (lineNo == this.lineNo && connectHandler != null) {
            connected = true;
            ConnectHandler connectHandler = this.connectHandler;
            this.connectHandler = null;
            connectHandler.onConnected(currentTimeMs, true);
        }
    }

    @Override
    public void onRS485ConfigureFailed(long currentTimeMs, int lineNo, ExceptionType exceptionType) {
        primaryStateMachineHooks.onRS485ConfigureFailed(currentTimeMs, lineNo, exceptionType);
        if (lineNo == this.lineNo && connectHandler != null) {
            connected = false;
            ConnectHandler connectHandler = this.connectHandler;
            this.connectHandler = null;
            this.disconnectHandler = null;
            connectHandler.onConnected(currentTimeMs, false);
        }
    }

    @Override
    public void onRS485AddressDoublingDetected(long currentTimeMs, int lineNo) {
        primaryStateMachineHooks.onRS485AddressDoublingDetected(currentTimeMs, lineNo);
    }

    @Override
    public void onRS485AddressDoublingFixed(long currentTimeMs, int lineNo) {
        primaryStateMachineHooks.onRS485AddressDoublingFixed(currentTimeMs, lineNo);
    }

}
