package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class Idle extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.IDLE;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        Idle handler = new Idle();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().connect(currentTimeMs);
        return State.FIRST_TIME_CONNECTING;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        return State.IDLE;
    }
}
