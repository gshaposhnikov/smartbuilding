package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.data.util.Pair;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock.LogicType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionCheckType;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model.LogicBlockWrapper;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model.ScenarioTimeLineBlockAction;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model.ScenarioTimeLineBlockRepr;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model.ScenarioWrapper;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseWriter;

/**
 * Класс для формирования бинарной БД на основе переданных данных.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru), Андрей Лисовой
 *
 */
public class DatabaseBuilder extends DatabaseWriter {

    /**
     * Адрес блока в сценарии - по сути пара "индекс сценария : индекс блока в
     * сценарии". Индексы локальные, нумерация с 0
     */
    private class ActionBlockAddress {
        public int scenarioIdx;
        public int blockIdx;

        public ActionBlockAddress(int scenarioIdx, int blockIdx) {
            this.scenarioIdx = scenarioIdx;
            this.blockIdx = blockIdx;
        }
    }

    /** Размер адреса в памяти прибора */
    private static final int DB_ADDRESS_SIZE = 3;
    /** Версия хэша, записываемого с ПК */
    private static final byte[] DB_HASH_VERSION = { 0x01 };
    /** Еще одна версия хэша */
    private static final byte[] DB_SEC_HASH_VERSION = { 0x00 };
    /** Сигнатура базы: "BASE" */
    private static final byte[] DB_SIGNATURE = { 0x42, 0x41, 0x53, 0x45 };
    /** Версия БД */
    private static final byte[] DB_VERSION = { 0x08, 0x01 };
    /** Заглушка для CRC БД, которая заполнится после формирования БД */
    private static final byte[] DB_CRC_STUB = { 0x00, 0x00 };
    /** Смещение главной части БД */
    private static final int DB_MAIN_OFFSET = 0x800000;
    /** Смещение БД с конфигом локальных устройств */
    private static final int DB_CONFIG_OFFSET = 0x9F0000;
    /** Смещение от начала DB_CONFIG_OFFSET */
    private static final int DB_CONFIG_ADDRESS_OFFSET = 0x02;
    /** Размер конфига локальных устройств */
    private static final int DB_CONFIG_RESERVED_SIZE = 7000;
    /** Смещение области базы с дополнительной конфигурацией устройств */
    private static final int DB_EXTRA_CONFIG_OFFSET = 0x9FFF00;
    /** Размер области базы с дополнительной конфигурацией устройств */
    private static final int DB_EXTRA_CONFIG_SIZE = 256;
    /** Заглушка для адреса */
    private static final byte[] EMPTY_MEMORY_ADDRESS = { 0x00, 0x00, 0x00 };
    /** Заглушка для 2-байтового счетчика */
    private static final byte[] EMPTY_COUNTER = { 0x00, 0x00 };

    /** Смещение восстановительной информации */
    private static final int DB_RECOVERY_OFFSET = 0x200000;
    /** Наибольшая длина восстановительной информации */
    private static final int DB_RECOVERY_MAX_LENGTH = 0x200000;
    /** Размер поля "длина" восстановительной информации */
    private static final int DB_RECOVERY_LENGTH_SIZE = 4;

    /** Размер хеша MD5 в самом начале БД */
    private static final int MD5_HASH_SIZE = 16;
    /** Размер резерва для компа в начале БД */
    private static final int RESERVED_BLOCK_SIZE = 256;
    /** Количество таблиц с устройствами */
    private static final int DEVICE_TABLE_COUNT = 500;
    /** Количество линий */
    private static final int LINE_COUNT = 2;
    /** Количество устройств на одной линии */
    private static final int DEVICE_ON_LINE = 250;

    /** Количество указателей на устройства-мониторы */
    private static final int MONITOR_POINTER_COUNT = 64;
    /** Размер указателя на устройства-мониторы */
    private static final int MONITOR_POINTER_SIZE = 1;

    /** Размер сущности описания внешнего сценария */
    private static final int EXTERNAL_SCENARIO_SIZE = 23;

    /** Размер текстового описания устройства */
    private static final int DEVICE_DESCRIPTION_SIZE = 20;

    /** Размер виртуального типа */
    private static final int TABLE_VIRTUAL_TYPE_SIZE = 1;
    /** Размер номера направления */
    private static final int TABLE_DIRECTION_NO_SIZE = 1;

    /** Имя по умолчанию для исполнительного устройства */
    private static final String TABLE_EXECUTIVE_DEFAULT_NAME = "Исполнительное устройство";
    /** Описание состояния "Вкл" исполнительного устройства по умолчанию */
    private static final String TABLE_EXECUTIVE_DEFAULT_ON_TITLE = "Вкл";
    /** Описание состояния "Выкл" исполнительного устройства по умолчанию */
    private static final String TABLE_EXECUTIVE_DEFAULT_OFF_TITLE = "Выкл";
    /**
     * Заглушка для размера переменной части конфига исполнительного устройства
     */
    private static final byte[] TABLE_EXECUTIVE_ENTRY_LENGTH_STUB = { 0x00, 0x00 };
    /** Длина поля "тип внешнего исполнительного устройства" */
    private static final int TABLE_EXTERNAL_EXECUTIVE_TYPE_SIZE = 2;
    /** Индекс таблицы внешних исполнительных устройств */
    private static final int TABLE_EXTERNAL_EXECUTIVE_INDEX = 8;

    /** Имя по умолчанию для виртуального контейнера */
    private static final String TABLE_VIRTUAL_CONTAINER_DEFAULT_NAME = "Вирт. контейнер";

    /** Имя по умолчанию для сенсора */
    private static final String TABLE_SENSOR_DEFAULT_NAME = "Сенсор";
    /** Описание состояния "Норма" для сенсора по умолчанию */
    private static final String TABLE_SENSOR_DEFAULT_NONE_TITLE = "Норма";
    /** Описание состояния "Сработка 1го датчика" для сенсора по умолчанию */
    private static final String TABLE_SENSOR_DEFAULT_FIRST_TITLE = "Сработка 1го датчика";
    /** Описание состояния "Сработка 2го датчика" для сенсора по умолчанию */
    private static final String TABLE_SENSOR_DEFAULT_SECOND_TITLE = "Сработка 2го датчика";
    /** Описание состояния "Сработка 2х датчиков" для сенсора по умолчанию */
    private static final String TABLE_SENSOR_DEFAULT_BOTH_TITLE = "Сработка 2х датчиков";

    /** Маска бита "признак светозвукового оборудования" (только для РМ) */
    private static final int TABLE_DEVICE_LIGHT_AND_SOUND_BIT = 0x01;
    /** Маска бита "Ведущий МПТ" (только для ПМТ) */
    private static final int TABLE_MPT_IS_MASTER_BIT = 0x08;
    /** Бит "1й адрес в составном устройстве" в недоставляевом конфиге */
    private static final int TABLE_DEVICE_CONTAINER_FIRST_DEVICE_BIT = 0b010;
    /** Маска бита "привязана логика "неисправность"" (только для встроенных реле) */
    private static final int TABLE_DEVICE_FAILURE_FEATURE_BIT = 0x01;
    /** Маска бита "привязана логика "включение по пожару в 2х зонах"" (только для встроенных реле) */
    private static final int TABLE_TWO_REGIONS_FIRE_FEATURE_BIT = 0x02;
    /** Смещение битов для указания смещения адреса в недоставляемом конфиге */
    private static final int TABLE_DEVICE_CONTAINER_OFFSET_BIT = 4;
    /** Заглушка длины переменной части конфига устройства */
    private static final byte[] TABLE_DEVICE_CONFIG_LENGTH_STUB = { 0x00, 0x00 };
    /** Длина количества связанных шаблонов логики */
    private static final int TABLE_DEVICE_LOGIC_BLOCK_REF_COUNT_SIZE = 2;

    /** Длина номера зоны */
    private static final int TABLE_REGION_NO_SIZE = 2;
    /** Длина имени зоны */
    private static final int TABLE_REGION_TITLE_SIZE = 20;
    /** Заглушка длины сущности описания зоны */
    private static final byte[] TABLE_REGION_ENTRY_LENGTH_STUB = { 0x00, 0x00 };
    /** Заглушка длины переменной части конфига зоны */
    private static final byte[] TABLE_REGION_VARIABLE_CONFIG_LENGTH_STUB = { 0x00 };
    /** Код для пожарной зоны */
    private static final int TABLE_REGION_CONFIG_FIRE = 0x00;
    /** Код для охранной зоны */
    private static final int TABLE_REGION_CONFIG_SECURITY = 0x01;
    /** Бит зоны без права снятия */
    private static final int TABLE_REGION_CONFIG_SEC_NO_OFF = 0x01;
    /** Бит зоны с тихой тревогой */
    private static final int TABLE_REGION_CONFIG_SEC_SILENCE_ALARM = 0x02;
    /** Бит зоны с задержкой входа/выхода */
    private static final int TABLE_REGION_CONFIG_SEC_TIMEOUT = 0x08;
    /** Бит зоны с собственными датчиками */
    private static final int TABLE_REGION_CONFIG_SEC_SELF_DETECTORS = 0x20;
    /** Размер значения кол-ва датчиков пожарной зоны для перехода в Пожар-2 */
    private static final int TABLE_REGION_FIRE_EVENT_COUNT_SIZE = 1;
    /** Размер значения задержки входа/выхода */
    private static final int TABLE_REGION_INPUT_OUTPUT_TIMEOUT_SIZE = 2;
    /** Размер значения времени автоперевзятия */
    private static final int TABLE_REGION_DELAY_SIZE = 1;
    /** Размер количества датчиков для перехода в Пожар-2 в ночном режиме */
    private static final int TABLE_REGION_NIGHT_FIRE_EVENT_COUNT_SIZE = 1;
    /** Размер значения времени подтверждения срабатывания */
    private static final int TABLE_REGION_CONFIRMATION_TIME_SIZE = 1;
    /** Размер значения времени ожидания после подтверждения */
    private static final int TABLE_REGION_WAIT_TIME_SIZE = 2;
    /** Размер количества внешних приборов, участвующих в межприборном "И" */
    private static final int TABLE_REGION_EXTERNAL_DEVICE_COUNT_SIZE = 1;
    /** Размер количества связанных шаблонов логики */
    private static final int TABLE_REGION_LOGIC_BLOCK_REF_COUNT_SIZE = 2;

    /** Размер номера виртуального состояния */
    private static final int TABLE_VIRTUAL_STATE_NO_SIZE = 2;
    /** Размер адреса прибора в виртуальном состоянии */
    private static final int TABLE_VIRTUAL_STATE_DEVICE_ADDRESS_SIZE = 1;
    /** Значение адреса прибора для локального прибора */
    private static final byte[] TABLE_VIRTUAL_STATE_DEVICE_ADDRESS_LOCAL = { 0x00 };
    /** Размер сообщения об установке / снятии виртуального состояния */
    private static final int TABLE_VIRTUAL_STATE_MESSAGE_SIZE = 20;
    /** Значение резерва в описании виртуального состояния */
    private static final byte[] TABLE_VIRTUAL_STATE_RESERVE_STUB = { 0x00 };
    /** Заглушка для длины описания виртуального состояния */
    private static final byte[] TABLE_VIRTUAL_STATE_ENTRY_LENGTH_STUB = { 0x00, 0x00 };
    /** Размер количества связанных шаблонов логики */
    private static final int TABLE_VIRTUAL_STATE_LOGIC_BLOCK_REF_COUNT_SIZE = 2;

    /** Логика "И" в блоке логики */
    private static final int TABLE_LOGIC_BLOCK_CONFIG_AND = 0x01;
    /** Логика "НЕ" в блоке логики */
    private static final int TABLE_LOGIC_BLOCK_CONFIG_NOT = 0x02;
    /** Флаг логики запуска */
    private static final int TABLE_LOGIC_BLOCK_CONFIG_START_LOGIC = 0x04;
    /** Флаг наличия логики остановки */
    private static final int TABLE_LOGIC_BLOCK_CONFIG_HAS_STOP_LOGIC = 0x08;
    /** Смещение в байте конфига для значения вложенности блока логики */
    private static final int TABLE_LOGIC_BLOCK_CONFIG_DEPTH_OFFSET = 4;
    /** Константа для указания на то, что у блока логики есть вложенные блоки */
    private static final int TABLE_LOGIC_BLOCK_TRIGGER_SUB_BLOCKS = 0x3B;
    /** Размер количества проверяемых в блоке логики сущностей */
    private static final int TABLE_LOGIC_BLOCK_CHILD_COUNT_SIZE = 2;

    /** Размер индекса сценария */
    private static final int TABLE_SCENARIO_NO_SIZE = 2;
    /** Бит ручной остановки сценария */
    private static final int TABLE_SCENARIO_CONFIG_MANUAL_STOP = 0x01;
    /** Бит командного сценария */
    private static final int TABLE_SCENARIO_CONFIG_COMMAND = 0x02;
    /** Бит остановки сценария при глобальном сбросе */
    private static final int TABLE_SCENARIO_CONFIG_STOP_ON_RESET = 0x08;
    /** Тип сценария : не задан */
    private static final int TABLE_SCENARIO_CONFIG_TYPE_UNASSIGNED = 0;
    /** Тип сценария : СОУЭ */
    private static final int TABLE_SCENARIO_CONFIG_TYPE_ENS = 1;
    /** Тип сценария : пожаротушение */
    private static final int TABLE_SCENARIO_CONFIG_TYPE_FIRE_FIGHTING = 2;
    /** Тип сценария : инженерный */
    private static final int TABLE_SCENARIO_CONFIG_TYPE_ENGINEERING = 3;
    /** Смещение типа сценария в байте конфига */
    private static final int TABLE_SCENARIO_CONFIG_TYPE_OFFSET = 4;
    /** Длина названия сценария */
    private static final int TABLE_SCENARIO_TITLE_SIZE = 20;
    /** Длина значения задержки исполнительного блока сценария */
    private static final int TABLE_SCENARIO_BLOCK_DELAY_SIZE = 1;
    /** Заглушка длины исполнительного блока сценария */
    private static final byte[] TABLE_SCENARIO_ACTION_BLOCK_LENGTH_STUB = { 0x00, 0x00 };
    /** Бит проверки отсутствия указанного состояния в блоке сценария */
    private static final int TABLE_SCENARIO_ACTION_CHECK_INACTIVE = 0x02;
    /** Бит блока слежения в исполнительном блоке сценария */
    private static final int TABLE_SCENARIO_ACTION_TRACING = 0x01;
    /** Смещение кода действия в исполнительном блоке сценария */
    private static final int TABLE_SCENARIO_ACTION_CODE_OFFSET = 4;
    /** Бит "необходимо генерировать сообщение" в блоке сценария */
    private static final int TABLE_SCENARIO_NEED_STATE_MESSAGE = 0x04;
    /** Байт-указатель последнего исполнительного блока сценария */
    private static final byte[] TABLE_SCENARIO_LAST_BLOCK_BYTE = { 0x00 };
    /** Размер количества связанных устройств с линии в сценарии */
    private static final int TABLE_SCENARIO_BLOCK_ACTION_LINE_COUNT = 1;
    /** Размер количества связанных вирт. состояний и т.д. в сценарии */
    private static final int TABLE_SCENARIO_BLOCK_ACTION_OTHER_COUNT = 2;
    /** Константное значение байта для проверки установки вирт. состояния */
    private static final int TABLE_SCENARIO_VIRTUAL_STATE_ACTIVE = 0x0D;

    /** Длина генерируемых сообщений */
    private static final int TABLE_MESSAGE_LENGTH = 20;
    /** Единственное предопределенное сообщение в БД */
    private static final String TABLE_MESSAGE_HARDCODED = "Контроль наряда";
    /** Код предопределенного сообщения */
    private static final byte TABLE_MESSAGE_HARDCODED_CODE = 0x01;
    /** Количество генерируемых сообщений */
    private static final int TABLE_MESSAGE_COUNT = 1;
    /** Длина сущности генерируемого сообщения */
    private static final int TABLE_MESSAGE_SIZE = 21;

    /** Размер количества таблицы прибора */
    private static final int TABLE_SELF_COUNT_SIZE = 2;

    /** Значение номера линии всех устройств с втунренней адресацией */
    private static final int TABLE_INTERNAL_DEVICE_LINE_NO = 0;

    /** Заглушка серийного номера в конфиге локальных устройств */
    private static final byte[] CONFIG_SERIAL_STUB = { 0x00, 0x00, 0x00 };
    /** Заглушка для версии прошивки в конфиге локальных устройств */
    private static final byte[] CONFIG_SW_VERSION_STUB = { 0x00 };
    /** Заглушка для версии протокола в конфиге локальных устройств */
    private static final byte[] CONFIG_PROTOCOL_VERSION_STUB = { 0x00 };
    /** Смещение начала фактических конфигов */
    private static final int DB_CONFIG_CONFIGS_OFFSET = 0x06
            + LINE_COUNT * DEVICE_ON_LINE * EMPTY_MEMORY_ADDRESS.length;

    /** Размер произвольных данных в конце БД */
    private static final int ARBITRARY_DATA_SIZE = 20;
    /** Биты конфигурации АЛС */
    private static final int LINE_1_IS_RING_BIT_NO = 0;
    private static final int LINE_2_IS_RING_BIT_NO = 1;
    private static final int LINE_1_ISOLATOR_BIT_NO = 2;
    private static final int LINE_2_ISOLATOR_BIT_NO = 3;
    /** Размер конфигурации АЛС */
    private static final int TABLE_LINES_CONFIG_SIZE = 1;

    /** Длина адресного пути прибора */
    private static final int CONTROL_DEVICE_ADDRESS_PATH_LENGTH = 6;

    private Rubezh2OP3StateMachineContext stateMachineContext;

    /** Шапка БД с фиксированным размером */
    private byte[] dbMain;
    /** Динамические данные БД */
    private byte[] dbConfig;
    /** Дополнительные данные конфигурации БД (расположены в секторе dbConfig) */
    private byte[] dbExtraConfig;
    /** Восстановительная информация */
    private byte[] dbRecovery;
    /** Адрес расположения контрольной суммы в динамических данных БД */
    private int configCrcOffset;
    /** Адрес фактического окончания конфигурации в динамических данных БД */
    private int configCrcEndOffset;
    /** Адреса конфигураций устройств по линии и адресу на линии */
    private int[][] deviceConfigOffsets;

    /** Хидеры таблиц устройств по типам */
    private List<TableHeader> deviceTableHeaders;
    /** Хидер таблицы указателей на зоны */
    private TableHeader regionPointersTableHeader;
    /**
     * Хидер таблицы внешних для прибора зон, в которых не локальные ИП
     * управляют локальными ИУ (в логике «межприборное И» у ИУ)
     */
    private TableHeader externalRegionsTableHeader;
    /** Хидер таблицы указателей на внешние устройства */
    private TableHeader externalExecutiveDevicePointersTableHeader;
    /**
     * Хидер таблицы указателей на служебную таблицу (список
     * устройств-мониторов)
     */
    private TableHeader monitorPointersTableHeader;
    /**
     * Хидер таблицы указателей на направления тушения (НЕ ИСПОЛЬЗУЕТСЯ, «0»
     * заполнить)
     */
    private TableHeader directionPointersTableHeader;
    /** Хидер таблицы шаблонов логик */
    private TableHeader logicBlocksTableHeader;
    /** Хидер таблицы указателей на сценарии */
    private TableHeader scenarioPointersTableHeader;
    /** Хидеры таблиц указателей на устройства по шлейфам */
    private TableHeader[] lineDevicePointersTableHeaders;
    /** Хидер таблицы указателей на параметры конфига локальных устройств */
    private TableHeader localDeviceConfigPointersTableHeader;
    /** Хидер таблицы внешних сценариев */
    private TableHeader externalScenariosTableHeader;
    /** Хидер таблицы контейнера составных устройств (многоадресный) */
    private TableHeader virtualContainersTableHeader;
    /** Хидер таблицы виртуальных состояний */
    private TableHeader virtualStatesTableHeader;
    /** Хидер таблицы генерируемых сообщений */
    private TableHeader messagesTableHeader;
    /** Хидер таблицы прибора */
    private TableHeader selfTableHeader;
    /** Хидер таблицы указателей на блоки сценариев */
    private TableHeader scenarioLogicBlockPointersTableHeader;

    /* Резерв для компа */
    private byte[] dbHash;
    private byte[] dbSecHash;
    private int dbHashOffset;
    private int dbCrcOffset;
    private int dbEndAddress;

    /** Обертки для сценариев */
    private List<ScenarioWrapper> scenarios;
    /** Обертки для шаблонов логики */
    private List<LogicBlockWrapper> logicBlocks;

    /** МПТ, привязанные к зонам */
    private Map<String, String> mptRegionDeviceIds;
    /**
     * Смещения описаний дочерних устройств, в том числе внешних исполнительных
     * устройств
     */
    private Map<String, Integer> localAndExternalChildDeviceOffsets;
    /** Отсортированный список внешних исполнительных устройств */
    private List<ActiveDevice> sortedExternalExecutiveDevices;
    /** Смещения описаний зон в БД */
    private Map<String, Integer> regionOffsets;
    /** Локальные номера зон */
    private Map<String, Integer> regionNos;
    /** Смещения описаний сценариев в БД */
    private Map<String, Integer> scenarioOffsets;
    /** Смещения указателей на описания сценариев в БД */
    private Map<String, Integer> scenarioPointerOffsets;
    /** Смещения описаний виртуальных состояния в БД */
    private Map<String, Integer> virtualStateOffsets;
    /** Смещения описаний шаблонов логики в БД */
    private Map<Integer, Integer> logicBlockOffsets;

    /**
     * Признак первого прохода формирования БД
     */
    private boolean firstPass;
    /** Списки номеров связанных шаблонов логики для зон */
    private Map<String, List<Integer>> regionLogicBlockUsages;
    /** Списки номеров связанных шаблонов логики для дочерних устройств */
    private Map<String, List<Integer>> childDeviceLogicBlockUsages;
    /** Списки номеров связанных шаблонов логики для виртуальных состояний */
    private Map<String, List<Integer>> virtualStateLogicBlockUsages;

    /**
     * Списки пар "номер сценария : номер исполнительного блока", где в условии
     * используются устройства
     */
    private Map<String, List<ActionBlockAddress>> childDeviceConditionUsages;
    /**
     * Списки пар "номер сценария : номер исполнительного блока", где в условии
     * используются виртуальные состояния
     */
    private Map<String, List<ActionBlockAddress>> virtualStateConditionUsages;

    /* Переменные, используемые для валидации БД */

    /**
     * Идентификаторы устройств, используемых в блоках действий сценариев
     */
    private Set<String> engagedInScenarioActionsDeviceIds;
    /** МПТ, не привязанные к зонам */
    private Set<String> mptNotInRegionDeviceIds;
    /** Зоны, содержащие более одного МПТ */
    private Set<String> multipleMptRegionIds;
    /** Идентификаторы датчиков, не добавленных в зону */
    private Set<String> noRegionSensorIds;
    /** Идентификаторы исполнительных устройств, которые не могут быть использованы в сценариях */
    private Set<String> illegalScenarioExecutiveIds;
    /** Идентификаторы виртуальных контейнеров, не содержащих необходимого количества устройств */
    private Set<String> notEnoughInternalDevicesVCIds;

    /**
     * Структура для хранения короткого пути устройства (с учетом контейнера)
     */
    private class ShortDevicePath {
        public int parentAddress;
        public int lineNo;
        public int lineAddress;
    }

    public DatabaseBuilder(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    public List<DataBlock> getDbBlocks(boolean recoveryNecessary) {
        List<DataBlock> blocks = new ArrayList<>();

        blocks.add(new DataBlock(DB_MAIN_OFFSET, dbMain));
        blocks.add(new DataBlock(DB_CONFIG_OFFSET, dbConfig));
        blocks.add(new DataBlock(DB_EXTRA_CONFIG_OFFSET, dbExtraConfig));
        if (recoveryNecessary)
            blocks.add(new DataBlock(DB_RECOVERY_OFFSET, dbRecovery));

        return blocks;
    }

    /**
     * Формирование списка диапазонов секторов, стираемых независимо от наполнения базы
     */
    @Override
    public List<SectorsRange> getSectorsToErase() {
        return new ArrayList<>();
    }

    public Integer getRegionNoById(String regionId) {
        return regionNos.get(regionId);
    }

    public String getRegionIdByLocalNo(int regionNo) {
        String result = null;
        for (Map.Entry<String, Integer> entry : regionNos.entrySet()) {
            if (entry.getValue() == regionNo) {
                result = entry.getKey();
                break;
            }
        }
        return result;
    }

    /**
     * Получить отсортированный список охранных и пожарных зон перед записью в БД
     *
     * Необходимо писать сначала охранные зоны по порядку нумерации, а затем аналогично пожарные.
     */
    private List<ActiveRegion> getSortedFireAndSecurityRegions() {
        List<ActiveRegion> regions = new ArrayList<>(stateMachineContext.getRegions());
        // Исключаем не охранные и не пожарные зоны из списка - в общую БД пишутся только они
        regions.removeIf(activeRegion ->
        activeRegion.getRegionProject().getSubsystem() != Subsystem.FIRE
                && activeRegion.getRegionProject().getSubsystem() != Subsystem.SECURITY);
        // Сортируем по индексу, но охранные идут вперёд
        regions.sort(Comparator.comparingLong(
                o -> o.getRegionProject().getIndex() +
                        (o.getRegionProject().getSubsystem() == Subsystem.SECURITY ? 0L : (long) Integer.MAX_VALUE)
        ));
        return regions;
    }

    /**
     * Дамп БД в файлы с заданным префиксом
     *
     * @param prefix
     *            префикс файлов
     */
    public void dumpDb(String prefix) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(new File(prefix + "_rubezh_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineNo()) + "_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineAddress())
                    + ".bin"));
            fos.write(dbMain);
            fos.close();
            fos = new FileOutputStream(new File(prefix + "_rubezh_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineNo()) + "_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineAddress())
                    + ".par"));
            fos.write(dbConfig);
            fos.close();
            fos = new FileOutputStream(new File(prefix + "_rubezh_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineNo()) + "_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineAddress())
                    + ".ext"));
            fos.write(dbExtraConfig);
            fos.close();
            fos = new FileOutputStream(new File(prefix + "_rubezh_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineNo()) + "_"
                    + Integer.toString(stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineAddress())
                    + ".rec"));
            fos.write(dbRecovery);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateDeviceConfig(String deviceId, Map<String, String> activeConfigValues) {
        ActiveDevice device = stateMachineContext.updateChildDeviceConfig(deviceId, activeConfigValues);
        if (device != null) {
            Device deviceProject = device.getDeviceProject();
            DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                    .get(deviceProject.getDeviceProfileId());
            if (deviceProfile != null) {
                if (updateExtraConfig(dbExtraConfig, deviceProject, deviceProfile))
                    return;

                ShortDevicePath shortDevicePath = getRealShortDevicePath(device);

                if ((shortDevicePath.lineNo - 1) == LINE_COUNT) {
                    // номер линии следующий за максимальным: перед нами насосная станция
                    byte[] vcTable = null;
                    try {
                        vcTable = getVirtualContainerTableEntry(device);
                    } catch (IOException e) {
                        stateMachineContext.getLogger().warn("VC table was not created" +
                                " after device config update: {}", deviceId);
                        return;
                    }
                    int vcTableOffset = localAndExternalChildDeviceOffsets.get(deviceId);
                    System.arraycopy(vcTable, 0, dbMain, vcTableOffset, vcTable.length);
                    recalculateCrc(dbMain, dbCrcOffset, DB_CRC_STUB.length, dbCrcOffset + 2, dbMain.length - 20);
                    return;
                }

                int configPointersListSize = LINE_COUNT * DEVICE_ON_LINE * EMPTY_MEMORY_ADDRESS.length;
                int deviceConfigOffset = deviceConfigOffsets[shortDevicePath.lineNo - 1][shortDevicePath.lineAddress - 1] -
                        DB_CONFIG_OFFSET - DB_CONFIG_CONFIGS_OFFSET +
                        CONFIG_SERIAL_STUB.length + CONFIG_SW_VERSION_STUB.length + CONFIG_PROTOCOL_VERSION_STUB.length +
                        configCrcOffset + 4 + configPointersListSize;

                byte[] result = dbConfig;

                byte[] rawConfig = new byte[deviceProfile.getConfigSize()];
                stateMachineContext.getChildDeviceConfigCodec().encodeDeviceProfileConfig(rawConfig,
                        deviceProfile.getConfigProperties(), deviceProject.getProjectConfigValues());

                System.arraycopy(rawConfig, 0, result, deviceConfigOffset, rawConfig.length);

                recalculateCrc(result, configCrcOffset, DB_CRC_STUB.length, configCrcOffset + 2, configCrcEndOffset);
                dbConfig = result;
            } else {
                stateMachineContext.getLogger().warn("Device profile not found: {}",
                        deviceProject.getDeviceProfileId());
            }
        } else {
            stateMachineContext.getLogger().warn("Active device not found: {}", deviceId);
        }
    }

    /**
     * Генерация бинарного представления хидера таблицы
     *
     * @param tableHeader
     *            хидер таблицы
     * @param isLongCount
     *            является ли поле количества элементов 2-х байтовым (иначе - 1
     *            байт)
     * @return 5 (если isLongCount == false) или 6 байт с закодированным хидером
     */
    private byte[] genTableHeader(TableHeader tableHeader, boolean isLongCount) {
        byte[] result = new byte[isLongCount ? 6 : 5];
        int address = tableHeader.getAddress();
        result[2] = (byte) (address & 0xFF);
        result[1] = (byte) ((address >> 8) & 0xFF);
        result[0] = (byte) ((address >> 16) & 0xFF);
        result[3] = (byte) (tableHeader.getEntryLength() & 0xFF);
        if (isLongCount) {
            result[5] = (byte) (tableHeader.getEntryCount() & 0xFF);
            result[4] = (byte) ((tableHeader.getEntryCount() >> 8) & 0xFF);
        } else {
            result[4] = (byte) (tableHeader.getEntryCount() & 0xFF);
        }
        return result;
    }

    /**
     * Метод, обрабатывающий логический блоки сценария и сохраняющий блок и все
     * его подблоки в линейный список. Возвращает индекс сохраненного в массиве
     * блока логики
     */
    private int wrapScenarioLogicBlocks(ScenarioLogicBlock logicBlock, ActiveScenario scenario, int depth,
            int parentIdx, boolean hasStopLogic, boolean isStartLogic) {
        ScenarioTriggerType triggerType = findScenarioTriggerType(logicBlock.getTriggerTypeId());
        List<ScenarioLogicBlock> subLogics = logicBlock.getSubLogics();
        boolean needFakeLogic = false;
        if (scenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_LOGIC && depth == 1
                && subLogics.size() == 0
                && (logicBlock.getTriggerTypeId().equals("") || (triggerType != null && triggerType.isFake()))) {
            // Для логики "запись в прибор" нам нужно создать фиктивный блок
            // логики, всегда ложный
            needFakeLogic = true;
        }
        if (needFakeLogic) {
            ScenarioLogicBlock mainBlock = new ScenarioLogicBlock();
            int idx = logicBlocks.size();
            LogicBlockWrapper mainWrapper = new LogicBlockWrapper(mainBlock, scenario, idx, depth, parentIdx,
                    hasStopLogic, isStartLogic);
            logicBlocks.add(mainWrapper);
            return idx;
        } else {
            if (subLogics.size() > 0 || (triggerType != null && !triggerType.isFake())) {
                int idx = logicBlocks.size();
                LogicBlockWrapper wrapper = new LogicBlockWrapper(logicBlock, scenario, idx, depth, parentIdx,
                        hasStopLogic, isStartLogic);
                logicBlocks.add(wrapper);

                for (ScenarioLogicBlock childLogicBlock : logicBlock.getSubLogics()) {
                    int childIdx = wrapScenarioLogicBlocks(childLogicBlock, scenario, depth + 1, idx, hasStopLogic,
                            isStartLogic);
                    if (childIdx != -1) {
                        wrapper.childIdxs.add(childIdx);
                    }
                }
                return idx;
            } else {
                return -1;
            }
        }
    }

    /**
     * Метод, который подготовит сценарии для записи
     */
    private void prepareScenarios() {
        // TODO: Выбирать сценарии, которые пишутся на этот прибор
        scenarios = new ArrayList<>();

        // Будем собирать информацию об использовании дочерних устройств и вирт.
        // состояний в условных блоках сценария
        childDeviceConditionUsages = new HashMap<>();
        virtualStateConditionUsages = new HashMap<>();

        int scenarioIdx = 0;
        for (ActiveScenario scenario : stateMachineContext.getScenarios()) {
            /* Не пишем управляющий сценарий, если не задана логика запуска. */
            Scenario scenarioProject = scenario.getScenarioProject();

            ActiveScenario scenarioCopy = null;
            if (scenarioProject.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS){
                scenarioCopy = new ActiveScenario(scenario.getScenarioProject());
                Scenario scenarioProjectCopy = scenarioCopy.getScenarioProject();
                scenarioProjectCopy.getStartLogic().getSubLogics().clear();
                scenarioProjectCopy.setTimeLineBlocks(scenarioProjectCopy.getTimeLineBlocks()
                        .stream()
                        .filter(tlBlock -> tlBlock.getBlockType() == ScenarioTimeLineBlock.BlockType.EXECUTIVE)
                        .collect(Collectors.toList()));
                for (ScenarioTimeLineBlock tlBlock: scenarioProjectCopy.getTimeLineBlocks()) {
                    if (tlBlock.getBlockType() != ScenarioTimeLineBlock.BlockType.EXECUTIVE){
                        scenarioProjectCopy.getTimeLineBlocks().remove(tlBlock);
                    }
                }
                if (!scenarioProjectCopy.getTimeLineBlocks().isEmpty()) {
                    ScenarioTimeLineBlock tlBlock = scenarioProjectCopy.getTimeLineBlocks().get(0);
                    scenarioProjectCopy.getTimeLineBlocks().clear();
                    scenarioProjectCopy.getTimeLineBlocks().add(tlBlock);
                }
                scenarioProjectCopy.setStopLogic(null);
                scenarioProjectCopy.getTimeLineBlocks().get(0).setTimeDelaySec(0);
                scenarioProjectCopy.getTimeLineBlocks().get(0).setConditionType(ScenarioTimeLineBlock.ConditionType.NONE);
            }

            /* Не пишем сценарий, если он выключен. */
            if (!scenario.getScenarioProject().isEnabled())
                continue;

            ScenarioWrapper wrapper;
            wrapper = (scenarioCopy != null)
                    ? new ScenarioWrapper(scenarioCopy)
                    : new ScenarioWrapper(scenario);
            scenarios.add(wrapper);

            for (String entityId : wrapper.childDeviceConditionUsages.keySet()) {
                List<Integer> blockIdxs = wrapper.childDeviceConditionUsages.get(entityId);

                if (!childDeviceConditionUsages.containsKey(entityId))
                    childDeviceConditionUsages.put(entityId, new ArrayList<>());
                for (Integer blockIdx : blockIdxs)
                    childDeviceConditionUsages.get(entityId).add(new ActionBlockAddress(scenarioIdx, blockIdx));
            }

            for (String entityId : wrapper.virtualStateConditionUsages.keySet()) {
                List<Integer> blockIdxs = wrapper.virtualStateConditionUsages.get(entityId);

                if (!virtualStateConditionUsages.containsKey(entityId))
                    virtualStateConditionUsages.put(entityId, new ArrayList<>());
                for (Integer blockIdx : blockIdxs)
                    virtualStateConditionUsages.get(entityId).add(new ActionBlockAddress(scenarioIdx, blockIdx));
            }

            scenarioIdx++;
        }
        scenarioOffsets = new HashMap<>();
        scenarioPointerOffsets = new HashMap<>();
        logicBlocks = new ArrayList<>();
        logicBlockOffsets = new HashMap<>();

        // Формируем блоки логики
        for (ScenarioWrapper scenarioWrapper : scenarios) {
            boolean hasStopLogic = (scenarioWrapper.scenario.getScenarioProject().getStopType() == StopType.BY_LOGIC
                    && scenarioWrapper.scenario.getScenarioProject().getStopLogic() != null);
            // Делаем сначала один фиктивный блок логики, который будет
            // проверять реальные условия как свои подблоки
            ScenarioLogicBlock startLogicWrapper = new ScenarioLogicBlock();
            startLogicWrapper.setLogicType(LogicType.OR);
            startLogicWrapper.getSubLogics().add(scenarioWrapper.scenario.getScenarioProject().getStartLogic());
            wrapScenarioLogicBlocks(startLogicWrapper, scenarioWrapper.scenario, 0, -1, hasStopLogic, true);
            if (hasStopLogic) {
                // Если есть логика выключения - делаем аналогично
                ScenarioLogicBlock stopLogicWrapper = new ScenarioLogicBlock();
                stopLogicWrapper.setLogicType(LogicType.OR);
                stopLogicWrapper.getSubLogics().add(scenarioWrapper.scenario.getScenarioProject().getStopLogic());
                wrapScenarioLogicBlocks(stopLogicWrapper, scenarioWrapper.scenario, 0, -1, hasStopLogic, false);
            }
        }

        // Сортируем блоги логики по вложенности
        Collections.sort(logicBlocks);
    }

    /**
     * Метод для подготовки всех структур перед генерацией БД
     */
    private void prepareDatabase() {
        deviceTableHeaders = new ArrayList<>(DEVICE_TABLE_COUNT);
        for (int i = 0; i < DEVICE_TABLE_COUNT; i++)
            deviceTableHeaders.add(new TableHeader());
        regionPointersTableHeader = new TableHeader();
        externalRegionsTableHeader = new TableHeader();
        externalExecutiveDevicePointersTableHeader = new TableHeader();
        monitorPointersTableHeader = new TableHeader();
        directionPointersTableHeader = new TableHeader();
        logicBlocksTableHeader = new TableHeader();
        scenarioPointersTableHeader = new TableHeader();
        lineDevicePointersTableHeaders = new TableHeader[LINE_COUNT];
        for (int i = 0; i < LINE_COUNT; i++)
            lineDevicePointersTableHeaders[i] = new TableHeader();
        localDeviceConfigPointersTableHeader = new TableHeader(DB_CONFIG_OFFSET + DB_CONFIG_ADDRESS_OFFSET,
                DB_ADDRESS_SIZE, LINE_COUNT * DEVICE_ON_LINE);
        externalScenariosTableHeader = new TableHeader();
        virtualContainersTableHeader = new TableHeader();
        virtualStatesTableHeader = new TableHeader();
        messagesTableHeader = new TableHeader();
        selfTableHeader = new TableHeader();
        scenarioLogicBlockPointersTableHeader = new TableHeader();

        dbHash = new byte[MD5_HASH_SIZE];
        dbSecHash = new byte[MD5_HASH_SIZE];
        dbEndAddress = 0;

        mptRegionDeviceIds = new HashMap<>();
        localAndExternalChildDeviceOffsets = new HashMap<>();
        sortedExternalExecutiveDevices = new ArrayList<>(stateMachineContext.getExternalExecutiveChildDevices());
        /*
         * Сортируем внешние ИУ по адресу прибора, номеру линии и собственному
         * адресу
         */
        sortedExternalExecutiveDevices.sort((activeDev0, activeDev1) -> {
            Device dev0 = activeDev0.getDeviceProject();
            Device dev1 = activeDev1.getDeviceProject();
            Device controlDev0 = stateMachineContext.getControlDevicesByChildDeviceIds().get(dev0.getId())
                    .getDeviceProject();
            Device controlDev1 = stateMachineContext.getControlDevicesByChildDeviceIds().get(dev1.getId())
                    .getDeviceProject();
            if (controlDev0 != controlDev1) {
                return controlDev0.getLineAddress() - controlDev1.getLineAddress();
            } else if (dev0.getLineNo() != dev1.getLineNo()) {
                return dev0.getLineNo() - dev1.getLineNo();
            } else {
                return dev0.getLineAddress() - dev1.getLineAddress();
            }
        });
        regionOffsets = new HashMap<>();
        regionNos = new HashMap<>();
        virtualStateOffsets = new HashMap<>();

        regionLogicBlockUsages = new HashMap<>();
        childDeviceLogicBlockUsages = new HashMap<>();
        virtualStateLogicBlockUsages = new HashMap<>();

        /* Переменные, используемые для валидации БД */
        engagedInScenarioActionsDeviceIds = stateMachineContext.getEngagedInScenarioActionsDeviceIds();
        mptNotInRegionDeviceIds = new HashSet<>();
        multipleMptRegionIds = new HashSet<>();
        noRegionSensorIds = new HashSet<>();
        illegalScenarioExecutiveIds = new HashSet<>();
        notEnoughInternalDevicesVCIds = new HashSet<>();

        prepareScenarios();
    }

    /**
     * Получение короткого адреса устройства, с учетом контейнера
     */
    private ShortDevicePath getRealShortDevicePath(ActiveDevice device) {
        ShortDevicePath shortDevicePath = new ShortDevicePath();
        shortDevicePath.parentAddress = device.getDeviceProject().getParentLineAddress();
        shortDevicePath.lineAddress = device.getDeviceProject().getLineAddress();
        shortDevicePath.lineNo = device.getDeviceProject().getLineNo();

        return shortDevicePath;
    }

    /**
     * Получение списка устройств, информация о которых записывается в БД
     * прибора, в паре с профилем устройства
     */
    private List<Pair<ActiveDevice, DeviceProfile>> getWriteableDevicesAndProfiles(List<ActiveDevice> activeDevices) {
        List<Pair<ActiveDevice, DeviceProfile>> result = new ArrayList<>();

        for (ActiveDevice activeDevice : activeDevices) {
            DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                    .get(activeDevice.getDeviceProject().getDeviceProfileId());
            // пропускаем устройства с отрицательным индексом - они пишутся в базу иначе или не пишутся вовсе
            if (deviceProfile == null || deviceProfile.getDbDeviceType() < 0)
                continue;
            // добавляем датчики и исполнительные устройства
            DeviceCategory deviceCategory = activeDevice.getDeviceProject().getDeviceCategory();
            if (deviceCategory == DeviceCategory.EXECUTIVE || deviceCategory == DeviceCategory.SENSOR) {
                result.add(Pair.of(activeDevice, deviceProfile));
            }
        }

        return result;
    }

    /**
     * Получение контейнера переданного устройства, или null
     */
    private ActiveDevice getContainerDevice(ActiveDevice device) {
        String parentDeviceId = device.getParentDeviceId();
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices())
            if (activeDevice.getId().equals(parentDeviceId)) {
                DeviceProfile parentDeviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                        .get(activeDevice.getDeviceProject().getDeviceProfileId());
                if (parentDeviceProfile != null && parentDeviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER)
                    return activeDevice;
            }
        return null;
    }

    /**
     * Получение отображения переданного устройства, или null
     */
    private DeviceProfileView getDeviceProfileView(Device device) {
        return stateMachineContext.getDeviceProfileViewsByIds().get(device.getDeviceProfileId());
    }

    /**
     * Формирование описания устройства по шаблону "[Имя] [Адрес
     * родителя].[Номер линии].[Адрес устройства]"
     */
    private String generateDeviceDescription(String name, ShortDevicePath shortDevicePath) {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" ");
        sb.append(shortDevicePath.parentAddress);
        sb.append(".");
        sb.append(shortDevicePath.lineNo);
        sb.append(".");
        sb.append(shortDevicePath.lineAddress);
        return sb.toString();
    }

    private void writeExecutives(ByteArrayOutputStream stream, List<Pair<ActiveDevice, DeviceProfile>> executiveDevices) throws IOException {
        for (Pair<ActiveDevice, DeviceProfile> pair : executiveDevices) {
            ActiveDevice device = pair.getFirst();
            DeviceProfile deviceProfile = pair.getSecond();
            byte[] rawDeviceEntry = getExecutiveDeviceTableEntry(device, deviceProfile);

            localAndExternalChildDeviceOffsets.put(device.getId(), stream.size());
            stream.write(rawDeviceEntry);
        }
    }

    private void writeExternalExecutives(ByteArrayOutputStream stream,
            List<Pair<ActiveDevice, DeviceProfile>> executiveDevices) throws IOException {
        for (Pair<ActiveDevice, DeviceProfile> pair : executiveDevices) {
            ActiveDevice device = pair.getFirst();
            DeviceProfile deviceProfile = pair.getSecond();
            byte[] rawDeviceEntry = getExternalExecutiveDeviceTableEntry(device, deviceProfile);

            localAndExternalChildDeviceOffsets.put(device.getId(), stream.size());
            stream.write(rawDeviceEntry);
        }
    }

    private class ExecutiveDeviceEntryOffsets {
        int variableConfigLengthOffset;
        int entryLengthOffset;
        int variableConfigBegin;
        int variableConfigEnd;
    }

    private byte[] getExecutiveDeviceTableEntry(ActiveDevice device, DeviceProfile deviceProfile) throws IOException {
        Device deviceProject = device.getDeviceProject();
        Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
        DeviceProfileView deviceProfileView = getDeviceProfileView(deviceProject);
        ActiveDevice containerDevice = getContainerDevice(device);
        ShortDevicePath shortDevicePath = getRealShortDevicePath(device);

        // Данные по устройству сначала пишем в отдельный бинарный массив,
        // чтобы потом подставить все переменные длины
        ByteArrayOutputStream deviceStream = new ByteArrayOutputStream();

        /*
         * Формируем запись таблицы ИУ, указывая в качестве адреса прибора 0
         * (для локальных ИУ)
         */
        ExecutiveDeviceEntryOffsets entryOffsets = writeExecutiveDeviceTableEntry(deviceProject, deviceProfile,
                deviceProfileView, options, containerDevice, shortDevicePath, (byte) 0x00, deviceStream);

        // Берем данные этого устройства как байтовый массив, высчитываем
        // все переменные размеры и подставляем
        byte[] rawDeviceEntry = deviceStream.toByteArray();
        int variableConfigLength = entryOffsets.variableConfigEnd - entryOffsets.variableConfigBegin;
        insertInteger(rawDeviceEntry, entryOffsets.variableConfigLengthOffset, variableConfigLength,
                TABLE_DEVICE_CONFIG_LENGTH_STUB.length);
        insertInteger(rawDeviceEntry, entryOffsets.entryLengthOffset, rawDeviceEntry.length,
                TABLE_EXECUTIVE_ENTRY_LENGTH_STUB.length);
        return rawDeviceEntry;
    }

    private byte[] getExternalExecutiveDeviceTableEntry(ActiveDevice device, DeviceProfile deviceProfile)
            throws IOException {
        Device deviceProject = device.getDeviceProject();
        Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
        DeviceProfileView deviceProfileView = getDeviceProfileView(deviceProject);
        ActiveDevice containerDevice = getContainerDevice(device);
        ShortDevicePath shortDevicePath = getRealShortDevicePath(device);

        // Данные по устройству сначала пишем в отдельный бинарный массив,
        // чтобы потом подставить все переменные длины
        ByteArrayOutputStream deviceStream = new ByteArrayOutputStream();

        /*
         * Пока нет поддержки адресных преобразователей протоколов, пишем только
         * тип внешнего ИУ
         */
        deviceStream.write(writeInteger(deviceProfile.getRsr3RawDeviceType(), TABLE_EXTERNAL_EXECUTIVE_TYPE_SIZE));

        /*
         * Формируем точно такую же запись, как и для обычных ИУ, только с
         * указанием действительного адреса прибора
         */
        Device controlDevice = stateMachineContext.getControlDevicesByChildDeviceIds().get(deviceProject.getId()).getDeviceProject();
        ExecutiveDeviceEntryOffsets entryOffsets = writeExecutiveDeviceTableEntry(deviceProject, deviceProfile,
                deviceProfileView, options, containerDevice, shortDevicePath,
                (byte) (controlDevice.getLineAddress() & 0xFF), deviceStream);

        // Берем данные этого устройства как байтовый массив, высчитываем
        // все переменные размеры и подставляем
        byte[] rawDeviceEntry = deviceStream.toByteArray();
        int variableConfigLength = entryOffsets.variableConfigEnd - entryOffsets.variableConfigBegin;
        insertInteger(rawDeviceEntry, entryOffsets.variableConfigLengthOffset, variableConfigLength,
                TABLE_DEVICE_CONFIG_LENGTH_STUB.length);
        insertInteger(rawDeviceEntry, entryOffsets.entryLengthOffset, rawDeviceEntry.length,
                TABLE_EXECUTIVE_ENTRY_LENGTH_STUB.length);
        return rawDeviceEntry;
    }

    private ExecutiveDeviceEntryOffsets writeExecutiveDeviceTableEntry(Device deviceProject,
            DeviceProfile deviceProfile,
            DeviceProfileView deviceProfileView, Set<ControlDeviceDatabaseOptionType> options,
            ActiveDevice containerDevice, ShortDevicePath shortDevicePath, byte controlDeviceAddress,
            ByteArrayOutputStream deviceStream)
            throws IOException {

        ExecutiveDeviceEntryOffsets entryOffsets = new ExecutiveDeviceEntryOffsets();

        // Адрес прибора привязки в сети (0 для локальной)
        deviceStream.write(controlDeviceAddress);
        // Адрес или номер АСПТ
        deviceStream.write(writeInteger(shortDevicePath.lineAddress, 1));
        // Шлейф привязки
        if (deviceProject.getAddressType() == AddressType.INTERNAL)
            deviceStream.write(writeInteger(TABLE_INTERNAL_DEVICE_LINE_NO, 1));
        else
            deviceStream.write(writeInteger(shortDevicePath.lineNo - 1, 1));
        // Описание
        String deviceName = DeviceProperty.getString(deviceProject, DeviceProperties.AlternateName);
        if (deviceName == null)
            deviceName = (deviceProfileView != null) ? deviceProfileView.getName() : TABLE_EXECUTIVE_DEFAULT_NAME;
        deviceStream.write(
                writeString(generateDeviceDescription(deviceName, shortDevicePath), DEVICE_DESCRIPTION_SIZE));
        entryOffsets.variableConfigLengthOffset = deviceStream.size();
        deviceStream.write(TABLE_DEVICE_CONFIG_LENGTH_STUB);
        entryOffsets.entryLengthOffset = deviceStream.size();
        deviceStream.write(TABLE_EXECUTIVE_ENTRY_LENGTH_STUB);
        entryOffsets.variableConfigBegin = deviceStream.size();
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_UNDELIVERABLE_CONFIG)) {
            writeExecutiveUndeliverableConfig(deviceProject, deviceProfile, containerDevice, options, deviceStream);
        }
        writeDeviceLogicBlockReferences(deviceProject, deviceStream);
        // Виртуальный тип
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_VIRTUAL_TYPE))
            deviceStream.write(writeInteger(deviceProfileView.getDeviceProfile().getRsr3VirtualDeviceType(),
                    TABLE_VIRTUAL_TYPE_SIZE));
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_ON_OFF_DESCRIPTIONS)) {
            writeExecutiveOnOffDescriptions(deviceProject, deviceStream);
        }
        if (options.contains(ControlDeviceDatabaseOptionType.DEVICE_IS_MPT)) {
            writeMPTSpecific(deviceProject, deviceStream);
        }
        // Номер направления
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_DIRECTION_NUMBER)){
            deviceStream.write(writeInteger(0, TABLE_DIRECTION_NO_SIZE));
        }
        // Зона устройства
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_REGION)){
            String regionId = deviceProject.getRegionId();
            int regionNo = regionNos.getOrDefault(regionId, 0);
            deviceStream.write(writeInteger(regionNo, TABLE_REGION_NO_SIZE));
        }
        // Виртуальный тип из параметра конфигурации
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_VIRTUAL_TYPE_CONFIG_PROPERTY)){
            // NOTE: старый файрсек пишет сюда 0, а значит на данный момент наш код ему соответсвует
            // NOTE: но на самом деле, виртуальный тип МКД (0x98..0x9B) надо извлекать из конфиг. параметра KOD2Mode
            int virtualType = DeviceConfigProperty.getInteger(deviceProject, deviceProfile, DeviceProperties.VirtualType);
            deviceStream.write(writeInteger(virtualType, TABLE_VIRTUAL_TYPE_SIZE));
        }

        entryOffsets.variableConfigEnd = deviceStream.size();

        // Ссылка на контейнер в который входит устройство или «0»
        int virtualContainerOffset = 0;
        if (deviceProject.getVirtualContainerId() != null)
            virtualContainerOffset = localAndExternalChildDeviceOffsets.getOrDefault(
                    deviceProject.getVirtualContainerId(), 0);
        deviceStream.write(writeInteger(virtualContainerOffset, EMPTY_MEMORY_ADDRESS.length));

        return entryOffsets;
    }

    private void writeDeviceLogicBlockReferences(Device deviceProject, ByteArrayOutputStream deviceStream)
            throws IOException {
        // Количество связанных шаблонов логик
        List<Integer> logicBlockRefs = childDeviceLogicBlockUsages.getOrDefault(deviceProject.getId(), new ArrayList<>());
        deviceStream.write(writeInteger(logicBlockRefs.size(), TABLE_DEVICE_LOGIC_BLOCK_REF_COUNT_SIZE));
        for (Integer logicBlockIdx : logicBlockRefs) {
            int address = logicBlockOffsets.getOrDefault(logicBlockIdx, 0);
            deviceStream.write(writeInteger(address, EMPTY_MEMORY_ADDRESS.length));
        }
    }

    private void writeMPTSpecific(Device deviceProject, ByteArrayOutputStream deviceStream)
            throws IOException {
        // Номер привязанной зоны (2)
        String regionId = deviceProject.getRegionId();
        if (regionId == null) {
            mptNotInRegionDeviceIds.add(deviceProject.getId());
            deviceStream.write(writeInteger(0, TABLE_REGION_NO_SIZE));
        } else {
            /*
             * На первом проходе определяем кол-во зон, в которые
             * добавлено МПТ
             */
            if (firstPass) {
                if (mptRegionDeviceIds.containsKey(deviceProject.getRegionId()))
                    multipleMptRegionIds.add(deviceProject.getRegionId());
                else
                    mptRegionDeviceIds.put(deviceProject.getRegionId(), deviceProject.getId());
            }
            int regionNo = regionNos.getOrDefault(deviceProject.getRegionId(), 0);
            deviceStream.write(writeInteger(regionNo, TABLE_REGION_NO_SIZE));
        }
        // TODO: определять линию и адрес родительского МПТ (ведущего?)
        // Адрес родителя (1)
        deviceStream.write(writeInteger(0, 1));
        // Шлейф родителя (1)
        deviceStream.write(writeInteger(0, 1));
        // Количество связанных блоков времени или «0» (2)
        List<ActionBlockAddress> conditionUsages = childDeviceConditionUsages.getOrDefault(deviceProject.getId(),
                new ArrayList<>());
        deviceStream.write(writeInteger(conditionUsages.size(), EMPTY_COUNTER.length));
        for (ActionBlockAddress actionBlockAddress : conditionUsages) {
            // Локальный номер сценария, где есть условие с МПТ (с «1») (2)
            deviceStream.write(writeInteger(actionBlockAddress.scenarioIdx + 1, EMPTY_COUNTER.length));
            // Номер блока времени в сценарии, где есть условие с МПТ  (с «0») (2)
            deviceStream.write(writeInteger(actionBlockAddress.blockIdx, EMPTY_COUNTER.length));
        }
    }

    private void writeExecutiveOnOffDescriptions(Device deviceProject, ByteArrayOutputStream deviceStream)
            throws IOException {
        // Описание состояния Вкл (по умолчанию: Вкл)
        String enabledMessage = DeviceProperty.getString(deviceProject, DeviceProperties.EnabledMessage);
        if (enabledMessage == null)
            enabledMessage = TABLE_EXECUTIVE_DEFAULT_ON_TITLE;
        deviceStream.write(writeString(enabledMessage, DEVICE_DESCRIPTION_SIZE));
        // Описание состояния Выкл (по умолчанию: Выкл)
        String disabledMessage = DeviceProperty.getString(deviceProject, DeviceProperties.DisabledMessage);
        if (disabledMessage == null)
            disabledMessage = TABLE_EXECUTIVE_DEFAULT_OFF_TITLE;
        deviceStream.write(writeString(disabledMessage, DEVICE_DESCRIPTION_SIZE));
    }

    private void writeExecutiveUndeliverableConfig(Device deviceProject, DeviceProfile deviceProfile,
            ActiveDevice containerDevice, Set<ControlDeviceDatabaseOptionType> options,
            ByteArrayOutputStream deviceStream) {
        byte undeliverableConfig = 0x00;
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_LIGHT_AND_SOUND_BIT)) {
            if (DeviceProperty.getBoolean(deviceProject, deviceProfile, DeviceProperties.LightAndSoundAlarm))
                undeliverableConfig |= TABLE_DEVICE_LIGHT_AND_SOUND_BIT;
        }
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_DEVICE_FAILURE_FEATURE)) {
            if (DeviceProperty.getBoolean(deviceProject, deviceProfile, DeviceProperties.DeviceFailureFeature))
                undeliverableConfig |= TABLE_DEVICE_FAILURE_FEATURE_BIT;
        }
        if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_TWO_REGIONS_FIRE_FEATURE)) {
            if (DeviceProperty.getBoolean(deviceProject, deviceProfile, DeviceProperties.TwoRegionsFireFeature)) {
                undeliverableConfig |= TABLE_TWO_REGIONS_FIRE_FEATURE_BIT;
                illegalScenarioExecutiveIds.add(deviceProject.getId());
            }
        }
        if (containerDevice != null) {
            int addressOffset = deviceProject.getLineAddress()
                    - containerDevice.getDeviceProject().getLineAddress();
            if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_FIRST_ADDRESS_BIT)
                    && addressOffset == 0)
                undeliverableConfig |= TABLE_DEVICE_CONTAINER_FIRST_DEVICE_BIT;
            if (options.contains(ControlDeviceDatabaseOptionType.EXECUTIVE_HAS_ADDRESS_OFFSET))
                undeliverableConfig |= (byte) ((addressOffset & 0x0f) << TABLE_DEVICE_CONTAINER_OFFSET_BIT);
        }
        if (options.contains(ControlDeviceDatabaseOptionType.DEVICE_IS_MPT)) {
            // TODO: проверять режим МПТ
            undeliverableConfig |= TABLE_MPT_IS_MASTER_BIT;
        }
        deviceStream.write(undeliverableConfig);
    }

    private void writeSensors(ByteArrayOutputStream stream, List<Pair<ActiveDevice, DeviceProfile>> sensorDevices) throws IOException {
        for (Pair<ActiveDevice, DeviceProfile> pair : sensorDevices) {
            // Данные по устройству сначала пишем в отдельный бинарный массив,
            // чтобы потом подставить все переменные длины
            ByteArrayOutputStream deviceStream = new ByteArrayOutputStream();

            ActiveDevice device = pair.getFirst();
            Device deviceProject = device.getDeviceProject();
            Set<ControlDeviceDatabaseOptionType> options = pair.getSecond().getControlDeviceDatabaseOptionTypes();
            DeviceProfileView deviceProfileView = getDeviceProfileView(deviceProject);
            ActiveDevice containerDevice = getContainerDevice(device);

            ShortDevicePath shortDevicePath = getRealShortDevicePath(device);

            // Адрес
            deviceStream.write(writeInteger(shortDevicePath.lineAddress, 1));
            // Шлейф привязки
            if (deviceProject.getAddressType() == AddressType.INTERNAL)
                deviceStream.write(writeInteger(TABLE_INTERNAL_DEVICE_LINE_NO, 1));
            else
                deviceStream.write(writeInteger(shortDevicePath.lineNo - 1, 1));
            // Номер зоны
            int regionNo = regionNos.getOrDefault(deviceProject.getRegionId(), 0);

            if (deviceProfileView.getDeviceProfile().isAttachableToRegion()) {
                if (regionNo == 0) {
                    /* Запоминаем идентификатор датчика без зоны */
                    noRegionSensorIds.add(deviceProject.getId());
                } else {
                    /*
                     * Удаляем идентификатор датчика, если он был добавлен на
                     * предыдущих проходах
                     */
                    noRegionSensorIds.remove(deviceProject.getId());
                }
            }

            deviceStream.write(writeInteger(regionNo, TABLE_REGION_NO_SIZE));
            // Длина блока данных устройства
            int variableConfigLengthOffset = deviceStream.size();
            deviceStream.write(TABLE_DEVICE_CONFIG_LENGTH_STUB);
            // Описание
            String deviceName = DeviceProperty.getString(deviceProject, DeviceProperties.AlternateName);
            if (deviceName == null)
                deviceName = (deviceProfileView != null) ? deviceProfileView.getName() : TABLE_SENSOR_DEFAULT_NAME;
            deviceStream.write(
                    writeString(generateDeviceDescription(deviceName, shortDevicePath), DEVICE_DESCRIPTION_SIZE));
            int variableConfigBegin = deviceStream.size();

            writeDeviceLogicBlockReferences(deviceProject, deviceStream);
            // Виртуальный тип
            if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_VIRTUAL_TYPE))
                deviceStream.write(writeInteger(deviceProfileView.getDeviceProfile().getRsr3VirtualDeviceType(),
                        TABLE_VIRTUAL_TYPE_SIZE));
            // Недоставляемая конфигурация
            if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_UNDELIVERABLE_CONFIG)) {
                byte undeliverableConfig = 0x00;
                // TODO: Проверять SENSOR_HAS_FEP_BIT
                if (containerDevice != null) {
                    int addressOffset = deviceProject.getLineAddress()
                            - containerDevice.getDeviceProject().getLineAddress();
                    if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_FIRST_ADDRESS_BIT)
                            && addressOffset == 0)
                        undeliverableConfig |= TABLE_DEVICE_CONTAINER_FIRST_DEVICE_BIT;
                    if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_ADDRESS_OFFSET))
                        undeliverableConfig |= (byte) ((addressOffset & 0x0f) << TABLE_DEVICE_CONTAINER_OFFSET_BIT);
                }
                deviceStream.write(undeliverableConfig);
            }
            if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_FOUR_STATE_DESCRIPTIONS)) {
                // Описание состояния Норма
                String noOneTriggeredMessage = DeviceProperty.getString(deviceProject,
                        DeviceProperties.NoOneTriggeredMessage);
                if (noOneTriggeredMessage == null)
                    noOneTriggeredMessage = TABLE_SENSOR_DEFAULT_NONE_TITLE;
                deviceStream.write(writeString(noOneTriggeredMessage, DEVICE_DESCRIPTION_SIZE));
                // Описание состояния Сработка 1го датчика
                String firstTriggeredMessage = DeviceProperty.getString(deviceProject,
                        DeviceProperties.FirstTriggeredMessage);
                if (firstTriggeredMessage == null)
                    firstTriggeredMessage = TABLE_SENSOR_DEFAULT_FIRST_TITLE;
                deviceStream.write(writeString(firstTriggeredMessage, DEVICE_DESCRIPTION_SIZE));
                // Описание состояния Сработка 2го датчика
                String secondTriggeredMessage = DeviceProperty.getString(deviceProject,
                        DeviceProperties.SecondTriggeredMessage);
                if (secondTriggeredMessage == null)
                    secondTriggeredMessage = TABLE_SENSOR_DEFAULT_SECOND_TITLE;
                deviceStream.write(writeString(secondTriggeredMessage, DEVICE_DESCRIPTION_SIZE));
                // Описание состояния Сработка 2х датчиков
                String bothTriggeredMessage = DeviceProperty.getString(deviceProject,
                        DeviceProperties.BothTriggeredMessage);
                if (bothTriggeredMessage == null)
                    bothTriggeredMessage = TABLE_SENSOR_DEFAULT_BOTH_TITLE;
                deviceStream.write(writeString(bothTriggeredMessage, DEVICE_DESCRIPTION_SIZE));
            }
            // Количество связанных блоков времени или "0"
            if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_HAS_ASSOCIATED_TIMELINE_BLOCKS)) {
                List<ActionBlockAddress> conditionUsages = childDeviceConditionUsages
                        .getOrDefault(deviceProject.getId(), new ArrayList<>());
                deviceStream.write(writeInteger(conditionUsages.size(), EMPTY_COUNTER.length));
                for (ActionBlockAddress actionBlockAddress : conditionUsages) {
                    deviceStream.write(writeInteger(actionBlockAddress.scenarioIdx + 1, EMPTY_COUNTER.length));
                    deviceStream.write(writeInteger(actionBlockAddress.blockIdx, EMPTY_COUNTER.length));
                }
            }
            int variableConfigEnd = deviceStream.size();

            // Ссылка на контейнер в который входит устройство или «0»
            int virtualContainerOffset = 0;
            if (deviceProject.getVirtualContainerId() != null)
                virtualContainerOffset = localAndExternalChildDeviceOffsets.getOrDefault(
                        deviceProject.getVirtualContainerId(), 0);
            deviceStream.write(writeInteger(virtualContainerOffset, EMPTY_MEMORY_ADDRESS.length));

            // Берем данные этого устройства как байтовый массив, высчитываем
            // все переменные размеры и подставляем
            byte[] rawDeviceEntry = deviceStream.toByteArray();
            int variableConfigLength = variableConfigEnd - variableConfigBegin;
            insertInteger(rawDeviceEntry, variableConfigLengthOffset, variableConfigLength,
                    TABLE_DEVICE_CONFIG_LENGTH_STUB.length);

            localAndExternalChildDeviceOffsets.put(device.getId(), stream.size());
            stream.write(rawDeviceEntry);
        }
    }

    /**
     * Запись всех устройств в порядке хидеров таблиц устройств по типам
     */
    private void writeDevicesTable(ByteArrayOutputStream stream) throws IOException {
        // устройства, записываемые в базу прибора, сгруппированные по типу (индексу в базе)
        // используется TreeMap, чтобы далее получать список ключей в отсортированном виде
        Map<Integer, List<Pair<ActiveDevice, DeviceProfile>>> devicesByTableIndex = new TreeMap<>();
        List<Pair<ActiveDevice, DeviceProfile>> devices = getWriteableDevicesAndProfiles(
                stateMachineContext.getChildDevices());
        for (Pair<ActiveDevice, DeviceProfile> pair : devices) {
            Integer tableIndex = pair.getSecond().getDbDeviceType();
            if (!devicesByTableIndex.containsKey(tableIndex))
                devicesByTableIndex.put(tableIndex, new ArrayList<>());
            devicesByTableIndex.get(tableIndex).add(pair);
        }

        /* добавляем внешние исполнительные устройства, у них свой индекс */
        if (!sortedExternalExecutiveDevices.isEmpty()) {
            devicesByTableIndex.put(TABLE_EXTERNAL_EXECUTIVE_INDEX,
                    getWriteableDevicesAndProfiles(sortedExternalExecutiveDevices));
        }


        for (Integer tableIndex : devicesByTableIndex.keySet()) {
            // все активные устройства данного типа
            devices = devicesByTableIndex.get(tableIndex);

            // заполнение хидера таблицы устройств данного типа
            TableHeader tableHeader = deviceTableHeaders.get(tableIndex);
            tableHeader.setAddress(stream.size());
            tableHeader.setEntryCount(devices.size());
            tableHeader.setEntryLength(0);

            if (tableIndex.equals(TABLE_EXTERNAL_EXECUTIVE_INDEX)) {
                writeExternalExecutives(stream, devices);
            } else {
                /*
                 * Сортируем устройства по номеру линии и адресу, чтобы порядок
                 * следования устройств здесь совпадал с порядком следования
                 * указателей на них в таблице указателей (одно из требований к
                 * БД Рубеж-2ОП3)
                 */
                devices.sort((arg0, arg1) -> {
                    Device dev0 = arg0.getFirst().getDeviceProject();
                    Device dev1 = arg1.getFirst().getDeviceProject();
                    if (dev0.getLineNo() != dev1.getLineNo()) {
                        return dev0.getLineNo() - dev1.getLineNo();
                    } else {
                        return dev0.getLineAddress() - dev1.getLineAddress();
                    }
                });
                DeviceCategory category = devices.get(0).getSecond().getDeviceCategory();

                // запись таблицы устройств данного типа
                switch (category) {
                case SENSOR:
                    writeSensors(stream, devices);
                    break;
                case EXECUTIVE:
                    writeExecutives(stream, devices);
                    break;
                default:
                    stateMachineContext.getLogger().error("Can not write device category: {}", category);
                    break;
                }
            }
        }
    }

    /**
     * Запись таблицы указателей на зоны
     */
    private void writeRegionPointersTable(ByteArrayOutputStream stream) throws IOException {
        List<ActiveRegion> regions = getSortedFireAndSecurityRegions();
        regionPointersTableHeader.setAddress(stream.size());
        regionPointersTableHeader.setEntryCount(regions.size());
        regionPointersTableHeader.setEntryLength(0);
        for (ActiveRegion region : regions)
            stream.write(writeInteger(regionOffsets.getOrDefault(region.getId(), 0), EMPTY_MEMORY_ADDRESS.length));
    }

    /**
     * Запись таблицы внешних для прибора зон
     */
    private void writeExternalRegionsTable(ByteArrayOutputStream stream) throws IOException {
        externalRegionsTableHeader.setAddress(stream.size());
        // TODO: Писать внешние зоны
    }

    /**
     * Запись таблицы устройств-мониторов
     */
    private void writeMonitorPointersTable(ByteArrayOutputStream stream) throws IOException {
        monitorPointersTableHeader.setAddress(stream.size());
        monitorPointersTableHeader.setEntryCount(MONITOR_POINTER_COUNT);
        monitorPointersTableHeader.setEntryLength(MONITOR_POINTER_SIZE);
        for (int i = 0; i < MONITOR_POINTER_COUNT; ++i) {
            stream.write(0x00);
        }
    }

    /**
     * Поиск триггера сценария по имени
     */
    private ScenarioTriggerType findScenarioTriggerType(String triggerTypeId) {
        for (ScenarioTriggerType triggerType : stateMachineContext.getDriverProfile().getScenarioTriggerTypes())
            if (triggerType.getId().equals(triggerTypeId))
                return triggerType;
        return null;
    }

    /**
     * Поиск состояния устройства по идентификаторам устройства и состояния
     */
    private State findDeviceState(String deviceId, String stateId) {
        ActiveDevice device = findChildDevice(deviceId);
        State deviceState = null;
        if (device != null) {
            DeviceProfile profile = stateMachineContext.getSupportedDeviceProfiles().get(
                    device.getDeviceProject().getDeviceProfileId());
            if (profile != null) {
                for (State state : profile.getCustomMaskedStates()) {
                    if (state.getId().equals(stateId)) {
                        deviceState = state;
                        break;
                    }
                }
            }
        }

        return deviceState;
    }

    /**
     * Запись одного шаблона логики
     */
    private void writeLogicBlockWrapper(ByteArrayOutputStream stream, LogicBlockWrapper wrapper) throws IOException {
        ScenarioLogicBlock logicBlock = wrapper.logicBlock;
        ScenarioTriggerType triggerType = findScenarioTriggerType(logicBlock.getTriggerTypeId());
        int idx = wrapper.idx;
        logicBlockOffsets.put(idx, stream.size());

        byte config = 0x00;
        {
            ScenarioLogicBlock.LogicType logicType = logicBlock.getLogicType();
            if (logicType == LogicType.AND || logicType == LogicType.AND_NOT)
                config |= TABLE_LOGIC_BLOCK_CONFIG_AND;
            if (logicType == LogicType.AND_NOT || logicType == LogicType.OR_NOT)
                config |= TABLE_LOGIC_BLOCK_CONFIG_NOT;
            if (wrapper.depth == 0) {
                if (wrapper.isStartLogic)
                    config |= TABLE_LOGIC_BLOCK_CONFIG_START_LOGIC;
                if (wrapper.hasStopLogic)
                    config |= TABLE_LOGIC_BLOCK_CONFIG_HAS_STOP_LOGIC;
            }
            config |= (wrapper.depth << TABLE_LOGIC_BLOCK_CONFIG_DEPTH_OFFSET);
        }
        stream.write(config);
        stream.write(new byte[] { 0x00, 0x00 });

        // Есть ли подблоки
        boolean hasSubBlocks = (wrapper.childIdxs.size() > 0);
        // Является ли шаблон фейковым
        boolean isFake = (!hasSubBlocks && triggerType == null);

        // Код проверяемого состояния
        byte triggerByte = 0x00;
        // Количество проверяемых сущностей
        int childCount = 0;
        // Адрес родительского шаблона или сценария
        int parentOffset = 0x00;

        /* Идентификаторы сущностей, принадлежащих прибору */
        Set<String> ownEntityIds = new HashSet<>();
        if (hasSubBlocks) {
            triggerByte = TABLE_LOGIC_BLOCK_TRIGGER_SUB_BLOCKS;
            childCount = wrapper.childIdxs.size();
        } else if (triggerType != null) {
            triggerByte = (byte) triggerType.getCode();
            String ownControlDeviceId = stateMachineContext.getRubezh2op3Device().getId();
            for (String entityId : logicBlock.getEntityIds()) {
                switch (triggerType.getEntityType()) {
                case CONTROL_DEVICE:
                    if (ownControlDeviceId.equals(entityId))
                        ownEntityIds.add(entityId);
                    break;
                case DEVICE:
                    if (findChildDevice(entityId) != null)
                        ownEntityIds.add(entityId);
                    break;
                case REGION:
                    if (findRegion(entityId) != null)
                        ownEntityIds.add(entityId);
                    break;
                case VIRTUAL_STATE: {
                    VirtualState virtualState = findVirtualState(entityId);
                    if (virtualState != null && virtualState.getDeviceId().equals(ownControlDeviceId))
                        ownEntityIds.add(entityId);
                    break;
                }
                }
            }
            childCount = ownEntityIds.size();
        }
        if (wrapper.parentIdx == -1)
            parentOffset = scenarioOffsets.getOrDefault(wrapper.scenario.getId(), 0);
        else
            parentOffset = logicBlockOffsets.getOrDefault(wrapper.parentIdx, 0);

        if (isFake) {
            triggerByte = 0x00;
            childCount = 1;
        }

        stream.write(triggerByte);
        stream.write(writeInteger(childCount, TABLE_LOGIC_BLOCK_CHILD_COUNT_SIZE));
        stream.write(writeInteger(parentOffset, EMPTY_MEMORY_ADDRESS.length));
        if (isFake) {
            stream.write(EMPTY_MEMORY_ADDRESS);
        } else if (hasSubBlocks) {
            for (int subBlockIdx : wrapper.childIdxs)
                stream.write(writeInteger(logicBlockOffsets.getOrDefault(subBlockIdx, 0), EMPTY_MEMORY_ADDRESS.length));
        } else {
            /* Пишем только принадлежащие прибору объекты */
            for (String entityId : ownEntityIds) {
                int entityOffset = 0;
                if (triggerType != null)
                    switch (triggerType.getEntityType()) {
                    case DEVICE:
                        entityOffset = localAndExternalChildDeviceOffsets.getOrDefault(entityId, 0);
                        if (firstPass) {
                            if (!childDeviceLogicBlockUsages.containsKey(entityId))
                                childDeviceLogicBlockUsages.put(entityId, new ArrayList<>());
                            childDeviceLogicBlockUsages.get(entityId).add(wrapper.idx);
                        }
                        break;
                    case REGION:
                        entityOffset = regionOffsets.getOrDefault(entityId, 0);
                        if (firstPass) {
                            if (!regionLogicBlockUsages.containsKey(entityId))
                                regionLogicBlockUsages.put(entityId, new ArrayList<>());
                            regionLogicBlockUsages.get(entityId).add(wrapper.idx);
                        }
                        break;
                    case CONTROL_DEVICE:
                        break;
                    case VIRTUAL_STATE:
                        entityOffset = virtualStateOffsets.getOrDefault(entityId, 0);
                        if (firstPass) {
                            if (!virtualStateLogicBlockUsages.containsKey(entityId))
                                virtualStateLogicBlockUsages.put(entityId, new ArrayList<>());
                            virtualStateLogicBlockUsages.get(entityId).add(wrapper.idx);
                        }
                        break;
                    }
                stream.write(writeInteger(entityOffset, EMPTY_MEMORY_ADDRESS.length));
            }
        }
    }

    /**
     * Запись таблицы шаблонов логики
     */
    private void writeLogicBlocksTable(ByteArrayOutputStream stream) throws IOException {
        logicBlocksTableHeader.setAddress(stream.size());
        logicBlocksTableHeader.setEntryLength(0);
        if (logicBlocks.size() > 0) {
            // Для шаблонов нулевого уровня вложенности - мы их подсчитываем в
            // таблице
            int zeroLevelCounter = 0;
            for (LogicBlockWrapper wrapper : logicBlocks)
                if (wrapper.depth == 0) {
                    zeroLevelCounter++;
                    writeLogicBlockWrapper(stream, wrapper);
                }
            logicBlocksTableHeader.setEntryCount(zeroLevelCounter);

            // Для шаблонов с уровнем больше нулевого - просто пишем после нулевых
            for (LogicBlockWrapper wrapper : logicBlocks)
                if (wrapper.depth != 0)
                    writeLogicBlockWrapper(stream, wrapper);
        }
    }

    /**
     * Запись таблицы указателей на сценарии
     */
    private void writeScenarioPointersTable(ByteArrayOutputStream stream) throws IOException {
        if (scenarios.size() > 0) {
            scenarioPointersTableHeader.setAddress(stream.size());
            scenarioPointersTableHeader.setEntryCount(scenarios.size());
            scenarioPointersTableHeader.setEntryLength(0);
            for (ScenarioWrapper scenarioWrapper : scenarios) {
                String id = scenarioWrapper.scenario.getId();
                scenarioPointerOffsets.put(id, stream.size());
                stream.write(writeInteger(scenarioOffsets.getOrDefault(id, 0), EMPTY_MEMORY_ADDRESS.length));
            }
        }
    }

    /**
     * Запись таблицы указателей на шаблоны логики
     */
    private void writeScenarioLogicBlockPointersTable(ByteArrayOutputStream stream) throws IOException {
        if (logicBlocks.size() > 0) {
            scenarioLogicBlockPointersTableHeader.setAddress(stream.size());
            scenarioLogicBlockPointersTableHeader.setEntryLength(0);
            scenarioLogicBlockPointersTableHeader.setEntryCount(logicBlocks.size());

            for (LogicBlockWrapper wrapper : logicBlocks)
                stream.write(writeInteger(logicBlockOffsets.getOrDefault(wrapper.idx, 0), EMPTY_MEMORY_ADDRESS.length));
        }
    }

    /**
     * Поиск типа действия по идентификатору
     */
    private ScenarioActionType findActionType(String actionTypeId) {
        for (ScenarioActionType actionType : stateMachineContext.getDriverProfile().getScenarioActionTypes())
            if (actionType.getId().equals(actionTypeId))
                return actionType;
        return null;
    }

    /**
     * Поиск устройства по идентификатору среди дочерних
     */
    private ActiveDevice findChildDevice(String childDeviceId) {
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices())
            if (activeDevice.getId().equals(childDeviceId))
                return activeDevice;
        return null;
    }

    /** Поиск зоны по идентификатору среди приндлежащих прибору */
    private ActiveRegion findRegion(String regionId) {
        for (ActiveRegion activeRegion : stateMachineContext.getRegions())
            if (activeRegion.getId().equals(regionId))
                return activeRegion;
        return null;
    }

    /** Поиск виртуального состояния по идентификатору */
    private VirtualState findVirtualState(String virtualStateId) {
        for (VirtualState virtualState : stateMachineContext.getVirtualStates())
            if (virtualState.getId().equals(virtualStateId))
                return virtualState;
        return null;
    }

    /** Поиск устройства по идентификатору среди внешних ИУ */
    private ActiveDevice findExternalExecutiveDevice(String deviceId) {
        for (ActiveDevice activeDevice : stateMachineContext.getExternalExecutiveChildDevices())
            if (activeDevice.getId().equals(deviceId))
                return activeDevice;
        return null;
    }

    /**
     * Запись исполнительного блока сценария
     */
    private void writeScenarioTimeLineBlock(ScenarioWrapper scenarioWrapper, ByteArrayOutputStream stream,
            ScenarioTimeLineBlockRepr block)
            throws IOException {
        /*
         * Распределим действия по линиям, включая последний массив с действиями
         * над внешними ИУ, зонами, вирт. состояниями и пр.
         */
        List<List<ScenarioTimeLineBlockAction>> actionsByLine = getScenarioTimeLineBlockActionsByLine(scenarioWrapper,
                block);

        for (List<ScenarioTimeLineBlockAction> actionList : actionsByLine) {
            /*
             * Обрабатываем ли мы массив с действиями над внешними ИУ, зонами,
             * вирт. состояниями и т.д.
             */
            boolean isLastActionList = (actionList == actionsByLine.get(LINE_COUNT));
            int actionCount = actionList.size();
            int actionCountSize = !isLastActionList ? TABLE_SCENARIO_BLOCK_ACTION_LINE_COUNT
                    : TABLE_SCENARIO_BLOCK_ACTION_OTHER_COUNT;
            stream.write(writeInteger(actionCount, actionCountSize));
            if (actionCount != 0) {
                ByteArrayOutputStream subStream = new ByteArrayOutputStream();
                if (!isLastActionList)
                    subStream.write(TABLE_SCENARIO_ACTION_BLOCK_LENGTH_STUB);
                for (ScenarioTimeLineBlockAction actionWrapper : actionList) {
                    ScenarioTimeLineBlock.Action action = actionWrapper.getAction();
                    ScenarioActionType actionType = findActionType(action.getActionTypeId());

                    // Указатель на размещение сущности, над которой
                    // производится действие
                    int address = 0;
                    String entityId = action.getEntityId();
                    DeviceProfile actionDeviceProfile = null;
                    switch (actionType.getEntityType()) {
                    case DEVICE:
                        address = localAndExternalChildDeviceOffsets.getOrDefault(entityId, 0);
                        ActiveDevice device = findChildDevice(entityId);
                        if (device == null)
                            device = findExternalExecutiveDevice(entityId);
                        if (device != null)
                            actionDeviceProfile = getDeviceProfileView(device.getDeviceProject()).getDeviceProfile();
                        break;
                    case REGION:
                        address = regionOffsets.getOrDefault(entityId, 0);
                        break;
                    case SCENARIO:
                        address = scenarioOffsets.getOrDefault(entityId, 0);
                        break;
                    case VIRTUAL_STATE:
                        address = virtualStateOffsets.getOrDefault(entityId, 0);
                        break;
                    default:
                        break;
                    }
                    subStream.write(writeInteger(address, EMPTY_MEMORY_ADDRESS.length));

                    // Байт кода проверяемого в сценарии в условном блоке
                    // состояния устройства
                    byte conditionCode = 0x00;
                    // Байт с битовыми признаками для условного блока и кодом
                    // действия
                    byte config = 0x00;
                    switch (actionWrapper.getConditionType()) {
                    case DEVICE_STATE:
                        State state = findDeviceState(actionWrapper.getConditionEntityId(),
                                actionWrapper.getConditionStateId());
                        if (state != null) {
                            conditionCode = (byte) state.getScenarioConditionCode();
                            if (actionWrapper.getConditionCheckType() == ConditionCheckType.IS_INACTIVE)
                                config |= TABLE_SCENARIO_ACTION_CHECK_INACTIVE;
                        } else {
                            stateMachineContext.getLogger().warn("State not found: {}",
                                    actionWrapper.getConditionStateId());
                        }
                        break;
                    case VIRTUAL_STATE:
                        conditionCode = (byte) TABLE_SCENARIO_VIRTUAL_STATE_ACTIVE;
                        if (actionWrapper.getConditionCheckType() == ConditionCheckType.IS_INACTIVE)
                            config |= TABLE_SCENARIO_ACTION_CHECK_INACTIVE;
                        break;
                    default:
                        break;
                    }
                    if (actionWrapper.isHavingTracingBlock())
                        config |= TABLE_SCENARIO_ACTION_TRACING;
                    config |= (actionType.getCode() << TABLE_SCENARIO_ACTION_CODE_OFFSET);
                    if (actionType.getEntityType() == ActionEntityType.VIRTUAL_STATE)
                        config |= TABLE_SCENARIO_NEED_STATE_MESSAGE;

                    subStream.write(conditionCode);
                    subStream.write(config);


                    if (actionDeviceProfile != null && actionDeviceProfile.getScenarioActionParameterSize() > 0) {
                        /* TODO: записывать параметры, задаваемые пользователем */
                        subStream.write(writeInteger(actionType.getParameterStub(),
                                actionDeviceProfile.getScenarioActionParameterSize()));
                    }
                }
                byte[] data = subStream.toByteArray();
                if (!isLastActionList) {
                    int length = data.length - TABLE_SCENARIO_ACTION_BLOCK_LENGTH_STUB.length;
                    insertInteger(data, 0, length, TABLE_SCENARIO_ACTION_BLOCK_LENGTH_STUB.length);
                }
                stream.write(data);
            }
        }
    }

    /**
     * Распределить действия исполнительного блока сценария по линиям, также
     * выделив отдельно группу действий с внешними ИУ, сценариями, зонами,
     * виртуальными состояниями и т.д.
     *
     * @param scenarioWrapper
     *            сценарий в обертке
     * @param block
     *            исполнительный блок
     * @return список групп действий: индекс в списке соответствует номеру
     *         линии, а последний элемент - выделенная группа с объектами, не
     *         являющимися локальными устройствами
     */
    private List<List<ScenarioTimeLineBlockAction>> getScenarioTimeLineBlockActionsByLine(
            ScenarioWrapper scenarioWrapper, ScenarioTimeLineBlockRepr block) {

        List<List<ScenarioTimeLineBlockAction>> actionsByLine = new ArrayList<>();
        for (int i = 0; i < LINE_COUNT; i++)
            actionsByLine.add(new ArrayList<>());
        // Добавим еще один массив в конце для действий над зонами, вирт.
        // состояниями и т.д.
        actionsByLine.add(new ArrayList<>());

        for (ScenarioTimeLineBlockAction actionWrapper : block.getActions()) {
            ScenarioTimeLineBlock.Action action = actionWrapper.getAction();
            ScenarioActionType actionType = findActionType(action.getActionTypeId());
            if (actionType != null) {
                if (actionType.getEntityType() == ActionEntityType.DEVICE) {
                    /* Определим - локальное ли устройство, или внешнее */
                    ActiveDevice controlDevice = stateMachineContext.getControlDevicesByChildDeviceIds()
                            .get(action.getEntityId());
                    if (controlDevice != null) {
                        if (controlDevice == stateMachineContext.getRubezh2op3Device()) {
                            /*
                             * Для действий над локальными устройствами -
                             * возьмем их номер линии и запишем действие в
                             * соответствующий массив
                             */
                            ActiveDevice device = findChildDevice(action.getEntityId());
                            if (device != null) {
                                int lineNoIdx = device.getDeviceProject().getLineNo() - 1;
                                if (lineNoIdx >= 0 && lineNoIdx < LINE_COUNT) {
                                    actionsByLine.get(lineNoIdx).add(actionWrapper);
                                    /*
                                     * Отметим для валидации проекта, что есть
                                     * сработка для устройства
                                     */
                                    engagedInScenarioActionsDeviceIds.add(device.getId());
                                } else if (device.getDeviceProject().getAddressType() == AddressType.INTERNAL) {
                                    /*
                                     * Для действий над встроенными исполнительными устройствами
                                     * с внутренней адресацией - запишем их в последний массив
                                     */
                                    actionsByLine.get(LINE_COUNT).add(actionWrapper);
                                    engagedInScenarioActionsDeviceIds.add(device.getId());
                                }
                            }
                        } else {
                            if (findExternalExecutiveDevice(action.getEntityId()) != null) {
                                /*
                                 * Для действий над внешними исполнительными
                                 * устройствами - запишем их в последний массив
                                 */
                                actionsByLine.get(LINE_COUNT).add(actionWrapper);
                            }
                        }
                    }
                } else {
                    // Для действий над другими сущностями - запишем их в
                    // последний массив
                    actionsByLine.get(LINE_COUNT).add(actionWrapper);
                }
            }
        }
        return actionsByLine;
    }

    /**
     * Запись всех локальных сценариев
     */
    private void writeScenarios(ByteArrayOutputStream stream) throws IOException {
        int localNo = 1;
        for (ScenarioWrapper scenarioWrapper : scenarios) {
            Scenario scenario = scenarioWrapper.scenario.getScenarioProject();
            String id = scenario.getId();
            scenarioOffsets.put(id, stream.size());

            // Глобальный номер сценария
            stream.write(writeInteger(scenario.getGlobalNo(), TABLE_SCENARIO_NO_SIZE));
            // Номер сценария локальный
            stream.write(writeInteger(localNo, TABLE_SCENARIO_NO_SIZE));
            // Биты конфига
            byte config = 0x00;
            {
                /*
                 * Флаг ручной остановки сценария, допустим только для
                 * управляющих сценариев
                 */
                if (scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_LOGIC
                        && scenario.getStopType() == StopType.MANUAL)
                    config |= TABLE_SCENARIO_CONFIG_MANUAL_STOP;
                /* Бит командного сценария с момента поддержки прибором многоуровневой логики должен быть всегда включен */
                config |= TABLE_SCENARIO_CONFIG_COMMAND;
                if (scenario.isStopOnGlobalReset())
                    config |= TABLE_SCENARIO_CONFIG_STOP_ON_RESET;
                int scenarioType = 0;
                switch (scenario.getScenarioType()) {
                case UNASSIGNED:
                    scenarioType = TABLE_SCENARIO_CONFIG_TYPE_UNASSIGNED;
                    break;
                case ENS:
                    scenarioType = TABLE_SCENARIO_CONFIG_TYPE_ENS;
                    break;
                case FIRE_FIGHTING:
                    scenarioType = TABLE_SCENARIO_CONFIG_TYPE_FIRE_FIGHTING;
                    break;
                case ENGINEERING:
                    scenarioType = TABLE_SCENARIO_CONFIG_TYPE_ENGINEERING;
                    break;
                default:
                    break;
                }
                config |= (scenarioType << TABLE_SCENARIO_CONFIG_TYPE_OFFSET);
            }
            stream.write(config);
            // Название сценария
            stream.write(writeString(scenario.getName(), TABLE_SCENARIO_TITLE_SIZE));

            // Блоки времени
            for (ScenarioTimeLineBlockRepr timeLineBlockRepr : scenarioWrapper.timeLineBlocks) {
                stream.write(writeInteger(timeLineBlockRepr.getRelativeDelay(), TABLE_SCENARIO_BLOCK_DELAY_SIZE));
                writeScenarioTimeLineBlock(scenarioWrapper, stream, timeLineBlockRepr);
            }

            stream.write(TABLE_SCENARIO_LAST_BLOCK_BYTE);

            localNo++;
        }
    }

    /**
     * Запись таблицы внешних сценариев
     */
    private void writeExternalScenarios(ByteArrayOutputStream stream) throws IOException {
        List<ActiveScenario> externalScenarios = stateMachineContext.getExternalScenarios();
        if (!externalScenarios.isEmpty()) {
            externalScenariosTableHeader.setAddress(stream.size());
            externalScenariosTableHeader.setEntryLength(EXTERNAL_SCENARIO_SIZE);
            int nExternalScenarios = 0;
            for (ActiveScenario scenario : externalScenarios) {
                ActiveDevice scenarioControlDevice = null;
                /*
                 * Сценарий может пренадлежать нескольким приборам, нам нужен
                 * первый попавшийся
                 */
                Iterator<String> scenarioControlDeviceIdsIt = stateMachineContext.getControlDeviceIdsByScenarioIds()
                        .get(scenario.getId()).iterator();
                if (scenarioControlDeviceIdsIt.hasNext()) {
                    scenarioControlDevice = stateMachineContext.getAllProjectDevicesByIds()
                            .get(scenarioControlDeviceIdsIt.next());
                }
                if (scenarioControlDevice != null) {
                    ++nExternalScenarios;
                    scenarioOffsets.put(scenario.getId(), stream.size());
                    /* Глобальный номер сценария */
                    stream.write(writeInteger(scenario.getScenarioProject().getGlobalNo(), TABLE_SCENARIO_NO_SIZE));
                    /* Адрес прибора */
                    stream.write(scenarioControlDevice.getDeviceProject().getLineAddress() & 0xFF);
                    /* Название сценария */
                    stream.write(writeString(scenario.getScenarioProject().getName(), TABLE_SCENARIO_TITLE_SIZE));
                }

            }
            externalScenariosTableHeader.setEntryCount(nExternalScenarios);
        }
    }

    /**
     * Запись таблицы виртуальных контейнеров
     */

    private void writeVirtualContainers(ByteArrayOutputStream stream) throws IOException {
        // выбираем все виртуальные контейнеры
        List<ActiveDevice> virtualContainers = new ArrayList<>();
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices())
            if (activeDevice.getDeviceProject().getDeviceCategory() == DeviceCategory.VIRTUAL_CONTAINER)
                virtualContainers.add(activeDevice);
        if (!virtualContainers.isEmpty()) {
            // заполняем дескриптор таблицы
            virtualContainersTableHeader.setAddress(stream.size());
            virtualContainersTableHeader.setEntryLength(0);
            virtualContainersTableHeader.setEntryCount(virtualContainers.size());
            // записываем таблицу
            virtualContainers.sort((device1, device2) -> Integer.compare(
                    device1.getDeviceProject().getLineAddress(),
                    device2.getDeviceProject().getLineAddress()));
            for (ActiveDevice virtualContainer : virtualContainers) {
                localAndExternalChildDeviceOffsets.put(virtualContainer.getId(), stream.size());
                stream.write(getVirtualContainerTableEntry(virtualContainer));
            }
        }
    }

    private byte[] getVirtualContainerTableEntry(ActiveDevice device) throws IOException {

        // Данные по устройству сначала пишем в отдельный бинарный массив,
        // чтобы потом подставить все переменные длины
        ByteArrayOutputStream deviceStream = new ByteArrayOutputStream();

        /*
         * Формируем запись таблицы ВК, указывая в качестве адреса прибора 0
         * (для локальных ВК)
         */
        ExecutiveDeviceEntryOffsets entryOffsets =
                writeVirtualContainerTableEntry(device, (byte) 0x00, deviceStream);

        // Берем данные этого устройства как байтовый массив, высчитываем
        // все переменные размеры и подставляем
        byte[] rawDeviceEntry = deviceStream.toByteArray();
        int variableConfigLength = entryOffsets.variableConfigEnd - entryOffsets.variableConfigBegin;
        insertInteger(rawDeviceEntry, entryOffsets.variableConfigLengthOffset, variableConfigLength,
                TABLE_DEVICE_CONFIG_LENGTH_STUB.length);
        insertInteger(rawDeviceEntry, entryOffsets.entryLengthOffset, rawDeviceEntry.length,
                TABLE_EXECUTIVE_ENTRY_LENGTH_STUB.length);

        return deviceStream.toByteArray();
    }

    private ExecutiveDeviceEntryOffsets writeVirtualContainerTableEntry(ActiveDevice device,
            byte controlDeviceAddress, ByteArrayOutputStream deviceStream) throws IOException {

        Device deviceProject = device.getDeviceProject();
        DeviceProfile deviceProfile =
                stateMachineContext.getSupportedDeviceProfiles().get(deviceProject.getDeviceProfileId());
        Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
        ShortDevicePath shortDevicePath = getRealShortDevicePath(device);

        String deviceName = DeviceProperty.getString(deviceProject, DeviceProperties.AlternateName);
        if (deviceName == null) {
            DeviceProfileView deviceProfileView = getDeviceProfileView(deviceProject);
            deviceName = (deviceProfileView != null) ?
                    deviceProfileView.getName() : TABLE_VIRTUAL_CONTAINER_DEFAULT_NAME;
        }

        List<String> sortedLinkedDeviceIds = new ArrayList<>();
        for (DeviceProfile.InternalDeviceItem internalDeviceItem : deviceProfile.getInternalDevices()) {
            int numOfLinkedDevices = 0;
            for (String deviceProfileId : internalDeviceItem.getAvailableDeviceProfileIds())
                if (deviceProject.getLinkedDeviceIdsMap().containsKey(deviceProfileId)) {
                    sortedLinkedDeviceIds.addAll(deviceProject.getLinkedDeviceIdsMap().get(deviceProfileId));
                    numOfLinkedDevices += deviceProject.getLinkedDeviceIdsMap().get(deviceProfileId).size();
                }
            // Валидация: каждая насосная станция должна содержать как минимум один пожарный насос
            if (numOfLinkedDevices < internalDeviceItem.getnMandatoryDevices())
                notEnoughInternalDevicesVCIds.add(device.getId());
        }

        ExecutiveDeviceEntryOffsets entryOffsets = new ExecutiveDeviceEntryOffsets();

        // Адрес прибора привязки в сети (0 для локальной) (1)
        deviceStream.write(controlDeviceAddress);
        // Номер (1-250)	(1)
        deviceStream.write(writeInteger(shortDevicePath.lineAddress, 1));
        // Описание. Автогенерировать: Название номер	(20)
        deviceStream.write(
                writeString(generateDeviceDescription(deviceName, shortDevicePath), DEVICE_DESCRIPTION_SIZE));
        // Описание состояний Вкл и Выкл	(2 по 20)
        writeExecutiveOnOffDescriptions(deviceProject, deviceStream);
        // Виртуальный тип	(1)
        deviceStream.write(writeInteger(deviceProfile.getRsr3VirtualDeviceType(), TABLE_VIRTUAL_TYPE_SIZE));
        // Длина записи	(2)
        entryOffsets.entryLengthOffset = deviceStream.size();
        deviceStream.write(TABLE_EXECUTIVE_ENTRY_LENGTH_STUB);
        // Связанные шаблоны логик	(2 + N * 3)
        writeDeviceLogicBlockReferences(deviceProject, deviceStream);
        // Количество входящих в состав устройств		(1)
        deviceStream.write((byte)sortedLinkedDeviceIds.size());
        // Указатели на входящие в состав устройства	(N * 3)
        for (String linkedDeviceId : sortedLinkedDeviceIds)
            deviceStream.write(writeInteger(
                    localAndExternalChildDeviceOffsets.getOrDefault(linkedDeviceId, 0), DB_ADDRESS_SIZE));

        // Длина переменной части блока с конфигурацией	(2)
        entryOffsets.variableConfigLengthOffset = deviceStream.size();
        deviceStream.write(TABLE_DEVICE_CONFIG_LENGTH_STUB);
        // Блок данных устройств (типозависим)
        entryOffsets.variableConfigBegin = deviceStream.size();
        if (options.contains(ControlDeviceDatabaseOptionType.VIRTUAL_CONTAINER_HAS_CONFIG)) {
            byte[] rawConfig = new byte[deviceProfile.getConfigSize()];
            stateMachineContext.getChildDeviceConfigCodec().encodeDeviceProfileConfig(rawConfig,
                    deviceProfile.getConfigProperties(), deviceProject.getProjectConfigValues());
            deviceStream.write(rawConfig);
        }
        entryOffsets.variableConfigEnd = deviceStream.size();

        return entryOffsets;
    }

    private void writeExternalExecutiveDevicePointersTable(ByteArrayOutputStream stream)
            throws IOException {
        if (sortedExternalExecutiveDevices.size() > 0) {
            externalExecutiveDevicePointersTableHeader.setAddress(stream.size());
            externalExecutiveDevicePointersTableHeader.setEntryCount(sortedExternalExecutiveDevices.size());
            for (ActiveDevice activeDevice : sortedExternalExecutiveDevices) {
                if (localAndExternalChildDeviceOffsets.containsKey(activeDevice.getId())) {
                    stream.write(
                            writeInteger(localAndExternalChildDeviceOffsets.get(activeDevice.getId()),
                            EMPTY_MEMORY_ADDRESS.length));
                } else {
                    stream.write(EMPTY_MEMORY_ADDRESS);
                }
            }
        }
    }

    /**
     * Запись указателей на устройства на линии
     */
    private void writeLineDevicePointersTable(ByteArrayOutputStream stream, int lineNoIdx) throws IOException {
        int lastLineAddress = -1;

        List<ActiveDevice> devices = new ArrayList<>();
        for (ActiveDevice device : stateMachineContext.getChildDevices()) {
            if (device.getDeviceProject().getDeviceCategory() == DeviceCategory.SENSOR
                    || device.getDeviceProject().getDeviceCategory() == DeviceCategory.EXECUTIVE) {
                devices.add(device);
            }
        }


        Map<Integer, ActiveDevice> lineDevices = new HashMap<>();
        for (ActiveDevice device : devices) {
            ShortDevicePath shortDevicePath = getRealShortDevicePath(device);
            if (shortDevicePath.lineNo == lineNoIdx + 1) {
                lineDevices.put(shortDevicePath.lineAddress - 1, device);
                lastLineAddress = Integer.max(lastLineAddress, shortDevicePath.lineAddress - 1);
            }
        }

        TableHeader tableHeader = lineDevicePointersTableHeaders[lineNoIdx];
        if (lastLineAddress >= 0) {
            tableHeader.setAddress(stream.size());
            tableHeader.setEntryCount(lastLineAddress + 1);
            for (int i = 0; i <= lastLineAddress; i++) {
                if (lineDevices.containsKey(i)) {
                    ActiveDevice device = lineDevices.get(i);
                    if (localAndExternalChildDeviceOffsets.containsKey(device.getId())) {
                        stream.write(writeInteger(localAndExternalChildDeviceOffsets.get(device.getId()), EMPTY_MEMORY_ADDRESS.length));
                    } else {
                        stream.write(EMPTY_MEMORY_ADDRESS);
                    }
                } else {
                    stream.write(EMPTY_MEMORY_ADDRESS);
                }
            }
        }
    }

    /**
     * Запись таблицы виртуальных состояний
     */
    private void writeVirtualStatesTable(ByteArrayOutputStream stream) throws IOException {
        List<VirtualState> virtualStates = stateMachineContext.getVirtualStates();
        String currentDeviceId = stateMachineContext.getRubezh2op3Device().getId();

        if (virtualStates.size() != 0) {
            virtualStatesTableHeader.setAddress(stream.size());
            virtualStatesTableHeader.setEntryCount(virtualStates.size());
        }

        for (VirtualState virtualState : virtualStates) {
            ByteArrayOutputStream virtualStateStream = new ByteArrayOutputStream();
            // Глобальный номер
            virtualStateOffsets.put(virtualState.getId(), stream.size());
            int globalNo = virtualState.getGlobalNo();
            virtualStateStream.write(writeInteger(globalNo, TABLE_VIRTUAL_STATE_NO_SIZE));
            /*
             * Адрес прибора: 0 - для локального ВС и для
             * "битой ссылки на прибор", реальный адрес - для внешнего ВС
             */
            ActiveDevice virtualStateControlDevice = stateMachineContext.getAllProjectDevicesByIds()
                    .get(virtualState.getDeviceId());
            if (currentDeviceId.equals(virtualState.getDeviceId()) || virtualStateControlDevice == null) {
                virtualStateStream.write(TABLE_VIRTUAL_STATE_DEVICE_ADDRESS_LOCAL);
            } else {
                virtualStateStream.write(writeInteger(virtualStateControlDevice.getDeviceProject().getLineAddress(),
                        TABLE_VIRTUAL_STATE_DEVICE_ADDRESS_SIZE));
            }
            // Длина записи
            int entryLengthOffset = virtualStateStream.size();
            virtualStateStream.write(TABLE_VIRTUAL_STATE_ENTRY_LENGTH_STUB);
            // Резерв
            virtualStateStream.write(TABLE_VIRTUAL_STATE_RESERVE_STUB);
            // Сообщение при установке
            virtualStateStream.write(writeString(virtualState.getMessageOn(), TABLE_VIRTUAL_STATE_MESSAGE_SIZE));
            // Сообщение при удалении
            virtualStateStream.write(writeString(virtualState.getMessageOff(), TABLE_VIRTUAL_STATE_MESSAGE_SIZE));

            // Количество связанных шаблонов логик или «0»
            List<Integer> logicBlockRefs = virtualStateLogicBlockUsages.getOrDefault(virtualState.getId(),
                    new ArrayList<>());
            virtualStateStream
                    .write(writeInteger(logicBlockRefs.size(), TABLE_VIRTUAL_STATE_LOGIC_BLOCK_REF_COUNT_SIZE));
            for (Integer logicBlockIdx : logicBlockRefs) {
                int address = logicBlockOffsets.getOrDefault(logicBlockIdx, 0);
                virtualStateStream.write(writeInteger(address, EMPTY_MEMORY_ADDRESS.length));
            }

            // Количество связанных блоков времени или "0"
            List<ActionBlockAddress> conditionUsages = virtualStateConditionUsages.getOrDefault(virtualState.getId(),
                    new ArrayList<>());
            virtualStateStream.write(writeInteger(conditionUsages.size(), EMPTY_COUNTER.length));
            for (ActionBlockAddress actionBlockAddress : conditionUsages) {
                virtualStateStream.write(writeInteger(actionBlockAddress.scenarioIdx + 1, EMPTY_COUNTER.length));
                virtualStateStream.write(writeInteger(actionBlockAddress.blockIdx, EMPTY_COUNTER.length));
            }

            byte[] rawVirtualStateEntry = virtualStateStream.toByteArray();
            insertInteger(rawVirtualStateEntry, entryLengthOffset, rawVirtualStateEntry.length,
                    TABLE_REGION_ENTRY_LENGTH_STUB.length);

            stream.write(rawVirtualStateEntry);
        }
    }

    /**
     * Запись таблицы сообщений
     */
    private void writeMessagesTable(ByteArrayOutputStream stream) throws IOException {
        messagesTableHeader.setAddress(stream.size());
        messagesTableHeader.setEntryCount(TABLE_MESSAGE_COUNT);
        messagesTableHeader.setEntryLength(TABLE_MESSAGE_SIZE);
        stream.write(TABLE_MESSAGE_HARDCODED_CODE);
        stream.write(writeString(TABLE_MESSAGE_HARDCODED, TABLE_MESSAGE_LENGTH));
    }

    /**
     * Запись таблицы прибора
     */
    private void writeSelfTable(ByteArrayOutputStream stream) throws IOException {
        int selfCount = 0;
        List<Integer> offsets = new ArrayList<>();
        for (LogicBlockWrapper logicBlockWrapper : logicBlocks) {
            boolean isFake = logicBlockWrapper.childIdxs.size() == 0
                    && findScenarioTriggerType(logicBlockWrapper.logicBlock.getTriggerTypeId()) == null;
            if (isFake) {
                selfCount++;
                offsets.add(logicBlockOffsets.getOrDefault(logicBlockWrapper.idx, 0));
            }
        }

        selfTableHeader.setAddress(stream.size());
        selfTableHeader.setEntryCount(selfCount);
        selfTableHeader.setEntryLength(EMPTY_MEMORY_ADDRESS.length);
        stream.write(writeInteger(selfCount, TABLE_SELF_COUNT_SIZE));
        for (Integer offset : offsets)
            stream.write(writeInteger(offset, EMPTY_MEMORY_ADDRESS.length));
    }

    /**
     * Запись таблицы зон
     */
    private void writeRegions(ByteArrayOutputStream stream) throws IOException {
        int regionLocalNo = 1;
        for (ActiveRegion region : getSortedFireAndSecurityRegions()) {
            ByteArrayOutputStream regionStream = new ByteArrayOutputStream();
            // Номер зоны
            regionStream.write(writeInteger(regionLocalNo, TABLE_REGION_NO_SIZE));
            regionNos.put(region.getId(), regionLocalNo);
            // Наименование
            regionStream.write(writeString(region.getRegionProject().getName(), TABLE_REGION_TITLE_SIZE));
            // Длина записи
            int entryLengthOffset = regionStream.size();
            regionStream.write(TABLE_REGION_ENTRY_LENGTH_STUB);
            // Длина нижеследующих парметров
            int variableConfigLengthOffset = regionStream.size();
            regionStream.write(TABLE_REGION_VARIABLE_CONFIG_LENGTH_STUB);
            int variableConfigBegin = regionStream.size();
            // Конфиг
            byte config = 0x00;
            byte bitAttrs = 0x00;
            Subsystem regionSubsystem = region.getRegionProject().getSubsystem();
            if (regionSubsystem == Subsystem.FIRE) {
                config = TABLE_REGION_CONFIG_FIRE;
            } else if (regionSubsystem == Subsystem.SECURITY) {
                config = TABLE_REGION_CONFIG_SECURITY;
                SecurityRegionType securityRegionType = region.getRegionProject().getSecurityRegionType();
                if (securityRegionType == SecurityRegionType.NO_OFF)
                    bitAttrs |= TABLE_REGION_CONFIG_SEC_NO_OFF;
                else if (securityRegionType == SecurityRegionType.TIMEOUT)
                    bitAttrs |= TABLE_REGION_CONFIG_SEC_TIMEOUT;
                if (region.getRegionProject().getSilenceAlarm())
                    bitAttrs |= TABLE_REGION_CONFIG_SEC_SILENCE_ALARM;
                // TODO: if (локальная зона)
                bitAttrs |= TABLE_REGION_CONFIG_SEC_SELF_DETECTORS;
            }
            regionStream.write(config);
            regionStream.write(bitAttrs);
            // К-во датчиков для формирования "Пожар-2"
            regionStream.write(
                    writeInteger(region.getRegionProject().getFireEventCount(), TABLE_REGION_FIRE_EVENT_COUNT_SIZE));
            // Глобальный номер зоны
            regionStream.write(writeInteger(region.getRegionProject().getIndex(), TABLE_REGION_NO_SIZE));
            // Время входной/выходной задержки
            regionStream.write(writeInteger(region.getRegionProject().getInputOutputTimeout(),
                    TABLE_REGION_INPUT_OUTPUT_TIMEOUT_SIZE));
            // Номер направления (не используется)
            regionStream.write(writeInteger(0, TABLE_DIRECTION_NO_SIZE));
            // Время автоперевзятия
            regionStream.write(writeInteger(region.getRegionProject().getAutoRelock(), TABLE_REGION_DELAY_SIZE));
            // TODO: Счетчик для перехода в пожар-2 в ночном режиме
            regionStream.write(writeInteger(0, TABLE_REGION_NIGHT_FIRE_EVENT_COUNT_SIZE));
            // TODO: Время подтверждения срабатывания, сек
            regionStream.write(writeInteger(0, TABLE_REGION_CONFIRMATION_TIME_SIZE));
            // TODO: Время ожидания после подтверждения, сек
            regionStream.write(writeInteger(0, TABLE_REGION_WAIT_TIME_SIZE));
            int variableConfigEnd = regionStream.size();
            // Указатель на ведущее МПТ в зоне из таблицы МПТ или 0
            if (mptRegionDeviceIds.containsKey(region.getId())) {
                String mptDeviceId = mptRegionDeviceIds.get(region.getId());
                regionStream.write(writeInteger(localAndExternalChildDeviceOffsets.getOrDefault(mptDeviceId, 0), DB_ADDRESS_SIZE));
            } else {
                regionStream.write(EMPTY_MEMORY_ADDRESS);
            }
            // TODO: Количество внешних приборов, ИУ которого могут управляться
            // нашими ИП по логике «межприборное И» или 0
            regionStream.write(writeInteger(0, TABLE_REGION_EXTERNAL_DEVICE_COUNT_SIZE));

            // Количество связанных шаблонов логик * или «0»
            List<Integer> logicBlockRefs = regionLogicBlockUsages.getOrDefault(region.getId(), new ArrayList<>());
            regionStream.write(writeInteger(logicBlockRefs.size(), TABLE_REGION_LOGIC_BLOCK_REF_COUNT_SIZE));
            for (Integer logicBlockIdx : logicBlockRefs) {
                int address = logicBlockOffsets.getOrDefault(logicBlockIdx, 0);
                regionStream.write(writeInteger(address, EMPTY_MEMORY_ADDRESS.length));
            }

            byte[] rawRegionEntry = regionStream.toByteArray();
            int variableConfigLength = variableConfigEnd - variableConfigBegin;
            insertInteger(rawRegionEntry, entryLengthOffset, rawRegionEntry.length,
                    TABLE_REGION_ENTRY_LENGTH_STUB.length);
            insertInteger(rawRegionEntry, variableConfigLengthOffset, variableConfigLength,
                    TABLE_REGION_VARIABLE_CONFIG_LENGTH_STUB.length);

            regionOffsets.put(region.getId(), stream.size());
            stream.write(rawRegionEntry);
            ++regionLocalNo;
        }
    }

    /**
     * Сбор "произвольных данных" по сущностям проекта
     */
    private Map<Integer, Integer> collectArbitraryDataFields() {
        Map<Integer, Integer> arbitraryDataFields = new HashMap<>();
        // поиск по дочерним устройствам
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                    .get(activeDevice.getDeviceProject().getDeviceProfileId());
            if (deviceProfile == null)
                continue;
            Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
            // номер зоны пишется в таблицу "произвольных данных"
            if (options.contains(ControlDeviceDatabaseOptionType.SENSOR_ARBITRARY_DATA_HAS_REGION)) {
                Integer regionNo = regionNos.get(activeDevice.getRegionId());
                Integer regionNoOffset = deviceProfile.getArbitraryDataRegionOffset();
                if (regionNo != null && regionNoOffset != null) {
                    arbitraryDataFields.put(regionNoOffset, regionNo);
                }
                // валидация проекта: устройство с данной опцией должно быть привязано к зоне
                if (regionNo == null)
                    noRegionSensorIds.add(activeDevice.getId());
                else
                    noRegionSensorIds.remove(activeDevice.getId());
            }
        }
        return arbitraryDataFields;
    }

    /**
     * Запись таблицы "произвольные данные" (пишется "в самом конце" базы)
     */
    private void writeArbitraryTable(ByteArrayOutputStream outputStream) throws IOException {
        Device device = stateMachineContext.getRubezh2op3Device().getDeviceProject();
        DeviceProfile profile = stateMachineContext.getRubezh2op3DeviceProfile();
        ByteArrayOutputStream arbitraryDataStream = new ByteArrayOutputStream();
        Map<Integer, Integer> arbitraryDataFields = collectArbitraryDataFields();
        // 1 байт  - Конфигурация АЛС («1» по маске 0x01-1 АЛС кольцевая, «1» по маске 0x02-2я АЛС кольцевая),
        //  «1» по маске 0x04-«Неадресный изолятор в АЛС 1», «1» по маске 0x08-«Неадресный изолятор в АЛС 2»
        BitSet linesConfig = new BitSet(8);
        linesConfig.set(LINE_1_IS_RING_BIT_NO, DeviceProperty.getBoolean(device, profile, DeviceProperties.Line1IsRing));
        linesConfig.set(LINE_2_IS_RING_BIT_NO, DeviceProperty.getBoolean(device, profile, DeviceProperties.Line2IsRing));
        linesConfig.set(LINE_1_ISOLATOR_BIT_NO, DeviceProperty.getBoolean(device, profile, DeviceProperties.Line1Isolator));
        linesConfig.set(LINE_2_ISOLATOR_BIT_NO, DeviceProperty.getBoolean(device, profile, DeviceProperties.Line2Isolator));
        int linesConfigValue = linesConfig.isEmpty() ? 0 : linesConfig.toByteArray()[0];
        arbitraryDataStream.write(writeInteger(linesConfigValue, TABLE_LINES_CONFIG_SIZE));
        // 2 байта - Зона привязки кнопки «тревога» или «0» если не привязана ни к чему
        arbitraryDataStream.write(
                writeInteger(arbitraryDataFields.getOrDefault(arbitraryDataStream.size(), 0), TABLE_REGION_NO_SIZE));
        // 2 байта - Зона привязки кнопки «пожар» или «0» если не привязана ни к чему
        arbitraryDataStream.write(
                writeInteger(arbitraryDataFields.getOrDefault(arbitraryDataStream.size(), 0), TABLE_REGION_NO_SIZE));
        // 2 байта - Время начала «Ночного режима», чч:мм. При выключенном режиме на приборе: 0xff 0xff
        //stream.write(writeTimeBCD(nightModeBeginTime));
        arbitraryDataStream.write((byte) 0xFF);
        arbitraryDataStream.write((byte) 0xFF);
        // 2 байта - Время окончания «Ночного режима», чч:мм. При выключенном режиме на приборе: 0xff 0xff
        //stream.write(writeTimeBCD(nightModeEndTime));
        arbitraryDataStream.write((byte) 0xFF);
        arbitraryDataStream.write((byte) 0xFF);
        // Заполнить 0 надо неиспользованное (до 20 байт)
        assert arbitraryDataStream.size() <= ARBITRARY_DATA_SIZE;
        for (int i = arbitraryDataStream.size(); i < ARBITRARY_DATA_SIZE; i++)
            arbitraryDataStream.write(0x00);
        arbitraryDataStream.writeTo(outputStream);
    }

    /**
     * Генерация основной части БД
     */
    private byte[] generateMainDatabase() {
        byte[] result = null;

        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            /* Резерв для компа (для MD5 и т.д.) */
            stream.write(DB_HASH_VERSION);
            dbHashOffset = stream.size();
            stream.write(dbHash);
            stream.write(DB_SEC_HASH_VERSION);
            stream.write(dbSecHash);
            while (stream.size() < RESERVED_BLOCK_SIZE)
                stream.write(new byte[] { 0 });
            /* Сигнатура базы */
            stream.write(DB_SIGNATURE);
            /*
             * CRC - пишется отдельно после 2-х проходов, тут просто запоминаем
             * смещение
             */
            dbCrcOffset = stream.size();
            stream.write(DB_CRC_STUB);
            /* Версия базы */
            stream.write(DB_VERSION);
            /* Абсолютный указатель на конец базы */
            stream.write(writeInteger(dbEndAddress, 3));

            /* Хидеры таблиц */
            for (TableHeader deviceTableHeader : deviceTableHeaders)
                stream.write(genTableHeader(deviceTableHeader, true));
            stream.write(genTableHeader(regionPointersTableHeader, true));
            stream.write(genTableHeader(externalRegionsTableHeader, true));
            stream.write(genTableHeader(externalExecutiveDevicePointersTableHeader, true));
            stream.write(genTableHeader(monitorPointersTableHeader, true));
            stream.write(genTableHeader(directionPointersTableHeader, true));
            stream.write(genTableHeader(logicBlocksTableHeader, true));
            stream.write(genTableHeader(scenarioPointersTableHeader, true));
            for (int lineNoIdx = 0; lineNoIdx < lineDevicePointersTableHeaders.length; lineNoIdx++)
                stream.write(genTableHeader(lineDevicePointersTableHeaders[lineNoIdx], false));
            stream.write(genTableHeader(localDeviceConfigPointersTableHeader, true));
            stream.write(genTableHeader(externalScenariosTableHeader, true));
            stream.write(genTableHeader(virtualContainersTableHeader, true));
            stream.write(genTableHeader(virtualStatesTableHeader, true));
            stream.write(genTableHeader(messagesTableHeader, true));
            stream.write(genTableHeader(selfTableHeader, true));
            stream.write(genTableHeader(scenarioLogicBlockPointersTableHeader, true));
            /* Таблицы */
            writeRegionPointersTable(stream);
            writeExternalExecutiveDevicePointersTable(stream);
            for (int lineNoIdx = 0; lineNoIdx < lineDevicePointersTableHeaders.length; lineNoIdx++)
                writeLineDevicePointersTable(stream, lineNoIdx);
            writeScenarioPointersTable(stream);
            writeExternalRegionsTable(stream);
            writeRegions(stream);
            writeMonitorPointersTable(stream);
            writeDevicesTable(stream);
            writeLogicBlocksTable(stream);
            writeScenarioLogicBlockPointersTable(stream);
            writeScenarios(stream);
            writeExternalScenarios(stream);
            writeVirtualContainers(stream);
            writeVirtualStatesTable(stream);
            writeMessagesTable(stream);
            writeSelfTable(stream);

            // TODO: Таблица указателей на внешние устройства
            // TODO: Таблица указателей на направления тушения

            /* Произвольные данные */
            writeArbitraryTable(stream);

            result = stream.toByteArray();
            dbEndAddress = (stream.size() - 1);
        } catch (IOException e) {

        }

        return result;
    }

    /**
     * Запись БД с конфигурацией локальных устройств
     */
    private byte[] generateConfigDatabase() {
        byte[] result = null;

        // Берем все дочерние устройства (кроме безадресных)
        List<Pair<ActiveDevice, DeviceProfile>> configurableDevices = getWriteableDevicesAndProfiles(
                stateMachineContext.getChildDevices());
        // исключаем бесшлейфовые (с внутренней адресацией)
        configurableDevices.removeIf(deviceAndProfile ->
                deviceAndProfile.getFirst().getDeviceProject().getAddressType() == AddressType.INTERNAL);
        // сортируем по линии и адресу
        configurableDevices.sort((o1, o2) -> {
            int v1 = o1.getFirst().getDeviceProject().getLineNo() << 8
                    + o1.getFirst().getDeviceProject().getLineAddress();
            int v2 = o2.getFirst().getDeviceProject().getLineNo() << 8
                    + o2.getFirst().getDeviceProject().getLineAddress();
            return Integer.compare(v1, v2);
        });

        deviceConfigOffsets = new int[LINE_COUNT][DEVICE_ON_LINE];

        try {
            ByteArrayOutputStream configs = new ByteArrayOutputStream();
            for (Pair<ActiveDevice, DeviceProfile> pair : configurableDevices) {
                ActiveDevice device = pair.getFirst();
                DeviceProfile deviceProfile = pair.getSecond();

                ShortDevicePath shortDevicePath = getRealShortDevicePath(device);
                deviceConfigOffsets[shortDevicePath.lineNo - 1][shortDevicePath.lineAddress - 1] = DB_CONFIG_OFFSET
                        + DB_CONFIG_CONFIGS_OFFSET + configs.size();

                configs.write(CONFIG_SERIAL_STUB);
                configs.write(CONFIG_SW_VERSION_STUB);
                configs.write(CONFIG_PROTOCOL_VERSION_STUB);

                byte[] rawConfig = new byte[deviceProfile.getConfigSize()];
                stateMachineContext.getChildDeviceConfigCodec().encodeDeviceProfileConfig(rawConfig,
                        deviceProfile.getConfigProperties(), device.getDeviceProject().getProjectConfigValues());

                Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
                if (options.contains(ControlDeviceDatabaseOptionType.DEVICE_CONFIG_HAS_CRC)) {
                    int crc = CRC16CCITT(rawConfig, 0, rawConfig.length);
                    configs.write(writeInteger(crc, 2));
                }
                configs.write(rawConfig);
            }

            int configPointersListSize = LINE_COUNT * DEVICE_ON_LINE * EMPTY_MEMORY_ADDRESS.length;
            int configSize = configs.size();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(new byte[] { 0x00, 0x00 });
            configCrcOffset = stream.size();
            configCrcEndOffset = configCrcOffset + 4 + configPointersListSize + configSize;
            stream.write(DB_CRC_STUB);
            stream.write(writeInteger(configSize + configPointersListSize, 2));
            for (int lineNoIdx = 0; lineNoIdx < LINE_COUNT; lineNoIdx++)
                for (int deviceNo = 0; deviceNo < DEVICE_ON_LINE; deviceNo++)
                    stream.write(writeInteger(deviceConfigOffsets[lineNoIdx][deviceNo], EMPTY_MEMORY_ADDRESS.length));

            stream.write(configs.toByteArray());

            for (int i = 0; i < DB_CONFIG_RESERVED_SIZE - configSize; ++i)
                stream.write((byte) 0x00);

            result = stream.toByteArray();

            recalculateCrc(result, configCrcOffset, DB_CRC_STUB.length, configCrcOffset + 2, configCrcEndOffset);

        } catch (IOException e) {
        }

        return result;
    }

    /**
     * Запись БД с дополнительной конфигурацией устройств
     */
    private byte[] generateExtraConfigDatabase() {
        byte[] extraConfig = new byte[DB_EXTRA_CONFIG_SIZE];
        Arrays.fill(extraConfig, (byte)0xFF);
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            Device deviceProject = activeDevice.getDeviceProject();
            DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                    .get(deviceProject.getDeviceProfileId());
            updateExtraConfig(extraConfig, deviceProject, deviceProfile);
        }
        return extraConfig;
    }

    /**
     * Сериализация в JSON
     */
    private byte[] writeObjectAsJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        try {
            return mapper.writeValueAsBytes(object);
        } catch (JsonProcessingException e) {
            stateMachineContext.getLogger().error("JSON Error: {}", e);
        }
        return null;
    }

    /**
     * Формирование списка проектных сущностей КА, включая внешние сценарии и исп. устройства
     */
    public ControlDeviceEntities getAllControlDeviceEntities() {
        ControlDeviceEntities entities = getControlDeviceEntities();

        for (ActiveDevice externalDevice : stateMachineContext.getExternalExecutiveChildDevices())
            entities.getDevices().add(externalDevice.getDeviceProject());
        for (ActiveScenario externalScenario : stateMachineContext.getExternalScenarios())
            entities.getScenarios().add(externalScenario.getScenarioProject());

        return entities;
    }

    /**
     * Формирование списка проектных сущностей КА
     */
    public ControlDeviceEntities getControlDeviceEntities() {
        List<Device> devices = new ArrayList<>();
        List<VirtualState> virtualStates = stateMachineContext.getVirtualStates();
        List<Scenario> scenarios = new ArrayList<>();
        List<Region> regions = new ArrayList<>();

        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices())
            devices.add(activeDevice.getDeviceProject());
        devices.add(stateMachineContext.getRubezh2op3Device().getDeviceProject());

        for (ActiveScenario activeScenario: stateMachineContext.getScenarios())
            scenarios.add(activeScenario.getScenarioProject());

        for (ActiveRegion activeRegion : stateMachineContext.getRegions())
            regions.add(activeRegion.getRegionProject());

        return new ControlDeviceEntities(devices, virtualStates, scenarios, regions);
    }

    /**
     * Запись БД с информацией для восстановления
     */
    private byte[] generateRecoveryDatabase() {
        ByteArrayOutputStream recoveryStream = new ByteArrayOutputStream();

        // проектные сущности
        ControlDeviceEntities entities = getControlDeviceEntities();

        // JSON
        byte[] jsonData = writeObjectAsJson(entities);

        try {
            // размер JSON
            int jsonLength = (jsonData == null) ? 0 : jsonData.length;
            int headerLength = DB_RECOVERY_LENGTH_SIZE + 2 * DB_CRC_STUB.length;
            if (jsonLength > (DB_RECOVERY_MAX_LENGTH - headerLength))
                jsonLength = 0;
            recoveryStream.write(writeInteger(jsonLength, DB_RECOVERY_LENGTH_SIZE));
            // CRC JSON
            int jsonCrc = (jsonLength == 0) ? 0 : CRC16CCITT(jsonData, 0, jsonLength);
            recoveryStream.write(writeInteger(jsonCrc, DB_CRC_STUB.length));
            // CRC заголовка
            byte[] header = recoveryStream.toByteArray();
            int headerCrc = CRC16CCITT(header, 0, header.length);
            recoveryStream.write(writeInteger(headerCrc, DB_CRC_STUB.length));
            // данные JSON
            if (jsonLength != 0)
                recoveryStream.write(jsonData);
        } catch (IOException e) {
            stateMachineContext.getLogger().error("Error: {}", e);
        }

        return recoveryStream.toByteArray();
    }

    /**
     * Адрес начала основной базы данных прибора
     *
     * @return адрес в памяти прибора
     */
    public int getMainDatabaseAddress() {
        return DB_MAIN_OFFSET;
    }

    /**
     * Адрес начала блока восстановительной информации
     *
     * @return адрес в памяти прибора
     */
    public int getRecoveryDatabaseAddress() {
        return DB_RECOVERY_OFFSET;
    }

    /**
     * Длина заголовка блока восстановительной информации
     *
     * @return длина заголовка в байтах
     */
    public int getRecoveryDatabaseHeaderLength() {
        return DB_RECOVERY_LENGTH_SIZE + 2 * DB_CRC_STUB.length;
    }


    /**
     * Хеш (MD5) текущей основной базы
     *
     * @return массив байт с MD5
     */
    public byte[] getDbHash() {
        return dbHash;
    }

    /**
     * Смещение Хеш-а основной базы относительно начала
     *
     * @return смещение
     */
    public int getDbHashOffset() {
        return dbHashOffset;
    }

    /**
     * Разбор заголовка блока восстановительной информации
     *
     * @param header
     *            заголовок
     * @return длина основного блока данных (JSON)
     */
    public int parseRecoveryDataLength(byte[] header) {
        // CRC заголовка
        int headerCrc = readInteger(header, DB_RECOVERY_LENGTH_SIZE + DB_CRC_STUB.length, DB_CRC_STUB.length);
        if (headerCrc != CRC16CCITT(header, 0, DB_RECOVERY_LENGTH_SIZE + DB_CRC_STUB.length))
            return 0;
        // размер JSON
        return readInteger(header, 0, DB_RECOVERY_LENGTH_SIZE);
    }

    /**
     * Десериализация набора проектных сущностей из JSON
     */
    private ControlDeviceEntities parseControlDeviceEntities(byte[] jsonData) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonData, ControlDeviceEntities.class);
        } catch (IOException e) {
            stateMachineContext.getLogger().error("JSON Error: {}", e);
        }
        return null;
    }

    /**
     * Разбор блока восстановительной информации
     *
     * @param header
     *            заголовок
     * @param jsonData
     *            восстановительная информация
     * @return восстановленные сущности
     */
    public ControlDeviceEntities parseRecoveryDatabase(byte[] header, byte[] jsonData) {
        // CRC JSON
        int jsonCrc = readInteger(header, DB_RECOVERY_LENGTH_SIZE, DB_CRC_STUB.length);
        if (jsonCrc != CRC16CCITT(jsonData, 0, jsonData.length))
            return null;
        // проектные сущности
        return parseControlDeviceEntities(jsonData);
    }

    /**
     * Подсчёт хэша устройства для упорядочивания по линии и адресу
     */
    private static String getDevicePathHash(Device device) {
        // получаем путь вида ,1,1,1...
        String addressPath = device.getAddressPath();
        // отрезаем первые 6 символов чтобы адрес-путь прибора и МС не влияли на сравнение
        if (addressPath.startsWith(",") && addressPath.length() >= CONTROL_DEVICE_ADDRESS_PATH_LENGTH)
            return addressPath.substring(CONTROL_DEVICE_ADDRESS_PATH_LENGTH);
        else
            return addressPath;
    }

    /**
     * Замена конфигурации устройств с совпадающими линией/адресом
     *
     * @param devicesSrc
     *            устройства, откуда копируются параметры
     * @param devicesDst
     *            устройства, куда копируются параметры
     */
    private static void replaceDevicesProperties(List<Device> devicesSrc, List<Device> devicesDst) {
        // карта соответствия линии/адреса устройства его идентификатору
        Map<String, Device> currentDevicesByPath = new HashMap<>();
        for (Device deviceSrc : devicesSrc)
            currentDevicesByPath.put(getDevicePathHash(deviceSrc), deviceSrc);

        // замена параметров для совпадающих устройств
        for (Device deviceDst : devicesDst) {
            Device deviceSrc = currentDevicesByPath.get(getDevicePathHash(deviceDst));
            if (deviceSrc != null) {
                deviceDst.setProjectConfigValues(deviceSrc.getProjectConfigValues());
                deviceDst.setDevicePropertyValues(deviceSrc.getDevicePropertyValues());
            }
        }
    }

    /**
     * Применение текущей конфигурации к совпадающим по линии/адресу устройствам
     *
     * @param recoveredDevices
     *            восстановленные устройства
     */
    public void applyCurrentDevicesConfig(List<Device> recoveredDevices) {
        // текущие устрйства проекта
        List<Device> currentDevices = new ArrayList<>();
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices())
            currentDevices.add(activeDevice.getDeviceProject());
        currentDevices.add(stateMachineContext.getRubezh2op3Device().getDeviceProject());

        // замена конфигурационных и прочих параметров
        replaceDevicesProperties(currentDevices, recoveredDevices);
    }

    /**
     * Обновить дополнительную конфигурацию устройства
     */
    private boolean updateExtraConfig(byte[] extraConfig, Device deviceProject, DeviceProfile deviceProfile) {
        Set<ControlDeviceDatabaseOptionType> options = deviceProfile.getControlDeviceDatabaseOptionTypes();
        if (options.contains(ControlDeviceDatabaseOptionType.DEVICE_HAS_EXTRA_CONFIG)) {
            stateMachineContext.getChildDeviceConfigCodec().encodeDeviceProfileConfig(extraConfig,
                    deviceProfile.getConfigProperties(), deviceProject.getProjectConfigValues(), deviceProject.getLineAddress());
            return true;
        }
        return false;
    }

    private boolean fillDatabaseErrors(List<ActivationValidateMessage> validateMessages) {
        boolean dbValid = true;
        /* Проверяем исполнительные устройства */
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            if (activeDevice.getDeviceProject().getDeviceCategory() == DeviceCategory.EXECUTIVE
                    && !engagedInScenarioActionsDeviceIds.contains(activeDevice.getId())) {
                ActivationValidateMessage unusedExecutiveDeviceMessage = new ActivationValidateMessage();
                unusedExecutiveDeviceMessage.level = Level.WARNING;
                unusedExecutiveDeviceMessage.code = Code.UNUSED_EXECUTIVE_DEVICES;
                unusedExecutiveDeviceMessage.entityType = EntityType.DEVICE;
                unusedExecutiveDeviceMessage.entityId = activeDevice.getId();
                unusedExecutiveDeviceMessage.description = Code.UNUSED_EXECUTIVE_DEVICES.description;
                validateMessages.add(unusedExecutiveDeviceMessage);
            }
            if (activeDevice.getDeviceProject().getDeviceCategory() == DeviceCategory.EXECUTIVE
                    && engagedInScenarioActionsDeviceIds.contains(activeDevice.getId())
                    && illegalScenarioExecutiveIds.contains(activeDevice.getId())) {
                ActivationValidateMessage illegalScenarioExecutiveMessage = new ActivationValidateMessage();
                illegalScenarioExecutiveMessage.level = Level.ERROR;
                illegalScenarioExecutiveMessage.code = Code.ILLEGAL_SCENARIO_EXECUTIVE_DEVICES;
                illegalScenarioExecutiveMessage.entityType = EntityType.DEVICE;
                illegalScenarioExecutiveMessage.entityId = activeDevice.getId();
                illegalScenarioExecutiveMessage.description = Code.ILLEGAL_SCENARIO_EXECUTIVE_DEVICES.description;
                validateMessages.add(illegalScenarioExecutiveMessage);
                dbValid = false;
            }
        }
        /* Проверяем наличие зон с названием превышающим 20 символов */
        for (ActiveRegion activeRegion : stateMachineContext.getRegions()) {
            Region region = activeRegion.getRegionProject();
            if (region.getName() != null && region.getName().length() > TABLE_REGION_TITLE_SIZE) {
                ActivationValidateMessage largeRegionNameMessage = new ActivationValidateMessage();
                largeRegionNameMessage.level = Level.WARNING;
                largeRegionNameMessage.code = Code.REGION_NAME_IS_LARGE;
                largeRegionNameMessage.entityType = EntityType.REGION;
                largeRegionNameMessage.entityId = activeRegion.getId();
                largeRegionNameMessage.description = Code.REGION_NAME_IS_LARGE.description;
                validateMessages.add(largeRegionNameMessage);
            }
        }
        /* Проверяем наличие устройств МПТ, не привязанных к зоне */
        for (String mptDeviceId : mptNotInRegionDeviceIds) {
            ActivationValidateMessage mptNotInRegionMessage = new ActivationValidateMessage();
            mptNotInRegionMessage.level = Level.ERROR;
            mptNotInRegionMessage.code = Code.MPT_NOT_IN_REGION;
            mptNotInRegionMessage.entityType = EntityType.DEVICE;
            mptNotInRegionMessage.entityId = mptDeviceId;
            mptNotInRegionMessage.description = Code.MPT_NOT_IN_REGION.description;
            validateMessages.add(mptNotInRegionMessage);
            dbValid = false;
        }
        /* Проверяем наличие более одного устройства МПТ в какой-либо зоне */
        for (String mptRegionId : multipleMptRegionIds) {
            ActivationValidateMessage multipleMptInRegionMessage = new ActivationValidateMessage();
            multipleMptInRegionMessage.level = Level.ERROR;
            multipleMptInRegionMessage.code = Code.MULTIPLE_MPT_IN_REGION;
            multipleMptInRegionMessage.entityType = EntityType.REGION;
            multipleMptInRegionMessage.entityId = mptRegionId;
            multipleMptInRegionMessage.description = Code.MULTIPLE_MPT_IN_REGION.description;
            validateMessages.add(multipleMptInRegionMessage);
            dbValid = false;
        }
        /* Проверяем наличие датчиков, не привязанных к зоне */
        for (String noRegionSensorId : noRegionSensorIds) {
            ActivationValidateMessage noRegionSensorMessage = new ActivationValidateMessage();
            noRegionSensorMessage.level = Level.ERROR;
            noRegionSensorMessage.code = Code.SENSOR_WITHOUT_REGION;
            noRegionSensorMessage.entityType = EntityType.DEVICE;
            noRegionSensorMessage.entityId = noRegionSensorId;
            noRegionSensorMessage.description = Code.SENSOR_WITHOUT_REGION.description;
            validateMessages.add(noRegionSensorMessage);
            dbValid = false;
        }
        // Валидация: каждая насосная станция должна содержать как минимум один пожарный насос
        for (String notEnoughInternalDevicesVCId : notEnoughInternalDevicesVCIds) {
            ActivationValidateMessage notEnoughInternalDevicesMessage = new ActivationValidateMessage();
            notEnoughInternalDevicesMessage.level = Level.ERROR;
            notEnoughInternalDevicesMessage.code = Code.NOT_ENOUGH_INTERNAL_DEVICES;
            notEnoughInternalDevicesMessage.entityType = EntityType.DEVICE;
            notEnoughInternalDevicesMessage.entityId = notEnoughInternalDevicesVCId;
            notEnoughInternalDevicesMessage.description = Code.NOT_ENOUGH_INTERNAL_DEVICES.description;
            validateMessages.add(notEnoughInternalDevicesMessage);
            dbValid = false;
        }
        // TODO: Проверять, есть ли логика “И” с зоной, содержащей безадресное устройство
        return dbValid;
    }

    public void writeDbHash() {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(dbMain);
            digest.digest(dbHash, 0, dbHash.length);
        } catch (NoSuchAlgorithmException | DigestException e) {
            stateMachineContext.getLogger().error("Failed calculate MD5");
        }
        System.arraycopy(dbHash, 0, dbMain, dbHashOffset, dbHash.length);
    }

    public boolean generateDatabase(List<ActivationValidateMessage> validateMessages) {
        prepareDatabase();

        // Делаем проход 3 раза, т.к. на первый раз подсчитываются ссылки на
        // устройства из шаблонов логики, во второй раз - запоминаются смещения
        // всех сущностей, а на третий - эти смещения подставляются в ссылках
        firstPass = true;
        for (int i = 0; i < 3; i++) {
            dbMain = generateMainDatabase();
            dbConfig = generateConfigDatabase();
            if (firstPass) {
                dbExtraConfig = generateExtraConfigDatabase();
                dbRecovery = generateRecoveryDatabase();
            }
            firstPass = false;
        }
        recalculateCrc(dbMain, dbCrcOffset, DB_CRC_STUB.length, dbCrcOffset + 2, dbMain.length - 20);

        /* Добавим MD5 */
        writeDbHash();

        return fillDatabaseErrors(validateMessages);
    }
}
