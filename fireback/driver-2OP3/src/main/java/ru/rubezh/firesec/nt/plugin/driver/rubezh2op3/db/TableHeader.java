package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

public class TableHeader {
    int address = 0;
    int entryLength = 0;
    int entryCount = 0;

    public TableHeader() {

    }

    public TableHeader(int address, int entryLength, int entryCount) {
        this.address = address;
        this.entryLength = entryLength;
        this.entryCount = entryCount;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getEntryLength() {
        return entryLength;
    }

    public void setEntryLength(int entryLength) {
        this.entryLength = entryLength;
    }

    public int getEntryCount() {
        return entryCount;
    }

    public void setEntryCount(int entryCount) {
        this.entryCount = entryCount;
    }

}
