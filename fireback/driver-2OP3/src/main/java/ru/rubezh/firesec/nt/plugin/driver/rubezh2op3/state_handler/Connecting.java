package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public class Connecting extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.CONNECTING;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        Connecting handler = new Connecting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType mode) {
        stateMachineContext.setRubezh2op3DeviceMode(mode);
        stateMachineContext.getStateMachineHooks().onConnected(currentTimeMs);
        if (mode == Rubezh2OP3UpdateSoftwareStatusType.NORMAL_OPERATION) {
            stateMachineContext.getStateMachineHooks().onFirmwareUpdateResumed(currentTimeMs);
            stateMachineContext.getActionExecutor().getControlDeviceInfo(currentTimeMs);
            return State.GETTING_RUBEZH2OP3_INFO;
        } else {
            stateMachineContext.getLogger().warn("Wrong device mode: {}", mode);
            stateMachineContext.getStateMachineHooks().onFirmwareUpdateFailed(currentTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.CHECK_MODE,
                    stateMachineContext.getModeCheckingInterval(), true);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.EXECUTE_TASK,
                    stateMachineContext.getTaskExecuteInterval(), true);
            return State.WAITING_TASK;
        }
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RECONNECT,
                stateMachineContext.getReconnectInterval(), false);
        return State.WAIT_RECONNECT;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        return onDisconnected(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        return onDisconnected(currentTimeMs);
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        return onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        return State.IDLE;
    }
}
