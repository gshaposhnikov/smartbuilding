package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class FixedWorkSchemaTE implements SkudTableEntry {

    /** Идентификатор графика работ */
    private String workScheduleId;

    public FixedWorkSchemaTE(String workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        WorkSchedule workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules().get(workScheduleId);
        assert workSchedule != null;
        assert workSchedule.getWorkScheduleType() != WorkScheduleType.OTHER;

        writeId(workSchedule.getIndex(), stream);
        boolean accessGrantedTimesExist = false;
        for (WorkScheduleDay day : workSchedule.getDays()) {
            writeId(day.getIndex(), stream);
            /*
             * TODO: выходные, по документации, заполняются 0xFFFF. Но пока этого не стал делать, т.к. для выходных тоже
             * можно определить интервалы прохода. Признак выходного дня скорее нужен при формировании отчета.
             */
            if (!day.getAccessGrantedTimes().isEmpty()) {
                accessGrantedTimesExist = true;
            }
        }
        writeBitFlags(stream);

        if (!accessGrantedTimesExist) {
            skudDatabaseBuilder.addWarning(Code.WORK_SCHEDULE_WITHOUT_ANY_ACCESS_GRANTED_TIME, EntityType.WORK_SCHEDULE,
                    workScheduleId);
        }
    }

}
