package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

public enum Rubezh2OP3UpdatingDatabaseStage {
    INVOKING_ARM_UPDATE_MODE,
    INVOKING_LOADER_UPDATE_MODE,
    ERASING_SECTORS,
    WRITING_DATA,
    RESETTING,
    ABORTING_ON_FAILURE
}
