package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import ru.rubezh.firesec.nt.amqp.message.request.DeleteTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.PauseTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.SetDriverDeviceConfigRequest;
import ru.rubezh.firesec.nt.amqp.message.response.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.Issue.IssueParameterNames;
import ru.rubezh.firesec.nt.domain.v1.license.ControlRestriction;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HXPParsingHandler;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile.MemoryType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.Rijndael;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.Task;

public class Rubezh2OP3ExternalRequestHandlerImpl implements Rubezh2OP3ExternalRequestHandler {
    private final String DECIPHER_KEY = "A71216EAE1754DFCA306A1484390BCF2";
    private final int BLOCK_SIZE = 32;
    private final String SIGNATURE = "FRMWARE";
    private final int FILE_KEY_SIZE =256;
    private final int FILE_BLOCK_SIZE = 256;
    private final int SIGNATURE_DATA_LENGTH = 12;

    private Rubezh2OP3StateMachineContext stateMachineContext;

    public Rubezh2OP3ExternalRequestHandlerImpl(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    private List<ActiveDevice> findAllActiveDeviceByRegionId(String regionId) {
        List<ActiveDevice> regions = new ArrayList<>();
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            if (regionId.equals(activeDevice.getRegionId())) {
                regions.add(activeDevice);
            }
        }
        return regions;
    }

    private ActiveDevice findActiveDevice(String deviceId) {
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            if (activeDevice.getId().equals(deviceId)) {
                return activeDevice;
            }
        }
        return null;
    }

    private ActiveScenario findActiveScenarioByGlobalNumber(int scenarioNo) {
        for (ActiveScenario activeScenario : stateMachineContext.getScenarios()) {
            if (activeScenario.getScenarioProject().getGlobalNo() == scenarioNo) {
                return activeScenario;
            }
        }
        return null;
    }

    @Override
    public void handleMessage(Issue issue, CreateIssueDriverResponse response) {
        if (!stateMachineContext.getStateHandler().isConnected()) {
            Issue actualIssue = new Issue(issue);
            actualIssue.generateId();
            Date currentDate = new Date();
            actualIssue.setCreateDateTime(currentDate);
            actualIssue.setStartDateTime(currentDate);
            actualIssue.setFinishDateTime(currentDate);
            actualIssue.setStatus(IssueStatus.FAILED);
            actualIssue.setStatusMessage("Связь с устройством потеряна");
            actualIssue.setProgress(100);
            response.getActualIssues().add(actualIssue);
            return;
        }

        switch (issue.getAction()) {
        case WRITE_CONFIG_TO_DEVICE:
            writeActiveDeviceConfig(issue, response);
            break;
        case READ_CONFIG_FROM_DEVICE:
            readActiveDeviceConfig(issue, response);
            break;
        case RESET_STATE_ON_DEVICE:
        case RESET_STATE_ON_ALL_DEVICES:
            resetDeviceState(issue, response);
            break;
        case WRITE_CONTROL_DEVICE_DATABASE:
        case WRITE_ALL_CONTROL_DEVICES_DATABASE:
            writeControlDeviceDatabase(issue, response);
            break;
        case UPDATE_CONTROL_DEVICE_FIRMWARE:
        case UPDATE_ALL_CONTROL_DEVICE_FIRMWARE:
            updateControlDeviceSoftware(issue, response);
            break;
        case ENABLE_REGION_DEVICES_POLLING_STATE:
        case DISABLE_REGION_DEVICES_POLLING_STATE:
            setRegionDevicesPollingState(issue, response);
            break;
        case ENABLE_DEVICE_POLLING_STATE:
        case DISABLE_DEVICE_POLLING_STATE:
            setDevicePollingState(issue, response);
            break;
        case ENABLE_REGION_GUARD:
        case DISABLE_REGION_GUARD:
            changeSecurityRegionStatus(issue, response);
            break;
        case SET_CONTROL_DEVICE_TIME:
            setControlDeviceTime(issue, response);
            break;
        case SET_CONTROL_DEVICE_USER_PASSWORD:
            setControlDeviceUserPassword(issue, response);
            break;
        case READ_CONTROL_DEVICE_DATABASE:
            readControlDeviceDatabase(issue, response);
            break;
        case RESET_CONTROL_DEVICE:
            resetControlDevice(issue, response);
            break;
        case READ_EVENTS:
            readEvents(issue, response);
            break;
        case PERFORM_DEVICE_ACTION:
            writeDeviceCommand(issue, response, true);
            break;
        case PERFORM_SCENARIO_ACTION:
        case PERFORM_INDICATOR_SCENARIO_ACTION:
            writeScenarioCommand(issue, response, true);
            break;
        case ENABLE_INDICATOR_REGIONS_GUARD:
        case DISABLE_INDICATOR_REGIONS_GUARD:
            changeIndicatorSecurityRegionsStatus(issue, response);
            break;
        case ENABLE_INDICATOR_REGIONS_POLLING_STATE:
        case DISABLE_INDICATOR_REGIONS_POLLING_STATE:
            setIndicatorRegionsPollingState(issue, response);
            break;
        case ENABLE_INDICATOR_DEVICES_POLLING_STATE:
        case DISABLE_INDICATOR_DEVICES_POLLING_STATE:
            setIndicatorDevicesPollingState(issue, response);
            break;
        case WRITE_SKUD_DATABASE:
            enqueueWriteVariableSkudDatabase(issue, response);
            break;
        case START_READ_ACCESS_KEY:
        case STOP_READ_ACCESS_KEY:
            enqueueSetWaitingAccessKeyStatus(issue, response);
            break;
        default:
            Issue actualIssue = new Issue(issue);
            actualIssue.generateId();
            Date currentDate = new Date();
            actualIssue.setCreateDateTime(currentDate);
            actualIssue.setStartDateTime(currentDate);
            actualIssue.setFinishDateTime(currentDate);
            actualIssue.setStatus(IssueStatus.FAILED);
            actualIssue.setStatusMessage("Задача не поддерживается: " + issue.getAction());
            actualIssue.setProgress(100);
            response.getActualIssues().add(actualIssue);
        }
    }

    @Override
    public void startIssue(Issue issue) {
        switch (issue.getAction()) {
        case WRITE_CONFIG_TO_DEVICE:
            startWriteActiveDeviceConfig(issue);
            break;
        case READ_CONFIG_FROM_DEVICE:
            startReadActiveDeviceConfig(issue);
            break;
        case RESET_STATE_ON_DEVICE:
        case RESET_STATE_ON_ALL_DEVICES:
            startResetDeviceState(issue);
            break;
        case WRITE_CONTROL_DEVICE_DATABASE:
        case WRITE_ALL_CONTROL_DEVICES_DATABASE:
            startWriteControlDeviceDatabase(issue);
            break;
        case UPDATE_CONTROL_DEVICE_FIRMWARE:
        case UPDATE_ALL_CONTROL_DEVICE_FIRMWARE:
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(new Date().getTime(), issue.getId(),
                    IssueStatus.FAILED, "Данные для выполнения задачи утеряны");
            break;
        case ENABLE_DEVICE_POLLING_STATE:
        case DISABLE_DEVICE_POLLING_STATE:
            startSetDevicePollingState(issue);
            break;
        case ENABLE_REGION_GUARD:
        case DISABLE_REGION_GUARD:
            startChangeSecurityRegionStatus(issue);
            break;
        case SET_CONTROL_DEVICE_TIME:
            startSetControlDeviceTime(issue);
            break;
        case SET_CONTROL_DEVICE_USER_PASSWORD:
            startSetControlDeviceUserPassword(issue);
            break;
        case READ_CONTROL_DEVICE_DATABASE:
            startReadControlDeviceDatabase(issue);
            break;
        case RESET_CONTROL_DEVICE:
            startResetControlDevice(issue);
            break;
        case READ_EVENTS:
            startReadEvents(issue);
            break;
        case PERFORM_DEVICE_ACTION:
            startWriteDeviceCommand(issue);
            break;
        case PERFORM_SCENARIO_ACTION:
            startWriteScenarioCommand(issue);
            break;
        case WRITE_SKUD_DATABASE:
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(new Date().getTime(), issue.getId(),
                    IssueStatus.FAILED, "Данные для выполнения задачи утеряны");
            break;
        case START_READ_ACCESS_KEY:
        case STOP_READ_ACCESS_KEY:
            restoreSetWaitingAccessKeyStatus(issue);
            break;
        default:
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage("Задача не поддерживается: " + issue.getAction());
        }
        /* TODO: отправлять уведомление по задачам с ошибками */
    }

    private void readActiveDeviceConfig(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        startReadActiveDeviceConfig(actualIssue);
    }

    private void startReadActiveDeviceConfig(Issue issue) {
        ActiveDevice activeDevice = findActiveDevice(issue.getDeviceId());
        if (activeDevice != null) {
            Device deviceProject = activeDevice.getDeviceProject();
            if (deviceProject.getAddressType() == AddressType.GENERIC) {
                stateMachineContext.getTaskManager().enqueueGetChildDeviceConfig(
                        activeDevice, issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
            } else {
                issue.setStatus(IssueStatus.FAILED);
                issue.setStatusMessage("Чтение конигурации нешлейфового устройства: " + issue.getDeviceId());
            }
        } else {
            activeDevice = stateMachineContext.getRubezh2op3Device();
            if (activeDevice.getId().equals(issue.getDeviceId())) {
                stateMachineContext.getTaskManager().enqueueGetRubezh2op3Config(
                        activeDevice, issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
            } else {
                issue.setStatus(IssueStatus.FAILED);
                issue.setStatusMessage("Устройство не найдено: " + issue.getDeviceId());
            }
        }
    }

    private void writeActiveDeviceConfig(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        startWriteActiveDeviceConfig(actualIssue);
    }

    private void startWriteActiveDeviceConfig(Issue issue) {
        ActiveDevice activeDevice = findActiveDevice(issue.getDeviceId());
        if (activeDevice != null) {
            Device deviceProject = activeDevice.getDeviceProject();
            if (deviceProject.getAddressType() == AddressType.GENERIC) {
                DeviceProfile profile = stateMachineContext.getSupportedDeviceProfiles()
                        .get(deviceProject.getDeviceProfileId());
                Map<String, String> propertyValues = deviceProject.getProjectConfigValues();
                for (String propertyId : profile.getConfigProperties().keySet()) {
                    propertyValues.put(propertyId,
                            propertyValues.getOrDefault(propertyId,
                                    profile.getConfigProperties().get(propertyId).getDefaultValue()));
                }
                stateMachineContext.getTaskManager().enqueueSetChildDeviceConfig(activeDevice,
                        propertyValues, issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
            } else {
                issue.setStatus(IssueStatus.FAILED);
                issue.setStatusMessage("Запись конфигурации нешлейфового устройства: " + issue.getDeviceId());
            }
        } else {
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage("Устройство не найдено: " + issue.getDeviceId());
        }
    }

    private void resetDeviceState(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startResetDeviceState(actualIssue);
    }

    private void startResetDeviceState(Issue issue) {
        ManualResetStateGroup manualResetStateGroup = ManualResetStateGroup
                .valueOf((String) issue.getParameters().get("manualResetStateGroup"));
        switch (manualResetStateGroup) {
        case FIRE:
        case ALARM:
            stateMachineContext.getTaskManager().enqueueResetStateDevice(stateMachineContext.getRubezh2op3Device(),
                    issue.getId(), manualResetStateGroup, issue.getStatus() == IssueStatus.PAUSE);
            break;
        default:
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage("Не поддерживается сброс данной группы: " + manualResetStateGroup);
            break;
        }
    }

    private void writeControlDeviceDatabase(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startWriteControlDeviceDatabase(actualIssue);
    }

    private void startWriteControlDeviceDatabase(Issue issue) {
        Boolean writeRecovery = (Boolean)issue.getParameters().get("recoveryNecessary");
        Boolean writeConstantSkud = (Boolean) issue.getParameters().get("accessControlNecessary");

        if (writeConstantSkud != null && writeConstantSkud && !stateMachineContext.getSkudDatabaseBuilder().isDatabaseValid()) {
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage("БД СКУД содержит критические ошибки");
            return;
        }
        stateMachineContext.getTaskManager().enqueueWriteDeviceDatabase(issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE, true,
                writeRecovery == null ? false : writeRecovery,
                writeConstantSkud == null ? false : writeConstantSkud, false);

        // перед записью базы необходимо (попытаться) записать адресный лист прибора (для работы межприборных ссылок)
        stateMachineContext.getTaskManager().enqueueSetAddressList();
    }

    private void enqueueWriteVariableSkudDatabase(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        if (!stateMachineContext.getSkudDatabaseBuilder().isDatabaseValid()) {
            actualIssue.setStatus(IssueStatus.FAILED);
            actualIssue.setStatusMessage("БД СКУД содержит критические ошибки");
            return;
        }
        /* stateMachineContext.getSkudDatabaseBuilder().dumpDb("SSL"); */
        stateMachineContext.getTaskManager().enqueueWriteDeviceDatabase(actualIssue.getId(),
                actualIssue.getStatus() == IssueStatus.PAUSE, false, false, true, true);
    }

    private void updateControlDeviceSoftware(Issue issue, CreateIssueDriverResponse response) {
        try {
            
            String fwFileContent = (String) issue.getParameters().get("fwFile");
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser saxParser = spf.newSAXParser();
            XMLReader xmlReader = saxParser.getXMLReader();
            HXPParsingHandler handler = new HXPParsingHandler(stateMachineContext.getLogger());
            xmlReader.setContentHandler(handler);
            xmlReader.setProperty("http://xml.org/sax/properties/lexical-handler", handler);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.getDecoder().decode(fwFileContent));
            Rijndael encryptor = new Rijndael(FILE_KEY_SIZE, FILE_BLOCK_SIZE, inputStream.available());
            int blockCounter = inputStream.available() / (encryptor.getfBlockSize() / 8);
            if ((inputStream.available() % (encryptor.getfBlockSize() / BLOCK_SIZE)) != 0) {
                blockCounter++;
            }
            encryptor.initialize(DECIPHER_KEY, DECIPHER_KEY.length());
            /** расшифровываем блоками по 32 байта */
            for (int i = 0; i < blockCounter; i++) {
                byte[] pIn = new byte[BLOCK_SIZE];
                inputStream.read(pIn, 0, pIn.length);
                inputStream.mark(BLOCK_SIZE * i + BLOCK_SIZE);
                encryptor.decipher(pIn);
            }
            if (!new String(Arrays.copyOf(encryptor.getpOut(), SIGNATURE.length())).equals(SIGNATURE)) {
                response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
                response.setErrorString("Файл прошивки невозможно расшифровать");
            } else {
                /* 
                 * после расшифровки получаем заархивированые данные,
                 * которые разархивируем. 
                 * */
                byte[] zipData = new byte[encryptor.getpOut().length - SIGNATURE_DATA_LENGTH];
                System.arraycopy(encryptor.getpOut(), SIGNATURE_DATA_LENGTH, zipData, 0, encryptor.getpOut().length - SIGNATURE_DATA_LENGTH);
                Inflater decompressor = new Inflater();
                decompressor.setInput(zipData);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream(zipData.length);
                /*
                 * c 8 по 11 byte хранится длина разархивированого массива
                 * склеиваем 4 byte в int.
                 */
                byte[] buffer = new byte[((encryptor.getpOut()[8])) 
                                      | (encryptor.getpOut()[9] << 8)
                                      | (encryptor.getpOut()[10] << 16) 
                                      | (encryptor.getpOut()[11] << 24)];
                while (!decompressor.finished()) {
                    try {
                        int count = decompressor.inflate(buffer);
                        outputStream.write(buffer, 0, count);
                    } catch (DataFormatException e) {
                        response.setError(ErrorType.INTERNAL_ERROR);
                        response.setErrorString("Неизвестная ошибка: " + e.getMessage());
                        return;
                    }
                }
                try {
                    outputStream.close();
                } catch (IOException e) {
                    response.setError(ErrorType.INTERNAL_ERROR);
                    response.setErrorString("Неизвестная ошибка: " + e.getMessage());
                    return;
                }
                byte[] decompressedData = outputStream.toByteArray();
                inputStream = new ByteArrayInputStream(decompressedData);
                xmlReader.parse(new InputSource(inputStream));

                /* TODO: брать название устройства из профиля драйвера */
                if (!handler.getRequiredDeviceName().equals("Рубеж-2ОП3")) {
                    response.setError(ErrorType.CONTENT_NOT_SUPPORTED);
                    response.setErrorString("Файл прошивки - для устройства " + handler.getRequiredDeviceName());
                } else if (handler.getHexFiles().isEmpty()) {
                    response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
                    response.setErrorString("Файл прошивки не содержит HEX-файлов");
                } else {
                    for (HexFile hexFile : handler.getHexFiles()) {
                        Issue actualIssue = new Issue(issue);
                        actualIssue.generateId();
                        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
                        actualIssue.getParameters().put("fwFile", hexFile.getFileName());
                        response.getActualIssues().add(actualIssue);
                        if (hexFile.areAttributesValid() && hexFile.isDataValid()) {
                            if (hexFile.getMemoryType() == MemoryType.APP ||
                                    hexFile.getMemoryType() == MemoryType.LOADER_FLASH) {
                                stateMachineContext.getTaskManager().enqueueUpdateDeviceSoftware(actualIssue.getId(),
                                        hexFile, handler.getVersion(), actualIssue.getStatus() == IssueStatus.PAUSE);
                            } else {
                                Date currentDate = new Date();
                                actualIssue.setCreateDateTime(currentDate);
                                actualIssue.setStartDateTime(currentDate);
                                actualIssue.setFinishDateTime(currentDate);
                                actualIssue.setProgress(100);
                                actualIssue.setStatus(IssueStatus.FAILED);
                                actualIssue.setStatusMessage(
                                        "Прошивка " + hexFile.getMemoryType().toString() + " не поддерживается");
                            }
                        } else {
                            Date currentDate = new Date();
                            actualIssue.setCreateDateTime(currentDate);
                            actualIssue.setStartDateTime(currentDate);
                            actualIssue.setFinishDateTime(currentDate);
                            actualIssue.setProgress(100);
                            actualIssue.setStatus(IssueStatus.FAILED);
                            actualIssue.setStatusMessage(hexFile.getFailureMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            response.setError(ErrorType.INTERNAL_ERROR);
            response.setErrorString("Неизвестная ошибка: " + e.getMessage());
        }
    }

    private void setControlDeviceTime(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startSetControlDeviceTime(actualIssue);
    }

    private void startSetControlDeviceTime(Issue issue) {
        stateMachineContext.getTaskManager().enqueueSetRubezh2op3Time(issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE);
    }

    private DeviceUserType getControlDeviceUserType(String usernameString) {
        try {
            return DeviceUserType.valueOf(usernameString);
        } catch (IllegalArgumentException | NullPointerException ex) {
            return null;
        }
    }

    private String getControlDeviceUserPassword(String passwordString) {
        String passwordPattern = stateMachineContext.getRubezh2op3DeviceProfile().getUserPasswordPattern();
        try {
            if (passwordString != null && passwordPattern != null &&
                    passwordString.matches(passwordPattern))
                return passwordString;
        } catch (PatternSyntaxException ex) {
        }
        return null;
    }

    private void setControlDeviceUserPassword(Issue issue, CreateIssueDriverResponse response) {
        DeviceUserType userType = getControlDeviceUserType((String) issue.getParameters().get("username"));
        String userPassword = getControlDeviceUserPassword((String) issue.getParameters().get("password"));
        if (userType == null || userPassword == null) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Имя пользователя или пароль заданы не верно");
        } else {
            Issue actualIssue = new Issue(issue);
            actualIssue.generateId();
            actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
            response.getActualIssues().add(actualIssue);
            startSetControlDeviceUserPassword(actualIssue, userType, userPassword);
        }
    }

    private void startSetControlDeviceUserPassword(Issue issue) {
        DeviceUserType userType = getControlDeviceUserType((String) issue.getParameters().get("username"));
        String userPassword = getControlDeviceUserPassword((String) issue.getParameters().get("password"));
        if (userType == null || userPassword == null) {
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage("Имя пользователя или пароль заданы не верно");
        } else {
            startSetControlDeviceUserPassword(issue, userType, userPassword);
        }
    }

    private void startSetControlDeviceUserPassword(Issue issue, DeviceUserType userType, String password) {
        stateMachineContext.getTaskManager().enqueueSetRubezh2op3UserPassword(
                userType, password, issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
    }

    private void readControlDeviceDatabase(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startReadControlDeviceDatabase(actualIssue);
    }

    private void startReadControlDeviceDatabase(Issue issue) {
        stateMachineContext.getTaskManager().enqueueDatabaseConfigRecovery(
                issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
    }

    private void startWriteDeviceCommand(Issue issue) {
        CreateIssueDriverResponse response = new CreateIssueDriverResponse();
        writeDeviceCommand(issue, response, false);
        if (response.getError() != ErrorType.OK) {
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage(response.getErrorString());
        }
    }

    public static Boolean getBooleanProperty(Device device, DeviceProfile deviceProfile, DeviceProperties propertyKind) {
        return DeviceProperty.getFlagOrDefault(
                device.getDevicePropertyValues().get(propertyKind.toString()),
                deviceProfile.getDeviceProperties().get(propertyKind.toString()));
    }

    private void writeDeviceCommand(Issue issue, CreateIssueDriverResponse response, boolean createIssueNecessary) {
        String actionId;
        Map<?, ?> wildcardActionParameters;
        Map<String, String> actionParameters = null;
        try {
            actionId = (String) issue.getParameters().get("actionId");
            wildcardActionParameters = (Map<?, ?>) issue.getParameters().get("actionParameters");
            if (wildcardActionParameters != null) {
                actionParameters = new HashMap<>();
                for (Map.Entry<?, ?> entry : wildcardActionParameters.entrySet()) {
                    actionParameters.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
                }
            }
        } catch (ClassCastException e) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Несоответствие типов аргументов");
            return;
        }
        if (actionId == null) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Не задан идентификатор действия");
            return;
        }
        ActiveDevice activeDevice = findActiveDevice(issue.getDeviceId());
        if (activeDevice == null) {
            response.setError(ErrorType.UNKNOWN_DEVICE);
            response.setErrorString("Не найдено устройство: " + issue.getDeviceId());
            return;
        }
        Device deviceProject = activeDevice.getDeviceProject();
        DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                .get(deviceProject.getDeviceProfileId());
        if (deviceProfile == null) {
            response.setError(ErrorType.INTERNAL_ERROR);
            response.setErrorString("Не найден профиль устройства: " +
                    deviceProject.getDeviceProfileId());
            return;
        }
        if (!getBooleanProperty(deviceProject, deviceProfile, DeviceProperties.ManageAllowed)) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Управление данным устройством запрещено");
            return;
        }
        if (deviceProfile.getSupportedActions() == null ||
                !deviceProfile.getSupportedActions().containsKey(actionId)) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Для даннго устройства действие не поддерживается: " + actionId);
            return;
        }
        ControlRestriction control = stateMachineContext.getLicenseReader().readLicenseRestrictions().getControl();
        if (deviceProfile.isFirefightingDevice()) {
            if (!control.isFirefightingEnabled()) {
                response.setError(ErrorType.FORBIDDEN);
                response.setErrorString("Отсутствует лицензия на управление пожаротушением");
                return;
            }
        } else {
            if (!control.isEngineeringEnabled()) {
               response.setError(ErrorType.FORBIDDEN);
               response.setErrorString("Отсутствует лицензия на инженерное управление");
               return;
           }
        }

        Issue actualIssue = issue;
        if (createIssueNecessary) {
            actualIssue = new Issue(issue);
            actualIssue.generateId();
            response.getActualIssues().add(actualIssue);
        }

        stateMachineContext.getTaskManager().enqueuePerformDeviceActionTask(
                activeDevice, actionId, actionParameters,
                actualIssue.getId(), actualIssue.getStatus() == IssueStatus.PAUSE);
    }

    private void startWriteScenarioCommand(Issue issue) {
        CreateIssueDriverResponse response = new CreateIssueDriverResponse();
        writeScenarioCommand(issue, response, false);
        if (response.getError() != ErrorType.OK) {
            issue.setStatus(IssueStatus.FAILED);
            issue.setStatusMessage(response.getErrorString());
        }
    }

    private ScenarioManageAction getSupportedScenarioManageAction(String actionString) {
        try {
            return ScenarioManageAction.valueOf(actionString);
        } catch (IllegalArgumentException | NullPointerException ex) {
            return null;
        }
    }

    private void writeScenarioCommand(Issue issue, CreateIssueDriverResponse response, boolean createIssueNecessary) {
        ScenarioManageAction manageAction = null;
        int scenarioNo = -1;
        try {
            manageAction = getSupportedScenarioManageAction(
                    (String) issue.getParameters().get("actionId"));
            scenarioNo = (int) issue.getParameters().get("scenarioNo");
        } catch (ClassCastException e) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Несоответствие типов аргументов");
            return;
        }
        if (manageAction == null) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Действие не задано");
            return;
        }
        switch (manageAction) {
            case START:
            case STOP:
            case BLOCK:
            case UNBLOCK:
                break;
            default:
                response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
                response.setErrorString("Действие не поддерживается");
                return;
        }
        if (scenarioNo < 0 || scenarioNo > 0xffff) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Не задан номер сценария");
            return;
        }
        ActiveScenario activeScenario = findActiveScenarioByGlobalNumber(scenarioNo);
        if (activeScenario == null) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Сценарий с номером " + scenarioNo + " не найден");
            return;
        } else if (activeScenario.getScenarioProject().getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS){
            response.setError(ErrorType.CONTENT_NOT_SUPPORTED);
            response.setErrorString("Действия по сценариям с тактическим назначением запрещены");
            return;
        }
        ControlRestriction control = stateMachineContext.getLicenseReader().readLicenseRestrictions().getControl();
        if (!control.isFirefightingEnabled() && !control.isEngineeringEnabled()) {
            response.setError(ErrorType.FORBIDDEN);
            response.setErrorString("Отсутствует лицензия на управление сценариями");
            return;
        }

        if (createIssueNecessary) {
            issue = new Issue(issue);
            issue.generateId();
            response.getActualIssues().add(issue);
        }

        stateMachineContext.getTaskManager().enqueuePerformScenarioActionTask(
                activeScenario, manageAction,
                issue.getId(), issue.getStatus() == IssueStatus.PAUSE);
    }

    private void resetControlDevice(Issue issue, CreateIssueDriverResponse response){
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startResetControlDevice(actualIssue);
    }

    private void startResetControlDevice(Issue issue){
        stateMachineContext.getTaskManager().enqueueResetRubezh2op3(issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE);
    }

    private void changeIndicatorSecurityRegionsStatus(Issue issue, CreateIssueDriverResponse response) {
        try {
            List<String> regionIds = new ArrayList<>();
            for (Object object : (List<?>) issue.getParameters().get("entityIds")) {
                regionIds.add(String.valueOf(object));
            }
            List<String> stateMachineRegionIds =
                    stateMachineContext.getRegions().stream().map(ActiveRegion::getId).collect(Collectors.toList());
            for (String regionId : regionIds) {
                if (stateMachineRegionIds.contains(regionId)) {
                    Issue subIssue = new Issue(issue);
                    subIssue.generateId();
                    subIssue.setAction(issue.getAction() == IssueAction.ENABLE_INDICATOR_REGIONS_GUARD
                            ? IssueAction.ENABLE_REGION_GUARD : IssueAction.DISABLE_REGION_GUARD);
                    subIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
                    subIssue.getParameters().put("regionId", regionId);
                    response.getActualIssues().add(subIssue);
                    startChangeSecurityRegionStatus(subIssue);
                }
            }
        } catch (ClassCastException ex){
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Идентификаторы зон заданы неправильно");
        }
    }

    private void changeSecurityRegionStatus(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        actualIssue.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        response.getActualIssues().add(actualIssue);
        startChangeSecurityRegionStatus(actualIssue);
    }

    private void startChangeSecurityRegionStatus(Issue issue) {
        String regionId = issue.getParameters().get("regionId").toString();
        for (ActiveRegion region : stateMachineContext.getRegions()) {
            if (region.getId().equals(regionId)) {
                break;
            }
        }
        stateMachineContext.getTaskManager().enqueueSetSecurityRegionStatus(stateMachineContext.getRubezh2op3Device(),
                regionId, issue.getAction() == IssueAction.ENABLE_REGION_GUARD, issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE);
    }

    private void setIndicatorRegionsPollingState(Issue issue, CreateIssueDriverResponse response){
        try {
            List<String> regionIds = new ArrayList<>();
            for (Object object : (List<?>) issue.getParameters().get("entityIds")) {
                regionIds.add(String.valueOf(object));
            }
            for (String regionId : regionIds) {
                for (ActiveDevice device : findAllActiveDeviceByRegionId(regionId)) {
                    Issue subIssue = new Issue(issue);
                    subIssue.getParameters().clear();
                    subIssue.generateId();
                    subIssue.setDeviceId(device.getId());
                    subIssue.setAction(issue.getAction() == IssueAction.ENABLE_INDICATOR_REGIONS_POLLING_STATE ?
                            IssueAction.ENABLE_DEVICE_POLLING_STATE : IssueAction.DISABLE_DEVICE_POLLING_STATE);
                    response.getActualIssues().add(subIssue);
                    startSetDevicePollingState(subIssue);
                }
            }
        } catch (ClassCastException ex) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Идентификаторы зон заданы неправильно");
        }
    }

    private void setIndicatorDevicesPollingState(Issue issue, CreateIssueDriverResponse response){
        try {
            List<String> deviceIds = new ArrayList<>();
            for (Object object : (List<?>) issue.getParameters().get("entityIds")) {
                deviceIds.add(String.valueOf(object));
            }
            List<String> stateMachineDeviceIds = stateMachineContext.getChildDevices().stream()
                    .map(ActiveDevice::getId).collect(Collectors.toList());
            for (String deviceId: deviceIds) {
                if (stateMachineDeviceIds.contains(deviceId)) {
                    Issue subIssue = new Issue(issue);
                    subIssue.getParameters().clear();
                    subIssue.generateId();
                    subIssue.setDeviceId(deviceId);
                    subIssue.setAction(issue.getAction() == IssueAction.ENABLE_INDICATOR_DEVICES_POLLING_STATE ?
                            IssueAction.ENABLE_DEVICE_POLLING_STATE : IssueAction.DISABLE_DEVICE_POLLING_STATE);
                    response.getActualIssues().add(subIssue);
                    startSetDevicePollingState(subIssue);
                }
            }
        } catch (ClassCastException ex) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Идентификаторы устройств заданы неправильно");
        }
    }

    private void setRegionDevicesPollingState(Issue issue, CreateIssueDriverResponse response) {
        String regionId = issue.getParameters().get("regionId").toString();
        for (ActiveDevice device: findAllActiveDeviceByRegionId(regionId)) {
            Issue actualIssue = new Issue(issue);
            actualIssue.setAction(issue.getAction() == IssueAction.ENABLE_REGION_DEVICES_POLLING_STATE ?
                    IssueAction.ENABLE_DEVICE_POLLING_STATE : IssueAction.DISABLE_DEVICE_POLLING_STATE);
            actualIssue.setDeviceId(device.getId());
            actualIssue.generateId();
            response.getActualIssues().add(actualIssue);
            startSetDevicePollingState(actualIssue);
        }
    }

    private void setDevicePollingState(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        startSetDevicePollingState(actualIssue);
    }

    private void startSetDevicePollingState(Issue issue) {
        ActiveDevice activeDevice = findActiveDevice(issue.getDeviceId());
        stateMachineContext.getTaskManager().enqueueSetDevicePollingState(activeDevice,
                issue.getAction() == IssueAction.ENABLE_DEVICE_POLLING_STATE, issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE);
    }

    private void readEvents(Issue issue, CreateIssueDriverResponse response) {
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        startReadEvents(actualIssue);
    }

    private void startReadEvents(Issue issue) {
        List<String> issueLogIds = new ArrayList<>();
        for (Object issueLogId: (List<?>) issue.getParameters().get("logIds")) {
            issueLogIds.add(String.valueOf(issueLogId));
        }
        stateMachineContext.getTaskManager().enqueueReadEvents(issueLogIds, issue.getId(),
                issue.getStatus() == IssueStatus.PAUSE);
    }

    private void enqueueSetWaitingAccessKeyStatus(Issue issue, CreateIssueDriverResponse response) {
        if (issue.getAction() == IssueAction.START_READ_ACCESS_KEY
                && issue.getParameters().get(IssueParameterNames.accessKeyId.toString()) == null) {
            response.setError(ErrorType.BAD_ISSUE_PARAMETERS);
            response.setErrorString("Не указан идентификатор ключа доступа");
        }
        Issue actualIssue = new Issue(issue);
        actualIssue.generateId();
        response.getActualIssues().add(actualIssue);
        restoreSetWaitingAccessKeyStatus(actualIssue);
    }

    private void restoreSetWaitingAccessKeyStatus(Issue issue) {
        ActiveDevice activeDevice = findActiveDevice(issue.getDeviceId());
        Object oAccessKeyId = issue.getParameters().get(IssueParameterNames.accessKeyId.toString());

        stateMachineContext.getTaskManager().enqueueWaitingAccessKeyStatus(issue.getId(), activeDevice,
                issue.getAction() == IssueAction.START_READ_ACCESS_KEY,
                oAccessKeyId != null ? oAccessKeyId.toString() : null,
                issue.getStatus() == IssueStatus.PAUSE);
    }

    @Override
    public SetDriverDeviceConfigResponse handleMessage(SetDriverDeviceConfigRequest request) {
        ActiveDevice activeDevice = findActiveDevice(request.getDeviceId());
        SetDriverDeviceConfigResponse response = new SetDriverDeviceConfigResponse();
        if (activeDevice != null) {
            stateMachineContext.getDatabaseBuilder().updateDeviceConfig(
                    request.getDeviceId(), request.getActiveConfigValues());
        } else {
            activeDevice = stateMachineContext.getRubezh2op3Device();
            if (activeDevice.getId().equals(request.getDeviceId())) {
                // TODO: выполнить вызов "обновить настройки прибора" когда/если таковые появятся
            } else {
                response.setError(ErrorType.UNKNOWN_DEVICE);
                response.setErrorString("Устройство не найдено: " + request.getDeviceId());
            }
        }
        return response;
    }

    @Override
    public DeleteTaskDriverResponse handleMessage(DeleteTaskDriverRequest request) {
        DeleteTaskDriverResponse response = new DeleteTaskDriverResponse();
        if (request.getIssueId() == null) {
            /* Очистка очереди */
            response.setDeletedIssueIds(stateMachineContext.getTaskManager().clearAllExceptCurrent());
        } else {
            Task currentTask = stateMachineContext.getTaskManager().getCurrentTask();
            if (currentTask != null && request.getIssueId().equals(currentTask.getIssueId())) {
                /* Отмена текущей задачи */
                response.setError(ErrorType.ISSUE_ACTIVE);
                response.setErrorString("Задача уже выполняется");
            } else {
                /* Отмена задачи из очереди */
                stateMachineContext.getTaskManager().removeTaskByIssueId(request.getIssueId());
            }
        }
        return response;
    }

    @Override
    public PauseTaskDriverResponse handleMessage(PauseTaskDriverRequest request) {
        PauseTaskDriverResponse response = new PauseTaskDriverResponse();
        Task currentTask = stateMachineContext.getTaskManager().getCurrentTask();
        if (currentTask != null && request.getIssueId().equals(currentTask.getIssueId())) {
            /* Текущая задача не должна меняться */
            response.setError(ErrorType.ISSUE_ACTIVE);
            response.setErrorString("Задача уже выполняется");
        } else {
            if(request.isPause())
                stateMachineContext.getTaskManager().deferTaskByIssueId(request.getIssueId());
            else
                stateMachineContext.getTaskManager().returnDeferredTaskByIssueId(request.getIssueId());
        }
        return response;
    }

    @Override
    public boolean updateVarSkudDatabase(List<Employee> employees, List<AccessKey> accessKeys,
            List<WorkSchedule> workSchedules, Map<String, List<AccessKey>> accessKeysByEmployeeIds,
            List<ActivationValidateMessage> validateMessages) {
        stateMachineContext.updateVarSkudDatabase(employees, accessKeys, workSchedules, accessKeysByEmployeeIds);
        return stateMachineContext.getSkudDatabaseBuilder().generateVarDatabase(validateMessages);
    }

}
