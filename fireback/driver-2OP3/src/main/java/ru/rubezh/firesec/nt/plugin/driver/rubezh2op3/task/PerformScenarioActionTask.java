package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveScenario;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.ScenarioManageAction;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class PerformScenarioActionTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveScenario activeScenario;
    private ScenarioManageAction manageAction;

    public PerformScenarioActionTask(ActiveScenario activeScenario, ScenarioManageAction manageAction, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeScenario = activeScenario;
        this.manageAction = manageAction;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        int scenarioNo = activeScenario.getScenarioProject().getGlobalNo();
        stateMachineContext.getActionExecutor().performScenarioAction(currentTimeMs, scenarioNo, manageAction);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.PERFORMING_SCENARIO_ACTION;
    }

    public ActiveScenario getActiveScenario() {
        return activeScenario;
    }

    public ScenarioManageAction getManageAction() {
        return manageAction;
    }

}
