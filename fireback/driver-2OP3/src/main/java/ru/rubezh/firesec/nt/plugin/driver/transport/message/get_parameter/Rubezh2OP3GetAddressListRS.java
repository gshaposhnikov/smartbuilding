package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.List;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetAddressListRS extends Rubezh2OP3GetParameterRS {

    private List<Integer> addressList;

    public Rubezh2OP3GetAddressListRS() {
        super(Rubezh2OP3ParameterType.ADDRESS_LIST);
    }

    public List<Integer> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Integer> addressList) {
        this.addressList = addressList;
    }

}
