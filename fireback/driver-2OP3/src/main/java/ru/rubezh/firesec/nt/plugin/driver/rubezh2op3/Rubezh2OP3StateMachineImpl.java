package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.io.IOException;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;

public class Rubezh2OP3StateMachineImpl implements StateMachine {

    private Rubezh2OP3StateMachineContext stateMachineContext;
    private boolean started;
    private long startTimeMs;

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public Rubezh2OP3StateMachineImpl(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    @Override
    public boolean start(long currentTimeMs, List<ActivationValidateMessage> validateMessages) {
        if (stateMachineContext.getDatabaseBuilder().generateDatabase(validateMessages)) {
            /*
             * БД СКУД также генерируется при запуске КА, но ее корректность будет влиять только в момент выполнения
             * задачи на запись БД прибора. Ошибки валидации же пользователь получит при активации.
             */
            stateMachineContext.getSkudDatabaseBuilder().generateConstDatabase(validateMessages);
            stateMachineContext.getStateHandler().onConnectRequest(currentTimeMs);
            stateMachineContext.getActionExecutor().startSendingEventsUpstream(currentTimeMs);
            startTimeMs = currentTimeMs;
            started = true;
        }
        return started;
    }

    @Override
    public void step(long currentTimeMs) {
        if (isStarted()) {
            stateMachineContext.getClock().process(currentTimeMs);
            try {
                stateMachineContext.getTransport().process(currentTimeMs);
            } catch (IOException e) {
                stateMachineContext.getLogger().error("IO exception with message: {}", e.getMessage());
            }
        }
    }

    @Override
    public void stop(long currentTimeMs) {
        if (isStarted()) {
            stateMachineContext.getActionExecutor().stopSendingEventsUpstream(currentTimeMs);
            stateMachineContext.getStateHandler().onDisconnectRequest(currentTimeMs);
        }
    }

    @Override
    public long getLastActivityTime() {
        long lastActivityTimeMs = stateMachineContext.getTransport().getLastActivityTime();
        if (lastActivityTimeMs == 0)
            lastActivityTimeMs = startTimeMs;
        return lastActivityTimeMs;
    }

}
