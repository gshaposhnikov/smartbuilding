package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model;

import java.util.*;
import java.util.stream.Collectors;

import ru.rubezh.firesec.nt.domain.v1.ActiveScenario;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;

/**
 * Обертка над сценарием - обрабатывает исполнительные блоки, сортирует их,
 * преобразует абсолютные задержки в относительные и подсчитывает использования
 * сущностей в условиях для записи в БД
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ScenarioWrapper {
    /**
     * Максимальное время задержки относительно предыдущего блока, которое не
     * нужно разбивать на подблоки
     */
    private static int MAXIMAL_BLOCK_RELATIVE_DELAY = 250;

    /** Оригинальный сценарий */
    public ActiveScenario scenario = null;
    /** Исполнительные блоки */
    public List<ScenarioTimeLineBlockRepr> timeLineBlocks = new ArrayList<>();

    /** Использования дочерних устройств в условиях к блокам */
    public Map<String, List<Integer>> childDeviceConditionUsages = new HashMap<>();
    /** Использования виртуальных состояний в условиях к блокам */
    public Map<String, List<Integer>> virtualStateConditionUsages = new HashMap<>();

    private boolean checkTracingBlock(ScenarioTimeLineBlock timeLineBlock, List<ScenarioTimeLineBlock> tracingTLBlocks){
        return tracingTLBlocks.stream().anyMatch(
                tracingTLBlock -> timeLineBlock.getTimeDelaySec() >= tracingTLBlock.getTracingStartSec()
                    && timeLineBlock.getTimeDelaySec() <= tracingTLBlock.getTracingStartSec() + tracingTLBlock.getTracingPeriodSec()
        );
    }

    public ScenarioWrapper(ActiveScenario scenario) {
        this.scenario = scenario;

        // Сначала оборачиваем все исполнительные блоки и блоки слежения, пока
        // выставляем им абсолютное время начала
        List<ScenarioTimeLineBlockRepr> tmpBlocks = new ArrayList<>();
        List<ScenarioTimeLineBlock> tracingTLBlocks = scenario.getScenarioProject().getTimeLineBlocks()
                .stream()
                .filter(tlBlock -> tlBlock.getBlockType() == BlockType.TRACING)
                .collect(Collectors.toList());
        for (ScenarioTimeLineBlock timeLineBlock : scenario.getScenarioProject().getTimeLineBlocks()) {
            ScenarioTimeLineBlockRepr timeLineBlockRepr = null;
            if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE)
                timeLineBlockRepr = new ScenarioTimeLineBlockRepr(timeLineBlock,
                        checkTracingBlock(timeLineBlock, tracingTLBlocks));
            else if (timeLineBlock.getBlockType() == BlockType.TRACING)
                timeLineBlockRepr = new ScenarioTimeLineBlockRepr(timeLineBlock.getTracingStartSec());

            if (timeLineBlockRepr != null) {
                tmpBlocks.add(timeLineBlockRepr);
            }
        }
        // Сортируем по времени начала
        Collections.sort(tmpBlocks);

        // Теперь объединяем все блоки, имеющие одно время начала
        int previousDelay = 0;
        for (int i = 0; i < tmpBlocks.size();) {
            ScenarioTimeLineBlockRepr currentRepr = tmpBlocks.get(i);
            int currentDelay = currentRepr.getRelativeDelay();
            int relativeDelay = currentDelay - previousDelay;

            // Если у нас задержка относительно предыдущего блока больше
            // заданной, вставляем нужное количество фейковых блоков
            while (relativeDelay > MAXIMAL_BLOCK_RELATIVE_DELAY) {
                ScenarioTimeLineBlockRepr delayBlock = new ScenarioTimeLineBlockRepr(MAXIMAL_BLOCK_RELATIVE_DELAY);
                timeLineBlocks.add(delayBlock);
                relativeDelay -= MAXIMAL_BLOCK_RELATIVE_DELAY;
            }

            // Все блоки с одинаковым временем объединим в один
            for (++i; i < tmpBlocks.size() && tmpBlocks.get(i).getRelativeDelay() == currentDelay; ++i) {
                currentRepr.getActions().addAll(tmpBlocks.get(i).getActions());
            }
            currentRepr.setRelativeDelay(relativeDelay);
            timeLineBlocks.add(currentRepr);
            previousDelay = currentDelay;
        }

        // Запомним ссылки дочерних устройств и витруальных состояний
        // на блоки времени в соответствии с интервалами блоков слежения
        for (ScenarioTimeLineBlock block : scenario.getScenarioProject().getTimeLineBlocks()) {
            if (block.getBlockType() == BlockType.TRACING) {
                int currentDelay = 0;
                for (int i = 0; i < timeLineBlocks.size(); i++) {
                    currentDelay += timeLineBlocks.get(i).getRelativeDelay();
                    if (currentDelay > (block.getTracingStartSec() + block.getTracingPeriodSec())) {
                        break;
                    }
                    if (currentDelay >= block.getTracingStartSec()) {
                        switch (block.getTracingEntityType()) {
                        case SENSOR_DEVICE:
                            addConditionUsage(childDeviceConditionUsages, block.getTracingEntityId(), i);
                            break;
                        case VIRTUAL_STATE:
                            addConditionUsage(virtualStateConditionUsages, block.getTracingEntityId(), i);
                            break;
                        default:
                            break;
                        }
                    }
                }
            }
        }
        
        // Запомним ссылки дочерних устройств и витруальных состояний
        // на блоки времени в соответствии с наличием условий к действиям
        for (int i = 0; i < timeLineBlocks.size(); i++) {
            ScenarioTimeLineBlockRepr blockRepr = timeLineBlocks.get(i);
            for (ScenarioTimeLineBlockAction action : blockRepr.getActions()) {
                switch (action.getConditionType()) {
                case DEVICE_STATE:
                    addConditionUsage(childDeviceConditionUsages, action.getConditionEntityId(), i);
                    break;
                case VIRTUAL_STATE:
                    addConditionUsage(virtualStateConditionUsages, action.getConditionEntityId(), i);
                    break;
                default:
                    break;
                }
            }
        }
    }

    private void addConditionUsage(Map<String, List<Integer>> usagesMap, String entityId, int blockIdx) {
        if (!usagesMap.containsKey(entityId))
            usagesMap.put(entityId, new ArrayList<>());
        if (!usagesMap.get(entityId).contains(blockIdx))
            usagesMap.get(entityId).add(blockIdx);
    }
}