package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.ByteOrder;

/**
 * Поток, сериализующий данные для записи в БД прибора
 *
 * @author Андрей Лисовой
 *
 */
public class DatabaseOutputStream extends ByteArrayOutputStream {

    /** Кодировка текста в приборе */
    private static final Charset CHARSET = Charset.forName("Windows-1251");

    /**
     * Запись в поток числа старшим байтом вперёд
     * 
     * @param value
     *            число для записи
     * @param size
     *            фактический размер числа (число значимых байт)
     */
    public void writeInteger(int value, int size) {
        assert size >= 1 && size <= 4 : "Недопустимый размер числа: " + size;
        // помещаем целое в буфер (по умолчанию big-endian)
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(value);
        // пишем заданное число байт в поток
        for (int index = 4 - size; index < 4; index++)
            write(buffer.get(index));
    }

    /**
     * Запись в поток числа младшим байтом вперёд
     * 
     * @param value
     *            число для записи
     * @param size
     *            фактический размер числа (число значимых байт)
     */
    public void writeIntegerLittleEndian(int value, int size) {
        assert size >= 1 && size <= 4 : "Недопустимый размер числа: " + size;
        // помещаем целое в буфер (устанавливаем порядок little-endian)
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(value);
        // пишем заданное число байт в поток
        for (int index = 0; index < size; index++)
            write(buffer.get(index));
    }

    /**
     * Запись в поток массива байт
     * 
     * @param data
     *            массив байт для записи
     */
    public void writeByteArray(byte[] data) {
        write(data, 0, data.length);
    }

    /**
     * Запись нулей в поток
     * 
     * @param numOfZeros
     *            количество байт, заполняемых нулями
     * @return размер потока до записи
     */
    public int writeZeros(int numOfZeros) {
        int oldSize = size();
        for (int index = 0; index < numOfZeros; index++)
            write(0);
        return oldSize;
    }

    /**
     * Запись в поток строки фиксированного размера (обрезая или дополняя пробелами)
     * 
     * @param text
     *            текст для записи
     * @param size
     *            размер строки
     */
    public void writeText(String text, int size) {
        assert size > 0 : "Недопустимый размер строки: " + size;
        // Дополнение пробелами справа и кодирование
        byte[] bytes = String.format("%1$-" + size + "s", text).getBytes(CHARSET);
        // Обрезка массива справа и запись в поток
        write(bytes, 0, size);
    }

}
