package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

public enum DeviceUserType {
    DUTY,
    INSTALLER,
    ADMINISTRATOR,
}
