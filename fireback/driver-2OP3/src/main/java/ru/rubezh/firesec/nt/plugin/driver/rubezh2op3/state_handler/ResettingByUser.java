package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;


public class ResettingByUser extends AbstractConnected {

    public final static int FINISH_PROGRESS = 90;
    public final static int PROGRESS_INCREMENT = 10;

    @Override
    public State getCurrentState() {
        return State.RESETTING_BY_USER;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        ResettingByUser handler = new ResettingByUser();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onResetInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RESET_TIMEOUT,
                stateMachineContext.getReconnectInterval() / 10, true);
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        return onResetInvoked(currentTimeMs, 0);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        return onResetInvoked(currentTimeMs, 0);
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.RESET_TIMEOUT)) {
            if (stateMachineContext.getResetProgress() < FINISH_PROGRESS){
                updateResetProgress();
                stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                        stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                        stateMachineContext.getResetProgress());
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RESET_TIMEOUT,
                        stateMachineContext.getReconnectInterval() / 10, true);
                return getCurrentState();
            } else {
                String taskIssueId = stateMachineContext.getTaskManager().getCurrentTask().getIssueId();
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, taskIssueId,
                        IssueStatus.FINISHED, "");
                stateMachineContext.setResetProgress(0);
                stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RECONNECT,
                        stateMachineContext.getReconnectInterval(), false);
                return State.WAIT_RECONNECT;
            }
        }
        return getCurrentState();
    }

    private void updateResetProgress(){
        stateMachineContext.setResetProgress(stateMachineContext.getResetProgress() + PROGRESS_INCREMENT);
    }
}