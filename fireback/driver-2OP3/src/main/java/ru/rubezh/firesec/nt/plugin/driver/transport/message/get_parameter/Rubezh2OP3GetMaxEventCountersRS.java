package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.Map;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetMaxEventCountersRS extends Rubezh2OP3GetParameterRS {
    private Map<String, Long> logCounters;

    public Rubezh2OP3GetMaxEventCountersRS() {
        super(Rubezh2OP3ParameterType.MAX_EVENT_COUNTERS);
    }

    public Map<String, Long> getLogCounters() {
        return logCounters;
    }

    public void setLogCounters(Map<String, Long> logCounters) {
        this.logCounters = logCounters;
    }
}
