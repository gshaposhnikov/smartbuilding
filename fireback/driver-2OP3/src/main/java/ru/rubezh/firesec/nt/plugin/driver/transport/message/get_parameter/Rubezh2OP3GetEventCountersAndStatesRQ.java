package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetEventCountersAndStatesRQ extends Rubezh2OP3GetParameterRQ {

    public Rubezh2OP3GetEventCountersAndStatesRQ() {
        super(Rubezh2OP3ParameterType.EVENT_COUNTERS_AND_STATES);
    }

}
