package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetDataBlockRQ extends Rubezh2OP3GetParameterRQ {

    private int address;
    private int length;

    public Rubezh2OP3GetDataBlockRQ(int address, int length) {
        super(Rubezh2OP3ParameterType.DB_BLOCK);
        this.address = address;
        this.length = length;
    }

    public int getAddress() {
        return address;
    }

    public int getLength() {
        return length;
    }

}
