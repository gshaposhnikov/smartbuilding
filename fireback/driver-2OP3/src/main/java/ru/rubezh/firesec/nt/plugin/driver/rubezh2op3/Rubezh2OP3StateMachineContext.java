package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseReader;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.ChildDeviceConfigCodec;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud.SkudDatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.StateHandler;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.Rubezh2OP3MessageCodec;

public interface Rubezh2OP3StateMachineContext {
    public StateHandler getStateHandler();

    public Rubezh2OP3ActionExecutor getActionExecutor();

    public DriverAlertSender getDriverAlertSender();

    public DriverProfile getDriverProfile();

    public List<DriverProfileView> getDriverProfileViews();

    public Map<String, DeviceProfileView> getDeviceProfileViewsByIds();

    public DeviceProfile getRubezh2op3DeviceProfile();

    public ActiveDevice getRubezh2op3Device();

    public void setRubezh2op3Device(ActiveDevice device);

    public List<ActiveDevice> getChildDevices();

    public ActiveDevice updateChildDeviceConfig(String deviceId, Map<String, String> configValues);

    public List<ActiveRegion> getRegions();

    public List<ActiveScenario> getScenarios();

    public List<ActiveScenario> getExternalScenarios();

    public Map<String, Set<String>> getControlDeviceIdsByScenarioIds();

    public List<VirtualState> getVirtualStates();

    public Map<String, ActiveDevice> getControlDevicesByChildDeviceIds();

    public List<ActiveDevice> getExternalExecutiveChildDevices();

    public Set<String> getEngagedInScenarioActionsDeviceIds();

    public Map<String, ActiveDevice> getAllProjectDevicesByIds();

    public Transport<ByteBuffer> getTransport();

    public Clock getClock();

    public Rubezh2OP3MessageCodec getMessageCodec();

    public Rubezh2OP3StateMachineHooks getStateMachineHooks();

    public void setStateMachineHooks(Rubezh2OP3StateMachineHooks stateMachineHooks);

    public ChildDeviceConfigCodec getChildDeviceConfigCodec();

    public DatabaseBuilder getDatabaseBuilder();

    public Logger getLogger();

    public int getReconnectInterval();

    public int getResetTimeout();

    public int getGetDeviceConfigurationRetryInterval();

    public int getCheckEventsInterval();

    public int getCheckRegionHeadersInterval();

    public int getCheckRegionStatesInterval();

    public int getCheckChildDeviceStatesInterval();

    public int getSetTimeInterval();

    public int getTaskExecuteInterval();

    public int getUpdateSoftwareModeRetry();

    public int getStartWritingRetryTO();

    public int getModeCheckingInterval();

    public Map<String, DeviceProfile> getSupportedDeviceProfiles();

    public Rubezh2OP3TaskManager getTaskManager();

    public Rubezh2OP3ReadEventsState getReadEventsState();

    public ActiveRegion getNextActiveRegionToCheckStates();

    public Rubezh2OP3ChildDevicePollingState getChildDevicePollingState();

    public Rubezh2OP3ReadEventsState getFetchingEventsState();

    public void setFetchingEventsState(Rubezh2OP3ReadEventsState fetchingEventsState);

    public void setDeviceMaxEventCounters(Map<String, Long> maxEventCounters);

    public Map<String, Long> getDeviceMaxEventCounters();

    public void setDeviceEventCounters(Map<String, Long> eventCounters);

    public Map<String, Long> getDeviceEventCounters();

    public void setControlDeviceInfo(Rubezh2OP3DeviceInfo controlDeviceInfo);

    public Rubezh2OP3DeviceInfo getControlDeviceInfo();

    public void setLongOperationWaitingTimeMs(int waitingTimeMs);

    public int getLongOperationWaitingTimeMs();

    public Rubezh2OP3UpdatingDatabaseState getUpdatingDatabaseState();

    public Rubezh2OP3UpdatingSoftwareState getUpdatingSoftwareState();

    public void resetUpdatingSoftwareState();

    public BitSet getRegionStateBits(String regionId);

    public void setRegionStateBits(String regionId, BitSet regionStateBits);

    public boolean checkEventsReadyToSend();

    public int getBatchEventsCount();

    public int getEventsSendTimeOut();

    public int getWriteBlockSize();

    public List<ControlDeviceEvent> getEvents();

    public void setEvents(List<ControlDeviceEvent> events);

    public void addEvent(ControlDeviceEvent event);

    public int getResetProgress();

    public void setResetProgress(int resetProgress);

    public LicenseReader getLicenseReader();

    public Rubezh2OP3UpdateSoftwareStatusType getRubezh2op3DeviceMode();

    public void setRubezh2op3DeviceMode(Rubezh2OP3UpdateSoftwareStatusType mode);

    public boolean isBadSignature();

    public void setBadSignature(boolean badSignature);

    /* СКУД */

    public SkudDatabaseBuilder getSkudDatabaseBuilder();

    public void updateVarSkudDatabase(List<Employee> employees, List<AccessKey> accessKeys,
            List<WorkSchedule> workSchedules, Map<String, List<AccessKey>> accessKeysByEmployeeIds);

    public Map<String, Employee> getEmployees();

    public Map<String, AccessKey> getAccessKeys();

    public Map<String, WorkSchedule> getWorkSchedules();

    public Map<String, List<AccessKey>> getAccessKeysByEmployeeIds();

    public String getWaitingAccessKeyId();

    public void setWaitingAccessKeyId(String accessKeyId);

}
