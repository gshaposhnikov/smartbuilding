package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public interface Rubezh2OP3SetGetParameterRQ extends Rubezh2OP3Message {
    public Rubezh2OP3ParameterType getParameterType();
}
