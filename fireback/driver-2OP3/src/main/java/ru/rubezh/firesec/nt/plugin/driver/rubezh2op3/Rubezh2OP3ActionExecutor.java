package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.Date;
import java.util.Map;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.DeviceAction;
import ru.rubezh.firesec.nt.domain.v1.ManualResetStateGroup;
import ru.rubezh.firesec.nt.domain.v1.ScenarioManageAction;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;

public interface Rubezh2OP3ActionExecutor {
    public void connect(long currentTimeMs);
    public void disconnect(long currentTimeMs);
    public void setTimer(long currentTimeMs, String name, int interval, boolean isRepeatable);
    public void setIssueCloseTimer(long currentTimeMs, String issueId, int interval);
    public void resetTimer(long currentTimeMs, String name);
    public void resetAllTimers(long currentTimeMs);
    public void startSendingEventsUpstream(long currentTimeMs);
    public void stopSendingEventsUpstream(long currentTimeMs);
    public void forcedSendEventsUpstream(long currentTimeMs);
    public void getControlDeviceEventCountersAndStates(long currentTimeMs);
    public void getFirmwareVersion(long currentTimeMs);
    public void getDatabaseVersion(long currentTimeMs);
    public void getControlDeviceInfo(long currentTimeMs);
    public void getDataBlock(long currentTimeMs, int address, int length);
    public void getDeviceMaxEventCounters(long currentTimeMs);
    public void getEvent(long currentTimeMs, int logCode, long eventIndex);
    public void getRegionStates(long currentTimeMs, String regionId);
    public void getChildDeviceStates(long currentTimeMs, int lineNo, int address, String regionId);
    public void getChildDeviceConfig(long currentTimeMs, int lineNo, int address);
    public void setChildDeviceConfig(long currentTimeMs, int type, int lineNo, int address,
            Map<String, String> propertyValues);
    public void performDeviceAction(long currentTimeMs, int type, int lineNo, int address, DeviceAction deviceAction,
            Map<String, String> propertyValues);
    public void resetStateDevice(long currentTimeMs, int lineNo, int address, ManualResetStateGroup targetResetGroup);
    public void invokeUpdateSoftwareMode(long currentTimeMs, Rubezh2OP3UpdateSoftwareModeType softwareType);
    public void checkDeviceReadyState(long currentTimeMs);
    public void reset(long currentTimeMs);
    public void getAddressList(long currentTimeMs);
    public void setAddressList(long currentTimeMs, List<Integer> addressList);

    /**
     * Стереть сектора флеш-памяти.
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @param startSector
     *            начальный сектор (ДОЛЖЕН БЫТЬ СТРОГО БОЛЬШЕ 0, НУЛЕВОЙ СЕКТОР
     *            СТИРАТЬ НЕЛЬЗЯ!!!)
     * @param endSector
     *            конечный сектор
     * @return true, если параметры для отправки команды верные
     */
    public boolean eraseSectors(long currentTimeMs, int startSector, int endSector);
    public void writeMemory(long currentTimeMs, int address, byte[] data);
    public void getUpdateSoftwareError(long currentTimeMs);
    public void getUpdateSoftwareStatus(long currentTimeMs);
    public void changeSecurityRegionStatus(long currentTimeMs, String regionId, boolean onGuard);
    public void changeDevicePollingState(long currentTimeMs, int type, int lineNo, int address, boolean pollingState);
    public void setDeviceTime(long currentTimeMs, Date dateTime);
    public void setDeviceUserPassword(long currentTimeMs, DeviceUserType userType, String password);
    public void setFrimwareVersion(long currentTimeMs, int hexWordVersion);
    public void performScenarioAction(long currentTimeMs, int globalScenarioNo, ScenarioManageAction manageAction);
    public void setWaitingAccessKeyStatus(long currentTimeMs, int lineNo, int address, boolean waiting);
}
