package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetWaitingAccessKeyStatusRQ extends Rubezh2OP3SetParameterRQ {
    private int lineNo;
    private int address;
    private boolean waiting;

    public Rubezh2OP3SetWaitingAccessKeyStatusRQ(int lineNo, int address, boolean waiting) {
        super(Rubezh2OP3ParameterType.WAITING_ACCESS_KEY_STATUS);
        this.lineNo = lineNo;
        this.address = address;
        this.waiting = waiting;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

}
