package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfileView;
import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;

/**
 * Элемент таблицы списка точек доступа, сформированный по ключу доступа.
 * 
 * Должен использоваться, когда точки доступа прописаны ключу доступа и не
 * прописаны сотруднику.
 * 
 * @author Антон Васильев
 *
 */
public class AccessKeyAccessPointTE extends AccessPointListTE {

    public AccessKeyAccessPointTE(String ownerId) {
        super(ownerId);
    }

    @Override
    protected List<ActiveDevice> getAccessPointDevices(SkudDatabaseBuilder skudDatabaseBuilder) {
        List<String> deviceIds = skudDatabaseBuilder.getStateMachineContext().getAccessKeys()
                .get(ownerId).getAccessMap().getAccessPointDeviceIds();
        List<ActiveDevice> devices = new ArrayList<>();
        for (String deviceId : deviceIds) {
            ActiveDevice device = skudDatabaseBuilder.getStateMachineContext()
                    .getAllProjectDevicesByIds().get(deviceId);
            DeviceProfileView deviceProfileView = skudDatabaseBuilder.getStateMachineContext()
                    .getDeviceProfileViewsByIds()
                    .get(device.getDeviceProject().getDeviceProfileId());
            if (deviceProfileView != null && deviceProfileView.getDeviceProfile().isAccessPoint()) {
                devices.add(device);
            } else {
                skudDatabaseBuilder.addWarning(Code.WRONG_ACCESS_POINT_REFERENCE,
                        EntityType.ACCESS_KEY, ownerId);
            }
        }
        return devices;
    }

}
