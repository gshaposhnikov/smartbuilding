package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3ReadEventsState.EventDescriptor;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.ReadEventsRubezh2op3;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;


public class WaitingDeviceEventsByUser extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_DEVICE_EVENTS_BY_USER;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingDeviceEventsByUser handler = new WaitingDeviceEventsByUser();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onLogEventReceived(long currentTimeMs, ControlDeviceEvent logEvent, BitSet regionStates, String regionId) {
        if (regionStates != null && regionId != null)
            stateMachineContext.setRegionStateBits(regionId, regionStates);
        EventDescriptor receivedEvent = stateMachineContext.getReadEventsState().getCurrentEvent();
        EventDescriptor eventToRead = stateMachineContext.getReadEventsState().getNextEvent();

        if (logEvent != null) {
            logEvent.setLogTypeId(receivedEvent.logId);
            logEvent.setEventNo(receivedEvent.index);
            if (logEvent.getDeviceInfo() != null && logEvent.getDeviceInfo().isDeviceLocal())
                logEvent.getDeviceInfo().setControlDeviceAddress(
                        stateMachineContext.getRubezh2op3Device().getDeviceProject().getLineAddress());
            logEvent.getControlDeviceInfo().setId(stateMachineContext.getRubezh2op3Device().getId());
            logEvent.setOnline(false);

            stateMachineContext.getStateMachineHooks().onLogEventReceived(currentTimeMs, logEvent);

            stateMachineContext.addEvent(logEvent);
            if (stateMachineContext.checkEventsReadyToSend()) {
                stateMachineContext.getActionExecutor().forcedSendEventsUpstream(currentTimeMs);
            }
        }

        try {
            ReadEventsRubezh2op3 task = (ReadEventsRubezh2op3) stateMachineContext.getTaskManager().getCurrentTask();
            if (eventToRead != null) {
                task.eventHasBeenRead();
                Double progressUpdate = task.getProgressUpdate();
                if (progressUpdate != null)
                    stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs, task.getIssueId(),
                            progressUpdate);
            } else {
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                        IssueStatus.FINISHED, "");
            }
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }

        if (eventToRead != null) {
            stateMachineContext.getActionExecutor().getEvent(currentTimeMs, eventToRead.logCode, eventToRead.index);
            return getCurrentState();
        } else {
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.START_NEXT_STAGE)) {
            try {
                ReadEventsRubezh2op3 task =
                        (ReadEventsRubezh2op3) stateMachineContext.getTaskManager().getCurrentTask();
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                            IssueStatus.FINISHED, "");
            } catch (ClassCastException e) {
                stateMachineContext.getLogger().warn("Error casting Task");
            }
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
        return super.onTimer(currentTimeMs, timerName);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        try {
            ReadEventsRubezh2op3 task = (ReadEventsRubezh2op3) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onLogEventReceiveFailed(currentTimeMs, exception);
            if (task.getIssueId() != null)
                stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                        task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}