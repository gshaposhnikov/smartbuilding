package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

/* 
 * Переходы при подключении через МС:
 *  ->
 *  onSoftwareUpdateModeInvoked -> setTimer(LONG_OPERATION_CHECK)
 *                          |
 *  onTimer(LONG_OPERATION_CHECK) -> checkDeviceReadyState
 *                          |
 *  onDeviceReadyStateReceived -> getUpdateSoftwareError
 *                          |
 *  onUpdateSoftwareErrorReceived -> getUpdateSoftwareStatus
 *                          |
 *  onUpdateSoftwareStatusReceived -> invokeUpdateSoftwareMode
 *  ->
 */
/* 
 * Переходы при подключении по USB:
 *  ->
 *  onDisconnected -> setTimer(UPDATE_SOFTWARE_MODE_RECONNECT)
 *                          |
 *  onTimer(UPDATE_SOFTWARE_MODE_RECONNECT) -> connect
 *                          |
 *  onConnected -> getUpdateSoftwareStatus
 *                          |
 *  onUpdateSoftwareStatusReceived -> invokeUpdateSoftwareMode
 *  ->
 */
public class InvokingArmUpdateMode extends InvokingUpdateMode {

    @Override
    public State getCurrentState() {
        return State.INVOKING_ARM_UPDATE_MODE;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        InvokingArmUpdateMode handler = new InvokingArmUpdateMode();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    protected Rubezh2OP3UpdateSoftwareModeType getInvokingSoftwareMode() {
        return Rubezh2OP3UpdateSoftwareModeType.ARM;
    }

    @Override
    protected Rubezh2OP3UpdateSoftwareStatusType getAwaitingSoftwareStatus() {
        return Rubezh2OP3UpdateSoftwareStatusType.UPDATE_ARM;
    }

    @Override
    protected State toNextState(long currentTimeMs) {
        stateMachineContext.getUpdatingDatabaseState()
                .setStage(Rubezh2OP3UpdatingDatabaseStage.INVOKING_LOADER_UPDATE_MODE);
        try {
            WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs, task.getIssueId(),
                    WriteDatabaseTask.getArmUpdateProgress());
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
        stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                Rubezh2OP3UpdateSoftwareModeType.LOADER);
        return State.INVOKING_LOADER_UPDATE_MODE;
    }

}
