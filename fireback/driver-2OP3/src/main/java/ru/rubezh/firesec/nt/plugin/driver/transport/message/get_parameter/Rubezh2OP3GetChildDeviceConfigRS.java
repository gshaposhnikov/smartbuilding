package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetChildDeviceConfigRS extends Rubezh2OP3GetParameterRS {
    ChildDeviceConfigInfo childDeviceConfigInfo;

    public Rubezh2OP3GetChildDeviceConfigRS() {
        super(Rubezh2OP3ParameterType.CHILD_DEVICE_CONFIG);
    }

    public ChildDeviceConfigInfo getChildDeviceConfigInfo() {
        return childDeviceConfigInfo;
    }

    public void setChildDeviceConfigInfo(ChildDeviceConfigInfo childDeviceConfigInfo) {
        this.childDeviceConfigInfo = childDeviceConfigInfo;
    }

}
