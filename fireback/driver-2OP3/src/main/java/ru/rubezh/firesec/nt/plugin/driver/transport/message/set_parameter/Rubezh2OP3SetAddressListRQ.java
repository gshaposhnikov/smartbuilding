package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import java.util.List;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetAddressListRQ extends Rubezh2OP3SetParameterRQ {

    private List<Integer> addressList;

    public Rubezh2OP3SetAddressListRQ(List<Integer> addressList) {
        super(Rubezh2OP3ParameterType.ADDRESS_LIST);
        this.addressList = addressList;
    }

    public List<Integer> getAddressList() {
        return addressList;
    }

}
