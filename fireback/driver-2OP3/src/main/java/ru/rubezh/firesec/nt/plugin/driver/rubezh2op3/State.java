package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.EnumMap;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.*;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.UpdateFWStates.*;

public enum State {
    NONE(null),
    IDLE(new Idle()),
    FIRST_TIME_CONNECTING(new FirstTimeConnecting()),
    CONNECTING(new Connecting()),
    MODE_CHECKING(new ModeChecking()),
    /* Получение информации о приборе после успешного подключения к нему */
    GETTING_RUBEZH2OP3_INFO(new GettingRubezh2OP3Info()),
    WAIT_RECONNECT(new WaitReconnect()),
    WAITING_TASK(new WaitingTask()),
    WAITING_CONTROL_DEVICE_STATES_AND_EVENT_COUNTERS(new WaitingControlDeviceStatesAndEventCounters()),
    WAITING_DEVICE_EVENTS(new WaitingDeviceEvents()),
    WAITING_REGION_STATES(new WaitingRegionStates()),
    WAITING_CHILD_DEVICE_STATES(new WaitingChildDeviceStates()),
    WAITING_CHILD_DEVICE_CONFIG(new WaitingChildDeviceConfig()),
    WAITING_RUBEZH2OP3_CONFIG(new WaitingRubezh2op3Config()),
    SETTING_CHILD_DEVICE_CONFIG(new SettingChildDeviceConfig()),
    WAITING_RESET_STATE_DEVICE(new WaitingResetDeviceState()),
    /* Запись БД */
    INVOKING_ARM_UPDATE_MODE(new InvokingArmUpdateMode()),
    INVOKING_LOADER_UPDATE_MODE(new InvokingLoaderUpdateMode()),
    ERASING_SECTORS(new ErasingSectors()),
    WRITING_DATABASE(new WritingDatabase()),
    SETTING_ADDRESS_LIST(new SettingAddressList()),
    RESETTING(new Resetting()),

    /* Обновление ПО */
    UPDATE_FW_STATE5(new State5()), // запуск процесса прошивки

    UPDATE_FW_STATE10(new State10()), // вход в АРМ-режим
    UPDATE_FW_STATE11(new State11()), // ТО после входа в АРМ
    UPDATE_FW_STATE12(new State12()), // проверка готовности устройства
    UPDATE_FW_STATE13(new State13()), // проверка ошибки при обновлении ПО
    UPDATE_FW_STATE14(new State14()), // проверка статуса обновления ПО (режима ОС)
    UPDATE_FW_STATE15(new State15()), // вход в LOADER-режим
    UPDATE_FW_STATE16(new State16()), // ТО после входа в LOADER
    UPDATE_FW_STATE17(new State17()), // проверка готовности устройства
    UPDATE_FW_STATE18(new State18()), // проверка ошибки при обновлении ПО
    UPDATE_FW_STATE19(new State19()), // проверка статуса обновления ПО (режима ОС)

    UPDATE_FW_STATE20(new State20()), // запуск стирания секторов
    UPDATE_FW_STATE21(new State21()), // ТО после стирвания секторов
    UPDATE_FW_STATE22(new State22()), // проверка готовности устройства
    UPDATE_FW_STATE23(new State23()), // проверка ошибки при обновлении ПО

    UPDATE_FW_STATE30(new State30()), // команда на запись данных
    UPDATE_FW_STATE31(new State31()), // ТО на запись данных
    UPDATE_FW_STATE32(new State32()), // устновка новой версии прибора

    UPDATE_FW_STATE40(new State40()), // ТО перед перезапуском прибора
    UPDATE_FW_STATE41(new State41()), // команда на перезапуск прибора
    UPDATE_FW_STATE42(new State42()), // ТО на перезапуск прибора
    
    /* Установка статуса зоны */
    WAITING_SET_SECURITY_REGION_STATUS(new SetSecurityRegionStatus()),
    /* Отключение устройства */
    WAITING_SET_DEVICE_POLLING_STATE(new SetDevicePollingState()),
    /* Установка времени */
    WAITING_SET_RUBEZH2OP3_TIME(new WaitingSetRubezh2op3Time()),
    /* Установка пароля */
    WAITING_SET_RUBEZH2OP3_USER_PASSWORD(new WaitingSetRubezh2op3UserPassword()),
    /* Перезагрузка прибора по команде пользователя */
    RESETTING_BY_USER(new ResettingByUser()),
    /* Чтение событий по команде пользователя */
    WAITING_DEVICE_EVENTS_BY_USER(new WaitingDeviceEventsByUser()),
    /* Выполнение действия устройства */
    PERFORMING_DEVICE_ACTION(new PerformingDeviceAction()),
    /* Выполнение действия над сценарием */
    PERFORMING_SCENARIO_ACTION(new PerformingScenarioAction()),
    /* Восстановление конфигурации из базы данных прибора */
    DATABASE_CONFIG_RECOVERY(new DatabaseConfigRecovery()),

    SETTING_ACCESS_KEY_WAITING_STATUS(new SettingAccessKeyWaitingStatus());

    private final StateHandler handler;
    private State(StateHandler handler) {
        this.handler = handler;
    }

    private static final EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);
    static {
        for (State state : State.values()) {
            handlersByState.put(state, state.handler);
        }
    }

    public static StateHandler getHandler(State state) {
        return handlersByState.get(state);
    }
}
