package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;
import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceConfigInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceStatesInfo;

public interface StateHandler {
    public State getCurrentState();
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext);
    public boolean isConnected();

    public State onConnectRequest(long currentTimeMs);
    public State onDisconnectRequest(long currentTimeMs);
    public State onConnected(long currentTimeMs);
    public State onConnectFailure(long currentTimeMs);
    public State onDisconnected(long currentTimeMs);
    public State onReceiveTimeout(long currentTimeMs);
    public State onTimer(long currentTimeMs, String timerName);
    public State onIssueCloseTimer(long currentTimeMs, String issueId);
    public State onControlDeviceStatesAndEventCountersReceived(long currentTimeMs, List<String> deviceStates,
            Map<String, Long> eventCounters);
    public State onLogEventReceived(long currentTimeMs, ControlDeviceEvent logEvent, BitSet regionStates, String regionId);
    public State onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceConfiguration);
    public State onDeviceMaxEventCountersReceived(long currentTimeMs, Map<String, Long> maxEventCounters);
    public State onDeviceMaxEventCountersReceiveFailed(long currentTimeMs);
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception);
    public State onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates);
    public State onChildDeviceStatesReceived(long currentTimeMs, ChildDeviceStatesInfo childDeviceStatesInfo);
    public State onChildDeviceConfigReceived(long currentTimeMs, ChildDeviceConfigInfo childDeviceConfigInfo);
    public State onChildDeviceConfigSet(long currentTimeMs);
    public State onResetStateDeviceReceived(long currentTimeMs);
    public State onSoftwareUpdateModeInvoked(long currentTimeMs, int waitingTimeMs);
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady);
    public State onResetInvoked(long currentTimeMs, int waitingTimeMs);
    public State onEraseSectorsInvoked(long currentTimeMs, int waitingTimeMs);
    public State onWriteMemoryInvoked(long currentTimeMs, int waitingTimeMs);
    public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error);
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status);
    public State onSetSecurityRegionStatusReceived(long currentTimeMs);
    public State onFirmwareVersionReceived(long currentTimeMs, String buildNumber);
    public State onDatabaseVersionReceived(long currentTimeMs, String databaseVersion);
    public State onSetDevicePollingStateReceived(long currentTimeMs);
    public State onDeviceTimeSet(long currentTimeMs);
    public State onDeviceUserPasswordSet(long currentTimeMs);
    public State onFirmwareVersionSet(long currentTimeMs);
    public State onDeviceActionPerformed(long currentTimeMs);
    public State onScenarioActionPerformed(long currentTimeMs);
    public State onDataBlockReceived(long currentTimeMs, byte[] data);
    public State onWaitingAccessKeyStatusSet(long currentTimeMs);
    public State onAddressListReceived(long currentTimeMs, List<Integer> addressList);
    public State onAddressListSent(long currentTimeMs, int waitingTimeMs);
}
