package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.function.Function;

import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter.*;

public class LocalUSBDestinationDecorator implements Rubezh2OP3MessageCodec {

    private Logger logger;
    private Rubezh2OP3MessageCodec rubezh2op3MessageCodec;

    public LocalUSBDestinationDecorator(Logger logger, Rubezh2OP3MessageCodec rubezh2op3MessageCodec) {
        this.logger = logger;
        this.rubezh2op3MessageCodec = rubezh2op3MessageCodec;
    }

    private ByteBuffer prependLocalDeviceDestination(ByteBuffer primaryEncodedMessage) {
        primaryEncodedMessage.flip();
        ByteBuffer encodedMessage = ByteBuffer.allocateDirect(primaryEncodedMessage.remaining() + 1);
        encodedMessage.put((byte) (DestinationType.toInteger(DestinationType.LOCAL_DEVICE) & 0xFF));
        encodedMessage.put(primaryEncodedMessage);
        return encodedMessage;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetEventCountersAndStatesRQ getEventCountersAndStateBytesRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getEventCountersAndStateBytesRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetMaxEventCountersRQ getMaxEventCountersRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getMaxEventCountersRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetControlDeviceInfoRQ getDeviceSerialRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getDeviceSerialRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDataBlockRQ getDataBlockRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getDataBlockRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetLogEventRQ getLogEventRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getLogEventRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetRegionStatesRQ getRegionStatesRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getRegionStatesRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceStatesRQ getChildDeviceStatesRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getChildDeviceStatesRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceConfigRQ getChildDeviceConfigRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getChildDeviceConfigRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetChildDeviceConfigRQ setChildDeviceConfigRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setChildDeviceConfigRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetStateDeviceResetRQ setFireStateDeviceResetRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setFireStateDeviceResetRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3InvokeUpdateSoftwareModeRQ setUpdateSoftwareModeRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setUpdateSoftwareModeRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3CheckDeviceReadyRQ checkDeviceReadyRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(checkDeviceReadyRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3ResetRQ resetRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(resetRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3EraseSectorsRQ eraseSectorsRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(eraseSectorsRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3WriteMemoryRQ writeMemoryRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(writeMemoryRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareErrorRQ getUpdateSoftwareErrorRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getUpdateSoftwareErrorRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareStatusRQ getUpdateStatusRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getUpdateStatusRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetFirmwareVersionRQ getBuildNumberRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getBuildNumberRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDatabaseVersionRQ getDatabaseVersionRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getDatabaseVersionRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetSecurityRegionStatusRQ setSecurityRegionStatusRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setSecurityRegionStatusRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetDevicePollingStateRQ setDevicePollingStateRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setDevicePollingStateRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetCurrentTimeRQ setCurrentTimeRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setCurrentTimeRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetUserPasswordRQ setUserPasswordRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setUserPasswordRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetFirmwareVersionRQ setFirmwareVersionRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setFirmwareVersionRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformDeviceActionRQ performDeviceActionRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(performDeviceActionRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformScenarioActionRQ performScenarioActionRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(performScenarioActionRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetWaitingAccessKeyStatusRQ waitingAccessKeyStatusRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(waitingAccessKeyStatusRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetAddressListRQ getAddressListRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(getAddressListRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetAddressListRQ setAddressListRQ) {
        ByteBuffer primaryEncodedMessage = rubezh2op3MessageCodec.encode(setAddressListRQ);
        return prependLocalDeviceDestination(primaryEncodedMessage);
    }

    private void extractAndCheckDestinationType(ByteBuffer byteBuffer) {
        DestinationType destinationType = DestinationType.fromInteger(byteBuffer.get());
        if (destinationType != DestinationType.LOCAL_DEVICE) {
            ByteBuffer bb = byteBuffer.duplicate();
            bb.flip();
            byte[] b = new byte[bb.remaining()];
            bb.get(b);
            logger.warn("Wrong destination type in message: {}", Hex.encodeHexString(b));
        }
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetEventCountersAndStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetEventCountersAndStatesRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetMaxEventCountersRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetMaxEventCountersRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetControlDeviceInfoRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetControlDeviceInfoRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDataBlockRS(ByteBuffer byteBuffer, int length) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetDataBlockRS(byteBuffer, length);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetLogEventRS(ByteBuffer byteBuffer,
            Function<Integer, String> getRegionIdByLocalNo) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetLogEventRS(byteBuffer, getRegionIdByLocalNo);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetRegionStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetRegionStatesRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceStatesRS(ByteBuffer byteBuffer, BitSet regionStateBits)
            throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetChildDeviceStatesRS(byteBuffer, regionStateBits);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetChildDeviceConfigRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetChildDeviceConfigRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetResetStateDeviceRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetResetStateDeviceRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeInvokeUpdateSoftwareModeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeInvokeUpdateSoftwareModeRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeCheckDeviceReadyRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeCheckDeviceReadyRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeResetRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeResetRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeEraseSectorsRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeEraseSectorsRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeWriteMemoryRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeWriteMemoryRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareErrorRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetUpdateSoftwareErrorRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetUpdateSoftwareStatusRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetFirmwareVersionRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDatabaseVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetDatabaseVersionRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetSecurityRegionStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetSecurityRegionStatusRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDevicePollingStateRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetDevicePollingStateRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetCurrentTimeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetCurrentTimeRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDeviceUserPasswordRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetDeviceUserPasswordRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetFirmwareVersionRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformDeviceActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodePerformDeviceActionRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformScenarioActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodePerformScenarioActionRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetWaitingAccessKeyStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetWaitingAccessKeyStatusRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeGetAddressListRS(byteBuffer);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        extractAndCheckDestinationType(byteBuffer);
        return rubezh2op3MessageCodec.decodeSetAddressListRS(byteBuffer);
    }

}
