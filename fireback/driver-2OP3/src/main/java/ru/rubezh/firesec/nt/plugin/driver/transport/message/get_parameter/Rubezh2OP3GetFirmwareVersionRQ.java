package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetFirmwareVersionRQ extends Rubezh2OP3GetParameterRQ {

    public Rubezh2OP3GetFirmwareVersionRQ() {
        super(Rubezh2OP3ParameterType.FIRMWARE_VERSION);
    }

}
