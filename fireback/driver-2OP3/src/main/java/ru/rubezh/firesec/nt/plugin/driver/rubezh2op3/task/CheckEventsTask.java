package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class CheckEventsTask extends Task {

    private int eventsCounter = 0;

    public CheckEventsTask() {
        super(PRIORITY_LOW);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getControlDeviceEventCountersAndStates(currentTimeMs);
        return State.WAITING_CONTROL_DEVICE_STATES_AND_EVENT_COUNTERS;
    }

    public void eventHasBeenRead() {
        eventsCounter++;
    }

    public boolean shouldReadAnotherEvent() {
        return eventsCounter < stateMachineContext.getBatchEventsCount();
    }

}
