package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw;

import java.nio.ByteBuffer;
import java.util.EnumMap;
import java.util.HashMap;

public class HexFile {

    public enum MemoryType {
        LOADER_RAM485(0x01),
        LOADER_FLASH(0x02),
        APP(0x03),
        AVR(0x04),
        LOADER_RAMUSB(0x05);

        private final int value;

        private MemoryType(int value) {
            this.value = value;
        }

        private static final EnumMap<MemoryType, Integer> valuesByType = new EnumMap<>(MemoryType.class);
        private static final HashMap<Integer, MemoryType> typesByValue = new HashMap<>();
        static {
            for (MemoryType type : MemoryType.values()) {
                valuesByType.put(type, type.value);
                typesByValue.put(type.value, type);
            }
        }

        public static int getValue(MemoryType type) {
            return valuesByType.get(type);
        }

        public static MemoryType getType(int value) {
            return typesByValue.get(value);
        }

        public static MemoryType getType(String value) {
            return typesByValue.get(Integer.valueOf(value));
        }
    }
    
    private String fileName;

    private MemoryType memoryType;

    private Integer lowAddress;

    private Integer highAddress;

    private Integer version;

    private Integer crc;

    private ByteBuffer continuousData;

    private boolean dataValid = true;

    private String failureMessage;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public MemoryType getMemoryType() {
        return memoryType;
    }

    public void setMemoryType(MemoryType memoryType) {
        this.memoryType = memoryType;
    }

    public Integer getLowAddress() {
        return lowAddress;
    }

    public void setLowAddress(Integer lowAddress) {
        this.lowAddress = lowAddress;
    }

    public Integer getHighAddress() {
        return highAddress;
    }

    public void setHighAddress(Integer highAddress) {
        this.highAddress = highAddress;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getCrc() {
        return crc;
    }

    public void setCrc(Integer crc) {
        this.crc = crc;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public boolean areAttributesValid() {
        return fileName != null && memoryType != null && lowAddress != null && highAddress != null && version != null
                && crc != null && lowAddress >= 0 && highAddress >= lowAddress;
    }

    public boolean isDataValid() {
        return dataValid;
    }

    public void setDataValid(boolean dataValid) {
        this.dataValid = dataValid;
    }

    public ByteBuffer getContinuousData() {
        return continuousData;
    }

    public void setContinuousData(ByteBuffer continuousData) {
        this.continuousData = continuousData;
    }

    @Override
    public String toString() {
        return "fileName: " + fileName + ", " +
                "memoryType: " + memoryType + ", " +
                "lowAddress: " + String.format("0x%06X", lowAddress) + ", " +
                "highAddress: " + String.format("0x%06X", highAddress) + ", " +
                "version: " + String.format("0x%04X", version) + ", " +
                "crc: " + String.format("0x%04X", crc) + ", " +
                "continuousData.limit: " + continuousData.limit() + ", " +
                "dataValid: " + dataValid + ", " +
                "failureMessage: " + failureMessage;
    }

}
