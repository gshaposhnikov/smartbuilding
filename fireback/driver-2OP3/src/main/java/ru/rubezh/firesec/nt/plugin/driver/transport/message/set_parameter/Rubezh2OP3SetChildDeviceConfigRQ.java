package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import java.util.Map;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetChildDeviceConfigRQ extends Rubezh2OP3SetParameterRQ {

    private int type;
    private int lineNo;
    private int address;
    private Map<String, String> propertyValues;

    public Rubezh2OP3SetChildDeviceConfigRQ(int type, int lineNo, int address, Map<String, String> propertyValues) {
        super(Rubezh2OP3ParameterType.CHILD_DEVICE_CONFIG);
        this.type = type;
        this.lineNo = lineNo;
        this.address = address;
        this.propertyValues = propertyValues;
    }

    public int getType() {
        return type;
    }

    public int getLineNo() {
        return lineNo;
    }

    public int getAddress() {
        return address;
    }

    public Map<String, String> getPropertyValues() {
        return propertyValues;
    }

}
