package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.domain.v1.ManualResetStateGroup;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetStateDeviceResetRQ extends Rubezh2OP3SetParameterRQ {

    private ManualResetStateGroup targetResetGroup;

    public Rubezh2OP3SetStateDeviceResetRQ(ManualResetStateGroup targetResetGroup) {
        super(Rubezh2OP3ParameterType.RESET_STATE);
        this.targetResetGroup = targetResetGroup;
    }

    public ManualResetStateGroup getTargetResetGroup() {
        return targetResetGroup;
    }

    public void setTargetResetGroup(ManualResetStateGroup targetResetGroup) {
        this.targetResetGroup = targetResetGroup;
    }
}
