package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.ActiveRegion;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

/**
 * Базовый класс для всех состояний, которые происходят во время отработки задач.
 * Добавляет периодические задачи в очередь заданий по таймерам.
 * В случае, когда наследник этого класса переопределяет {@link #onTimer(long, String)}, он должен вызывать этот метод у данного класса.
 *
 * @author Артем Седанов
 */
public abstract class AbstractTaskState extends AbstractConnected {
    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        switch (timerName) {
            case TimerNames.CHECK_EVENTS:
                stateMachineContext.getTaskManager().enqueueCheckEvents();
                break;
            case TimerNames.CHECK_REGION_STATES:
                ActiveRegion regionToCheck = stateMachineContext.getNextActiveRegionToCheckStates();
                if (regionToCheck != null) {
                    switch (regionToCheck.getRegionProject().getSubsystem()) {
                        case FIRE:
                        case SECURITY:
                            stateMachineContext.getTaskManager().enqueueGetRegionStates(regionToCheck);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case TimerNames.CHECK_CHILD_DEVICE_STATES:
                ActiveDevice deviceToCheck = stateMachineContext.getChildDevicePollingState().getNextDeviceToPoll();
                if (deviceToCheck != null) {
                    stateMachineContext.getTaskManager().enqueueGetChildDeviceStates(deviceToCheck);
                    /* TODO: реализовать в TaskManager-е настраиваемое расписание
                     *    для запуска последовательностей фоновых задач */
                    if (stateMachineContext.getChildDevicePollingState().isPollCompleted())
                        stateMachineContext.getTaskManager().enqueueSetAddressList();
                }
                break;
            case TimerNames.SET_TIME:
                stateMachineContext.getTaskManager().enqueueSetRubezh2op3Time();
                break;
            case TimerNames.CHECK_MODE:
                stateMachineContext.getTaskManager().enqueueCheckMode();
                break;
        }
        return getCurrentState();
    }
    @Override
    public State onIssueCloseTimer(long currentTimeMs, String issueId) {
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.FINISHED, "");
        return getCurrentState();
    }
}
