package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.List;
import java.util.Map;

/**
 * Класс для передачи информации о состояниях устройства из драйвера
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ChildDeviceStatesInfo {
    /** Номер линии устойства */
    private int lineNo;

    /** Адрес устройства на линии */
    private int address;

    /** Идентификатор профиля устройства */
    private String deviceProfileId;

    /** Список активных состояний на устройства в соответствии с профилем устройства */
    private List<String> activeStates;

    /** Список текущих значений наблюдаемых параметров */
    private Map<String, String> activeValues;

    /** Качество соединения */
    private int qos;

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public List<String> getActiveStates() {
        return activeStates;
    }

    public void setActiveStates(List<String> activeStates) {
        this.activeStates = activeStates;
    }

    public Map<String, String> getActiveValues() {
        return activeValues;
    }

    public void setActiveValues(Map<String, String> activeValues) {
        this.activeValues = activeValues;
    }

    public int getQos() {
        return qos;
    }

    public void setQos(int qos) {
        this.qos = qos;
    }
}
