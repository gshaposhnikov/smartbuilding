package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3CheckDeviceReadyRS extends Rubezh2OP3CommonRS {
    boolean isReady;

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean isReady) {
        this.isReady = isReady;
    }

    public Rubezh2OP3CheckDeviceReadyRS() {
        super(Rubezh2OP3FunctionType.CHECK_READY);
    }

}
