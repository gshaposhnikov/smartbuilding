package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile.MemoryType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.TimerNames;

public class UpdateFWTask extends Task {

    private final static int ARM_UPDATE_PROGRESS = 5;
    private final static int ERASING_SECTORS_PROGRESS = 10;
    private final static int RESETTING_PROGRESS = 95;

    private HexFile hexFile;
    private int version;

    public UpdateFWTask(String issueId, HexFile hexFile, int version) {
        super(PRIORITY_HIGH);
        if (hexFile.getMemoryType() == MemoryType.LOADER_FLASH || hexFile.getMemoryType() == MemoryType.LOADER_RAM485
                || hexFile.getMemoryType() == MemoryType.LOADER_RAMUSB)
            priority = PRIORITY_VERY_HIGH;

        this.issueId = issueId;
        this.hexFile = hexFile;
        this.version = version;
    }

    @Override
    public boolean canBeExecuted() {
        assert stateMachineContext.getRubezh2op3DeviceMode() != null : "Не считан режим прибора";
        switch (stateMachineContext.getRubezh2op3DeviceMode()) {
        case NORMAL_OPERATION:
        case UPDATE_ARM:
        case UPDATE_LOADER:
            return true;
        default:
            return false;
        }
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        stateMachineContext.resetUpdatingSoftwareState();
        stateMachineContext.getUpdatingSoftwareState().setHexFile(hexFile);
        stateMachineContext.getUpdatingSoftwareState().setVersion(version);

        stateMachineContext.getLogger().info("Version: {}, HexFile: {}", version, hexFile);

        if (Driver.tryAcquireExclusiveMode(currentTimeMs)) {
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId,
                    IssueStatus.IN_PROGRESS, "");
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER, 0, false);
            return State.UPDATE_FW_STATE5;
        } else {
            stateMachineContext.getLogger().error("Не удалось захватить мютекс");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    public static int getArmUpdateProgress() {
        return ARM_UPDATE_PROGRESS;
    }

    public static int getErasingSectorsProgress() {
        return ERASING_SECTORS_PROGRESS;
    }

    public static int getResettingProgress() {
        return RESETTING_PROGRESS;
    }

}
