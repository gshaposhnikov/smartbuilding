package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import ru.rubezh.firesec.nt.domain.v1.DeviceProfile.LogTypeItem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Структура, описывающая состояние чтения событий с прибора
 */
public class Rubezh2OP3ReadEventsState {
    // Максимальное значение индекса события (4 байта без знака)
    public static final long MAX_EVENT_INDEX = 0xFF_FF_FF_FFL;

    /** Описатели журналов + контексты их чтения */
    private List<LogState> logStates = new ArrayList<>();
    /** Указатель на следующий журнал в списке обхода */
    private Iterator<LogState> logsToRead;
    /** Журнал, из которого осуществляется чтение */
    private LogState currentLog;
    /** Индексы чтения и записи проинициализированы */
    private boolean initialized = false;

    /** Состояние чтения/записи одного журнала */
    private class LogState {
        public RingBufferContext readContext = new RingBufferContext(MAX_EVENT_INDEX + 1L);
        public LogTypeItem logType;

        public LogState(LogTypeItem logType, long lastEventReadIndex, long lastEventWriteIndex) {
            readContext.setIndexes(lastEventReadIndex, lastEventWriteIndex);
            this.logType = logType;
        }

        @Override
        public String toString() {
            return logType.getId() + ": " + readContext;
        }
    }

    /** Описатель события */
    public class EventDescriptor {
        /** Идентификатор журнала */
        public String logId;
        /** Код журнала в приборе */
        public int logCode;
        /** Номер сообщения журнала */
        public long index;

        public EventDescriptor(LogState logState) {
            assert logState != null;
            logId = logState.logType.getId();
            logCode = logState.logType.getCode();
            index = logState.readContext.getReadIndex();
        }
    }

    public EventDescriptor getCurrentEvent() {
        return currentLog == null ? null : new EventDescriptor(currentLog);
    }

    public EventDescriptor getNextEvent() {
        if (currentLog != null) {
            // отмечаем текущее сообщение как прочитанное
            currentLog.readContext.getNextReadIndex();
            // поиск следующего в других журналах
            searchFirstUnread();
            if (currentLog == null)
                resetReadIterator();
        }
        return getCurrentEvent();
    }

    public Rubezh2OP3ReadEventsState(List<LogTypeItem> logTypes) {
        logStates.clear();
        for (LogTypeItem item : logTypes)
            logStates.add(new LogState(item, 0l, 0l));
    }

    public void prepareReadingAllUnread(Map<String, Long> readIndexesByType, Map<String, Long> writeIndexesByType,
            Map<String, Long> maxReadEventsByType) {
        // подготовка индексов для чтения всех непрочитанных
        for (LogState logState : logStates) {
            long writeIndex = writeIndexesByType.getOrDefault(logState.logType.getId(), 0l);
            long readIndex = readIndexesByType.getOrDefault(logState.logType.getId(), writeIndex);
            // индекс следущего равен индексу считанного + 1
            // полагаем, что индекс записи < индекса чтения означает "битый" индекс чтения
            logState.readContext.setIndexes(Long.min(readIndex + 1l, writeIndex), writeIndex);
            // индекс чтения ограничивается снизу максимальным числом событий в журнале
            Long maxReadEvents = maxReadEventsByType.getOrDefault(logState.logType.getId(), 10l);
            logState.readContext.fixMaxReadItems(maxReadEvents);
        }
        resetReadIterator();
        initialized = true;
    }

    public void prepareReadingOneFromEach(Map<String, Long> readIndexesByType, Map<String, Long> writeIndexesByType,
            Map<String, Long> maxReadEventsByType) {
        // подготовка индексов для чтения одного последнего в каждом журнале
        for (LogState logState : logStates) {
            long writeIndex = writeIndexesByType.getOrDefault(logState.logType.getId(), 0l);
            long readIndex = readIndexesByType.getOrDefault(logState.logType.getId(), writeIndex);
            // индекс предыдущего равен индексу следующего - 1
            // полагаем, что индекс записи 0 означает пустой журнал
            logState.readContext.setIndexes(Long.max(writeIndex - 1l, 0l), writeIndex);
        }
        resetReadIterator();
        initialized = true;
    }

    public void prepareFullRead(Map<String, Long> writeIndexesByType, Map<String, Long> maxReadEventsByType,
            List<String> requiredLogIds) {
        // подготовка индексов для чтения указанных журналов
        for (LogState logState : logStates) {
            Long writeIndex = requiredLogIds.contains(logState.logType.getId()) ?
                    writeIndexesByType.getOrDefault(logState.logType.getId(), 0l) : 0l;
            logState.readContext.setIndexes(0l, writeIndex);
            // индекс чтения ограничивается снизу максимальным числом событий в журнале
            Long maxReadEvents = maxReadEventsByType.getOrDefault(logState.logType.getId(), 10l);
            logState.readContext.fixMaxReadItems(maxReadEvents);
        }
        resetReadIterator();
        initialized = true;
    }

    public void shiftWriteIndexes(Map<String, Long> writeIndexesByType, Map<String, Long> maxReadEventsByType) {
        // сдвиг индексов записи вперёд
        for (LogState logState : logStates) {
            Long writeIndex = writeIndexesByType.get(logState.logType.getId());
            if (writeIndex == null)
                continue;
            logState.readContext.shiftWriteIndex(writeIndex);
            // индекс чтения ограничивается снизу максимальным числом событий в журнале
            Long maxReadEvents = maxReadEventsByType.getOrDefault(logState.logType.getId(), 10l);
            logState.readContext.fixMaxReadItems(maxReadEvents);
        }
        resetReadIterator();
    }

    public long countEventsToRead() {
        long amount = 0l;
        for (LogState logState : logStates)
            amount += logState.readContext.itemsToRead();
        return amount;
    }

    public boolean isInitialized() {
        return initialized;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (LogState logState : logStates)
            result.append(logState).append("; ");
        return result.toString();
    }

    private void resetReadIterator() {
        // инициализация контекста обхода журналов
        logsToRead = logStates.iterator();
        searchFirstUnread();
    }

    private void searchFirstUnread() {
        // поиск первого непрочитанного начиная со следующего журнала
        do currentLog = logsToRead.hasNext() ? logsToRead.next() : null;
        while (currentLog != null && currentLog.readContext.getReadIndex() == null);
    }
}
