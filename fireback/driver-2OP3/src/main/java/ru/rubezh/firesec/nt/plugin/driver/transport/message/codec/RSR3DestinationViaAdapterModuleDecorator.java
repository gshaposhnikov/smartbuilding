package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.function.Function;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter.*;

public class RSR3DestinationViaAdapterModuleDecorator implements Rubezh2OP3MessageCodec {

    @SuppressWarnings("unused")
    private Logger logger;
    private MessageCodec amodMessageCodec;
    private Rubezh2OP3MessageCodec rubezh2op3MessageCodec;
    DestinationType destinationType;
    int rs485Address;

    public RSR3DestinationViaAdapterModuleDecorator(Logger logger, MessageCodec amodMessageCodec,
            Rubezh2OP3MessageCodec rubezh2op3MessageCodec, DestinationType destinationType, int rs485Address) {
        this.amodMessageCodec = amodMessageCodec;
        this.rubezh2op3MessageCodec = rubezh2op3MessageCodec;
        this.destinationType = destinationType;
        this.rs485Address = rs485Address;
    }

    private ByteBuffer encodeWithProxy(ByteBuffer message) {
        message.flip();
        RS485ProxyRQ proxyRq = new RS485ProxyRQ(destinationType, rs485Address);
        proxyRq.setFunctionContextData(message);
        return amodMessageCodec.encodeForLine(proxyRq);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetEventCountersAndStatesRQ getEventCountersAndStateBytesRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getEventCountersAndStateBytesRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetMaxEventCountersRQ getMaxEventCountersRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getMaxEventCountersRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetControlDeviceInfoRQ getDeviceConfigurationRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getDeviceConfigurationRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDataBlockRQ getDataBlockRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getDataBlockRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetLogEventRQ getLogEventRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getLogEventRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetRegionStatesRQ getRegionStatesRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getRegionStatesRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceStatesRQ getChildDeviceStatesRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getChildDeviceStatesRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceConfigRQ getChildDeviceConfigRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getChildDeviceConfigRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetChildDeviceConfigRQ setChildDeviceConfigRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setChildDeviceConfigRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetStateDeviceResetRQ setStateDeviceResetRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setStateDeviceResetRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3InvokeUpdateSoftwareModeRQ setUpdateSoftwareModeRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setUpdateSoftwareModeRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3CheckDeviceReadyRQ checkDeviceReadyRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(checkDeviceReadyRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3ResetRQ resetRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(resetRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3EraseSectorsRQ eraseSectorsRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(eraseSectorsRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3WriteMemoryRQ writeMemoryRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(writeMemoryRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareErrorRQ getUpdateSoftwareErrorRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getUpdateSoftwareErrorRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareStatusRQ getUpdateSoftwareStatusRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getUpdateSoftwareStatusRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetFirmwareVersionRQ getBuildNumberRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getBuildNumberRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDatabaseVersionRQ getBuildNumberRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getBuildNumberRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetSecurityRegionStatusRQ setSecurityRegionStatusRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setSecurityRegionStatusRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetDevicePollingStateRQ setDevicePollingStateRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setDevicePollingStateRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetCurrentTimeRQ setCurrentTimeRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setCurrentTimeRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetUserPasswordRQ setUserPasswordRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setUserPasswordRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetFirmwareVersionRQ setFirmwareVersionRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setFirmwareVersionRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformDeviceActionRQ performDeviceActionRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(performDeviceActionRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformScenarioActionRQ performScenarioActionRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(performScenarioActionRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetWaitingAccessKeyStatusRQ waitingAccessKeyStatusRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(waitingAccessKeyStatusRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetAddressListRQ getAddressListRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(getAddressListRQ));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetAddressListRQ setAddressListRQ) {
        return encodeWithProxy(rubezh2op3MessageCodec.encode(setAddressListRQ));
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetEventCountersAndStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetEventCountersAndStatesRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetMaxEventCountersRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetMaxEventCountersRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetControlDeviceInfoRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetControlDeviceInfoRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDataBlockRS(ByteBuffer byteBuffer, int length) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetDataBlockRS(proxyRS.getFunctionContextData(), length);
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetLogEventRS(ByteBuffer byteBuffer,
            Function<Integer, String> getRegionIdByLocalNo) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetLogEventRS(proxyRS.getFunctionContextData(),
                    getRegionIdByLocalNo);
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetRegionStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetRegionStatesRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceStatesRS(ByteBuffer byteBuffer, BitSet regionStateBits) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetChildDeviceStatesRS(proxyRS.getFunctionContextData(), regionStateBits);
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetChildDeviceConfigRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetChildDeviceConfigRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetResetStateDeviceRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetResetStateDeviceRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeInvokeUpdateSoftwareModeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeInvokeUpdateSoftwareModeRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeCheckDeviceReadyRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeCheckDeviceReadyRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeResetRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeResetRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeEraseSectorsRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeEraseSectorsRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeWriteMemoryRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeWriteMemoryRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareErrorRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetUpdateSoftwareErrorRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetUpdateSoftwareStatusRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetFirmwareVersionRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDatabaseVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetDatabaseVersionRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetSecurityRegionStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetSecurityRegionStatusRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDevicePollingStateRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetDevicePollingStateRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetCurrentTimeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetCurrentTimeRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDeviceUserPasswordRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetDeviceUserPasswordRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetFirmwareVersionRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformDeviceActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodePerformDeviceActionRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformScenarioActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodePerformScenarioActionRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetWaitingAccessKeyStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetWaitingAccessKeyStatusRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeGetAddressListRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;
        RS485ProxyRS proxyRS = amodMessageCodec.decodeRS485ProxyRS(byteBuffer);
        if (proxyRS.getException() != null) {
            response = new Rubezh2OP3ExceptionRS(proxyRS.getException());
        } else {
            response = rubezh2op3MessageCodec.decodeSetAddressListRS(proxyRS.getFunctionContextData());
        }
        return response;
    }

}
