package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetFirmwareVersionRQ extends Rubezh2OP3SetParameterRQ {
    private int majorVersion;
    private int minorVersion;

    public Rubezh2OP3SetFirmwareVersionRQ(int hexWordVersion) {
        super(Rubezh2OP3ParameterType.FIRMWARE_VERSION);
        majorVersion = (hexWordVersion >> 8) & 0xFF;
        minorVersion = hexWordVersion & 0xFF;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(int minorVersion) {
        this.minorVersion = minorVersion;
    }

}
