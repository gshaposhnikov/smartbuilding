package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.Map;

/**
 * Класс для передачи информация о конфиге устройства из кодека
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ChildDeviceConfigInfo {
    /** Адрес устройства на линии */
    private int address;

    /** Идентификатор профиля устройства */
    private String deviceProfileId;

    /** Серийный номер устройства */
    private String serial;
    
    /** Версия ПО устройства */
    private String firmwareVersion;

    /** Значения параметров в соответствии с профилем устройства */
    private Map<String, String> propertyValues;

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }
    
    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }
    
    public Map<String, String> getPropertyValues() {
        return propertyValues;
    }

    public void setPropertyValues(Map<String, String> propertyValues) {
        this.propertyValues = propertyValues;
    }
}
