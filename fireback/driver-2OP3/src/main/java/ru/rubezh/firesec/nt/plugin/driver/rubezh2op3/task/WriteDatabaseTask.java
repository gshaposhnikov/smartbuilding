package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DataBlock;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.SectorsRange;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud.SkudDatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.TimerNames;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;

public class WriteDatabaseTask extends Task {

    private final static int ARM_UPDATE_PROGRESS = 10;
    private final static int LOADER_UPDATE_PROGRESS = 20;
    private final static int ERASING_SECTORS_PROGRESS = 30;
    private final static int WRITING_DATABASE_PROGRESS = 90;

    /** Необходимо писать общую БД */
    private boolean writeCommonDB;
    /** Необходимо записать восстановительную информацию */
    private boolean writeRecovery;
    /** Необходимо записать постоянную часть БД СКУД */
    private boolean writeConstantSkud;
    /** Необходимо записать изменяемую часть БД СКУД */
    private boolean writeVariableSkud;

    public WriteDatabaseTask(String issueId, boolean writeCommonDB, boolean writeRecovery, boolean writeConstantSkud,
            boolean writeVariableSkud) {
        super(PRIORITY_HIGH);
        this.issueId = issueId;
        this.writeCommonDB = writeCommonDB;
        this.writeRecovery = writeRecovery;
        this.writeConstantSkud = writeConstantSkud;
        this.writeVariableSkud = writeVariableSkud;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        List<DataBlock> dataBlocks = new ArrayList<>();
        List<SectorsRange> sectorsRanges = new ArrayList<>();
        if (writeCommonDB || writeRecovery) {
            DatabaseBuilder databaseBuilder = stateMachineContext.getDatabaseBuilder();
            dataBlocks.addAll(databaseBuilder.getDbBlocks(writeRecovery));
            sectorsRanges.addAll(databaseBuilder.getSectorsToErase());
        }
        if (writeConstantSkud || writeVariableSkud) {
            SkudDatabaseBuilder skudDatabaseBuilder = stateMachineContext.getSkudDatabaseBuilder();
            sectorsRanges.addAll(skudDatabaseBuilder.getSectorsToErase());
            dataBlocks.addAll(skudDatabaseBuilder.getConstDataBlocks());
            if (writeVariableSkud)
                dataBlocks.addAll(skudDatabaseBuilder.getVarDataBlocks());
        }
        stateMachineContext.getUpdatingDatabaseState().reset(dataBlocks, sectorsRanges);
        if (Driver.tryAcquireExclusiveMode(currentTimeMs)) {
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId,
                    IssueStatus.IN_PROGRESS, "");
            return startDatabaseWriting(currentTimeMs);
        } else {
            stateMachineContext.getLogger().error("Не удалось захватить мютекс");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    public boolean canBeExecuted() {
        assert stateMachineContext.getRubezh2op3DeviceMode() != null : "Не считан режим прибора";
        switch (stateMachineContext.getRubezh2op3DeviceMode()) {
        case NORMAL_OPERATION:
        case UPDATE_ARM:
        case UPDATE_LOADER:
            return true;
        default:
            return false;
        }
    }

    private State startDatabaseWriting(long currentTimeMs) {
        switch (stateMachineContext.getRubezh2op3DeviceMode()) {
        default:
        case NORMAL_OPERATION:
            stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                    Rubezh2OP3UpdateSoftwareModeType.ARM);
            return State.INVOKING_ARM_UPDATE_MODE;
        case UPDATE_ARM:
            stateMachineContext.getUpdatingDatabaseState()
                    .setStage(Rubezh2OP3UpdatingDatabaseStage.INVOKING_LOADER_UPDATE_MODE);
            stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                    Rubezh2OP3UpdateSoftwareModeType.LOADER);
            return State.INVOKING_LOADER_UPDATE_MODE;
        case UPDATE_LOADER:
            stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.ERASING_SECTORS);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.START_NEXT_STAGE, 10, false);
            return State.ERASING_SECTORS;
        }
    }

    public static int getArmUpdateProgress() {
        return ARM_UPDATE_PROGRESS;
    }

    public static int getLoaderUpdateProgress() {
        return LOADER_UPDATE_PROGRESS;
    }

    public static int getErasingSectorsProgress() {
        return ERASING_SECTORS_PROGRESS;
    }

    public static int getWritingDatabaseProgress() {
        return WRITING_DATABASE_PROGRESS;
    }

}
