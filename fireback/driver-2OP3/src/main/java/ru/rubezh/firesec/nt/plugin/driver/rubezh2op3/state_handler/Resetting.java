package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class Resetting extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.RESETTING;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        Resetting handler = new Resetting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onResetInvoked(long currentTimeMs, int waitingTimeMs) {
        super.onDisconnected(currentTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RESET_TIMEOUT,
                stateMachineContext.getResetTimeout(), false);
        return getCurrentState();
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        return onResetInvoked(currentTimeMs, 0);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        return onResetInvoked(currentTimeMs, 0);
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.RESET_TIMEOUT)) {
            try {
                WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
                if (stateMachineContext.getUpdatingDatabaseState()
                        .getStage() == Rubezh2OP3UpdatingDatabaseStage.ABORTING_ON_FAILURE) {
                    stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                            IssueStatus.FAILED, "Потеря связи");
                } else {
                    stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                            IssueStatus.FINISHED, "");
                }
            } catch (ClassCastException e) {
                stateMachineContext.getLogger().warn("Error casting Task");
            }
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RECONNECT,
                    stateMachineContext.getReconnectInterval(), false);
            return State.WAIT_RECONNECT;
        }
        return getCurrentState();
    }
}
