package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DataBlock;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;

public class WritingDatabase extends AbstractWritingDatabase {

    @Override
    public State getCurrentState() {
        return State.WRITING_DATABASE;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WritingDatabase handler = new WritingDatabase();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    private boolean writeBlock(long currentTimeMs, DataBlock dataBlock) {
        if (dataBlock == null)
            return false;

        stateMachineContext.getLogger().trace("address = {}, length = {}, progress = {}%",
                dataBlock.getAddress(), dataBlock.getData().length,
                (int)(stateMachineContext.getUpdatingDatabaseState().getDataProgress() * 100));

        stateMachineContext.getActionExecutor().writeMemory(currentTimeMs,
                dataBlock.getAddress(), dataBlock.getData());

        return true;
    }

    @Override
    public State onWriteMemoryInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK, waitingTimeMs,
                false);
        return getCurrentState();
    }

    private void updateProgress(long currentTimeMs) {
        final float erasingProgress = WriteDatabaseTask.getErasingSectorsProgress();
        final float writingProgress = WriteDatabaseTask.getWritingDatabaseProgress();
        float globalProgress = erasingProgress + (writingProgress - erasingProgress) *
                stateMachineContext.getUpdatingDatabaseState().getDataProgress();
        try {
            WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs, task.getIssueId(),
                    globalProgress);
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        stateMachineContext.getUpdatingDatabaseState().writeFailed();
        if (stateMachineContext.getUpdatingDatabaseState().shouldRetryWrite()) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.DATABASE_WRITING_TIMER,
                    stateMachineContext.getStartWritingRetryTO(), false);
            return getCurrentState();
        } else {
            return onDisconnected(currentTimeMs);
        }
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.START_NEXT_STAGE) ||
                timerName.equals(TimerNames.LONG_OPERATION_CHECK)) {
            if (timerName.equals(TimerNames.LONG_OPERATION_CHECK))
                updateProgress(currentTimeMs);
            if (!writeBlock(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getNextDataBlock(
                    stateMachineContext.getWriteBlockSize()))) {
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RESET_TIMEOUT,
                        stateMachineContext.getResetTimeout(), false);
            }
        } else if (timerName.equals(TimerNames.DATABASE_WRITING_TIMER)) {
            stateMachineContext.getLogger().warn("Retrying write data");
            writeBlock(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getLastDataBlock());
            return getCurrentState();
        } else if (timerName.equals(TimerNames.RESET_TIMEOUT)) {
            Driver.freeExclusiveMode();
            stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.RESETTING);
            stateMachineContext.getActionExecutor().reset(currentTimeMs);
            stateMachineContext.getChildDevicePollingState().prepareUrgentPollAllDevices();
            return State.RESETTING;
        }
        return getCurrentState();
    }

}
