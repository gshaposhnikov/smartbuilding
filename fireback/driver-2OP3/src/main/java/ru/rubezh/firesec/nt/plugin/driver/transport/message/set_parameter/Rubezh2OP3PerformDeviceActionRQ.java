package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.DeviceAction;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3PerformDeviceActionRQ extends Rubezh2OP3SetParameterRQ {

    private int type;
    private int lineNo;
    private int address;
    private DeviceAction deviceAction;
    private Map<String, String> propertyValues;

    public Rubezh2OP3PerformDeviceActionRQ(int type, int lineNo, int address,
            DeviceAction deviceAction, Map<String, String> propertyValues) {
        super(Rubezh2OP3ParameterType.RESET_STATE);
        this.type = type;
        this.lineNo = lineNo;
        this.address = address;
        this.deviceAction = deviceAction;
        this.propertyValues = propertyValues;
    }

    public int getType() {
        return type;
    }

    public int getLineNo() {
        return lineNo;
    }

    public int getAddress() {
        return address;
    }

    public DeviceAction getDeviceAction() {
        return deviceAction;
    }

    public Map<String, String> getPropertyValues() {
        return propertyValues;
    }

}
