package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.GetChildDeviceConfigTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceConfigInfo;

public class WaitingChildDeviceConfig extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_CHILD_DEVICE_CONFIG;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingChildDeviceConfig handler = new WaitingChildDeviceConfig();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onChildDeviceConfigReceived(long currentTimeMs, ChildDeviceConfigInfo childDeviceConfigInfo) {
        try {
            GetChildDeviceConfigTask task = (GetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigReceived(currentTimeMs,
                    task.getActiveDevice().getId(), childDeviceConfigInfo.getPropertyValues(),
                    childDeviceConfigInfo.getSerial(), childDeviceConfigInfo.getFirmwareVersion());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        try {
            GetChildDeviceConfigTask task = (GetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            GetChildDeviceConfigTask task = (GetChildDeviceConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onChildDeviceConfigReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
