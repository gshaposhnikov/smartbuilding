package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class WaitingTask extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_TASK;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingTask handler = new WaitingTask();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        super.onTimer(currentTimeMs, timerName);
        if (timerName.equals(TimerNames.EXECUTE_TASK)) {
            return stateMachineContext.getTaskManager().executeNextTask(currentTimeMs);
        } else {
            return getCurrentState();
        }
    }
}
