package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils;

import java.nio.charset.Charset;
import java.util.List;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.SectorsRange;

/**
 * Набор функций для работы с массивом байт при формировании БД прибора
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru), Андрей Лисовой
 *
 */
public abstract class DatabaseWriter {

    /** Размер сектора в БД прибора */
    public static final int DB_SECTOR_SIZE = 64 * 1024;

    /** Кодировка текста в приборе */
    private static final Charset CHARSET = Charset.forName("Windows-1251");

    /**
     * Подсчет CRC16 CCITT
     *
     * @param data
     *            массив данных
     * @param from
     *            номер первого байта, для которого нужно считать CRC
     * @param to
     *            номер следующего за последним байта, для которого нужно
     *            считать CRC
     * @return CRC16 CCITT подмассива data[from:to]
     */
    public static int CRC16CCITT(byte[] data, int from, int to) {
        int crc = 0;

        for (int j = from; j < to; j++) {
            byte b = data[j];
            crc = ((crc >>> 8) | (crc << 8)) & 0xffff;
            crc ^= (b & 0xff);
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }
        crc &= 0xffff;
        return crc;
    }

    /**
     * Запись числа в нужное количество байт в порядке big endian
     * 
     * @param number
     *            исходное число
     * @param byteCount
     *            кол-во байт на выходе
     * @return массив байт с записанным числом в порядке следования байт big
     *         endian (младший разряд в последнем байте)
     */
    public static byte[] writeInteger(int number, int byteCount) {
        byte[] buffer = new byte[byteCount];
        for (int i = byteCount - 1; i >= 0; --i, number >>= 8)
            buffer[i] = (byte) (number & 0xFF);
        return buffer;
    }

    /**
     * Чтение числа в нужное количество байт в порядке big endian
     * 
     * @param buffer
     *            массив байт, содержащий число
     * @param offset
     *            индекс первого байта числа в массиве
     * @param size
     *            кол-во байт, составляющих число
     * @return полученное число
     */
    public static int readInteger(byte[] buffer, int offset, int size) {
        int number = 0;
        for (int i = offset; i < (offset + size); i++)
            number = (number << 8) | (buffer[i] & 0xFF);
        return number;
    }

    /**
     * Запись строки в нужное число байт (обрезается до нужного, пустое
     * заполняется пробелами 0x20)
     */
    public static byte[] writeString(String string, int byteCount) {
        byte[] result = new byte[byteCount];
        byte[] stringBytes;
        stringBytes = string.getBytes(CHARSET);
        int len = Integer.min(byteCount, stringBytes.length);
        System.arraycopy(stringBytes, 0, result, 0, len);
        for (int i = stringBytes.length; i < byteCount; i++)
            result[i] = (byte) 0x20;
        return result;
    }

    /**
     * Запись числа в нужное число байт по указанному смещению в массиве байт
     */
    public static void insertInteger(byte[] dest, int offset, int number, int byteCount) {
        assert offset + byteCount <= dest.length;
        byte[] numberRaw = writeInteger(number, byteCount);
        System.arraycopy(numberRaw, 0, dest, offset, byteCount);
    }

    /**
     * Метод пересчитывает CRC для заданного куска данных и подставляет ее в
     * заданное место в том же массиве данных
     */
    public static void recalculateCrc(byte[] data, int crcOffset, int crcSize, int from, int to) {
        int crc = CRC16CCITT(data, from, to);
        insertInteger(data, crcOffset, crc, crcSize);
    }

    public abstract List<SectorsRange> getSectorsToErase();
}
