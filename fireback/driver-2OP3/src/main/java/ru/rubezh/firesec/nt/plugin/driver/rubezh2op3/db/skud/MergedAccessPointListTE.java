package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfileView;
import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;

/**
 * Элемент таблицы списка точек доступа, сформированный по иерархии нескольких
 * сущностей (сотруднику, ключу доступа, ...).
 * 
 * Должен использоваться, когда и ключу доступа, и сотруднику прописаны свои
 * точки доступа.
 * 
 * @author Антон Васильев
 *
 */
public class MergedAccessPointListTE extends AccessPointListTE {

    public MergedAccessPointListTE(String ownerId) {
        super(ownerId);
    }

    @Override
    protected List<ActiveDevice> getAccessPointDevices(SkudDatabaseBuilder skudDatabaseBuilder) {
        AccessKey ownerAccessKey = skudDatabaseBuilder.getStateMachineContext().getAccessKeys().get(ownerId);
        Set<String> deviceIds = new HashSet<>(
                ownerAccessKey.getAccessMap().getAccessPointDeviceIds());
        deviceIds.addAll(skudDatabaseBuilder.getStateMachineContext().getEmployees()
                .get(ownerAccessKey.getEmployeeId()).getAccessMap().getAccessPointDeviceIds());
        List<ActiveDevice> devices = new ArrayList<>();
        for (String deviceId : deviceIds) {
            ActiveDevice device = skudDatabaseBuilder.getStateMachineContext()
                    .getAllProjectDevicesByIds().get(deviceId);
            DeviceProfileView deviceProfileView = skudDatabaseBuilder.getStateMachineContext()
                    .getDeviceProfileViewsByIds()
                    .get(device.getDeviceProject().getDeviceProfileId());
            if (deviceProfileView != null && deviceProfileView.getDeviceProfile().isAccessPoint()) {
                devices.add(device);
            } else {
                skudDatabaseBuilder.addWarning(Code.WRONG_ACCESS_POINT_REFERENCE,
                        EntityType.ACCESS_KEY, ownerId);
            }
        }
        return devices;
    }

}
