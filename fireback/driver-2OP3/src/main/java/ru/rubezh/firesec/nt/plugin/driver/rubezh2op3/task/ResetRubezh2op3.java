package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class ResetRubezh2op3 extends Task {

    public ResetRubezh2op3() {
        super(PRIORITY_HIGH);
    }

    public ResetRubezh2op3(String issueId_) {
        super(PRIORITY_HIGH, issueId_);
    }

    @Override
    public boolean canBeExecuted() {
        return true;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().reset(currentTimeMs);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId, IssueStatus.IN_PROGRESS,
                "");
        return State.RESETTING_BY_USER;
    }

}
