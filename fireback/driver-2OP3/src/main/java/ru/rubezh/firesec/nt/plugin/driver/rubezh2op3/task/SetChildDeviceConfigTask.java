package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetChildDeviceConfigTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;
    private Map<String, String> propertyValues;

    public SetChildDeviceConfigTask(ActiveDevice activeDevice, Map<String, String> propertyValues, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.propertyValues = propertyValues;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                .get(activeDevice.getDeviceProject().getDeviceProfileId());
        int type = deviceProfile.getRsr3VirtualDeviceType();
        if (type == 0)
            type = deviceProfile.getRsr3RawDeviceType();
        stateMachineContext.getActionExecutor().setChildDeviceConfig(currentTimeMs,
                type, activeDevice.getDeviceProject().getLineNo(),
                activeDevice.getDeviceProject().getLineAddress(), propertyValues);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.SETTING_CHILD_DEVICE_CONFIG;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public Map<String, String> getPropertyValues() {
        return propertyValues;
    }
}
