package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetEventCountersAndStatesRS extends Rubezh2OP3GetParameterRS {
    private Map<String, Long> logCounters;
    private List<String> deviceStates;

    public Rubezh2OP3GetEventCountersAndStatesRS() {
        super(Rubezh2OP3ParameterType.EVENT_COUNTERS_AND_STATES);
    }

    public Map<String, Long> getLogCounters() {
        return logCounters;
    }

    public void setLogCounters(Map<String, Long> logCounters) {
        this.logCounters = logCounters;
    }

    public List<String> getDeviceStates() {
        return deviceStates;
    }

    public void setDeviceStates(List<String> deviceStates) {
        this.deviceStates = deviceStates;
    }

}
