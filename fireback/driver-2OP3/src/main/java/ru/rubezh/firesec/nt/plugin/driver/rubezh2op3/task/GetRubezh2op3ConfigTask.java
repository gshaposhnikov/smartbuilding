package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class GetRubezh2op3ConfigTask extends Task {

    private ActiveDevice activeDevice;

    private String firmwareVersion;
    private String serialNumber;

    public GetRubezh2op3ConfigTask(ActiveDevice activeDevice, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getControlDeviceInfo(currentTimeMs);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.WAITING_RUBEZH2OP3_CONFIG;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
