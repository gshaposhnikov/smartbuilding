package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile;

/*
 * Структура, описывающее текущее состояние обновления ПО на приборе
 */

public class Rubezh2OP3UpdatingSoftwareState {

    /** Максимальное число попыток записи данных */
    private static final int MAX_WRITE_ATTEMPTS = 5;
    /** Максималное число попыток переподключений */
    private static final int MAX_RECONNECT_ATTEMPTS = 5;
    /** Максималное число попыток запроса текущего режима */
    private static final int MAX_GETMMODE_ATTEMPTS = 5;
    /** Таймаут переподключения в мс */
    private static final int RECONNECT_TIMEOUT = 1000;

    /** HEX-файлы, которые были внутри файла прошивки */
    private HexFile hexFile;

    private int version;

    /** Записанная часть */
    private double writtenPart = 0.0;

    /** Кол-во оставшихся попыток записи */
    private int writeAttemptsLeft = MAX_WRITE_ATTEMPTS;

    /** Число оставшихся попыток переподключения */
    private int nReconnectAttemptsLeft = MAX_RECONNECT_ATTEMPTS;
    /** Число оставшихся попыток запроса текущего режима */
    private int nGetModeAttemptsLeft = MAX_GETMMODE_ATTEMPTS;

    public Rubezh2OP3UpdatingSoftwareState() {
    }

    public HexFile getHexFile() {
        return hexFile;
    }

    public void setHexFile(HexFile hexFile) {
        this.hexFile = hexFile;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public double getWrittenPart() {
        return writtenPart;
    }

    public void setWrittenPart(double writtenPart) {
        this.writtenPart = writtenPart;
    }

    public int getWriteAttemptsLeft() {
        return writeAttemptsLeft;
    }

    public void setWriteAttemptsLeft(int writeAttemptsLeft) {
        this.writeAttemptsLeft = writeAttemptsLeft;
    }

    public int getnReconnectAttemptsLeft() {
        return nReconnectAttemptsLeft;
    }

    public void setnReconnectAttemptsLeft(int nReconnectAttemptsLeft) {
        this.nReconnectAttemptsLeft = nReconnectAttemptsLeft;
    }

    public int getnGetModeAttemptsLeft() {
        return nGetModeAttemptsLeft;
    }

    public void setnGetModeAttemptsLeft(int nGetModeAttemptsLeft) {
        this.nGetModeAttemptsLeft = nGetModeAttemptsLeft;
    }

    public static int getReconnectTimeout() {
        return RECONNECT_TIMEOUT;
    }

}
