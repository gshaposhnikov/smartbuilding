package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetSecurityRegionStatusTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;

    private String regionId;
    private boolean onGuard;

    public SetSecurityRegionStatusTask(ActiveDevice activeDevice, String regionId, boolean onGuard, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.regionId = regionId;
        this.onGuard = onGuard;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().changeSecurityRegionStatus(currentTimeMs, regionId, onGuard);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.WAITING_SET_SECURITY_REGION_STATUS;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public String getRegionId() {
        return regionId;
    }

    public boolean isOnGuard() {
        return onGuard;
    }
}
