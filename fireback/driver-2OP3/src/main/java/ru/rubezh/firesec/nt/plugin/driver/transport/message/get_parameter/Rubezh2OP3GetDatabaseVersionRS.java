package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetDatabaseVersionRS extends Rubezh2OP3GetParameterRS {
    private String databaseVersion;

    public Rubezh2OP3GetDatabaseVersionRS() {
        super(Rubezh2OP3ParameterType.DB_BLOCK);
    }

    public String getDatabaseVersion() {
        return databaseVersion;
    }

    public void setDatabaseVersion(String databaseVersion) {
        this.databaseVersion = databaseVersion;
    }
}
