package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public abstract class Task implements Comparable<Task>{
    protected Rubezh2OP3StateMachineContext stateMachineContext;
    protected String issueId = null;
    protected int priority;

    final static int PRIORITY_VERY_LOW = 10;
    final static int PRIORITY_LOW = 100;
    final static int PRIORITY_DEFAULT = 1000;
    final static int PRIORITY_HIGH = 10000;
    final static int PRIORITY_VERY_HIGH = 100000;

    public Task(int priority) {
        this.priority = priority;
    }

    public Task(int priority, String issueId) {
        this.priority = priority;
        this.issueId = issueId;
    }

    public void setStateMachineContext(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    public Rubezh2OP3StateMachineContext getStateMachineContext() {
        return stateMachineContext;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    @Override
    public int compareTo(Task other) {
        return Integer.compare(this.priority, other.priority);
    }

    public abstract State execute(long currentTimeMs);

    public boolean canBeExecuted() {
        assert stateMachineContext.getRubezh2op3DeviceMode() != null : "Не считан режим прибора";

        return stateMachineContext.getRubezh2op3DeviceMode() == Rubezh2OP3UpdateSoftwareStatusType.NORMAL_OPERATION;
    }

    public void fail(long currentTimeMs) {
        if (issueId != null && !issueId.isEmpty()) {
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    issueId, IssueStatus.FAILED, "Задача не может быть выполнена");
        }
    }

}
