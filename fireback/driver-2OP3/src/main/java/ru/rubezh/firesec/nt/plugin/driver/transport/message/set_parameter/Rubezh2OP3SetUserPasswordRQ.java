package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.DeviceUserType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetUserPasswordRQ extends Rubezh2OP3SetParameterRQ {

    private DeviceUserType userType;
    private String password;

    public Rubezh2OP3SetUserPasswordRQ(DeviceUserType userType_, String password_) {
        super(Rubezh2OP3ParameterType.DB_BLOCK);
        userType = userType_;
        password = password_;
    }

    public DeviceUserType getUserType() {
        return userType;
    }

    public String getPassword() {
        return password;
    }

}
