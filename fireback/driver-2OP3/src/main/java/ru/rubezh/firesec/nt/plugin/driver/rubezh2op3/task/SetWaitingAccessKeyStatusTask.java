package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetWaitingAccessKeyStatusTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private final static int CONTROL_DEVICE_LINE_NO_STUMB = 0xFF;
    private final static int CONTROL_DEVICE_ADDRESS_STUMB = 0xFF;

    private ActiveDevice activeDevice;
    private boolean waitingAccessKey;
    private String accessKeyId;

    public SetWaitingAccessKeyStatusTask(String issueId, ActiveDevice activeDevice, boolean waitingAccessKey,
            String accessKeyId) {
        super(PRIORITY_DEFAULT, issueId);

        this.activeDevice = activeDevice;
        this.waitingAccessKey = waitingAccessKey;
        this.accessKeyId = accessKeyId;
    }

    @Override
    public State execute(long currentTimeMs) {
        int lineNo = CONTROL_DEVICE_LINE_NO_STUMB, address = CONTROL_DEVICE_ADDRESS_STUMB;
        if (activeDevice != stateMachineContext.getRubezh2op3Device()) {
            lineNo = activeDevice.getDeviceProject().getLineNo();
            address = activeDevice.getDeviceProject().getLineAddress();
        }

        stateMachineContext.getActionExecutor().setWaitingAccessKeyStatus(currentTimeMs, lineNo, address,
                isWaitingAccessKey());
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId, IssueStatus.IN_PROGRESS,
                "");
        return State.SETTING_ACCESS_KEY_WAITING_STATUS;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public boolean isWaitingAccessKey() {
        return waitingAccessKey;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

}
