package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

public final class TimerNames {
    public final static String EXECUTE_TASK = "executeTask";
    public final static String RECONNECT = "reconnect";
    public final static String GET_STATE_BYTES = "getStateBytes";
    public final static String GET_DEVICE_CONFIGURATION_RETRY = "getDeviceConfiguration";
    public final static String CHECK_EVENTS = "checkEvents";
    public final static String CHECK_REGION_STATES = "checkRegionStates";
    public final static String CHECK_CHILD_DEVICE_STATES = "checkChildDeviceStates";
    public final static String LONG_OPERATION_CHECK = "longOperationCheck";
    public final static String RESET_TIMEOUT = "resetTimeout";
    public final static String START_NEXT_STAGE = "startNextStage";
    public final static String UPDATE_SOFTWARE_MODE_RETRY = "updateSoftwareModeRetry";
    public final static String UPDATE_SOFTWARE_MODE_RECONNECT = "updateSoftwareModeReconnect";
    public final static String SET_TIME = "setTime";
    public final static String DATABASE_WRITING_TIMER = "databaseWritingTimer";
    public final static String SEND_EVENTS = "sendEvents";
    public final static String UPDATE_FW_TIMER = "updateFWtimer";
    public final static String CHECK_MODE = "checkMode";
}
