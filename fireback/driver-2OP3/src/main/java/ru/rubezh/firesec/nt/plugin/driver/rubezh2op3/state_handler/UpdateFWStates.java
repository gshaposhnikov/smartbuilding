package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingSoftwareState;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile.MemoryType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.Task;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.UpdateFWTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareErrorType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public class UpdateFWStates {

    public static abstract class Base extends AbstractConnected {

        @Override
        public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
            try {
                AbstractNone handler = this.getClass().getConstructor().newInstance();
                handler.stateMachineContext = stateMachineContext;
                return handler;
            } catch (Exception e) {
                stateMachineContext.getLogger().fatal("Exceprion: {}", e);
            }
            return null;
        }

        @Override
        public State onDisconnectRequest(long currentTimeMs) {
            stateMachineContext.getLogger().warn("Writing firmware aborted by user");
            Task task = stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                    IssueStatus.FAILED, "Прервано");
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            Driver.freeExclusiveMode();
            return super.onDisconnectRequest(currentTimeMs);
        }

        @Override
        public State onDisconnected(long currentTimeMs) {
            return onHardwareSideAborted(currentTimeMs, "Потеря связи");
        }

        @Override
        public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
            stateMachineContext.getLogger().warn("Writing firmware aborted, exception: {}", exception.toString());
            Task task = stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                    IssueStatus.FAILED, "Потеря связи");
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            Driver.freeExclusiveMode();
            return super.onDisconnected(currentTimeMs);
        }

        protected State onHardwareSideAborted(long currentTimeMs, String errorMessage) {
            stateMachineContext.getLogger().warn("Writing firmware aborted");
            Task task = stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                    IssueStatus.FAILED, errorMessage);
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            Driver.freeExclusiveMode();
            return super.onDisconnected(currentTimeMs);
        }

        protected void writeNextFWBlock(long currentTimeMs, HexFile hexFile) {
            int blockAddress = hexFile.getLowAddress() + hexFile.getContinuousData().position();
            int blockSize = Integer.min(hexFile.getContinuousData().remaining(),
                    stateMachineContext.getWriteBlockSize());
            byte[] blockData = new byte[blockSize];
            hexFile.getContinuousData().get(blockData);
            stateMachineContext.getActionExecutor().writeMemory(currentTimeMs, blockAddress, blockData);
        }

        protected State beginErasingOrWriting(long currentTimeMs) {
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                    stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                    UpdateFWTask.getArmUpdateProgress());
            Rubezh2OP3UpdatingSoftwareState updatingState = stateMachineContext.getUpdatingSoftwareState();
            updatingState.getHexFile().getContinuousData().position(0);
            updatingState.setWrittenPart(0.0);
            int startSector = updatingState.getHexFile().getLowAddress() >> 16;
            int endSector = updatingState.getHexFile().getHighAddress() >> 16;
            assert startSector >= 0
                    && endSector >= startSector : "В задачу должен передаваться файл с корректными адресами";

            /* Нулевой сектор стирать нельзя */
            if (startSector == 0 && endSector == 0) {
                HexFile hexFile = updatingState.getHexFile();
                writeNextFWBlock(currentTimeMs, hexFile);
                return State.UPDATE_FW_STATE30;
            } else {
                if (startSector == 0)
                    ++startSector;
                if (stateMachineContext.getActionExecutor().eraseSectors(currentTimeMs, startSector, endSector))
                    return State.UPDATE_FW_STATE20;
                else
                    return onHardwareSideAborted(currentTimeMs, "Не удалось запустить стирание секторов");
            }
        }

    }

    /* запуск процесса прошивки */
    public static class State5 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE5;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            assert stateMachineContext.getRubezh2op3DeviceMode() != null : "Не считан режим прибора";
            switch (stateMachineContext.getRubezh2op3DeviceMode()) {
            default:
                stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                        Rubezh2OP3UpdateSoftwareModeType.ARM);
                return State.UPDATE_FW_STATE10;
            case UPDATE_ARM:
                stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                        Rubezh2OP3UpdateSoftwareModeType.LOADER);
                return State.UPDATE_FW_STATE15;
            case UPDATE_LOADER:
                return beginErasingOrWriting(currentTimeMs);
            }
        }

    }

    private static abstract class InvokingSoftwareUpdateMode extends Base {

        abstract protected State getNextState();

        abstract protected State getNextStateAfterReconnect();

        @Override
        public State onSoftwareUpdateModeInvoked(long currentTimeMs, int waitingTimeMs) {
            stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER, waitingTimeMs,
                    false);
            return getNextState();
        }

        @Override
        public State onDisconnected(long currentTimeMs) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                    Rubezh2OP3UpdatingSoftwareState.getReconnectTimeout(), false);
            return getCurrentState();
        }

        @Override
        public State onReceiveTimeout(long currentTimeMs) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                    Rubezh2OP3UpdatingSoftwareState.getReconnectTimeout(), false);
            return getCurrentState();
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getActionExecutor().connect(currentTimeMs);
            return getCurrentState();
        }

        @Override
        public State onConnectFailure(long currentTimeMs) {
            int nReconnectAttemptsLeft = stateMachineContext.getUpdatingSoftwareState().getnReconnectAttemptsLeft();
            if (nReconnectAttemptsLeft > 0) {
                stateMachineContext.getUpdatingSoftwareState().setnReconnectAttemptsLeft(nReconnectAttemptsLeft - 1);
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        Rubezh2OP3UpdatingSoftwareState.getReconnectTimeout(), false);
                return getCurrentState();
            }

            /* Разрываем связь и завершаем с неуспехом задачу, если подключение так и не восстанавливается */
            return super.onDisconnected(currentTimeMs);
        }

        @Override
        public State onConnected(long currentTimeMs) {
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
            return getNextStateAfterReconnect();
        }

    }

    /* вход в АРМ-режим */
    public static class State10 extends InvokingSoftwareUpdateMode {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE10;
        }

        @Override
        protected State getNextState() {
            return State.UPDATE_FW_STATE11;
        }

        @Override
        protected State getNextStateAfterReconnect() {
            return State.UPDATE_FW_STATE12;
        }

    }

    /* ТО после входа в АРМ */
    public static class State11 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE11;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
            return State.UPDATE_FW_STATE12;
        }
    }

    /* проверка готовности устройства */
    public static class State12 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE12;
        }

        @Override
        public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
            if (isReady) {
                stateMachineContext.getActionExecutor().getUpdateSoftwareError(currentTimeMs);
                return State.UPDATE_FW_STATE13;
            } else {
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getLongOperationWaitingTimeMs(), false);
                return State.UPDATE_FW_STATE11;
            }
        }
    }

    /* проверка ошибки при обновлении ПО */
    public static class State13 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE13;
        }

        @Override
        public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
            if (error == Rubezh2OP3UpdateSoftwareErrorType.SUCCESS) {
                stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
                return State.UPDATE_FW_STATE14;
            } else {
                stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                        Rubezh2OP3UpdateSoftwareModeType.ARM);
                return State.UPDATE_FW_STATE10;
            }
        }
    }

    /* проверка статуса обновления ПО (режима ОС) */
    public static class State14 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE14;
        }

        @Override
        public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status) {
            if (status == Rubezh2OP3UpdateSoftwareStatusType.UPDATE_ARM) {
                stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                        Rubezh2OP3UpdateSoftwareModeType.LOADER);
                return State.UPDATE_FW_STATE15;
            }

            int nGetModeAttemptsLeft = stateMachineContext.getUpdatingSoftwareState().getnGetModeAttemptsLeft();
            if (nGetModeAttemptsLeft > 0) {
                stateMachineContext.getUpdatingSoftwareState().setnGetModeAttemptsLeft(nGetModeAttemptsLeft - 1);
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getUpdateSoftwareModeRetry(), false);
                return getCurrentState();
            }

            /* Разрываем связь и завершаем с неуспехом задачу, если в ARM-режим так и не перешли */
            return super.onDisconnected(currentTimeMs);
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
            return getCurrentState();
        }

    }

    /* вход в LOADER-режим */
    public static class State15 extends InvokingSoftwareUpdateMode {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE15;
        }

        @Override
        protected State getNextState() {
            return State.UPDATE_FW_STATE16;
        }

        @Override
        protected State getNextStateAfterReconnect() {
            return State.UPDATE_FW_STATE17;
        }

    }

    /* ТО после входа в LOADER */
    public static class State16 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE16;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
            return State.UPDATE_FW_STATE17;
        }
    }

    /* проверка готовности устройства */
    public static class State17 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE17;
        }

        @Override
        public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
            if (isReady) {
                stateMachineContext.getActionExecutor().getUpdateSoftwareError(currentTimeMs);
                return State.UPDATE_FW_STATE18;
            } else {
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getLongOperationWaitingTimeMs(), false);
                return State.UPDATE_FW_STATE16;
            }
        }

    }

    /* проверка ошибки при обновлении ПО */
    public static class State18 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE18;
        }

        @Override
        public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
            if (error == Rubezh2OP3UpdateSoftwareErrorType.SUCCESS) {
                stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
                return State.UPDATE_FW_STATE19;
            } else {
                stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs,
                        Rubezh2OP3UpdateSoftwareModeType.ARM);
                return State.UPDATE_FW_STATE10;
            }
        }
    }

    /* проверка статуса обновления ПО (режима ОС) */
    public static class State19 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE19;
        }

        @Override
        public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status) {
            if (status == Rubezh2OP3UpdateSoftwareStatusType.UPDATE_LOADER) {
                return beginErasingOrWriting(currentTimeMs);
            }

            int nGetModeAttemptsLeft = stateMachineContext.getUpdatingSoftwareState().getnGetModeAttemptsLeft();
            if (nGetModeAttemptsLeft > 0) {
                stateMachineContext.getUpdatingSoftwareState().setnGetModeAttemptsLeft(nGetModeAttemptsLeft - 1);
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getUpdateSoftwareModeRetry(), false);
                return getCurrentState();
            }

            /* Разрываем связь и завершаем с неуспехом задачу, если в LOADER-режим так и не перешли */
            return super.onDisconnected(currentTimeMs);
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
            return getCurrentState();
        }

    }

    private static abstract class AfterErasingOrWritingStarted extends Base {

        @Override
        public State onReceiveTimeout(long currentTimeMs) {
            Rubezh2OP3UpdatingSoftwareState updatingState = stateMachineContext.getUpdatingSoftwareState();
            if (updatingState.getWriteAttemptsLeft() > 0) {
                updatingState.setWriteAttemptsLeft(updatingState.getWriteAttemptsLeft() - 1);
                stateMachineContext.getLogger().warn("Restart writing firmware, attempts left {}",
                        updatingState.getWriteAttemptsLeft());
                return beginErasingOrWriting(currentTimeMs);
            } else
                return onDisconnected(currentTimeMs);
        }

    }

    /* запуск стирания секторов */
    public static class State20 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE20;
        }

        @Override
        public State onEraseSectorsInvoked(long currentTimeMs, int waitingTimeMs) {
            stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                    waitingTimeMs, false);
            return State.UPDATE_FW_STATE21;
        }
    }

    /* ТО после стирвания секторов */
    public static class State21 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE21;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                    stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                    UpdateFWTask.getErasingSectorsProgress());
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
            return State.UPDATE_FW_STATE22;
        }
    }

    /* проверка готовности устройства */
    public static class State22 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE22;
        }

        @Override
        public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
            if (isReady) {
                stateMachineContext.getActionExecutor().getUpdateSoftwareError(currentTimeMs);
                return State.UPDATE_FW_STATE23;
            } else {
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getLongOperationWaitingTimeMs(), false);
                return State.UPDATE_FW_STATE21;
            }
        }

    }

    /* проверка ошибки при обновлении ПО */
    public static class State23 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE23;
        }

        @Override
        public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
            if (error == Rubezh2OP3UpdateSoftwareErrorType.SUCCESS) {
                HexFile hexFile = stateMachineContext.getUpdatingSoftwareState().getHexFile();
                writeNextFWBlock(currentTimeMs, hexFile);
                return State.UPDATE_FW_STATE30;
            } else {
                return onHardwareSideAborted(currentTimeMs, "Ошибка при стирании секторов");
            }
        }

    }

    /* команда на запись данных */
    public static class State30 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE30;
        }

        @Override
        public State onWriteMemoryInvoked(long currentTimeMs, int waitingTimeMs) {
            stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER, waitingTimeMs,
                    false);
            return State.UPDATE_FW_STATE31;
        }

    }

    /* ТО на запись данных */
    public static class State31 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE31;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            HexFile hexFile = stateMachineContext.getUpdatingSoftwareState().getHexFile();
            if (hexFile.getContinuousData().hasRemaining()) {

                writeNextFWBlock(currentTimeMs, hexFile);

                double writtenPart = ((double) hexFile.getContinuousData().position())
                        / ((double) hexFile.getContinuousData().limit());

                if (stateMachineContext.getUpdatingSoftwareState().getWrittenPart() + 0.02 < writtenPart) {
                    stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                            stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                            UpdateFWTask.getErasingSectorsProgress() + ((UpdateFWTask.getResettingProgress()
                                    - UpdateFWTask.getErasingSectorsProgress()) * writtenPart));

                    stateMachineContext.getUpdatingSoftwareState().setWrittenPart(writtenPart);
                }

                return State.UPDATE_FW_STATE30;
            } else if (hexFile.getMemoryType() == MemoryType.APP) {
                /* Если записывалось ПО верхнего уровня, то обновим версию */
                stateMachineContext.getActionExecutor().setFrimwareVersion(currentTimeMs,
                        stateMachineContext.getUpdatingSoftwareState().getVersion());
                return State.UPDATE_FW_STATE32;
            } else {
                /* Не перезагружать прибор, а возвращать его в рабочий режим, ели обновлялся не верхний уровень */
                stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                        stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                        UpdateFWTask.getResettingProgress());
                stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                        stateMachineContext.getResetTimeout(), false);
                return State.UPDATE_FW_STATE40;
            }
        }
    }

    /* устновка новой версии прибора */
    public static class State32 extends AfterErasingOrWritingStarted {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE32;
        }

        @Override
        public State onFirmwareVersionSet(long currentTimeMs) {
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                    stateMachineContext.getTaskManager().getCurrentTask().getIssueId(),
                    UpdateFWTask.getResettingProgress());
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER,
                    stateMachineContext.getResetTimeout(), false);
            return State.UPDATE_FW_STATE40;
        }

    }

    /* ТО перед перезапуском прибора */
    public static class State40 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE40;
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            Driver.freeExclusiveMode();
            stateMachineContext.getActionExecutor().reset(currentTimeMs);
            return State.UPDATE_FW_STATE41;
        }

    }

    /* команда на перезапуск прибора */
    public static class State41 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE41;
        }

        @Override
        public State onResetInvoked(long currentTimeMs, int waitingTime) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_FW_TIMER, waitingTime,
                    false);
            return State.UPDATE_FW_STATE42;
        }

        @Override
        public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
            return onResetInvoked(currentTimeMs, stateMachineContext.getResetTimeout());
        }

        @Override
        public State onDisconnected(long currentTimeMs) {
            /* При подключении по USB теряется HID-устройство */
            return onResetInvoked(currentTimeMs, stateMachineContext.getResetTimeout());
        }

        @Override
        public State onReceiveTimeout(long currentTimeMs) {
            /* При подключении по USB теряется HID-устройство и ответ на последнюю команду не приходит */
            return onResetInvoked(currentTimeMs, stateMachineContext.getResetTimeout());
        }

    }

    /* ТО на перезапуск прибора */
    public static class State42 extends Base {

        @Override
        public State getCurrentState() {
            return State.UPDATE_FW_STATE42;
        }

        @Override
        public State onDisconnected(long currentTimeMs) {
            return getCurrentState();
        }

        @Override
        public State onReceiveTimeout(long currentTimeMs) {
            return getCurrentState();
        }

        @Override
        public State onTimer(long currentTimeMs, String timerName) {
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    stateMachineContext.getTaskManager().getCurrentTask().getIssueId(), IssueStatus.FINISHED, "");
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RECONNECT,
                    stateMachineContext.getReconnectInterval(), false);
            return State.WAIT_RECONNECT;
        }

    }

}
