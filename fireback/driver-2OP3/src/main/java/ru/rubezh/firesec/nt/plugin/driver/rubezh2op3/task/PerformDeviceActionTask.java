package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceAction;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class PerformDeviceActionTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;
    private String actionId;
    private Map<String, String> propertyValues;

    public PerformDeviceActionTask(ActiveDevice activeDevice, String actionId,
            Map<String, String> propertyValues, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.actionId = actionId;
        this.propertyValues = propertyValues;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                .get(activeDevice.getDeviceProject().getDeviceProfileId());
        int deviceType = deviceProfile.getRsr3VirtualDeviceType();
        if (deviceType == 0)
            deviceType = deviceProfile.getRsr3RawDeviceType();
        DeviceAction deviceAction = deviceProfile.getSupportedActions().get(actionId);
        stateMachineContext.getActionExecutor().performDeviceAction(currentTimeMs,
                deviceType, activeDevice.getDeviceProject().getLineNo(),
                activeDevice.getDeviceProject().getLineAddress(), deviceAction, propertyValues);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.PERFORMING_DEVICE_ACTION;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public String getActionId() {
        return actionId;
    }

}
