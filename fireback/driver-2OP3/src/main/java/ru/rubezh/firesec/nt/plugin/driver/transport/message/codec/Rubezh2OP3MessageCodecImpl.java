package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;


import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.Function;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent.*;
import ru.rubezh.firesec.nt.domain.v1.DecodableEventStateSet.BitDecodableItem;
import ru.rubezh.firesec.nt.domain.v1.DecodableEventStateSet.MaskedDecodableItem;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.DeviceUserType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.ChildDeviceConfigCodec;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter.*;

public class Rubezh2OP3MessageCodecImpl implements Rubezh2OP3MessageCodec {

    private static final int BUFFER_SIZE = 512;
    private static final byte EXCEPTION_FLAG_MASK = (byte) (0x80 & 0xFF);
    private static final byte FUNCTION_MASK = 0x3F;

    /** Количество байт в блоке общего состояния устройства */
    private static final int STATE_BYTES_BLOCK_SIZE = 4;
    /** Количество блоков общего состояния устройства */
    private static final int STATE_BYTES_BLOCK_COUNT = 2;
    /** Количество байт в GUID устройства */
    private static final int GUID_SIZE = 16;
    /** Смещение начала GUID относительно начала настроечных параметров */
    private static final int GUID_OFFSET = 0xE4;
    /** Количество байт в серийном номере устройства */
    private static final int SERIAL_SIZE = 12;
    /** Количетсво байт в серийном номере дочернего устройства */
    private static final int CHILD_SERIAL_SIZE = 3;
    /** Размер поля со счетчиком событий */
    private static final int EVENT_COUNTER_SIZE = 4;
    /** Размер поля с максимальным количеством событий */
    private static final int MAX_EVENT_COUNTER_SIZE = 2;
    /** Номера байт в событии, определяющих состояние зоны */
    private static final int[] REGION_STATE_BYTE_NOS = new int[]{ 16, 15, 23, -1};
    /** Размер поля "адрес" */
    private static final int ADDRESS_LENGTH = 4;
    /** Размер поля "размер" */
    private static final int SIZE_LENGTH = 1;
    /** Размер поля с паролем */
    private static final int PASSWORD_LENGTH = 3;
    /** Смещение блока с паролями */
    private static final int PASSWORDS_OFFSET = 4;
    /** Пустой символ в пароле */
    private static final int PASSWORD_CHARACTERS_NUMBER = 6;
    /** Пустой символ в пароле */
    private static final char PASSWORD_EMPTY_CHARACTER = 'f';
    /** Пустой байт в пароле */
    private static final byte PASSWORD_EMPTY_BYTE = (byte)0xff;
    /** Наибольший размер конфигурации дочернего устройства в байтах */
    private static final int MAX_CHILD_CONFIG_SIZE = 1024;
    /** Код команды управления сценарием сообщения "0x02 0x54" */
    private static final int SCENARIO_MANAGE_COMMAND_CODE = 13;
    /** Смещение в событии байта с длиной значения ключа */
    private static final int ACCESS_KEY_VALUE_SIZE_EVENT_OFFSET = 15;
    /** Смещение в событии начала массива байт - значения ключа */
    private static final int ACCESS_KEY_VALUE_EVENT_OFFSET = 18;
    /** Размер адресного листа прибора */
    private static final int ADDRESS_LIST_SIZE = 32;

    private static final int RUBEZH2OP3_INFO_ADDRESS = 0xFF0000;
    private static final int RUBEZH2OP3_INFO_SIZE = 0x100;

    private Logger logger;
    private ChildDeviceConfigCodec childDeviceConfigCodec;
    private DeviceProfile rubezh2op3DeviceProfile;
    private Map<String, DeviceProfile> supportedDeviceProfiles;
    private DriverProfile driverProfile;

    public Rubezh2OP3MessageCodecImpl(Logger logger, ChildDeviceConfigCodec childDeviceConfigCodec,
            DeviceProfile rubezh2op3DeviceProfile, Map<String, DeviceProfile> supportedDeviceProfiles,
            DriverProfile driverProfile) {

        this.logger = logger;
        this.childDeviceConfigCodec = childDeviceConfigCodec;
        this.rubezh2op3DeviceProfile = rubezh2op3DeviceProfile;
        this.supportedDeviceProfiles = supportedDeviceProfiles;
        this.driverProfile = driverProfile;
    }

    private String getHex(ByteBuffer byteBuffer) {
        ByteBuffer bb = byteBuffer.duplicate();
        bb.flip();
        byte[] b = new byte[bb.remaining()];
        bb.get(b);
        return Hex.encodeHexString(b);
    }


    private ByteBuffer startEncodeMessage(Rubezh2OP3Message message) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
        byteBuffer.put((byte) (Rubezh2OP3FunctionType.toInteger(message.getFunctionType()) & 0xFF));
        return byteBuffer;
    }

    private ByteBuffer startEncodeSetGetParameterRQ(Rubezh2OP3SetGetParameterRQ setGetParameterRQ, byte[] context) {
        ByteBuffer byteBuffer = startEncodeMessage(setGetParameterRQ);
        byteBuffer.put((byte) (Rubezh2OP3ParameterType.toInteger(setGetParameterRQ.getParameterType()) & 0xFF));
        if (context != null) {
            byteBuffer.put(context);
        }
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetEventCountersAndStatesRQ getEventCountersAndStateBytesRQ) {
        return startEncodeSetGetParameterRQ(getEventCountersAndStateBytesRQ, null);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetMaxEventCountersRQ getMaxEventCountersRQ) {
        return startEncodeSetGetParameterRQ(getMaxEventCountersRQ, null);
    }

    private byte[] encodeGetDataBlockRQContext(int address, int size) {
        assert size <= 256 : "Нельзя считывать больше 256 байт командой GET 0x52";

        byte[] context = new byte[5];
        for (int i = 3; i >= 0; --i) {
            context[i] = (byte) address;
            address >>= 8;
        }
        context[4] = (byte) ((size - 1) & 0xFF);
        return context;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetControlDeviceInfoRQ getDeviceConfigurationRQ) {
        return startEncodeSetGetParameterRQ(getDeviceConfigurationRQ,
                encodeGetDataBlockRQContext(RUBEZH2OP3_INFO_ADDRESS, RUBEZH2OP3_INFO_SIZE));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDataBlockRQ getDataBlockRQ) {
        int address = getDataBlockRQ.getAddress();
        int length = getDataBlockRQ.getLength();

        return startEncodeSetGetParameterRQ(getDataBlockRQ, encodeGetDataBlockRQContext(address, length));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetLogEventRQ getLogEventRQ) {
        int logCode = getLogEventRQ.getLogCode();
        long eventIndex = getLogEventRQ.getEventIndex();

        byte[] context = new byte[5];
        context[0] = (byte) logCode;
        for (int i = 4; i >= 1; i--) {
            context[i] = (byte) (eventIndex & 0xFF);
            eventIndex >>= 8;
        }

        return startEncodeSetGetParameterRQ(getLogEventRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetRegionStatesRQ getRegionStatesRQ) {
        int regionNo = getRegionStatesRQ.getRegionNo();

        byte[] context = new byte[3];
        context[0] = (byte) ((regionNo >> 8) & 0xFF);
        context[1] = (byte) (regionNo & 0xFF);
        context[2] = (byte) (0x01 & 0xFF);

        return startEncodeSetGetParameterRQ(getRegionStatesRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceStatesRQ getChildDeviceStatesRQ) {
        int lineNo = getChildDeviceStatesRQ.getLineNo();
        int address = getChildDeviceStatesRQ.getAddress();

        byte[] context = new byte[3];
        context[0] = (byte) (lineNo & 0xFF);
        context[1] = (byte) (address & 0xFF);
        context[2] = (byte) (0x01 & 0xFF);

        return startEncodeSetGetParameterRQ(getChildDeviceStatesRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetChildDeviceConfigRQ getChildDeviceConfigRQ) {
        int lineNo = getChildDeviceConfigRQ.getLineNo();
        int address = getChildDeviceConfigRQ.getAddress();

        byte[] context = new byte[3];
        context[0] = (byte) (lineNo & 0xFF);
        context[1] = (byte) (address & 0xFF);
        context[2] = (byte) (0x01 & 0xFF);

        return startEncodeSetGetParameterRQ(getChildDeviceConfigRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetChildDeviceConfigRQ setChildDeviceConfigRQ) {
        int type = setChildDeviceConfigRQ.getType();
        int lineNo = setChildDeviceConfigRQ.getLineNo();
        int address = setChildDeviceConfigRQ.getAddress();

        DeviceProfile deviceProfile = findDeviceProfileByDeviceType(type);
        if (deviceProfile != null) {
            int configSize = deviceProfile.getConfigSize();
            byte[] params = new byte[configSize];
            childDeviceConfigCodec.encodeDeviceProfileConfig(params,
                    deviceProfile.getConfigProperties(), setChildDeviceConfigRQ.getPropertyValues());

            byte[] context = new byte[7 + configSize];
            context[0] = (byte) (type & 0xFF);
            context[1] = (byte) (address & 0xFF);
            context[2] = context[3] = context[4] = (byte) 0;
            context[5] = (byte) (deviceProfile.getConfigSize() & 0xFF);
            context[6] = (byte) (lineNo & 0xFF);
            System.arraycopy(params, 0, context, 7, params.length);

            return startEncodeSetGetParameterRQ(setChildDeviceConfigRQ, context);
        } else {
            logger.error("Can't find appropriate device profile with type={}", type);
            return null;
        }
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetCurrentTimeRQ setCurrentTimeRQ) {
        return startEncodeSetGetParameterRQ(setCurrentTimeRQ, encodeDateTime(setCurrentTimeRQ.getDateTime()));
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetStateDeviceResetRQ setStateDeviceResetRQ) {
        byte[] context;
        switch (setStateDeviceResetRQ.getTargetResetGroup()) {
        case FIRE:
            /*
             * Длина контекста: 8
             * 1 - код команды (0x10 - сброс пожара)
             * 2 - Локальный номер зоны (старший байт)
             * 3 - Локальный номер зоны (младший байт)
             * Для сброса пожара на всём приборе в номере зоны должны быть нули.
             */
            context = new byte[]{
                    0x10,
                    0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00 };
            break;
        case ALARM:
            /*
             * Длина контекста: 8
             * 1 - код команды (0x09 - сброс тревоги)
             * 2 - Локальный номер зоны (старший байт)
             * 3 - Локальный номер зоны (младший байт)
             * В случае сброса тревоги на всем приборе номер зоны должны 0xFF00.
             */
            context = new byte[]{
                    0x09,
                    (byte)0xFF, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00 };
            break;
        default:
            context = new byte[]{
                    0x00,
                    0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00 };
            break;
        }

        return startEncodeSetGetParameterRQ(setStateDeviceResetRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3InvokeUpdateSoftwareModeRQ setUpdateSoftwareModeRQ) {
        ByteBuffer byteBuffer = startEncodeMessage(setUpdateSoftwareModeRQ);
        byteBuffer.put(
                (byte) Rubezh2OP3UpdateSoftwareModeType.toInteger(setUpdateSoftwareModeRQ.getSoftwareType()).intValue());
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3CheckDeviceReadyRQ checkDeviceReadyRQ) {
        return startEncodeMessage(checkDeviceReadyRQ);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3ResetRQ resetRQ) {
        return startEncodeMessage(resetRQ);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3EraseSectorsRQ eraseSectorsRQ) {
        ByteBuffer byteBuffer = startEncodeMessage(eraseSectorsRQ);
        byteBuffer.put((byte)eraseSectorsRQ.getStartSector());
        byteBuffer.put((byte)eraseSectorsRQ.getEndSector());
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3WriteMemoryRQ writeMemoryRQ) {
        ByteBuffer byteBuffer = startEncodeMessage(writeMemoryRQ);
        int address = writeMemoryRQ.getAddress();
        byte[] addressRaw = new byte[4];
        for (int i = 3; i >= 0; --i) {
            addressRaw[i] = (byte) (address & 0xFF);
            address >>= 8;
        }
        byteBuffer.put(addressRaw);
        byteBuffer.put(writeMemoryRQ.getData());
        if (logger.isTraceEnabled())
        {
            ByteBuffer bb = byteBuffer.duplicate();
            bb.flip();
            byte[] b = new byte[bb.remaining()];
            bb.get(b);
            logger.trace("{}", Hex.encodeHexString(b));
        }
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareErrorRQ getUpdateSoftwareErrorRQ) {
        return startEncodeMessage(getUpdateSoftwareErrorRQ);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetUpdateSoftwareStatusRQ getUpdateSoftwareStatusRQ) {
        return startEncodeSetGetParameterRQ(getUpdateSoftwareStatusRQ, null);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetFirmwareVersionRQ getFirmwareVersionRQ) {
        return startEncodeSetGetParameterRQ(getFirmwareVersionRQ, null);
    }

     private int getUserPasswordOffset(DeviceUserType userType) {
        switch (userType) {
        default:
        case DUTY:
            return PASSWORDS_OFFSET;
        case INSTALLER:
            return PASSWORDS_OFFSET + 1 * PASSWORD_LENGTH;
        case ADMINISTRATOR:
            return PASSWORDS_OFFSET + 2 * PASSWORD_LENGTH;
        }
    }

    private byte[] encodeUserPassword(String shortPassword) {
        StringBuilder fullPassword = new StringBuilder(PASSWORD_CHARACTERS_NUMBER);
        fullPassword.append(shortPassword);
        while (fullPassword.length() < PASSWORD_CHARACTERS_NUMBER)
            fullPassword.append(PASSWORD_EMPTY_CHARACTER);
        try {
            return Hex.decodeHex(fullPassword.toString().toCharArray());
        } catch (DecoderException ex) {
            byte[] defaultPassword = new byte[PASSWORD_LENGTH];
            Arrays.fill(defaultPassword, PASSWORD_EMPTY_BYTE);
            return defaultPassword;
        }
   }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetUserPasswordRQ setUserPasswordRQ) {
        int passwordOffset = getUserPasswordOffset(setUserPasswordRQ.getUserType());
        byte[] passwordBytes = encodeUserPassword(setUserPasswordRQ.getPassword());
        byte[] CONTEXT = new byte[ADDRESS_LENGTH + SIZE_LENGTH + PASSWORD_LENGTH];
        // address
        CONTEXT[0] = 0;
        CONTEXT[1] = 0;
        CONTEXT[2] = 0;
        CONTEXT[3] = (byte)passwordOffset;
        // size
        CONTEXT[4] = (byte)(PASSWORD_LENGTH - 1);
        // data
        System.arraycopy(passwordBytes, 0, CONTEXT, ADDRESS_LENGTH + SIZE_LENGTH, PASSWORD_LENGTH);

        return startEncodeSetGetParameterRQ(setUserPasswordRQ, CONTEXT);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetDatabaseVersionRQ getDatabaseVersionRQ) {
        final byte[] CONTEXT = {
            0x00, (byte) 0x80, 0x01, 0x06, // address
            0x02 // size
        };

        return startEncodeSetGetParameterRQ(getDatabaseVersionRQ, CONTEXT);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetSecurityRegionStatusRQ setSecurityRegionStatusRQ) {
        byte[] context;
        int address = setSecurityRegionStatusRQ.getRegionNo();
        if (setSecurityRegionStatusRQ.isOnGuard()) {
            /* Постановка зоны на охрану
             * Длина контекста: 8
             * 1 - код команды (0x08 - постановка на охрану)
             * 2 - Локальный номер зоны (старший байт)
             * 3 - Локальный номер зоны (младший байт)
             */
            context = new byte[]{
                    0x08,
                    (byte) ((address >> 8) & 0xFF), (byte) (address & 0xFF),
                    0x00, 0x00, 0x00, 0x00, 0x00};
        } else {
            /* Снятие зоны с охраны
             * Длина контекста: 8
             * 1 - код команды (0x09 - снятие с охраны)
             * 2 - Локальный номер зоны (старший байт)
             * 3 - Локальный номер зоны (младший байт)
             */
            context = new byte[]{
                    0x09,
                    (byte) ((address >> 8) & 0xFF), (byte) (address & 0xFF),
                    0x00, 0x00, 0x00, 0x00, 0x00 };
        }
        return startEncodeSetGetParameterRQ(setSecurityRegionStatusRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetDevicePollingStateRQ setDevicePollingStateRQ) {
        byte[] context;
        /* Включение/выключение устройств
         * Длина контекста: 8
         * 1 - Код команды (11 - управление устройством)
         * 2 - Включение/выключение устройства
         * 4 - Адрес устройства
         * 5 - Тип устройства
         * 8 - Линия
         */
        context = new byte[]{
                0x0B,
                (setDevicePollingStateRQ.isPollingState() ? (byte)0x00 : (byte)0x01),
                0x00,
                (byte) (setDevicePollingStateRQ.getAddress() & 0xFF),
                (byte) (setDevicePollingStateRQ.getType() & 0xFF),
                0x00,
                0x00,
                (byte) ((setDevicePollingStateRQ.getLineNo() - 1) & 0xFF)};
        return startEncodeSetGetParameterRQ(setDevicePollingStateRQ, context);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetFirmwareVersionRQ setFirmwareVersionRQ) {
        ByteBuffer byteBuffer = startEncodeSetGetParameterRQ(setFirmwareVersionRQ, null);
        byte minorBcd = (byte) ((((setFirmwareVersionRQ.getMinorVersion() / 10) << 4)
                | (setFirmwareVersionRQ.getMinorVersion() % 10)) & 0xFF);
        byte majorBcd = (byte) ((((setFirmwareVersionRQ.getMajorVersion() / 10) << 4)
                | (setFirmwareVersionRQ.getMajorVersion() % 10)) & 0xFF);
        byteBuffer.put(majorBcd);
        byteBuffer.put(minorBcd);
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformDeviceActionRQ performDeviceActionRQ) {
        int type = performDeviceActionRQ.getType();
        int lineNo = performDeviceActionRQ.getLineNo();
        int address = performDeviceActionRQ.getAddress();

        DeviceAction deviceAction = performDeviceActionRQ.getDeviceAction();
        DeviceAction.Extension actionExtension = deviceAction.getExtension();

        ByteBuffer byteBuffer = startEncodeSetGetParameterRQ(performDeviceActionRQ, null);

        byteBuffer.put((byte)deviceAction.getCommand());
        byteBuffer.put((byte)deviceAction.getSubCommand());
        byteBuffer.put((byte)0);
        byteBuffer.put((byte)address);
        byteBuffer.put((byte)type);
        if (actionExtension == null) {
            byteBuffer.put((byte)0);
            byteBuffer.put((byte)0);
        } else {
            byteBuffer.put((byte)actionExtension.getSubCommand2());
            byteBuffer.put((byte)actionExtension.getConfigSize());
        }
        byteBuffer.put((byte)(lineNo - 1));

        if (actionExtension != null) {
            int configSize = actionExtension.getConfigSize();
            Map<String, DeviceConfigProperty> configProperties = actionExtension.getConfigProperties();
            Map<String, String> propertyValues = performDeviceActionRQ.getPropertyValues();
            if (configSize > 0 && configSize <= MAX_CHILD_CONFIG_SIZE) {
                byte[] config = new byte[configSize];
                if (configProperties != null)
                    childDeviceConfigCodec.encodeDeviceProfileConfig(config, configProperties,
                            propertyValues);
                byteBuffer.put(config);
            }
        }

        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3PerformScenarioActionRQ performScenarioActionRQ) {
        int globalScenarioNo = performScenarioActionRQ.getGlobalScenarioNo();
        ScenarioManageAction manageAction = performScenarioActionRQ.getManageAction();

        ByteBuffer byteBuffer = startEncodeSetGetParameterRQ(performScenarioActionRQ, null);

        byteBuffer.put((byte)SCENARIO_MANAGE_COMMAND_CODE);
        byteBuffer.put((byte)(globalScenarioNo >> 8));
        byteBuffer.put((byte)(globalScenarioNo));
        byteBuffer.put((byte)0);
        switch (manageAction) {
            default:
            case STOP:
                byteBuffer.put((byte)0);
                break;
            case START:
                byteBuffer.put((byte)1);
                break;
            case UNBLOCK:
                byteBuffer.put((byte)2);
                break;
            case BLOCK:
                byteBuffer.put((byte)3);
                break;
        }
        byteBuffer.put((byte)0);
        byteBuffer.put((byte)0);
        byteBuffer.put((byte)0);

        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetWaitingAccessKeyStatusRQ waitingAccessKeyStatusRQ) {
        ByteBuffer byteBuffer = startEncodeSetGetParameterRQ(waitingAccessKeyStatusRQ, null);
        byteBuffer.put((byte) (waitingAccessKeyStatusRQ.isWaiting() ? 1 : 0));
        byteBuffer.put((byte) (waitingAccessKeyStatusRQ.getAddress() & 0xFF));
        byteBuffer.put((byte) ((waitingAccessKeyStatusRQ.getLineNo()) & 0xFF));
        return byteBuffer;
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3GetAddressListRQ getAddressListRQ) {
        return startEncodeSetGetParameterRQ(getAddressListRQ, null);
    }

    @Override
    public ByteBuffer encode(Rubezh2OP3SetAddressListRQ setAddressListRQ) {
        List<Integer> addressList = setAddressListRQ.getAddressList();
        // NOTE: в документации указан размер контекста 32 байта, но работает так
        byte[] context = new byte[2 * ADDRESS_LIST_SIZE];
        int index = 0;
        for (Integer address : addressList) {
            if (index < ADDRESS_LIST_SIZE)
                context[index++] = (byte)(address & 0xFF);
            else
                break;
        }
        return startEncodeSetGetParameterRQ(setAddressListRQ, context);
    }

    private int parseInteger(ByteBuffer byteBuffer, int byteCount) {
        assert byteCount <= 4;
        int result = 0;
        for (int i = 0; i < byteCount; i++) {
            result = (result << 8) | (byteBuffer.get() & 0xFF);
        }
        return result;
    }

    private long parseLong(ByteBuffer byteBuffer, int byteCount) {
        assert byteCount <= 8;
        long result = 0;
        for (int i = 0; i < byteCount; i++) {
            result = (result << 8) | (byteBuffer.get() & 0xFF);
        }
        return result;
    }

    protected List<String> decodeDeviceProfileStates(byte[] stateBytes, DeviceProfile deviceProfile, BitSet regionStateBits) {
        List<String> activeStates = new ArrayList<String>();

        List<DecodableState> decodableStates = deviceProfile.getCustomBitStates();
        for (DecodableState state : decodableStates) {
            if ((state.getBitNo() / 8) >= stateBytes.length) {
                logger.error(
                        "State decode bit number is out of range. State id: {}, bitNo: {}, number of state bytes: {}",
                        state.getId(), state.getBitNo(), stateBytes.length);
                continue;
            }

            if (state.decodeBit(stateBytes, regionStateBits))
                activeStates.add(state.getId());
        }

        decodableStates = deviceProfile.getCustomMaskedStates();
        for (DecodableState state : decodableStates) {
            if (state.getByteNo() >= stateBytes.length) {
                logger.error(
                        "State decode byte number is out of range. State id: {}, byteNo: {}, number of state bytes: {}",
                        state.getId(), state.getByteNo(), stateBytes.length);
                continue;
            }

            if (state.decodeMask(stateBytes, regionStateBits))
                activeStates.add(state.getId());
        }

        return activeStates;
    }

    protected Map<String, String> decodeMonitorableValue(byte[] data, boolean isEvent, DeviceProfile deviceProfile) {
        Map<String, String> result = new HashMap<>();
        for (MonitorableValue value: deviceProfile.getCustomMonitorableValues()) {
            result.put(value.getProfile().getId(), value.decode(data, isEvent));
        }
        return result;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetEventCountersAndStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                /*
                 * Судя по всему, в устройстве эти счетчики беззнаковые по 4
                 * байта, поэтому читаем в long
                 */
                Map<String, Long> counters = new HashMap<>();
                List<DeviceProfile.LogTypeItem> logTypes = rubezh2op3DeviceProfile.getLogTypes();
                for (DeviceProfile.LogTypeItem logType: logTypes) {
                    long counter = parseLong(byteBuffer, EVENT_COUNTER_SIZE);
                    if (counter == 0xFFFFFFFFl)
                        counter = 0l;
                    else
                        counter++;
                    counters.put(logType.getId(), counter);
                }

                /* Читаем байты общего состояния */
                byte[] stateBytes = new byte[STATE_BYTES_BLOCK_COUNT * STATE_BYTES_BLOCK_SIZE];
                for (int blockIdx = 0; blockIdx < STATE_BYTES_BLOCK_COUNT; blockIdx++) {
                    /* Байты в пределах блока нужны в обратном порядке */
                    for (int blockByteIdx = STATE_BYTES_BLOCK_SIZE - 1; blockByteIdx >= 0; blockByteIdx--)
                        stateBytes[STATE_BYTES_BLOCK_SIZE * blockIdx + blockByteIdx] = byteBuffer.get();
                }
                List<String> activeStates = decodeDeviceProfileStates(stateBytes,
                        rubezh2op3DeviceProfile, new BitSet()); // прибор не относится к зонам

                Rubezh2OP3GetEventCountersAndStatesRS getEventCountersAndStateBytesRS = new Rubezh2OP3GetEventCountersAndStatesRS();
                getEventCountersAndStateBytesRS.setLogCounters(counters);
                getEventCountersAndStateBytesRS.setDeviceStates(activeStates);
                response = getEventCountersAndStateBytesRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetMaxEventCountersRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                Map<String, Long> counters = new HashMap<>();
                List<DeviceProfile.LogTypeItem> logTypes = rubezh2op3DeviceProfile.getLogTypes();
                for (DeviceProfile.LogTypeItem logType: logTypes) {
                    long maxEventsToRead = parseLong(byteBuffer, MAX_EVENT_COUNTER_SIZE);
                    counters.put(logType.getId(), maxEventsToRead);
                }
                Rubezh2OP3GetMaxEventCountersRS getMaxEventCountersRS = new Rubezh2OP3GetMaxEventCountersRS();
                getMaxEventCountersRS.setLogCounters(counters);
                response = getMaxEventCountersRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetControlDeviceInfoRS(ByteBuffer byteBuffer) throws BufferUnderflowException {

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
            Rubezh2OP3CommonRS response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
            return response;
        }
        if (functionType == null || functionType != Rubezh2OP3FunctionType.GET_PARAMETER) {
            logger.error("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            return null;
        }
        if (byteBuffer.remaining() < RUBEZH2OP3_INFO_SIZE) {
            byte[] data = new byte[byteBuffer.remaining()];
            byteBuffer.get(data);
            logger.error("Empty or incomplete response data. Length: {}, data: 0x{}", data.length,
                    Hex.encodeHexString(data));
            return null;
        }
        /*
         * TODO: Парсить все поля (документ Полное распределение флэш памяти)
         */
        byte[] guidBytes = new byte[GUID_SIZE];
        byteBuffer.position(byteBuffer.position() + GUID_OFFSET);
        byteBuffer.get(guidBytes);
        ByteBuffer guidBuffer = ByteBuffer.wrap(guidBytes);
        long guidHigh = guidBuffer.getLong();
        long guidLow = guidBuffer.getLong();
        UUID guid = new UUID(guidHigh, guidLow);

        byte[] serialBytes = new byte[SERIAL_SIZE];
        byteBuffer.get(serialBytes);
        String serial = "wrong format";
        StringBuilder serialSB = new StringBuilder(SERIAL_SIZE);
        for (byte digit : serialBytes)
            if (digit >= 0x30 && digit <= 0x39)
                serialSB.append((char) digit);
            else
                break;
        if (serialSB.length() == SERIAL_SIZE)
            serial = serialSB.toString();
        else
            logger.warn("Rubezh2OP3 serial number parse failed: {}", serial);

        Rubezh2OP3GetControlDeviceInfoRS getControlDeviceInfoRS = new Rubezh2OP3GetControlDeviceInfoRS();
        Rubezh2OP3DeviceInfo deviceConfiguration = new Rubezh2OP3DeviceInfo();
        deviceConfiguration.setGuid(guid.toString());
        deviceConfiguration.setSerial(serial);
        getControlDeviceInfoRS.setDeviceInfo(deviceConfiguration);
        return getControlDeviceInfoRS;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDataBlockRS(ByteBuffer byteBuffer, int length) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                byte[] data = new byte[length];
                byteBuffer.get(data);
                response = new Rubezh2OP3GetDataBlockRS(data);
                logger.trace("Read data block {} bytes: {}", length, Hex.encodeHexString(data));
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    /**
     * Проверка CRC переданного массива данных. Сравнивает CRC от всех байт
     * данных, кроме последнего, с последним байтом данных
     */
    protected boolean checkCRC(byte[] data) {
        byte crc = 0x00;
        for (int i = 0; i < data.length - 1; i++) {
            crc ^= data[i];
        }
        return crc == data[data.length - 1];
    }

    int getBitRange(long data, int from, int to) {
        return (int) ((data >> from) & (0x01 << (to - from + 1)) - 1);
    }

    Date decodeDateTime(byte[] raw) {
        long val = 0x00;
        for (int i = 0; i < 4; i++)
            val = (val << 8) | (raw[3 - i] & 0xFF);
        int year = getBitRange(val, 9, 14) + 2000;
        int month = Integer.min(getBitRange(val, 5, 8), 12);
        int day = Integer.min(getBitRange(val, 0, 4), 31);
        int hour = Integer.min(getBitRange(val, 15, 19), 23);
        int minute = Integer.min(getBitRange(val, 20, 25), 59);
        int second = Integer.min(getBitRange(val, 26, 31), 59);

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        cal.set(year, month - 1, day, hour, minute, second);
        Date result = cal.getTime();
        result.setTime((result.getTime() / 1000) * 1000); // Обнуление миллисекунд
        return result;
    }

    byte[] encodeDateTime(Date dateTime) {
        // обновление временной зоны необходимо для корректного кодирования даты/времени для прибора
        updateTimeZone();

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateTime);

        // Текущее время - 4 байта
        // Day - 5(0:4), Month - 4(5:8), Year - 6(9:14), Hours - 5(15:19), Min - 6(20:25), Sec - 6(26-31)
        long val = 0;
        val = (val << 6) | (~(~0 << 6) & cal.get(Calendar.SECOND));
        val = (val << 6) | (~(~0 << 6) & cal.get(Calendar.MINUTE));
        val = (val << 5) | (~(~0 << 5) & cal.get(Calendar.HOUR_OF_DAY));
        val = (val << 6) | (~(~0 << 6) & Integer.max(cal.get(Calendar.YEAR), 2000) - 2000);
        val = (val << 4) | (~(~0 << 4) & (cal.get(Calendar.MONTH) + 1));
        val = (val << 5) | (~(~0 << 5) & cal.get(Calendar.DAY_OF_MONTH));

        return new byte[] {
            (byte) (val & 0xFF),
            (byte) ((val >> 8) & 0xFF),
            (byte) ((val >> 16) & 0xFF),
            (byte) ((val >> 24) & 0xFF)
        };
    }

    private void updateTimeZone() {
        // чтобы JVM обновила временную зону, необходимо сбить эти параметры
        TimeZone.setDefault(null);
        System.clearProperty("user.timezone");
    }

    List<String> decodeEventStates(byte[] raw, DecodableEventStateSet decodableEventStateSet, BitSet regionBitSet) {
        List<String> result = new ArrayList<>();
        int byteNo = decodableEventStateSet.getByteNo();
        for (BitDecodableItem bitDecodableItem : decodableEventStateSet.getBitDecodableStates()) {
            if (bitDecodableItem.decode(raw, byteNo, regionBitSet)) {
                result.add(bitDecodableItem.getState().getId());
            }
        }
        for (MaskedDecodableItem maskedDecodableItem : decodableEventStateSet.getMaskDecodableStates()) {
            if (maskedDecodableItem.decode(raw, byteNo, regionBitSet)) {
                result.add(maskedDecodableItem.getState().getId());
            }
        }
        return result;
    }

    protected DeviceProfile findDeviceProfileByDeviceType(int deviceType) {
        /*
         * Сначала ищем по виртуальному типу устройства, если не находим - по
         * физическому типу
         */
        DeviceProfile result = null;

        for (String deviceProfileName : supportedDeviceProfiles.keySet()) {
            DeviceProfile deviceProfile = supportedDeviceProfiles.get(deviceProfileName);
            if (deviceProfile.getRsr3VirtualDeviceType() == deviceType) {
                result = deviceProfile;
                break;
            }
        }
        if (result == null) {
            for (String deviceProfileName : supportedDeviceProfiles.keySet()) {
                DeviceProfile deviceProfile = supportedDeviceProfiles.get(deviceProfileName);
                if (deviceProfile.getRsr3RawDeviceType() == deviceType) {
                    result = deviceProfile;
                    break;
                }
            }
        }
        return result;
    }

    List<String> getControlDeviceStates(byte raw[], EventType eventType) {
        List<String> generalDecodableStateSetIds = eventType.getGeneralDecodableStateSetIds();
        assert generalDecodableStateSetIds != null && !generalDecodableStateSetIds.isEmpty();

        List<String> stateIds = new ArrayList<>();
        BitSet emptyRegionBits = new BitSet();
        List<DecodableEventStateSet> decodableEventStateSets = driverProfile
                .getDecodableEventStateSets();
        for (DecodableEventStateSet decodableEventStateSet : decodableEventStateSets)
            if (generalDecodableStateSetIds.contains(decodableEventStateSet.getId()))
                stateIds.addAll(decodeEventStates(raw, decodableEventStateSet, emptyRegionBits));

        return stateIds;
    }

    EventDeviceInfo getEventDeviceInfo(byte raw[], EventType eventType, BitSet eventRegionStatus) {
        EventDeviceInfo deviceInfo = null;
        List<DecodableEventStateSet> decodableEventStateSets = driverProfile
                .getDecodableEventStateSets();

        int deviceType = raw[7] & 0xFF;
        DeviceProfile currentDeviceProfile = findDeviceProfileByDeviceType(deviceType);

        if (currentDeviceProfile != null) {
            deviceInfo = new EventDeviceInfo();
            deviceInfo.setProfileId(currentDeviceProfile.getId());
            if (currentDeviceProfile.getLineAddressSubstitution() != null)
                deviceInfo.setAddress(currentDeviceProfile.getLineAddressSubstitution());
            else
                deviceInfo.setAddress(raw[8] & 0xFF);
            if (currentDeviceProfile.getLineNumberSubstitution() != null)
                deviceInfo.setLineNo(currentDeviceProfile.getLineNumberSubstitution());
            else
                deviceInfo.setLineNo((raw[17] & 0xFF) + 1);
            deviceInfo.setControlDeviceAddress(raw[6] & 0xFF);
            if (deviceInfo.getControlDeviceAddress() == 0)
                deviceInfo.setDeviceLocal(true);
            else
                deviceInfo.setDeviceLocal(false);

            String currentDeviceDecodableStateSetId = eventType.getDeviceDecodableStateSetIds()
                    .get(currentDeviceProfile.getId());
            List<String> generalDecodableStateSetIds = eventType.getGeneralDecodableStateSetIds();
            Set<String> setOfStateSetIds = new HashSet<>();
            if (currentDeviceDecodableStateSetId != null){
                setOfStateSetIds.add(currentDeviceDecodableStateSetId);
            }
            if (generalDecodableStateSetIds != null){
                setOfStateSetIds.addAll(generalDecodableStateSetIds);
            }
            if (!setOfStateSetIds.isEmpty()){
                for (String stateSetId : setOfStateSetIds) {
                    DecodableEventStateSet currentDecodableEventStateSet = null;
                    for (DecodableEventStateSet decodableEventStateSet : decodableEventStateSets) {
                        if (decodableEventStateSet.getId().equals(stateSetId)) {
                            currentDecodableEventStateSet = decodableEventStateSet;
                            break;
                        }
                    }
                    if (currentDecodableEventStateSet != null) {
                        if (deviceInfo.getActiveStateIds() == null) {
                            deviceInfo.setActiveStateIds(decodeEventStates(raw,
                                    currentDecodableEventStateSet, eventRegionStatus));
                        } else {
                            deviceInfo.addActiveStateIds(decodeEventStates(raw,
                                    currentDecodableEventStateSet, eventRegionStatus));
                        }
                    }
                }
            }
            deviceInfo.setValues(decodeMonitorableValue(raw, true, currentDeviceProfile));
            logger.trace("device address: {}, line no: {}. control device address: {}",
                    deviceInfo.getAddress(), deviceInfo.getLineNo(), deviceInfo.getControlDeviceAddress());
        } else {
            logger.warn("Unknown device type {} on event {} (raw event data: {})", deviceType, eventType.getId(),
                    Hex.encodeHexString(raw));
        }

        return deviceInfo;
    }

    EventRegionInfo getEventRegionInfo(byte[] raw, EventType eventType, BitSet eventRegionStatus) {
        EventRegionInfo regionInfo = new EventRegionInfo();
        List<DecodableEventStateSet> decodableEventStateSets = driverProfile
                .getDecodableEventStateSets();

        regionInfo.setGlobalNo((raw[19] & 0xFF) | ((raw[20] & 0xFF) << 8));
        regionInfo.setLocalNo((raw[10] & 0xFF) | ((raw[11] & 0xFF) << 8));

        List<String> activeStates = new ArrayList<>();
        List<String> regionDecodableStateSetIds = eventType.getRegionDecodableStateSetIds();
        List<String> generalDecodableStateSetIds = eventType.getGeneralDecodableStateSetIds();
        if (generalDecodableStateSetIds != null) {
            regionDecodableStateSetIds.addAll(generalDecodableStateSetIds);
        }
        for (String regionStateSetId : eventType.getRegionDecodableStateSetIds()) {
            DecodableEventStateSet currentDecodableEventStateSet = null;
            for (DecodableEventStateSet decodableEventStateSet : decodableEventStateSets) {
                if (decodableEventStateSet.getId().equals(regionStateSetId)) {
                    currentDecodableEventStateSet = decodableEventStateSet;
                    break;
                }
            }
            if (currentDecodableEventStateSet != null) {
                activeStates.addAll(decodeEventStates(raw, currentDecodableEventStateSet, eventRegionStatus));
            }
        }
        regionInfo.setActiveStateIds(activeStates);
        logger.trace("region global no: {}, local no: {}", regionInfo.getGlobalNo(), regionInfo.getLocalNo());

        return regionInfo;
    }

    EventVirtualStateInfo getEventVirtualStateInfo(byte[] raw) {
        EventVirtualStateInfo virtualStateInfo = new EventVirtualStateInfo();

        virtualStateInfo.setGlobalNo(((raw[10] & 0xFF) << 8) | (raw[11] & 0xFF));
        logger.trace("virtual state global no: {}", virtualStateInfo.getGlobalNo());

        return virtualStateInfo;
    }

    EventScenarioInfo getEventScenarioInfo(byte[] raw, EventType eventType, BitSet eventRegionStatus) {
        EventScenarioInfo scenarioInfo = new EventScenarioInfo();
        List<DecodableEventStateSet> decodableEventStateSets = driverProfile
                .getDecodableEventStateSets();

        List<String> scenarioStateSetIds = eventType.getGeneralDecodableStateSetIds();
        if (scenarioStateSetIds != null){
            for (String scenarioStateSetId : scenarioStateSetIds) {
                DecodableEventStateSet currentDecodableEventStateSet = null;
                for (DecodableEventStateSet decodableEventStateSet : decodableEventStateSets) {
                    if (decodableEventStateSet.getId().equals(scenarioStateSetId)) {
                        currentDecodableEventStateSet = decodableEventStateSet;
                        break;
                    }
                }
                if (currentDecodableEventStateSet != null) {
                    scenarioInfo.setActiveStateIds(decodeEventStates(raw, currentDecodableEventStateSet,
                            eventRegionStatus));
                }
            }
        }
        scenarioInfo.setGlobalNo(((raw[10] & 0xFF) << 8) | (raw[11] & 0xFF));
        logger.trace("scenario global no: {}", scenarioInfo.getGlobalNo());

        return scenarioInfo;
    }

    private EventLineInfo getEventLineInfo(byte[] raw) {
        EventLineInfo lineInfo = new EventLineInfo();

        int lineNo;
        int rawLineNo = raw[6] & 0xFF;
        switch (rawLineNo) {
        case 0:
            lineNo = 1;
            break;
        case 1:
            lineNo = 2;
            break;
        default:
            lineNo = -1;
            break;
        }
        lineInfo.setLineNo(lineNo);
        logger.trace("line no: {}", lineInfo.getLineNo());

        return lineInfo;
    }

    private EventEmployeeInfo getEventEmployeeInfo(byte[] raw) {
        EventEmployeeInfo employeeInfo = new EventEmployeeInfo();
        employeeInfo.setEmployeeId((raw[21] & 0xFF) | (raw[22] & 0xFF << 8));
        return employeeInfo;
    }

    private void getEventAccesskeyValue(byte[] raw, ControlDeviceEvent eventInfo) {
        int length = raw[ACCESS_KEY_VALUE_SIZE_EVENT_OFFSET];
        if (length >= AccessKey.MIN_KEY_VALUE_LENGTH / 2 && length <= AccessKey.MAX_KEY_VALUE_LENGTH / 2) {
            eventInfo.setAccessKeyValue(Hex.encodeHexString(
                    Arrays.copyOfRange(raw, ACCESS_KEY_VALUE_EVENT_OFFSET, ACCESS_KEY_VALUE_EVENT_OFFSET + length)));
        } else {
            logger.warn("Length of access key value is out of range. Event dump: {}",
                    Hex.encodeHexString(raw));
        }
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetLogEventRS(ByteBuffer byteBuffer,
            Function<Integer, String> getRegionIdByLocalNo) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                byte[] raw = new byte[32];
                byteBuffer.get(raw);

                if (checkCRC(raw)) {
                    Rubezh2OP3GetLogEventRS getLogEventRS = new Rubezh2OP3GetLogEventRS();
                    ControlDeviceEvent logEvent = new ControlDeviceEvent();
                    getLogEventRS.setLogEvent(logEvent);
                    logEvent.setOccurredDateTime(decodeDateTime(Arrays.copyOfRange(raw, 1, 5)));
                    logEvent.setReceivedDateTime(new Date());

                    List<EventProfile> eventProfiles = driverProfile.getEventProfiles();
                    int eventCode = raw[0] & 0xFF;
                    EventProfile currentEventProfile = null;
                    EventType currentEventType = null;
                    for (EventProfile eventProfile : eventProfiles) {
                        EventType mainEventType = eventProfile.getMainEventType();
                        if (mainEventType != null && mainEventType.getCode() != null
                                && mainEventType.getCode() == eventCode) {
                            currentEventProfile = eventProfile;
                            currentEventType = mainEventType;
                            break;
                        }
                    }
                    if (currentEventProfile != null) {
                        int subeventCode = (currentEventProfile.isHavingDeviceTypes())
                                ? raw[9] & 0xFF
                                : raw[5] & 0xFF;
                        for (EventType subeventType : currentEventProfile.getEventSubTypes()) {
                            if (subeventType != null && subeventType.getCode() == subeventCode) {
                                if (!currentEventProfile.isHavingDeviceTypes()
                                        || subeventType.getDeviceTypes().contains(raw[7] & 0xFF)) {
                                    currentEventType = subeventType;
                                    break;
                                }
                            }
                        }
                        logger.trace("currentEventType: {}, Raw data: {}",
                                currentEventType.getId(), Hex.encodeHexString(raw));

                        BitSet eventRegionStatus = new BitSet();
                        if (currentEventType.isHavingControlDeviceStates()
                                && currentEventType.getGeneralDecodableStateSetIds() != null
                                && !currentEventType.getGeneralDecodableStateSetIds().isEmpty()) {
                            logEvent.getControlDeviceInfo()
                                    .setActiveStateIds(getControlDeviceStates(raw, currentEventType));
                        }
                        if (currentEventType.isHavingRegionStateInfo()) {
                            eventRegionStatus = getEventRegionBitInfo(raw);
                            getLogEventRS.setRegionStateBits(eventRegionStatus);
                            getLogEventRS.setRegionId(decodeEventRegionId(raw, getRegionIdByLocalNo));
                        }
                        if (currentEventType.isHavingDeviceInfo()) {
                            logEvent.setDeviceInfo(getEventDeviceInfo(raw, currentEventType, eventRegionStatus));
                        }
                        if (currentEventType.isHavingRegionInfo()) {
                            logEvent.setRegionInfo(getEventRegionInfo(raw, currentEventType, eventRegionStatus));
                        }
                        if (currentEventType.isHavingVirtualStateInfo()) {
                            logEvent.setVirtualStateInfo(getEventVirtualStateInfo(raw));
                        }
                        if (currentEventType.isHavingScenarioInfo()) {
                            logEvent.setScenarioInfo(getEventScenarioInfo(raw, currentEventType, eventRegionStatus));
                        }
                        if (currentEventType.isHavingLineInfo()) {
                            logEvent.setLineInfo(getEventLineInfo(raw));
                        }
                        if (currentEventType.isHavingEmployeeInfo()){
                            logEvent.setEmployeeInfo(getEventEmployeeInfo(raw));
                        }
                        if (currentEventType.isHavingAccessKeyValue()) {
                            getEventAccesskeyValue(raw, logEvent);
                        }
                    } else {
                        logger.warn("Unknown event code: {}", eventCode & 0xFF);
                    }
                    if (currentEventType != null) {
                        logEvent.setTypeId(currentEventType.getId());
                    }
                    response = getLogEventRS;
                } else {
                    logger.warn("Event CRC check failed! Length: {}, data: 0x{}",
                            raw.length, Hex.encodeHexString(raw));
                    // отдаём "пустое" событие
                    response = new Rubezh2OP3GetLogEventRS();
                }
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    private BitSet getEventRegionBitInfo(byte[] raw) {
        byte[] result = new byte[REGION_STATE_BYTE_NOS.length];
        for (int i = 0; i < REGION_STATE_BYTE_NOS.length; i++) {
            int byteNo = REGION_STATE_BYTE_NOS[i];
            if (byteNo >= 0 && raw.length > byteNo) result[i] = raw[byteNo];
        }
        return BitSet.valueOf(result);
    }

    private String decodeEventRegionId(byte[] raw, Function<Integer, String> getRegionIdByLocalNo) {
        int regionLocalNo = ((raw[10] & 0xFF) << 8) | (raw[11] & 0xFF);
        return getRegionIdByLocalNo.apply(regionLocalNo);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetRegionStatesRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                int parameterCode = byteBuffer.get() & 0xFF;
                Rubezh2OP3ParameterType parameterType = Rubezh2OP3ParameterType.fromInteger(parameterCode);
                if (parameterType != null && parameterType == Rubezh2OP3ParameterType.REGION_STATES) {
                    Rubezh2OP3GetRegionStatesRS getRegionStatesRS = new Rubezh2OP3GetRegionStatesRS();

                    if (byteBuffer.remaining() >= 6) {
                        int regionNo = (byteBuffer.get() & 0xFF) << 8;
                        regionNo |= (byteBuffer.get() & 0xFF);

                        byte[] stateBytes = new byte[4];
                        stateBytes[3] = byteBuffer.get();
                        stateBytes[2] = byteBuffer.get();
                        stateBytes[1] = byteBuffer.get();
                        stateBytes[0] = byteBuffer.get();
                        BitSet regionStateBits = BitSet.valueOf(stateBytes);

                        getRegionStatesRS.setRegionNo(regionNo);
                        getRegionStatesRS.setRegionStateBits(regionStateBits);
                    } else {
                        getRegionStatesRS.setSuccess(false);
                    }

                    response = getRegionStatesRS;
                } else {
                    logger.warn("Wrong parameter code in response: {}, whole packet: {}", parameterCode,
                            getHex(byteBuffer));
                }
            } else {
                logger.warn("Wrong function code in response: {}, whole packet: {}", functionCode & FUNCTION_MASK,
                        getHex(byteBuffer));
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceStatesRS(ByteBuffer byteBuffer, BitSet regionStateBits) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                int parameterCode = byteBuffer.get() & 0xFF;
                Rubezh2OP3ParameterType parameterType = Rubezh2OP3ParameterType.fromInteger(parameterCode);
                if (parameterType != null && parameterType == Rubezh2OP3ParameterType.CHILD_DEVICE_STATES) {
                    Rubezh2OP3GetChildDeviceStatesRS getChildDeviceStatesRS = new Rubezh2OP3GetChildDeviceStatesRS();
                    int lineNo = byteBuffer.get() & 0xFF;
                    if (byteBuffer.remaining() > 0) {
                        int address = byteBuffer.get() & 0xFF;
                        int deviceType = byteBuffer.get() & 0xFF;
                        int statesSize = byteBuffer.get() & 0xFF;
                        byte[] stateBytes = new byte[statesSize - 1];
                        byteBuffer.get(stateBytes);
                        /* Потому что так привыкли... */
                        if (stateBytes.length >= 2) {
                            byte tmp = stateBytes[0];
                            stateBytes[0] = stateBytes[1];
                            stateBytes[1] = tmp;
                        }
                        int qos = byteBuffer.get() & 0xFF;

                        ChildDeviceStatesInfo childDeviceStatesInfo = new ChildDeviceStatesInfo();
                        childDeviceStatesInfo.setLineNo(lineNo);
                        childDeviceStatesInfo.setAddress(address);
                        childDeviceStatesInfo.setQos(qos);
                        DeviceProfile deviceProfile = findDeviceProfileByDeviceType(deviceType);
                        if (deviceProfile != null) {
                            childDeviceStatesInfo.setDeviceProfileId(deviceProfile.getId());
                            List<String> activeStates = decodeDeviceProfileStates(stateBytes, deviceProfile, regionStateBits);
                            Map<String, String> values = decodeMonitorableValue(stateBytes, false, deviceProfile);
                            childDeviceStatesInfo.setActiveStates(activeStates);
                            childDeviceStatesInfo.setActiveValues(values);
                        }
                        getChildDeviceStatesRS.setChildDeviceStatesInfo(childDeviceStatesInfo);
                    }
                    response = getChildDeviceStatesRS;
                } else {
                    logger.warn("Wrong parameter code in response: {}, whole packet: {}", parameterCode,
                            getHex(byteBuffer));
                }
            } else {
                logger.warn("Wrong function code in response: {}, whole packet: {}", functionCode & FUNCTION_MASK,
                        getHex(byteBuffer));
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                Rubezh2OP3GetChildDeviceConfigRS getChildDeviceConfigRS = new Rubezh2OP3GetChildDeviceConfigRS();
                int address = byteBuffer.get() & 0xFF;
                int deviceType = byteBuffer.get() & 0xFF;
                int configSize = byteBuffer.get() & 0xFF;
                int serial = parseInteger(byteBuffer, CHILD_SERIAL_SIZE);
                int firmwareVersionMinor = byteBuffer.get() & 0xFF;
                int service = byteBuffer.get() & 0xFF; // сервисный байт (до конца не изучен)
                int firmwareVersionMajor = ((service & 0x06) >> 1) + 1; // 2-битовое поле, ранее - "Версия протокола"
                byte[] properties = new byte[configSize - CHILD_SERIAL_SIZE - 2];
                byteBuffer.get(properties);

                ChildDeviceConfigInfo childDeviceConfigInfo = new ChildDeviceConfigInfo();
                childDeviceConfigInfo.setAddress(address);
                childDeviceConfigInfo.setSerial(Integer.toString(serial));
                childDeviceConfigInfo.setFirmwareVersion(
                        Integer.toString(firmwareVersionMajor) + "." +
                        Integer.toString(firmwareVersionMinor));
                DeviceProfile deviceProfile = findDeviceProfileByDeviceType(deviceType);
                if (deviceProfile != null) {
                    childDeviceConfigInfo.setDeviceProfileId(deviceProfile.getId());
                    Map<String, String> propertyValues = childDeviceConfigCodec
                            .decodeDeviceProfileConfig(properties,
                                    deviceProfile.getConfigProperties());
                    childDeviceConfigInfo.setPropertyValues(propertyValues);
                }
                getChildDeviceConfigRS.setChildDeviceConfigInfo(childDeviceConfigInfo);
                response = getChildDeviceConfigRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }


    private <T extends Rubezh2OP3CommonRS> Rubezh2OP3CommonRS decodeEmptySetParameterRS(ByteBuffer byteBuffer,
            Class<T> aClass) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        try {
            byte functionCode = byteBuffer.get();
            Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
            if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
                if (functionType != null && functionType == Rubezh2OP3FunctionType.SET_PARAMETER) {
                    response = aClass.newInstance();
                } else {
                    logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
                }
            } else {
                response = new Rubezh2OP3CommonRS(functionType);
                response.setSuccess(false);
                response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
            }
        } catch (InstantiationException e) {
            logger.error("InstantiationException on decoding: {}", e.getMessage());
        } catch (IllegalAccessException e) {
            logger.error("IllegalAccessException on decoding: {}", e.getMessage());
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetChildDeviceConfigRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetChildDeviceConfigRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetResetStateDeviceRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetStateDeviceResetRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeInvokeUpdateSoftwareModeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.UPDATE_SOFTWARE) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.ACKNOWLEDGE) {
                    Rubezh2OP3InvokeUpdateSoftwareModeRS invokeUpdateSoftwareModeRS = new Rubezh2OP3InvokeUpdateSoftwareModeRS();
                    byte waitingTime = byteBuffer.get();
                    invokeUpdateSoftwareModeRS.setWaitingTimeMs(1 << (waitingTime));
                    response = invokeUpdateSoftwareModeRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3InvokeUpdateSoftwareModeRS invokeUpdateSoftwareModeRS = new Rubezh2OP3InvokeUpdateSoftwareModeRS();
                invokeUpdateSoftwareModeRS.setWaitingTimeMs(0);
                response = invokeUpdateSoftwareModeRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeCheckDeviceReadyRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.CHECK_READY) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.SLAVE_DEVICE_BUSY) {
                    Rubezh2OP3CheckDeviceReadyRS checkDeviceReadyRS = new Rubezh2OP3CheckDeviceReadyRS();
                    checkDeviceReadyRS.setReady(false);
                    response = checkDeviceReadyRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3CheckDeviceReadyRS checkDeviceReadyRS = new Rubezh2OP3CheckDeviceReadyRS();
                checkDeviceReadyRS.setReady(true);
                response = checkDeviceReadyRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeResetRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.RESET) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.ACKNOWLEDGE) {
                    Rubezh2OP3ResetRS resetRS = new Rubezh2OP3ResetRS();
                    byte waitingTime = byteBuffer.get();
                    resetRS.setWaitingTimeMs(1 << (waitingTime));
                    response = resetRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3ResetRS resetRS = new Rubezh2OP3ResetRS();
                resetRS.setWaitingTimeMs(0);
                response = resetRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeEraseSectorsRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.ERASE_SECTORS) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.ACKNOWLEDGE) {
                    Rubezh2OP3EraseSectorsRS eraseSectorsRS = new Rubezh2OP3EraseSectorsRS();
                    byte waitingTime = byteBuffer.get();
                    eraseSectorsRS.setWaitingTimeMs(1 << (waitingTime));
                    response = eraseSectorsRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3EraseSectorsRS eraseSectorsRS = new Rubezh2OP3EraseSectorsRS();
                eraseSectorsRS.setWaitingTimeMs(0);
                response = eraseSectorsRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeWriteMemoryRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.WRITE_MEMORY) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.ACKNOWLEDGE) {
                    Rubezh2OP3WriteMemoryRS writeMemoryRS = new Rubezh2OP3WriteMemoryRS();
                    byte waitingTime = byteBuffer.get();
                    writeMemoryRS.setWaitingTimeMs(1 << (waitingTime));
                    response = writeMemoryRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3WriteMemoryRS writeMemoryRS = new Rubezh2OP3WriteMemoryRS();
                writeMemoryRS.setWaitingTimeMs(0);
                response = writeMemoryRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareErrorRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_UPDATE_SORFWARE_ERROR) {
                Rubezh2OP3GetUpdateSoftwareErrorRS getUpdateSoftwareErrorRS = new Rubezh2OP3GetUpdateSoftwareErrorRS();
                getUpdateSoftwareErrorRS.setError(Rubezh2OP3UpdateSoftwareErrorType.fromInteger(byteBuffer.get()));;
                response = getUpdateSoftwareErrorRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetUpdateSoftwareStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                Rubezh2OP3GetUpdateSoftwareStatusRS getUpdateSoftwareStatusRS = new Rubezh2OP3GetUpdateSoftwareStatusRS();
                int statusInt = parseInteger(byteBuffer, 2);
                getUpdateSoftwareStatusRS.setStatus(Rubezh2OP3UpdateSoftwareStatusType.fromInteger(statusInt));
                response = getUpdateSoftwareStatusRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                Rubezh2OP3GetFirmwareVersionRS getFirmwareVersionRS = new Rubezh2OP3GetFirmwareVersionRS();

                byte twoBCDDigits = byteBuffer.get();
                int fwVersionMajor = ((twoBCDDigits & 0xF0) >> 4) * 10 + (twoBCDDigits & 0x0F);
                twoBCDDigits = byteBuffer.get();
                int fwVersionMinor = ((twoBCDDigits & 0xF0) >> 4) * 10 + (twoBCDDigits & 0x0F);

                String fwVersion = Integer.toString(fwVersionMajor) + '.' + Integer.toString(fwVersionMinor);
                getFirmwareVersionRS.setFirmwareVersion(fwVersion);
                response = getFirmwareVersionRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDeviceUserPasswordRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetUserPasswordRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetDatabaseVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                Rubezh2OP3GetDatabaseVersionRS getDatabaseVersionRS = new Rubezh2OP3GetDatabaseVersionRS();

                byte dbVersionMajor = byteBuffer.get();
                byte dbVersionMinor = byteBuffer.get();

                String dbVersion = Integer.toString(dbVersionMajor) + '.' + Integer.toString(dbVersionMinor);
                getDatabaseVersionRS.setDatabaseVersion(dbVersion);
                response = getDatabaseVersionRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeGetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
            if (functionType != null && functionType == Rubezh2OP3FunctionType.GET_PARAMETER) {
                List<Integer> addressList = new ArrayList<>();
                for (int i = 0; i < ADDRESS_LIST_SIZE; i++) {
                    byte address = byteBuffer.get();
                    if (address == 0)
                        break;
                    addressList.add(Byte.toUnsignedInt(address));
                }
                // NOTE: следующие 32 байта - качество связи с каждым устройством из адресного листа
                Rubezh2OP3GetAddressListRS getAddressListRS = new Rubezh2OP3GetAddressListRS();
                getAddressListRS.setAddressList(addressList);
                response = getAddressListRS;
            } else {
                logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
        } else {
            response = new Rubezh2OP3CommonRS(functionType);
            response.setSuccess(false);
            response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetAddressListRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        Rubezh2OP3CommonRS response = null;

        byte functionCode = byteBuffer.get();
        Rubezh2OP3FunctionType functionType = Rubezh2OP3FunctionType.fromInteger(functionCode & FUNCTION_MASK);
        if (functionType != null && functionType == Rubezh2OP3FunctionType.SET_PARAMETER) {
            if ((functionCode & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(byteBuffer.get());
                if (exception == ExceptionType.ACKNOWLEDGE) {
                    Rubezh2OP3SetAddressListRS setAddressListRS = new Rubezh2OP3SetAddressListRS();
                    byte waitingTime = byteBuffer.get();
                    setAddressListRS.setWaitingTimeMs(1 << (waitingTime));
                    response = setAddressListRS;
                } else {
                    response = new Rubezh2OP3CommonRS(functionType);
                    response.setSuccess(false);
                    response.setExceptionType(exception);
                }
            } else {
                Rubezh2OP3SetAddressListRS setAddressListRS = new Rubezh2OP3SetAddressListRS();
                setAddressListRS.setWaitingTimeMs(0);
                response = setAddressListRS;
            }
        } else {
            logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
        }

        return response;
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetSecurityRegionStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetSecurityRegionStatusRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetDevicePollingStateRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetDevicePollingStateRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetCurrentTimeRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetCurrentTimeRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetFirmwareVersionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetFirmwareVersionRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformDeviceActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3PerformDeviceActionRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodePerformScenarioActionRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3PerformScenarioActionRS.class);
    }

    @Override
    public Rubezh2OP3CommonRS decodeSetWaitingAccessKeyStatusRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        return decodeEmptySetParameterRS(byteBuffer, Rubezh2OP3SetWaitingAccessKeyStatusRS.class);
    }

}
