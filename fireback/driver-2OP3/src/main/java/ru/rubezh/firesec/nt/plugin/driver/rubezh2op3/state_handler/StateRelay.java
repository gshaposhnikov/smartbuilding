package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareErrorType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceConfigInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceStatesInfo;

public class StateRelay implements StateHandler {
    Rubezh2OP3StateMachineContext stateMachineContext;

    State state = State.IDLE;
    private EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);

    private State setState(State state) {
        if (state != this.state) {
            stateMachineContext.getLogger().info("Rubezh2OP3 (address path: {}) state transition: {} -> {}",
                    stateMachineContext.getRubezh2op3Device().getAddressPath(),
                    this.state, state);
            this.state = state;
        }
        return this.state;
    }

    public StateRelay(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
        for (State state: State.values()) {
            if (state != State.NONE)
                handlersByState.put(state, State.getHandler(state).cloneBinded(stateMachineContext));
        }
    }

    @Override
    public State getCurrentState() {
        return state;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        return new StateRelay(stateMachineContext);
    }

    @Override
    public boolean isConnected() {
        return handlersByState.get(state).isConnected();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnectRequest(currentTimeMs));
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDisconnectRequest(currentTimeMs));
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnected(currentTimeMs));
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnectFailure(currentTimeMs));
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDisconnected(currentTimeMs));
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onReceiveTimeout(currentTimeMs));
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().traceEntry();
        return setState(handlersByState.get(state).onTimer(currentTimeMs, timerName));
    }

    @Override
    public State onIssueCloseTimer(long currentTimeMs, String issueId) {
        stateMachineContext.getLogger().traceEntry();
        return setState(handlersByState.get(state).onIssueCloseTimer(currentTimeMs, issueId));
    }

    @Override
    public State onControlDeviceStatesAndEventCountersReceived(long currentTimeMs, List<String> deviceStates,
            Map<String, Long> eventCounters) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onControlDeviceStatesAndEventCountersReceived(currentTimeMs,
                deviceStates, eventCounters));
    }

    @Override
    public State onLogEventReceived(long currentTimeMs, ControlDeviceEvent logEvent, BitSet regionStates, String regionId) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onLogEventReceived(currentTimeMs, logEvent, regionStates, regionId));
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onExceptionReceived(currentTimeMs, exception));
    }

    @Override
    public State onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceConfiguration) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onControlDeviceInfoReceived(currentTimeMs, deviceConfiguration));
    }

    @Override
    public State onDeviceMaxEventCountersReceived(long currentTimeMs, Map<String, Long> maxEventCounters) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceMaxEventCountersReceived(currentTimeMs, maxEventCounters));
    }

    @Override
    public State onDeviceMaxEventCountersReceiveFailed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceMaxEventCountersReceiveFailed(currentTimeMs));
    }

    @Override
    public State onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onRegionStatesReceived(currentTimeMs, regionId, regionStates));
    }

    @Override
    public State onChildDeviceStatesReceived(long currentTimeMs, ChildDeviceStatesInfo childDeviceStatesInfo) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onChildDeviceStatesReceived(currentTimeMs, childDeviceStatesInfo));
    }

    @Override
    public State onChildDeviceConfigReceived(long currentTimeMs, ChildDeviceConfigInfo childDeviceConfigInfo) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onChildDeviceConfigReceived(currentTimeMs, childDeviceConfigInfo));
    }

    @Override
    public State onChildDeviceConfigSet(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onChildDeviceConfigSet(currentTimeMs));
    }

    @Override
    public State onResetStateDeviceReceived(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onResetStateDeviceReceived(currentTimeMs));
    }

    @Override
    public State onSoftwareUpdateModeInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onSoftwareUpdateModeInvoked(currentTimeMs, waitingTimeMs));
    }

    @Override
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceReadyStateReceived(currentTimeMs, isReady));
    }

    @Override
    public State onResetInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onResetInvoked(currentTimeMs, waitingTimeMs));
    }

    @Override
    public State onEraseSectorsInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onEraseSectorsInvoked(currentTimeMs, waitingTimeMs));
    }

    @Override
    public State onWriteMemoryInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onWriteMemoryInvoked(currentTimeMs, waitingTimeMs));
    }

    @Override
    public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onUpdateSoftwareErrorReceived(currentTimeMs, error));
    }

    @Override
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onUpdateSoftwareStatusReceived(currentTimeMs, status));
    }

    @Override
    public State onSetSecurityRegionStatusReceived(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onSetSecurityRegionStatusReceived(currentTimeMs));
    }

    @Override
    public State onFirmwareVersionReceived(long currentTimeMs, String buildNumber) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onFirmwareVersionReceived(currentTimeMs, buildNumber));
    }

    @Override
    public State onDatabaseVersionReceived(long currentTimeMs, String databaseVersion) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDatabaseVersionReceived(currentTimeMs, databaseVersion));
    }

    @Override
    public State onSetDevicePollingStateReceived(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onSetDevicePollingStateReceived(currentTimeMs));
    }

    @Override
    public State onDeviceTimeSet(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceTimeSet(currentTimeMs));
    }

    @Override
    public State onDeviceUserPasswordSet(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceUserPasswordSet(currentTimeMs));
    }

    @Override
    public State onFirmwareVersionSet(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onFirmwareVersionSet(currentTimeMs));
    }

    @Override
    public State onDeviceActionPerformed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDeviceActionPerformed(currentTimeMs));
    }

    @Override
    public State onScenarioActionPerformed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onScenarioActionPerformed(currentTimeMs));
    }

    @Override
    public State onDataBlockReceived(long currentTimeMs, byte[] data) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDataBlockReceived(currentTimeMs, data));
    }

    @Override
    public State onWaitingAccessKeyStatusSet(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onWaitingAccessKeyStatusSet(currentTimeMs));
    }

    @Override
    public State onAddressListReceived(long currentTimeMs, List<Integer> addressList) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onAddressListReceived(currentTimeMs, addressList));
    }

    @Override
    public State onAddressListSent(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onAddressListSent(currentTimeMs, waitingTimeMs));
    }

}
