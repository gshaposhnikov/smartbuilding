package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3EraseSectorsRS extends Rubezh2OP3CommonRS {

    int waitingTimeMs;

    public Rubezh2OP3EraseSectorsRS() {
        super(Rubezh2OP3FunctionType.ERASE_SECTORS);
    }

    public int getWaitingTimeMs() {
        return waitingTimeMs;
    }

    public void setWaitingTimeMs(int waitingTimeMs) {
        this.waitingTimeMs = waitingTimeMs;
    }

}
