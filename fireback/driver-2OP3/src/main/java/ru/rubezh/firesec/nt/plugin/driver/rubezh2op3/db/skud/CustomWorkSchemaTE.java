package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class CustomWorkSchemaTE implements SkudTableEntry {

    private static final int DAY_COUNT_SIZE = 1;

    /** Идентификатор графика работ */
    private String workScheduleId;

    public CustomWorkSchemaTE(String workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        WorkSchedule workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules().get(workScheduleId);
        assert workSchedule != null;

        writeId(workSchedule.getIndex(), stream);
        stream.writeInteger(workSchedule.getDays().size(), DAY_COUNT_SIZE);
        writeBitFlags(stream);
        boolean accessGrantedTimesExist = false;
        for (WorkScheduleDay day : workSchedule.getDays()) {
            writeId(day.getIndex(), stream);
            if (!day.getAccessGrantedTimes().isEmpty()) {
                accessGrantedTimesExist = true;
            }
        }
        if (!accessGrantedTimesExist) {
            skudDatabaseBuilder.addWarning(Code.WORK_SCHEDULE_WITHOUT_ANY_ACCESS_GRANTED_TIME, EntityType.WORK_SCHEDULE,
                    workScheduleId);
        }
    }

}
