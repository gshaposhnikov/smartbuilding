package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public class InvokingLoaderUpdateMode extends InvokingUpdateMode {

    @Override
    public State getCurrentState() {
        return State.INVOKING_LOADER_UPDATE_MODE;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        InvokingLoaderUpdateMode handler = new InvokingLoaderUpdateMode();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    protected Rubezh2OP3UpdateSoftwareModeType getInvokingSoftwareMode() {
        return Rubezh2OP3UpdateSoftwareModeType.LOADER;
    }

    @Override
    protected Rubezh2OP3UpdateSoftwareStatusType getAwaitingSoftwareStatus() {
        return Rubezh2OP3UpdateSoftwareStatusType.UPDATE_LOADER;
    }

    @Override
    protected State toNextState(long currentTimeMs) {
        try {
            WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs, task.getIssueId(),
                    WriteDatabaseTask.getLoaderUpdateProgress());
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
        stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.ERASING_SECTORS);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.START_NEXT_STAGE, 10, false);
        return State.ERASING_SECTORS;
    }

}
