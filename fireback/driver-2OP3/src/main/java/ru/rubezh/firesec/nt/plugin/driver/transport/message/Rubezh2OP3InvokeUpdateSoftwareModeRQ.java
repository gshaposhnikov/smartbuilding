package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3InvokeUpdateSoftwareModeRQ implements Rubezh2OP3Message {

    private Rubezh2OP3UpdateSoftwareModeType softwareType;

    public Rubezh2OP3InvokeUpdateSoftwareModeRQ(Rubezh2OP3UpdateSoftwareModeType softwareType) {
        this.softwareType = softwareType;
    }

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return Rubezh2OP3FunctionType.UPDATE_SOFTWARE;
    }

    public Rubezh2OP3UpdateSoftwareModeType getSoftwareType() {
        return softwareType;
    }

}
