package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3WriteMemoryRS extends Rubezh2OP3CommonRS {
    private int waitingTimeMs;

    public Rubezh2OP3WriteMemoryRS() {
        super(Rubezh2OP3FunctionType.WRITE_MEMORY);
    }

    public int getWaitingTimeMs() {
        return waitingTimeMs;
    }

    public void setWaitingTimeMs(int waitingTimeMs) {
        this.waitingTimeMs = waitingTimeMs;
    }
}
