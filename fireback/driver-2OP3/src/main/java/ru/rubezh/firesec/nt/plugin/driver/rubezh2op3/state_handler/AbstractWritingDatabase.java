package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public abstract class AbstractWritingDatabase extends AbstractConnected {

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().warn("Writing control device database aborted");
        try {
            WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, task.getIssueId(),
                    IssueStatus.FAILED, "Прервано");
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
        Driver.freeExclusiveMode();
        stateMachineContext.getActionExecutor().reset(currentTimeMs);
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        stateMachineContext.getActionExecutor().disconnect(currentTimeMs);
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return State.IDLE;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().warn("Writing control device database aborted");
        stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.ABORTING_ON_FAILURE);
        Driver.freeExclusiveMode();
        /* Попытка - не пытка (вдруг сбросится) */
        stateMachineContext.getActionExecutor().reset(currentTimeMs);
        return State.RESETTING;
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Got exception: {}", exception);
        stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.ABORTING_ON_FAILURE);
        Driver.freeExclusiveMode();
        /* Попытка - не пытка (вдруг сбросится) */
        stateMachineContext.getActionExecutor().reset(currentTimeMs);
        return State.RESETTING;
    }

}
