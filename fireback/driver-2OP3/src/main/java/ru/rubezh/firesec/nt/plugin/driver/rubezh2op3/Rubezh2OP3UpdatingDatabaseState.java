package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DataBlock;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.SectorsRange;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseWriter;

/**
 * Текущее состояние обновления БД на приборе
 */
public class Rubezh2OP3UpdatingDatabaseState {
    /** Число попыток стирания секторов */
    private static final int MAX_ERASE_RETRIES = 5;
    /** Число попыток записи данных */
    private static final int MAX_WRITE_RETRIES = 5;
    /** Таймаут переподключения в мс */
    private static final int RECONNECT_TIMEOUT = 1000;
    /** Максималное число попыток переподключений */
    private static final int MAX_RECONNECT_ATTEMPTS = 5;
    /** Максималное число попыток запроса текущего режима */
    private static final int MAX_GETMMODE_ATTEMPTS = 5;

    /** Стадия обновления */
    private Rubezh2OP3UpdatingDatabaseStage stage;
    /** Список буферов данных для записи */
    private List<DataBuffer> dataBuffers = new ArrayList<>();
    /** Индекс текущего буфера */
    private int currentBufferIndex;
    /** Текущий блок данных для записи */
    DataBlock currentDataBlock;

    /** Список диапазонов секторов, которые необходимо очистить перед записью данных */
    private List<SectorsRange> sectorsRanges = new ArrayList<>();
    /** Индекс текущего диапазона секторов */
    private int currentSectorsIndex;

    /** Число ошибок при стирании секторов */
    private int eraseFails;
    /** Число ошибок при записи данных */
    private int writeFails;

    /** Число оставшихся попыток переподключения */
    private int nReconnectAttemptsLeft = MAX_RECONNECT_ATTEMPTS;
    /** Число оставшихся попыток запроса текущего режима */
    private int nGetModeAttemptsLeft = MAX_GETMMODE_ATTEMPTS;

    private class DataBuffer extends ByteArrayInputStream {
        /* Адрес начала блока */
        private int address;

        public DataBuffer(int address, byte[] data) {
            super(data);
            this.address = address;
        }

        public byte[] read(int size) {
            byte[] data = new byte[size];
            try {
                read(data);
            } catch (IOException ex) {
                throw new UncheckedIOException(ex);
            }
            return data;
        }

        public int getAddress() {
            return address;
        }

        public int getPos() {
            return pos;
        }

        public int getCount() {
            return count;
        }
    }

    public void reset(List<DataBlock> dbBlocks, List<SectorsRange> sectorsToErase) {
        stage = Rubezh2OP3UpdatingDatabaseStage.INVOKING_ARM_UPDATE_MODE;

        dataBuffers.clear();
        sectorsRanges.clear();
        sectorsRanges.addAll(sectorsToErase);

        for (DataBlock dataBlock : dbBlocks) {
            dataBuffers.add(new DataBuffer(dataBlock.getAddress(), dataBlock.getData()));
            int firstSector = dataBlock.getAddress() / DatabaseWriter.DB_SECTOR_SIZE;
            int lastSector = (dataBlock.getAddress() + dataBlock.getSize() - 1) / DatabaseWriter.DB_SECTOR_SIZE;
            sectorsRanges.add(new SectorsRange(firstSector, lastSector));
        }

        // объединение последовательных диапазонов секторов
        sectorsRanges.sort(Comparator.comparingInt(SectorsRange::getFirst));
        SectorsRange oldRange = null;
        Iterator<SectorsRange> ranges = sectorsRanges.iterator();
        while (ranges.hasNext()) {
            SectorsRange nextRange = ranges.next();
            if (oldRange != null && nextRange.getFirst() <= (oldRange.getLast() + 1)) {
                oldRange.setLast(Integer.max(nextRange.getLast(), oldRange.getLast()));
                ranges.remove();
            } else
                oldRange = nextRange;
        }

        currentBufferIndex = 0;
        currentDataBlock = null;
        currentSectorsIndex = 0;
        eraseFails = 0;
        writeFails = 0;
    }

    public float getSectorsProgress() {
        if (sectorsRanges.size() > 0)
            return (float)currentSectorsIndex / sectorsRanges.size();
        else
            return (float)1;
    }

    public float getDataProgress() {
        if (currentBufferIndex < dataBuffers.size() && dataBuffers.size() > 0) {
            DataBuffer currentBuffer = dataBuffers.get(currentBufferIndex);
            return (currentBufferIndex +
                    (float)currentBuffer.getPos() / currentBuffer.getCount()) / dataBuffers.size();
        } else
            return (float)1;
    }

    private DataBlock getNextDataBlock(int maxSize, DataBuffer buffer) {
        if (buffer.available() <= 0)
            return null;
        int address = buffer.getAddress() + buffer.getPos();
        int size = Integer.min(maxSize, buffer.available());
        return new DataBlock(address, buffer.read(size));
    }

    public DataBlock getNextDataBlock(int maxSize) {
        if (maxSize <= 0)
            return null;
        currentDataBlock = null;
        while (true) {
            if (currentBufferIndex < dataBuffers.size())
                currentDataBlock = getNextDataBlock(maxSize, dataBuffers.get(currentBufferIndex));
            else
                break;
            if (currentDataBlock != null)
                break;
            else
                currentBufferIndex++;
        }
        return currentDataBlock;
    }

    public DataBlock getLastDataBlock() {
        return currentDataBlock;
    }

    public SectorsRange getNextSectorsRange() {
        if (currentSectorsIndex < sectorsRanges.size())
            return sectorsRanges.get(currentSectorsIndex++);
        else
            return null;
    }

    public SectorsRange getLastSectorsRange() {
        if ((currentSectorsIndex - 1) < sectorsRanges.size() && sectorsRanges.size() > 1)
            return sectorsRanges.get(currentSectorsIndex - 1);
        else
            return null;
    }

    public Rubezh2OP3UpdatingDatabaseStage getStage() {
        return stage;
    }

    public void setStage(Rubezh2OP3UpdatingDatabaseStage stage) {
        this.stage = stage;
    }

    public void eraseFailed() {
        eraseFails++;
    }

    public boolean shouldRetryErase() {
        return eraseFails < MAX_ERASE_RETRIES;
    }

    public void writeFailed() {
        writeFails++;
    }

    public boolean shouldRetryWrite() {
        return writeFails < MAX_WRITE_RETRIES;
    }

    public static int getReconnectTimeout() {
        return RECONNECT_TIMEOUT;
    }

    public int getnReconnectAttemptsLeft() {
        return nReconnectAttemptsLeft;
    }

    public void setnReconnectAttemptsLeft(int nReconnectAttemptsLeft) {
        this.nReconnectAttemptsLeft = nReconnectAttemptsLeft;
    }

    public int getnGetModeAttemptsLeft() {
        return nGetModeAttemptsLeft;
    }

    public void setnGetModeAttemptsLeft(int nGetModeAttemptsLeft) {
        this.nGetModeAttemptsLeft = nGetModeAttemptsLeft;
    }

}
