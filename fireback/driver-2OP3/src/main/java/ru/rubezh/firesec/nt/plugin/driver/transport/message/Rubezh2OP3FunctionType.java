package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum Rubezh2OP3FunctionType {
    EXCEPTION(0x00),
    GET_PARAMETER(0x01),
    SET_PARAMETER(0x02),
    CHECK_READY(0x3C),
    UPDATE_SOFTWARE(0x39),
    RESET(0x3A),
    ERASE_SECTORS(0x3B),
    WRITE_MEMORY(0x3E),
    GET_UPDATE_SORFWARE_ERROR(0x3D);

    private final int value;
    private Rubezh2OP3FunctionType(int value) {
        this.value = value;
    }

    private static final Map<Integer, Rubezh2OP3FunctionType> typesByValue = new HashMap<>();
    static {
        for (Rubezh2OP3FunctionType type : Rubezh2OP3FunctionType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<Rubezh2OP3FunctionType, Integer> valuesByType = new EnumMap<>(Rubezh2OP3FunctionType.class);
    static {
        for (Rubezh2OP3FunctionType type : Rubezh2OP3FunctionType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static Rubezh2OP3FunctionType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(Rubezh2OP3FunctionType type) {
        return valuesByType.get(type);
    }
}
