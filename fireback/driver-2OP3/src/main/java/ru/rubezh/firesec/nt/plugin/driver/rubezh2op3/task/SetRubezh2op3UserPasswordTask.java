package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.DeviceUserType;

public class SetRubezh2op3UserPasswordTask extends Task {

    private DeviceUserType userType;
    private String password;

    public SetRubezh2op3UserPasswordTask(String issueId_, DeviceUserType userType_, String password_) {
        super(1000);
        issueId = issueId_;
        userType = userType_;
        password = password_;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setDeviceUserPassword(currentTimeMs, userType, password);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId,
                IssueStatus.IN_PROGRESS, "");
        return State.WAITING_SET_RUBEZH2OP3_USER_PASSWORD;
    }

}
