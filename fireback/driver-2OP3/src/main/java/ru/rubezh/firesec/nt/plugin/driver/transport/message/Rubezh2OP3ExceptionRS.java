package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3ExceptionRS extends Rubezh2OP3CommonRS {
    public Rubezh2OP3ExceptionRS(ExceptionType exception) {
        super(Rubezh2OP3FunctionType.EXCEPTION);
        setSuccess(false);
        setExceptionType(exception);
    }
}
