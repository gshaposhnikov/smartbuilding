package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseState;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareErrorType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareModeType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

/* 
 * Переходы при подключении через МС:
 *  ->
 *  onSoftwareUpdateModeInvoked -> setTimer(LONG_OPERATION_CHECK)
 *                          |
 *  onTimer(LONG_OPERATION_CHECK) -> checkDeviceReadyState
 *                          |
 *  onDeviceReadyStateReceived -> getUpdateSoftwareError
 *                          |
 *  onUpdateSoftwareErrorReceived -> getUpdateSoftwareStatus
 *                          |
 *  onUpdateSoftwareStatusReceived -> toNextState
 *  ->
 */
/* 
 * Переходы при подключении по USB:
 *  ->
 *  onDisconnected -> setTimer(UPDATE_SOFTWARE_MODE_RECONNECT)
 *                          |
 *  onTimer(UPDATE_SOFTWARE_MODE_RECONNECT) -> connect
 *                          |
 *  onConnected -> checkDeviceReadyState
 *                          |
 *  onDeviceReadyStateReceived -> getUpdateSoftwareError
 *                          |
 *  onUpdateSoftwareErrorReceived -> getUpdateSoftwareStatus
 *                          |
 *  onUpdateSoftwareStatusReceived -> toNextState
 *  ->
 */
public abstract class InvokingUpdateMode extends AbstractWritingDatabase {

    abstract protected Rubezh2OP3UpdateSoftwareModeType getInvokingSoftwareMode();

    abstract protected Rubezh2OP3UpdateSoftwareStatusType getAwaitingSoftwareStatus();

    abstract protected State toNextState(long currentTimeMs);

    @Override
    public State onSoftwareUpdateModeInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK, waitingTimeMs,
                false);
        return getCurrentState();
    }

    @Override
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
        if (isReady)
            stateMachineContext.getActionExecutor().getUpdateSoftwareError(currentTimeMs);
        else
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK,
                    stateMachineContext.getLongOperationWaitingTimeMs(), false);
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
        if (error == Rubezh2OP3UpdateSoftwareErrorType.SUCCESS)
            stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
        else
            stateMachineContext.getActionExecutor().invokeUpdateSoftwareMode(currentTimeMs, getInvokingSoftwareMode());
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status) {
        if (status == getAwaitingSoftwareStatus()) {
            return toNextState(currentTimeMs);
        }

        int nGetModeAttemptsLeft = stateMachineContext.getUpdatingDatabaseState().getnGetModeAttemptsLeft();
        if (nGetModeAttemptsLeft > 0) {
            stateMachineContext.getUpdatingDatabaseState().setnGetModeAttemptsLeft(nGetModeAttemptsLeft - 1);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_SOFTWARE_MODE_RETRY,
                    stateMachineContext.getUpdateSoftwareModeRetry(), false);
            return getCurrentState();
        }

        /* Разрываем связь и завершаем с неуспехом задачу, если в режим так и не перешли */
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.LONG_OPERATION_CHECK))
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
        else if (timerName.equals(TimerNames.UPDATE_SOFTWARE_MODE_RETRY))
            stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
        else if (timerName.equals(TimerNames.UPDATE_SOFTWARE_MODE_RECONNECT))
            stateMachineContext.getActionExecutor().connect(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_SOFTWARE_MODE_RECONNECT,
                Rubezh2OP3UpdatingDatabaseState.getReconnectTimeout(), false);
        return getCurrentState();
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_SOFTWARE_MODE_RECONNECT,
                Rubezh2OP3UpdatingDatabaseState.getReconnectTimeout(), false);
        return getCurrentState();
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        int nReconnectAttemptsLeft = stateMachineContext.getUpdatingDatabaseState().getnReconnectAttemptsLeft();
        if (nReconnectAttemptsLeft > 0) {
            stateMachineContext.getUpdatingDatabaseState().setnReconnectAttemptsLeft(nReconnectAttemptsLeft - 1);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.UPDATE_SOFTWARE_MODE_RECONNECT,
                    Rubezh2OP3UpdatingDatabaseState.getReconnectTimeout(), false);
            return getCurrentState();
        }

        /* Разрываем связь и завершаем с неуспехом задачу, если подключение так и не восстанавливается */
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
        return getCurrentState();
    }

}
