package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetAddressListTask extends Task {

    public SetAddressListTask() {
        super(PRIORITY_VERY_HIGH);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getAddressList(currentTimeMs);
        return State.SETTING_ADDRESS_LIST;
    }

}
