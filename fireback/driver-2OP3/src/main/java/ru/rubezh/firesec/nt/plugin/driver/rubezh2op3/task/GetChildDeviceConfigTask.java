package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class GetChildDeviceConfigTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;

    public GetChildDeviceConfigTask(ActiveDevice activeDevice, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getChildDeviceConfig(currentTimeMs,
                activeDevice.getDeviceProject().getLineNo(), activeDevice.getDeviceProject().getLineAddress());
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.WAITING_CHILD_DEVICE_CONFIG;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }
}
