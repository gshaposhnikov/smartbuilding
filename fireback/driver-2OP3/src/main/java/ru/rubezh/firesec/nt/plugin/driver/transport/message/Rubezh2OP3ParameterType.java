package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum Rubezh2OP3ParameterType {
    UPDATE_SOFTWARE_STATUS(0x01),
    DEVICE_TYPE(0x03),
    CURRENT_TIME(0x11),
    FIRMWARE_VERSION(0x12),
    ADDRESS_LIST(0x14),
    LOG_EVENT(0x20),
    EVENT_COUNTERS_AND_STATES(0x25),
    MAX_EVENT_COUNTERS(0x26),
    DB_BLOCK(0x52),
    RESET_STATE(0x54),
    REGION_STATES(0x62),
    CHILD_DEVICE_STATES(0x63),
    CHILD_DEVICE_CONFIG(0x64),
    WAITING_ACCESS_KEY_STATUS(0x68);

    private final int value;

    private Rubezh2OP3ParameterType(int value) {
        this.value = value;
    }

    private static final Map<Integer, Rubezh2OP3ParameterType> typesByValue = new HashMap<>();
    static {
        for (Rubezh2OP3ParameterType type : Rubezh2OP3ParameterType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<Rubezh2OP3ParameterType, Integer> valuesByType = new EnumMap<>(Rubezh2OP3ParameterType.class);
    static {
        for (Rubezh2OP3ParameterType type : Rubezh2OP3ParameterType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static Rubezh2OP3ParameterType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(Rubezh2OP3ParameterType type) {
        return valuesByType.get(type);
    }
}
