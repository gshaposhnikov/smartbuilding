package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class GetChildDeviceStatesTask extends ControlDeviceDBMatchingProjectRequiredTask {

    /* Максимальное число устройств, опрашиваемых за раз (при срочной выборке) */
    public static final int MAX_DEVICES_TO_POLL = 30;

    /* Число опрошенных устройств */
    private int polledDevices = 0;

    private ActiveDevice activeDevice;

    public GetChildDeviceStatesTask(ActiveDevice activeDevice) {
        super(PRIORITY_VERY_LOW);
        this.activeDevice = activeDevice;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getChildDeviceStates(currentTimeMs,
                activeDevice.getDeviceProject().getLineNo(),
                activeDevice.getDeviceProject().getLineAddress(),
                activeDevice.getDeviceProject().getRegionId());

        return State.WAITING_CHILD_DEVICE_STATES;
    }

    public void statesReceived() {
        polledDevices++;
    }

    public boolean completed() {
        return polledDevices >= MAX_DEVICES_TO_POLL;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

}
