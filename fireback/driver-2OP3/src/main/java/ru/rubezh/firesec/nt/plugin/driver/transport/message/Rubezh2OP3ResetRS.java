package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3ResetRS extends Rubezh2OP3CommonRS {

    private int waitingTimeMs;

    public int getWaitingTimeMs() {
        return waitingTimeMs;
    }

    public void setWaitingTimeMs(int waitingTimeMs) {
        this.waitingTimeMs = waitingTimeMs;
    }

    public Rubezh2OP3ResetRS() {
        super(Rubezh2OP3FunctionType.RESET);
    }
}
