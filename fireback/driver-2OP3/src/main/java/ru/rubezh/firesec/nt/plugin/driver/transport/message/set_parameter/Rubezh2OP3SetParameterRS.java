package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3CommonRS;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3FunctionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public abstract class Rubezh2OP3SetParameterRS extends Rubezh2OP3CommonRS {
    private Rubezh2OP3ParameterType parameterType;

    public Rubezh2OP3SetParameterRS(Rubezh2OP3ParameterType parameterType) {
        super(Rubezh2OP3FunctionType.SET_PARAMETER);
        this.parameterType = parameterType;
    }

    public Rubezh2OP3ParameterType getParameterType() {
        return parameterType;
    }
}
