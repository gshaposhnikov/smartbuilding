package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3GetUpdateSoftwareErrorRS extends Rubezh2OP3CommonRS {

    private Rubezh2OP3UpdateSoftwareErrorType error;

    public Rubezh2OP3GetUpdateSoftwareErrorRS() {
        super(Rubezh2OP3FunctionType.GET_UPDATE_SORFWARE_ERROR);
    }

    public Rubezh2OP3UpdateSoftwareErrorType getError() {
        return error;
    }

    public void setError(Rubezh2OP3UpdateSoftwareErrorType error) {
        this.error = error;
    }

}
