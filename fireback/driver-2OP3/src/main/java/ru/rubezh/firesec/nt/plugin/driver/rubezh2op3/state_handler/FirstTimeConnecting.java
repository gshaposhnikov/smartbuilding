package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class FirstTimeConnecting extends Connecting {

    @Override
    public State getCurrentState() {
        return State.FIRST_TIME_CONNECTING;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        FirstTimeConnecting handler = new FirstTimeConnecting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
