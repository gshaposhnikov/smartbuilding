package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public interface Rubezh2OP3Message {
    Rubezh2OP3FunctionType getFunctionType();
}
