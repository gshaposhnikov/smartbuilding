package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;
import ru.rubezh.firesec.nt.domain.v1.AddressType;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Состояние опроса состояний дочерних устройств
 */
public class Rubezh2OP3ChildDevicePollingState {

    /** Опрашиваемые устройства */
    private List<ActiveDevice> pollingDevices = new ArrayList<>();
    /** Устройства, которые необходимо срочно опросить */
    private Queue<ActiveDevice> urgentPollingDevices = new LinkedList<>();
    /** Индекс следующего устройства для опроса */
    private int nextDeviceToPollIndex = 0;

    public Rubezh2OP3ChildDevicePollingState(List<ActiveDevice> activeDevices) {
        for (ActiveDevice activeDevice : activeDevices)
            if (devicePollingIsAcceptable(activeDevice.getDeviceProject()))
                pollingDevices.add(activeDevice);
    }

    public boolean isPollCompleted() {
        return urgentPollingDevices.isEmpty() && !pollingDevices.isEmpty() &&
                nextDeviceToPollIndex == pollingDevices.size();
    }

    public ActiveDevice getNextDeviceToPoll() {
        if (!urgentPollingDevices.isEmpty())
            return urgentPollingDevices.poll();
        if (pollingDevices.isEmpty())
            return null;
        if (nextDeviceToPollIndex == pollingDevices.size())
            nextDeviceToPollIndex = 0;
        return pollingDevices.get(nextDeviceToPollIndex++);
    }

    public ActiveDevice getUrgentDeviceToPoll() {
        return urgentPollingDevices.poll();
    }

    public void addUrgentDeviceToPoll(ActiveDevice activeDevice) {
        if (devicePollingIsAcceptable(activeDevice.getDeviceProject()))
            urgentPollingDevices.add(activeDevice);
    }

    public void prepareUrgentPollAllDevices() {
        urgentPollingDevices.addAll(pollingDevices);
    }

    private boolean devicePollingIsAcceptable(Device device) {
        return device.getDeviceCategory() != DeviceCategory.CONTAINER &&
                (device.getAddressType() == AddressType.GENERIC ||
                device.getDeviceCategory() == DeviceCategory.VIRTUAL_CONTAINER);
    }

}
