package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetDevicePollingStateTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;
    private boolean pollingState;

    public SetDevicePollingStateTask(ActiveDevice activeDevice, boolean pollingState, String issueId) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.pollingState = pollingState;
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        DeviceProfile deviceProfile = stateMachineContext.getSupportedDeviceProfiles()
                .get(activeDevice.getDeviceProject().getDeviceProfileId());
        int type = deviceProfile.getRsr3VirtualDeviceType();
        if (type == 0)
            type = deviceProfile.getRsr3RawDeviceType();
        stateMachineContext.getActionExecutor().changeDevicePollingState(currentTimeMs,
                type, activeDevice.getDeviceProject().getLineNo(),
                activeDevice.getDeviceProject().getLineAddress(), pollingState);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.WAITING_SET_DEVICE_POLLING_STATE;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public boolean isPollingState() {
        return pollingState;
    }
}
