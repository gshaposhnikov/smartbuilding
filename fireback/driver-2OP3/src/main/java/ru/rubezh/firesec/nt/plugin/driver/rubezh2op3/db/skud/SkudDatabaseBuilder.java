package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey.KeyType;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DataBlock;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.SectorsRange;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseWriter;

/**
 * Класс для формирования БД СКУД на основе данных КА драйвера
 *
 * @author Андрей Лисовой, Антон Васильев
 *
 */
@SuppressWarnings("unused")
public class SkudDatabaseBuilder extends DatabaseWriter {

    /** Смещение базы СКУД */
    private static final int DB_OFFSET = 0xC00000;
    /** Наибольший размер базы СКУД */
    private static final int DB_MAX_SIZE = 0x300000;

    /** Смещение постоянной части базы */
    private static final int CONST_DB_PART_OFFSET = 0xC00000;
    /** Наибольший размер постоянной части базы */
    private static final int CONST_DB_PART_MAX_SIZE = 0x10000;
    /** Смещение таблицы абонентов */
    private static final int EMPLOYEES_TABLE_OFFSET = 0xC10000;
    /** Наибольший размер таблицы абонентов */
    private static final int EMPLOYEES_TABLE_MAX_SIZE = 0x60000;
    /** Смещение таблицы списков точек доступа */
    private static final int ACCESS_POINT_LISTS_TABLE_OFFSET = 0xC70000;
    /** Наибольший размер таблицы списков точек доступа */
    private static final int ACCESS_POINT_LISTS_TABLE_MAX_SIZE = 0x10000;
    /** Смещение таблицы ключей */
    private static final int CARD_ACCESS_KEYS_TABLE_OFFSET = 0xC80000;
    /** Наибольший размер таблицы ключей */
    private static final int CARD_ACCESS_KEYS_TABLE_MAX_SIZE = 0x90000;
    /** Смещение таблицы паролей */
    private static final int PASSWORD_ACCESS_KEYS_TABLE_OFFSET = 0xD10000;
    /** Наибольший размер таблицы паролей */
    private static final int PASSWORD_ACCESS_KEYS_TABLE_MAX_SIZE = 0x90000;
    /** Смещение таблицы списков охранных зон */
    private static final int SECURITY_REGION_LISTS_TABLE_OFFSET = 0xDA0000;
    /** Наибольший размер таблицы списков охранных зон */
    private static final int SECURITY_REGION_LISTS_TABLE_MAX_SIZE = 0x20000;
    /** Смещение таблицы списков сценариев */
    private static final int SCENARIO_LISTS_TABLE_OFFSET = 0xDC0000;
    /** Наибольший размер таблицы списков сценариев */
    private static final int SCENARIO_LISTS_TABLE_MAX_SIZE = 0x60000;
    /** Смещение таблицы графиков */
    private static final int WORK_SCHEDULES_TABLE_OFFSET = 0xE20000;
    /** Наибольший размер таблицы графиков */
    private static final int WORK_SCHEDULES_TABLE_MAX_SIZE = 0x8000;
    /** Смещение таблицы временных интервалов */
    private static final int ACCESS_GRANTED_TIMES_TABLE_OFFSET = 0xE28000;
    /** Наибольший размер таблицы временных интервалов */
    private static final int ACCESS_GRANTED_TIMES_TABLE_MAX_SIZE = 0x8000;
    /** Смещение таблицы именованных интервалов */
    private static final int ACCESS_GRANTED_TIME_LISTS_TABLE_OFFSET = 0xE30000;
    /** Наибольший размер таблицы именованных интервалов */
    private static final int ACCESS_GRANTED_TIME_LISTS_TABLE_MAX_SIZE = 0x10000;
    /** Смещение таблицы недельных схем работы */
    private static final int WEEK_WORK_SCHEMAS_TABLE_OFFSET = 0xE40000;
    /** Наибольший размер таблицы недельных схем работы */
    private static final int WEEK_WORK_SCHEMAS_TABLE_MAX_SIZE = 0x10000;
    /** Смещение таблицы посменных схем работы */
    private static final int OTHER_WORK_SCHEMAS_TABLE_OFFSET = 0xE50000;
    /** Наибольший размер таблицы посменных схем работы */
    private static final int OTHER_WORK_SCHEMAS_TABLE_MAX_SIZE = 0x10000;
    /** Смещение таблицы месячных схем работы */
    private static final int MONTH_WORK_SCHEMAS_TABLE_OFFSET = 0xE60000;
    /** Наибольший размер таблицы месячных схем работы */
    private static final int MONTH_WORK_SCHEMAS_TABLE_MAX_SIZE = 0x10000;
    /** Смещение таблицы праздников */
    private static final int HOLIDAYS_TABLE_OFFSET = 0xE70000;
    /** Наибольший размер таблицы праздников */
    private static final int HOLIDAYS_TABLE_MAX_SIZE = 0x90000;

    /** Размер служебной части */
    private static final int SERVICE_INFORMATION_SIZE = 256;
    /** Сигнатура базы */
    private static final byte[] DB_SIGNATURE = { 0x53, 0x43, 0x55, 0x44 };
    /** Размер CRC (контрольной суммы) */
    private static final int CRC_SIZE = 2;
    /** Версия базы */
    private static final byte[] DB_VERSION = { 0x01, 0x00 };
    /** Размер поля "размер постоянной части" */
    private static final int CONST_DB_LENGTH_SIZE = 3;
    /** Размер части БД с заголовками */
    private static final int DB_HEADERS_SIZE = 321;
    /** Размер полей "длина записи в таблице" */
    private static final int TABLE_ITEM_LENGTH_SIZE = 1;
    /** Размер полей "указатель на таблицу" */
    private static final int TABLE_POINTER_SIZE = 3;
    /** Размер полей "указатель на элемент таблицы" */
    private static final int TABLE_ENTRY_POINTER_SIZE = 3;

    /** Размер одной записи таблицы точек доступа */
    private static final int ACCESS_POINTS_TABLE_ITEM_SIZE = 9;
    /** Размер одной записи таблицы зон доступа */
    private static final int ACCESS_REGIONS_TABLE_ITEM_SIZE = 22;
    /** Размер одной записи таблицы абонентов */
    private static final int EMPLOYEES_TABLE_ITEM_SIZE = 25;
    /** Размер одной записи таблицы ключей */
    private static final int CARD_ACCESS_KEYS_TABLE_ITEM_SIZE = 27;
    /** Размер одной записи таблицы паролей */
    private static final int PASSWORD_ACCESS_KEYS_TABLE_ITEM_SIZE = 27;
    /** Размер одной записи таблицы графиков */
    private static final int WORK_SCHEMAS_TABLE_ITEM_SIZE = 8;
    /** Размер одной записи таблицы временных интервалов */
    private static final int ACCESS_GRANTED_TIMES_TABLE_ITEM_SIZE = 5;
    /** Размер одной записи таблицы недельных схем работы */
    private static final int WEEK_WORK_SCHEMAS_TABLE_ITEM_SIZE = 17;
    /** Размер одной записи таблицы месячных схем работы */
    private static final int MONTH_WORK_SCHEMAS_TABLE_ITEM_SIZE = 65;

    /** Размер поля "Номер шлейфа" */
    private static final int LINE_NUMBER_FIELD_SIZE = 1;
    /** Размер поля "Адрес устройства" */
    private static final int LINE_ADDRESS_FIELD_SIZE = 1;

    /** Маска поля "Настройка правила "antipassback"" */
    private static final int ANTIPASSBACK_RULES_FIELD_MASK = 0x7;
    /** Смещение поля "Настройка правила "antipassback"" */
    private static final int ANTIPASSBACK_RULES_FIELD_SHIFT = 0;
    /** Маска поля "Настройка режима доступа" */
    private static final int ACCESS_MODE_FIELD_MASK = 0x7;
    /** Смещение поля "Настройка правила "antipassback"" */
    private static final int ACCESS_MODE_FIELD_SHIFT = 3;
    /** Размер поля "Настройки точки доступа" */
    private static final int ACCESS_POINT_SETTINGS_FIELD_SIZE = 1;

    /** Размер поля "Индекс зоны для Firesec" */
    private static final int REGION_INDEX_FIELD_SIZE = 2;
    /** Размер поля "Наименование зоны" */
    private static final int REGION_NAME_FIELD_SIZE = 20;

    /** Контекст конечного автомата драйвера */
    private Rubezh2OP3StateMachineContext stateMachineContext;

    /** Результаты валидации БД */
    private List<ActivationValidateMessage> validateMessages = new ArrayList<>();
    /** Критических ошибок при валидации не найдено */
    private boolean databaseValid;

    /** Карта указателей на записи таблицы зон по Id зоны */
    private Map<String, Integer> regionsTableItemPtrs = new HashMap<>();
    /** Указатель на таблицу точек доступа */
    private int accessPointsTablePtr;
    /** Указатель на таблицу зон доступа */
    private int accessRegionsTablePtr;
    /** Сгенерированные блоки постоянной части БД */
    private List<DataBlock> constDataBlocks;

    /* Изменяемая часть БД СКУД */

    /** Сгенерированные блоки изменяемой части БД */
    private List<DataBlock> varDataBlocks;
    /** Элементы таблицы списков точек доступа */
    private List<SkudTableEntry> accessPointListTEs;
    private Map<String, Integer> accessPointListTEIndexByAccessKeyIds = new HashMap<>();
    /** Элементы таблицы сотрудников (абонентов) */
    private List<SkudTableEntry> employeeTEs;
    /** Элементы таблицы разрешенных интервалов прохода */
    private List<SkudTableEntry> accessGrantedTimeTEs;
    /** Элементы таблицы списков разрешенных интервалов прохода */
    private List<SkudTableEntry> accessGrantedTimeListTEs;
    /** Элементы таблицы недельных схем работ */
    private List<SkudTableEntry> weekWorkSchemaTEs;
    /** Элементы таблицы месячных схем работ */
    private List<SkudTableEntry> monthWorkSchemaTEs;
    /** Элементы таблицы сменных схем работ */
    private List<SkudTableEntry> otherWorkSchemaTEs;
    /** Элементы таблицы графиков работ */
    private List<SkudTableEntry> workScheduleTEs;
    /** Элементы таблицы ключей доступа - карточек */
    private List<SkudTableEntry> cardAccessKeyTEs;
    /** Элементы таблицы ключей доступа - паролей */
    private List<SkudTableEntry> passwordAccessKeyTEs;
    /*
     * TODO: для автосохранения изменений БД СКУД нужны карты, отражающие элементы
     * таблиц БД СКУД на сущности, от которых они зависят
     */

    /**
     * Конструктор (единственный)
     */
    public SkudDatabaseBuilder(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    public Rubezh2OP3StateMachineContext getStateMachineContext() {
        return stateMachineContext;
    }

    /**
     * Очистка контекста
     */
    private void clearContext() {
        databaseValid = true;
        validateMessages.clear();
        regionsTableItemPtrs.clear();
        accessPointListTEIndexByAccessKeyIds.clear();
        AccessPointListTE.resetIdCounter();
    }

    /**
     * Формирование постоянной части базы данных
     */
    public boolean generateConstDatabase(List<ActivationValidateMessage> validateMessages) {
        constDataBlocks = new ArrayList<>();
        clearContext();
        constDataBlocks.add(new DataBlock(CONST_DB_PART_OFFSET, generateConstantDbPart()));
        validateMessages.addAll(this.validateMessages);
        return databaseValid;
    }

    public boolean generateVarDatabase(List<ActivationValidateMessage> validateMessages) {
        varDataBlocks = new ArrayList<>();
        clearContext(); // TODO: подумать - надо ли сохранять предупреждения от формирования постоянной
                        // БД, а также regionsTableItemPtrs

        // TODO: проверка размера таблиц
        accessPointListTEs = getAccessPointListTableEntries();
        varDataBlocks.add(new DataBlock(ACCESS_POINT_LISTS_TABLE_OFFSET, generateSkudTable(accessPointListTEs)));
        employeeTEs = getEmployeeTableEntries();
        varDataBlocks.add(new DataBlock(EMPLOYEES_TABLE_OFFSET, generateSkudTable(employeeTEs)));
        accessGrantedTimeTEs = getAccessGrantedTimeTableEntries();
        varDataBlocks.add(new DataBlock(ACCESS_GRANTED_TIMES_TABLE_OFFSET, generateSkudTable(accessGrantedTimeTEs)));
        accessGrantedTimeListTEs = getAccessGrantedTimeListTableEntries();
        varDataBlocks.add(
                new DataBlock(ACCESS_GRANTED_TIME_LISTS_TABLE_OFFSET, generateSkudTable(accessGrantedTimeListTEs)));
        weekWorkSchemaTEs = getFixedWorkSchemaTableEntries(WorkScheduleType.WEEK);
        varDataBlocks.add(new DataBlock(WEEK_WORK_SCHEMAS_TABLE_OFFSET, generateSkudTable(weekWorkSchemaTEs)));
        monthWorkSchemaTEs = getFixedWorkSchemaTableEntries(WorkScheduleType.MONTH);
        varDataBlocks.add(new DataBlock(MONTH_WORK_SCHEMAS_TABLE_OFFSET, generateSkudTable(monthWorkSchemaTEs)));
        otherWorkSchemaTEs = getCustomWorkSchemaTableEntries();
        varDataBlocks.add(new DataBlock(OTHER_WORK_SCHEMAS_TABLE_OFFSET, generateSkudTable(otherWorkSchemaTEs)));
        workScheduleTEs = getWorkScheduleTableEntries();
        varDataBlocks.add(new DataBlock(WORK_SCHEDULES_TABLE_OFFSET, generateSkudTable(workScheduleTEs)));
        cardAccessKeyTEs = getAccessKeyTableEntries(KeyType.CARD);
        varDataBlocks.add(new DataBlock(CARD_ACCESS_KEYS_TABLE_OFFSET, generateSkudTable(cardAccessKeyTEs)));
        passwordAccessKeyTEs = getAccessKeyTableEntries(KeyType.PASSWORD);
        varDataBlocks.add(new DataBlock(PASSWORD_ACCESS_KEYS_TABLE_OFFSET, generateSkudTable(passwordAccessKeyTEs)));
        // TODO: остальные таблицы

        validateMessages.addAll(this.validateMessages);
        return databaseValid;
    }

    private byte[] generateSkudTable(List<SkudTableEntry> tableEntries) {
        DatabaseOutputStream stream = new DatabaseOutputStream();
        for (SkudTableEntry tableEntry : tableEntries) {
            tableEntry.write(this, stream);
        }
        return stream.toByteArray();
    }

    private List<SkudTableEntry> getAccessPointListTableEntries() {
        List<SkudTableEntry> accessPointListTEs = new ArrayList<>();
        for (Employee employee : stateMachineContext.getEmployees().values()) {
            if (employee.getAccessMap().getAccessPointDeviceIds().isEmpty()) {
                for (AccessKey accessKey : stateMachineContext.getAccessKeysByEmployeeIds().get(employee.getId())) {
                    AccessPointListTE accessPointListTE = new AccessKeyAccessPointTE(accessKey.getId());
                    accessPointListTEIndexByAccessKeyIds.put(accessKey.getId(), accessPointListTE.getId());
                    accessPointListTEs.add(accessPointListTE);
                }
            } else {
                boolean accessKeysHaveOwnAccessPoints = false;
                for (AccessKey accessKey : stateMachineContext.getAccessKeysByEmployeeIds().get(employee.getId())) {
                    if (!accessKey.getAccessMap().getAccessPointDeviceIds().isEmpty()) {
                        accessKeysHaveOwnAccessPoints = true;
                        break;
                    }
                }
                if (accessKeysHaveOwnAccessPoints) {
                    for (AccessKey accessKey : stateMachineContext.getAccessKeysByEmployeeIds().get(employee.getId())) {
                        AccessPointListTE accessPointListTE = new MergedAccessPointListTE(accessKey.getId());
                        accessPointListTEIndexByAccessKeyIds.put(accessKey.getId(), accessPointListTE.getId());
                        accessPointListTEs.add(accessPointListTE);
                    }
                } else {
                    AccessPointListTE accessPointListTE = new EmployeeAccessPointListTE(employee.getId());
                    accessPointListTEs.add(accessPointListTE);
                    for (AccessKey accessKey : stateMachineContext.getAccessKeysByEmployeeIds().get(employee.getId())) {
                        accessPointListTEIndexByAccessKeyIds.put(accessKey.getId(), accessPointListTE.getId());
                    }
                }
            }
        }
        return accessPointListTEs;
    }

    private List<SkudTableEntry> getEmployeeTableEntries() {
        List<SkudTableEntry> employeeTableEntries = new ArrayList<>();
        for (String employeeId : stateMachineContext.getEmployees().keySet()) {
            employeeTableEntries.add(new EmployeeTE(employeeId));
        }
        return employeeTableEntries;
    }

    private List<SkudTableEntry> getAccessGrantedTimeTableEntries() {
        List<SkudTableEntry> accessGrantedTimeTableEntries = new ArrayList<>();
        for (WorkSchedule workSchedule : stateMachineContext.getWorkSchedules().values()) {
            for (int dayNo = 0; dayNo < workSchedule.getDays().size(); ++dayNo) {
                for (int timeNo = 0; timeNo < workSchedule.getDays().get(dayNo).getAccessGrantedTimes()
                        .size(); ++timeNo) {
                    accessGrantedTimeTableEntries.add(new AccessGrantedTimeTE(workSchedule.getId(), dayNo, timeNo));
                }
            }
        }
        return accessGrantedTimeTableEntries;
    }

    private List<SkudTableEntry> getAccessGrantedTimeListTableEntries() {
        List<SkudTableEntry> accessGrantedTimeListTableEntries = new ArrayList<>();
        for (WorkSchedule workSchedule : stateMachineContext.getWorkSchedules().values()) {
            for (int dayNo = 0; dayNo < workSchedule.getDays().size(); ++dayNo) {
                accessGrantedTimeListTableEntries.add(new AccessGrantedTimeListTE(workSchedule.getId(), dayNo));
            }
        }
        return accessGrantedTimeListTableEntries;
    }

    private List<SkudTableEntry> getFixedWorkSchemaTableEntries(WorkScheduleType workScheduleType) {
        List<SkudTableEntry> workSchemaTableEntries = new ArrayList<>();
        for (WorkSchedule workSchedule : stateMachineContext.getWorkSchedules().values()) {
            if (workSchedule.getWorkScheduleType() == workScheduleType) {
                workSchemaTableEntries.add(new FixedWorkSchemaTE(workSchedule.getId()));
            }
        }
        return workSchemaTableEntries;
    }

    private List<SkudTableEntry> getCustomWorkSchemaTableEntries() {
        List<SkudTableEntry> workSchemaTableEntries = new ArrayList<>();
        for (WorkSchedule workSchedule : stateMachineContext.getWorkSchedules().values()) {
            if (workSchedule.getWorkScheduleType() == WorkScheduleType.OTHER) {
                workSchemaTableEntries.add(new CustomWorkSchemaTE(workSchedule.getId()));
            }
        }
        return workSchemaTableEntries;
    }

    private List<SkudTableEntry> getWorkScheduleTableEntries() {
        List<SkudTableEntry> workScheduleTableEntries = new ArrayList<>();
        for (WorkSchedule workSchedule : stateMachineContext.getWorkSchedules().values()) {
            workScheduleTableEntries.add(new WorkScheduleTE(workSchedule.getId()));
        }
        return workScheduleTableEntries;
    }

    private List<SkudTableEntry> getAccessKeyTableEntries(KeyType keyType) {
        List<SkudTableEntry> accessKeyTableEntries = new ArrayList<>();
        for (AccessKey accessKey : stateMachineContext.getAccessKeys().values()) {
            if (accessKey.getKeyType() == keyType) {
                accessKeyTableEntries.add(new AccessKeyTE(accessKey.getId()));
            }
        }
        return accessKeyTableEntries;
    }

    public List<DataBlock> getConstDataBlocks() {
        return constDataBlocks;
    }

    public List<DataBlock> getVarDataBlocks() {
        return varDataBlocks;
    }

    public List<ActivationValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public boolean isDatabaseValid() {
        return databaseValid;
    }

    /** Дамп БД в файлы с заданным префиксом
     *
     * @param prefix префикс файлов */
    public void dumpDb(String prefix) {
        FileOutputStream fos;
        try {
            for (DataBlock dataBlock : constDataBlocks) {
                fos = new FileOutputStream(
                        new File(prefix + "_rubezh_0x" + Integer.toHexString(dataBlock.getAddress()) + "_skud.bin"));
                fos.write(dataBlock.getData());
                fos.close();
            }
            for (DataBlock dataBlock : varDataBlocks) {
                fos = new FileOutputStream(
                        new File(prefix + "_rubezh_0x" + Integer.toHexString(dataBlock.getAddress()) + "_skud.bin"));
                fos.write(dataBlock.getData());
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, Integer> getAccessPointListTEIndexByAccessKeyIds() {
        return accessPointListTEIndexByAccessKeyIds;
    }

    /**
     * Формирование списка диапазонов секторов, стираемых независимо от наполнения базы
     */
    @Override
    public List<SectorsRange> getSectorsToErase() {
        List<SectorsRange> ranges = new ArrayList<>();
        ranges.add(new SectorsRange(DB_OFFSET / DB_SECTOR_SIZE, (DB_OFFSET + DB_MAX_SIZE - 1) / DB_SECTOR_SIZE));
        return ranges;
    }

    /**
     * Формирование постоянной части базы
     */
    private byte[] generateConstantDbPart() {

        DatabaseOutputStream stream = new DatabaseOutputStream();
        LinkedList<Consumer<byte[]>> finalizers = new LinkedList<>();

        // Заголовок постоянной части базы
        generateDBHeaders(stream, finalizers);

        // Контроль размера заголовка
        assert stream.size() == DB_HEADERS_SIZE :
                "Недопустимый размер заголовка: " + stream.size() + " вместо " + DB_HEADERS_SIZE;

        // Таблица точек доступа
        accessPointsTablePtr = CONST_DB_PART_OFFSET + stream.size();
        generateAccessPointsTable(stream, finalizers);
        // Таблица зон доступа
        accessRegionsTablePtr = CONST_DB_PART_OFFSET + stream.size();
        generateAccessRegionsTable(stream);

        // Контроль размера постоянной части базы
        if (stream.size() > CONST_DB_PART_MAX_SIZE)
            return new byte[0];

        // Заполнение отложенных полей (в обратном порядке)
        byte[] constantDb = stream.toByteArray();
        Iterator<Consumer<byte[]>> descFinalizers = finalizers.descendingIterator();
        while (descFinalizers.hasNext())
            descFinalizers.next().accept(constantDb);

        return constantDb;
    }

    /**
     * Формирование заголовка постоянной части базы
     */
    private void generateDBHeaders(DatabaseOutputStream stream, List<Consumer<byte[]>> finalizers) {
        // Служебная часть
        stream.writeZeros(SERVICE_INFORMATION_SIZE);
        // Сигнатура базы
        stream.writeByteArray(DB_SIGNATURE);
        // CRC постоянной части базы
        int crcOffset = stream.writeZeros(CRC_SIZE);
        int crcDataOffset = stream.size();
        finalizers.add(constDb -> recalculateCrc(constDb, crcOffset, CRC_SIZE, crcDataOffset, constDb.length));
        // Версия базы
        stream.writeByteArray(DB_VERSION);
        // Размер постоянной части
        int constDbSizeOffset = stream.writeZeros(CONST_DB_LENGTH_SIZE);
        finalizers.add(constDb -> insertInteger(constDb, constDbSizeOffset, constDb.length, CONST_DB_LENGTH_SIZE));
        // Длина записи в таблице точек доступа
        stream.writeInteger(ACCESS_POINTS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу точек доступа
        int apTablePtrOffset = stream.writeZeros(TABLE_POINTER_SIZE);
        finalizers.add(constDb -> insertInteger(constDb, apTablePtrOffset, accessPointsTablePtr, TABLE_POINTER_SIZE));
        // Длина записи в таблице зон доступа
        stream.writeInteger(ACCESS_REGIONS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу зон доступа
        int arTablePtrOffset = stream.writeZeros(TABLE_POINTER_SIZE);
        finalizers.add(constDb -> insertInteger(constDb, arTablePtrOffset, accessRegionsTablePtr, TABLE_POINTER_SIZE));
        // Длина записи в таблице абонентов
        stream.writeInteger(EMPLOYEES_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу абонентов
        stream.writeInteger(EMPLOYEES_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу списков точек доступа
        stream.writeInteger(ACCESS_POINT_LISTS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице ключей
        stream.writeInteger(CARD_ACCESS_KEYS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу ключей
        stream.writeInteger(CARD_ACCESS_KEYS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице паролей
        stream.writeInteger(PASSWORD_ACCESS_KEYS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу паролей
        stream.writeInteger(PASSWORD_ACCESS_KEYS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу списков охранных зон
        stream.writeInteger(SECURITY_REGION_LISTS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу списков сценариев
        stream.writeInteger(SCENARIO_LISTS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице графиков
        stream.writeInteger(WORK_SCHEMAS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу графиков
        stream.writeInteger(WORK_SCHEDULES_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице временных интервалов
        stream.writeInteger(ACCESS_GRANTED_TIMES_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу временных интервалов
        stream.writeInteger(ACCESS_GRANTED_TIMES_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу именованных интервалов
        stream.writeInteger(ACCESS_GRANTED_TIME_LISTS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице недельных схем работы
        stream.writeInteger(WEEK_WORK_SCHEMAS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу недельных схем работы
        stream.writeInteger(WEEK_WORK_SCHEMAS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу посменных схем работы
        stream.writeInteger(OTHER_WORK_SCHEMAS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Длина записи в таблице месячных схем работы
        stream.writeInteger(MONTH_WORK_SCHEMAS_TABLE_ITEM_SIZE, TABLE_ITEM_LENGTH_SIZE);
        // Указатель на таблицу месячных схем работы
        stream.writeInteger(MONTH_WORK_SCHEMAS_TABLE_OFFSET, TABLE_POINTER_SIZE);
        // Указатель на таблицу праздников
        stream.writeInteger(HOLIDAYS_TABLE_OFFSET, TABLE_POINTER_SIZE);
    }

    /**
     * Формирование таблицы точек доступа
     */
    private void generateAccessPointsTable(DatabaseOutputStream stream, List<Consumer<byte[]>> finalizers) {
        // Профили всех поддерживаемых точек доступа
        Map<String, DeviceProfile> accessPointProfiles = new HashMap<>();
        for (DeviceProfile deviceProfile : stateMachineContext.getSupportedDeviceProfiles().values())
             if (deviceProfile.isAccessPoint())
                accessPointProfiles.put(deviceProfile.getId(), deviceProfile);

        // Проекты точек доступа
        List<Device> accessPointProjects = new ArrayList<>();
        for (ActiveDevice activeDevice : stateMachineContext.getChildDevices()) {
            String deviceProfileId = activeDevice.getDeviceProject().getDeviceProfileId();
            if (accessPointProfiles.containsKey(deviceProfileId))
                accessPointProjects.add(activeDevice.getDeviceProject());
        }

        // Сортировка по линии и адресу
        accessPointProjects.sort((dev0, dev1) -> {
            if (dev0.getLineNo() != dev1.getLineNo()) {
                return dev0.getLineNo() - dev1.getLineNo();
            } else {
                return dev0.getLineAddress() - dev1.getLineAddress();
            }
        });

        // Запись элементов таблицы
        for (Device accessPoint : accessPointProjects) {
            DeviceProfile accessPointProfile = accessPointProfiles.get(accessPoint.getDeviceProfileId());

            stateMachineContext.getLogger().debug("Access point table entry at {}: line {}, addr {}",
                    stream.size(), accessPoint.getLineNo() - 1, accessPoint.getLineAddress());

            // Номер шлейфа (Если FF в адресе и шлейфе то  это считыватель на приборе)
            stream.writeInteger(accessPoint.getLineNo() - 1, LINE_NUMBER_FIELD_SIZE);
            // Адрес устройства
            stream.writeInteger(accessPoint.getLineAddress(), LINE_ADDRESS_FIELD_SIZE);
            // Зона расположения (Указатель на зону в соответствующей таблице)
            // Зона назначения (Указатель на зону в соответствующей таблице)
            String regionFromId = DeviceProperty.getString(accessPoint, DeviceProperties.RegionFromId);
            String regionToId = DeviceProperty.getString(accessPoint, DeviceProperties.RegionToId);
            int regionFromPtrOffset = stream.writeZeros(TABLE_ENTRY_POINTER_SIZE);
            int regionToPtrOffset = stream.writeZeros(TABLE_ENTRY_POINTER_SIZE);
            finalizers.add(constDb -> {
                Integer regionFromPtr = regionsTableItemPtrs.get(regionFromId);
                Integer regionToPtr = regionsTableItemPtrs.get(regionToId);
                if (regionFromPtr == null || regionToPtr == null) {
                    addWarning(Code.ACCESS_POINT_WRONG_REGIONS, EntityType.DEVICE, accessPoint.getId());
                    if (regionFromPtr == null)
                        regionFromPtr = 0;
                    if (regionToPtr == null)
                        regionToPtr = 0;
                } else if (regionFromPtr == regionToPtr) {
                    addWarning(Code.ACCESS_POINT_SAME_REGIONS, EntityType.DEVICE, accessPoint.getId());
                }
                insertInteger(constDb, regionFromPtrOffset, regionFromPtr, TABLE_ENTRY_POINTER_SIZE);
                insertInteger(constDb, regionToPtrOffset, regionToPtr, TABLE_ENTRY_POINTER_SIZE);
            });
            // Настройка правила "antipassback"
            // ('000' - выключен, '001' - уведомления, '010' - таймаут, '011' - контроль по зонам)
            // Настройка режима доступа
            // '000' - нормальный, '001' - двойная идентификация, '010' - правило двух лиц
            int apbRules = DeviceProperty.getInteger(accessPoint, accessPointProfile, DeviceProperties.APBRules);
            int accessMode = DeviceProperty.getInteger(accessPoint, accessPointProfile, DeviceProperties.AccessMode);
            apbRules = (apbRules & ANTIPASSBACK_RULES_FIELD_MASK) << ANTIPASSBACK_RULES_FIELD_SHIFT;
            accessMode = (accessMode & ACCESS_MODE_FIELD_MASK) << ACCESS_MODE_FIELD_SHIFT;
            int accessPointSettings = apbRules | accessMode;
            stream.writeInteger(accessPointSettings, ACCESS_POINT_SETTINGS_FIELD_SIZE);
        }
    }

    /**
     * Формирование таблицы зон доступа
     */
    private void generateAccessRegionsTable(DatabaseOutputStream stream) {

        // Проекты зон СКУД
        List<Region> regionProjects = new ArrayList<>();
        for (ActiveRegion activeRegion : stateMachineContext.getRegions())
            if (activeRegion.getRegionProject().getSubsystem() == Subsystem.SKUD)
                regionProjects.add(activeRegion.getRegionProject());

        // Сортировка по индексу
        regionProjects.sort(Comparator.comparingInt(Region::getIndex));

        // Запись элементов таблицы
        for (Region region : regionProjects) {
            // Сохранение указателя на элемент таблицы
            regionsTableItemPtrs.put(region.getId(), CONST_DB_PART_OFFSET + stream.size());

            stateMachineContext.getLogger().debug("Access region table entry at {}: index {}, name {}",
                    stream.size(), region.getIndex(), region.getName());

            // Индекс зоны для Firesec
            stream.writeIntegerLittleEndian(region.getIndex(), REGION_INDEX_FIELD_SIZE);
            // Наименование зоны
            stream.writeText(region.getName(), REGION_NAME_FIELD_SIZE);
        }
    }

    public void addWarning(Code code, EntityType entityType, String entityId) {
        validateMessages.add(new ActivationValidateMessage(Level.WARNING, code, entityType, entityId));
    }

    public void addError(Code code, EntityType entityType, String entityId) {
        validateMessages.add(new ActivationValidateMessage(Level.ERROR, code, entityType, entityId));
        databaseValid = false;
    }

}
