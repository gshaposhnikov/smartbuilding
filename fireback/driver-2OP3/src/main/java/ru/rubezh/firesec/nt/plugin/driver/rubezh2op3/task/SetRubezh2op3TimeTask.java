package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.util.Date;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SetRubezh2op3TimeTask extends Task {

    public SetRubezh2op3TimeTask() {
        super(1000);
    }

    public SetRubezh2op3TimeTask(String issueId_) {
        super(1000, issueId_);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().setDeviceTime(currentTimeMs, new Date());
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs, issueId, IssueStatus.IN_PROGRESS,
                "");
        return State.WAITING_SET_RUBEZH2OP3_TIME;
    }

}
