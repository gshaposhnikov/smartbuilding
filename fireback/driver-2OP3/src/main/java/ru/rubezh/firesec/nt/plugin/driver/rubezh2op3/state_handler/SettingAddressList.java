package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class SettingAddressList extends AbstractTaskState {

    public static final int TECHNOLOGY_ADDRESS = 255;
    public static final int ADAPTER_MODULE_ADDRESS = 33;

    @Override
    public State getCurrentState() {
        return State.SETTING_ADDRESS_LIST;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        SettingAddressList handler = new SettingAddressList();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    private List<Integer> getActualAddressList() {
        List<Integer> addressList = new ArrayList<>();

        // адреса приборов
        Device controlDevice = stateMachineContext.getRubezh2op3Device().getDeviceProject();
        for (ActiveDevice activeDevice : stateMachineContext.getAllProjectDevicesByIds().values()) {
            Device device = activeDevice.getDeviceProject();
            if (device.getDeviceCategory() == DeviceCategory.CONTROL &&
                    device.getLineNo() == controlDevice.getLineNo() &&
                    device.getParentDeviceId() != null &&
                    device.getParentDeviceId().equals(controlDevice.getParentDeviceId())) {
                addressList.add(device.getLineAddress());
            }
        }
        // адрес МС
        addressList.add(ADAPTER_MODULE_ADDRESS);
        // технологический адрес
        addressList.add(TECHNOLOGY_ADDRESS);

        Collections.sort(addressList);
        return addressList;
    }

    @Override
    public State onAddressListReceived(long currentTimeMs, List<Integer> addressList) {
        List<Integer> actualAddressList = getActualAddressList();
        if (!addressList.equals(actualAddressList)) {
            stateMachineContext.getActionExecutor().setAddressList(currentTimeMs, actualAddressList);
            return getCurrentState();
        } else {
            stateMachineContext.getLogger().debug("Current address list is actual");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onAddressListSent(long currentTimeMs, int waitingTimeMs) {
        if (waitingTimeMs != 0) {
            stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK,
                    waitingTimeMs, false);
            return getCurrentState();
        } else {
            stateMachineContext.getLogger().debug("Address list write completed");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        super.onTimer(currentTimeMs, timerName);
        if (timerName.equals(TimerNames.LONG_OPERATION_CHECK)) {
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
            return getCurrentState();
        }
        return getCurrentState();
    }

    @Override
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
        if (!isReady) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK,
                    stateMachineContext.getLongOperationWaitingTimeMs(), false);
        } else {
            stateMachineContext.getLogger().debug("Address list write completed (long operation)");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
        return getCurrentState();
    }

}
