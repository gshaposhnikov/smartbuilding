package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

public class ModeChecking extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.MODE_CHECKING;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        ModeChecking handler = new ModeChecking();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType mode) {
        stateMachineContext.setRubezh2op3DeviceMode(mode);
        stateMachineContext.getStateMachineHooks().onConnected(currentTimeMs);
        if (mode == Rubezh2OP3UpdateSoftwareStatusType.NORMAL_OPERATION) {
            stateMachineContext.getStateMachineHooks().onFirmwareUpdateResumed(currentTimeMs);
            stateMachineContext.getActionExecutor().getControlDeviceInfo(currentTimeMs);
            stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
            stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
            return State.GETTING_RUBEZH2OP3_INFO;
        }
        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
