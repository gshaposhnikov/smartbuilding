package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.*;

import ru.rubezh.firesec.nt.amqp.message.alert.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent.AccessKeyValueInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;

public class Rubezh2OP3StateMachineHooksImpl implements Rubezh2OP3StateMachineHooks {

    /** Окружение машины состояний */
    private Rubezh2OP3StateMachineContext stateMachineContext;

    @Override
    public Rubezh2OP3StateMachineContext getStateMachineContext() {
        return stateMachineContext;
    }

    @Override
    public void setStateMachineContext(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    @Override
    public void onControlDeviceStatesReceived(long currentTimeMs, List<String> deviceStates) {
        Set<String> newActiveStates = new HashSet<>(deviceStates);
        ActiveDevice controlDevice = stateMachineContext.getRubezh2op3Device();
        if (!controlDevice.getActiveStates().equals(newActiveStates)) {
            controlDevice.setActiveStates(newActiveStates);
            SetActiveDeviceStates alert = new SetActiveDeviceStates(controlDevice.getId(), deviceStates);
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onControlDeviceStatesReceiveFailed(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error("Error getting active states: {}", exception);
    }

    @Override
    public void onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo controlDeviceInfo) {
        ControlDeviceInfo alert = new ControlDeviceInfo();
        alert.setDeviceId(stateMachineContext.getRubezh2op3Device().getId());
        alert.setGuid(controlDeviceInfo.getGuid());
        alert.setSerial(controlDeviceInfo.getSerial());
        alert.setDatabaseMatchingProject(controlDeviceInfo.isDatabaseMatchingProject());

        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onControlDeviceInfoReceiveFailed(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error("Error getting control device information: {}", exception);
    }

    @Override
    public void onConnected(long currentTimeMs) {
        ru.rubezh.firesec.nt.domain.v1.State deviceLostState = stateMachineContext.getRubezh2op3DeviceProfile()
                .getDeviceLostState();
        if (deviceLostState != null && deviceLostState.getId() != null) {
            RemoveActiveDeviceState alert = new RemoveActiveDeviceState(
                    stateMachineContext.getRubezh2op3Device().getId(), deviceLostState.getId());
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onDisconnected(long currentTimeMs) {
        ru.rubezh.firesec.nt.domain.v1.State deviceLostState = stateMachineContext.getRubezh2op3DeviceProfile()
                .getDeviceLostState();
        if (deviceLostState != null && deviceLostState.getId() != null) {
            AddActiveDeviceState alert = new AddActiveDeviceState(stateMachineContext.getRubezh2op3Device().getId(),
                    deviceLostState.getId());
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onFirmwareUpdateFailed(long currentTimeMs) {
        String updateFailureStateId = stateMachineContext.getRubezh2op3DeviceProfile().getFirmwareUpdateFailureStateId();
        if (updateFailureStateId != null && !updateFailureStateId.isEmpty()) {
            AddActiveDeviceState alert = new AddActiveDeviceState(
                    stateMachineContext.getRubezh2op3Device().getId(),
                    updateFailureStateId);
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onFirmwareUpdateResumed(long currentTimeMs) {
        String updateFailureStateId = stateMachineContext.getRubezh2op3DeviceProfile().getFirmwareUpdateFailureStateId();
        if (updateFailureStateId != null && !updateFailureStateId.isEmpty()) {
            RemoveActiveDeviceState alert = new RemoveActiveDeviceState(
                    stateMachineContext.getRubezh2op3Device().getId(),
                    updateFailureStateId);
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onLogEventReceived(long currentTimeMs, ControlDeviceEvent event) {
        stateMachineContext.getLogger().debug("Event received: {}", event.toString());
    }

    @Override
    public void onLogEventReceiveFailed(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error("Error getting log event: {}", exception);
    }

    @Override
    public void onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates) {
        stateMachineContext.getLogger().debug("Region {} states: {}", regionId, regionStates);
    }

    @Override
    public void onRegionStatesReceiveFailed(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error("Error getting region states: {}", exception);
    }

    @Override
    public void onChildDeviceStatesReceived(long currentTimeMs, String deviceId, List<String> deviceStates,
                                            Map<String, String> values, int qos) {
        // TODO: реализовать отправку qos в виде стандартных наблюдаемых параметров
        stateMachineContext.getLogger().debug("Device {} qos: {}", deviceId, qos);
        stateMachineContext.getDriverAlertSender().send(new SetActiveDeviceStates(deviceId, deviceStates));
        stateMachineContext.getDriverAlertSender().send(new SetActiveDeviceMonitorableValues(deviceId, values));
    }

    @Override
    public void onChildDeviceStatesReceiveFailed(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error("Error getting child device states: {}", exception);
    }

    @Override
    public void onChildDeviceConfigReceived(long currentTimeMs, String deviceId, Map<String, String> propertyValues,
            String serialNumber, String firmwareVersion) {
        ActiveDeviceConfig alert = new ActiveDeviceConfig();
        alert.setDeviceId(deviceId);
        alert.setConfigPropertyValues(propertyValues);
        alert.setAction(IssueAction.READ_CONFIG_FROM_DEVICE);
        alert.setSerialNumber(serialNumber);
        alert.setFirmwareVersion(firmwareVersion);
        alert.setPerformedDateTime(new Date());
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onChildDeviceConfigReceiveFailed(long currentTimeMs, String deviceId) {
        stateMachineContext.getLogger().debug("Error getting child device config \"{}\"", deviceId);
    }

    @Override
    public void onControlDeviceConfigReceived(long currentTimeMs, String deviceId,
            String serialNumber, String firmwareVersion, String databaseVersion) {
        ActiveDeviceConfig alert = new ActiveDeviceConfig();
        alert.setDeviceId(deviceId);
        alert.setAction(IssueAction.READ_CONFIG_FROM_DEVICE);
        alert.setSerialNumber(serialNumber);
        alert.setFirmwareVersion(firmwareVersion);
        alert.setDatabaseVersion(databaseVersion);
        alert.setPerformedDateTime(new Date());
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onControlDeviceConfigReceiveFailed(long currentTimeMs, String deviceId) {
        stateMachineContext.getLogger().debug("Error getting Rubezh2OP3 config \"{}\"", deviceId);
    }

    @Override
    public void onChildDeviceConfigSetReceived(long currentTimeMs, String deviceId, Map<String, String> propertyValues) {
        ActiveDeviceConfig alert = new ActiveDeviceConfig();
        alert.setDeviceId(deviceId);
        alert.setAction(IssueAction.WRITE_CONFIG_TO_DEVICE);
        alert.setConfigPropertyValues(propertyValues);
        alert.setPerformedDateTime(new Date());
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onChildDeviceConfigSetReceiveFailed(long currentTimeMs, String deviceId) {
        stateMachineContext.getLogger().debug("Error setting child device config \"{}\"", deviceId);
    }

    @Override
    public void onStateReset(long currentTimeMs, String deviceId, ManualResetStateGroup group) {
        stateMachineContext.getLogger().debug("Device state was reset. Device id: {}, state group: {}", deviceId,
                group);
    }

    @Override
    public void onStateResetFailed(long currentTimeMs, String deviceId, ManualResetStateGroup group) {
        stateMachineContext.getLogger().debug("Error reset status \"{}\" on device \"{}\"", group, deviceId);
    }

    @Override
    public void onIssueProgressChange(long currentTimeMs, String issueId, double progress) {
        IssueProgress alert = new IssueProgress();
        alert.setIssueId(issueId);
        alert.setProgress(progress);
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onIssueStatusChange(long currentTimeMs, String issueId, IssueStatus status, String statusMessage) {
        IssueProgress alert = new IssueProgress();
        alert.setIssueId(issueId);
        alert.setStatus(status);
        alert.setStatusMessage(statusMessage);
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onSecurityRegionStatusChangeFailed(long currentTimeMs, String controlDeviceId) {
        stateMachineContext.getLogger().debug("Error change security region status on control device \"{}\"", controlDeviceId);
    }

    @Override
    public void onDevicePollingStateSetReceived(long currentTimeMs, String deviceId, boolean pollingState) {
        stateMachineContext.getLogger().debug("Device '{}' polling state: {}", deviceId, pollingState);
    }

    @Override
    public void onDevicePollingStateSetReceiveFailed(long currentTimeMs, String deviceId) {
        stateMachineContext.getLogger().debug("Error setting device polling state \"{}\"", deviceId);
    }

    @Override
    public void onRubezh2op3TimeSet(long currentTimeMs, String controlDeviceId) {
        stateMachineContext.getLogger().debug("Control device '{}' time set", controlDeviceId);
    }

    @Override
    public void onRubezh2op3TimeSetFailed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Error setting control device time");
    }

    @Override
    public void onRubezh2op3PasswordSet(long currentTimeMs, String controlDeviceId) {
        stateMachineContext.getLogger().debug("Set control device user password success");
    }

    @Override
    public void onRubezh2op3PasswordSetFailed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Error setting control device user password");
    }

    @Override
    public void onDeviceActionPerformed(long currentTimeMs, String deviceId, String actionId) {
        stateMachineContext.getLogger().debug("Device " + deviceId + "action " + actionId + " performed");
    }

    @Override
    public void onDeviceActionFailed(long currentTimeMs, String deviceId, String actionId) {
        stateMachineContext.getLogger().debug("Device " + deviceId + "action " + actionId + " failed");
    }

    @Override
    public void onScenarioActionPerformed(long currentTimeMs, String scenarioId, ScenarioManageAction manageAction) {
        stateMachineContext.getLogger().debug("Scenario " + scenarioId + "action " + manageAction.toString() + " performed");
    }

    @Override
    public void onScenarioActionFailed(long currentTimeMs, String scenarioId, ScenarioManageAction manageAction) {
        stateMachineContext.getLogger().debug("Scenario " + scenarioId + "action " + manageAction.toString() + " failed");
    }

    @Override
    public void onDatabaseConfigRecovered(long currentTimeMs, String issueId,
            ControlDeviceEntities currentEntities, ControlDeviceEntities recoveredEntities) {
        stateMachineContext.getLogger().debug("Database configuration recovered: " +
                "{} devices, {} virtual states, {} scenarios, {} regions (created {})",
                recoveredEntities.getDevices().size(),
                recoveredEntities.getVirtualStates().size(),
                recoveredEntities.getScenarios().size(),
                recoveredEntities.getRegions().size(),
                recoveredEntities.getCreated()
        );
        DatabaseReadingCompleted alert = new DatabaseReadingCompleted();
        alert.setIssueId(issueId);
        alert.setCurrentEntities(currentEntities);
        alert.setRecoveredEntities(recoveredEntities);
        stateMachineContext.getDriverAlertSender().send(alert);
    }

    @Override
    public void onDatabaseConfigRecoveryFailed(long currentTimeMs, String reason) {
        stateMachineContext.getLogger().debug("Database configuration recovery failed: {}", reason);
    }

    @Override
    public void onAccessKeyValueRead(long currentTimeMs, String accessKeyId, String accessKeyValue) {
        stateMachineContext.getLogger().debug("Access key value read, id: {}, value: {}", accessKeyId, accessKeyValue);
        AccessKeyValueAlert accessKeyValueAlert = new AccessKeyValueAlert();
        accessKeyValueAlert.setAccessKeyValueInfo(new AccessKeyValueInfo(accessKeyId, accessKeyValue));
        stateMachineContext.getDriverAlertSender().send(accessKeyValueAlert);
    }

}
