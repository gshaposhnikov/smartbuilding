package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.GetRubezh2op3ConfigTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;

public class WaitingRubezh2op3Config extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_RUBEZH2OP3_CONFIG;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingRubezh2op3Config handler = new WaitingRubezh2op3Config();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceConfiguration) {
        try {
            GetRubezh2op3ConfigTask task = (GetRubezh2op3ConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            task.setSerialNumber(deviceConfiguration.getSerial());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getActionExecutor().getFirmwareVersion(currentTimeMs);

        return getCurrentState();
    }

    @Override
    public State onFirmwareVersionReceived(long currentTimeMs, String firmwareVersion) {
        try {
            GetRubezh2op3ConfigTask task = (GetRubezh2op3ConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            task.setFirmwareVersion(firmwareVersion);
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getActionExecutor().getDatabaseVersion(currentTimeMs);

        return getCurrentState();
    }

    @Override
    public State onDatabaseVersionReceived(long currentTimeMs, String databaseVersion) {
        try {
            GetRubezh2op3ConfigTask task = (GetRubezh2op3ConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onControlDeviceConfigReceived(currentTimeMs,
                    task.getActiveDevice().getId(), task.getSerialNumber(), task.getFirmwareVersion(), databaseVersion);
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        try {
            GetRubezh2op3ConfigTask task = (GetRubezh2op3ConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onControlDeviceConfigReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            GetRubezh2op3ConfigTask task = (GetRubezh2op3ConfigTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onControlDeviceConfigReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
