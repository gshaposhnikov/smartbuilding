package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetChildDeviceStatesRQ extends Rubezh2OP3GetParameterRQ {

    private int lineNo;
    private int address;
    private String regionId;

    public Rubezh2OP3GetChildDeviceStatesRQ(int lineNo, int address, String regionId) {
        super(Rubezh2OP3ParameterType.CHILD_DEVICE_STATES);
        this.lineNo = lineNo;
        this.address = address;
        this.regionId = regionId;
    }

    public int getLineNo() {
        return lineNo;
    }

    public int getAddress() {
        return address;
    }

    public String getRegionId() {
        return regionId;
    }

}
