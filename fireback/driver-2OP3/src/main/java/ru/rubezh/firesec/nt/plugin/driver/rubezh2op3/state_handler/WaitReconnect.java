package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class WaitReconnect extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.WAIT_RECONNECT;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitReconnect handler = new WaitReconnect();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.RECONNECT)) {
            stateMachineContext.getActionExecutor().connect(currentTimeMs);
            return State.CONNECTING;
        } else {
            return getCurrentState();
        }
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        return State.IDLE;
    }
}
