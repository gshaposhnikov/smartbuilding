package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/** Статус прибора */
public enum Rubezh2OP3UpdateSoftwareStatusType {
    NORMAL_OPERATION(0x00),
    UPDATE_ARM(0x01),
    UPDATE_AVR(0x02),
    UPDATE_EEPROM(0x04),
    UPDATE_LOADER(0x08);

    private final int value;
    private Rubezh2OP3UpdateSoftwareStatusType(int value) {
        this.value = value;
    }

    private static final Map<Integer, Rubezh2OP3UpdateSoftwareStatusType> typesByValue = new HashMap<>();
    static {
        for (Rubezh2OP3UpdateSoftwareStatusType type : Rubezh2OP3UpdateSoftwareStatusType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<Rubezh2OP3UpdateSoftwareStatusType, Integer> valuesByType = new EnumMap<>(Rubezh2OP3UpdateSoftwareStatusType.class);
    static {
        for (Rubezh2OP3UpdateSoftwareStatusType type : Rubezh2OP3UpdateSoftwareStatusType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static Rubezh2OP3UpdateSoftwareStatusType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(Rubezh2OP3UpdateSoftwareStatusType type) {
        return valuesByType.get(type);
    }
}
