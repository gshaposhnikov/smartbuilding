package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.function.BiConsumer;
import java.util.function.Function;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3CommonRS;

/** Запрос к прибору
 * Включает:
 *  - отправку запроса
 *  - ожидание ответа
 *  - повтор запроса при INVALID_DEVICE_REPLY
 *  - вызов однотипных колбеков при неудачном выполнении запроса
 *  - вызов лямбды при удачном выполнении запроса
 *
 * @author Андрей Лисовой
 */
public class Rubezh2OP3Request {

    /** Закодированный запрос */
    private ByteBuffer requestRaw;
    /** Число оставшихся попыток */
    private int attempts;
    /** Интервал между попытками */
    private int interval;
    /** Функция, декодирующая ответ */
    private Function<ByteBuffer, Rubezh2OP3CommonRS> decoder;
    /** Функция, выполняемая при удачном выполнении запроса */
    private BiConsumer<Rubezh2OP3CommonRS, Long> onSuccess;
    /** Функция, выполняемая при неудачном выполнении запроса (опциональная) */
    private BiConsumer<Rubezh2OP3CommonRS, Long> onFail;
    /** Контекст конечного автомата драйвера */
    private Rubezh2OP3StateMachineContext stateMachineContext;

    public Rubezh2OP3Request(Rubezh2OP3StateMachineContext stateMachineContext,
            int attempts, int interval, ByteBuffer requestRaw,
            Function<ByteBuffer, Rubezh2OP3CommonRS> decoder,
            BiConsumer<Rubezh2OP3CommonRS, Long> onSuccess) {
        this.stateMachineContext = stateMachineContext;
        this.requestRaw = requestRaw;
        this.attempts = attempts;
        this.interval = interval;
        this.decoder = decoder;
        this.onSuccess = onSuccess;
    }

    public Rubezh2OP3Request(Rubezh2OP3StateMachineContext stateMachineContext, int attempts,
            int interval, ByteBuffer requestRaw, Function<ByteBuffer, Rubezh2OP3CommonRS> decoder,
            BiConsumer<Rubezh2OP3CommonRS, Long> onSuccess,
            BiConsumer<Rubezh2OP3CommonRS, Long> onFail) {
        this.stateMachineContext = stateMachineContext;
        this.requestRaw = requestRaw;
        this.attempts = attempts;
        this.interval = interval;
        this.decoder = decoder;
        this.onSuccess = onSuccess;
        this.onFail = onFail;
    }

    public void run(long currentTimeMs) {
        stateMachineContext.getTransport().send(currentTimeMs, requestRaw,
                this::requestSended, this::sendReceiveFailed);
    }

    protected void requestSended(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getTransport().receive(currentTimeMs, sequenceNo,
                this::resultReceived, this::sendReceiveFailed, this::receiveTimeout);
    }

    protected void resultReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        try {
            Rubezh2OP3CommonRS response = decoder.apply(rawData);
            if (response.isSuccess()) {
                onSuccess.accept(response, currentTimeMs);
            } else if (onFail != null) {
                onFail.accept(response, currentTimeMs);
            } else {
                if (response.getExceptionType() == ExceptionType.INVALID_DEVICE_REPLY && --attempts > 0) {
                    stateMachineContext.getLogger().warn("INVALID_DEVICE_REPLY, request: {}, attempts: {}",
                            sequenceNo, attempts);
                    stateMachineContext.getClock().setTimer(Integer.toString(sequenceNo), currentTimeMs, interval,
                            this::timerHandler, false);
                } else {
                    stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs,
                            response.getExceptionType());
                }
            }
        } catch (ClassCastException | BufferUnderflowException e) {
            stateMachineContext.getLogger().error(
                    "Wrong request or response type. Exception: ", e);
            stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs, ExceptionType.NONE);
        } catch (Exception e) {
            stateMachineContext.getLogger().error("Couldn't decode response. Exception: ", e);
            stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs, ExceptionType.NONE);
        }
    }

    protected void sendReceiveFailed(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
    }

    protected void receiveTimeout(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getStateHandler().onReceiveTimeout(currentTimeMs);
    }

    protected void timerHandler(long currentTimeMs) {
        run(currentTimeMs);
    }

}
