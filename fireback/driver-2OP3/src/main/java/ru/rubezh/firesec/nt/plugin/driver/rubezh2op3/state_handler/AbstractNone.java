package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;
import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceConfigInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.ChildDeviceStatesInfo;

abstract public class AbstractNone implements StateHandler {

    protected Rubezh2OP3StateMachineContext stateMachineContext;

    @Override
    public boolean isConnected() {
        return false;
    }

    private String getLogMessage() {
        StringBuilder message = new StringBuilder()
                .append("Rubezh2OP3 (address path: ")
                .append(stateMachineContext.getRubezh2op3Device().getAddressPath())
                .append(") wrong event for the state ")
                .append(getCurrentState().toString());
        return message.toString();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onIssueCloseTimer(long currentTimeMs, String issueId) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onControlDeviceStatesAndEventCountersReceived(long currentTimeMs, List<String> deviceStates,
            Map<String, Long> eventCounters) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onLogEventReceived(long currentTimeMs, ControlDeviceEvent logEvent, BitSet regionStates, String regionId) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceConfiguration) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceMaxEventCountersReceived(long currentTimeMs, Map<String, Long> maxEventCounters) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceMaxEventCountersReceiveFailed(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onChildDeviceStatesReceived(long currentTimeMs, ChildDeviceStatesInfo childDeviceStatesInfo) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onChildDeviceConfigReceived(long currentTimeMs, ChildDeviceConfigInfo childDeviceConfigInfo) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onChildDeviceConfigSet(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onResetStateDeviceReceived(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onSoftwareUpdateModeInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onResetInvoked(long currentTimeMs, int waitingTime) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onEraseSectorsInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onWriteMemoryInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareStatusReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareStatusType status) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onSetSecurityRegionStatusReceived(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onFirmwareVersionReceived(long currentTimeMs, String buildNumber) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDatabaseVersionReceived(long currentTimeMs, String databaseVersion) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onSetDevicePollingStateReceived(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceTimeSet(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceUserPasswordSet(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onFirmwareVersionSet(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDeviceActionPerformed(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onScenarioActionPerformed(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDataBlockReceived(long currentTimeMs, byte[] data) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onWaitingAccessKeyStatusSet(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onAddressListReceived(long currentTimeMs, List<Integer> addressList) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onAddressListSent(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

}
