package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class EmployeeTE implements SkudTableEntry {

    private static final int EMPLOYEE_NAME_SIZE = 20;
    private static final int EMPLOYEE_LOCATION_SIZE = 2;
    private static final int EMPLOYEE_LOCATION_VALUE = 0x0000;

    private String employeeId;

    public EmployeeTE(String employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        Employee employee = skudDatabaseBuilder.getStateMachineContext().getEmployees().get(employeeId);
        assert employee != null;

        stream.writeText(employee.getName(), EMPLOYEE_NAME_SIZE);
        writeId(employee.getIndex(), stream);
        stream.writeInteger(EMPLOYEE_LOCATION_VALUE, EMPLOYEE_LOCATION_SIZE);
        writeBitFlags(stream);
    }

}
