package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.domain.v1.ScenarioManageAction;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3PerformScenarioActionRQ extends Rubezh2OP3SetParameterRQ {

    private int globalScenarioNo;
    private ScenarioManageAction manageAction;

    public Rubezh2OP3PerformScenarioActionRQ(int globalScenarioNo, ScenarioManageAction manageAction) {
        super(Rubezh2OP3ParameterType.RESET_STATE);
        this.globalScenarioNo = globalScenarioNo;
        this.manageAction = manageAction;
    }

    public int getGlobalScenarioNo() {
        return globalScenarioNo;
    }

    public ScenarioManageAction getManageAction() {
        return manageAction;
    }

}
