package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

/*
 * Блок данных в памяти прибора
 */

public class DataBlock {
    /* Адрес начала блока */
    private int address;
    /* Данные блока */
    private byte[] data;

    public DataBlock(int address, byte[] data) {
        this.address = address;
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public int getAddress() {
        return address;
    }

    public int getSize() {
        return (data == null) ? 0 : data.length;
    }
}
