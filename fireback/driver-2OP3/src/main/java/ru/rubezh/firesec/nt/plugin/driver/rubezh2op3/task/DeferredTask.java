package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

/**
 * Класс для заданий, которые требуют выполенния каких-либо действий после выполнения
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public abstract class DeferredTask extends Task {

    public DeferredTask(int priority) {
        super(priority);
    }

    public abstract void onComplete(long currentTimeMs);

}
