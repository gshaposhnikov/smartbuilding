package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

/*
 * Диапазон секторов памяти прибора
 */

public class SectorsRange {
    /* Адрес первого байта блока */
    private int first;
    /* Адрес последнего байта блока */
    private int last;

    public SectorsRange(int first, int last) {
        this.first = first;
        this.last = last;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }
}
