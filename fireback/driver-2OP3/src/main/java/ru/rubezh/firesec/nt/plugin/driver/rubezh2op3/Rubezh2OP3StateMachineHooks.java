package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.BitSet;
import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;

/**
 * Набор методов, в которых должна реализовываться реакция на конкретные результаты работы конечного автомата драйвера модуля сопряжения,
 * но которые не должны при этом влиять на работу самого автомата.
 *
 * @author Антон Васильев
 * @author Артем Седанов
 *
 */
public interface Rubezh2OP3StateMachineHooks {
    public void onConnected(long currentTimeMs);
    public void onDisconnected(long currentTimeMs);

    public void onFirmwareUpdateFailed(long currentTimeMs);
    public void onFirmwareUpdateResumed(long currentTimeMs);

    public void onControlDeviceStatesReceived(long currentTimeMs, List<String> deviceStates);
    public void onControlDeviceStatesReceiveFailed(long currentTimeMs, ExceptionType exception);

    public void onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceConfiguration);
    public void onControlDeviceInfoReceiveFailed(long currentTimeMs, ExceptionType exception);

    public void onLogEventReceived(long currentTimeMs, ControlDeviceEvent event);
    public void onLogEventReceiveFailed(long currentTimeMs, ExceptionType exception);

    public void onRegionStatesReceived(long currentTimeMs, String regionId, BitSet regionStates);
    public void onRegionStatesReceiveFailed(long currentTimeMs, ExceptionType exception);

    public void onChildDeviceStatesReceived(long currentTimeMs, String deviceId, List<String> deviceStates, Map<String, String> values, int qos);
    public void onChildDeviceStatesReceiveFailed(long currentTimeMs, ExceptionType exception);

    public void onChildDeviceConfigReceived(long currentTimeMs, String deviceId, Map<String, String> propertyValues,
            String serialNumber, String firmwareVersion);
    public void onChildDeviceConfigReceiveFailed(long currentTimeMs, String deviceId);

    public void onControlDeviceConfigReceived(long currentTimeMs, String deviceId,
            String serialNumber, String firmwareVersion, String databaseVersion);
    public void onControlDeviceConfigReceiveFailed(long currentTimeMs, String deviceId);

    public void onChildDeviceConfigSetReceived(long currentTimeMs, String deviceId, Map<String, String> propertyValues);
    public void onChildDeviceConfigSetReceiveFailed(long currentTimeMs, String deviceId);

    public void onStateReset(long currentTimeMs, String deviceId, ManualResetStateGroup group);
    public void onStateResetFailed(long currentTimeMs, String deviceId, ManualResetStateGroup group);

    public void onIssueProgressChange(long currentTimeMs, String issueId, double progress);
    public void onIssueStatusChange(long currentTimeMs, String issueId, IssueStatus status, String statusMessage);

    /* Доступ к контексту конечного автомата */
    public Rubezh2OP3StateMachineContext getStateMachineContext();
    public void setStateMachineContext(Rubezh2OP3StateMachineContext stateMachineContext);

    public void onSecurityRegionStatusChangeFailed(long currentTimeMs, String controlDeviceId);

    public void onDevicePollingStateSetReceived(long currentTimeMs, String deviceId, boolean pollingState);
    public void onDevicePollingStateSetReceiveFailed(long currentTimeMs, String deviceId);

    public void onRubezh2op3TimeSet(long currentTimeMs, String controlDeviceId);
    public void onRubezh2op3TimeSetFailed(long currentTimeMs);

    public void onRubezh2op3PasswordSet(long currentTimeMs, String controlDeviceId);
    public void onRubezh2op3PasswordSetFailed(long currentTimeMs);

    public void onDeviceActionPerformed(long currentTimeMs, String deviceId, String actionId);
    public void onDeviceActionFailed(long currentTimeMs, String deviceId, String actionId);
    
    public void onScenarioActionPerformed(long currentTimeMs, String scenarioId, ScenarioManageAction manageAction);
    public void onScenarioActionFailed(long currentTimeMs, String scenarioId, ScenarioManageAction manageAction);

    public void onDatabaseConfigRecovered(long currentTimeMs, String issueId,
            ControlDeviceEntities currentEntities, ControlDeviceEntities recoveredEntities);
    public void onDatabaseConfigRecoveryFailed(long currentTimeMs, String reason);

    public void onAccessKeyValueRead(long currentTimeMs, String accessKeyId, String accessKeyValue);

}
