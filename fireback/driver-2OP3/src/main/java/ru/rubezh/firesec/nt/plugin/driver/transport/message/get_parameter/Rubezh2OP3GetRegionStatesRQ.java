package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetRegionStatesRQ extends Rubezh2OP3GetParameterRQ {

    private int regionNo;
    private String regionId;

    public Rubezh2OP3GetRegionStatesRQ(int regionNo, String regionId) {
        super(Rubezh2OP3ParameterType.REGION_STATES);
        this.regionNo = regionNo;
        this.regionId = regionId;
    }

    public int getRegionNo() {
        return regionNo;
    }

    public String getRegionId() {
        return regionId;
    }

}
