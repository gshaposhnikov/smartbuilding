package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetLogEventRQ extends Rubezh2OP3GetParameterRQ {

    private int logCode;
    private long eventIndex;

    public Rubezh2OP3GetLogEventRQ(int logCode, long eventIndex) {
        super(Rubezh2OP3ParameterType.LOG_EVENT);
        this.logCode = logCode;
        this.eventIndex = eventIndex;
    }

    public int getLogCode() {
        return logCode;
    }

    public long getEventIndex() {
        return eventIndex;
    }

}
