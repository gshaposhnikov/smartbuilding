package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class CheckModeTask extends Task {

    public CheckModeTask() {
        super(PRIORITY_LOW);
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getUpdateSoftwareStatus(currentTimeMs);
        return State.MODE_CHECKING;
    }

    @Override
    public boolean canBeExecuted() {
        return true;
    }

}
