package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActiveScenario;
import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock;

/**
 * Обертка для шаблона логики - хранит локальные индексы дочерних блоков, глубину и т.д.
 * Также поддерживает сравнение по глубине вложенности для сортировки.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class LogicBlockWrapper implements Comparable<LogicBlockWrapper> {
    /** Оборачиваемый шаблон логики */
    public ScenarioLogicBlock logicBlock;
    /** Сценарий, к которому принадлежит шаблон */
    public ActiveScenario scenario;
    /** Локальный индекс */
    public int idx;
    /** Глубина вложенности */
    public int depth;
    /** Индекс родительского шаблона */
    public int parentIdx;
    /** Флаг наличия условия выключения */
    public boolean hasStopLogic;
    /** Флаг логики включения */
    public boolean isStartLogic;
    /** Список локальных индексов дочерних шаблонов */
    public List<Integer> childIdxs;

    public LogicBlockWrapper(ScenarioLogicBlock logicBlock, ActiveScenario scenario, int idx, int depth,
            int parentIdx, boolean hasStopLogic, boolean isStartLogic) {
        this.logicBlock = logicBlock;
        this.scenario = scenario;
        this.idx = idx;
        this.depth = depth;
        this.parentIdx = parentIdx;
        this.hasStopLogic = hasStopLogic;
        this.isStartLogic = isStartLogic;
        this.childIdxs = new ArrayList<>();
    }

    @Override
    public int compareTo(LogicBlockWrapper o) {
        if (this.depth != o.depth)
            return o.depth - this.depth;
        else
            return this.idx - o.idx;
    }
}