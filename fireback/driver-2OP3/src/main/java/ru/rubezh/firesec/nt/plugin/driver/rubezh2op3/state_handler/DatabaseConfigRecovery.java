package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.Arrays;
import java.io.ByteArrayOutputStream;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEntities;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.DatabaseConfigRecoveryTask;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.Task;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DatabaseBuilder;

/**
 * Чтения резервной копии конфигурации прибора
 * 
 * @author Андрей Лисовой
 *
 */
public class DatabaseConfigRecovery extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.DATABASE_CONFIG_RECOVERY;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        DatabaseConfigRecovery handler = new DatabaseConfigRecovery();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    private void reportTaskAsFailed(long currentTimeMs, String reason, Task task) {
        stateMachineContext.getStateMachineHooks().onDatabaseConfigRecoveryFailed(currentTimeMs, reason);
        if (task != null)
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, reason);
    }

    private State closeTaskAsFailed(long currentTimeMs, String reason, Task task) {
        reportTaskAsFailed(currentTimeMs, reason, task);
        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    private State closeTaskAndDisconnect(long currentTimeMs, String reason, Task task) {
        reportTaskAsFailed(currentTimeMs, reason, task);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDataBlockReceived(long currentTimeMs, byte[] data) {
        DatabaseConfigRecoveryTask task = null;
        try {
            task = (DatabaseConfigRecoveryTask)stateMachineContext.getTaskManager().getCurrentTask();
        } catch (ClassCastException e) {
            return closeTaskAsFailed(currentTimeMs, e.toString(), null);
        }
        DatabaseBuilder databaseBuilder = stateMachineContext.getDatabaseBuilder();
        int headerLength = 0;
        int chunkSize = task.getReadChunkSize();
        int currentChunkNo = task.getCurrentChunk();
        if (currentChunkNo <= 0) {
            // первый блок
            headerLength = databaseBuilder.getRecoveryDatabaseHeaderLength();
            byte[] dataHeader = Arrays.copyOfRange(data, 0, headerLength);
            int dataLength = databaseBuilder.parseRecoveryDataLength(dataHeader);
            if (dataLength <= 0) {
                // не удалось разобрать заголовок
                return closeTaskAsFailed(currentTimeMs, "Прибор не содержит восстановительной информации", task);
            }
            task.setRecoveryData(new ByteArrayOutputStream(dataLength));
            task.setDataLength(headerLength + dataLength);
            task.setDataHeader(dataHeader);
            task.setLastChunk((headerLength + dataLength) / chunkSize);
        }
        // запоминаем считанные данные
        task.getRecoveryData().write(data, headerLength,
                Integer.min(task.getDataLength() - currentChunkNo * chunkSize, chunkSize - headerLength));
        if (currentChunkNo < task.getLastChunk()) {
            // очередной блок
            task.setCurrentChunk(++currentChunkNo);
            stateMachineContext.getActionExecutor().getDataBlock(currentTimeMs,
                    databaseBuilder.getRecoveryDatabaseAddress() + currentChunkNo * chunkSize, chunkSize);
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs,
                    task.getIssueId(), 100.0 * currentChunkNo / task.getLastChunk());
            return getCurrentState();
        } else {
            // последний блок
            ControlDeviceEntities recoveredEntities =
                    databaseBuilder.parseRecoveryDatabase(task.getDataHeader(), task.getRecoveryData().toByteArray());
            if (recoveredEntities == null) {
                // не удалось разобрать JSON
                return closeTaskAsFailed(currentTimeMs, "Восстановительная информация повреждена", task);
            }
            databaseBuilder.applyCurrentDevicesConfig(recoveredEntities.getDevices());
            ControlDeviceEntities currentEntities = databaseBuilder.getAllControlDeviceEntities();
            stateMachineContext.getStateMachineHooks().onDatabaseConfigRecovered(currentTimeMs,
                    task.getIssueId(), currentEntities, recoveredEntities);
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        Task task = null;
        try {
            task = stateMachineContext.getTaskManager().getCurrentTask();
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().error("{}", e);
        }
        return closeTaskAndDisconnect(currentTimeMs, "Ошибка протокола RSR3: " + exception.toString(), task);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        Task task = null;
        try {
            task = stateMachineContext.getTaskManager().getCurrentTask();
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().error("{}", e);
        }
        return closeTaskAndDisconnect(currentTimeMs, "Потеря связи", task);
    }

}
