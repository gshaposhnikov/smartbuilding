package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3UpdatingDatabaseStage;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.SectorsRange;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.WriteDatabaseTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareErrorType;


public class ErasingSectors extends AbstractWritingDatabase {

    @Override
    public State getCurrentState() {
        return State.ERASING_SECTORS;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        ErasingSectors handler = new ErasingSectors();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    private boolean erasePart(long currentTimeMs, SectorsRange range) {
        if (range == null)
            return false;

        stateMachineContext.getLogger().trace("first = {}, last = {}, progress = {}%",
                range.getFirst(), range.getLast(),
                (int)(stateMachineContext.getUpdatingDatabaseState().getSectorsProgress() * 100));

        stateMachineContext.getActionExecutor().eraseSectors(currentTimeMs,
                range.getFirst(), range.getLast());
        return true;
    }

    private void updateProgress(long currentTimeMs) {
        final float loaderProgress = WriteDatabaseTask.getLoaderUpdateProgress();
        final float erasingProgress = WriteDatabaseTask.getErasingSectorsProgress();
        float globalProgress = loaderProgress + (erasingProgress - loaderProgress) *
                stateMachineContext.getUpdatingDatabaseState().getSectorsProgress();
        try {
            WriteDatabaseTask task = (WriteDatabaseTask) stateMachineContext.getTaskManager().getCurrentTask();
            stateMachineContext.getStateMachineHooks().onIssueProgressChange(currentTimeMs, task.getIssueId(),
                    globalProgress);
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
    }

    private State jumpToNextStage(long currentTimeMs) {
        stateMachineContext.getUpdatingDatabaseState().setStage(Rubezh2OP3UpdatingDatabaseStage.WRITING_DATA);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.START_NEXT_STAGE, 10, false);
        return State.WRITING_DATABASE;
    }

    @Override
    public State onEraseSectorsInvoked(long currentTimeMs, int waitingTimeMs) {
        stateMachineContext.setLongOperationWaitingTimeMs(waitingTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK, waitingTimeMs,
                false);
        return getCurrentState();
    }

    @Override
    public State onDeviceReadyStateReceived(long currentTimeMs, boolean isReady) {
        if (isReady) {
            stateMachineContext.getActionExecutor().getUpdateSoftwareError(currentTimeMs);
        } else {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.LONG_OPERATION_CHECK,
                    stateMachineContext.getLongOperationWaitingTimeMs(), false);
        }
        return getCurrentState();
    }

    @Override
    public State onUpdateSoftwareErrorReceived(long currentTimeMs, Rubezh2OP3UpdateSoftwareErrorType error) {
        if (error == Rubezh2OP3UpdateSoftwareErrorType.SUCCESS) {
            updateProgress(currentTimeMs);
            if (!erasePart(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getNextSectorsRange()))
                return jumpToNextStage(currentTimeMs);
        } else {
            erasePart(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getLastSectorsRange());
        }
        return getCurrentState();
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        stateMachineContext.getUpdatingDatabaseState().eraseFailed();
        if (stateMachineContext.getUpdatingDatabaseState().shouldRetryErase()) {
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.DATABASE_WRITING_TIMER,
                    stateMachineContext.getStartWritingRetryTO(), false);
            return getCurrentState();
        } else {
            return onDisconnected(currentTimeMs);
        }
    }

    @Override
    public State onTimer(long currentTimeMs, String timerName) {
        if (timerName.equals(TimerNames.START_NEXT_STAGE)) {
            if (!erasePart(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getNextSectorsRange()))
                return jumpToNextStage(currentTimeMs);
        } else if (timerName.equals(TimerNames.DATABASE_WRITING_TIMER)) {
            stateMachineContext.getLogger().warn("Retrying erase sectors");
            erasePart(currentTimeMs, stateMachineContext.getUpdatingDatabaseState().getLastSectorsRange());
            return getCurrentState();
        } else if (timerName.equals(TimerNames.LONG_OPERATION_CHECK)) {
            stateMachineContext.getActionExecutor().checkDeviceReadyState(currentTimeMs);
        }
        return getCurrentState();
    }

}
