package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.ManualResetStateGroup;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class ResetDeviceStateTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveDevice activeDevice;
    private ManualResetStateGroup targetGroup;

    public ResetDeviceStateTask(ActiveDevice activeDevice, String issueId, ManualResetStateGroup targetGroup) {
        super(PRIORITY_DEFAULT);
        this.activeDevice = activeDevice;
        this.issueId = issueId;
        this.targetGroup = targetGroup;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetStateDevice(currentTimeMs,
                activeDevice.getDeviceProject().getLineNo(), activeDevice.getDeviceProject().getLineAddress(),
                targetGroup);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.WAITING_RESET_STATE_DEVICE;
    }

    public ActiveDevice getActiveDevice() {
        return activeDevice;
    }

    public void setActiveDevice(ActiveDevice activeDevice) {
        this.activeDevice = activeDevice;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public ManualResetStateGroup getTargetGroup() {
        return targetGroup;
    }

    public void setTargetGroup(ManualResetStateGroup targetGroup) {
        this.targetGroup = targetGroup;
    }
}
