package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.BitSet;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3ReadEventsState.EventDescriptor;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.CheckEventsTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class WaitingDeviceEvents extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_DEVICE_EVENTS;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingDeviceEvents handler = new WaitingDeviceEvents();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    // событие считано
    @Override
    public State onLogEventReceived(long currentTimeMs, ControlDeviceEvent logEvent, BitSet regionStates, String regionId) {
        if (regionStates != null && regionId != null)
            stateMachineContext.setRegionStateBits(regionId, regionStates);

        EventDescriptor receivedEvent = stateMachineContext.getFetchingEventsState().getCurrentEvent();
        EventDescriptor eventToRead = stateMachineContext.getFetchingEventsState().getNextEvent();

        /* "Битые" и неизвестные события игнорируем */
        if (logEvent != null && logEvent.getTypeId() != null) {
            logEvent.setLogTypeId(receivedEvent.logId);
            logEvent.setEventNo(receivedEvent.index);
            if (logEvent.getDeviceInfo() != null && logEvent.getDeviceInfo().isDeviceLocal())
                logEvent.getDeviceInfo().setControlDeviceAddress(stateMachineContext
                        .getRubezh2op3Device().getDeviceProject().getLineAddress());
            logEvent.getControlDeviceInfo().setId(stateMachineContext.getRubezh2op3Device().getId());

            stateMachineContext.getStateMachineHooks().onLogEventReceived(currentTimeMs, logEvent);

            // если событие содержит значение ключа доступа и оно ожидалось
            if (logEvent.getAccessKeyValue() != null
                    && stateMachineContext.getWaitingAccessKeyId() != null) {
                // вызываем хук для отправки сообщения наверх
                stateMachineContext.getStateMachineHooks().onAccessKeyValueRead(currentTimeMs,
                        stateMachineContext.getWaitingAccessKeyId(), logEvent.getAccessKeyValue());
                // больше не ожидаем значения ключа доступа
                stateMachineContext.setWaitingAccessKeyId(null);
            }

            // "полуфабрикат" события добавляется в контекст КА
            stateMachineContext.addEvent(logEvent);
            // и, при наполнении пачки, сразу отправляется в сервер БЛ для
            // записи в базу и последующего отображения
            // (в любом случае, драйвер отправляет все накопленные события раз в
            // EVENTS_SEND_TIMEOUT миллисекунд)
            if (stateMachineContext.checkEventsReadyToSend()) {
                stateMachineContext.getActionExecutor().forcedSendEventsUpstream(currentTimeMs);
            }
        }

        // отметка в задаче о считывании события
        boolean shouldReadAnotherEvent = false;
        try {
            CheckEventsTask task = (CheckEventsTask) stateMachineContext.getTaskManager().getCurrentTask();
            task.eventHasBeenRead();
            shouldReadAnotherEvent = task.shouldReadAnotherEvent();
        } catch (ClassCastException e) {
            stateMachineContext.getLogger().warn("Error casting Task");
        }
        // если в приборе есть непрочитанные события и не превышен лимит выборки событий за раз (batchEventsCount)
        if (shouldReadAnotherEvent && eventToRead != null) {
            // считывание слудующего события
            stateMachineContext.getActionExecutor().getEvent(currentTimeMs, eventToRead.logCode, eventToRead.index);
            return getCurrentState();
        } else {
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getStateMachineHooks().onLogEventReceiveFailed(currentTimeMs, exception);
        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
