package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import java.util.EnumMap;

import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleType;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

public class WorkScheduleTE implements SkudTableEntry {

    private static final int SCHEDULE_TYPE_SIZE = 1;
    private static final EnumMap<WorkScheduleType, Integer> SCHEDULE_TYPE_VALUE = new EnumMap<>(WorkScheduleType.class);
    static {
        SCHEDULE_TYPE_VALUE.put(WorkScheduleType.WEEK, 0);
        SCHEDULE_TYPE_VALUE.put(WorkScheduleType.MONTH, 1);
        SCHEDULE_TYPE_VALUE.put(WorkScheduleType.OTHER, 2);
    }
    private static final int HOLIDAYS_ID_STUB = 0xFFFF & 0xFFFF;
    private static final int FLAGS_WITH_IGNORE_HOLIDAYS = 0xFF & 0xFF;

    /** Идентификатор графика работ */
    private String workScheduleId;

    public WorkScheduleTE(String workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        WorkSchedule workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules().get(workScheduleId);
        assert workSchedule != null;

        writeId(workSchedule.getIndex(), stream);
        stream.writeInteger(SCHEDULE_TYPE_VALUE.get(workSchedule.getWorkScheduleType()), SCHEDULE_TYPE_SIZE);
        writeId(workSchedule.getIndex(), stream); // в качестве идентификатора смены пишем тот же идентификатор, т.к.
                                                  // они у нас совпадают
        writeId(HOLIDAYS_ID_STUB, stream);
        stream.writeInteger(FLAGS_WITH_IGNORE_HOLIDAYS, BIT_FLAGS_SIZE);
    }

}
