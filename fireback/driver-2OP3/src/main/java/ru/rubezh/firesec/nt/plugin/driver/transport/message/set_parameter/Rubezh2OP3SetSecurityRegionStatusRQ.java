package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetSecurityRegionStatusRQ extends Rubezh2OP3SetParameterRQ {

    private int regionNo = 0;

    private boolean onGuard = true;

    public Rubezh2OP3SetSecurityRegionStatusRQ() {
        super(Rubezh2OP3ParameterType.RESET_STATE);
    }

    public int getRegionNo() {
        return regionNo;
    }

    public void setRegionNo(int regionNo) {
        this.regionNo = regionNo;
    }

    public boolean isOnGuard() {
        return onGuard;
    }

    public void setOnGuard(boolean onGuard) {
        this.onGuard = onGuard;
    }

}
