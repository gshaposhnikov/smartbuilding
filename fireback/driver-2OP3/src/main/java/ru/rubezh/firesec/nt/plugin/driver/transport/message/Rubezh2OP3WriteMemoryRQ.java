package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3WriteMemoryRQ implements Rubezh2OP3Message {

    private int address;
    private byte[] data;

    public Rubezh2OP3WriteMemoryRQ(int address, byte[] data) {
        assert address % 256 == 0;
        assert data.length <= 256;
        this.address = address;
        this.data = data;
    }

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return Rubezh2OP3FunctionType.WRITE_MEMORY;
    }

    public int getAddress() {
        return address;
    }

    public byte[] getData() {
        return data;
    }

}
