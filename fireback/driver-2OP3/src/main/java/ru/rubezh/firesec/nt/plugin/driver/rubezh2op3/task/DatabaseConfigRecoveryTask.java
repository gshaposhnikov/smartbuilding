package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import java.io.ByteArrayOutputStream;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3UpdateSoftwareStatusType;

/**
 * Задача чтения резервной копии конфигурации прибора
 * 
 * @author Андрей Лисовой
 *
 */
public class DatabaseConfigRecoveryTask extends Task {

    private static final int READ_CHUNK_SIZE = 256;

    private int currentChunk = 0;
    private int lastChunk;
    private int dataLength;
    private byte[] dataHeader;
    private ByteArrayOutputStream recoveryData;

    public DatabaseConfigRecoveryTask(String issueId) {
        super(PRIORITY_DEFAULT);
        this.issueId = issueId;
    }

    @Override
    public State execute(long currentTimeMs) {
        int recoveryDatabaseAddress = stateMachineContext.getDatabaseBuilder().getRecoveryDatabaseAddress();
        stateMachineContext.getActionExecutor().getDataBlock(currentTimeMs, recoveryDatabaseAddress, READ_CHUNK_SIZE);
        stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                issueId, IssueStatus.IN_PROGRESS, "");
        return State.DATABASE_CONFIG_RECOVERY;
    }

    @Override
    public boolean canBeExecuted() {
        assert stateMachineContext.getRubezh2op3DeviceMode() != null : "Не считан режим прибора";

        return stateMachineContext.getRubezh2op3DeviceMode() == Rubezh2OP3UpdateSoftwareStatusType.NORMAL_OPERATION;
    }

    public int getReadChunkSize() {
        return READ_CHUNK_SIZE;
    }

    public int getCurrentChunk() {
        return currentChunk;
    }

    public void setCurrentChunk(int currentChunk) {
        this.currentChunk = currentChunk;
    }

    public int getLastChunk() {
        return lastChunk;
    }

    public void setLastChunk(int lastChunk) {
        this.lastChunk = lastChunk;
    }

    public int getDataLength() {
        return dataLength;
    }

    public void setDataLength(int dataLength) {
        this.dataLength = dataLength;
    }

    public ByteArrayOutputStream getRecoveryData() {
        return recoveryData;
    }

    public void setRecoveryData(ByteArrayOutputStream recoveryData) {
        this.recoveryData = recoveryData;
    }

    public byte[] getDataHeader() {
        return dataHeader;
    }

    public void setDataHeader(byte[] dataHeader) {
        this.dataHeader = dataHeader;
    }

}
