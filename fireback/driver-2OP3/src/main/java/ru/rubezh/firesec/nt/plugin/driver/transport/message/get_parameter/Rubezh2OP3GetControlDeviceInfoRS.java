package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetControlDeviceInfoRS extends Rubezh2OP3GetParameterRS {
    private Rubezh2OP3DeviceInfo deviceInfo;

    public Rubezh2OP3GetControlDeviceInfoRS() {
        super(Rubezh2OP3ParameterType.DB_BLOCK);
    }

    public Rubezh2OP3DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(Rubezh2OP3DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}
