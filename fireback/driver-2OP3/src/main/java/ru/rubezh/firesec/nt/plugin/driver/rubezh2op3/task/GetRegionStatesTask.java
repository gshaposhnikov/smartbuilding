package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

import ru.rubezh.firesec.nt.domain.v1.ActiveRegion;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public class GetRegionStatesTask extends ControlDeviceDBMatchingProjectRequiredTask {

    private ActiveRegion activeRegion;

    public GetRegionStatesTask(ActiveRegion activeRegion) {
        super(PRIORITY_VERY_LOW);
        this.activeRegion = activeRegion;
    }

    @Override
    public State execute(long currentTimeMs) {
        stateMachineContext.getActionExecutor().getRegionStates(currentTimeMs,
                activeRegion.getRegionProject().getId());

        return State.WAITING_REGION_STATES;
    }

    public ActiveRegion getActiveRegion() {
        return activeRegion;
    }

}
