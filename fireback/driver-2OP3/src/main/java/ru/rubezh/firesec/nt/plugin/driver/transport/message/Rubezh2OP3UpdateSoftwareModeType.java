package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum Rubezh2OP3UpdateSoftwareModeType {
    ARM(0x01),
    AVR(0x02),
    EEPROM(0x03),
    LOADER(0x04);

    private final int value;
    private Rubezh2OP3UpdateSoftwareModeType(int value) {
        this.value = value;
    }

    private static final Map<Integer, Rubezh2OP3UpdateSoftwareModeType> typesByValue = new HashMap<>();
    static {
        for (Rubezh2OP3UpdateSoftwareModeType type : Rubezh2OP3UpdateSoftwareModeType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<Rubezh2OP3UpdateSoftwareModeType, Integer> valuesByType = new EnumMap<>(Rubezh2OP3UpdateSoftwareModeType.class);
    static {
        for (Rubezh2OP3UpdateSoftwareModeType type : Rubezh2OP3UpdateSoftwareModeType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static Rubezh2OP3UpdateSoftwareModeType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(Rubezh2OP3UpdateSoftwareModeType type) {
        return valuesByType.get(type);
    }
}
