package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.skud;

import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.AccessGrantedTime;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.utils.DatabaseOutputStream;

/**
 * Элемент таблицы "именованных интервалов", по сути - списков интервалов.
 * Один список интервалов соответствует одному экземпляру дня графика работ.
 * 
 * @author Антон Васильев
 *
 */
public class AccessGrantedTimeListTE implements SkudTableEntry {

    private static final int TIMES_COUNT_SIZE = 1;

    /** Идентификатор рабочего графика */
    private String workScheduleId;
    /** Номер дня в графике работ */
    private int dayNo;

    public AccessGrantedTimeListTE(String workScheduleId, int dayNo) {
        this.workScheduleId = workScheduleId;
        this.dayNo = dayNo;
    }

    @Override
    public void write(SkudDatabaseBuilder skudDatabaseBuilder, DatabaseOutputStream stream) {
        WorkSchedule workSchedule = skudDatabaseBuilder.getStateMachineContext().getWorkSchedules().get(workScheduleId);
        assert workSchedule != null;
        assert dayNo < workSchedule.getDays().size();

        WorkScheduleDay day = workSchedule.getDays().get(dayNo);
        assert day != null;

        writeId(day.getIndex(), stream);
        stream.writeInteger(day.getAccessGrantedTimes().size(), TIMES_COUNT_SIZE);
        writeBitFlags(stream);
        for (AccessGrantedTime grantedTime : day.getAccessGrantedTimes()) {
            writeId(grantedTime.getIndex(), stream);
        }
    }

}
