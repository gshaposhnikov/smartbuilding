package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.SetDevicePollingStateTask;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class SetDevicePollingState extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_SET_DEVICE_POLLING_STATE;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        SetDevicePollingState handler = new SetDevicePollingState();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onSetDevicePollingStateReceived(long currentTimeMs) {
        try {
            SetDevicePollingStateTask task = (SetDevicePollingStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDevicePollingStateSetReceived(currentTimeMs,
                    task.getActiveDevice().getId(), task.isPollingState());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FINISHED, "");
            if (task.isPollingState())
                stateMachineContext.getChildDevicePollingState().addUrgentDeviceToPoll(task.getActiveDevice());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Error setting device polling state: {}", exception);

        try {
            SetDevicePollingStateTask task = (SetDevicePollingStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDevicePollingStateSetReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Ошибка протокола RSR3: " + exception.toString());
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        try {
            SetDevicePollingStateTask task = (SetDevicePollingStateTask) stateMachineContext.getTaskManager()
                    .getCurrentTask();
            stateMachineContext.getStateMachineHooks().onDevicePollingStateSetReceiveFailed(currentTimeMs,
                    task.getActiveDevice().getId());
            stateMachineContext.getStateMachineHooks().onIssueStatusChange(currentTimeMs,
                    task.getIssueId(), IssueStatus.FAILED, "Потеря связи");
        } catch (Exception e) {
            stateMachineContext.getLogger().error("{}", e);
        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
