package ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3SetAddressListRS extends Rubezh2OP3SetParameterRS {

    private int waitingTimeMs;

    public Rubezh2OP3SetAddressListRS() {
        super(Rubezh2OP3ParameterType.ADDRESS_LIST);
    }

    public int getWaitingTimeMs() {
        return waitingTimeMs;
    }

    public void setWaitingTimeMs(int waitingTimeMs) {
        this.waitingTimeMs = waitingTimeMs;
    }

}
