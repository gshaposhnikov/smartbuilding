package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3ReadEventsState;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3ReadEventsState.EventDescriptor;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class WaitingControlDeviceStatesAndEventCounters extends AbstractTaskState {

    @Override
    public State getCurrentState() {
        return State.WAITING_CONTROL_DEVICE_STATES_AND_EVENT_COUNTERS;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        WaitingControlDeviceStatesAndEventCounters handler = new WaitingControlDeviceStatesAndEventCounters();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    /** Статус прибора */
    public enum ControlDeviceStatus {
        /* новый (пустой UUID) */
        NEW,
        /* тот же (старый UUID) */
        SAME,
        /* заменён (новый UUID) */
        REPLACED
    };

    // Проверка идентификатора прибора
    private ControlDeviceStatus checkControlDeviceID() {
        assert stateMachineContext.getControlDeviceInfo() != null : "Не считана конфигурация прибора";

        String realGuid = stateMachineContext.getControlDeviceInfo().getGuid();
        String savedGuid = stateMachineContext.getRubezh2op3Device().getGuid();

        ControlDeviceStatus status;
        if (savedGuid.isEmpty())
            status = ControlDeviceStatus.NEW;
        else if (!savedGuid.equals(realGuid))
            status = ControlDeviceStatus.REPLACED;
        else
            status = ControlDeviceStatus.SAME;

        stateMachineContext.getRubezh2op3Device().setGuid(realGuid);

        return status;
    }

    private void updateReadEventsState(Map<String, Long> writeIndexes) {
        Rubezh2OP3ReadEventsState readEventsState = stateMachineContext.getFetchingEventsState();
        Map<String, Long> maxEventCounters = stateMachineContext.getDeviceMaxEventCounters();
        Map<String, Long> readIndexes = stateMachineContext.getRubezh2op3Device().getEventCounts();
        switch (checkControlDeviceID()) {
        case NEW:
            readEventsState.prepareReadingAllUnread(readIndexes, writeIndexes, maxEventCounters);
            stateMachineContext.getLogger().trace("After state create (NEW): {}", readEventsState);
            break;
        case REPLACED:
            readEventsState.prepareReadingOneFromEach(readIndexes, writeIndexes, maxEventCounters);
            stateMachineContext.getLogger().trace("After device replace: {}", readEventsState);
            break;
        case SAME:
        default:
            if (readEventsState.isInitialized()) {
                readEventsState.shiftWriteIndexes(writeIndexes, maxEventCounters);
                stateMachineContext.getLogger().trace("After write's shift: {}", readEventsState);
            } else {
                readEventsState.prepareReadingAllUnread(readIndexes, writeIndexes, maxEventCounters);
                stateMachineContext.getLogger().trace("After state create (SAME): {}", readEventsState);
            }
            break;
        }
    }

    @Override
    public State onControlDeviceStatesAndEventCountersReceived(long currentTimeMs, List<String> deviceStates,
            Map<String, Long> eventCounters) {
        stateMachineContext.getStateMachineHooks().onControlDeviceStatesReceived(currentTimeMs, deviceStates);
        stateMachineContext.setDeviceEventCounters(eventCounters);
        updateReadEventsState(eventCounters);

        // режим "после неудачной записи базы" устанавливается/снимается по биту "база повреждена"
        stateMachineContext.setBadSignature(
                deviceStates.contains(stateMachineContext.getRubezh2op3DeviceProfile().getBadSignatureStateId()));

        EventDescriptor eventToRead = stateMachineContext.getFetchingEventsState().getCurrentEvent();
        if (eventToRead != null) {
            stateMachineContext.getActionExecutor().getEvent(currentTimeMs, eventToRead.logCode, eventToRead.index);
            return State.WAITING_DEVICE_EVENTS;
        } else {
            return stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        }
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getStateMachineHooks().onControlDeviceStatesReceiveFailed(currentTimeMs, exception);

//        if (stateMachineContext.checkEventsReadyToSend(currentTimeMs)) {
//            stateMachineContext.getStateMachineHooks().sendLogEventsUpstream(currentTimeMs);
//        }

        stateMachineContext.getTaskManager().completeCurrentTask(currentTimeMs);
        return super.onDisconnected(currentTimeMs);
    }

}
