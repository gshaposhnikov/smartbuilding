package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task;

public abstract class ControlDeviceDBMatchingProjectRequiredTask extends Task {

    public ControlDeviceDBMatchingProjectRequiredTask(int priority, String issueId) {
        super(priority, issueId);
    }

    public ControlDeviceDBMatchingProjectRequiredTask(int priority) {
        super(priority);
    }

    @Override
    public boolean canBeExecuted() {
        return super.canBeExecuted()
                && stateMachineContext.getControlDeviceInfo() != null
                && stateMachineContext.getControlDeviceInfo().isDatabaseMatchingProject()
                && !stateMachineContext.isBadSignature();
    }

}
