package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetChildDeviceStatesRS extends Rubezh2OP3GetParameterRS {
    ChildDeviceStatesInfo childDeviceStatesInfo;

    public Rubezh2OP3GetChildDeviceStatesRS() {
        super(Rubezh2OP3ParameterType.CHILD_DEVICE_STATES);
    }

    public ChildDeviceStatesInfo getChildDeviceStatesInfo() {
        return childDeviceStatesInfo;
    }

    public void setChildDeviceStatesInfo(ChildDeviceStatesInfo childDeviceStatesInfo) {
        this.childDeviceStatesInfo = childDeviceStatesInfo;
    }

}
