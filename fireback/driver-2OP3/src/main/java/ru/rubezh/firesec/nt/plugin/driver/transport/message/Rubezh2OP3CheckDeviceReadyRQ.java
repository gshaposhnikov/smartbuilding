package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3CheckDeviceReadyRQ implements Rubezh2OP3Message {

    @Override
    public Rubezh2OP3FunctionType getFunctionType() {
        return Rubezh2OP3FunctionType.CHECK_READY;
    }

}
