package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.amqp.message.request.DeleteTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.PauseTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.SetDriverDeviceConfigRequest;
import ru.rubezh.firesec.nt.amqp.message.response.CreateIssueDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.DeleteTaskDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.PauseTaskDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.SetDriverDeviceConfigResponse;
import ru.rubezh.firesec.nt.domain.v1.Issue;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;

/**
 * Интерфейс для обработки внешних запросов к драйверу
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public interface Rubezh2OP3ExternalRequestHandler {
    void handleMessage(Issue issue, CreateIssueDriverResponse response);

    void startIssue(Issue issue);

    SetDriverDeviceConfigResponse handleMessage(SetDriverDeviceConfigRequest request);

    DeleteTaskDriverResponse handleMessage(DeleteTaskDriverRequest request);

    PauseTaskDriverResponse handleMessage(PauseTaskDriverRequest request);

    boolean updateVarSkudDatabase(List<Employee> employees, List<AccessKey> accessKeys,
            List<WorkSchedule> workSchedules, Map<String, List<AccessKey>> accessKeysByEmployeeIds,
            List<ActivationValidateMessage> validateMessages);

}
