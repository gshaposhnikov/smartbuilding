package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import java.util.*;
import java.util.stream.Collectors;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.fw.HexFile;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.task.*;

/**
 * Класс для работы с заданиями (тасками). Производит работу с добавлением и
 * выполнением заданий в соответствии с приоритетами. Ей задается исходное
 * состояние машины состояний, из которого выполняются задачи и в которое машина
 * состояний возвращается по окончанию задания. Когда машина состояний хочет
 * выполнить очередное задание (с максимальным приоритетом), она должна вызвать
 * метод {@link #executeNextTask(long)} и перейти в возвращенное методом
 * состояние. Когда задача выполнена, она должна вызвать метод
 * {@link #completeCurrentTask(long)}, чтобы перейти в исходное состояние.
 *
 * @author Артем Седанов
 */
public class Rubezh2OP3TaskManager {
    /*
     * TODO: обеспечить потокобезопасный доступ к taskQueue и deferredTasks
     * (обращение к ним происходит из разных потоков).
     */
    private PriorityQueue<Task> taskQueue = new PriorityQueue<>(Collections.reverseOrder());
    private List<Task> deferredTasks = new ArrayList<>();
    private Rubezh2OP3StateMachineContext stateMachineContext;
    private State sourceState;
    private Task currentTask = null;

    /**
     *
     * @param stateMachineContext контекст машины состояний
     * @param sourceState состояние, из которого будут выполняться задания и в которое она будет возвращаться после их выполнения
     */
    public Rubezh2OP3TaskManager(Rubezh2OP3StateMachineContext stateMachineContext, State sourceState) {
        this.stateMachineContext = stateMachineContext;
        this.sourceState = sourceState;
    }

    /**
     * Начать выполнение следующего задания.
     *
     * При вызове берется задание с текущим приоритетом, и у него вызывается метод {@link Task#execute(long)},
     * который обычно производит обращение к {@link Rubezh2OP3ActionExecutor}
     * и возвращет состояние, которое реагирует на результат выполения действий.
     *
     * @param currentTimeMs Текущее время в мс
     * @return Состояние, в которое должна перейти машина состояний
     */
    public State executeNextTask(long currentTimeMs) {
        if (!taskQueue.isEmpty() && !Driver.isExclusiveMode()) {
            Task nextTask = taskQueue.remove();
            if (nextTask.canBeExecuted()) {
                currentTask = nextTask;
                return currentTask.execute(currentTimeMs);
            } else {
                nextTask.fail(currentTimeMs);
                return executeNextTask(currentTimeMs);
            }
        } else {
            return sourceState;
        }
    }

    /**
     * Получить текущее выполняемое состояние.
     *
     * Вызывается в интервал времени после начала выполнения задания (вызов {@link #executeNextTask(long)}) и его окончанием (вызов {@link #completeCurrentTask(long)}. Служит для получение контекста задания при его выполении.
     * @return Текущее выполняемое состояние
     */
    public Task getCurrentTask() {
        return currentTask;
    }

    /**
     * Завершить текущую задачу.
     *
     * @return Состояние, в которое нужно перейти после завершения задания
     */
    public State completeCurrentTask(long currentTimeMs) {
        try {
            DeferredTask deferredTask = (DeferredTask) currentTask;
            deferredTask.onComplete(currentTimeMs);
        } catch (ClassCastException e) {
            // Нормальное явление, если Task не является отложенным
        }
        currentTask = null;
        return sourceState;
    }

    /**
     * Удалить все задания кроме текущего активного
     * и вернуть список задач.
     */
    public List<String> clearAllExceptCurrent() {
        List<Task> tasks = new ArrayList<>(taskQueue);
        taskQueue.clear();
        return tasks.stream().map(Task::getIssueId).collect(Collectors.toList());
    }

    /**
     * Найти и удалить задание по задаче.
     */
    public void removeTaskByIssueId(String issueId) {
        for (Task task : taskQueue) {
            if (issueId.equals(task.getIssueId())) {
                taskQueue.remove(task);
                return;
            }
        }
        for (Task task : deferredTasks) {
            if (issueId.equals(task.getIssueId())) {
                deferredTasks.remove(task);
                return;
            }
        }
    }

    /**
     * Найти и отложить задание по задаче.
     */
    public void deferTaskByIssueId(String issueId) {
        for (Task task : taskQueue) {
            if (issueId.equals(task.getIssueId())) {
                taskQueue.remove(task);
                deferredTasks.add(task);
                break;
            }
        }
    }

    /**
     * Найти и вернуть отложенное задание по задаче.
     */
    public void returnDeferredTaskByIssueId(String issueId) {
        for (Task task : deferredTasks) {
            if (issueId.equals(task.getIssueId())) {
                taskQueue.add(task);
                deferredTasks.remove(task);
                break;
            }
        }
    }

    private void prepareAndEnqueue(Task task, boolean isPause) {
        task.setStateMachineContext(stateMachineContext);
        if (isPause) deferredTasks.add(task);
        else taskQueue.add(task);
    }

    public void enqueueCheckEvents() {
        CheckEventsTask task = new CheckEventsTask();
        prepareAndEnqueue(task, false);
    }

    public void enqueueGetRegionStates(ActiveRegion activeRegion) {
        GetRegionStatesTask task = new GetRegionStatesTask(activeRegion);
        prepareAndEnqueue(task, false);
    }

    public void enqueueGetChildDeviceStates(ActiveDevice activeDevice) {
        GetChildDeviceStatesTask task = new GetChildDeviceStatesTask(activeDevice);
        prepareAndEnqueue(task, false);
    }

    public void enqueueGetChildDeviceConfig(ActiveDevice activeDevice, String issueId, boolean isPause) {
        GetChildDeviceConfigTask task = new GetChildDeviceConfigTask(activeDevice, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueGetRubezh2op3Config(ActiveDevice activeDevice, String issueId, boolean isPause) {
        GetRubezh2op3ConfigTask task = new GetRubezh2op3ConfigTask(activeDevice, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueSetChildDeviceConfig(ActiveDevice activeDevice, Map<String, String> propertyValues,
                                            String issueId, boolean isPause) {
        SetChildDeviceConfigTask task = new SetChildDeviceConfigTask(activeDevice, propertyValues, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueResetStateDevice(ActiveDevice activeDevice, String issueId,
                                        ManualResetStateGroup targetGroup, boolean isPause) {
        ResetDeviceStateTask task = new ResetDeviceStateTask(activeDevice, issueId, targetGroup);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueWriteDeviceDatabase(String issueId, boolean isPause,
            boolean writeCommonDB, boolean writeRecovery, boolean writeConstantSkud,
            boolean writeVariableSkud) {
        WriteDatabaseTask task = new WriteDatabaseTask(issueId, writeCommonDB, writeRecovery,
                writeConstantSkud, writeVariableSkud);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueUpdateDeviceSoftware(String issueId, HexFile hexFile, int version, boolean isPause) {
        UpdateFWTask task = new UpdateFWTask(issueId, hexFile, version);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueSetSecurityRegionStatus(ActiveDevice activeDevice, String regionId, boolean onGuard,
                                               String issueId, boolean isPause) {
        SetSecurityRegionStatusTask task = new SetSecurityRegionStatusTask(activeDevice, regionId, onGuard, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueSetDevicePollingState(ActiveDevice activeDevice, boolean pollingState,
                                             String issueId, boolean isPause) {
        SetDevicePollingStateTask task = new SetDevicePollingStateTask(activeDevice, pollingState, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueSetRubezh2op3Time() {
        SetRubezh2op3TimeTask task = new SetRubezh2op3TimeTask();
        prepareAndEnqueue(task, false);
    }

    public void enqueueSetRubezh2op3Time(String issueId, boolean isPause) {
        SetRubezh2op3TimeTask task = new SetRubezh2op3TimeTask(issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueSetRubezh2op3UserPassword(DeviceUserType userType, String userPassword,
            String issueId, boolean isPause) {
        SetRubezh2op3UserPasswordTask task = new SetRubezh2op3UserPasswordTask(issueId, userType, userPassword);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueResetRubezh2op3(String issueId, boolean isPause) {
        ResetRubezh2op3 task = new ResetRubezh2op3(issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueReadEvents(List<String> logIds, String issueId, boolean isPause) {
        ReadEventsRubezh2op3 task = new ReadEventsRubezh2op3(issueId, logIds);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueuePerformDeviceActionTask(ActiveDevice activeDevice, String actionId,
            Map<String, String> actionParameters, String issueId, boolean isPause) {
        PerformDeviceActionTask task =
                new PerformDeviceActionTask(activeDevice, actionId, actionParameters, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueuePerformScenarioActionTask(ActiveScenario activeScenario, ScenarioManageAction manageAction,
            String issueId, boolean isPause) {
        PerformScenarioActionTask task =
                new PerformScenarioActionTask(activeScenario, manageAction, issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueDatabaseConfigRecovery(String issueId, boolean isPause) {
        DatabaseConfigRecoveryTask task = new DatabaseConfigRecoveryTask(issueId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueWaitingAccessKeyStatus(String issueId, ActiveDevice activeDevice, boolean waitingAccessKey,
            String accessKeyId, boolean isPause) {

        SetWaitingAccessKeyStatusTask task = new SetWaitingAccessKeyStatusTask(issueId, activeDevice, waitingAccessKey,
                accessKeyId);
        prepareAndEnqueue(task, isPause);
    }

    public void enqueueCheckMode() {
        CheckModeTask task = new CheckModeTask();
        prepareAndEnqueue(task, false);
    }

    public void enqueueSetAddressList() {
        Device controlDevice = stateMachineContext.getRubezh2op3Device().getDeviceProject();
        if (controlDevice.getParentDeviceId() != null && !controlDevice.getParentDeviceId().isEmpty()) {
            /* Установка адресного листа допустима только для некорневых (подключенных штатно по RS485) приборов */
            SetAddressListTask task = new SetAddressListTask();
            prepareAndEnqueue(task, false);
        }
    }

}
