package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;

public abstract class AbstractConnected extends AbstractNone {
    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        stateMachineContext.getActionExecutor().disconnect(currentTimeMs);
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return State.IDLE;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetAllTimers(currentTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.RECONNECT,
                stateMachineContext.getReconnectInterval(), false);
        stateMachineContext.getActionExecutor().forcedSendEventsUpstream(currentTimeMs);
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return State.WAIT_RECONNECT;
    }

    @Override
    public State onReceiveTimeout(long currentTimeMs) {
        return onDisconnected(currentTimeMs);
    }

}
