package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class Rubezh2OP3DeviceInfo {
    private String guid;
    private String serial;
    private boolean databaseMatchingProject;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean isDatabaseMatchingProject() {
        return databaseMatchingProject;
    }

    public void setDatabaseMatchingProject(boolean databaseMatchingProject) {
        this.databaseMatchingProject = databaseMatchingProject;
    }

}
