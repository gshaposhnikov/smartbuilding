package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionCheckType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;

/**
 * Обертка над исполнительным блоком, который обрабатывает и оборачиваем все
 * действия и поддерживает сортировку по времени
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ScenarioTimeLineBlockRepr implements Comparable<ScenarioTimeLineBlockRepr> {
    /**
     * Задержка, в секундах, относительно предыдущего блока (или начала сценария
     * для первого блока)
     */
    private int relativeDelay = 0;
    /** Список действий */
    private List<ScenarioTimeLineBlockAction> actions = new ArrayList<>();

    /**
     * Конструктор для создания обертки над исполнительным блоком времени
     * @param timeLineBlock исполнительный блок времени
     * @param havingTracingBlock признак наличия блока слежения
     */
    public ScenarioTimeLineBlockRepr(ScenarioTimeLineBlock timeLineBlock, boolean havingTracingBlock) {
        assert(timeLineBlock.getBlockType() == BlockType.EXECUTIVE);
        
        ConditionType conditionType = timeLineBlock.getConditionType();
        ConditionCheckType conditionCheckType = timeLineBlock.getConditionCheckType();
        String conditionEntityId = timeLineBlock.getConditionEntityId();
        String conditionStateId = timeLineBlock.getConditionDeviceStateId();
        this.relativeDelay = timeLineBlock.getTimeDelaySec();

        for (Action action : timeLineBlock.getActions()) {
            ScenarioTimeLineBlockAction actionRepr = new ScenarioTimeLineBlockAction();
            actionRepr.setAction(action);
            actionRepr.setConditionType(conditionType);
            actionRepr.setConditionCheckType(conditionCheckType);
            actionRepr.setConditionEntityId(conditionEntityId);
            actionRepr.setConditionStateId(conditionStateId);
            actionRepr.setHavingTracingBlock(havingTracingBlock);
            actions.add(actionRepr);
        }
    }

    public ScenarioTimeLineBlockRepr() {

    }

    public ScenarioTimeLineBlockRepr(int relativeDelay) {
        this.relativeDelay = relativeDelay;
    }

    public int getRelativeDelay() {
        return relativeDelay;
    }

    public void setRelativeDelay(int relativeDelay) {
        this.relativeDelay = relativeDelay;
    }

    public List<ScenarioTimeLineBlockAction> getActions() {
        return actions;
    }

    public void setActions(List<ScenarioTimeLineBlockAction> actions) {
        this.actions = actions;
    }

    @Override
    public int compareTo(ScenarioTimeLineBlockRepr o) {
        return Integer.compare(relativeDelay, o.relativeDelay);
    }
}
