package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler;

import java.util.*;

import org.apache.commons.codec.binary.Hex;

import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;
import ru.rubezh.firesec.nt.domain.v1.DeviceProperty;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.Rubezh2OP3StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.State;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.DatabaseBuilder;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3DeviceInfo;

public class GettingRubezh2OP3Info extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.GETTING_RUBEZH2OP3_INFO;
    }

    @Override
    public StateHandler cloneBinded(Rubezh2OP3StateMachineContext stateMachineContext) {
        GettingRubezh2OP3Info handler = new GettingRubezh2OP3Info();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onControlDeviceInfoReceived(long currentTimeMs, Rubezh2OP3DeviceInfo deviceInfo) {
        stateMachineContext.getLogger().info("Rubezh2OP3 (address path: {}) GUID: {}, serial: {}",
                stateMachineContext.getRubezh2op3Device().getAddressPath(),
                deviceInfo.getGuid(), deviceInfo.getSerial());
        stateMachineContext.setControlDeviceInfo(deviceInfo);

        /* Теперь получим хеш базы данных */
        DatabaseBuilder databaseBuilder = stateMachineContext.getDatabaseBuilder();
        stateMachineContext.getActionExecutor().getDataBlock(currentTimeMs,
                databaseBuilder.getMainDatabaseAddress() + databaseBuilder.getDbHashOffset(),
                databaseBuilder.getDbHash().length);

        return getCurrentState();

    }

    public State onDataBlockReceived(long currentTimeMs, byte[] data) {
        stateMachineContext.getLogger().info("Rubezh2OP3 (address path: {}) DB hash: {}",
                stateMachineContext.getRubezh2op3Device().getAddressPath(), Hex.encodeHexString(data));
        Rubezh2OP3DeviceInfo deviceInfo = stateMachineContext.getControlDeviceInfo();
        deviceInfo.setDatabaseMatchingProject(
                Arrays.equals(stateMachineContext.getDatabaseBuilder().getDbHash(), data));
        stateMachineContext.getStateMachineHooks().onControlDeviceInfoReceived(currentTimeMs, deviceInfo);

        if (DeviceProperty.getBoolean(stateMachineContext.getRubezh2op3Device().getDeviceProject(),
                stateMachineContext.getRubezh2op3DeviceProfile(), DeviceProperties.TimeAutoSet))
            stateMachineContext.getActionExecutor().setDeviceTime(currentTimeMs, new Date());
        else
            stateMachineContext.getActionExecutor().getDeviceMaxEventCounters(currentTimeMs);

        return getCurrentState();
    }

    @Override
    public State onDeviceTimeSet(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onRubezh2op3TimeSet(currentTimeMs,
                stateMachineContext.getRubezh2op3Device().getId());
        stateMachineContext.getActionExecutor().getDeviceMaxEventCounters(currentTimeMs);

        return getCurrentState();
    }

    @Override
    public State onDeviceMaxEventCountersReceived(long currentTimeMs, Map<String, Long> maxEventCounters) {
        stateMachineContext.setDeviceMaxEventCounters(maxEventCounters);

        // необходимо считать счётчики событий до перехода к выполнению задач
        stateMachineContext.getActionExecutor().getControlDeviceEventCountersAndStates(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onDeviceMaxEventCountersReceiveFailed(long currentTimeMs) {
        stateMachineContext.getLogger().warn("Failed getting control device logs sizes");
        /*
         * Ошибку игнорируем (поддержка запрашиваемого параметра 0x26 при
         * подключении по USB есть начиная с 21-ой версии прошивки прибора).
         * 
         * Максимальные размеры журналов останутся, как заданы в профиле прибора
         * 
         * Необходимо, как и в случае успеха, считать счётчики событий до
         * перехода к выполнению задач
         */
        stateMachineContext.getActionExecutor()
                .getControlDeviceEventCountersAndStates(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onControlDeviceStatesAndEventCountersReceived(long currentTimeMs, List<String> deviceStates,
            Map<String, Long> eventCounters) {
        stateMachineContext.getStateMachineHooks().onControlDeviceStatesReceived(currentTimeMs, deviceStates);
        stateMachineContext.setDeviceEventCounters(eventCounters);

        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.EXECUTE_TASK,
                stateMachineContext.getTaskExecuteInterval(), true);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.CHECK_EVENTS,
                stateMachineContext.getCheckEventsInterval(), true);

        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.CHECK_REGION_STATES,
                stateMachineContext.getCheckRegionStatesInterval(), true);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.CHECK_CHILD_DEVICE_STATES,
                stateMachineContext.getCheckChildDeviceStatesInterval(), true);
        if (DeviceProperty.getBoolean(stateMachineContext.getRubezh2op3Device().getDeviceProject(),
                stateMachineContext.getRubezh2op3DeviceProfile(), DeviceProperties.TimeAutoSet))
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, TimerNames.SET_TIME,
                    stateMachineContext.getSetTimeInterval(), true);

        stateMachineContext.getTaskManager().enqueueSetAddressList();

        return State.WAITING_TASK;
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getStateMachineHooks().onControlDeviceInfoReceiveFailed(currentTimeMs, exception);
        return super.onDisconnected(currentTimeMs);
    }

}
