package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db.model;

import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionCheckType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;

/**
 * Обертка для действия в исполнительном блоке - нужно, т.к. в модели данных у
 * нас условия хранятся для всего блока, а в БД прибора - для каждого действия
 * отдельно
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ScenarioTimeLineBlockAction {

    /** Тип условия */
    private ConditionType conditionType = ConditionType.NONE;
    /** Тип проверки условия */
    private ConditionCheckType conditionCheckType = ConditionCheckType.IS_ACTIVE;
    /**
     * Идентификатор сущности (устройства или виртуального состояния) для
     * проверки условия
     */
    private String conditionEntityId = "";
    /**
     * Идентфикатор состояния (устройства или виртуального состояния) для
     * проверки условия
     */
    private String conditionStateId = "";
    /** Признак наличия блока слежения */
    private boolean havingTracingBlock;

    /** Действие */
    private Action action;

    public ConditionType getConditionType() {
        return conditionType;
    }

    public void setConditionType(ConditionType conditionType) {
        this.conditionType = conditionType;
    }

    public ConditionCheckType getConditionCheckType() {
        return conditionCheckType;
    }

    public void setConditionCheckType(ConditionCheckType conditionCheckType) {
        this.conditionCheckType = conditionCheckType;
    }

    public String getConditionEntityId() {
        return conditionEntityId;
    }

    public void setConditionEntityId(String conditionEntityId) {
        this.conditionEntityId = conditionEntityId;
    }

    public String getConditionStateId() {
        return conditionStateId;
    }

    public void setConditionStateId(String conditionStateId) {
        this.conditionStateId = conditionStateId;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public boolean isHavingTracingBlock() {
        return havingTracingBlock;
    }

    public void setHavingTracingBlock(boolean havingTracingBlock) {
        this.havingTracingBlock = havingTracingBlock;
    }

}
