package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetFirmwareVersionRS extends Rubezh2OP3GetParameterRS {
    private String firmwareVersion;

    public Rubezh2OP3GetFirmwareVersionRS() {
        super(Rubezh2OP3ParameterType.FIRMWARE_VERSION);
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }
}
