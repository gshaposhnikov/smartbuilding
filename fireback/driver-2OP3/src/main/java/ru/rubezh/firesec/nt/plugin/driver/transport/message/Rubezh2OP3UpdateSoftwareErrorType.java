package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum Rubezh2OP3UpdateSoftwareErrorType {
    SUCCESS(0x00),
    DATA_OUTSIDE_MEMORY(0x01),
    CMD_PREPARE_SECTORS_FAILED(0x02),
    CMD_ERASE_SECTORS_FAILED(0x03),
    CMD_COPY_RAM_TO_FLASH_FAILED(0x04),
    CMD_COMPARE_FAILED(0x05),
    CMD_BLANK_CHECK_SECTORS_FAILED(0x06),
    CORRUPT_CODE(0x07),
    DISCONNECT(0x08),
    UNKNOWN(0x09),
    CONFIGURATION_FAILED(0x0A);

    private final int value;
    private Rubezh2OP3UpdateSoftwareErrorType(int value) {
        this.value = value;
    }

    private static final Map<Integer, Rubezh2OP3UpdateSoftwareErrorType> typesByValue = new HashMap<>();
    static {
        for (Rubezh2OP3UpdateSoftwareErrorType type : Rubezh2OP3UpdateSoftwareErrorType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<Rubezh2OP3UpdateSoftwareErrorType, Integer> valuesByType = new EnumMap<>(Rubezh2OP3UpdateSoftwareErrorType.class);
    static {
        for (Rubezh2OP3UpdateSoftwareErrorType type : Rubezh2OP3UpdateSoftwareErrorType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static Rubezh2OP3UpdateSoftwareErrorType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(Rubezh2OP3UpdateSoftwareErrorType type) {
        return valuesByType.get(type);
    }
}
