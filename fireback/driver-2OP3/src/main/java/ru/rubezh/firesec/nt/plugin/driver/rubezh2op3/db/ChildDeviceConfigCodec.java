package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.db;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.domain.v1.DeviceConfigProperty;
import ru.rubezh.firesec.nt.domain.v1.PropertyType;

/**
 * Кодер/декодер конфигураций для дочерних устройств
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */

public class ChildDeviceConfigCodec {

    private Logger logger;

    public ChildDeviceConfigCodec(Logger logger) {
        this.logger = logger;
    }

    public Map<String, String> decodeDeviceProfileConfig(byte[] properties, Map<String, DeviceConfigProperty> configProperties) {
        Map<String, String> propertyValues = new HashMap<>();

        for (String propertyId : configProperties.keySet()) {
            DeviceConfigProperty property = configProperties.get(propertyId);
            if (property.isReadOnly())
                continue;
            if (property.getType() == PropertyType.SECTION)
                continue;
            propertyValues.put(propertyId, property.decode(properties));
        }

        return propertyValues;
    }

    public void encodeDeviceProfileConfig(byte[] dest,
            Map<String, DeviceConfigProperty> configProperties, Map<String, String> propertyValues) {
        encodeDeviceProfileConfig(dest, configProperties, propertyValues, 0);
    }

    public void encodeDeviceProfileConfig(byte[] dest,
            Map<String, DeviceConfigProperty> configProperties, Map<String, String> propertyValues, int address) {
        for (String propertyId : configProperties.keySet()) {
            DeviceConfigProperty property = configProperties.get(propertyId);
            if (property.isReadOnly())
                continue;
            if (property.getType() == PropertyType.SECTION)
                continue;
            String value = (propertyValues == null) ? null : propertyValues.get(propertyId);
            if (value == null)
                value = property.getDefaultValue();
            if (value != null) {
                if (!property.encode(dest, value, address)) {
                    logger.warn("Bad property value for config property {}: {}", propertyId, value);
                }
            } else {
                logger.warn("Neither property value of default value are passed for config property {}", propertyId);
            }
        }
    }
}
