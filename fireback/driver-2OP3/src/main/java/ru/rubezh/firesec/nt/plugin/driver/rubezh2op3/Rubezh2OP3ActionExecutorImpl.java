package ru.rubezh.firesec.nt.plugin.driver.rubezh2op3;

import ru.rubezh.firesec.nt.amqp.message.alert.ControlDeviceEvents;
import ru.rubezh.firesec.nt.domain.v1.DeviceAction;
import ru.rubezh.firesec.nt.domain.v1.ManualResetStateGroup;
import ru.rubezh.firesec.nt.domain.v1.ScenarioManageAction;
import ru.rubezh.firesec.nt.plugin.driver.rubezh2op3.state_handler.TimerNames;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.set_parameter.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

public class Rubezh2OP3ActionExecutorImpl implements Rubezh2OP3ActionExecutor {

    public static final int ACTION_ATTEMPTS = 3;
    public static final int ATTEMPTS_INTERVAL = 150;

    protected Rubezh2OP3StateMachineContext stateMachineContext;

    public Rubezh2OP3ActionExecutorImpl(Rubezh2OP3StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    @Override
    public void connect(long currentTimeMs) {
        stateMachineContext.getTransport().connect(currentTimeMs, this::connected, this::disconnected);
    }

    @Override
    public void disconnect(long currentTimeMs) {
        try {
            stateMachineContext.getTransport().disconnect(currentTimeMs);
        }
        catch (IOException e) {
            stateMachineContext.getLogger().error("IO exception with message: {}", e.getMessage());
        }
    }

    @Override
    public void setTimer(long currentTimeMs, String name, int interval, boolean isRepeatable) {
        stateMachineContext.getClock().setTimer(name, currentTimeMs, interval,
                timerTimeMs -> timerHandler(timerTimeMs, name), isRepeatable);
    }

    @Override
    public void setIssueCloseTimer(long currentTimeMs, String issueId, int interval) {
        stateMachineContext.getClock().setTimer(issueId, currentTimeMs, interval,
                timerTimeMs -> issueCloseTimerHandler(timerTimeMs, issueId), false);
    }

    @Override
    public void resetTimer(long currentTimeMs, String name) {
        stateMachineContext.getClock().resetTimer(name);
    }

    @Override
    public void resetAllTimers(long currentTimeMs) {
        /*
         * Сбрасываем все таймера, кроме отправки событий. Отправка событий -
         * это отдельная фоновая задача.
         */
        stateMachineContext.getClock().resetAllTimersExcludingOne(TimerNames.SEND_EVENTS);
    }

    @Override
    public void startSendingEventsUpstream(long currentTimeMs) {
        stateMachineContext.getClock().setTimer(TimerNames.SEND_EVENTS, currentTimeMs,
                stateMachineContext.getEventsSendTimeOut(),
                this::forcedSendEventsUpstream,
                true);
    }

    @Override
    public void stopSendingEventsUpstream(long currentTimeMs) {
        stateMachineContext.getClock().resetTimer(TimerNames.SEND_EVENTS);
    }

    @Override
    public void forcedSendEventsUpstream(long currentTimeMs) {
        if (!stateMachineContext.getEvents().isEmpty()) {
            ControlDeviceEvents deviceLogEvents = new ControlDeviceEvents();
            deviceLogEvents.setControlDeviceEvents(stateMachineContext.getEvents());
            stateMachineContext.setEvents(new ArrayList<>());
            stateMachineContext.getDriverAlertSender().send(deviceLogEvents);
        }
        stateMachineContext.getClock().setTimer(TimerNames.SEND_EVENTS, currentTimeMs,
                stateMachineContext.getEventsSendTimeOut(), this::forcedSendEventsUpstream, true);
    }

    protected void connected(long currentTimeMs, boolean isSuccess) {
        if (isSuccess)
            stateMachineContext.getStateHandler().onConnected(currentTimeMs);
        else
            stateMachineContext.getStateHandler().onConnectFailure(currentTimeMs);
    }

    protected void disconnected(long currentTimeMs) {
        stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
    }

    protected void timerHandler(long currentTimeMs, String timerName) {
        stateMachineContext.getStateHandler().onTimer(currentTimeMs, timerName);
    }

    protected void issueCloseTimerHandler(long currentTimeMs, String issueId) {
        stateMachineContext.getStateHandler().onIssueCloseTimer(currentTimeMs, issueId);
    }

    /* Получение информации о приборе */

    @Override
    public void getControlDeviceInfo(long currentTimeMs) {
        Rubezh2OP3GetControlDeviceInfoRQ request = new Rubezh2OP3GetControlDeviceInfoRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetControlDeviceInfoRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetControlDeviceInfoRS getControlDeviceInfoRS =
                            (Rubezh2OP3GetControlDeviceInfoRS) response;
                    stateMachineContext.getStateHandler().onControlDeviceInfoReceived(featureTimeMs,
                            getControlDeviceInfoRS.getDeviceInfo());
                }
        );
        action.run(currentTimeMs);
    }

    /* Чтение блока данных из FLASH памяти прибора */

    @Override
    public void getDataBlock(long currentTimeMs, int address, int length) {
        Rubezh2OP3GetDataBlockRQ request = new Rubezh2OP3GetDataBlockRQ(address, length);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetDataBlockRS(responseRaw, length),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetDataBlockRS getDataBlockRS = (Rubezh2OP3GetDataBlockRS) response;
                    stateMachineContext.getStateHandler().onDataBlockReceived(featureTimeMs, getDataBlockRS.getData());
                }
        );
        action.run(currentTimeMs);
    }

    /* Получение максимального количества событий */

    @Override
    public void getDeviceMaxEventCounters(long currentTimeMs) {
        Rubezh2OP3GetMaxEventCountersRQ request = new Rubezh2OP3GetMaxEventCountersRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetMaxEventCountersRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetMaxEventCountersRS getMaxEventCountersRS =
                            (Rubezh2OP3GetMaxEventCountersRS) response;
                    stateMachineContext.getStateHandler().onDeviceMaxEventCountersReceived(featureTimeMs,
                            getMaxEventCountersRS.getLogCounters());
                },
                (response, featureTimeMs) -> stateMachineContext.getStateHandler()
                        .onDeviceMaxEventCountersReceiveFailed(featureTimeMs)

        );
        action.run(currentTimeMs);
    }

    /* Получение счетчиков событий и байт состояния */

    @Override
    public void getControlDeviceEventCountersAndStates(long currentTimeMs) {
        Rubezh2OP3GetEventCountersAndStatesRQ request = new Rubezh2OP3GetEventCountersAndStatesRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetEventCountersAndStatesRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetEventCountersAndStatesRS getEventCountersAndStateBytesRS =
                            (Rubezh2OP3GetEventCountersAndStatesRS)response;
                    stateMachineContext.getStateHandler().onControlDeviceStatesAndEventCountersReceived(featureTimeMs,
                            getEventCountersAndStateBytesRS.getDeviceStates(),
                            getEventCountersAndStateBytesRS.getLogCounters());
                }
        );
        action.run(currentTimeMs);
    }

    /* Получение события */

    @Override
    public void getEvent(long currentTimeMs, int logCode, long eventIndex) {
        Rubezh2OP3GetLogEventRQ request = new Rubezh2OP3GetLogEventRQ(logCode, eventIndex);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetLogEventRS(
                        responseRaw,
                        regionLocalNo -> stateMachineContext.getDatabaseBuilder()
                                .getRegionIdByLocalNo(regionLocalNo)),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetLogEventRS getLogEventRS = (Rubezh2OP3GetLogEventRS)response;
                    BitSet regionStateBits = getLogEventRS.getRegionStateBits();
                    String regionId = getLogEventRS.getRegionId();
                    stateMachineContext.getStateHandler().onLogEventReceived(featureTimeMs,
                            getLogEventRS.getLogEvent(), regionStateBits, regionId);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение статусов зоны
     */

    @Override
    public void getRegionStates(long currentTimeMs, String regionId) {
        Integer regionNo = stateMachineContext.getDatabaseBuilder().getRegionNoById(regionId);
        if (regionNo == null)
            return;
        Rubezh2OP3GetRegionStatesRQ request = new Rubezh2OP3GetRegionStatesRQ(regionNo, regionId);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetRegionStatesRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetRegionStatesRS getRegionStatesRS = (Rubezh2OP3GetRegionStatesRS) response;
                    BitSet regionStateBits = getRegionStatesRS.getRegionStateBits();
                    stateMachineContext.getStateHandler().onRegionStatesReceived(featureTimeMs,
                            regionId, regionStateBits);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение статусов устройства
     */

    @Override
    public void getChildDeviceStates(long currentTimeMs, int lineNo, int address, String regionId) {
        Rubezh2OP3GetChildDeviceStatesRQ request = new Rubezh2OP3GetChildDeviceStatesRQ(lineNo, address, regionId);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> {
                    BitSet regionStateBits = stateMachineContext.getRegionStateBits(regionId);
                    return stateMachineContext.getMessageCodec().decodeGetChildDeviceStatesRS(responseRaw, regionStateBits);
                },
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetChildDeviceStatesRS getChildDeviceStatesRS =
                            (Rubezh2OP3GetChildDeviceStatesRS) response;
                    stateMachineContext.getStateHandler().onChildDeviceStatesReceived(featureTimeMs,
                            getChildDeviceStatesRS.getChildDeviceStatesInfo());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение конфигурации устройства
     */

    @Override
    public void getChildDeviceConfig(long currentTimeMs, int lineNo, int address) {
        Rubezh2OP3GetChildDeviceConfigRQ request = new Rubezh2OP3GetChildDeviceConfigRQ(lineNo, address);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetChildDeviceConfigRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetChildDeviceConfigRS getChildDeviceConfigRS =
                            (Rubezh2OP3GetChildDeviceConfigRS) response;
                    stateMachineContext.getStateHandler().onChildDeviceConfigReceived(featureTimeMs,
                            getChildDeviceConfigRS.getChildDeviceConfigInfo());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Запись конфигурации устройства
     */

    @Override
    public void setChildDeviceConfig(long currentTimeMs, int type, int lineNo, int address,
            Map<String, String> propertyValues) {
        Rubezh2OP3SetChildDeviceConfigRQ request = new Rubezh2OP3SetChildDeviceConfigRQ(type, lineNo, address,
                propertyValues);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetChildDeviceConfigRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onChildDeviceConfigSet(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Выполнение действия устройства
     */

    @Override
    public void performDeviceAction(long currentTimeMs, int type, int lineNo, int address, DeviceAction deviceAction,
            Map<String, String> propertyValues) {
        Rubezh2OP3PerformDeviceActionRQ request =
                new Rubezh2OP3PerformDeviceActionRQ(type, lineNo, address, deviceAction, propertyValues);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodePerformDeviceActionRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onDeviceActionPerformed(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Выполнение действия над сценарием
     */

    @Override
    public void performScenarioAction(long currentTimeMs, int globalScenarioNo, ScenarioManageAction manageAction) {
        Rubezh2OP3PerformScenarioActionRQ request =
                new Rubezh2OP3PerformScenarioActionRQ(globalScenarioNo, manageAction);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodePerformScenarioActionRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onScenarioActionPerformed(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Сброс состояния
     */

    @Override
    public void resetStateDevice(long currentTimeMs, int lineNo, int address, ManualResetStateGroup targetResetGroup) {
        Rubezh2OP3SetStateDeviceResetRQ request = new Rubezh2OP3SetStateDeviceResetRQ(targetResetGroup);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetResetStateDeviceRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onResetStateDeviceReceived(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Переход в режим обновления ПО
     */

    @Override
    public void invokeUpdateSoftwareMode(long currentTimeMs, Rubezh2OP3UpdateSoftwareModeType softwareType) {
        Rubezh2OP3InvokeUpdateSoftwareModeRQ request = new Rubezh2OP3InvokeUpdateSoftwareModeRQ(softwareType);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeInvokeUpdateSoftwareModeRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3InvokeUpdateSoftwareModeRS invokeUpdateSoftwareModeRS =
                            (Rubezh2OP3InvokeUpdateSoftwareModeRS) response;
                    stateMachineContext.getStateHandler().onSoftwareUpdateModeInvoked(featureTimeMs,
                            invokeUpdateSoftwareModeRS.getWaitingTimeMs());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Запрос статуса готовности устройства
     */

    @Override
    public void checkDeviceReadyState(long currentTimeMs) {
        Rubezh2OP3CheckDeviceReadyRQ request = new Rubezh2OP3CheckDeviceReadyRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeCheckDeviceReadyRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3CheckDeviceReadyRS checkDeviceReadyRS = (Rubezh2OP3CheckDeviceReadyRS) response;
                    stateMachineContext.getStateHandler().onDeviceReadyStateReceived(featureTimeMs,
                            checkDeviceReadyRS.isReady());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Запрос на перезагрузку устройства
     */

    @Override
    public void reset(long currentTimeMs) {
        Rubezh2OP3ResetRQ request = new Rubezh2OP3ResetRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeResetRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3ResetRS resetRS = (Rubezh2OP3ResetRS) response;
                    stateMachineContext.getStateHandler().onResetInvoked(featureTimeMs, resetRS.getWaitingTimeMs());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Чтение адресного листа прибора
     */

    @Override
    public void getAddressList(long currentTimeMs) {
        Rubezh2OP3GetAddressListRQ request = new Rubezh2OP3GetAddressListRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetAddressListRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetAddressListRS getAddressListRS = (Rubezh2OP3GetAddressListRS) response;
                    stateMachineContext.getStateHandler().onAddressListReceived(featureTimeMs,
                            getAddressListRS.getAddressList());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Запись адресного листа прибора
     */

    @Override
    public void setAddressList(long currentTimeMs, List<Integer> addressList) {
        Rubezh2OP3SetAddressListRQ request = new Rubezh2OP3SetAddressListRQ(addressList);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetAddressListRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3SetAddressListRS setAddressListRS = (Rubezh2OP3SetAddressListRS) response;
                    stateMachineContext.getStateHandler().onAddressListSent(featureTimeMs,
                            setAddressListRS.getWaitingTimeMs());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Стирание секторов
     */

    @Override
    public boolean eraseSectors(long currentTimeMs, int startSector, int endSector) {
        if (startSector <= 0 || endSector < startSector)
            return false;
        Rubezh2OP3EraseSectorsRQ request = new Rubezh2OP3EraseSectorsRQ(startSector, endSector);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeEraseSectorsRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3EraseSectorsRS eraseSectorsRS = (Rubezh2OP3EraseSectorsRS) response;
                    stateMachineContext.getStateHandler().onEraseSectorsInvoked(featureTimeMs,
                            eraseSectorsRS.getWaitingTimeMs());
                }
        );
        action.run(currentTimeMs);
        return true;
    }

    /*
     * Запись в память
     */

    @Override
    public void writeMemory(long currentTimeMs, int address, byte[] data) {
        Rubezh2OP3WriteMemoryRQ request = new Rubezh2OP3WriteMemoryRQ(address, data);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeWriteMemoryRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3WriteMemoryRS writeMemoryRS = (Rubezh2OP3WriteMemoryRS) response;
                    stateMachineContext.getStateHandler().onWriteMemoryInvoked(featureTimeMs,
                            writeMemoryRS.getWaitingTimeMs());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение ошибки при обновлении ПО
     */

    @Override
    public void getUpdateSoftwareError(long currentTimeMs) {
        Rubezh2OP3GetUpdateSoftwareErrorRQ request = new Rubezh2OP3GetUpdateSoftwareErrorRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetUpdateSoftwareErrorRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetUpdateSoftwareErrorRS getUpdateSoftwareErrorRS =
                            (Rubezh2OP3GetUpdateSoftwareErrorRS) response;
                    stateMachineContext.getStateHandler().onUpdateSoftwareErrorReceived(featureTimeMs,
                            getUpdateSoftwareErrorRS.getError());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение статуса обновления ПО
     */

    @Override
    public void getUpdateSoftwareStatus(long currentTimeMs) {
        Rubezh2OP3GetUpdateSoftwareStatusRQ request = new Rubezh2OP3GetUpdateSoftwareStatusRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetUpdateSoftwareStatusRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetUpdateSoftwareStatusRS getUpdateSoftwareStatusRS =
                            (Rubezh2OP3GetUpdateSoftwareStatusRS) response;
                    stateMachineContext.getStateHandler().onUpdateSoftwareStatusReceived(featureTimeMs,
                            getUpdateSoftwareStatusRS.getStatus());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение номера сборки (версии прошивки прибора)
     */

    @Override
    public void getFirmwareVersion(long currentTimeMs) {
        Rubezh2OP3GetFirmwareVersionRQ request = new Rubezh2OP3GetFirmwareVersionRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetFirmwareVersionRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetFirmwareVersionRS getFirmwareVersionRS = (Rubezh2OP3GetFirmwareVersionRS) response;
                    stateMachineContext.getStateHandler().onFirmwareVersionReceived(featureTimeMs,
                            getFirmwareVersionRS.getFirmwareVersion());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Получение версии базы данных прибора
     */

    @Override
    public void getDatabaseVersion(long currentTimeMs) {
        Rubezh2OP3GetDatabaseVersionRQ request = new Rubezh2OP3GetDatabaseVersionRQ();
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeGetDatabaseVersionRS(responseRaw),
                (response, featureTimeMs) -> {
                    Rubezh2OP3GetDatabaseVersionRS getDatabaseVersionRS = (Rubezh2OP3GetDatabaseVersionRS) response;
                    stateMachineContext.getStateHandler().onDatabaseVersionReceived(featureTimeMs,
                            getDatabaseVersionRS.getDatabaseVersion());
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Смена статуса охранной зоны
     */

    @Override
    public void changeSecurityRegionStatus(long currentTimeMs, String regionId, boolean onGuard) {
        int regionNo = stateMachineContext.getDatabaseBuilder().getRegionNoById(regionId);
        Rubezh2OP3SetSecurityRegionStatusRQ request = new Rubezh2OP3SetSecurityRegionStatusRQ();
        request.setRegionNo(regionNo);
        request.setOnGuard(onGuard);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetSecurityRegionStatusRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onSetSecurityRegionStatusReceived(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Смена статуса опроса устройства
     */

    @Override
    public void changeDevicePollingState(long currentTimeMs, int type, int lineNo, int address, boolean pollingState) {
        Rubezh2OP3SetDevicePollingStateRQ request = new Rubezh2OP3SetDevicePollingStateRQ(type, lineNo, address,
                pollingState);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetDevicePollingStateRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onSetDevicePollingStateReceived(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Установка текущего времени на приборе
     */

    @Override
    public void setDeviceTime(long currentTimeMs, Date dateTime) {
        Rubezh2OP3SetCurrentTimeRQ request = new Rubezh2OP3SetCurrentTimeRQ(dateTime);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetCurrentTimeRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onDeviceTimeSet(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /*
     * Установка пароля пользователя на приборе
     */

    @Override
    public void setDeviceUserPassword(long currentTimeMs, DeviceUserType userType, String password) {
        Rubezh2OP3SetUserPasswordRQ request = new Rubezh2OP3SetUserPasswordRQ(userType, password);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetDeviceUserPasswordRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onDeviceUserPasswordSet(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /* Установка версии прошивки прибора */

    @Override
    public void setFrimwareVersion(long currentTimeMs, int hexWordVersion) {
        Rubezh2OP3SetFirmwareVersionRQ request = new Rubezh2OP3SetFirmwareVersionRQ(hexWordVersion);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetFirmwareVersionRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onFirmwareVersionSet(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

    /* Установка статуса ожидания ключа доступа */

    @Override
    public void setWaitingAccessKeyStatus(long currentTimeMs, int lineNo, int address, boolean waiting) {
        Rubezh2OP3SetWaitingAccessKeyStatusRQ request = new Rubezh2OP3SetWaitingAccessKeyStatusRQ(lineNo, address,
                waiting);
        Rubezh2OP3Request action = new Rubezh2OP3Request(stateMachineContext, ACTION_ATTEMPTS, ATTEMPTS_INTERVAL,
                (ByteBuffer)stateMachineContext.getMessageCodec().encode(request).flip(),
                responseRaw -> stateMachineContext.getMessageCodec().decodeSetWaitingAccessKeyStatusRS(responseRaw),
                (response, featureTimeMs) -> {
                    stateMachineContext.getStateHandler().onWaitingAccessKeyStatusSet(featureTimeMs);
                }
        );
        action.run(currentTimeMs);
    }

}
