package ru.rubezh.firesec.nt.plugin.driver.transport.message.get_parameter;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.Rubezh2OP3ParameterType;

public class Rubezh2OP3GetDataBlockRS extends Rubezh2OP3GetParameterRS {

    private byte[] data;

    public Rubezh2OP3GetDataBlockRS(byte[] data) {
        super(Rubezh2OP3ParameterType.DB_BLOCK);
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

}
