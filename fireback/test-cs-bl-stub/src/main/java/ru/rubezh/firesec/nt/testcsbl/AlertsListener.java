package ru.rubezh.firesec.nt.testcsbl;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.amqp.message.alert.*;
import ru.rubezh.firesec.nt.domain.v1.driver_entity.ControlDeviceLogEventInfo;
import ru.rubezh.firesec.nt.testcsbl.driverrepo.DriverRepository;
import ru.rubezh.firesec.nt.testcsbl.driverrepo.DriverWorker;

public class AlertsListener {

    private Logger logger;
    private DriverRepository driverRepository;

    @Autowired
    public AlertsListener(Logger logger, DriverRepository driverRepository) {
        super();
        this.logger = logger;
        this.driverRepository = driverRepository;
    }

    public DriverRepository getDriverRepository() {
        return driverRepository;
    }

    public void setDriverRepository(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public void handleMessage(DriverRegisteredAlert driverRegisteredAlert) {
        if (logger != null) {
            logger.info("Driver registered. Id: {}, requests queue: {}, alerts exchange: {}",
                    driverRegisteredAlert.getDriverId(), driverRegisteredAlert.getRequestQueue(),
                    driverRegisteredAlert.getAlertsExchange());
        }
        DriverWorker driverWorker = driverRepository.registerDriver(driverRegisteredAlert.getDriverId(),
                driverRegisteredAlert.getRequestQueue(), driverRegisteredAlert.getAlertsExchange());
        if (driverWorker != null) {
            driverWorker.sendDeviceTree();
        }
    }

    public void handleMessage(AddActiveDeviceState alert) {
        logger.info("State {} was activated on device {}", alert.getState(), alert.getActiveDeviceId());
    }

    public void handleMessage(RemoveActiveDeviceState alert) {
        logger.info("State {} was reset on device {}", alert.getState(), alert.getActiveDeviceId());
    }

    public void handleMessage(SetActiveDeviceStates alert) {
        String states = "";
        for (String state : alert.getActiveStates())
            states += ", " + state;
        if (states.length() > 0) {
            states = states.substring(2);
            logger.info("States {} are active on device {}", states, alert.getActiveDeviceId());
        } else {
            logger.info("No active states on device {}", alert.getActiveDeviceId());
        }
    }

    public void handleMessage(DeviceInfo alert) {
        logger.info("Got device info from driver {} for device {}. GUID: {}, Serial: {}", alert.getDriverId(),
                alert.getDeviceId(), alert.getGuid(), alert.getSerial());
    }

    public void handleMessage(DeviceLogEvents deviceLogEvent) {
        ControlDeviceLogEventInfo basicEvent = deviceLogEvent.getBasicEvent();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        logger.info("New event from driver {}: {} ({} # {}) at {}. DeviceInfo: {}, RegionInfo: {}",
                deviceLogEvent.getDriverId(), basicEvent.getTypeId(), basicEvent.getSubsystem(),
                basicEvent.getEventNo(), sdf.format(basicEvent.getOccurredDateTime()), basicEvent.getDeviceInfo(),
                basicEvent.getRegionInfo());
    }

    public void handleMessage(ActiveDeviceConfig activeDeviceConfig) {
        logger.info("Got active device config for device \"{}\": {}", activeDeviceConfig.getDeviceId(),
                activeDeviceConfig.getConfigPropertyValues());
    }

    void handleMessage(IssueProgress issueProgress) {
        logger.info("Got issue progress {}: {} / {}", issueProgress.getIssueId(), issueProgress.getStatus(), issueProgress.getProgress());
    }

}
