package ru.rubezh.firesec.nt.testcsbl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CsBlStub {
    
    private ApplicationContext applicationContext;

	public static void main(String[] args) {
		new CsBlStub().run();
	}
	
	public void run() {
	    applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	}

}
