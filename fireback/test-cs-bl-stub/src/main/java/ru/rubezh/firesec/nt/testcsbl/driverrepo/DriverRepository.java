package ru.rubezh.firesec.nt.testcsbl.driverrepo;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public class DriverRepository {
    private Logger logger;

    private Map<String, DriverWorker> drivers = new HashMap<>();

    private RabbitTemplate rabbitTemplate;
    private String commonNamePrefix;
    private String responseQueueSuffix;

    private long driverTimeoutMillis = 5000;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public String getCommonNamePrefix() {
        return commonNamePrefix;
    }

    public void setCommonNamePrefix(String commonNamePrefix) {
        this.commonNamePrefix = commonNamePrefix;
    }

    public String getResponseQueueSuffix() {
        return responseQueueSuffix;
    }

    public void setResponseQueueSuffix(String responseQueueSuffix) {
        this.responseQueueSuffix = responseQueueSuffix;
    }

    private String generateResponseQueueName(String driverId) {
        return commonNamePrefix + ".drv." + driverId + "." + responseQueueSuffix;
    }

    public DriverWorker registerDriver(String driverId, String requestQueueName, String alertsExchangeName) {
        if (!drivers.containsKey(driverId)) {
            String responseQueueName = generateResponseQueueName(driverId);
            DriverWorker driverWorker = new DriverWorker(logger, this, driverId, requestQueueName, responseQueueName,
                    alertsExchangeName, rabbitTemplate);
            drivers.put(driverId, driverWorker);
            return driverWorker;
        } else {
            return drivers.get(driverId);
        }
    }

    public DriverWorker getDriverWorker(String driverId) {
        return drivers.get(driverId);
    }

    public void process() {
        long currentTimeMillis = System.currentTimeMillis();
        for (String driverId : drivers.keySet()) {
            DriverWorker driverWorker = drivers.get(driverId);
            long lastResponseTimeMillis = driverWorker.getLastResponseTimeMillis();
            if (currentTimeMillis - lastResponseTimeMillis > driverTimeoutMillis) {
                if (logger != null) {
                    logger.warn("Driver {} timeout", driverId);
                }
                drivers.remove(driverId);
            } else {
                driverWorker.ping();
            }
        }
    }
}
