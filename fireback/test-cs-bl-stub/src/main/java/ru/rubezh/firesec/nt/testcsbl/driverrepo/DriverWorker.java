package ru.rubezh.firesec.nt.testcsbl.driverrepo;

import java.lang.reflect.Field;
import java.util.*;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Value;

import ru.rubezh.firesec.nt.amqp.message.request.*;
import ru.rubezh.firesec.nt.amqp.message.response.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionCheckType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.TracingEntityType;

public class DriverWorker {
    private Logger logger;
    private DriverRepository driverRepository;

    private String driverId;
    private String alertsExchangeName;

    private RabbitTemplate rabbitTemplate;
    private Queue requestQueue;
    private Queue responseQueue;

    long lastResponseTimeMillis;

    private SimpleMessageListenerContainer listenerContainer;

    private MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
        @Override
        public Message postProcessMessage(Message message) throws AmqpException {
            message.getMessageProperties().setReplyTo(responseQueue.getName());
            message.getMessageProperties().setCorrelationIdString(UUID.randomUUID().toString());
            return message;
        }
    };

    @Value("#{'${serial}'}")
    private String serial;

    public DriverWorker(Logger logger, DriverRepository driverRepository, String driverId,
            String requestQueueName, String responseQueueName, String alertsExchangeName,
            RabbitTemplate rabbitTemplate) {
        this.logger = logger;
        this.driverRepository = driverRepository;
        this.driverId = driverId;
        this.rabbitTemplate = rabbitTemplate;
        this.alertsExchangeName = alertsExchangeName;
        this.requestQueue = new Queue(requestQueueName);
        this.responseQueue = new Queue(responseQueueName);
        this.lastResponseTimeMillis = System.currentTimeMillis();

        listenerContainer = new SimpleMessageListenerContainer();
        listenerContainer.setConnectionFactory(rabbitTemplate.getConnectionFactory());
        listenerContainer.setQueueNames(responseQueue.getName());
        listenerContainer.setMessageListener(new MessageListenerAdapter(new Object() {
            void handleMessage(PingResponse pingResponse) {
                logger.info("Got ping response from {}", pingResponse.getDriverId());
                lastResponseTimeMillis = System.currentTimeMillis();
            }

            void handleMessage(SetActiveProjectResponse response) {
                logger.info("Device tree sending OK, got response from {}", response.getDriverId());


//                new Timer().schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        logger.info("Send READ_CONFIG_FROM_DEVICE request to driver");
//                        Issue issue = new Issue();
//                        issue.setDeviceId("am4t_1");
//                        issue.setProjectId("projectTest");
//                        issue.setAction(IssueAction.READ_CONFIG_FROM_DEVICE);
//
//                        CreateIssueDriverRequest request = new CreateIssueDriverRequest();
//                        request.setDeviceId("am4t_1");
//                        request.setDriverId(driverId);
//                        request.setProjectId("projectTest");
//                        request.setIssue(issue);
//                        rabbitTemplate.convertAndSend(requestQueue.getName(), request, messagePostProcessor);
//                    }
//                }, 1000);
//
//                new Timer().schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        logger.info("Send WRITE_CONFIG_TO_DEVICE request to driver");
//                        Issue issue = new Issue();
//                        issue.setDeviceId("am4t_1");
//                        issue.setProjectId("projectTest");
//                        issue.setAction(IssueAction.WRITE_CONFIG_TO_DEVICE);
//
//                        CreateIssueDriverRequest request = new CreateIssueDriverRequest();
//                        request.setDeviceId("am4t_1");
//                        request.setDriverId(driverId);
//                        request.setProjectId("projectTest");
//                        request.setIssue(issue);
//
//                        Map<String, String> propertyValues = new HashMap<>();
//                        propertyValues.put("State1", "0");
//                        propertyValues.put("EnableSecond", "2");
//                        propertyValues.put("Config", "2");
//                        propertyValues.put("Limit1", "40");
//                        propertyValues.put("Limit2", "74");
//                        propertyValues.put("Limit3", "100");
//                        propertyValues.put("Noise", "5");
//                        request.setPropertyValues(propertyValues);
//                        rabbitTemplate.convertAndSend(requestQueue.getName(), request, messagePostProcessor);
//                    }
//                }, 5000);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        logger.info("Send WRITE_CONTROL_DEVICE_DATABASE request to driver");
                        Issue issue = new Issue();
                        issue.setDeviceId("rubezh2op3_1");
                        issue.setProjectId("projectTest");
                        issue.setAction(IssueAction.WRITE_CONTROL_DEVICE_DATABASE);

                        CreateIssueDriverRequest request = new CreateIssueDriverRequest();
                        request.setDeviceId("rubezh2op3_1");
                        request.setDriverId(driverId);
                        request.setProjectId("projectTest");
                        request.setIssue(issue);

                        rabbitTemplate.convertAndSend(requestQueue.getName(), request, messagePostProcessor);
                    }
                }, 2000);
            }

            void handleMessage(CreateIssueDriverResponse response) {
                logger.info("CreateIssueDriverResponse sending OK, got response from {}", response.getDriverId());
            }

            void handleMessage(ErrorResponse errorResponse) {
                logger.warn("Got error response from {}: {}", errorResponse.getDriverId(), errorResponse.getErrorString());
                lastResponseTimeMillis = System.currentTimeMillis();
            }
        }, rabbitTemplate.getMessageConverter()));
        listenerContainer.start();
    }

    public DriverRepository getDriverRepository() {
        return driverRepository;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getAlertsExchangeName() {
        return alertsExchangeName;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public Queue getRequestQueue() {
        return requestQueue;
    }

    public Queue getResponseQueue() {
        return responseQueue;
    }

    public long getLastResponseTimeMillis() {
        return lastResponseTimeMillis;
    }

    public void sendDeviceTree() {
        VirtualState state1 = new VirtualState();
        state1.setProjectId("projectTest");
        state1.setName("Состояние 1");
        state1.setGlobalNo(1);
        state1.setMessageOn("Состояние 1 уст");
        state1.setMessageOff("Состояние 1 сн");
        state1.setDeviceId("rubezh2op3_1");

        Region fireRegionInstance = new Region();
        fireRegionInstance.setProjectId("projectTest");
        fireRegionInstance.setName("Пожарная зона");
        fireRegionInstance.setIndex(1);
        fireRegionInstance.setSubsystem(Subsystem.FIRE);
        fireRegionInstance.setFireEventCount(2);
        ActiveRegion fireRegionActiveInstance = new ActiveRegion(fireRegionInstance);

        Region secRegionInstance = new Region();
        secRegionInstance.setProjectId("projectTest");
        secRegionInstance.setName("Охранная зона");
        secRegionInstance.setIndex(2);
        secRegionInstance.setSubsystem(Subsystem.SECURITY);
        ActiveRegion secRegionActiveInstance = new ActiveRegion(secRegionInstance);

        Device amod1Instance = new Device("amod1DeviceProfile", DeviceCategory.TRANSPORT,
                "projectTest", null, 0, 1, 33, ",33");
        //amod1Instance.setSerialNo(serial);
        ActiveDevice amod1ActiveInstance = new ActiveDevice(amod1Instance);
        amod1ActiveInstance.setParentDeviceId(amod1Instance.getParentDeviceId());

        Device rubezh2op3Instance = new Device("rubezh2op3DeviceProfile", DeviceCategory.CONTROL,
                "projectTest", "amod1_1", 33, 1, 1, ",33,1,1");
        ActiveDevice rubezh2op3ActiveInstance = new ActiveDevice(rubezh2op3Instance);
        rubezh2op3ActiveInstance.setParentDeviceId(rubezh2op3Instance.getParentDeviceId());

        Long fireEventCount = 62000L;
        Long secEventCount = 4390L;
        rubezh2op3ActiveInstance.getEventCounts().put(Subsystem.FIRE, fireEventCount);
        rubezh2op3ActiveInstance.getEventCounts().put(Subsystem.SECURITY, secEventCount);
        rubezh2op3ActiveInstance.setGuid("020be02f-16f9-d443-b753-b0f3f3f03ce3");

        Device am4ContainerInstance = new Device("am4ContainerDeviceProfile", DeviceCategory.SENSOR,
                "projectTest", "rubezh2op3_1", 1, 1, 244, ",1,1,1,1,244");
        ActiveDevice am4ContainerActiveInstance = new ActiveDevice(am4ContainerInstance);
        am4ContainerActiveInstance.setParentDeviceId(am4ContainerInstance.getParentDeviceId());

        Device am4s1Instance = new Device("am4SecDeviceProfile", DeviceCategory.SENSOR,
                "projectTest", "am4", 1, 1, 244, ",1,1,1,1,244");
        am4s1Instance.setRegionId("sec_1");
        ActiveDevice am4s1ActiveInstance = new ActiveDevice(am4s1Instance);
        am4s1ActiveInstance.setParentDeviceId(am4s1Instance.getParentDeviceId());
        am4s1ActiveInstance.setRegionId(am4s1Instance.getRegionId());

        Device am4f1Instance = new Device("am4FireDeviceProfile", DeviceCategory.SENSOR,
                "projectTest", "am4", 1, 1, 247, ",1,1,1,1,247");
        am4f1Instance.setRegionId("fire_1");
        ActiveDevice am4f1ActiveInstance = new ActiveDevice(am4f1Instance);
        am4f1ActiveInstance.setParentDeviceId(am4f1Instance.getParentDeviceId());
        am4f1ActiveInstance.setRegionId(am4f1Instance.getRegionId());

        Device rm4ContainerInstance = new Device("rm4ContainerDeviceProfile",
                DeviceCategory.EXECUTIVE, "projectTest", "rubezh2op3_1", 1, 1, 240, ",1,1,1,1,240");
        ActiveDevice rm4ContainerActiveInstance = new ActiveDevice(rm4ContainerInstance);
        rm4ContainerActiveInstance.setParentDeviceId(rm4ContainerInstance.getParentDeviceId());

        Device rm41Instance = new Device("rm4DeviceProfile", DeviceCategory.EXECUTIVE,
                "projectTest", "rm4", 1, 1, 240, ",1,1,1,1,240");
        ActiveDevice rm41ActiveInstance = new ActiveDevice(rm41Instance);
        rm41ActiveInstance.setParentDeviceId(rm41Instance.getParentDeviceId());


        ScenarioTimeLineBlock.Action action1 = new ScenarioTimeLineBlock.Action();
        action1.setActionTypeId("satVirtualStateSetActive");
        action1.setEntityId("state_1");
        List<ScenarioTimeLineBlock.Action> actionList1 = new ArrayList<>();
        actionList1.add(action1);
        ScenarioTimeLineBlock execBlock1 = new ScenarioTimeLineBlock();
        execBlock1.setBlockType(BlockType.EXECUTIVE);
        execBlock1.setConditionType(ConditionType.DEVICE_STATE);
        execBlock1.setConditionCheckType(ConditionCheckType.IS_ACTIVE);
        execBlock1.setConditionDeviceStateId("sttAmTOnOff");
        execBlock1.setConditionEntityId("am4s_1");
        execBlock1.setTimeDelaySec(0);
        execBlock1.setActions(actionList1);

        ScenarioTimeLineBlock execBlock2 = new ScenarioTimeLineBlock();
        execBlock2.setBlockType(BlockType.TRACING);
        execBlock2.setTimeDelaySec(5);
        execBlock2.setTracingStartSec(5);
        execBlock2.setTracingPeriodSec(10);
        execBlock2.setTracingEntityType(TracingEntityType.SENSOR_DEVICE);
        execBlock2.setTracingEntityId("am4s_1");
        execBlock2.setTracingBlockNo(1);

        ScenarioTimeLineBlock.Action action3 = new ScenarioTimeLineBlock.Action();
        action3.setActionTypeId("satExecDeviceOn");
        action3.setEntityId("rm4_1");
        List<ScenarioTimeLineBlock.Action> actionList3 = new ArrayList<>();
        actionList3.add(action3);
        ScenarioTimeLineBlock execBlock3 = new ScenarioTimeLineBlock();
        execBlock3.setBlockType(BlockType.EXECUTIVE);
        execBlock3.setConditionType(ConditionType.DEVICE_STATE);
        execBlock3.setConditionCheckType(ConditionCheckType.IS_ACTIVE);
        execBlock3.setConditionDeviceStateId("sttAmTOffOn");
        execBlock3.setConditionEntityId("am4s_1");
        execBlock3.setTimeDelaySec(10);
        execBlock3.setActions(actionList3);
        execBlock3.setHavingTracingBlock(true);
        execBlock3.setTracingBlockNo(1);


        ScenarioTimeLineBlock.Action action4 = new ScenarioTimeLineBlock.Action();
        action4.setActionTypeId("satExecDeviceOn");
        action4.setEntityId("rm4_1");
        List<ScenarioTimeLineBlock.Action> actionList4 = new ArrayList<>();
        actionList4.add(action4);
        ScenarioTimeLineBlock execBlock4 = new ScenarioTimeLineBlock();
        execBlock4.setBlockType(BlockType.EXECUTIVE);
        execBlock4.setConditionType(ConditionType.DEVICE_STATE);
        execBlock4.setConditionCheckType(ConditionCheckType.IS_ACTIVE);
        execBlock4.setConditionDeviceStateId("sttAmTOnOn");
        execBlock4.setConditionEntityId("am4s_1");
        execBlock4.setTimeDelaySec(20);
        execBlock4.setActions(actionList4);

        Scenario scenarioInstance1 = new Scenario();
        scenarioInstance1.setProjectId("projectTest");
        scenarioInstance1.setGlobalNo(1);
        scenarioInstance1.setName("Сценарий 1");
        scenarioInstance1.setScenarioPurpose(ScenarioPurpose.EXEC_BY_LOGIC);
        scenarioInstance1.setScenarioType(ScenarioType.UNASSIGNED);
        List<ScenarioTimeLineBlock> timeLineBlocks1 = new ArrayList<>();
        timeLineBlocks1.add(execBlock1);
        timeLineBlocks1.add(execBlock2);
        timeLineBlocks1.add(execBlock3);
        timeLineBlocks1.add(execBlock4);
        scenarioInstance1.setTimeLineBlocks(timeLineBlocks1);
        ActiveScenario scenarioActiveInstance1 = new ActiveScenario(scenarioInstance1);


        try {
            Field idField = BasicEntity.class.getDeclaredField("id");
            idField.setAccessible(true);

            idField.set(state1, "state_1");

            idField.set(fireRegionInstance, "fire_1");
            idField.set(fireRegionActiveInstance, fireRegionInstance.getId());
            idField.set(secRegionInstance, "sec_1");
            idField.set(secRegionActiveInstance, secRegionInstance.getId());

            idField.set(amod1Instance, "amod1_1");
            idField.set(amod1ActiveInstance, amod1Instance.getId());
            idField.set(rubezh2op3Instance, "rubezh2op3_1");
            idField.set(rubezh2op3ActiveInstance, rubezh2op3Instance.getId());
            idField.set(am4ContainerInstance, "am4");
            idField.set(am4ContainerActiveInstance, am4ContainerInstance.getId());
            idField.set(am4s1Instance, "am4s_1");
            idField.set(am4s1ActiveInstance, am4s1Instance.getId());;
            idField.set(am4f1Instance, "am4f_1");
            idField.set(am4f1ActiveInstance, am4f1Instance.getId());
            idField.set(rm4ContainerInstance, "rm4");
            idField.set(rm4ContainerActiveInstance, rm4ContainerInstance.getId());
            idField.set(rm41Instance, "rm4_1");
            idField.set(rm41ActiveInstance, rm41Instance.getId());

            idField.set(scenarioInstance1, "sc1");
            idField.set(scenarioActiveInstance1, scenarioInstance1.getId());
        }
        catch (NoSuchFieldException | IllegalAccessException e) {
            logger.error(e.getMessage());
        }

        List<ActiveRegion> regions = new ArrayList<>();
        regions.add(secRegionActiveInstance);
        regions.add(fireRegionActiveInstance);

        List<ActiveDevice> devices = new ArrayList<>();
        devices.add(amod1ActiveInstance);
        devices.add(rubezh2op3ActiveInstance);
        devices.add(am4ContainerActiveInstance);
        devices.add(am4f1ActiveInstance);
        devices.add(am4s1ActiveInstance);
        devices.add(rm4ContainerActiveInstance);
        devices.add(rm41ActiveInstance);

        List<ActiveScenario> scenarios = new ArrayList<>();
        scenarios.add(scenarioActiveInstance1);

        List<VirtualState> virtualStates = new ArrayList<>();
        virtualStates.add(state1);

        SetActiveProjectRequest setActiveProjectRequest = new SetActiveProjectRequest();
        setActiveProjectRequest.setProjectId("projectTest");
        setActiveProjectRequest.setDriverId(driverId);
        setActiveProjectRequest.setDevices(devices);
        setActiveProjectRequest.setRegions(regions);
        setActiveProjectRequest.setScenarios(scenarios);
        setActiveProjectRequest.setVirtualStates(virtualStates);

        logger.info("Sending device tree to {}", driverId);
        rabbitTemplate.convertAndSend(requestQueue.getName(), setActiveProjectRequest, messagePostProcessor);
    }

    public void ping() {
        if (rabbitTemplate != null) {
            PingRequest req = new PingRequest();
            req.setDriverId(getDriverId());
            if (logger != null) {
                logger.info("Sending ping to {}", driverId);
            }
            rabbitTemplate.convertAndSend(requestQueue.getName(), req, message -> {
                message.getMessageProperties().setReplyTo(responseQueue.getName());
                message.getMessageProperties().setCorrelationIdString(UUID.randomUUID().toString());
                return message;
            });

        }
    }
}
