Тест драйвера Рубеж-2ОП3 с помощью заглушки коммуникационного и БЛ серверов
=====================================================================

Последовательность действий:
----------------------------

1. Установить сервер очередей: https://www.rabbitmq.com/download.html
2. Включить плагин WEB-managment:
    * Перейти в каталог sbin с установленным дистрибутивом RabbitMQ
    * Выполнить:
     
            ./rabbitmq-plugins.bat enable rabbitmq_management

3. Через WEB-интерфейс добавить:
    * exchange ru.rubezh.firesec.nt.alerts
    * очередь ru.rubezh.firesec.nt.drv.2op3.requests
    * очередь ru.rubezh.firesec.nt.drv.2op3.response
4. Скомпилировать fireback:
    * перейти в директорию firesec/fireback
    * выполнить:
    
            mvn clean install

5. Запустить заглушку:
    * перейти в директорию firesec/fireback/test-cs-bl-stub
    * выполнить:
    
            java -jar target/test-cs-bl-stub*.jar
    
6. Запустить драйвер 2ОП3:
    * перейти в директорию firesec/fireback/driver-2OP3
    * выполнить:
    
            java -jar target/driver-2OP3.jar -a ru.rubezh.firesec.nt.alerts -i 2op3 -r ru.rubezh.firesec.nt.drv.2op3.requests
        
7. Подключить по USB устройство 2ОП3 через МС-1 с серийным номером "000000000000".


Ожидаемый результат:
--------------------

1. После запуска, драйвер должен получить дерево устройств от заглушки и в стандартный вывод должен каждую секунду писать, что его пингуют:

        23:32:58.642 [driver-2OP3-0.12.4][r.r.f.n.p.d.DriverRequestHandlerImpl.handleMessage] INFO  Got ping request
        23:32:58.682 [driver-2OP3-0.12.4][r.r.f.n.p.d.DriverRequestHandlerImpl.handleMessage] INFO  Got set device tree request
        23:32:59.471 [driver-2OP3-0.12.4][r.r.f.n.p.d.DriverRequestHandlerImpl.handleMessage] INFO  Got ping request

    И заглушка должна писать, что зарегистрировала драйвер, отправила дерево устройств и получает ответ на пинг:

        23:32:58.543 [test-cs-bl-stub-0.12.4][r.r.f.n.t.AlertsListener.handleMessage] INFO  Driver registered. Id: 2op3, requests queue: ru.rubezh.firesec.nt.drv.2op3.requests, alerts exchange: ru.rubezh.firesec.nt.alerts
        23:32:58.550 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker.sendDeviceTree] INFO  Sending device tree to 2op3
        23:32:59.467 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker.ping] INFO  Sending ping to 2op3
        23:32:59.473 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker.handleMessage] INFO  Got ping response from 2op3
        23:33:00.467 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker.ping] INFO  Sending ping to 2op3
        23:33:00.470 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker.handleMessage] INFO  Got ping response from 2op3

2. После подключения устройства 2ОП3 драйвер должен подключиться к ней, сконфигурировать МС-1, получить конфигурацию 2ОП3 (попутно отправляя оповещения о состоянии устройств) и выйти на состояние WAITING_TASK:

        23:32:58.692 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.onConnectRequest] TRACE Enter
        23:32:59.173 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.setState] TRACE Adapter module (sn: null) state transition: IDLE -> CONNECTING
        23:32:59.173 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onConnectRequest] TRACE Enter
        23:32:59.174 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.onConnectRequest] TRACE Enter
        23:32:59.174 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: IDLE -> CONNECTING
        23:32:59.225 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.onConnected] TRACE Enter
        23:32:59.226 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.RemoveActiveDeviceState.
        23:32:59.231 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.setState] TRACE Adapter module (sn: null) state transition: CONNECTING -> WAIT_CONFIG
        23:32:59.475 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.onConfigReceived] TRACE Enter
        23:32:59.477 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.setState] TRACE Adapter module (sn: null) state transition: WAIT_CONFIG -> SETTING_CONFIG
        23:32:59.754 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.onSetConfigConfirmed] TRACE Enter
        23:32:59.755 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onConnected] TRACE Enter
        23:32:59.755 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.RemoveActiveDeviceState.
        23:32:59.757 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: CONNECTING -> WAITING_DEVICE_CONFIGURATION
        23:32:59.758 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceStates.
        23:32:59.762 [driver-2OP3-0.12.4][r.r.f.n.p.d.a.s.StateRelay.setState] TRACE Adapter module (sn: null) state transition: SETTING_CONFIG -> READY
        23:33:00.144 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onDeviceConfigurationReceived] TRACE Enter
        23:33:00.145 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.DeviceInfo.
        23:33:00.534 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onDeviceMaxEventCountersReceived] TRACE Enter
        23:33:00.535 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: WAITING_DEVICE_CONFIGURATION -> WAITING_TASK

3. Далее драйвер должен периодически выполнять задания на проверку счетчиков событий, выгрузку новых, и т.д.

4. Через 1 секунду после отправки дерева устройств, заглушка должна отправить запрос драйверу на чтение конфигурации подключенного к 2ОП3 АМ-4Т, а через 5 секунд - на установку конфигурации:

        23:43:42.967 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker$2.handleMessage] INFO  ReadActiveDeviceConfig sending OK, got response from 2op3
        ...
        23:43:42.992 [test-cs-bl-stub-0.12.4][r.r.f.n.t.d.DriverWorker$2.handleMessage] INFO  WriteActiveDeviceConfig sending OK, get response from 2op3


    Драйвер должен, во-первых, написать в лог о том что эти сообщения получил:

        23:43:44.518 [driver-2OP3-0.12.4][r.r.f.n.p.d.DriverRequestHandlerImpl.handleMessage] INFO  Got read active device config request (deviceId=am4t_1)
        ...
        23:43:48.520 [driver-2OP3-0.12.4][r.r.f.n.p.d.DriverRequestHandlerImpl.handleMessage] INFO  Got write active device config request (deviceId=am4t_1, propertyValues={State1=0, Limit3=100, Noise=5, EnableSecond=2, Config=2, Limit2=74, Limit1=40})

    А во-вторых, должен на каждый запрос перейти в соответствующее состояние, из которого отправить оповещение с конфигом устройства:

        23:43:45.119 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: WAITING_TASK -> WAITING_CHILD_DEVICE_CONFIG
        23:43:45.261 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onChildDeviceConfigReceived] TRACE Enter
        23:43:45.261 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.ActiveDeviceConfig.
        23:43:45.265 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: WAITING_CHILD_DEVICE_CONFIG -> WAITING_TASK
        ...
        23:43:49.117 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: WAITING_TASK -> SETTING_CHILD_DEVICE_CONFIG
        23:43:49.259 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.onChildDeviceConfigSetted] TRACE Enter
        23:43:49.259 [driver-2OP3-0.12.4][r.r.f.n.a.s.DriverAlertSender.send] TRACE Driver alert message send. Message type: ru.rubezh.firesec.nt.amqp.message.alert.ActiveDeviceConfig.
        23:43:49.263 [driver-2OP3-0.12.4][r.r.f.n.p.d.r.s.StateRelay.setState] TRACE Rubezh2OP3 (sn: null) state transition: SETTING_CHILD_DEVICE_CONFIG -> WAITING_TASK

    Заглушка должна соответственно и на запрос чтения, и на запрос записи получить от драйвера оповещение с конфигом:

        23:43:45.267 [test-cs-bl-stub-0.12.4][r.r.f.n.t.AlertsListener.handleMessage] INFO  Got active device config for device "am4t_1": {State1=0, Limit3=100, Noise=5, Config=2, EnableSecond=2, Limit2=74, Limit1=40}
        ...
        23:43:49.263 [test-cs-bl-stub-0.12.4][r.r.f.n.t.AlertsListener.handleMessage] INFO  Got active device config for device "am4t_1": {State1=0, Limit3=100, Noise=5, EnableSecond=2, Config=2, Limit2=74, Limit1=40}
