package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.dao.v1.LogMonitorableValueRepository;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;

@Service
public class LogMonitorableValueServiceImpl implements LogMonitorableValueService {

    @Autowired
    Logger logger;

    @Autowired
    private LogMonitorableValueRepository logMonitorableValueRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    private Map<String, String> needToSave(ActiveDevice device, List<MonitorableValue> monitorableValueDescriptions,
                                           Map<String, String> monitorableValues) {
        Map<String, String> valuesToSave = new HashMap<>();
        for (MonitorableValue monitorableValue : monitorableValueDescriptions) {
            LogMonitorableValue lastValue = logMonitorableValueRepository.
                    findFirstByProjectIdAndDeviceIdAndProfileIdOrderByReceivedDesc(
                            device.getProjectId(), device.getId(), monitorableValue.getProfile().getId());
            String currentValue = lastValue == null ? monitorableValue.getProfile().getDefaultValue() : lastValue.getValue();
            String newValue = monitorableValues.getOrDefault(monitorableValue.getProfile().getId(),
                    monitorableValue.getProfile().getDefaultValue());
            double diff = Math.abs(Double.valueOf(currentValue) - Double.valueOf(newValue));
            if (diff >= monitorableValue.getProfile().getUpdateMinStep()) {
                valuesToSave.put(monitorableValue.getProfile().getId(), newValue);
            }
        }
        return valuesToSave;
    }

    @Override
    public void saveAll(ActiveDevice device, Map<String, String> monitorableValues, Date currentTime, boolean force) {
        List<LogMonitorableValue> logValues = new ArrayList<>();
        DeviceProfile profile = deviceProfileRepository.findOne(device.getDeviceProject().getDeviceProfileId());
        List<MonitorableValue> monitorableValueDescriptions = profile.getCustomMonitorableValues();

        if (!force) monitorableValues = needToSave(device, monitorableValueDescriptions, monitorableValues);

        for (MonitorableValue monitorableValueDescription : monitorableValueDescriptions) {
            String newValue = monitorableValues.get(monitorableValueDescription.getProfile().getId());

            if (newValue != null) {
                logValues.add(new LogMonitorableValue(monitorableValueDescription.getProfile().getId(),
                        device.getId(), device.getProjectId(), currentTime, newValue));
            }
        }
        logMonitorableValueRepository.insert(logValues);
    }

    @Override
    public Page<LogMonitorableValue> getByProjectIdAndDeviceAndReceivedRange(
            String projectId, String deviceId, Date from, Date to, Pageable pageable) {
        if (from == null) from = new Date(0);
        if (to == null) to = new Date();
        return logMonitorableValueRepository.
                findAllByProjectIdAndDeviceIdAndReceivedBetweenOrderByReceivedAsc(
                        projectId, deviceId, from, to, pageable);
    }

    @Override
    public void removeByReceivedBefore(Date received) {
        logMonitorableValueRepository.deleteAllByReceivedBefore(received);
    }

}
