package ru.rubezh.firesec.nt.service.v1;

public interface ActiveScenarioService extends ActiveEntityWithStatesService {

    public void addActiveState(String scenarioId, String stateId);
    public void removeActiveState(String scenarioId, String stateId);

}
