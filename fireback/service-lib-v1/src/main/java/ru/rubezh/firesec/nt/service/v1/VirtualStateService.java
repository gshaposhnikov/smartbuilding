package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.TracingEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Order(4)
public class VirtualStateService implements ActivationFilterableService {

    private static final int MIN_VIRTUAL_STATE_GLOBAL_NO = 1;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private StateCategoryRepository stateCategoryRepository;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;
    
    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;
    
    @Autowired
    private ScenarioRepository scenarioRepository;

    private boolean validate(VirtualState virtualState, StringBuilder errorMessage) {
        boolean valid = false;

        /* Проверка обязательных для заполнения полей */
        if (virtualState.getName().isEmpty())
            errorMessage.append("Поле название: не задано");
        else if (virtualState.getDeviceId() == null || virtualState.getDeviceId().isEmpty())
            errorMessage.append("Поле прибор: не задано");
        else if (virtualState.getStateCategoryId() == null)
            errorMessage.append("Поле класс состояния: не задано");
        else if (virtualState.getMessageOn().isEmpty())
            errorMessage.append("Поле сообщение при постановке: не задано");
        else if (virtualState.getMessageOff().isEmpty())
            errorMessage.append("Поле сообщение при пропадании: не задано");

        /* Проверка на граничные значения */
        else if (virtualState.getName().length() > VirtualState.MESSAGE_AND_NAME_MAX_LENGTH)
            errorMessage.append("Поле название: превышена максимальная длина (длина:")
                    .append(virtualState.getName().length()).append(", макс: ")
                    .append(VirtualState.MESSAGE_AND_NAME_MAX_LENGTH).append(")");
        else if (virtualState.getMessageOn().length() > VirtualState.MESSAGE_AND_NAME_MAX_LENGTH)
            errorMessage.append("Поле сообщение при постановке: превышена максимальная длина (длина:")
                    .append(virtualState.getMessageOn().length()).append(", макс: ")
                    .append(VirtualState.MESSAGE_AND_NAME_MAX_LENGTH).append(")");
        else if (virtualState.getMessageOff().length() > VirtualState.MESSAGE_AND_NAME_MAX_LENGTH)
            errorMessage.append("Поле сообщение при пропадании: превышена максимальная длина (длина:")
                    .append(virtualState.getMessageOff().length()).append(", макс: ")
                    .append(VirtualState.MESSAGE_AND_NAME_MAX_LENGTH).append(")");
        else if (virtualState.getDescription().length() > VirtualState.DESCRIPTION_MAX_LENGTH)
            errorMessage.append("Поле примечание: превышена максимальная длина (длина:")
                    .append(virtualState.getDescription().length()).append(", макс: ")
                    .append(VirtualState.DESCRIPTION_MAX_LENGTH).append(")");
        else if (virtualState.getGlobalNo() < MIN_VIRTUAL_STATE_GLOBAL_NO)
            errorMessage.append("Номер должен быть больше нуля (")
                    .append(virtualState.getGlobalNo()).append(")");

        /* Проверка на уникальность глобального номера */
        else if (virtualStateRepository.countByProjectIdAndGlobalNoAndIdNot(
                virtualState.getProjectId(), virtualState.getGlobalNo(), virtualState.getId()) > 0) {
            errorMessage.append("Номер ").append(virtualState.getGlobalNo())
                    .append(" уже используется");
        }

        /* Проверка на наличие нужных объектов */
        else {
            Device device = deviceRepository.findByProjectIdAndId(
                    virtualState.getProjectId(), virtualState.getDeviceId());
            if (device == null)
                errorMessage.append("Устройство не найдено (")
                        .append(virtualState.getDeviceId()).append(")");
            else {
                DeviceProfile deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId());
                if (deviceProfile.getDeviceCategory() != DeviceCategory.CONTROL)
                    errorMessage.append("Устройство не является панелью (")
                            .append(virtualState.getDeviceId()).append(")");
                else if (stateCategoryRepository.findOne(virtualState.getStateCategoryId()) == null)
                    errorMessage.append("Класс состояния не найден (")
                            .append(virtualState.getStateCategoryId()).append(")");
                else
                    valid = true;
            }
        }

        return valid;
    }

    private VirtualState validateOnUpdate(String virtualStateId, VirtualState virtualState,
                                          StringBuilder errorMessage) {
        VirtualState savedVirtualState = null;

        if (virtualStateId == null || virtualStateId.isEmpty()) {
            errorMessage.append("Необходимо указать идентификатор виртуального состояния");
        } else {
            savedVirtualState = virtualStateRepository.findByIdAndProjectId(virtualStateId, virtualState.getProjectId());
            if (savedVirtualState == null) {
                errorMessage.append("Вирт. состояние (").append(virtualStateId)
                        .append(") не найдено в проекте (").append(virtualState.getProjectId()).append(")");
            } else {
                savedVirtualState.update(virtualState);
                if (!validate(virtualState, errorMessage))
                    savedVirtualState = null;
            }
        }

        return savedVirtualState;
    }

    public VirtualState create(VirtualState virtualState, StringBuilder errorMessage) {
        VirtualState savedVirtualState = null;

        if (projectService.checkProjectIsEditable(virtualState.getProjectId(), errorMessage) &&
                validate(virtualState, errorMessage)) {
            savedVirtualState = virtualStateRepository.insert(virtualState);
        }

        return savedVirtualState;
    }

    public VirtualState update(String id, VirtualState virtualState, StringBuilder errorMessage) {
        VirtualState existedVirtualState = null;

        if (projectService.checkProjectIsEditable(virtualState.getProjectId(), errorMessage)) {
            existedVirtualState = validateOnUpdate(id, virtualState, errorMessage);
            if (existedVirtualState != null)
                existedVirtualState = virtualStateRepository.save(existedVirtualState);
        }

        return existedVirtualState;
    }

    private boolean checkLogicBlockContainsVirtualState(Scenario scenario, ScenarioLogicBlock logicBlock, String virtualStateId, StringBuilder errorMessage) {
        ScenarioTriggerType triggerType = scenarioTriggerTypeRepository.findOne(logicBlock.getTriggerTypeId());
        if (triggerType != null && triggerType.getEntityType() == TriggerEntityType.VIRTUAL_STATE && logicBlock.getEntityIds().contains(virtualStateId)) {
            errorMessage.append("Вирт. состояний используется в сценарии " + scenario.getName() + " в блоке логики");
            return true;
        }
        for (ScenarioLogicBlock subLogic: logicBlock.getSubLogics()) {
            if (checkLogicBlockContainsVirtualState(scenario, subLogic, virtualStateId, errorMessage)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean removeByIdAndProjectId(String id, String projectId, StringBuilder errorMessage) {

        if (!projectService.checkProjectIsEditable(projectId, errorMessage)){
            return false;
        }
            
        /*  Проверим, что вирт. состояние не используется в сценариях */
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        for (Scenario scenario: scenarios) {
            for (ScenarioTimeLineBlock timeLineBlock: scenario.getTimeLineBlocks()) {
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE) {
                    for (Action action: timeLineBlock.getActions()) {
                        ScenarioActionType actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
                        if (actionType.getEntityType() == ActionEntityType.VIRTUAL_STATE) {
                            if (action.getEntityId().equals(id)) {
                                errorMessage.append("Вирт. состояний используется в сценарии " + scenario.getName() + " в одном из действий");
                                return false;
                            }
                        }
                    }
                    if (timeLineBlock.getConditionType() == ConditionType.VIRTUAL_STATE && timeLineBlock.getConditionEntityId().equals(id)) {
                        errorMessage.append("Вирт. состояний используется в сценарии " + scenario.getName() + " в условии исполнительного блока");
                        return false;
                    }
                }
                else if (timeLineBlock.getBlockType() == BlockType.TRACING) {
                    if (timeLineBlock.getTracingEntityType() == TracingEntityType.VIRTUAL_STATE && timeLineBlock.getTracingEntityId().equals(id)) {
                        errorMessage.append("Вирт. состояний используется в сценарии " + scenario.getName() + " в блоке слежения");
                        return false;
                    }
                }
            }
            if (checkLogicBlockContainsVirtualState(scenario, scenario.getStartLogic(), id, errorMessage)) {
                return false;
            }
            if (scenario.getStopType() == StopType.BY_LOGIC && scenario.getStopLogic() != null && checkLogicBlockContainsVirtualState(scenario, scenario.getStopLogic(), id, errorMessage)) {
                return false;
            }
        }

        Long count = virtualStateRepository.deleteByIdAndProjectId(id, projectId);
        if (count == null || count == 0) {
            errorMessage.append("Виртуальное состояние (").append(id)
            .append(") не найдено в проекте (").append(projectId).append(")");
            return false;
        }

        return true;
    }
    public boolean checkAndAdditionVirtualStateToImportProject(List<VirtualState> virtualStates, String projectId,
                    Map<String, String> oldAndNewVirtualStatesId, Map<String, String> oldAndNewDevicesId, StringBuilder errorMessage) {
        for (VirtualState virtualState : virtualStates) {
            virtualState.setProjectId(projectId);
            String oldVirtualStateId = virtualState.getId();
            if ((virtualState.getDeviceId() != null) && oldAndNewDevicesId.containsKey(virtualState.getDeviceId())) {
                virtualState.setDeviceId(oldAndNewDevicesId.get(virtualState.getDeviceId()));
            } else {
                errorMessage.append("Параметры для виртуального состояния ").append(virtualState.getName()).append(" заданы неверно");
                return false;
            }
            if (!validate(virtualState, errorMessage)) {
                return false;
            }
            virtualState.clearId();
            oldAndNewVirtualStatesId.put(oldVirtualStateId, virtualStateRepository.insert(virtualState).getId());
        }
        return true;
    } 

    private BitSet findUsedVirtualStatesGlobalNos(String projectId) {
        BitSet usedVirtualStatesGlobalNos = new BitSet();
        for (VirtualState virtualState : virtualStateRepository.findByProjectId(projectId))
            usedVirtualStatesGlobalNos.set(virtualState.getGlobalNo());
        return usedVirtualStatesGlobalNos;
    }

    public List<VirtualState> createByRecovery(String projectId, List<VirtualState> virtualStates,
            Map<String, String> oldToNewIds, Map<String, String> deviceIds, StringBuilder errorMessage) {
        // назначение идентификатора проекта и новых глобальных номеров
        BitSet usedVirtualStateGlobalNos = findUsedVirtualStatesGlobalNos(projectId);
        Map<Integer, String> globalNosToOldIds = new HashMap<>();
        for (VirtualState oldVirtualState : virtualStates) {
            oldVirtualState.setProjectId(projectId);
            // поиск свободного номера
            int nextGlobalNo = usedVirtualStateGlobalNos.nextClearBit(MIN_VIRTUAL_STATE_GLOBAL_NO);
            usedVirtualStateGlobalNos.set(nextGlobalNo);
            oldVirtualState.setGlobalNo(nextGlobalNo);
            globalNosToOldIds.put(nextGlobalNo, oldVirtualState.getId());
            // проверка полей и устройства
            String newDeviceId = deviceIds.get(oldVirtualState.getDeviceId());
            if (newDeviceId == null) {
                errorMessage.append("Не найден прибор, к которому относится виртуальное сосотояние " +
                        oldVirtualState.getName());
                return null;
            }
            oldVirtualState.setDeviceId(newDeviceId);
            if (!validate(oldVirtualState, errorMessage))
                return null;
        }

        // добавление в базу
        virtualStates.forEach(VirtualState::clearId);
        virtualStates = virtualStateRepository.insert(virtualStates);

        // заполнение карты индексов
        for (VirtualState newVirtualState : virtualStates)
            oldToNewIds.put(
                    globalNosToOldIds.get(newVirtualState.getGlobalNo()),
                    newVirtualState.getId());

        return virtualStates;
    }

    public void remove(String projectId, List<VirtualState> virtualStates) {
        List<String> virtualStateIds = virtualStates.stream().map(VirtualState::getId).collect(Collectors.toList());
        virtualStateRepository.deleteByProjectIdAndIdIn(projectId, virtualStateIds);
    }

    @Override
    public void clearTags(String projectId) {
        List<VirtualState> virtualStates = virtualStateRepository.findByProjectId(projectId);
        virtualStates.forEach(virtualState -> virtualState.getFilterTags().getTags().clear());
        virtualStateRepository.save(virtualStates);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<VirtualState> virtualStates = virtualStateRepository.findByProjectId(projectId);
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);

        virtualStates.forEach(virtualState -> {
            FilterTags filterTags = virtualState.getFilterTags();

            Set<String> virtualStateTags = filterTags.chooseSubsystemTags();
            List<Scenario> linkedScenarios = findLinkedScenarios(virtualState.getId(), scenarios);
            linkedScenarios.forEach(scenario -> virtualStateTags
                    .addAll(scenario.getFilterTags().chooseSubsystemTags()));
        });

        virtualStateRepository.save(virtualStates);
    }

    private List<Scenario> findLinkedScenarios(String virtualStateId, List<Scenario> scenarios){
        List<Scenario> linkedScenarios = new ArrayList<>();

        for (Scenario scenario : scenarios) {
            //проверка блоков логики на наличие в них виртуального состояния
            if ((!scenario.getStartLogic().isEmpty() && checkLogicBlockContainsVirtualState(scenario,
                    scenario.getStartLogic(), virtualStateId, new StringBuilder()))
                    || (scenario.getStopLogic() != null && !scenario.getStopLogic().isEmpty()
                        && checkLogicBlockContainsVirtualState(scenario,
                    scenario.getStopLogic(), virtualStateId, new StringBuilder()))){
                linkedScenarios.add(scenario);
            // проверка исполнительных блоков на ниличие в них виртуального состояния
            } else {
                for (ScenarioTimeLineBlock scenarioTimeLineBlock : scenario.getTimeLineBlocks()){
                    boolean found = false;
                    for (Action action : scenarioTimeLineBlock.getActions()){
                        if (action.getEntityId().equals(virtualStateId)) {
                            ScenarioActionType scenarioActionType =
                                    scenarioActionTypeRepository.findOne(action.getEntityId());
                            if (scenarioActionType != null
                                    && scenarioActionType.getEntityType() == ActionEntityType.VIRTUAL_STATE){
                                linkedScenarios.add(scenario);
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found) {
                        break;
                    }
                }
            }
        }

        return linkedScenarios;
    }
}
