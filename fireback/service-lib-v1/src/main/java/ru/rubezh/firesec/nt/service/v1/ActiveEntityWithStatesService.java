package ru.rubezh.firesec.nt.service.v1;


import ru.rubezh.firesec.nt.domain.v1.BasicActiveEntityWithStates;
import ru.rubezh.firesec.nt.domain.v1.State;
import ru.rubezh.firesec.nt.domain.v1.StateCategory;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс сервисных функций для работы с оперативными сущностями, хранящими состояния.
 * @author Александр Горячкин, Антон Васильев
 */
public interface ActiveEntityWithStatesService extends ActiveEntityService {

    /**
     * Добавить активные состояния в оперативную сущность.
     * @param entity оперативная сущность
     * @param activeStateIds набор идентификаторов состояний
     * @param commit если true, то будет автоматически вызван метод commitAfterStateUpdated
     */
    void addActiveStates(BasicActiveEntityWithStates entity, Collection<String> activeStateIds, boolean commit);

    /**
     * Удалить активные состояния из оперативной сущности.
     * @param entity оперативная сущность
     * @param activeStateIds набор идентификаторов состояний
     * @param commit если true, то будет автоматически вызван метод commitAfterStateUpdated
     */
    void removeActiveStates(BasicActiveEntityWithStates entity, Collection<String> activeStateIds, boolean commit);

    /**
     * Установить набор наследуемый активных состояний оперативной сущности.
     * 
     * @param entity оперативная сущность
     * @param parentEntityId идентификатор родительской сущности
     * @param inheritedStateIds набор идентификаторов состояний
     * @param commit если true, то будет автоматически вызван метод commitAfterStateUpdated
     */
    void setInheritedActiveStates(BasicActiveEntityWithStates entity, String parentEntityId,
            Set<String> inheritedStateIds, boolean commit);

    /**
     * Подготовить сущность к тому, чтобы зафиксировать изменения оперативных
     * данных. (Запомнить текущее время в качестве времени последнего получения
     * состояния, актуализировать главную категорию состояния и пр.)
     * 
     * @param entity
     *            оперативная сущность с состояниями
     * @param defaultStateCategory
     *            категория состояний по умолчанию
     * @param currentTime
     *            текущее время
     * @param statesHash
     *            хеш состояний (опциональный параметр - допустимо передавать null)
     * @return true, если изменения были
     */
    void prepareCommitAfterStateUpdated(BasicActiveEntityWithStates entity, StateCategory defaultStateCategory, Date currentTime, Map<String, State> statesHash);

    /**
     * Зафиксировать изменения оперативных данных после обновления его
     * состояния.
     * 
     * Подготовка перед фиксацией и сохранение в БД
     * выполняется, если forced == true либо isNeedCommitState == true.
     * 
     * @param entity
     *            оперативная сущность с состояниями
     * @param forced
     *            если true, то флаг необходимости сохранения состояния сущности
     *            игнорируется
     */
    void commitAfterStateUpdated(BasicActiveEntityWithStates entity, boolean forced);

    /**
     * Определить класс состояния по умолчанию
     * 
     * @return класс состояния по умолчанию
     */
    StateCategory getDefaultStateCategory();
    
    /**
     * Обновить приоритетную категорию состояния для наблюдаемой сущности, используя хеш состояний.
     * 
     * @param entity наблюдаемая сущность
     * @param defaultCategory категория состояний по умолчанию
     * @param statesHash хеш состояний
     */
    void updateStateCategory(BasicActiveEntityWithStates entity, StateCategory defaultCategory,
            Map<String, State> statesHash);

}
