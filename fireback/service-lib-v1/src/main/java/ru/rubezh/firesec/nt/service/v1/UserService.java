package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

import ru.rubezh.firesec.nt.dao.v1.UserGroupRepository;
import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.UserGroup;

import java.util.*;

@Service
public class UserService {

    // Идентификатор права на редактирование встроенных пользователей
    public final static String USER_BUILTIN_UPDATE="USER_BUILTIN_UPDATE";

    // Паттерны для валидации
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_PATTERN = "^\\+[0-9]{11}$";

    private static final int USER_NAME_MAX_LENGTH = 30;
    private static final int USER_FULL_NAME_MAX_LENGTH = 100;
    private static final int USER_PASSWORD_MIN_LENGTH = 6;
    private static final int USER_PASSWORD_MAX_LENGTH = 30;
    private static final int USER_EMAIL_MAX_LENGTH = 50;
    private static final int USER_PHONENO_MAX_LENGTH = 12;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    public static boolean userExistPermission(User user, String permission) {
        boolean result = false;
        for (String perm: user.getPermissionIds()) {
            if (perm.equals(permission)) {
                result = true;
                break;
            }
        }
        if (!result) {
            for (String perm: user.getGroupPermissions()) {
                if (perm.equals(permission)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private boolean validateUserData(boolean update, User user, User currentUser, StringBuilder errorMessage) {
        boolean result = false;
        // TODO: Добавить проверки на заполнение индикатора и интерфейса
        if (user.isBuiltin() && !userExistPermission(currentUser, USER_BUILTIN_UPDATE))
            errorMessage.append("Нет права на создание/изменение встроенного пользователя");
        // имя
        else if (user.getName() == null)
            errorMessage.append("Имя пользователя обязательно");
        else if (user.getName().length() > USER_NAME_MAX_LENGTH)
            errorMessage.append("Имя пользователя должно быть не больше ")
                    .append(USER_NAME_MAX_LENGTH).append(" символов");
        // полное имя
        else if (user.getFullName().length() > USER_FULL_NAME_MAX_LENGTH)
            errorMessage.append("Полное имя пользователя должно быть не больше ")
                    .append(USER_FULL_NAME_MAX_LENGTH).append(" символов");
        // email
        else if (user.getEmail() == null)
            errorMessage.append("Email обязателен");
        else if (user.getEmail().length() > USER_EMAIL_MAX_LENGTH)
            errorMessage.append("Email должен быть не больше ")
                    .append(USER_EMAIL_MAX_LENGTH).append(" символов");
        else if (!Pattern.compile(EMAIL_PATTERN).matcher(user.getEmail()).matches())
            errorMessage.append("Email не корректен");
        // телефон
        else if (user.getPhoneNo() == null)
            errorMessage.append("Номер телефона обязателен");
        else if (user.getPhoneNo().length() != USER_PHONENO_MAX_LENGTH)
            errorMessage.append("Номер телефона должен быть ")
                    .append(USER_PHONENO_MAX_LENGTH).append(" символов");
        else if (!Pattern.compile(PHONE_PATTERN).matcher(user.getPhoneNo()).matches())
            errorMessage.append("Номер телефона не корректен");
        // пароль
        else if (user.getPassword() != null) {
            if (user.getPassword().length() < USER_PASSWORD_MIN_LENGTH)
                errorMessage.append("Пароль должен быть не меньше ")
                        .append(USER_PASSWORD_MIN_LENGTH).append(" символов");
            else if (user.getPassword().length() > USER_PASSWORD_MAX_LENGTH)
                errorMessage.append("Пароль должен быть не больше ")
                        .append(USER_PASSWORD_MAX_LENGTH).append(" символов");
            else
                result = true;
        } else if (!update)
            errorMessage.append("Пароль обязателен");
        else
            result = true;

        if (result) {
            // группы
            List<UserGroup> userGroups = userGroupRepository.findByRemovedIsFalseAndIdIn(user.getUserGroupIds());
            if (userGroups == null || userGroups.size() != user.getUserGroupIds().size()) {
                errorMessage.append("Указана не существующая группа пользователей");
                result = false;
            }
        }
        return result;
    }

    public User createNew(User user, User currentUser, StringBuilder errorMessage) {
        User savedUser = null;
        if (validateUserData(false, user, currentUser, errorMessage))
            try {
                savedUser = userRepository.insert(user);
            } catch (DuplicateKeyException ex) {
                errorMessage.append("Данное имя или email занято");
            }
        return savedUser;
    }

    public User update(String id, User user, User currentUser, StringBuilder errorMessage) {
        User result = null;
        if (validateUserData(true, user, currentUser, errorMessage)) {
            User savedUser = userRepository.findOne(id);
            if (savedUser == null)
                errorMessage.append("Пользователь не найден");
            else {
                savedUser.update(user);
                try {
                    result = userRepository.save(savedUser);
                } catch (DuplicateKeyException ex) {
                    errorMessage.append("Данное имя или email занято");
                }
            }
        }
        return result;
    }

    public boolean updateActiveState(String id, boolean newActiveState, User currentUser, StringBuilder errorMessage) {
        boolean result = false;
        User user = userRepository.findOne(id);
        if (user == null)
            errorMessage.append("Пользователь не найден");
        else if (user.isBuiltin() && !userExistPermission(currentUser, UserService.USER_BUILTIN_UPDATE))
            errorMessage.append("Нет права на удаление встроенной группы пользователей");
        else if (user.isActive() != newActiveState) {
            user.setActive(newActiveState);
            user.setUpdateDateTime(new Date());
            userRepository.save(user);
            result = true;
        }
        return result;
    }

    public static User fillGroupPermissions(User user, List<UserGroup> userGroups) {
        Set<String> groupPermissions = new HashSet<>();
        for (UserGroup group : userGroups) {
            groupPermissions.addAll(group.getPermissionIds());
        }
        user.setGroupPermissions(new ArrayList<>(groupPermissions));
        return user;
    }

    public User fillGroupPermissions(User user) {
        return fillGroupPermissions(user, userGroupRepository.findByRemovedIsFalseAndIdIn(user.getUserGroupIds()));
    }
}
