package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.amqp.message.alert.SetEntityStates;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@Service
public class HttpService {

    private static final String address = "http://10.0.0.56/api/external/incident";
    private HttpURLConnection connection;
    private static final String urlFixedParameters = "\"latitude\": 55.7522, \"longitude\": 37.6156, " +
            "\"incident_type\": \"0000019\", \"system_id\": 18";
    private Logger logger;
    private boolean alertSent;

    @Autowired
    public HttpService(Logger logger){
        this.logger = logger;
        try {
            URL url = new URL(address);
            connection = (HttpURLConnection) url.openConnection();
            logger.info("New connection with emergency system is established");
        } catch (IOException e) {
            logger.error("Could not connect to emergency server. {}", e.getMessage());
        }
    }

    public void sendAlertIfNeeded(SetEntityStates message) {
        // TODO: remove hardcode when there will be some other logic for this situation
        Integer alertDeviceValue = message.getDeviceInfoWithValues().get("03;LIGHT;01");
        if (message.getOffRegionNos().contains(3)){
            alertSent = false;
        } else if (alertDeviceValue != null) {
            if (alertDeviceValue == 0) {
                alertSent = false;
            } else if (alertDeviceValue == 5 && !alertSent) {
                sendAlert();
            }
        }
        logger.warn("find me: " + alertSent);
    }

    private void sendAlert(){
        try {
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; utf-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            OutputStream outputStream = connection.getOutputStream();

            int id = (int) Math.random();
            long occurred = new Date().getTime()/1000;
            String jsonInputString = "[{" + urlFixedParameters + ", \"id\": " + id + ",\"occurred\": "
                    + occurred + "}]";
            byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
            outputStream.write(input, 0, input.length);
            outputStream.flush();
            outputStream.close();

            int responseCode = connection.getResponseCode();
            if (responseCode - 200 >= 100){
                logger.error("Failed to send the alert to emergency system. Response message: {}",
                        connection.getResponseMessage());
            } else {
                logger.info("Alert successfully sent to emergency server. Response message: {}",
                        connection.getResponseMessage());
                alertSent = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void onClose(){
        logger.info("Closing connection with emergency system");
        connection.disconnect();
    }
}
