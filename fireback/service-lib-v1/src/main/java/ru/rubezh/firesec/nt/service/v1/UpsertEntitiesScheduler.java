package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import ru.rubezh.firesec.nt.dao.v1.ActiveProjectRepository;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.EntityObserver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Планировщик по итерационному выполнению указанных ему в конфигурации upsert-процессоров.
 * 
 * @author Антон Васильев
 *
 */
public class UpsertEntitiesScheduler {

    @Autowired
    private Logger logger;

    @Autowired
    private EntityObserveMutexService entityObserveMutexService;

    @Autowired
    private EntityObserverService entityObserverService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ActiveProjectRepository activeProjectRepository;

    /** Интервал "тиков" в мс */
    private final static int TICKER_INTERVAL = 10;
    /** Максимальное время захвата мютекса в мс */
    private final static int MAX_MUTEX_LOCK_TIME = 60000;

    /* Конфигурация */

    public static class UpsertEntitiesProcessorItem {
        private String observerId;

        private UpsertEntitiesProcessor upsertEntitiesProcessor;

        public String getObserverId() {
            return observerId;
        }

        public void setObserverId(String observerId) {
            this.observerId = observerId;
        }

        public UpsertEntitiesProcessor getUpsertEntitiesProcessor() {
            return upsertEntitiesProcessor;
        }

        public void setUpsertEntitiesProcessor(UpsertEntitiesProcessor upsertEntitiesProcessor) {
            this.upsertEntitiesProcessor = upsertEntitiesProcessor;
        }

    }

    /** Список upsert-процессоров и соответствующих им идентификаторов наблюдателей */
    private List<UpsertEntitiesProcessorItem> upsertEntitiesProcessors = new ArrayList<>();

    /** Время между выполнением каждого отдельного upsert-процессора в одной итерации */
    private int upsertProcessInterval = 100;

    /** Время между итерациями */
    private int iterationInterval = 500;

    /** Идентификатор мютекса, который шедулер должен захватить перед началом итерации */
    private String entityObserveMutexId = "EOM@";

    public List<UpsertEntitiesProcessorItem> getUpsertEntitiesProcessors() {
        return upsertEntitiesProcessors;
    }

    public void setUpsertEntitiesProcessors(List<UpsertEntitiesProcessorItem> upsertEntitiesProcessors) {
        this.upsertEntitiesProcessors = upsertEntitiesProcessors;
    }

    public int getUpsertProcessInterval() {
        return upsertProcessInterval;
    }

    public void setUpsertProcessInterval(int upsertProcessInterval) {
        this.upsertProcessInterval = upsertProcessInterval;
    }

    public int getIterationInterval() {
        return iterationInterval;
    }

    public void setIterationInterval(int iterationInterval) {
        this.iterationInterval = iterationInterval;
    }

    public String getEntityObserveMutexId() {
        return entityObserveMutexId;
    }

    public void setEntityObserveMutexId(String entityObserveMutexId) {
        this.entityObserveMutexId = entityObserveMutexId;
    }

    /* Состояние */

    private int lastUpsertTick = 0;

    private volatile int currentTick = 0;

    private Date iterationStartedTime;

    private Integer upsertEntitiesProcessorNo;

    @Scheduled(fixedRate = 10)
    public void ticker() {
        currentTick += TICKER_INTERVAL;
    }

    public void process() {
        if (!upsertEntitiesProcessors.isEmpty()) {
            Date currentTime = new Date();
            int currentTick = this.currentTick;
            if (upsertEntitiesProcessorNo == null && lastUpsertTick + iterationInterval <= currentTick) {
                if (entityObserveMutexService.tryLock(entityObserveMutexId, currentTime, MAX_MUTEX_LOCK_TIME)) {
                    upsertEntitiesProcessorNo = 0;
                    iterationStartedTime = currentTime;
                }
            }
            if (upsertEntitiesProcessorNo != null && lastUpsertTick + upsertProcessInterval <= currentTick) {
                lastUpsertTick = currentTick;
                ActiveProject lastActiveProject = projectService.getLastActive();
                if (lastActiveProject != null) {
                    String observerId = upsertEntitiesProcessors.get(upsertEntitiesProcessorNo).observerId;
                    UpsertEntitiesProcessor processor = upsertEntitiesProcessors
                            .get(upsertEntitiesProcessorNo).upsertEntitiesProcessor;
                    try {
                        EntityObserver observer = entityObserverService.getObserver(observerId,
                                lastActiveProject.getStateGetDateTime());

                        logger.debug("Process upsert. Observer id: {}, Observer oid: {}, iteration timestamp: {}",
                                observer.getId(), observer.getLastObservedObjectId(), iterationStartedTime.getTime());
                        long timeBefore = System.nanoTime();
                        ObjectId updatedLastObservedOid = processor.processUpsert(lastActiveProject,
                                observer.getLastObservedObjectId(), new Date(iterationStartedTime.getTime()));
                        long timeAfter = System.nanoTime();
                        if (timeBefore + ((long) upsertProcessInterval) * 1000000 < timeAfter) {
                            logger.warn(
                                    "Upsert process exceded it's scheduled interval. Observer id: {}, Observer oid: {}, iteration timestamp: {}, upsert time in ms: {}",
                                    observer.getId(), observer.getLastObservedObjectId(),
                                    iterationStartedTime.getTime(), (timeAfter - timeBefore) / 1000000);
                        }
                        logger.debug("Upsert done");

                        entityObserverService.upsertObserver(observer, updatedLastObservedOid);
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
                if (++upsertEntitiesProcessorNo >= upsertEntitiesProcessors.size() || lastActiveProject == null) {
                    upsertEntitiesProcessorNo = null;
                    entityObserveMutexService.unlock(entityObserveMutexId);
                }
            }
        }
    }

}
