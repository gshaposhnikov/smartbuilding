package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.amqp.message.alert.ActiveProjectValidateStatus;
import ru.rubezh.firesec.nt.amqp.message.request.SetActiveProjectRequest;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.Project;

public interface ProjectService {

    public Project add(Project project, StringBuilder errorMessage);

    public Project update(String projectId, Project project, StringBuilder errorMessage);

    public boolean remove(String projectId, StringBuilder errorMessage);

    public Project getActive();
    
    /**
     * Получить последний активный проект (оперативную сущность).
     * 
     * @return Если есть - то текущий активный проект, иначе тот, который был последним активным.
     *         Если активного проекта никогда не было, то null.  
     */
    public ActiveProject getLastActive();

    /**
     * Перевести проект в состояние "активный"
     * 
     * Переводит проект в состояние активный, создает оперативные сущности,
     * переводит, если был, прежний активный проект в состояние "в разработке" и
     * отправляет оповещения для бизнес-логики
     * 
     * @param projectId
     *            идентификатор проекта
     * @param errorMessage
     *            возвращаемое сообщение в случае ошибки
     * @return true в случае успеха, иначе false
     */
    public boolean setActive(String projectId, StringBuilder errorMessage);

    /**
     * Перевести проект в состояние "в разработке"
     * 
     * Переводит проект в состояние "в разработке", удаляет все оперативные
     * сущности и отправляет оповещение для бизнес-логики
     * 
     * @param projectId
     *            идентификатор проекта
     * @param errorMessage
     *            возвращаемое сообщение в случае ошибки
     * @return true в случае успеха, иначе false
     */
    public boolean setBuild(String projectId, StringBuilder errorMessage);

    // TODO: Реализовать эту проверку через аспект
    public boolean checkProjectIsEditable(String projectId, StringBuilder errorMessage);

    public boolean isProjectActive(String projectId);
    public boolean checkProjectIsActive(String projectId, StringBuilder errorMessage);

    public SetActiveProjectRequest getSetActiveProjectRequest(String projectId);

    /**
     * Обновить оперативный статус проекта по оповещению с результатами
     * валидации от нижнего уровня.
     * 
     * @param activeProjectValidateStatus
     *            результаты валидации проекта
     */
    public void updateStatus(ActiveProjectValidateStatus activeProjectValidateStatus);

    /** Интерфейс подписчика на изменения статуса проекта */
    public interface ProjectStatusChangedHandler {
        public void onProjectStatusChanged(String projectId, boolean projectNowActive);
    }

    /**
     * Подписаться на уведомления об изменении статуса проекта
     * 
     * @param handler обработичик, вызываемый при изменении статуса проекта
     * @return идентификатор текущего активного проекта или null, если нет активного
     */
    public String subscribe(ProjectStatusChangedHandler handler);

    /**
     * Отписаться от уведомлений об изменении статуса проекта
     * 
     * @param handler ранее подписанный обработичик
     */
    public void unsubsribe(ProjectStatusChangedHandler handler);

}
