package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.EntityObserveMutexRepository;
import ru.rubezh.firesec.nt.domain.v1.EntityObserveMutex;

@Service
public class EntityObserveMutexService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private EntityObserveMutexRepository entityObserveMutexRepository;

    /**
     * Попытаться захватить мютекс.
     * 
     * @param mutexId идентификатор мютекса
     * @param currentTime текущее время
     * @param lockTimeOutMs максимальное время, в течении которого должна действовать блокировка
     * @return true, если захватили, иначе - false
     */
    boolean tryLock(String mutexId, Date currentTime, int lockTimeOutMs) {
        boolean locked = false;

        /* Удалим просрочивыший (зависший) мютекс */
        entityObserveMutexRepository.deleteByIdAndLockedIsTrueAndLockExpireBefore(mutexId, currentTime);

        Query query = Query.query(Criteria.where("_id").is(mutexId).and("locked").is(false));
        Update update = new Update();
        update.set("locked", true);
        update.set("lockExpire", new Date(currentTime.getTime() + lockTimeOutMs));

        try {
            mongoTemplate.upsert(query, update, EntityObserveMutex.class);
            locked = true;
        } catch (DuplicateKeyException e) {
            /* мютекс занят */
        }

        return locked;
    }

    /**
     * Освободить мютекс.
     * 
     * @param mutexId идентификатор мютекса.
     */
    void unlock(String mutexId) {
        EntityObserveMutex mutex = new EntityObserveMutex();
        mutex.setId(mutexId);
        mutex.setLocked(false);
        entityObserveMutexRepository.save(mutex);
    }

}
