package ru.rubezh.firesec.nt.service.v1;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.EntityObserverRepository;
import ru.rubezh.firesec.nt.domain.v1.EntityObserver;

import java.util.Date;

@Service
public class EntityObserverService {

    @Autowired
    private EntityObserverRepository entityObserverRepository;

    /**
     * Получить объект наблюдателя.
     * 
     * @param observerId идентификатор наблюдателя
     * @param initialObserveTime текущее время (для инициализации нового наблюдателя)
     * @return объект наблюдателя.
     */
    public EntityObserver getObserver(String observerId, Date initialObserveTime) {
        EntityObserver entityObserver = entityObserverRepository.findOne(observerId);
        if (entityObserver == null) {
            entityObserver = new EntityObserver();
            entityObserver.setId(observerId);
            entityObserver.setLastObservedObjectId(new ObjectId(initialObserveTime, 0));
            entityObserver = entityObserverRepository.insert(entityObserver);
        }
        return entityObserver;
    }

    /**
     * Сохранить новый или измененный объект наблюдателя.
     * 
     * @param entityObserver наблюдатель
     * @param lastObservedOid oid последнего наблюдения
     */
    public void upsertObserver(EntityObserver entityObserver, ObjectId lastObservedOid) {
        if (!entityObserver.getLastObservedObjectId().equals(lastObservedOid)) {
            entityObserver.setLastObservedObjectId(lastObservedOid);
            entityObserverRepository.save(entityObserver);
        }
    }

}
