package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.LogMonitorableValue;

/**
 * Интерфейс сервисных функций для работы с историей набл. параметров.
 * @author Александр Горячкин
 */
public interface LogMonitorableValueService {

    /**
     * Сохранение новых значений набл. параметров.
     * @param device сущность активного устройства
     * @param monitorableValues набор значений для сохранения
     * @param currentTime время создания записей
     * @param force принудительное сохранение (без проверки на необходимость)
     */
    void saveAll(ActiveDevice device, Map<String, String> monitorableValues, Date currentTime, boolean force);

    /**
     * Получить список набл. параметров за промежуток времени по устройству.
     * @param projectId идентификатор проекта
     * @param deviceId идентификатор устройства
     * @param from начало промежутка времени
     * @param to конец промежутка времени
     * @param pageable параметры сортировки
     * @return Набор списков <профиль_параметра, список_значений_параметров>
     */
    Page<LogMonitorableValue> getByProjectIdAndDeviceAndReceivedRange(
            String projectId, String deviceId, Date from, Date to, Pageable pageable);

    /**
     * Удалить историю набл. параметров по всем устройствам,
     * позже указанного времени.
     * @param received время для удаления
     */
    void removeByReceivedBefore(Date received);

}
