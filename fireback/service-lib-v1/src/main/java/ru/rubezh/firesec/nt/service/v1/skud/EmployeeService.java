package ru.rubezh.firesec.nt.service.v1.skud;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.AccessKeyRepository;
import ru.rubezh.firesec.nt.dao.v1.EmployeeRepository;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@Service
public class EmployeeService extends SkudEntityService<Employee, EmployeeRepository> {

    @Autowired
    private AccessKeyRepository accessKeyRepository;

    @Autowired
    private SkudService skudService;

    @Override
    public boolean add(String projectId, Employee employee, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        if (!checkEmployee(projectId, employee, errorMessage)){
            return false;
        }

        employee.setProjectId(projectId);
        employee.setName(calculateShortName(employee));

        /* Вычисление числового идентификатора */
        synchronized (this) {
            /*
             * Блок синхронизации - чтобы не было возможности создать нескольких сотрудников
             * с одним индексом
             */
            /*
             * При поиске свободного индекса не должен учитываться флаг removed,
             * т.к. при автообновлении БД СКУД сущности, отмеченные на удаление,
             * будут фактически удалены после применения изменения к БД
             * приборов. И чтобы не возникло конфликта в БД приборов, когда
             * добавляется сущность с индексом, а другая сущность с тем же
             * индексом еще не удалена, занимать индексы до фактического
             * удаления нельзя.
             */
            Employee topEmployee = entityRepository.findTopByProjectIdOrderByIndexDesc(projectId);
            if (topEmployee != null) {
                employee.setIndex(topEmployee.getIndex() + 1);
            } else {
                employee.setIndex(0x0001);
            }
            if (employee.getIndex() > 0xFFFF) {
                /* TODO: искать пустые идентификаторы в середине числового множества */
                errorMessage.append("Превышено максимальное значение счетчика учетных записей сотрудников");
                return false;
            }

            entityRepository.insert(employee);
            return true;
        }
    }

    private boolean checkEmployee(String projectId, Employee employee, StringBuilder errorMessage){
        /* Проверка имени */
        if (!checkName(employee, errorMessage)) {
            return false;
        }
        /* Проверка карты доступа */
        if (!skudService.checkAccessMapReferences(projectId, employee.getAccessMap(), errorMessage)) {
            return false;
        }
        return true;
    }

    /* Вычисление имени с инициалами размером не более 20 символов */
    private String calculateShortName(Employee employee) {
        StringBuilder name = new StringBuilder(employee.getLastName().length() > 15
                        ? employee.getLastName().substring(0, 15)
                        : employee.getLastName());
        name.append(' ');
        name.append(employee.getFirstName().charAt(0));
        name.append('.');
        name.append(employee.getMiddleName().charAt(0));
        name.append('.');
        return name.toString();
    }

    private boolean checkName(Employee employee, StringBuilder errorMessage) {
        if (employee.getFirstName() == null || employee.getFirstName().isEmpty()) {
            errorMessage.append("Не задано имя сотрудника");
            return false;
        }
        if (employee.getMiddleName() == null || employee.getMiddleName().isEmpty()) {
            errorMessage.append("Не задано отчество сотрудника");
            return false;
        }

        if (employee.getLastName() == null || employee.getLastName().isEmpty()) {
            errorMessage.append("Не задана фамилия сотрудника");
            return false;
        }

        return true;
    }

    @Override
    public boolean update(String projectId, String employeeId, Employee employee, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        Employee dstEmployee = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, employeeId);
        if (dstEmployee == null) {
            errorMessage.append("Учетная запись сотрудника не найдена");
            return false;
        }

        /* Проверка имени */
        if (!checkName(employee, errorMessage)) {
            return false;
        }

        /* Проверка карты доступа */
        if (!skudService.checkAccessMapReferences(projectId, employee.getAccessMap(), errorMessage)) {
            return false;
        }

        employee.setName(calculateShortName(employee));

        dstEmployee.update(employee);
        entityRepository.save(dstEmployee);

        return true;
    }

    @Override
    public boolean remove(String projectId, String employeeId, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        Employee employee = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, employeeId);
        if (employee == null) {
            errorMessage.append("Учетная запись сотрудника не найдена");
            return false;
        }

        employee.setRemoved(true);
        entityRepository.save(employee);

        /* Отмечаем на удаление все ключи сотрудника */
        List<AccessKey> accessKeys = accessKeyRepository.findByProjectIdAndEmployeeIdAndRemovedIsFalse(projectId,
                employeeId);
        if (!accessKeys.isEmpty()) {
            for (AccessKey accessKey : accessKeys) {
                accessKey.setRemoved(true);
            }
            accessKeyRepository.save(accessKeys);
        }

        return true;
    }

    @Override
    public String getKeyForUpdatedEntities() {
        return "employees";
    }

    public boolean checkAndAdditionEmployeeToImportProject(String projectId,
                                                               List<Employee> employees,
                                                               Map<String, String> oldAndNewEmployeesId,
                                                               Map<String, String> oldAndNewDevicesId,
                                                               Map<String, String> oldAndNewWorkSchedulesId,
                                                               Map<String, String> oldAndNewRegionsId,
                                                               StringBuilder errorMessage){
        List<String> oldEmployeeIds = new ArrayList<>();
        List<Integer> employeeIndexes = new ArrayList<>();
        for (Employee employee: employees) {

            List<String> newAccessPointDeviceIds = new ArrayList<>();
            for (String accessPointDeviceId : employee.getAccessMap().getAccessPointDeviceIds()){
                String newAccessPointDeviceId = oldAndNewDevicesId.get(accessPointDeviceId);
                if (newAccessPointDeviceId == null){
                    errorMessage.append("У сотрудника ").append(employee.getName()).append(" указана несуществующая точка доступа");
                    return false;
                }
                newAccessPointDeviceIds.add(newAccessPointDeviceId);

            }
            employee.getAccessMap().setAccessPointDeviceIds(newAccessPointDeviceIds);

            if (employee.getAccessMap().getWorkScheduleId() != null &&
                    !oldAndNewWorkSchedulesId.keySet().contains(employee.getAccessMap().getWorkScheduleId())){
                errorMessage.append("У сотрудника ").append(employee.getName()).append(" указан несуществующий график работ");
                return false;
            }
            employee.getAccessMap().setWorkScheduleId(oldAndNewWorkSchedulesId.get(employee.getAccessMap().getWorkScheduleId()));

            List<String> newWorkRegionIds = new ArrayList<>();
            for (String workRegionId : employee.getAccessMap().getWorkRegionIds()){
                String newWorkRegionId = oldAndNewRegionsId.get(workRegionId);
                if (newWorkRegionId == null){
                    errorMessage.append("У сотрудника ").append(employee.getName()).append(" указана несуществующая рабочая зона");
                    return false;
                }
                newWorkRegionIds.add(newWorkRegionId);
            }
            employee.getAccessMap().setWorkRegionIds(newWorkRegionIds);

            if (!checkEmployee(projectId, employee, errorMessage)){
                return false;
            }

            if (employee.getIndex() > MAX_INDEX_VALUE) {
                errorMessage.append("Превышено максимальное значение счетчика учетных записей сотрудников");
                return false;
            }

            if (employeeIndexes.contains(employee.getIndex())){
                errorMessage.append("У сотрудника ").append(employee.getName()).append(" неуникальный индекс");
                return false;
            }
            employeeIndexes.add(employee.getIndex());

            oldEmployeeIds.add(employee.getId());
            employee.clearId();
            employee.setProjectId(projectId);

        }
        Iterator<String> itOnOld = oldEmployeeIds.iterator();
        Iterator<String> itOnNew = entityRepository.insert(employees).stream().map(Employee::getId).iterator();
        while (itOnOld.hasNext() && itOnNew.hasNext()){
            oldAndNewEmployeesId.put(itOnOld.next(), itOnNew.next());
        }
        return true;
    }
}
