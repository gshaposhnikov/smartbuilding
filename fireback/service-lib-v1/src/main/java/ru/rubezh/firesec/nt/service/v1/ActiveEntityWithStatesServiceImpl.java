package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import ru.rubezh.firesec.nt.dao.v1.StateCategoryRepository;
import ru.rubezh.firesec.nt.dao.v1.StateRepository;
import ru.rubezh.firesec.nt.domain.v1.BasicActiveEntityWithStates;
import ru.rubezh.firesec.nt.domain.v1.State;
import ru.rubezh.firesec.nt.domain.v1.StateCategory;

import java.util.*;

public abstract class ActiveEntityWithStatesServiceImpl implements ActiveEntityWithStatesService {

    @Autowired
    StateCategoryRepository stateCategoryRepository;

    @Autowired
    StateRepository stateRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void addActiveStates(BasicActiveEntityWithStates entity, Collection<String> activeStateIds, boolean commit) {
        /* добавляем активные состояния и взводим флаг, если что-то новое добавилось */
        if (entity.getActiveStates().addAll(activeStateIds))
            entity.setNeedsStateCommit(true);

        /* если надо - фиксируем изменения */
        if (commit)
            commitAfterStateUpdated(entity, false);
    }

    @Override
    public void removeActiveStates(BasicActiveEntityWithStates entity, Collection<String> activeStateIds, boolean commit) {
        /* удаляем активные состояния и взводим флаг, если что-то удалилось */
        if (entity.getActiveStates().removeAll(activeStateIds))
            entity.setNeedsStateCommit(true);

        /* если надо - фиксируем изменения */
        if (commit)
            commitAfterStateUpdated(entity, false);
    }

    @Override
    public void setInheritedActiveStates(BasicActiveEntityWithStates entity, String parentEntityId,
            Set<String> inheritedStateIds, boolean commit) {
        Set<String> oldInheritedStateIds = entity.getInheritedActiveStates().get(parentEntityId);
        if ((oldInheritedStateIds == null && !inheritedStateIds.isEmpty())
                || (oldInheritedStateIds != null && !oldInheritedStateIds.equals(inheritedStateIds))) {
            entity.getInheritedActiveStates().put(parentEntityId, inheritedStateIds);

            entity.setNeedsStateCommit(true);

            /* если надо - фиксируем изменения */
            if (commit)
                commitAfterStateUpdated(entity, false);
        }

    }

    public static StateCategory calculateGeneralStateCategory(List<State> states, StateCategory defaultCategory) {
        StateCategory generalStateCategory = defaultCategory;
        for (State state : states) {
            if (state.getStateCategory().getPriority() < generalStateCategory.getPriority())
                generalStateCategory = state.getStateCategory();
        }
        return generalStateCategory;
    }

    @Override
    public void prepareCommitAfterStateUpdated(BasicActiveEntityWithStates entity, StateCategory defaultStateCategory,
            Date currentTime, Map<String, State> statesHash) {
        entity.setStateGetDateTime(currentTime);
        updateStateCategory(entity, defaultStateCategory, statesHash);
    }

    @Override
    public void commitAfterStateUpdated(BasicActiveEntityWithStates entity, boolean forced) {
        if (forced || entity.isNeedsStateCommit()) {
            prepareCommitAfterStateUpdated(entity, getDefaultStateCategory(), new Date(), null);
            mongoTemplate.save(entity);
            entity.setNeedsStateCommit(false);
        }
    }

    /**
     * Определить класс состояния по умолчанию
     * 
     * @return класс состояния по умолчанию
     */
    @Override
    public StateCategory getDefaultStateCategory() {
        StateCategory defaultCategory = stateCategoryRepository.findOneByDefaultCategoryIsTrue();
        if (defaultCategory == null)
            defaultCategory = new StateCategory();
        return defaultCategory;
    }

    /**
     * Обновить приоритетную категорию состояния для наблюдаемой сущности, используя хеш состояний.
     * 
     * @param entity наблюдаемая сущность
     * @param defaultCategory категория состояний по умолчанию
     * @param statesHash хеш состояний
     * @return список состояний наблюдаемой сущности
     */
    @Override
    public void updateStateCategory(BasicActiveEntityWithStates entity, StateCategory defaultCategory,
            Map<String, State> statesHash) {

        Set<String> stateIds = new HashSet<>();
        stateIds.addAll(entity.getActiveStates());
        for (Set<String> inheritedStateIds : entity.getInheritedActiveStates().values()) {
            stateIds.addAll(inheritedStateIds);
        }

        List<State> states = new ArrayList<>();

        /* Ищем сначала состояния в хеше */
        if (statesHash != null) {
            for (Iterator<String> it = stateIds.iterator(); it.hasNext();) {
                String stateId = it.next();
                State state = statesHash.get(stateId);
                if (state != null) {
                    states.add(state);
                    it.remove();
                }
            }
        }

        /* Получаем состояния устройства, которых не было в хеше */
        if (!stateIds.isEmpty()) {
            List<State> nonHashedStates = stateRepository.findByIdIn(stateIds);
            if (statesHash != null) {
                for (State state : nonHashedStates) {
                    statesHash.put(state.getId(), state);
                }
            }
            states.addAll(nonHashedStates);
        }

        /* Выбираем и запоминаем самый приоритетный класс состояния */
        entity.setGeneralStateCategoryId(calculateGeneralStateCategory(states, defaultCategory).getId());
    }

}
