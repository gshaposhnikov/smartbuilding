package ru.rubezh.firesec.nt.service.v1;

/**
 * Интерфейс сервисов, чьи обьекты могут быть отфильтрованы при активации проекта
 *
 * @author Anton Karavaev
 */
public interface ActivationFilterableService {

    /** Очистить метки */
    default void clearTags(String projectId){}

    /** Установить метки фильтрации по подсистемам */
    void addSubsystemTags(String projectId);
}
