package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile.InternalDeviceItem;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.TracingEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;
import ru.rubezh.firesec.nt.domain.v1.representation.DeviceCreateView;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey.KeyType;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DeviceService {

    /** Минимальный возможный номер линии (при общей адресации) */
    public static final int DEVICE_MIN_LINE_NO = 1;

    @Autowired
    Logger logger;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;

    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccessKeyRepository accessKeyRepository;

    /**
     * Проверить совместимость подключения
     *
     * @param deviceProfile Профиль добавляемого устройства
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляются устройства
     * @param errorMessage Сообщение, заполняемое в случае ошибки
     * @return true если устройства совместимы
     */
    private boolean checkСompatibility(DeviceProfile deviceProfile, DeviceProfile parentDeviceProfile,
            StringBuilder errorMessage) {
        if (parentDeviceProfile == null){
            if (deviceProfile.getDeviceCategory() == DeviceCategory.TRANSPORT
                    || deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL){
                return true;
            } else {
                errorMessage.append("Для устройства ").append(deviceProfile.getId())
                        .append(" некорректно указано родительское устройство.");
                return false;
            }
        }
        if (parentDeviceProfile.getOutputDeviceProfileIds().contains(deviceProfile.getId())
                || parentDeviceProfile.getInternalDevices().stream()
                    .flatMap(intDevs -> intDevs.getAvailableDeviceProfileIds().stream()).collect(Collectors.toSet())
                    .contains(deviceProfile.getId())) {
            return true;
        } else {
            errorMessage.append("Эти устройства не могут быть подключены друг к другу: ")
                    .append(deviceProfile.getId()).append(", ").append(parentDeviceProfile.getId());
            return false;
        }
    }

    /**
     * Проверить возможность подключать устройство по адресу
     *
     * @param deviceInfo Информация об устройстве (не полная - только то, что указал клиент)
     * @param deviceSize Размер одного устройства
     * @param projectId Идентификатор проекта
     * @param parentDevice
     *            Устройство, в состав которого добавляются встроенные устройства
     *            или null при подключении к корню
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляются встроенные устройства
     * @param errorMessage Сообщение, заполняемое в случае ошибки
     * @return Возможность подключения
     */
    private boolean checkDeviceAddressValid(DeviceCreateView deviceInfo, int deviceSize, String projectId,
            Device parentDevice, DeviceProfile parentDeviceProfile, StringBuilder errorMessage) {
        if (parentDevice != null) {
            return checkRS485Addressing(deviceInfo, parentDevice, parentDeviceProfile, deviceSize, errorMessage);
        } else {
            return checkRootDeviceAddressing(projectId, deviceInfo.getLineAddress(), errorMessage);
        }
    }

    private boolean checkRS485Addressing(DeviceCreateView deviceInfo, Device parentDevice,
            DeviceProfile parentDeviceProfile, int deviceSize, StringBuilder errorMessage) {
        boolean deviceValid = checkRS485AddressingValues(deviceInfo.getLineNo(), deviceInfo.getLineAddress(),
                parentDeviceProfile, errorMessage);

        // проверка количества свободных адресов на линии
        if (deviceValid) {
            // выделяем все устройства, подключенные к той же линии того же родительского устройства, и вложенные в них
            String regexp = "^" + parentDevice.getAddressPath() + "," + deviceInfo.getLineNo() + ",";
            // для транспорта ограничиваем глубину вложенности адресов (внуки не занимают адрес на линии транспорта)
            if (parentDevice.getDeviceCategory() == DeviceCategory.TRANSPORT)
                regexp += "\\d+$";
            // все устройства проекта, кроме контейнеров (делят адрес с первым устройством в своём составе)
            int nAddressesTaken = deviceRepository.countByProjectIdAndAddressPathRegexAndDeviceCategoryNot(
                    parentDevice.getProjectId(), regexp, DeviceCategory.CONTAINER);
            if ((deviceSize * deviceInfo.getCount() + nAddressesTaken) > parentDeviceProfile.getLineDeviceCount()) {
                errorMessage.append("Нельзя подключить больше ").append(parentDeviceProfile.getLineDeviceCount())
                        .append(" устройств");
                deviceValid = false;
            }
        }

        if (deviceValid && deviceSize > 0) {
            deviceValid = false;
            for (AddressRange addressRange : parentDevice.getLineAddressRanges()
                    .get(deviceInfo.getLineNo())) {
                if (deviceInfo.getLineAddress() >= addressRange.getFirstAddress()
                        && (deviceInfo.getLineAddress() + deviceSize - 1) <
                        (addressRange.getFirstAddress() + addressRange.getAddressCount())) {
                    deviceValid = true;
                    break;
                }
            }
            if (!deviceValid) {
                errorMessage.append("Выбранный адрес или диапазон адресов не может быть назначен");
            }
        }

        return deviceValid;
    }

    private boolean checkRS485AddressingValues(int lineNo, int lineAddress, DeviceProfile parentDeviceProfile,
            StringBuilder errorMessage) {
        boolean deviceValid = true;
        if (deviceValid && lineNo <= 0) {
            errorMessage.append("Номер линии должен быть больше нуля");
            deviceValid = false;
        }

        if (deviceValid && (lineAddress < Device.MIN_LINE_ADDRESS || lineAddress > Device.MAX_LINE_ADDRESS)) {
            errorMessage.append("Адрес на линии должен быть не меньше ").append(Device.MIN_LINE_ADDRESS)
                    .append(" и не больше ").append(Device.MAX_LINE_ADDRESS);
            deviceValid = false;
        }
        if (deviceValid && parentDeviceProfile.getLineCount() < lineNo) {
            errorMessage.append("Эта линия не поддерживается");
            deviceValid = false;
        }
        return deviceValid;
    }

    private boolean checkRootDeviceAddressing(String projectId, int address, StringBuilder errorMessage) {
        boolean deviceValid = true;

        if (address < Device.MIN_LINE_ADDRESS || address > Device.MAX_LINE_ADDRESS) {
            errorMessage.append("Адрес на линии должен быть не меньше ").append(Device.MIN_LINE_ADDRESS)
                    .append(" и не больше ").append(Device.MAX_LINE_ADDRESS);
            deviceValid = false;
        }

        if (deviceValid
                && deviceRepository.countByProjectIdAndAddressPath(projectId, "," + Integer.toString(address)) > 0) {
            errorMessage.append("Адрес уже используется");
            deviceValid = false;
        }

        return deviceValid;
    }

    /**
     * Разместить устройства на свободных адресах
     *
     * @param deviceInfo Информация о линии, начальном адресе и количестве добавляемых устройств
     * @param deviceSize Количество адресов, занимаемых устройством на линии
     * @param freeAddressRanges Список свободных участков адресов
     * @return Адреса устройств на линиях или null
     */
    private Map<Integer, List<Integer>> allocateDevices(DeviceCreateView deviceInfo, int deviceSize,
            List<List<AddressRange>> freeAddressRanges) {
        Map<Integer, List<Integer>> devicesAllocation = new HashMap<>();
        int devicesToAllocate = deviceInfo.getCount();
        if (devicesToAllocate <= 0)
            return null;
        int addressOffset;
        int addressCount;
        int lineNo = 0;
        for (List<AddressRange> lineAddressRanges : freeAddressRanges) {
            if (lineNo >= deviceInfo.getLineNo()) {
                for (AddressRange addressRange : lineAddressRanges) {
                    if (lineNo == deviceInfo.getLineNo() &&
                            devicesToAllocate == deviceInfo.getCount()) {
                        if (deviceInfo.getLineAddress() < addressRange.getFirstAddress())
                            continue;
                        addressOffset = deviceInfo.getLineAddress() - addressRange.getFirstAddress();
                        addressCount = addressRange.getAddressCount() - addressOffset;
                    } else {
                        addressOffset = 0;
                        addressCount = addressRange.getAddressCount();
                    }
                    List<Integer> lineAllocation = devicesAllocation.get(lineNo);
                    while (addressCount >= deviceSize) {
                        if (lineAllocation == null) {
                            lineAllocation = new ArrayList<>();
                            devicesAllocation.put(lineNo, lineAllocation);
                        }
                        lineAllocation.add(addressRange.getFirstAddress() + addressOffset);
                        devicesToAllocate--;
                        if (devicesToAllocate == 0)
                            return devicesAllocation;
                        addressCount -= deviceSize;
                        addressOffset += deviceSize;
                    }
                }
            }
            lineNo++;
        }
        return null;
    }

    /**
     * Установить конфигурационные параметры устройства по умолчанию
     *
     * @param device Объект устройства
     * @param deviceProfile Профиль устройства
     */
    private void setDefaultProjectConfig(Device device, DeviceProfile deviceProfile) {
        if (deviceProfile.getConfigProperties() != null) {
            Map<String, String> defaultPropertyValues = new HashMap<>();
            for (String propertyId : deviceProfile.getConfigProperties().keySet()) {
                DeviceConfigProperty property = deviceProfile.getConfigProperties().get(propertyId);
                defaultPropertyValues.put(propertyId, property.getDefaultValue());
            }
            device.setProjectConfigValues(defaultPropertyValues);
        }
    }

    /**
     * Установить параметры устройства по умолчанию
     *
     * @param device Объект устройства
     * @param deviceProfile Профиль устройства
     */
    private void setDefaultProjectProperties(Device device, DeviceProfile deviceProfile) {
        Map<String, DeviceProperty> propertiesMap = deviceProfile.getDeviceProperties();
        if ((propertiesMap != null) && !propertiesMap.isEmpty()) {
            Map<String, String> defaultPropertyValues = new HashMap<>();
            for (String propertyId : propertiesMap.keySet()) {
                DeviceProperty property = propertiesMap.get(propertyId);
                defaultPropertyValues.put(propertyId, property.getDefaultValue());
            }
            device.setDevicePropertyValues(defaultPropertyValues);
        }
    }

    /**
     * Создать встроенные устройства
     *
     * @param parentDevice Устройство, в состав которого добавляются встроенные устройства
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляются встроенные устройства
     * @return Список созданных устройств
     */
    private List<Device> createInternalDevices(Device parentDevice, DeviceProfile parentDeviceProfile) {
        List<Device> internalDevices = new ArrayList<>();
        int internalDeviceNo = 0;
        for (InternalDeviceItem internalDeviceItem : parentDeviceProfile.getInternalDevices()) {
            if (!internalDeviceItem.getAvailableDeviceProfileIds().isEmpty()) {
                DeviceProfile internalDeviceProfile = deviceProfileRepository
                        .findOne(internalDeviceItem.getAvailableDeviceProfileIds().get(0));
                if (internalDeviceProfile == null) {
                    logger.error("Device profile {} not found",
                            internalDeviceItem.getAvailableDeviceProfileIds().get(0));
                    continue;
                }
                for (int deviceIndex = 0; deviceIndex < internalDeviceItem.getnDevices(); ++deviceIndex) {
                    Device device = createInternalDevice(internalDeviceProfile, parentDevice, parentDeviceProfile,
                            internalDeviceNo++, deviceIndex < internalDeviceItem.getnMandatoryDevices());
                    device.setEmbedded(true);
                    device.getAcceptableDeviceProfileIds().addAll(internalDeviceItem.getAvailableDeviceProfileIds());
                    internalDevices.add(device);
                }
            }
        }
        return internalDevices;
    }

    /**
     * Создать динамически-добавляемое внутреннее устройство
     *
     * @param deviceProfile Профиль добавляемого устройства
     * @param parentDevice Устройство, в состав которого добавляется внутреннее устройство
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляется внутреннее устройство
     * @return Созданное устройство
     */
    private Device createAttachableInternalDevice(DeviceProfile deviceProfile,
            Device parentDevice, DeviceProfile parentDeviceProfile) {
        for (InternalDeviceItem internalDeviceItem : parentDeviceProfile.getInternalDevices()) {
            if (!internalDeviceItem.getAvailableDeviceProfileIds().contains(deviceProfile.getId()))
                continue;

            int nAttachedDevices = parentDevice.getnAttachedInternalDevices().getOrDefault(deviceProfile.getId(), 0);
            if (nAttachedDevices >= internalDeviceItem.getMaxAttachableDevices())
                continue;

            Device device =
                    createInternalDevice(deviceProfile, parentDevice, parentDeviceProfile, nAttachedDevices, false);
            device.getAcceptableDeviceProfileIds().addAll(internalDeviceItem.getAvailableDeviceProfileIds());
            parentDevice.getnAttachedInternalDevices().put(deviceProfile.getId(), nAttachedDevices + 1);
            return device;
        }
        return null;
    }

    /**
     * Создать встроенное устройство
     *
     * @param deviceProfile Профиль добавляемого устройства
     * @param parentDevice Устройство, в состав которого добавляется внутреннее устройство
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляется внутреннее устройство
     * @param internalDeviceIndex индекс внутреннего устройства
     * @param deviceIsMandatory признак неотключаемого устройства
     * @return Созданное устройство
     */
    private Device createInternalDevice(DeviceProfile deviceProfile,
            Device parentDevice, DeviceProfile parentDeviceProfile,
            int internalDeviceIndex, boolean deviceIsMandatory) {

        Device device = createDevice(deviceProfile, parentDevice);

        device.setProjectId(parentDevice.getProjectId());

        device.setEditableAddress(false);

        if (deviceProfile.getLineNumberSubstitution() != null &&
                deviceProfile.getLineAddressSubstitution() != null) {
            // встроенное устройство без адресации
            device.setLineNo(deviceProfile.getLineNumberSubstitution());
            device.setLineAddress(deviceProfile.getLineAddressSubstitution());
            device.setAddressType(AddressType.LACK);
        } else if (deviceProfile.getLineNumberSubstitution() != null) {
            // встроенное устройство с внутренней адресацией
            device.setLineNo(deviceProfile.getLineNumberSubstitution());
            device.setLineAddress(internalDeviceIndex + 1);
            device.setAddressType(AddressType.INTERNAL);
        } else {
            // встроенное устройство с внешней адресацией
            device.setLineNo(parentDevice.getLineNo());
            device.setLineAddress(internalDeviceIndex + parentDevice.getLineAddress());
        }

        /* Генерируем поисковый адресный путь устройства */
        calculateAddressPathAndLevel(device, parentDevice);

        // Возможность отключения внутренних устройств контейнера и прибора
        if (deviceProfile.getCanBeDisabled() &&
                (parentDeviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER ||
                parentDeviceProfile.getDeviceCategory() == DeviceCategory.CONTROL)) {
            device.setDisableNotAllowed(deviceIsMandatory);
            device.setDisabled(!deviceIsMandatory);
        }

        return device;
    }

    /**
     * Добавить устройства
     *
     * @param deviceInfo
     *            информация об устройстве (не полная - только то, что указал клиент)
     * @param projectId
     *            идентификатор проекта
     * @param modifiedDeviceIds
     *            идентификаторы устройств, возвращаемые в случае, если
     *            устройства были изменены
     * @param errorMessage
     *            сообщение, заполняемое в случае ошибки
     * @return список созданных устройств или null в случае ошибки
     */
    public List<Device> add(DeviceCreateView deviceInfo, String projectId,
                                       List<String> modifiedDeviceIds, StringBuilder errorMessage) {
        DeviceProfile deviceProfile = null;
        Device parentDevice = null;
        DeviceProfile parentDeviceProfile = null;

        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            /* Получим сущности, от которых зависит добавляемое устройство */
            deviceProfile = deviceProfileRepository.findOne(deviceInfo.getDeviceProfileId());
            if (deviceProfile == null) {
                errorMessage.append("Профиль устройства не найден");
                return null;
            }
            if (deviceInfo.getParentDeviceId() != null && !deviceInfo.getParentDeviceId().isEmpty()) {
                parentDevice = deviceRepository.findByProjectIdAndId(projectId, deviceInfo.getParentDeviceId());
                if (parentDevice == null) {
                    errorMessage.append("Не найдено родительское устройство (" + deviceInfo.getParentDeviceId() + ")");
                    return null;
                }
                parentDeviceProfile = deviceProfileRepository.findOne(parentDevice.getDeviceProfileId());
                if (parentDeviceProfile == null) {
                    errorMessage.append("Не найден профиль родительского устройства (" +
                            parentDevice.getDeviceProfileId() + ")");
                    return null;
                }
            }

            /* Подключение встроенных устройств */
            if (deviceProfile.getDeviceCategory() == DeviceCategory.VIRTUAL_CONTAINER) {
                List<Device> createdDevices = new ArrayList<>();
                int nAttachedDevices = 0;
                while (nAttachedDevices++ < deviceInfo.getCount()) {
                    Device device = createAttachableInternalDevice(deviceProfile, parentDevice, parentDeviceProfile);
                    if (device == null) {
                        errorMessage.append("Подключение устройства невозможно.");
                        return null;
                    }
                    createdDevices.add(device);
                }
                parentDevice = deviceRepository.save(parentDevice);
                createdDevices = deviceRepository.insert(createdDevices);
                modifiedDeviceIds.add(parentDevice.getId());
                return createdDevices;
            }

            /*
             * Вычислим кол-во адресов, которые будут заняты при добавлении данного
             * устройства с учетом встроенных устройств
             */
            int deviceSize = 1;
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTAINER) {
                deviceSize = 0;
            }
            for (InternalDeviceItem internalDeviceItem : deviceProfile.getInternalDevices()) {
                if (internalDeviceItem.getAddressType() == AddressType.GENERIC) {
                    deviceSize += internalDeviceItem.getnDevices();
                }
            }

            /* Проверим корректность параметров добавляемого устройства */
            if (checkСompatibility(deviceProfile, parentDeviceProfile, errorMessage) &&
                    checkDeviceAddressValid(deviceInfo, deviceSize, projectId,
                            parentDevice, parentDeviceProfile, errorMessage)) {
                /* Подсчёт диапазонов свободных адресов для корневого устройства */
                List<AddressRange> rootDevicesAddressRanges = new ArrayList<>();
                if (parentDevice == null) {
                    List<Device> rootDevices =
                            deviceRepository.findByProjectIdAndParentDeviceIdAndLineNoOrderByLineAddressAsc
                                    (projectId, null, 1);
                    boolean[] used = new boolean[Device.MAX_LINE_ADDRESS + 1];
                    Arrays.fill(used, false);
                    /* Отметим занятыми все адреса найденных потомков */
                    for (Device rootDevice : rootDevices) {
                        used[rootDevice.getLineAddress()] = true;
                    }
                    usedAddressMap2AddressRanges(used, rootDevicesAddressRanges);
                }

                /* Проверим возможность подключить необходимое кол-во устройств */
                List<List<AddressRange>> freeAddressRanges = null;
                if (parentDevice != null)
                    freeAddressRanges = parentDevice.getLineAddressRanges();
                else {
                    freeAddressRanges = new ArrayList<>();
                    freeAddressRanges.add(new ArrayList<>());
                    freeAddressRanges.add(rootDevicesAddressRanges);
                }
                Map<Integer, List<Integer>> devicesAllocation =
                        allocateDevices(deviceInfo, deviceSize, freeAddressRanges);
                if (devicesAllocation == null) {
                    errorMessage.append("Не хватает свободных участков адресов для размещения устройств.");
                    return null;
                }

                // создание и сохранение основных устройств
                List<Device> createdDevices = new ArrayList<>();
                int minLineNo = Integer.MAX_VALUE;
                int maxLineNo = Integer.MIN_VALUE;
                for (Integer lineNo : devicesAllocation.keySet()) {
                    List<Integer> lineAllocation = devicesAllocation.get(lineNo);
                    for (Integer lineAddress : lineAllocation)
                        createdDevices.add(createLineDevice(projectId, lineNo, lineAddress,
                                deviceProfile, parentDevice, parentDeviceProfile));
                    minLineNo = Integer.min(minLineNo, lineNo);
                    maxLineNo = Integer.max(maxLineNo, lineNo);
                }
                createdDevices = deviceRepository.insert(createdDevices);

                // создание и сохранение встроенных устройств
                List<Device> createdInternalDevices = new ArrayList<>();
                for (Device device : createdDevices)
                        createdInternalDevices.addAll(createInternalDevices(device, deviceProfile));
                createdInternalDevices = deviceRepository.insert(createdInternalDevices);

                if (parentDevice != null) {
                    /*
                     * Обновляем диапазоны свободных адресов у родительского
                     * устройства.
                     */
                    for (int lineNo = minLineNo; lineNo <= maxLineNo; lineNo++) {
                        updateLineAddressRanges(parentDevice, parentDeviceProfile, lineNo);
                    }
                    /*
                     * Запоминаем изменения родительского устройства в БД
                     * и его идентификатор в качестве результата
                     */
                    parentDevice = deviceRepository.save(parentDevice);
                    modifiedDeviceIds.add(parentDevice.getId());
                }

                createdDevices.addAll(createdInternalDevices);
                return createdDevices;
            }
        }

        return null;
    }

    /**
     * Создать шлейфовое устройство
     *
     * @param projectId идентификатор проекта
     * @param lineNo линия добавляемого устройства
     * @param lineAddress адрес добавляемого устройства на линии
     * @param deviceProfile Профиль добавляемого устройства
     * @param parentDevice Устройство, в состав которого добавляется внутреннее устройство
     * @param parentDeviceProfile Профиль устройства, в состав которого добавляется внутреннее устройство
     * @return Созданное устройство
     */
    private Device createLineDevice(String projectId, int lineNo, int lineAddress, DeviceProfile deviceProfile,
            Device parentDevice, DeviceProfile parentDeviceProfile) {
        /* Создадим сущность по проверенным параметрам */
        Device device = createDevice(deviceProfile, parentDevice);

        device.setProjectId(projectId);

        device.setLineNo(lineNo);
        device.setLineAddress(lineAddress);

        /* Генерируем поисковый адресный путь устройства */
        calculateAddressPathAndLevel(device, parentDevice);

        /*
         * Запоминаем список допустимых типов устройств, если
         * добавляемое устройство не корневое, и не прибор, и не
         * контейнер, и не транспорт.
         */
        if (parentDevice != null && parentDeviceProfile != null &&
                parentDeviceProfile.getOutputDeviceProfileIds() != null
                && (deviceProfile.getDeviceCategory() == DeviceCategory.EXECUTIVE
                    || deviceProfile.getDeviceCategory() == DeviceCategory.SENSOR)) {
            List<DeviceProfile> deviceProfileList = deviceProfileRepository.findByIdIn(parentDeviceProfile.getOutputDeviceProfileIds());

            for (DeviceProfile currentDeviceProfile : deviceProfileList) {
                if (currentDeviceProfile.getDeviceCategory() == DeviceCategory.EXECUTIVE
                        || currentDeviceProfile.getDeviceCategory() == DeviceCategory.SENSOR) {
                    device.getAcceptableDeviceProfileIds().add(currentDeviceProfile.getId());
                }
            }
        }

        return device;
    }

    private void updateParentLineAddress(Device device, Device parent) {
        /* Обновляем адрес родителя */
        if (parent.getDeviceCategory() == DeviceCategory.CONTAINER) {
            /* Если родитель - контейнер, берём адрес его родителя */
            device.setParentLineAddress(parent.getParentLineAddress());
        } else {
            device.setParentLineAddress(parent.getLineAddress());
        }
    }

    /**
     * Создать устройство на основе профиля и родителя
     *
     * @param deviceProfile Профиль устройства
     * @param parentDevice Родительское устройство
     * @return Созданное устройство
     */
    private Device createDevice(DeviceProfile deviceProfile, Device parentDevice) {
        Device device = new Device();
        /*
         * Запоминаем категорию устройства и другие дублируемые
         * параметры профиля
         */
        device.setDeviceProfileId(deviceProfile.getId());
        device.setDeviceCategory(deviceProfile.getDeviceCategory());
        device.setAcceptableAsScenarioCondition(deviceProfile.isAcceptableAsScenarioCondition());

        if (parentDevice != null) {
            device.setParentDeviceId(parentDevice.getId());
            updateParentLineAddress(device , parentDevice);
        }

        /* Заполняем диапазоны свободных адресов */
        cleanLineAddressRanges(device, deviceProfile);

        /* Добавляет проектный конфиг по умолчанию */
        setDefaultProjectConfig(device, deviceProfile);

        /* Добавляет настройки по умолчанию */
        setDefaultProjectProperties(device, deviceProfile);

        return device;
    }

    private void cleanLineAddressRanges(Device device, DeviceProfile deviceProfile) {
        /*
         * Инициализируем диапазоны свободных адресов на линиях, если
         * добавляемое устройство имеет выходные линии.
         *
         */
        if (deviceProfile.getLineCount() > 0) {
            /* Очистка списка диапазонов */
            device.getLineAddressRanges().clear();
            /*
             * В нулевую позицию добавляем пустой список диапазонов,
             * т.к. 0-вой линии не бывает (нумерация линий ведется с
             * 1-цы).
             */
            device.getLineAddressRanges().add(new ArrayList<>());
            /* В остальные позиции - копируем из профиля */
            for (int i = 1; i <= deviceProfile.getLineCount(); ++i) {
                List<AddressRange> currentLineAddressRanges = new ArrayList<>();
                for (AddressRange addressRange : deviceProfile.getLineAddressRanges()) {
                    currentLineAddressRanges.add(new AddressRange(addressRange));
                }
                device.getLineAddressRanges().add(currentLineAddressRanges);
            }
        }
    }

    /**
     * Обновить ссылки (на устройства) виртуального контейнера
     *
     * @param device
     *            проектная сущность устройства
     * @param deviceProfile
     *            профиль устройства
     * @param newLinkDeviceIdsList
     *            новый список идентификаторов устройств (ссылок)
     * @param errorMessage
     *            сообщение, заполняемое в случае ошибки
     * @return список идентификаторов обновлённых устройств или null в случае ошибки
     */
    public List<String> updateLinks(Device device, DeviceProfile deviceProfile,
            List<String> newLinkDeviceIdsList, StringBuilder errorMessage) {
        String projectId = device.getProjectId();
        /* Проверка редактируемости проекта */
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }
        /* Проверка категории устройства */
        if (device.getDeviceCategory() != DeviceCategory.VIRTUAL_CONTAINER) {
            errorMessage.append("Устройство не может содержать ссылок");
            return null;
        }
        /* Формирование списков добавляемых и удаляемых устройств */
        Set<String> newLinkDeviceIds = new HashSet<>(newLinkDeviceIdsList);
        Set<String> oldLinkDeviceIds = device.getLinkedDeviceIdsMap().values()
                .stream().flatMap(set -> set.stream()).collect(Collectors.toSet());
        if (newLinkDeviceIds.equals(oldLinkDeviceIds))
            return new ArrayList<>();
        List<String> linkDeviceIds = new ArrayList<>(newLinkDeviceIds);
        linkDeviceIds.removeAll(oldLinkDeviceIds);
        List<String> unlinkDeviceIds = new ArrayList<>(oldLinkDeviceIds);
        unlinkDeviceIds.removeAll(newLinkDeviceIds);
        /* Получение присоединяемых устройств */
        List<Device> linkDevices =
                deviceRepository.findByProjectIdAndIdInAndDisabled(projectId, linkDeviceIds, false);
        if (linkDevices.size() != linkDeviceIds.size()) {
            errorMessage.append("Присоединяемое устройство не найдено или отключено");
            return null;
        }
        /* Проверка на принадлежность прибору */
        List<Device> parentDevicesTree = findDeviceTreeDevices(projectId, device.getParentDeviceId());
        List<String> parentTreeDeviceIds = parentDevicesTree.stream().map(Device::getId).collect(Collectors.toList());
        if (!parentTreeDeviceIds.containsAll(linkDeviceIds)) {
            errorMessage.append("Присоединяемое устройство подключено к другому прибору");
            return null;
        }
        /* Присоединение устройств */
        for (Device linkDevice : linkDevices) {
            boolean deviceLinked = false;
            for (InternalDeviceItem internalDeviceItem : deviceProfile.getInternalDevices()) {
                if (internalDeviceItem.getAvailableDeviceProfileIds().contains(linkDevice.getDeviceProfileId())) {
                    Set<String> linkedDeviceIds = device.getLinkedDeviceIdsMap().get(linkDevice.getDeviceProfileId());
                    int nLinkedDevices = (linkedDeviceIds == null) ? 0 : linkedDeviceIds.size();
                    if (nLinkedDevices < internalDeviceItem.getMaxAttachableDevices()) {
                        if (linkedDeviceIds == null) {
                            linkedDeviceIds = new HashSet<>();
                            device.getLinkedDeviceIdsMap().put(linkDevice.getDeviceProfileId(), linkedDeviceIds);
                        }
                        linkedDeviceIds.add(linkDevice.getId());
                        linkDevice.setVirtualContainerId(device.getId());
                        deviceLinked = true;
                        break;
                    }
                }
            }
            if (!deviceLinked) {
                errorMessage.append("Устройство ").append(linkDevice.getId()).append(" не может быть присоединено");
                return null;
            }
        }
        /* Получение отсоединяемых устройств */
        List<Device> unlinkDevices =
                deviceRepository.findByProjectIdAndIdInAndDisabled(projectId, unlinkDeviceIds, false);
        if (unlinkDevices.size() != unlinkDeviceIds.size()) {
            errorMessage.append("Отсоединяемое устройство не найдено или отключено");
            return null;
        }
        /* Отсоединение устройств */
        for (Set<String> linkedDeviceIds : device.getLinkedDeviceIdsMap().values())
            linkedDeviceIds.removeAll(unlinkDeviceIds);
        for (Device unlinkDevice : unlinkDevices)
            unlinkDevice.setVirtualContainerId(null);
        /* Сохранение изменённых устройств */
        List<Device> modifiedDevices = new ArrayList<>();
        modifiedDevices.add(device);
        modifiedDevices.addAll(linkDevices);
        modifiedDevices.addAll(unlinkDevices);
        modifiedDevices = deviceRepository.save(modifiedDevices);
        return modifiedDevices.stream().map(Device::getId).collect(Collectors.toList());
    }

    private boolean checkLogicBlockContainsDevice(Scenario scenario, ScenarioLogicBlock logicBlock,
            String deviceId, StringBuilder errorMessage) {
        ScenarioTriggerType triggerType = scenarioTriggerTypeRepository.findOne(logicBlock.getTriggerTypeId());
        if (triggerType != null && (triggerType.getEntityType() == TriggerEntityType.DEVICE ||
                triggerType.getEntityType() == TriggerEntityType.CONTROL_DEVICE) &&
                logicBlock.getEntityIds().contains(deviceId)) {
            errorMessage.append("Устройство используется в сценарии \"" + scenario.getName() + "\" в блоке логики");
            return true;
        }
        for (ScenarioLogicBlock subLogic: logicBlock.getSubLogics()) {
            if (checkLogicBlockContainsDevice(scenario, subLogic, deviceId, errorMessage)) {
                return true;
            }
        }
        return false;
    }

    public void updateLineAddressRanges(String projectId, List<String> deviceIds) {
        List<Device> devices = deviceRepository.findByProjectIdAndIdIn(projectId, deviceIds);
        Map<String, DeviceProfile> devicesProfiles = findDeviceProfiles(devices);
        assert devicesProfiles != null : "Не найден профиль обновляемого устройства";
        Iterator<Device> device = devices.iterator();
        while (device.hasNext()) {
            Device nextDevice = device.next();
            DeviceProfile profile = devicesProfiles.get(nextDevice.getDeviceProfileId());
            if (profile.getLineCount() <= 0)
                device.remove();
            else
                for (int lineNo = DEVICE_MIN_LINE_NO; lineNo <= profile.getLineCount(); lineNo++)
                    updateLineAddressRanges(nextDevice, profile, lineNo);
        }
        deviceRepository.save(devices);
    }

    private void updateLineAddressRanges(Device parentDevice, DeviceProfile parentDeviceProfile, int lineNo) {
        /* Получаем карту занятости адресов по диапазонам профиля */
        boolean[] used = addressRanges2UsedAddressMap(parentDeviceProfile.getLineAddressRanges());

        /* Ищем устройства - потомки */
        String addressPathRegexp = "^" + parentDevice.getAddressPath() + ","
                + Integer.toString(lineNo) + ",";
        if (parentDevice.getDeviceCategory() == DeviceCategory.TRANSPORT)
            addressPathRegexp += "\\d+$";
        List<Device> childDevices = deviceRepository
                .findByProjectIdAndAddressPathRegex(parentDevice.getProjectId(), addressPathRegexp);

        /* Отметим занятыми все адреса найденных потомков */
        for (Device childDevice : childDevices) {
            used[childDevice.getLineAddress()] = true;
        }

        /*
         * По карте занятости формируем новый список диапазонов свободных
         * адресов
         */
        usedAddressMap2AddressRanges(used, parentDevice.getLineAddressRanges().get(lineNo));
    }

    private void usedAddressMap2AddressRanges(boolean[] used, List<AddressRange> addressRanges) {
        addressRanges.clear();
        boolean prevVal = true;
        int firstAddress = 0;
        for (int i = Device.MIN_LINE_ADDRESS; i <= Device.MAX_LINE_ADDRESS; ++i) {
            if (used[i] != prevVal) {
                if (prevVal) {
                    firstAddress = i;
                } else {
                    addressRanges
                            .add(new AddressRange(firstAddress, i - firstAddress));
                }
                prevVal = used[i];
            }
        }
        if (!prevVal) {
            addressRanges
                    .add(new AddressRange(firstAddress, Device.MAX_LINE_ADDRESS - firstAddress));
        }
    }

    private boolean[] addressRanges2UsedAddressMap(List<AddressRange> addressRanges) {
        boolean[] used = new boolean[Device.MAX_LINE_ADDRESS + 1];
        Arrays.fill(used, true);
        for (AddressRange addressRange : addressRanges) {
            Arrays.fill(used, addressRange.getFirstAddress(),
                    addressRange.getFirstAddress() + addressRange.getAddressCount(),
                    false);
        }
        return used;
    }

    public List<String> remove(String projectId, String deviceId, List<String> updatedDeviceIds,
            StringBuilder errorMessage) {
        List<String> removedDeviceIds = null;
        boolean success = true;
        Device device = null;
        /* Проверяем редактируемость проекта */
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            success = false;
        }
        /* Получаем объект устройства */
        if (success && (device = deviceRepository.findByProjectIdAndId(projectId, deviceId)) == null) {
            errorMessage.append("Устройство не найдено");
            success = false;
        }
        /* Проверяем, что в сценариях и виртуальных контейнерах нет ссылок на устройства из удаляемой ветки */
        if (success && checkDeviceTreeReferences(projectId, deviceId, errorMessage)) {
            success = false;
        }
        /* Проверяем, что устройство не является встроенным */
        if (success && device.isEmbedded()) {
            errorMessage.append("Нельзя удалять встроенные устройства");
            success = false;
        }

        if (success) {
            /*
             * Сначала необходимо запомнить идентификаторы всех устройств,
             * которые будут удалены (т.е. запрошенного и его потомков)
             */
            String addressPathMask = "^" + device.getAddressPath() + ",";
            List<Device> devicesToRemove = deviceRepository.findByProjectIdAndAddressPathRegex(projectId,
                    addressPathMask);
            devicesToRemove.add(device);
            removedDeviceIds = new ArrayList<>();
            for (Device deviceToRemove : devicesToRemove) {
                removedDeviceIds.add(deviceToRemove.getId());
            }

            /* При удалении виртуального контейнера надо отсоединить все устройства, присоединённые к нему */
            if (device.getDeviceCategory() == DeviceCategory.VIRTUAL_CONTAINER) {
                List<String> unlinkedDeviceIds = updateLinks(device, null, new ArrayList<>(), new StringBuilder());
                if (unlinkedDeviceIds != null && !unlinkedDeviceIds.isEmpty()) {
                    unlinkedDeviceIds.remove(deviceId);
                    updatedDeviceIds.addAll(unlinkedDeviceIds);
                }
            }

            /* Собственно удаление */
            deviceRepository.deleteByProjectIdAndIdIn(projectId, removedDeviceIds);

            /* При удалении добавляемых встроенных устройств
             * необходимо обновить счётчик подключенных встроенных устройств родителя */
            if (device.getAddressType() != AddressType.GENERIC) {
                Device parentDevice = deviceRepository.findByProjectIdAndId(projectId, device.getParentDeviceId());
                if (parentDevice != null) {
                    Integer nAttachedDevices =
                            parentDevice.getnAttachedInternalDevices().get(device.getDeviceProfileId());
                    if (nAttachedDevices != null && nAttachedDevices > 0)
                        parentDevice.getnAttachedInternalDevices().put(device.getDeviceProfileId(),
                                nAttachedDevices - 1);
                    parentDevice = deviceRepository.save(parentDevice);
                    updatedDeviceIds.add(parentDevice.getId());
                }
                return removedDeviceIds;
            }

            /*
             * Если есть родительское устройство, то у него необходимо
             * актуализировать диапазоны доступных адресов
             */
            if (device.getParentDeviceId() != null && !device.getParentDeviceId().isEmpty()) {
                /* Ищем первого родителя - не контейнера */
                Device parentDevice = deviceRepository.findByProjectIdAndId(projectId, device.getParentDeviceId());
                while (parentDevice != null && parentDevice.getDeviceCategory() == DeviceCategory.CONTAINER) {
                    parentDevice = deviceRepository.findByProjectIdAndId(projectId, parentDevice.getParentDeviceId());
                }
                if (parentDevice != null) {
                    DeviceProfile parentDeviceProfile = deviceProfileRepository
                            .findOne(parentDevice.getDeviceProfileId());
                    if (parentDeviceProfile == null) {
                        /*
                         * Если не найден профиль родительского устройства - то
                         * это ошибка. Однако мы не должны выбрасывать
                         * исключение, ведь мы уже удалили его потомков, т.е.
                         * запрошенную операцию сделали. Поэтому просто запретим
                         * в дальнейшем подключать к этому устройству новых
                         * потомков и выведем сообщение об ошибке в логах.
                         */
                        parentDevice.setLineAddressRanges(new ArrayList<>());
                        logger.error("Не найден профиль {} устройства {}", parentDevice.getDeviceProfileId(),
                                parentDevice.getId());
                    } else {
                        updateLineAddressRanges(parentDevice, parentDeviceProfile, device.getLineNo());
                    }
                    parentDevice = deviceRepository.save(parentDevice);
                    updatedDeviceIds.add(parentDevice.getId());
                }
            }
        }
        return removedDeviceIds;
    }

    private List<Device> findDeviceTreeDevices(String projectId, String rootDeviceId) {
        Device rootDevice = deviceRepository.findByProjectIdAndId(projectId, rootDeviceId);
        String addressPathMask = "^" + rootDevice.getAddressPath();
        return deviceRepository.findByProjectIdAndAddressPathRegex(projectId, addressPathMask);
    }

    private boolean checkDeviceTreeReferences(String projectId, String rootDeviceId, StringBuilder errorMessage) {
        List<Device> devices = findDeviceTreeDevices(projectId, rootDeviceId);
        for (Device device : devices) {
            if (checkDeviceInScenarios(projectId, device.getId(), errorMessage)
                    || checkDeviceInVirtualContainers(device, errorMessage)
                    || checkDeviceInAccessMaps(device, errorMessage)) {
                return true;
            }
        }
        return false;
    }

    public boolean setDeviceProfile(Device device, String deviceProfileId, StringBuilder errorMessage) {
        boolean success = false;
        if (projectService.checkProjectIsEditable(device.getProjectId(), errorMessage)) {
            if (checkDeviceInScenarios(device.getProjectId(), device.getId(), errorMessage)
                    || checkDeviceInVirtualContainers(device, errorMessage)
                    || checkDeviceInAccessMaps(device, errorMessage)) {
                success = false;
            } else {
                DeviceProfile newDeviceProfile = deviceProfileRepository.findOne(deviceProfileId);
                if (newDeviceProfile != null && device.getAcceptableDeviceProfileIds().contains(deviceProfileId)) {
                    if (deviceProfileRepository.findOne(device.getDeviceProfileId()).getSubsystem() != newDeviceProfile
                            .getSubsystem()) {
                        device.setRegionId(null);
                    }
                    device.setDeviceProfileId(deviceProfileId);
                    device.setDeviceCategory(newDeviceProfile.getDeviceCategory());
                    device.setAcceptableAsScenarioCondition(newDeviceProfile.isAcceptableAsScenarioCondition());
                    device.getFilterTags().getTags().clear();
                    device.setCustomSubsystem(null);
                    setDefaultProjectConfig(device, newDeviceProfile);
                    setDefaultProjectProperties(device, newDeviceProfile);
                    deviceRepository.save(device);
                    success = true;
                }
            }
        }
        return success;
    }

    public Device setDeviceProjectConfig(String deviceId, String projectId, Map<String, String> propertyValues,
            StringBuilder errorMessage) {
        Device device = deviceRepository.findByProjectIdAndId(projectId, deviceId);
        if (device == null) {
            errorMessage.append("Устройство не найдено (deviceId: ").append(deviceId).append(", projectId: ")
                    .append(projectId).append(")");
        } else {
            DeviceProfile deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId());
            Map<String, DeviceConfigProperty> configProperties = deviceProfile.getConfigProperties();

            boolean success = true;
            for (String propertyId : propertyValues.keySet()) {
                DeviceConfigProperty property = configProperties.get(propertyId);
                if (property == null) {
                    errorMessage.append("Не найдено свойство в профиле устройства: ");
                    errorMessage.append(propertyId);
                    success = false;
                    break;
                } else if (!property.checkValue(propertyValues.get(propertyId))) {
                    errorMessage.append("Ошибка значения свойства ");
                    errorMessage.append(propertyId);
                    success = false;
                    break;
                }
            }
            if (success) {
                Map<String, String> oldPropertyValues = device.getProjectConfigValues();
                Map<String, String> newPropertyValues = new HashMap<>();
                for (String propertyId : configProperties.keySet()) {
                    DeviceConfigProperty property = configProperties.get(propertyId);
                    String newPropertyValue = null;
                    if (propertyValues.containsKey(propertyId))
                        newPropertyValue = propertyValues.get(propertyId);
                    else if (oldPropertyValues.containsKey(propertyId))
                        newPropertyValue = oldPropertyValues.get(propertyId);
                    else
                        newPropertyValue = property.getDefaultValue();
                    newPropertyValues.put(propertyId, newPropertyValue);
                }
                device.setProjectConfigValues(newPropertyValues);
                device = deviceRepository.save(device);
            } else {
                device = null;
            }
        }
        return device;
    }

    public Device setDeviceProperties(String deviceId, String projectId, Map<String, String> propertyValues,
            StringBuilder errorMessage) {

        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }

        Device device = deviceRepository.findByProjectIdAndId(projectId, deviceId);
        if (device == null) {
            errorMessage.append("Устройство не найдено");
            return null;
        }

        DeviceProfile deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId());
        assert (deviceProfile != null);
        Map<String, DeviceProperty> properties = deviceProfile.getDeviceProperties();

        if (!checkDeviceProperties(propertyValues, errorMessage, device, deviceProfile, properties)) {
            return null;
        }

        for (String keyStr : propertyValues.keySet()) {
            device.getDevicePropertyValues().put(keyStr, propertyValues.get(keyStr));
        }

        return deviceRepository.save(device);
    }

    private boolean checkDeviceProperties(Map<String, String> propertyValues, StringBuilder errorMessage, Device device,
            DeviceProfile deviceProfile, Map<String, DeviceProperty> properties) {

        for (String keyStr : propertyValues.keySet()) {
            DeviceProperty property = properties.get(keyStr);
            if (property == null) {
                errorMessage.append("Не найдено свойство в профиле устройства: ");
                errorMessage.append(keyStr);
                return false;
            }
            String value = propertyValues.get(keyStr);

            try {
                DeviceProperties key = DeviceProperties.valueOf(keyStr);
                switch (key) {
                case RegionToId:
                case RegionFromId:
                    if (!deviceProfile.isAccessPoint()) {
                        errorMessage.append(
                                "Свойства 'зона на вход' и 'зона на выход' не моугт быть применены к устройству, которое не является точкой доступа");
                        return false;
                    }
                    if (value != null && !value.isEmpty()) {
                        Region region = regionRepository.findByProjectIdAndId(device.getProjectId(), value);
                        if (region == null) {
                            errorMessage.append("Не найдена зона, на которую ссылается свойство ");
                            errorMessage.append(keyStr);
                            return false;
                        }
                        if (region.getSubsystem() != Subsystem.SKUD) {
                            errorMessage.append("Зона, на которую ссылается свойство ");
                            errorMessage.append(keyStr);
                            errorMessage.append(" - не СКУД");
                            return false;
                        }

                    }
                    break;

                default:
                    break;
                }
            } catch (IllegalArgumentException e) {
                /* Провалимся сюда, если ключа нет в списке используемых в коде */
            }

            if (!property.checkValue(value)) {
                errorMessage.append("Недопустимое значение свойства ");
                errorMessage.append(keyStr);
                return false;
            }
        }

        return true;
    }

    public Device setDeviceDisabled(Device device, DeviceProfile deviceProfile,
            boolean disabled, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsEditable(device.getProjectId(), errorMessage) ||
                checkDeviceInScenarios(device.getProjectId(), device.getId(), errorMessage) ||
                checkDeviceInVirtualContainers(device, errorMessage) ||
                checkDeviceInAccessMaps(device, errorMessage)) {
            return null;
        }
        if (!deviceProfile.getCanBeDisabled() || device.isDisableNotAllowed()) {
            errorMessage.append("Устройство не может быть отключено");
            return null;
        }
        if (disabled == device.isDisabled()) {
            return device;
        }
        device.setDisabled(disabled);
        return deviceRepository.save(device);
    }

    public Device setActiveDeviceProjectConfig(String deviceId, String projectId, Map<String, String> propertyValues,
            StringBuilder errorMessage) {
        Device device = null;
        if (projectService.checkProjectIsActive(projectId, errorMessage)) {
            device = setDeviceProjectConfig(deviceId, projectId, propertyValues, errorMessage);
        }
        return device;
    }

    public List<String> findDeviceTreeScenarioIds(Device rootDevice) {
        // TODO: По хорошему, надо из драйвера возвращать информацию - каким приборам принадлежит сценарий.
        //  Там процедура несколько не тривиальная, и будет еще усложняться.
        List<Device> devices = findDeviceTreeDevices(rootDevice.getProjectId(), rootDevice.getId());
        Set<String> deviceIds = devices.stream().map(Device::getId).collect(Collectors.toSet());;
        return findDeviceLinkedScenarios(rootDevice.getProjectId(), deviceIds);
    }

    private List<String> findDeviceLinkedScenarios(String projectId, Set<String> deviceIds) {
        List<String> linkedScenarioIds = new ArrayList<>();
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        for (Scenario scenario : scenarios) {
            if (scenario.getControlDeviceId() != null &&
                    deviceIds.contains(scenario.getControlDeviceId())) {
                linkedScenarioIds.add(scenario.getId());
                continue;
            }
            for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks()) {
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE) {
                    for (Action action : timeLineBlock.getActions()) {
                        ScenarioActionType actionType =
                                scenarioActionTypeRepository.findOne(action.getActionTypeId());
                        if (actionType.getEntityType() == ActionEntityType.DEVICE) {
                            if (action.getEntityId() != null &&
                                    deviceIds.contains(action.getEntityId())) {
                                linkedScenarioIds.add(scenario.getId());
                                continue;
                            }
                        }
                    }
                    if (timeLineBlock.getConditionType() == ConditionType.DEVICE_STATE &&
                            timeLineBlock.getConditionEntityId() != null &&
                            deviceIds.contains(timeLineBlock.getConditionEntityId())) {
                        linkedScenarioIds.add(scenario.getId());
                        continue;
                    }
                } else if (timeLineBlock.getBlockType() == BlockType.TRACING) {
                    if (timeLineBlock.getTracingEntityType() == TracingEntityType.SENSOR_DEVICE &&
                            timeLineBlock.getTracingEntityId() != null &&
                            deviceIds.contains(timeLineBlock.getTracingEntityId())) {
                        linkedScenarioIds.add(scenario.getId());
                        continue;
                    }
                }
            }
            if (scenario.getStartLogic() != null &&
                    checkLogicBlockContainsDevices(scenario, scenario.getStartLogic(), deviceIds)) {
                linkedScenarioIds.add(scenario.getId());
                continue;
            }
            if (scenario.getStopType() == StopType.BY_LOGIC && scenario.getStopLogic() != null &&
                    checkLogicBlockContainsDevices(scenario, scenario.getStopLogic(), deviceIds)) {
                linkedScenarioIds.add(scenario.getId());
                continue;
            }
        }
        return linkedScenarioIds;
    }

    private boolean checkLogicBlockContainsDevices(Scenario scenario, ScenarioLogicBlock logicBlock,
            Set<String> deviceIds) {
        ScenarioTriggerType triggerType = scenarioTriggerTypeRepository.findOne(logicBlock.getTriggerTypeId());
        if (triggerType != null && (triggerType.getEntityType() == TriggerEntityType.DEVICE ||
                triggerType.getEntityType() == TriggerEntityType.CONTROL_DEVICE) &&
                logicBlock.getEntityIds() != null &&
                logicBlock.getEntityIds().removeAll(deviceIds)) {
            return true;
        }
        for (ScenarioLogicBlock subLogic: logicBlock.getSubLogics()) {
            if (checkLogicBlockContainsDevices(scenario, subLogic, deviceIds)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDeviceInScenarios(String projectId, String deviceId, StringBuilder errorMessage) {
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        for (Scenario scenario : scenarios) {
            if (scenario.getControlDeviceId() != null &&
                    scenario.getControlDeviceId().equals(deviceId)) {
                errorMessage.append("Устройство используется в сценарии \"" + scenario.getName()
                        + "\" в качестве приёмо-контрольного прибора");
                return true;
            }
            for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks()) {
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE) {
                    for (Action action : timeLineBlock.getActions()) {
                        ScenarioActionType actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
                        if (actionType.getEntityType() == ActionEntityType.DEVICE) {
                            if (action.getEntityId().equals(deviceId)) {
                                errorMessage.append("Устройство используется в сценарии \"" + scenario.getName()
                                        + "\" в одном из действий");
                                return true;
                            }
                        }
                    }
                    if (timeLineBlock.getConditionType() == ConditionType.DEVICE_STATE
                            && timeLineBlock.getConditionEntityId().equals(deviceId)) {
                        errorMessage.append("Устройство используется в сценарии \"" + scenario.getName()
                                + "\" в условии исполнительного блока");
                        return true;
                    }
                } else if (timeLineBlock.getBlockType() == BlockType.TRACING) {
                    if (timeLineBlock.getTracingEntityType() == TracingEntityType.SENSOR_DEVICE
                            && timeLineBlock.getTracingEntityId().equals(deviceId)) {
                        errorMessage.append(
                                "Устройство используется в сценарии \"" + scenario.getName() + "\" в блоке слежения");
                        return true;
                    }
                }
            }
            if (checkLogicBlockContainsDevice(scenario, scenario.getStartLogic(), deviceId, errorMessage)) {
                return true;
            }
            if (scenario.getStopType() == StopType.BY_LOGIC && scenario.getStopLogic() != null
                    && checkLogicBlockContainsDevice(scenario, scenario.getStopLogic(), deviceId, errorMessage)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDeviceInVirtualContainers(Device device, StringBuilder errorMessage) {
        if (device.getVirtualContainerId() != null) {
            errorMessage.append("Устройство привязано к виртуальному контейнеру ")
                    .append(device.getVirtualContainerId());
            return true;
        } else {
            return false;
        }
    }

    private boolean checkDeviceInAccessMaps(Device device, StringBuilder errorMessage) {
        List<Employee> employees = employeeRepository.findByProjectIdAndAccessMap_AccessPointDeviceIdsAndRemovedIsFalse(
                device.getProjectId(), device.getId());
        if (!employees.isEmpty()) {
            errorMessage.append(
                    "Удаляемое устройство или одно из дочерних привязано в качестве точек доступа к сотрудникам СКУД:");
            for (Employee employee : employees) {
                errorMessage.append('\n');
                errorMessage.append(employee.getName());
            }
            return true;
        }
        List<AccessKey> accessKeys = accessKeyRepository
                .findByProjectIdAndAccessMap_AccessPointDeviceIdsAndRemovedIsFalse(device.getProjectId(),
                        device.getId());
        if (!accessKeys.isEmpty()) {
            errorMessage.append(
                    "Удаляемое устройство или одно из дочерних привязано в качестве точек доступа к ключам СКУД:");
            for (AccessKey accessKey : accessKeys) {
                errorMessage.append('\n').append(accessKey.getKeyType() == KeyType.CARD ? "Карта: " : "Пароль: ")
                        .append(accessKey.getKeyValue());
            }
            return true;
        }
        return false;
    }

    private List<Device> updateChildDevicesAddressPath(Device parentDevice) {
        List<Device> updatedDevices = new ArrayList<>();
        List<Device> childDevices = deviceRepository.findByProjectIdAndParentDeviceId(parentDevice.getProjectId(),
                parentDevice.getId());
        for (Device childDevice : childDevices) {
            /* Обновляем адрес родителя */
            updateParentLineAddress(childDevice , parentDevice);

            /* Вычисляем новый поисковый адресный путь */
            calculateAddressPathAndLevel(childDevice, parentDevice);

            /* Спускаемся на следующий уровень дерева */
            updatedDevices.addAll(updateChildDevicesAddressPath(childDevice));
        }
        updatedDevices.addAll(childDevices);
        return updatedDevices;
    }

    public Device updateRootDeviceAddress(Device device, int address, StringBuilder errorMessage) {
        /* Проверяем редактируемость проекта */
        if (!projectService.checkProjectIsEditable(device.getProjectId(), errorMessage)) {
            return null;
        }

        if (checkRootDeviceAddressing(device.getProjectId(), address, errorMessage)) {
            device.setLineAddress(address);
            calculateAddressPathAndLevel(device, null);
            device = deviceRepository.save(device);

            List<Device> updatedDevices = updateChildDevicesAddressPath(device);
            deviceRepository.save(updatedDevices);

            return device;
        } else {
            return null;
        }
    }

    public Device updateNonRootDeviceAddress(Device device,
            int lineNo, int address,
            StringBuilder errorMessage) {
        /* Проверяем редактируемость проекта */
        if (!projectService.checkProjectIsEditable(device.getProjectId(), errorMessage)) {
            return null;
        }

        Device parentDevice = deviceRepository.findByProjectIdAndId(device.getProjectId(), device.getParentDeviceId());
        if (parentDevice == null) {
            errorMessage.append("Родительское устройство с идентификатором ").append(device.getParentDeviceId())
                    .append(" не найдено");
            return null;
        }
        if (parentDevice.getDeviceCategory() == DeviceCategory.CONTAINER) {
            errorMessage.append("Нельзя поменять адрес отдельно у устройства в контейнере");
            return null;
        }
        DeviceProfile parentDeviceProfile = deviceProfileRepository.findOne(parentDevice.getDeviceProfileId());
        if (parentDeviceProfile == null) {
            errorMessage.append("Профиль родительского устройства не найден");
            return null;
        }
        if (!checkRS485AddressingValues(lineNo, address, parentDeviceProfile, errorMessage))
            return null;
        /* Контейнер */
        if (device.getDeviceCategory() == DeviceCategory.CONTAINER) {
            return updateNonRootContainerDeviceAddress(device, lineNo, address, errorMessage, parentDevice,
                    parentDeviceProfile);
        }
        else {
            return updateNonRootNonContainerDeviceAddress(device, lineNo, address, errorMessage, parentDevice,
                    parentDeviceProfile);
        }
    }

    private Device updateNonRootContainerDeviceAddress(Device device, int lineNo, int address,
            StringBuilder errorMessage, Device parentDevice, DeviceProfile parentDeviceProfile) {
        int fromLineNo = device.getLineNo();
        int fromFirstAddress = device.getLineAddress();
        int nInternalDevices = deviceRepository.countByProjectIdAndParentDeviceId(device.getProjectId(),
                device.getId());
        for (int i = 0; i < nInternalDevices; ++i) {
            if (!checkRS485AddressingValues(lineNo, address + i, parentDeviceProfile, errorMessage))
                return null;
        }
        /*
         * Получим карту занятости адресов по диапазонам линии родительского
         * устройства
         */
        boolean[] usedOnFromLine = addressRanges2UsedAddressMap(
                parentDevice.getLineAddressRanges().get(fromLineNo));
        /* Отметим в карте незанятыми освобождаемые адреса */
        Arrays.fill(usedOnFromLine, fromFirstAddress, fromFirstAddress + nInternalDevices, false);

        boolean[] usedOnToLine = usedOnFromLine;
        if (fromLineNo != lineNo) {
            /*
             * Если меняется линия, сразу вычислим и запомним диапазоны
             * адресов на прежней линии, а для новой линии - получим другую
             * карту занятости
             */
            usedAddressMap2AddressRanges(usedOnFromLine, parentDevice.getLineAddressRanges().get(fromLineNo));
            usedOnToLine = addressRanges2UsedAddressMap(parentDevice.getLineAddressRanges().get(lineNo));
        }

        /* Проверим, что все занимаемые теперь адреса были свободны */
        for (int i = 0; i < nInternalDevices; ++i) {
            if (usedOnToLine[address + i]) {
                errorMessage.append("Выбранный адрес или диапазон адресов не может быть назначен");
                return null;
            }
        }

        /*
         * Перед обновлением адресов у потомков обновим адрес и адресные
         * пути у самого контейнера
         */
        device.setLineNo(lineNo);
        device.setLineAddress(address);
        calculateAddressPathAndLevel(device, parentDevice);
        device = deviceRepository.save(device);

        List<Device> internalDevices = deviceRepository
                .findByProjectIdAndParentDeviceIdAndLineNoOrderByLineAddressAsc(device.getProjectId(),
                        device.getId(), fromLineNo);
        int internalDeviceNo = 0;
        for (Device childDevice : internalDevices) {
            childDevice.setLineNo(lineNo);
            childDevice.setLineAddress(address + internalDeviceNo);
            calculateAddressPathAndLevel(childDevice, device);
            usedOnToLine[address + internalDeviceNo] = true;
            ++internalDeviceNo;
        }
        deviceRepository.save(internalDevices);

        /*
         * Вычислим и запомним новые диапазоны адресов по измененной карте
         * занятости адресов
         */
        usedAddressMap2AddressRanges(usedOnToLine, parentDevice.getLineAddressRanges().get(lineNo));
        deviceRepository.save(parentDevice);

        return device;
    }

    private Device updateNonRootNonContainerDeviceAddress(Device device, int lineNo, int address, StringBuilder errorMessage,
            Device parentDevice, DeviceProfile parentDeviceProfile) {
        /* Проверим значения номера линии и адреса */
        if (!checkRS485AddressingValues(lineNo, address, parentDeviceProfile, errorMessage))
            return null;
        int fromLineNo = device.getLineNo();
        int fromAddress = device.getLineAddress();
        /*
         * Получим карту занятости адресов по диапазонам линии родительского
         * устройства
         */
        boolean[] usedOnFromLine = addressRanges2UsedAddressMap(
                parentDevice.getLineAddressRanges().get(fromLineNo));
        /* Отметим в карте незанятым освобождаемый адрес */
        usedOnFromLine[fromAddress] = false;

        boolean[] usedOnToLine = usedOnFromLine;
        if (fromLineNo != lineNo) {
            /*
             * Если меняется линия, сразу вычислим и запомним диапазоны
             * адресов на прежней линии, а для новой линии - получим другую
             * карту занятости
             */
            usedAddressMap2AddressRanges(usedOnFromLine, parentDevice.getLineAddressRanges().get(fromLineNo));
            usedOnToLine = addressRanges2UsedAddressMap(parentDevice.getLineAddressRanges().get(lineNo));
        }

        if (usedOnToLine[address]) {
            errorMessage.append("Выбранный адрес или диапазон адресов не может быть назначен");
            return null;
        }

        device.setLineNo(lineNo);
        device.setLineAddress(address);
        calculateAddressPathAndLevel(device, parentDevice);
        device = deviceRepository.save(device);

        /* Обновим диапазоны адресов родительского устройства */
        usedOnToLine[address] = true;
        usedAddressMap2AddressRanges(usedOnToLine, parentDevice.getLineAddressRanges().get(lineNo));
        deviceRepository.save(parentDevice);

        /* Обновим адресные пути потомков, если они есть */
        List<Device> childDevices = updateChildDevicesAddressPath(device);
        deviceRepository.save(childDevices);

        return device;
    }

    private void calculateAddressPathAndLevel(Device device, Device parentDevice) {
        StringBuilder addressPath = new StringBuilder(32);
        if (parentDevice != null) {
            addressPath.append(parentDevice.getAddressPath());
            addressPath.append(",").append(device.getLineNo());
            device.setAddressLevel(parentDevice.getAddressLevel() + 1);
        }
        addressPath.append(",").append(device.getLineAddress());
        device.setAddressPath(addressPath.toString());
    }

    public boolean checkAndAdditionDeviceToImportProject(List<Device> devices, String projectId,
            Map<String, String> oldAndNewDevicesId, Map<String, String> oldAndNewRegionsId, StringBuilder errorMessage) {
        String regionFromId = DeviceProperties.RegionFromId.toString();
        String regionToId = DeviceProperties.RegionToId.toString();
        Map<String, Set<String>> mapLinkedDeviceIds = new HashMap<>();
        List<Device> devicesWithNewId = new ArrayList<>();
        Device parentDevice = new Device();
        for (Device device : devices) {
            device.setProjectId(projectId);
            String oldDeviceId = device.getId();
            device.clearId();
            Device addedDevice = deviceRepository.insert(device);
            devicesWithNewId.add(addedDevice);
            oldAndNewDevicesId.put(oldDeviceId, addedDevice.getId());
        }
        for (Device deviceWithNewId : devicesWithNewId) {
            if (deviceWithNewId.getParentDeviceId() != null) {
                if (oldAndNewDevicesId.containsKey(deviceWithNewId.getParentDeviceId())) {
                    deviceWithNewId.setParentDeviceId(oldAndNewDevicesId.get(deviceWithNewId.getParentDeviceId()));
                } else {
                    errorMessage.append("Для устройства с адресом ").append(deviceWithNewId.getAddressPath())
                            .append(" не найдено указанное родительское устройство");
                    return false;
                }
            }
            if (deviceWithNewId.getRegionId() != null) {
                if (oldAndNewRegionsId.containsKey(deviceWithNewId.getRegionId())) {
                    deviceWithNewId.setRegionId(oldAndNewRegionsId.get(deviceWithNewId.getRegionId()));
                } else {
                    errorMessage.append("Для устройства с адресом ").append(deviceWithNewId.getAddressPath())
                            .append(" не найдена указанная зона");
                    return false;
                }
            }
            if (deviceWithNewId.getVirtualContainerId() != null) {
                if (oldAndNewDevicesId.containsKey(deviceWithNewId.getVirtualContainerId())) {
                    deviceWithNewId
                            .setVirtualContainerId(oldAndNewDevicesId.get(deviceWithNewId.getVirtualContainerId()));
                } else {
                    errorMessage.append("Для устройства с адресом ").append(deviceWithNewId.getAddressPath())
                            .append(" не найден указанный виртуальный индикатор");
                    return false;
                }
            }
            if (deviceWithNewId.getLinkedDeviceIdsMap() != null && !deviceWithNewId.getLinkedDeviceIdsMap().isEmpty()) {

                for (String key : deviceWithNewId.getLinkedDeviceIdsMap().keySet()) {
                    Set<String> setDevicewithNewId = new HashSet<>();
                    for (String deviceId : deviceWithNewId.getLinkedDeviceIdsMap().get(key)) {
                        if (oldAndNewDevicesId.containsKey(deviceId)) {
                            setDevicewithNewId.add(oldAndNewDevicesId.get(deviceId));
                        } else {
                            errorMessage.append("Для устройства с адресом ").append(deviceWithNewId.getAddressPath())
                                    .append(" не найдено связанное устройство");
                            return false;
                        }
                    }
                    mapLinkedDeviceIds.put(key, setDevicewithNewId);
                }
                deviceWithNewId.setLinkedDeviceIdsMap(mapLinkedDeviceIds);
            }
            DeviceProfile deviceProfile = deviceProfileRepository.findById(deviceWithNewId.getDeviceProfileId());
            if (deviceProfile != null && deviceProfile.isAccessPoint()) {
                String desktopRegionId;
                DeviceProperty deviceProperty = deviceProfile.getDeviceProperties().get(regionFromId);
                if (deviceProperty != null && deviceProperty.getType() == PropertyType.ENTITY_ID
                        && deviceProperty.getEntityType() == EntityType.REGION
                        && deviceProperty.getEntitySubsystem() == Subsystem.SKUD) {
                    desktopRegionId = deviceWithNewId.getDevicePropertyValues().get(regionFromId);
                    if (oldAndNewRegionsId.containsKey(desktopRegionId)) {
                        deviceWithNewId.getDevicePropertyValues().put(regionFromId,
                                oldAndNewRegionsId.get(desktopRegionId));
                    }
                }
                deviceProperty = deviceProfile.getDeviceProperties().get(regionToId);
                if (deviceProperty != null && deviceProperty.getType() == PropertyType.ENTITY_ID
                        && deviceProperty.getEntityType() == EntityType.REGION
                        && deviceProperty.getEntitySubsystem() == Subsystem.SKUD) {
                    desktopRegionId = deviceWithNewId.getDevicePropertyValues().get(regionToId);
                    if (oldAndNewRegionsId.containsKey(desktopRegionId)) {
                        deviceWithNewId.getDevicePropertyValues().put(regionToId,
                                oldAndNewRegionsId.get(desktopRegionId));
                    }
                }
            }
            deviceRepository.save(deviceWithNewId);
            if (!(deviceWithNewId.getDeviceProfileId() != null && deviceWithNewId.getId() != null)) {
                errorMessage.append("У устройства с адресом ").append(deviceWithNewId.getAddressPath())
                        .append(" нет необходимых идентификаторов");
                return false;
            }
            if (deviceWithNewId.getParentDeviceId() != null) {
                parentDevice = deviceRepository.findByProjectIdAndId(deviceWithNewId.getProjectId(),
                        deviceWithNewId.getParentDeviceId());
                DeviceProfile parentDeviceProfile = deviceProfileRepository.findById(parentDevice.getDeviceProfileId());
                if (deviceWithNewId.isEditableAddress()) {
                    if (!checkRS485AddressingValues(deviceWithNewId.getLineNo(), deviceWithNewId.getLineAddress(),
                            parentDeviceProfile, errorMessage)) {
                        return false;
                    }
                }
            } else {
                if (!checkRootDeviceAddressing(deviceWithNewId.getId(), deviceWithNewId.getLineAddress(),
                        errorMessage)) {
                    return false;
                }
            }
            if (deviceWithNewId.getLinkedDeviceIdsMap() != null && !deviceWithNewId.getLinkedDeviceIdsMap().isEmpty()) {
                for (Map.Entry<String, Set<String>> entry : deviceWithNewId.getLinkedDeviceIdsMap().entrySet()) {
                    for (String linkedDeviceId : entry.getValue()) {
                        Device linkedDevice = deviceRepository.findByProjectIdAndId(projectId, linkedDeviceId);
                        if (!entry.getKey().equals(linkedDevice.getDeviceProfileId())) {
                            errorMessage.append("Для устройства с адресом ").append(linkedDevice.getAddressPath())
                                    .append(" не задан профиль");
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public Device getDeviceAndProfile(String projectId, String deviceId,
            Map<String, DeviceProfile> deviceProfilesHash) {
        Device device = deviceRepository.findByProjectIdAndId(projectId, deviceId);
        if (device == null) {
            return null;
        }

        if (deviceProfilesHash.get(device.getDeviceProfileId()) != null) {
            return device;
        }

        DeviceProfile deviceProfile = deviceProfileRepository.findById(device.getDeviceProfileId());
        if (deviceProfile == null) {
            return null;
        }

        deviceProfilesHash.put(device.getDeviceProfileId(), deviceProfile);
        return device;
    }

    /**
     * Поиск профилей устройств в базе
     *
     * @return карта профилей устройств или null если не был найден профиль хотя бы одного устройства
     */
    private Map<String, DeviceProfile> findDeviceProfiles(List<Device> devices) {
        // профили всех устройств
        List<String> deviceProfileIds =
                devices.stream().map(Device::getDeviceProfileId).distinct().collect(Collectors.toList());
        List<DeviceProfile> deviceProfiles = deviceProfileRepository.findByIdIn(deviceProfileIds);
        if (deviceProfileIds.size() != deviceProfiles.size())
            return null;
        // карта профилей устройств
        Map<String, DeviceProfile> profilesById =
                deviceProfiles.stream().collect(Collectors.toMap(DeviceProfile::getId, profile -> profile));
        return profilesById;
    }

    /**
     * Поиск профилей добавляемых устройств и устройств, на которые их можно заменить
     *
     * @return карта профилей устройств или null если не был найден профиль хотя бы одного добавляемого устройства
     */
    private Map<String, DeviceProfile> findRelatedDeviceProfiles(List<Device> devices) {
        Set<String> deviceProfileIds = new HashSet<>();
        Set<String> allDeviceProfileIds = new HashSet<>();
        for (Device device : devices) {
            deviceProfileIds.add(device.getDeviceProfileId());
            allDeviceProfileIds.addAll(device.getAcceptableDeviceProfileIds());
        }
        allDeviceProfileIds.addAll(deviceProfileIds);
        Map<String, DeviceProfile> profilesById = deviceProfileRepository.findByIdIn(allDeviceProfileIds)
                .stream().collect(Collectors.toMap(DeviceProfile::getId, profile -> profile));
        if (!profilesById.keySet().containsAll(deviceProfileIds))
            return null;
        return profilesById;
    }

    /**
     * Замена идентификаторов зон
     */
    private boolean replaceRegionIds(Device device, Map<String, String> regionIds, StringBuilder errorMessage) {
        String oldRegionId = device.getRegionId();
        if (oldRegionId != null) {
            String newRegionId = regionIds.get(oldRegionId);
            if (newRegionId == null) {
                errorMessage.append("Не найдена зона, к которой относится устройство " + device.getId());
                return false;
            }
            device.setRegionId(newRegionId);
        }
        String oldRegionFromId = device.getDevicePropertyValues().get(DeviceProperties.RegionFromId.toString());
        if (oldRegionFromId != null) {
            String newRegionId = regionIds.get(oldRegionFromId);
            if (newRegionId == null) {
                errorMessage.append("Не найдена зона выхода, к которой относится устройство " + device.getId());
                return false;
            }
            device.getDevicePropertyValues().put(DeviceProperties.RegionFromId.toString(), newRegionId);
        }
        String oldRegionToId = device.getDevicePropertyValues().get(DeviceProperties.RegionToId.toString());
        if (oldRegionToId != null) {
            String newRegionId = regionIds.get(oldRegionToId);
            if (newRegionId == null) {
                errorMessage.append("Не найдена зона входа, к которой относится устройство " + device.getId());
                return false;
            }
            device.getDevicePropertyValues().put(DeviceProperties.RegionToId.toString(), newRegionId);
        }
        return true;
    }

    /**
     * Поиск точек подключения и заполнение карты дочерних устройств
     */
    private List<String> findMountPoints(List<Device> devices, Map<String, Device> devicesById,
            Map<String, DeviceProfile> profilesById, Map<String, List<String>> childsMap,
            StringBuilder errorMessage) {
        List<String> mountPointDeviceIds = new ArrayList<>();
        for (Device device : devices) {
            String parentId = device.getParentDeviceId();
            if (parentId != null) {
                if (!devicesById.containsKey(parentId)) {
                    // родитель устройства отсутствует в списке добавляемых устройств
                    // возможно, это точка подключения
                    mountPointDeviceIds.add(parentId);
                }
                // заполнение карты обратных ссылок
                if (!childsMap.containsKey(parentId))
                    childsMap.put(parentId, new ArrayList<>());
                childsMap.get(parentId).add(device.getId());
                // для подключаемых устройств диапазоны адресов будут заполнены "с нуля"
                if (device.getParentDeviceId() != null)
                    cleanLineAddressRanges(device, profilesById.get(device.getDeviceProfileId()));
            } else {
                /** TODO: импорт дерева, включающего МС или приборы, подключенные по USB
                 * 1. проверка уровня адреса (должен быть 1)
                 * 2. назначение незанятого адреса (либо проверка с генерацией ошибки)
                 * 3. обнуление диапазонов свободных адресов (см. выше)
                 * 4. перенос из devicesById в mountPointDevices
                 * 5. заполнение карты обратных ссылок (см. выше)
                 */
                errorMessage.append("Импорт полного дерева устройств не поддерживается");
                return null;
            }
        }
        return mountPointDeviceIds;
    }

    /**
     * Воссоздание устройств из сохранённой конфигурации со всяческими проверками
     */
    private List<Device> remakeDevices(String projectId, List<Device> devices,
            Map<String, DeviceProfile> profilesById, Map<String, String> regionIds,
            StringBuilder errorMessage) {

        // заполнение некоторых полей и составление карты добавляемых устройств
        Map<String, Device> devicesById = new HashMap<>();
        for (Device device : devices) {
            // назначение идентификатора проекта
            device.setProjectId(projectId);

            // замена идентификаторов зон
            if (!replaceRegionIds(device, regionIds, errorMessage))
                return null;

            // заполнение карты устройств
            devicesById.put(device.getId(), device);

            // удаление отсутствующих профилей из списка замены
            device.getAcceptableDeviceProfileIds().retainAll(profilesById.keySet());
        }

        // поиск точек подключения и составление карты обратных ссылок (родитель -> дети)
        Map<String, List<String>> childsMap = new HashMap<>();
        List<String> mountPointDeviceIds = findMountPoints(devices, devicesById, profilesById, childsMap, errorMessage);
        if (mountPointDeviceIds == null)
            return null;

        // выборка точек подключения из базы
        if (mountPointDeviceIds.size() <= 0) {
            errorMessage.append("Не найдена точка подключения дерева устройств");
            return null;
        }
        List<Device> mountPointDevices = deviceRepository.findByProjectIdAndIdIn(projectId, mountPointDeviceIds);
        if (mountPointDevices.size() != mountPointDeviceIds.size()) {
            errorMessage.append("Не найдено родительское устройство");
            return null;
        }
        Map<String, DeviceProfile> mountPointProfilesById = findDeviceProfiles(mountPointDevices);
        if (mountPointProfilesById == null) {
            errorMessage.append("Не найден профиль точки подключения");
            return null;
        }
        profilesById.putAll(mountPointProfilesById);

        // проверка и обновление деревьев устройств
        if (!remakeDeviceTrees(mountPointDevices, devicesById, profilesById, childsMap, errorMessage))
                return null;

        return mountPointDevices;
    }

    /**
     * Воссоздание деревьев устройств, начиная с корней (по списку точек подключения)
     */
    private boolean remakeDeviceTrees(List<Device> mountPointDevices,
            Map<String, Device> devicesById, Map<String, DeviceProfile> profilesById,
            Map<String, List<String>> childsMap, StringBuilder errorMessage) {
        for (Device mountPoint : mountPointDevices) {
            // проверка категории корневого устройства
            if (mountPoint.getDeviceCategory() != DeviceCategory.TRANSPORT) {
                // TODO: добавить тип CONTROL после реализации импорта полного дерева (без точки подключения)
                errorMessage.append("Точка подключения не транспорт " + mountPoint.getId());
                return false;
            }
            // обновление и проверка деревьев точек подключения
            List<Device> children = new ArrayList<>();
            for (String childId : childsMap.get(mountPoint.getId())) {
                Device child = remakeDeviceTree(childId, mountPoint, devicesById, profilesById, childsMap,
                        children, errorMessage);
                if (child == null)
                    return false;
            }
        }
        // проверка "устройств вне дерева"
        if (!devicesById.isEmpty()) {
            errorMessage.append("Обнаружены устройства, не подключенные к дереву");
            return false;
        }
        return true;
    }

    /**
     * Воссоздание дерева устройств, начиная с ветки (рекурсивное)
     */
    private Device remakeDeviceTree(String deviceId, Device parent,
            Map<String, Device> devicesById, Map<String, DeviceProfile> profilesById,
            Map<String, List<String>> childsMap, List<Device> allAddressHolders,
            StringBuilder errorMessage) {
        Device device = devicesById.get(deviceId);
        if (device == null) {
            errorMessage.append("Обнаружена зацикленность в дереве устройств");
            return null;
        }
        // пересчёт адресного пути и глубины адреса
        calculateAddressPathAndLevel(device, parent);
        // назначение адреса родителя
        updateParentLineAddress(device, parent);
        // проверка совместимости интерфейсов подключения
        if (!checkСompatibility(profilesById.get(device.getDeviceProfileId()),
                profilesById.get(parent.getDeviceProfileId()), errorMessage))
            return null;
        // удаление устройства из карты для исключения зацикленностей
        devicesById.remove(deviceId);

        // список дочерних устройств, занимающих адрес на линии данного устройства
        List<Device> children = new ArrayList<>();
        // список дочерних виртуальных контейнеров
        List<Device> virtualContainers = new ArrayList<>();

        // обработка дочерних устройств
        if (childsMap.containsKey(deviceId)) {
            for (String childId : childsMap.get(deviceId)) {
                Device child = remakeDeviceTree(childId, device, devicesById, profilesById, childsMap,
                        children, errorMessage);
                if (child == null)
                    return null;
                if (child.getDeviceCategory() == DeviceCategory.VIRTUAL_CONTAINER)
                    virtualContainers.add(device);
            }
        }

        // заполнение списка устройств, занимающих адрес на линии
        List<Device> addressHolders = getAddressHolders(device, children);
        allAddressHolders.addAll(addressHolders);

        // проверка адресации и обновление диапазонов свободных адресов на линиях родителя
        if (parent.getDeviceCategory() != DeviceCategory.CONTAINER &&
                !appendUsedAddresses(parent, addressHolders, errorMessage))
            return null;

        // контроль кросс-ссылок виртуальных контейнеров
        for (Device virtualContainer : virtualContainers)
            if (!checkLinks(virtualContainer, children, errorMessage))
                return null;

        return device;
    }

    /**
     * Получить список устройств, занимающих адреса на линиях родителя
     */
    private List<Device> getAddressHolders(Device device, List<Device> children) {
        List<Device> addressHolders = new ArrayList<>();
        if (device.getDeviceCategory() == DeviceCategory.CONTAINER) {
            // контейнер занимает адреса, занимаемые дочерними устройстами
            addressHolders.addAll(children);
        } else if (device.getAddressType() == AddressType.GENERIC &&
                device.getDeviceCategory() != DeviceCategory.VIRTUAL_CONTAINER) {
            // устройства с общей адресацией за исключением НС
            addressHolders.add(device);
        }
        return addressHolders;
    }

    /**
     * Дополнение карты свободных адресов добавленными дочерними устройствами
     */
    private boolean appendUsedAddresses(Device device, List<Device> addressHolders, StringBuilder errorMessage) {
        for (Device child : addressHolders) {
            // поиск списка диапазонов для линии устройства
            int lineNo = child.getLineNo();
            if (lineNo < DEVICE_MIN_LINE_NO || lineNo >= device.getLineAddressRanges().size()) {
                errorMessage.append("Неверно задан номер линии устройства " + child.getId() + ": " + lineNo);
                return false;
            }
            List<AddressRange> freeAddressRanges = device.getLineAddressRanges().get(lineNo);

            // поиск диапазона, в котором содержится адрес устройства
            int address = child.getLineAddress();
            AddressRange targetRange = null;
            Iterator<AddressRange> ranges = freeAddressRanges.iterator();
            while (ranges.hasNext()) {
                AddressRange nextRange = ranges.next();
                if (nextRange.contains(address)) {
                    targetRange = nextRange;
                    ranges.remove();
                    break;
                }
            }
            if (targetRange == null) {
                errorMessage.append("Адрес устройства уже используется " + child.getId() + ": " + address);
                return false;
            }

            // коррекция или разделение диапазона
            if (targetRange.getAddressCount() > 1) {
                int size = targetRange.getAddressCount();
                int first = targetRange.getFirstAddress();
                int last = first + size - 1;
                if (address == first)
                    freeAddressRanges.add(new AddressRange(first + 1, size));
                else if (address == last)
                    freeAddressRanges.add(new AddressRange(first, size - 1));
                else {
                    freeAddressRanges.add(new AddressRange(first, address - first));
                    freeAddressRanges.add(new AddressRange(address + 1, last - address));
                }
                // сортировка диапазонов
                freeAddressRanges.sort(Comparator.comparingInt(AddressRange::getFirstAddress));
            }
        }
        return true;
    }

    /**
     * Проверка ссылок виртуальных контейнеров и на виртуальные контейнеры
     */
    private boolean checkLinks(Device device, List<Device> addressHolders, StringBuilder errorMessage) {
        Map<String, Device> addressHoldersMap =
                addressHolders.stream().collect(Collectors.toMap(Device::getId, addressHolder -> addressHolder));
        // проверка карты подключенных устройств
        for (String profileId : device.getLinkedDeviceIdsMap().keySet()) {
            // ссылки
            Set<String> linkedDeviceIds = device.getLinkedDeviceIdsMap().get(profileId);
            if (!addressHoldersMap.keySet().containsAll(linkedDeviceIds)) {
                errorMessage.append("Неверная ссылка на подключенное устройство " + device.getId());
                return false;
            }
            for (String linkedDeviceId : linkedDeviceIds) {
                Device linkedDevice = addressHoldersMap.get(linkedDeviceId);
                // обратные ссылки
                if (linkedDevice.getVirtualContainerId() == null ||
                        !linkedDevice.getVirtualContainerId().equals(device.getId())) {
                    errorMessage.append("Неверная ссылка на виртуальный контейнер " + linkedDeviceId);
                    return false;
                }
                // тип
                if (!linkedDevice.getDeviceProfileId().equals(profileId)) {
                    errorMessage.append("Неверный тип подключенного устройства " + linkedDeviceId);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Замена ссылок устройства
     */
    private void replaceDeviceIds(List<Device> devices, Map<String, String> oldToNewIds) {
        // на этом этапе карта идентификаторов содержит все затронутые устройства
        for (Device device : devices) {
            // идентификатор родителя
            String parentDeviceId = device.getParentDeviceId();
            if (parentDeviceId != null) {
                String parentDeviceNewId = oldToNewIds.get(parentDeviceId);
                device.setParentDeviceId(parentDeviceNewId);
            }
            // идентификатор виртуального контейнера
            String virtualContainerId = device.getVirtualContainerId();
            if (virtualContainerId != null) {
                String virtualContainerNewId = oldToNewIds.get(virtualContainerId);
                device.setParentDeviceId(virtualContainerNewId);
            }
            // идентификаторы устройств, подключенных к виртуальному контейнеру
            for (Set<String> linkedDeviceIds : device.getLinkedDeviceIdsMap().values()) {
                List<String> linkedDeviceIdsList = new ArrayList<>(linkedDeviceIds);
                linkedDeviceIdsList.replaceAll(id -> oldToNewIds.get(id));
                linkedDeviceIds.clear();
                linkedDeviceIds.addAll(linkedDeviceIdsList);
            }
        }
    }

    /**
     * Добавление устройств из сохранённой конфигурации
     * @param projectId идентификатор проекта
     * @param devices устройства (проектные сущности)
     * @param regionIds отображение старых идентификаторов зон на новые
     * @param oldToNewIds отображение старых идентификаторов устройств на новые (заполняется в методе)
     * @param errorMessage возвращаемый текст ошибки
     * @return список элементов дерева устройств или null в случае ошибки
     */
    public List<Device> createByRecovery(String projectId, List<Device> devices, Map<String, String> regionIds,
            Map<String, String> oldToNewIds, StringBuilder errorMessage) {
        // поиск профилей устройств
        Map<String, DeviceProfile> profilesById = findRelatedDeviceProfiles(devices);
        if (profilesById == null) {
            errorMessage.append("Не найден профиль добавляемого устройства");
            return null;
        }

        // проверка и внедрение устройств в текущий проект
        List<Device> updatedDevices = remakeDevices(projectId, devices, profilesById, regionIds, errorMessage);
        if (updatedDevices == null)
            return null;

        // проверка уникальности адресного пути и составление карты addressPath -> oldId
        Map<String, String> oldIdsByAddressPath = new HashMap<>();
        for (Device oldDevice : devices)
            if (!oldIdsByAddressPath.containsKey(oldDevice.getAddressPath()))
                oldIdsByAddressPath.put(oldDevice.getAddressPath(), oldDevice.getId());
            else {
                errorMessage.append("Обнаружены устройства с одинаковым адресным путём");
                return null;
            }

        // добавление новых, обновление старых устройств
        devices.forEach(Device::clearId);
        devices = deviceRepository.insert(devices);
        updatedDevices = deviceRepository.save(updatedDevices);

        // заполнение карты индексов
        for (Device newDevice : devices)
            oldToNewIds.put(
                    oldIdsByAddressPath.get(newDevice.getAddressPath()),
                    newDevice.getId());
        for (Device updatedDevice : updatedDevices)
            oldToNewIds.put(updatedDevice.getId(), updatedDevice.getId());

        // обновление ссылок на устройства и сохранение в базу
        replaceDeviceIds(devices, oldToNewIds);
        devices = deviceRepository.save(devices);

        return updatedDevices;
    }

}
