package ru.rubezh.firesec.nt.service.v1;

import java.lang.StringBuilder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.dao.v1.UserGroupRepository;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.UserGroup;

@Service
public class UserGroupService {

    private static final int USER_GROUP_NAME_MAX_LENGTH = 30;
    private static final int USER_GROUP_DESCRIPTION_MAX_LENGTH = 100;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    private boolean validateUserGroupData(UserGroup userGroup, User currentUser, StringBuilder errorMessage) {
        boolean result = false;
        if (userGroup.isBuiltin() && !UserService.userExistPermission(currentUser, UserService.USER_BUILTIN_UPDATE))
            errorMessage.append("Нет права на создание/изменение встроенной группы пользователей");
        // имя
        else if (userGroup.getName() == null)
            errorMessage.append("Имя группы пользователей обязательно");
        else if (userGroup.getName().length() > USER_GROUP_NAME_MAX_LENGTH)
            errorMessage.append("Имя группы пользователей должно быть не больше ")
                    .append(USER_GROUP_NAME_MAX_LENGTH).append(" символов");
        // описание
        else if (userGroup.getDescription().length() > USER_GROUP_DESCRIPTION_MAX_LENGTH)
            errorMessage.append("Описание группы пользователей должно быть не больше ")
                    .append(USER_GROUP_DESCRIPTION_MAX_LENGTH).append(" символов");
        else
            result = true;
        return result;
    }

    public UserGroup createNew(UserGroup userGroup, User currentUser, StringBuilder errorMessage) {
        UserGroup savedUserGroup = null;
        if (validateUserGroupData(userGroup, currentUser, errorMessage))
            try {
                savedUserGroup = userGroupRepository.insert(userGroup);
            } catch (DuplicateKeyException ex) {
                errorMessage.append("Данное название занято");
            }
        return savedUserGroup;
    }

    public UserGroup update(String id, UserGroup userGroup, User currentUser, StringBuilder errorMessage) {
        UserGroup result = null;
        if (validateUserGroupData(userGroup, currentUser, errorMessage)) {UserGroup savedUserGroup = userGroupRepository.findByRemovedIsFalseAndId(id);
            if (savedUserGroup == null)
                errorMessage.append("Группа пользователей не найдена");
            else {
                savedUserGroup.update(userGroup);
                try {
                    result = userGroupRepository.save(savedUserGroup);
                } catch (DuplicateKeyException ex) {
                    errorMessage.append("Данное название занято");
                }
            }
        }
        return result;
    }

    public boolean remove(String id, User currentUser, StringBuilder errorMessage) {
        boolean result = false;
        UserGroup savedUserGroup = userGroupRepository.findByRemovedIsFalseAndId(id);
        if (savedUserGroup == null)
            errorMessage.append("Группа пользователей не найдена");
        else if (userRepository.existsByUserGroupIdsContaining(id))
            errorMessage.append("У удаляемой группы пользователей есть подключенные пользователи");
        else if (savedUserGroup.isBuiltin() && !UserService.userExistPermission(currentUser, UserService.USER_BUILTIN_UPDATE))
            errorMessage.append("Нет права на удаление встроенной группы пользователей");
        else {
            savedUserGroup.setRemoved(true);
            userGroupRepository.save(savedUserGroup);
            result = true;
        }
        return result;
    }

    public boolean addGroupLink(String userId, String groupId, StringBuilder errorMessage) {
        User user = userRepository.findOne(userId);
        UserGroup userGroup = userGroupRepository.findByRemovedIsFalseAndId(groupId);
        boolean success = false;

        if (user == null)
            errorMessage.append("Пользователь не найден");
        else if (userGroup == null)
            errorMessage.append("Группа пользователей не найдена");
        else {
            success = true;
            if (!user.getUserGroupIds().contains(groupId)) {
                List<String> userGroupIds = user.getUserGroupIds();
                userGroupIds.add(groupId);
                user.setUserGroupIds(userGroupIds);
                userRepository.save(user);
            }
        }

        return success;
    }

    public boolean removeGroupLink(String userId, String groupId, StringBuilder errorMessage) {
        User user = userRepository.findOne(userId);
        UserGroup userGroup = userGroupRepository.findByRemovedIsFalseAndId(groupId);
        boolean success = false;

        if (user == null)
            errorMessage.append("Пользователь не найден");
        else if (userGroup == null)
            errorMessage.append("Группа пользователей не найдена");
        else {
            success = true;
            if (user.getUserGroupIds().contains(groupId)) {
                List<String> userGroupIds = user.getUserGroupIds();
                userGroupIds.remove(groupId);
                user.setUserGroupIds(userGroupIds);
                userRepository.save(user);
            }
        }

        return success;
    }

}
