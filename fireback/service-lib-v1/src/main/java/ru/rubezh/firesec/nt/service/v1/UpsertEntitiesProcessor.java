package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;

import org.bson.types.ObjectId;

import ru.rubezh.firesec.nt.domain.v1.ActiveProject;

public interface UpsertEntitiesProcessor {

    /**
     * Выполнение итерации по обновлению/добавлению сущностей.
     * 
     * @param activeProject текущий активный проект
     * @param observeFrom oid (или timestamp, зашифрованный в нем) последней обработанной сущности
     * @param currentTime текущее время
     * @return oid, по которому можно определить последнюю обработанную процессором сущность(и)
     */
    ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFrom, Date currentTime);
    
}
