package ru.rubezh.firesec.nt.service.v1;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@Service
public class RegionService {

    private static final int MIN_REGION_INDEX = 1;
    private static final int MAX_REGION_INDEX = 65535;
    /* Минимальная длина названия зоны */
    private static final int MIN_REGION_NAME_LENGTH = 1;
    /* Максимальная длина названия зоны */
    private static final int MAX_REGION_NAME_LENGTH = 256;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccessKeyRepository accessKeyRepository;

    private boolean checkRegionSubsystem(Region region, StringBuilder errorMessage) {
        boolean valid = true;

        switch (region.getSubsystem()) {
            case UNKNOWN:
                errorMessage.append("Подсистема зоны не может быть неизвестной");
                valid = false;
                break;
            case FIRE:
                if (region.getFireEventCount() <= 0) {
                    errorMessage.append("Кол-во извещателей для перехода в состояния Пожар-2 должно быть больше нуля");
                    valid = false;
                }
                break;
            case GENERAL:
                errorMessage.append("Подсистема зоны не может быть общей");
                valid = false;
                break;
            default:
                break;
        }

        return valid;
    }

    private boolean checkRegionValid(Region region, StringBuilder errorMessage) {
        boolean valid = true;

        /* Проверка индекса */
        if (region.getIndex() < MIN_REGION_INDEX || region.getIndex() > MAX_REGION_INDEX) {
            errorMessage.append("Номер зоны должен быть в диапазоне от " +
                    MIN_REGION_INDEX + " до " + MAX_REGION_INDEX);
            valid = false;
        } else {
            /* Проверки для указанной подсистемы */
            valid = checkRegionSubsystem(region, errorMessage);
        }
        /* Проверка названия */
        if (valid) {
            if (region.getName() == null || region.getName().length() < MIN_REGION_NAME_LENGTH) {
              errorMessage.append("Не задано название зоны");
              valid = false;
            } else {
              if (region.getName().length() > MAX_REGION_NAME_LENGTH) {
                errorMessage.append("Максимальная длина названия зоны " + MAX_REGION_NAME_LENGTH + " символов");
                valid = false;
              }
            }
        }

        if (valid) {
            if (regionRepository.countByProjectIdAndIndexAndIdNot(
                    region.getProjectId(), region.getIndex(), region.getId()) > 0) {
                errorMessage.append("Номер зоны уже используется");
                valid = false;
            }
        }

        return valid;
    }

    public Region add(String projectId, Region region, StringBuilder errorMessage) {
        Region savedRegion = null;

        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            region.setProjectId(projectId);
            if (checkRegionValid(region, errorMessage)) {
                savedRegion = regionRepository.insert(region);
            }
        }

        return savedRegion;
    }

    public Region update(String projectId, String regionId, Region region, StringBuilder errorMessage) {
        Region existedRegion = null;

        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            region.setProjectId(projectId);
            if (regionId == null)
                errorMessage.append("Необходимо указать идентификатор зоны");
            else {
                existedRegion = regionRepository.findOne(regionId);
                if (existedRegion == null)
                    errorMessage.append("Зона не найдена (").append(regionId).append(")");
                else {
                    existedRegion.update(region);
                    if (checkRegionValid(existedRegion, errorMessage))
                        existedRegion = regionRepository.save(existedRegion);
                    else
                        existedRegion = null;
                }
            }
        }
        return existedRegion;
    }

    public UpdatedScenariosAndDevices remove(String projectId, String regionId, StringBuilder errorMessage) {
        UpdatedScenariosAndDevices updatedScenariosAndDevices = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            updatedScenariosAndDevices = new UpdatedScenariosAndDevices();
            updatedScenariosAndDevices.setUpdatedDeviceIds(removeRegionFromDevices(projectId, regionId));
            updatedScenariosAndDevices.setUpdatedScenarioIds(removeRegionFromScenarios(projectId, regionId));
            removeRegionFromAccessMaps(projectId, regionId);
            regionRepository.deleteByProjectIdAndId(projectId, regionId);
        }
        return updatedScenariosAndDevices;
    }

    private void removeRegionFromAccessMaps(String projectId, String regionId) {
        Date currentDateTime = new Date();

        List<Employee> employees = employeeRepository
                .findByProjectIdAndAccessMap_WorkRegionIdsAndRemovedIsFalse(projectId, regionId);
        if (!employees.isEmpty()) {
            for (Employee employee : employees) {
                employee.getAccessMap().getWorkRegionIds().remove(regionId);
                employee.setUpdateDateTime(currentDateTime);
            }
            employeeRepository.save(employees);
        }

        List<AccessKey> accessKeys = accessKeyRepository
                .findByProjectIdAndAccessMap_WorkRegionIdsAndRemovedIsFalse(projectId, regionId);
        if (!accessKeys.isEmpty()) {
            for (AccessKey accessKey : accessKeys) {
                accessKey.getAccessMap().getWorkRegionIds().remove(regionId);
                accessKey.setUpdateDateTime(currentDateTime);
            }
            accessKeyRepository.save(accessKeys);
        }
    }

    public Device addDeviceRegionLink(String deviceId, String regionId, String projectId, StringBuilder errorMessage) {
        Region region;
        Device device;
        DeviceProfile deviceProfile;

        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            if ((region = regionRepository.findOne(regionId)) == null)
                errorMessage.append("Зона не найдена (").append(regionId).append(")");
            else if ((device = deviceRepository.findByProjectIdAndId(projectId, deviceId)) == null)
                errorMessage.append("Устройство не найдено").append(deviceId).append(")");
            else if (device.isDisabled())
                errorMessage.append("Отключенное устройство не может быть привязано к зоне");
            else if ((deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId())) == null)
                errorMessage.append("Не найден профиль устройства (").append(device.getDeviceProfileId()).append(")");
            else if (!deviceProfile.isAttachableToRegion())
                errorMessage.append("Устройство не может быть привязано к зоне");
            else if (deviceProfile.getSubsystem() != Subsystem.GENERAL &&
                    region.getSubsystem() != Subsystem.GENERAL &&
                    deviceProfile.getSubsystem() != region.getSubsystem())
                errorMessage.append("Подсистемы устройства и зоны не совпадают (")
                        .append("device subsystem: ").append(deviceProfile.getSubsystem())
                        .append(", region subsystem: ").append(region.getSubsystem()).append(")");
            else {
                device.setRegionId(regionId);
                return device;
            }
        }
        return null;
    }

    public Device removeDeviceRegionLink(String deviceId, String regionId, String projectId,
            StringBuilder errorMessage) {
        Device device;

        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            if ((device = deviceRepository.findByProjectIdAndId(projectId, deviceId)) == null)
                errorMessage.append("Устройство не найдено (").append(deviceId).append(")");
            else if (device.getRegionId() == null || !device.getRegionId().equals(regionId))
                errorMessage.append("Устройство (").append(deviceId).append(") не в зоне (")
                        .append(regionId).append(")");
            else {
                device.setRegionId(null);
                return device;
            }
        }
        return null;
    }

    public boolean removeInSubLogic(List<ScenarioLogicBlock> subLogic, String regionId, Map<String, TriggerEntityType> triggerEntityIdAndType) {
        boolean isRegionRemoved = false;
        String currentEntityTypeId = null;
        TriggerEntityType currentTriggerEntityType = null;
        for (ScenarioLogicBlock scenarioSubLogicBlock : subLogic) {
            if (!scenarioSubLogicBlock.getTriggerTypeId().isEmpty()) {
                if (triggerEntityIdAndType.containsKey(scenarioSubLogicBlock.getTriggerTypeId())) {
                    currentEntityTypeId = scenarioSubLogicBlock.getTriggerTypeId();
                    currentTriggerEntityType = triggerEntityIdAndType.get(currentEntityTypeId);
                } else {
                    currentEntityTypeId = scenarioSubLogicBlock.getTriggerTypeId();
                    currentTriggerEntityType = scenarioTriggerTypeRepository
                            .findOne(scenarioSubLogicBlock.getTriggerTypeId()).getEntityType();
                    triggerEntityIdAndType.put(currentEntityTypeId, currentTriggerEntityType);
                }
                if (scenarioSubLogicBlock.getTriggerTypeId().equals(currentEntityTypeId)
                        && currentTriggerEntityType == TriggerEntityType.REGION) {
                    for (ListIterator<String> scenarioLogicBlockIterator = scenarioSubLogicBlock.getEntityIds()
                            .listIterator(); scenarioLogicBlockIterator.hasNext();) {
                        String currentScenarioLogicBlock = scenarioLogicBlockIterator.next();
                        if (currentScenarioLogicBlock.equals(regionId)) {
                            scenarioLogicBlockIterator.remove();
                            isRegionRemoved = true;
                        }
                    }
                }
            }
            if (!scenarioSubLogicBlock.getSubLogics().isEmpty()
                    && removeInSubLogic(scenarioSubLogicBlock.getSubLogics(), regionId, triggerEntityIdAndType)) {
                isRegionRemoved = true;
            }
        }
        return isRegionRemoved;
    }

    public List<String> removeRegionFromScenarios(String projectId, String regionId) {
        List<Scenario> scenarioList = scenarioRepository.findByProjectId(projectId);
        List<String> updatedScenarios = new ArrayList<>();
        Map<String, TriggerEntityType> triggerEntityIdAndType = new HashMap<>();
        Map<String, ActionEntityType> actionEntityIdAndType = new HashMap<>();
        String currentEntityTypeId = null;
        ActionEntityType currenActionEntityType = null;
        boolean isRegionRemoved = false;
        for (Scenario scenario : scenarioList) {
            isRegionRemoved = false;
            if (scenario.getStopLogic() != null) {
                if (removeInScenarioLogicBlock(scenario.getStopLogic(), regionId, triggerEntityIdAndType)) {
                    isRegionRemoved = true;
                }
            }
            if (removeInScenarioLogicBlock(scenario.getStartLogic(), regionId, triggerEntityIdAndType)) {
                isRegionRemoved = true;
            }
            for (ScenarioTimeLineBlock scenariotimeLineBlock : scenario.getTimeLineBlocks()) {
                for (ListIterator<ScenarioTimeLineBlock.Action> scenarioTimeLineBlockActionIterator = scenariotimeLineBlock
                        .getActions().listIterator(); scenarioTimeLineBlockActionIterator.hasNext();) {
                    ScenarioTimeLineBlock.Action currentAction = scenarioTimeLineBlockActionIterator.next();
                    if (actionEntityIdAndType.containsKey(currentAction.getActionTypeId())) {
                        currentEntityTypeId = currentAction.getActionTypeId();
                        currenActionEntityType = actionEntityIdAndType.get(currentEntityTypeId);
                    } else {
                        currentEntityTypeId = scenario.getStartLogic().getTriggerTypeId();
                        currenActionEntityType = scenarioActionTypeRepository.findOne(currentAction.getActionTypeId())
                                .getEntityType();
                        actionEntityIdAndType.put(currentEntityTypeId, currenActionEntityType);
                    }
                    if (currentAction.getEntityId().equals(regionId)
                            & currenActionEntityType == ActionEntityType.REGION) {
                        scenarioTimeLineBlockActionIterator.remove();
                        isRegionRemoved = true;
                    }
                }
            }
            if (isRegionRemoved) {
                updatedScenarios.add(scenario.getId());
                scenarioRepository.save(scenario);
            }
        }
        return updatedScenarios;
    }

    public boolean removeInScenarioLogicBlock (ScenarioLogicBlock scenarioLogicBlock, String regionId, Map<String, TriggerEntityType> triggerEntityIdAndType ) {
        boolean isRegionRemoved = false;
        String currentEntityTypeId = null;
        TriggerEntityType currentTriggerEntityType = null;
        if (!scenarioLogicBlock.getEntityIds().isEmpty()) {
            if (triggerEntityIdAndType.containsKey(scenarioLogicBlock.getTriggerTypeId())) {
                currentEntityTypeId = scenarioLogicBlock.getTriggerTypeId();
                currentTriggerEntityType = triggerEntityIdAndType.get(currentEntityTypeId);
            } else {
                currentEntityTypeId = scenarioLogicBlock.getTriggerTypeId();
                currentTriggerEntityType = scenarioTriggerTypeRepository.findOne(scenarioLogicBlock.getTriggerTypeId())
                        .getEntityType();
                triggerEntityIdAndType.put(currentEntityTypeId, currentTriggerEntityType);
            }
            if (scenarioLogicBlock.getTriggerTypeId().equals(currentEntityTypeId)
                    && currentTriggerEntityType == TriggerEntityType.REGION) {
                for (ListIterator<String> scenarioStopLogicIterator = scenarioLogicBlock.getEntityIds()
                        .listIterator(); scenarioStopLogicIterator.hasNext();) {
                    String currentScenarioStopLogic = scenarioStopLogicIterator.next();
                    if (currentScenarioStopLogic.equals(regionId)) {
                        scenarioStopLogicIterator.remove();
                        isRegionRemoved = true;
                    }
                }
            }
        }
        if (!scenarioLogicBlock.getSubLogics().isEmpty()
                && removeInSubLogic(scenarioLogicBlock.getSubLogics(), regionId, triggerEntityIdAndType)) {
            isRegionRemoved = true;
        }
        return isRegionRemoved;
    }

    public List<String> removeRegionFromDevices (String projectId, String regionId){
        List<String> updatedDevices = new ArrayList<>();
        List<Device> deviceList = deviceRepository.findByProjectId(projectId);
        for (Device device : deviceList) {
            boolean updated = false;

            if (device.getRegionId() != null && device.getRegionId().equals(regionId)) {
                device.setRegionId(null);
                updated = true;
            }

            String regionFromIdKey = DeviceProperties.RegionFromId.toString();
            String regionToIdKey = DeviceProperties.RegionToId.toString();
            if (device.getDevicePropertyValues().get(regionFromIdKey) != null
                    && device.getDevicePropertyValues().get(regionFromIdKey).equals(regionId)) {
                device.getDevicePropertyValues().remove(regionFromIdKey);
                updated = true;
            }

            if (device.getDevicePropertyValues().get(regionToIdKey) != null
                    && device.getDevicePropertyValues().get(regionToIdKey).equals(regionId)) {
                device.getDevicePropertyValues().remove(regionToIdKey);
                updated = true;
            }

            if (updated) {
                updatedDevices.add(deviceRepository.save(device).getId());
            }
        }
        return updatedDevices;
    }

    public boolean checkAndAdditionRegionToImportProject(String projectId, List<Region> regions,
                                                         Map<String, String> oldAndNewRegionsId, StringBuilder errorMessage) {
        for (Region region : regions) {
            region.setProjectId(projectId);
            String oldRegionId = region.getId();
            if (region.getIndex() < MIN_REGION_INDEX || region.getIndex() > MAX_REGION_INDEX) {
                errorMessage.append("Индекс зоны ").append(region.getName()).append(" вне допустимого значения");
                return false;
            } else if (!checkRegionSubsystem(region, new StringBuilder())) {
                errorMessage.append("У зоны ").append(region.getName()).append(" некорректная подсистема");
                return false;
            }
            region.clearId();
            oldAndNewRegionsId.put(oldRegionId, regionRepository.insert(region).getId());
        }
        return true;
    }

    private BitSet findUsedRegionIndexes(String projectId) {
        BitSet usedRegionIndexes = new BitSet();
        for (Region region : regionRepository.findByProjectId(projectId))
            usedRegionIndexes.set(region.getIndex());
        return usedRegionIndexes;
    }

    public List<Region> createByRecovery(String projectId, List<Region> regions, Map<String, String> oldToNewIds,
            StringBuilder errorMessage) {
        // назначение идентификатора проекта и новых индексов
        BitSet usedRegionIndexes = findUsedRegionIndexes(projectId);
        Map<Integer, String> indexesToOldIds = new HashMap<>();
        for (Region oldRegion : regions) {
            oldRegion.setProjectId(projectId);
            // поиск свободного индекса
            int nextIndex = usedRegionIndexes.nextClearBit(MIN_REGION_INDEX);
            if (nextIndex > MAX_REGION_INDEX) {
                errorMessage.append("Число зон превышает допустимое значение");
                return null;
            }
            usedRegionIndexes.set(nextIndex);
            oldRegion.setIndex(nextIndex);
            indexesToOldIds.put(nextIndex, oldRegion.getId());
            // проверка подсистемы
            if (!checkRegionSubsystem(oldRegion, errorMessage))
                return null;
        }

        // добавление в базу
        regions.forEach(Region::clearId);
        regions = regionRepository.insert(regions);

        // заполнение карты индексов
        for (Region newRegion : regions)
            oldToNewIds.put(indexesToOldIds.get(newRegion.getIndex()), newRegion.getId());

        return regions;
    }

}
