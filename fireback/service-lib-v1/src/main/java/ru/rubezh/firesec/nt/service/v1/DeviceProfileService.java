package ru.rubezh.firesec.nt.service.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;

@Service
public class DeviceProfileService {

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    public DeviceProfile add(DeviceProfile profile) {
        return deviceProfileRepository.insert(profile);
    }

    public void remove(String profileId) {
        deviceProfileRepository.delete(profileId);
    }

    public List<DeviceProfile> getByIdIn(List<String> ids) {
        return deviceProfileRepository.findByIdIn(ids);
    }

    public DeviceProfile getById(String id) {
        return deviceProfileRepository.findOne(id);
    }

}
