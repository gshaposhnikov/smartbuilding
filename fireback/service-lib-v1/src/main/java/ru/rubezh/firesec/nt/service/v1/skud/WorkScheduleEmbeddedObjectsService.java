package ru.rubezh.firesec.nt.service.v1.skud;

import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.AccessGrantedTime;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;

public interface WorkScheduleEmbeddedObjectsService {

    public boolean addDay(String projectId, String workScheduleId, WorkScheduleDay day, StringBuilder errorMessage);

    public boolean updateDay(String projectId, String workScheduleId, int dayNo, WorkScheduleDay day,
            StringBuilder errorMessage);

    public boolean removeDay(String projectId, String workScheduleId, int dayNo, StringBuilder errorMessage);

    public boolean addAccessGrantedTime(String projectId, String workScheduleId, int dayNo,
            AccessGrantedTime grantedTime, StringBuilder errorMessage);

    public boolean updateAccessGrantedTime(String projectId, String workScheduleId, int dayNo, int timeNo,
            AccessGrantedTime grantedTime, StringBuilder errorMessage);

    public boolean removeAccessGrantedTime(String projectId, String workScheduleId, int dayNo, int timeNo,
            StringBuilder errorMessage);
}
