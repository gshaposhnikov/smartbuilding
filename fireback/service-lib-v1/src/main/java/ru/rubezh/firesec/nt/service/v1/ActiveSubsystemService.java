package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.ActiveDeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.ActiveRegionRepository;
import ru.rubezh.firesec.nt.dao.v1.ActiveSubsystemRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;
import java.util.stream.Collectors;

@Service("activeSubsystemService")
public class ActiveSubsystemService extends ActiveEntityWithStatesServiceImpl implements UpsertEntitiesProcessor,
        ActivationFilterableService {

    @Autowired
    private ActiveSubsystemRepository activeSubsystemRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private Logger logger;

    @Autowired
    private StateService stateService;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Override
    public void createActiveEntities(String projectId, Date createDateTime) {
        StateCategory defaultCategory = getDefaultStateCategory();
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, ActiveSubsystem.class);
        Map<Subsystem, ActiveSubsystem> activeSubsystems = new HashMap<>();
        for (Subsystem subsystem: Subsystem.values()) {
            activeSubsystems.computeIfAbsent(subsystem, activeSubsystem -> new ActiveSubsystem(subsystem));
            ActiveSubsystem activeSubsystem = activeSubsystems.get(subsystem);
            activeSubsystem.setGeneralStateCategoryId(defaultCategory.getId());
            activeSubsystem.setStateGetDateTime(createDateTime);
        }
        if (!activeSubsystems.isEmpty()) {
            bulkOperations.insert(new ArrayList<>(activeSubsystems.values()));
            bulkOperations.execute();
        }
    }

    @Override
    public void destroyActiveEntities() {
        activeSubsystemRepository.deleteAll();
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {
        List<ActiveSubsystem> activeSubsystems = activeSubsystemRepository.findAll();

        List<ActiveRegion> activeRegions = activeRegionRepository.findAll();
        List<ActiveDevice> activeDevices = activeDeviceRepository.findAll();
        List<String> deviceProfileIds = activeDevices
                .stream()
                .map(activeDevice -> activeDevice.getDeviceProject().getDeviceProfileId()).collect(Collectors.toList());
        List<String> accessPointDeviceProfileIds = deviceProfileRepository
                .findByIdInAndAccessPointIsTrue(deviceProfileIds)
                .stream()
                .map(DeviceProfile::getId)
                .collect(Collectors.toList());
        Map<String, State> statesByIds = stateService.getAllHashed();
        StateCategory defaultStateCategory = getDefaultStateCategory();
        List<ActiveSubsystem> updatedSubsystems = new ArrayList<>();
        for (ActiveSubsystem activeSubsystem : activeSubsystems) {
            updateStatesByEntitiesStates(activeSubsystem, activeRegions, activeDevices, statesByIds, accessPointDeviceProfileIds, false);
            if (activeSubsystem.isNeedsStateCommit()) {
                prepareCommitAfterStateUpdated(activeSubsystem, defaultStateCategory, currentTime, statesByIds);
                updatedSubsystems.add(activeSubsystem);
            }
        }

        if (!updatedSubsystems.isEmpty())
            activeSubsystemRepository.save(updatedSubsystems);
        return new ObjectId(currentTime, 0);
    }

    private void updateStatesByEntitiesStates(ActiveSubsystem activeSubsystem, List<ActiveRegion> activeRegions,
                                              List<ActiveDevice> activeDevices, Map<String, State> statesByIds,
                                              List<String> accessPointDeviceProfileIds, boolean commit) {
        if (activeSubsystem.getSubsystem() == Subsystem.SECURITY) {
            updateActiveSubsystem(activeRegions, activeSubsystem, statesByIds, accessPointDeviceProfileIds);
        } else {
            updateActiveSubsystem(activeDevices, activeSubsystem, statesByIds, accessPointDeviceProfileIds);
        }
        if (commit)
            commitAfterStateUpdated(activeSubsystem, false);
    }

    private void updateActiveSubsystem(List<? extends BasicActiveEntityWithStates> activeEntities,
                                       ActiveSubsystem activeSubsystem, Map<String, State> statesByIds,
                                       List<String> accessPointDeviceProfileIds) {
        /* очищаем и заполняем набор активных состояний зоны */
        activeSubsystem.getActiveStates().clear();
        Map<String, Set<String>> oldInheritedStates = new HashMap<>(activeSubsystem.getInheritedActiveStates());
        activeSubsystem.getInheritedActiveStates().clear();
        Map<String, ActiveSubsystem.Counter> lastCounters = activeSubsystem.getCounters();
        activeSubsystem.setCounters(new HashMap<>());

        for (BasicActiveEntityWithStates activeEntity: activeEntities){
            for (String stateId : activeEntity.getActiveStates()) {
                if (checkStateCompatibility(stateId, statesByIds, activeSubsystem))
                    updateCounterAndActiveStates(activeSubsystem, statesByIds.get(stateId), activeEntity,
                            accessPointDeviceProfileIds);
            }
            for (String parentDeviceId: activeEntity.getInheritedActiveStates().keySet()){
                Set<String> stateIds = new HashSet<>();
                for (String stateId: activeEntity.getInheritedActiveStates().get(parentDeviceId)){
                    if (checkStateCompatibility(stateId, statesByIds, activeSubsystem)){
                        stateIds.add(stateId);
                    }
                }
                activeSubsystem.getInheritedActiveStates().put(parentDeviceId, stateIds);
            }
        }

        if (!lastCounters.equals(activeSubsystem.getCounters())
                || !activeSubsystem.getInheritedActiveStates().equals(oldInheritedStates))
            activeSubsystem.setNeedsStateCommit(true);
    }

    private boolean checkStateCompatibility(String stateId, Map<String, State> statesByIds, ActiveSubsystem activeSubsystem){
        State state = statesByIds.get(stateId);
        if (state == null){
            logger.warn("State with id: " + stateId + " not found");
            return false;
        }
        /*
         * Логика определения принадлежности состояния к подсистеме:
         * 1. Если подсистема состояния - GENERAL/UNKNOWN И подсистема категории состояния совпадает с выбранной.
         * 2. Если подсистема состояния совпадает с выбранной.
         * 3. Если подсистема находится в списке дополнительных подсистем состояния
         */
        return (((state.getSubsystem() == Subsystem.GENERAL || state.getSubsystem() == Subsystem.UNKNOWN)
                    && state.getStateCategory().getSubsystem() == activeSubsystem.getSubsystem())
                || activeSubsystem.getSubsystem() == state.getSubsystem()
                || state.getAdditionalSubsystems().contains(activeSubsystem.getSubsystem()));
    }

    private void updateCounterAndActiveStates(ActiveSubsystem activeSubsystem, State state,
                                              BasicActiveEntityWithStates activeEntity,
                                              List<String> accessPointDeviceProfileIds) {
        ActiveSubsystem.Counter activeSubsystemCounter = activeSubsystem.getCounters()
                .get(state.getStateCategory().getId());
        if (activeSubsystemCounter == null)
            activeSubsystemCounter = new ActiveSubsystem.Counter();
        if (activeSubsystem.getSubsystem() == Subsystem.FIRE && activeEntity.getClass().equals(ActiveDevice.class)) {
            ActiveDevice activeDevice = (ActiveDevice) activeEntity;
            DeviceCategory deviceCategory = activeDevice.getDeviceProject().getDeviceCategory();
            if (deviceCategory == DeviceCategory.CONTROL || deviceCategory == DeviceCategory.CONTAINER
                    || deviceCategory == DeviceCategory.VIRTUAL_CONTAINER || deviceCategory == DeviceCategory.TRANSPORT
                    || deviceCategory == DeviceCategory.OTHER)
                return;
        } else if (activeSubsystem.getSubsystem() == Subsystem.SKUD
                && activeEntity.getClass().equals(ActiveDevice.class)){
            ActiveDevice activeDevice = (ActiveDevice) activeEntity;
            if (!accessPointDeviceProfileIds.contains(activeDevice.getDeviceProject().getDeviceProfileId())){
                return;
            }
        }
        activeSubsystem.getActiveStates().add(state.getId());
        activeSubsystemCounter.addActiveEntityId(activeEntity.getId());
        activeSubsystem.getCounters().put(state.getStateCategory().getId(), activeSubsystemCounter);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<ActiveSubsystem> activeSubsystems = activeSubsystemRepository.findAll();
        activeSubsystems.forEach(activeSubsystem -> {
            activeSubsystem.getFilterTags().getTags().add(activeSubsystem.getSubsystem().toString());
        });

        activeSubsystemRepository.save(activeSubsystems);
    }
}
