package ru.rubezh.firesec.nt.service.v1.skud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.AccessKeyRepository;
import ru.rubezh.firesec.nt.dao.v1.EmployeeRepository;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AccessKeyService extends SkudEntityService<AccessKey, AccessKeyRepository> {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    private SkudService skudService;

    @Override
    public boolean add(String projectId, AccessKey accessKey, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        if (!checkAccessKey(projectId, accessKey, errorMessage)){
            return false;
        }

        accessKey.setProjectId(projectId);

        /* Вычисление числового идентификатора */
        synchronized (this) {
            /*
             * Блок синхронизации - чтобы не было возможности создать нескольких
             * ключей с одним индексом
             */
            /*
             * При поиске свободного индекса не должен учитываться флаг removed,
             * т.к. при автообновлении БД СКУД сущности, отмеченные на удаление,
             * будут фактически удалены после применения изменения к БД
             * приборов. И чтобы не возникло конфликта в БД приборов, когда
             * добавляется сущность с индексом, а другая сущность с тем же
             * индексом еще не удалена, занимать индексы до фактического
             * удаления нельзя.
             */
            AccessKey topKey = entityRepository
                    .findTopByProjectIdAndKeyTypeOrderByIndexDesc(projectId, accessKey.getKeyType());
            if (topKey != null) {
                accessKey.setIndex(topKey.getIndex() + 1);
            } else {
                accessKey.setIndex(0x0001);
            }
            if (accessKey.getIndex() > 0xFFFF) {
                /* TODO: искать пустые идентификаторы в середине числового множества */
                errorMessage.append("Превышено максимальное значение счетчика ключей");
                return false;
            }

            entityRepository.insert(accessKey);
            return true;
        }
    }

    private boolean checkAccessKey(String projectId, AccessKey accessKey, StringBuilder errorMessage){
        if (accessKey.getKeyType() == null) {
            errorMessage.append("Не указан тип ключа");
            return false;
        }

        if (accessKey.getKeyValue() != null)
            accessKey.setKeyValue(accessKey.getKeyValue().toUpperCase());

        if (!checkKeyValue(projectId, accessKey, errorMessage)) {
            return false;
        }
        if (entityRepository.existsByProjectIdAndKeyTypeAndKeyValue(projectId, accessKey.getKeyType(),
                accessKey.getKeyValue())) {
            errorMessage.append("Ключ указанного типа с указанным значением уже существует");
            return false;
        }

        if (accessKey.getEmployeeId() == null || accessKey.getEmployeeId().isEmpty()) {
            errorMessage.append("Не указана учетная запись сотдрудника");
            return false;
        }

        if (!employeeRepository.existsByProjectIdAndIdAndRemovedIsFalse(projectId, accessKey.getEmployeeId())) {
            errorMessage.append("Указана не существующая учетная запись сотдрудника");
            return false;
        }

        if (!roundAndCheckActiveDates(accessKey, errorMessage)) {
            return false;
        }

        /* Проверка карты доступа */
        if (!skudService.checkAccessMapReferences(projectId, accessKey.getAccessMap(), errorMessage)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean update(String projectId, String accessKeyId, AccessKey accessKey, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        AccessKey dstAccessKey = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, accessKeyId);
        if (dstAccessKey == null) {
            errorMessage.append("Изменяемый ключ доступа не найден");
            return false;
        }
        
        if (accessKey.getKeyValue() != null)
            accessKey.setKeyValue(accessKey.getKeyValue().toUpperCase());

        if (!checkKeyValue(projectId, accessKey, errorMessage)) {
            return false;
        }
        if (entityRepository.existsByProjectIdAndIdNotAndKeyTypeAndKeyValue(projectId, accessKeyId,
                accessKey.getKeyType(), accessKey.getKeyValue())) {
            errorMessage.append("Ключ указанного типа с указанным значением уже существует");
            return false;
        }

        if (!roundAndCheckActiveDates(accessKey, errorMessage)) {
            return false;
        }

        /* Проверка карты доступа */
        if (!skudService.checkAccessMapReferences(projectId, accessKey.getAccessMap(), errorMessage)) {
            return false;
        }

        dstAccessKey.update(accessKey);
        entityRepository.save(dstAccessKey);
        return true;
    }

    @SuppressWarnings("deprecation")
    private boolean roundAndCheckActiveDates(AccessKey accessKey, StringBuilder errorMessage) {
        /* Округляем до даты */
        if (accessKey.getActiveFrom() != null) {
            accessKey.getActiveFrom().setHours(0);
            accessKey.getActiveFrom().setMinutes(0);
            accessKey.getActiveFrom().setSeconds(0);
        }

        /* Округляем до даты */
        if (accessKey.getActiveTo() != null) {
            accessKey.getActiveTo().setHours(0);
            accessKey.getActiveTo().setMinutes(0);
            accessKey.getActiveTo().setSeconds(0);
        }

        if (accessKey.getActiveFrom() != null && accessKey.getActiveTo() != null
                && accessKey.getActiveTo().before(accessKey.getActiveFrom())) {
            errorMessage.append("Указана неверная дата окончания действия");
            return false;
        }
        return true;
    }

    private boolean checkKeyValue(String projectId, AccessKey accessKey, StringBuilder errorMessage) {
        if (accessKey.getKeyValue() != null && !accessKey.getKeyValue().isEmpty()) {
            if (accessKey.getKeyValue().length() < AccessKey.MIN_KEY_VALUE_LENGTH) {
                errorMessage.append("Значение ключа не должно быть меньше ").append(AccessKey.MIN_KEY_VALUE_LENGTH)
                        .append(" символов");
                return false;
            }
            if (accessKey.getKeyValue().length() > AccessKey.MAX_KEY_VALUE_LENGTH) {
                errorMessage.append("Значение ключа не должно превышать ").append(AccessKey.MAX_KEY_VALUE_LENGTH)
                        .append(" символов");
                return false;
            }
            try {
                Long.parseUnsignedLong(accessKey.getKeyValue(), 16);
            } catch (NumberFormatException e) {
                errorMessage.append("Значение ключа должно должно содержать строку из шестнадцатиричных цифр");
                return false;
            }

        }

        return true;
    }

    @Override
    public boolean remove(String projectId, String accessKeyId, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        AccessKey accessKey = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, accessKeyId);
        if (accessKey == null) {
            errorMessage.append("Удаляемый ключ доступа не найден");
            return false;
        }

        accessKey.setRemoved(true);
        entityRepository.save(accessKey);
        return true;
    }

    @Override
    public String getKeyForUpdatedEntities() {
        return "accessKeys";
    }

    public boolean checkAndAdditionAccessKeyToImportProject(String projectId,
                                                            List<AccessKey> accessKeys,
                                                            Map<String, String> oldAndNewEmployeesId,
                                                            Map<String, String> oldAndNewDevicesId,
                                                            Map<String, String> oldAndNewWorkSchedulesId,
                                                            Map<String, String> oldAndNewRegionsId,
                                                            StringBuilder errorMessage){
        List<Integer> passwordIndexes = new ArrayList<>();
        List<Integer> cardIndexes = new ArrayList<>();
        for (AccessKey accessKey: accessKeys) {

            if (!oldAndNewEmployeesId.keySet().contains(accessKey.getEmployeeId())){
                errorMessage.append("Для ключа доступа не найден сотрудник");
                return false;
            }
            accessKey.setEmployeeId(oldAndNewEmployeesId.get(accessKey.getEmployeeId()));

            List<String> newAccessPointDeviceIds = new ArrayList<>();
            for (String accessPointDeviceId : accessKey.getAccessMap().getAccessPointDeviceIds()){
                String newAccessPointDeviceId = oldAndNewDevicesId.get(accessPointDeviceId);
                if (newAccessPointDeviceId == null){
                    errorMessage.append("У ключа доступа указана несуществующая точка доступа");
                    return false;
                }
                newAccessPointDeviceIds.add(newAccessPointDeviceId);

            }
            accessKey.getAccessMap().setAccessPointDeviceIds(newAccessPointDeviceIds);

            if (accessKey.getAccessMap().getWorkScheduleId() != null
                    && !oldAndNewWorkSchedulesId.keySet().contains(accessKey.getAccessMap().getWorkScheduleId())){
                errorMessage.append("У ключа доступа указан несуществующий график работ");
                return false;
            }
            accessKey.getAccessMap().setWorkScheduleId(oldAndNewWorkSchedulesId.get(accessKey.getAccessMap().getWorkScheduleId()));

            List<String> newWorkRegionIds = new ArrayList<>();
            for (String workRegionId : accessKey.getAccessMap().getWorkRegionIds()){
                String newWorkRegionId = oldAndNewRegionsId.get(workRegionId);
                if (newWorkRegionId == null){
                    errorMessage.append("У ключа доступа указана несуществующая рабочая зона");
                    return false;
                }
                newWorkRegionIds.add(newWorkRegionId);
            }
            accessKey.getAccessMap().setWorkRegionIds(newWorkRegionIds);

            if (!checkAccessKey(projectId, accessKey, errorMessage)){
                return false;
            }

            if (accessKey.getIndex() > MAX_INDEX_VALUE) {
                errorMessage.append("Превышено максимальное значение счетчика ключей");
                return false;
            }

            if (accessKey.getKeyType() == AccessKey.KeyType.PASSWORD){
              if (passwordIndexes.contains(accessKey.getIndex())){
                  errorMessage.append("У пароля сотрудника указан неуникальный индекс");
                  return false;
              }
              passwordIndexes.add(accessKey.getIndex());
            } else {
                if (cardIndexes.contains(accessKey.getIndex())){
                    errorMessage.append("У карточки сотрудника указан неуникальный индекс");
                    return false;
                }
                cardIndexes.add(accessKey.getIndex());
            }

            accessKey.clearId();
            accessKey.setProjectId(projectId);
        }
        entityRepository.insert(accessKeys);
        return true;
    }
}
