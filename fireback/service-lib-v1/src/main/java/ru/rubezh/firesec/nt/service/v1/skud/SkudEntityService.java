package ru.rubezh.firesec.nt.service.v1.skud;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.dao.v1.ProjectEntityRepository;
import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;
import ru.rubezh.firesec.nt.domain.v1.IssueAction;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.service.v1.IssueService;
import ru.rubezh.firesec.nt.service.v1.ProjectEntityService;
import ru.rubezh.firesec.nt.service.v1.ProjectService;

public abstract class SkudEntityService<T extends BasicEntityWithDate, R extends ProjectEntityRepository<T>>
        extends ProjectEntityService<T, R> {

    protected ProjectService projectService;

    protected static final int MAX_INDEX_VALUE = 0xFFFF;

    @Autowired
    public void setProjectService(ProjectService projectService, IssueService issueService) {
        this.projectService = projectService;

        projectService.subscribe((String projectId, boolean projectNowActive) -> {
            doRemove(new Date());
        });

        issueService.subscribe((issue, issueProgress) -> {
            if (issue.getAction() == IssueAction.WRITE_SKUD_DATABASE
                    && issueProgress.getStatus() == IssueStatus.FINISHED && issue.getProjectId() != null
                    && !issue.getProjectId().isEmpty() && issue.getDeviceId() != null
                    && !issue.getDeviceId().isEmpty()) {
                /*
                 * TODO: когда будет реализовано автосохранение, удалять будет можно только те сущности, которые
                 * принадлежали прибору, от которого прилетел статус задачи
                 */
                doRemove(issue.getCreateDateTime());
            }
        });
    }

    @Override
    public void doRemove(Date upTo) {
        Project activeProject = projectService.getActive();
        if (activeProject == null) {
            /* Фактическое удаление сущностей СКУД может выполняться только для активного проекта */
            return;
        }

        entityRepository.deleteByRemovedIsTrueAndProjectIdAndUpdateDateTimeBefore(activeProject.getId(), upTo);
    }

    @Override
    public boolean shouldRemoveOnUpLevelNotified() {
        return false;
    }

}
