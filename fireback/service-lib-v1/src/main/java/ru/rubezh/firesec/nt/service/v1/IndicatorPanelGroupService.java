package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.domain.v1.IndicatorPanelGroup;

import java.util.ArrayList;
import java.util.List;

public interface IndicatorPanelGroupService {

    class RemovedGroupAndPanelIds{
        public String projectId;
        public String removedIndicatorPanelGroupId;
        public List<String> removedIndicatorPanelIds = new ArrayList<>();

        public RemovedGroupAndPanelIds() {
        }

        public RemovedGroupAndPanelIds(String projectId) {
            this.projectId = projectId;
        }
    }

    /**
     * Обновляет данные группы панелей индикаторов
     *
     * @param indicatorPanelGroup группа панелей индикаторов с новыми параметрами
     * @param projectId id проекта
     * @param id id обновляемой группы панелей индикаторов
     * @param errorMessage сообщение возможной ошибки
     * @return обновленная группа панелей индикаторов при удачном обновлении, иначе null
     */
    IndicatorPanelGroup update(IndicatorPanelGroup indicatorPanelGroup, String projectId,
                               String id, StringBuilder errorMessage);

    /**
     * Добавляет группу панелей индикаторов
     *
     * @param indicatorPanelGroup создаваемая группа панелей индикаторов
     * @param projectId id проекта
     * @param errorMessage сообщение возможной ошибки
     * @return добавленная группа панелей индикаторов при удачном добавлении, иначе null
     */
    IndicatorPanelGroup add(IndicatorPanelGroup indicatorPanelGroup,
                            String projectId, StringBuilder errorMessage);

    /**
     * Удаляет группу и все входящие в нее панеди индикаторов
     *
     * @param projectId id проекта
     * @param id id удаляемой группы панелей индикаторов
     * @param errorMessage сообщение возможной ошибки
     * @return id проекта, в котором находится группа,
     * id группы и набор id панелей индикаторов, входящих в группу.
     * Если удаление не было выполнено, то возвращается null.
     */
    RemovedGroupAndPanelIds remove(String projectId, String id, StringBuilder errorMessage);
}
