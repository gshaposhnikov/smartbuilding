package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.IndicatorPanelGroupRepository;
import ru.rubezh.firesec.nt.dao.v1.IndicatorPanelRepository;
import ru.rubezh.firesec.nt.domain.v1.FilterTags;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanelGroup;

import java.util.List;

@Service
@Order(6)
public class IndicatorPanelGroupServiceImpl implements IndicatorPanelGroupService, ActivationFilterableService {

    @Autowired
    private IndicatorPanelGroupRepository indicatorPanelGroupRepository;

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private ProjectService projectService;

    public IndicatorPanelGroupServiceImpl() {
    }

    public IndicatorPanelGroup update(IndicatorPanelGroup indicatorPanelGroup,
                                      String projectId, String id, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsEditable(projectId, errorMessage))
            return null;
        IndicatorPanelGroup updatablePanelGroup = indicatorPanelGroupRepository.findByProjectIdAndId(projectId, id);
        if (updatablePanelGroup != null){
            updatablePanelGroup.update(indicatorPanelGroup);
            indicatorPanelGroupRepository.save(updatablePanelGroup);
        } else {
            errorMessage.append("Обновляемая группа панелей индикаторов не найдена");
            return null;
        }
        return updatablePanelGroup;
    }

    public IndicatorPanelGroup add(IndicatorPanelGroup indicatorPanelGroup,
                                   String projectId, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsEditable(projectId, errorMessage))
            return null;
        indicatorPanelGroup.setProjectId(projectId);
        indicatorPanelGroupRepository.insert(indicatorPanelGroup);
        return indicatorPanelGroup;
    }

    public RemovedGroupAndPanelIds remove(String projectId, String id, StringBuilder errorMessage) {
        RemovedGroupAndPanelIds removedGroupAndPanelIds = new RemovedGroupAndPanelIds(projectId);
        if (!projectService.checkProjectIsEditable(projectId, errorMessage))
            return null;
        if (indicatorPanelGroupRepository.deleteByProjectIdAndId(projectId, id) == 1){
            removedGroupAndPanelIds.removedIndicatorPanelGroupId = id;
            List<IndicatorPanel> deleteIndicatorPanels = indicatorPanelRepository.deleteByIndicatorGroupId(id);
            for (IndicatorPanel deletedIndicatorPanel : deleteIndicatorPanels) {
                removedGroupAndPanelIds.removedIndicatorPanelIds.add(deletedIndicatorPanel.getId());
            }
            return removedGroupAndPanelIds;
        } else {
            errorMessage.append("Удаляемая группа панелей индикаторов не найдена");
            return null;
        }
    }

    @Override
    public void clearTags(String projectId) {
        List<IndicatorPanelGroup> indicatorPanelGroups = indicatorPanelGroupRepository.findByProjectId(projectId);
        indicatorPanelGroups.forEach(indicatorPanelGroup -> {
            FilterTags filterTags = indicatorPanelGroup.getFilterTags();
            if (filterTags != null){
                filterTags.getTags().clear();
            }
        });
        indicatorPanelGroupRepository.save(indicatorPanelGroups);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<IndicatorPanelGroup> indicatorPanelGroups = indicatorPanelGroupRepository.findByProjectId(projectId);
        List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);

        indicatorPanelGroups.forEach(indicatorPanelGroup -> {
            FilterTags filterTags = indicatorPanelGroup.getFilterTags();
            if (filterTags == null){
                indicatorPanelGroup.setFilterTags(new FilterTags());
                filterTags = indicatorPanelGroup.getFilterTags();
            }

            for (IndicatorPanel indicatorPanel : indicatorPanels){
                if (indicatorPanel.getIndicatorGroupId().equals(indicatorPanelGroup.getId())){
                    filterTags.getTags().addAll(indicatorPanel.getFilterTags().chooseSubsystemTags());
                }
            }
        });

        indicatorPanelGroupRepository.save(indicatorPanelGroups);
    }
}
