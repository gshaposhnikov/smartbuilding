package ru.rubezh.firesec.nt.service.v1.skud;

import java.util.*;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessMap;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.service.v1.ActiveDeviceService;
import ru.rubezh.firesec.nt.service.v1.DeviceService;
import ru.rubezh.firesec.nt.service.v1.ProjectService;

@Service
public class SkudServiceImpl implements SkudService {

    private final String OBSERVER_ID = "ACTIVE_SKUD_CHECKER";

    @Autowired
    Logger logger;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private WorkScheduleRepository workScheduleRepository;

    @Autowired
    private EntityObserverRepository entityObserverRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccessKeyService accessKeyService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccessKeyRepository accessKeyRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveDeviceService activeDeviceService;

    @Override
    public boolean checkAccessMapReferences(String projectId, AccessMap accessMap, StringBuilder errorMessage) {
        /* Проверка точек доступа */
        if (!accessMap.getAccessPointDeviceIds().isEmpty()) {
            Map<String, DeviceProfile> deviceProfilesHash = new HashMap<>();
            for (String accessPointId : accessMap.getAccessPointDeviceIds()) {
                Device apDevice = deviceService.getDeviceAndProfile(projectId, accessPointId, deviceProfilesHash);
                if (apDevice == null) {
                    errorMessage.append("Не найдено устройство - точка доступа (id: ");
                    errorMessage.append(accessPointId);
                    errorMessage.append(')');
                    return false;
                }

                if (!deviceProfilesHash.get(apDevice.getDeviceProfileId()).isAccessPoint()) {
                    errorMessage.append("Устройство (id: ");
                    errorMessage.append(accessPointId);
                    errorMessage.append(") не является точкой доступа");
                    return false;
                }
            }
        }

        /* Проверка рабочих зон */
        for (String regionId : accessMap.getWorkRegionIds()) {
            Region region = regionRepository.findByProjectIdAndId(projectId, regionId);
            if (region == null) {
                errorMessage.append("Не найдена зона (id: ");
                errorMessage.append(regionId);
                errorMessage.append(')');
                return false;
            }

            if (region.getSubsystem() != Subsystem.SKUD) {
                errorMessage.append("Зона (id: ");
                errorMessage.append(regionId);
                errorMessage.append(") не является СКУД-зоной");
                return false;
            }
        }

        /* Проверка рабочего графика */
        if (accessMap.getWorkScheduleId() != null && !accessMap.getWorkScheduleId().isEmpty()
                && workScheduleRepository.countByProjectIdAndIdAndRemovedIsFalse(projectId,
                        accessMap.getWorkScheduleId()) != 1) {
            errorMessage.append("Не найден график работ (id: ");
            errorMessage.append(accessMap.getWorkScheduleId());
            errorMessage.append(')');
            return false;
        }

        return true;
    }

    @Override
    public void markControlDevicesWithSkudDBOutOfDate() {
        Project activeProject = projectService.getActive();
        if (activeProject == null) {
            this.entityObserverRepository.delete(OBSERVER_ID);
            return; // активный проект отсутствует
        }
        EntityObserver activeObserver = getActiveObserver();

        Date lastObservedTime = activeObserver.getLastObservedObjectId().getDate();
        Date currentTime = new Date();

        UpdatedEntities.ProjectEntitiesContainer<Employee> employeeChanges = employeeService
                .getChanges(lastObservedTime, currentTime);
        UpdatedEntities.ProjectEntitiesContainer<AccessKey> accessKeyChanges = accessKeyService
                .getChanges(lastObservedTime, currentTime);

        if (employeeChanges.isEmpty() && accessKeyChanges.isEmpty()) {
            return; // нет изменений
        }

        /*
         * Объединяем списки созданных, измененных и удаленных сущностей в одно множество, фильтруя их по принадлежности
         * активному проекту
         */
        Map<String, Employee> allUpdatedEmployees = new HashMap<>();
        for (Employee employee : employeeChanges.getCreated()) {
            if (employee.getProjectId().equals(activeProject.getId()))
                allUpdatedEmployees.put(employee.getId(), employee);
        }
        for (Employee employee : employeeChanges.getUpdated()) {
            if (employee.getProjectId().equals(activeProject.getId()))
                allUpdatedEmployees.put(employee.getId(), employee);
        }
        for (Employee employee : employeeChanges.getDeleted()) {
            if (employee.getProjectId().equals(activeProject.getId()))
                allUpdatedEmployees.put(employee.getId(), employee);
        }
        Map<String, AccessKey> allUpdatedAccessKeys = new HashMap<>();
        for (AccessKey accessKey : accessKeyChanges.getCreated()) {
            if (accessKey.getProjectId().equals(activeProject.getId()))
                allUpdatedAccessKeys.put(accessKey.getId(), accessKey);
        }
        for (AccessKey accessKey : accessKeyChanges.getUpdated()) {
            if (accessKey.getProjectId().equals(activeProject.getId()))
                allUpdatedAccessKeys.put(accessKey.getId(), accessKey);
        }
        for (AccessKey accessKey : accessKeyChanges.getDeleted()) {
            if (accessKey.getProjectId().equals(activeProject.getId()))
                allUpdatedAccessKeys.put(accessKey.getId(), accessKey);
        }

        /* Дополняем множество сотрудников владельцами измененных ключей */
        for (AccessKey accessKey : allUpdatedAccessKeys.values()) {
            if (!allUpdatedEmployees.containsKey(accessKey.getEmployeeId())) {
                Employee employee = employeeRepository.findOne(accessKey.getEmployeeId());
                /* Проверим на null на случай, если сотрудник был удален чуть раньше, чем его ключ */
                if (employee != null) {
                    allUpdatedEmployees.put(employee.getId(), employee);
                }
            }
        }

        /* Дополняем множество ключей ключами измененных сотрудников */
        for (Employee employee : allUpdatedEmployees.values()) {
            List<AccessKey> employeeAccessKeys = accessKeyRepository.findByProjectIdAndEmployeeId(activeProject.getId(),
                    employee.getId());
            for (AccessKey accessKey : employeeAccessKeys) {
                allUpdatedAccessKeys.put(accessKey.getId(), accessKey);
            }
        }

        /* По полученным множествам сотрудников и ключей формируем общее множество их точек доступа */
        Set<String> accessPointIds = new HashSet<>();
        for (Employee employee : allUpdatedEmployees.values()) {
            accessPointIds.addAll(employee.getAccessMap().getAccessPointDeviceIds());
        }
        for (AccessKey accessKey : allUpdatedAccessKeys.values()) {
            accessPointIds.addAll(accessKey.getAccessMap().getAccessPointDeviceIds());
        }
        List<ActiveDevice> accessPointDevices = activeDeviceRepository.findAllByProjectIdAndIdIn(activeProject.getId(),
                accessPointIds);

        /* Для каждой точки доступа находим ее прибор и формируем общее множество приборов */
        Map<String, ActiveDevice> devicesHash = new HashMap<>();
        Map<String, ActiveDevice> controlDevices = new HashMap<>();
        for (ActiveDevice accessPointDevice : accessPointDevices) {
            ActiveDevice controlDevice = activeDeviceService.findControlDevice(accessPointDevice, devicesHash);
            if (controlDevice != null) {
                controlDevices.put(controlDevice.getId(), controlDevice);
            }
        }

        /* Добавляем состояние "БД СКУД устарела" всем приборам полученного множества */
        for (ActiveDevice controlDevice : controlDevices.values()) {
            activeDeviceService.addActiveState(controlDevice, LogicStates.SKUD_DB_IS_OUT_OF_DATE.toString());
        }

        /* Запоминаем текущее время минус 1, т.к. изменения между датами были получены не включительно */
        currentTime.setTime(currentTime.getTime() - 1);
        activeObserver.setLastObservedObjectId(new ObjectId(currentTime, 0));
        entityObserverRepository.save(activeObserver);
    }

    private EntityObserver getActiveObserver() {
        EntityObserver activeObserver = entityObserverRepository.findOne(OBSERVER_ID);
        if (activeObserver == null) {
            activeObserver = new EntityObserver();
            activeObserver.setId(OBSERVER_ID);
            activeObserver.setLastObservedObjectId(new ObjectId(new Date(0), 0));
            activeObserver = entityObserverRepository.insert(activeObserver);
        }
        if (activeObserver.getLastObservedObjectId() == null)
            activeObserver.setLastObservedObjectId(new ObjectId(new Date(0), 0));
        return activeObserver;
    }

    @Override
    public void unmarkControlDeviceWithSkudDBOutOfDate(String projectId, String controlDeviceId,
            Date dbUpdatedDateTime) {
        if (!projectService.isProjectActive(projectId)) {
            logger.warn("Failed unmark control device - project is inactive");
            return;
        }

        ActiveDevice controlDevice = activeDeviceRepository.findByProjectIdAndId(projectId, controlDeviceId);
        if (controlDevice == null) {
            logger.error("Control device not found, project id: {}, id: {}", projectId, controlDeviceId);
            return;
        }

        if (controlDevice.getDeviceProject().getDeviceCategory() != DeviceCategory.CONTROL) {
            logger.error("Device is not control, project id: {}, id: {}", projectId, controlDeviceId);
            return;
        }

        /* Удаляем состояние "БД СКУД устарела" */
        activeDeviceService.removeActiveState(controlDevice, LogicStates.SKUD_DB_IS_OUT_OF_DATE.toString());

        /* Смещаем время наблюдателя */
        EntityObserver entityObserver = getActiveObserver();
        entityObserver.setLastObservedObjectId(new ObjectId(dbUpdatedDateTime, 0));
        entityObserverRepository.save(entityObserver);
    }

}
