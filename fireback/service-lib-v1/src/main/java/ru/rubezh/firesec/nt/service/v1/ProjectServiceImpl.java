package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.amqp.message.alert.ActiveProjectValidateStatus;
import ru.rubezh.firesec.nt.amqp.message.request.SetActiveProjectRequest;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.domain.v1.ProjectStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private ScenarioRepository scenarioRepository;
    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;
    @Autowired
    private ActiveRegionRepository activeRegionRepository;
    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;
    @Autowired
    private ActiveDeviceService activeDeviceService;
    @Autowired
    private ActiveRegionService activeRegionService;
    @Autowired
    private ActiveSubsystemService activeSubsystemService;
    @Autowired
    private ActiveScenarioService activeScenarioService;
    @Autowired
    private VirtualStateRepository virtualStateRepository;
    @Autowired
    private ActiveProjectRepository activeProjectRepository;
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AccessKeyRepository accessKeyRepository;
    @Autowired
    private WorkScheduleRepository workScheduleRepository;
    @Autowired
    private ControlDeviceEventRepository controlDeviceEventRepository;
    @Autowired
    private EntityObserverRepository entityObserverRepository;

    private List<ProjectStatusChangedHandler> projectStatusChangedHandlers = new ArrayList<>();

    @Override
    public Project add(Project project, StringBuilder errorMessage) {
        if (projectRepository.existsByNameAndVersion(project.getName(), project.getVersion())) {
            errorMessage.append("Проект с таким названием и версией уже существует");
            return null;
        }
        return projectRepository.insert(project);
    }

    @Override
    public Project update(String projectId, Project project, StringBuilder errorMessage) {
        Project updatedProject;
        if ((updatedProject = projectRepository.findOne(projectId)) == null) {
            errorMessage.append("Проект не найден");
            return null;
        } else {
            if (!checkProjectIsEditable(projectId, errorMessage)) {
                return null;
            }
            if (projectRepository.existsByNameAndVersionAndIdNot(project.getName(), project.getVersion(),
                    project.getId())) {
                errorMessage.append("Проект с таким названием и версией уже существует");
                return null;
            }
            updatedProject.update(project);
            return projectRepository.save(updatedProject);
        }
    }

    @Override
    public boolean remove(String projectId, StringBuilder errorMessage) {
        if (checkProjectIsEditable(projectId, errorMessage)) {
            deviceRepository.deleteByProjectId(projectId);
            regionRepository.deleteByProjectId(projectId);
            scenarioRepository.deleteByProjectId(projectId);
            virtualStateRepository.deleteByProjectId(projectId);
            workScheduleRepository.deleteByProjectId(projectId);
            employeeRepository.deleteByProjectId(projectId);
            accessKeyRepository.deleteByProjectId(projectId);
            projectRepository.delete(projectId);
            return true;
        }

        return false;
    }

    @Override
    public Project getActive() {
        return projectRepository.findOneByStatus(ProjectStatus.ACTIVE);
    }

    @Override
    public ActiveProject getLastActive() {
        return activeProjectRepository.findOneByOrderByStateGetDateTimeAsc();
    }

    @Override
    public boolean setActive(String projectId, StringBuilder errorMessage) {
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден");
            return false;
        }
        if (project.getStatus() != ProjectStatus.ACTIVE) {
            Project activeProject = getActive();
            if (activeProject != null && !activeProject.getId().equals(projectId)) {
                errorMessage.append("Активен другой проект: ").append(activeProject.getName()).append(" (версия ")
                        .append(activeProject.getVersion()).append(")");
                return false;
            }

            /* Очистка старых оперативных сущностей */
            activeProjectRepository.deleteAll();
            activeDeviceService.destroyActiveEntities();
            activeRegionService.destroyActiveEntities();
            activeSubsystemService.destroyActiveEntities();
            activeScenarioService.destroyActiveEntities();
            controlDeviceEventRepository.deleteAll();
            entityObserverRepository.deleteAll();

            /* Создаем оперативные сущности. Их время должно совпадать со временем проекта,
               иначе они будут приняты за обновленные. Время округляется до секунд,
               т.к. в entity observer-ах не хранятся милисекунды */
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.MILLISECOND, 0);
            Date createDateTime = calendar.getTime();
            activeDeviceService.createActiveEntities(projectId, createDateTime);
            activeRegionService.createActiveEntities(projectId, createDateTime);
            activeSubsystemService.createActiveEntities(projectId, createDateTime);
            activeScenarioService.createActiveEntities(projectId, createDateTime);
            /* Сохраняем проект с новым статусом */
            project.setStatus(ProjectStatus.ACTIVE);
            project = projectRepository.save(project);
            /* Создаем оперативную сущность самого проекта */
            ActiveProject activatedProject = new ActiveProject(project);
            activatedProject.setStateGetDateTime(createDateTime);
            activeProjectRepository.insert(activatedProject);
        }
        return true;
    }

    @Override
    public boolean setBuild(String projectId, StringBuilder errorMessage) {
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден");
            return false;
        }
        if (project.getStatus() == ProjectStatus.ACTIVE) {
            /* Удаление здесь для того чтобы после деактивации проекта в notifier-e
               не формировались сущности для отправки по веб-сокетам */
            activeDeviceService.destroyActiveEntities();
            activeRegionService.destroyActiveEntities();
            activeSubsystemService.destroyActiveEntities();
            activeScenarioService.destroyActiveEntities();
            project.setStatus(ProjectStatus.BUILD);
            projectRepository.save(project);
        }
        return true;
    }

    @Override
    public boolean checkProjectIsEditable(String projectId, StringBuilder errorMessage) {
        boolean success = false;
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден (").append(projectId).append(")");
        } else if (project.getStatus() == ProjectStatus.ACTIVE) {
            errorMessage.append("Проект в работе");
        } else {
            success = true;
        }
        return success;
    }

    @Override
    public boolean isProjectActive(String projectId) {
        Project project = projectRepository.findOne(projectId);
        if (project == null || project.getStatus() != ProjectStatus.ACTIVE)
            return false;
        return true;
    }

    @Override
    public boolean checkProjectIsActive(String projectId, StringBuilder errorMessage) {
        boolean success = false;
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден (").append(projectId).append(")");
        } else if (project.getStatus() != ProjectStatus.ACTIVE) {
            errorMessage.append("Проект не активен");
        } else {
            success = true;
        }
        return success;
    }

    @Override
    public SetActiveProjectRequest getSetActiveProjectRequest(String projectId) {
        SetActiveProjectRequest request = new SetActiveProjectRequest();
        request.setProjectId(projectId);
        Project project = projectRepository.findOne(projectId);
        if (project != null && project.getStatus() == ProjectStatus.ACTIVE) {
            /* Заполняем запрос сущностями активного проекта */
            request.setDevices(activeDeviceRepository.findByProjectId(projectId));
            request.setRegions(activeRegionRepository.findByProjectId(projectId));

            request.setScenarios(activeScenarioRepository.findByProjectId(projectId));
            request.setVirtualStates(virtualStateRepository.findByProjectId(projectId));

            request.setEmployees(employeeRepository.findByProjectIdAndRemovedIsFalse(projectId));
            request.setAccessKeys(accessKeyRepository.findByProjectIdAndRemovedIsFalse(projectId));
            request.setWorkSchedules(workScheduleRepository.findByProjectIdAndRemovedIsFalse(projectId));

            List<IssueStatus> statuses = new ArrayList<>();
            statuses.add(IssueStatus.PAUSE);
            statuses.add(IssueStatus.CREATED);
            statuses.add(IssueStatus.IN_PROGRESS);
            request.setIssues(issueRepository.findByProjectIdAndStatusIn(projectId, statuses));
        }
        return request;
    }

    @Override
    public void updateStatus(ActiveProjectValidateStatus activeProjectValidateStatus) {
        /* Находим сущность активного проекта */
        ActiveProject project = activeProjectRepository.findOne(activeProjectValidateStatus.getProjectId());
        boolean projectNowActive = false;
        if (project != null) {
            /* Запоминаем актуальный статус и сообщения валидации */
            project.setStatus(
                    activeProjectValidateStatus.isValidateSucceded() ? ProjectStatus.ACTIVE : ProjectStatus.FAILURE);
            project.setValidateMessages(activeProjectValidateStatus.getValidateMessages());
            project.setStateGetDateTime(new Date());
            activeProjectRepository.save(project);
            projectNowActive = project.getStatus() == ProjectStatus.ACTIVE;
        }
        for (ProjectStatusChangedHandler handlers : projectStatusChangedHandlers) {
            handlers.onProjectStatusChanged(activeProjectValidateStatus.getProjectId(), projectNowActive);
        }
    }

    @Override
    public String subscribe(ProjectStatusChangedHandler handler) {
        projectStatusChangedHandlers.add(handler);
        List<ActiveProject> activeProjects = activeProjectRepository.findAll();
        if (!activeProjects.isEmpty()) {
            ActiveProject activeProject = activeProjects.get(0);
            if (activeProject.getStatus() == ProjectStatus.ACTIVE)
                return activeProject.getId();
        }
        return null;
    }

    @Override
    public void unsubsribe(ProjectStatusChangedHandler handler) {
        projectStatusChangedHandlers.remove(handler);
    }

}
