package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;

/**
 * Интерфейс сервисных функций для работы с оперативными сущностями.
 * @author Александр Горячкин, Антон Васильев
 */
public interface ActiveEntityService {

    /**
     * Создать оперативные сущности проекта.
     * @param projectId идентификатор проекта
     * @param createDateTime время создания активной сущности
     */
    public void createActiveEntities(String projectId, Date createDateTime);

    /**
     * Уничтожить оперативные сущности проекта.
     */
    public void destroyActiveEntities();

}
