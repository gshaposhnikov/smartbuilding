package ru.rubezh.firesec.nt.service.v1.skud;

import java.util.Date;

import ru.rubezh.firesec.nt.domain.v1.skud.AccessMap;

public interface SkudService {

    /**
     * Проверить ссылки карты доступа на другие сущности.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param accessMap
     *            карта доступа
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true если все корректно, иначе false
     */
    public boolean checkAccessMapReferences(String projectId, AccessMap accessMap, StringBuilder errorMessage);

    /** Добавляет состояние "БД СКУД устарела" приборам, к которым он относится, сдвигая текущее время наблюдателя за
     * сущностями СКУД на текущее время */
    public void markControlDevicesWithSkudDBOutOfDate();

    /**
     * Удаляет состояние "БД СКУД устарела" прибору, сдвигая текущее время наблюдателя за сущностями СКУД на время,
     * когда была обновлена БД данного прибора
     * 
     * @param projectId идентификатор проекта (должен совпадать с активным)
     * @param controlDeviceId идентификатор прибора
     * @param dbUpdatedDateTime время обновления БД СКУД прибора
     */
    public void unmarkControlDeviceWithSkudDBOutOfDate(String projectId, String controlDeviceId,
            Date dbUpdatedDateTime);

}
