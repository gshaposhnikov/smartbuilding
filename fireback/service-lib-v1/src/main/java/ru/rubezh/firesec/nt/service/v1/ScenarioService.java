package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.domain.v1.DriverProfile;

public interface ScenarioService {

    /**
     * Затянуть все типы триггеров сценариев из профиля драйвера в БД.
     * @param driverProfile профиль драйвера
     */
    public void fetchScenarioTriggerTypes(DriverProfile driverProfile);
    
    /**
     * Затянуть все типы действий сценариев из профиля драйвера в БД.
     * @param driverProfile профиль драйвера
     */
    public void fetchScenarioActionTypes(DriverProfile driverProfile);

}
