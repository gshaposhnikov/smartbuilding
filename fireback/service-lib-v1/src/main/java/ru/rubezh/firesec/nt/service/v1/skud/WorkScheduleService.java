package ru.rubezh.firesec.nt.service.v1.skud;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.AccessKeyRepository;
import ru.rubezh.firesec.nt.dao.v1.EmployeeRepository;
import ru.rubezh.firesec.nt.dao.v1.WorkScheduleRepository;
import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.ProjectEntitiesContainer;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.AccessGrantedTime;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleType;

@Service
public class WorkScheduleService extends SkudEntityService<WorkSchedule, WorkScheduleRepository>
        implements WorkScheduleEmbeddedObjectsService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    AccessKeyRepository accessKeyRepository;

    @Override
    public List<WorkSchedule> get(String projectId, StringBuilder errorMessage) {
        List<WorkSchedule> workSchedules = super.get(projectId, errorMessage);

        if (workSchedules != null) {
            /* Удалим отмеченные на удаление вложенные объекты дней и периодов прохода */
            for (WorkSchedule workSchedule : workSchedules) {
                deleteEmbeddedIfRemoved(workSchedule);
            }
        }

        return workSchedules;
    }
    
    @Override
    public WorkSchedule get(String projectId, String workScheduleId, StringBuilder errorMessage) {
        WorkSchedule workSchedule = super.get(projectId, workScheduleId, errorMessage);

        /* Удалим отмеченные на удаление вложенные объекты дней и периодов прохода */
        deleteEmbeddedIfRemoved(workSchedule);

        return workSchedule;
    }

    private void deleteEmbeddedIfRemoved(WorkSchedule workSchedule) {
        for (Iterator<WorkScheduleDay> it = workSchedule.getDays().iterator(); it.hasNext();) {
            WorkScheduleDay day = it.next();
            if (day.isRemoved()) {
                it.remove();
            } else {
                for (Iterator<AccessGrantedTime> agtIt = day.getAccessGrantedTimes().iterator(); agtIt
                        .hasNext();) {
                    AccessGrantedTime accessGrantedTime = agtIt.next();
                    if (accessGrantedTime.isRemoved()) {
                        agtIt.remove();
                    }
                }
            }
        }
    }

    @Override
    public boolean add(String projectId, WorkSchedule workSchedule, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }
        workSchedule.setProjectId(projectId);

        roundActiveFromDate(workSchedule);

        workSchedule.setDays(new ArrayList<>());
        int nDays = workSchedule.getWorkScheduleType() == WorkScheduleType.WEEK
                ? 7
                : workSchedule.getWorkScheduleType() == WorkScheduleType.MONTH
                    ? 31
                    : 1;
        
        synchronized (this) {
            /*
             * Блок синхронизации - чтобы не было возможности создать нескольких
             * графиков с совпадающими числовыми идентификаторами
             */
            
            int freeWorkScheduleIndex = getFreeWorkScheduleIndex(projectId);
            int freeWorkScheduleDayIndex = getFreeWorkScheduleDayIndex(projectId);
            
            if (freeWorkScheduleIndex > MAX_INDEX_VALUE) {
                errorMessage.append("Превышено максимальное значение счетчика графиков");
                return false;
            }
            workSchedule.setIndex(freeWorkScheduleIndex);

            for (int i = 0; i < nDays; ++i) {
                if (freeWorkScheduleDayIndex > MAX_INDEX_VALUE) {
                    errorMessage.append("Превышено максимальное значение счетчика дней");
                    return false;
                }
                WorkScheduleDay day = new WorkScheduleDay();
                day.setIndex(freeWorkScheduleDayIndex++);
                workSchedule.getDays().add(day);
            }

            entityRepository.insert(workSchedule);
            return true;
        }
    }

    private int getFreeWorkScheduleIndex(String projectId) {
        /*
         * При поиске свободного индекса не должен учитываться флаг removed,
         * т.к. при автообновлении БД СКУД сущности, отмеченные на удаление,
         * будут фактически удалены после применения изменения к БД приборов. И
         * чтобы не возникло конфликта в БД приборов, когда добавляется сущность
         * с индексом, а другая сущность с тем же индексом еще не удалена,
         * занимать индексы до фактического удаления нельзя.
         */
        WorkSchedule workSchedule = entityRepository.findTopByProjectIdOrderByIndexDesc(projectId);
        if (workSchedule != null) {
            return workSchedule.getIndex() + 1;
        } else {
            return 1;
        }
    }

    private int getFreeWorkScheduleDayIndex(String projectId) {
        WorkSchedule workSchedule = entityRepository
                .findTopByProjectIdOrderByDays_IndexDesc(projectId);
        if (workSchedule != null) {
            int index = 1;
            for (WorkScheduleDay day : workSchedule.getDays()) {
                if (day.getIndex() > index) {
                    index = day.getIndex();
                }
            }
            return index + 1;
        } else {
            return 1;
        }
    }

    private int getFreeAccessGrantedTimeIndex(String projectId) {
        WorkSchedule workSchedule = entityRepository
                .findTopByProjectIdOrderByDays_AccessGrantedTimes_IndexDesc(projectId);
        if (workSchedule != null) {
            int index = 0;
            for (WorkScheduleDay day : workSchedule.getDays()) {
                for (AccessGrantedTime time : day.getAccessGrantedTimes()) {
                    if (time.getIndex() > index) {
                        index = time.getIndex();
                    }
                }
            }
            return index + 1;
        } else {
            return 1;
        }
    }

    @Override
    public boolean update(String projectId, String workScheduleId, WorkSchedule workSchedule,
            StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule dstWorkSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId,
                workScheduleId);
        if (dstWorkSchedule == null) {
            errorMessage.append("Изменяемый график не найден");
            return false;
        }

        roundActiveFromDate(workSchedule);

        synchronized (this) {
            /* Блок синхронизации - чтобы не было возможности создать нескольких графиков с совпадающими числовыми
             * идентификаторами */

            /* При изменении типа графика возможно надо добавить или удалить дни */
            if (workSchedule.getWorkScheduleType() != dstWorkSchedule.getWorkScheduleType()
                    && workSchedule.getWorkScheduleType() != WorkScheduleType.OTHER) {

                int nDays = workSchedule.getWorkScheduleType() == WorkScheduleType.WEEK ? 7 : 31;

                /* Вычисляем текущее кол-во дней (с учетом отмеченных на удаление - они не в счет) */
                int nWereDays = 0;
                for (WorkScheduleDay day : dstWorkSchedule.getDays()) {
                    if (!day.isRemoved()) {
                        ++nWereDays;
                    }
                }
                if (nWereDays > nDays) {
                    removeExtraDays(dstWorkSchedule, nDays);
                }
                if (nWereDays < nDays) {
                    int nDaysToAdd = nDays - nWereDays;
                    if (!putAdditionalDays(projectId, dstWorkSchedule, nDaysToAdd, errorMessage)) {
                        return false;
                    }
                }
            }
            dstWorkSchedule.update(workSchedule);
            entityRepository.save(dstWorkSchedule);
            return true;
        }
    }

    /** Добавляет дни - копии последнего, включая периоды прохода */
    private boolean putAdditionalDays(String projectId, WorkSchedule dstWorkSchedule, int nDaysToAdd,
            StringBuilder errorMessage) {
        int freeWorkScheduleDayIndex = getFreeWorkScheduleDayIndex(projectId);
        int freeAccessGrantedTimeIndex = getFreeAccessGrantedTimeIndex(projectId);
        WorkScheduleDay lastDay = new WorkScheduleDay();
        for (WorkScheduleDay day : dstWorkSchedule.getDays()) {
            if (!day.isRemoved())
                lastDay = day;
        }
        for (int i = 0; i < nDaysToAdd; ++i) {
            if (freeWorkScheduleDayIndex > 0xFFFF) {
                errorMessage.append("Превышено максимальное значение счетчика дней");
                return false;
            }

            WorkScheduleDay newDay = new WorkScheduleDay(lastDay);
            newDay.setIndex(freeWorkScheduleDayIndex++);
            for (AccessGrantedTime grantedTime : lastDay.getAccessGrantedTimes()) {
                if (freeAccessGrantedTimeIndex > 0xFFFF) {
                    errorMessage.append("Превышено максимальное значение счетчика периодов прохода");
                    return false;
                }

                AccessGrantedTime newGrantedTime = new AccessGrantedTime(grantedTime);
                newGrantedTime.setIndex(freeAccessGrantedTimeIndex++);
                newDay.getAccessGrantedTimes().add(newGrantedTime);
            }
            dstWorkSchedule.getDays().add(newDay);
        }
        return true;
    }

    /** Отмечает на удаление лишние дни */
    private void removeExtraDays(WorkSchedule dstWorkSchedule, int nDays) {
        int i = 0;
        for (WorkScheduleDay day : dstWorkSchedule.getDays()) {
            if (!day.isRemoved())
                ++i;
            if (i > nDays) {
                day.setRemoved(true);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void roundActiveFromDate(WorkSchedule workSchedule) {
        if (workSchedule.getActiveFrom() != null) {
            /* Округляем до даты */
            workSchedule.getActiveFrom().setHours(0);
            workSchedule.getActiveFrom().setMinutes(0);
            workSchedule.getActiveFrom().setSeconds(0);
        }
    }

    @Override
    public boolean remove(String projectId, String workScheduleId, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Удаляемый рабочий график не найден");
            return false;
        }

        workSchedule.setRemoved(true);
        entityRepository.save(workSchedule);

        Date currentDateTime = new Date();

        List<Employee> employees = employeeRepository
                .findByProjectIdAndAccessMap_WorkScheduleIdAndRemovedIsFalse(projectId, workScheduleId);
        if (!employees.isEmpty()) {
            for (Employee employee : employees) {
                employee.getAccessMap().setWorkScheduleId(null);
                employee.setUpdateDateTime(currentDateTime);
            }
            employeeRepository.save(employees);
        }

        List<AccessKey> accessKeys = accessKeyRepository
                .findByProjectIdAndAccessMap_WorkScheduleIdAndRemovedIsFalse(projectId, workScheduleId);
        if (!accessKeys.isEmpty()) {
            for (AccessKey accessKey : accessKeys) {
                accessKey.getAccessMap().setWorkScheduleId(null);
                accessKey.setUpdateDateTime(currentDateTime);
            }
            accessKeyRepository.save(accessKeys);
        }

        return true;
    }

    @Override
    public ProjectEntitiesContainer<WorkSchedule> getChanges(Date from, Date upTo) {
        ProjectEntitiesContainer<WorkSchedule> changes = super.getChanges(from, upTo);
        for (WorkSchedule workSchedule : changes.getUpdated()) {
            deleteEmbeddedIfRemoved(workSchedule);
        }
        return changes;
    }

    @Override
    public void doRemove(Date upTo) {
        Project activeProject = projectService.getActive();
        if (activeProject == null) {
            /* Фактическое удаление сущностей СКУД может выполняться только для активного проекта */
            return;
        }
        
        List<WorkSchedule> notRemovedWorkSchedules = entityRepository
                .findByProjectIdAndRemovedIsFalse(activeProject.getId());
        doRemoveDaysAndTimes(notRemovedWorkSchedules, upTo);

        entityRepository.deleteByRemovedIsTrueAndProjectIdAndUpdateDateTimeBefore(activeProject.getId(), upTo);
    }

    /** Удаляет дни и периоды прохода, отмеченные на удаление */
    private void doRemoveDaysAndTimes(List<WorkSchedule> workSchedules, Date upTo) {
        List<WorkSchedule> workSchedulesToSave = new ArrayList<>();
        for (WorkSchedule workSchedule : workSchedules) {
            boolean needSave = false;
            for (Iterator<WorkScheduleDay> dayIt = workSchedule.getDays().iterator(); dayIt.hasNext();) {
                WorkScheduleDay day = dayIt.next();
                if (day.isRemoved() && day.getUpdateDateTime().before(upTo)) {
                    dayIt.remove();
                    needSave = true;
                } else {
                    for (Iterator<AccessGrantedTime> timeIt = day.getAccessGrantedTimes().iterator(); timeIt
                            .hasNext();) {
                        AccessGrantedTime time = timeIt.next();
                        if (time.isRemoved() && time.getUpdateDateTime().before(upTo)) {
                            timeIt.remove();
                            needSave = true;
                        }
                    }
                }
            }
            if (needSave) {
                workSchedulesToSave.add(workSchedule);
            }
        }
        if (!workSchedulesToSave.isEmpty()) {
            entityRepository.save(workSchedulesToSave);
        }
    }

    @Override
    public String getKeyForUpdatedEntities() {
        return "workSchedules";
    }

    private boolean checkDayIntervals(WorkScheduleDay day, StringBuilder errorMessage) {
        if (day.isWorkday()) {
            /* Проверим интервалы рабочего дня */
            if (day.getWorkStartTime() < 0 || day.getWorkStopTime() < 0 || day.getWorkStartTime() > 1440
                    || day.getWorkStopTime() > 1440 || day.getWorkStartTime() > day.getWorkStopTime()) {
                errorMessage.append("Некорректно задан рабочий интервал времени");
                return false;
            }

            if ((day.getLunchStartTime() >= 0 && day.getLunchStopTime() < 0)
                    || (day.getLunchStartTime() < 0 && day.getLunchStopTime() >= 0)
                    || (day.getLunchStartTime() >= 0 && day.getLunchStopTime() >= 0
                            && (day.getLunchStartTime() < day.getWorkStartTime()
                                    || day.getLunchStopTime() < day.getWorkStartTime()
                                    || day.getLunchStartTime() > day.getWorkStopTime()
                                    || day.getLunchStopTime() > day.getWorkStopTime()))) {
                errorMessage.append("Некорректно задан интервал времени перерыва");
                return false;
            }
        }
        return true;
    }

    private WorkScheduleDay findNotRemovedDay(WorkSchedule workSchedule, int dayNo) {
        WorkScheduleDay dstDay = null;
        int i = 0;
        for (WorkScheduleDay someDay : workSchedule.getDays()) {
            if (!someDay.isRemoved()) {
                if (i == dayNo) {
                    dstDay = someDay;
                    break;
                }
                ++i;
            }
        }
        return dstDay;
    }

    @Override
    public boolean addDay(String projectId, String workScheduleId, WorkScheduleDay day, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        if (workSchedule.getWorkScheduleType() != WorkScheduleType.OTHER) {
            errorMessage.append("Нельзя добавить день в фиксированный график");
            return false;
        }

        if (!checkDayIntervals(day, errorMessage)) {
            return false;
        }
        
        synchronized (this) {
            /* Блок синхронизации - чтобы не было возможности создать несколько дней с совпадающими числовыми
             * идентификаторами */
            int freeDayIndex = getFreeWorkScheduleDayIndex(projectId);
            if (freeDayIndex > 0xFFFF) {
                errorMessage.append("Превышено максимальное значение счетчика дней");
                return false;
            }
            WorkScheduleDay newDay = new WorkScheduleDay(day);
            newDay.setIndex(freeDayIndex);

            workSchedule.getDays().add(newDay);
            workSchedule.setUpdateDateTime(newDay.getCreateDateTime());
            entityRepository.save(workSchedule);
            return true;
        }
    }

    @Override
    public boolean updateDay(String projectId, String workScheduleId, int dayNo, WorkScheduleDay day,
            StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        WorkScheduleDay dstDay = findNotRemovedDay(workSchedule, dayNo);
        if (dstDay == null) {
            errorMessage.append("Отсутствует день №").append(dayNo);
            return false;
        }

        if (!checkDayIntervals(day, errorMessage)) {
            return false;
        }

        dstDay.update(day);
        workSchedule.setUpdateDateTime(dstDay.getUpdateDateTime());
        entityRepository.save(workSchedule);
        return true;

    }

    @Override
    public boolean removeDay(String projectId, String workScheduleId, int dayNo, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        if (workSchedule.getWorkScheduleType() != WorkScheduleType.OTHER) {
            errorMessage.append("Нельзя удалить день из фиксированного графика");
            return false;
        }

        WorkScheduleDay dayToRemove = findNotRemovedDay(workSchedule, dayNo);
        if (dayToRemove == null) {
            errorMessage.append("Отсутствует день №").append(dayNo);
            return false;
        }

        dayToRemove.setRemoved(true);
        workSchedule.setUpdateDateTime(new Date());
        entityRepository.save(workSchedule);
        return true;
    }

    private boolean checkGrantedTimeIntervals(AccessGrantedTime grantedTime, StringBuilder errorMessage) {
        if (grantedTime.getFrom() < 0 || grantedTime.getTo() < 0 || grantedTime.getFrom() > 1440
                || grantedTime.getTo() > 1440 || grantedTime.getFrom() > grantedTime.getTo()) {
            errorMessage.append("Некорректно задан интервал времени разрешенного прохода");
            return false;
        }
        return true;
    }

    private AccessGrantedTime findNotRemovedGrantedTime(WorkScheduleDay day, int timeNo) {
        AccessGrantedTime grantedTime = null;
        int i = 0;
        for (AccessGrantedTime someTime : day.getAccessGrantedTimes()) {
            if (!someTime.isRemoved()) {
                if (i == timeNo) {
                    grantedTime = someTime;
                    break;
                }
                ++i;
            }
        }
        return grantedTime;
    }

    @Override
    public boolean addAccessGrantedTime(String projectId, String workScheduleId, int dayNo,
            AccessGrantedTime grantedTime, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        WorkScheduleDay dayToUpdate = findNotRemovedDay(workSchedule, dayNo);
        if (dayToUpdate == null) {
            errorMessage.append("Отсутствует день №").append(dayNo);
            return false;
        }

        if (!checkGrantedTimeIntervals(grantedTime, errorMessage)) {
            return false;
        }

        synchronized (this) {
            /* Блок синхронизации - чтобы не было возможности создать несколько дней с совпадающими числовыми
             * идентификаторами */
            int freeGrantedTimeIndex = getFreeAccessGrantedTimeIndex(projectId);
            if (freeGrantedTimeIndex > 0xFFFF) {
                errorMessage.append("Превышено максимальное значение счетчика периодов прохода");
                return false;
            }
            AccessGrantedTime newGrantedTime = new AccessGrantedTime(grantedTime);
            newGrantedTime.setIndex(freeGrantedTimeIndex++);

            dayToUpdate.getAccessGrantedTimes().add(newGrantedTime);
            dayToUpdate.setUpdateDateTime(newGrantedTime.getCreateDateTime());
            workSchedule.setUpdateDateTime(newGrantedTime.getCreateDateTime());
            entityRepository.save(workSchedule);
            return true;
        }
    }

    @Override
    public boolean updateAccessGrantedTime(String projectId, String workScheduleId, int dayNo, int timeNo,
            AccessGrantedTime grantedTime, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        WorkScheduleDay dayToUpdate = findNotRemovedDay(workSchedule, dayNo);
        if (dayToUpdate == null) {
            errorMessage.append("Отсутствует день №").append(dayNo);
            return false;
        }

        AccessGrantedTime dstGrantedTime = findNotRemovedGrantedTime(dayToUpdate, timeNo);
        if (dstGrantedTime == null) {
            errorMessage.append("Отсутствует период прохода №").append(timeNo);
            return false;
        }

        if (!checkGrantedTimeIntervals(grantedTime, errorMessage)) {
            return false;
        }

        dstGrantedTime.update(grantedTime);
        dayToUpdate.setUpdateDateTime(dstGrantedTime.getUpdateDateTime());
        workSchedule.setUpdateDateTime(dstGrantedTime.getUpdateDateTime());
        entityRepository.save(workSchedule);
        return true;
    }

    @Override
    public boolean removeAccessGrantedTime(String projectId, String workScheduleId, int dayNo, int timeNo,
            StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return false;
        }

        WorkSchedule workSchedule = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, workScheduleId);
        if (workSchedule == null) {
            errorMessage.append("Рабочий график не найден");
            return false;
        }

        WorkScheduleDay dayToUpdate = findNotRemovedDay(workSchedule, dayNo);
        if (dayToUpdate == null) {
            errorMessage.append("Отсутствует день №").append(dayNo);
            return false;
        }

        AccessGrantedTime grantedTimeToRemove = findNotRemovedGrantedTime(dayToUpdate, timeNo);
        if (grantedTimeToRemove == null) {
            errorMessage.append("Отсутствует период прохода №").append(timeNo);
            return false;
        }

        Date currentDateTime = new Date();
        grantedTimeToRemove.setRemoved(true);
        dayToUpdate.setUpdateDateTime(currentDateTime);
        workSchedule.setUpdateDateTime(currentDateTime);
        entityRepository.save(workSchedule);
        return true;
    }

    public boolean checkAndAdditionWorkScheduleToImportProject(String projectId,
                                                               List<WorkSchedule> workSchedules,
                                                               Map<String, String> oldAndNewWorkScheduleIds,
                                                               StringBuilder errorMessage){
        List<String> oldWorkScheduleIds = new ArrayList<>();
        List<Integer> workScheduleIndexes = new ArrayList<>();
        List<Integer> workScheduleDayIndexes = new ArrayList<>();
        List<Integer> accessGrantedTimeIndexes = new ArrayList<>();
        for (WorkSchedule workSchedule : workSchedules){
            if (workSchedule.getIndex() > MAX_INDEX_VALUE
                    || workSchedule.getDays().stream().anyMatch(workScheduleDay -> workSchedule.getIndex() > MAX_INDEX_VALUE)){
                errorMessage.append("Рабочий график ").append(workSchedule.getName()).append(" содержит недопустмый индекс");
                return false;
            }

            if (workScheduleIndexes.contains(workSchedule.getIndex())){
                errorMessage.append("У рабочего графика ").append(workSchedule.getName()).append(" неуникальный индекс");
                return false;
            }
            workScheduleIndexes.add(workSchedule.getIndex());
            for (WorkSchedule.WorkScheduleDay workScheduleDay : workSchedule.getDays()){
                if (workScheduleDayIndexes.contains(workScheduleDay.getIndex())){
                    errorMessage.append("В дне рабочего графика ").append(workSchedule.getName()).append(" указан неуникальный индекс");
                    return false;
                }
                for (AccessGrantedTime accessGrantedTime: workScheduleDay.getAccessGrantedTimes()){
                    if (accessGrantedTimeIndexes.contains(accessGrantedTime.getIndex())){
                        errorMessage.append("Неуникальный индекс периода разрешенного прохода в графике ").append(workSchedule.getName());
                        return false;
                    }
                    accessGrantedTimeIndexes.add(accessGrantedTime.getIndex());
                }
                workScheduleDayIndexes.add(workScheduleDay.getIndex());
            }

            oldWorkScheduleIds.add(workSchedule.getId());
            workSchedule.clearId();
            workSchedule.setProjectId(projectId);
        }
        Iterator<String> itOnOld = oldWorkScheduleIds.iterator();
        Iterator<String> itOnNew = entityRepository.insert(workSchedules).stream().map(WorkSchedule::getId).iterator();
        while (itOnOld.hasNext() && itOnNew.hasNext()){
            oldAndNewWorkScheduleIds.put(itOnOld.next(), itOnNew.next());
        }
        return true;
    }
}
