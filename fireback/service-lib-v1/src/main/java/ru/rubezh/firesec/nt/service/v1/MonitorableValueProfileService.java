package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.MonitorableValueProfileRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;
import ru.rubezh.firesec.nt.domain.v1.MonitorableValueProfile;
import ru.rubezh.firesec.nt.domain.v1.MonitorableValueProfileView;

@Service
public class MonitorableValueProfileService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    MonitorableValueProfileRepository monitorableValueProfileRepository;

    public void fetchValues(DriverProfileView driverProfileView) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, MonitorableValueProfile.class);
        /* Сначала удаляем старые версии состояний */
        for (MonitorableValueProfileView valueView: driverProfileView.getMonitorableValueProfileViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(valueView.getValue().getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии состояний */
        for (MonitorableValueProfileView valueView: driverProfileView.getMonitorableValueProfileViews())
            bulkOps.insert(valueView.getValue());
        bulkOps.execute();
    }

    public void remove(MonitorableValueProfile value) {
        monitorableValueProfileRepository.delete(value);
    }

}
