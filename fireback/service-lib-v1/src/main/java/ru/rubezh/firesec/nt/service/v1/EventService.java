package ru.rubezh.firesec.nt.service.v1;

import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.*;

public interface EventService {

    /**
     * Сгенерировать и зафиксировать событие (де)авторизации пользователя.
     * @param eventTypeId Идентификатор типа события.
     * @param user Пользователя.
     * @return Сущность зафиксированного события или null
     */
    Event commitEventByEventTypeIdAndUsername(String eventTypeId, User user);

    /**
     * Сгенерировать и зафиксировать событие прибора.
     * @param eventType Тип события.
     * @param controlDeviceId Идентификатор прибора.
     * @return Сущность зафиксированного события или null
     */
    Event commitEventByEventTypeAndControlDeviceId(EventType eventType, String controlDeviceId);

    /**
     * Сгенерировать и зафиксировать событие устройства.
     * @param eventType Тип события.
     * @param deviceId Идентификатор прибора.
     * @return Сущность зафиксированного события или null
     */
    Event commitEventByEventTypeAndDeviceId(EventType eventType, String deviceId);

    /**
     * Сгенерировать и зафиксировать событие устройства.
     * @param eventType Тип события.
     * @param issue Задача.
     * @param user Пользователь.
     * @return Сущность зафиксированного события или null
     */
    Event commitEventByEventTypeAndIssueAndUsername(EventType eventType, Issue issue, User user);

    void commitIssueEvent(Issue issue, User user, boolean success);

    void commitProjectEvent(ProjectStatus status, User user, boolean success);

    void addEventProfile(EventProfile eventProfile);

    void addEventProfiles(List<EventProfile> eventProfiles);

    void addEventTypes(List<EventType> eventTypes);

    void addDecodateEventStateSet(DecodableEventStateSet decodableEventStateSet);

    void addDecodableEventStateSets(List<DecodableEventStateSet> decodableEventStateSets);

    void removeEventProfile(EventProfile eventProfile);

    void removeEventProfiles(List<EventProfile> eventProfiles);

    void removeDecodableEventStateSet(DecodableEventStateSet decodableEventStateSet);

    void removeDecodableEventStateSets(List<DecodableEventStateSet> decodableEventStateSets);

    EventProfile getEventProfileById(String id);

    List<EventProfile> getEventProfilesByIdIn(List<String> ids);

    DecodableEventStateSet getDecodableEventStateSetById(String id);

    List<DecodableEventStateSet> getDecodableEventStatesByIdIn(List<String> ids);

}
