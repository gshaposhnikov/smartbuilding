package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent.*;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

import java.util.*;
import java.util.stream.Collectors;

/*
 * TODO: Реализовать write-behind кеширование для производительности.
 * Возможное решение:
 * https://apacheignite.readme.io/v1.4/docs/data-grid
 * https://github.com/gridgain/gridgain-advanced-examples/tree/master/src/main/java/org/gridgain/examples/datagrid/store
 * 
 */
@Service("eventService")
public class EventServiceImpl implements EventService, UpsertEntitiesProcessor {

    @Autowired
    private Logger logger;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private ActiveDeviceService activeDeviceService;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Autowired
    private ControlDeviceEventRepository controlDeviceEventRepository;

    private void putControlDeviceInfo(Event event, EventType eventType, ControlDeviceEvent controlDeviceEvent) {
        event.setControlDeviceId(controlDeviceEvent.getControlDeviceInfo().getId());
        if (eventType.isHavingControlDeviceStates()) {
            List<String> stateIds = controlDeviceEvent.getControlDeviceInfo().getActiveStateIds();
            if (stateIds != null && !stateIds.isEmpty()) {
                /* запоминаем состояния прибора, полученные с событием */
                event.getControlDeviceStateIds().addAll(stateIds);
            }
        }
        /* TODO: добавлять флаг о пожаротушении, если приборы могут сообщать */
    }

    private void putDeviceInfo(Event event, EventType eventType, EventDeviceInfo eventDeviceInfo, ActiveDevice device,
            DeviceProfile deviceProfile) {
        event.setDeviceId(device.getId());

        /* запоминаем состояния устройства, полученные с событием */
        if (eventDeviceInfo.getActiveStateIds() != null) {
            event.getActiveDeviceStateIds().addAll(eventDeviceInfo.getActiveStateIds());
        }

        /* заполняем данные о пожаротушении */
        if (eventType.getFireExtinctionEvent() != null && deviceProfile.isFireExtinctionDevice()) {
            event.setFireExtinctionEvent(eventType.getFireExtinctionEvent());
        }

    }

    private void putRegionInfo(Event event, EventRegionInfo eventRegionInfo, ActiveRegion region) {
        event.setRegionId(region.getId());
        /* запоминаем состояния зоны, полученные с событием */
        event.getActiveRegionStateIds().addAll(eventRegionInfo.getActiveStateIds());
    }

    private void putVirtualStateInfo(Event event, EventVirtualStateInfo eventVirtualStateInfo, EventType eventType,
            String activeProjectId, Map<Integer, VirtualState> virtualStateMap) {
        VirtualState virtualState = virtualStateMap.get(eventVirtualStateInfo.getGlobalNo());
        if (virtualState != null) {
            event.setVirtualStateId(virtualState.getId());
        } else {
            logger.warn("Virtual state with project id {} and global number {} not found",
                    activeProjectId, eventVirtualStateInfo.getGlobalNo());
        }
    }

    private void putScenarioInfo(Event event, EventScenarioInfo eventScenarioInfo, ActiveScenario scenario) {
        event.setScenarioId(scenario.getId());
        /* запоминаем состояния сценария, полученные с событием */
        event.getScenarioStateIds().addAll(eventScenarioInfo.getActiveStateIds());
    }

    private void putLineInfo(Event event, EventLineInfo eventLineInfo) {
        event.setLineNo(eventLineInfo.getLineNo());
    }

    private void putEmployeeInfo(Event event, Employee employee) {
        event.setEmployeeId(employee.getId());
    }

    private List<ControlDeviceEvent> checkAlreadySavedEvents(List<ControlDeviceEvent> eventInfos) {
        List<ControlDeviceEvent> needSaveEventInfos = new ArrayList<>();
        Map<String, Map<Long, ControlDeviceEvent>> eventInfosByCDIdByRecordNo = new HashMap<>();
        Set<Long> recordNosToFind = new HashSet<>();
        Set<String> controlDeviceIdsToFind = new HashSet<>();
        for (ControlDeviceEvent eventInfo: eventInfos) {
            String controlDeviceId = eventInfo.getControlDeviceInfo().getId();
            Map<Long, ControlDeviceEvent> eventInfosByRecordNo = eventInfosByCDIdByRecordNo.get(controlDeviceId);
            if (eventInfosByRecordNo == null) {
                eventInfosByRecordNo = new HashMap<>();
                eventInfosByCDIdByRecordNo.put(controlDeviceId, eventInfosByRecordNo);
            }
            eventInfosByRecordNo.put(eventInfo.getEventNo(), eventInfo);
            recordNosToFind.add(eventInfo.getEventNo());
            controlDeviceIdsToFind.add(controlDeviceId);
        }
        if (!controlDeviceIdsToFind.isEmpty()) {
            List<Event> savedEvents = eventRepository.findByControlDeviceIdInAndRecordNoIn(controlDeviceIdsToFind,
                    recordNosToFind);
            for (Event savedEvent: savedEvents) {
                String controlDeviceId = savedEvent.getControlDeviceId();
                Map<Long, ControlDeviceEvent> eventInfosByRecordNo = eventInfosByCDIdByRecordNo.get(controlDeviceId);
                if (eventInfosByRecordNo != null) {
                    ControlDeviceEvent eventInfo = eventInfosByRecordNo.get(savedEvent.getRecordNo());
                    if (eventInfo != null && savedEvent.getEventTypeId().equals(eventInfo.getTypeId())
                            && savedEvent.getOccurred().equals(eventInfo.getOccurredDateTime())) {
                        eventInfosByRecordNo.remove(savedEvent.getRecordNo());
                    }
                }
            }
            for (Map<Long, ControlDeviceEvent> eventInfosByRecordNo : eventInfosByCDIdByRecordNo.values()) {
                needSaveEventInfos.addAll(eventInfosByRecordNo.values());
            }
        }
        needSaveEventInfos.sort((eventA, eventB) -> {
            return eventA.getId().compareTo(eventB.getId());
        });
        return needSaveEventInfos;
    }

    @Override
    public Event commitEventByEventTypeIdAndUsername(String eventTypeId, User user) {
        /*
         * TODO: сделать, чтобы генерируемые события ложились в БД не раньше их предшественников, пришедших от
         * драйвера
         */

        Event event = new Event();
        event.setEventTypeId(eventTypeId);
        event.setSubsystem(Subsystem.GENERAL);
        if (user != null) {
            event.setUserId(user.getId());
            event.setUserName(user.getFullName());
        } else {
            event.setUserName("Неизвестный пользователь");
        }

        return eventRepository.insert(event);
    }

    @Override
    public Event commitEventByEventTypeAndControlDeviceId(EventType eventType, String controlDeviceId) {
        /*
         * TODO: сделать, чтобы генерируемые события ложились в БД не раньше их предшественников, пришедших от драйвера
         */

        Event event = new Event();
        rememberEventType(event, eventType);
        event.setControlDeviceId(controlDeviceId);

        return eventRepository.insert(event);
    }

    @Override
    public Event commitEventByEventTypeAndDeviceId(EventType eventType, String deviceId) {
        /*
         * TODO: сделать, чтобы генерируемые события ложились в БД не раньше их предшественников, пришедших от драйвера
         */

        Event event = new Event();
        rememberEventType(event, eventType);
        event.setDeviceId(deviceId);

        return eventRepository.insert(event);
    }

    private void rememberEventType(Event event, EventType eventType){
        event.setEventTypeId(eventType.getId());
        if (eventType.getSubsystem() == null) {
            event.setSubsystem(eventType.getCategory().getSubsystem());
        } else {
            event.setSubsystem(eventType.getSubsystem());
        }
    }

    @Override
    public Event commitEventByEventTypeAndIssueAndUsername(EventType eventType, Issue issue, User user){
        /*
         * TODO: сделать, чтобы генерируемые события ложились в БД не раньше их предшественников, пришедших от драйвера
         */

        Event event = new Event();
        rememberEventType(event, eventType);
        if (user != null) {
            event.setUserId(issue.getUserId());
            event.setUserName(user.getFullName());
        } else {
            event.setUserName("Неизвестный пользователь");
        }
        if (eventType.isHavingControlDeviceInfo()){
            event.setControlDeviceId(issue.getDeviceId());
        } else if (eventType.isHavingDeviceInfo()){
            event.setDeviceId(issue.getDeviceId());
        } else if (eventType.isHavingRegionInfo()) {
            event.setRegionId(issue.getParameters().get("regionId").toString());
        } else if (eventType.isHavingVirtualIndicatorInfo()){
            event.setIndicatorPanelId(issue.getParameters().get("indicatorPanelId").toString());
            event.setIndicatorColNo((int)issue.getParameters().get("colNo"));
            event.setIndicatorRowNo((int)issue.getParameters().get("rowNo"));
        }
        return eventRepository.insert(event);
    }

    @Override
    public void commitIssueEvent (Issue issue, User user, boolean success){
        try {
            if (issue.getAction() == IssueAction.RESET_STATE_ON_ALL_DEVICES
                    || issue.getAction() == IssueAction.RESET_STATE_ON_DEVICE) {
                String eventTypeId = null;
                ManualResetStateGroup manualResetStateGroup = ManualResetStateGroup
                        .valueOf((String) issue.getParameters().get("manualResetStateGroup"));
                switch (manualResetStateGroup) {
                    case TESTS:
                        eventTypeId = "resetTests";
                        break;
                    case SCENARIOS:
                        eventTypeId = "resetScenarios";
                        break;
                    case FIRE:
                        eventTypeId = "resetFire";
                        break;
                    case MALFUNCTIONS:
                        eventTypeId = "resetMalfunctions";
                        break;
                    case ALARM:
                        eventTypeId = "resetAlert";
                        break;
                    case LOCK:
                        eventTypeId = "resetLock";
                        break;

                default:
                    throw new Exception(
                            "Неизвестная группа сбрасываемых состояний: " + manualResetStateGroup.toString());
                }
                eventTypeId += (success) ? "Success" : "Failure";
                commitEventByEventTypeIdAndUsername(eventTypeId, user);
            } else if (issue.getAction() == IssueAction.PERFORM_DEVICE_ACTION){
                EventType eventType = eventTypeRepository.findOneByDeviceActionKeyAndIssueAction(
                        issue.getParameters().get("actionId").toString(),
                        issue.getAction());
                if (eventType != null) {
                    commitEventByEventTypeAndDeviceId(eventType, issue.getDeviceId());
                }
            } else {
                List<EventType> eventTypes = eventTypeRepository.findByIssueAction(issue.getAction());
                for (EventType eventType: eventTypes) {
                    if ((success && eventType.getId().contains("Success"))
                            || (!success && eventType.getId().contains("Failure"))) {
                        commitEventByEventTypeAndIssueAndUsername(eventType, issue, user);
                        break;
                    }
                }
            }
        } catch (Exception ex){
            logger.error("Failed to commit an event: ", ex);
        }
    }

    @Override
    public void commitProjectEvent(ProjectStatus status, User user, boolean success) {
        try {
            String eventTypeId = (status == ProjectStatus.ACTIVE) ? "setProjectActive" : "setProjectBuild";
            eventTypeId += (success) ? "Success" : "Failure";
            commitEventByEventTypeIdAndUsername(eventTypeId, user);
        } catch (Exception ex){
            logger.error("Failed to commit an event: ", ex);
        }
    }

    @Override
    public void addEventProfile(EventProfile eventProfile) {
        BulkOperations eventTypeBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventType.class);
        eventTypeBulk.insert(eventProfile.getMainEventType());
        eventTypeBulk.insert(eventProfile.getEventSubTypes());
        eventTypeBulk.execute();
        mongoTemplate.save(eventProfile);
    }

    @Override
    public void addEventProfiles(List<EventProfile> eventProfiles) {
        BulkOperations eventTypeBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventType.class);
        BulkOperations eventProfileBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventProfile.class);
        /* Сначала удаляем старые версии профилей и типов событий */
        for (EventProfile eventProfile : eventProfiles) {
            eventTypeBulk.remove(new Query(Criteria.where("_id").is(eventProfile.getMainEventType().getId())));
            for (EventType eventSubType: eventProfile.getEventSubTypes()) {
                eventTypeBulk.remove(new Query(Criteria.where("_id").is(eventSubType.getId())));
            }
            eventProfileBulk.remove(new Query(Criteria.where("_id").is(eventProfile.getId())));
        }
        eventTypeBulk.execute();
        eventProfileBulk.execute();
        /* Теперь сохраняем новые версии профилей и типов событий */
        for (EventProfile eventProfile : eventProfiles) {
            eventTypeBulk.insert(eventProfile.getMainEventType());
            eventTypeBulk.insert(eventProfile.getEventSubTypes());
            eventProfileBulk.insert(eventProfile);
        }
        eventTypeBulk.execute();
        eventProfileBulk.execute();
    }

    @Override
    public void addEventTypes(List<EventType> eventTypes) {
        BulkOperations eventTypeBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventType.class);
        /* Удаляем старые */
        for (EventType eventType : eventTypes) {
            eventTypeBulk.remove(new Query(Criteria.where("_id").is(eventType.getId())));
        }
        eventTypeBulk.execute();
        /* Сохраняем новые */
        eventTypeBulk.insert(eventTypes);
        eventTypeBulk.execute();
    }

    @Override
    public void addDecodateEventStateSet(DecodableEventStateSet decodableEventStateSet) {
        mongoTemplate.save(decodableEventStateSet);
    }

    @Override
    public void addDecodableEventStateSets(List<DecodableEventStateSet> decodableEventStateSets) {
        BulkOperations decodableEventStateSetBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, DecodableEventStateSet.class);
        /* Сначала удаляем старые версии наборов состояний */
        for (DecodableEventStateSet stateSet: decodableEventStateSets)
            decodableEventStateSetBulk.remove(new Query(Criteria.where("_id").is(stateSet.getId())));
        decodableEventStateSetBulk.execute();
        /* Теперь сохраняем новые версии наборов состояний */
        decodableEventStateSetBulk.insert(decodableEventStateSets);
        decodableEventStateSetBulk.execute();
    }

    @Override
    public void removeEventProfile(EventProfile eventProfile) {
        BulkOperations eventTypeBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventType.class);
        eventTypeBulk.remove(Query.query(Criteria.where("_id").is(eventProfile.getMainEventType().getId())));
        for (EventType eventType : eventProfile.getEventSubTypes())
            eventTypeBulk.remove(Query.query(Criteria.where("_id").is(eventType.getId())));
        eventTypeBulk.execute();
        mongoTemplate.remove(eventProfile);
    }

    @Override
    public void removeEventProfiles(List<EventProfile> eventProfiles) {
        BulkOperations eventTypeBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventType.class);
        BulkOperations eventProfileBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventProfile.class);
        for (EventProfile eventProfile : eventProfiles) {
            eventTypeBulk.remove(Query.query(Criteria.where("_id").is(eventProfile.getMainEventType().getId())));
            for (EventType eventType : eventProfile.getEventSubTypes())
                eventTypeBulk.remove(Query.query(Criteria.where("_id").is(eventType.getId())));
            eventProfileBulk.remove(Query.query(Criteria.where("_id").is(eventProfile.getId())));
        }
        eventTypeBulk.execute();
        eventProfileBulk.execute();
    }

    @Override
    public void removeDecodableEventStateSet(DecodableEventStateSet decodableEventStateSet) {
        mongoTemplate.remove(decodableEventStateSet);
    }

    @Override
    public void removeDecodableEventStateSets(List<DecodableEventStateSet> decodableEventStateSets) {
        BulkOperations decodableEventStateSetBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED,
                DecodableEventStateSet.class);
        for (DecodableEventStateSet decodableEventStateSet : decodableEventStateSets)
            decodableEventStateSetBulk.remove(Query.query(Criteria.where("_id").is(decodableEventStateSet.getId())));
        decodableEventStateSetBulk.execute();
    }

    @Override
    public EventProfile getEventProfileById(String id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), EventProfile.class);
    }

    @Override
    public List<EventProfile> getEventProfilesByIdIn(List<String> ids) {
        return mongoTemplate.find(Query.query(Criteria.where("_id").in(ids)), EventProfile.class);
    }

    @Override
    public DecodableEventStateSet getDecodableEventStateSetById(String id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), DecodableEventStateSet.class);
    }

    @Override
    public List<DecodableEventStateSet> getDecodableEventStatesByIdIn(List<String> ids) {
        return mongoTemplate.find(Query.query(Criteria.where("_id").in(ids)), DecodableEventStateSet.class);
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {
        String activeProjectId = activeProject.getId();

        List<ControlDeviceEvent> controlDeviceEvents = controlDeviceEventRepository.findFirst100ByOrderByIdAsc();
        if (controlDeviceEvents.isEmpty())
            return observeFromOid;
        controlDeviceEventRepository.delete(controlDeviceEvents);

        List<ControlDeviceEvent> eventInfos = checkAlreadySavedEvents(controlDeviceEvents);
        if (eventInfos.isEmpty())
            return observeFromOid;

        /* выберем все необходимые типы событий */
        List<String> eventTypeIds = eventInfos.stream().map(ControlDeviceEvent::getTypeId).collect(Collectors.toList());
        Map<String, EventType> eventTypeMap = eventTypeRepository.findAllByIdIn(eventTypeIds).stream()
                .collect(Collectors.toMap(EventType::getId, eventType -> eventType));

        /* выберем все необходимые приборы */
        List<String> deviceIds = eventInfos.stream().map(ControlDeviceEvent::getControlDeviceInfo)
                .map(ControlDeviceEvent.EventControlDeviceInfo::getId).collect(Collectors.toList());
        Map<String, ActiveDevice> activeDeviceMap = activeDeviceRepository
                .findAllByProjectIdAndIdIn(activeProjectId, deviceIds).stream()
                .collect(Collectors.toMap(ActiveDevice::getId, device -> device));

        /* выберем все необходимые сценарии */
        List<Integer> scenarioNos = eventInfos.stream().map(ControlDeviceEvent::getScenarioInfo)
                .filter(Objects::nonNull).map(EventScenarioInfo::getGlobalNo).collect(Collectors.toList());
        Map<Integer, ActiveScenario> scenarioMap = activeScenarioRepository
                .findAllByProjectIdAndGlobalNoIn(activeProjectId, scenarioNos).stream()
                .collect(Collectors.toMap(ActiveScenario::getGlobalNo, scenario -> scenario));

        /* выберем все необходимые вирт. состояния */
        List<Integer> virtualStateNos = eventInfos.stream().map(ControlDeviceEvent::getVirtualStateInfo)
                .filter(Objects::nonNull).map(EventVirtualStateInfo::getGlobalNo).collect(Collectors.toList());
        Map<Integer, VirtualState> virtualStateMap = virtualStateRepository
                .findAllByProjectIdAndGlobalNoIn(activeProjectId, virtualStateNos).stream()
                .collect(Collectors.toMap(VirtualState::getGlobalNo, virtualState -> virtualState));

        /* выберем все необходимые активные зоны */
        List<Integer> activeRegionNos = eventInfos.stream().map(ControlDeviceEvent::getRegionInfo)
                .filter(Objects::nonNull).map(EventRegionInfo::getGlobalNo).collect(Collectors.toList());
        Map<Integer, ActiveRegion> activeRegionMap = activeRegionRepository
                .findAllByProjectIdAndIndexIn(activeProjectId, activeRegionNos).stream()
                .collect(Collectors.toMap(ActiveRegion::getIndex, activeRegion -> activeRegion));

        List<Integer> employeeIds = eventInfos.stream().map(ControlDeviceEvent::getEmployeeInfo)
                .filter(Objects::nonNull).map(EventEmployeeInfo::getEmployeeId).collect(Collectors.toList());
        Map<Integer, Employee> employeeMap = employeeRepository
                .findAllByProjectIdAndIndexInAndRemovedIsFalse(activeProjectId, employeeIds).stream()
                .collect(Collectors.toMap(Employee::getIndex, employee -> employee));

        List<Event> events = new ArrayList<>();
        for (ControlDeviceEvent eventInfo : eventInfos) {
            EventType eventType = eventTypeMap.get(eventInfo.getTypeId());
            if (eventType == null) {
                logger.warn("Event type '{}' not found", eventInfo.getTypeId());
                continue;
            }

            ActiveDevice sourceControlDevice = activeDeviceMap.get(eventInfo.getControlDeviceInfo().getId());
            if (sourceControlDevice == null) {
                logger.warn("Control device '{}' not found", eventInfo.getControlDeviceInfo().getId());
                continue;
            }

            Event event = new Event();
            /* запоминаем тип события */
            rememberEventType(event, eventType);
            /* запоминаем информацию по прибору, с которого получено событие */
            putControlDeviceInfo(event, eventType, eventInfo);
            /* запоминаем идентификатор журнала */
            event.setLogTypeId(eventInfo.getLogTypeId());
            /* запоминаем времена */
            event.setOccurred(eventInfo.getOccurredDateTime());
            event.setReceived(eventInfo.getReceivedDateTime());
            /* номер записи в журнале */
            event.setRecordNo(eventInfo.getEventNo());
            /* режим получения события */
            event.setOnline(eventInfo.isOnline());

            ActiveDevice addressDevice = null;
            DeviceProfile addressDeviceProfile = null;
            EventDeviceInfo eventDeviceInfo = null;
            ActiveRegion region = null;
            ActiveScenario scenario = null;

            /* обрабатываем информацию по устройству, если она есть */
            if (eventType.isHavingDeviceInfo()) {
                eventDeviceInfo = eventInfo.getDeviceInfo();
                if (eventDeviceInfo != null) {
                    /* TODO: реализовать групповое получение дочерних устройств и их профилей */
                    addressDevice = activeDeviceService.getChildDevice(sourceControlDevice.getDeviceProject(),
                            eventDeviceInfo.getControlDeviceAddress(), eventDeviceInfo.getLineNo(),
                            eventDeviceInfo.getAddress());
                    if (addressDevice != null) {
                        addressDeviceProfile = deviceProfileRepository
                                .findById(addressDevice.getDeviceProject().getDeviceProfileId());
                        if (addressDeviceProfile != null) {
                            putDeviceInfo(event, eventType, eventDeviceInfo, addressDevice, addressDeviceProfile);
                        } else {
                            logger.error("Device profile not found, device address path: {}",
                                    addressDevice.getAddressPath());
                        }
                    }
                } else {
                    logger.warn("Event must have device information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            /* обрабатываем информацию по зоне, если она есть */
            if (eventType.isHavingRegionInfo()) {
                EventRegionInfo eventRegionInfo = eventInfo.getRegionInfo();
                if (eventInfo.getRegionInfo() != null) {
                    region = activeRegionMap.get(eventRegionInfo.getGlobalNo());
                    if (region != null) {
                        putRegionInfo(event, eventRegionInfo, region);
                    } else {
                        logger.warn("Active region with project id {} and global number {} not found",
                                activeProjectId, eventRegionInfo.getGlobalNo());
                    }
                } else {
                    logger.warn("Event must have region information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            /* обрабатываем информацию о вирт. состоянии, если она есть */
            if (eventType.isHavingVirtualStateInfo()) {
                EventVirtualStateInfo eventVirtualStateInfo = eventInfo.getVirtualStateInfo();
                if (eventVirtualStateInfo != null) {
                    putVirtualStateInfo(event, eventVirtualStateInfo, eventType, activeProjectId, virtualStateMap);
                } else {
                    logger.warn(
                            "Event must have virtual state information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            /* обрабатываем информацию по сценарию, если она есть */
            if (eventType.isHavingScenarioInfo()) {
                EventScenarioInfo eventScenarioInfo = eventInfo.getScenarioInfo();
                if (eventScenarioInfo != null) {
                    scenario = scenarioMap.get(eventScenarioInfo.getGlobalNo());
                    if (scenario != null) {
                        putScenarioInfo(event, eventScenarioInfo, scenario);
                    } else {
                        logger.warn("Scenario with project id {} and global number {} not found", activeProjectId,
                                eventScenarioInfo.getGlobalNo());
                    }
                } else {
                    logger.warn("Event must have scenario information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            /* обрабатываем информацию по линии, если она есть */
            if (eventType.isHavingLineInfo()) {
                EventLineInfo eventLineInfo = eventInfo.getLineInfo();
                if (eventLineInfo != null) {
                    putLineInfo(event, eventLineInfo);
                } else {
                    logger.warn("Event must have line information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            /* обрабатываем информацию по сотруднику, если она есть */
            if (eventType.isHavingEmployeeInfo()) {
                EventEmployeeInfo eventEmployeeInfo = eventInfo.getEmployeeInfo();
                if (eventEmployeeInfo != null) {
                    Employee employee = employeeMap.get(eventEmployeeInfo.getEmployeeId());
                    if (employee != null) {
                        putEmployeeInfo(event, employee);
                    } else {
                        logger.warn("Employee with project id {} and id {} not found", activeProjectId,
                                eventEmployeeInfo.getEmployeeId());
                    }
                } else {
                    logger.warn("Event must have employee information but it doesn't (event type: {}, record no: {})",
                            eventType.getId(), event.getRecordNo());
                }
            }

            events.add(event);

            if (eventInfo.isOnline()) {
                /* запоминаем номер последней считанной записи журнала в приборе - источнике события */
                /*
                 * TODO: сделать групповое и/или перенести в отдельный
                 * upsert-процессор
                 */
                activeDeviceService.updateEventCount(sourceControlDevice.getId(), eventInfo.getLogTypeId(),
                        event.getRecordNo());
            }
        }

        if (!events.isEmpty())
            eventRepository.insert(events);

        return new ObjectId(currentTime, 0);// Данный процессор не использует минимальную метку времени, поэтому можно
                                         // вернуть любое значение.
    }

}
