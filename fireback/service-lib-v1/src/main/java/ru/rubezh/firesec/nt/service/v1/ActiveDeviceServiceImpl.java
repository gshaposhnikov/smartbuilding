package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service("activeDeviceService")
public class ActiveDeviceServiceImpl extends ActiveEntityWithStatesServiceImpl
        implements ActiveDeviceService, UpsertEntitiesProcessor {

    @Autowired
    private Logger logger;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private LogMonitorableValueService logMonitorableValueService;

    @Autowired
    private ActiveScenarioService activeScenarioService;

    /** Кол-во устройств, за раз извлекаемое из базы при создании активных сущностей */
    private static final int ACTIVE_DEVICES_SAFE_PAGE_SIZE = 3000;

    @Override
    public void createActiveEntities(String projectId, Date createDateTime) {
        StateCategory defaultCategory = getDefaultStateCategory();
        long nPages = (deviceRepository.countByProjectIdAndDisabledIsFalse(projectId)
                / ACTIVE_DEVICES_SAFE_PAGE_SIZE) + 1;
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, ActiveDevice.class);
        for (int pageIndex = 0; pageIndex < nPages; pageIndex++) {
            Pageable pageable = new PageRequest(pageIndex, ACTIVE_DEVICES_SAFE_PAGE_SIZE);
            List<Device> devices = deviceRepository.findByProjectIdAndDisabledIsFalse(projectId, pageable);
            if (!devices.isEmpty()) {
                List<String> deviceProfileIds = new ArrayList<>();
                devices.forEach(device -> deviceProfileIds.add(device.getDeviceProfileId()));
                Map<String, DeviceProfile> deviceProfilesMap = deviceProfileRepository.findByIdIn(deviceProfileIds)
                        .stream()
                        .collect(Collectors.toMap(DeviceProfile::getId, Function.identity()));
                for (Device device : devices) {
                    DeviceProfile deviceProfile = deviceProfilesMap.get(device.getDeviceProfileId());
                    ActiveDevice activeDevice = new ActiveDevice(device, deviceProfile);
                    activeDevice.setGeneralStateCategoryId(defaultCategory.getId());
                    activeDevice.setStateGetDateTime(createDateTime);
                    if (deviceProfile.isFireExtinctionDevice())
                        activeDevice.setFireExtinctionDelay(getFireExtinctionDelay(device, deviceProfile));
                    bulkOperations.insert(activeDevice);
                }
                bulkOperations.execute();
            }
        }
    }

    @Override
    public void destroyActiveEntities() {
        activeDeviceRepository.deleteAll();
    }

    private List<ActiveDevice> getAllChildDevices(ActiveDevice parentDevice) {
        List<ActiveDevice> childDevices = activeDeviceRepository
                .findByProjectIdAndParentDeviceId(parentDevice.getProjectId(), parentDevice.getId());
        List<ActiveDevice> subChildDevices = new ArrayList<>();
        for (ActiveDevice childDevice : childDevices) {
            List<ActiveDevice> nextChildDevices = getAllChildDevices(childDevice);
            if (!nextChildDevices.isEmpty()) {
                subChildDevices.addAll(getAllChildDevices(childDevice));
            }
        }
        childDevices.addAll(subChildDevices);
        return childDevices;
    }

    private void resetControlDeviceChildrenStates(ActiveDevice parentDevice, List<ActiveDevice> childDevices, State state,
            Map<String, ActiveDevice> updatedDevices) {

        if (parentDevice.getDeviceProject().getDeviceCategory() != DeviceCategory.CONTROL)
            return;
        if (state.getControlAutoResetStateIds().isEmpty())
            return;

        for (ActiveDevice childDevice : childDevices) {
            if (childDevice.getActiveStates().removeAll(state.getControlAutoResetStateIds())) {
                updatedDevices.put(childDevice.getId(), childDevice);
            }
        }
    }

    private void updateChildrenInheritedStates(ActiveDevice parentDevice, List<ActiveDevice> childDevices,
            Map<String, ActiveDevice> updatedDevices) {
        if (childDevices.isEmpty())
            return;
        DeviceProfile parentDeviceProfile = deviceProfileRepository
                .findOne(parentDevice.getDeviceProject().getDeviceProfileId());
        if (parentDeviceProfile == null)
            return;

        if (parentDeviceProfile.getStateInheritance() == null || parentDeviceProfile.getStateInheritance().isEmpty())
            return;

        for (ActiveDevice device : childDevices) {
            /* Формируем множество актуальных унаследованных состояний */
            Set<String> inheritedStateIds = new HashSet<>();
            for (String stateId : parentDevice.getActiveStates()) {
                String inheritedStateId = parentDeviceProfile.getStateInheritance().get(stateId);
                if (inheritedStateId != null) {
                    inheritedStateIds.add(inheritedStateId);
                }
            }

            /* Применяем новое множество актуальных унаследованных состояний */
            setInheritedActiveStates(device, parentDevice.getId(), inheritedStateIds, false);
            if (device.isNeedsStateCommit())
                updatedDevices.put(device.getId(), device);
        }
    }

    private void commitAfterStateUpdated(Collection<ActiveDevice> updatedDevices) {

        Date currentTime = new Date();
        StateCategory defaultCategory = getDefaultStateCategory();
        Map<String, State> statesHash = new HashMap<>();

        /* Коммит устройств с одним обращением к БД на запись */
        for (ActiveDevice updatedDevice : updatedDevices) {
            updatedDevice.setStateGetDateTime(currentTime);
            updateStateCategory(updatedDevice, defaultCategory, statesHash);
        }
        updatedDevices = activeDeviceRepository.save(updatedDevices);
    }

    private void generateEventsByState(ActiveDevice device, State state, boolean isSetState) {
        Collection<String> eventTypeIds = isSetState ?
                state.getEnterEventTypeIds() : state.getLeaveEventTypeIds();

        for (String eventTypeId: eventTypeIds) {
            EventType eventType = eventTypeRepository.findOne(eventTypeId);
            if (eventType == null) {
                logger.error("Event type not found (id: {})", eventTypeId);
            } else if (eventType.isHavingControlDeviceInfo() &&
                    device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTROL) {
                eventService.commitEventByEventTypeAndControlDeviceId(eventType, device.getId());
            } else if (eventType.isHavingDeviceInfo()) {
                eventService.commitEventByEventTypeAndDeviceId(eventType, device.getId());
            }
        }
    }

    private void applyActiveStateToScenarios(ActiveDevice device, String stateId, boolean actionIsSet) {
        if (device.getDeviceProject().getDeviceCategory() != DeviceCategory.CONTROL)
            return;
        List<String> scenarioIds = deviceService.findDeviceTreeScenarioIds(device.getDeviceProject());
        for (String scenarioId : scenarioIds) {
            if (actionIsSet)
                activeScenarioService.addActiveState(scenarioId, stateId);
            else
                activeScenarioService.removeActiveState(scenarioId, stateId);
        }
    }

    @Override
    public void addActiveState(String deviceId, String stateId) {
        ActiveDevice device = activeDeviceRepository.findOne(deviceId);
        if (device != null) {
            addActiveState(device, stateId);
        }
        else
            logger.warn("Active device (id: {}) not found", deviceId);
    }

    @Override
    public void addActiveState(ActiveDevice device, String stateId) {
        if (device.getActiveStates().add(stateId)) {
            State state = stateRepository.findOne(stateId);
            if (state != null) {
                generateEventsByState(device, state, true);
                if (state.isApplicableToScenarios())
                    applyActiveStateToScenarios(device, stateId, true);

                List<ActiveDevice> childDevices = getAllChildDevices(device);
                if (!childDevices.isEmpty()) {
                    Map<String, ActiveDevice> updatedDevices = new HashMap<>();
                    /* Наследование состояний потомками */
                    updateChildrenInheritedStates(device, childDevices, updatedDevices);
                    /* Добавляем родителя для коммита */
                    updatedDevices.put(device.getId(), device);
                    /* Коммит всех измененных устройств */
                    commitAfterStateUpdated(updatedDevices.values());
                } else {
                    /* Коммит одного устройства (без потомков) */
                    commitAfterStateUpdated(device, true);
                }
            } else
                logger.warn("State (id: {}) not found", stateId);
        }
    }

    @Override
    public void removeActiveState(String deviceId, String stateId) {
        ActiveDevice device = activeDeviceRepository.findOne(deviceId);
        if (device != null) {
            removeActiveState(device, stateId);
        }
        else
            logger.warn("Active device (id: {}) not found", deviceId);
    }

    @Override
    public void removeActiveState(ActiveDevice device, String stateId) {
        if (device.getActiveStates().remove(stateId)) {
            State state = stateRepository.findOne(stateId);
            if (state != null) {
                generateEventsByState(device, state, false);
                if (state.isApplicableToScenarios())
                    applyActiveStateToScenarios(device, stateId, false);

                List<ActiveDevice> childDevices = getAllChildDevices(device);
                if (!childDevices.isEmpty()) {
                    Map<String, ActiveDevice> updatedDevices = new HashMap<>();
                    /* Наследование и сброс состояний потомками */
                    resetControlDeviceChildrenStates(device, childDevices, state, updatedDevices);
                    updateChildrenInheritedStates(device, childDevices, updatedDevices);
                    /* Добавляем родителя для коммита */
                    updatedDevices.put(device.getId(), device);
                    /* Коммит всех измененных устройств */
                    commitAfterStateUpdated(updatedDevices.values());
                } else {
                    /* Коммит одного устройства (без потомков) */
                    commitAfterStateUpdated(device, true);
                }
            } else
                logger.warn("State (id: {}) not found", stateId);
        }
    }

    @Override
    public void setActivePollingStates(String deviceId, Collection<String> stateIds) {
        ActiveDevice device = activeDeviceRepository.findOne(deviceId);
        setActivePollingStates(device, stateIds);
    }

    @Override
    public void setActivePollingStates(ActiveDevice device, Collection<String> stateIds) {
        if (device != null) {
            Set<String> newStateIdsSet = new HashSet<String>(stateIds);
            if (!device.getActiveStates().equals(newStateIdsSet)) {
                List<State> currentDeviceStates = stateRepository.findByIdIn(device.getActiveStates());
                List<ActiveDevice> childDevices = getAllChildDevices(device);
                Map<String, ActiveDevice> updatedDevices = new HashMap<>();
                for (State state : currentDeviceStates) {
                    if (!newStateIdsSet.contains(state.getId())) {
                        if (state.isEventReason() || state.isLogic()) {
                            /*
                             * Если состояние является причиной события или сгенерировано логикой, то его необходимо
                             * оставить
                             */
                            newStateIdsSet.add(state.getId());
                        } else {
                            /* Автогенерация событий по удалению состояния */
                            generateEventsByState(device, state, false);
                            /* Удаление состояния у подключенных устройств при удалении оного на приборе */
                            if (!childDevices.isEmpty())
                                resetControlDeviceChildrenStates(device, childDevices, state, updatedDevices);
                            /* Удаление состояния в сценариях */
                            if (state.isApplicableToScenarios())
                                applyActiveStateToScenarios(device, state.getId(), false);
                        }
                    }
                }
                if (!device.getActiveStates().equals(newStateIdsSet)) {
                    for (String stateId : newStateIdsSet) {
                        if (!device.getActiveStates().contains(stateId)) {
                            State addedState = stateRepository.findOne(stateId);
                            /* Автогенерация событий по добавлению состояния */
                            generateEventsByState(device, addedState, true);
                            /* Добавление состояния в сценарии */
                            if (addedState.isApplicableToScenarios())
                                applyActiveStateToScenarios(device, stateId, true);
                        }
                    }
                    device.setActiveStates(newStateIdsSet);
                    if (!childDevices.isEmpty()) {
                        /* Наследование состояний потомками */
                        updateChildrenInheritedStates(device, childDevices, updatedDevices);
                        /* Добавляем родителя для коммита */
                        updatedDevices.put(device.getId(), device);
                        /* Коммит всех измененных устройств */
                        commitAfterStateUpdated(updatedDevices.values());
                    } else {
                        commitAfterStateUpdated(device, true);
                    }
                }
            }
        }
        else
            logger.warn("Active device (id: {}) not found", device.getId());
    }

    @Override
    public void updateEventCount(String deviceId, String logTypeId, long count) {
        ActiveDevice device = activeDeviceRepository.findOne(deviceId);
        if (device != null) {
            if (!device.getEventCounts().containsKey(logTypeId) || device.getEventCounts().get(logTypeId) != count) {
                device.getEventCounts().put(logTypeId, count);
                activeDeviceRepository.save(device);
            }
        }
        else
            logger.warn("Active device (id: {}) not found", deviceId);
    }

    @Override
    public ActiveDevice getChildDevice(Device controlDevice,
            int controlDeviceAddress, int deviceLineNo, int deviceAddress) {
        // адрес совпал - значит искать прибор не нужно, он у нас уже есть
        if (controlDevice.getLineAddress() == controlDeviceAddress)
            return getChildDevice(controlDevice, deviceLineNo, deviceAddress);
        // поиск внешнего прибора по адресу (на той же линии того же родителя)
        Device externalControlDevice = deviceRepository.findByProjectIdAndParentDeviceIdAndLineNoAndLineAddress(
                controlDevice.getProjectId(), controlDevice.getParentDeviceId(),
                deviceLineNo, controlDeviceAddress);
        if (externalControlDevice != null)
            return getChildDevice(externalControlDevice, deviceLineNo, deviceAddress);
        else {
            logger.warn(
                    "Child device not found. Source control device address path: {},"
                    + " real control device address: {}, line no: {}, child device address: {}",
                    controlDevice.getAddressPath(), controlDeviceAddress, deviceLineNo, deviceAddress);
            return null;
        }
    }

    private List<Device> getChildContainers(Device parentDevice, int lineNo) {
        /* ищем контейнеры на указанной линии */
        List<Device> devices = deviceRepository.findByProjectIdAndParentDeviceIdAndLineNoAndDeviceCategory(
                parentDevice.getProjectId(), parentDevice.getId(), lineNo, DeviceCategory.CONTAINER);
        return devices;
    }

    private ActiveDevice getChildDevice(Device controlDevice, int deviceLineNo, int deviceAddress) {
        // окончание строки для поиска устройства
        String pathEnding = "," + Integer.toString(deviceLineNo) + "," + Integer.toString(deviceAddress);
        // сначала предполагаем, что устройство подключено к корневому устройству
        String pathBeginning = controlDevice.getAddressPath();
        ActiveDevice device = activeDeviceRepository.findOneByAddressPath(pathBeginning + pathEnding);
        if (device == null) {
            // устройство может быть подключено к контейнеру (номер линии у контейнера тот же)
            for (Device deviceContainer: getChildContainers(controlDevice, deviceLineNo)) {
                pathBeginning = deviceContainer.getAddressPath();
                device = activeDeviceRepository.findOneByAddressPath(pathBeginning + pathEnding);
                if (device != null)
                    break;
            }
        } else if (device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTAINER) {
            // если найден контейнер с теми же линией/адресом - ищем устройство в нём
            pathBeginning = device.getAddressPath();
            device = activeDeviceRepository.findOneByAddressPath(pathBeginning + pathEnding);
        }
        return device;
    }

    @Override
    public void addActiveStates(BasicActiveEntityWithStates entity, Collection<String> activeStateIds, boolean commit) {
        super.addActiveStates(entity, getOnlyValidDeviceStateIds((ActiveDevice)entity, activeStateIds), commit);
    }

    protected List<String> getOnlyValidDeviceStateIds(ActiveDevice device, Collection<String> states) {
        String deviceProfileId = device.getDeviceProject().getDeviceProfileId();
        DeviceProfile deviceProfile = deviceProfileRepository.findById(deviceProfileId);

        List<String> validStates = deviceProfile.getValidDeviceStateIds();
        validStates.retainAll(states);

        return validStates;
    }

    @Override
    public ActiveRegion getContainingActiveRegion(ActiveDevice device) {
        if (device.getDeviceProject().getRegionId() != null) {
            return activeRegionRepository.findByProjectIdAndId(device.getProjectId(), device.getRegionId());
        }
        return null;
    }

    @Override
    public boolean setDeviceGuid(String deviceId, String guid) {
        boolean controlDeviceReplaced = false;
        ActiveDevice device = activeDeviceRepository.findOne(deviceId);
        if (device == null) {
            logger.warn("Active device (id: {}) not found", deviceId);
        } else if (!device.getGuid().equals(guid)) {
            if (!device.getGuid().isEmpty() && device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTROL)
                controlDeviceReplaced = true;
            device.setGuid(guid);
            activeDeviceRepository.save(device);
        }
        return controlDeviceReplaced;
    }

    @Override
    public void setDeviceActiveConfig(String deviceId, Map<String, String> propertyValues,
            String serialNumber, String firmwareVersion, String databaseVersion, Date readDateTime, Date writeDateTime) {
        ActiveDevice activeDevice = activeDeviceRepository.findOne(deviceId);
        if (activeDevice != null) {
            DeviceProfile deviceProfile = deviceProfileRepository
                    .findOne(activeDevice.getDeviceProject().getDeviceProfileId());
            if (deviceProfile != null) {
                Map<String, String> currentPropertyValues = activeDevice.getActiveConfigValues();
                Map<String, String> newPropertyValues = new HashMap<>();
                for (String propertyId : deviceProfile.getConfigProperties().keySet()) {
                    String propertyValue = null;
                    if (propertyValues != null && propertyValues.containsKey(propertyId))
                        propertyValue = propertyValues.get(propertyId);
                    if ((propertyValue == null) && currentPropertyValues.containsKey(propertyId))
                        propertyValue = currentPropertyValues.get(propertyId);
                    newPropertyValues.put(propertyId, propertyValue);
                }

                // read only параметры (не null только при чтении)
                if (serialNumber != null)
                    newPropertyValues.put(DeviceProperties.SerialNumber.toString(), serialNumber);
                if (firmwareVersion != null)
                    newPropertyValues.put(DeviceProperties.FirmwareVersion.toString(), firmwareVersion);
                if (databaseVersion != null)
                    newPropertyValues.put(DeviceProperties.DatabaseVersion.toString(), databaseVersion);

                // дата считвания/записи (соответствующее поле не null)
                if (readDateTime != null)
                    newPropertyValues.put(DeviceProperties.LastReadDateTime.toString(), DeviceProperty.getDate(readDateTime));
                if (writeDateTime != null)
                    newPropertyValues.put(DeviceProperties.LastWriteDateTime.toString(), DeviceProperty.getDate(writeDateTime));

                activeDevice.setActiveConfigValues(newPropertyValues);
                activeDeviceRepository.save(activeDevice);
            } else {
                logger.warn("Device profile (id: {}) not found", activeDevice.getDeviceProject().getDeviceProfileId());
            }
        } else {
            logger.warn("Active device (id: {}) not found", deviceId);
        }
    }

    @Override
    public void setMonitorableValues(SetActiveDeviceMonitorableValues message) {
        ActiveDevice activeDevice;
        if (message.getDeviceId() != null) {
            activeDevice = activeDeviceRepository.findOne(message.getDeviceId());
        } else if (message.getRegionNo() != null && message.getDeviceType() != null && message.getDeviceNo() != null){
            ActiveRegion region = activeRegionRepository.findByIndex(Integer.parseInt(message.getRegionNo()));
            if (region == null){
                return;
            }
            DeviceProfile deviceProfile = deviceProfileRepository.findByDeviceType(message.getDeviceType());
            if (deviceProfile == null){
                return;
            }
            Device device = deviceRepository.findByRegionIdAndDeviceProfileIdAndLineAddress(region.getId(),
                    deviceProfile.getId(), Integer.parseInt(message.getDeviceNo()));
            if (device == null) {
                return;
            }
            activeDevice = activeDeviceRepository.findOne(device.getId());

        } else {
            // NOTE: наблюдаемые параметры читаются по запросу - значит ID устройства всегда известен
            ActiveDevice sourceDevice = activeDeviceRepository.findOne(message.getControlDeviceId());
            activeDevice = getChildDevice(
                    sourceDevice.getDeviceProject(),
                    message.getControlDeviceAddress(),
                    message.getLineNo(),
                    message.getAddress());
        }

        if (activeDevice != null && !activeDevice.getMonitorableValues().equals(message.getMonitorableValues())) {
            Date currentDate = new Date();
            activeDevice.setMonitorableValuesTimestamp(currentDate);
            activeDevice.setMonitorableValues(message.getMonitorableValues());
            super.commitAfterStateUpdated(activeDevice, true);

            logMonitorableValueService.saveAll(activeDevice, message.getMonitorableValues(), currentDate, false);
        }
    }

    private int getFireExtinctionDelay(Device deviceProject, DeviceProfile deviceProfile) {
        int delayValue = Integer.MAX_VALUE;
        // перебор выходов типа "тушение"
        String otFireExtinction = deviceProfile.getFireExtinctionOutputTypeValue();
        for (String outputTypePropName : deviceProfile.getFireExtinctionDelayPropertiesMap().keySet()) {
            if (deviceProject.getProjectConfigValues().containsKey(outputTypePropName) &&
                    deviceProject.getProjectConfigValues().get(outputTypePropName).equals(otFireExtinction)) {
                // получение параметра выхода "задержка тушения"
                String delayPropertyName = deviceProfile.getFireExtinctionDelayPropertiesMap().get(outputTypePropName);
                String delayPropertyValue = deviceProject.getProjectConfigValues().get(delayPropertyName);
                try {
                    // выбор наименьшего нач. зн. таймера среди всех выходов типа "тушение"
                    delayValue = Integer.min(delayValue, Integer.parseInt(delayPropertyValue));
                } catch (NumberFormatException ex) {}
            }
        }
        // если нет выходов "тушение" на момент активации проекта
        if (delayValue == Integer.MAX_VALUE)
            return 0;
        // вычетаем 3 секунды "на погрешность"
        return Integer.max(0, delayValue - 3);
    }

    public ActiveDevice addNoteToDevice(String deviceId, String userName, ActiveDevice.Note note){
        boolean success = true;
        ActiveDevice device;
        if ((device = activeDeviceRepository.findOne(deviceId)) == null) {
            success = false;
        }
        if (success){
            note.setCreateDateTime(new Date());
            note.setUserName(userName);
            device.addNote(note);
            activeDeviceRepository.save(device);
        }
        return device;
    }

    @Override
    public ActiveDevice findControlDevice(ActiveDevice device, Map<String, ActiveDevice> devicesHash) {
        if (device.getDeviceProject().getDeviceCategory() == DeviceCategory.CONTROL) {
            return device;
        }
        if (device.getParentDeviceId() == null || device.getParentDeviceId().isEmpty()) {
            return null;
        }
        if (devicesHash.get(device.getParentDeviceId()) != null) {
            return findControlDevice(devicesHash.get(device.getParentDeviceId()), devicesHash);
        }
        ActiveDevice parentDevice = activeDeviceRepository.findByProjectIdAndId(device.getProjectId(),
                device.getParentDeviceId());
        if (parentDevice == null) {
            return null;
        }
        devicesHash.put(parentDevice.getId(), parentDevice);
        return findControlDevice(parentDevice, devicesHash);
    }

    @Override
    public void setOffStateOnAllEntities() {
        State offState = stateRepository.findByOffStateIsTrue();
        if (offState != null){
            Set<String> singleStateSet = new HashSet<>();
            singleStateSet.add(offState.getId());
            List<ActiveDevice> activeDevices = activeDeviceRepository.findAll();
            activeDevices.forEach(activeDevice -> setActivePollingStates(activeDevice, singleStateSet));
        } else {
            logger.error("Couldn't update device states. 'All-off' state not defined");
        }
    }

    @Override
    public void setOffStateByRegions(Set<Integer> offStateRegionNos) {
        State offState = stateRepository.findByOffStateIsTrue();
        if (offState != null){
            Set<String> singleStateSet = new HashSet<>();
            singleStateSet.add(offState.getId());
            Set<String> offStateRegionIds = activeRegionRepository.findByIndexIn(offStateRegionNos)
                    .stream()
                    .map(BasicEntity::getId)
                    .collect(Collectors.toSet());
            List<ActiveDevice> offStateActiveDevices = activeDeviceRepository.findByRegionIdIn(offStateRegionIds);
            offStateActiveDevices.forEach(activeDevice -> setActivePollingStates(activeDevice, singleStateSet));
        } else {
            logger.error("Couldn't update device states. 'All-off' state not defined");
        }
    }

    @Override
    public void setActivePollingStates(Map<String, Integer> deviceInfoWithStateIds) {
        deviceInfoWithStateIds.keySet().forEach(deviceInfo -> {
            // too many repo calls
            State state = stateRepository.findByNumber(deviceInfoWithStateIds.get(deviceInfo));

            String[] deviceInfoParams = deviceInfo.split(";");
            ActiveRegion region = activeRegionRepository.findByIndex(Integer.parseInt(deviceInfoParams[0]));
            DeviceProfile deviceProfile = deviceProfileRepository
                    .findByDeviceType(DeviceType.valueOf(deviceInfoParams[1]));

            if (state != null && region != null && deviceProfile != null) {
                Device device = deviceRepository.findByRegionIdAndDeviceProfileIdAndLineAddress(region.getId(),
                        deviceProfile.getId(), Integer.parseInt(deviceInfoParams[2]));
                if (device != null) {
                    Set<String> singleStateSet = new HashSet<>();
                    singleStateSet.add(state.getId());
                    setActivePollingStates(device.getId(), singleStateSet);
                }
            }
        });
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {
        String activeProjectId = activeProject.getId();

        /*
         * Получим события, отсортированыые по возрастанию идентификатора, для которых:
         *  - online == true
         *  - есть deviceId или regionId или activeDeviceStateIds
         *  - идентификатор: (observeFrom, ObjectId(currentTime)]
         */
        List<Event> eventsToApply = eventRepository
                .findByIdAfterAndOnlineIsTrueAnd_DeviceIdOrRegionIdOrControlDeviceStateIdsNotEmpty(observeFromOid,
                        new Sort(Direction.ASC, "_id"));
        if (eventsToApply.isEmpty())
            return observeFromOid;

        /* Сформируем коллекции нужных идентификаторов */
        Set<String> eventTypeIds = new HashSet<>();
        Set<String> deviceIds = new HashSet<>();
        Set<String> regionIds = new HashSet<>();
        for (Event event : eventsToApply) {
            eventTypeIds.add(event.getEventTypeId());
            if (event.getDeviceId() != null && !event.getDeviceId().isEmpty())
                deviceIds.add(event.getDeviceId());
            if (event.getRegionId() != null && !event.getRegionId().isEmpty()) {
                regionIds.add(event.getRegionId());
            }
        }

        /* Выберем все необходимые типы событий */
        Map<String, EventType> eventTypesByIds = eventTypeRepository.findAllByIdIn(eventTypeIds).stream()
                .collect(Collectors.toMap(EventType::getId, eventType -> eventType));

        /* Выберем все приборы, для которых могут быть состояния в событиях */
        Set<String> controlDeviceIds = eventsToApply.stream()
                .filter(event -> eventTypesByIds.get(event.getEventTypeId()).isHavingControlDeviceStates())
                .map(Event::getControlDeviceId).collect(Collectors.toSet());
        Map<String, ActiveDevice> controlDevicesByIds = activeDeviceRepository
                .findAllByProjectIdAndIdIn(activeProjectId, controlDeviceIds).stream()
                .collect(Collectors.toMap(ActiveDevice::getId, device -> device));

        /* Выберем все необходимые устройства по событиям */
        Map<String, ActiveDevice> addressDevicesByIds = activeDeviceRepository
                .findAllByProjectIdAndIdIn(activeProjectId, deviceIds).stream()
                .collect(Collectors.toMap(ActiveDevice::getId, device -> device));

        /* Выберем все необходимые устройства по зонам событий */
        addressDevicesByIds.putAll(activeDeviceRepository.findByProjectIdAndRegionIdIn(activeProjectId, regionIds)
                .stream().collect(Collectors.toMap(ActiveDevice::getId, device -> device)));

        /* Выберем профили устройств */
        Set<String> deviceProfileIds = addressDevicesByIds.values().stream()
                .map(device -> device.getDeviceProject().getDeviceProfileId()).collect(Collectors.toSet());
        deviceProfileIds.addAll(controlDevicesByIds.values().stream()
                .map(device -> device.getDeviceProject().getDeviceProfileId()).collect(Collectors.toSet()));
        Map<String, DeviceProfile> deviceProfilesByIds = deviceProfileRepository.findByIdIn(deviceProfileIds).stream()
                .collect(Collectors.toMap(DeviceProfile::getId, deviceProfile -> deviceProfile));

        /* Сгруппируем устройства по зонам */
        Map<String, List<ActiveDevice>> addressDevicesByRegionIds = new HashMap<>();
        addressDevicesByIds.values().stream().forEach(device -> {
            if (!addressDevicesByRegionIds.containsKey(device.getRegionId())) {
                addressDevicesByRegionIds.put(device.getRegionId(), new ArrayList<>());
            }
            addressDevicesByRegionIds.get(device.getRegionId()).add(device);
        });

        for (Event event : eventsToApply) {
            EventType eventType = eventTypesByIds.get(event.getEventTypeId());
            if (eventType == null) {
                logger.error("Event type not found. Id: {}", event.getEventTypeId());
                continue;
            }

            try {
                if (eventType.isHavingControlDeviceStates() && event.getControlDeviceId() != null
                        && !event.getControlDeviceId().isEmpty()) {
                    ActiveDevice controlDevice = controlDevicesByIds.get(event.getControlDeviceId());
                    if (controlDevice == null)
                        throw new AssertionError("control device not found", null);

                    DeviceProfile controlDeviceProfile =
                            deviceProfilesByIds.get(controlDevice.getDeviceProject().getDeviceProfileId());
                    if (controlDeviceProfile == null)
                        throw new AssertionError("control device profile not found", null);

                    applyEventToControlDevice(event, eventType, controlDevice, controlDeviceProfile);
                }

                if (event.getDeviceId() != null && !event.getDeviceId().isEmpty()) {
                    ActiveDevice device = addressDevicesByIds.get(event.getDeviceId());
                    if (device == null)
                        throw new AssertionError("address device not found", null);

                    DeviceProfile deviceProfile =
                            deviceProfilesByIds.get(device.getDeviceProject().getDeviceProfileId());
                    if (deviceProfile == null)
                        throw new AssertionError("address device profile not found", null);

                    applyEventToDevice(event, eventType, device, deviceProfile);
                }

                if (event.getRegionId() != null && !event.getRegionId().isEmpty()) {
                    List<ActiveDevice> regionDevices = addressDevicesByRegionIds.get(event.getRegionId());
                    if (regionDevices == null)
                        throw new AssertionError("region devices not found", null);

                    applyEventTypeToRegionDevices(eventType, regionDevices, deviceProfilesByIds);
                }

            } catch (AssertionError | NullPointerException e) {
                logger.error("Something not found. Even id: {}, error message: {}", event.getId(), e.getMessage());
            }
        }

        List<ActiveDevice> updatedDevices = addressDevicesByIds.values().stream()
                .filter(ActiveDevice::isNeedsStateCommit).collect(Collectors.toList());

        Map<String, State> statesHash = new HashMap<>();
        if (!updatedDevices.isEmpty()) {
            StateCategory defaultCategory = getDefaultStateCategory();
            for (ActiveDevice device : updatedDevices) {
                prepareCommitAfterStateUpdated(device, defaultCategory, currentTime, statesHash);
            }
            activeDeviceRepository.save(updatedDevices);
        }

        /* Список событий отсортирован по id, поэтому последний ObjectId берем от последнего события в списке */
        return new ObjectId(eventsToApply.get(eventsToApply.size() - 1).getId());
    }

    private void applyEventToControlDevice(Event event, EventType eventType, ActiveDevice controlDevice,
            DeviceProfile deviceProfile) {

        Set<String> stateIdsToApply = new HashSet<>();

        if (event.getControlDeviceStateIds() != null && !event.getControlDeviceStateIds().isEmpty()) {
            stateIdsToApply.addAll(event.getControlDeviceStateIds());
        }
        if (eventType.getAutoSetStateIds() != null && !eventType.getAutoSetStateIds().isEmpty()) {
            stateIdsToApply.addAll(eventType.getAutoSetStateIds());
        }

        if (!stateIdsToApply.isEmpty())
            controlDevice.addValidStates(stateIdsToApply, deviceProfile);

        if (!eventType.getAutoResetStateIds().isEmpty())
            controlDevice.removeStates(eventType.getAutoResetStateIds());
    }

    private void applyEventToDevice(Event event, EventType eventType, ActiveDevice device,
            DeviceProfile deviceProfile) {

        Set<String> stateIdsToApply = new HashSet<>();

        if (event.getActiveDeviceStateIds() != null && !event.getActiveDeviceStateIds().isEmpty()) {
            stateIdsToApply.addAll(event.getActiveDeviceStateIds());
        }
        if (eventType.getAutoSetStateIds() != null && !eventType.getAutoSetStateIds().isEmpty()) {
            stateIdsToApply.addAll(eventType.getAutoSetStateIds());
        }

        if (!stateIdsToApply.isEmpty())
            device.addValidStates(stateIdsToApply, deviceProfile);

        if (!eventType.getAutoResetStateIds().isEmpty())
            device.removeStates(eventType.getAutoResetStateIds());
    }

    private void applyEventTypeToRegionDevices(EventType eventType, List<ActiveDevice> devices,
            Map<String, DeviceProfile> deviceProfilesByIds) {
        if (!eventType.getRegionAutoSetStateIds().isEmpty()) {
            for (ActiveDevice device : devices) {
                DeviceProfile deviceProfile = deviceProfilesByIds.get(device.getDeviceProject().getDeviceProfileId());
                if (deviceProfile != null) {
                    device.addValidStates(eventType.getRegionAutoSetStateIds(), deviceProfile);
                } else {
                    logger.error("Device profile not found");
                }
            }
        }
        if (!eventType.getRegionAutoResetStateIds().isEmpty()) {
            for (ActiveDevice device : devices) {
                device.removeStates(eventType.getRegionAutoResetStateIds());
            }
        }
    }
}
