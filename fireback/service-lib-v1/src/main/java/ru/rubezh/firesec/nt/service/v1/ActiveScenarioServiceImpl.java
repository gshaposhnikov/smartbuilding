package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.ActiveScenarioRepository;
import ru.rubezh.firesec.nt.dao.v1.EventRepository;
import ru.rubezh.firesec.nt.dao.v1.EventTypeRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioRepository;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;
import java.util.stream.Collectors;

@Service("activeScenarioService")
public class ActiveScenarioServiceImpl extends ActiveEntityWithStatesServiceImpl
        implements ActiveScenarioService, UpsertEntitiesProcessor {

    @Autowired
    Logger logger;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private EventRepository eventRepository;

    public static final int ACTIVE_SCENARIOS_SAFE_PAGE_SIZE = 300;

    @Override
    public void createActiveEntities(String projectId, Date createDateTime) {
        StateCategory defaultCategory = getDefaultStateCategory();
        long nPages = (scenarioRepository.countByProjectIdAndEnabledIsTrue(projectId)
                / ACTIVE_SCENARIOS_SAFE_PAGE_SIZE) + 1;
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkMode.UNORDERED, ActiveScenario.class);
        for (int pageIndex = 0; pageIndex < nPages; pageIndex++) {
            Pageable pageable = new PageRequest(pageIndex, ACTIVE_SCENARIOS_SAFE_PAGE_SIZE);
            List<Scenario> scenarios = scenarioRepository.findByProjectIdAndEnabledIsTrue(projectId, pageable);
            if (!scenarios.isEmpty()) {
                for (Scenario scenario : scenarios) {
                    ActiveScenario activeScenario = new ActiveScenario(scenario);
                    activeScenario.setGeneralStateCategoryId(defaultCategory.getId());
                    activeScenario.setStateGetDateTime(createDateTime);
                    bulkOperations.insert(activeScenario);
                }
                bulkOperations.execute();
            }
        }
    }

    @Override
    public void destroyActiveEntities() {
        activeScenarioRepository.deleteAll();
    }

    @Override
    public void addActiveState(String scenarioId, String stateId) {
        ActiveScenario activeScenario = activeScenarioRepository.findOne(scenarioId);
        if (activeScenario == null) {
            logger.warn("Active scenario not found, id: {}", scenarioId);
            return;
        }

        if (activeScenario.getActiveStates().add(stateId))
            commitAfterStateUpdated(activeScenario, true);
    }

    @Override
    public void removeActiveState(String scenarioId, String stateId) {
        ActiveScenario activeScenario = activeScenarioRepository.findOne(scenarioId);
        if (activeScenario == null) {
            logger.warn("Active scenario not found, id: {}", scenarioId);
            return;
        }

        if (activeScenario.getActiveStates().remove(stateId))
            commitAfterStateUpdated(activeScenario, true);
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {
        String activeProjectId = activeProject.getId();

        List<Event> eventsToApply = eventRepository
                .findByIdAfterAndOnlineIsTrueAndScenarioIdNotNull(observeFromOid, new Sort(Direction.ASC, "_id"));
        if (eventsToApply.isEmpty())
            return observeFromOid;

        /* Сформируем коллекции нужных идентификаторов */
        Set<String> eventTypeIds = new HashSet<>();
        Set<String> scenarioIds = new HashSet<>();
        for (Event event : eventsToApply) {
            eventTypeIds.add(event.getEventTypeId());
            if (event.getScenarioId() != null) {
                scenarioIds.add(event.getScenarioId());
            }
        }

        /* Список событий отсортирован по id, поэтому последний ObjectId берем от последнего события в списке */
        ObjectId lastObservedOid = new ObjectId(eventsToApply.get(eventsToApply.size() - 1).getId());

        if (scenarioIds.isEmpty())
            return lastObservedOid;

        /* Выберем все необходимые типы событий */
        Map<String, EventType> eventTypesByIds = eventTypeRepository.findAllByIdIn(eventTypeIds).stream()
                .collect(Collectors.toMap(EventType::getId, eventType -> eventType));

        /* Выберем все сценарии по событиям */
        Map<String, ActiveScenario> scenariosByIds = activeScenarioRepository
                .findByProjectIdAndIdIn(activeProjectId, scenarioIds).stream()
                .collect(Collectors.toMap(ActiveScenario::getId, scenario -> scenario));

        for (Event event : eventsToApply) {
            EventType eventType = eventTypesByIds.get(event.getEventTypeId());
            if (eventType == null) {
                logger.error("Event type not found. Id: {}", event.getEventTypeId());
                continue;
            }

            try {
                ActiveScenario scenario = scenariosByIds.get(event.getScenarioId());
                assert scenario != null : "scenario not found";

                if (!eventType.getAutoSetStateIds().isEmpty())
                    scenario.addStates(eventType.getAutoSetStateIds());
                if (!eventType.getAutoResetStateIds().isEmpty())
                    scenario.removeStates(eventType.getAutoResetStateIds());

            } catch (AssertionError | NullPointerException e) {
                logger.error("Something not found. Even id: {}, error message: {}", event.getId(), e.getMessage());
            }
        }

        List<ActiveScenario> updatedScenarios = scenariosByIds.values().stream()
                .filter(ActiveScenario::isNeedsStateCommit).collect(Collectors.toList());

        Map<String, State> statesHash = new HashMap<>();
        if (!updatedScenarios.isEmpty()) {
            StateCategory defaultCategory = getDefaultStateCategory();
            for (ActiveScenario scenario : updatedScenarios) {
                prepareCommitAfterStateUpdated(scenario, defaultCategory, currentTime, statesHash);
            }
            activeScenarioRepository.save(updatedScenarios);
        }

        return lastObservedOid;
    }

}
