package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.ActiveDeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.ActiveRegionRepository;
import ru.rubezh.firesec.nt.dao.v1.RegionRepository;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;
import java.util.stream.Collectors;

@Service("activeRegionService")
public class ActiveRegionServiceImpl extends ActiveEntityWithStatesServiceImpl
        implements ActiveRegionService, UpsertEntitiesProcessor, ActivationFilterableService {

    @Autowired
    Logger logger;

    @Autowired
    RegionRepository regionRepository;

    @Autowired
    ActiveRegionRepository activeRegionRepository;

    @Autowired
    ActiveDeviceRepository activeDeviceRepository;

    /** Кол-во зон, за раз извлекаемое из базы при создании активных сущностей */
    public static final int ACTIVE_REGIONS_SAFE_PAGE_SIZE = 3000;

    @Override
    public void createActiveEntities(String projectId, Date createDateTime) {
        StateCategory defaultCategory = getDefaultStateCategory();
        long nPages = (regionRepository.countByProjectId(projectId)
                / ACTIVE_REGIONS_SAFE_PAGE_SIZE) + 1;
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkMode.UNORDERED, ActiveRegion.class);
        for (int pageIndex = 0; pageIndex < nPages; pageIndex++) {
            Pageable pageable = new PageRequest(pageIndex, ACTIVE_REGIONS_SAFE_PAGE_SIZE);
            List<Region> regions = regionRepository.findByProjectId(projectId, pageable);
            if (!regions.isEmpty()) {
                for (Region region: regions) {
                    ActiveRegion activeRegion = new ActiveRegion(region);
                    activeRegion.setGeneralStateCategoryId(defaultCategory.getId());
                    activeRegion.setStateGetDateTime(createDateTime);
                    bulkOperations.insert(activeRegion);
                }
                bulkOperations.execute();
            }
        }
    }

    @Override
    public void destroyActiveEntities() {
        activeRegionRepository.deleteAll();
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromdOid, Date currentTime) {
        String activeProjectId = activeProject.getId();

        List<ActiveDevice> devicesToApply = activeDeviceRepository
                .findByProjectIdAndStateGetDateTimeAfter(activeProjectId, observeFromdOid.getDate());
        if (devicesToApply.isEmpty())
            return observeFromdOid;

        /* Сформируем множества идентификаторов зон и устройств, принадлежащих зонам */
        Set<String> regionIds = new HashSet<>();
        Set<String> deviceIds = new HashSet<>();
        for (ActiveDevice device : devicesToApply) {
            if (device.getRegionId() != null && !device.getRegionId().isEmpty()) {
                regionIds.add(device.getRegionId());
                deviceIds.add(device.getId());
            }
        }

        if (regionIds.isEmpty())
            return new ObjectId(currentTime, 0);

        /* Выберем зоны устройств */
        List<ActiveRegion> regions = activeRegionRepository.findByProjectIdAndIdIn(activeProjectId, regionIds);

        /* Дополним множество устройств недостающими устройствами зон */
        List<ActiveDevice> devices = new ArrayList<>(devicesToApply);
        devices.addAll(
                activeDeviceRepository.findByProjectIdAndRegionIdInAndIdNotIn(activeProjectId, regionIds, deviceIds));

        /* Сформируем карту устройств по зонам */
        Map<String, List<ActiveDevice>> devicesByRegionIds = new HashMap<>();
        for (ActiveDevice device : devices) {
            String regionId = device.getRegionId();
            if (!devicesByRegionIds.containsKey(regionId))
                devicesByRegionIds.put(regionId, new ArrayList<>());
            devicesByRegionIds.get(regionId).add(device);
        }

        StateCategory defaultStateCategory = getDefaultStateCategory();
        Map<String, State> statesHash = new HashMap<>();
        List<ActiveRegion> updatedRegions = new ArrayList<>();

        /* Обновим состояния зон */
        for (ActiveRegion region : regions) {
            List<ActiveDevice> regionDevices = devicesByRegionIds.get(region.getId());

            Set<String> oldActiveStates = region.getActiveStates();
            region.setActiveStates(new HashSet<>());
            Map<String, Set<String>> oldInheritedActiveStates = region.getInheritedActiveStates();
            region.setInheritedActiveStates(new HashMap<>());

            /* Заполняем заново набор активных состояний зоны */
            for (ActiveDevice device : regionDevices) {
                region.getActiveStates().addAll(device.getActiveStates());

                /* В том числе набор унаследованных активных состояний */
                for (String parentId : device.getInheritedActiveStates().keySet()) {
                    Set<String> inheritedStateIds = region.getInheritedActiveStates().computeIfAbsent(parentId, k -> new HashSet<>());
                    inheritedStateIds.addAll(device.getInheritedActiveStates().get(parentId));
                }
            }

            if (!oldActiveStates.equals(region.getActiveStates())
                    || !oldInheritedActiveStates.equals(region.getInheritedActiveStates())) {
                prepareCommitAfterStateUpdated(region, defaultStateCategory, currentTime, statesHash);
                updatedRegions.add(region);
            }
        }

        if (!updatedRegions.isEmpty()) {
            activeRegionRepository.save(updatedRegions);
        }

        return new ObjectId(currentTime, 0);
    }

    @Override
    public void prepareCommitAfterStateUpdated(BasicActiveEntityWithStates entity, StateCategory defaultStateCategory,
            Date currentTime, Map<String, State> statesHash) {
        super.prepareCommitAfterStateUpdated(entity, defaultStateCategory, currentTime, statesHash);
        ActiveRegion region = (ActiveRegion) entity;
        if (region.getRegionProject().getSubsystem() == Subsystem.SECURITY) {
            updateOnGuardStatus(region, statesHash != null ? statesHash : new HashMap<>());
        }
    }

    private void updateOnGuardStatus(ActiveRegion region, Map<String, State> statesHash) {
        /* СОберем в одно множество идентификаторы состояний зоны */
        Set<String> allRegionStateIds = new HashSet<>();
        allRegionStateIds.addAll(region.getActiveStates());
        for (Set<String> inheritedStateIds : region.getInheritedActiveStates().values()) {
            allRegionStateIds.addAll(inheritedStateIds);
        }

        /* Дополним хеш состояний, если надо */
        Set<String> nonHashedStateIds = allRegionStateIds.stream().filter(stateId -> !statesHash.containsKey(stateId))
                .collect(Collectors.toSet());
        if (!nonHashedStateIds.isEmpty())
            statesHash.putAll(stateRepository.findByIdIn(nonHashedStateIds).stream()
                    .collect(Collectors.toMap(State::getId, state -> state)));

        /* Определи статус охранной зоны */
        if (allRegionStateIds.stream().map(stateId -> statesHash.get(stateId))
                .anyMatch(state -> state.getStateCategory().isOnGuardCategory())) {
            region.setOnGuard(true);
        } else {
            region.setOnGuard(false);
        }
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<ActiveRegion> activeRegions = activeRegionRepository.findAll();
        activeRegions.forEach(activeRegion -> {
            activeRegion.getFilterTags().getTags().add(activeRegion.getRegionProject().getSubsystem().toString());
        });

        activeRegionRepository.save(activeRegions);
    }
}
