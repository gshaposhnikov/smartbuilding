package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@Order(5)
public class IndicatorPanelServiceImpl implements IndicatorPanelService, ActivationFilterableService {

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private IndicatorPanelGroupRepository indicatorPanelGroupRepository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    public IndicatorPanel addNew(IndicatorPanel indicatorPanel, String projectId, StringBuilder errorMessage) {
        if (allowInput(indicatorPanel, projectId, errorMessage)) {
            indicatorPanel.setProjectId(projectId);
            indicatorPanel.addEmptyIndicators();
            indicatorPanelRepository.insert(indicatorPanel);
            return indicatorPanel;
        } else {
            return null;
        }
    }

    public IndicatorPanel update(IndicatorPanel indicatorPanel, String id, String projectId, StringBuilder errorMessage) {
        IndicatorPanel updatablePanel = indicatorPanelRepository.findByProjectIdAndId(projectId, id);
        if (updatablePanel == null){
            errorMessage.append("Обновляемая панель индикаторов не найдена");
            return null;
        } else {
            if (allowInput(indicatorPanel, updatablePanel.getProjectId(), errorMessage)){
                if (updatablePanel.getRowCount() != indicatorPanel.getRowCount()
                        || updatablePanel.getColCount() != indicatorPanel.getColCount())
                    updatablePanel.updatePanelCells(indicatorPanel.getRowCount(), indicatorPanel.getColCount());
                updatablePanel.update(indicatorPanel);
                indicatorPanelRepository.save(updatablePanel);
                return updatablePanel;
            } else {
                return null;
            }
        }
    }

    public boolean remove(String projectId, String id, StringBuilder errorMessage){
        if (!projectService.checkProjectIsEditable(projectId, errorMessage))
            return false;
        return (indicatorPanelRepository.deleteByProjectIdAndId(projectId, id) == 1);
    }

    public IndicatorPanel updateIndicator(Indicator indicator, String projectId, String id,
                                          int colNo, int rowNo, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)){
            return null;
        }
        IndicatorPanel updatablePanel = indicatorPanelRepository.findByProjectIdAndId(projectId, id);
        if (updatablePanel != null) {
            EntityType entityType = indicator.getEntityType();
            if (entityType != EntityType.DEVICE && entityType != EntityType.REGION
                    && entityType != EntityType.SCENARIO && entityType != EntityType.NONE) {
                errorMessage.append("Задан неправильный тип сущности");
                return null;
            }
            if (indicator.getEntityType() == EntityType.SCENARIO && indicator.getEntityIds().size() > 1){
                errorMessage.append("Больше одного сценария не допускается");
                return null;
            }
            if (updatablePanel.getIndicators().size() <= rowNo
                    || updatablePanel.getIndicators().get(rowNo).size() <= colNo){
                errorMessage.append("Неправильные параметры ячейки индикатора");
                return null;
            }
            if (indicator.getStyles() != null && (indicator.getStyles().getFontSize() < Indicator.Style.MIN_FONT_SIZE
                    || indicator.getStyles().getFontSize() > Indicator.Style.MAX_FONT_SIZE)){
                errorMessage.append("Неправильно задан размер шрифта");
                return null;
            }
            if (!checkEntities(indicator.getEntityType(), indicator.getEntityIds(), projectId, errorMessage)) {
                return null;
            }
            updatablePanel.getIndicators().get(rowNo).get(colNo).update(indicator);
            indicatorPanelRepository.save(updatablePanel);
            return updatablePanel;
        } else {
            errorMessage.append("Обновляемая панель индикаторов не найдена");
            return null;
        }
    }

    public List<IndicatorPanel> removeEntityIdsFromIndicatorPanels(String projectId,
                                                                   List<String> entityIds, EntityType entityType) {
        List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);
        List<IndicatorPanel> updatedIndicatorPanels = new ArrayList<>();
        for (IndicatorPanel indicatorPanel : indicatorPanels) {
            boolean updated = false;
            for (int i = 0; i < indicatorPanel.getRowCount(); i++) {
                for (int j = 0; j < indicatorPanel.getColCount(); j++) {
                    Indicator currentIndicator = indicatorPanel.getIndicators().get(i).get(j);
                    if (entityType == currentIndicator.getEntityType()
                            && currentIndicator.getEntityIds().removeAll(entityIds))
                        updated = true;
                }
            }
            if (updated)
                updatedIndicatorPanels.add(indicatorPanel);
        }
        indicatorPanelRepository.save(indicatorPanels);
        return updatedIndicatorPanels;
    }

    /* проверяет существование указанных в индикаторе сущностей */
    private boolean checkEntities(EntityType entityType, List<String> entityIds, String projectId,
                                  StringBuilder errorMessage){
        if (entityIds.isEmpty())
            return true;
        if (entityType == EntityType.SCENARIO) {
            /* сценарий может быть только один */
            if (scenarioRepository.existsByProjectIdAndId(projectId, entityIds.get(0))){
                return true;
            } else {
                errorMessage.append("Указанного сценария не существует");
                return false;
            }
        } else if (entityType == EntityType.DEVICE) {
            List<Device> devices = deviceRepository.findByProjectIdAndIdIn(projectId, entityIds);
            if (devices.isEmpty() || devices.size() != entityIds.size()){
                errorMessage.append("Устройства заданы неправильно");
                return false;
            }
            for (Device device : devices) {
                DeviceCategory deviceCategory = device.getDeviceCategory();
                if (deviceCategory == DeviceCategory.CONTROL
                        || deviceCategory == DeviceCategory.TRANSPORT
                        || deviceCategory == DeviceCategory.CONTAINER){
                    errorMessage.append("Неправильный тип устройства");
                    return false;
                }
            }
            return true;
        } else if (entityType == EntityType.REGION){
            List<Region> regions = regionRepository.findByProjectIdAndIdIn(projectId, entityIds);
            if (regions.size() != entityIds.size()) {
                errorMessage.append("Зоны заданы неправильно");
                return false;
            } else if (!regions.stream().allMatch(region -> region.getSubsystem() == regions.get(0).getSubsystem())) {
                errorMessage.append("В индикаторе не могут находиться зоны с разными подсистемами");
                return false;
            }
        }
        return true;
    }


    private boolean allowInput(IndicatorPanel indicatorPanel, String projectId, StringBuilder errorMessage){
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return false;
        }
        if (!indicatorPanelGroupRepository.exists(indicatorPanel.getIndicatorGroupId())){
            errorMessage.append("Не найдена группа панелей индикаторов по указанному идентификатору");
            return false;
        }
        if (indicatorPanel.getColCount() < 3 || indicatorPanel.getColCount() > 20){
            errorMessage.append("Число столбцов вне допустимого интервала");
            return false;
        }
        if (indicatorPanel.getRowCount() < 3 || indicatorPanel.getRowCount() > 100){
            errorMessage.append("Число строк вне допустимого интервала");
            return false;
        }
        return true;
    }

    @Override
    public void clearTags(String projectId) {
        List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);
        indicatorPanels.forEach(indicatorPanel -> {
            indicatorPanel.getFilterTags().getTags().clear();
            indicatorPanel.getIndicators().forEach(indicatorRow -> indicatorRow
                    .forEach(indicator -> indicator.getFilterTags().getTags().clear()));
        });
        indicatorPanelRepository.save(indicatorPanels);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);

        Map<String, FilterTags> deviceIdToFilterTags = deviceRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Device::getId, Device::getFilterTags));
        Map<String, Subsystem> regionIdToSubsystem = regionRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Region::getId, Region::getSubsystem));
        Map<String, FilterTags> scenarioIdToFilterTags= scenarioRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Scenario::getId, Scenario::getFilterTags));
        indicatorPanels.forEach(indicatorPanel -> {
            FilterTags filterTags = indicatorPanel.getFilterTags();

            List<FilterTags> indicatorsFilterTags = setIndicatorsSubsystemTagsAndGetList(indicatorPanel.getIndicators(),
                    deviceIdToFilterTags, regionIdToSubsystem, scenarioIdToFilterTags);
            for (FilterTags indicatorFilterTags : indicatorsFilterTags) {
                filterTags.getTags().addAll(indicatorFilterTags.chooseSubsystemTags());
            }
        });

        indicatorPanelRepository.save(indicatorPanels);
    }

    private List<FilterTags> setIndicatorsSubsystemTagsAndGetList(List<List<Indicator>> indicators,
                                                               Map<String, FilterTags> deviceIdToFilterTags,
                                                               Map<String, Subsystem> regionIdToSubsystem,
                                                               Map<String, FilterTags> scenarioIdToFilterTags){
        List<FilterTags> filterTagsList = new ArrayList<>();
        indicators.forEach(indicatorRow -> {
            indicatorRow.forEach(indicator -> {
                FilterTags filterTags = indicator.getFilterTags();

                Set<String> indicatorTags = filterTags.getTags();
                switch (indicator.getEntityType()){
                    case CONTROL_DEVICE:
                    case DEVICE:
                        inheritSubsystemFilterTags(deviceIdToFilterTags, indicatorTags, indicator.getEntityIds());
                        break;
                    case REGION:
                        indicator.getEntityIds().forEach(regionId -> indicatorTags
                                .add(regionIdToSubsystem.get(regionId).toString()));
                        break;
                    case SCENARIO:
                        inheritSubsystemFilterTags(scenarioIdToFilterTags, indicatorTags, indicator.getEntityIds());
                        break;
                }

                filterTagsList.add(filterTags);
            });
        });

        return filterTagsList;
    }

    private void inheritSubsystemFilterTags(Map<String, FilterTags> entityIdToFilterTags,
                                            Set<String> availableSubsystems, List<String> entityIds){
        entityIds.forEach(entityId -> {
            FilterTags filterTags = entityIdToFilterTags.get(entityId);
            if (filterTags != null) {
                availableSubsystems
                        .addAll(entityIdToFilterTags.get(entityId).chooseSubsystemTags());
            }
        });
    }
}
