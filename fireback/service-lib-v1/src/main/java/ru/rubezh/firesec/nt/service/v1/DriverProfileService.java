package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.dao.v1.DriverProfileRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverProfile;

@Service
public class DriverProfileService {

    @Autowired
    private DriverProfileRepository driverProfileRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private EventService eventService;

    public DriverProfile getById(String id) {
        return driverProfileRepository.findOne(id);
    }

    public DriverProfile add(DriverProfile driverProfile) {
        eventService.addDecodableEventStateSets(driverProfile.getDecodableEventStateSets());
        eventService.addEventProfiles(driverProfile.getEventProfiles());
        return driverProfileRepository.save(driverProfile);
    }

    public void remove(String driverProfileId) {
        DriverProfile driverProfile = driverProfileRepository.findOne(driverProfileId);

        eventService.removeDecodableEventStateSets(driverProfile.getDecodableEventStateSets());
        eventService.removeEventProfiles(driverProfile.getEventProfiles());

        for (String deviceProfileId: driverProfile.getDeviceProfileIds()) {
            if (driverProfileRepository.countByDeviceProfileIdsContaining(deviceProfileId) == 1) {
                deviceProfileRepository.delete(deviceProfileId);
            }
        }

        driverProfileRepository.delete(driverProfile);
    }

}
