package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.amqp.message.alert.IssueProgress;
import ru.rubezh.firesec.nt.amqp.message.request.AbstractRoutedDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.CreateIssueDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.response.CreateIssueDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.Issue.IssueParameterNames;
import ru.rubezh.firesec.nt.domain.v1.IssueParams.WriteSkudDatabaseParams;
import ru.rubezh.firesec.nt.service.v1.skud.AccessKeyService;
import ru.rubezh.firesec.nt.service.v1.skud.EmployeeService;
import ru.rubezh.firesec.nt.service.v1.skud.WorkScheduleService;

import java.util.*;

@Service
public class IssueService {

    @Autowired
    private Logger logger;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private DriverInstanceRepository driverInstanceRepository;

    @Autowired
    private DriverProfileRepository driverProfileRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccessKeyService accessKeyService;

    @Autowired
    private WorkScheduleService workScheduleService;

    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private StateRepository stateRepository;

    public IssueService() {
    }

    public interface IssueProgressHandler {
        void onIssueProgress(Issue issue, IssueProgress issueProgress);
    }

    private List<IssueProgressHandler> issueProgressHandlers = new ArrayList<>();

    public boolean validate(Issue issue, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsActive(issue.getProjectId(), errorMessage)) {
            return false;
        }
        if (!validateByIssueActionEntityType(issue, errorMessage)) {
            return false;
        }
        if (!validateByIssueAction(issue, errorMessage)) {
            return false;
        }

        return true;
    }

    private boolean validateByIssueAction(Issue issue, StringBuilder errorMessage) {
        switch (issue.getAction()) {
        case READ_EVENTS:
            try {
                List<?> issueLogIds = (List<?>) issue.getParameters().get("logIds");
                if (issueLogIds == null || issueLogIds.isEmpty()) {
                    errorMessage.append("Не указаны журналы для чтения");
                    return false;
                } else {
                    Device device = deviceRepository.findOne(issue.getDeviceId());
                    DeviceProfile deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId());
                    List<String> logIds = new ArrayList<>();
                    for (DeviceProfile.LogTypeItem item : deviceProfile.getLogTypes()) {
                        if (issueLogIds.contains(item.getId()))
                            logIds.add(item.getId());
                    }
                    if (logIds.isEmpty()) {
                        errorMessage.append("Не указано ни одного поддерживаемого журнала прибора");
                        return false;
                    }
                }
            } catch (Exception ex) {
                errorMessage.append("Неверные данные для чтения журнала");
                return false;
            }
            break;
        case START_READ_ACCESS_KEY:
            Object oAccesskeyId = issue.getParameters().get(IssueParameterNames.accessKeyId.toString());
            if (oAccesskeyId == null) {
                errorMessage.append("Не указан идентификатор ключа");
                return false;
            }
            if (oAccesskeyId.toString().isEmpty()) {
                errorMessage.append("Не указан идентификатор ключа");
                return false;
            }
            /* Существование ключа проверять не надо, т.к. это может быть еще не сохраненный новый ключ */

            /* Следующая проверка - такая же, как для отмены считывания ключа доступа */
        case STOP_READ_ACCESS_KEY:
            /* Проверяем, что в качестве устройства указан считыватель ключа */
            ActiveDevice device = activeDeviceRepository.findByProjectIdAndId(issue.getProjectId(),
                    issue.getDeviceId());
            if (device != null) {
                DeviceProfile deviceProfile = deviceProfileRepository
                        .findById(device.getDeviceProject().getDeviceProfileId());
                if (!deviceProfile.isAccessKeyReader()) {
                    errorMessage.append("Устройство не является считывателем ключа");
                    return false;
                }
            }
            break;
        case WRITE_CONFIG_TO_DEVICE:
            ActiveDevice activeDevice = activeDeviceRepository.findByProjectIdAndId(issue.getProjectId(),issue.getDeviceId());
            if (activeDevice != null
                    && activeDevice.getRegionId() != null
                    && activeRegionRepository.existsByIdAndOnGuardIsTrue(activeDevice.getRegionId())){
                errorMessage.append("Конфигурация устройства, находящегося на охране, не может быть изменена");
                return false;
            }
            break;
        default:
            break;
        }
        return true;
    }

    private boolean validateByIssueActionEntityType(Issue issue, StringBuilder errorMessage) {
        switch (issue.getAction().getEntityType()) {
        case DEVICE:
            if (!activeDeviceRepository.exists(issue.getDeviceId())) {
                errorMessage.append("Устройство не найдено");
                return false;
            }
            break;
        case CONTROL_DEVICE:
            if (!deviceRepository.existsByProjectIdAndDeviceCategoryAndId(issue.getProjectId(), DeviceCategory.CONTROL,
                    issue.getDeviceId())) {
                errorMessage.append("Прибор не найден");
                return false;
            }
            break;
        case REGION:
            if (!issue.getParameters().containsKey("regionId")
                    || !activeRegionRepository.exists(issue.getParameters().get("regionId").toString())) {
                errorMessage.append("Зона не найдена");
                return false;
            }
            break;
        case SCENARIO:
            Object scenarioNo = issue.getParameters().get("scenarioNo");
            if (scenarioNo instanceof Integer){
                ActiveScenario activeScenario = activeScenarioRepository.findByProjectIdAndGlobalNo(issue.getProjectId(), (int)scenarioNo);
                if (activeScenario == null){
                    errorMessage.append("Сценарий не найден");
                    return false;
                }
                else if (!activeScenario.getScenarioProject().isManualStartStopAllowed()) {
                    errorMessage.append("В сценарии запрещено ручное управление");
                    return false;
                }
            }
            break;
        case VIRTUAL_INDICATOR:
            Map<String, Object> issueParameters = issue.getParameters();
            IndicatorPanel indicatorPanel = indicatorPanelRepository
                    .findByProjectIdAndId(issue.getProjectId(),
                            issueParameters.get(IssueParameterNames.indicatorPanelId.toString()).toString());
            if (indicatorPanel == null){
                errorMessage.append("Панель индикаторов не найдена");
                return false;
            }
            EntityType entityType = EntityType
                    .valueOf(issue.getParameters().get(IssueParameterNames.entityType.toString()).toString());
            List<String> entityIds = new ArrayList<>();
            for (Object object : (List<?>) issue.getParameters().get(IssueParameterNames.entityIds.toString())) {
                if (object instanceof String) {
                    entityIds.add(String.valueOf(object));
                } else {
                    errorMessage.append("Идентификаторы объектов заданы неверно");
                    return false;
                }
            }
            Object oRowNo = issueParameters.get(IssueParameterNames.rowNo.toString());
            Object oColNo = issueParameters.get(IssueParameterNames.colNo.toString());
            if (!(oRowNo instanceof Integer) || !(oColNo instanceof Integer)
                    || indicatorPanel.getIndicators().size() <= (int)oRowNo
                    || indicatorPanel.getIndicators().get(0).size() <= (int)oColNo){
                errorMessage.append("Параметры ячейки индикатора заданы неправильно");
                return false;
            }
            int rowNo = (int)oRowNo;
            int colNo = (int)oColNo;
            Indicator indicator = indicatorPanel.getIndicators().get(rowNo).get(colNo);
            if (indicator.getEntityType() != entityType){
                errorMessage.append("Тип индикатора не совпадает с действительным");
                return false;
            }
            for (String entityId: entityIds){
                if (!indicator.getEntityIds().contains(entityId)){
                    errorMessage.append("Объекты индикатора не совпадают");
                    return false;
                }
            }
            if (entityType == EntityType.REGION && !activeRegionRepository.existsAllByIdIn(entityIds)) {
                errorMessage.append("Зона не найдена");
                return false;
            } else if (entityType == EntityType.DEVICE && !activeDeviceRepository.existsAllByIdIn(entityIds)) {
                errorMessage.append("Устройство не найдено");
                return false;
            } else if (entityType == EntityType.SCENARIO && !scenarioRepository.existsAllByIdIn(entityIds)) {
                errorMessage.append("Сценарий не найден");
                return false;
            }
            break;

        default:
            break;
        }
        return true;
    }

    public String routeRequestToDriverByDeviceId(AbstractRoutedDriverRequest request, String deviceId) {
        Device device = deviceRepository.findOne(deviceId);
        if (device != null) {
            DriverProfile driverProfile = driverProfileRepository.findOneByDeviceProfileIdsContaining(device.getDeviceProfileId());
            DriverInstance driverInstance = driverInstanceRepository.findOneByProfileId(driverProfile.getId());

            if (driverInstance != null) {
                request.setDriverId(driverInstance.getId());
                return driverInstance.getRequestQueueName();
            }
        }
        return null;
    }

    public String routeRequestToFirstOneDriver(AbstractRoutedDriverRequest request) {
        List<DriverInstance> driverInstances = driverInstanceRepository.findAll();
        if (!driverInstances.isEmpty()) {
            request.setDriverId(driverInstances.get(0).getId());
            return driverInstances.get(0).getRequestQueueName();
        } else {
            return null;
        }
    }

    public CreateIssueDriverRequest getIssueRequestToDriver(Issue issue, StringBuilder errorMessage) {
        CreateIssueDriverRequest request = new CreateIssueDriverRequest();

        if (!fillIssueRequestParams(issue, request, errorMessage)) {
            return null;
        }

        String driverRequestQueueName;
        switch (issue.getAction().getEntityType()) {
        case DEVICE:
        case CONTROL_DEVICE:
            driverRequestQueueName = routeRequestToDriverByDeviceId(request, request.getDeviceId());
            break;
        default:
            /* TODO: реализовать отправку всем драйверам когда их будет несколько */
            driverRequestQueueName = routeRequestToFirstOneDriver(request);
            break;
        }

        if (driverRequestQueueName == null || request.getDriverId() == null) {
            errorMessage.append("Не найден драйвер для выполнения задачи");
            return null;
        }

        request.setDriverRequestQueueName(driverRequestQueueName);
        return request;
    }

    private boolean fillIssueRequestParams(Issue issue, CreateIssueDriverRequest request, StringBuilder errorMessage) {
        switch (issue.getAction()) {
            case WRITE_SKUD_DATABASE:
                if (!fillWriteSkudDBParams(issue, request, errorMessage)) {
                    return false;
                }
                break;
            case PERFORM_INDICATOR_SCENARIO_ACTION:
                if (!fillPerformIndicatorScenarioActionParams(issue, request, errorMessage)) {
                    return false;
                }
                break;
            case SET_DEVICE_STATE:
                if (!fillStateNumberParam(issue, errorMessage) ||
                        !fillDeviceAndRegionIndexParams(issue, errorMessage)){
                    return false;
                }
                break;
            case SET_REGION_STATE:
                if (!fillStateNumberParam(issue, errorMessage) ||
                        !fillRegionIndexParam(issue, errorMessage)){
                    return false;
                }
                break;
            case SET_APARTMENT_STATE:
                if (!fillStateNumberParam(issue, errorMessage)){
                    return false;
                }
                break;
            case SET_DEVICE_MONITORABLE_VALUE:
                if (!fillDeviceAndRegionIndexParams(issue, errorMessage)){
                    return false;
                }
                break;
            default:
                break;
        }
        request.setProjectId(issue.getProjectId());
        request.setDeviceId(issue.getDeviceId());
        request.setIssue(issue);
        return true;
    }

    private boolean fillWriteSkudDBParams(Issue issue, CreateIssueDriverRequest request, StringBuilder errorMessage) {
        WriteSkudDatabaseParams params = new WriteSkudDatabaseParams();

        /* Извлекаем и проверяем список приборов */
        Object oControlDeviceIds = issue.getParameters().get(IssueParameterNames.controlDeviceIds.toString());
        if (oControlDeviceIds == null) {
            errorMessage.append("Отсутствует параметр со списком приборов для записи БД СКУД");
            return false;
        }
        if (!(oControlDeviceIds instanceof List<?>)) {
            errorMessage.append("Неверный формат параметра controlDeviceIds: ")
                    .append(oControlDeviceIds.getClass().getName());
            return false;
        }
        List<?> controlDeviceIds = (List<?>) oControlDeviceIds;
        if (controlDeviceIds.isEmpty()) {
            errorMessage.append("Список приборов пуст");
            return false;
        }
        for (Object oControlDeviceId : controlDeviceIds) {
            if (!(oControlDeviceId instanceof String)) {
                errorMessage.append("Неверный формат идентификатора прибора: ")
                        .append(oControlDeviceId.getClass().getName());
                return false;
            }
            String controlDeviceId = (String) oControlDeviceId;
            if (!deviceRepository.existsByProjectIdAndDeviceCategoryAndId(issue.getProjectId(), DeviceCategory.CONTROL,
                    controlDeviceId)) {
                errorMessage.append("Прибор не найден: ").append(controlDeviceId);
                return false;
            }

            params.getControlDeviceIds().add(controlDeviceId);
        }

        /* Заполняем набор сущностей СКУД */
        params.setEmployees(employeeService.get(issue.getProjectId(), errorMessage));
        if (params.getEmployees() == null) {
            return false;
        }
        params.setAccessKeys(accessKeyService.get(issue.getProjectId(), errorMessage));
        if (params.getAccessKeys() == null) {
            return false;
        }
        params.setWorkSchedules(workScheduleService.get(issue.getProjectId(), errorMessage));
        if (params.getWorkSchedules() == null) {
            return false;
        }
        issue.getInjectedParameters().writeSkudDatabaseParams = params;
        return true;
    }

    private boolean fillPerformIndicatorScenarioActionParams(Issue issue, CreateIssueDriverRequest request, StringBuilder errorMessage) {
        /* TODO:
        Object oIndicatorPanelId = issue.getParameters().get(IssueParameterNames.indicatorPanelId);
        Object oColNo = issue.getParameters().get(IssueParameterNames.colNo);
        Object oRowNo = issue.getParameters().get(IssueParameterNames.rowNo);
        if (oIndicatorPanelId == null || oColNo == null || oRowNo == null) {
            errorMessage.append("Не достаточно параметров для выполнения задачи");
            return false;
        }
        if (!(oIndicatorPanelId instanceof String) || !(oColNo instanceof Integer)
                || !(oRowNo instanceof Integer)) {
            errorMessage.append("Неверный формат параметров задачи");
            return false;
        }
        String inducatorPanelId = (String) oIndicatorPanelId;
        int indicatorColNo = (int) oColNo;
        int indicatorRowNo = (int) oRowNo;
        IndicatorPanel = ...
        */
        if (EntityType.valueOf(issue.getParameters().get("entityType").toString()) == EntityType.SCENARIO){
            try {
                Object scenarioId = ((List<?>)issue.getParameters().get("entityIds")).get(0);
                issue.getParameters().put("scenarioNo",
                        (scenarioRepository.findByProjectIdAndId(issue.getProjectId(), String.valueOf(scenarioId))).getGlobalNo());
            } catch (ClassCastException ex) {
                errorMessage.append("Неправильно указан идентификатор сценария");
                return false;
            }
        }
        return true;
    }

    private boolean fillDeviceAndRegionIndexParams(Issue issue, StringBuilder errorMessage){
        Device device = deviceRepository.findByProjectIdAndId(issue.getProjectId(), issue.getDeviceId());
        if (device == null){
            errorMessage.append("Устройство не найдено");
            return false;
        }
        DeviceProfile deviceProfile = deviceProfileRepository.findOne(device.getDeviceProfileId());
        if (deviceProfile == null){
            errorMessage.append("Профиль устройства не найден");
            return false;
        }
        issue.getParameters().put(IssueParameterNames.regionId.toString(), device.getRegionId());
        if (!fillRegionIndexParam(issue, errorMessage)){
            return false;
        }
        issue.getParameters().put(IssueParameterNames.deviceNo.toString(), device.getLineAddress());
        issue.getParameters().put(IssueParameterNames.deviceType.toString(), deviceProfile.getDeviceType());
        issue.getParameters().put(IssueParameterNames.withPostFix.toString(),
                !deviceProfile.getDeviceType().getOnOffKeyword().isEmpty());
        return true;
    }

    private boolean fillRegionIndexParam(Issue issue, StringBuilder errorMessage){
        String regionId = issue.getParameters().get(IssueParameterNames.regionId.toString()).toString();
        Region region = regionRepository.findByProjectIdAndId(issue.getProjectId(), regionId);
        if (region == null){
            errorMessage.append("Зона не найдена");
            return false;
        }
        issue.getParameters().put(IssueParameterNames.regionIndex.toString(), region.getIndex());
        return true;
    }

    private boolean fillStateNumberParam(Issue issue, StringBuilder errorMessage){
        State state = stateRepository.findOne(issue.getParameters()
                .get(IssueParameterNames.stateId.toString()).toString());
        if (state == null){
            errorMessage.append("Состояние не найдено");
            return false;
        }
        issue.getParameters().put(IssueParameterNames.stateNumber.toString(), state.getNumber());
        return true;
    }

    public List<Issue> saveIssuesByDriverResponse(CreateIssueDriverResponse response, User user, StringBuilder errorMessage) {
        if (response.getError() != ErrorType.OK) {
            errorMessage.append(response.getErrorString());
            return null;
        } else {
            for (Issue issue : response.getActualIssues()) {
                try {
                    issue.setUserId(user.getId());
                } catch (Exception ex){
                    logger.error("Couldn't get user id", ex);
                }
            }
            return issueRepository.insert(response.getActualIssues());
        }
    }

    public Issue setIssueProgress(String issueId, IssueStatus status, String statusMessage, double progress) {
        Issue issue = issueRepository.findOne(issueId);
        issue = setIssueProgress(issue, status, statusMessage, progress);
        if (issue == null)
            logger.error("Issue \"{}\" not found", issueId);
        return issue;
    }

    public Issue setIssueProgress(Issue issue, IssueStatus status, String statusMessage, double progress) {
        if (issue != null) {
            /* Если пришёл статус NONE, то нужно обновить прогресс задачи.
             * Иначе обновить нужно именно статус.
             */
            if (status == IssueStatus.NONE) {
                /* Обновляем прогресс если изменение польше порога
                 * или при перезапуске задачи
                 */
                if (progress < issue.getProgress() || progress - issue.getProgress() > 5) {
                    issue.setProgress(progress);
                }
            } else {
                issue.setStatus(status);
                issue.setStatusMessage(statusMessage);
                switch (issue.getStatus()) {
                case FINISHED:
                case FAILED:
                    issue.setProgress(100);
                    issue.setFinishDateTime(new Date());
                    if (issue.getStartDateTime().getTime() == 0) issue.setStartDateTime(issue.getFinishDateTime());
                    break;
                case IN_PROGRESS: {
                    issue.setStartDateTime(new Date());
                    break;
                }
                default:
                    break;
                }
            }
            issue = issueRepository.save(issue);
        }
        return issue;
    }

    public void subscribe(IssueProgressHandler issueProgressHandler) {
        issueProgressHandlers.add(issueProgressHandler);
    }

    public void unsubscribe(IssueProgressHandler issueProgressHandler) {
        issueProgressHandlers.remove(issueProgressHandler);
    }

    public void onIssueProgress(IssueProgress issueProgress) {
        if (!issueProgressHandlers.isEmpty()) {
            Issue issue = issueRepository.findOne(issueProgress.getIssueId());
            if (issue != null) {
                for (IssueProgressHandler issueProgressHandler : issueProgressHandlers) {
                    issueProgressHandler.onIssueProgress(issue, issueProgress);
                }
            }
        }
    }

    public void addSubsystemTags(Issue issue){
        FilterTags filterTags = issue.getFilterTags();

        if (filterTags == null){
            issue.setFilterTags(new FilterTags());
            filterTags = issue.getFilterTags();
        }

        Set<String> availableSubsystems = new HashSet<>();
        String projectId = issue.getProjectId();

        switch (issue.getAction().getEntityType()){
            case CONTROL_DEVICE:
            case DEVICE:
                FilterTags deviceFilterTags = deviceRepository.findByProjectIdAndId(projectId,
                        issue.getDeviceId()).getFilterTags();
                if (deviceFilterTags != null) {
                    availableSubsystems.addAll(deviceFilterTags.chooseSubsystemTags());
                }
                break;
            case REGION:
                availableSubsystems.add(regionRepository.findByProjectIdAndId(projectId,
                        issue.getParameters().get("regionId").toString()).getSubsystem().toString());
                break;
            case SCENARIO:
                FilterTags scenarioFilterTags = scenarioRepository.findByProjectIdAndId(projectId,
                        issue.getParameters().get("scenarioNo").toString()).getFilterTags();
                if (scenarioFilterTags != null){
                    availableSubsystems.addAll(scenarioFilterTags.chooseSubsystemTags());
                }
                break;
            case VIRTUAL_INDICATOR:
                Map<String, Object> issueParameters = issue.getParameters();
                IndicatorPanel indicatorPanel = indicatorPanelRepository.findByProjectIdAndId(projectId,
                        issueParameters.get(IssueParameterNames.indicatorPanelId.toString()).toString());
                FilterTags indicatorFilterTags = indicatorPanel.getIndicators()
                        .get((int)issueParameters.get(IssueParameterNames.rowNo))
                        .get((int)issueParameters.get(IssueParameterNames.colNo))
                        .getFilterTags();
                if (indicatorFilterTags != null) {
                    availableSubsystems.addAll(indicatorFilterTags.chooseSubsystemTags());
                }
                break;
            case NONE:
                availableSubsystems.add(Subsystem.GENERAL.toString());
                break;
        }

        filterTags.getTags().addAll(availableSubsystems);
    }
}
