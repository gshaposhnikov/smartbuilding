package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.dao.v1.ProjectEntityRepository;
import ru.rubezh.firesec.nt.dao.v1.ProjectRepository;
import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.ProjectEntitiesContainer;

public abstract class ProjectEntityService<T extends BasicEntityWithDate, R extends ProjectEntityRepository<T>> {

    @Autowired
    protected R entityRepository;

    @Autowired
    protected ProjectRepository projectRepository;

    /**
     * Получить все сущности проекта.
     * 
     * Сущности, отмеченные на удаление, не должны войти в результирующий список.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return список учетных записей проекта или null в случае ошибки
     */
    public List<T> get(String projectId, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return null;
        }

        return entityRepository.findByProjectIdAndRemovedIsFalse(projectId);
    }

    /**
     * Получить сущность по идентификатору.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param entityId
     *            идентификатор сущности
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return экземпляр сущности или null в случае ошибки
     */
    public T get(String projectId, String entityId, StringBuilder errorMessage) {
        if (!projectRepository.exists(projectId)) {
            errorMessage.append("Проект не найден");
            return null;
        }

        T entity = entityRepository.findByProjectIdAndIdAndRemovedIsFalse(projectId, entityId);
        if (entity == null) {
            errorMessage.append("Объект не найден в проекте");
        }

        return entity;
    }

    /**
     * Добавить сущность в проект.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param entity
     *            данные сущности
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    public abstract boolean add(String projectId, T entity, StringBuilder errorMessage);

    /**
     * Изменить данные сущности.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param entityId
     *            идентификатор сущности
     * @param entity
     *            новые данные сущности
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    public abstract boolean update(String projectId, String entityId, T entity, StringBuilder errorMessage);

    /**
     * Удалить сущность из проекта.
     * 
     * Сущность должна быть только отмечена к удалению.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param entityId
     *            идентификатор сущности
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    public abstract boolean remove(String projectId, String entityId, StringBuilder errorMessage);

    /**
     * Получить изменения в промежутке указанного времени (все созданные, измененные и удаленные сущности).
     * 
     * @param from
     *            момент времени, изменения от которого запрашиваются
     * @param upTo
     *            момент времени, изменения до которого запрашиваются
     * @return контейнер измененных сущностей
     */
    public UpdatedEntities.ProjectEntitiesContainer<T> getChanges(Date from, Date upTo) {
        /* Заполняем изменения */
        ProjectEntitiesContainer<T> changes = new ProjectEntitiesContainer<>();
        changes.setCreated(
                entityRepository.findByRemovedIsFalseAndCreateDateTimeBetween(from, upTo));
        changes.setUpdated(
                entityRepository.findByRemovedIsFalseAndUpdateDateTimeBetween(from, upTo));
        changes.setDeleted(
                entityRepository.findByRemovedIsTrueAndUpdateDateTimeBetween(from, upTo));

        return changes;
    }

    /**
     * Выполнить фактическое удаление отмеченных для этого сущностей.
     * 
     * @param upTo верхняя граница времени: будут удалены только те сущности, которые были отмечены на удаление до этого
     *            времени.
     */
    public void doRemove(Date upTo) {
        entityRepository.deleteByRemovedIsTrueAndUpdateDateTimeBefore(upTo);
    }

    /**
     * Признак необходимости удаления отмеченных на удаления сущностей сразу после уведомления верхнего уровня.
     * 
     * @return удалять или нет
     */
    public boolean shouldRemoveOnUpLevelNotified() {
        return true;
    }

    /**
     * Название поля, в котором будут сохраняться изменения при отправке уведомлений.
     * 
     * @return название поля (ключа)
     */
    public abstract String getKeyForUpdatedEntities();

}
