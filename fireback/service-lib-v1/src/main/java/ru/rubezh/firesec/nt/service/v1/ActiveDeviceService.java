package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceMonitorableValues;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.ActiveRegion;
import ru.rubezh.firesec.nt.domain.v1.Device;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс сервисных функций для работы с оперативными сущностями устройств.
 * @author Антон Васильев
 *
 */
public interface ActiveDeviceService extends ActiveEntityWithStatesService {

    /**
     * Добавить активное состояние устройства.
     * 
     * Метод также запоминает текущее время в качестве времени последнего получения состояния устройства, актуализирует
     * текущую категорию состояния, если набор состояний изменился, генерирует авто-события, применяет цепочки
     * наследования состояний другими сущностями.
     * 
     * @param deviceId идентификатор устройства
     * @param stateId идентификатор состояния
     */
    public void addActiveState(String deviceId, String stateId);

    /**
     * Добавить активное состояние устройства.
     * 
     * Метод также запоминает текущее время в качестве времени последнего получения состояния устройства, актуализирует
     * текущую категорию состояния, если набор состояний изменился, генерирует авто-события, применяет цепочки
     * наследования состояний другими сущностями.
     * 
     * @param device оперативная сущность устройства 
     * @param stateId идентификатор состояния
     */
    public void addActiveState(ActiveDevice device, String stateId);

    /**
     * Удалить активное состояние устройства.
     * 
     * Метод также запоминает текущее время в качестве времени последнего получения состояния устройства, актуализирует
     * текущую категорию состояния, если набор состояний изменился, генерирует авто-события, применяет цепочки
     * наследования состояний другими сущностями.
     *  
     * @param deviceId идентификатор устройства
     * @param stateId идентификатор состояния
     */
    public void removeActiveState(String deviceId, String stateId);

    /**
     * Удалить активное состояние устройства.
     * 
     * Метод также запоминает текущее время в качестве времени последнего получения состояния устройства, актуализирует
     * текущую категорию состояния, если набор состояний изменился, генерирует авто-события, применяет цепочки
     * наследования состояний другими сущностями.
     *  
     * @param device оперативная сущность устройства 
     * @param stateId идентификатор состояния
     */
    public void removeActiveState(ActiveDevice device, String stateId);

    /**
     * Установить устройству активные состояния, полученные по опросу.
     * 
     * Если устройство содержит состояния - причины событий или выставленные логикой, то они остаются.
     * 
     * Метод также запоминает текущее время в качестве времени последнего получения состояния устройства, актуализирует
     * текущую категорию состояния, если набор состояний изменился, генерирует авто-события, применяет цепочки
     * наследования состояний другими сущностями.
     * 
     * @param deviceId идентификатор устройства
     * @param stateIds набор идентификаторов состояний
     */
    public void setActivePollingStates(String deviceId, Collection<String> stateIds);

    public void setActivePollingStates(ActiveDevice device, Collection<String> stateIds);

    /**
     * Обновить счетчик полученных с прибора событий.
     * @param deviceId идентификатор устройства (прибора)
     * @param logTypeId идентификатор журнала
     * @param count новое значение счетчика
     */
    public void updateEventCount(String deviceId, String logTypeId, long count);

    /**
     * Получить оперативную сущность устройства.
     * @param controlDevice прибор, через который получены данные
     * @param controlDeviceAddress адрес прибора-источника данных
     * @param deviceLineNo номер линии устроства
     * @param deviceAddress адрес на линии устроства
     * @return оперативную сущность устройства или null
     */
    public ActiveDevice getChildDevice(Device controlDevice, int controlDeviceAddress, int deviceLineNo, int deviceAddress);

    /**
     * Получить оперативную сущность зоны, которая содержит данное устройство.
     * @param device оперативная сущность устройства
     * @return найденную оперативную сущность зоны или null
     */
    public ActiveRegion getContainingActiveRegion(ActiveDevice device);

    /**
     * Установить GUID для устройства.
     * @param deviceId идентификатор  устройства
     * @param guid GUID который требуется установить
     * @return true если изменился GUID контрольного прибора
     */
    public boolean setDeviceGuid(String deviceId, String guid);

    /**
     * Установить активную конфигурацию устройства.
     * @param deviceId идентификатор устройства
     * @param propertyValues значения параметров конфигурации
     * @param serialNumber заводской номер устройства устройства
     * @param firmwareVersion версия встроенного ПО (прошивки) устройства
     * @param databaseVersion версия базы данных прибора
     * @param readDateTime время чтения конфигурации из устройств (null если запись)
     * @param writeDateTime время записи конфигурации в устройство (null если чтение)
     */
    public void setDeviceActiveConfig(String deviceId, Map<String, String> propertyValues,
            String serialNumber, String firmwareVersion, String databaseVersion, Date readDateTime, Date writeDateTime);

    /**
     * Установить значения наблюдаемых параметров устройства.
     * @param message оповещение с информацией об устройстве и наблюдаемых параметрах
     */
    public void setMonitorableValues(SetActiveDeviceMonitorableValues message);

    /**
     * Добавить заметку по устройству
     * @param deviceId идентификатор устройства
     * @param userName имя пользователя, сделавшего заметку
     * @param note текст заметки
     * @return устройство, в которое была добавлена заметка
     */
    public ActiveDevice addNoteToDevice(String deviceId, String userName, ActiveDevice.Note note);

    /**
     * Найти прибор, которому принадлежит устройство.
     * 
     * @param device устройство, чей прибор надо найти
     * @param devicesHash хеш устройств
     * @return оперативная сущность найденного прибора или null
     */
    public ActiveDevice findControlDevice(ActiveDevice device, Map<String, ActiveDevice> devicesHash);

    /**
     * Change states of all project devices and regions to "off"
     * */
    void setOffStateOnAllEntities();

    /**
     * Set devices states.
     * @param deviceInfoWithStateIds device info and a state id to set.
     */
    void setActivePollingStates(Map<String, Integer> deviceInfoWithStateIds);

    /**
     * Set "off" state on regions
     * @param offRegionNos numbers of regions with "off" state.
     */
    void setOffStateByRegions(Set<Integer> offRegionNos);
}
