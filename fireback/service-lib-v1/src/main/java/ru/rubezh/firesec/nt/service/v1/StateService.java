package ru.rubezh.firesec.nt.service.v1;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.StateRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;
import ru.rubezh.firesec.nt.domain.v1.State;
import ru.rubezh.firesec.nt.domain.v1.StateView;

@Service
public class StateService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    private StateRepository stateRepository;

    private Map<String, State> statesCache;

    public void fetchStates(DriverProfileView driverProfileView) {
        /* Обнуляем кэш */
        statesCache = null;

        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, State.class);
        /* Сначала удаляем старые версии состояний */
        for (StateView stateView: driverProfileView.getStateViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(stateView.getState().getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии состояний */
        for (StateView stateView: driverProfileView.getStateViews())
            bulkOps.insert(stateView.getState());
        bulkOps.execute();
    }
    
    public State getById(String id) {
        return stateRepository.findOne(id);
    }

    public List<State> getByIdIn(List<String> ids) {
        return stateRepository.findByIdIn(ids);
    }

    public Map<String, State> getAllHashed() {
        /* TODO: проверять HASH коллекции states */
        if (statesCache == null) {
            statesCache = stateRepository.findAll().stream().collect(Collectors.toMap(State::getId, state -> state));
        }
        return statesCache;
    }

    public Collection<State> getAll() {
        /* TODO: проверять HASH коллекции states */
        if (statesCache == null) {
            statesCache = stateRepository.findAll().stream().collect(Collectors.toMap(State::getId, state -> state));
        }
        return statesCache.values();
    }

}
