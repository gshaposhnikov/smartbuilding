package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.ActiveDeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.EventRepository;
import ru.rubezh.firesec.nt.dao.v1.FireExtinctionNotificationRepository;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.Event;
import ru.rubezh.firesec.nt.domain.v1.FireExtinctionNotification;

@Service("fireExtinctionNotificationService")
public class FireExtinctionNotificationService implements UpsertEntitiesProcessor {

    @Autowired
    private Logger logger;

    @Autowired
    private EventRepository eventRepository;
    
    @Autowired
    private FireExtinctionNotificationRepository fireExtinctionNotificationRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {

        List<Event> eventsToCheck = eventRepository.findByIdAfterAndOnlineIsTrueAndFireExtinctionEventNotNull(
                observeFromOid, new Sort(Direction.ASC, "_id"));

        if (eventsToCheck.isEmpty())
            return observeFromOid;

        /* Список событий отсортирован по id, поэтому последний ObjectId берем от последнего события в списке */
        ObjectId lastObservedOid = new ObjectId(eventsToCheck.get(eventsToCheck.size() - 1).getId());

        eventsToCheck = eventsToCheck.stream()
                .filter(event -> event.getDeviceId() != null && !event.getDeviceId().isEmpty())
                .collect(Collectors.toList());

        if (!eventsToCheck.isEmpty()) {
            List<FireExtinctionNotification> notifications = generateFireExtinctionNotifications(eventsToCheck);
            if (notifications != null && !notifications.isEmpty()) {
                fireExtinctionNotificationRepository.save(notifications);
                /*
                 * TODO: реализовать REST для уведомлений о пожаротушении и загрузку актуальных уведомлений в
                 * ВЕБ-клиенте
                 */
            }
        }

        return lastObservedOid;
    }

    /**
     * Сгенерировать оповещения о пожаротушении по событиям, считанным из журнала прибора.
     * 
     * @param events События
     * @return Оповещения о пожаротушении или null
     */
    private List<FireExtinctionNotification> generateFireExtinctionNotifications(List<Event> events) {
        /* оставляем только события с информацией о пожаротушении */
        List<Event> fireExtinctionEvents = events.stream().filter(event -> event.getFireExtinctionEvent() != null)
                .collect(Collectors.toList());
        if (fireExtinctionEvents.isEmpty())
            return null;

        /* выберем все необходимые устройства */
        List<String> deviceIds = fireExtinctionEvents.stream().map(Event::getDeviceId).collect(Collectors.toList());
        Map<String, ActiveDevice> deviceMap = activeDeviceRepository.findByIdIn(deviceIds).stream()
                .collect(Collectors.toMap(ActiveDevice::getId, device -> device));


        /* формирование оповещения о пожаротушении для событий, содержащих такую информацию */
        List<FireExtinctionNotification> notifications = new ArrayList<>();
        for (Event event : fireExtinctionEvents) {
            ActiveDevice activeDevice = deviceMap.get(event.getDeviceId());
            if (activeDevice == null)
                logger.error("Didn't find device for fire extinction event. Event id: {}", event.getId());
            else {
                FireExtinctionNotification notification = new FireExtinctionNotification();
                notification.setDeviceId(activeDevice.getId());
                notification.setRegionId(activeDevice.getRegionId());
                notification.setEvent(event.getFireExtinctionEvent());
                notification.setDelay(activeDevice.getFireExtinctionDelay());
                notifications.add(notification);
            }
        }

        return notifications;
    }

}
