package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType;
import ru.rubezh.firesec.nt.domain.v1.DriverProfile;

@Service
public class ScenarioServiceImpl implements ScenarioService {
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void fetchScenarioTriggerTypes(DriverProfile driverProfile) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, ScenarioTriggerType.class);
        /* Сначала удаляем старые версии типов триггеров */
        for (ScenarioTriggerType triggerType: driverProfile.getScenarioTriggerTypes())
            bulkOps.remove(new Query(Criteria.where("_id").is(triggerType.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии типов триггеров */
        bulkOps.insert(driverProfile.getScenarioTriggerTypes());
        bulkOps.execute();
    }

    @Override
    public void fetchScenarioActionTypes(DriverProfile driverProfile) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, ScenarioActionType.class);
        /* Сначала удаляем старые версии типов действий */
        for (ScenarioActionType actionType: driverProfile.getScenarioActionTypes())
            bulkOps.remove(new Query(Criteria.where("_id").is(actionType.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии типов действий */
        bulkOps.insert(driverProfile.getScenarioActionTypes());
        bulkOps.execute();
    }

}
