package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.Indicator;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;

import java.util.List;

public interface IndicatorPanelService {

    /**
     * Добавляет панель индикаторов.
     *
     * Панель индикаторов создает двумерный массив с пустыми индикаторами,
     * размер которого определяется пользователем. Панель создается только
     * в случае, когда ее размеры удовлетворяют требуемым условиям.
     *
     * @param indicatorPanel параметры создаваемой панели
     * @param projectId id проекта
     * @param errorMessage сообщение возможной ошибки
     * @return созданная панель индикаторов
     */
    IndicatorPanel addNew(IndicatorPanel indicatorPanel, String projectId, StringBuilder errorMessage);

    /**
     * Изменяет параметры панели индикаторов.
     *
     * Если размеры массива индикаторов в новых параметрах
     * заданы неправильно, то панель не будет изменена.
     *
     * @param indicatorPanel новые параметры панели
     * @param id идентификатор изеняемой панели
     * @param projectId идентификатор проекта
     * @param errorMessage сообщение возможной ошибки
     * @return измененная панель индикаторов
     */
    IndicatorPanel update(IndicatorPanel indicatorPanel, String id, String projectId, StringBuilder errorMessage);

    /**
     * Изменяет параметры индикатора(ячейки панели индикаторов).
     *
     * @param indicator новые параметры индикатора
     * @param id идентификатор панели, в которой находится индикатор
     * @param colNo номер столбца изменяемого индикатора
     * @param rowNo номер строки изменяемого индикатора
     * @param errorMessage сообщение возможной ошибки
     * @return измененная панель индикаторов
     */
    IndicatorPanel updateIndicator(Indicator indicator, String projectId,
                                   String id, int colNo, int rowNo, StringBuilder errorMessage);

    /**
     * Удаляет панель индикаторов.
     *
     * @param projectId идентификатор проекта
     * @param id идентификатор удаляемой панели индикаторов
     * @param errorMessage сообщение возможной ошибки
     * @return true, если панель успешно удалена, иначе false
     */
    boolean remove(String projectId, String id, StringBuilder errorMessage);

    /**
     * Убирает id удаленных сущностей из индикаторов всех панелей проекта.
     *
     * @param projectId id проекта
     * @param entityIds id сущностей, которые надо удалить
     * @param entityType тип удаляемых сущностей
     * @return список панелей индикаторов, в которых были удалены id
     */
    List<IndicatorPanel> removeEntityIdsFromIndicatorPanels(String projectId,
                                                            List<String> entityIds, EntityType entityType);
}
