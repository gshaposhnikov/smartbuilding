package ru.rubezh.firesec.nt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class AM1ExampleApplication implements ApplicationListener<ContextClosedEvent> {

    /* планировщик */
    
    @Autowired ThreadPoolTaskScheduler scheduler;

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        scheduler.shutdown();
    }
    
    /* main */

    public static void main(String[] args) {
        new AM1ExampleApplication().run();
    }

    public void run() {
        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml")) {
            while(true) Thread.sleep(4000);
        }
        catch (InterruptedException e) {
            System.out.println("Closed by interrupt");
        }
    }
}
