package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.nio.ByteBuffer;

import org.apache.logging.log4j.Logger;
import org.hid4java.HidServices;


import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.USBRS485AdapterModule;
import ru.rubezh.firesec.nt.plugin.driver.Driver;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.message.MessageCodec;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.clock.SingleClientClock;
import ru.rubezh.firesec.nt.plugin.driver.transport.RubezhUSBTransport;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;

public class DriverImpl extends Driver {
    
    private HidServices hidServices;
    private MessageCodec messageCodec;
    private int vendorId = 0xffffc251;
    private int productId = 0x1303;

    public DriverImpl(Logger logger, PlanRepository planRepository, DeviceRepository deviceRepository, HidServices hidServices, MessageCodec messageCodec) {
        super(logger, planRepository, deviceRepository);
        this.hidServices = hidServices;
        this.messageCodec = messageCodec;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    protected StateMachine getStateMachine(Plan plan, Device device) {
        if (device instanceof USBRS485AdapterModule) {
            USBRS485AdapterModule adapterModule = (USBRS485AdapterModule) device;
            Clock clock = new SingleClientClock();
            Transport<ByteBuffer> transport = new RubezhUSBTransport(logger, hidServices, vendorId, productId, adapterModule.getSerialNumber(), adapterModule.getModelName());
            //NOTE: временно хардкодим 0-вую линию для МС-1
            StateMachineContext stateMachineContext = new StateMachineContextImpl(logger, transport, messageCodec, clock, deviceRepository, adapterModule, 0);
            StateMachine stateMachine = new StateMachineImpl(stateMachineContext);
            return stateMachine;
        }
        else
            return null;
    }

}
