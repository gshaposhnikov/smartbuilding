package ru.rubezh.firesec.nt;

import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.dao.DeviceRepository;
import ru.rubezh.firesec.nt.dao.PlanRepository;
import ru.rubezh.firesec.nt.domain.v0.AdminStatus;
import ru.rubezh.firesec.nt.domain.v0.Panel;
import ru.rubezh.firesec.nt.domain.v0.Plan;
import ru.rubezh.firesec.nt.domain.v0.USBRS485AdapterModule;

public class DeviceTreeInitializer {

    @Autowired
    public DeviceTreeInitializer(PlanRepository planRepository, DeviceRepository deviceRepository) {
        deviceRepository.deleteAll();
        planRepository.deleteAll();
        Plan plan = new Plan();
        plan.setAdminStatus(AdminStatus.on);
        plan = planRepository.insert(plan);
        USBRS485AdapterModule adapterModule = new USBRS485AdapterModule();
        adapterModule.setSerialNumber("111111111111");
        adapterModule.setPlanId(plan.getId());
        adapterModule = deviceRepository.insert(adapterModule);
        Panel panel = new Panel();
        panel.setPlanId(plan.getId());
        panel.setParentDeviceId(adapterModule.getId());
        deviceRepository.insert(panel);
    }
}
