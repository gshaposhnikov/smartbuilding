// mongeez formatted javascript

// changeset Anton_Karavaev:FS-619

//создание уникального составного индекса
indexes = db.devices.getIndexes();
if (indexes.length > 0){
    projectIdAndAddressPath = indexes.find(i => i.key.projectId && i.key.addressPath)
    if (!projectIdAndAddressPath){
        projectId = indexes.find(i  => i.key.projectId);
        addressPath = indexes.find(i  => i.key.addressPath);
        db.devices.dropIndex(projectId.name);
        db.devices.dropIndex(addressPath.name);
        db.devices.createIndex({"projectId": 1, "addressPath": 1}, {unique: true});
    }
}