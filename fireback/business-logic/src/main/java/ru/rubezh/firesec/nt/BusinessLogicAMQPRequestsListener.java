package ru.rubezh.firesec.nt;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ru.rubezh.firesec.nt.amqp.listener.RequestListener;
import ru.rubezh.firesec.nt.amqp.message.request.*;
import ru.rubezh.firesec.nt.amqp.message.response.*;
import ru.rubezh.firesec.nt.amqp.sender.DriverRoutedRequestSender;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;

/**
 * Класс для получения запросов.
 *
 * @author Александр Горячкин
 */
public class BusinessLogicAMQPRequestsListener extends RequestListener {

    @Autowired
    private DriverInstanceRepository driverInstanceRepository;
    @Autowired
    private DriverProfileRepository driverProfileRepository;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private DriverRoutedRequestSender driverRoutedRequestSender;

    @Autowired
    public BusinessLogicAMQPRequestsListener(Logger logger) {
        super(logger);
    }

    private String routeRequestToDriver(AbstractDriverRequest request, String deviceId) {
        Device device = deviceRepository.findOne(deviceId);
        if (device != null) {
            DriverProfile driverProfile = driverProfileRepository.findOneByDeviceProfileIdsContaining(device.getDeviceProfileId());
            DriverInstance driverInstance = driverInstanceRepository.findOneByProfileId(driverProfile.getId());

            if (driverInstance != null) {
                request.setDriverId(driverInstance.getId());
                return driverInstance.getRequestQueueName();
            }
        }
        return null;
    }

    SetDeviceConfigResponse handleMessage(SetDeviceConfigRequest request) {
        logger.debug("Message of set config on device received. Project id: {}, device id: {}",
                request.getProjectId(), request.getDeviceId());

        SetDriverDeviceConfigRequest subRequest = new SetDriverDeviceConfigRequest();
        subRequest.setProjectId(request.getProjectId());
        subRequest.setDeviceId(request.getDeviceId());
        subRequest.setActiveConfigValues(request.getActiveConfigValues());

        String driverRequestQueueName = routeRequestToDriver(subRequest, request.getDeviceId());
        SetDeviceConfigResponse response = new SetDeviceConfigResponse();
        if (driverRequestQueueName == null || subRequest.getDriverId() == null) {
            response.setError(ErrorType.UNKNOWN_DRIVER);
            response.setErrorString("Can't find appropriate driver for device");
        } else {
            try {
                SetDriverDeviceConfigResponse subResponse = (SetDriverDeviceConfigResponse) driverRoutedRequestSender
                        .sendAndReceive(driverRequestQueueName, subRequest);
                if (subResponse != null) {
                    response.setError(subResponse.getError());
                    response.setErrorString(subResponse.getErrorString());
                } else {
                    response.setError(ErrorType.INTERNAL_ERROR);
                    response.setErrorString("Response is null");
                }
            } catch (ClassCastException e) {
                response.setError(ErrorType.INTERNAL_ERROR);
                response.setErrorString("Response has bad type");
            }
        }
        return response;
    }

}
