package ru.rubezh.firesec.nt;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.domain.v1.EventType;
import ru.rubezh.firesec.nt.service.v1.EventService;

@Component
public class EventTypeSaver {

    @Autowired
    public EventTypeSaver(@NotNull ApplicationContext applicationContext,
                          @NotNull EventService eventService) {
        Map<String, EventType> eventTypes = applicationContext.getBeansOfType(EventType.class);
        eventService.addEventTypes(new ArrayList<>(eventTypes.values()));
    }

}
