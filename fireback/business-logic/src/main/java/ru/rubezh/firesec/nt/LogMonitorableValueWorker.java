package ru.rubezh.firesec.nt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.rubezh.firesec.nt.dao.v1.ActiveDeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.LogMonitorableValueRepository;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.LogMonitorableValue;
import ru.rubezh.firesec.nt.service.v1.LogMonitorableValueService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogMonitorableValueWorker {

    @Value("#{'${server.log_monitorable_value.life_time:1}'}")
    private long logMonitorableValueLifeTime;

    @Value("#{'${server.log_monitorable_value.remove_and_update_time:1}'}")
    private long logMonitorableValueUpdateTime;

    @Autowired
    private LogMonitorableValueRepository logMonitorableValueRepository;

    @Autowired
    private LogMonitorableValueService logMonitorableValueService;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    public void removeOldAndUpdateInactive() {
        Date currentTime = new Date();
        Date updateTime = new Date(currentTime.getTime() - logMonitorableValueUpdateTime);

        // Обновляем последние данные неактивных устройств
        List<ActiveDevice> activeDevices = activeDeviceRepository.findAll();
        Map<String, String> monitorableValues;
        for (ActiveDevice device : activeDevices) {
            monitorableValues = new HashMap<>();
            for (String monitorableValueProfileId : device.getMonitorableValues().keySet()) {
                LogMonitorableValue lastValue = logMonitorableValueRepository.
                        findFirstByProjectIdAndDeviceIdAndProfileIdOrderByReceivedDesc(
                                device.getProjectId(), device.getId(), monitorableValueProfileId);
                if (lastValue == null || lastValue.getReceived().compareTo(updateTime) <= 0) {
                    monitorableValues.put(monitorableValueProfileId,
                            device.getMonitorableValues().get(monitorableValueProfileId));
                }
            }
            if (monitorableValues.size() > 0)
                logMonitorableValueService.saveAll(device, monitorableValues, currentTime, true);
        }

        // Удаляем старые записи
        Date lifeTime = new Date(currentTime.getTime() - logMonitorableValueLifeTime);
        logMonitorableValueService.removeByReceivedBefore(lifeTime);
    }

}
