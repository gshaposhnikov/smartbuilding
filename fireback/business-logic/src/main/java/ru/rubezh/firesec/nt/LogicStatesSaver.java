package ru.rubezh.firesec.nt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.dao.v1.StateRepository;
import ru.rubezh.firesec.nt.domain.v1.State;

@Component
public class LogicStatesSaver {

    @Autowired
    public LogicStatesSaver(StateRepository stateRepository, List<State> logicStates) {
        stateRepository.delete(logicStates);
        stateRepository.insert(logicStates);
    }

}
