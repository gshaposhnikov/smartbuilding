package ru.rubezh.firesec.nt;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.dao.v1.StateCategoryRepository;
import ru.rubezh.firesec.nt.domain.v1.StateCategory;

@Component
public class StateCategorySaver {

    @Autowired
    public StateCategorySaver(@NotNull StateCategoryRepository stateCategoryRepository,
            List<StateCategory> stateCategories) {
        stateCategoryRepository.deleteAll();
        stateCategoryRepository.insert(stateCategories);
    }

}
