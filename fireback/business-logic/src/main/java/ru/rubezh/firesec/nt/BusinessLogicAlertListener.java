package ru.rubezh.firesec.nt;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import ru.rubezh.firesec.nt.amqp.listener.AlertListener;
import ru.rubezh.firesec.nt.amqp.message.alert.*;
import ru.rubezh.firesec.nt.amqp.message.request.SetActiveProjectRequest;
import ru.rubezh.firesec.nt.amqp.sender.AlertSender;
import ru.rubezh.firesec.nt.amqp.sender.DriverMessagePostProcessor;
import ru.rubezh.firesec.nt.amqp.sender.DriverRequestSender;
import ru.rubezh.firesec.nt.dao.v1.ControlDeviceEventRepository;
import ru.rubezh.firesec.nt.dao.v1.DriverInstanceRepository;
import ru.rubezh.firesec.nt.dao.v1.EventTypeRepository;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.service.v1.*;

import java.util.*;

/**
 * Класс для получения оповещений. Можно использовать как rabbit:listner, либо
 * как upsert-процессор.
 *
 * @author Александр Горячкин
 */
public class BusinessLogicAlertListener extends AlertListener implements UpsertEntitiesProcessor {

    /* TODO: убрать хардкод идентификаторов событий */
    private static final String CONTROL_DEVICE_REPLACED_EVENT_TYPE_ID = "controlDeviceReplaced";

    private RabbitTemplate rabbitAsyncTemplate;
    private Queue alertsQueue;

    private ProjectService projectService;
    private ActiveDeviceService activeDeviceService;
    private EventService eventService;
    private AlertSender alertSender;
    private DriverInstanceRepository driverInstanceRepository;
    private DriverMessagePostProcessor driverMessagePostProcessor;
    private EventTypeRepository eventTypeRepository;
    private IssueService issueService;
    private ControlDeviceEventRepository controlDeviceEventRepository;
    private HttpService httpService;

    private interface HandlerAdapter {
        void handleMessage(Object message);
    }

    private Map<String, HandlerAdapter> handlerAdapters;

    public BusinessLogicAlertListener(Logger logger, RabbitTemplate rabbitAsyncTemplate,
            Queue alertsQueue, ProjectService projectService,
            ActiveDeviceService activeDeviceService, EventService eventService,
            AlertSender alertSender, DriverInstanceRepository driverInstanceRepository,
            DriverMessagePostProcessor driverMessagePostProcessor,
            EventTypeRepository eventTypeRepository, IssueService issueService,
            ControlDeviceEventRepository controlDeviceEventRepository, HttpService httpService) {
        super(logger);
        this.rabbitAsyncTemplate = rabbitAsyncTemplate;
        this.alertsQueue = alertsQueue;
        this.projectService = projectService;
        this.activeDeviceService = activeDeviceService;
        this.eventService = eventService;
        this.alertSender = alertSender;
        this.driverInstanceRepository = driverInstanceRepository;
        this.driverMessagePostProcessor = driverMessagePostProcessor;
        this.eventTypeRepository = eventTypeRepository;
        this.issueService = issueService;
        this.controlDeviceEventRepository = controlDeviceEventRepository;
        this.httpService = httpService;
        initHandlerAdapters();
    }

    private void initHandlerAdapters() {
        handlerAdapters = new HashMap<>();
        handlerAdapters.put(DriverRegisteredAlert.class.getSimpleName(),
                msg -> handleMessage((DriverRegisteredAlert) msg));
        handlerAdapters.put(AddActiveDeviceState.class.getSimpleName(),
                msg -> handleMessage((AddActiveDeviceState) msg));
        handlerAdapters.put(RemoveActiveDeviceState.class.getSimpleName(),
                msg -> handleMessage((RemoveActiveDeviceState) msg));
        handlerAdapters.put(SetActiveDeviceStates.class.getSimpleName(),
                msg -> handleMessage((SetActiveDeviceStates) msg));
        handlerAdapters.put(SetEntityStates.class.getSimpleName(),
                msg -> handleMessage((SetEntityStates) msg));
        handlerAdapters.put(ControlDeviceEvents.class.getSimpleName(),
                msg -> handleMessage((ControlDeviceEvents) msg));
        handlerAdapters.put(SetActiveDeviceMonitorableValues.class.getSimpleName(),
                msg -> handleMessage((SetActiveDeviceMonitorableValues) msg));
        handlerAdapters.put(ControlDeviceInfo.class.getSimpleName(),
                msg -> handleMessage((ControlDeviceInfo) msg));
        handlerAdapters.put(ActiveDeviceConfig.class.getSimpleName(),
                msg -> handleMessage((ActiveDeviceConfig) msg));
        handlerAdapters.put(ActiveProjectUpdated.class.getSimpleName(),
                msg -> handleMessage((ActiveProjectUpdated) msg));
        handlerAdapters.put(ActiveProjectValidateStatus.class.getSimpleName(),
                msg -> handleMessage((ActiveProjectValidateStatus) msg));
        handlerAdapters.put(IssueProgress.class.getSimpleName(),
                msg -> handleMessage((IssueProgress) msg));
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFrom, Date currentTime) {
        Object message = rabbitAsyncTemplate.receiveAndConvert(alertsQueue.getName());
        while (message != null) {
            HandlerAdapter handlerAdapter = handlerAdapters.get(message.getClass().getSimpleName());
            if (handlerAdapter != null) {
                handlerAdapter.handleMessage(message);
            } else {
                handleMessage((AbstractAlert) message);
            }
            message = rabbitAsyncTemplate.receiveAndConvert(alertsQueue.getName());
        }
        return observeFrom;/*
                            * Возвращаемый objectId здесь не важен, т.к. он не
                            * используется в выборке сообщений
                            */
    }

    /** Отправить драйверу активный (или пустой) проект */
    private void sendActiveProjectRequest(SetActiveProjectRequest request, String driverId, String queueName) {
        request.setDriverId(driverId);
        /* TODO: сделать, чтобы сендер не создавался каждый раз */
        DriverRequestSender requestSender = new DriverRequestSender(logger, rabbitAsyncTemplate, queueName,
                driverMessagePostProcessor);
        requestSender.send(request);
    }

    void handleMessage(DriverRegisteredAlert message) {
        try {
            logger.info("Driver registered message received. Driver id: {}", message.getDriverId());
            Project activeProject = projectService.getActive();
            if (activeProject != null) {
                if (driverInstanceRepository.findOne(message.getDriverId()) != null) {
                    sendActiveProjectRequest(projectService.getSetActiveProjectRequest(activeProject.getId()),
                            message.getDriverId(), message.getRequestQueue());
                } else if (!message.getDriverId().contains("unknown")) {
                    logger.warn("Driver registered message received, but driver id ({}) is unknown.",
                            message.getDriverId());
                }
            }
        }
        catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(AddActiveDeviceState message) {
        /*
         * TODO: перенести изменение состояний в фон с логикой MongoDB-агрегирования, а здесь сохранять в коллекцию -
         * буфер
         */
        try {
            logger.info("Add device state message received. Driver id: {}, deviceId: {}, state: {}",
                    message.getDriverId(), message.getActiveDeviceId(), message.getState());

            activeDeviceService.addActiveState(message.getActiveDeviceId(), message.getState());
        }
        catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(RemoveActiveDeviceState message) {
        /*
         * TODO: перенести изменение состояний в фон с логикой MongoDB-агрегирования, а здесь сохранять в коллекцию -
         * буфер
         */
        try {
            logger.info("Remove device state message received."
                    + " Driver id: {}, deviceId: {}, state: {}",
                    message.getDriverId(), message.getActiveDeviceId(), message.getState());

            activeDeviceService.removeActiveState(message.getActiveDeviceId(), message.getState());
        }
        catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(SetActiveDeviceStates message) {
        /*
         * TODO: перенести изменение состояний в фон с логикой MongoDB-агрегирования, а здесь сохранять в коллекцию -
         * буфер
         */
        try {
            String activeStatesString = "";
            for (String state: message.getActiveStates())
                activeStatesString = activeStatesString + ", " + state;
            if (activeStatesString.length() > 0)
                activeStatesString = activeStatesString.substring(2);

            logger.info("Set device state message received."
                    + " Driver id: {}, deviceId: {}, states: '{}'",
                    message.getDriverId(), message.getActiveDeviceId(), activeStatesString);

            activeDeviceService.setActivePollingStates(message.getActiveDeviceId(), message.getActiveStates());
        }
        catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(SetEntityStates message){
        logger.info("Set entity states message received. Driver id: {}", message.getDriverId());

        if (message.isAllOff()) {
            activeDeviceService.setOffStateOnAllEntities();
        } else {
            Map<String, Integer> deviceInfoWithStateIds = message.getDeviceInfoWithValues();
            if (!message.getOffRegionNos().isEmpty()){
                // devices with these region numbers should be ignored and not updated
                activeDeviceService.setOffStateByRegions(message.getOffRegionNos());
                deviceInfoWithStateIds.keySet().removeIf(key ->
                    message.getOffRegionNos().contains(Integer.parseInt(key.split(";")[0])));
            }
            activeDeviceService.setActivePollingStates(deviceInfoWithStateIds);
        }
        httpService.sendAlertIfNeeded(message);
    }

    void handleMessage(ControlDeviceEvents controlDeviceEvents) {
        try {
            logger.info("Control device events received. Driver id: {}, count: {}", controlDeviceEvents.getDriverId(),
                    controlDeviceEvents.getControlDeviceEvents().size());
            controlDeviceEventRepository.insert(controlDeviceEvents.getControlDeviceEvents());
        }
        catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(SetActiveDeviceMonitorableValues message) {
        try {
            if (message.getDeviceId() != null) {
                logger.info("Set device monitorable values received. Driver id: {}, deviceId: {}",
                        message.getDriverId(), message.getDeviceId());
            } else {
                logger.info(
                        "Set device monitorable values message received. Driver id: {}, controlDeviceId: {}"
                                + ", controlDeviceAddress: {}, lineNo: {}, address: {}",
                        message.getDriverId(), message.getControlDeviceId(), message.getControlDeviceAddress(),
                        message.getLineNo(), message.getAddress());
            }
            activeDeviceService.setMonitorableValues(message);
        } catch (Exception exception) {
            logger.error("Exception: {}. Stack trace: {}", exception.getMessage(), exception.getStackTrace());
        }
    }

    void handleMessage(ControlDeviceInfo message) {
        try {
            logger.info("Device info message received. {}", message.toString());
            boolean controlDeviceReplaced =
                    activeDeviceService.setDeviceGuid(message.getDeviceId(), message.getGuid());
            if (controlDeviceReplaced) {
                EventType eventType = eventTypeRepository.findOne(CONTROL_DEVICE_REPLACED_EVENT_TYPE_ID);
                if (eventType != null)
                    eventService.commitEventByEventTypeAndControlDeviceId(eventType, message.getDeviceId());
            }

            if (!message.isDatabaseMatchingProject()) {
                activeDeviceService.addActiveState(message.getDeviceId(),
                        LogicStates.DB_NOT_MATCHING_PROJECT.toString());
            } else {
                activeDeviceService.removeActiveState(message.getDeviceId(),
                        LogicStates.DB_NOT_MATCHING_PROJECT.toString());
            }
        } catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    void handleMessage(ActiveDeviceConfig message) {
        try {
            String action = null;
            Date writePerformed = null;
            Date readPerformed = null;
            Date performed = message.getPerformedDateTime();
            switch (message.getAction()) {
                case READ_CONFIG_FROM_DEVICE:
                    action = "read";
                    readPerformed = performed;
                    break;
                case WRITE_CONFIG_TO_DEVICE:
                    action = "write";
                    writePerformed = performed;
                    break;
                default:
                    logger.error("Wrong issue action: {}", message.getAction());
                    return;
            }
            logger.info(
                    "Device config {} completed for device \"{}\" by driver \"{}\": {}"
                            + ", serial: {}, firmware: {}, database: {}, performed: {}",
                    action, message.getDeviceId(), message.getDriverId(),
                    message.getConfigPropertyValues(), message.getSerialNumber(),
                    message.getFirmwareVersion(), message.getDatabaseVersion(),
                    performed);

            activeDeviceService.setDeviceActiveConfig(message.getDeviceId(),
                    message.getConfigPropertyValues(), message.getSerialNumber(),
                    message.getFirmwareVersion(), message.getDatabaseVersion(),
                    readPerformed, writePerformed);

            DeviceConfigChanged alert = new DeviceConfigChanged();
            alert.setDeviceId(message.getDeviceId());
            alert.setProjectId(projectService.getActive().getId());
            alertSender.send(alert);
        } catch (Exception e) {
            logger.error("Exception: {}. Stack trace: {}", e.getMessage(), e.getStackTrace());
        }
    }

    /*
     * TODO: вынести функционал в сервис драйверов
     */
    void handleMessage(ActiveProjectUpdated alert) {
        try {
            logger.info("Active project updated. Id: {}", alert.getProjectId());
            List<DriverInstance> driverInstances = driverInstanceRepository.findAll();
            if (!driverInstances.isEmpty()) {
                SetActiveProjectRequest request = projectService.getSetActiveProjectRequest(alert.getProjectId());
                for (DriverInstance driverInstance : driverInstances) {
                    sendActiveProjectRequest(request, driverInstance.getId(), driverInstance.getRequestQueueName());
                }
            }
        } catch (Exception exception) {
            logger.error("Exception: {}. Stack trace: {}", exception.getMessage(), exception.getStackTrace());
        }
    }

    void handleMessage(ActiveProjectValidateStatus activeProjectValidateStatus) {
        try {
            logger.info("Active project validate status received. Project Id: {}, validateSucceded: {}",
                    activeProjectValidateStatus.getProjectId(), activeProjectValidateStatus.isValidateSucceded());
            projectService.updateStatus(activeProjectValidateStatus);
        } catch (Exception exception) {
            logger.error("Exception: {}. Stack trace: {}", exception.getMessage(), exception.getStackTrace());
        }
    }

    void handleMessage(IssueProgress issueProgress) {
        try {
            logger.info("Issue progress received. Issue id: {}, status: {}", issueProgress.getIssueId(),
                    issueProgress.getStatus());
            issueService.onIssueProgress(issueProgress);
        } catch (Exception exception) {
            logger.error("Exception: {}. Stack trace: {}", exception.getMessage(), exception.getStackTrace());
        }
    }

}
