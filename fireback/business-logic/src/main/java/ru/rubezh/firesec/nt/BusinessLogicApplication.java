package ru.rubezh.firesec.nt;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BusinessLogicApplication {

    public static void main(String[] args) {
        new BusinessLogicApplication().run();
    }

    public void run() {
        try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml")) {
            context.registerShutdownHook();
            while(true) Thread.sleep(4000);
        }
        catch (InterruptedException e) {
            System.out.println("Closed by interrupt");
        }
    }

}
