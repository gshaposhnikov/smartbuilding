package ru.rubezh.firesec.nt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import ru.rubezh.firesec.nt.domain.v1.IssueAction;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.service.v1.IssueService;
import ru.rubezh.firesec.nt.service.v1.skud.SkudService;

public class SkudChangesChecker {

    private SkudService skudService;

    @Autowired
    public SkudChangesChecker(SkudService skudService, IssueService issueService) {
        this.skudService = skudService;

        /* Подписка на изменение статуса задач */
        issueService.subscribe((issue, issueProgress) -> {
            if (issue.getAction() == IssueAction.WRITE_SKUD_DATABASE
                    && issueProgress.getStatus() == IssueStatus.FINISHED && issue.getProjectId() != null
                    && !issue.getProjectId().isEmpty() && issue.getDeviceId() != null
                    && !issue.getDeviceId().isEmpty()) {

                /* Удаляем состояние "БД СКУД устарела" у прибора */
                skudService.unmarkControlDeviceWithSkudDBOutOfDate(issue.getProjectId(), issue.getDeviceId(),
                        issue.getCreateDateTime());
            }
        });
    }

    /* Фоновая процедура определения приборов с устаревшей БД СКУД */
    @Scheduled(fixedDelay = 10000)
    public void checkSkudChanges() {
        skudService.markControlDevicesWithSkudDBOutOfDate();
    }

}
