package ru.rubezh.firesec.nt.domain.v1.license;

/**
 * Лицензионное ограничение СКУД.
 */
public class SkudRestriction {

    /** СКУД персонал */
    private boolean staffEnabled;
    /** СКУД Проходная */
    private boolean checkpointEnabled;
    /** СКУД Отчёты */
    private boolean reportsEnabled;

    public boolean isStaffEnabled() {
        return staffEnabled;
    }
    public void setStaffEnabled(boolean staffEnabled) {
        this.staffEnabled = staffEnabled;
    }

    public boolean isCheckpointEnabled() {
        return checkpointEnabled;
    }
    public void setCheckpointEnabled(boolean checkpointEnabled) {
        this.checkpointEnabled = checkpointEnabled;
    }

    public boolean isReportsEnabled() {
        return reportsEnabled;
    }
    public void setReportsEnabled(boolean reportsEnabled) {
        this.reportsEnabled = reportsEnabled;
    }

    public boolean equals(SkudRestriction another) {
        return this.staffEnabled == another.staffEnabled &&
                this.checkpointEnabled == another.checkpointEnabled &&
                this.reportsEnabled == another.reportsEnabled;
    }

}
