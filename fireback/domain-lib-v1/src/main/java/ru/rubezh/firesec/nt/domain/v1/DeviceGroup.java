package ru.rubezh.firesec.nt.domain.v1;

/**
 * Группа устройств.
 * 
 * Используется для обозначения принадлежности устройства конкретному типу.
 *
 * @author Антон Караваев
 *
 */

public enum DeviceGroup {

    INTERFACE_MODULE("Модуль сопряжения"),

    CONTROL_DEVICE("ППКП"),

    FIRE_NOTIFIER("Извещатели пожарные"),

    SECURITY_NOTIFIER("Извещатели охранные"),

    ADDRESS_TAG("Адресная метка"),

    EXECUTIVE_DEVICE("Исполнительные устройства"),

    POWER_SUPPLY("Источники питания"),

    CONTAINER("Контейнеры"),

    FIRE_EXTINGUISHER("Модуль пожаротушения"),

    RADIO_DEVICE("Радиоканальное устройство"),

    SKUD("СКУД"),

    MANAGE_PANEL("Шкафы"),

    OTHER("Прочее");

    private final String viewName;

    DeviceGroup(String viewName) {
        this.viewName = viewName;
    }

    public String getViewName() {
        return viewName;
    }
}
