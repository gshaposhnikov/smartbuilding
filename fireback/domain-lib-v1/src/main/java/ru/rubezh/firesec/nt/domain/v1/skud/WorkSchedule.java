package ru.rubezh.firesec.nt.domain.v1.skud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;

/**
 * График работы (СКУД).
 * 
 * Расписание рабочих дней, перерывов, периодов прохода через точки доступа.
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 */
@Document(collection = "work_schedules")
@TypeAlias(value = "WorkSchedule")
@JsonTypeName("WorkSchedule")
@CompoundIndexes({ @CompoundIndex(name = "wsIndex", def = "{'projectId':1, 'index':1}", unique = true),
        @CompoundIndex(name = "wsDaysIndex", def = "{'projectId':1, 'days.index':1}"),
        @CompoundIndex(name = "wsAccessGrantedTimesIndex", def = "{'projectId':1, 'days.accessGrantedTimes.index':1}")
})
public class WorkSchedule extends BasicEntityWithDate {
    /** Идентификатор проекта */
    @Indexed
    private String projectId;

    /** Название */
    private String name;

    /** Описание */
    private String description;

    /** Числовой идентификатор (беззнаковое слово) */
    private int index;

    /** Дата начала действия */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date activeFrom;

    /** Тип графика работы. */
    public enum WorkScheduleType {
        WEEK,
        MONTH,
        OTHER
    }

    WorkScheduleType workScheduleType = WorkScheduleType.OTHER;

    /** Время разрешенного прохода. */
    public static class AccessGrantedTime extends BasicEntityWithDate {
        /** Числовой идентификатор (беззнаковое слово) */
        private int index;
        /** Время начала - минуты */
        private int from;
        /** Время окончания - минуты */
        private int to;

        public AccessGrantedTime() {
            super();
        }

        public AccessGrantedTime(AccessGrantedTime srcTime) {
            super();
            setFrom(srcTime.getFrom());
            setTo(srcTime.getTo());
        }

        public void update(AccessGrantedTime srcTime) {
            setFrom(srcTime.getFrom());
            setTo(srcTime.getTo());
            setUpdateDateTime(new Date());
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getTo() {
            return to;
        }

        public void setTo(int to) {
            this.to = to;
        }
    }

    /** Определение дня графика работы. */
    public static class WorkScheduleDay extends BasicEntityWithDate {
        /** Числовой идентификатор (беззнаковое слово) */
        private int index;
        /** Рабочий день или выходной */
        private boolean workday = true;
        /** Время начала рабочего дня в минутах */
        private int workStartTime = 480; // 8:00
        /** Время окончания рабочего дня в минутах */
        private int workStopTime = 1020; // 17:00
        /** Время начала перерыва в минутах */
        private int lunchStartTime = -1;
        /** Время окончания перерыва в минутах */
        private int lunchStopTime = -1;
        /** Периоды разрешенного прохода */
        private List<AccessGrantedTime> accessGrantedTimes = new ArrayList<>();

        public WorkScheduleDay() {
            super();
        }

        public WorkScheduleDay(WorkScheduleDay srcDay) {
            super();
            setWorkday(srcDay.isWorkday());
            setWorkStartTime(srcDay.getWorkStartTime());
            setWorkStopTime(srcDay.getWorkStopTime());
            setLunchStartTime(srcDay.getLunchStartTime());
            setLunchStopTime(srcDay.getLunchStopTime());
        }

        public void update(WorkScheduleDay srcDay) {
            setWorkday(srcDay.isWorkday());
            setWorkStartTime(srcDay.getWorkStartTime());
            setWorkStopTime(srcDay.getWorkStopTime());
            setLunchStartTime(srcDay.getLunchStartTime());
            setLunchStopTime(srcDay.getLunchStopTime());
            setUpdateDateTime(new Date());
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public boolean isWorkday() {
            return workday;
        }

        public void setWorkday(boolean workday) {
            this.workday = workday;
        }

        public int getWorkStartTime() {
            return workStartTime;
        }

        public void setWorkStartTime(int workStartTime) {
            this.workStartTime = workStartTime;
        }

        public int getWorkStopTime() {
            return workStopTime;
        }

        public void setWorkStopTime(int workStopTime) {
            this.workStopTime = workStopTime;
        }

        public int getLunchStartTime() {
            return lunchStartTime;
        }

        public void setLunchStartTime(int lunchStartTime) {
            this.lunchStartTime = lunchStartTime;
        }

        public int getLunchStopTime() {
            return lunchStopTime;
        }

        public void setLunchStopTime(int lunchStopTime) {
            this.lunchStopTime = lunchStopTime;
        }

        public List<AccessGrantedTime> getAccessGrantedTimes() {
            return accessGrantedTimes;
        }

        public void setAccessGrantedTimes(List<AccessGrantedTime> accessGrantedTimes) {
            this.accessGrantedTimes = accessGrantedTimes;
        }
    }

    /** Дни графика (кол-во зависит от типа) */
    private List<WorkScheduleDay> days = new ArrayList<>();

    public void update(WorkSchedule srcWorkSchedule) {
        setName(srcWorkSchedule.getName());
        setDescription(srcWorkSchedule.getDescription());
        setActiveFrom(srcWorkSchedule.getActiveFrom());
        setWorkScheduleType(srcWorkSchedule.getWorkScheduleType());
        setUpdateDateTime(new Date());
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public WorkScheduleType getWorkScheduleType() {
        return workScheduleType;
    }

    public void setWorkScheduleType(WorkScheduleType workScheduleType) {
        this.workScheduleType = workScheduleType;
    }

    public List<WorkScheduleDay> getDays() {
        return days;
    }

    public void setDays(List<WorkScheduleDay> days) {
        this.days = days;
    }

}
