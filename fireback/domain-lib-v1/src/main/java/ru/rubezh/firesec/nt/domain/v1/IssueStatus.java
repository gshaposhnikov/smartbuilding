package ru.rubezh.firesec.nt.domain.v1;

/**
 * Статус задачи.
 * 
 * @author Александр Горячкин
 */
public enum IssueStatus {
    /** Создана, но не запущена */
    CREATED,
    /** Создана, но не должна выполняться */
    PAUSE,
    /** Выполняется */
    IN_PROGRESS,
    /** Завершена успешно */
    FINISHED,
    /** Завершена с ошибкой */
    FAILED,
    /** Отменена - удалена пользователем или по истечению времени
     * (только для отправки оповещений по WS)
     */
    CANCELED,
    NONE
}