package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.util.*;

/**
 * Устройство, оперативная часть.
 * 
 * @author Антон Васильев
 *
 */
@Document(collection="active_devices")
@TypeAlias(value="ActiveDevice")
@JsonTypeName("ActiveDevice")
public class ActiveDevice extends BasicActiveEntityWithStates {
    /** Устройство, проектная часть */
    @DBRef private Device deviceProject;

    /* Адресация в дереве устройств  */
    /** Идентификатор родительского устройства */
    @Indexed
    private String parentDeviceId;
    /**
     * Поисковый адресный путь в дереве устройств (,A,B,C…,Q) (должен
     * дублировать адресный путь в проектной части)
     */
    @Indexed
    private String addressPath;
    /** Уровень адреса (должен дублировать уровень адреса в проектной части) */
    @Indexed
    private int addressLevel;

    /** Проект (идентификатор) */
    @Indexed
    private String projectId = "";

    /** Идентификатор зоны, если устройство привязываемо к зоне */
    @Indexed
    private String regionId;

    /** Статус тест-кнопки (нажата/сброшена) */
    private boolean testButtonPressed;

    /** Режимные параметры (согласно профилю устройства) */
    private Map<String, String> modeProperties = new HashMap<>();

    /** Значения параметров конфигурации в устройстве (согласно профилю устройства) */
    private Map<String, String> activeConfigValues = new HashMap<>();

    /** Частные параметры состояния (согласно профилю устройства) */
    private Map<String, String> customStateProperties = new HashMap<>();

    /** Набор наблюдаемых значений (согласно профилю устройства) */
    private Map<String, String> monitorableValues = new HashMap<>();

    /** Время последнего обновления наблюдаемых значений */
    private Date monitorableValuesTimestamp;

    /* Атрибуты приборов */
    /** GUID */
    private String guid = "";
    /** Счетчики событий по журналам */
    private Map<String, Long> eventCounts = new HashMap<>();

    /** Задержка перед началом пожаротушения */
    private int fireExtinctionDelay;

    /**
     * Запись (сообщение) в блокноте устройства.
     * 
     * @author Антон Караваев
     *
     */
    public static class Note extends AdjectiveEntity {
        public Note(){}

        public Note(Date createDateTime, String userName, String message){
            this.createDateTime = createDateTime;
            this.userName = userName;
            this.message = message;
        }

        @DateTimeFormat(iso = ISO.DATE_TIME)
        private Date createDateTime;

        private String userName;

        private String message;

        public Date getCreateDateTime() {
            return createDateTime;
        }

        public void setCreateDateTime(Date createDateTime) {
            this.createDateTime = createDateTime;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    /** Набор записей по устройству из Блокнота*/
    private List<Note> notes = new ArrayList<>();

    public ActiveDevice() {
        super();
    }

    public ActiveDevice(Device deviceProject) {
        this(deviceProject, null);
    }

    public ActiveDevice(Device deviceProject, DeviceProfile deviceProfile) {
        super();
        this.id = deviceProject.getId();
        this.deviceProject = deviceProject;
        this.parentDeviceId = deviceProject.getParentDeviceId();
        this.projectId = deviceProject.getProjectId();
        this.addressPath = deviceProject.getAddressPath();
        this.addressLevel = deviceProject.getAddressLevel();
        this.regionId = deviceProject.getRegionId();

        if (deviceProfile.getConfigProperties() != null)
            for (String propertyId : deviceProfile.getConfigProperties().keySet())
                this.activeConfigValues.put(propertyId, null);
        if (deviceProfile.getCustomMonitorableValues() != null)
            for (MonitorableValue monitorableValue : deviceProfile.getCustomMonitorableValues())
                this.monitorableValues.put(monitorableValue.getProfile().getId(), monitorableValue.getProfile().getDefaultValue());
    }

    public Device getDeviceProject() {
        return deviceProject;
    }

    public void setDeviceProject(Device device) {
        this.deviceProject = device;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public String getAddressPath() {
        return addressPath;
    }

    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    public int getAddressLevel() {
        return addressLevel;
    }

    public void setAddressLevel(int addressLevel) {
        this.addressLevel = addressLevel;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public boolean isTestButtonPressed() {
        return testButtonPressed;
    }

    public void setTestButtonPressed(boolean testButtonPressed) {
        this.testButtonPressed = testButtonPressed;
    }

    public Map<String, String> getModeProperties() {
        return modeProperties;
    }

    public void setModeProperties(Map<String, String> modeProperties) {
        this.modeProperties = modeProperties;
    }

    public Map<String, String> getActiveConfigValues() {
        return activeConfigValues;
    }

    public void setActiveConfigValues(Map<String, String> activeConfigValues) {
        this.activeConfigValues = activeConfigValues;
    }

    public Map<String, String> getCustomStateProperties() {
        return customStateProperties;
    }

    public void setCustomStateProperties(Map<String, String> customStateProperties) {
        this.customStateProperties = customStateProperties;
    }

    public Map<String, String> getMonitorableValues() {
        return monitorableValues;
    }

    public void setMonitorableValues(Map<String, String> monitorableValues) {
        this.monitorableValues = monitorableValues;
    }

    public Date getMonitorableValuesTimestamp() {
        return monitorableValuesTimestamp;
    }

    public void setMonitorableValuesTimestamp(Date monitorableValuesTimestamp) {
        this.monitorableValuesTimestamp = monitorableValuesTimestamp;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Map<String, Long> getEventCounts() {
        return eventCounts;
    }

    public void setEventCounts(Map<String, Long> eventCounts) {
        this.eventCounts = eventCounts;
    }

    public int getFireExtinctionDelay() {
        return fireExtinctionDelay;
    }

    public void setFireExtinctionDelay(int fireExtinctionDelay_) {
        fireExtinctionDelay = fireExtinctionDelay_;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes){
        this.notes = notes;
    }

    public void addNote(ActiveDevice.Note note){
        this.notes.add(note);
    }

    public void addValidStates(Collection<String> stateIds, DeviceProfile deviceProfile) {
        List<String> validStates = deviceProfile.getValidDeviceStateIds();
        validStates.retainAll(stateIds);

        if (!validStates.isEmpty())
            addStates(validStates);
    }

}
