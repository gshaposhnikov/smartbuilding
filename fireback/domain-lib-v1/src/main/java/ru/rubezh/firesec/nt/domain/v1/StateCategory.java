package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Класс состояния.
 * 
 * Определяет глобальную приоретизацию и группировку событий, состояний устройств, зон и подсистем.
 * 
 * @author Александр Горячкин, Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection = "state_categories")
@TypeAlias(value = "StateCategory")
public class StateCategory extends ReferenceEntity {
    /**
     * Приоритет класса состояния.
     * 
     * 0 - самый высокий приоритет, далее чем больше число, тем менее приоритеный класс.
     */
    @Indexed
    int priority = Integer.MAX_VALUE;
    
    /** Идентификатор подсистемы */
    Subsystem subsystem = Subsystem.UNKNOWN;
    
    /** Флаг - является ли категория категорией по умолчанию */
    boolean defaultCategory;

    /** Флаг, указывающий на то, что сценарий запущен*/
    boolean scenarioRunning;

    /**
     * Флаг, указывающий на то, что категория состояний влияет на статус "на охране" у активной зоны
     */
    boolean onGuardCategory;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public boolean isDefaultCategory() {
        return defaultCategory;
    }

    public void setDefaultCategory(boolean defaultCategory) {
        this.defaultCategory = defaultCategory;
    }

    public void setScenarioRunning(boolean scenarioRunning) {
        this.scenarioRunning = scenarioRunning;
    }

    public boolean isScenarioRunning(){
        return scenarioRunning;
    }

    public boolean isOnGuardCategory(){
        return onGuardCategory;
    }

    public void setOnGuardCategory(boolean onGuardCategory){
        this.onGuardCategory = onGuardCategory;
    }
}
