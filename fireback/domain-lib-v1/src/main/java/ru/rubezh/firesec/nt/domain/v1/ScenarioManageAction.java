package ru.rubezh.firesec.nt.domain.v1;

/**
 * Управляющее действие над сценарием.
 * 
 * @author Андрей Лисовой
 *
 */
public enum ScenarioManageAction {
    /** Запустить */
    START,
    /** Остановить */
    STOP,
    /** Заблокировать */
    BLOCK,
    /** Разблокировать */
    UNBLOCK,
}
