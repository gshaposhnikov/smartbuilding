package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Блок логики сценария.
 * 
 * Используется для определения многоуровневого логического выражения, проверяемого прибором в качестве условия
 * автоматического запуска или остановки сценария.
 * 
 * @author Антон Васильев
 *
 */
public class ScenarioLogicBlock extends AdjectiveEntity {

    /** тип логики. */
    public enum LogicType {
        /** Логика по "И": срабатывает, если триггерное состояние зафиксировано во всех сущностях. */
        AND,
        /** Логика по "ИЛИ": срабатывает, если триггерное состояние зафиксировано хотя бы в одной сущности. */
        OR,
        /** Логика по "И (НЕ)": срабатывает, если триггерное состояние не зафиксировано ни в одной сущности. */
        AND_NOT,
        /** Логика по "ИЛИ (НЕ)": срабатывает, если триггерное состояние не зафиксировано хотя бы в одной сущности. */
        OR_NOT
    }
    /** Тип логики */
    private LogicType logicType = LogicType.OR;
    /** Идентификатор типа триггера сценария */
    private String triggerTypeId = "";
    /** Идентификаторы проверяемых логикой сущностей (устройств, зон, приборов или виртуальных состояний) */
    private List<String> entityIds = new ArrayList<>();
    /** BSON идентификаторы проверяемых логикой сущностей */
    @JsonIgnore
    private List<ObjectId> entityOIds = new ArrayList<>();
    /** Вложенные блоки логики */
    private List<ScenarioLogicBlock> subLogics = new ArrayList<>();
    
    public ScenarioLogicBlock() {
        
    }
    
    public ScenarioLogicBlock(ScenarioLogicBlock source) {
        setLogicType(source.getLogicType());
        setTriggerTypeId(source.getTriggerTypeId());
        setEntityIds(source.getEntityIds());
        setSubLogics(source.getSubLogics());
    }
    
    public LogicType getLogicType() {
        return logicType;
    }
    
    public void setLogicType(LogicType logicType) {
        this.logicType = logicType;
    }

    public String getTriggerTypeId() {
        return triggerTypeId;
    }
    
    public void setTriggerTypeId(String triggerTypeId) {
        if (triggerTypeId == null) {
            this.triggerTypeId = "";
        }
        else {
            this.triggerTypeId = triggerTypeId;
        }
    }
    
    public List<String> getEntityIds() {
        return entityIds;
    }
    
    public void setEntityIds(List<String> entityIds) {
        this.entityIds = entityIds;
        this.entityOIds.clear();
        entityIds.forEach(entityId -> this.entityOIds.add(new ObjectId(entityId)));
    }

    public List<ObjectId> getEntityOIds() {
        return entityOIds;
    }

    public void setEntityOIds(List<ObjectId> entityIds) {
        this.entityOIds = entityIds;
    }

    public List<ScenarioLogicBlock> getSubLogics() {
        return subLogics;
    }

    public void setSubLogics(List<ScenarioLogicBlock> subLogics) {
        this.subLogics = subLogics;
    }

    @JsonIgnore
    public boolean isEmpty() {
        if (entityIds != null && !entityIds.isEmpty()) {
            return false;
        }
        if (subLogics != null && !subLogics.isEmpty()) {
            for (ScenarioLogicBlock logicBlock : subLogics) {
                if (!logicBlock.isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }
}
