package ru.rubezh.firesec.nt.domain.v1;

import java.util.BitSet;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Состояние с параметрами раскодирования.
 * 
 * @author Антон Васильев
 *
 */
@Document(collection="states")
@TypeAlias(value="DecodableState")
@JsonTypeName("DecodableState")
public class DecodableState extends State {
    /** Номер бита для "побитового" раскодирования */
    private int bitNo = -1;

    /* Далее - три параметра для раскодирования по маске */

    /** Номер байта в бинарном пакете данных общего состояния */
    private int byteNo = -1;

    /** Маска, которую необходимо накладывать */
    private int mask = 0xFFFFFFFF;

    /** Значение - признак активности */
    private int activeValue = 1;

    /** Номер бита статуса зоны. При наличии - необходимо учитывать ("и") */
    private Integer regionBitNo;

    public int getBitNo() {
        return bitNo;
    }

    public void setBitNo(int bitNo) {
        this.bitNo = bitNo;
    }

    public int getByteNo() {
        return byteNo;
    }

    public void setByteNo(int byteNo) {
        this.byteNo = byteNo;
    }

    public int getMask() {
        return mask;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }

    public int getActiveValue() {
        return activeValue;
    }

    public void setActiveValue(int activeValue) {
        this.activeValue = activeValue;
    }

    /** Раскодировать один бит */
    public boolean decodeBit(byte[] bytes, BitSet regionStateBits) {
        boolean result = true;
        if (bitNo >= 0)
            result = ((bytes[bitNo / 8] >> (bitNo % 8)) & 0x01) == 0x01;
        return applyRegionBit(result, regionStateBits);
    }

    /** Раскодировать по маске */
    public boolean decodeMask(byte[] bytes, BitSet regionStateBits) {
        boolean result = true;
        if (byteNo >= 0)
            result = (bytes[byteNo] & mask) == activeValue;
        return applyRegionBit(result, regionStateBits);
    }

    /**
     * Раскодировать по биту, или по маске, или по биту зоны, смотря что задано
     */
    public boolean decodeAuto(byte[] bytes, BitSet regionStateBits) {
        boolean result = true;
        if (bitNo >= 0)
            result = ((bytes[bitNo / 8] >> (bitNo % 8)) & 0x01) == 0x01;
        else if (byteNo >= 0)
            result = (bytes[byteNo] & mask) == activeValue;
        return applyRegionBit(result, regionStateBits);
    }

    /** Применить бит зоны */
    protected boolean applyRegionBit(boolean result, BitSet regionStateBits) {
        if (regionBitNo != null)
            return result && regionStateBits.get(regionBitNo);
        return result;
    }

    public Integer getRegionBitNo() {
        return regionBitNo;
    }

    public void setRegionBitNo(Integer bitNo) {
        this.regionBitNo = bitNo;
    }

}
