package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.util.*;

/**
 * Задача.
 * 
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 * @ingroup BasicSystem
 */
@Document(collection = "issues")
@TypeAlias(value = "Issue")
@JsonTypeName("Issue")
public class Issue extends BasicEntity {

    /** Действие, которое нужно сделать */
    private IssueAction action;

    /** Идентификатор проекта */
    private String projectId = "";

    /** Идентификатор устройства */
    private String deviceId = "";

    /**
     * Доп. параметры для выполнения команды
     * При сбросе состояния должена передаваться команда группы состояний
     * с ручным сбросом по ключу 'stateManualResetGroup'
     */
    private Map<String, Object> parameters = new HashMap<>();

    /** Внедренные по пути следования сообщения к драйверу параметры (не сохраняются в БД) */
    @Transient
    private IssueParams injectedParameters = new IssueParams();

    /** Название параметров задач. */
    public enum IssueParameterNames {
        /* TODO: перечислить здесь все используемые параметры задач */
        regionId,

        /* Используемые в СКУД */

        /** Список строковых идентификаторов приборв */
        controlDeviceIds,
        /** Идентификатор ключа доступа */
        accessKeyId,

        /* Используемые в индикаторах */

        /** Тип сущности */
        entityType,
        /** Список строковых идентификаторов сущностей */
        entityIds,
        /** Строковый идентификатор панели индикаторов */
        indicatorPanelId,
        /** Номер строки панели индикаторов */
        rowNo,
        /** Номер колонки панели индикаторов */
        colNo,

        /* Used in smart building */

        /* Used everywhere: */
        /** Number that represents the <code>State</code> */
        stateNumber,

        /** Id of the state that should be set to the entity */
        stateId,

        /* Used on regions and devices */
        /** Index region where entity is placed */
        regionIndex,

        /* Used on devices only */
        /** Device's serial number in the region,
         * counted by the same <code>DeviceType</code> class  */
        deviceNo,
        /** The type of used device.
         *  @see DeviceType */
        deviceType,
        /** Flag which indicates that device has a postfix instead of a serial number */
        withPostFix;
    }

    /** Процент прогресса, [0..100] */
    private double progress = 0;

    /** Статус */
    private IssueStatus status = IssueStatus.CREATED;

    /** Дополнительная информация о статусе (описание ошибки) */
    private String statusMessage = "";

    /** Дата и время создания */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date createDateTime = new Date();

    /** Дата и время запуска */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date startDateTime = new Date(0);

    /** Дата и время завершения */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date finishDateTime = new Date(0);

    /** Random-объект для генерации идентификаторов задач */
    private static Random random = new Random();

    /** Идентификатор пользователя, создавшего задачу */
    private String userId = "";

    private FilterTags filterTags;

    /**
     * Сообщения валидации (для возврата в качестве результата выполнения задачи более одной ошибки а также
     * предупреждений)
     */
    private List<ActivationValidateMessage> validateMessages;

    public Issue() {
    }

    public Issue(Issue sourceIssue) {
        this.id = sourceIssue.getId();
        setAction(sourceIssue.getAction());
        setProjectId(sourceIssue.getProjectId());
        setDeviceId(sourceIssue.getDeviceId());
        setParameters(sourceIssue.getParameters());
        setProgress(sourceIssue.getProgress());
        setStatus(sourceIssue.getStatus());
        setStatusMessage(sourceIssue.getStatusMessage());
        setCreateDateTime(sourceIssue.getCreateDateTime());
        setStartDateTime(sourceIssue.getStartDateTime());
        setFinishDateTime(sourceIssue.getFinishDateTime());
        setValidateMessages(sourceIssue.getValidateMessages());
        setUserId(sourceIssue.getUserId());
    }

    public void generateId() {
        StringBuilder idBuilder = new StringBuilder();
        idBuilder.append(getAction().toString());
        idBuilder.append(Long.toUnsignedString(new Date().getTime()));
        idBuilder.append(Integer.toUnsignedString(random.nextInt()));
        id = idBuilder.toString();
    }

    public IssueAction getAction() {
        return action;
    }

    public void setAction(IssueAction action) {
        this.action = action;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Date getFinishDateTime() {
        return finishDateTime;
    }

    public void setFinishDateTime(Date finishDateTime) {
        this.finishDateTime = finishDateTime;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = new HashMap<>(parameters);
    }

    public IssueParams getInjectedParameters() {
        return injectedParameters;
    }

    public void setInjectedParameters(IssueParams injectedParameters) {
        this.injectedParameters = injectedParameters;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public List<ActivationValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public void setValidateMessages(List<ActivationValidateMessage> validateMessages) {
        this.validateMessages = validateMessages;
    }


    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
