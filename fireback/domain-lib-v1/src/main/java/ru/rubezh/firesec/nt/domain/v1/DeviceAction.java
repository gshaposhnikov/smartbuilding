package ru.rubezh.firesec.nt.domain.v1;

import java.util.Map;

/**
 * Команда устройству.
 * 
 * @author Андрей Лисовой
 * @ingroup BasicReferences
 */
public class DeviceAction extends ReferenceEntity {
    /** Команда (байт 0 контекста сообщения "0x02 0x54") */
    private int command;
    /** Подкоманда (байт 1 контекста сообщения "0x02 0x54") */
    private int subCommand;
    /** Дополнительные данные сложной команды */
    private Extension extension;

    /** Расширенные параметры команды устройству. */
    public static class Extension {
        /** Подкоманда 2 (байт 5 контекста сообщения "0x02 0x54") */
        private int subCommand2;
        /** Размер конфигурации действия в байтах (байт 6 контекста сообщения "0x02 0x54") */
        private int configSize;
        /** Параметры, определяющие конфигурацию сложной команды (configSize байт начиная с 8-го) */
        private Map<String, DeviceConfigProperty> configProperties;

        public int getSubCommand2() {
            return subCommand2;
        }

        public void setSubCommand2(int subCommand2_) {
            subCommand2 = subCommand2_;
        }

        public int getConfigSize() {
            return configSize;
        }

        public void setConfigSize(int configSize_) {
            configSize = configSize_;
        }

        public Map<String, DeviceConfigProperty> getConfigProperties() {
            return configProperties;
        }

        public void setConfigProperties(Map<String, DeviceConfigProperty> configProperties_) {
            configProperties = configProperties_;
        }
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command_) {
        command = command_;
    }

    public int getSubCommand() {
        return subCommand;
    }

    public void setSubCommand(int subCommand_) {
        subCommand = subCommand_;
    }

    public DeviceAction.Extension getExtension() {
        return extension;
    }

    public void setExtension(DeviceAction.Extension extension_) {
        extension = extension_;
    }
}
