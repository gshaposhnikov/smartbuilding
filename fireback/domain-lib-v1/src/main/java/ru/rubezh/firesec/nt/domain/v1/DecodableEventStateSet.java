package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/* TODO: должна быть возможность прописать еще и regionBitNo для любого состояния */
/**
 * Набор состояний, декодируемых из записи события.
 * 
 * Используется, чтобы при раскодировании записи события определить возникшие на устройстве или в зоне состояния. Аналог
 * dictionary в rubezhrsr3.xml.
 *
 * @author Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection = "decodable_event_state_sets")
@TypeAlias(value = "DecodableEventStateSet")
@JsonTypeName("DecodableEventStateSet")
public class DecodableEventStateSet extends ReferenceEntity {

    /** Номер байта в 32-байтовой записи журнала событий */
    private int byteNo = -1;

    /** Элемент, определяющие состояние по биту. */
    public static class BitDecodableItem {
        /** Ссылка на состояние, обнаруживаемое внутри события */
        @DBRef
        private State state;

        /** Номер бита для "побитового" раскодирования */
        private int bitNo;

        /** Номер бита в статусе зоны. При наличии - необходимо учитывать ("и") */
        private Integer regionBitNo;

        public State getState() {
            return state;
        }

        public void setState(State state) {
            this.state = state;
        }

        public int getBitNo() {
            return bitNo;
        }

        public void setBitNo(int bitNo) {
            this.bitNo = bitNo;
        }

        public Integer getRegionBitNo() {
            return regionBitNo;
        }

        public void setRegionBitNo(Integer regionBitNo) {
            this.regionBitNo = regionBitNo;
        }

        /** Раскодировать по номеру бита */
        public boolean decode(byte[] bytes, int byteNo, BitSet regionStateBits) {
            boolean result = byteNo < 0 || ((bytes[byteNo] >> (bitNo)) & 0x01) == 0x01;
            if (regionBitNo != null) result &= regionStateBits.get(regionBitNo);
            return result;
        }
    }

    /** Элемент, определяющий состояние по маске и по биту в состояниях зоны. */
    public static class MaskedDecodableItem {
        /** Ссылка на состояние, обнаруживаемое внутри события */
        @DBRef
        private State state;

        /**
         * Маска, которую необходимо накладывать при маскируемом раскодировании
         */
        private int mask = 0xFFFFFFFF;

        /**
         * Значения - признак активности, используемое при маскируемом раскодировании
         */
        private List<Integer> activeValues = new ArrayList<>();

        /** Номер бита в статусе зоны. При наличии - необходимо учитывать ("и") */
        private Integer regionBitNo;

        public State getState() {
            return state;
        }

        public void setState(State state) {
            this.state = state;
        }

        public int getMask() {
            return mask;
        }

        public void setMask(int mask) {
            this.mask = mask;
        }

        public List<Integer> getActiveValues() {
            return activeValues;
        }

        public void setActiveValues(List<Integer> activeValues) {
            this.activeValues = activeValues;
        }

        public Integer getRegionBitNo() {
            return regionBitNo;
        }

        public void setRegionBitNo(Integer regionBitNo) {
            this.regionBitNo = regionBitNo;
        }

        /** Раскодировать по маске */
        public boolean decode(byte[] bytes, int byteNo, BitSet regionStateBits) {
            boolean result = byteNo < 0 || activeValues.isEmpty() || activeValues.contains(bytes[byteNo] & mask);
            if (regionBitNo != null) result &= regionStateBits.get(regionBitNo);
            return result;
        }

    }

    /** Состояния, определяемые одним битом */
    List<BitDecodableItem> bitDecodableStates = new ArrayList<>();

    /** Состояния, определяемые маскируемым значением байта */
    List<MaskedDecodableItem> maskDecodableStates = new ArrayList<>();

    public int getByteNo() {
        return byteNo;
    }

    public void setByteNo(int byteNo) {
        this.byteNo = byteNo;
    }

    public List<BitDecodableItem> getBitDecodableStates() {
        return bitDecodableStates;
    }

    public void setBitDecodableStates(List<BitDecodableItem> bitDecodableStates) {
        this.bitDecodableStates = bitDecodableStates;
    }

    public List<MaskedDecodableItem> getMaskDecodableStates() {
        return maskDecodableStates;
    }

    public void setMaskDecodableStates(List<MaskedDecodableItem> maskDecodableStates) {
        this.maskDecodableStates = maskDecodableStates;
    }

}
