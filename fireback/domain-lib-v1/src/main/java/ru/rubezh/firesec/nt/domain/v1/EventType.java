package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Тип события.
 * 
 * На зарегестрированные в системе типы событий должны ссылаться все записи событий.
 * 
 * @author Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection = "event_types")
@TypeAlias(value = "EventType")
@JsonTypeName("EventType")
public class EventType extends ReferenceEntity {
    /**
     * Категория (класс) события.
     * Не имеет значения по умолчанию.
     */
    @DBRef
    private StateCategory category;

    /**
     * Код события или подсобытия (0-ой или 5-ый байт).
     * Не имеет значения по умолчанию.
     */
    private Integer code;

    /**
     * Флаг, указывающий, что событие имеет информацию о приборе, на котором оно было зафиксировано.
     * По умолчанию - true.
     */
    private boolean havingControlDeviceInfo = true;

    /**
     * Флаг, указывающий, что событие имеет информацию о состоянии прибора.
     */
    private Boolean havingControlDeviceStates;
    /*
     * TODO: убрать этот флаг или точнее - объединить его с havingControlDeviceInfo
     */

    /**
     * Флаг, указывающий, что событие имеет информацию об устройстве.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingDeviceInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о зоне.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingRegionInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о пользователе.
     * По умолчанию - false.
     */
    private boolean havingUserInfo;

    /**
     * Флаг, указывающий, что событие генерируется системой.
     * По умолчанию - false.
     */
    private boolean logicGenerated;

    /**
     * Флаг, указывающий, что событие имеет информацию о вирт. состоянии.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingVirtualStateInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о сценарии.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingScenarioInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о состоянии зоны.
     * По умолчанию - false.
     */
    private boolean havingRegionStateInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о линии.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingLineInfo;

    /**
     * Флаг, указывающий, что событие имеет информацию о виртуальном индикаторе.
     * Не имеет значения по умолчанию.
     */
    private Boolean havingVirtualIndicatorInfo;

    /**
     * Действие, соответствующее задаче, инициируемой пользователем (для пользовательских событий)
     */
    private IssueAction issueAction;

    /** Идентификаторы автоматически выставляемых состояний при возникновении события */
    private List<String> autoSetStateIds = new ArrayList<>();

    /** Идентификаторы автоматически сбрасываемых состояний при возникновении события */
    private List<String> autoResetStateIds = new ArrayList<>();

    /** Идентификаторы состояний, автоматически выставляемых всем устройствам зоны при возникновении события */
    private List<String> regionAutoSetStateIds = new ArrayList<>();

    /** Идентификаторы состояний, автоматически сбрасываемых всем устройствам зоны при возникновении события */
    private List<String> regionAutoResetStateIds = new ArrayList<>();

    /**
     * Идентификаторы поддерживаемых наборов состояний устройств, которые может
     * содержать событие.
     * 
     * Ключ - идентификатор профиля устройства, значение - идентификатор набора
     * состояний, вычленяемых из события.
     * 
     * Идентификаторы профилей устройств, выступающие в качестве ключей, должны
     * ссылаться на соответствующие объявленные в профиле драйвера профили.
     * 
     * Идентификаторы наборов расшифровываемых событийных состояний, выступающие
     * в качестве значений, должны ссылаться на соответствующие объявленные в
     * профиле драйвера наборы.
     */
    Map<String, String> deviceDecodableStateSetIds;

    /**
     * Идентификаторы поддерживаемых наборов состояний зон, которые может
     * содержать событие
     *
     * Идентификаторы наборов расшифровываемых событийных состояний должны
     * ссылаться на соответствующие объявленные в профиле драйвера наборы.
     */
    List<String> regionDecodableStateSetIds = new ArrayList<>();

    /** Событие пожаротушения, генерируемое данным событием */
    private FireExtinctionEvent fireExtinctionEvent;

    /**
     * Идентификатор набора состояний,
     * не привязанных к конкретному устройству
     */
    private List<String> generalDecodableStateSetIds;

    /**
     * Набор целочисленных идентификаторов устройств,
     * по которым может придти данный тип события
     */
    private List<Integer> deviceTypes = new ArrayList<>();


    /** Флаг, указывающий, что событие имеет информацию о сотруднике */
    private Boolean havingEmployeeInfo;

    /**
     * Флаг, указывающий, что событие содержит код ключа доступа.
     * По умолчанию - false.
     */
    private boolean havingAccessKeyValue = false;

    /**
     * Подситсема, к которой относится событие
     */
    private Subsystem subsystem;

    private String permissionId = "";

    private String deviceActionKey = "";

    /**
     * Флаг, указывающий, что событие должно содержать
     * сообщение об установке/снятии виртуального состояния.
     */
    private Boolean virtualStateOn;

    public StateCategory getCategory() {
        return category;
    }

    public void setCategory(StateCategory category) {
        this.category = category;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isHavingControlDeviceInfo() {
        return havingControlDeviceInfo;
    }

    public void setHavingControlDeviceInfo(boolean havingControlDeviceInfo) {
        this.havingControlDeviceInfo = havingControlDeviceInfo;
    }

    public Boolean isHavingControlDeviceStates() {
        return havingControlDeviceStates;
    }

    public void setHavingControlDeviceStates(Boolean havingControlDeviceStates) {
        this.havingControlDeviceStates = havingControlDeviceStates;
    }

    public Boolean isHavingDeviceInfo() {
        return havingDeviceInfo;
    }

    public void setHavingDeviceInfo(Boolean havingDeviceInfo) {
        this.havingDeviceInfo = havingDeviceInfo;
    }

    public Boolean isHavingRegionInfo() {
        return havingRegionInfo;
    }

    public void setHavingRegionInfo(Boolean havingRegionInfo) {
        this.havingRegionInfo = havingRegionInfo;
    }

    public boolean isHavingUserInfo() {
        return havingUserInfo;
    }

    public boolean isLogicGenerated() {
        return logicGenerated;
    }

    public void setLogicGenerated(boolean logicGenerated) {
        this.logicGenerated = logicGenerated;
    }

    public void setHavingUserInfo(boolean havingUserInfo) {
        this.havingUserInfo = havingUserInfo;
    }

    public Boolean isHavingVirtualStateInfo() {
        return havingVirtualStateInfo;
    }

    public void setHavingVirtualStateInfo(Boolean havingVirtualStateInfo) {
        this.havingVirtualStateInfo = havingVirtualStateInfo;
    }

    public Boolean isHavingScenarioInfo() {
        return havingScenarioInfo;
    }

    public void setHavingScenarioInfo(Boolean havingScenarioInfo) {
        this.havingScenarioInfo = havingScenarioInfo;
    }

    public boolean isHavingRegionStateInfo() {
        return havingRegionStateInfo;
    }

    public void setHavingRegionStateInfo(boolean havingRegionStateInfo) {
        this.havingRegionStateInfo = havingRegionStateInfo;
    }

    public Boolean isHavingLineInfo() {
        return havingLineInfo;
    }

    public void setHavingLineInfo(Boolean havingLineInfo) {
        this.havingLineInfo = havingLineInfo;
    }

    public List<String> getAutoSetStateIds() {
        return autoSetStateIds;
    }

    public void setAutoSetStateIds(List<String> autoSetStateIds) {
        this.autoSetStateIds = autoSetStateIds;
    }

    public List<String> getAutoResetStateIds() {
        return autoResetStateIds;
    }

    public void setAutoResetStateIds(List<String> autoResetStateIds) {
        this.autoResetStateIds = autoResetStateIds;
    }

    public List<String> getRegionAutoSetStateIds() {
        return regionAutoSetStateIds;
    }

    public void setRegionAutoSetStateIds(List<String> regionAutoSetStateIds) {
        this.regionAutoSetStateIds = regionAutoSetStateIds;
    }

    public List<String> getRegionAutoResetStateIds() {
        return regionAutoResetStateIds;
    }

    public void setRegionAutoResetStateIds(List<String> regionAutoResetStateIds) {
        this.regionAutoResetStateIds = regionAutoResetStateIds;
    }

    public Map<String, String> getDeviceDecodableStateSetIds() {
        return deviceDecodableStateSetIds;
    }

    public void setDeviceDecodableStateSetIds(Map<String, String> deviceDecodableStateSetIds) {
        this.deviceDecodableStateSetIds = deviceDecodableStateSetIds;
    }

    public FireExtinctionEvent getFireExtinctionEvent() {
        return fireExtinctionEvent;
    }

    public void setFireExtinctionEvent(FireExtinctionEvent fireExtinctionEvent) {
        this.fireExtinctionEvent = fireExtinctionEvent;
    }

    public IssueAction getIssueAction() {
        return issueAction;
    }

    public void setIssueAction(IssueAction issueAction) {
        this.issueAction = issueAction;
    }

    public List<String> getGeneralDecodableStateSetIds() {
        return generalDecodableStateSetIds;
    }

    public void setGeneralDecodableStateSetIds(List<String> generalDecodableStateSetIds) {
        this.generalDecodableStateSetIds = generalDecodableStateSetIds;
    }

    public List<String> getRegionDecodableStateSetIds() {
        return regionDecodableStateSetIds;
    }

    public void setRegionDecodableStateSetIds(List<String> regionDecodableStateSetIds) {
        this.regionDecodableStateSetIds = regionDecodableStateSetIds;
    }

    public List<Integer> getDeviceTypes() {
        return deviceTypes;
    }

    public void setDeviceTypes(List<Integer> deviceTypes) {
        this.deviceTypes = deviceTypes;
    }

    public Boolean isHavingVirtualIndicatorInfo() {
        return havingVirtualIndicatorInfo;
    }

    public void setHavingVirtualIndicatorInfo(Boolean havingVirtualIndicatorInfo) {
        this.havingVirtualIndicatorInfo = havingVirtualIndicatorInfo;
    }

    public Boolean isHavingEmployeeInfo() {
        return havingEmployeeInfo;
    }

    public void setHavingEmployeeInfo(Boolean havingEmployeeInfo) {
        this.havingEmployeeInfo = havingEmployeeInfo;
    }
    public boolean isHavingAccessKeyValue() {
        return havingAccessKeyValue;
    }

    public void setHavingAccessKeyValue(boolean havingAccessKeyValue) {
        this.havingAccessKeyValue = havingAccessKeyValue;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
    public String getDeviceActionKey() {
        return deviceActionKey;
    }

    public void setDeviceActionKey(String deviceActionKey) {
        this.deviceActionKey = deviceActionKey;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public Boolean isVirtualStateOn() {
        return virtualStateOn;
    }

    public void setVirtualStateOn(Boolean virtualStateOn) {
        this.virtualStateOn = virtualStateOn;
    }
}
