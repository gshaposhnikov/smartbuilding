package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип охранной зоны.
 * 
 * Используется для указания выполнения некоторых специфических действий.
 * 
 * @author Александр Горячкин
 */
public enum SecurityRegionType {
    /** Обычная */
    DEFAULT,
    /** С задержкой входа/выхода */
    TIMEOUT,
    /** Без права снятия */
    NO_OFF
}
