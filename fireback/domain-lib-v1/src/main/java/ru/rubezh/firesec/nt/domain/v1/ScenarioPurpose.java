package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип назначения сценария.
 * 
 * @author Антон Васильев
 *
 */
public enum ScenarioPurpose {

    /** Управляющий сценарий - запускаемый по логическому условию или по команде пользователя */
    EXEC_BY_LOGIC,
    /** Исполнительный сценарий - запускаемый из других сценариев */
    EXEC_BY_INVOKE,
    /** Специальный сценарий - запускаемый по состоянию охранной зоны*/
    EXEC_BY_TACTICS
}
