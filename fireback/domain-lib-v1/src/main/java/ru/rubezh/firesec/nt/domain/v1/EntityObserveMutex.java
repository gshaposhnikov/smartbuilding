package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Мютекс наблюдения за сущностями.
 * 
 * Предназначен для разграничения во времени операций по записи/чтению наблюдаемых оперативных сущностей.
 * 
 * @author Антон Васильев
 *
 */
@Document(collection = "entity_observer_mutexes")
public class EntityObserveMutex extends ReferenceEntity {

    @Indexed
    private boolean locked;

    @Indexed
    private Date lockExpire;

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Date getLockExpire() {
        return lockExpire;
    }

    public void setLockExpire(Date lockExpire) {
        this.lockExpire = lockExpire;
    }

}
