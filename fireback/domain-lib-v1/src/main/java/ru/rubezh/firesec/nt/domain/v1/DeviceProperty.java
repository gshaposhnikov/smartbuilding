package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;
import java.util.List;

/**
 * Сущность, описывающая один параметр устройства.
 */
public class DeviceProperty extends AdjectiveEntity {

    /** Тип параметра */
    protected PropertyType type;

    /** Минимальное допустимое значение. Может быть null, если ограничения снизу нет. */
    protected String min;

    /** Максимальное допустимое значение. Может быть null, если ограничения сверху нет.  */
    protected String max;

    /** Список возможных значений. Если не задан, возможными считаются все значения между min и max. */
    protected List<String> possibleValues;

    /** Значение по умолчанию */
    private String defaultValue;

    /** Является ли этот параметр ненастраиваемым пользователем */
    private boolean readOnly = false;

    /** Может ли параметр принимать значение null */
    private boolean nullable = true;

    /** Значение по умолчанию определяется отображением */
    private boolean viewInit = false;

    /** Идентификатор раздела, в который включен данный параметр */
    private String section;
    
    /** Идентификатор параметра старой версии firesec NG */
    private String desktopFSPropertyId;

    /** Тип сущности (для типа параметра - идентификатор сущности) */
    private EntityType entityType;

    /** Подсистема сущностий (для типа параметра - идентификатор сущности) */
    private Subsystem entitySubsystem;

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(List<String> possibleValues) {
        this.possibleValues = possibleValues;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable_) {
        nullable = nullable_;
    }

    public boolean isViewInit() {
        return viewInit;
    }

    public void setViewInit(boolean viewInit_) {
        viewInit = viewInit_;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section_) {
        section = section_;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Subsystem getEntitySubsystem() {
        return entitySubsystem;
    }

    public void setEntitySubsystem(Subsystem entitySubsystem) {
        this.entitySubsystem = entitySubsystem;
    }

    // Проверка значения перед cохранением в БД
    public boolean checkValue(String value) {
        if ((value == null) || value.isEmpty())
            return nullable;
        if (possibleValues != null)
            return possibleValues.contains(value);
        switch (type) {
            case TEXT:
                return checkText(value);
            case INTEGER:
                return checkInteger(value);
            case FLOAT:
                return checkFloat(value);
            case DATE:
                return checkDate(value);
            case FLAG:
            case ICON:
            case ENTITY_ID: /* Идентификатор сущности должен проверяться в сервисе */
                return true;
            default:
                return false;
        }
    }

    // Чтение целого значения из строки или значния по умолчанию параметра
    public static int getIntegerOrDefault(String value, DeviceProperty property) {
        try {
            if (value != null)
                return Integer.parseInt(value);
        } catch (NumberFormatException ex) {}
        try {
            if (property != null && property.getDefaultValue() != null)
                return Integer.parseInt(property.getDefaultValue());
        } catch (NumberFormatException ex) {}
        return 0;
    }

    // Чтение булевого значения из строки или значния по умолчанию параметра
    public static boolean getFlagOrDefault(String value, DeviceProperty property) {
        if (value != null)
            return Boolean.parseBoolean(value);
        if (property != null) {
            return Boolean.parseBoolean(property.getDefaultValue());
        }
        return false;
    }

    // Запись даты/времени в строку
    static public String getDate(Date date) {
        return Long.toString(date.getTime());
    }

    private boolean checkText(String value) {
        try {
            int length = value.length();
            if (min != null) {
                int minLength = Integer.parseInt(min);
                if (length < minLength)
                    return false;
            }
            if (max != null) {
                int maxLength = Integer.parseInt(max);
                if (length > maxLength)
                    return false;
            }
            return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean checkDate(String value) {
        try {
            long longValue = Long.parseLong(value);
            return longValue >= 0;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean checkInteger(String value) {
        try {
            int intValue = Integer.parseInt(value);
            if (min != null) {
                int minValue = Integer.parseInt(min);
                if (intValue < minValue)
                    return false;
            }
            if (max != null) {
                int maxValue = Integer.parseInt(max);
                if (intValue > maxValue)
                    return false;
            }
            return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean checkFloat(String value) {
        try {
            float floatValue = Float.parseFloat(value);
            if (min != null) {
                float minValue = Float.parseFloat(min);
                if (floatValue < minValue)
                    return false;
            }
            if (max != null) {
                float maxValue = Float.parseFloat(max);
                if (floatValue > maxValue)
                    return false;
            }
            return true;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }

    public static String getString(Device device, DeviceProperties propertyKind) {
        String string = device.getDevicePropertyValues().get(propertyKind.toString());
        if ((string == null) || string.isEmpty())
            return null;
        else
            return string;
    }

    public static int getInteger(Device device, DeviceProfile deviceProfile, DeviceProperties propertyKind) {
        return DeviceProperty.getIntegerOrDefault(
                device.getDevicePropertyValues().get(propertyKind.toString()),
                deviceProfile.getDeviceProperties().get(propertyKind.toString()));
    }

    public static boolean getBoolean(Device device, DeviceProfile deviceProfile, DeviceProperties propertyKind) {
        return DeviceProperty.getFlagOrDefault(
                device.getDevicePropertyValues().get(propertyKind.toString()),
                deviceProfile.getDeviceProperties().get(propertyKind.toString()));
    }

    public String getDesktopFSPropertyId() {
        return desktopFSPropertyId;
    }

    public void setDesktopFSPropertyId(String desktopFSPropertyId) {
        this.desktopFSPropertyId = desktopFSPropertyId;
    }

}
