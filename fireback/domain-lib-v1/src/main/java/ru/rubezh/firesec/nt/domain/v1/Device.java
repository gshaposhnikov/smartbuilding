package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

/**
 * Устройство.
 * 
 * Описывает характеристики конкретного устройства (проектная часть).
 * 
 * @author Александр Горячкин, Антон Васильев
 * @ingroup BasicDomain
 */

@Document(collection="devices")
@TypeAlias(value="Device.v1")
@JsonTypeName("Device.v1")
@CompoundIndex(def = "{'projectId':1, 'addressPath':1}", unique = true)
public class Device extends BasicEntity {

    /** Тип (идентификатор профиля) устройства*/
    @Indexed
    private String deviceProfileId = "";

    /**
     * Категория устройства (должна совпадать с категорией, заданной в профиле)
     */
    @Indexed
    DeviceCategory deviceCategory = DeviceCategory.CONTROL;

    /**
     * Устройство может использоваться в условиях исполнительных блоков
     * сценариев или в блоках слежения сценариев
     */
    private boolean acceptableAsScenarioCondition = false;

    /**
     * Множество допустимых типов (идентификаторо профилей), которые можно
     * назначить устройству. Заполняется при создании устройства в зависимости
     * от того, куда устройство подключается (кто его родитель), и вообще от
     * списка поддерживаемых типов устройств на момент создания.
     */
    private Set<String> acceptableDeviceProfileIds = new LinkedHashSet<>();

    /** Проект (идентификатор) */
    private String projectId = "";

        /* Адресация в дереве устройств */
    /** Идентификатор родительского устройства */
    @Indexed
    private String parentDeviceId;
    /**
     * Полный адресный путь в дереве устройств
     * Формат: {@code <parentAddressPath,lineNo,lineAddress> }
     * Если у устройства нет родителя, то в адресе будет {@code <,lineAddress> }
     */
    private String addressPath = "";
    /**
     * Уровень адреса (кол-во узлов в адресе).
     */
    @Indexed
    private int addressLevel = 1;
    /** Возможность редактировать адрес устройства */
    private boolean editableAddress = true;

        /* Адресация в сети RS-485 */
    /**
     * Адрес родительского устройства (на линии пра-родительского).
     * Если родительское устройство - контейнер, то здесь должен быть адрес пра-родительского устройства.
     **/
    private int parentLineAddress;

    /** Номер линии (родительского устройства)
     * [1 .. deviceProfile.getLineCount()]
     */
    @Indexed
    private int lineNo;

    /** Собственный адрес (на линии)
     * [1 .. deviceProfile.getLineDeviceCount()]
     */
    @Indexed
    private int lineAddress;

    /** Минимальный возможный адрес */
    public final static int MIN_LINE_ADDRESS = 1;
    /** Максимальный возможный адрес */
    public final static int MAX_LINE_ADDRESS = 255;

    /** Идентификатор зоны, если устройство привязываемо к зоне */
    @Indexed
    private String regionId;

    /**
     * Дупликат идентификатора зоны в формате ObjectId (для сопоставления в
     * lookUp-операторах при агрегировании)
     */
    @JsonIgnore
    private ObjectId regionOid;

    /** Параметры подключения (согласно профилю устройства) */
    private Map<String, String> connectionProperties = new HashMap<>();

    /** Значения параметров конфигурации в проекте (согласно профилю устройства) */
    private Map<String, String> projectConfigValues = new HashMap<>();

    /** Значения параметров устройства в проекте (согласно профилю устройства) */
    private Map<String, String> devicePropertyValues = new HashMap<>();

        /* Атрибуты приборов и транспортных устройств */
    /** Диапазоны свободных адресов (свой список диапазонов на каждую линию) */
    private List<List<AddressRange>> lineAddressRanges = new ArrayList<>();

        /* Атрибуты приборов */
    /** Пароль инсталлятора */
    private String installerPassword = "";
    /** Пароль администратора */
    private String adminPassword = "";
    /** Пароль дежурного */
    private String dutyPassword = "";

    /** Отключение устройства запрещено */
    private boolean disableNotAllowed = false;
    /** Устройство отключено */
    private boolean disabled = false;

    /** Признак встроенного устройства */
    private boolean embedded = false;
    /** Тип адреса устройства */
    private AddressType addressType = AddressType.GENERIC;

    /** Количество динамически подключенных внутренних устройств (по типам) */
    private Map<String, Integer> nAttachedInternalDevices = new HashMap<>();

        /* Атрибуты виртуальных контейнеров */
    /** Отображение Id профилей устройств на списки Id связанных устройств */
    private Map<String, Set<String>> linkedDeviceIdsMap = new HashMap<>();
    /** Идентификатор виртуального контейнера, содержащего ссылку на устройство */
    private String virtualContainerId;

    private FilterTags filterTags = new FilterTags();
    /** Подсистема для фильтрации, задаваемая пользователем */
    private Subsystem customSubsystem;

    public Device() {
        super();
    }

    public Device(String deviceProfileId,
            DeviceCategory deviceCategory,
            String projectId,
            String parentDeviceId,
            int parentLineAddress, int lineNo,
            int lineAddress, String addressPath) {
        super();
        this.deviceProfileId = deviceProfileId;
        this.deviceCategory = deviceCategory;
        this.projectId = projectId;
        this.parentDeviceId = parentDeviceId;
        this.parentLineAddress = parentLineAddress;
        this.lineNo = lineNo;
        this.lineAddress = lineAddress;
        this.addressPath = addressPath;
    }

    public boolean isEditableAddress() {
        return editableAddress;
    }

    public void setEditableAddress(boolean editableAddress) {
        this.editableAddress = editableAddress;
    }

    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public boolean isAcceptableAsScenarioCondition() {
        return acceptableAsScenarioCondition;
    }

    public void setAcceptableAsScenarioCondition(boolean acceptableAsScenarioCondition) {
        this.acceptableAsScenarioCondition = acceptableAsScenarioCondition;
    }

    public Set<String> getAcceptableDeviceProfileIds() {
        return acceptableDeviceProfileIds;
    }

    public void setAcceptableDeviceProfileIds(Set<String> acceptableDeviceProfileIds) {
        this.acceptableDeviceProfileIds = acceptableDeviceProfileIds;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public int getParentLineAddress() {
        return parentLineAddress;
    }

    public void setParentLineAddress(int parentLineAddress) {
        this.parentLineAddress = parentLineAddress;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getLineAddress() {
        return lineAddress;
    }

    public void setLineAddress(int lineAddress) {
        this.lineAddress = lineAddress;
    }

    public String getAddressPath() {
        return addressPath;
    }

    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    public int getAddressLevel() {
        return addressLevel;
    }

    public void setAddressLevel(int addressLevel) {
        this.addressLevel = addressLevel;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
        this.regionOid = (regionId != null)
            ? new ObjectId(regionId)
            : null;
    }

    public ObjectId getRegionOid() {
        return regionOid;
    }

    public void setRegionOid(ObjectId regionOid) {
        this.regionOid = regionOid;
    }

    public Map<String, String> getConnectionProperties() {
        return connectionProperties;
    }

    public void setConnectionProperties(Map<String, String> connectionProperties) {
        this.connectionProperties = connectionProperties;
    }

    public Map<String, String> getDevicePropertyValues() {
        return devicePropertyValues;
    }

    public void setDevicePropertyValues(Map<String, String> devicePropertyValues) {
        this.devicePropertyValues = devicePropertyValues;
    }

    public Map<String, String> getProjectConfigValues() {
        return projectConfigValues;
    }

    public void setProjectConfigValues(Map<String, String> projectConfigValues) {
        this.projectConfigValues = projectConfigValues;
    }

    public List<List<AddressRange>> getLineAddressRanges() {
        return lineAddressRanges;
    }

    public void setLineAddressRanges(List<List<AddressRange>> lineAddressRanges) {
        this.lineAddressRanges = lineAddressRanges;
    }

    public String getInstallerPassword() {
        return installerPassword;
    }

    public void setInstallerPassword(String installerPassword) {
        this.installerPassword = installerPassword;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

    public String getDutyPassword() {
        return dutyPassword;
    }

    public void setDutyPassword(String dutyPassword) {
        this.dutyPassword = dutyPassword;
    }

    public boolean isDisableNotAllowed() {
        return disableNotAllowed;
    }

    public void setDisableNotAllowed(boolean disableNotAllowed) {
        this.disableNotAllowed = disableNotAllowed;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isEmbedded() {
        return embedded;
    }

    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public Map<String, Integer> getnAttachedInternalDevices() {
        return nAttachedInternalDevices;
    }

    public void setnAttachedInternalDevices(Map<String, Integer> nAttachedInternalDevices) {
        this.nAttachedInternalDevices = nAttachedInternalDevices;
    }

    public Map<String, Set<String>> getLinkedDeviceIdsMap() {
        return linkedDeviceIdsMap;
    }

    public void setLinkedDeviceIdsMap(Map<String, Set<String>> linkedDeviceIdsMap) {
        this.linkedDeviceIdsMap = linkedDeviceIdsMap;
    }

    public String getVirtualContainerId() {
        return virtualContainerId;
    }

    public void setVirtualContainerId(String virtualContainerId) {
        this.virtualContainerId = virtualContainerId;
    }

    public Subsystem getCustomSubsystem() {
        return customSubsystem;
    }

    public void setCustomSubsystem(Subsystem customSubsystem) {
        this.customSubsystem = customSubsystem;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
