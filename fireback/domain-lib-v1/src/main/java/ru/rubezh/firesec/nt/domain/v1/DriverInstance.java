package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Экземпляр драйвера.
 *
 * @author Артем Седанов
 * @ingroup BasicSystem
 */
@Document(collection = "driver_instances")
@TypeAlias(value = "DriverInstance")
@JsonTypeName("DriverInstance")
public class DriverInstance extends BasicEntity {

    @JsonIgnore
    @Transient
    static final public String DEFAULT_DRIVER_COMMAND_LINE = "java -jar driver-smart-controller.jar";

    /** Идентификатор профиля драйвера */
    private String profileId;

    /** Имя очереди для отправки запросов драйверу */
    private String requestQueueName;

    /** Командная строка, запускающая драйвер */
    private String commandLine = DEFAULT_DRIVER_COMMAND_LINE;

    public DriverInstance() {

    }

    public DriverInstance(String id, String profileId, String requestQueueName, String commandLine) {
        this.id = id;
        this.profileId = profileId;
        this.requestQueueName = requestQueueName;
        this.commandLine = commandLine;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getRequestQueueName() {
        return requestQueueName;
    }

    public void setRequestQueueName(String requestQueueName) {
        this.requestQueueName = requestQueueName;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

}
