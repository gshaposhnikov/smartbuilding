package ru.rubezh.firesec.nt.domain.v1;

import java.util.Arrays;
import java.util.BitSet;

/**
 * Сущность, описывающая один параметр конфигурации устройства.
 * 
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class DeviceConfigProperty extends DeviceProperty {

    /** Множитель, на который нужно умножать значение перед записью и на который делить при чтении */
    private int multiplier = 1;

    /** Сдвиг в памяти, где начинается этот параметр */
    private int shiftInMemory = 0;

    /** Номер первого бита параметра */
    private int bitRangeFrom = 0;

    /** Номер последнего бита параметра */
    private int bitRangeTo = 7;

    /** Дополнительный сдвиг на приращение адреса устройства, в байтах */
    private int bytesShiftByEachAddressShift = 0;

    /** Является ли этот параметр скрытым (ненастраиваемым пользователем) */
    private boolean hidden = false;
    
    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getShiftInMemory() {
        return shiftInMemory;
    }

    public void setShiftInMemory(int shiftInMemory) {
        this.shiftInMemory = shiftInMemory;
    }

    public int getBitRangeFrom() {
        return bitRangeFrom;
    }

    public void setBitRangeFrom(int bitRangeFrom) {
        this.bitRangeFrom = bitRangeFrom;
    }

    public int getBitRangeTo() {
        return bitRangeTo;
    }

    public void setBitRangeTo(int bitRangeTo) {
        this.bitRangeTo = bitRangeTo;
    }

    public int getBytesShiftByEachAddressShift() {
        return bytesShiftByEachAddressShift;
    }   

    public void setBytesShiftByEachAddressShift(int bytesShiftByEachAddressShift) {
        this.bytesShiftByEachAddressShift = bytesShiftByEachAddressShift;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /** Установить одновременно bitRangeFrom и bitRangeTo */
    public void setBitNo(int bitNo) {
        this.bitRangeFrom = bitNo;
        this.bitRangeTo = bitNo;
    }

    /** Раскодировать значение из переданных данных */
    public String decode(byte[] data) {
        int numOfBytes = bitRangeTo / 8 + 1;
        int firstByteIndex = shiftInMemory;
        BitSet bits = BitSet.valueOf(Arrays.copyOfRange(data, firstByteIndex, firstByteIndex + numOfBytes))
                .get(bitRangeFrom, bitRangeTo + 1);
        int rawVal = bits.isEmpty() ? 0 : (int)bits.toLongArray()[0];

        String resultStr = null;
        if (type == PropertyType.INTEGER) {
            int result = rawVal / multiplier;
            resultStr = Integer.toString(result);
        } else if (type == PropertyType.FLOAT) {
            float result = rawVal / (float) multiplier;
            resultStr = Float.toString(result);
        }
        return resultStr;
    }

    /** Закодировать значение (с учётом адреса устройства) */
    public boolean encode(byte[] dest, String value, int address) {
        int rawVal = 0;
        try {
            if (type == PropertyType.INTEGER) {
                rawVal = Integer.parseInt(value) * multiplier;
            } else if (type == PropertyType.FLOAT) {
                rawVal = Math.round(Float.parseFloat(value) * multiplier);
            }
        } catch (Exception e) {
            return false;
        }
        if (rawVal < 0) {
            return false;
        }

        int numOfBytes = bitRangeTo / 8 + 1;
        int firstByteIndex = shiftInMemory + (address - 1) * bytesShiftByEachAddressShift;
        BitSet bits = BitSet.valueOf(Arrays.copyOfRange(dest, firstByteIndex, firstByteIndex + numOfBytes));
        bits.clear(bitRangeFrom, bitRangeTo + 1);
        bits.or(BitSet.valueOf(new long[]{ rawVal << bitRangeFrom }).get(0, bitRangeTo + 1));
        byte[] bytes = bits.isEmpty() ? new byte[numOfBytes] : bits.toByteArray();
        System.arraycopy(Arrays.copyOfRange(bytes, 0, numOfBytes), 0, dest, firstByteIndex, numOfBytes);

        return true;
    }

    public static int getInteger(Device device, DeviceProfile deviceProfile, DeviceProperties propertyKind) {
        return DeviceProperty.getIntegerOrDefault(
                device.getProjectConfigValues().get(propertyKind.toString()),
                deviceProfile.getConfigProperties().get(propertyKind.toString()));
    }

}
