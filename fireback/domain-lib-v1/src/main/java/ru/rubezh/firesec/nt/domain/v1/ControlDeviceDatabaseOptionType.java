package ru.rubezh.firesec.nt.domain.v1;

/**
 * Список опций, которые нужно писать в БД прибора для исполнительных устройств и датчиков.
 *
 * @author Артем Седанов (artem_sedanov@satellite-soft.ru)
 *
 */
public enum ControlDeviceDatabaseOptionType {
    // Для исполнительных устройств:
    /** Имеется недоставляемая конфигурация */
    EXECUTIVE_HAS_UNDELIVERABLE_CONFIG,
    /** Недоставляемая конфигурация: признак светозвукового оборудования на РМ */
    EXECUTIVE_HAS_LIGHT_AND_SOUND_BIT,
    /** Недоставляемая конфигурация: признак первого занятого адреса многоадресного устройства */
    EXECUTIVE_HAS_FIRST_ADDRESS_BIT,
    /** Недоставляемая конфигурация: смещение адреса с 0 от начала у многоадресного устройства */
    EXECUTIVE_HAS_ADDRESS_OFFSET,
    /** Имеется описание состояний Вкл / Выкл */
    EXECUTIVE_HAS_ON_OFF_DESCRIPTIONS,
    /** Недоставляемая конфигурация: признак привязки логики "неисправность" */
    EXECUTIVE_HAS_DEVICE_FAILURE_FEATURE,
    /** Недоставляемая конфигурация: признак привязки логики "включение по пожару в 2х зонах" */
    EXECUTIVE_HAS_TWO_REGIONS_FIRE_FEATURE,
    /** Имеется виртуальный тип */
    EXECUTIVE_HAS_VIRTUAL_TYPE,
    /** Имеется номер направления */
    EXECUTIVE_HAS_DIRECTION_NUMBER,
    /** Зона устройства записывается в таблицу испольнительных устройств */
    EXECUTIVE_HAS_REGION,
    /** Виртуальный тип устройства задаётся конфигурационным параметром */
    EXECUTIVE_HAS_VIRTUAL_TYPE_CONFIG_PROPERTY,

    // Для сенсоров:
    /** Имеется виртуальный тип */
    SENSOR_HAS_VIRTUAL_TYPE,
    /** Имеется недоставляемая конфигурация */
    SENSOR_HAS_UNDELIVERABLE_CONFIG,
    /** Недоставляемая конфигурация: в составе ППУ (панель противопожарных устройств - fire extinguishing panel) */
    SENSOR_HAS_FEP_BIT,
    /** Недоставляемая конфигурация: признак первого занятого адреса многоадресного устройства */
    SENSOR_HAS_FIRST_ADDRESS_BIT,
    /** Недоставляемая конфигурация: смещение адреса с 0 от начала у многоадресного устройства */
    SENSOR_HAS_ADDRESS_OFFSET,
    /** Имеется описание состояний Норма / Сработка 1го датчика / Сработка 2го датчика / Сработка 2х датчиков */
    SENSOR_HAS_FOUR_STATE_DESCRIPTIONS,
    /** Есть связанные блоки времени */
    SENSOR_HAS_ASSOCIATED_TIMELINE_BLOCKS,
    /** Зона устройства записывается в блок произвольных данных (смещение задаётся полем arbitraryDataRegionOffset) */
    SENSOR_ARBITRARY_DATA_HAS_REGION,

    // Для всех устройств:
    /** Конфигурация устройства должна содержать CRC */
    DEVICE_CONFIG_HAS_CRC,
    /** Устройство является модулем пожаротушения (МПТ) */
    DEVICE_IS_MPT,
    /** Конфигурация устройства пишется в специальную область БД */
    DEVICE_HAS_EXTRA_CONFIG,

    // Для виртуальных контейнеров
    /** Конфигурация записывается в таблицу ВК */
    VIRTUAL_CONTAINER_HAS_CONFIG,
}
