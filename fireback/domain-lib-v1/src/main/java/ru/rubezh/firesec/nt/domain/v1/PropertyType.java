package ru.rubezh.firesec.nt.domain.v1;

import java.util.EnumMap;
import java.util.HashMap;

/**
 * Тип параметра устройства
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru), Андрей Лисовой
 */
public enum PropertyType {
    ICON("icon"),
    TEXT("text"),
    FLAG("flag"),
    DATE("date"),
    INTEGER("integer"),
    FLOAT("float"),
    SECTION("section"),
    ENTITY_ID("entityId");

    private final String value;

    private PropertyType(String value) {
        this.value = value;
    }

    private static final EnumMap<PropertyType, String> valuesByType = new EnumMap<>(PropertyType.class);
    private static final HashMap<String, PropertyType> typesByValue = new HashMap<>();
    static {
        for (PropertyType type : PropertyType.values()) {
            valuesByType.put(type, type.value);
            typesByValue.put(type.value, type);
        }
    }

    public static String getValue(PropertyType type) {
        return valuesByType.get(type);
    }

    public static PropertyType getType(String value) {
        return typesByValue.get(value);
    }
}
