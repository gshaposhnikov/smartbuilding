package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Профиль драйвера.
 * 
 * Описывает характеристики (модели всех справочных сущностей) конкретного типа драйвера.
 *
 * @author Александр Горячкин
 * @ingroup BasicReferences
 */
@Document(collection = "driver_profiles")
@TypeAlias(value = "DriverProfile")
public class DriverProfile extends ReferenceEntity {

    /*
     * TODO: переделать, чтобы сущности профилей устройств и состояний
     * находились непосредственно в профиле драйвера с аннотацией DBRef, а в
     * отображениях - идентификаторы.
     *
     * КС должен их получать при регистрации драйвера не из отображений, а из
     * профиля драйвера. Отображения должны запрашиваться отдельно и
     * опционально. Бизнес-логика не должна быть завязана на UI-сущности.
     */

    /** Набор поддерживаемых профилей устройств */
    private List<String> deviceProfileIds = new ArrayList<>();

    /*
     * Все нижеследующие параметры реализованы по принципу вышеописанного TODO
     */

    /** Наборы раскодируемых из событий состояний */
    @DBRef
    private List<DecodableEventStateSet> decodableEventStateSets = new ArrayList<>();

    /** Профили событий, поддерживаемых драйвером */
    @DBRef
    private List<EventProfile> eventProfiles = new ArrayList<>();

    /** Типы поддерживаемых драйвером триггеров сценариев */
    @DBRef
    private List<ScenarioTriggerType> scenarioTriggerTypes = new ArrayList<>();

    /** Типы действий сценариев, поддерживаемых драйвером */
    @DBRef
    private List<ScenarioActionType> scenarioActionTypes = new ArrayList<>();

    public List<String> getDeviceProfileIds() {
        return deviceProfileIds;
    }

    public void setDeviceProfileIds(List<String> deviceProfileIds) {
        this.deviceProfileIds = deviceProfileIds;
    }

    public List<DecodableEventStateSet> getDecodableEventStateSets() {
        return decodableEventStateSets;
    }

    public void setDecodableEventStateSets(List<DecodableEventStateSet> decodableEventStateSets) {
        this.decodableEventStateSets = decodableEventStateSets;
    }

    public List<EventProfile> getEventProfiles() {
        return eventProfiles;
    }

    public void setEventProfiles(List<EventProfile> eventProfiles) {
        this.eventProfiles = eventProfiles;
    }

    public List<ScenarioTriggerType> getScenarioTriggerTypes() {
        return scenarioTriggerTypes;
    }

    public void setScenarioTriggerTypes(List<ScenarioTriggerType> scenarioTriggerTypes) {
        this.scenarioTriggerTypes = scenarioTriggerTypes;
    }

    public List<ScenarioActionType> getScenarioActionTypes() {
        return scenarioActionTypes;
    }

    public void setScenarioActionTypes(List<ScenarioActionType> scenarioActionTypes) {
        this.scenarioActionTypes = scenarioActionTypes;
    }

}
