package ru.rubezh.firesec.nt.domain.v1;

/**
 * Подсистема.
 * 
 * Используется для разграничения других сущностей по сфере использования.
 * 
 * @author Александр Горячкин
 * @ingroup BasicReferences
 */
public enum Subsystem {
    /** Не известная (не указанная)*/
    UNKNOWN(false),
    /** общая */
    GENERAL(false),
    /** пожарная */
    FIRE(true),
    /** охранная */
    SECURITY(true),
    /** СКУД */
    SKUD(true),
    /** автоматика */
    AUTO(false);

    private boolean isAFilterTag;

    Subsystem(boolean isAFilterTag){
        this.isAFilterTag = isAFilterTag;
    }

    public boolean isAFilterTag() {
        return isAFilterTag;
    }

    public void setAFilterTag(boolean AFilterTag) {
        this.isAFilterTag = AFilterTag;
    }}
