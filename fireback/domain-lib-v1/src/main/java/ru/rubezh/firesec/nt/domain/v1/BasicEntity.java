package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.Id;

/**
 * Базовый класс для определения сущностей, содержащих идентификатор.
 * 
 * @author Антон Васильев
 *
 */
public class BasicEntity {
    /** Идентификатор */
    @Id
    protected String id;
    
    public BasicEntity() {
    }

    /**
     * Если использовать наследников класса в хэш-сете, то может возникнуть ситуация,
     * когда в коллекции лежат одни и те же обьекты.
     * Чтобы в сете не было повторяющихся элементов, добавлены {@code hashCode()} и {@code equals()}.
     */
    @Override
    public boolean equals(Object o) {
        if (id == null){
           return super.equals(o);
        } else {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BasicEntity entity = (BasicEntity) o;
            return id.equals(entity.id);
        }
    }

    @Override
    public int hashCode() {
        if (id == null){
            return super.hashCode();
        } else {
            return id.hashCode();
        }
    }

    public BasicEntity(BasicEntity source) {
        this.id = source.id;
    }
    
    public String getId() {
        return id;
    }
    
    public void clearId () {
        id = null;
    }

}
