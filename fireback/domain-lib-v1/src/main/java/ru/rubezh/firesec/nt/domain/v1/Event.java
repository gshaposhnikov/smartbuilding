package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Событие.
 * 
 * Описывает характеристики элемента журнала событий.
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 */
@Document(collection = "events")
@TypeAlias(value="Event.v1")
@JsonTypeName("Event.v1")
public class Event extends BasicEntity {
    
    /** Идентификатор типа события */
    @Indexed
    private String eventTypeId = "";
    
    /** Подсистема события */
    private Subsystem subsystem = Subsystem.UNKNOWN;

    /** Журнал, из которого получено событие */
    private String logTypeId = null;

    /** Время возникновения события */
    @Indexed
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date occurred = null;
    
    /* Атрибуты, относящиеся к событиям, получаемым с приборов (havingControlDeviceInfo == true в EventType) */
    
    /** Время получения события с прибора */
    @Indexed
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date received = new Date();
    
    /** Номер записи в журнале событий прибора */
    @Indexed
    private long recordNo;
    
    /** Идентификатор прибора, с которого получено событие */
    @Indexed
    private String controlDeviceId = "";
    
    /** Флаг - событие получено в реальном времени по опросу */
    @Indexed
    private boolean online;

    /* Атрибуты, относящиеся к устройствам (не приборам) (havingDeviceInfo == true в EventType) */
    
    /** Идентификатор устройства */
    @Indexed
    private String deviceId = "";

    /** Перечень активных состояний устройства */
    @Indexed
    private List<String> activeDeviceStateIds = new ArrayList<>();

    /** Перечень состояний прибора */
    private List<String> controlDeviceStateIds = new ArrayList<>();
    
    /* TODO: значения параметров*/
    
    /* Атрибуты, относящиеся к зонам (havingRegionInfo == true в EventType) */
    
    /** Идентификатор зоны */
    @Indexed
    private String regionId = "";

    /** Перечень активных состояний зоны */
    private List<String> activeRegionStateIds = new ArrayList<>();

    /* TODO: значения параметров*/
    
    /* Атрибуты, относящиеся к пользователям (havingUserInfo == true в EventType) */

    /** Идентификатор пользователя */
    @Indexed
    private String userId = "";

    /** Имя пользователя */
    private String userName = "";

    /* Атрибуты, относящиеся к вирт. состояниям (havingVirtualStateInfo == true в EventType) */

    /** Идентификатор вирт. состяния */
    @Indexed
    private String virtualStateId = "";

    /* Атрибуты, относящиеся к сценариям (havingScenarioInfo == true в EventType) */

    /** Идентификатор сценария */
    @Indexed
    private String scenarioId = "";

    /** Перечень активных состояний сценария */
    private List<String> scenarioStateIds = new ArrayList<>();

    /** Номер линии */
    @Indexed
    private int lineNo;

    /* Атрибуты, относящиеся к виртуальным индикаторам (havingVirtualIndicatorInfo == true в EventType) */

    /** идентификатора панели виртуальных индикаторов */
    private String indicatorPanelId = "";

    /** номер строки виртуального индикатора */
    private int indicatorRowNo;

    /** номер столбца виртуального индикатора */
    private int indicatorColNo;

    /** Событие пожаротушения, генерируемое данным событием */
    @Indexed
    private FireExtinctionEvent fireExtinctionEvent;

    private String employeeId = "";

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public String getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(String logTypeId) {
        this.logTypeId = logTypeId;
    }

    public Date getOccurred() {
        return occurred;
    }

    public void setOccurred(Date occurred) {
        this.occurred = occurred;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public long getRecordNo() {
        return recordNo;
    }

    public void setRecordNo(long recordNo) {
        this.recordNo = recordNo;
    }

    public String getControlDeviceId() {
        return controlDeviceId;
    }

    public void setControlDeviceId(String controlDeviceId) {
        this.controlDeviceId = controlDeviceId;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public List<String> getActiveDeviceStateIds() {
        return activeDeviceStateIds;
    }

    public void setActiveDeviceStateIds(List<String> activeDeviceStateIds) {
        this.activeDeviceStateIds = activeDeviceStateIds;
    }

    public List<String> getControlDeviceStateIds() {
        return controlDeviceStateIds;
    }

    public void setControlDeviceStateIds(List<String> controlDeviceStateIds) {
        this.controlDeviceStateIds = controlDeviceStateIds;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public List<String> getActiveRegionStateIds() {
        return activeRegionStateIds;
    }

    public void setActiveRegionStateIds(List<String> activeRegionStateIds) {
        this.activeRegionStateIds = activeRegionStateIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVirtualStateId() {
        return virtualStateId;
    }

    public void setVirtualStateId(String virtualStateId) {
        this.virtualStateId = virtualStateId;
    }

    public String getScenarioId() {
        return scenarioId;
    }

    public void setScenarioId(String scenarioId) {
        this.scenarioId = scenarioId;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public FireExtinctionEvent getFireExtinctionEvent() {
        return fireExtinctionEvent;
    }

    public void setFireExtinctionEvent(FireExtinctionEvent fireExtinctionEvent) {
        this.fireExtinctionEvent = fireExtinctionEvent;
    }

    public List<String> getScenarioStateIds() {
        return scenarioStateIds;
    }

    public void setScenarioStateIds(List<String> scenarioStateIds) {
        this.scenarioStateIds = scenarioStateIds;
    }

    public String getIndicatorPanelId() {
        return indicatorPanelId;
    }

    public void setIndicatorPanelId(String indicatorPanelId) {
        this.indicatorPanelId = indicatorPanelId;
    }

    public int getIndicatorRowNo() {
        return indicatorRowNo;
    }

    public void setIndicatorRowNo(int indicatorRowNo) {
        this.indicatorRowNo = indicatorRowNo;
    }

    public int getIndicatorColNo() {
        return indicatorColNo;
    }

    public void setIndicatorColNo(int indicatorColNo) {
        this.indicatorColNo = indicatorColNo;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
