package ru.rubezh.firesec.nt.domain.v1;

/**
 * Базовый класс сообщения валидации проекта.
 * 
 * @author Антон Васильев
 *
 */
public class ValidateMessage {
    
    /** Уровень сообщения. */
    public enum Level {
        /** Предупреждение */
        WARNING,
        /** Ошибка */
        ERROR
    };

    /** Текстовое описание сообщения */
    public String description;

    /** Тип сущности, к которой относится сообщение */
    public EntityType entityType;

    /** Идентификатор сущности, к которой относится сообщение */
    public String entityId;

    /** Уровень (важность) сообщения */
    public Level level;

    public ValidateMessage() {
    }
}
