package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Стандартная наблюдаемая величина.
 * 
 * @author Александр Горячкин
 * @ingroup BasicReferences
 */
@Document(collection="monitorable_value_profiles")
@TypeAlias(value="MonitorableValueProfile")
@JsonTypeName("MonitorableValueProfile")
public class MonitorableValueProfile extends ReferenceEntity {

    /** Значение по умолчанию */
    private String defaultValue = "0";

    /** Минимальный шаг изменения для сохранения в БД */
    private double updateMinStep = 0;


    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public double getUpdateMinStep() {
        return updateMinStep;
    }

    public void setUpdateMinStep(double updateMinStep) {
        this.updateMinStep = updateMinStep;
    }
}
