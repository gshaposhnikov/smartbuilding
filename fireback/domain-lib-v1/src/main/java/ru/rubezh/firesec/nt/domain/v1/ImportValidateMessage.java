package ru.rubezh.firesec.nt.domain.v1;

/**
 * Сообщение валидации, возвращаемое при импорте проекта.
 * 
 * @author Антон Лягин
 *
 */
public class ImportValidateMessage extends ValidateMessage{

    /** Код сообщения. */
    public enum Code {
        /* TODO: убрать хардкод описаний в XML */
        /** Если в блоке логике зупуска сценария не корректные данные*/
        SCENARIO_START_LOGIC_ERROR("Логика запуска не прошла валидацию"),
        /** Если в блоке логике остановки сценария не корректные данные*/
        SCENARIO_STOP_LOGIC_ERROR("Логика остановки не прошла валидацию"),
        /** Если устрйоство не смогли добавить в зону*/
        DEVICE_NOT_ADDED_IN_ZONE("Устройство не добавлено в зону"),
        /** Если исполнительный блок не прошел валидацию*/
        SCENARIO_TIME_LINE_BLOCK_NOT_ADDED("Исполнительный блок не добавлен в сценарий"),
        /** Если данные о сценарии были не валидны*/
        SCENARIO_NOT_ADDED("Сценарий не добавлен"),
        /** Если данные устройства не были валидны*/
        DEVICE_NOT_ADDED("Устройство не добавлено"),
        /** Если параметр устройства был не корректен или выходил за границу допустимых значений в свойствах "Прочие"*/
        DEVICE_PARAMETR_WAS_CHANCHED_TO_DEFAULT_VALUE_IN_OTHER_PROPERTY("Параметр был изменен на значение по умолчанию в свойствах \"Прочие\""),
        /** Если параметр устройства был не корректен или выходил за границу допустимых значений в свойствах "Конфигурирование"*/
        DEVICE_PARAMETR_WAS_CHANCHED_TO_DEFAULT_VALUE_IN_CONFIGURATION_PROPERTY("Параметр был изменен на значение по умолчанию в свойствах \"Конфигурирование\""),
        /** Если зона не добавилась*/
        REGION_NOT_ADDED("Зона не добавлена"),
        /** Если виртуальное состояние не добавилась*/
        VIRTUAL_STATE_NOT_ADDED("Вирстуальное состояние не добавлено"),
        /** Если устройство не смогли добавить в исполнительный блок*/
        DEVICE_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK("Устройство не добавлено в исполнительный блок сценария"),
        /** Если зону не смогли добавить в исполнительный блок*/
        REGIO_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK("Зона не добавлена в исполнительный блок сценария"),
        /** Если виртуальное состояние не смогли добавить в исполнительный блок*/
        VIRTUAL_STATE_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK("Вирутальное состояние не добавлено в исполнительный блок сценария"),
        /** Если сценарий не смогли добавить в исполнительный блок*/
        SCENARIO_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK("Сценарий не добавлен в исполнительный блок сценария"),
        /** Если мы пытались добавить устройство у коготого нам не известен профиль устройства*/
        UNKNOWN_DEVICE_NOT_ADDED("Неизвестное устройство не добавлено"),
        /** Если устрйоство не смогли добавить в индикатор*/
        DEVICE_NOT_ADDED_TO_VIRTUAL_INDICATOR("Устройство не добавлено в индикатор"),
        /** Если зону не смогли добавить в индикатор*/
        REGION_NOT_ADDED_TO_VIRTUAL_INDICATOR("Зона не добавлена в индикатор"),
        /** Если сценарий не смогли добавить в индикатор*/
        SCENARIO_NOT_ADDED_TO_VIRTUAL_INDICATOR("Сценарий не добавлен в индикатор"),
        /** Если панель индикатор не прошла валидацию*/ 
        VIRTUAL_INDICATOR_PANEL_NOT_ADDED("Панель индикаторов не добавлена"),
        /** Если индикатор не прошел валидацию*/
        VIRTUAL_INDICATOR_NOT_ADDED("Индикатор не добавлен"),
        /** Если группа панелей индикаторов не прошла валидацию*/
        VIRTUAL_INDICATOR_PANEL_GROUP_NOT_ADDED("Группа панелей индикаторов не добавлена");
        
        public final String description;

        private Code(String description) {
            this.description = description;
        }
    }
    /** Код сообщения */
    public Code code;
    
    public ImportValidateMessage() {
    }
   
    public ImportValidateMessage(Level level, Code code, EntityType entityType, String entityId) {
        this.level = level;
        this.code = code;
        this.description = code.description;
        this.entityType = entityType;
        this.entityId = entityId;
    }
    
    public ImportValidateMessage(Level level, Code code, String description, EntityType entityType, String entityId) {
        this.level = level;
        this.description = description;
        this.entityType = entityType;
        this.entityId = entityId;
        this.code = code;
    }
}
