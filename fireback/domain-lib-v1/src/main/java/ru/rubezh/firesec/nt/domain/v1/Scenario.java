package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Сценарий.
 * 
 * Запрограммированная функция, выполняемая одним или несколькими устройствами или выполняющая определенные действия на
 * компьютере при возникновении предустановленных условий, или по команде пользователя, или вызовом из другого сценария.
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 */
@Document(collection="scenarios")
@TypeAlias(value="Scenario")
@JsonTypeName("Scenario")
public class Scenario extends BasicEntity {
    /** Проект (идентификатор) */
    @Indexed
    private String projectId = "";

    /**Локальный номер (уникальный в приборе) */
    private int localNo;

    /** Глобальный номер (уникальный в проекте) */
    @Indexed
    private int globalNo;

    /** Название сценария (передаваемое в прибор) */
    private String name = "Сценарий";

    /** Описание */
    private String description = "";

    /** Назначение сценария (управляющий или исполнительный) */
    private ScenarioPurpose scenarioPurpose;

    /** Тип сценария (пожаротушение, инженерные работы, СОУЭ, без указания типа) */
    private ScenarioType scenarioType = ScenarioType.UNASSIGNED;
    
    /** Разрешено ручное управление (запуск/остановка/блокировка/разблокировка) из ОЗ */
    private boolean manualStartStopAllowed;

    /** Выключение сценария при их глобальном сбросе */
    private boolean stopOnGlobalReset;

    /** Тип выключения сценария. */
    public enum StopType {
        /**
         * Выключение сценария (возврат измененных им сущностей в исходное)
         * производится только по команде пользователя
         */
        MANUAL,
        /**
         * Выключение сценария (возврат измененных им сущностей в исходное)
         * производится автоматически после пропадания логики включения
         */
        AUTO,
        /**
         * Выключение сценария (возврат измененных им сущностей в исходное)
         * производится автоматически после сработки отдельной логики выключения
         */
        BY_LOGIC,
    }

    /** Тип выключения сценария */
    private StopType stopType = StopType.AUTO;
    
    /** Идентификатор приемно-контрольного устройства (актуален и обязателен для исполнительных сценариев) */
    private String controlDeviceId;

    /** Cценарий задействован */
    private boolean enabled;

    /** Логика включения сценария */
    private ScenarioLogicBlock startLogic = new ScenarioLogicBlock();
    
    /** Логика выключения сценария (актуальна, если stopType == BY_LOGIC) */
    private ScenarioLogicBlock stopLogic;
    
    /** Блоки сценария (исполняемые, слежения, действий на компьютере) */
    private List<ScenarioTimeLineBlock> timeLineBlocks = new ArrayList<>();

    private FilterTags filterTags = new FilterTags();
    
    public Scenario() {
    }
    
    public Scenario(Scenario source) {
        super(source);
        setProjectId(source.getProjectId());
        setLocalNo(source.getLocalNo());
        setGlobalNo(source.getGlobalNo());
        setName(source.getName());
        setScenarioPurpose(source.getScenarioPurpose());
        setScenarioType(source.getScenarioType());
        setManualStartStopAllowed(source.isManualStartStopAllowed());
        setStopOnGlobalReset(source.isStopOnGlobalReset());
        setStopType(source.getStopType());
        setStopLogic(source.getStopLogic());
        setControlDeviceId(source.getControlDeviceId());
        setEnabled(source.isEnabled());
        setStartLogic(source.getStartLogic());
        setTimeLineBlocks(source.getTimeLineBlocks());
    }
    
    public String getProjectId() {
        return projectId;
    }
    
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
    
    public int getLocalNo() {
        return localNo;
    }

    public void setLocalNo(int localNo) {
        this.localNo = localNo;
    }

    public int getGlobalNo() {
        return globalNo;
    }

    public void setGlobalNo(int globalNo) {
        this.globalNo = globalNo;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public ScenarioType getScenarioType() {
        return scenarioType;
    }

    public void setScenarioType(ScenarioType scenarioType) {
        this.scenarioType = scenarioType;
    }

    public boolean isManualStartStopAllowed() {
        return manualStartStopAllowed;
    }

    public void setManualStartStopAllowed(boolean manualStartStopAllowed) {
        this.manualStartStopAllowed = manualStartStopAllowed;
    }

    public boolean isStopOnGlobalReset() {
        return stopOnGlobalReset;
    }

    public void setStopOnGlobalReset(boolean stopOnGlobalReset) {
        this.stopOnGlobalReset = stopOnGlobalReset;
    }

    public StopType getStopType() {
        return stopType;
    }

    public void setStopType(StopType stopType) {
        this.stopType = stopType;
        if (stopType != StopType.BY_LOGIC) {
            this.stopLogic = null;
        }
    }

    public String getControlDeviceId() {
        return controlDeviceId;
    }

    public void setControlDeviceId(String controlDeviceId) {
        this.controlDeviceId = controlDeviceId;
    }

    public boolean isEnabled() {
        return enabled;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public ScenarioLogicBlock getStartLogic() {
        return startLogic;
    }

    public void setStartLogic(ScenarioLogicBlock startLogic) {
        this.startLogic = startLogic;
    }

    public ScenarioLogicBlock getStopLogic() {
        if (stopType == StopType.BY_LOGIC) {
            return stopLogic;
        }
        else {
            return null;
        }
    }

    public void setStopLogic(ScenarioLogicBlock stopLogic) {
        if (stopType == StopType.BY_LOGIC) {
            this.stopLogic = stopLogic;
        }
        else {
            this.stopLogic = null;
        }
    }

    public List<ScenarioTimeLineBlock> getTimeLineBlocks() {
        return timeLineBlocks;
    }

    public void setTimeLineBlocks(List<ScenarioTimeLineBlock> timeLineBlocks) {
        this.timeLineBlocks = timeLineBlocks;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
