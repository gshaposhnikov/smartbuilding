package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип сценария.
 * 
 * @author Антон Васильев
 *
 */
public enum ScenarioType {

    /** Пожаротушение */
    FIRE_FIGHTING,
    /** Инженерные работы */
    ENGINEERING,
    /** СОУЭ (по английски emergency notification system) */
    ENS,
    /** Без указания типа */
    UNASSIGNED
}
