package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Виртуальное состояние.
 * 
 * Состояние прибора, создаваемое пользователем, для отслеживания появления нескольких событий и запуска по ним
 * сценариев.
 * 
 * @author Александр Горячкин
 * @ingroup BasicDomain
 */
@Document(collection="virtual_states")
@TypeAlias(value="VirtualState")
@JsonTypeName("VirtualState")
public class VirtualState extends BasicEntity {

    public static int MESSAGE_AND_NAME_MAX_LENGTH = 20;
    public static int DESCRIPTION_MAX_LENGTH = 569;

    /** Название */
    private String name = "";
    /** Описание */
    private String description = "";
    /** Номер в проекте */
    @Indexed
    private int globalNo = 0;
    /** Категория состояния */
    private String stateCategoryId = null;
    /** Сообщение при постановке */
    private String messageOn = "";
    /** Сообщение при пропадании */
    private String messageOff = "";
    /** Идентификатор прибора */
    private String deviceId = "";
    /** Идентификатор проекта */
    @Indexed
    private String projectId = "";
    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    public void update(VirtualState virtualState) {
        this.name = virtualState.getName();
        this.description = virtualState.getDescription();
        this.globalNo = virtualState.getGlobalNo();
        this.stateCategoryId = virtualState.getStateCategoryId();
        this.messageOn = virtualState.getMessageOn();
        this.messageOff = virtualState.getMessageOff();
        this.deviceId = virtualState.getDeviceId();
        this.projectId = virtualState.getProjectId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGlobalNo() {
        return globalNo;
    }

    public void setGlobalNo(int globalNo) {
        this.globalNo = globalNo;
    }

    public String getStateCategoryId() {
        return stateCategoryId;
    }

    public void setStateCategoryId(String stateCategoryId) {
        this.stateCategoryId = stateCategoryId;
    }

    public String getMessageOn() {
        return messageOn;
    }

    public void setMessageOn(String messageOn) {
        this.messageOn = messageOn;
    }

    public String getMessageOff() {
        return messageOff;
    }

    public void setMessageOff(String messageOff) {
        this.messageOff = messageOff;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
