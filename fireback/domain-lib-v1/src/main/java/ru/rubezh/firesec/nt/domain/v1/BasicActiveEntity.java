package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.util.Date;

/**
 * Базовый класс для определения наблюдаемых (активных) сущностей.
 * 
 * @author Александр Горячкин
 */
public class BasicActiveEntity extends BasicEntity {

    /** Дата и время последнего изменения состояния сущности */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @Indexed
    protected Date stateGetDateTime = new Date();

    /** Флаг: состояние локальной копии сущности требует сохранения */
    @Transient
    @JsonIgnore
    protected boolean needsStateCommit;

    public Date getStateGetDateTime() {
        return stateGetDateTime;
    }

    public void setStateGetDateTime(Date stateGetDateTime) {
        this.stateGetDateTime = stateGetDateTime;
    }

    public boolean isNeedsStateCommit() {
        return needsStateCommit;
    }

    public void setNeedsStateCommit(boolean needsStateCommit) {
        this.needsStateCommit = needsStateCommit;
    }

}
