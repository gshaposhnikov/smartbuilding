package ru.rubezh.firesec.nt.domain.v1;

/**
 * Событие пожаротушения.
 * 
 * @author Андрей Лисовой
 *
 */
public enum FireExtinctionEvent {
    // Тушение запланировано (через delay секунд)
    SCHEDULED,
    // Тушение начато
    STARTED,
    // Тушение отменено
    CANCELLED,
    // Тушение отложено
    POSTPONED,
    // Отсчёт времени до тушения возобновлён
    RESUMED,
}
