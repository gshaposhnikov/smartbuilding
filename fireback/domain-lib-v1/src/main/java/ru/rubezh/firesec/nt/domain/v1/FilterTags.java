package ru.rubezh.firesec.nt.domain.v1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Метки для фильтрации данных, отображаемых пользователю в ОЗ.
 * 
 * @author Антон Караваев
 *
 */
public class FilterTags {

    private Set<String> tags = new HashSet<>();

    public FilterTags(){}

    public FilterTags(String... tags) {
        this.tags.addAll(Arrays.asList(tags));
    }

    public Set<String> getTags() {
        return tags;
    }

    public Set<String> chooseSubsystemTags(){
        Set<String> subsystemTags = new HashSet<>();
        this.tags.forEach(tag-> {
            for (Subsystem subsystem: Subsystem.values()){
                if (subsystem.toString().equals(tag)){
                    subsystemTags.add(tag);
                    break;
                }
            }
        });
        return subsystemTags;
    }

}
