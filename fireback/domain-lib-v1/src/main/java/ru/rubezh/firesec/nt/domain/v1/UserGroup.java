package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Группа пользователей.
 * 
 * Группа учётных записей, используется для назначения прав нескольким пользователям.
 * 
 * @author Александр Горячкин
 * @ingroup BasicSystem
 */
@Document(collection="user_groups")
@TypeAlias(value="UserGroup")
@JsonTypeName("UserGroup")
public class UserGroup extends BasicEntityWithDate {

    /** название */
    @Indexed(unique = true)
    private String name = "";
    /** описание */
    private String description = "";
    /** список прав */
    private List<String> permissionIds = new ArrayList<>();
    /** встроенная запись */
    private boolean builtin = false;

    public void update(UserGroup userGroup) {
        this.name = userGroup.getName();
        this.description = userGroup.getDescription();
        this.permissionIds = userGroup.getPermissionIds();

        this.updateDateTime = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<String> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public boolean isBuiltin() {
        return builtin;
    }

    public void setBuiltin(boolean builtin) {
        this.builtin = builtin;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }
}
