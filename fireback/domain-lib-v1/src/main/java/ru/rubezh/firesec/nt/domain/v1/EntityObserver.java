package ru.rubezh.firesec.nt.domain.v1;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Наблюдатель за сущностями системы.
 * 
 * Предназначен для сохранения параметров и состояния процесса наблюдения.
 * 
 * @author Антон Васильев
 *
 */
@Document(collection = "entity_observers")
public class EntityObserver extends ReferenceEntity {
    /** Идентификатор (или timstamp в формате ObjectId) последнего обработанного объекта */
    private ObjectId lastObservedObjectId;

    public ObjectId getLastObservedObjectId() {
        return lastObservedObjectId;
    }

    public void setLastObservedObjectId(ObjectId lastObservedObjectId) {
        this.lastObservedObjectId = lastObservedObjectId;
    }

}
