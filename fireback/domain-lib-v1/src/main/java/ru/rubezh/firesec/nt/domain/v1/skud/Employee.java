package ru.rubezh.firesec.nt.domain.v1.skud;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;

/**
 * Сотрудник (СКУД).
 * 
 * Тот, кому предоставляется или не предоставляется доступ, рассчитывается рабочее время. Должен обладать ключами
 * доступа.
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 */
@Document(collection = "employees")
@TypeAlias(value = "Employee")
@JsonTypeName("Employee")
@CompoundIndex(def = "{'projectId':1, 'index':1}", unique = true)
public class Employee extends BasicEntityWithDate {

    /** Числовой идентификатор (беззнаковое слово) */
    private int index;

    /** Идентификатор проекта */
    @Indexed
    private String projectId;

    /** Имя */
    private String firstName;

    /** Отчество */
    private String middleName;

    /** Фамилия */
    private String lastName;

    /** Фамилия и инициалы, не более 20 символов (вычисляемая строка) */
    private String name;

    /** Описание */
    private String description = "";

    /** Карта доступа */
    private AccessMap accessMap = new AccessMap();

    public void update(Employee srcEmployee) {
        setFirstName(srcEmployee.getFirstName());
        setMiddleName(srcEmployee.getMiddleName());
        setLastName(srcEmployee.getLastName());
        setName(srcEmployee.getName());
        setDescription(srcEmployee.getDescription());
        setAccessMap(srcEmployee.getAccessMap());
        setUpdateDateTime(new Date());
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccessMap getAccessMap() {
        return accessMap;
    }

    public void setAccessMap(AccessMap accessMap) {
        this.accessMap = accessMap;
    }

}
