package ru.rubezh.firesec.nt.domain.v1;

import java.util.HashMap;
import java.util.Map;

/**
 * Группа сбрасываемых состояний.
 *
 * Определяет группу состояний, сбрасываемых одним запросом пользователя.
 *
 * @author Александр Горячкин
 */
public enum ManualResetStateGroup {
    /** Пустая группа для установки по умолчанию для состояний без ручного сброса */
    NONE(Subsystem.UNKNOWN, ""),
    /** Пожар */
    FIRE(Subsystem.FIRE, "Пожар"),
    /** Неисправности */
    MALFUNCTIONS(Subsystem.GENERAL, "Неисправности"),
    /** Тесты */
    TESTS(Subsystem.GENERAL, "Тесты"),
    /** Сценарии */
    SCENARIOS(Subsystem.GENERAL, "Сценарии"),
    /** Тревога */
    ALARM(Subsystem.SECURITY, "Тревога"),
    /** Блокировка */
    LOCK(Subsystem.SECURITY, "Блокировка");

    // TODO: Убрать хардкод описания

    private Subsystem subsystem;
    private String description;

    public static Map<String, String> groupsBySubsystem(Subsystem subsystem) {
        Map<String, String> result = new HashMap<>();
        for (ManualResetStateGroup group : ManualResetStateGroup.values()) {
            if (group.subsystem == subsystem)
                result.put(group.name(), group.description);
        }
        return result;
    }

    private ManualResetStateGroup(Subsystem subsystem, String description) {
        this.subsystem = subsystem;
        this.description = description;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public String getDescription() {
        return description;
    }
}
