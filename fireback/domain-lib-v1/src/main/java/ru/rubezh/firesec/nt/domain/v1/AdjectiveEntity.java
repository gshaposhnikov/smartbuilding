package ru.rubezh.firesec.nt.domain.v1;

/**
 * Базовый класс для определения несамостоятельных сущностей, т.е. не хранимых в отдельной коллекции, а только в виде
 * встроенных объектов.
 * 
 * @author Антон Васильев
 *
 */
public class AdjectiveEntity {
    
    public AdjectiveEntity() {
    }

    public AdjectiveEntity(AdjectiveEntity source) {
    }
}
