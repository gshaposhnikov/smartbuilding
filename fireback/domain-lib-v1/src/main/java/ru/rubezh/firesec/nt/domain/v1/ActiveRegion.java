package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Зона, оперативная часть.
 * 
 * @author Александр Горячкин
 */
@Document(collection="active_regions")
@TypeAlias(value="ActiveRegion")
@JsonTypeName("ActiveRegion")
public class ActiveRegion extends BasicActiveEntityWithStates {

    /** Зона, проектная часть */
    @DBRef private Region regionProject;

    /** Проект (идентификатор) */
    private String projectId = "";

    /** Номер (в рамках проекта) */
    private int index;

    /** Статус "на охране" охранной зоны */
    private boolean onGuard = false;

    private FilterTags filterTags = new FilterTags();

    public ActiveRegion() {
    }
    
    public ActiveRegion(Region regionProject) {
        this.regionProject = regionProject;
        this.id = regionProject.getId();
        this.projectId = regionProject.getProjectId();
        this.index = regionProject.getIndex();
    }

    public Region getRegionProject() {
        return regionProject;
    }

    public void setRegionProject(Region regionProject) {
        this.regionProject = regionProject;
        this.id = regionProject.getId();
        this.projectId = regionProject.getProjectId();
        this.index = regionProject.getIndex();
    }

    public String getProjectId() {
        return projectId;
    }
    
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isOnGuard() {
        return onGuard;
    }

    public void setOnGuard(boolean onGuard) {
        this.onGuard = onGuard;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
