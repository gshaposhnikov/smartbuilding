package ru.rubezh.firesec.nt.domain.v1;

/**
 * Базовый класс для справочной сущности (которая должна храниться в БД, но при этом должна быть константной, т.е. неизменяемой в результате работы ПО).
 * 
 * @author Антон Васильев
 *
 */
public class ReferenceEntity extends BasicEntity {
    public ReferenceEntity() {
        
    }
    
    public ReferenceEntity(ReferenceEntity source) {
        super(source);
    }
    
    /** Сеттер идентификатора, необходимый для установки идентификаторов справочным сущностям, настраиваемым через файлы конфигураций */
    public void setId(String id) {
        this.id = id;
    }
}
