package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Оповещение о пожаротушении.
 * 
 * @author Андрей Лисовой
 * @ingroup BasicDomain
 */
@Document(collection = "fire_ext_notifications")
@TypeAlias("FireExtinctionNotification")
@JsonTypeName("FireExtinctionNotification")
public class FireExtinctionNotification extends BasicEntity {

    /** Идентификатор устройства */
    private String deviceId;
    /** Идентификатор зоны */
    private String regionId;
    /** Событие пожаротушения */
    private FireExtinctionEvent event;
    /** Начальное значение таймера обратного отсчёта, с */
    private Integer delay;
    
    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRegionId() {
        return regionId;
    }
    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public FireExtinctionEvent getEvent() {
        return event;
    }
    public void setEvent(FireExtinctionEvent event) {
        this.event = event;
    }

    public Integer getDelay() {
        return delay;
    }
    public void setDelay(Integer delay) {
        this.delay = delay;
    }

}
