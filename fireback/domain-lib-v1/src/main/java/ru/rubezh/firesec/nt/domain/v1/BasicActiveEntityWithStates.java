package ru.rubezh.firesec.nt.domain.v1;

import java.util.*;

/**
 * Базовый класс для наблюдаемых (активных) сущностей с состояниями.
 * 
 * @author Антон Васильев
 *
 */
public class BasicActiveEntityWithStates extends BasicActiveEntity {

    /** Идентификатор текущего класса состояний */
    private String generalStateCategoryId = "";

    /** Набор идентификаторов активных состояний */
    private Set<String> activeStates = new HashSet<>();

    /** Карта унаследованных от родителя активных состояний: {@code id родителя -> набор id состояний } */
    private Map<String, Set<String>> inheritedActiveStates = new HashMap<>();

    public String getGeneralStateCategoryId() {
        return generalStateCategoryId;
    }

    public void setGeneralStateCategoryId(String generalStateCategoryId) {
        this.generalStateCategoryId = generalStateCategoryId;
    }

    public Set<String> getActiveStates() {
        return activeStates;
    }

    public void setActiveStates(Set<String> activeStates) {
        this.activeStates = activeStates;
    }

    public Map<String, Set<String>> getInheritedActiveStates() {
        return inheritedActiveStates;
    }

    public void setInheritedActiveStates(Map<String, Set<String>> inheritedActiveStates) {
        this.inheritedActiveStates = inheritedActiveStates;
    }

    public void addStates(Collection<String> stateIds) {
        /* Добавляем состояния и взводим флаг, если что-то добавилось */
        if (getActiveStates().addAll(stateIds))
            setNeedsStateCommit(true);
    }

    public void removeStates(Collection<String> stateIds) {
        /* Удаляем автосостояния и взводим флаг, если что-то удалилось */
        if (getActiveStates().removeAll(stateIds))
            setNeedsStateCommit(true);
    }

}
