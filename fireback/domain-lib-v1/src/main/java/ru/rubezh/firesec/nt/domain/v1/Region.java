package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Зона.
 * 
 * Описывает некоторую территорию, часть объекта наблюдения (проектная часть).
 * 
 * @author Александр Горячкин
 * @ingroup BasicDomain
 */
@Document(collection="regions")
@TypeAlias(value="Region")
@JsonTypeName("Region")
public class Region extends BasicEntity {

    /** Название */
    private String name = "";
    /** Описание */
    private String description = "";

    /** Номер (в рамках проекта) */
    @Indexed
    private int index;
    /** Проект (идентификатор) */
    @Indexed
    private String projectId = "";
    /** Подсистема */
    private Subsystem subsystem = Subsystem.UNKNOWN;

        /* Атрибуты пожарной зоны */
    /** Кол-во извещателей для перехода в состояния Пожар-2 */
    private int fireEventCount;
    // TODO: Счетчик для перехода в пожар-2 в ночном режиме

        /* Атрибуты охранной зоны: */
    /** Вид зоны (обычная, с задержкой входа выхода, без права снятия) */
    private SecurityRegionType securityRegionType = SecurityRegionType.DEFAULT;
    /** Входная/выходная задержка, сек */
    private int inputOutputTimeout;
    /** Автоперевзятие, сек */
    private int autoRelock;
    /** Тихая тревога */
    private boolean silenceAlarm;
    // TODO: Время автоперевзятия
    // TODO: Время подтверждения срабатывания, сек
    // TODO: Время ожидания после подтверждения, сек


    public Region() {
        super();
    }

    public void update(Region region) {
        this.name = region.getName();
        this.description = region.getDescription();
        this.index = region.getIndex();
        this.fireEventCount = region.getFireEventCount();
        this.securityRegionType = region.getSecurityRegionType();
        this.inputOutputTimeout = region.getInputOutputTimeout();
        this.autoRelock = region.getAutoRelock();
        this.silenceAlarm = region.getSilenceAlarm();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public int getFireEventCount() {
        return fireEventCount;
    }

    public void setFireEventCount(int fireEventCount) {
        this.fireEventCount = fireEventCount;
    }

    public SecurityRegionType getSecurityRegionType() {
        return securityRegionType;
    }

    public void setSecurityRegionType(SecurityRegionType securityRegionType) {
        this.securityRegionType = securityRegionType;
    }

    public int getInputOutputTimeout() {
        return inputOutputTimeout;
    }

    public void setInputOutputTimeout(int inputOutputTimeout) {
        this.inputOutputTimeout = inputOutputTimeout;
    }

    public int getAutoRelock() {
        return autoRelock;
    }

    public void setAutoRelock(int autoRelock) {
        this.autoRelock = autoRelock;
    }

    public boolean getSilenceAlarm() {
        return silenceAlarm;
    }

    public void setSilenceAlarm(boolean silenceAlarm) {
        this.silenceAlarm = silenceAlarm;
    }

}
