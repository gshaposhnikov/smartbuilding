package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Группа панелей индикаторов.
 * 
 * Набор панелей индикаторов. Предоставляет доступ пользователям к панелям по указанным правам.
 * 
 * @author Александр Горячкин, Антон Караваев
 * @ingroup UIDomain
 */
@Document(collection = "indicator_panel_groups")
@TypeAlias(value = "IndicatorPanelGroup")
public class IndicatorPanelGroup extends BasicEntity {

    /** Название */
    private String name = "";
    /** Описание */
    private String description = "";

    /* TODO: права доступа */

    /** идентификатор проекта */
    private String projectId;

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public void update(IndicatorPanelGroup indicatorPanelGroup) {
        setName(indicatorPanelGroup.getName());
        setDescription(indicatorPanelGroup.getDescription());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
