package ru.rubezh.firesec.nt.domain.v1;

import java.util.Arrays;
import java.util.List;

public enum DeviceType {

    SOCKET("02", ""),
    LIGHT("21", "", "DTS", "LUX"),
    LOCK("515", ""),
    RADIATOR("CLIM", "MODE", "ERR", "HEATON", "MTEMP", "MTIME", "NONEDELAY",
            "NONETEMP", "NTEMP", "NTIME", "TPOINT"),
    AC("COND", "ONOFF","LOUVRE", "MODE", "FANSPEED", "SETPOINT"),
    ROOM_LIGHT("L0", ""),
    WATER_TAP("910", "", "R104_900_01_CONTROL", "R104_900_01_STATE"),
    ROLLS("515", "ROLL_FEEDBACK", "UP", "DOWN"),
    HUMIDITY_SENSOR("AVHUM", "AVHUM", "AVHUM"),
    THERMOMETER("AVTEMP", "AVTEMP", "AVTEMP"),
    LUXOMETER("AVLUX", "AVLUX", "AVLUX"),
    NONE("", "");

    DeviceType(String code, String onOffKeyword, String... parameterNames){
        this.code = code;
        this.onOffKeyword = onOffKeyword;
        this.parameterNames = Arrays.asList(parameterNames);
    }

    private String code;

    private String onOffKeyword;

    private List<String> parameterNames;

    public String getCode() {
        return code;
    }

    public String getOnOffKeyword() {
        return onOffKeyword;
    }

    public List<String> getParameterNames() {
        return parameterNames;
    }
}
