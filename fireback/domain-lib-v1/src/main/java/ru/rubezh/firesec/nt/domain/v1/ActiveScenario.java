package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Оперативная часть сущности сценария (копирует проектную через наследование).
 * 
 * @author Антон Васильев
 *
 */
@Document(collection="active_scenarios")
@TypeAlias(value="ActiveScenario")
@JsonTypeName("ActiveScenario")
public class ActiveScenario extends BasicActiveEntityWithStates {

    @DBRef private Scenario scenarioProject;

    /** Проект (идентификатор) */
    private String projectId = "";
    
    /** Глобальный номер */
    private int globalNo;

    public ActiveScenario() {
    }
    
    public ActiveScenario(Scenario scenario) {
        setScenarioProject(scenario);
    }

    public Scenario getScenarioProject() {
        return scenarioProject;
    }
    
    public void setScenarioProject(Scenario scenario) {
        this.scenarioProject = scenario;
        this.id = scenario.id;
        this.projectId = scenario.getProjectId();
        this.globalNo = scenario.getGlobalNo();
    }

    public String getProjectId() {
        return projectId;
    }
    
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public int getGlobalNo() {
        return globalNo;
    }

    public void setGlobalNo(int globalNo) {
        this.globalNo = globalNo;
    }

}
