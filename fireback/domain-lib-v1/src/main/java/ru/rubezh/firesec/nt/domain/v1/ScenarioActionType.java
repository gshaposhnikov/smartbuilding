package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Тип действия, выполняемого сценарием.
 * 
 * @author Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection="scenario_action_types")
@TypeAlias(value="ScenarioActionType")
@JsonTypeName("ScenarioActionType")
public class ScenarioActionType extends ReferenceEntity {

    /** Тип сущности действия, выполняемого сценарием. */
    public enum ActionEntityType {
        /** Устройство */
        DEVICE,
        /** Зона */
        REGION,
        /** Сценарий */
        SCENARIO,
        /** Виртуальное состояние */
        VIRTUAL_STATE
    }
    /** Тип сущности, к которой применимо действия данного типа */
    private ActionEntityType entityType = ActionEntityType.DEVICE;

    /* TODO: убрать это поле, оно не используется */
    /** Категория устройств, к которым применимо данное действие (если действие применимо к устройствам) */
    private DeviceCategory deviceCategory = DeviceCategory.EXECUTIVE;

    /** Подсистема, к которой применимо данное действие (если действие применимо к зонам) */
    private Subsystem subsystem = Subsystem.GENERAL;

    /** Тип назначения сценария */
    private ScenarioPurpose scenarioPurpose;

    /** Код действия, записываемый в БД прибора */
    private int code;

    /** Заглушка для параметра действия */
    private int parameterStub;

    /** Значимость действия (нужно для установки действия по умолчанию) */
    private int severity = 0;

    /** Номер типа действия в desktop версии*/
    private String desktopActionNumber;

    /* TODO: реализовать параметры, задаваемые для действий */
    
    public ScenarioActionType() {
    }
    
    public ScenarioActionType(ScenarioActionType source) {
        super(source);
        
        this.entityType = source.entityType;
        this.deviceCategory = source.deviceCategory;
        this.subsystem = source.subsystem;
        this.scenarioPurpose = source.scenarioPurpose;
        this.code = source.code;
        this.severity = source.severity;
    }
    
    public String getDesktopActionNumber() {
        return desktopActionNumber;
    }

    public void setDesktopActionNumber(String desktopActionNumber) {
        this.desktopActionNumber = desktopActionNumber;
    }

    public ActionEntityType getEntityType() {
        return entityType;
    }
    
    public void setEntityType(ActionEntityType entityType) {
        this.entityType = entityType;
    }
    
    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }
    
    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }
    
    public Subsystem getSubsystem() {
        return subsystem;
    }
    
    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getParameterStub() {
        return parameterStub;
    }

    public void setParameterStub(int parameterStub) {
        this.parameterStub = parameterStub;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

}
