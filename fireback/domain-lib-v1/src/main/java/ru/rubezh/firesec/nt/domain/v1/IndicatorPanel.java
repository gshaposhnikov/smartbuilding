package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Панель индикаторов.
 * 
 * Набор индикаторов, размещённых в таблице.
 * 
 * @author Александр Горячкин, Антон Караваев
 * @ingroup UIDomain
 */
@Document(collection = "indicator_panels")
@TypeAlias(value = "IndicatorPanel")
public class IndicatorPanel extends BasicEntity {

    /** Название */
    private String name = "";
    /** Описание */
    private String description = "";

    /** кол-во строк {@code (>=3 && <=100) } */
    private int rowCount;
    /** кол-во столбцов {@code (>=3 && <=20) } */
    private int colCount;
    /** группа панелей индикаторов */
    private String indicatorGroupId;
    /** идентификатор проекта*/
    private String projectId;

    /** набор индикаторов панели*/
    private List<List<Indicator>> indicators = new ArrayList<>();

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    public void update(IndicatorPanel indicatorPanel) {
        setName(indicatorPanel.getName());
        setDescription(indicatorPanel.getDescription());
    }

    public void addEmptyIndicators(){
        for (int i = 0; i < rowCount; i++) {
            indicators.add(new ArrayList<>());
            for (int j = 0; j < colCount; j++) {
                indicators.get(i).add(new Indicator(i, j));
            }
        }
    }

    public void updatePanelCells(int newRowCount, int newColCount) {
        if (newRowCount > rowCount && newColCount < colCount){
            reduceColumns(newColCount);
            addRows(newRowCount);
        } else {
            /* изменяем строки */
            if (newRowCount > rowCount){
                addRows(newRowCount);
            } else if (newRowCount < rowCount) {
                reduceRows(newRowCount);
            }

            /* изменяем столбцы */
            if (newColCount > colCount){
                addColumns(newColCount);
            } else if (newColCount < colCount){
                reduceColumns(newColCount);
            }
        }
    }

    private void addColumns(int newColCount){
        for (int i = 0; i < rowCount; i++) {
            for (int j = colCount; j < newColCount; j++) {
                indicators.get(i).add(new Indicator(i, j));
            }
        }
        colCount = newColCount;
    }

    private void addRows(int newRowCount){
        for (int i = rowCount; i < newRowCount; i++) {
            if (indicators.size() < newRowCount)
                indicators.add(new ArrayList<>());
            for (int j = 0; j < colCount; j++) {
                indicators.get(i).add(new Indicator(i, j));
            }
        }
        rowCount = newRowCount;
    }

    private void reduceColumns(int newColCount){
        for (int i = 0; i < rowCount; i++) {
            indicators.get(i).subList(newColCount, colCount).clear();
        }
        colCount = newColCount;
    }

    private void reduceRows(int newRowCount){
        indicators.subList(newRowCount, rowCount).clear();
        rowCount = newRowCount;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public int getColCount() {
        return colCount;
    }

    public void setColCount(int colCount) {
        this.colCount = colCount;
    }

    public String getIndicatorGroupId() {
        return indicatorGroupId;
    }

    public void setIndicatorGroupId(String indicatorGroupId) {
        this.indicatorGroupId = indicatorGroupId;
    }

    public void setIndicators(List<List<Indicator>> indicators){
        this.indicators = indicators;
    }

    public List<List<Indicator>> getIndicators(){
        return indicators;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
