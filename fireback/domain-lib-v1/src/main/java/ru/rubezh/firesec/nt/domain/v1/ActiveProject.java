package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Проект (оперативная часть).
 * 
 * Объединяет собой наблюдаемые и управляемые системой сущности в рамках одного объекта наблюдения.
 * 
 * @author Антон Васильев
 */
@Document(collection = "active_projects")
public class ActiveProject extends BasicActiveEntity {

    /** Оперативный статус проекта */
    private ProjectStatus status = ProjectStatus.INACTIVE;

    /** Сообщения валидации проекта */
    private List<ActivationValidateMessage> validateMessages = new ArrayList<>();

    public ActiveProject() {
    }

    public ActiveProject(Project project) {
        this.id = project.getId();
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public List<ActivationValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public void setValidateMessages(List<ActivationValidateMessage> validateMessages) {
        this.validateMessages = validateMessages;
    }
}
