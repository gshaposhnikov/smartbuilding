package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Проектные сущности прибора.
 */
public class ControlDeviceEntities {
    /** Устройства (включая прибор) */
    private List<Device> devices;
    /** Виртуальные состояния */
    private List<VirtualState> virtualStates;
    /** Сценарии */
    private List<Scenario> scenarios;
    /** Зоны */
    private List<Region> regions;
    /** Дата создания */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date created;

    public ControlDeviceEntities() {
    }

    public ControlDeviceEntities(
            List<Device> devices,
            List<VirtualState> virtualStates,
            List<Scenario> scenarios,
            List<Region> regions) {
        this.devices = devices;
        this.virtualStates = virtualStates;
        this.scenarios = scenarios;
        this.regions = regions;
        this.created = new Date();
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public List<VirtualState> getVirtualStates() {
        return virtualStates;
    }

    public void setVirtualStates(List<VirtualState> virtualStates) {
        this.virtualStates = virtualStates;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
