package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Пользователь - учётная запись.
 * 
 * Хранит контактную информацию, список групп и индивидуальных прав.
 * 
 * @author Александр Горячкин
 * @ingroup BasicSystem
 */
@Document(collection="users")
@TypeAlias(value="User")
@JsonTypeName("User")
public class User extends BasicEntity {

    /** имя (оно же логин) */
    @Indexed(unique = true)
    private String name = "";
    /** полное имя */
    private String fullName = "";
    /** пароль */
    @Transient
    private String password = null;
    /** hash-сумма пароля */
    @JsonIgnore
    private String passwordHash = null;
    /** номер телефона */
    private String phoneNo = "";
    /** электронная почта */
    @Indexed(unique = true)
    private String email = "";

    /** идентификатор индикатора */
    private String indicatorId = null;
    /** идентификатор интерфейса */
    private String workspaceId = null;

    /** активная или удалённая запись */
    private boolean active = true;
    /** встроенная запись */
    private boolean builtin = false;

    /** список групп пользователей */
    private List<String> userGroupIds = new ArrayList<>();
    /** список индивидуальных прав */
    private List<String> permissionIds = new ArrayList<>();
    /** список прав групп */
    @Transient
    private List<String> groupPermissions = new ArrayList<>();

    /** Дата и время последнего изменения состояния пользователя */
    @JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updateDateTime = new Date();

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags("GENERAL");
    /** Признак того, что фильтрация по меткам включеня */
    private boolean filterTagsOn;

    public void update(User user) {
        this.name = user.getName();
        this.fullName = user.getFullName();
        if (user.getPasswordHash() != null)
            this.passwordHash = user.getPasswordHash();
        this.phoneNo = user.getPhoneNo();
        this.email = user.getEmail();
        this.indicatorId = user.getIndicatorId();
        this.workspaceId = user.getWorkspaceId();
        this.builtin = user.isBuiltin();
        this.userGroupIds = user.getUserGroupIds();
        this.permissionIds = user.getPermissionIds();
        this.filterTagsOn = user.isFilterTagsOn();
        this.filterTags = user.getFilterTags();

        this.updateDateTime = new Date();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(String indicatorId) {
        this.indicatorId = indicatorId;
    }

    public String getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(String workspaceId) {
        this.workspaceId = workspaceId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBuiltin() {
        return builtin;
    }

    public void setBuiltin(boolean builtin) {
        this.builtin = builtin;
    }

    public List<String> getUserGroupIds() {
        return userGroupIds;
    }

    public void setUserGroupIds(List<String> userGroupIds) {
        this.userGroupIds = userGroupIds;
    }

    public List<String> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<String> permissionIds) {
        this.permissionIds = permissionIds;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public List<String> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(List<String> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

    public boolean isFilterTagsOn() {
        return filterTagsOn;
    }

    public void setFilterTagsOn(boolean filterTagsOn) {
        this.filterTagsOn = filterTagsOn;
    }
}
