package ru.rubezh.firesec.nt.domain.v1;

/**
 * Перечисление состояний, выставляемых бизнес-логикой.
 * 
 * @author Антон Васильев
 */
public enum LogicStates {
    SKUD_DB_IS_OUT_OF_DATE("skudDBIsOutOfDate"),
    DB_NOT_MATCHING_PROJECT("DBNotMatchingProject");

    private final String id;

    private LogicStates(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }
}
