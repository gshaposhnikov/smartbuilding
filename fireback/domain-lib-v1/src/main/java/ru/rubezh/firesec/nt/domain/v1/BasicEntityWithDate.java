package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Базовый класс для сущностей с идентификатором, датой создания и последнего изменения.
 *
 * @author Александр Горячкин
 */
public class BasicEntityWithDate extends BasicEntity {

    /** Дата и время создания */
    @JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Indexed
    protected Date createDateTime = new Date();

    /** Дата и время последнего изменения, в том числе отметки на удаление */
    @JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Indexed
    protected Date updateDateTime = null;

    /** Подготовлен для удаления */
    @JsonIgnore
    @Indexed
    protected boolean removed = false;

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
        this.updateDateTime = new Date();
    }
}
