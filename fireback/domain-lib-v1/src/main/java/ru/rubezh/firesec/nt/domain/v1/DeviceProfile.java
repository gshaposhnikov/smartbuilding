package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

/**
 * Профиль устройства (изделия).
 * 
 * Описывает характеристики конкретного типа устройства.
 * 
 * @author Александр Горячкин, Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection = "device_profiles")
@TypeAlias(value = "DeviceProfile")
public class DeviceProfile extends ReferenceEntity {

    /** Категория устройства */
    private DeviceCategory deviceCategory = DeviceCategory.OTHER;
    /** Список типов интерфейсов подключения */
    private List<ConnectionInterfaceType> connectionTypes = new ArrayList<>();
    /** Тип исходящего интерфейса подключения */
    private ConnectionInterfaceType outputConnectionType = ConnectionInterfaceType.NONE;
    /** Кол-во каналов (портов/линий) */
    private int lineCount = 0;
    /** Максимальное кол-во подключаемых на канале (порту/линии) устройств */
    private int lineDeviceCount = 0;
    /** Диапазоны адресов для подключения устройств по RS-485 (одинаковые для всех линий) */
    private List<AddressRange> lineAddressRanges = new ArrayList<>();
    /** Список совместимых для подключения устройств (идентификаторов изделий) */
    private List<String> outputDeviceProfileIds = new ArrayList<>();
    /** Тип подсистемы */
    private Subsystem subsystem = Subsystem.UNKNOWN;
    /** Тип устройства в протоколе RSR3 для физических устройств */
    private int rsr3RawDeviceType;
    /** Тип устройства в протоколе RSR3 для виртуальных устройств */
    private int rsr3VirtualDeviceType;
    /** Тип устройства для настройки МС */
    private int amodDeviceType;
    /** Тип устройства в БД прибора (индекс хидера) */
    private int dbDeviceType = -1;
    /** Название продукта, вшитое в устройство (для верификации при подключении) */
    private String productName;
    /** Идентификатор устройства старой версии firesec NG*/
    private String desktopFSId;
    /** Можно ли привязать к зоне */
    private boolean attachableToRegion = false;
    /** Можно ли отключать обход */
    private boolean ignorable = true;
    /** Отключаемое устройство */
    private boolean canBeDisabled = false;
    /** можно менять тип профиля для встроенных устройство*/
    private boolean changeDeviceProfile = false;
    /** Подстановка номера линии (для встроенных устройств) */
    private Integer lineNumberSubstitution;
    /** Подстановка адреса на линии (для встроенных устройств без адресации) */
    private Integer lineAddressSubstitution;

    /*
     * TODO: заменить все DBRef на мануальные ссылки, т.к. они не могут
     * использоваться в pipeline в mongodb v3.4, и в документации mongo их в
     * принципе советуют не использовать
     */

    /** Состояние потери связи с устройством */
    @DBRef
    private State deviceLostState;
    /** Состояние готовности устройства к работе */
    @DBRef
    private State deviceReadyState;
    /** Состояние неготовности устройства к работе */
    @DBRef
    private State deviceNotReadyState;
    /** Состояние "В обходе" */
    private String deviceIgnoreStateId;
    /** Идентификатор состояния после незавершённой прошивки */
    private String firmwareUpdateFailureStateId;
    /** Идентификатор состояния "после неудачной записи базы прибора" (используется драйвером) */
    private String badSignatureStateId;

    //TODO: добавить состояния по умолчанию
    /** Поддерживаемые настраиваемые состояния, определяемые одним битом */
    @DBRef private List<DecodableState> customBitStates = new ArrayList<>();
    /** Поддерживаемые настраиваемые состояния, определяемые маскируемым значением байта */
    @DBRef private List<DecodableState> customMaskedStates = new ArrayList<>();
    /** Прочие поддерживаемые состояния (идентификаторы) */
    private List<String> otherCustomStateIds = new ArrayList<>();
    /** Карта наследования состояний дочерними устройствами: {@code id состояния родителя -> id состояния потомков } */
    private Map<String, String> stateInheritance = new HashMap<>();
    /** Поддерживаемые настраиваемые наблюдаемые параметры */
    private List<MonitorableValue> customMonitorableValues = new ArrayList<>();
    /** Поддерживаемые триггеры для срабатывания логики сценариев (идентифкаторы типов) */
    private List<String> supportedScenarioTriggerIds = new ArrayList<>();
    /** Поддерживаемые действия сценариев над данным типом устройств (идентификаторы типов) */
    private List<String> supportedScenarioActionIds = new ArrayList<>();
    /** Размер параметров действий в сценариях */
    private int scenarioActionParameterSize = 0;

    /**
     * Устройство может использоваться в условиях исполнительных блоков
     * сценариев или в блоках слежения сценариев
     */
    private boolean acceptableAsScenarioCondition = false;

    /** Устройство пожаротушения (управление таким устройством - управление пожаротушением) */
    private boolean firefightingDevice = false;

    /** Устройство, генерирующее события пожаротушения */
    private boolean fireExtinctionDevice = false;
    /** Карта имён параметров "тип выхода" : "задержка тушения" */
    private Map<String, String> fireExtinctionDelayPropertiesMap;
    /** Референсное значение типа выхода "тушение" */
    private String fireExtinctionOutputTypeValue;

    /** Список пользователей устройства */
    private List<String> deviceUsers;
    /** Регулярное выражение, определяющее допустимость пароля пользователя */
    private String userPasswordPattern;

    /* Параметры контейнеров (в том числе приборов) */

    /** Описатель встроенных устройств. */
    public static class InternalDeviceItem extends AdjectiveEntity {
        /** Идентификатор профилей возможных типов устройств */
        private List<String> availableDeviceProfileIds = new ArrayList<>();

        /** Тип адреса устройств */
        private AddressType addressType = AddressType.GENERIC;

        /** кол-во встроенных устройств */
        private int nDevices;

        /** кол-во обязательных (не отключаемых) встроенных устройств */
        private int nMandatoryDevices;

        /** наибольшее количество подключаемых встроенных устройств */
        private int maxAttachableDevices;

        public List<String> getAvailableDeviceProfileIds() {
            return availableDeviceProfileIds;
        }

        public void setAvailableDeviceProfileIds(List<String> availableDeviceProfileIds) {
            this.availableDeviceProfileIds = availableDeviceProfileIds;
        }

        public AddressType getAddressType() {
            return addressType;
        }

        public void setAddressType(AddressType addressType) {
            this.addressType = addressType;
        }

        public int getnDevices() {
            return nDevices;
        }
        public void setnDevices(int nDevices) {
            this.nDevices = nDevices;
        }

        public int getnMandatoryDevices() {
            return nMandatoryDevices;
        }

        public void setnMandatoryDevices(int nMandatoryDevices) {
            this.nMandatoryDevices = nMandatoryDevices;
        }

        public int getMaxAttachableDevices() {
            return maxAttachableDevices;
        }

        public void setMaxAttachableDevices(int maxAttachableDevices) {
            this.maxAttachableDevices = maxAttachableDevices;
        }

    }

    private List<InternalDeviceItem> internalDevices = new ArrayList<>();

    /** Параметры устройства в виде пар "идентификатор параметра: его описание" */
    private Map<String, DeviceProperty> deviceProperties = new HashMap<>();
    /** Параметры конфигурации в виде пар "идентификатор параметра: его описание" */
    private Map<String, DeviceConfigProperty> configProperties = new HashMap<>();
    /** Количество байт в конфиге устройства */
    private int configSize;
    /** Список опций (для исполнительных устройств и сенсоров) для записи информации об устройстве в БД прибора */
    private EnumSet<ControlDeviceDatabaseOptionType> controlDeviceDatabaseOptionTypes = EnumSet.noneOf(ControlDeviceDatabaseOptionType.class);
    /** Смещение в блоке произвольных данных БД прибора (при наличии опции SENSOR_ARBITRARY_DATA_HAS_REGION) */
    private Integer arbitraryDataRegionOffset;

    /** Описатель журнала. */
    public static class LogTypeItem extends AdjectiveEntity {
        /** Идентификатор */
        private String id = "";
        /** Название */
        private String name = "";
        /** Размер */
        private int size = 1024;
        /** Код журнала */
        private int code = 0x00;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    /** Список доступных журналов */
    private List<LogTypeItem> logTypes = new ArrayList<>();

    /** Поддерживаемые действия устройства в виде пар "идентификатор действия: его описание" */
    private Map<String, DeviceAction> supportedActions;

    /** Устройство является точкой доступа СКУД */
    private boolean accessPoint = false;
    /** Устройство умеет считывать код с ключа доступа */
    private boolean accessKeyReader = false;

    /** Группа устройства */
    private DeviceGroup deviceGroup = DeviceGroup.OTHER;

    /** Список дополнительных подсистем */
    private List<Subsystem> additionalSubsystems = new ArrayList<>();

    private DeviceType deviceType;

    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public List<ConnectionInterfaceType> getConnectionTypes() {
        return connectionTypes;
    }

    public void setConnectionTypes(List<ConnectionInterfaceType> connectionTypes) {
        this.connectionTypes = connectionTypes;
    }

    public ConnectionInterfaceType getOutputConnectionType() {
        return outputConnectionType;
    }

    public void setOutputConnectionType(ConnectionInterfaceType outputConnectionType) {
        this.outputConnectionType = outputConnectionType;
    }

    public int getLineCount() {
        return lineCount;
    }

    public void setLineCount(int lineCount) {
        this.lineCount = lineCount;
    }

    public int getLineDeviceCount() {
        return lineDeviceCount;
    }

    public void setLineDeviceCount(int lineDeviceCount) {
        this.lineDeviceCount = lineDeviceCount;
    }

    public List<AddressRange> getLineAddressRanges() {
        return lineAddressRanges;
    }
    
    public void setLineAddressRanges(List<AddressRange> lineAddressRanges) {
        this.lineAddressRanges = lineAddressRanges;
    }
    
    public List<String> getOutputDeviceProfileIds() {
        return outputDeviceProfileIds;
    }

    public void setOutputDeviceProfileIds(List<String> outputDeviceProfileIds) {
        this.outputDeviceProfileIds = outputDeviceProfileIds;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public int getRsr3RawDeviceType() {
        return rsr3RawDeviceType;
    }

    public void setRsr3RawDeviceType(int rsr3RawDeviceType) {
        this.rsr3RawDeviceType = rsr3RawDeviceType;
    }

    public int getRsr3VirtualDeviceType() {
        return rsr3VirtualDeviceType;
    }

    public void setRsr3VirtualDeviceType(int rsr3VirtualDeviceType) {
        this.rsr3VirtualDeviceType = rsr3VirtualDeviceType;
    }

    public int getAmodDeviceType() {
        return amodDeviceType;
    }

    public void setAmodDeviceType(int amodDeviceType) {
        this.amodDeviceType = amodDeviceType;
    }

    public int getDbDeviceType() {
        return dbDeviceType;
    }

    public void setDbDeviceType(int dbDeviceType) {
        this.dbDeviceType = dbDeviceType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean isAttachableToRegion() {
        return attachableToRegion;
    }

    public void setAttachableToRegion(boolean attachableToRegion) {
        this.attachableToRegion = attachableToRegion;
    }

    public boolean isIgnorable() {
        return ignorable;
    }

    public void setIgnorable(boolean ignorable) {
        this.ignorable = ignorable;
    }

    public State getDeviceLostState() {
        return deviceLostState;
    }

    public void setDeviceLostState(State deviceLostState) {
        this.deviceLostState = deviceLostState;
    }

    public State getDeviceReadyState() {
        return deviceReadyState;
    }

    public void setDeviceReadyState(State deviceReadyState) {
        this.deviceReadyState = deviceReadyState;
    }

    public State getDeviceNotReadyState() {
        return deviceNotReadyState;
    }

    public void setDeviceNotReadyState(State deviceNotReadyState) {
        this.deviceNotReadyState = deviceNotReadyState;
    }

    public String getDeviceIgnoreStateId() {
        return deviceIgnoreStateId;
    }

    public void setDeviceIgnoreStateId(String deviceIgnoreStateId) {
        this.deviceIgnoreStateId = deviceIgnoreStateId;
    }

    public String getFirmwareUpdateFailureStateId() {
        return firmwareUpdateFailureStateId;
    }

    public void setFirmwareUpdateFailureStateId(String firmwareUpdateFailureStateId) {
        this.firmwareUpdateFailureStateId = firmwareUpdateFailureStateId;
    }

    public String getBadSignatureStateId() {
        return badSignatureStateId;
    }

    public void setBadSignatureStateId(String badSignatureStateId) {
        this.badSignatureStateId = badSignatureStateId;
    }

    public List<DecodableState> getCustomBitStates() {
        return customBitStates;
    }

    public void setCustomBitStates(List<DecodableState> customBitStates) {
        this.customBitStates = customBitStates;
    }

    public List<DecodableState> getCustomMaskedStates() {
        return customMaskedStates;
    }

    public void setCustomMaskedStates(List<DecodableState> customMaskedStates) {
        this.customMaskedStates = customMaskedStates;
    }

    public List<String> getOtherCustomStateIds() {
        return otherCustomStateIds;
    }

    public void setOtherCustomStateIds(List<String> otherCustomStateIds) {
        this.otherCustomStateIds = otherCustomStateIds;
    }

    public Map<String, String> getStateInheritance() {
        return stateInheritance;
    }

    public void setStateInheritance(Map<String, String> stateInheritance) {
        this.stateInheritance = stateInheritance;
    }

    public List<MonitorableValue> getCustomMonitorableValues() {
        return customMonitorableValues;
    }

    public void setCustomMonitorableValues(List<MonitorableValue> customMonitorableValues) {
        this.customMonitorableValues = customMonitorableValues;
    }

    public List<String> getSupportedScenarioTriggerIds() {
        return supportedScenarioTriggerIds;
    }

    public void setSupportedScenarioTriggerIds(List<String> supportedScenarioTriggerIds) {
        this.supportedScenarioTriggerIds = supportedScenarioTriggerIds;
    }

    public List<String> getSupportedScenarioActionIds() {
        return supportedScenarioActionIds;
    }

    public void setSupportedScenarioActionIds(List<String> supportedScenarioActionIds) {
        this.supportedScenarioActionIds = supportedScenarioActionIds;
    }

    public int getScenarioActionParameterSize() {
        return scenarioActionParameterSize;
    }

    public void setScenarioActionParameterSize(int scenarioActionParameterSize) {
        this.scenarioActionParameterSize = scenarioActionParameterSize;
    }

    public boolean isAcceptableAsScenarioCondition() {
        return acceptableAsScenarioCondition;
    }

    public void setAcceptableAsScenarioCondition(boolean acceptableAsScenarioCondition) {
        this.acceptableAsScenarioCondition = acceptableAsScenarioCondition;
    }

    public boolean isFirefightingDevice() {
        return firefightingDevice;
    }

    public void setFirefightingDevice(boolean firefightingDevice) {
        this.firefightingDevice = firefightingDevice;
    }

    public boolean isFireExtinctionDevice() {
        return fireExtinctionDevice;
    }

    public void setFireExtinctionDevice(boolean fireExtinctionDevice) {
        this.fireExtinctionDevice = fireExtinctionDevice;
    }

    public Map<String, String> getFireExtinctionDelayPropertiesMap() {
        return fireExtinctionDelayPropertiesMap;
    }

    public void setFireExtinctionDelayPropertiesMap(Map<String, String> fireExtinctionDelayPropertiesMap) {
        this.fireExtinctionDelayPropertiesMap = fireExtinctionDelayPropertiesMap;
    }

    public String getFireExtinctionOutputTypeValue() {
        return fireExtinctionOutputTypeValue;
    }

    public void setFireExtinctionOutputTypeValue(String fireExtinctionOutputTypeValue) {
        this.fireExtinctionOutputTypeValue = fireExtinctionOutputTypeValue;
    }

    public List<String> getDeviceUsers() {
        return deviceUsers;
    }

    public void setDeviceUsers(List<String> deviceUsers) {
        this.deviceUsers = deviceUsers;
    }

    public String getUserPasswordPattern() {
        return userPasswordPattern;
    }

    public void setUserPasswordPattern(String userPasswordPattern) {
        this.userPasswordPattern = userPasswordPattern;
    }

    public List<InternalDeviceItem> getInternalDevices() {
        return internalDevices;
    }

    public void setInternalDevices(List<InternalDeviceItem> internalDevices) {
        this.internalDevices = internalDevices;
    }

    public Map<String, DeviceProperty> getDeviceProperties() {
        return deviceProperties;
    }

    public void setDeviceProperties(Map<String, DeviceProperty> deviceProperties) {
        this.deviceProperties = deviceProperties;
    }

    public Map<String, DeviceAction> getSupportedActions() {
        return supportedActions;
    }

    public void setSupportedActions(Map<String, DeviceAction> supportedActions_) {
        supportedActions = supportedActions_;
    }

    public Map<String, DeviceConfigProperty> getConfigProperties() {
        return configProperties;
    }

    public void setConfigProperties(Map<String, DeviceConfigProperty> configProperties) {
        this.configProperties = configProperties;
    }

    public int getConfigSize() {
        return configSize;
    }

    public void setConfigSize(int configSize) {
        this.configSize = configSize;
    }

    public Set<ControlDeviceDatabaseOptionType> getControlDeviceDatabaseOptionTypes() {
        return controlDeviceDatabaseOptionTypes;
    }

    public Integer getArbitraryDataRegionOffset() {
        return arbitraryDataRegionOffset;
    }

    public void setArbitraryDataRegionOffset(Integer arbitraryDataRegionOffset_) {
        arbitraryDataRegionOffset = arbitraryDataRegionOffset_;
    }

    public boolean getCanBeDisabled() {
        return canBeDisabled;
    }

    public void setCanBeDisabled(boolean canBeDisabled) {
        this.canBeDisabled = canBeDisabled;
    }

    public Integer getLineNumberSubstitution() {
        return lineNumberSubstitution;
    }

    public void setLineNumberSubstitution(Integer lineNumberSubstitution) {
        this.lineNumberSubstitution = lineNumberSubstitution;
    }

    public Integer getLineAddressSubstitution() {
        return lineAddressSubstitution;
    }

    public void setLineAddressSubstitution(Integer lineAddressSubstitution) {
        this.lineAddressSubstitution = lineAddressSubstitution;
    }

    public void setControlDeviceDatabaseOptionTypes(
            Set<ControlDeviceDatabaseOptionType> controlDeviceDatabaseOptionTypes) {
        this.controlDeviceDatabaseOptionTypes.clear();
        this.controlDeviceDatabaseOptionTypes.addAll(controlDeviceDatabaseOptionTypes);
    }

    public List<String> getValidDeviceStateIds() {
        List<String> validStates = new ArrayList<>();
        if (deviceLostState != null && deviceLostState.getId() != null)
            validStates.add(deviceLostState.getId());
        if (deviceReadyState != null && deviceReadyState.getId() != null)
            validStates.add(deviceReadyState.getId());
        if (deviceNotReadyState != null && deviceNotReadyState.getId() != null)
            validStates.add(deviceNotReadyState.getId());
        if (deviceIgnoreStateId != null)
            validStates.add(deviceIgnoreStateId);
        if (firmwareUpdateFailureStateId != null)
            validStates.add(firmwareUpdateFailureStateId);
        for (State state : customBitStates)
            validStates.add(state.getId());
        for (State state : customMaskedStates)
            validStates.add(state.getId());
        validStates.addAll(otherCustomStateIds);
        return validStates;
    }

    public List<LogTypeItem> getLogTypes() {
        return logTypes;
    }

    public void setLogTypes(List<LogTypeItem> logTypes) {
        this.logTypes = logTypes;
    }

    public boolean isAccessPoint() {
        return accessPoint;
    }

    public void setAccessPoint(boolean accessPoint) {
        this.accessPoint = accessPoint;
    }

    public boolean isAccessKeyReader() {
        return accessKeyReader;
    }

    public void setAccessKeyReader(boolean accessKeyReader) {
        this.accessKeyReader = accessKeyReader;
    }

    public String getDesktopFSId() {
        return desktopFSId;
    }

    public void setDesktopFSId(String desktopFSId) {
        this.desktopFSId = desktopFSId;
    }
    

    public boolean isChangeDeviceProfile() {
        return changeDeviceProfile;
    }

    public void setChangeDeviceProfile(boolean changeDeviceProfile) {
        this.changeDeviceProfile = changeDeviceProfile;
    }

    public DeviceGroup getDeviceGroup() {
        return deviceGroup;
    }

    public void setDeviceGroup(DeviceGroup deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    public List<Subsystem> getAdditionalSubsystems() {
        return additionalSubsystems;
    }

    public void setAdditionalSubsystems(List<Subsystem> additionalSubsystems) {
        this.additionalSubsystems = additionalSubsystems;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }
}
