package ru.rubezh.firesec.nt.domain.v1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Подсистема, оперативная часть.
 * 
 * @author Александр Горячкин
 */
@Document(collection="active_subsystems")
@TypeAlias(value="ActiveSubsystem")
@JsonTypeName("ActiveSubsystem")
public class ActiveSubsystem extends BasicActiveEntityWithStates {

    /**
     * Счетчик состояний.
     * 
     * @author Александр Горячкин
     *
     */
    public static class Counter extends AdjectiveEntity {
        /** Значение счётчика */
        private int value = 0;

        /** Список идентификаторов активных сущностей */
        private Set<String> activeEntitiesIds = new HashSet<>();

        public int getValue() {
            return value;
        }

        public Set<String> getActiveEntitiesIds() {
            return activeEntitiesIds;
        }

        public void addActiveEntityId(String activeEntityId) {
            if (activeEntitiesIds.add(activeEntityId))
                value++;
        }

        @Override
        public boolean equals(Object obj) {

            if (this == obj)
                return true;

            if (!(obj instanceof Counter)) {
                return false;
            }

            Counter anotherCounter = (Counter) obj;
            return value == anotherCounter.getValue()
                    && activeEntitiesIds.equals(anotherCounter.getActiveEntitiesIds());
        }

        @Override
        public int hashCode(){
            int result = 17;
            result = 31 * result + value;
            result = 31 * result + activeEntitiesIds.hashCode();
            return result;
        }
    }

    /** Подсистема */
    private Subsystem subsystem = Subsystem.UNKNOWN;

    private FilterTags filterTags = new FilterTags();

    /**
     * Счётчики по каждой категории состояний {@code <идентификатор категории состояний, счётчик>}
     */
    private Map<String, Counter> counters = new HashMap<>();

    public ActiveSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public Map<String, Counter> getCounters() {
        return counters;
    }

    public void setCounters(Map<String, Counter> counters) {
        this.counters = counters;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

}
