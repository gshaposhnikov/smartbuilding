package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.TypeAlias;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Базовый класс для временных блоков сценариев.
 * 
 * @author Антон Васильев
 *
 */
@TypeAlias(value="ScenarioTimeLineBlock")
@JsonTypeName("ScenarioTimeLineBlock")
public class ScenarioTimeLineBlock extends AdjectiveEntity {
    /* Общие поля */

    /** Тип блока сценария. */
    public enum BlockType {
        /** Исполнительный блок */
        EXECUTIVE,
        /** Блок слежения */
        TRACING,
        /** Блок действия на компьютере */
        COMPUTER_ACTION
    }
    /** Тип блока времени */
    private BlockType blockType;
    /** Название блока */
    private String name = "Блок сценария";
    /**
     * Задержка по времени, в секундах.
     * Не используется в блоке слежения.
     * В исполнительных блоках при havingTracingBlock здесь должна быть копия времени начала слежения.
     **/
    private int timeDelaySec;

        /* Поля блока слежения */

    /** Тип отслеживаемой сущности. */
    public enum TracingEntityType {
        /** Устройство */
        SENSOR_DEVICE,
        /** Виртуальное состояние */
        VIRTUAL_STATE
    }
    /** Тип отслеживаемой сущности */
    private TracingEntityType tracingEntityType = TracingEntityType.SENSOR_DEVICE;
    /** Идентификатор отслеживаемой сущности (устройства-датчика или виртуального состояния) */
    private String tracingEntityId = "";
    /** BSON идентификатор отслеживаемой сущности*/
    @JsonIgnore
    private ObjectId tracingEntityOId;
    /** Время начала слежения, в секундах */
    private int tracingStartSec;
    /** Период слежения, в секундах */
    private int tracingPeriodSec;

        /* Поля исполнительного блока */

    /** Тип условия исполнительного блока сценария. */
    public enum ConditionType {
        /** Условие отсутствует */
        NONE,
        /** Условие: по наличию/отсутствию состояния у устройства */
        DEVICE_STATE,
        /** Условие: по наличию/отсутствию виртуального состояния */
        VIRTUAL_STATE
    }

    /**
     * Тип условия.
     * 
     * При havingTracingBlock должно выставляться автоматически в зависимости от типа наблюдаемой сущности.
     **/
    private ConditionType conditionType = ConditionType.NONE;

    /** Тип проверки условия исполнительного блока сценария. */
    public enum ConditionCheckType {
        /** Проверка на активность */
        IS_ACTIVE,
        /** Проверка на неактивность */
        IS_INACTIVE
    }

    /** Тип проверки условия */
    private ConditionCheckType conditionCheckType = ConditionCheckType.IS_ACTIVE;

    /**
     * Идентификатор сущности устройства или виртуального состояния для проверки условия.
     * 
     * При havingTracingBlock здесь должна быть копия соответствующего идентификатора из блока слежения.
     * 
     */
    private String conditionEntityId = "";

    /**
     * BSON идентификатор сущности устройства или виртуального состояния для проверки условия
     * */
    @JsonIgnore
    private ObjectId conditionEntityOId;
    /**
     * Идентфикатор состояния, возникающего на устройстве, для проверки условия.
     * 
     * Актуально при сonditionType == DEVICE_STATE.
     * 
     */
    private String conditionDeviceStateId = "";

    /** Действие исполнительного блока сценария. */
    public static class Action {
        /** Идентификатор типа действия */
        private String actionTypeId = "";
        /** Идентификатор сущности (устройства, зоны, сценария или виртуального состояния -
         * - в зависимости от типа действия)
         */
        private String entityId = "";

        /** BSON идентификатор сущности */
        @JsonIgnore
        private ObjectId entityOId;

        /* TODO: добавить параметры действий */

        public Action() {
        }

        public Action(ScenarioTimeLineBlock.Action source) {
            this.actionTypeId = source.actionTypeId;
            this.entityId = source.entityId;
            this.entityOId = new ObjectId(entityId);
        }

        public String getActionTypeId() {
            return actionTypeId;
        }

        public void setActionTypeId(String actionTypeId) {
            this.actionTypeId = actionTypeId;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
            setEntityOId(entityId);
        }

        public ObjectId getEntityOId() {
            return entityOId;
        }

        private void setEntityOId(String entityId) {
            if (ObjectId.isValid(entityId))
                this.entityOId = new ObjectId(entityId);
        }

        public void setEntityOId(ObjectId entityOId){
            this.entityOId = entityOId;
        }
    }

    /** Список действий. */
    private List<ScenarioTimeLineBlock.Action> actions = new ArrayList<>();

    /* Поля для блока "Действие на компьютере" */

    /** Действие на компьютере исполнительного блока сценария. */
    public static class ComputerAction{
        /** Заголовок */
        private String title = "";

        /** Сообщение */
        private String message = "";

        private Command command = Command.SHOW_MESSAGE;

        /** Время до запуска, в секундах */
        private int countdownTimeSec;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCountdownTimeSec() {
            return countdownTimeSec;
        }

        public void setCountdownTimeSec(int countdownTimeSec) {
            this.countdownTimeSec = countdownTimeSec;
        }

        public Command getCommand() {
            return command;
        }

        public void setCommand(Command command) {
            this.command = command;
        }
    }

    /** Действия на компьютере. */
    private List<ComputerAction> computerActions = new ArrayList<>();

    /** Команда для выполнения на компьютере. */
    public enum Command {
        SHOW_MESSAGE("Показать сообщение"),

        COUNTDOWN("Обратный отсчет");

        private String label;

        Command(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    /* TODO: добавить вложенные блоки действий на компьютере */

    public ScenarioTimeLineBlock() {

    }

    public ScenarioTimeLineBlock(ScenarioTimeLineBlock source) {
        super(source);

        this.blockType = source.blockType;
        this.name = source.name;
        this.timeDelaySec = source.timeDelaySec;

        this.tracingEntityType = source.tracingEntityType;
        this.tracingEntityId = source.tracingEntityId;
        this.tracingPeriodSec = source.tracingPeriodSec;

        this.conditionType = source.conditionType;
        this.conditionEntityId = source.conditionEntityId;
        this.conditionCheckType = source.conditionCheckType;
        this.actions = source.actions;

        computerActions = source.computerActions;
    }

    public BlockType getBlockType() {
        return blockType;
    }

    public void setBlockType(BlockType blockType) {
        this.blockType = blockType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimeDelaySec() {
        return timeDelaySec;
    }

    public void setTimeDelaySec(int timeDelaySec) {
        this.timeDelaySec = timeDelaySec;
    }

    public TracingEntityType getTracingEntityType() {
        return tracingEntityType;
    }

    public void setTracingEntityType(TracingEntityType tracingEntityType) {
        this.tracingEntityType = tracingEntityType;
    }

    public String getTracingEntityId() {
        return tracingEntityId;
    }

    public void setTracingEntityId(String tracingEntityId) {
        this.tracingEntityId = tracingEntityId;
        setTracingEntityOId(tracingEntityId);
    }

    public int getTracingStartSec() {
        return tracingStartSec;
    }

    public void setTracingStartSec(int tracingStartSec) {
        this.tracingStartSec = tracingStartSec;
    }

    public int getTracingPeriodSec() {
        return tracingPeriodSec;
    }

    public void setTracingPeriodSec(int tracingPeriodSec) {
        this.tracingPeriodSec = tracingPeriodSec;
    }

    public ConditionType getConditionType() {
        return conditionType;
    }

    public void setConditionType(ConditionType conditionType) {
        this.conditionType = conditionType;
    }

    public ConditionCheckType getConditionCheckType() {
        return conditionCheckType;
    }

    public void setConditionCheckType(ConditionCheckType conditionCheckType) {
        this.conditionCheckType = conditionCheckType;
    }

    public String getConditionEntityId() {
        return conditionEntityId;
    }

    public void setConditionEntityId(String conditionEntityId) {
        this.conditionEntityId = conditionEntityId;
        setConditionEntityOId(conditionEntityId);
    }

    public String getConditionDeviceStateId() {
        return conditionDeviceStateId;
    }

    public void setConditionDeviceStateId(String conditionDeviceStateId) {
        this.conditionDeviceStateId = conditionDeviceStateId;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public void setComputerActions(List<ComputerAction> computerActions){
        this.computerActions = computerActions;
    }

    public List<ComputerAction> getComputerActions(){
        return computerActions;
    }

    public ObjectId getTracingEntityOId() {
        return tracingEntityOId;
    }

    private void setTracingEntityOId(String tracingEntityId) {
        if (ObjectId.isValid(tracingEntityId))
            this.tracingEntityOId = new ObjectId(tracingEntityId);
    }

    public void setTracingEntityOId(ObjectId tracingEntityOId) {
        this.tracingEntityOId = tracingEntityOId;
    }

    public ObjectId getConditionEntityOId() {
        return conditionEntityOId;
    }

    private void setConditionEntityOId(String conditionEntityId) {
        if (ObjectId.isValid(conditionEntityId))
            this.conditionEntityOId = new ObjectId(conditionEntityId);
    }

    private void setConditionEntityOId(ObjectId conditionEntityOId) {
        this.conditionEntityOId = conditionEntityOId;
    }
}
