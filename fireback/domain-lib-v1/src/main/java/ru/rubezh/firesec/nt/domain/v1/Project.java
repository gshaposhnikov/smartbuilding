package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Проект.
 * 
 * Объединяет собой наблюдаемые и управляемые системой сущности в рамках одного объекта наблюдения.
 * 
 * @author Александр Горячкин
 * @ingroup BasicSystem
 */
 @Document(collection = "projects")
public class Project extends BasicEntity {

    /** Название */
    private String name;
    /** Описание */
    private String description;
    /** Версия */ 
    private String version;
    /** Автор */
    private String userId;
    /** Статус */
    private ProjectStatus status = ProjectStatus.BUILD;
    /** Признак - был ли проект импортирован */
    private boolean isImported = false;
    /** Сообщения об ошибках импорта проекта */
    private List<ImportValidateMessage> importValidateMessages = new ArrayList<>();
    /** Дата создания */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date createDateTime = new Date();

    /** Дата последнего изменения */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date updateDateTime = createDateTime;

    public void update(Project project) {
       this.name = project.getName();
       this.description = project.getDescription();
       this.version = project.getVersion();
       this.updateDateTime = new Date();
   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setImported(boolean isImported) {
        this.isImported = isImported;
    }

    public List<ImportValidateMessage> getImportValidateMessages() {
        return importValidateMessages;
    }

    public void setImportValidateMessages(List<ImportValidateMessage> importValidateMessages) {
        this.importValidateMessages = importValidateMessages;
    }

}
