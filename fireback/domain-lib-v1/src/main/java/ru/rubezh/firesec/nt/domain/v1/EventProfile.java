package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Профиль события.
 * 
 * Используется для интерпретации считываемых из журналов приборов событий.
 * 
 * @author Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection = "event_profiles")
@TypeAlias(value = "EventProfile")
@JsonTypeName("EventProfile")
public class EventProfile extends ReferenceEntity {
    /**
     * Базовый тип события (по 0-вому байту).
     * Идентификатор должен быть уникальным в рамках профиля драйвера.
     * Числовой код также должен быть уникальным в рамках профиля драйвера.
     */
    @DBRef
    private EventType mainEventType = new EventType();

    /**
     * Возможные подтипы события (по 5-ому байту).
     * Идентификаторы должны быть уникальными в рамках профиля драйвера.
     */
    @DBRef
    private List<EventType> eventSubTypes = new ArrayList<>();

    /**
     * Флаг говорит о том, что подтипы событий зависят от устройства.
     */
    private boolean havingDeviceTypes = false;

    public EventType getMainEventType() {
        return mainEventType;
    }

    //TODO: юнит-тесты на автозаполнение
    public void setMainEventType(EventType mainEventType) {
        if (mainEventType.isHavingControlDeviceStates() == null)
            mainEventType.setHavingControlDeviceStates(false);
        if (mainEventType.isHavingDeviceInfo() == null)
            mainEventType.setHavingDeviceInfo(false);
        if (mainEventType.isHavingRegionInfo() == null)
            mainEventType.setHavingRegionInfo(false);
        if (mainEventType.isHavingVirtualStateInfo() == null)
            mainEventType.setHavingVirtualStateInfo(false);
        if (mainEventType.isHavingScenarioInfo() == null)
            mainEventType.setHavingScenarioInfo(false);
        if (mainEventType.isHavingLineInfo() == null)
            mainEventType.setHavingLineInfo(false);
        if (mainEventType.getDeviceDecodableStateSetIds() == null)
            mainEventType.setDeviceDecodableStateSetIds(new HashMap<>());
        if (mainEventType.isHavingEmployeeInfo() == null)
            mainEventType.setHavingEmployeeInfo(false);
        this.mainEventType = mainEventType;
    }

    public List<EventType> getEventSubTypes() {
        return eventSubTypes;
    }

    //TODO: юнит-тесты на автозаполнение
    public void setEventSubTypes(List<EventType> eventSubTypes) {
        int code = 0;
        for (EventType eventSubType : eventSubTypes) {
            if (eventSubType.getId() == null)
                eventSubType.setId(mainEventType.getId() + Integer.toString(code));
            if (eventSubType.getCode() == null)
                eventSubType.setCode(code);
            if (eventSubType.getCategory() == null)
                eventSubType.setCategory(mainEventType.getCategory());
            if (eventSubType.isHavingControlDeviceStates() == null)
                eventSubType.setHavingControlDeviceStates(mainEventType.isHavingControlDeviceStates());
            if (eventSubType.isHavingDeviceInfo() == null)
                eventSubType.setHavingDeviceInfo(mainEventType.isHavingDeviceInfo());
            if (eventSubType.isHavingRegionInfo() == null)
                eventSubType.setHavingRegionInfo(mainEventType.isHavingRegionInfo());
            if (eventSubType.isHavingVirtualStateInfo() == null)
                eventSubType.setHavingVirtualStateInfo(mainEventType.isHavingVirtualStateInfo());
            if (eventSubType.isHavingScenarioInfo() == null)
                eventSubType.setHavingScenarioInfo(mainEventType.isHavingScenarioInfo());
            if (eventSubType.isHavingLineInfo() == null)
                eventSubType.setHavingLineInfo(mainEventType.isHavingLineInfo());
            eventSubType.setHavingRegionStateInfo(mainEventType.isHavingRegionStateInfo());
            if (eventSubType.getDeviceDecodableStateSetIds() == null)
                eventSubType.setDeviceDecodableStateSetIds(mainEventType.getDeviceDecodableStateSetIds());
            if (eventSubType.getGeneralDecodableStateSetIds() == null)
                eventSubType.setGeneralDecodableStateSetIds(mainEventType.getGeneralDecodableStateSetIds());
            if (eventSubType.getRegionDecodableStateSetIds() == null)
                eventSubType.setRegionDecodableStateSetIds(mainEventType.getRegionDecodableStateSetIds());
            if (eventSubType.isHavingEmployeeInfo() == null)
                eventSubType.setHavingEmployeeInfo(mainEventType.isHavingEmployeeInfo());
            if (eventSubType.getSubsystem() == null && mainEventType.getSubsystem()!= null){
                eventSubType.setSubsystem(mainEventType.getSubsystem());
            }
            code++;
        }
        this.eventSubTypes = eventSubTypes;
    }

    public boolean isHavingDeviceTypes() {
        return havingDeviceTypes;
    }

    public void setHavingDeviceTypes(boolean havingDeviceTypes) {
        this.havingDeviceTypes = havingDeviceTypes;
    }
}
