package ru.rubezh.firesec.nt.domain.v1.skud;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.AdjectiveEntity;

/**
 * Карта доступа (СКУД).
 * 
 * Служебная сущность, агрегирующая в себе настройки доступа для определенной идентифицируемой единицы (сотрудника,
 * ключа и т.п.).
 * 
 * @author Антон Васильев
 */
public class AccessMap extends AdjectiveEntity {
    /** Точки доступа (идентификаторы устройств с флагом accessPoint в профиле) */
    private List<String> accessPointDeviceIds = new ArrayList<>();

    /** График работы (идентификатор) */
    private String workScheduleId;

    /** Рабочие зоны (идентификаторы СКУД-зон) */
    private List<String> workRegionIds = new ArrayList<>();

    public List<String> getAccessPointDeviceIds() {
        return accessPointDeviceIds;
    }

    public void setAccessPointDeviceIds(List<String> accessPointDeviceIds) {
        this.accessPointDeviceIds = accessPointDeviceIds;
    }

    public String getWorkScheduleId() {
        return workScheduleId;
    }

    public void setWorkScheduleId(String workScheduleId) {
        this.workScheduleId = workScheduleId;
    }

    public List<String> getWorkRegionIds() {
        return workRegionIds;
    }

    public void setWorkRegionIds(List<String> workRegionIds) {
        this.workRegionIds = workRegionIds;
    }

}
