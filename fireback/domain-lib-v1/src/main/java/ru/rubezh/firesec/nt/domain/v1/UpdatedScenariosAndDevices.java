package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Множество измененных сценариев и устройств.
 * 
 * @author Антон Васильев
 *
 */
public class UpdatedScenariosAndDevices {

    private List<String> updatedScenarioIds = new ArrayList<>();
    private List<String> updatedDeviceIds = new ArrayList<>();
    
    public List<String> getUpdatedScenarioIds() {
        return updatedScenarioIds;
    }
    public void setUpdatedScenarioIds(List<String> updatedScenarioIds) {
        this.updatedScenarioIds = updatedScenarioIds;
    }
    public List<String> getUpdatedDeviceIds() {
        return updatedDeviceIds;
    }
    public void setUpdatedDeviceIds(List<String> updatedDeviceIds) {
        this.updatedDeviceIds = updatedDeviceIds;
    }
}
