package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Историческое значение наблюдаемого параметра.
 *
 * @author Александр Горячкин
 */
@Document(collection="log_monitorable_values")
@TypeAlias(value="LogMonitorableValue")
@JsonTypeName("LogMonitorableValue")
public class LogMonitorableValue extends BasicEntity {

    /** Идентификатор профиля наблюдаемого параметра */
    @Indexed
    private String profileId;

    /** Идентификатор устройства */
    @Indexed
    private String deviceId;

    /** Идентификатор проекта */
    @Indexed
    private String projectId;

    /** Время фиксирования значения */
    @Indexed
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date received;

    /** Значение параметра */
    private String value;


    public LogMonitorableValue(String profileId, String deviceId, String projectId, Date received, String value) {
        this.profileId = profileId;
        this.deviceId = deviceId;
        this.projectId = projectId;
        this.received = received;
        this.value = value;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
