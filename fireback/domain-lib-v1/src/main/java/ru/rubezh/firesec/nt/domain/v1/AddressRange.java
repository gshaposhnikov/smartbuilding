package ru.rubezh.firesec.nt.domain.v1;

/**
 * Диапазон адресов.
 * 
 * @author Антон Васильев
 *
 */
public class AddressRange extends AdjectiveEntity {

    /** Первый адрес диапазона */
    private int firstAddress;

    /** Кол-во адресов в диапазоне, включая первый */
    private int addressCount;

    public AddressRange() {
    }

    public boolean contains(int address) {
        return address >= firstAddress &&
                address < (firstAddress + addressCount);
    }

    public AddressRange(int firstAddress, int addressCount) {
        this.setFirstAddress(firstAddress);
        this.setAddressCount(addressCount);
    }

    public AddressRange(AddressRange source) {
        this.setFirstAddress(source.getFirstAddress());
        this.setAddressCount(source.getAddressCount());
    }

    public int getFirstAddress() {
        return firstAddress;
    }

    public void setFirstAddress(int firstAddress) {
        this.firstAddress = firstAddress;
    }

    public int getAddressCount() {
        return addressCount;
    }

    public void setAddressCount(int addressCount) {
        this.addressCount = addressCount;
    }
}
