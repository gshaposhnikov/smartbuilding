package ru.rubezh.firesec.nt.domain.v1.license;

/**
 * Лицензионное ограничение управления устройствами и сценариями.
 * 
 */
public class ControlRestriction {

    /** Инженерное управление */
    private boolean engineeringEnabled;
    /** Управление пожаротушением */
    private boolean firefightingEnabled;

    public boolean isEngineeringEnabled() {
        return engineeringEnabled;
    }
    public void setEngineeringEnabled(boolean engineeringEnabled) {
        this.engineeringEnabled = engineeringEnabled;
    }

    public boolean isFirefightingEnabled() {
        return firefightingEnabled;
    }
    public void setFirefightingEnabled(boolean firefightingEnabled) {
        this.firefightingEnabled = firefightingEnabled;
    }

    public boolean equals(ControlRestriction another) {
        return this.engineeringEnabled == another.engineeringEnabled &&
                this.firefightingEnabled == another.firefightingEnabled;
    }
}
