package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Базовая информации о событии, которая получается от прибора и заполняется в драйвере,
 * и которая в дальнейшем пересылается в бизнес-логику.
 *
 * @author Артем Седанов
 */
@Document(collection = "control_device_events")
@TypeAlias(value = "ControlDeviceEvent")
@JsonTypeName("ControlDeviceEvent")
public class ControlDeviceEvent extends BasicEntity {
    /** Идентификатор типа события (из XML) */
    private String typeId;

    /** Журнал, из которого получено событие */
    private String logTypeId;

    /** Порядковый номер события в БД устройства */
    private long eventNo;

    /** Время возникновения события */
    private Date occurredDateTime;

    /** Время получения события драйвером */
    private Date receivedDateTime;

    /** Данные события о приборе. */
    public static class EventControlDeviceInfo {
        /** Идентификатор прибора, с которого получено событие */
        private String id;

        /** Состояния прибора, устанавливаемые данным событием  (при наличии, иначе {@code null}) */
        private List<String> activeStateIds;

        public List<String> getActiveStateIds() {
            return activeStateIds;
        }

        public void setActiveStateIds(List<String> activeStateIds) {
            this.activeStateIds = activeStateIds;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    /** Данные события об адресном устройстве. */
    public static class EventDeviceInfo {
        /** Локальное устройство (подключенное к прибору, с которого получено событие), или нет (подключенное к
         * соседнему прибору) */
        private boolean deviceLocal;

        /** Адрес управляющего устройства */
        private int controlDeviceAddress;

        /** Идентификатор профиля устройства (из XML) */
        private String profileId;

        /** Адрес устройства */
        private int address;

        /** Линия, на которой подключено устройтсво */
        private int lineNo;

        /** Состояния, которые активны на данном устройтсве */
        private List<String> activeStateIds;

        /** Значения наблюдаемых параметров */
        private Map<String, String> values;

        public String getProfileId() {
            return profileId;
        }

        public void setProfileId(String profileId) {
            this.profileId = profileId;
        }

        public boolean isDeviceLocal() {
            return deviceLocal;
        }

        public void setDeviceLocal(boolean deviceLocal) {
            this.deviceLocal = deviceLocal;
        }

        public int getAddress() {
            return address;
        }

        public void setAddress(int address) {
            this.address = address;
        }

        public int getLineNo() {
            return lineNo;
        }

        public void setLineNo(int lineNo) {
            this.lineNo = lineNo;
        }

        public int getControlDeviceAddress() {
            return controlDeviceAddress;
        }

        public void setControlDeviceAddress(int controlDeviceAddress) {
            this.controlDeviceAddress = controlDeviceAddress;
        }

        public List<String> getActiveStateIds() {
            return activeStateIds;
        }

        public void setActiveStateIds(List<String> activeStateIds) {
            this.activeStateIds = activeStateIds;
        }

        public Map<String, String> getValues() {
            return values;
        }

        public void setValues(Map<String, String> values) {
            this.values = values;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{Device profileId=");
            sb.append(profileId);
            sb.append(", address=");
            sb.append(address);
            sb.append(", line=");
            sb.append(lineNo);
            sb.append(", panelAddress=");
            sb.append(controlDeviceAddress);
            sb.append(", activeStateIds=");
            sb.append(activeStateIds);
            sb.append("}");
            return sb.toString();
        }

        public void addActiveStateIds(List<String> activeStateIds) {
            this.activeStateIds.addAll(activeStateIds);
        }
    }

    /** Данные события о зоне. */
    public static class EventRegionInfo {
        /** Локальный номер зоны в БД устройства */
        private int localNo;

        /** Глобальный номер зоны */
        private int globalNo;

        /** Состояния, активные для данной зоны */
        private List<String> activeStateIds;

        public int getLocalNo() {
            return localNo;
        }

        public void setLocalNo(int localNo) {
            this.localNo = localNo;
        }

        public int getGlobalNo() {
            return globalNo;
        }

        public void setGlobalNo(int globalNo) {
            this.globalNo = globalNo;
        }

        public List<String> getActiveStateIds() {
            return activeStateIds;
        }

        public void setActiveStateIds(List<String> activeStateIds) {
            this.activeStateIds = activeStateIds;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{Region localNo=");
            sb.append(localNo);
            sb.append(", globalNo=");
            sb.append(globalNo);
            sb.append(", activeStateIds=");
            sb.append(activeStateIds);
            sb.append("}");
            return sb.toString();
        }
    }

    /** Данные события о виртуальном состоянии. */
    public static class EventVirtualStateInfo {

        /** Глобальный номер виртуального состояния */
        private int globalNo;

        public int getGlobalNo() {
            return globalNo;
        }

        public void setGlobalNo(int globalNo) {
            this.globalNo = globalNo;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{VirtualState globalNo=");
            sb.append(globalNo);
            sb.append("}");
            return sb.toString();
        }

    }

    /** Данные события о сценарии. */
    public static class EventScenarioInfo {

        /** Глобальный номер сценария */
        private int globalNo;

        /** Состояния, активные для данного сценария */
        private List<String> activeStateIds;

        public int getGlobalNo() {
            return globalNo;
        }

        public void setGlobalNo(int globalNo) {
            this.globalNo = globalNo;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{Scenario globalNo=");
            sb.append(globalNo);
            sb.append("}");
            return sb.toString();
        }

        public List<String> getActiveStateIds() {
            return activeStateIds;
        }

        public void setActiveStateIds(List<String> activeStateIds) {
            this.activeStateIds = activeStateIds;
        }
    }

    /** Данные события о шлейфе. */
    public static class EventLineInfo {

        /** Номер линии (1 или 2) */
        private int lineNo;

        public int getLineNo() {
            return lineNo;
        }

        public void setLineNo(int lineNo) {
            this.lineNo = lineNo;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{Line lineNo=");
            sb.append(lineNo);
            sb.append("}");
            return sb.toString();
        }

    }

    /** Данные события о сотруднике (СКУД). */
    public static class EventEmployeeInfo {

        /** Идентификатор абонента */
        private int employeeId;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{Employee id=");
            sb.append(employeeId);
            sb.append("}");
            return sb.toString();
        }

        public int getEmployeeId() {
            return this.employeeId;
        }

        public void setEmployeeId(int employeeId) {
            this.employeeId = employeeId;
        }
    }

    /** Данные события о ключе доступа (СКУД). */
    public static class AccessKeyValueInfo {

        /** Идентификатор ключа доступа */
        private String accessKeyId;

        /** Значение (код) ключа */
        private String accessKeyValue;

        public AccessKeyValueInfo() {
        }

        public AccessKeyValueInfo(String accessKeyId, String accessKeyValue) {
            this.accessKeyId = accessKeyId;
            this.accessKeyValue = accessKeyValue;
        }

        public String getAccessKeyId() {
            return accessKeyId;
        }

        public void setAccessKeyId(String accessKeyId) {
            this.accessKeyId = accessKeyId;
        }

        public String getAccessKeyValue() {
            return accessKeyValue;
        }

        public void setAccessKeyValue(String accessKeyValue) {
            this.accessKeyValue = accessKeyValue;
        }

    }

    /** Информация о приборе */
    private EventControlDeviceInfo controlDeviceInfo = new EventControlDeviceInfo();

    /** Информация об устройстве (при наличии, иначе {@code null}) */
    private EventDeviceInfo deviceInfo;

    /** Информация о зоне (при наличии, иначе {@code null}) */
    private EventRegionInfo regionInfo;

    /** Информация о виртуальном состоянии (при наличии, иначе {@code null}) */
    private EventVirtualStateInfo virtualStateInfo;

    /** Информация о сценарии (при наличии, иначе {@code null}) */
    private EventScenarioInfo scenarioInfo;

    /** Информация о линии (при наличии, иначе {@code null}) */
    private EventLineInfo lineInfo;

    /** Информация о сотруднике (при наличии, иначе {@code null}) */
    private EventEmployeeInfo employeeInfo;

    /** Значение (код) ключа доступа (при наличии, иначе {@code null}) */
    private String accessKeyValue;

    /** Событие получено не по заданию снятия слепка */
    private boolean online = true;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(String logTypeId) {
        this.logTypeId = logTypeId;
    }

    public long getEventNo() {
        return eventNo;
    }

    public void setEventNo(long eventNo) {
        this.eventNo = eventNo;
    }

    public Date getOccurredDateTime() {
        return occurredDateTime;
    }

    public void setOccurredDateTime(Date occurredDateTime) {
        this.occurredDateTime = occurredDateTime;
    }

    public Date getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(Date receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }

    public EventControlDeviceInfo getControlDeviceInfo() {
        return controlDeviceInfo;
    }

    public void setControlDeviceInfo(EventControlDeviceInfo controlDeviceInfo) {
        this.controlDeviceInfo = controlDeviceInfo;
    }

    public EventDeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(EventDeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public EventRegionInfo getRegionInfo() {
        return regionInfo;
    }

    public void setRegionInfo(EventRegionInfo regionInfo) {
        this.regionInfo = regionInfo;
    }

    public EventVirtualStateInfo getVirtualStateInfo() {
        return virtualStateInfo;
    }

    public void setVirtualStateInfo(EventVirtualStateInfo virtualStateInfo) {
        this.virtualStateInfo = virtualStateInfo;
    }

    public EventScenarioInfo getScenarioInfo() {
        return scenarioInfo;
    }

    public void setScenarioInfo(EventScenarioInfo scenarioInfo) {
        this.scenarioInfo = scenarioInfo;
    }

    public EventLineInfo getLineInfo() {
        return lineInfo;
    }

    public void setLineInfo(EventLineInfo lineInfo) {
        this.lineInfo = lineInfo;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public EventEmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(EventEmployeeInfo employeeInfo) {
        this.employeeInfo = employeeInfo;
    }
    
    public String getAccessKeyValue() {
        return accessKeyValue;
    }

    public void setAccessKeyValue(String accessKeyValue) {
        this.accessKeyValue = accessKeyValue;
    }

}
