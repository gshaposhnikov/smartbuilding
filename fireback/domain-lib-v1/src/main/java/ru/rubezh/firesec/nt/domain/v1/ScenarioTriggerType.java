package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Тип триггера сценария.
 * 
 * Тип события (состояния), которое может возникнуть на устройстве, зоне или приборе и которое может быть
 * проанализировано прибором для принятия решения о включении/выключении сценария в составе логики включения/выключения.
 * 
 * Объекты этого класса должны настраиваться в XML драйверов и заноситься в БД при их регистрации.
 * 
 * @author Антон Васильев
 * @ingroup BasicReferences
 */
@Document(collection="scenario_trigger_types")
@TypeAlias(value="ScenarioTriggerType")
@JsonTypeName("ScenarioTriggerType")
public class ScenarioTriggerType extends ReferenceEntity {

    /** Тип сущности триггера сценария. */
    public enum TriggerEntityType {
        /** Устройство (не прибор) */
        DEVICE,
        /** Зона */
        REGION,
        /** Прибор */
        CONTROL_DEVICE,
        /** Виртуальное состояние */
        VIRTUAL_STATE
    }
    
    /** Тип сущности, к которой относится данный тип триггера сценария */
    private TriggerEntityType entityType = TriggerEntityType.REGION;
    /** Подсистема (поле актуально только для триггеров по зоне) */
    private Subsystem subsystem = Subsystem.GENERAL;
    /** Код состояния в БД прибора */
    private int code;
    /** "Подделка" - логика с триггером с этим флагом не должна писаться в БД прибора (только сам сценарий) */
    private boolean fake;
    /** Должна быть единственной группой в настройке логики включения без возможности выбора «во всех из» */
    private boolean onlyOne;
    /** Тип назначения сценария, для которого разрешено использовать триггер. */
    private ScenarioPurpose scenarioPurpose = ScenarioPurpose.EXEC_BY_LOGIC;
    /** Принудительно выставляемый тип логики */
    private ScenarioLogicBlock.LogicType forcedScenarioLogicType;
    /** Идентификатор типа действия, принудительно применяемый к исполнительным блокам */
    private String forcedScenarioActionTypeId;
    /** Название состояния в desktop версии*/
    private String desktopTriggerTypeName;

    public ScenarioTriggerType() {
    }
    
    public ScenarioTriggerType(ScenarioTriggerType source) {
        super(source);
        
        this.entityType = source.entityType;
        this.subsystem = source.subsystem;
        this.code = source.code;
    }
    
    public String getDesktopTriggerTypeName() {
        return desktopTriggerTypeName;
    }

    public void setDesktopTriggerTypeName(String desktopTriggerTypeName) {
        this.desktopTriggerTypeName = desktopTriggerTypeName;
    }

    public TriggerEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(TriggerEntityType entityType) {
        this.entityType = entityType;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public int getCode() {
        return code;
    }
    
    public void setCode(int code) {
        this.code = code;
    }

    public boolean isFake() {
        return fake;
    }
    
    public void setFake(boolean fake) {
        this.fake = fake;
    }

    public boolean isOnlyOne() {
        return onlyOne;
    }
    
    public void setOnlyOne(boolean onlyOne) {
        this.onlyOne = onlyOne;
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public String getForcedScenarioActionTypeId() {
        return forcedScenarioActionTypeId;
    }

    public void setForcedScenarioActionTypeId(String forcedScenarioActionTypeId) {
        this.forcedScenarioActionTypeId = forcedScenarioActionTypeId;
    }

    public ScenarioLogicBlock.LogicType getForcedScenarioLogicType() {
        return forcedScenarioLogicType;
    }

    public void setForcedScenarioLogicType(ScenarioLogicBlock.LogicType forcedScenarioLogicType) {
        this.forcedScenarioLogicType = forcedScenarioLogicType;
    }

}
