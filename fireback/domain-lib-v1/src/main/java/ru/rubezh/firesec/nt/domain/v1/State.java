package ru.rubezh.firesec.nt.domain.v1;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Состояние.
 * 
 * Характеристика сущности (устройства, зоны, подсистемы) логического типа (есть/нет).
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 * @ingroup BasicReferences
 */
@Document(collection="states")
@TypeAlias(value="State")
@JsonTypeName("State")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = DecodableState.class, name = "DecodableState"),
})

public class State extends ReferenceEntity {
    /** Категория (класс) состояния */
    @DBRef private StateCategory stateCategory;

    /** Подсистема */
    private Subsystem subsystem = Subsystem.UNKNOWN;

    /** Допустимо использование в качестве условия в исполнительных блоках сценариев */
    private boolean acceptableAsScenarioCondition;
    
    /** Код для записи в БД при использовании в качестве условия в исполнительных блоках сценариев */
    private int scenarioConditionCode;

    /** Группа состояний с ручным сбросом */
    private ManualResetStateGroup manualResetStateGroup = ManualResetStateGroup.NONE;

    /** Список идентификаторов типов событий, генерируемых при установке состояния */
    private List<String> enterEventTypeIds = new ArrayList<>();

    /** Список идентификаторов типов событий, генерируемых при сбросе состояния */
    private List<String> leaveEventTypeIds = new ArrayList<>();

    /** Идентификаторы состояний, автоматически сбрасываемых всем устройствам прибора при сбросе состояния прибора */
    private List<String> controlAutoResetStateIds = new ArrayList<>();

    /** Применить состояние к сценариям прибора */
    private boolean applicableToScenarios;

    /**
     * Флаг: состояние является причиной возникшего события и не может
     * устанавливаться или сбрасываться по опросу
     */
    private boolean eventReason;

    /**
     * Флаг: состояние является логическим, т.е. было определено и установлено сервером бизнес-логики и не может
     * устанавливаться или сбрасываться по опросу оборудования
     */
    private boolean logic;

    /** Название состояния в desktop версии*/
    private String desktopStateName;

    /** Список дополнительных подсистем  */
    private List<Subsystem> additionalSubsystems = new ArrayList<>();

    private boolean offState;

    private Integer number;
    
    public String getDesktopStateName() {
        return desktopStateName;
    }

    public void setDesktopStateName(String desktopStateName) {
        this.desktopStateName = desktopStateName;
    }

    public StateCategory getStateCategory() {
        return stateCategory;
    }

    public void setStateCategory(StateCategory stateCategory) {
        this.stateCategory = stateCategory;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public boolean isAcceptableAsScenarioCondition() {
        return acceptableAsScenarioCondition;
    }

    public void setAcceptableAsScenarioCondition(boolean acceptableAsScenarioCondition) {
        this.acceptableAsScenarioCondition = acceptableAsScenarioCondition;
    }

    public int getScenarioConditionCode() {
        return scenarioConditionCode;
    }

    public void setScenarioConditionCode(int scenarioConditionCode) {
        this.scenarioConditionCode = scenarioConditionCode;
    }

    public ManualResetStateGroup getManualResetStateGroup() {
        return manualResetStateGroup;
    }

    public void setManualResetStateGroup(ManualResetStateGroup manualResetStateGroup) {
        this.manualResetStateGroup = manualResetStateGroup;
    }

    public List<String> getEnterEventTypeIds() {
        return enterEventTypeIds;
    }

    public void setEnterEventTypeIds(List<String> enterEventTypeIds) {
        this.enterEventTypeIds = enterEventTypeIds;
    }

    public List<String> getLeaveEventTypeIds() {
        return leaveEventTypeIds;
    }

    public void setLeaveEventTypeIds(List<String> leaveEventTypeIds) {
        this.leaveEventTypeIds = leaveEventTypeIds;
    }

    public List<String> getControlAutoResetStateIds() {
        return controlAutoResetStateIds;
    }

    public void setControlAutoResetStateIds(List<String> controlAutoResetStateIds) {
        this.controlAutoResetStateIds = controlAutoResetStateIds;
    }

    public boolean isApplicableToScenarios() {
        return applicableToScenarios;
    }

    public void setApplicableToScenarios(boolean applicableToScenarios_) {
        applicableToScenarios = applicableToScenarios_;
    }

    public boolean isEventReason() {
        return eventReason;
    }

    public void setEventReason(boolean eventReason) {
        this.eventReason = eventReason;
    }

    public boolean isLogic() {
        return logic;
    }

    public void setLogic(boolean logic) {
        this.logic = logic;
    }

    public List<Subsystem> getAdditionalSubsystems() {
        return additionalSubsystems;
    }

    public void setAdditionalSubsystems(List<Subsystem> additionalSubsystems) {
        this.additionalSubsystems = additionalSubsystems;
    }

    public boolean isOffState() {
        return offState;
    }

    public void setOffState(boolean offState) {
        this.offState = offState;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
