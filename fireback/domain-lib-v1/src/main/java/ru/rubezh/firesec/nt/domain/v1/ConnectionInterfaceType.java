package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип интерфейса подключения.
 * 
 * @author Антон Васильев
 *
 */
public enum ConnectionInterfaceType {
    NONE,
    USB,
    ETHERNET,
    WIRELESS,
    RS_232,
    RS_485,
    INTERNAL_BUS
}
