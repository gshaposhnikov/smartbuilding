package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Виртуальный индикатор.
 * 
 * Альтернативный способ отображения состояний устройств, зон и сценариев. Используется для отображения состояния группы
 * элементов.
 * 
 * @author Александр Горячкин, Антон Караваев
 * @ingroup UIDomain
 */
public class Indicator extends BasicEntity{

    /** Название */
    private String name = "";
    /** Описание */
    private String description = "";

    Indicator(){}

    public Indicator(int rowNo, int colNo){
        setRowNo(rowNo);
        setColNo(colNo);
        setEntityType(EntityType.NONE);
    }

    /** Флаг необходимости запроса пароля при управлении объектом через индикатор */
    private boolean passwordNeeded;
    /** Номер строки в панели */
    private int rowNo;
    /** Номер столбца в панели */
    private int colNo;
    /** Тип связанных объектов */
    private EntityType entityType;
    /** Набор идентификаторов сущностей */
    private List<String> entityIds = new ArrayList<>();
    /** Стиль индикатора **/
    private Style styles;
    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    /** Стилевые параметры вирутального индикатора. */
    public static class Style{
        /** Минимальный размер шрифта */
        public static final int MIN_FONT_SIZE = 8;
        /** Максимальный размер шрифта */
        public static final int MAX_FONT_SIZE = 36;
        /** Стиль написания (italic) */
        private String fontStyle;
        /** Толщина шрифта (bold) */
        private String fontWeight;
        /** Ориентация текста {@code (<LEFT|CENTER|RIGHT>) } */
        private String textAlign;
        /** Оформление текста (underline) */
        private String textDecoration;
        /** Размер текста */
        private int fontSize;

        public String getFontStyle() {
            return fontStyle;
        }

        public void setFontStyle(String fontStyle) {
            this.fontStyle = fontStyle;
        }

        public String getFontWeight() {
            return fontWeight;
        }

        public void setFontWeight(String fontWeight) {
            this.fontWeight = fontWeight;
        }

        public String getTextAlign() {
            return textAlign;
        }

        public void setTextAlign(String textAlign) {
            this.textAlign = textAlign;
        }

        public String getTextDecoration() {
            return textDecoration;
        }

        public void setTextDecoration(String textDecoration) {
            this.textDecoration = textDecoration;
        }

        public int getFontSize() {
            return fontSize;
        }

        public void setFontSize(int fontSize) {
            this.fontSize = fontSize;
        }
    }

    public void update(Indicator indicator){
        setName(indicator.getName());
        setDescription(indicator.getDescription());
        setStyles(indicator.getStyles());
        setPasswordNeeded(indicator.isPasswordNeeded());
        setEntityType(indicator.getEntityType());
        if (indicator.entityType == EntityType.NONE)
            entityIds.clear();
        else
            setEntityIds(indicator.getEntityIds());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public int getColNo() {
        return colNo;
    }

    public void setColNo(int colNo) {
        this.colNo = colNo;
    }

    public List<String> getEntityIds() {
        return entityIds;
    }

    public void setEntityIds(List<String> entityIds) {
        this.entityIds = entityIds;
    }

    public boolean isPasswordNeeded() {
        return passwordNeeded;
    }

    public void setPasswordNeeded(boolean passwordNeeded) {
        this.passwordNeeded = passwordNeeded;
    }

    public int getRowNo() {
        return rowNo;
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public Style getStyles() {
        return styles;
    }

    public void setStyles(Style styles) {
        this.styles = styles;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
