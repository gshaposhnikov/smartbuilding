package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип сущности.
 * 
 * @author Антон Васильев
 *
 */
public enum EntityType {
    /** Устройство */
    DEVICE,
    /** Устройство - прибор */
    CONTROL_DEVICE,
    /** Зона */
    REGION,
    /** Сценарий */
    SCENARIO,
    /** Виртуальное состояние */
    VIRTUAL_STATE,
    /** Виртуальный индикатор */
    VIRTUAL_INDICATOR,
    /** Сотрудник (СКУД) */
    EMPLOYEE,
    /** Ключ доступа (карта или пароль СКУД) */
    ACCESS_KEY,
    /** Рабочий график (СКУД) */
    WORK_SCHEDULE,
    /** Не определено */
    NONE,
    /** Виртуальная панель индикаторов*/
    VIRTUAL_INDICATOR_PANEL,
    /** Виртуальная группа панелей индикаторов*/
    VIRTUAL_INDICATOR_PANEL_GROUP
}
