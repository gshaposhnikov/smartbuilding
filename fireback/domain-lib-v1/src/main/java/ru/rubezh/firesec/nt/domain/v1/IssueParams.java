package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;

/**
 * Структурированные определения параметров задач (union).
 * 
 * @author Антон Васильев
 *
 */
@JsonTypeName("IssueParams")
public class IssueParams {

    /**
     * Параметры задачи на запись БД СКУД в прибор.
     * 
     * @author Антон Васильев
     *
     */
    public static class WriteSkudDatabaseParams {

        public WriteSkudDatabaseParams(){}

        public WriteSkudDatabaseParams(List<String> controlDeviceIds, List<Employee> employees,
                                       List<AccessKey> accessKeys, List<WorkSchedule> workSchedules){
            this.controlDeviceIds = controlDeviceIds;
            this.employees = employees;
            this.accessKeys = accessKeys;
            this.workSchedules = workSchedules;
        }

        /** Идентификаторы приборов. */
        private List<String> controlDeviceIds = new ArrayList<>();

        /** Сотрдуники */
        private List<Employee> employees = new ArrayList<>();

        /** Ключи доступа */
        private List<AccessKey> accessKeys = new ArrayList<>();

        /** Графики работ */
        private List<WorkSchedule> workSchedules = new ArrayList<>();

        public List<String> getControlDeviceIds() {
            return controlDeviceIds;
        }

        public void setControlDeviceIds(List<String> controlDeviceIds) {
            this.controlDeviceIds = controlDeviceIds;
        }

        public List<Employee> getEmployees() {
            return employees;
        }

        public void setEmployees(List<Employee> employees) {
            this.employees = employees;
        }

        public List<AccessKey> getAccessKeys() {
            return accessKeys;
        }

        public void setAccessKeys(List<AccessKey> accessKeys) {
            this.accessKeys = accessKeys;
        }

        public List<WorkSchedule> getWorkSchedules() {
            return workSchedules;
        }

        public void setWorkSchedules(List<WorkSchedule> workSchedules) {
            this.workSchedules = workSchedules;
        };

    }

    public WriteSkudDatabaseParams writeSkudDatabaseParams;

}
