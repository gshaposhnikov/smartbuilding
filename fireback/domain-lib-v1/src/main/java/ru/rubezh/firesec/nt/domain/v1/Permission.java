package ru.rubezh.firesec.nt.domain.v1;

/**
 * Права доступа пользователей и групп.
 * 
 * Используется для разграничения доступа к функциям системы.
 * 
 * @author Александр Горячкин
 * @ingroup BasicReferences
 */
public class Permission extends ReferenceEntity {

    /** Уровень доступа. */
    public static class AccessLevel extends ReferenceEntity {
        /** Описание уровня доступа права */
        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    /** Функция доступа. */
    public static class Target extends ReferenceEntity {
        /** Описание цели права */
        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    /** Название */
    private String name;
    /** Описание */
    private String description = "";
    /** Идентификатор цели права */
    private String targetId;
    /** Идентификатор уровня доступа права */
    private String accessLevelId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getAccessLevelId() {
        return accessLevelId;
    }

    public void setAccessLevelId(String accessLevelId) {
        this.accessLevelId = accessLevelId;
    }
}
