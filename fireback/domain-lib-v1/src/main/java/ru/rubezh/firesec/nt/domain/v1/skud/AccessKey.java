package ru.rubezh.firesec.nt.domain.v1.skud;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonTypeName;

import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;

/**
 * Ключ доступа (СКУД).
 * 
 * Может быть пароль или карта, по которым пользователь проходит через точки доступа (СКУД).
 * 
 * @author Антон Васильев
 * @ingroup BasicDomain
 */
@Document(collection = "access_keys")
@TypeAlias(value = "AccessKey")
@JsonTypeName("AccessKey")
@CompoundIndexes({
    @CompoundIndex(def = "{'projectId':1, 'keyType':1, 'keyValue':1}", unique = true),
    @CompoundIndex(def = "{'projectId':1, 'keyType':1, 'index':1}", unique = true) 
})
public class AccessKey extends BasicEntityWithDate {

    /** Тип ключа доступа. */
    public enum KeyType {
        PASSWORD,
        CARD
    }

    /** Тип ключа */
    private KeyType keyType;

    /** Значение ключа (от 1 до 6 символов) */
    private String keyValue;

    /** Числовой идентификатор (беззнаковое слово) */
    private int index;

    /** Идентификатор проекта */
    @Indexed
    private String projectId;

    /** Идентификатор сотрудника */
    private String employeeId;

    /** Дата начала действия */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date activeFrom;

    /** Дата окончания действия */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date activeTo;

    /** Ключ доступа заблокирован */
    private boolean locked;

    /** Игнорировать АПБ */
    private boolean ignoreAPBOn;

    /** Подтверждающий ключ */
    private boolean confirmationKeyOn;

    /** Без доп. кода */
    private boolean advancedCodeOff;

    /** По принуждению */
    private boolean disarmForcedOn;

    /** Карта доступа */
    private AccessMap accessMap = new AccessMap();

    public final static int MIN_KEY_VALUE_LENGTH = 4;

    public final static int MAX_KEY_VALUE_LENGTH = 12;

    public void update(AccessKey srcAccessKey) {
        setKeyValue(srcAccessKey.getKeyValue());
        setActiveFrom(srcAccessKey.getActiveFrom());
        setActiveTo(srcAccessKey.getActiveTo());
        setLocked(srcAccessKey.isLocked());
        setIgnoreAPBOn(srcAccessKey.isIgnoreAPBOn());
        setConfirmationKeyOn(srcAccessKey.isConfirmationKeyOn());
        setAdvancedCodeOff(srcAccessKey.isAdvancedCodeOff());
        setDisarmForcedOn(srcAccessKey.isDisarmForcedOn());
        setAccessMap(srcAccessKey.getAccessMap());
        setUpdateDateTime(new Date());
    }

    public KeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(KeyType keyType) {
        this.keyType = keyType;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Date getActiveFrom() {
        return activeFrom;
    }

    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = activeFrom;
    }

    public Date getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(Date activeTo) {
        this.activeTo = activeTo;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isIgnoreAPBOn() {
        return ignoreAPBOn;
    }

    public void setIgnoreAPBOn(boolean ignoreAPBOn) {
        this.ignoreAPBOn = ignoreAPBOn;
    }

    public boolean isConfirmationKeyOn() {
        return confirmationKeyOn;
    }

    public void setConfirmationKeyOn(boolean confirmationKeyOn) {
        this.confirmationKeyOn = confirmationKeyOn;
    }

    public boolean isAdvancedCodeOff() {
        return advancedCodeOff;
    }

    public void setAdvancedCodeOff(boolean advancedCodeOff) {
        this.advancedCodeOff = advancedCodeOff;
    }

    public boolean isDisarmForcedOn() {
        return disarmForcedOn;
    }

    public void setDisarmForcedOn(boolean disarmForcedOn) {
        this.disarmForcedOn = disarmForcedOn;
    }

    public AccessMap getAccessMap() {
        return accessMap;
    }

    public void setAccessMap(AccessMap accessMap) {
        this.accessMap = accessMap;
    }

}
