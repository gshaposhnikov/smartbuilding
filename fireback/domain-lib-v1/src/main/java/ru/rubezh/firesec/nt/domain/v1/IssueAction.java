package ru.rubezh.firesec.nt.domain.v1;

/** Тип задачи. */
public enum IssueAction {
    /** Запись конфигурации в устройство */
    WRITE_CONFIG_TO_DEVICE(EntityType.DEVICE),
    /** Чтение конфигурации из устройство */
    READ_CONFIG_FROM_DEVICE(EntityType.DEVICE),
    /** Сброс состояния на устройстве */
    RESET_STATE_ON_DEVICE(EntityType.DEVICE),
    /** Сброс состояния на всех устройствах */
    RESET_STATE_ON_ALL_DEVICES(EntityType.NONE),
    /** Запуск сценария на приборе */
    START_SCENARIO(EntityType.SCENARIO),
    /** Остановка сценария на приборе */
    STOP_SCENARIO(EntityType.SCENARIO),
    /** Включение сценария на приборе */
    ENABLE_SCENARIO(EntityType.SCENARIO),
    /** Отключение сценария на приборе */
    DISABLE_SCENARIO(EntityType.SCENARIO),
    /** Запись БД в прибор */
    WRITE_CONTROL_DEVICE_DATABASE(EntityType.CONTROL_DEVICE),
    /** Запись БД во все приборы */
    WRITE_ALL_CONTROL_DEVICES_DATABASE(EntityType.NONE),
    /** Включить опрос устройства */
    ENABLE_DEVICE_POLLING_STATE(EntityType.DEVICE),
    /** Включить опрос всех устройств зоны */
    ENABLE_REGION_DEVICES_POLLING_STATE(EntityType.REGION),
    /** Отключить опрос устройства */
    DISABLE_DEVICE_POLLING_STATE(EntityType.DEVICE),
    /** Отключить опрос всех устройств зоны */
    DISABLE_REGION_DEVICES_POLLING_STATE(EntityType.REGION),
    /** Поставить зону на охрану */
    ENABLE_REGION_GUARD(EntityType.REGION),
    /** Снять зону с охраны */
    DISABLE_REGION_GUARD(EntityType.REGION),
    /** Установить время на приборе */
    SET_CONTROL_DEVICE_TIME(EntityType.CONTROL_DEVICE),
    /** Установить пароль пользователя прибора */
    SET_CONTROL_DEVICE_USER_PASSWORD(EntityType.CONTROL_DEVICE),
    /** Перезагрузить прибор*/
    RESET_CONTROL_DEVICE(EntityType.CONTROL_DEVICE),
    /** Обновление ПО на приборе */
    UPDATE_CONTROL_DEVICE_FIRMWARE(EntityType.CONTROL_DEVICE),
    /** Обновление ПО на всех приборах */
    UPDATE_ALL_CONTROL_DEVICE_FIRMWARE(EntityType.NONE),
    /** Чтение журналов с прибора */
    READ_EVENTS(EntityType.CONTROL_DEVICE),
    /** Выполнить действие устройства */
    PERFORM_DEVICE_ACTION(EntityType.DEVICE),
    /** Выполнить действие сценария */
    PERFORM_SCENARIO_ACTION(EntityType.SCENARIO),
    /** Поставить на охрану все зоны виртуального индикатора */
    ENABLE_INDICATOR_REGIONS_GUARD(EntityType.VIRTUAL_INDICATOR),
    /** Снять с охраны все зоны виртуального индикатора */
    DISABLE_INDICATOR_REGIONS_GUARD(EntityType.VIRTUAL_INDICATOR),
    /** Включить опрос всех устройств зон индикатора */
    ENABLE_INDICATOR_REGIONS_POLLING_STATE(EntityType.VIRTUAL_INDICATOR),
    /** Отключить опрос всех устройств зон виртуального индикатора */
    DISABLE_INDICATOR_REGIONS_POLLING_STATE(EntityType.VIRTUAL_INDICATOR),
    /** Включить опрос всех устройств виртуального индикатора */
    ENABLE_INDICATOR_DEVICES_POLLING_STATE(EntityType.VIRTUAL_INDICATOR),
    /** Отключить опрос всех устройств виртуального индикатора */
    DISABLE_INDICATOR_DEVICES_POLLING_STATE(EntityType.VIRTUAL_INDICATOR),
    /** Выполнить действие сценария у индикатора*/
    PERFORM_INDICATOR_SCENARIO_ACTION(EntityType.VIRTUAL_INDICATOR),
    /** Считать сохранённую конфигурацию прибора */
    READ_CONTROL_DEVICE_DATABASE(EntityType.CONTROL_DEVICE),
    /** Записать БД СКУД в перечисленные в параметрах приборы */
    WRITE_SKUD_DATABASE(EntityType.NONE),
    /** Прочитать ключ доступа (код) */
    START_READ_ACCESS_KEY(EntityType.DEVICE),
    /** Отменить чтение ключа доступа */
    STOP_READ_ACCESS_KEY(EntityType.DEVICE),
    /** Change device state */
    SET_DEVICE_STATE(EntityType.NONE),
    /** Change region state */
    SET_REGION_STATE(EntityType.NONE),
    /** Change apartment state */
    SET_APARTMENT_STATE(EntityType.NONE),
    /** Set monitorable parameter on device */
    SET_DEVICE_MONITORABLE_VALUE(EntityType.NONE);
    /**
     * Тип сущности, к которой должна относиться задача (для валидации задачи)
     */
    private EntityType entityType;

    private IssueAction(EntityType entityType) {
        this.entityType = entityType;
    }
    
    public EntityType getEntityType() {
        return entityType;
    }

}
