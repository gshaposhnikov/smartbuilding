package ru.rubezh.firesec.nt.domain.v1;

/**
 * Перечисление параметров устройств, определённых в xml и используемых в коде.
 * 
 * @author Андрей Лисовой
 */
public enum DeviceProperties {
    USBSerialNumber,
    RS485Bitrate,
    SerialNumber,
    FirmwareVersion,
    DatabaseVersion,
    LastReadDateTime,
    LastWriteDateTime,
    AlternateIcon,
    AlternateName,
    NoOneTriggeredMessage,
    FirstTriggeredMessage,
    SecondTriggeredMessage,
    BothTriggeredMessage,
    EnabledMessage,
    DisabledMessage,
    Line1IsRing,
    Line2IsRing,
    Line1Isolator,
    Line2Isolator,
    LightAndSoundAlarm,
    TimeAutoSet,
    DeviceFailureFeature,
    TwoRegionsFireFeature,
    ManageAllowed,
    VirtualType,
    /** Идентификатор зоны "куда" для точек доступа */
    RegionToId,
    /** Идентификатор зоны "откуда" для точек доступа */
    RegionFromId,
    /** Настройка правила "antipassback" */
    APBRules,
    /** Настройка режима доступа */
    AccessMode,
}
