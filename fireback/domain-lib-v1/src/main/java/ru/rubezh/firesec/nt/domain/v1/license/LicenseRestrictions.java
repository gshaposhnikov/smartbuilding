package ru.rubezh.firesec.nt.domain.v1.license;

/**
 * Лицензионные ограничения.
 */
public class LicenseRestrictions {

    /** Управление устройствами и сценариями */
    private ControlRestriction control = new ControlRestriction();
    /** СКУД */
    private SkudRestriction skud = new SkudRestriction();

    public ControlRestriction getControl() {
        return control;
    }
    public void setControl(ControlRestriction control) {
        this.control = control;
    }

    public SkudRestriction getSkud() {
        return skud;
    }
    public void setSkud(SkudRestriction skud) {
        this.skud = skud;
    }

    public boolean equals(LicenseRestrictions another) {
        return this.control.equals(another.control) &&
                this.skud.equals(another.skud);
    }

}
