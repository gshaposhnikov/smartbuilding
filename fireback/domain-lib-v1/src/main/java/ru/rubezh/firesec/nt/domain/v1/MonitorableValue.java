package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Профиль стандартной наблюдаемой величины.
 * 
 * Содержит параметры раскодирования и перевода сырых значений в диницы измерения наблюдаемой величины.
 * 
 * @todo поменять название
 *
 * @author Александр Горячкин
 */
public class MonitorableValue extends AdjectiveEntity {

    /** Профиль набл. параметра */
    @DBRef
    private MonitorableValueProfile profile;
    /**
     * Идентификатор профиля наблюдаемых параметров (требуется для присоединения
     * профиля в агрегирующих mongo-скриптах)
     */
    private String monitorableValueProfileId;

    /** Тип параметра */
    private PropertyType type;

    /** Минимальное значение */
    private double min = 0;

    /** Максимальное значение */
    private double max = 1;

    /** Множитель для перевода из сырого значения */
    private double multiplier = 1;

    /** Добавляемое значение для перевода из сырого значения */
    private double addend = 0;

    /** Сдвиг в данных состояния */
    private int shiftInStateMemory = 0;

    /** Сдвиг в данных события */
    private int shiftInEventMemory = 0;

    /** Номер первого бита параметра */
    private int bitRangeFrom = 0;

    /** Номер последнего бита параметра */
    private int bitRangeTo = 7;


    /** Раскодировать значение из переданных данных */
    public String decode(byte[] data, boolean isEventInfo) {
        byte targetByte = data[isEventInfo ? shiftInEventMemory : shiftInStateMemory];
        int rawVal = ((targetByte & 0xFF) >> bitRangeFrom) & ((1 << (bitRangeTo - bitRangeFrom + 1)) - 1);
        double result = rawVal * multiplier + addend;

        if (result < min) result = min;
        else if (result > max) result = max;

        String resultStr = null;
        if (type == PropertyType.INTEGER)
            resultStr = Integer.toString((int)result);
        else if (type == PropertyType.FLOAT)
            resultStr = Double.toString(result);

        return resultStr;
    }

    public MonitorableValueProfile getProfile() {
        return profile;
    }

    public void setProfile(MonitorableValueProfile profile) {
        this.profile = profile;
        this.monitorableValueProfileId = profile.getId();
    }

    public String getMonitorableValueProfileId() {
        return monitorableValueProfileId;
    }

    public void setMonitorableValueProfileId(String monitorableValueProfileId) {
        this.monitorableValueProfileId = monitorableValueProfileId;
    }

    public PropertyType getType() {
        return type;
    }

    public void setType(PropertyType type) {
        this.type = type;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public double getAddend() {
        return addend;
    }

    public void setAddend(double addend) {
        this.addend = addend;
    }

    public int getShiftInStateMemory() {
        return shiftInStateMemory;
    }

    public void setShiftInStateMemory(int shiftInStateMemory) {
        this.shiftInStateMemory = shiftInStateMemory;
    }

    public int getShiftInEventMemory() {
        return shiftInEventMemory;
    }

    public void setShiftInEventMemory(int shiftInEventMemory) {
        this.shiftInEventMemory = shiftInEventMemory;
    }

    public int getBitRangeFrom() {
        return bitRangeFrom;
    }

    public void setBitRangeFrom(int bitRangeFrom) {
        this.bitRangeFrom = bitRangeFrom;
    }

    public int getBitRangeTo() {
        return bitRangeTo;
    }

    public void setBitRangeTo(int bitRangeTo) {
        this.bitRangeTo = bitRangeTo;
    }

}
