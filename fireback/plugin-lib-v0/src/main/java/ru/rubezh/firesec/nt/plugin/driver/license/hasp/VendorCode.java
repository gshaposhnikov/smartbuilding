package ru.rubezh.firesec.nt.plugin.driver.license.hasp;

/**
 * Код производителя ПО
 */
public class VendorCode {

    public static final String RUBEZH_FIRESEC_NT = new String(
        "YZDjqyHpL8vID7wTmN23uKcdbF/yPIwO/oieppGxqS/OGo25wWrRi1oqxwXSdZKsFb0FgXO2mev5T0v4V9oyzmbUBhZbs5r+3Do+" +
        "HnqXWC5/fXYwPWklzHmLC6tTDkh09JkwnOhExL+MKP/GB5mCqi6yisGX2YpR0CB5mmTIgIATgdpGmlR8xkPj2TeCuJYUdjDTROx5" +
        "FNQltWI0cbuFwGdBgfShN3tpvJailE+HLkdUK/MUILLCC9y0zQ9CBC1b+RXY+ivGebLAi6rbFv5bT/NCAqoU21wyC1skQxAg73XO" +
        "H7fUc1WPLshXIv7KnYfsBwVQ6+xd+qx13/7Zn+CuPUgkw/6mz0TWAZeUfbuzKgbcMWek0rFpTXno/4xh/SdxYeYt0WLKu/aG4OE6" +
        "mZ9Z5HspdbLBUir/Trak5UeWSVdBJyjjfbXEikBlWDi+TkrUkOVZGGRyes9KdO2xmWN17/4bwTZCyLE6lv2PXOl+djriANI5xxo4" +
        "gBPxX07VtAvdcXFkMTy6nEYK4CwYx5dprQGCAGbFdxD8NtbxY4HW/GtWOgcGK+t/vIGqL6X8efkvBSuaqptZYN6dPPn86CoumKSJ" +
        "MtOlAYrxt7O0GPZ3VZIPD4E7DZTxL5qtTp+gjBYfAoEqp9mOR5IoyvXT5vfAIr3oJb26VHQUW6BlPlq9Ne0HzOrwFiJHzxCvw1qG" +
        "BgYh5QZj0T6CqbkGCrNWuz2KyUbcTPul+SEvSVsCwUSwmF2FN0wqwkIwRb7qQ4D1WMOhJp53gIOXeIJGwKD91RjpecBTsg4cHlGH" +
        "AES9pGMpMDFfBD9tg3PRBs8twuB3Sc7cB4xAzszYQUW5nefAnnqdulywTmevnWv6oqpAjV/+Nz6Fuj1//vnDXArp8C6qd5aPAqff" +
        "WskjAyQWWXLCXRIBNQMMtyIkrp4Rd5BK9RBWvVu5icLMVMWDVjxG51nW/wuRkz78pCZILkwKsNVajw6IiQ==");

}
