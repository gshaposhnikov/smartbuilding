package ru.rubezh.firesec.nt.plugin;

public interface Plugin {

    void onStart(long currentTimeMs);
    void onStop(long currentTimeMs);
    void process(long currentTimeMs);
}
