package ru.rubezh.firesec.nt.plugin.driver.clock;

public interface TimerHandler {
    public void onTime(long currentTimeMs);
}
