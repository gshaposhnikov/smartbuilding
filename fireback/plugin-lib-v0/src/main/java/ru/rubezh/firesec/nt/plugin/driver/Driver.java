package ru.rubezh.firesec.nt.plugin.driver;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rubezh.firesec.nt.amqp.message.alert.ActiveProjectValidateStatus;
import ru.rubezh.firesec.nt.amqp.message.alert.DriverRegisteredAlert;
import ru.rubezh.firesec.nt.amqp.message.alert.LicenseRestrictionsUpdated;
import ru.rubezh.firesec.nt.amqp.message.request.DeleteTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.PauseTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.SetDriverDeviceConfigRequest;
import ru.rubezh.firesec.nt.amqp.message.response.*;
import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.plugin.Plugin;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseChecker;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseReader;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Общий абстрактный класс для реализации драйвера. Минимальный набор действий
 * для создания нового драйвера:
 * <ul>
 * <li>Создать класс машины состояний, реализующий интерфейс
 * {@link StateMachine}.</li>
 * <li>Создать класс обработчика запросов к драйверу, реализующий интерфейс
 * {@link DriverRequestHandler}. Этот обработчик должен вызывать
 * {@link #setInServiceProject(long, String, List, List, List, List, List, List, List, List)} при приеме
 * соответствующего запроса на установку устройств.</li>
 * <li>Реализовать абстрактный метод
 * {@link #fillMapsAndValidateProjectAndFetchStateMachines(List, List, List)}, который должен для переданного
 * дерева устройств вернуть список машин
 * состояний, работающих с данными устройствами.</li>
 * <li>На момент старта драйвера менеджером плагинов (при помощи вызова
 * {@link #onStart(long)}), у драйвера должны быть установлены поля:</li>
 * <ul>
 * <li>{@link #requestQueueName}</li>
 * <li>{@link #alertsExchangeName}</li>
 * <li>{@link #driverRequestHandler}</li>
 * <li>{@link #driverAlertSender}</li>
 * <li>{@link #rabbitTemplate}</li>
 * <li>{@link #listenerContainer}</li>
 * <li>{@link #driverId}</li>
 * <li>{@link #licenseChecker}</li>
 * </ul>
 * </ul>
 * Вся работа драйвера должна быть реализована в соответствующих
 * {@link StateMachine} с обработкой запросов в {@link DriverRequestHandler} и
 * отправкой оповещений через {@link DriverAlertSender}.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 * @author Антон Васильев
 *
 */
abstract public class Driver implements Plugin {
    /* beans */
    protected Logger logger;
    /** Обработчик запросов к драйверу */
    protected DriverRequestHandler driverRequestHandler;
    /** Отправщик оповещений */
    protected DriverAlertSender driverAlertSender;
    /** Настройки для отправки и приема сообщений RabbitMQ */
    protected RabbitTemplate rabbitTemplate;
    /** Контейнер для прослушивания запросов к драйверу */
    protected SimpleMessageListenerContainer listenerContainer;

    /* Конфигурация */

    /** Идентификатор экземпляра драйвера */
    protected String driverId;

    /** Имя очереди для приема оповещений */
    protected String requestQueueName;
    /** Имя exchange'а для отправки оповещений */
    protected String alertsExchangeName;
    protected Queue requestQueue;

    /** Профиль драйвера */
    protected DriverProfile driverProfile;

    /** Индексированный доступ к профилям поддерживаемых типов устройств, для внутреннего использования */
    protected Map<String, DeviceProfile> supportedDeviceProfiles = new HashMap<>();

    /** Отображения профиля драйвера */
    protected List<DriverProfileView> driverProfileViews;

    /* Рабочие переменные */

    /** Монопольный режим (работает только один КА верхнего уровня) */
    private static volatile boolean exclusiveMode;

    /** Время захвата монопольного режима */
    private static volatile long exclusiveModeAcquiredTimeMs;

    /** Время последней выполненной итерации, в мс. */
    protected volatile long lastCurrentTimeMs;

    /**
     * Времена последней активности каждого КА, взятые на последней итерации.
     * Изменение содержимого списка не допустимо.
     */
    protected volatile List<Long> lastSMActivityTimesMs;

    /** Модуль обновления лицензии */
    private LicenseChecker licenseChecker;

    /** Флаг необходимости применить новый проект */
    protected boolean needSetNewProject = false;

    /** Идентификатор проекта, принимаемого в работу */
    protected String inServiceProjectIdToSet;

    /** Дерево обслуживаемых устройств */
    protected List<ActiveDevice> inServiceDevicesToSet;

    /** Список обслуживаемых зон */
    protected List<ActiveRegion> inServiceRegionsToSet;

    /** Список обслуживаемых сценариев */
    protected List<ActiveScenario> inServiceScenariosToSet;

    /** Список обслуживаемых виртуальных состояний */
    protected List<VirtualState> inServiceVirtualStatesToSet;

    /** Список первичных задач для выполнения */
    protected List<Issue> inServiceIssuesToSet;

    /** Список обслуживаемых сотрудников */
    protected List<Employee> inServiceEmployeesToSet;

    /** Список обслуживаемых ключей доступа */
    protected List<AccessKey> inServiceAccessKeysToSet;

    /** Список обслуживаемых графиков работ */
    protected List<WorkSchedule> inServiceWorkSchedulesToSet;

    /** Список конечных автоматов */
    protected List<StateMachine> stateMachines = new ArrayList<>();

    /** Cписок конечных автоматов, которые должны быть удалены после вызова одного process */
    protected List<StateMachine> stateMachinesToRemove = new ArrayList<>();

    /* Карты основных сущностей проекта по их идентификаторам */
    protected Map<String, ActiveDevice> allDevicesByIds;
    protected Map<String, ActiveRegion> allRegionsByIds;
    protected Map<String, VirtualState> allVirtualStatesByIds;
    protected Map<String, ActiveScenario> allScenariosByIds;
    protected Map<String, ScenarioTriggerType> triggerTypesByIds;
    protected Map<String, ScenarioActionType> actionTypesByIds;

    /* Карты сущностей СКУД проекта по их идентификаторам */
    protected Map<String, Employee> allEmployeesByIds;
    protected Map<String, AccessKey> allAccessKeysByIds;
    protected Map<String, WorkSchedule> allWorkSchedulesByIds;

    /* Все приборы проекта */
    protected List<ActiveDevice> allControlDevices;

    /* Карты списков сущностей по идентификаторам приборов, которым они принадлежат */
    protected Map<String, List<ActiveDevice>> devicesByControlDeviceIds;
    protected Map<String, List<ActiveRegion>> regionsByControlDeviceIds;
    protected Map<String, List<ActiveScenario>> scenariosByControlDeviceIds;
    /* Совмещенная карта - как собственных, так и внешних виртуальных состояний */
    protected Map<String, List<VirtualState>> virtualStatesByControlDeviceIds;

    /* Карты списков СКУД-сущностей по идентификаторам приборов, которым они принадлежат */
    protected Map<String, List<Employee>> employeesByControlDeviceIds;
    protected Map<String, List<AccessKey>> accessKeysByControlDeviceIds;
    protected Map<String, List<WorkSchedule>> workSchedulesByControlDeviceIds;
    /* Карта наборов ключей доступа по идентификаторам сотрудников */
    protected Map<String, List<AccessKey>> accessKeysByEmployeeIds;

    /* Карты внешних сущностей по идентификаторам приборов, которые на них ссылаются как на внешние */
    protected Map<String, List<ActiveScenario>> externalScenariosByControlDeviceIds;
    protected Map<String, List<ActiveDevice>> externalExecutiveDevicesByControlDeviceIds;

    /* Обратные карты: идентификатор сущности -> прибор или список приборов, которым она принадлежит */
    protected Map<String, ActiveDevice> controlDevicesByChildDeviceIds;
    protected Map<String, List<ActiveDevice>> controlDevicesByRegionIds;
    protected Map<String, Set<String>> controlDeviceIdsByScenarioIds;

    /* Результаты валидации проекта */

    /** Статус валидации проекта */
    protected ActiveProjectValidateStatus validateStatus;
    /** Общие сообщения валидации проекта */
    protected List<ActivationValidateMessage> commonValidateMessages;
    /** Сообщения валидации базы данных СКУД */
    protected List<ActivationValidateMessage> skudValidateMessages;

    /**
     * Проверить проект на наличие общих ошибок и создать набор конечных автоматов для взаимодействия с устройствами. 
     * 
     * @param validateMessages коллекция сообщений валидации для сохранения в ней найденных ошибок
     * @param skudValidateMessages коллекция сообщений валидации для сохранения в ней ошибок СКУД
     * @param stateMachines коллекция конечных автоматов (на входе должна быть пустая)
     * @return true если проект не содержал критичных ошибок, иначе false (и коллекция конечных автоматов остается пустой)
     */
    abstract protected boolean fillMapsAndValidateProjectAndFetchStateMachines(List<ActivationValidateMessage> validateMessages,
                                                                               List<ActivationValidateMessage> skudValidateMessages,
                                                                               List<StateMachine> stateMachines);

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public DriverRequestHandler getDriverRequestHandler() {
        return driverRequestHandler;
    }

    public void setDriverRequestHandler(DriverRequestHandler driverRequestHandler) {
        this.driverRequestHandler = driverRequestHandler;
    }

    public DriverAlertSender getDriverAlertSender() {
        return driverAlertSender;
    }

    public void setDriverAlertSender(DriverAlertSender driverAlertSender) {
        this.driverAlertSender = driverAlertSender;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public SimpleMessageListenerContainer getListenerContainer() {
        return listenerContainer;
    }

    public void setListenerContainer(SimpleMessageListenerContainer listenerContainer) {
        this.listenerContainer = listenerContainer;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getRequestQueueName() {
        return requestQueueName;
    }

    public void setRequestQueueName(String requestQueueName) {
        this.requestQueueName = requestQueueName;
    }

    public String getAlertsExchangeName() {
        return alertsExchangeName;
    }

    public void setAlertsExchangeName(String alertsExchangeName) {
        this.alertsExchangeName = alertsExchangeName;
    }

    public Driver(@NotNull Logger logger) {
        super();
        this.logger = logger;
    }

    public void setInServiceProject(long currentTimeMs, String projectId, List<ActiveDevice> inServiceDevices,
            List<ActiveRegion> inServiceRegions, List<ActiveScenario> inServiceScenarios,
            List<VirtualState> inServiceVirtualStates, List<Issue> inServiceIssues, List<Employee> inServiceEmployees,
            List<AccessKey> inServiceAccessKeys, List<WorkSchedule> inServiceWorkSchedules) {
        needSetNewProject = true;
        inServiceProjectIdToSet = projectId;
        inServiceDevicesToSet = inServiceDevices;
        inServiceRegionsToSet = inServiceRegions;
        inServiceScenariosToSet = inServiceScenarios;
        inServiceVirtualStatesToSet = inServiceVirtualStates;
        inServiceIssuesToSet = inServiceIssues;
        inServiceEmployeesToSet = inServiceEmployees;
        inServiceAccessKeysToSet = inServiceAccessKeys;
        inServiceWorkSchedulesToSet = inServiceWorkSchedules;
    }

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
    }

    /* TODO: брать список профилей устройств из профиля драйвера (для этого надо в прифиле драйвера содержать объекты, а не идентификаторы, на что есть отдельный todo) */
    @Autowired
    public void setSupportedDeviceProfiles(List<DeviceProfile> deviceProfiles) {
        supportedDeviceProfiles.clear();
        for (DeviceProfile deviceProfile: deviceProfiles) {
            supportedDeviceProfiles.put(deviceProfile.getId(), deviceProfile);
        }
    }

    public List<DriverProfileView> getDriverProfileViews() {
        return driverProfileViews;
    }

    public void setDriverProfileViews(List<DriverProfileView> driverProfileViews) {
        this.driverProfileViews = driverProfileViews;
    }

    protected void stopAllStateMachines(long currentTimeMs) {
        for (StateMachine stateMachine : stateMachines)
            stateMachine.stop(currentTimeMs);

        stateMachinesToRemove.addAll(stateMachines);
    }

    protected void initializeRabbit() {
        requestQueue = new Queue(requestQueueName, true, false, true);
        DriverRequestHandlerWrapper handlerWrapper = new DriverRequestHandlerWrapper(logger, driverRequestHandler,
                driverId);

        listenerContainer.setQueueNames(requestQueue.getName());
        listenerContainer
                .setMessageListener(new MessageListenerAdapter(handlerWrapper, rabbitTemplate.getMessageConverter()));
    }

    @Override
    public void onStart(long currentTimeMs) {
        if (requestQueueName == null || alertsExchangeName == null || driverRequestHandler == null
                || driverAlertSender == null || rabbitTemplate == null || listenerContainer == null
                || driverId == null || licenseChecker == null) {
            throw new IllegalStateException(
                    "Required params for Driver are: requestQueueName, alertsExchangeName, driverRequestHandler, " +
                    "driverAlertSender, rabbitTemplate, listenerContainer, driverId, licenseChecker");
        } else {
            initializeRabbit();
            logger.info("Driver initialized. Id: {}. Request queue: {}. Alerts exchange: {}", driverId,
                    requestQueueName, alertsExchangeName);

            DriverRegisteredAlert alert = new DriverRegisteredAlert();
            alert.setDriverId(driverId);
            alert.setRequestQueue(requestQueueName);
            alert.setAlertsExchange(alertsExchangeName);
            driverAlertSender.send(alert);
        }
    }

    @Override
    public void onStop(long currentTimeMs) {
        stopAllStateMachines(currentTimeMs);
    }

    @Override
    public void process(long currentTimeMs) {
        if (licenseChecker.licenseUpdated()) {
            LicenseRestrictionsUpdated alert = new LicenseRestrictionsUpdated();
            alert.setLicenseRestrictions(licenseChecker.readLicenseRestrictions());
            driverAlertSender.send(alert);
        }

        if (needSetNewProject) {
            needSetNewProject = false;

            stopAllStateMachines(currentTimeMs);
            stateMachines.clear();

            initDevicesHash();
            initRegionsHash();
            initVirtualStatesHash();
            initScenariosHash();
            initTriggerTypesHash();
            initActionTypesHash();
            initEmployeesHash();
            initAccessKeysHash();
            initWorkSchedulesHash();
            allControlDevices = new ArrayList<>();
            devicesByControlDeviceIds = new HashMap<>();
            controlDevicesByChildDeviceIds = new HashMap<>();
            regionsByControlDeviceIds = new HashMap<>();
            controlDevicesByRegionIds = new HashMap<>();
            virtualStatesByControlDeviceIds = new HashMap<>();
            scenariosByControlDeviceIds = new HashMap<>();
            externalScenariosByControlDeviceIds = new HashMap<>();
            controlDeviceIdsByScenarioIds = new HashMap<>();
            externalExecutiveDevicesByControlDeviceIds = new HashMap<>();

            /*
             * Результат валидации проекта может зависеть как от наличия в нем
             * общих ошибок, так и от ошибок по запуску каждого отдельного
             * конечного автомата
             */
            validateStatus = new ActiveProjectValidateStatus();
            validateStatus.setProjectId(inServiceProjectIdToSet);
            commonValidateMessages = new ArrayList<>();
            skudValidateMessages = new ArrayList<>();

            boolean projectValid = fillMapsAndValidateProjectAndFetchStateMachines(commonValidateMessages,
                    skudValidateMessages, stateMachines);
            /*
             * Запускаем конечные автоматы с запоминанием общего результата
             */
            for (StateMachine stateMachine : stateMachines) {
                projectValid &= stateMachine.start(currentTimeMs, commonValidateMessages);
            }
            if (projectValid) {
                /* Запускаем имеющиеся задачи */
                for (Issue inServiceIssue : inServiceIssuesToSet)
                    startIssue(inServiceIssue);
            } else {
                /*
                 * Сразу останавливаем все конечные автоматы, если есть ошибка
                 * запуска хотя бы в одном или были общие ошибки валидации
                 * проекта
                 */
                stopAllStateMachines(currentTimeMs);
                stateMachines.clear();
            }
            validateStatus.setValidateSucceded(projectValid && stateMachines.size() > 0);

            validateStatus.setValidateMessages(new ArrayList<>(commonValidateMessages));
            validateStatus.addValidateMessages(skudValidateMessages);
            driverAlertSender.send(validateStatus);
        }

        /*
         * Удаляемые конечные автоматы должны выполнить еще одну итерацию, чтобы
         * завершить остановку
         */
        for (StateMachine stateMachine : stateMachinesToRemove)
            stateMachine.step(currentTimeMs);
        stateMachinesToRemove.clear();

        List<Long> lastSMActivityTimesMs = new ArrayList<>();
        for (StateMachine stateMachine : stateMachines) {
            stateMachine.step(currentTimeMs);
            lastSMActivityTimesMs.add(stateMachine.getLastActivityTime());
        }

        lastCurrentTimeMs = currentTimeMs;
        this.lastSMActivityTimesMs = lastSMActivityTimesMs;
    }

    private void initActionTypesHash() {
        actionTypesByIds = new HashMap<>();
        for (ScenarioActionType actionType : driverProfile.getScenarioActionTypes()) {
            actionTypesByIds.put(actionType.getId(), actionType);
        }
    }

    private void initTriggerTypesHash() {
        triggerTypesByIds = new HashMap<>();
        for (ScenarioTriggerType triggerType : driverProfile.getScenarioTriggerTypes()) {
            triggerTypesByIds.put(triggerType.getId(), triggerType);
        }
    }

    private void initScenariosHash() {
        allScenariosByIds = new HashMap<>();
        for (ActiveScenario scenario : inServiceScenariosToSet) {
            allScenariosByIds.put(scenario.getId(), scenario);
        }
    }

    private void initRegionsHash() {
        /* Сформируем карту зон по идентификаторам для дальнейшего более быстрого разбора */
        allRegionsByIds = new HashMap<>();
        for (ActiveRegion region : inServiceRegionsToSet) {
            allRegionsByIds.put(region.getId(), region);
        }
    }

    private void initVirtualStatesHash() {
        /* Формируем общую карту идентификатор ВС -> ВС */
        allVirtualStatesByIds = new HashMap<>();
        for (VirtualState virtualState : inServiceVirtualStatesToSet) {
            allVirtualStatesByIds.put(virtualState.getId(), virtualState);
        }
    }

    protected void initDevicesHash() {
        /* Формируем общую карту идентификатор устройства -> устройство */
        allDevicesByIds = new HashMap<>();
        for (ActiveDevice device : inServiceDevicesToSet) {
            allDevicesByIds.put(device.getId(), device);
        }
    }

    private void initEmployeesHash() {
        /* Сформируем карту зон по идентификаторам для дальнейшего более быстрого разбора */
        allEmployeesByIds= new HashMap<>();
        for (Employee employee : inServiceEmployeesToSet) {
            allEmployeesByIds.put(employee.getId(), employee);
        }
    }

    private void initAccessKeysHash() {
        /* Формируем общую карту идентификатор ВС -> ВС */
        allAccessKeysByIds = new HashMap<>();
        for (AccessKey accessKey: inServiceAccessKeysToSet) {
            allAccessKeysByIds.put(accessKey.getId(), accessKey);
        }
    }

    private void initWorkSchedulesHash() {
        /* Формируем общую карту идентификатор устройства -> устройство */
        allWorkSchedulesByIds = new HashMap<>();
        for (WorkSchedule workSchedule: inServiceWorkSchedulesToSet) {
            allWorkSchedulesByIds.put(workSchedule.getId(), workSchedule);
        }
    }

    /**
     * Методы, которые конкретный драйвер может переопределить для реакции на
     * запросы.
     */

    /**
     * Запрос на создание задач(и)
     * 
     * @param issue
     *            сущность задания для постановки в очередь
     * @return ответ с заполненным error или с актуальными созданными задачами
     */
    public CreateIssueDriverResponse createIssue(Issue issue) {
        CreateIssueDriverResponse response = new CreateIssueDriverResponse();
        response.setError(ErrorType.METOD_NOT_SUPPORTED);
        return response;
    }

    /**
     * Запуск ранее созданной (полученной в запросе с установкой активного
     * проекта) задачи
     * 
     * @param issue
     *            задача
     */
    public void startIssue(Issue issue) {
    }

    /**
     * Запрос на изменение конфигурации устройства в БД панели
     *
     */
    public SetDriverDeviceConfigResponse setDeviceConfig(SetDriverDeviceConfigRequest request) {
        SetDriverDeviceConfigResponse response = new SetDriverDeviceConfigResponse();
        response.setError(ErrorType.METOD_NOT_SUPPORTED);
        return response;
    }

    /**
     * Запрос на удаление задачи
     *
     */
    public DeleteTaskDriverResponse deleteTask(DeleteTaskDriverRequest request) {
        DeleteTaskDriverResponse response = new DeleteTaskDriverResponse();
        response.setError(ErrorType.METOD_NOT_SUPPORTED);
        return response;
    }

    /**
     * Запрос на запрет/разрешение выполнения задачи
     *
     */
    public PauseTaskDriverResponse pauseTask(PauseTaskDriverRequest request) {
        PauseTaskDriverResponse response = new PauseTaskDriverResponse();
        response.setError(ErrorType.METOD_NOT_SUPPORTED);
        return response;
    }

    public LicenseReader getLicenseReader() {
        return licenseChecker;
    }

    public void setLicenseChecker(LicenseChecker licenseChecker) {
        this.licenseChecker = licenseChecker;
    }

    /**
     * Захват монопольного режима. Блокировка не требуется, предполагается, что
     * захват и освобождение всегда вызываются из одного потока.
     */
    public static boolean tryAcquireExclusiveMode(long currentTimeMs) {
        if (!exclusiveMode) {
            exclusiveMode = true;
            exclusiveModeAcquiredTimeMs = currentTimeMs;
            return true;
        }
        return false;
    }

    /**
     * Освобождение монопольного режима. Блокировка не требуется,
     * предполагается, что захват и освобождение всегда вызываются из одного
     * потока.
     */
    public static void freeExclusiveMode() {
        exclusiveMode = false;
    }

    /**
     * Статус монопольного режима. Блокировка не требуется, предполагается, что
     * захват и освобождение всегда вызываются из одного потока.
     */
    public static boolean isExclusiveMode() {
        return exclusiveMode;
    }

    /** Время захвата монопольного режима */
    public static long getExclusiveModeAcquiredTimeMs() {
        return exclusiveModeAcquiredTimeMs;
    }

    public long getLastCurrentTimeMs() {
        return lastCurrentTimeMs;
    }

    public List<Long> getLastSMActivityTimesMs() {
        return lastSMActivityTimesMs;
    }

}
