package ru.rubezh.firesec.nt.plugin.driver;

import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;

public interface StateMachine {
    /**
     * Выполнить запуск конечного автомата.
     * 
     * @param currentTimeMs
     *            текущее время в мс.
     * @param validateMessages
     *            возвращаемые сообщения валидации
     * @return true, если конечный автомат запущен успешно
     */
    public boolean start(long currentTimeMs, List<ActivationValidateMessage> validateMessages);

    /**
     * Выполнить шаг конечного автомата.
     * <p>
     * Данный интерфейс конечного автомата не накладывает жестких требований на постоянство временного шага, поэтому текущее время передается параметром.
     * </p>
     * @param currentTimeMs текущее время в мс.
     */
    public void step(long currentTimeMs);
    
    /**
     * Выполнить остановку конечного автомата.
     * @param currentTimeMs
     */
    public void stop(long currentTimeMs);

    /**
     * Время последней операции
     * @return время последней полезной операции, мс
     */
    public long getLastActivityTime();
}
