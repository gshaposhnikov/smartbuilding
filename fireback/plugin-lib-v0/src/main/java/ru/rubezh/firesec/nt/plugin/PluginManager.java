package ru.rubezh.firesec.nt.plugin;

import java.util.List;

public interface PluginManager {

    public void addPlugin(Plugin plugin);
    public void setPlugins(List<Plugin> plugins);
    public void removePlugin(Plugin plugin);
    public void removeAllPlugins();
    public void doPluginsExecution();
}
