package ru.rubezh.firesec.nt.plugin.driver;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class DriverApplicationArgs {
    private String driverId;
    private String requestQueue;
    private String alertsExchange;
    
    private static final Options options = new Options();
    static {
        options.addRequiredOption("i", "id", true, "Driver id");
        options.addRequiredOption("r", "request-queue", true, "Driver request queue name");
        options.addRequiredOption("a", "alerts-exchange", true, "Driver alerts exchange name");
    }
    
    public static void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("driver-amod1", options, true);
    }

    public DriverApplicationArgs(String[] args) throws ParseException {
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = commandLineParser.parse(options, args);
        if (commandLine.hasOption("id"))
            driverId = commandLine.getOptionValue("id");
        if (commandLine.hasOption("request-queue"))
            requestQueue = commandLine.getOptionValue("request-queue");
        if (commandLine.hasOption("alerts-exchange"))
            alertsExchange = commandLine.getOptionValue("alerts-exchange");
    }
    
    public DriverApplicationArgs(DriverApplicationArgs other) {
        this.driverId = other.driverId;
        this.requestQueue = other.requestQueue;
        this.alertsExchange = other.alertsExchange;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getRequestQueue() {
        return requestQueue;
    }

    public String getAlertsExchange() {
        return alertsExchange;
    }

}
