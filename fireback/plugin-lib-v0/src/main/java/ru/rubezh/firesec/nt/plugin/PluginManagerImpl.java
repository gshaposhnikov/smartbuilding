package ru.rubezh.firesec.nt.plugin;

import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class PluginManagerImpl implements PluginManager {

    private List<Plugin> plugins = new ArrayList<>();
    private Logger logger;

    @Override
    public void addPlugin(@NotNull Plugin plugin) {
        plugin.onStart(System.currentTimeMillis());
        plugins.add(plugin);
    }

    @Override
    public void setPlugins(@NotNull List<Plugin> newPlugins) {
        removeAllPlugins();
        for (Plugin plugin : newPlugins)
            addPlugin(plugin);
    }

    @Override
    public void removePlugin(@NotNull Plugin plugin) {
        plugins.remove(plugin);
        plugin.onStop(System.currentTimeMillis());
    }

    @Override
    public void removeAllPlugins() {
        long currentTimeMs = System.currentTimeMillis();
        for (Plugin plugin : plugins)
            plugin.onStop(currentTimeMs);
        plugins.clear();
    }

    @Override
    public void doPluginsExecution() {
        long currentTimeMs = System.currentTimeMillis();
        for (Plugin plugin : plugins)
            plugin.process(currentTimeMs);
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
