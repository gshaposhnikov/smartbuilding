package ru.rubezh.firesec.nt.plugin.driver;

import org.apache.commons.cli.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class DriverApplication implements ApplicationListener<ContextClosedEvent> {

    ApplicationContext applicationContext;
    DriverApplicationArgs applicationArgs;

    @Autowired
    ThreadPoolTaskScheduler scheduler;

    String[] args;

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        scheduler.shutdown();
    }

    public DriverApplication(String[] args) {
        this.args = args;
    }

    private void run() {
        try {
            applicationArgs = new DriverApplicationArgs(args);
        } catch (ParseException parseException) {
            System.out.println(parseException.getMessage());
            DriverApplicationArgs.printHelp();
            return;
        }

        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        BeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(DriverApplicationArgs.class)
                .addConstructorArgValue(applicationArgs).getBeanDefinition();
        beanFactory.registerBeanDefinition("applicationArgs", beanDefinition);
        GenericApplicationContext genericApplicationContext = new GenericApplicationContext(beanFactory);
        genericApplicationContext.refresh();
        applicationContext = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" },
                genericApplicationContext);
    }

    public static void main(String[] args) {
        new DriverApplication(args).run();
    }
}
