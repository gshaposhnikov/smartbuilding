package ru.rubezh.firesec.nt.plugin.driver.clock;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MultiClientClock implements Clock {

    private class MultiClientClockTimer {
        long startedMs;
        int valueMs;
        TimerHandler handler;
        boolean isRepeatable;

        public MultiClientClockTimer(long startedMs, int valueMs, TimerHandler handler, boolean isRepeatable) {
            super();
            this.startedMs = startedMs;
            this.valueMs = valueMs;
            this.handler = handler;
            this.isRepeatable = isRepeatable;
        }
    }

    private Map<String, MultiClientClockTimer> timers = new HashMap<>();

    @Override
    public void setTimer(long currentTimeMs, @Min(1) int timerValueMs, @NotNull TimerHandler timerHanler) {
        setTimer("", currentTimeMs, timerValueMs, timerHanler, false);
    }

    @Override
    public void setTimer(String timerName, long currentTimeMs, @Min(1) int timerValueMs,
            @NotNull TimerHandler timerHandler, boolean isRepeatable) {
        resetTimer(timerName);
        MultiClientClockTimer timer = new MultiClientClockTimer(currentTimeMs, timerValueMs, timerHandler,
                isRepeatable);
        timers.put(timerName, timer);
    }

    @Override
    public void process(long currentTimeMs) {
        String elapsedTimer = null;
        for (String timerName : timers.keySet()) {
            MultiClientClockTimer timer = timers.get(timerName);
            if (timer.startedMs + timer.valueMs <= currentTimeMs) {
                elapsedTimer = timerName;
                break;
            }
        }
        if (elapsedTimer != null) {
            MultiClientClockTimer timer = timers.get(elapsedTimer);
            if (!timer.isRepeatable) {
                timers.remove(elapsedTimer);
            } else {
                timer.startedMs = currentTimeMs;
            }
            timer.handler.onTime(currentTimeMs);
        }
    }

    @Override
    public void resetTimer(String timerName) {
        if (timers.containsKey(timerName)) {
            timers.remove(timerName);
        }
    }

    @Override
    public void resetAllTimers() {
        timers.clear();
    }

    @Override
    public void resetAllTimersExcludingOne(String excludingTimerName) {
        Map<String, MultiClientClockTimer> newTimers = new HashMap<>();
        if (timers.containsKey(excludingTimerName)) {
            newTimers.put(excludingTimerName, timers.get(excludingTimerName));
        }
        timers = newTimers;
    }
}
