package ru.rubezh.firesec.nt.plugin.driver;

import org.apache.logging.log4j.Logger;
import ru.rubezh.firesec.nt.amqp.message.request.*;
import ru.rubezh.firesec.nt.amqp.message.response.*;

import javax.validation.constraints.NotNull;

public class DriverRequestHandlerImpl implements DriverRequestHandler {

    private Logger logger;
    private Driver driver;

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Driver getDriverInstance() {
        return driver;
    }

    public void setDriverInstance(Driver driver) {
        this.driver = driver;
    }

    public DriverRequestHandlerImpl(@NotNull Logger logger) {
        this.logger = logger;
    }

    @Override
    public PingResponse handleMessage(PingRequest pingRequest) {

        PingResponse pingResponse = new PingResponse();

        pingResponse.setCurrentTimeMs(driver.getLastCurrentTimeMs());
        pingResponse.setLastSMActivityTimes(driver.getLastSMActivityTimesMs());

        pingResponse.setMemoryUsage(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
        pingResponse.setExclusiveActivityMode(Driver.isExclusiveMode());
        pingResponse.setLastExclusiveModeAcquireTimeMs(Driver.getExclusiveModeAcquiredTimeMs());

        logger.info("Got ping request. Ping response: {}", pingResponse.toString());
        return pingResponse;
    }

    @Override
    public void handleMessage(ShutdownRequest shutdownRequest) {
        logger.info("Got shutdown request. Exiting");
        System.exit(0);
    }

    @Override
    public SetActiveProjectResponse handleMessage(SetActiveProjectRequest setActiveProjectRequest) {
        logger.info("Got set active project request");
        driver.setInServiceProject(System.currentTimeMillis(), setActiveProjectRequest.getProjectId(),
                setActiveProjectRequest.getDevices(), setActiveProjectRequest.getRegions(),
                setActiveProjectRequest.getScenarios(), setActiveProjectRequest.getVirtualStates(),
                setActiveProjectRequest.getIssues(), setActiveProjectRequest.getEmployees(),
                setActiveProjectRequest.getAccessKeys(), setActiveProjectRequest.getWorkSchedules());
        return new SetActiveProjectResponse();
    }

    @Override
    public GetDriverProfileViewsResponse handleMessage(GetDriverProfileViewsRequest getDriverProfileViewsRequest) {
        logger.info("Got get driver profile request");
        GetDriverProfileViewsResponse response = new GetDriverProfileViewsResponse();
        response.setDriverProfileViews(driver.getDriverProfileViews());
        return response;
    }

    @Override
    public CreateIssueDriverResponse handleMessage(CreateIssueDriverRequest request) {
        return driver.createIssue(request.getIssue());
    }

    @Override
    public SetDriverDeviceConfigResponse handleMessage(SetDriverDeviceConfigRequest setDriverDeviceConfigRequest) {
        logger.info("Got update device config request (deviceId={}}", setDriverDeviceConfigRequest.getDeviceId());
        return driver.setDeviceConfig(setDriverDeviceConfigRequest);
    }

    @Override
    public DeleteTaskDriverResponse handleMessage(DeleteTaskDriverRequest request) {
        logger.info("Got delete task request");
        return driver.deleteTask(request);
    }

    @Override
    public PauseTaskDriverResponse handleMessage(PauseTaskDriverRequest request) {
        logger.info("Got pause task request");
        return driver.pauseTask(request);
    }

    @Override
    public GetLicenceRestrictionsResponse handleMessage(GetLicenceRestrictionsRequest request) {
        logger.info("Got licence restrictions request");
        GetLicenceRestrictionsResponse response = new GetLicenceRestrictionsResponse();
        response.setLicenseRestrictions(driver.getLicenseReader().readLicenseRestrictions());
        return response;
    }

}
