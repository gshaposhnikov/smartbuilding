package ru.rubezh.firesec.nt.plugin.driver;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.request.*;
import ru.rubezh.firesec.nt.amqp.message.response.AbstractDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;

//TODO: сделать враппер аспектом
public class DriverRequestHandlerWrapper {

    private Logger logger;
    private String driverId;
    private DriverRequestHandler wrappedHandler;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public DriverRequestHandler getWrappedHandler() {
        return wrappedHandler;
    }

    public void setWrappedHandler(DriverRequestHandler wrappedHandler) {
        this.wrappedHandler = wrappedHandler;
    }

    public DriverRequestHandlerWrapper(Logger logger, DriverRequestHandler wrappedHandler, String driverId) {
        this.logger = logger;
        this.wrappedHandler = wrappedHandler;
        this.driverId = driverId;
    }

    private boolean checkRequest(AbstractDriverRequest abstractDriverRequest) {
        if (abstractDriverRequest.getDriverId() != null && abstractDriverRequest.getDriverId().equals(driverId)) {
            return true;
        } else {
            logger.warn("Bad RMI request driverId: \"" + abstractDriverRequest.getDriverId() + "\" (expected: \"" + driverId + "\")");
            return false;
        }
    }

    private void prepareResponse(AbstractDriverResponse abstractDriverResponse) {
        if (abstractDriverResponse != null) {
            abstractDriverResponse.setDriverId(driverId);
        }
    }

    public AbstractDriverResponse handleMessage(PingRequest pingRequest) {
        AbstractDriverResponse response = null;
        if (checkRequest(pingRequest)) {
            response = wrappedHandler.handleMessage(pingRequest);
            if (response == null)
                response = new ErrorResponse(ErrorType.INTERNAL_ERROR, "Request handler returned null response");
        }
        else
            response = new ErrorResponse(ErrorType.INTERNAL_ERROR, "Error while checking request");
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(SetActiveProjectRequest setActiveProjectRequest) {
        AbstractDriverResponse response = null;
        if (checkRequest(setActiveProjectRequest)) {
            response = wrappedHandler.handleMessage(setActiveProjectRequest);
            if (response == null)
                response = new ErrorResponse(ErrorType.INTERNAL_ERROR, "Request handler returned null response");
        }
        else
            response = new ErrorResponse(ErrorType.INTERNAL_ERROR, "Error while checking request");
        prepareResponse(response);
        return response;
    }

    public void handleMessage(ShutdownRequest shutdownRequest) {
        if (checkRequest(shutdownRequest)) {
            wrappedHandler.handleMessage(shutdownRequest);
        }
    }

    public AbstractDriverResponse handleMessage(GetDriverProfileViewsRequest getDriverProfileViewsRequest) {
        AbstractDriverResponse response = null;
        if (checkRequest(getDriverProfileViewsRequest)) {
            response = wrappedHandler.handleMessage(getDriverProfileViewsRequest);
        }
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(CreateIssueDriverRequest request) {
        AbstractDriverResponse response = null;
        if (checkRequest(request)) {
            response = wrappedHandler.handleMessage(request);
        }
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(SetDriverDeviceConfigRequest request) {
        AbstractDriverResponse response = null;
        if (checkRequest(request)) {
            response = wrappedHandler.handleMessage(request);
        }
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(DeleteTaskDriverRequest request) {
        AbstractDriverResponse response = null;
        if (checkRequest(request)) {
            response = wrappedHandler.handleMessage(request);
        }
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(PauseTaskDriverRequest request) {
        AbstractDriverResponse response = null;
        if (checkRequest(request)) {
            response = wrappedHandler.handleMessage(request);
        }
        prepareResponse(response);
        return response;
    }

    public AbstractDriverResponse handleMessage(GetLicenceRestrictionsRequest request) {
        AbstractDriverResponse response = null;
        if (checkRequest(request)) {
            response = wrappedHandler.handleMessage(request);
        }
        prepareResponse(response);
        return response;
    }

}
