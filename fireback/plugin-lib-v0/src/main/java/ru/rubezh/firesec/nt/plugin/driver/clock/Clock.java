package ru.rubezh.firesec.nt.plugin.driver.clock;

public interface Clock {
    /**
     * Установить безымянный одиночный таймер. Аналогично вызову
     * {@link #setTimer(String, long, int, TimerHandler, boolean)} с {@code timerName=""} и {@code isRepeatable=false}.
     *
     * @param currentTimeMs текущее время в мс.
     * @param timerValueMs время в мс, через которое начиная с текущего должен сработать устанавливаемый таймер.
     * @param timerHanler обработчик, который будет вызван при срабатывании таймера.
     */
    public void setTimer(long currentTimeMs, int timerValueMs, TimerHandler timerHanler);

    /**
     * Установить таймер.
     *
     * @param timerName имя таймера
     * @param currentTimeMs текущее время в мс.
     * @param timerValueMs время в мс, через которое начиная с текущего должен сработать устанавливаемый таймер.
     * @param timerHandler обработчик, который будет вызван при срабатывании таймера.
     * @param isRepeatable должен ли таймер повторяться
     */
    public void setTimer(String timerName, long currentTimeMs, int timerValueMs, TimerHandler timerHandler, boolean isRepeatable);

    /**
     * Выполнение шага, осуществляющего проверку сработавших таймеров.
     * @param currentTimeMs текщущее время в мс.
     */
    public void process(long currentTimeMs);

    /**
     * Сбросить один таймер.
     * @param timerName Имя таймера
     */
    public void resetTimer(String timerName);

    /**
     * Сбросить все таймера.
     */
    public void resetAllTimers();

    /**
     * Сбросить все таймера кроме одного
     */
    public void resetAllTimersExcludingOne(String excludingTimerName);
}
