package ru.rubezh.firesec.nt.plugin.driver.license;

import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;

/**
 * Считыватель лицензии
 */
public interface LicenseReader {

    /**
     * Прочитать лицензионные ограничения
     */
    public LicenseRestrictions readLicenseRestrictions();

}
