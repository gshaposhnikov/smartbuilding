package ru.rubezh.firesec.nt.plugin.driver.clock;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Класс одиночного таймера.
 * <p>
 * В единицу времени может быть запущен только один таймер.
 * Т.е. при запуске очередного таймера, если уже был запущен какой-либо таймер, то он сбрасывается установкой нового.
 * Параметры timerName и isRepeatable игнорируются.
 * </p>
 * @author Антон Васильев
 *
 */
public class SingleClientClock implements Clock {

    long timerStartedMs;
    int timerValueMs;
    TimerHandler timerHandler;

    @Override
    public void setTimer(String timerName, long currentTimeMs, @Min(1) int timerValueMs, @NotNull TimerHandler timerHandler,
            boolean isRepeatable) {
        timerStartedMs = currentTimeMs;
        this.timerValueMs = timerValueMs;
        this.timerHandler = timerHandler;
    }

    @Override
    public void setTimer(long currentTimeMs, @Min(1) int timerValueMs, @NotNull TimerHandler timerHandler) {
        this.setTimer(null, currentTimeMs, timerValueMs, timerHandler, false);
    }

    @Override
    public void process(long currentTimeMs) {
        if (timerHandler != null && (timerStartedMs + timerValueMs) <= currentTimeMs) {
            TimerHandler tmpTimerHandler = timerHandler;
            timerHandler = null;
            tmpTimerHandler.onTime(currentTimeMs);
        }
    }

    @Override
    public void resetTimer(String timerName) {
        timerHandler = null;
    }

    @Override
    public void resetAllTimers() {
        timerHandler = null;
    }

    @Override
    public void resetAllTimersExcludingOne(String excludingTimerName) {
        /*
         * Данная реализация "часов" не поддерживает именования таймеров,
         * поэтому данный метод не отличается от двух других методов сброса
         */
        timerHandler = null;
    }

    @Override
    public String toString() {
        return "SingleClientClock (startedMs=" + timerStartedMs + ", valueMs=" + timerValueMs + ", handler=" + timerHandler + ")";
    }
}
