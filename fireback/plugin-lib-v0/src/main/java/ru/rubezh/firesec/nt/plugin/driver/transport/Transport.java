package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.io.IOException;

public interface Transport <R> {
    public interface ConnectHandler {
        /**
         * Интерфейс callback-обработчика для получения результата о выполнении
         * процедуры подключения
         * 
         * @param currentTimeMs
         *            текущее время в мс
         * @param isSuccess
         *            если подключение выполнено успешно - то true, иначе -
         *            false
         */
        public void onConnected(long currentTimeMs, boolean isSuccess);
    }

    public interface SendHandler {
        /**
         * Интерфейс callback-обработчика для получения уведомления об успешном
         * выполнении отправки данных
         * 
         * @param currentTimeMs
         *            время когда была фактически выполнена отправка данных
         * @param sequenceNo
         *            номер последовательности (идентификатор отправленного
         *            пакета данных, который должен совпадать с идентификатором,
         *            возвращенным запросом на отправку)
         */
        public void onSended(long currentTimeMs, int sequenceNo);
    }

    public interface ReceiveHandler<R> {
        /**
         * Интерфейс callback-обработчика, вызываемый при завершении процедуры
         * получения запрошенных данных
         * 
         * @param currentTimeMs
         *            время в мс - когда был фактически выполнен прием данных
         * @param sequenceNo
         *            номер последовательности (идентификатор принятого пакета
         *            данных, который должен совпадать с идентификатором,
         *            заданным в запросе на получение)
         * @param rawData
         *            принятые данные (не декодированные)
         */
        public void onReceived(long currentTimeMs, int sequenceNo, R rawData);
    }

    public interface SendReceiveFailedHandler {
        /**
         * Интерфейс callback-обработчика, вызываемый в при возникновении ошибки
         * приема/передачи данных по ранее полученному запросу
         * 
         * @param currentTimeMs
         *            текущее время в мс
         * @param sequenceNo
         *            номер последовательности (идентификатор пакета данных,
         *            который не удалось отправить или принять)
         */
        public void onFailed(long currentTimeMs, int sequenceNo);
    }

    public interface DisconnectHandler {
        /**
         * Интерфейс callback-обработчика, вызываемый при потере связи
         * 
         * @param currentTimeMs
         *            текущее время в мс
         */
        public void onDisconnected(long currentTimeMs);
    }

    /**
     * Выполнить подключение
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @param connectHandler
     *            callback-обработчик для получения результата
     * @param disconnectHandler
     *            callback-обработчик для получения события потери связи
     */
    public void connect(long currentTimeMs, ConnectHandler connectHandler, DisconnectHandler disconnectHandler);

    /**
     * Выполнить отправку данных
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @param rawData
     *            данные для отправки
     * @param sendHandler
     *            callback-обработчик, вызываемый в случае успешного выполнения
     *            отправки
     * @param failHandler
     *            callback-обработчик, вызываемый в случае ошибки выполнения
     *            отправки
     * @return номер последовательности (назначенный идентификатор отправляемого
     *         пакета данных)
     */
    public int send(long currentTimeMs, R rawData, SendHandler sendHandler, SendReceiveFailedHandler failHandler);

    /**
     * Запросить получение данных
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @param sequenceNo
     *            номер последовательности (идентификатор пакета запрашиваемых
     *            на получение данных)
     * @param receiveHandler
     *            callback-обработчик, вызываемый в случае успешного получения
     *            запрошенных данных
     * @param failHandler
     *            callback-обработчик, вызываемый в случае ошибки получения
     *            запрошенных данных
     */
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<R> receiveHandler,
            SendReceiveFailedHandler failHandler);

    /**
     * Запросить получение данных
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @param sequenceNo
     *            номер последовательности (идентификатор пакета запрашиваемых
     *            на получение данных)
     * @param receiveHandler
     *            callback-обработчик, вызываемый в случае успешного получения
     *            запрошенных данных
     * @param failHandler
     *            callback-обработчик, вызываемый в случае ошибки получения
     *            запрошенных данных
     * @param timeoutHandler
     *            callback-обработчик, вызываемый в случае таймаута получения
     *            запрошенных данных
     */
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<R> receiveHandler,
            SendReceiveFailedHandler failHandler, SendReceiveFailedHandler timeoutHandler);

    /**
     * Выполнить отключение
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @throws IOException
     *             ошибка ввода-вывода
     */
    public void disconnect(long currentTimeMs) throws IOException;

    /**
     * Выполнение одной итерации рабочей процедуры
     * 
     * @param currentTimeMs
     *            текущее время в мс
     * @throws IOException
     *             ошибка ввода-вывода
     */
    public void process(long currentTimeMs)  throws IOException;

    /**
     * Время последней операции
     * @return время последней полезной операции, мс
     */
    public long getLastActivityTime();

}
