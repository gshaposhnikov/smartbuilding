package ru.rubezh.firesec.nt.plugin.driver.license;

import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;

/**
 * Модуль обновления лицензии
 */
public class LicenseChecker implements LicenseReader {

    /** Считыватель лицензионных ограничений */
    private LicenseReader licenseReader;

    /** Лицензионные ограничения */
    private volatile LicenseRestrictions licenseRestrictions = new LicenseRestrictions();

    /** Лицензия изменилась */
    private static volatile boolean licenseUpdated = false;

    public LicenseChecker(LicenseReader licenseReader) {
        this.licenseReader = licenseReader;
    }

    @Override
    public LicenseRestrictions readLicenseRestrictions() {
        return licenseRestrictions;
    }

    public void updateLicense() {
        LicenseRestrictions restrictions = licenseReader.readLicenseRestrictions();
        if (!licenseRestrictions.equals(restrictions)) {
            licenseRestrictions = restrictions;
            licenseUpdated = true;
        }
    }

    public boolean licenseUpdated() {
        boolean result = licenseUpdated;
        licenseUpdated = false;
        return result;
    }

}
