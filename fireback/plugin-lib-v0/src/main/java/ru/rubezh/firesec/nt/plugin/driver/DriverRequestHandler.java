package ru.rubezh.firesec.nt.plugin.driver;

import ru.rubezh.firesec.nt.amqp.message.request.*;
import ru.rubezh.firesec.nt.amqp.message.response.*;

/**
 * Интерфейс обработчика запросов к драйверу.
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public interface DriverRequestHandler {

    PingResponse handleMessage(PingRequest pingRequest);

    void handleMessage(ShutdownRequest shutdownRequest);

    SetActiveProjectResponse handleMessage(SetActiveProjectRequest setActiveProjectRequest);

    GetDriverProfileViewsResponse handleMessage(GetDriverProfileViewsRequest getDriverProfileViewsRequest);

    CreateIssueDriverResponse handleMessage(CreateIssueDriverRequest request);

    SetDriverDeviceConfigResponse handleMessage(SetDriverDeviceConfigRequest setDeviceConfigRequest);

    DeleteTaskDriverResponse handleMessage(DeleteTaskDriverRequest request);

    PauseTaskDriverResponse handleMessage(PauseTaskDriverRequest request);

    GetLicenceRestrictionsResponse handleMessage(GetLicenceRestrictionsRequest request);
}
