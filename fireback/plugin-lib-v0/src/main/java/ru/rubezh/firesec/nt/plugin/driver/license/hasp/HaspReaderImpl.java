package ru.rubezh.firesec.nt.plugin.driver.license.hasp;

import Aladdin.Hasp;

import ru.rubezh.firesec.nt.domain.v1.license.*;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseReader;

public class HaspReaderImpl implements LicenseReader {

    public static final String VENDOR_CODE = VendorCode.RUBEZH_FIRESEC_NT;

    @Override
    public LicenseRestrictions readLicenseRestrictions() {
        LicenseRestrictions restrictions = new LicenseRestrictions();

        if (checkKeyPresence()) {
            readControlRestriction(restrictions.getControl());
            readSkudRestriction(restrictions.getSkud());
        }

        return restrictions;
    }

    private void readControlRestriction(ControlRestriction control) {
        if (checkFeatureAvailability(HaspFeature.CONTROL_FULL)) {
            control.setFirefightingEnabled(true);
            control.setEngineeringEnabled(true);
            return;
        } else if (checkFeatureAvailability(HaspFeature.CONTROL_ENGINEERING)) {
            control.setEngineeringEnabled(true);
        }
    }

    private void readSkudRestriction(SkudRestriction skud) {
        skud.setStaffEnabled(checkFeatureAvailability(HaspFeature.SKUD_STAFF));
        skud.setCheckpointEnabled(checkFeatureAvailability(HaspFeature.SKUD_CHECKPOINT));
        skud.setReportsEnabled(checkFeatureAvailability(HaspFeature.SKUD_REPORTS));
    }

    private boolean checkKeyPresence() {
        boolean result = false;

        Hasp hasp = new Hasp(Hasp.HASP_DEFAULT_FID);
        if (hasp.login(VENDOR_CODE))
            result = true;
        hasp.logout();

        return result;
    }

    private boolean checkFeatureAvailability(HaspFeature feature) {
        boolean result = false;

        Hasp hasp = new Hasp(feature.getId() | Hasp.HASP_PROGNUM_FEATURETYPE);
        if (hasp.login(VENDOR_CODE))
            result = true;
        hasp.logout();

        return result;
    }

}
