package ru.rubezh.firesec.nt.plugin.driver.license.hasp;

import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;
import ru.rubezh.firesec.nt.plugin.driver.license.LicenseReader;

/**
 * Заглушка считывателя HASP без лицензионных ограничений
 * Использовать только в отладочных целях!
 */
public class HaspReaderFake implements LicenseReader {

    @Override
    public LicenseRestrictions readLicenseRestrictions() {
        LicenseRestrictions restrictions = new LicenseRestrictions();

        restrictions.getControl().setFirefightingEnabled(true);
        restrictions.getControl().setEngineeringEnabled(true);
        restrictions.getSkud().setStaffEnabled(true);
        restrictions.getSkud().setCheckpointEnabled(true);
        restrictions.getSkud().setReportsEnabled(true);

        return restrictions;
    }

}
