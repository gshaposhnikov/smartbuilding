//mongeez formatted javascript
//changeset Andrey_Lisovoy:FS-1290

// Иконка и текстурка для МКД (модуль контроля доступа)
db.medias.save([
  {
    _id:"mkdIcon",
    mediaType:"ICON",
    name:"Иконка МКД",
    path:"mkd.ico",
    _class : "Media"
  },{
    _id: "mkdTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура МКД",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m 14,5 v 40 h 22 v -40 z" stroke-width="3" stroke="black" fill="white"/><path d="m 18,9 v 18 h 14 v -18 z" stroke-width="3" stroke="black" fill="white"/><path d="m 18,31 v 10 h 14 v -10 z" stroke-width="3" stroke="black" fill="white"/></g></svg>',
    _class: "Media"
  }
]);
