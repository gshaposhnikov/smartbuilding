//mongeez formatted javascript
//changeset Anton_Vasilyev:FS-1317

// Уникальность числового идентификатора рабочего дня в рамках проекта
db.work_schedules.createIndex({projectId: 1, "days.index": 1},
    {name: "wsDaysIndex", unique: true, partialFilterExpression:{"days.index": {$exists: true}}})
    
db.work_schedules.createIndex({projectId: 1, "days.accessGrantedTimes.index": 1},
    {name: "wsAccessGrantedTimesIndex", unique: true, partialFilterExpression: {"days.accessGrantedTimes.index": {$exists: true}}})

db.user_groups.update({"_id" : "admins"},
    {$push: {permissionIds: {$each: ["SKUD_CREATE", "SKUD_READ", "SKUD_UPDATE", "SKUD_REMOVE"]}}});
