// mongeez formatted javascript
// changeset Anton_Karavaev:FS-1511_upd
db.medias.save([
  {
    _id:"im1Icon",
    mediaType:"ICON",
    name:"Иконка ИМ-1",
    path:"im1.ico",
    _class : "Media"
  },{
    _id: "im1Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИМ-1",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><rect x="15" y="5" width="20" height="40" style="fill:white" /></g></svg>',
    _class: "Media"
  }
]);