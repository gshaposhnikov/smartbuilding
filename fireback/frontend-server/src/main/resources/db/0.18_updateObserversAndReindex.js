// mongeez formatted javascript

// changeset Anton_vasilyev:FS-1590

try {
  db.entity_observers.updateMany({}, {$rename: {lastObserved: "lastObservedObjectId"}});
  var observers = [];
  db.entity_observers.find().forEach(observer => {
    observer.lastObservedObjectId = ObjectId.fromDate(observer.lastObservedObjectId);
    observers.push(observer);
  });
  db.entity_observers.remove({});
  db.entity_observers.insertMany(observers);
}
catch(excp) {
}

db.events.reIndex();
