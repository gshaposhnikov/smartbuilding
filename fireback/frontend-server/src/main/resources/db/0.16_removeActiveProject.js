// mongeez formatted javascript

// changeset Andrey_Lisovoy:FS-1084
db.projects.updateMany({status: 'ACTIVE'}, {$set: {status: 'BUILD'}});
db.active_devices.remove({});
db.active_projects.remove({});
db.active_regions.remove({});
db.active_scenarios.remove({});
db.active_subsystems.remove({});
