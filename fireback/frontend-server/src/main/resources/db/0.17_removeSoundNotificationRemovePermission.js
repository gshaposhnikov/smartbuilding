// mongeez formatted javascript

// changeset Andrey_Goman:FS-1668
db.user_groups.updateMany(
  {},
  {
    $pull: {
      permissionIds: "SOUND_NOTIFICATION_REMOVE"
    }
  }
);
db.users.updateMany(
  {},
  {
    $pull: {
      permissionIds: "SOUND_NOTIFICATION_REMOVE"
    }
  }
);
