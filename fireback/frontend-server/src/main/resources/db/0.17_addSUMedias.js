//mongeez formatted javascript
//changeset Anton_Karavaev:FS-1128

// Иконки и текстурки для ШУ, ШУ-ПН, ШУ-ДН, Жокея, ШУЗ
db.medias.save([
  {
    _id:"sunIcon",
    mediaType:"ICON",
    name:"Иконка ШУН",
    path:"sun.ico",
    _class : "Media"
  },{
    _id: "sunTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ШУН",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g> <path d="m 25,8 a 18,18 0,1 0,0.00001 0 z 25,0" stroke-width="3" stroke="white" fill="transparent" /><path d="m 20,17 15,10 -15,8 z" stroke-width="3" stroke="white" fill="white" /></g></svg>',
    _class: "Media"
  },{
    _id:"suzIcon",
    mediaType:"ICON",
    name:"Иконка ШУЗ",
    path:"suz.ico",
    _class : "Media"
  },{
    _id: "suzTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ШУЗ",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><polygon points="5,50 5,30 25,40" style="fill:white;stroke-width:3"/><polygon points="45,50 45,30 25,40" style="fill:white;stroke-width:3"/><line x1="25" y1="40" x2="25" y2="25" style="stroke:green;stroke-width:3" /><circle cx="25" cy="18" r="8" stroke="white" stroke-width="3" fill="white" /></g></svg>',
    _class: "Media"
  }
]);
