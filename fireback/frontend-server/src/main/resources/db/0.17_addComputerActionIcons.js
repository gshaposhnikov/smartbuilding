//mongeez formatted javascript
//changeset Anton_Karavaev:FS-393

db.medias.save([
  {
    _id: "actionComputerAction",
    mediaType: "ANTD_ICON",
    name: "Действие на компьютере",
    path: "desktop",
    fillColor: "black",
    _class: "Media"
  },{
    _id: "actionShowMessage",
    mediaType: "ANTD_ICON",
    name: "Показать сообщение",
    path: "message",
    fillColor: "green",
    _class: "Media"
  },{
    _id: "actionCountdown",
    mediaType: "ANTD_ICON",
    name: "Обратный отсчет",
    path: "clock-circle",
    fillColor: "#ffbf00",
    _class: "Media"
   }
])