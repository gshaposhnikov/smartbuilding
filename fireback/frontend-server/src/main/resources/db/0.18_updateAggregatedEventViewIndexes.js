// mongeez formatted javascript

// changeset Anton_Karavaev:FS-1819_updateAggregatedEventViewIndexes
db.aggregated_event_views.dropIndex("indicatorEventInfo.indicatorPanelId");
db.aggregated_event_views.dropIndex("indicatorEventInfo.rowNo");
db.aggregated_event_views.dropIndex("indicatorEventInfo.colNo");
db.aggregated_event_views.dropIndex("filterTags");