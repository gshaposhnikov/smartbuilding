// mongeez formatted javascript

// changeset Andrey_Goman:FS-1451

// добавление прав на удаление представления журнала
db.user_groups.updateOne(
  { _id: "admins", permissionIds: "LOG_VIEW_DELETE" },
  { $set: { "permissionIds.$": "LOG_VIEW_REMOVE" } }
);
