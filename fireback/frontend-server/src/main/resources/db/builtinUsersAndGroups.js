// mongeez formatted javascript

// changeset satellite-soft:SET_BUILTIN_USERS_AND_GROUPS

// создание групп пользователей по умолчанию
db.user_groups.save([{
    "_id":"admins",
    "name":"Администраторы",
    "description":"Встроенная группа пользователей с полным доступом к системе",
    "permissionIds":[
        "PROJECT_READ","PROJECT_CREATE","PROJECT_UPDATE","PROJECT_REMOVE","PROJECT_IMPORT","PROJECT_EXPORT",
        "PROJECT_STATE_UPDATE",
        "USER_READ","USER_CREATE","USER_UPDATE","USER_REMOVE",
        "USER_BUILTIN_UPDATE","USER_SELF_UPDATE","USER_RESTORE",
        "USER_GROUP_READ","USER_GROUP_CREATE","USER_GROUP_UPDATE","USER_GROUP_REMOVE",
        "REGION_READ","REGION_CREATE","REGION_UPDATE","REGION_REMOVE",
        "DEVICE_READ","DEVICE_CREATE","DEVICE_UPDATE","DEVICE_REMOVE",
        "SCENARIO_READ","SCENARIO_CREATE","SCENARIO_UPDATE","SCENARIO_REMOVE",
        "VIRTUAL_STATE_READ","VIRTUAL_STATE_CREATE","VIRTUAL_STATE_UPDATE","VIRTUAL_STATE_REMOVE",
        "ADMINISTRATE",
        "LOG_VIEW_READ", "LOG_VIEW_CREATE", "LOG_VIEW_UPDATE", "LOG_VIEW_REMOVE",
        "SKUD_READ", "SKUD_CREATE", "SKUD_UPDATE", "SKUD_REMOVE",
        "MEDIA_CREATE", "MEDIA_UPDATE", "MEDIA_REMOVE",
        "SOUND_NOTIFICATION_READ", "SOUND_NOTIFICATION_CREATE", "SOUND_NOTIFICATION_UPDATE",
        "ISSUE_READ", "ISSUE_CREATE", "ISSUE_UPDATE", "ISSUE_REMOVE"
    ],
    "builtin":true,
    "removed":false
}]);

// создание пользователей по умолчанию
db.users.save([{
    "_id":"admin",
    "name":"admin",
    "fullName":"Администратор",
    "passwordHash":"7c4a8d09ca3762af61e59520943dc26494f8941b",
    "phoneNo":"+00000000000",
    "email":"admin@m.ru",
    "active":true,
    "builtin":true,
    "permissionIds":[],
    "userGroupIds":["admins"]
}]);
