// mongeez formatted javascript

// changeset Alexandr_Goryachkin:FS-669

// Иконка и текстурка для ИПР
db.medias.save([
  {
    _id:"iprIcon",
    mediaType:"ICON",
    name:"Иконка ИПР",
    path:"ipr.svg",
    _class : "Media"
  },{
    _id: "iprTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИПР",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m 5 5 C 5 32, 45 32, 45 5" stroke-width="3" stroke="white" fill="transparent"/><line y1="25" x1="25" y2="45" x2="25" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  }
]);

// Изменение текстур
db.medias.save([
  {
    _id: "io40920Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИО 40920",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="white" stroke-opacity="1" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M12.0,39.0 L31.0,11.0 L49.0,39.0 z" class="selected" transform="rotate(-90, 30.0, 25.0)"/></g></svg>',
    _class: "Media"
  },{
    _id: "io32920Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИО 32920",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="white" stroke-opacity="1" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M2.0,49.0 L2.0,3.0 L48.0,49.0 z" class="" transform="rotate(-90.0, 25.0, 25.0)"/></g></svg>',
    _class: "Media"
  },{
    _id: "ivepraTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИВЭПР-А",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="10.0" y1="21.0" x2="10.0" y2="29.0" stroke-width="3" stroke="white"/><line x1="8.5" y1="21.0" x2="14.0" y2="21.0" stroke-width="3" stroke="white"/><line x1="8.5" y1="29.0" x2="14.0" y2="29.0" stroke-width="3" stroke="white"/><line x1="15.0" y1="22.5" x2="15.0" y2="15.0" stroke-width="3" stroke="white"/><line x1="15.0" y1="27.5" x2="15.0" y2="35.0" stroke-width="3" stroke="white"/><line x1="13.5" y1="15.0" x2="45.0" y2="15.0" stroke-width="3" stroke="white"/><line x1="13.5" y1="35.0" x2="45.0" y2="35.0" stroke-width="3" stroke="white"/><line x1="43.5" y1="13.5" x2="43.5" y2="35.0" stroke-width="3" stroke="white"/><rect width="18" height="10" x="20" y="20" fill="white" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "smkTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура СМК",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><rect width="44" height="20" x="3" y="15" fill="white" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "mdu1Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура мду-1",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="none" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M15.0, 45.0 L25.0, 35.0 L35.0, 45.0 z" class="" transform="rotate(180.0, 25.0, 40.0)"/><line x1="25" y1="9.5" x2="25" y2="41.0" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  }
]);
