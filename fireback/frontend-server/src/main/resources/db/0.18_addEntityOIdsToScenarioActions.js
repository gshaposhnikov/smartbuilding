// mongeez formatted javascript

// changeset Anton_Karavaev:FS-997_addEntityOIdsToScenarioActions
function insertEntityIdsInLogicBlocks(logicBlock){
    if (logicBlock){
        logicBlock.entityOIds = [];
        try{
            logicBlock.entityIds.forEach(entityId => logicBlock.entityOIds.push(ObjectId(entityId)));
        } catch (error){}
        if (logicBlock.subLogics){
            logicBlock.subLogics.forEach(subLogic => insertEntityIdsInLogicBlocks(subLogic));
        }
    }
}

db.scenarios.find().forEach(scenario => {
    insertEntityIdsInLogicBlocks(scenario.startLogic);
    insertEntityIdsInLogicBlocks(scenario.stopLogic);
    scenario.timeLineBlocks.forEach(timeLineBlock => {
        try{
            if(timeLineBlock.tracingEntityId)
                timeLineBlock.tracingEntityOId = ObjectId(timeLineBlock.tracingEntityId);
        } catch (error){}
        try{
            if(timeLineBlock.conditionEntityId)
                timeLineBlock.conditionEntityOId = ObjectId(timeLineBlock.conditionEntityId);
        } catch (error){}
        timeLineBlock.actions.forEach(action => {
            if (action.entityId){
                try{
                    action.entityOId = ObjectId(action.entityId);
                } catch (error){}
            }
        })
    })
    db.scenarios.save(scenario);
})