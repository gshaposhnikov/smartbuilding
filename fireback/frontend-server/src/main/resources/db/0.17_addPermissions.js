// mongeez formatted javascript

// changeset Anton_Karavaev:FS-1152

// добавление прав
db.user_groups.update({"_id" : "admins"},
    {$push: {permissionIds: {$each: ["LOG_VIEW_CREATE", "LOG_VIEW_READ", "LOG_VIEW_UPDATE", "LOG_VIEW_REMOVE"]}}});