//mongeez formatted javascript
//changeset Andrey_Lisovoy:FS-807_upd1

// Иконка и текстурка для НС (насосная станция, pump station)
db.medias.save([
  {
    _id:"psIcon",
    mediaType:"ICON",
    name:"Иконка НС",
    path:"ps.ico",
    _class : "Media"
  },{
    _id: "psTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура НС",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m 15,37 c 0,10 21,10 21,0 v -12 l -3,-3 h -15 l -3,3 z" stroke-width="3" stroke="black" fill="white"/><path d="m 19,18 h 13 v -2 h -5 v -8 h 13 v -2 h -28 v 2 h 13 v 8 h -6 z" stroke-width="3" stroke="black" fill="white"/><path d="m 14,36 h -5 v 8 h -2 v -22 h 2 v 6 h 5 z" stroke-width="3" stroke="black" fill="white"/><path d="m 37,36 h 5 v 8 h 2 v -22 h -2 v 6 h -5 z" stroke-width="3" stroke="black" fill="white"/></g></svg>',
    _class: "Media"
  }
]);
