// mongeez formatted javascript

// changeset Anton_Karavaev:FS-997_addGetActiveProjectScenariosScripts_upd2
function getAggregatedActiveScenarioViewsByProjectIdAndTimeAfter(projectId, lastObservedTimeString, pageSize, pageNo){
    if (!projectId){
        projectId = ".*"
    }
    lastObservedTime = !lastObservedTimeString ? ISODate("1970-01-01T00:00:00.000Z") : ISODate(lastObservedTimeString)
    return db.active_scenarios.aggregate([
        {
            $match: { $and: [{projectId: {$regex: projectId}}, {stateGetDateTime: {$gt: lastObservedTime}}] }
        },{
            $skip: pageSize * pageNo
        },{
            $limit: pageSize
        },{
            $lookup: {
                from: "scenarios",
                localField: "_id",
                foreignField: "_id",
                as: "scenario"
            }
        },{
            $unwind: "$scenario"
        },{
            $lookup:{
                from: "state_category_views",
                localField: "generalStateCategoryId",
                foreignField: "_id",
                as: "generalStateCategoryView"
            }
        },{
           $unwind: "$generalStateCategoryView"
        },{
            $lookup: {
                from: "state_views",
                localField: "scenario.timeLineBlocks.conditionDeviceStateId",
                foreignField: "_id",
                as: "stateViews"
            }
        },{
            $unwind: {
                path: "$stateViews",
                preserveNullAndEmptyArrays: true
            }
        },{
            $addFields: {
                /* Сценарий сохраняется в отдельную переменную, т.к. далее его блоки времени раскрываются */
                scenarioCopy: "$scenario",
                /* Поле хранит BSON id связанных обьектов, сохраняя вложенность (как в самом сценарии).  */
                logicBlockEntityOIds: [
                    "$scenario.startLogic.entityOIds", "$scenario.startLogic.subLogics.entityOIds",
                    "$scenario.startLogic.subLogics.subLogics.entityOIds",
                    "$scenario.stopLogic.entityOIds", "$scenario.stopLogic.subLogics.entityOIds",
                    "$scenario.stopLogic.subLogics.subLogics.entityOIds"
                ],
                tlBlockEntityOIds: ["$scenario.timeLineBlocks.conditionEntityOId", "$scenario.timeLineBlocks.tracingEntityOId"]
            }
        },
        /* Чтобы вытащить все id на самый верхний уровень, нужно 2 раза сделать $unwind */
        {
            $unwind: {
                path: "$logicBlockEntityOIds",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$logicBlockEntityOIds",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$logicBlockEntityOIds",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "device_views",
                localField: "logicBlockEntityOIds",
                foreignField: "_id",
                as: "logicBlockDeviceViews"
            }
        },{
            $unwind: {
                path: "$logicBlockDeviceViews",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "regions",
                localField: "logicBlockEntityOIds",
                foreignField: "_id",
                as: "logicBlockRegions"
            }
        },{
            $unwind: {
                path: "$logicBlockRegions",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "virtual_states",
                localField: "logicBlockEntityOIds",
                foreignField: "_id",
                as: "logicBlockVirtualStates"
            }
        },{
            $unwind: {
                path: "$logicBlockVirtualStates",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$tlBlockEntityOIds",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$tlBlockEntityOIds",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "device_views",
                localField: "tlBlockEntityOIds",
                foreignField: "_id",
                as: "tlBlockDeviceViews"
            }
        },{
            $unwind: {
                path: "$tlBlockDeviceViews",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "virtual_states",
                localField: "tlBlockEntityOIds",
                foreignField: "_id",
                as: "tlBlockVirtualStates"
            }
        },{
            $unwind: {
                path: "$tlBlockVirtualStates",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$scenario.timeLineBlocks",
                preserveNullAndEmptyArrays: true
            }
        },{
            $unwind: {
                path: "$scenario.timeLineBlocks.actions",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "device_views",
                localField: "scenario.timeLineBlocks.actions.entityOId",
                foreignField: "_id",
                as: "actionDeviceViews"
            }
        },{
            $unwind: {
                path: "$actionDeviceViews",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from:"regions",
                localField: "scenario.timeLineBlocks.actions.entityOId",
                foreignField: "_id",
                as: "actionRegions"
            }
        },{
            $unwind: {
                path: "$actionRegions",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from: "scenarios",
                localField: "scenario.timeLineBlocks.actions.entityOId",
                foreignField: "_id",
                as: "actionScenarios"
            }
        },{
            $unwind: {
                path: "$actionScenarios",
                preserveNullAndEmptyArrays: true
            }
        },{
            $lookup: {
                from:"virtual_states",
                localField: "scenario.timeLineBlocks.actions.entityOId",
                foreignField: "_id",
                as: "actionVirtualStates"
            }
        },{
            $unwind: {
                path: "$actionVirtualStates",
                preserveNullAndEmptyArrays: true
            }
        },{
            $group: {
                _id: "$_id",
                scenario: {$first: "$scenarioCopy"},
                generalStateCategoryView: {$first: "$generalStateCategoryView"},

                tlBlockConditionStateViews: {$addToSet: "$stateViews"},

                logicBlockDeviceViews: {$addToSet: "$logicBlockDeviceViews"},
                logicBlockRegions: {$addToSet: "$logicBlockRegions"},
                logicBlockVirtualStates: {$addToSet: "$logicBlockVirtualStates"},

                tlBlockDeviceViews: {$addToSet: "$tlBlockDeviceViews"},
                tlBlockVirtualStates: {$addToSet: "$tlBlockVirtualStates"},

                actionDeviceViews: {$addToSet: "$actionDeviceViews"},
                actionRegions: {$addToSet: "$actionRegions"},
                actionScenarios: {$addToSet: "$actionScenarios"},
                actionVirtualStates: {$addToSet: "$actionVirtualStates"}
            }
        }
    ]).toArray()
}

//Поменять старое имя функции ("getAggregatedActiveScenarioViewsByProjectId") при следующем редактировании
db.system.js.remove({ _id: "getAggregatedActiveScenarioViewsByProjectId" });
db.system.js.insert([
  {
    _id: "getAggregatedActiveScenarioViewsByProjectIdAndTimeAfter",
    value: getAggregatedActiveScenarioViewsByProjectIdAndTimeAfter
  }
]);