// mongeez formatted javascript

// changeset Anton_Karavaev:FS-1288,FS-1289

// добавление прав для создания, обновления и удаления медиа
db.user_groups.update({"_id" : "admins"},
    {$push: {permissionIds: {$each: ["MEDIA_CREATE", "MEDIA_UPDATE", "MEDIA_REMOVE", "SOUND_NOTIFICATION_READ",
    "SOUND_NOTIFICATION_CREATE", "SOUND_NOTIFICATION_UPDATE"]}}});
