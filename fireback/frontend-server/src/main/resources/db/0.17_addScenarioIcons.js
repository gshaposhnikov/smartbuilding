//mongeez formatted javascript
//changeset Andrey_Goman:FS-1234

// Иконки для блоков слежения в сценариях
db.medias.save([
  {
    _id: "actionIn",
    mediaType: "ANTD_ICON",
    name: "Исполнение (без условия)",
    path: "setting",
    fillColor: "lightgray",
    _class: "Media"
  },
  {
    _id: "actionTrace",
    mediaType: "ANTD_ICON",
    name: "Слежение (вкл.)",
    path: "eye",
    fillColor: "green",
    _class: "Media"
  },
  {
    _id: "actionNoTrace",
    mediaType: "ANTD_ICON",
    name: "Слежение (выкл.)",
    path: "eye",
    fillColor: "red",
    _class: "Media"
  }
]);
