// mongeez formatted javascript

// changeset Andrey_Goman:FS-1447
db.plan_groups.remove({ projectId: "Plans_example_v1" });
db.plans.remove({ projectId: "Plans_example_v1" });
db.regions.remove({ projectId: "Plans_example_v1" });
db.region_views.remove({ projectId: "Plans_example_v1" });
db.active_regions.remove({ projectId: "Plans_example_v1" });
db.devices.remove({ projectId: "Plans_example_v1" });
db.device_views.remove({ projectId: "Plans_example_v1" });
db.active_devices.remove({ projectId: "Plans_example_v1" });
db.scenarios.remove({ projectId: "Plans_example_v1" });
db.active_scenarios.remove({ projectId: "Plans_example_v1" });
db.projects.remove({ _id: "Plans_example_v1" });
