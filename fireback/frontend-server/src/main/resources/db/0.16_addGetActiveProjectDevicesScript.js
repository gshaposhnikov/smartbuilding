// mongeez formatted javascript

// changeset Anton_Vasilyev:FS-912_addGetActiveProjectDevicesScript_upd_15
function getTreeItemActiveDeviceViewsByProjectIdAndTimeAfter(projectId, lastObservedTimeString, pageSize, pageNo) {
  if(!projectId){
    projectId = ".*"
  }
  lastObservedTime = !lastObservedTimeString ? ISODate("1970-01-01T00:00:00.000Z") : ISODate(lastObservedTimeString)
  return db.active_devices
    .aggregate([
      { $match: { $and: [{projectId: {$regex: projectId}}, {stateGetDateTime: {$gt: lastObservedTime}}] } },
      { $skip: pageSize * pageNo},
      { $limit: pageSize},
      {
        $lookup: {
          from: "devices",
          localField: "_id",
          foreignField: "_id",
          as: "device"
        }
      },
        { $unwind: "$device" },
      {
        $lookup: {
          from: "device_views",
          localField: "_id",
          foreignField: "_id",
          as: "deviceView"
        }
      },
      { $unwind: "$deviceView" },
      {
        $lookup: {
          from: "device_profiles",
          localField: "device.deviceProfileId",
          foreignField: "_id",
          as: "deviceProfile"
        }
      },
      { $unwind: "$deviceProfile" },
      {
        $lookup: {
          from: "device_profile_views",
          localField: "device.deviceProfileId",
          foreignField: "_id",
          as: "deviceProfileView"
        }
      },
      { $unwind: "$deviceProfileView" },
      {
        $lookup: {
          from: "regions",
          localField: "device.regionOid",
          foreignField: "_id",
          as: "region"
        }
      },
      { $unwind: { path: "$region", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "device_profile_views",
          localField: "device.acceptableDeviceProfileIds",
          foreignField: "_id",
          as: "acceptableDeviceProfiles"
        }
      },
      {
        $lookup: {
          from: "state_category_views",
          localField: "generalStateCategoryId",
          foreignField: "_id",
          as: "generalStateCategoryView"
        }
      },
      {
        $unwind: {
          path: "$generalStateCategoryView",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: "state_views",
          localField: "activeStates",
          foreignField: "_id",
          as: "rawActiveStateViews"
        }
      },
      /* Для следующего присоединения нужно сначала вытащить список идентификаторов на верхний уровень */
      {
        $addFields: {
          monitorableValueProfileViewIds: "$deviceProfile.customMonitorableValues.monitorableValueProfileId"
        }
      },
      {
        $lookup: {
          from: "monitorable_value_profile_views",
          localField: "monitorableValueProfileViewIds",
          foreignField: "_id",
          as: "monitorableValueProfileViews"
        }
      },
      {
        $project: {
          deviceProfileId: "$device.deviceProfileId", //
          subsystem: "$deviceProfile.subsystem", //
          customSubsystem: "$device.customSubsystem",
          deviceCategory: "$deviceProfile.deviceCategory", //
          projectId: 1, //
          acceptableAsScenarioCondition:
            "$deviceProfile.acceptableAsScenarioCondition", //
          attachableToRegion: "$deviceProfile.attachableToRegion", //
          regionId: 1, //
          regionName: "$region.name", //

          parentDeviceId: 1, //
          addressPath: 1, //

          fullAddressPath: "$deviceView.fullAddressPath", //
          middleAddressPath: "$deviceView.middleAddressPath", //
          shortAddressPath: "$deviceView.shortAddressPath", //
          addressLevel: 1, //
          lineAddressRanges: "$device.lineAddressRanges", //
          editableAddress: "$device.editableAddress", //
          addressType: "$device.addressType",

          name: "$deviceView.name", //
          description: "$deviceView.description", //
          iconMedia: "$deviceView.iconMedia", //
          textureMedia: "$deviceView.textureMedia", //
          planLayouts: "$deviceView.planLayouts", //
          deviceShapeLibraryId: "$deviceView.deviceShapeLibraryId",

          canBeDisabled: "$device.canBeDisabled",
          disableNotAllowed: "$device.disableNotAllowed",
          disabled: "$device.disabled",
          embedded: "$device.embedded",
          virtualContainerId: "$device.virtualContainerId",

          acceptableDeviceProfiles: {$map: {
              input: "$acceptableDeviceProfiles",
              in: {
                  deviceProfileId: "$device._id",
                  deviceProfileName: "$device.name"
              }
          }},

          /* Активное устройство */
          statePolling: {
            $and: [
              "deviceProfile.ignorable",
              "deviceProfile.deviceIgnoreState.$id",
              {
                $not: [
                  {
                    $in: [
                      "$deviceProfile.deviceIgnoreStateId",
                      "$activeStates"
                    ]
                  }
                ]
              }
            ]
          }, //

          generalStateCategoryView: 1,
          monitorableValuesTimestamp: 1,
          notes: 1,
          filterTags: "$device.filterTags",

          /* Поля - помощники для вычисления сложных агрегированных активных состояний */
          rawActiveStateViews: 1,
          devicePropertyValues: "$device.devicePropertyValues",
          deviceMonitorableValues: "$monitorableValues",
          monitorableValueProfileViews: 1,
          deviceConfigProperties: "$deviceProfile.configProperties",
          deviceConfigPropertyViews: "$deviceProfileView.configPropertyViews",
          projectDeviceConfigValues: "$device.projectConfigValues",
          activeDeviceConfigValues: "$activeConfigValues",
          deviceOtherProperties: "$deviceProfile.deviceProperties",
          deviceOtherPropertyViews: "$deviceProfileView.propertyViews",
          projectDeviceOtherValues: "$device.devicePropertyValues",
          linkedDeviceIdsMap: "$device.linkedDeviceIdsMap",
          supportedActions: "$deviceProfile.supportedActions",
          supportedActionViews: "$deviceProfileView.supportedActionViews"
        }
      }
    ])
    .toArray();
}

//Поменять старое имя функции ("getTreeItemActiveDeviceViewsByProjectId") при следующем редактировании
db.system.js.remove({ _id: "getTreeItemActiveDeviceViewsByProjectId" });
db.system.js.insert([
  {
    _id: "getTreeItemActiveDeviceViewsByProjectIdAndTimeAfter",
    value: getTreeItemActiveDeviceViewsByProjectIdAndTimeAfter
  }
]);
