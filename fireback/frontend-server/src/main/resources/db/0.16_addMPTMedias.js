//mongeez formatted javascript
//changeset Andrey_Lisovoy:FS-920

// Иконка и текстурка для МПТ
db.medias.save([
  {
    _id:"mptIcon",
    mediaType:"ICON",
    name:"Иконка МПТ",
    path:"mpt.ico",
    _class : "Media"
  },{
    _id: "mptTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура МПТ",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m 21,37 c 0,10 16,10 16,0 v -17 l -5,-6 h -6 l -5,6 z" stroke-width="3" stroke="blue" fill="red"/><path d="m 15,12 l 2,30 l -9,-1 z" stroke-width="3" stroke="black" fill="transparent"/><path d="m 15,12 L 30,8 L 38,5 L 30,8 L 40,13 L 30,8 v 3 h -2 v -3" stroke-width="3" stroke="black" fill="transparent"/></g></svg>',
    _class: "Media"
  }
]);
