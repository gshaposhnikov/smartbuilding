// mongeez formatted javascript

// changeset Andrey_Goman:FS-1792
var addressSubstitutionByProfileId = {
  rubezh2op3SecurityButtonProfile: 2,
  rubezh2op3FireButtonProfile: 1
};

function updateDeviceAddressPathAndSave(
  collectionName,
  device,
  deviceProfileId
) {
  var splittedAddressPath = device.addressPath.split(",");
  var addressPathLength = splittedAddressPath.length;
  splittedAddressPath[addressPathLength - 1] =
    addressSubstitutionByProfileId[deviceProfileId];
  var newAddressPath = splittedAddressPath.join(",");
  device.addressPath = newAddressPath;
  db[collectionName].save(device);
}

Object.keys(addressSubstitutionByProfileId).forEach(deviceProfileId => {
  db.devices.find({ deviceProfileId: deviceProfileId }).forEach(device => {
    updateDeviceAddressPathAndSave("devices", device, device.deviceProfileId);
    var activeDevice = db.active_devices.findOne({ _id: device._id });
    if (activeDevice) {
      updateDeviceAddressPathAndSave(
        "active_devices",
        activeDevice,
        device.deviceProfileId
      );
    }
    var deviceView = db.device_views.findOne({ _id: device._id });
    if (deviceView) {
      updateDeviceAddressPathAndSave(
        "device_views",
        deviceView,
        device.deviceProfileId
      );
    }
  });
});
