// mongeez formatted javascript
// changeset Anton_Lyagin:FS-1133_upd
db.medias.remove({ _id: "suIcon" });
db.medias.save([
  {
    _id:"suIcon",
    mediaType:"ICON",
    name:"Иконка ШУВ",
    path:"su.ico",
    _class : "Media"
  },{
    _id: "suTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ШУВ",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"> <g> <rect x="15" y="15" width="20" height="20" style="fill:transparent;stroke:white;stroke-width:2" /> <rect x="20" y="20" width="10" height="10" style="fill:white" /> </g></svg>',
    _class: "Media"
  }
]);
