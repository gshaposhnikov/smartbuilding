// mongeez formatted javascript

// changeset Anton_Karavaev:FS-996_addGetActiveProjectRegionsScript_upd3
function getAggregatedActiveRegionViewsByProjectIdAndTimeAfter(projectId, lastObservedTimeString, pageSize, pageNo){
    if (!projectId){
        projectId = ".*"
    }
    lastObservedTime = !lastObservedTimeString ? ISODate("1970-01-01T00:00:00.000Z") : ISODate(lastObservedTimeString)
    return db.active_regions.aggregate([
        {
            $match: { $and: [{projectId: {$regex: projectId}}, {stateGetDateTime: {$gt: lastObservedTime}}] }
        },{
            $skip: pageSize * pageNo
        },{
            $limit: pageSize
        },{
            $lookup: {
                from: "regions",
                localField: "_id",
                foreignField: "_id",
                as: "region"
            }
        },{
            $unwind: "$region"
        },{
            $lookup: {
                from: "region_views",
                localField: "_id",
                foreignField: "_id",
                as: "regionView"
            }
        },{
            $unwind: "$regionView"
        },{
            $lookup:{
                from: "state_category_views",
                localField: "generalStateCategoryId",
                foreignField:"_id",
                as: "generalStateCategoryView"
            }
        },{
           $unwind: {
               path: "$generalStateCategoryView",
               preserveNullAndEmptyArrays: true
           }
        },{
            $project: {
                region: "$region",
                name: "$region.name",
                description: "$region.description",

                index: 1,
                subsystem: 1,

                planLayouts: "$regionView.planLayouts",
                onGuard: 1,
                generalStateCategoryView: "$generalStateCategoryView",
                filterTags: 1,
            }
        }
    ]).toArray()
}
//Поменять старое имя функции ("getAggregatedActiveRegionViewsByProjectId") при следующем редактировании
db.system.js.remove({ _id: "getAggregatedActiveRegionViewsByProjectId" });
db.system.js.insert([
  {
    _id: "getAggregatedActiveRegionViewsByProjectIdAndTimeAfter",
    value: getAggregatedActiveRegionViewsByProjectIdAndTimeAfter
  }
]);
