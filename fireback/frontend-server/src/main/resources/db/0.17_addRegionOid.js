//mongeez formatted javascript

//changeset Anton_Vasilyev:FS-1009

// Добавляем по возможности regionOid всем устройствам
db.devices.find().forEach(device => {
  if (device.regionId) {
    try {
      device.regionOid = ObjectId(device.regionId);
      db.devices.save(device);
    } catch (error) {
    }
  }
})
