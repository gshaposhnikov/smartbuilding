// mongeez formatted javascript

// changeset Anton_Vasilyev:FS-1672

// добавление прав
db.user_groups.update({"_id" : "admins"},
    {$push: {permissionIds: {$each: ["ISSUE_CREATE", "ISSUE_READ", "ISSUE_UPDATE", "ISSUE_REMOVE"]}}});
