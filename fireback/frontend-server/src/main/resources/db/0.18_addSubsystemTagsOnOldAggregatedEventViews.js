// mongeez formatted javascript

// changeset Anton_Karavaev:FS-1385_addSubsystemTagsOnOldAggregatedEventViews
db.aggregated_event_views.updateMany(
    {},{
        $set: {
            filterTags: {
                "tags": ["GENERAL"]
            }
        }
    }
);