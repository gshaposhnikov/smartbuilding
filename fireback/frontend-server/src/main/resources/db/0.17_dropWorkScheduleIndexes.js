//mongeez formatted javascript
//changeset Anton_Vasilyev:FS-1317-HF

// Удаляем индексы, которые работают не так, как предполагалось
db.work_schedules.dropIndex("wsDaysIndex");
db.work_schedules.dropIndex("wsAccessGrantedTimesIndex");
