// mongeez formatted javascript

// changeset satellite-soft:SET_MEDIAS

// Иконки устройств
deviceIconMedias = [
  {
    _id: "amod1Icon",
    mediaType: "ICON",
    name: "Иконка МС-1",
    path: "amod1.ico",
    _class: "Media"
  },{
    _id: "rubezh2op3Icon",
    mediaType: "ICON",
    name: "Иконка Рубеж-2ОП3",
    path: "rubezh2op3.ico",
    _class: "Media"
  },{
    _id: "am4TechIcon",
    mediaType: "ICON",
    name: "Иконка АМ-4Т",
    path: "am4tech.ico",
    _class: "Media"
  },{
    _id: "am4SecIcon",
    mediaType: "ICON",
    name: "Иконка АМ-4О",
    path: "am4sec.ico",
    _class: "Media"
  },{
    _id: "am4FireIcon",
    mediaType: "ICON",
    name: "Иконка АМ-4П",
    path: "am4fire.ico",
    _class: "Media"
  },{
    _id: "rm4Icon",
    mediaType: "ICON",
    name: "Иконка РМ-4",
    path: "rm4.ico",
    _class: "Media"
  },{
    _id: "ip29Icon",
    mediaType: "ICON",
    name: "Иконка ИП-29",
    path: "ip29.ico",
    _class: "Media"
  },{
    _id: "ip64Icon",
    mediaType: "ICON",
    name: "Иконка ИП-64",
    path: "ip64.ico",
    _class: "Media"
  },{
    _id: "ip64ComboIcon",
    mediaType: "ICON",
    name: "Иконка ИП-64К",
    path: "ip29-64.ico",
    _class: "Media"
  },{
    _id: "io40920Icon",
    mediaType: "ICON",
    name: "Иконка ИО 40920",
    path: "io40920.ico",
    _class: "Media"
  },{
    _id: "io32920Icon",
    mediaType: "ICON",
    name: "Иконка ИО 32920",
    path: "io32920.ico",
    _class: "Media"
  },{
    _id: "smkIcon",
    mediaType: "ICON",
    name: "Иконка СМК",
    path: "smk.ico",
    _class: "Media"
  },{
    _id: "ivepraIcon",
    mediaType: "ICON",
    name: "Иконка ИВЭПР-А",
    path: "bat.ico",
    _class: "Media"
  },{
    _id: "mdu1con",
    mediaType: "ICON",
    name: "Иконка МДУ-1",
    path: "mdu.ico",
    _class: "Media"
  },{
    _id: "mro2mIcon",
    mediaType: "ICON",
    name: "Иконка МРО-2М",
    path: "mro.ico",
    _class: "Media"
  },{
    _id:"iprIcon",
    mediaType:"ICON",
    name:"Иконка ИПР",
    path:"ipr.ico",
    _class : "Media"
  },{
    _id:"eduIcon",
    mediaType:"ICON",
    name:"Иконка ЭДУ",
    path:"edu.ico",
    _class : "Media"
  }
];

// Иконки сценариев
scenarioIconMedias = [
  {
    _id: "actionOn",
    mediaType: "ANTD_ICON",
    name: "Включение",
    path: "play-circle",
    fillColor: "green",
    _class: "Media"
  },{
    _id: "actionFlashingOn",
    mediaType: "ANTD_ICON",
    name: "Включение с мерцанием",
    path: "play-circle",
    fillColor: "#FFBF00",
    _class: "Media"
  },{
    _id: "actionOff",
    mediaType: "ANTD_ICON",
    name: "Выключение",
    path: "pause-circle",
    fillColor: "red",
    _class: "Media"
  },{
    _id: "actionDisable",
    mediaType: "ANTD_ICON",
    name: "Блокировка",
    path: "lock",
    fillColor: "red",
    _class: "Media"
  },{
    _id: "actionEnable",
    mediaType: "ANTD_ICON",
    name: "Разблокировка",
    path: "unlock",
    fillColor: "green",
    _class: "Media"
  },{
    _id: "actionCondition",
    mediaType: "ANTD_ICON",
    name: "Условие",
    path: "question-circle",
    fillColor: "blue",
    _class: "Media"
  },{
    _id: "actionTime",
    mediaType: "ANTD_ICON",
    name: "Временная метка",
    path: "clock-circle",
    fillColor: "blue",
    _class: "Media"
  },{
    _id: "actionRoot",
    mediaType: "ANTD_ICON",
    name: "Корневой элемент",
    path: "arrow-down",
    fillColor: "#FFBF00",
    _class: "Media"
  },{
    _id: "actionLock",
    mediaType: "ANTD_ICON",
    name: "Блокировка",
    path: "lock",
    fillColor: "green",
    _class: "Media"
  },{
    _id: "actionUnlock",
    mediaType: "ANTD_ICON",
    name: "Разблокировка",
    path: "unlock",
    fillColor: "blue",
    _class: "Media"
  }
];

// Текстуры для отображения на плане
planTextureMedias = [
  {
    _id: "ip29Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИП-29",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="25" y1="7.0" x2="25" y2="43.0" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "ip64Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИП-64",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="20.0" y1="20.0" x2="30.0" y2="9.0" stroke-width="3" stroke="white"/><line x1="20.0" y1="18.5" x2="30.0" y2="30.0" stroke-width="3" stroke="white"/><line x1="20.0" y1="40" x2="30.0" y2="28.5" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "ip64ComboTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИП-64К",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="25" y1="7.0" x2="25" y2="43.0" stroke-width="3" stroke="white"/><line x1="20.0" y1="20.0" x2="30.0" y2="9.0" stroke-width="3" stroke="white"/><line x1="20.0" y1="18.5" x2="30.0" y2="30.0" stroke-width="3" stroke="white"/><line x1="20.0" y1="40" x2="30.0" y2="28.5" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "io40920Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИО 40920",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="white" stroke-opacity="1" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M12.0,39.0 L31.0,11.0 L49.0,39.0 z" class="selected" transform="rotate(-90, 30.0, 25.0)"/></g></svg>',
    _class: "Media"
  },{
    _id: "io32920Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИО 32920",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="white" stroke-opacity="1" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M2.0,49.0 L2.0,3.0 L48.0,49.0 z" class="" transform="rotate(-90.0, 25.0, 25.0)"/></g></svg>',
    _class: "Media"
  },{
    _id: "ivepraTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИВЭПР-А",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="10.0" y1="21.0" x2="10.0" y2="29.0" stroke-width="3" stroke="white"/><line x1="8.5" y1="21.0" x2="14.0" y2="21.0" stroke-width="3" stroke="white"/><line x1="8.5" y1="29.0" x2="14.0" y2="29.0" stroke-width="3" stroke="white"/><line x1="15.0" y1="22.5" x2="15.0" y2="15.0" stroke-width="3" stroke="white"/><line x1="15.0" y1="27.5" x2="15.0" y2="35.0" stroke-width="3" stroke="white"/><line x1="13.5" y1="15.0" x2="45.0" y2="15.0" stroke-width="3" stroke="white"/><line x1="13.5" y1="35.0" x2="45.0" y2="35.0" stroke-width="3" stroke="white"/><line x1="43.5" y1="13.5" x2="43.5" y2="35.0" stroke-width="3" stroke="white"/><rect width="18" height="10" x="20" y="20" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "smkTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура СМК",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><rect width="50" height="20" x="0" y="15" fill="white" stroke-width="0" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "mro2mTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура МРО-2М",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="12.0" y1="15.0" x2="12.0" y2="35.0" stroke-width="3" stroke="white"/><line x1="35.0" y1="10.0" x2="35.0" y2="40.0" stroke-width="3" stroke="white"/></g><line x1="12.0" y1="15.5" x2="35.0" y2="10.5" stroke-width="1" stroke="white"/><line x1="12.0" y1="34.5" x2="35.0" y2="39.5" stroke-width="1" stroke="white"/></svg>',
    _class: "Media"
  },{
    _id: "mdu1Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура мду-1",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path fill="white" fill-opacity="1" stroke="none" style="color: rgb(0, 0, 0);" stroke-width="3" stroke-dasharray="none" stroke-linejoin="miter" stroke-linecap="butt" stroke-dashoffset="" fill-rule="nonzero" opacity="1" marker-start="" marker-mid="" marker-end="" d="M15.0, 45.0 L25.0, 35.0 L35.0, 45.0 z" class="" transform="rotate(180.0, 25.0, 40.0)"/><line x1="25" y1="9.5" x2="25" y2="41.0" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "rubezh2op3Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура Рубеж-2ОП3",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><rect height="25" width="25" y="25" x="0" stroke-width="0" stroke="#000" fill="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "am4TechTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура АМ-4Т",
    content:'<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><text font-family="Helvetica, Arial, sans-serif" font-size="24" y="34.0" x="10" stroke-width="0" fill="white">4Т</text></g></svg>',
    _class: "Media"
  },{
    _id: "am4SecTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура АМ-4О",
    content:'<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><text font-family="Helvetica, Arial, sans-serif" font-size="24" y="34.0" x="10" stroke-width="0" fill="white">4О</text></g></svg>',
    _class: "Media"
  },{
    _id: "am4FireTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура АМ-4П",
    content:'<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><text font-family="Helvetica, Arial, sans-serif" font-size="24" y="34.0" x="10" stroke-width="0" fill="white">4П</text></g></svg>',
    _class: "Media"
  },{
    _id: "rm4Texture",
    mediaType: "SVG_CONTENT",
    name: "Текстура РМ-4",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><line x1="0.0" y1="25.0" x2="20.0" y2="25.0" stroke-width="1.5" stroke="white"/><line x1="30.0" y1="25.0" x2="50.0" y2="25.0" stroke-width="1.5" stroke="white"/><line x1="20.0" y1="25.0" x2="27.0" y2="20.0" stroke-width="1.5" stroke="white"/></g></svg>',
    _class: "Media"
  },{
    _id: "iprTexture",
    mediaType: "SVG_CONTENT",
    name: "Текстура ИПР",
    content: '<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m 7 7 C 7 32, 45 32, 45 7" stroke-width="3" stroke="white" fill="transparent"/><line y1="25" x1="25" y2="45" x2="25" stroke-width="3" stroke="white"/></g></svg>',
    _class: "Media"
   }
];

db.medias.deleteMany({})
db.medias.save(deviceIconMedias)
db.medias.save(scenarioIconMedias)
db.medias.save(planTextureMedias)
