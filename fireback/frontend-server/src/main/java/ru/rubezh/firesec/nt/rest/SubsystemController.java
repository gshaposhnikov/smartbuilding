package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.service.v1.SubsystemViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/active_subsystems")
public class SubsystemController {

    @Autowired
    private SubsystemViewService subsystemViewService;

    /** Запрос списка состояний подсистем */
    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll() {

        return ResponseEntity.ok(subsystemViewService.getAll());
    }

}
