package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedRegionDictionary;
import ru.rubezh.firesec.nt.service.v1.RegionDictionariesViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}")
public class RegionDictionaryController {

    @Autowired
    private RegionDictionariesViewService regionDictionariesViewService;

    @RequestMapping(value = "/region_dictionaries")
    public ResponseEntity<?> getRegionDictionaries() {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedRegionDictionary result =
                regionDictionariesViewService.getAggregatedRegionDictionaries(errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

}
