package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.rubezh.firesec.nt.dao.v1.MonitorableValueProfileViewRepository;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/monitorable_value_profiles")
public class MonitorableValueProfilesController {

    @Autowired
    private MonitorableValueProfileViewRepository monitorableValueProfileViewRepository;

    /** Запрос отображений профилей набл. параметров */
    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getEventTypeViews() {
        return ResponseEntity.ok(monitorableValueProfileViewRepository.findAll());
    }

}
