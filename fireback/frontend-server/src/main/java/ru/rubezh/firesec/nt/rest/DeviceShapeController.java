package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.domain.v1.DeviceShape;
import ru.rubezh.firesec.nt.service.v1.DeviceShapeService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/device_shapes")
public class DeviceShapeController {

    @Autowired
    private DeviceShapeService deviceShapeService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> getDeviceShapes() {
        StringBuilder errorMessage = new StringBuilder();
        List<DeviceShape> deviceShapes = deviceShapeService.getActual(errorMessage);
        if (deviceShapes != null) {
            return ResponseEntity.ok(deviceShapes);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{deviceShapeId}")
    public ResponseEntity<?> getDeviceShape(@PathVariable String deviceShapeId) {
        StringBuilder errorMessage = new StringBuilder();
        DeviceShape deviceShape = deviceShapeService.getIfActual(deviceShapeId, errorMessage);
        if (deviceShape != null) {
            return ResponseEntity.ok(deviceShape);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> addDeviceShape(@RequestBody DeviceShape deviceShape) {
        StringBuilder errorMessage = new StringBuilder();
        boolean added = deviceShapeService.add(deviceShape, errorMessage);
        if (added) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceShapeId}" })
    public ResponseEntity<?> updateDeviceShape(@PathVariable String deviceShapeId,
            @RequestBody DeviceShape deviceShape) {
        StringBuilder errorMessage = new StringBuilder();
        boolean updated = deviceShapeService.update(deviceShapeId, deviceShape, errorMessage);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{deviceShapeId}")
    public ResponseEntity<?> removeDeviceShape(@PathVariable String deviceShapeId) {
        StringBuilder errorMessage = new StringBuilder();
        boolean removed = deviceShapeService.remove(deviceShapeId, errorMessage);
        if (removed) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
