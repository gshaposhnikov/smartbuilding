package ru.rubezh.firesec.nt.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.rubezh.firesec.nt.amqp.message.alert.ActiveProjectUpdated;
import ru.rubezh.firesec.nt.amqp.sender.AlertSender;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.ImportedXmlProjectWrapper;
import ru.rubezh.firesec.nt.domain.v1.representation.ProjectWrapper;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;
import ru.rubezh.firesec.nt.security.AuthenticatedComponent;
import ru.rubezh.firesec.nt.service.v1.*;
import ru.rubezh.firesec.nt.service.v1.ProjectViewService.ActiveProjectEntitiesStreamSerializer;
import ru.rubezh.firesec.nt.service.v1.ProjectViewService.BasicProjectEntitiesStreamSerializer;
import ru.rubezh.firesec.nt.service.v1.skud.AccessKeyService;
import ru.rubezh.firesec.nt.service.v1.skud.EmployeeService;
import ru.rubezh.firesec.nt.service.v1.skud.WorkScheduleService;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects")
public class ProjectController extends AuthenticatedComponent {

    @Value("#{'${application.version:}'}")
    private String projectVersion;

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    @SuppressWarnings("unused")
    private Logger logger;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectViewService projectViewService;

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private AlertSender alertSender;

    @Autowired
    private EventService eventService;

    private HttpHeaders streamingResponseHeaders = new HttpHeaders();

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private RegionViewRepository regionViewpository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private PlanGroupRepository planGroupRepository;

    @Autowired
    private VirtualStateService virtualStateService;

    @Autowired
    private PlanService planService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private ScenarioViewService scenarioViewService;

    @Autowired
    private RegionViewService regionViewService;

    @Autowired
    private RegionService regionService;

    @Autowired
    private PlanGroupService planGroupService;

    @Autowired
    private WorkScheduleRepository workScheduleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccessKeyRepository accessKeyRepository;

    @Autowired
    private WorkScheduleService workScheduleService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccessKeyService accessKeyService;

    public ProjectController() {
        streamingResponseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    @Autowired
    private IssueController issueController;

    @PreAuthorize("hasRole('PROJECT_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> saveProject(@RequestBody Project project) {

        StringBuilder errorMessage = new StringBuilder();
        Project savedProject = projectViewService.add(project, errorMessage);
        if(savedProject != null) {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(savedProject);
        }else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getProjects(
        @RequestParam(name = "status", required = false, defaultValue = "") ProjectStatus status) {

        if (status != null)
            return ResponseEntity.ok(projectRepository.findByStatus(status));
        else
            return ResponseEntity.ok(projectRepository.findAll());
    }

    public interface WriteFinishedHandler {
        void onWriteFinished();
    }

    private class ProjectEntitiesStreamingResponseBody implements StreamingResponseBody {
        private BasicProjectEntitiesStreamSerializer projectEntitiesStreamSerializer;

        private WriteFinishedHandler writeFinishedHandler = null;

        public ProjectEntitiesStreamingResponseBody(
                BasicProjectEntitiesStreamSerializer projectEntitiesStreamSerializer) {
            this.projectEntitiesStreamSerializer = projectEntitiesStreamSerializer;
        }

        public ProjectEntitiesStreamingResponseBody(
                BasicProjectEntitiesStreamSerializer projectEntitiesStreamSerializer,
                WriteFinishedHandler writeFinishedHandler) {
            this.projectEntitiesStreamSerializer = projectEntitiesStreamSerializer;
            this.writeFinishedHandler = writeFinishedHandler;
        }

        @Override
        public void writeTo(OutputStream outputStream) throws IOException {
            ObjectMapper objectMapper = new ObjectMapper();
            projectEntitiesStreamSerializer.writeAll(outputStream, objectMapper);
            if (writeFinishedHandler != null) {
                writeFinishedHandler.onWriteFinished();
            }
        }
    }

    private class ErrorStreamingResponseBody implements StreamingResponseBody {
        private String errorMessage;

        public ErrorStreamingResponseBody(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public void writeTo(OutputStream outputStream) throws IOException {
            outputStream.write(("{\"error\":\"" + errorMessage + "\"}").getBytes(StandardCharsets.UTF_8));
        }

    }

    @RequestMapping(method = RequestMethod.GET, value = "/{projectId}")
    public ResponseEntity<StreamingResponseBody> getProjectEntities(@PathVariable String projectId) {

        Project project = projectRepository.findOne(projectId);
         if (project == null) {
            return new ResponseEntity<StreamingResponseBody>(new ErrorStreamingResponseBody("Проект не найден"),
                    streamingResponseHeaders,
                    HttpStatus.NOT_FOUND);
         } else {
            StringBuilder errorMessage = new StringBuilder();
            /*
             * TODO: сделать передачу project вместо projectId, чтобы проект не
             * запрашивался из Mongo повторно
             */
            BasicProjectEntitiesStreamSerializer projectEntitiesStreamSerializer;
            if (project.getStatus() == ProjectStatus.ACTIVE) {
                projectEntitiesStreamSerializer = projectViewService.getActiveProjectEntitiesSerializer(projectId,
                        language, errorMessage);
            } else {
                projectEntitiesStreamSerializer = projectViewService.getProjectEntitiesSerializer(projectId, language,
                        errorMessage);
            }
            if (projectEntitiesStreamSerializer != null) {
                return new ResponseEntity<StreamingResponseBody>(
                        new ProjectEntitiesStreamingResponseBody(projectEntitiesStreamSerializer), streamingResponseHeaders,
                        HttpStatus.OK);
            } else {
                return new ResponseEntity<StreamingResponseBody>(
                        new ErrorStreamingResponseBody(errorMessage.toString()), streamingResponseHeaders, HttpStatus.BAD_REQUEST);
            }
         }
    }

    @RequestMapping(method = RequestMethod.GET, value = { "/active", "/active/" })
    public ResponseEntity<StreamingResponseBody> getActiveProjectEntities() {

        Project project = projectService.getActive();
        if (project == null) {
            return new ResponseEntity<StreamingResponseBody>(
                    new ErrorStreamingResponseBody("Активный проект не найден"), streamingResponseHeaders,
                    HttpStatus.BAD_REQUEST);
        } else {
            StringBuilder errorMessage = new StringBuilder();
            /*
             * TODO: сделать передачу project вместо projectId, чтобы проект не
             * запрашивался из Mongo повторно
             */
            ActiveProjectEntitiesStreamSerializer projectEntitiesStreamSerializer = projectViewService
                    .getActiveProjectEntitiesSerializer(project.getId(),
                    language,
                    errorMessage);
            if (projectEntitiesStreamSerializer != null) {
                return new ResponseEntity<StreamingResponseBody>(
                        new ProjectEntitiesStreamingResponseBody(projectEntitiesStreamSerializer),
                        streamingResponseHeaders, HttpStatus.OK);
            } else {
                return new ResponseEntity<StreamingResponseBody>(
                        new ErrorStreamingResponseBody(errorMessage.toString()), streamingResponseHeaders,
                        HttpStatus.BAD_REQUEST);
            }
        }
    }

    @PreAuthorize("hasRole('PROJECT_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<?> removeProjectById(@PathVariable String id) {

        StringBuilder errorMessage = new StringBuilder();
        if (projectViewService.remove(id, errorMessage))
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasEventRole('PROJECT_STATE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{projectId}", params = "status")
    public ResponseEntity<StreamingResponseBody> updateProjectStatus(@PathVariable String projectId,
            @RequestParam(name = "status", required = true) ProjectStatus status) {

        StringBuilder errorMessage = new StringBuilder();
        if (status == ProjectStatus.ACTIVE) {
            if (!projectService.setActive(projectId, errorMessage)) {
                return new ResponseEntity<>(
                        new ErrorStreamingResponseBody(errorMessage.toString()), streamingResponseHeaders,
                        HttpStatus.BAD_REQUEST);
            }
        } else {
            if (!projectService.setBuild(projectId, errorMessage)) {
                return new ResponseEntity<>(
                        new ErrorStreamingResponseBody(errorMessage.toString()), streamingResponseHeaders,
                        HttpStatus.BAD_REQUEST);
            } else {
                List<Issue> issues = issueRepository.deleteByProjectId(projectId);
                for (Issue issue: issues) {
                    issue.setStatusMessage("Отменена при отключении проекта");
                }
                issueController.sendCanceledIssues(issues);
            }
        }
        BasicProjectEntitiesStreamSerializer projectEntitiesStreamSerializer;
        if (status == ProjectStatus.ACTIVE) {
            projectViewService.setFilterTagsOnEntities(projectId);
            projectEntitiesStreamSerializer = projectViewService.getActiveProjectEntitiesSerializer(projectId, language,
                    errorMessage);
        } else {
            projectEntitiesStreamSerializer = projectViewService.getProjectEntitiesSerializer(projectId, language,
                    errorMessage);
        }
        if (projectEntitiesStreamSerializer != null) {
            eventService.commitProjectEvent(status, findCurrentUser(), true);
            return new ResponseEntity<>(
                    new ProjectEntitiesStreamingResponseBody(projectEntitiesStreamSerializer,
                            new WriteFinishedHandler() {

                                @Override
                                public void onWriteFinished() {
                                    ActiveProjectUpdated activeProjectUpdated = new ActiveProjectUpdated();
                                    activeProjectUpdated.setProjectId(projectId);
                                    alertSender.send(activeProjectUpdated);
                                }
                            }),
                    streamingResponseHeaders,
                    HttpStatus.OK);
        } else {
            eventService.commitProjectEvent(status, findCurrentUser(), true);
            return new ResponseEntity<>(new ErrorStreamingResponseBody(errorMessage.toString()),
                    streamingResponseHeaders,
                    HttpStatus.BAD_REQUEST);
        }

    }

    @PreAuthorize("hasRole('PROJECT_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = {"/{id}"})
    public ResponseEntity<?> updateProjectByNameAndVersion(@RequestBody Project project, @PathVariable("id") String projectId ) {

        StringBuilder errorMessage = new StringBuilder();
        Project updatedProject = projectViewService.update(projectId, project, errorMessage);
        if(updatedProject != null) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(updatedProject);
        } else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('PROJECT_UPDATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"/{id}/entities"})
    public ResponseEntity<?> importControlDeviceEntities(@PathVariable("id") String projectId,
            @RequestBody ControlDeviceEntities entities) {

        StringBuilder errorMessage = new StringBuilder();
        UpdatedEntities updatedEntities = projectViewService.addEntities(projectId, entities, language, errorMessage);
        if (updatedEntities != null) {
            return ResponseEntity.status(HttpStatus.OK).body(updatedEntities);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('PROJECT_IMPORT')")
    @RequestMapping(method = RequestMethod.POST, value = {"/importNt"})
    public ResponseEntity<?> importProject(@RequestBody ProjectWrapper projectWrapper) {

        StringBuilder errorMessage = new StringBuilder();
        ProjectWrapper importedProject = projectWrapper;
        if (importedProject.getProjectBuild().equals(projectVersion)) {
            Project project = new Project();
            project.setDescription(importedProject.getProject().getDescription());
            project.setName(importedProject.getProject().getName());
            project.setVersion(importedProject.getProject().getVersion());
            Project projectSaved = projectViewService.add(project, errorMessage);
            if (projectSaved != null) {
                String projectId = projectSaved.getId();
                Map<String, String> oldAndNewDevicesId = new HashMap<>();
                Map<String, String> oldAndNewRegionsId = new HashMap<>();
                Map<String, String> oldAndNewVirtualStatesId = new HashMap<>();
                Map<String, String> oldAndNewPlansId = new HashMap<>();
                Map<String, String> oldAndNewPlanGroupsId = new HashMap<>();
                Map<String, String> oldAndNewScenariosId = new HashMap<>();
                Map<String, String> oldAndNewWorkScheduleId = new HashMap<>();
                Map<String, String> oldAndNewEmployeesId = new HashMap<>();
                if (planGroupService.checkAndAdditionPlanGroupToImportProject(projectId,
                        importedProject.getPlanGroups(), oldAndNewPlanGroupsId)
                        && regionService.checkAndAdditionRegionToImportProject(projectId, importedProject.getRegions(),
                                oldAndNewRegionsId, errorMessage)
                        && planService.checkAndAdditionPlanToImportProject(importedProject.getPlans(), projectId,
                                oldAndNewPlansId, oldAndNewPlanGroupsId, errorMessage)
                        && regionViewService.checkAndAdditionRegionViewToImportProject(projectId,
                                importedProject.getRegionViews(), oldAndNewPlansId, oldAndNewRegionsId, errorMessage)
                        && deviceService.checkAndAdditionDeviceToImportProject(importedProject.getDevices(), projectId,
                                oldAndNewDevicesId, oldAndNewRegionsId, errorMessage)
                        && deviceViewService.checkAndAdditionDeviceViewToImportProject(projectId,
                                importedProject.getDeviceViews(), oldAndNewPlansId, oldAndNewDevicesId, errorMessage)
                        && virtualStateService.checkAndAdditionVirtualStateToImportProject(
                                importedProject.getVirtualStates(), projectId, oldAndNewVirtualStatesId,
                                oldAndNewDevicesId, errorMessage)
                        && scenarioViewService.checkAndAdditionScenarioViewToImportProject(projectId,
                                importedProject.getScenarios(), oldAndNewScenariosId, oldAndNewDevicesId,
                                oldAndNewRegionsId, oldAndNewVirtualStatesId, errorMessage)
                        && workScheduleService.checkAndAdditionWorkScheduleToImportProject(projectId,
                                importedProject.getWorkSchedules(), oldAndNewWorkScheduleId, errorMessage)
                        && employeeService.checkAndAdditionEmployeeToImportProject(projectId,
                                importedProject.getEmployees(), oldAndNewEmployeesId, oldAndNewDevicesId,
                                oldAndNewWorkScheduleId, oldAndNewRegionsId, errorMessage)
                        && accessKeyService.checkAndAdditionAccessKeyToImportProject(projectId,
                                importedProject.getAccessKeys(), oldAndNewEmployeesId, oldAndNewDevicesId,
                                oldAndNewWorkScheduleId, oldAndNewRegionsId, errorMessage)) {
                    return ResponseEntity.status(HttpStatus.CREATED).body(projectSaved);
                } else {
                    projectViewService.remove(projectId, errorMessage);
                    return ResponseEntity.badRequest().body(new ErrorResponse("Не удалось импортировать проект, файл не прошел валидацию: " + errorMessage));
                }
            }
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Не удалось импортировать проект, версия проекта отличается от текущей версии ПО"));
        }
    }

    @PreAuthorize("hasRole('PROJECT_IMPORT')")
    @RequestMapping(method = RequestMethod.POST, value = {"/importNg"}, params = { "project_name"})
    public ResponseEntity<?> importProject(@RequestBody ImportedXmlProjectWrapper importedFile,
            @RequestParam(name = "project_name")  String fileName) {

            StringBuilder errorMessage = new StringBuilder();
            Project projectSaved = projectViewService.addImpotredNgProjectView(fileName, importedFile, errorMessage);
            if(projectSaved != null) {
                return ResponseEntity.status(HttpStatus.CREATED).body(projectSaved);
            }
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('PROJECT_EXPORT')")
    @RequestMapping(method = RequestMethod.POST, value = {"/{projectId}/export"})
    public ResponseEntity<?> exportProject(@PathVariable String projectId) {

        Project project = projectRepository.findOne(projectId);
        if(project != null) {
            project.setCreateDateTime(null);
            project.setUpdateDateTime(null);
            ProjectWrapper exportProject = new ProjectWrapper();
            exportProject.setProjectBuild(projectVersion);
            exportProject.setProject(project);
            exportProject.setDevices(deviceRepository.findByProjectId(projectId));
            exportProject.setDeviceViews(deviceViewRepository.findByProjectId(projectId));
            exportProject.setRegionViews(regionViewpository.findByProjectId(projectId));
            exportProject.setRegions(regionRepository.findByProjectId(projectId));
            exportProject.setScenarios(scenarioRepository.findByProjectId(projectId));
            exportProject.setVirtualStates(virtualStateRepository.findByProjectId(projectId));
            exportProject.setPlans(planRepository.findByProjectId(projectId));
            exportProject.setPlanGroups(planGroupRepository.findByProjectId(projectId));
            exportProject.setWorkSchedules(workScheduleRepository.findByProjectIdAndRemovedIsFalse(projectId));
            exportProject.setEmployees(employeeRepository.findByProjectIdAndRemovedIsFalse(projectId));
            exportProject.setAccessKeys(accessKeyRepository.findByProjectIdAndRemovedIsFalse(projectId));
            return ResponseEntity.status(HttpStatus.OK).body(exportProject);
        }else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Не удалось экспортировать проект"));
        }
    }

}
