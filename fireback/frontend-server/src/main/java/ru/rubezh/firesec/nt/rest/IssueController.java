package ru.rubezh.firesec.nt.rest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.amqp.message.request.CreateIssueDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.DeleteTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.PauseTaskDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.response.CreateIssueDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.DeleteTaskDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;
import ru.rubezh.firesec.nt.amqp.message.response.PauseTaskDriverResponse;
import ru.rubezh.firesec.nt.amqp.sender.DriverRoutedRequestSender;
import ru.rubezh.firesec.nt.dao.v1.DriverInstanceRepository;
import ru.rubezh.firesec.nt.dao.v1.IssueRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverInstance;
import ru.rubezh.firesec.nt.domain.v1.Issue;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.security.AuthenticatedComponent;
import ru.rubezh.firesec.nt.service.v1.EventService;
import ru.rubezh.firesec.nt.service.v1.IssueService;
import ru.rubezh.firesec.nt.ws.WebSocketTranslator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/issues")
public class IssueController extends AuthenticatedComponent {

    @Autowired
    private Logger logger;
    @Autowired
    private DriverInstanceRepository driverInstanceRepository;
    @Autowired
    private IssueRepository issueRepository;
    @Autowired
    private IssueService issueService;
    @Autowired
    private DriverRoutedRequestSender driverRoutedRequestSender;
    @Autowired
    private WebSocketTranslator wsTranslator;
    @Autowired
    private EventService eventService;

    @PreAuthorize("hasEventRole('ISSUE_CREATE')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addIssue(@RequestBody Issue issue) {
        StringBuilder errorMessage = new StringBuilder();
        if (issueService.validate(issue, errorMessage)) {
            CreateIssueDriverRequest request = issueService.getIssueRequestToDriver(issue, errorMessage);
            if (request != null) {
                CreateIssueDriverResponse response = (CreateIssueDriverResponse) driverRoutedRequestSender
                        .sendAndReceive(request.getDriverRequestQueueName(), request);
                if (response != null) {
                    List<Issue> createdIssues = issueService.saveIssuesByDriverResponse(response, findCurrentUser(),
                            errorMessage);
                    if (createdIssues != null) {
                        eventService.commitIssueEvent(issue, findCurrentUser(), true);
                        wsTranslator.send(createdIssues, Issue.class);
                        return ResponseEntity.ok(createdIssues);
                    }
                } else {
                    errorMessage.append("Не получен ответ от драйвера");
                }
            }
        }
        eventService.commitIssueEvent(issue, findCurrentUser(), false);
        return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('ISSUE_READ')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getIssues() {
        return ResponseEntity.ok(issueRepository.findAllByOrderByCreateDateTimeDesc());
    }

    private void sendCanceledIssue (Issue issue){
        issue.setStatus(IssueStatus.CANCELED);
        if (issue.getStatusMessage().isEmpty()) {
            issue.setStatusMessage("Удалена пользователем");
        }
        wsTranslator.send(issue);
    }

    void sendCanceledIssues(List<Issue> issues) {
        for (Issue issue : issues) {
            issue.setStatus(IssueStatus.CANCELED);
            if (issue.getStatusMessage().isEmpty()) issue.setStatusMessage("Удалена пользователем");
        }
        wsTranslator.send(issues, Issue.class);
    }

    @PreAuthorize("hasRole('ISSUE_REMOVE')")
    @RequestMapping(value = "", method = RequestMethod.DELETE, params = { "finished", "!error", "!queue" })
    public ResponseEntity<?> deleteFinishedIssues() {
        List<Issue> deletedIssues = issueRepository.deleteByStatus(IssueStatus.FINISHED);
        /* Отправим по WS отмену задач */
        sendCanceledIssues(deletedIssues);
        List<String> deletedIssueIds = deletedIssues.stream().map(Issue::getId).collect(Collectors.toList());
        return ResponseEntity.ok(deletedIssueIds);
    }

    @PreAuthorize("hasRole('ISSUE_REMOVE')")
    @RequestMapping(value = "", method = RequestMethod.DELETE, params = { "error", "!finished", "!queue" })
    public ResponseEntity<?> deleteErrorIssues() {
        List<Issue> deletedIssues = issueRepository.deleteByStatus(IssueStatus.FAILED);
        /* Отправим по WS отмену задач */
        sendCanceledIssues(deletedIssues);
        List<String> deletedIssueIds = deletedIssues.stream().map(Issue::getId).collect(Collectors.toList());
        return ResponseEntity.ok(deletedIssueIds);
    }

    @PreAuthorize("hasRole('ISSUE_REMOVE')")
    @RequestMapping(value = "", method = RequestMethod.DELETE, params = { "queue", "!finished", "!error" })
    public ResponseEntity<?> deleteQueueIssues() {
        List<String> deletedIssueIds = new ArrayList<>();
        List<DriverInstance> instances = driverInstanceRepository.findAll();
        for (DriverInstance instance : instances) {
            /* Запрос к драйверу на удаление задач из очереди. */
            DeleteTaskDriverRequest request = new DeleteTaskDriverRequest();
            request.setDriverId(instance.getId());
            try {
                DeleteTaskDriverResponse response = (DeleteTaskDriverResponse)
                        driverRoutedRequestSender.sendAndReceive(instance.getRequestQueueName(), request);
                deletedIssueIds.addAll(response.getDeletedIssueIds());
            } catch (ClassCastException e) {
                logger.warn("Response has bad type (driver profile: '{}')", instance.getProfileId());
            }
        }

        List<Issue> deletedIssues = issueRepository.deleteByIdIn(deletedIssueIds);
        /* Отправим по WS отмену задач */
        sendCanceledIssues(deletedIssues);
        return ResponseEntity.ok(deletedIssueIds);
    }

    @PreAuthorize("hasRole('ISSUE_REMOVE')")
    @RequestMapping(value = "/{issueId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteIssue(@PathVariable String issueId) {
        Issue issue = issueRepository.findOne(issueId);
        if (issue == null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else if (issue.getStatus() == IssueStatus.FINISHED || issue.getStatus() == IssueStatus.FAILED) {
            issueRepository.delete(issueId);
            sendCanceledIssue(issue);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            /* Запрос к драйверу на удаление задачи. */
            DeleteTaskDriverRequest request = new DeleteTaskDriverRequest();
            request.setIssueId(issue.getId());
            String driverRequestQueueName = issueService.routeRequestToDriverByDeviceId(request, issue.getDeviceId());
            if (driverRequestQueueName == null || request.getDriverId() == null) {
                logger.warn("Can't find driver for device '{}'", issue.getDeviceId());
                issueRepository.delete(issueId);
                sendCanceledIssue(issue);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                DeleteTaskDriverResponse driverResponse = (DeleteTaskDriverResponse)
                        driverRoutedRequestSender.sendAndReceive(driverRequestQueueName, request);
                if (driverResponse.getError().equals(ErrorType.OK)) {
                    issueRepository.delete(issueId);
                    sendCanceledIssue(issue);
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    return ResponseEntity.badRequest().body(new ErrorResponse(driverResponse.getErrorString()));
                }
            }
        }
    }

    @PreAuthorize("hasRole('ISSUE_UPDATE')")
    @RequestMapping(value = "/{issueId}", method = RequestMethod.PUT, params = { "play", "!pause" })
    public ResponseEntity<?> playIssue(@PathVariable String issueId) {
        Issue issue = issueRepository.findOne(issueId);
        if (issue == null) {
            logger.warn("Issue not find. id: '{}'", issueId);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResponse("Задача не найдена"));
        } else {
            /* Запрос к драйверу на разрешение выполнения задачи.
             * Если задачи нет, то вернуть ошибку.
             */
            PauseTaskDriverRequest request = new PauseTaskDriverRequest();
            request.setIssueId(issue.getId());
            request.setPause(false);

            String driverRequestQueueName = issueService.routeRequestToDriverByDeviceId(request, issue.getDeviceId());
            if (driverRequestQueueName == null || request.getDriverId() == null) {
                logger.warn("Не найден драйвер для устройства '{}'", issue.getDeviceId());
                wsTranslator.send(issueService.setIssueProgress(issue, IssueStatus.CREATED, "", 0));
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                PauseTaskDriverResponse driverResponse = (PauseTaskDriverResponse)
                        driverRoutedRequestSender.sendAndReceive(driverRequestQueueName, request);
                if (driverResponse.getError().equals(ErrorType.OK)) {
                    wsTranslator.send(issueService.setIssueProgress(issue, IssueStatus.CREATED, "", 0));
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    return ResponseEntity.badRequest().body(new ErrorResponse(driverResponse.getErrorString()));
                }
            }
        }
    }

    @PreAuthorize("hasRole('ISSUE_UPDATE')")
    @RequestMapping(value = "/{issueId}", method = RequestMethod.PUT, params = { "pause", "!play" })
    public ResponseEntity<?> pauseIssue(@PathVariable String issueId) {
        Issue issue = issueRepository.findOne(issueId);
        if (issue == null) {
            logger.warn("Задача не найдена. id: '{}'", issueId);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResponse("Задача не найдена"));
        } else {
            /* Запрос к драйверу на запрет выполнения задачи.
             * Если задачу уже нельзя отменить (в работе), то вернуть ошибку.
             */
            PauseTaskDriverRequest request = new PauseTaskDriverRequest();
            request.setIssueId(issue.getId());
            request.setPause(true);

            String driverRequestQueueName = issueService.routeRequestToDriverByDeviceId(request, issue.getDeviceId());
            if (driverRequestQueueName == null || request.getDriverId() == null) {
                logger.warn("Не найден драйвер для устройства '{}'", issue.getDeviceId());
                wsTranslator.send(issueService.setIssueProgress(issue, IssueStatus.PAUSE, "", 0));
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                PauseTaskDriverResponse driverResponse = (PauseTaskDriverResponse)
                        driverRoutedRequestSender.sendAndReceive(driverRequestQueueName, request);
                if (driverResponse.getError().equals(ErrorType.OK)) {
                    wsTranslator.send(issueService.setIssueProgress(issue, IssueStatus.PAUSE, "", 0));
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    return ResponseEntity.badRequest().body(new ErrorResponse(driverResponse.getErrorString()));
                }
            }
        }
    }

}
