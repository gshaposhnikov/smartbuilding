package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.service.v1.LogMonitorableValueService;

import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/devices/{deviceId}/monitorable_values")
public class MonitorableValueController {

    @Autowired
    private LogMonitorableValueService logMonitorableValueService;

    /** Запрос списка всех значений набл. параметров по устройству за промежуток времени */
    @RequestMapping(method = RequestMethod.GET, value = "")
    public ResponseEntity<?> getMonitorableValuesByDeviceIdAndReceivedRange(
            @PathVariable String projectId, @PathVariable String deviceId,
            @RequestParam(name = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from,
            @RequestParam(name = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date to,
            Pageable pageable) {

        return ResponseEntity.ok(logMonitorableValueService.getByProjectIdAndDeviceAndReceivedRange(
                projectId, deviceId, from, to, pageable));
    }

}
