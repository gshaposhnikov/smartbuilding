package ru.rubezh.firesec.nt.rest;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.dao.v1.DeviceViewRepository;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceView;
import ru.rubezh.firesec.nt.domain.v1.DeviceView.DevicePlanLayout;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;
import ru.rubezh.firesec.nt.domain.v1.representation.DeviceCreateView;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.service.v1.DeviceService;
import ru.rubezh.firesec.nt.service.v1.DeviceViewService;
import ru.rubezh.firesec.nt.service.v1.DeviceViewService.CreatedAndUpdatedEntities;
import ru.rubezh.firesec.nt.service.v1.DeviceViewService.RemovedAndUpdatedEntities;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/devices")
public class DeviceViewController {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @PreAuthorize("hasRole('DEVICE_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> saveDevice(@PathVariable String projectId,
            @RequestBody DeviceCreateView deviceInfo) {

        StringBuilder errorMessage = new StringBuilder();
        CreatedAndUpdatedEntities createdAndUpdatedEntities = deviceViewService
                .add(deviceInfo, projectId, language, errorMessage);
        if (createdAndUpdatedEntities != null) {
            return ResponseEntity.ok(createdAndUpdatedEntities);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceId}" }, params = { "config" })
    public ResponseEntity<?> updateDeviceProjectConfig(@PathVariable String projectId,
            @PathVariable String deviceId,
            @RequestBody Map<String, String> propertyValues) {
        StringBuilder errorMessage = new StringBuilder();
        Device device = deviceService.setDeviceProjectConfig(deviceId, projectId, propertyValues, errorMessage);
        if (device != null) {
            TreeItemDeviceView deviceTreeItemView = deviceViewService
                    .getDeviceTreeItemByProjectIdAndDeviceId(projectId, deviceId, language);
            return ResponseEntity.ok(deviceTreeItemView);
        }  else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceId}" }, params = { "properties"})
    public ResponseEntity<?> updateDeviceProperties(@PathVariable String projectId,
            @PathVariable String deviceId,
            @RequestBody Map<String, String> propertyValues) {

        StringBuilder errorMessage = new StringBuilder();
        TreeItemDeviceView deviceTreeItemView = deviceViewService.updateDeviceProperties(projectId, deviceId,
                propertyValues, language, errorMessage);
        if (deviceTreeItemView != null) {
            return ResponseEntity.ok(deviceTreeItemView);
        }  else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "", "/{deviceId}" }, params = { "device_profile_id"})
    public ResponseEntity<?> updateDeviceProfile(@PathVariable String projectId,
            @PathVariable String deviceId,
            @RequestParam(name = "device_profile_id") String deviceProfileId
            ) {

        CreatedAndUpdatedEntities createdAndUpdatedEntities = null;
        StringBuilder errorMessage = new StringBuilder();

        createdAndUpdatedEntities = deviceViewService.updateDeviceProfile(projectId,
                deviceId, deviceProfileId, language, errorMessage);

        if (createdAndUpdatedEntities != null) {
            return ResponseEntity.ok(createdAndUpdatedEntities);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "", "/{deviceId}" }, params = { "address"})
    public ResponseEntity<?> updateDeviceAddress(@PathVariable String projectId,
            @PathVariable String deviceId,
            @RequestParam(name = "address") String shortAddressPath) {

        CreatedAndUpdatedEntities createdAndUpdatedEntities = null;
        StringBuilder errorMessage = new StringBuilder();

        createdAndUpdatedEntities = deviceViewService.updateDeviceAddress(projectId,
                deviceId, shortAddressPath, language, errorMessage);

        if (createdAndUpdatedEntities != null) {
            return ResponseEntity.ok(createdAndUpdatedEntities);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "", "/{deviceId}" }, params = { "plan_layouts"})
    public ResponseEntity<?> updateDevicePlanLayouts(@PathVariable String projectId, @PathVariable String deviceId,
            @RequestBody List<DevicePlanLayout> planLayouts) {

        StringBuilder errorMessage = new StringBuilder();

        TreeItemDeviceView treeItemDeviceView = deviceViewService.updateDevicePlanLayouts(projectId, deviceId,
                planLayouts, language, errorMessage);

        if (treeItemDeviceView != null) {
            return ResponseEntity.ok(treeItemDeviceView);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceId}" }, params = { "disabled" })
    public ResponseEntity<?> updateDeviceDisabled(@PathVariable String projectId,
            @PathVariable String deviceId, @RequestParam boolean disabled) {

        StringBuilder errorMessage = new StringBuilder();
        TreeItemDeviceView deviceTreeItemView = deviceViewService.updateDeviceDisabled(projectId, deviceId,
                disabled, language, errorMessage);
        if (deviceTreeItemView != null) {
            return ResponseEntity.ok(deviceTreeItemView);
        }  else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceId}" }, params = { "links" })
    public ResponseEntity<?> updateVirtualDeviceLinks(@PathVariable String projectId, @PathVariable String deviceId,
            @RequestBody List<String> deviceLinks) {
        StringBuilder errorMessage = new StringBuilder();
        List<TreeItemDeviceView> deviceTreeItemViews = deviceViewService.updateDeviceLinks(projectId, deviceId,
                deviceLinks, language, errorMessage);
        if (deviceTreeItemViews != null) {
            return ResponseEntity.ok(deviceTreeItemViews);
        }  else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getDevicesByProjectId(@PathVariable String projectId) {

        List<TreeItemDeviceView> deviceViews;
        deviceViews = deviceViewService.getTreeItemDeviceViewsByProjectId(projectId, language);
        return ResponseEntity.ok(deviceViews);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{deviceId}")
    public ResponseEntity<?> getDeviceById(@PathVariable String projectId,
            @PathVariable String deviceId) {

        DeviceView result = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (result != null) {
            return ResponseEntity.ok(result);
        } else {
            logger.error("Отображение устройства не найдено: {}", deviceId);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/parent/{parentId}")
    public ResponseEntity<?> getDevicesByParentId(@PathVariable String projectId,
            @PathVariable String parentId) {

        /* TODO: сделать возврат отображений для дерева через сервис */
        return ResponseEntity
                .ok(deviceViewRepository.findByProjectIdAndParentDeviceId(projectId, parentId));
    }

    @PreAuthorize("hasRole('DEVICE_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{deviceId}")
    public ResponseEntity<?> removeDeviceById(@PathVariable String projectId,
            @PathVariable String deviceId) {

        StringBuilder errorMessage = new StringBuilder();
        RemovedAndUpdatedEntities removedAndUpdatedEntities = deviceViewService
                .remove(projectId, deviceId, language, errorMessage);
        if (removedAndUpdatedEntities != null) {
            return ResponseEntity.ok(removedAndUpdatedEntities);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @PutMapping(value = {"/{deviceId}"}, params = {"shape_library"})
    public ResponseEntity<?> updateDeviceShapeLibrary(@PathVariable String projectId,
                                                      @PathVariable String deviceId,
                                                      @RequestParam(name = "shape_library") String shapeLibraryId){
        StringBuilder errorMessage = new StringBuilder();
        TreeItemDeviceView result = deviceViewService
                .updateDeviceShapeLibrary(projectId, deviceId, shapeLibraryId, language, errorMessage);
        if (result != null){
            return ResponseEntity.ok(result);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @PutMapping(value = {"/{deviceId}"}, params = {"custom_subsystem"})
    public ResponseEntity<?> updateCustomSubsystem(@PathVariable String projectId,
                                                   @PathVariable String deviceId,
                                                   @RequestParam(name = "custom_subsystem") Subsystem customSubsystem) {
        StringBuilder errorMessage = new StringBuilder();

        CreatedAndUpdatedEntities createdAndUpdatedEntities = deviceViewService
                .updateCustomSubsystem(projectId, deviceId, customSubsystem, language, errorMessage);

        if (createdAndUpdatedEntities != null) {
            return ResponseEntity.ok(createdAndUpdatedEntities);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
