package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rubezh.firesec.nt.dao.v1.StateCategoryViewRepository;
import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/state_categories")
public class StateCategoryController {

    @Autowired
    private StateCategoryViewRepository stateCategoryViewRepository;

    @RequestMapping(value = {"", "/"})
    public ResponseEntity<?> getAll() {

        List<StateCategoryView> result = stateCategoryViewRepository.findAll();
        if (result != null) {
            return ResponseEntity.ok(result);
        } else
            return ResponseEntity.badRequest().body(new ErrorResponse("Ошибка получения данных из базы"));
    }

}
