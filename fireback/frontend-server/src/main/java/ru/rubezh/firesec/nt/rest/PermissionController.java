package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.rubezh.firesec.nt.domain.v1.representation.PermissionsView;
import ru.rubezh.firesec.nt.service.v1.PermissionService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/permissions")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /** Запрос отображений прав доступа */
    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getPermissionsView() {
        return ResponseEntity.ok(new PermissionsView(permissionService.getPermissions(),
                permissionService.getPermissionTargets(), permissionService.getPermissionAccessLevels()));
    }

}
