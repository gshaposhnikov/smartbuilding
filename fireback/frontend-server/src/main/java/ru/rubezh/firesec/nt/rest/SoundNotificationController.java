package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.dao.v1.SoundNotificationRepository;
import ru.rubezh.firesec.nt.domain.v1.SoundNotification;
import ru.rubezh.firesec.nt.service.v1.SoundNotificationService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/sound_notifications")
public class SoundNotificationController {

    @Autowired
    private SoundNotificationRepository soundNotificationRepository;

    @Autowired
    private SoundNotificationService soundNotificationServcie;

    @PreAuthorize("hasRole('SOUND_NOTIFICATION_READ')")
    @GetMapping(value = { "", "/" })
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(soundNotificationRepository.findAll());
    }

    @PreAuthorize("hasRole('SOUND_NOTIFICATION_READ')")
    @GetMapping(value = {"/{id}"})
    public ResponseEntity<?> getById(@PathVariable String id){
        SoundNotification media = soundNotificationRepository.findOne(id);
        if (media != null) {
            return ResponseEntity.ok(media);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Медиафайл не найден"));
        }
    }

    @PreAuthorize("hasRole('SOUND_NOTIFICATION_CREATE')")
    @PostMapping
    public ResponseEntity<?> addSoundNotification(@RequestBody SoundNotification soundNotification){
        StringBuilder errorMessage = new StringBuilder();
        if (soundNotificationServcie.addSoundNotification(soundNotification, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SOUND_NOTIFICATION_UPDATE')")
    @PutMapping(value = {"/{id}"})
    public ResponseEntity<?> updateMedia(@PathVariable String id, @RequestBody SoundNotification soundNotification){
        StringBuilder errorMessage = new StringBuilder();
        if (soundNotificationServcie.updateSoundNotification(id, soundNotification, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SOUND_NOTIFICATION_UPDATE')")
    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<?> deleteMedia(@PathVariable String id){
        StringBuilder errorMessage = new StringBuilder();
        if (soundNotificationServcie.deleteSoundNotification(id, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }
}
