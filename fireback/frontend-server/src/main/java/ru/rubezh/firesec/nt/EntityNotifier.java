package ru.rubezh.firesec.nt;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedActiveScenarioView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.ScenarioComputerAction;
import ru.rubezh.firesec.nt.service.v1.*;
import ru.rubezh.firesec.nt.ws.WebSocketTranslator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EntityNotifier implements UpsertEntitiesProcessor {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private AggregatedEventViewRepository aggregatedEventViewRepository;

    @Autowired
    private EventViewService eventViewService;

    @Autowired
    private WebSocketTranslator wsTranslator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private DeviceShapeLibraryService deviceShapeLibraryService;

    @Autowired
    private DeviceShapeService deviceShapeService;

    @Autowired
    private LogViewService logViewService;

    @Autowired
    private MediaService mediaService;

    @Autowired
    private SoundNotificationService soundNotificationService;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private ActiveSubsystemRepository activeSubsystemRepository;

    @Autowired
    private ActiveProjectRepository activeProjectRepository;

    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private RegionViewService regionViewService;

    @Autowired
    private SubsystemViewService subsystemViewService;

    @Autowired
    private ScenarioViewService scenarioViewService;

    @Autowired
    private List<ProjectEntityService<?, ?>> projectEntityServices;

    /** Найти новые события и отправить оповещения */
    private List<AggregatedEventView> getNewEvents() {
        /* Поиск среди представлений события, которое было сохранено позже всего */
        AggregatedEventView lastEventView = aggregatedEventViewRepository.findFirstByOrderByIdDesc();
        ObjectId lastEventObjectId = null;
        if (lastEventView == null) {
            // коллекция пуста
            lastEventObjectId = new ObjectId(new Date(0), 0);
        } else {
            lastEventObjectId = new ObjectId(lastEventView.getId());
        }

        /* Поиск событий, для которых нет представления */
        List<Event> events = eventRepository.findFirst100ByIdAfterOrderByIdAsc(lastEventObjectId);

        /* Сохраняем представления событий и подготавливаем набор для отправки */
        List<AggregatedEventView> aggregatedEventViews = eventViewService.commitAggregatedEventViews(events);
        if (aggregatedEventViews != null && aggregatedEventViews.size() != events.size()) {
            List<String> savedIds = aggregatedEventViews.stream().map(AggregatedEventView::getId).collect(Collectors.toList());
            logger.error("Can't save event view (ids: {})", events.removeIf(event -> !savedIds.contains(event.getId())));
        }
        return aggregatedEventViews;
    }

    /** Отправить оповещение по всем обновлённым и новым пользователям
     * @param lastObservedTime время последнего оповещения
     */
    private List<User> getNewAndUpdatedUsers(Date lastObservedTime) {
        List<User> users = userRepository.findByUpdateDateTimeAfter(lastObservedTime);
        return (users.size() > 0 ? users : null);
    }

    /** Отправить оповещение по всем обновлённым, новым и удалённым группам пользователей
     * @param lastObservedTime время последнего оповещения
     */
    private UpdatedEntities.EntitiesContainer<UserGroup> getNewAndUpdatedUserGroups(Date lastObservedTime) {
        UpdatedEntities.EntitiesContainer<UserGroup> result = new UpdatedEntities.EntitiesContainer<>();
        List<UserGroup> userGroups = userGroupRepository.findByRemovedIsFalseAndUpdateDateTimeAfter(lastObservedTime);
        result.setUpdated(userGroups.size() > 0 ? userGroups : null);

        userGroups = userGroupRepository.findByRemovedIsFalseAndCreateDateTimeAfter(lastObservedTime);
        result.setCreated(userGroups.size() > 0 ? userGroups : null);

        userGroups = userGroupRepository.findByRemovedIsTrue();
        if (userGroups.size() > 0) {
            List<String> deletedIssueIds = userGroups.stream().map(UserGroup::getId).collect(Collectors.toList());
            userGroupRepository.deleteByIdIn(deletedIssueIds);
            result.setDeletedIds(deletedIssueIds);
        }

        return result.isEmpty() ? null : result;
    }

    private boolean checkStatesAndNotify(Date lastObservedTime) {

        /* получаем из БД сущности, состояние которых изменилось */
        int nUpdatedDevices = activeDeviceRepository.countByStateGetDateTimeAfter(lastObservedTime);
        int nUpdatedRegions = activeRegionRepository.countByStateGetDateTimeAfter(lastObservedTime);
        List<ActiveSubsystem> activeSubsystems = activeSubsystemRepository
                .findByStateGetDateTimeAfter(lastObservedTime);
        List<ActiveProject> activeProjects = activeProjectRepository.findByStateGetDateTimeAfter(lastObservedTime);
        int nUpdatedScenarios = activeScenarioRepository.countByStateGetDateTimeAfter(lastObservedTime);

        boolean somethingNotified = nUpdatedDevices > 0 || nUpdatedRegions > 0 || !activeSubsystems.isEmpty()
                || !activeProjects.isEmpty() || nUpdatedScenarios > 0;

        /*
         * отправляем уведомления по каждому устройству, запоминая дату/время сессии наблюдения по самой поздней дате
         * обновления устройства
         */
        if (nUpdatedDevices > 0) {
            int nDevicePages = nUpdatedDevices / DeviceViewServiceImpl.AGGREGATED_ACTIVE_DEVICES_SAFE_PAGE_SIZE + 1;
            List<TreeItemActiveDeviceView> deviceViews = new ArrayList<>();
            for (int indexPage = 0; indexPage < nDevicePages; indexPage++) {
                deviceViews.addAll(deviceViewService.getActiveDeviceTreeItemsByTimeAfter(lastObservedTime,
                        DeviceViewServiceImpl.AGGREGATED_ACTIVE_DEVICES_SAFE_PAGE_SIZE, indexPage));
            }
            wsTranslator.send(deviceViews, TreeItemActiveDeviceView.class);
        }

        /* отправляем уведомления по каждой обновленной зоне */
        if (nUpdatedRegions > 0){
            int nRegionPages = nUpdatedRegions / RegionViewServiceImpl.AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE + 1;
            List<AggregatedActiveRegionView> regionViews = new ArrayList<>();
            for (int indexPage = 0; indexPage < nRegionPages; indexPage++) {
                regionViews.addAll(regionViewService.getActiveRegionViewsByTimeAfter(lastObservedTime,
                        RegionViewServiceImpl.AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE, indexPage));
            }
            wsTranslator.send(regionViews, AggregatedActiveRegionView.class);
        }

        /* отправляем уведомления по каждой подсистеме */
        for (ActiveSubsystem activeSubsystem : activeSubsystems) {
            SubsystemView subsystemView = subsystemViewService.getActiveSubsystem(activeSubsystem.getSubsystem());
            if (subsystemView != null)
                wsTranslator.send(subsystemView);
        }

        /*
         * Отправляем уведомления по статусам активных проектов (пока он может быть только один, но на будущее можно
         * оставить цикл)
         */
        for (ActiveProject activeProject : activeProjects) {
            wsTranslator.send(activeProject);
        }

        /* отправляем уведомления по каждому изменившемуся сценарию */
        if (nUpdatedScenarios > 0){
            int nScenarioPages = nUpdatedScenarios /
                    ScenarioViewServiceImpl.AGGREGATED_ACTIVE_SCENARIOS_SAFE_PAGE_SIZE + 1;
            for (int indexPage = 0; indexPage < nScenarioPages; indexPage++) {
                List<AggregatedActiveScenarioView> aggregatedActiveScenarioViews = scenarioViewService
                        .getAggregatedActiveScenarioViewsByTimeAfter(lastObservedTime,
                                ScenarioViewServiceImpl.AGGREGATED_ACTIVE_SCENARIOS_SAFE_PAGE_SIZE, indexPage);

                wsTranslator.send(aggregatedActiveScenarioViews, AggregatedActiveScenarioView.class);

                for (AggregatedActiveScenarioView activeScenarioView: aggregatedActiveScenarioViews) {
                    List<ScenarioComputerAction> scenarioComputerActions = scenarioViewService
                            .getComputerActionsFromScenario(activeScenarioView);
                    for (ScenarioComputerAction computerAction: scenarioComputerActions) {
                        wsTranslator.send(computerAction);
                    }
                }
            }
        }

        return somethingNotified;
    }

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {
        /*
         * TODO: раскидать все по сущностным сервисам с отдельным наблюдателем
         * для каждого типа сущности. Проблема - сервисы не должны работать с
         * wsTranslator-ом, потребуется промежуточное хранение отправляемых
         * сообщений. К тому же отправлать все изменения итерации желательно
         * одним "пучком".
         */

        try {
            Date observeFrom = observeFromOid.getDate();

            /* собираем данные для оповещения клиентов */
            UpdatedEntities message = new UpdatedEntities();
            message.setEvents(getNewEvents());
            message.setUsers(getNewAndUpdatedUsers(observeFrom));
            message.setUserGroups(getNewAndUpdatedUserGroups(observeFrom));
            message.setDeviceShapeLibraries(
                    deviceShapeLibraryService.getChangesAndDoRemove(observeFrom));
            message.setDeviceShapes(deviceShapeService.getChangesAndDoRemove(observeFrom));
            message.setLogViews(logViewService.getChangesAndDoRemove(observeFrom));
            message.setMedias(mediaService.getChangesAndDoRemove(observeFrom));
            message.setSoundNotifications(
                    soundNotificationService.getChangesAndDoRemove(observeFrom));

            for (ProjectEntityService<?, ?> service : projectEntityServices) {
                UpdatedEntities.ProjectEntitiesContainer<?> changes = service
                        .getChanges(observeFrom, currentTime);
                if (changes != null && !changes.isEmpty()) {
                    message.put(service.getKeyForUpdatedEntities(), changes);
                    if (service.shouldRemoveOnUpLevelNotified())
                        service.doRemove(currentTime);
                }
            }

            boolean somethingNotified = false;

            if (!message.isEmpty()) {
                wsTranslator.send(message);
                somethingNotified = true;
            }

            somethingNotified |= checkStatesAndNotify(observeFrom);

            if (somethingNotified) {
                /*
                 * Запоминаем текущее время минус 1, т.к. изменения между датами
                 * были получены не включительно
                 */
                return new ObjectId(new Date(currentTime.getTime() - 1), 0);
            }
        } catch (Exception exception){
            logger.error("Exception: {}. Stack trace: {}", exception.getMessage(), exception.getStackTrace());
        }
        return observeFromOid;
    }

}
