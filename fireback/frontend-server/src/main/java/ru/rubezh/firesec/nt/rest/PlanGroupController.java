package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.PlanGroupRepository;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.PlanGroup;
import ru.rubezh.firesec.nt.service.v1.PlanGroupService;
import ru.rubezh.firesec.nt.service.v1.PlanGroupService.PlanGroupReferences;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/plan_groups")
public class PlanGroupController {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private PlanGroupRepository planGroupRepository;

    @Autowired
    private PlanGroupService planGroupService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> get(@PathVariable String projectId) {
        List<PlanGroup> planGroups = planGroupRepository.findByProjectId(projectId);
        return ResponseEntity.ok(planGroups);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{planGroupId}")
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String planGroupId) {
        PlanGroup planGroup = planGroupRepository.findByProjectIdAndId(projectId, planGroupId);
        if (planGroup == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(planGroup);
    }

    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody PlanGroup planGroup) {
        StringBuilder errorMessage = new StringBuilder();
        PlanGroup createdPlanGroup = planGroupService.add(projectId, language, planGroup, errorMessage);
        if (createdPlanGroup != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPlanGroup);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{planGroupId}")
    public ResponseEntity<?> update(@PathVariable String projectId, @PathVariable String planGroupId,
            @RequestBody PlanGroup planGroup) {
        StringBuilder errorMessage = new StringBuilder();
        PlanGroup updatedPlanGroup = planGroupService.update(projectId, language, planGroupId, planGroup,
                errorMessage);
        if (updatedPlanGroup != null) {
            return ResponseEntity.ok(updatedPlanGroup);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{planGroupId}")
    public ResponseEntity<?> remove(@PathVariable String projectId, @PathVariable String planGroupId) {
        StringBuilder errorMessage = new StringBuilder();
        PlanGroupReferences planGroupReferences = planGroupService.remove(projectId, language, planGroupId,
                errorMessage);
        if (planGroupReferences != null) {
            return ResponseEntity.ok(planGroupReferences);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
