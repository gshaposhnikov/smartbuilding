package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.rubezh.firesec.nt.dao.v1.ProjectEntityRepository;
import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;
import ru.rubezh.firesec.nt.service.v1.ProjectEntityService;

/** Параметризованный контроллер для проектных сущностей, отвечающих на CUD-методы асинхронно по WebSockets.
 * 
 * ВНИМАНИЕ!!! При переопределении методов в наследниках все аннотации также надо переопределять. Для не
 * переорпеделенных методов аннотации, определенные здесь, работают.
 * 
 * @author Антон Васильев
 *
 * @param <T> сущность модели данных
 * @param <R> репозиторий сущности */
public class ProjectEntityController<T extends BasicEntityWithDate, R extends ProjectEntityRepository<T>> {
    @Autowired
    ProjectEntityService<T, R> entityService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> get(@PathVariable String projectId) {
        StringBuilder errorMessage = new StringBuilder();
        List<T> entities = entityService.get(projectId, errorMessage);
        if (entities != null) {
            return ResponseEntity.ok(entities);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{entityId}")
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String entityId) {
        StringBuilder errorMessage = new StringBuilder();
        T entity = entityService.get(projectId, entityId, errorMessage);
        if (entity != null) {
            return ResponseEntity.ok(entity);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody T entity) {
        StringBuilder errorMessage = new StringBuilder();
        boolean added = entityService.add(projectId, entity, errorMessage);
        if (added) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = { "/{entityId}" })
    public ResponseEntity<?> update(@PathVariable String projectId, @PathVariable String entityId,
            @RequestBody T entity) {
        StringBuilder errorMessage = new StringBuilder();
        boolean updated = entityService.update(projectId, entityId, entity, errorMessage);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{entityId}")
    public ResponseEntity<?> remove(@PathVariable String projectId, @PathVariable String entityId) {
        StringBuilder errorMessage = new StringBuilder();
        boolean removed = entityService.remove(projectId, entityId, errorMessage);
        if (removed) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
