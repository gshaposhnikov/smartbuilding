package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedActiveRegionView;
import ru.rubezh.firesec.nt.service.v1.RegionViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/active_regions")
public class ActiveRegionController {

    @Autowired
    private RegionViewService regionViewService;

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public @ResponseBody ResponseEntity<?> getAll(@PathVariable String projectId) {

        return ResponseEntity.ok(regionViewService.getAggregatedActiveRegionViews(projectId));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{regionId}")
    public ResponseEntity<?> getRegionById(@PathVariable String projectId, @PathVariable String regionId) {

        AggregatedActiveRegionView response = regionViewService.getAggregatedActiveRegionViewByRegionId(projectId,
                regionId);

        if (response != null)
            return ResponseEntity.ok(response);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Region not found"));
    }

}
