package ru.rubezh.firesec.nt.rest;

import java.util.List;
import java.util.ArrayList;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;

import ru.rubezh.firesec.nt.dao.v1.DeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.RegionRepository;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.RegionView;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.service.v1.DeviceViewService;
import ru.rubezh.firesec.nt.service.v1.RegionService;
import ru.rubezh.firesec.nt.service.v1.RegionViewService;
import ru.rubezh.firesec.nt.service.v1.RegionViewService.EntitiesUpdatedByRegion;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/regions")
public class RegionController {


    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private RegionService regionService;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private RegionViewService regionViewService;

    @Autowired
    private DeviceRepository deviceRepository;

    @PreAuthorize("hasRole('REGION_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> add(@PathVariable String projectId,
                                  @RequestBody Region region) {

        StringBuilder errorMessage = new StringBuilder();
        RegionView savedRegionView = regionViewService.add(projectId, region, errorMessage);
        if (savedRegionView != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(savedRegionView);
        }
        else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{regionId}")
    public ResponseEntity<?> update(@PathVariable String projectId,
            @PathVariable String regionId,
                                    @RequestBody Region region) {

        StringBuilder errorMessage = new StringBuilder();
        Region updatedRegion = regionService.update(projectId, regionId, region, errorMessage);
        if (updatedRegion != null)
            return ResponseEntity.ok(updatedRegion);
        else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll(@PathVariable String projectId) {

        return ResponseEntity.ok(regionRepository.findByProjectId(projectId));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{regionId}")
    public ResponseEntity<?> getById(@PathVariable String projectId,
            @PathVariable String regionId) {

        Region region = regionRepository.findByProjectIdAndId(projectId, regionId);
        if (region != null) {
            return ResponseEntity.ok(region);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @PreAuthorize("hasRole('REGION_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{regionId}")
    public ResponseEntity<?> removeRegionById(@PathVariable String projectId,
            @PathVariable String regionId) {

        StringBuilder errorMessage = new StringBuilder();
        EntitiesUpdatedByRegion entitiesUpdatedByRegion = regionViewService.remove(projectId, regionId, errorMessage);
        if (entitiesUpdatedByRegion != null) {
            return ResponseEntity.ok(entitiesUpdatedByRegion);
        }
        else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{regionId}/devices/{deviceId}")
    public ResponseEntity<?> addDeviceRegionLink(@PathVariable String projectId,
                                                 @PathVariable String regionId,
                                                 @PathVariable String deviceId) {

        StringBuilder errorMessage = new StringBuilder();
        Device updatedDevice = regionService.addDeviceRegionLink(deviceId, regionId, projectId, errorMessage);
        if (updatedDevice != null) {
            deviceRepository.save(updatedDevice);
            TreeItemDeviceView deviceTreeItemView = deviceViewService
                    .getDeviceTreeItemByProjectIdAndDeviceId(projectId, deviceId, language);
            return ResponseEntity.ok(deviceTreeItemView);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{regionId}/devices")
    public ResponseEntity<?> addDevicesRegionLink(@PathVariable String projectId,
                                                 @PathVariable String regionId,
                                                 @RequestBody List<String> deviceIds) {

        StringBuilder errorMessage = new StringBuilder();
        List<Device> updatedDevices = new ArrayList<>();

        for (String deviceId : deviceIds) {
            Device updatedDevice = regionService.addDeviceRegionLink(deviceId, regionId, projectId, errorMessage);
            if (updatedDevice != null)
                updatedDevices.add(updatedDevice);
            else
                break;
        }
        if (updatedDevices.size() == deviceIds.size()) {
            deviceRepository.save(updatedDevices);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{regionId}/devices/{deviceId}")
    public ResponseEntity<?> removeDeviceRegionLink(@PathVariable String projectId,
                                                    @PathVariable String regionId,
                                                    @PathVariable String deviceId) {

        StringBuilder errorMessage = new StringBuilder();
        Device updatedDevice = regionService.removeDeviceRegionLink(deviceId, regionId, projectId, errorMessage);
        if (updatedDevice != null) {
            deviceRepository.save(updatedDevice);
            TreeItemDeviceView deviceTreeItemView = deviceViewService
                    .getDeviceTreeItemByProjectIdAndDeviceId(projectId, deviceId, language);
            return ResponseEntity.ok(deviceTreeItemView);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{regionId}/devices")
    public ResponseEntity<?> removeDevicesRegionLink(@PathVariable String projectId,
                                                    @PathVariable String regionId,
                                                    @RequestBody List<String> deviceIds) {

        StringBuilder errorMessage = new StringBuilder();
        List<Device> updatedDevices = new ArrayList<>();

        for (String deviceId : deviceIds) {
            Device updatedDevice = regionService.removeDeviceRegionLink(deviceId, regionId, projectId, errorMessage);
            if (updatedDevice != null)
                updatedDevices.add(updatedDevice);
            else
                break;
        }
        if (updatedDevices.size() == deviceIds.size()) {
            deviceRepository.save(updatedDevices);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{regionId}/devices")
    public ResponseEntity<?> getDevices(@PathVariable String projectId,
            @PathVariable String regionId) {

        /* TODO: реализовать возврат отображений для дерева через сервис */
        return ResponseEntity.ok(deviceRepository.findByProjectIdAndRegionId(projectId, regionId));
    }

}
