package ru.rubezh.firesec.nt.rest.skud;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.rubezh.firesec.nt.dao.v1.AccessKeyRepository;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/access_keys")
public class AccessKeyController extends PreAuthorizedController<AccessKey, AccessKeyRepository> {

}
