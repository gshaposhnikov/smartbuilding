package ru.rubezh.firesec.nt.rest;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.EventTypeViewRepository;
import ru.rubezh.firesec.nt.service.v1.EventViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/")
public class EventViewController {

    @Autowired
    private EventViewService eventViewService;

    @Autowired
    private EventTypeViewRepository eventTypeViewRepository;

    /** Запрос списка событий */
    @RequestMapping(method = RequestMethod.GET, value = { "events", "events/" })
    public ResponseEntity<?> getEvents(
            @RequestParam(name = "idAfter", required = false) String idAfter,
            @RequestParam(name = "idBefore", required = false) String idBefore,
            @RequestParam(name = "subsystems", required = false) String subsystems,
            @RequestParam(name = "occurredAfter", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date occurredAfter,
            @RequestParam(name = "receivedAfter", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date receivedAfter,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "eventTypeIds", required = false) String eventTypeIds,
            @RequestParam(name = "controlDeviceIds", required = false) String controlDeviceIds,
            @RequestParam(name = "deviceIds", required = false) String deviceIds,
            @RequestParam(name = "regionIds", required = false) String regionIds,
            @RequestParam(name = "userIds", required = false) String userIds,
            @RequestParam(name = "virtualStateIds", required = false) String virtualStateIds,
            @RequestParam(name = "scenarioIds", required = false) String scenarioIds,
            @RequestParam(name = "lineNo", required = false) Integer lineNo,
            @RequestParam(name = "logTypeIds", required = false) String logTypeIds,
            @RequestParam(name = "stateCategoryIds", required = false) String stateCategoryIds,
            @RequestParam(name = "indicatorInfo", required = false) String indicatorsInfo,
            @RequestParam(name = "employeeIds", required = false) String employeeIds,
            @RequestParam(name = "occurredBefore", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date occurredBefore,
            @RequestParam(name = "receivedBefore", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date receivedBefore,
            @RequestParam(name = "tags", required = false) String tags,
            Pageable pageable) {

        return ResponseEntity.ok(eventViewService.getAggregatedEventViews(idAfter, idBefore, subsystems, occurredAfter,
                receivedAfter, name, eventTypeIds, controlDeviceIds, deviceIds, regionIds, userIds, virtualStateIds,
                scenarioIds, lineNo, logTypeIds, stateCategoryIds, indicatorsInfo, employeeIds, occurredBefore,
                receivedBefore, tags,
                pageable));
    }

    /** Запрос отображений типов событий */
    @RequestMapping(method = RequestMethod.GET, value = {"event_type_views", "event_type_views/"})
    public ResponseEntity<?> getEventTypeViews() {
        return ResponseEntity.ok(eventTypeViewRepository.findAll());
    }

    /** Запрос общего кол-ва событий */
    @RequestMapping(method = RequestMethod.GET, value = {"events/count", "events/count/"})
    public ResponseEntity<?> getEventsCount(@RequestParam(name = "idAfter", required = false) String idAfter,
            @RequestParam(name = "idBefore", required = false) String idBefore,
            @RequestParam(name = "subsystems", required = false) String subsystems,
            @RequestParam(name = "occurredAfter", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date occurredAfter,
            @RequestParam(name = "receivedAfter", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date receivedAfter,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "eventTypeIds", required = false) String eventTypeIds,
            @RequestParam(name = "controlDeviceIds", required = false) String controlDeviceIds,
            @RequestParam(name = "deviceIds", required = false) String deviceIds,
            @RequestParam(name = "regionIds", required = false) String regionIds,
            @RequestParam(name = "userIds", required = false) String userIds,
            @RequestParam(name = "virtualStateIds", required = false) String virtualStateIds,
            @RequestParam(name = "scenarioIds", required = false) String scenarioIds,
            @RequestParam(name = "lineNo", required = false) Integer lineNo,
            @RequestParam(name = "logTypeIds", required = false) String logTypeIds,
            @RequestParam(name = "stateCategoryIds", required = false) String stateCategoryIds,
            @RequestParam(name = "indicatorInfo", required = false) String indicatorsInfo,
            @RequestParam(name = "employeeIds", required = false) String employeeIds,
            @RequestParam(name = "occurredBefore", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date occurredBefore,
            @RequestParam(name = "receivedBefore", required = false) @DateTimeFormat(iso = ISO.DATE_TIME) Date receivedBefore,
            @RequestParam(name = "tags", required = false) String tags) {
        return ResponseEntity
                .ok("{\"eventsCount\": " + Long.toString(eventViewService.getAggregatedEventViewsCount(idAfter,
                        idBefore, subsystems, occurredAfter, receivedAfter, name, eventTypeIds, controlDeviceIds,
                        deviceIds, regionIds, userIds, virtualStateIds, scenarioIds, lineNo, logTypeIds,
                        stateCategoryIds, indicatorsInfo, employeeIds, occurredBefore, receivedBefore, tags)) + "}");
    }

}
