package ru.rubezh.firesec.nt.rest.skud;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.rubezh.firesec.nt.dao.v1.EmployeeRepository;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/employees")
public class EmployeeController extends PreAuthorizedController<Employee, EmployeeRepository> {

}
