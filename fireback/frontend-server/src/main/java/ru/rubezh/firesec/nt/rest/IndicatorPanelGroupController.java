package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.IndicatorPanelGroupRepository;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanelGroup;
import ru.rubezh.firesec.nt.service.v1.IndicatorPanelGroupService;
import ru.rubezh.firesec.nt.service.v1.IndicatorPanelGroupService.RemovedGroupAndPanelIds;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/indicator_groups")
public class IndicatorPanelGroupController{

    @Autowired
    private IndicatorPanelGroupRepository indicatorPanelGroupRepository;

    @Autowired
    private IndicatorPanelGroupService indicatorPanelGroupService;

    @RequestMapping(method = RequestMethod.GET, value = {"","/"})
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(indicatorPanelGroupRepository.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/{id}"})
    public ResponseEntity<?> get(@PathVariable String projectId,
                                 @PathVariable String id) {
        IndicatorPanelGroup indicatorPanelGroup = indicatorPanelGroupRepository.findByProjectIdAndId(projectId, id);
        if (indicatorPanelGroup != null)
            return ResponseEntity.ok(indicatorPanelGroup);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Группа панелей индикаторов не найдена"));
    }


    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> add(@RequestBody IndicatorPanelGroup indicatorPanelGroup,
                                 @PathVariable String projectId) {
        StringBuilder errorMessage = new StringBuilder();
        IndicatorPanelGroup createdPanelGroup =
                indicatorPanelGroupService.add(indicatorPanelGroup, projectId, errorMessage);
        if (createdPanelGroup != null)
            return ResponseEntity.ok(createdPanelGroup);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.PUT, value = {"/{id}"})
    public ResponseEntity<?> update(@RequestBody IndicatorPanelGroup indicatorPanelGroup, @PathVariable String id,
                                    @PathVariable String projectId) {
        StringBuilder errorMessage = new StringBuilder();
        IndicatorPanelGroup updatedPanelGroup = indicatorPanelGroupService.update(indicatorPanelGroup, projectId, id, errorMessage);
        if (updatedPanelGroup != null)
            return ResponseEntity.ok(updatedPanelGroup);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = {"/{id}"})
    public ResponseEntity<?> remove(@PathVariable String projectId,
                                    @PathVariable String id) {
        StringBuilder errorMessage = new StringBuilder();
        RemovedGroupAndPanelIds removedGroupAndPanelIds = indicatorPanelGroupService.remove(projectId, id, errorMessage);
        if (removedGroupAndPanelIds != null)
            return ResponseEntity.ok(removedGroupAndPanelIds);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }
}
