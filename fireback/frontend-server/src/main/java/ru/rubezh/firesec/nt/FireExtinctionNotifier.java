package ru.rubezh.firesec.nt;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import ru.rubezh.firesec.nt.dao.v1.FireExtinctionNotificationRepository;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.FireExtinctionNotification;
import ru.rubezh.firesec.nt.service.v1.UpsertEntitiesProcessor;
import ru.rubezh.firesec.nt.ws.WebSocketTranslator;

public class FireExtinctionNotifier implements UpsertEntitiesProcessor {

    @Autowired
    private FireExtinctionNotificationRepository fireExtinctionNotificationRepository;

    @Autowired
    private WebSocketTranslator wsTranslator;

    @Override
    public ObjectId processUpsert(ActiveProject activeProject, ObjectId observeFromOid, Date currentTime) {

        List<FireExtinctionNotification> notifications = fireExtinctionNotificationRepository
                .findByIdBetween(observeFromOid, new ObjectId(currentTime, 0), new Sort(Direction.ASC, "_id"));
        if (!notifications.isEmpty()) {
            for (FireExtinctionNotification notification : notifications) {
                wsTranslator.send(notification);
            }

            /*
             * Список уведомлений отсортирован по id, поэтому последний ObjectId берем от последнего уведомления в
             * списке
             */
            return new ObjectId(notifications.get(notifications.size() - 1).getId());
        }

        return observeFromOid;
    }

}
