package ru.rubezh.firesec.nt.rest;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.security.AuthenticatedComponent;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/users")
public class UserController extends AuthenticatedComponent {

    @Autowired(required = false)
    private ShaPasswordEncoder shaPasswordEncoder = null;

    @PreAuthorize("hasRole('USER_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> createNew(@RequestBody User user) {

        StringBuilder errorMessage = new StringBuilder();
        if (shaPasswordEncoder != null && user.getPassword() != null)
            user.setPasswordHash(shaPasswordEncoder.encodePassword(user.getPassword(), null));

        User currentUser = findCurrentUser();
        User savedUser = userService.createNew(user, currentUser, errorMessage);
        if (savedUser != null)
            return new ResponseEntity<>(HttpStatus.CREATED);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll() {

        List<User> result = userRepository.findAll();
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Ошибка получения списка пользователей из БД"));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable String id) {

        User result = userRepository.findOne(id);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Пользователь не найден"));
    }

    @PreAuthorize("hasRole('USER_REMOVE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", params={ "active" })
    public ResponseEntity<?> removeById(@PathVariable String id, @RequestParam boolean active) {

        if (id == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор пользователя"));
        } else {
            User currentUser = findCurrentUser();
            StringBuilder errorMessage = new StringBuilder();
            if (userService.updateActiveState(id, active, currentUser, errorMessage))
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('USER_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", params={ "!active" })
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody User user) {

        if (id == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор пользователя"));
        } else {
            StringBuilder errorMessage = new StringBuilder();
            if (shaPasswordEncoder != null && user.getPassword() != null)
                user.setPasswordHash(shaPasswordEncoder.encodePassword(user.getPassword(), null));

            User currentUser = findCurrentUser();
            User savedUser = userService.update(id, user, currentUser, errorMessage);
            if (savedUser != null)
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "currentUser")
    public ResponseEntity<?> getCurrentUser() {

        User currentUser = findCurrentUser();
        if (currentUser != null) {
            currentUser = userService.fillGroupPermissions(currentUser);
            return ResponseEntity.ok(currentUser);
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/currentUser", params = { "checkPassword" })
    public ResponseEntity<?> verifyPassword(@RequestParam String checkPassword){
        User currentUser = findCurrentUser();
        if (currentUser == null)
            return ResponseEntity.badRequest().body("Пользователь не авторизован");
        else {
            return ResponseEntity.ok().body(Collections.singletonMap("passwordMatched",
                    shaPasswordEncoder.isPasswordValid(currentUser.getPasswordHash(), checkPassword, null)));
        }
    }
}
