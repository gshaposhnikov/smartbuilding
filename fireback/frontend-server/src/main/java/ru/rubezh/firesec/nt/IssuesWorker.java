package ru.rubezh.firesec.nt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.rubezh.firesec.nt.dao.v1.IssueRepository;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;

import java.util.Date;

public class IssuesWorker {

    @Value("#{'${server.issues.lifetime:2592000000}'}")
    private long issuesLifeTime;

    @Autowired
    private IssueRepository issueRepository;

    public void removeOldFinishedIssues() {
        Date lifeTime = new Date(new Date().getTime() - issuesLifeTime);
        issueRepository.removeByStatusNotInAndFinishDateTimeBefore(IssueStatus.IN_PROGRESS, lifeTime);
    }

}
