package ru.rubezh.firesec.nt.security;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

import ru.rubezh.firesec.nt.dao.v1.EventTypeRepository;
import ru.rubezh.firesec.nt.domain.v1.EventType;
import ru.rubezh.firesec.nt.rest.UserController;
import ru.rubezh.firesec.nt.service.v1.EventService;

import java.util.List;

public class ExtendedMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private EventService eventService;
    private EventTypeRepository eventTypeRepository;
    private UserController userController;

    private Object filterObject;
    private Object returnObject;
    private Object target;

    ExtendedMethodSecurityExpressionRoot(Authentication authentication,
                                         EventService eventService,
                                         EventTypeRepository eventTypeRepository,
                                         UserController userController) {
        super(authentication);
        this.eventService = eventService;
        this.eventTypeRepository = eventTypeRepository;
        this.userController = userController;
    }

    @Override
    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }

    @Override
    public Object getFilterObject() {
        return filterObject;
    }

    @Override
    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    @Override
    public Object getReturnObject() {
        return returnObject;
    }

    void setThis(Object target) {
        this.target = target;
    }

    @Override
    public Object getThis() {
        return target;
    }

    public boolean hasEventRole(String role){
        if (!hasRole(role)){
            List<EventType> eventTypes = eventTypeRepository.findByPermissionId(role);
            EventType eventType = eventTypes.stream().filter(failureEvent -> failureEvent.getId().contains("Failure")).findFirst().orElse(null);
            if (eventType != null) {
                eventService.commitEventByEventTypeIdAndUsername(eventType.getId(), userController.findCurrentUser());
            }
            return false;
        }
        return true;
    }
}
