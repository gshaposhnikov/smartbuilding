package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.RegionViewRepository;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.RegionView;
import ru.rubezh.firesec.nt.domain.v1.RegionView.RegionPlanLayout;
import ru.rubezh.firesec.nt.service.v1.RegionViewService;
import ru.rubezh.firesec.nt.service.v1.RegionViewService.EntitiesUpdatedByRegion;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/region_views")
public class RegionViewController {
    @Autowired
    private Logger logger;

    @Autowired
    private RegionViewRepository regionViewRepository;

    @Autowired
    private RegionViewService regionViewService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> get(@PathVariable String projectId) {
        return ResponseEntity.ok(regionViewRepository.findByProjectId(projectId));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{regionId}")
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String regionId) {

        RegionView regionView = regionViewRepository.findByProjectIdAndId(projectId, regionId);
        if (regionView != null) {
            return ResponseEntity.ok(regionView);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PreAuthorize("hasRole('REGION_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody Region region) {

        StringBuilder errorMessage = new StringBuilder();
        RegionView createdRegionView = regionViewService.add(projectId, region, errorMessage);
        if (createdRegionView != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRegionView);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{regionId}", params = { "!plan_layouts" })
    public ResponseEntity<?> update(@PathVariable String projectId, @PathVariable String regionId,
            @RequestBody Region region) {

        StringBuilder errorMessage = new StringBuilder();
        RegionView updatedRegionView = regionViewService.update(projectId, regionId, region, errorMessage);
        if (updatedRegionView != null)
            return ResponseEntity.ok(updatedRegionView);
        else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "", "/{regionId}" }, params = { "plan_layouts" })
    public ResponseEntity<?> updateRegionPlanLayouts(@PathVariable String projectId, @PathVariable String regionId,
            @RequestBody List<RegionPlanLayout> planLayouts) {

        StringBuilder errorMessage = new StringBuilder();

        RegionView regionView = regionViewService.updateRegionPlanLayouts(projectId, regionId, planLayouts,
                errorMessage);

        if (regionView != null) {
            return ResponseEntity.ok(regionView);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('REGION_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{regionId}")
    public ResponseEntity<?> removeRegionById(@PathVariable String projectId, @PathVariable String regionId) {

        StringBuilder errorMessage = new StringBuilder();
        EntitiesUpdatedByRegion entitiesUpdatedByRegion = regionViewService.remove(projectId, regionId, errorMessage );
        if (entitiesUpdatedByRegion != null) {
            return ResponseEntity.ok(entitiesUpdatedByRegion);
        }
        else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
