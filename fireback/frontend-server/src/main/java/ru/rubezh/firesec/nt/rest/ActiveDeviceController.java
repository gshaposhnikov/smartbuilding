package ru.rubezh.firesec.nt.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import ru.rubezh.firesec.nt.amqp.message.request.SetDeviceConfigRequest;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;
import ru.rubezh.firesec.nt.amqp.message.response.SetDeviceConfigResponse;
import ru.rubezh.firesec.nt.amqp.sender.RequestSender;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemActiveDeviceView;
import ru.rubezh.firesec.nt.security.AuthenticatedComponent;
import ru.rubezh.firesec.nt.service.v1.ActiveDeviceService;
import ru.rubezh.firesec.nt.service.v1.DeviceService;
import ru.rubezh.firesec.nt.service.v1.DeviceViewService;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/active_devices")
public class ActiveDeviceController extends AuthenticatedComponent {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private RequestSender requestSender;

    @Autowired
    private ActiveDeviceService activeDeviceService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" }, params = { "!region" })
    public @ResponseBody ResponseEntity<StreamingResponseBody> getAll(@PathVariable String projectId) {
        HttpHeaders streamingResponseHeaders = new HttpHeaders();
        streamingResponseHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return new ResponseEntity<StreamingResponseBody>(new StreamingResponseBody() {

            @Override
            public void writeTo(OutputStream outputStream) throws IOException {
                ObjectMapper objectMapper = new ObjectMapper();
                deviceViewService.safelyWriteActiveDeviceTreeItemByProjectId(projectId, outputStream,
                        objectMapper);
            }
        }, streamingResponseHeaders, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" }, params = { "region" })
    public @ResponseBody ResponseEntity<?> getAll(@PathVariable String projectId,
            @RequestParam(name = "region", required = true) String regionId) {

        List<TreeItemActiveDeviceView> response = deviceViewService
                .getActiveDeviceTreeItemByProjectIdAndRegionId(projectId, regionId, language);

        if (response != null)
            return ResponseEntity.ok(response);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse("Зона не найдена"));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{deviceId}", params = {"!config"})
    public ResponseEntity<?> getDeviceById(@PathVariable String projectId,
                                           @PathVariable String deviceId) {
        TreeItemActiveDeviceView response =
                deviceViewService.getActiveDeviceTreeItemByProjectIdAndDeviceId(projectId, deviceId, language);

        if (response != null)
            return ResponseEntity.ok(response);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Device not found"));
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceId}" }, params = {"config"})
    public ResponseEntity<?> updateDeviceProjectConfig(@PathVariable String projectId,
            @PathVariable String deviceId,
            @RequestBody Map<String, String> propertyValues) {
        StringBuilder errorMessage = new StringBuilder();
        Device device = deviceService.setActiveDeviceProjectConfig(deviceId, projectId, propertyValues, errorMessage);
        if (device != null) {
            TreeItemActiveDeviceView activeDeviceTreeItemView = deviceViewService
                    .getActiveDeviceTreeItemByProjectIdAndDeviceId(projectId, deviceId, language);

            SetDeviceConfigRequest request = new SetDeviceConfigRequest();
            request.setProjectId(projectId);
            request.setDeviceId(deviceId);
            request.setActiveConfigValues(device.getProjectConfigValues());

            SetDeviceConfigResponse response = (SetDeviceConfigResponse) requestSender.sendAndReceive(request);
            if (response != null) {
                if (response.getError() == ErrorType.OK) {
                    return ResponseEntity.ok(activeDeviceTreeItemView);
                } else {
                    logger.error(response.getErrorString());
                    return ResponseEntity.badRequest()
                            .body(new ErrorResponse(response.getErrorString()));
                }
            } else {
                logger.error("Ошибка передачи данных по RabbitMQ - ответ не получен");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }  else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('DEVICE_UPDATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"/{activeDeviceId}"} , params = {"notepad", "!config", "!region"})
    public ResponseEntity<?> addNoteToDevice (@PathVariable String projectId,
                                              @PathVariable String activeDeviceId, @RequestBody ActiveDevice.Note note){
        User currentUser = findCurrentUser();
        ActiveDevice device;
        if (currentUser != null) {
            device = activeDeviceService.addNoteToDevice(activeDeviceId, currentUser.getFullName(), note);
        } else {
            device = activeDeviceService.addNoteToDevice(activeDeviceId, "Неизвестный пользователь", note);
        }
        if (device != null){
            TreeItemActiveDeviceView activeDeviceTreeItemView = deviceViewService
                    .getActiveDeviceTreeItemByProjectIdAndDeviceId(projectId, activeDeviceId, language);
            return ResponseEntity.ok(activeDeviceTreeItemView);
        } else {
            logger.error("Устройство не найдено");
            return ResponseEntity.badRequest().body(null);
        }
    }
}
