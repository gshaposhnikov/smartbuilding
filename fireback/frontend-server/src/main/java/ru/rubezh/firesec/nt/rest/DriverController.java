package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ru.rubezh.firesec.nt.amqp.sender.DriverRoutedRequestSender;
import ru.rubezh.firesec.nt.amqp.message.request.AbstractRoutedDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.request.GetLicenceRestrictionsRequest;
import ru.rubezh.firesec.nt.amqp.message.response.GetLicenceRestrictionsResponse;
import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;
import ru.rubezh.firesec.nt.service.v1.IssueService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/driver")
public class DriverController {

    @Autowired
    private IssueService issueService;

    @Autowired
    private DriverRoutedRequestSender driverRoutedRequestSender;

    /** Запрос лицензионных ограничений */
    @RequestMapping(method = RequestMethod.GET, value = {"/license"})
    public ResponseEntity<?> getDriverLicense() {
        AbstractRoutedDriverRequest request = new GetLicenceRestrictionsRequest();
        String driverRequestQueueName = issueService.routeRequestToFirstOneDriver(request);
        if (driverRequestQueueName == null || request.getDriverId() == null)
            return ResponseEntity.ok(new LicenseRestrictions());

        GetLicenceRestrictionsResponse response = (GetLicenceRestrictionsResponse)
                driverRoutedRequestSender.sendAndReceive(driverRequestQueueName, request);
        if (response == null)
            return ResponseEntity.ok(new LicenseRestrictions());

        return ResponseEntity.ok(response.getLicenseRestrictions());
    }

}
