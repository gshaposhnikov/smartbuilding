package ru.rubezh.firesec.nt.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.service.v1.EventService;

@Component
public class LogoutSuccessHandlerImpl extends SimpleUrlLogoutSuccessHandler {

    private static String logoutSuccessEventTypeId = "logoutSuccess";

    @Autowired
    private Logger logger;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventService eventService;

    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        if (authentication != null) {
            User authenticationUser = (User)authentication.getPrincipal();
            if(authenticationUser != null) {
                logger.info("Logout success. Username: {}", authenticationUser.getUsername());

                try {
                    ru.rubezh.firesec.nt.domain.v1.User user = userRepository.findByName(authenticationUser.getUsername());

                    if (user != null)
                        eventService.commitEventByEventTypeIdAndUsername(logoutSuccessEventTypeId, user);
                }
                catch (Exception e) {
                    logger.error("Failed commit logout success event: ", e);
                }
            }
        }

        String URL = request.getContextPath() + "/login";
        response.setStatus(HttpStatus.OK.value());
        response.sendRedirect(URL);
    }

}
