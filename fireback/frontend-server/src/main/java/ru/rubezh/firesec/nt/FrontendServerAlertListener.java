package ru.rubezh.firesec.nt;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ru.rubezh.firesec.nt.amqp.listener.AlertListener;
import ru.rubezh.firesec.nt.amqp.message.alert.*;
import ru.rubezh.firesec.nt.dao.v1.ActiveProjectRepository;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.ControlDeviceEntitiesComparision;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemActiveDeviceView;
import ru.rubezh.firesec.nt.service.v1.*;
import ru.rubezh.firesec.nt.ws.WebSocketTranslator;

/**
 * Класс для получения оповещений.
 *
 * @author Артем Седанов
 */
public class FrontendServerAlertListener extends AlertListener {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private WebSocketTranslator wsTranslator;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private IssueService issueService;

    @Autowired
    private PlanService planService;

    @Autowired
    private ProjectViewService projectViewService;

    @Autowired
    private ActiveProjectRepository activeProjectRepository;

    public FrontendServerAlertListener(Logger logger) {
        super(logger);
    }

    void handleMessage(ActiveDeviceUpdated activeDeviceUpdated) {
        Project activeProject = projectService.getActive();
        if (activeProject != null) {
            TreeItemActiveDeviceView deviceView = deviceViewService.getActiveDeviceTreeItemByProjectIdAndDeviceId(
                    activeProject.getId(), activeDeviceUpdated.getActiveDeviceId(), language);
            if (deviceView != null)
                wsTranslator.send(deviceView);
        }
    }

    void handleMessage(DeviceConfigChanged deviceConfigChanged) {
        Project activeProject = projectService.getActive();
        if (activeProject != null) {
            TreeItemActiveDeviceView treeItem = deviceViewService.getActiveDeviceTreeItemByProjectIdAndDeviceId(
                    deviceConfigChanged.getProjectId(), deviceConfigChanged.getDeviceId(), language);
            if (treeItem != null)
                wsTranslator.send(treeItem);
        }
    }

    void handleMessage(IssueProgress message) {
        logger.debug("Got issue \"{}\" status \"{}\" ({}) and progress \"{}\" update",
                message.getIssueId(), message.getStatus(), message.getStatusMessage(), message.getProgress());

        Issue issue = issueService.setIssueProgress(message.getIssueId(),
                message.getStatus(), message.getStatusMessage(), message.getProgress());
        if (issue != null) {
            wsTranslator.send(issue);
        }
    }

    void handleMessage(ActiveProjectStatusUpdated activeProjectStatusUpdated) {
        /* Находим сущность активного проекта */
        ActiveProject project = activeProjectRepository.findOne(activeProjectStatusUpdated.getProjectId());
        if (project != null) {
            wsTranslator.send(project);
        } else {
            logger.error("Active project not found. Id: {}", activeProjectStatusUpdated.getProjectId());
        }
    }

    void handleMessage(FireExtinctionNotificationCame alert) {
        for (FireExtinctionNotification notification: alert.getNotifications())
            wsTranslator.send(notification);
    }

    void handleMessage(DatabaseReadingCompleted databaseReadingCompleted) {
        Project activeProject = projectService.getActive();
        if (activeProject != null) {
            ControlDeviceEntitiesComparision entitiesComparision = new ControlDeviceEntitiesComparision();
            entitiesComparision.setIssueId(databaseReadingCompleted.getIssueId());
            entitiesComparision.setCurrentEntities(databaseReadingCompleted.getCurrentEntities());
            entitiesComparision.setRecoveredEntities(databaseReadingCompleted.getRecoveredEntities());
            entitiesComparision.setCurrentEntitiesView(projectViewService.getControlDeviceEntitiesView(
                    databaseReadingCompleted.getCurrentEntities(), language));
            entitiesComparision.setRecoveredEntitiesView(projectViewService.getControlDeviceEntitiesView(
                    databaseReadingCompleted.getRecoveredEntities(), language));
            wsTranslator.send(entitiesComparision);
        }
    }

    void handleMessage(LicenseRestrictionsUpdated licenseRestrictionsUpdated) {
        wsTranslator.send(licenseRestrictionsUpdated.getLicenseRestrictions());
    }

    void handleMessage(AccessKeyValueAlert accessKeyValueAlert) {
        if (projectService.getActive() != null) {
            wsTranslator.send(accessKeyValueAlert.getAccessKeyValueInfo());
        }
    }

}
