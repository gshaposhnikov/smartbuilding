package ru.rubezh.firesec.nt.rest;

import java.lang.StringBuilder;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import ru.rubezh.firesec.nt.dao.v1.UserGroupRepository;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.UserGroup;
import ru.rubezh.firesec.nt.service.v1.UserGroupService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/user_groups")
public class UserGroupController {

    @Autowired
    private UserController userController;

    @Autowired
    private UserGroupService userGroupService;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @PreAuthorize("hasRole('USER_GROUP_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> save(@RequestBody UserGroup userGroup) {

        StringBuilder errorMessage = new StringBuilder();
        User currentUser = userController.findCurrentUser();
        UserGroup savedUserGroup = userGroupService.createNew(userGroup, currentUser, errorMessage);
        if (savedUserGroup != null)
            return new ResponseEntity<>(HttpStatus.CREATED);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll() {

        List<UserGroup> result = userGroupRepository.findAllByRemovedIsFalse();
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Ошибка получения списка групп пользователей из БД"));
    }

    @PreAuthorize("hasRole('USER_GROUP_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<?> removeById(@PathVariable String id) {

        if (id == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор группы пользователей"));
        } else {
            StringBuilder errorMessage = new StringBuilder();
            User currentUser = userController.findCurrentUser();
            if (userGroupService.remove(id, currentUser, errorMessage))
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('USER_GROUP_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{groupId}/user/{userId}")
    public ResponseEntity<?> addUserLink(@PathVariable String groupId,
                                         @PathVariable String userId) {

        if (groupId == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор группы пользователей"));
        } else if (userId == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор пользователя"));
        } else {
            StringBuilder errorMessage = new StringBuilder();
            if (userGroupService.addGroupLink(userId, groupId, errorMessage))
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('USER_GROUP_UPDATE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{groupId}/user/{userId}")
    public ResponseEntity<?> removeUserLink(@PathVariable String groupId,
                                            @PathVariable String userId) {

        if (groupId == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор группы пользователей"));
        } else if (userId == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор пользователя"));
        } else {
            StringBuilder errorMessage = new StringBuilder();
            if (userGroupService.removeGroupLink(userId, groupId, errorMessage))
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('USER_GROUP_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody UserGroup userGroup) {

        if (id == null) {
            return ResponseEntity.badRequest().body(
                    new ErrorResponse("Необходимо указать идентификатор группы пользователей"));
        } else {
            StringBuilder errorMessage = new StringBuilder();
            User currentUser = userController.findCurrentUser();
            UserGroup savedUserGroup = userGroupService.update(id, userGroup, currentUser, errorMessage);
            if (savedUserGroup != null)
                return new ResponseEntity<>(HttpStatus.OK);
            else
                return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
