package ru.rubezh.firesec.nt.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.ScenarioAdvancedParams;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.ScenarioBasicParams;
import ru.rubezh.firesec.nt.service.v1.ScenarioViewService;
import ru.rubezh.firesec.nt.service.v1.ScenarioViewService.RemovedScenarioInfo;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/scenarios")
public class ScenarioController {

    @Autowired
    private ScenarioViewService scenarioViewService;

    @PreAuthorize("hasRole('SCENARIO_READ')")
    @RequestMapping(value = {"", "/"})
    public ResponseEntity<?> getAggregatedScenarioViews(@PathVariable String projectId) {

        return ResponseEntity.ok(scenarioViewService.getAllAggregatedScenarioViews(projectId));
    }

    @PreAuthorize("hasRole('SCENARIO_READ')")
    @RequestMapping(value = "/{scenarioId}")
    public ResponseEntity<?> getAggregatedScenarioView(@PathVariable String projectId,
                                                       @PathVariable String scenarioId) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.getAggregatedScenarioView(projectId, scenarioId, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{scenarioId}")
    public ResponseEntity<?> removeById(@PathVariable String projectId,
                                        @PathVariable String scenarioId) {

        StringBuilder errorMessage = new StringBuilder();
        RemovedScenarioInfo removedScenarioInfo = scenarioViewService.remove(projectId, scenarioId, errorMessage);
        if (removedScenarioInfo != null)
            return ResponseEntity.ok(removedScenarioInfo);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> createByBasicParams(@PathVariable String projectId,
                                                 @RequestBody ScenarioBasicParams scenarioBasicParams) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.createByBasicParams(projectId, scenarioBasicParams, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{scenarioId}", params = {"basic_params"})
    public ResponseEntity<?> updateBasicParams(@PathVariable String projectId, @PathVariable String scenarioId,
                                               @RequestBody ScenarioBasicParams scenarioBasicParams) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.updateBasicParams(projectId, scenarioId, scenarioBasicParams, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{scenarioId}", params = {"advanced_params"})
    public ResponseEntity<?> updateAdvancedParams(@PathVariable String projectId, @PathVariable String scenarioId,
                                                  @RequestBody ScenarioAdvancedParams scenarioAdvancedParams) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.updateAdvancedParams(projectId, scenarioId, scenarioAdvancedParams, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{scenarioId}", params = {"start_logic"})
    public ResponseEntity<?> updateStartLogic(@PathVariable String projectId, @PathVariable String scenarioId,
                                              @RequestBody ScenarioLogicBlock scenarioLogicBlock) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.updateStartLogic(projectId, scenarioId, scenarioLogicBlock, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{scenarioId}", params = {"stop_logic"})
    public ResponseEntity<?> updateStopLogic(@PathVariable String projectId, @PathVariable String scenarioId,
                                             @RequestBody ScenarioLogicBlock scenarioLogicBlock) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.updateStopLogic(projectId, scenarioId, scenarioLogicBlock, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = "/{scenarioId}/time_line_blocks")
    public ResponseEntity<?> createExecutiveBlock(@PathVariable String projectId, @PathVariable String scenarioId,
                                                  @RequestBody ScenarioTimeLineBlock scenarioTimeLineBlock) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.createTimeLineBlock(projectId, scenarioId, scenarioTimeLineBlock, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{scenarioId}/time_line_blocks/{blockNo}")
    public ResponseEntity<?> updateBlock(@PathVariable String projectId, @PathVariable String scenarioId,
                                         @PathVariable int blockNo,
                                         @RequestBody ScenarioTimeLineBlock scenarioTimeLineBlock) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.updateBlock(projectId, scenarioId, blockNo, scenarioTimeLineBlock, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('SCENARIO_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{scenarioId}/time_line_blocks/{blockNo}")
    public ResponseEntity<?> removeById(@PathVariable String projectId, @PathVariable String scenarioId,
                                        @PathVariable int blockNo) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioView result =
                scenarioViewService.removeBlock(projectId, scenarioId, blockNo, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

}
