package ru.rubezh.firesec.nt.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import ru.rubezh.firesec.nt.domain.v1.ActiveProject;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent.AccessKeyValueInfo;
import ru.rubezh.firesec.nt.domain.v1.FireExtinctionNotification;
import ru.rubezh.firesec.nt.domain.v1.Issue;
import ru.rubezh.firesec.nt.domain.v1.Plan;
import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;
import ru.rubezh.firesec.nt.domain.v1.representation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedActiveScenarioView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.ScenarioComputerAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для отправки оповещений по WebSocket
 *
 * @author Артем Седанов
 */
public class WebSocketTranslator {

    private Logger logger;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    public WebSocketTranslator(Logger logger) {
        this.logger = logger;
    }

    private void wrapAndSend(Object object, String contentType) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(object);
            StringBuilder sb = new StringBuilder();
            sb.append("{\"contentType\":\"");
            sb.append(contentType);
            sb.append("\",\"content\":");
            sb.append(json);
            sb.append("}");
            simpMessagingTemplate.convertAndSend(WebSocketConfigurer.webSocketDest, sb.toString());
        } catch (JsonProcessingException e) {
            logger.error("JSON Error: {}", e);
        }
    }

    private void wrapAndSendList(List<Object> objects, String contentType) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("{\"contentType\":\"");
            sb.append(contentType);
            sb.append("\",\"content\":[");
            if (!objects.isEmpty()) {
                for (Object object : objects) {
                    json = mapper.writeValueAsString(object);
                    sb.append(json);
                    sb.append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append("]}");
            simpMessagingTemplate.convertAndSend(WebSocketConfigurer.webSocketDest, sb.toString());
        } catch (JsonProcessingException e) {
            logger.error("JSON Error: {}", e);
        }
    }

    public void send(SubsystemView message) {
        wrapAndSend(message, "subsystemView");
    }

    public void send(TreeItemActiveDeviceView message) {
        wrapAndSend(message, "treeItemActiveDeviceView");
    }

    public void send(Issue message) {
        wrapAndSend(message, "issue");
    }

    public void send(List<?> messages, Class entityClass) {
        List<Object> objects = new ArrayList<>(messages);
        if (Issue.class.equals(entityClass)) {
            wrapAndSendList(objects, "issues");
        } else if (TreeItemActiveDeviceView.class.equals(entityClass)) {
            wrapAndSendList(objects, "treeItemActiveDeviceViews");
        } else if (AggregatedActiveRegionView.class.equals(entityClass)) {
            wrapAndSendList(objects, "aggregatedActiveRegionViews");
        } else if (AggregatedActiveScenarioView.class.equals(entityClass)) {
            wrapAndSendList(objects, "aggregatedActiveScenarioViews");
        }
    }

    public void send(Plan message) {
        wrapAndSend(message, "activePlan");
    }

    public void send(ActiveProject message) {
        wrapAndSend(message, "activeProject");
    }

    public void send(FireExtinctionNotification message) {
        wrapAndSend(message, "fireExtinctionNotification");
    }

    public void send(UpdatedEntities message) {
        wrapAndSend(message, "updatedEntities");
    }

    public void send(ControlDeviceEntitiesComparision message) {
        wrapAndSend(message, "controlDeviceEntitiesComparision");
    }

    public void send(ScenarioComputerAction message){
        wrapAndSend(message, "scenarioComputerAction");
    }

    public void send(LicenseRestrictions message){
        wrapAndSend(message, "licenseRestrictions");
    }

    public void send(AccessKeyValueInfo accessKeyValueInfo) {
        wrapAndSend(accessKeyValueInfo, "accessKeyValue");
    }

}
