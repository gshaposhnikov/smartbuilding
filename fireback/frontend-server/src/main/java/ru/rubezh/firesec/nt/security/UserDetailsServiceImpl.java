package ru.rubezh.firesec.nt.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.UserGroupRepository;
import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.UserGroup;

/**
 * Сервис авторизации пользователей.
 * Использует базу данных для получения данных пользователей.
 * @author Александр Горячкин
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userRepository.findByName(name);
        if (user == null || !user.isActive())
            throw new UsernameNotFoundException("User not found");

        Set<GrantedAuthority> roles = new HashSet<>();
        for (String permission: user.getPermissionIds())
            roles.add(new SimpleGrantedAuthority("ROLE_" + permission));

        List<UserGroup> userGroups = userGroupRepository.findByRemovedIsFalseAndIdIn(user.getUserGroupIds());
        for (UserGroup userGroup: userGroups) {
            for (String permission: userGroup.getPermissionIds())
                roles.add(new SimpleGrantedAuthority("ROLE_" + permission));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getName(), user.getPasswordHash(), roles);
    }

}
