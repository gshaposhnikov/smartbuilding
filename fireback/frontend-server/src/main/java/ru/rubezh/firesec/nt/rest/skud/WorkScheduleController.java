package ru.rubezh.firesec.nt.rest.skud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.WorkScheduleRepository;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.AccessGrantedTime;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule.WorkScheduleDay;
import ru.rubezh.firesec.nt.rest.ErrorResponse;
import ru.rubezh.firesec.nt.service.v1.skud.WorkScheduleEmbeddedObjectsService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/work_schedules")
public class WorkScheduleController extends PreAuthorizedController<WorkSchedule, WorkScheduleRepository> {

    @Autowired
    WorkScheduleEmbeddedObjectsService workScheduleEmbeddedObjectsService;


    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.POST, value = { "/{workScheduleId}/days", "/{workScheduleId}/days/" })
    public ResponseEntity<?> addDay(@PathVariable String projectId, @PathVariable String workScheduleId,
            @RequestBody WorkScheduleDay day) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.addDay(projectId, workScheduleId, day, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{workScheduleId}/days/{dayNo}")
    public ResponseEntity<?> updateDay(@PathVariable String projectId, @PathVariable String workScheduleId,
            @PathVariable int dayNo, @RequestBody WorkScheduleDay day) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.updateDay(projectId, workScheduleId, dayNo, day, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{workScheduleId}/days/{dayNo}")
    public ResponseEntity<?> removeDay(@PathVariable String projectId, @PathVariable String workScheduleId,
            @PathVariable int dayNo) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.removeDay(projectId, workScheduleId, dayNo, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.POST, value = { "/{workScheduleId}/days/{dayNo}/access_granted_times",
            "/{workScheduleId}/days/{dayNo}/access_granted_times/" })
    public ResponseEntity<?> addGrantedTime(@PathVariable String projectId, @PathVariable String workScheduleId,
            @PathVariable int dayNo, @RequestBody AccessGrantedTime grantedTime) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.addAccessGrantedTime(projectId, workScheduleId, dayNo, grantedTime,
                errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{workScheduleId}/days/{dayNo}/access_granted_times/{timeNo}")
    public ResponseEntity<?> updateGrantedTime(@PathVariable String projectId, @PathVariable String workScheduleId,
            @PathVariable int dayNo, @PathVariable int timeNo, @RequestBody AccessGrantedTime grantedTime) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.updateAccessGrantedTime(projectId, workScheduleId, dayNo, timeNo,
                grantedTime, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{workScheduleId}/days/{dayNo}/access_granted_times/{timeNo}")
    public ResponseEntity<?> removeGrantedTime(@PathVariable String projectId, @PathVariable String workScheduleId,
            @PathVariable int dayNo, @PathVariable int timeNo) {
        StringBuilder errorMessage = new StringBuilder();
        if (workScheduleEmbeddedObjectsService.removeAccessGrantedTime(projectId, workScheduleId, dayNo, timeNo,
                errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
