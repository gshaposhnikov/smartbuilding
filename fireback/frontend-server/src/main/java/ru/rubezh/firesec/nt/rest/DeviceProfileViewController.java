package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.domain.v1.ConnectionInterfaceType;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.service.v1.DeviceProfileViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/device_profile_views")
public class DeviceProfileViewController {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private DeviceProfileViewService deviceProfileViewService;

    /** Запрос списка возможных профилей устройств, которые
     *  могут обслуживаться текущими запущенными драйверами
     */
    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getDriversSupportedDeviceProfiles(
            @RequestParam(name = "root_conn_type", required = false, defaultValue="NONE") ConnectionInterfaceType connectionType) {

        if (connectionType == ConnectionInterfaceType.NONE)
            return ResponseEntity.ok(deviceProfileViewService.getByLanguage(language));
        else
            return ResponseEntity.ok(deviceProfileViewService.getByLanguageAndConnectionType(language, connectionType));
    }

    @GetMapping(value = {"/device_profiles_dictionary"})
    public ResponseEntity<?> getDeviceProfilesDictionary(){
        return ResponseEntity.ok(deviceProfileViewService.getDeviceProfilesDictionary());
    }
}
