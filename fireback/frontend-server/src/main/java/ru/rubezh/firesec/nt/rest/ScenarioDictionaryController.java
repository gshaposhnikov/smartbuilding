package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioActionTypeView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioDictionary;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioTriggerTypeView;
import ru.rubezh.firesec.nt.service.v1.ScenarioDictionariesViewService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}")
public class ScenarioDictionaryController {

    @Autowired
    private ScenarioDictionariesViewService scenarioDictionariesViewService;

    @RequestMapping(value = "/scenario_dictionaries")
    public ResponseEntity<?> getScenarioDictionaries() {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedScenarioDictionary result =
                scenarioDictionariesViewService.getAggregatedScenarioDictionaries(errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(value = "/scenario_trigger_types")
    public ResponseEntity<?> getScenarioTriggerTypes() {

        StringBuilder errorMessage = new StringBuilder();
        List<AggregatedScenarioTriggerTypeView> result =
                scenarioDictionariesViewService.getAggregatedScenarioTriggerTypeViews(errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(value = "/scenario_action_types")
    public ResponseEntity<?> getScenarioActionTypes() {

        StringBuilder errorMessage = new StringBuilder();
        List<AggregatedScenarioActionTypeView> result =
                scenarioDictionariesViewService.getAggregatedScenarioActionTypeViews(errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

}
