package ru.rubezh.firesec.nt.security;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationContext;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import ru.rubezh.firesec.nt.dao.v1.EventTypeRepository;
import ru.rubezh.firesec.nt.rest.UserController;
import ru.rubezh.firesec.nt.service.v1.EventService;

public class ExtendedMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    private final AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    @Autowired
    private EventService eventService;

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private UserController userController;

    @Override
    public void setReturnObject(Object returnObject, EvaluationContext ctx) {
        ((ExtendedMethodSecurityExpressionRoot) ctx.getRootObject().getValue()).setReturnObject(returnObject);
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication,
                                                                              MethodInvocation invocation) {
        final ExtendedMethodSecurityExpressionRoot root = new ExtendedMethodSecurityExpressionRoot(authentication,
                eventService, eventTypeRepository, userController);
        root.setThis(invocation.getThis());
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());

        return root;
    }
}
