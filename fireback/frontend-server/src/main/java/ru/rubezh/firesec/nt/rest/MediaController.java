package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.MediaRepository;
import ru.rubezh.firesec.nt.domain.v1.Media;
import ru.rubezh.firesec.nt.domain.v1.MediaType;
import ru.rubezh.firesec.nt.security.AuthenticatedComponent;
import ru.rubezh.firesec.nt.service.v1.MediaService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/medias")
public class MediaController extends AuthenticatedComponent {

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private MediaService mediaService;

    @GetMapping(value = { "", "/" }, params = { "type" })
    public ResponseEntity<?> getByType(@RequestParam(name = "type", required = true) MediaType mediaType) {
        return ResponseEntity.ok(mediaRepository.findByMediaType(mediaType));
    }

    @GetMapping(value = {"/{id}"})
    public ResponseEntity<?> getById(@PathVariable String id){
        Media media = mediaRepository.findOne(id);
        if (media != null) {
            return ResponseEntity.ok(media);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Медиафайл не найден"));
        }
    }

    @PreAuthorize("hasRole('MEDIA_CREATE')")
    @PostMapping
    public ResponseEntity<?> addMedia(@RequestBody Media media){
        StringBuilder errorMessage = new StringBuilder();
        if (mediaService.addMedia(media, findCurrentUser(), errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('MEDIA_UPDATE')")
    @PutMapping(value = {"/{id}"})
    public ResponseEntity<?> updateMedia(@PathVariable String id, @RequestBody Media media){
        StringBuilder errorMessage = new StringBuilder();
        if (mediaService.updateMedia(id, media, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('MEDIA_REMOVE')")
    @DeleteMapping(value = {"/{id}"})
    public ResponseEntity<?> deleteMedia(@PathVariable String id){
        StringBuilder errorMessage = new StringBuilder();
        if (mediaService.deleteMedia(id, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
