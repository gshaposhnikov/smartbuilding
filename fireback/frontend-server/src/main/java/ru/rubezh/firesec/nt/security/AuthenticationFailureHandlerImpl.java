package ru.rubezh.firesec.nt.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.service.v1.EventService;

@Component
public class AuthenticationFailureHandlerImpl extends SimpleUrlAuthenticationFailureHandler {

    private static String authenticationFailureEventTypeId = "authenticationFailure";

    @Autowired
    private Logger logger;

    @Autowired
    private EventService eventService;

    public AuthenticationFailureHandlerImpl() {
        super("/login?error");
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        String userName = request.getParameter("j_username");
        logger.warn("Authentication failure: {}. Username = {}", exception.getMessage(), userName);

        try {
            User user = new User();
            user.setFullName(userName);

            eventService.commitEventByEventTypeIdAndUsername(authenticationFailureEventTypeId, user);
        }
        catch (Exception e) {
            logger.error("Failed commit authentication failure event: ", e);
        }

        response.setHeader("error", exception.getMessage());
        response.sendError(HttpStatus.UNAUTHORIZED.value());
    }

}
