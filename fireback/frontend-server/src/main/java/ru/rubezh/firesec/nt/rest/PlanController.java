package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.PlanRepository;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Plan;
import ru.rubezh.firesec.nt.service.v1.PlanService;
import ru.rubezh.firesec.nt.service.v1.PlanService.PlanReferences;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/plans")
public class PlanController {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PlanService planService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> get(@PathVariable String projectId) {
        List<Plan> plans = planRepository.findByProjectId(projectId);
        return ResponseEntity.ok(plans);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{planId}")
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String planId) {
        Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
        if (plan == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(plan);
    }

    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody Plan plan) {
        StringBuilder errorMessage = new StringBuilder();
        Plan createdPlan = planService.add(projectId, language, plan, errorMessage);
        if (createdPlan != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPlan);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{planId}")
    public ResponseEntity<?> update(@PathVariable String projectId, @PathVariable String planId,
            @RequestBody Plan plan) {
        StringBuilder errorMessage = new StringBuilder();
        Plan updatePlan = planService.update(projectId, language, planId, plan,
                errorMessage);
        if (updatePlan != null) {
            return ResponseEntity.ok(updatePlan);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{planId}")
    public ResponseEntity<?> remove(@PathVariable String projectId, @PathVariable String planId) {
        StringBuilder errorMessage = new StringBuilder();
        PlanReferences plansAndGroupsModifier = planService.remove(projectId, language, planId,
                errorMessage);
        if (plansAndGroupsModifier != null) {
            return ResponseEntity.ok(plansAndGroupsModifier);
        } else {
            logger.error(errorMessage.toString());
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
