package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.ProjectRepository;
import ru.rubezh.firesec.nt.dao.v1.VirtualStateRepository;
import ru.rubezh.firesec.nt.domain.v1.VirtualState;
import ru.rubezh.firesec.nt.service.v1.VirtualStateService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/virtual_states")
public class VirtualStateController {

    @Autowired
    private VirtualStateService virtualStateService;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @PreAuthorize("hasRole('VIRTUAL_STATE_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> createNew(@PathVariable String projectId,
                                       @RequestBody VirtualState virtualState) {

        StringBuilder errorMessage = new StringBuilder();
        virtualState.setProjectId(projectId);
        VirtualState savedVirtualState = virtualStateService.create(virtualState, errorMessage);
        if (savedVirtualState != null)
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(savedVirtualState);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('VIRTUAL_STATE_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<?> update(@PathVariable String projectId,
                                    @PathVariable String id,
                                    @RequestBody VirtualState virtualState) {

        StringBuilder errorMessage = new StringBuilder();
        virtualState.setProjectId(projectId);
        VirtualState savedVirtualState = virtualStateService.update(id, virtualState, errorMessage);
        if (savedVirtualState != null)
            return ResponseEntity.ok(savedVirtualState);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @PreAuthorize("hasRole('VIRTUAL_STATE_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<?> removeById(@PathVariable String projectId,
                                        @PathVariable String id) {

        StringBuilder errorMessage = new StringBuilder();
        if (virtualStateService.removeByIdAndProjectId(id, projectId, errorMessage))
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll(@PathVariable String projectId) {

        if (projectRepository.exists(projectId)) {
            return ResponseEntity.ok(virtualStateRepository.findByProjectId(projectId));
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Проект не найден (" + projectId + ")"));
        }
    }

}
