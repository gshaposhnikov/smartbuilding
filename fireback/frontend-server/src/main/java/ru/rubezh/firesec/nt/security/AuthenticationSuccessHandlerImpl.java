package ru.rubezh.firesec.nt.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.service.v1.EventService;

@Component
public class AuthenticationSuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler {

    private final static String authenticationSuccessEventTypeId = "authenticationSuccess";

    @Autowired
    private Logger logger;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventService eventService;

    public AuthenticationSuccessHandlerImpl() {
        super();
        setUseReferer(true);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        User authenticationUser = (User)authentication.getPrincipal();
        logger.info("Authentication success. Username: {}, Roles: {}",
                    authenticationUser.getUsername(), authenticationUser.getAuthorities());

        try {
            ru.rubezh.firesec.nt.domain.v1.User user = userRepository.findByName(authenticationUser.getUsername());

            eventService.commitEventByEventTypeIdAndUsername(authenticationSuccessEventTypeId, user);
        }
        catch (Exception e) {
            logger.error("Failed commit authentication success event: ", e);
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }
}