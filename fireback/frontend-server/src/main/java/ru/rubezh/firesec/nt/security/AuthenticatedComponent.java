package ru.rubezh.firesec.nt.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.rubezh.firesec.nt.dao.v1.UserRepository;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.service.v1.UserService;

public abstract class AuthenticatedComponent {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected UserService userService;

    public User findCurrentUser() {
        User currentUser = null;
        if (SecurityContextHolder.getContext() != null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null && auth.isAuthenticated()) {
                currentUser = userService.fillGroupPermissions(userRepository.findByName(auth.getName()));
            }
        }
        return currentUser;
    }
}
