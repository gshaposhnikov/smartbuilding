package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.PlanRepository;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/active_plans")
public class ActivePlanController {

    @Autowired
    private PlanRepository planRepository;

    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll(@PathVariable String projectId) {

        return ResponseEntity.ok(planRepository.findByProjectId(projectId));
    }

}
