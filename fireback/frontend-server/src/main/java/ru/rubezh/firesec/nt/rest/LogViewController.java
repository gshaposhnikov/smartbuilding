package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.dao.v1.LogViewRepository;
import ru.rubezh.firesec.nt.domain.v1.LogView;
import ru.rubezh.firesec.nt.service.v1.LogViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/log_views")
public class LogViewController {

    @Autowired
    private LogViewService logViewService;

    @Autowired
    private LogViewRepository logViewRepository;

    @PreAuthorize("hasRole('LOG_VIEW_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> createNew(@RequestBody LogView logView) {
        StringBuilder errorMessage = new StringBuilder();
        if (logViewService.add(logView, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('LOG_VIEW_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{logViewId}")
    public ResponseEntity<?> update(@PathVariable String logViewId,
                                    @RequestBody LogView logView) {
        StringBuilder errorMessage = new StringBuilder();
        if (logViewService.update(logViewId, logView, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @PreAuthorize("hasRole('LOG_VIEW_READ')")
    @RequestMapping(method = RequestMethod.GET, value = {"", "/"})
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(logViewRepository.findByRemovedIsFalse());
    }

    @PreAuthorize("hasRole('LOG_VIEW_READ')")
    @RequestMapping(method = RequestMethod.GET, value = "/{logViewId}")
    public ResponseEntity<?> getById(@PathVariable String logViewId) {
        LogView logView = logViewRepository.findOneByRemovedIsFalseAndId(logViewId);
        if (logView != null){
            return ResponseEntity.ok(logView);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse("Представление " + logViewId + " не найдено."));
        }
    }

    @PreAuthorize("hasRole('LOG_VIEW_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{logViewId}")
    public ResponseEntity<?> remove(@PathVariable String logViewId) {
        StringBuilder errorMessage = new StringBuilder();
        if (logViewService.remove(logViewId, errorMessage)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }
}
