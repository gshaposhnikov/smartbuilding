package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.dao.v1.IndicatorPanelRepository;
import ru.rubezh.firesec.nt.domain.v1.Indicator;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.service.v1.IndicatorPanelService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/indicator_panels")
public class IndicatorPanelController{

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private IndicatorPanelService indicatorPanelService;

    @RequestMapping(method = RequestMethod.GET, value = {"","/"})
    public ResponseEntity<?> getAll(@PathVariable String projectId){
        return ResponseEntity.ok(indicatorPanelRepository.findByProjectId(projectId));
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/{id}"})
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String id) {
        IndicatorPanel indicatorPanel = indicatorPanelRepository.findByProjectIdAndId(projectId, id);
        if (indicatorPanel != null)
            return ResponseEntity.ok(indicatorPanel);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse("Панель индикаторов не найдена"));
    }

    @RequestMapping(method = RequestMethod.POST, value = {"", "/"})
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody IndicatorPanel indicatorPanel) {
        StringBuilder errorMessage = new StringBuilder();
        IndicatorPanel addedPanel = indicatorPanelService.addNew(indicatorPanel, projectId, errorMessage);
        if (addedPanel != null)
            return ResponseEntity.ok(addedPanel);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.PUT, value = {"/{id}"})
    public ResponseEntity<?> update(@RequestBody IndicatorPanel indicatorPanel,
                                    @PathVariable String id, @PathVariable String projectId) {
        StringBuilder errorMessage = new StringBuilder();
        IndicatorPanel updatedPanel = indicatorPanelService.update(indicatorPanel, id, projectId, errorMessage);
        if (updatedPanel != null)
            return ResponseEntity.ok(updatedPanel);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = {"/{id}"})
    public ResponseEntity<?> remove(@PathVariable String projectId, @PathVariable String id) {
        StringBuilder errorMessage = new StringBuilder();
        if (indicatorPanelService.remove(projectId, id, errorMessage)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = {"/{id}"}, params = {"col", "row"})
    public ResponseEntity<?> updateIndicator(@PathVariable String projectId, @PathVariable String id,
                                             @RequestParam int col, @RequestParam int row,
                                             @RequestBody Indicator indicator) {
        StringBuilder errorMessage = new StringBuilder();
        IndicatorPanel updatedPanel = indicatorPanelService.updateIndicator(indicator, projectId, id,
                col, row, errorMessage);
        if (updatedPanel != null)
            return ResponseEntity.ok(updatedPanel);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }
}