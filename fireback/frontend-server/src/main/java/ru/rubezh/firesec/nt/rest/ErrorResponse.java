package ru.rubezh.firesec.nt.rest;

/**
 * Ответ клиенту с текстом ошибки
 * @author Александр Горячкин
 */
public class ErrorResponse {
    String error = "";

    public ErrorResponse(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
