package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.ApplicationInfo;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/dictionaries")
public class DictionaryController {

    @Value("#{'${application.version:}'}")
    private String projectVersion;

    @RequestMapping(method = RequestMethod.GET, value = "/application")
    public ResponseEntity<?> getApplicationInfo() {

        return ResponseEntity.ok(new ApplicationInfo(projectVersion));
    }

}
