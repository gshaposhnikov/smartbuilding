package ru.rubezh.firesec.nt.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.rubezh.firesec.nt.domain.v1.DeviceShapeLibrary;
import ru.rubezh.firesec.nt.service.v1.DeviceShapeLibraryService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/device_shape_libraries")
public class DeviceShapeLibraryController {

    @Autowired
    private DeviceShapeLibraryService deviceShapeLibraryService;

    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> getDeviceShapeLibraries() {
        StringBuilder errorMessage = new StringBuilder();
        List<DeviceShapeLibrary> deviceShapeLibraries = deviceShapeLibraryService.getActual(errorMessage);
        if (deviceShapeLibraries != null) {
            return ResponseEntity.ok(deviceShapeLibraries);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{deviceShapeLibraryId}")
    public ResponseEntity<?> getDeviceShapeLibrary(@PathVariable String deviceShapeLibraryId) {
        StringBuilder errorMessage = new StringBuilder();
        DeviceShapeLibrary deviceShapeLibrary = deviceShapeLibraryService.getIfActual(deviceShapeLibraryId,
                errorMessage);
        if (deviceShapeLibrary != null) {
            return ResponseEntity.ok(deviceShapeLibrary);
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> addDeviceShapeLibrary(@RequestBody DeviceShapeLibrary deviceShapeLibrary) {
        StringBuilder errorMessage = new StringBuilder();
        boolean added = deviceShapeLibraryService.add(deviceShapeLibrary, errorMessage);
        if (added) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = { "/{deviceShapeLibraryId}" })
    public ResponseEntity<?> updateDeviceShapeLibrary(@PathVariable String deviceShapeLibraryId,
            @RequestBody DeviceShapeLibrary deviceShapeLibrary) {
        StringBuilder errorMessage = new StringBuilder();
        boolean updated = deviceShapeLibraryService.update(deviceShapeLibraryId, deviceShapeLibrary, errorMessage);
        if (updated) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{deviceShapeLibraryId}")
    public ResponseEntity<?> removeDeviceShapeLibrary(@PathVariable String deviceShapeLibraryId) {
        StringBuilder errorMessage = new StringBuilder();
        boolean removed = deviceShapeLibraryService.remove(deviceShapeLibraryId, errorMessage);
        if (removed) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
        }
    }

}
