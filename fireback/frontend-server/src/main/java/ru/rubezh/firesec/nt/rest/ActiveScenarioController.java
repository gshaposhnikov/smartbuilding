package ru.rubezh.firesec.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedActiveScenarioView;
import ru.rubezh.firesec.nt.service.v1.ScenarioViewService;

@CrossOrigin
@RestController
@RequestMapping("${server.contextPath}/projects/{projectId}/active_scenarios")
public class ActiveScenarioController {

    @Autowired
    private ScenarioViewService scenarioViewService;

    @RequestMapping(value = {"", "/"})
    public ResponseEntity<?> getAllAggregatedActiveScenarioViews(@PathVariable String projectId) {

        return ResponseEntity.ok(scenarioViewService.getAllAggregatedActiveScenarioViews(projectId));
    }

    @RequestMapping(value = "/{id}")
    public ResponseEntity<?> getAggregatedActiveScenarioView(@PathVariable String projectId,
                                                             @PathVariable String scenarioId) {

        StringBuilder errorMessage = new StringBuilder();
        AggregatedActiveScenarioView result =
                scenarioViewService.getAggregatedActiveScenarioView(projectId, scenarioId, errorMessage);
        if (result != null)
            return ResponseEntity.ok(result);
        else
            return ResponseEntity.badRequest().body(new ErrorResponse(errorMessage.toString()));
    }

}
