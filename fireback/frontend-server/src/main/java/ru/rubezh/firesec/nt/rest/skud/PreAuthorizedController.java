package ru.rubezh.firesec.nt.rest.skud;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ru.rubezh.firesec.nt.dao.v1.ProjectEntityRepository;
import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;
import ru.rubezh.firesec.nt.rest.ProjectEntityController;

public class PreAuthorizedController<T extends BasicEntityWithDate, R extends ProjectEntityRepository<T>>
        extends ProjectEntityController<T, R> {

    @Override
    @PreAuthorize("hasRole('SKUD_READ')")
    @RequestMapping(method = RequestMethod.GET, value = { "", "/" })
    public ResponseEntity<?> get(@PathVariable String projectId) {
        return super.get(projectId);
    }

    @Override
    @PreAuthorize("hasRole('SKUD_READ')")
    @RequestMapping(method = RequestMethod.GET, value = "/{entityId}")
    public ResponseEntity<?> get(@PathVariable String projectId, @PathVariable String entityId) {
        return super.get(projectId, entityId);
    }

    @Override
    @PreAuthorize("hasRole('SKUD_CREATE')")
    @RequestMapping(method = RequestMethod.POST, value = { "", "/" })
    public ResponseEntity<?> add(@PathVariable String projectId, @RequestBody T entity) {
        return super.add(projectId, entity);
    }

    @Override
    @PreAuthorize("hasRole('SKUD_UPDATE')")
    @RequestMapping(method = RequestMethod.PUT, value = { "/{entityId}" })
    public ResponseEntity<?> update(@PathVariable String projectId, @PathVariable String entityId,
            @RequestBody T entity) {
        return super.update(projectId, entityId, entity);
    }

    @Override
    @PreAuthorize("hasRole('SKUD_REMOVE')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{entityId}")
    public ResponseEntity<?> remove(@PathVariable String projectId, @PathVariable String entityId) {
        return super.remove(projectId, entityId);
    }

}
