package ru.rubezh.firesec.nt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import ru.rubezh.firesec.nt.dao.v1.StateCategoryViewRepository;
import ru.rubezh.firesec.nt.dao.v1.StateViewRepository;
import ru.rubezh.firesec.nt.domain.v1.EventTypeView;
import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;
import ru.rubezh.firesec.nt.domain.v1.StateView;
import ru.rubezh.firesec.nt.service.v1.EventViewService;

@Configuration
public class FrontendServerApplication {

    @Autowired
    public FrontendServerApplication(StateCategoryViewRepository stateCategoryViewRepository,
            List<StateCategoryView> stateCategoryViews, EventViewService eventViewService,
            List<EventTypeView> eventTypeViews, StateViewRepository stateViewRepository,
            List<StateView> logicStateViews) {

        stateCategoryViewRepository.deleteAll();
        stateCategoryViewRepository.insert(stateCategoryViews);

        eventViewService.addEventTypeViews(eventTypeViews);

        stateViewRepository.delete(logicStateViews);
        stateViewRepository.insert(logicStateViews);
    }

}
