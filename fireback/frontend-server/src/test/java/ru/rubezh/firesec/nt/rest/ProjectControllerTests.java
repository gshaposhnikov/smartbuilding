package ru.rubezh.firesec.nt.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import ru.rubezh.firesec.nt.dao.v1.ProjectRepository;
import ru.rubezh.firesec.nt.domain.v1.Project;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/frontend-server-servlet.xml" })
public class ProjectControllerTests {

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    private MockMvc mockMvc;

    @Autowired
    private ProjectController projectController;

    @Autowired
    private ProjectRepository projectRepository;

    private List<String> beforeTestProjectIds = new ArrayList<>();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(projectController)
                .addPlaceholderValue("server.contextPath", "/api/v1")
                .apply(documentationConfiguration(restDocumentation))
                .build();

        List<Project> beforeTestProjects = projectRepository.findAll();
        for (Project project : beforeTestProjects) {
            beforeTestProjectIds.add(project.getId());
        }
    }

    @After
    public void tearDown() {
        projectRepository.deleteByIdNotIn(beforeTestProjectIds);
    }

    @Test
    public void addProjectTest() throws Exception {
        Project testProject = new Project();
        testProject.setName("Тест создание");
        testProject.setVersion("0.1");
        testProject.setDescription("Тестовый проект");
        testProject.setUserId("admin");

        ObjectMapper mapper = new ObjectMapper();

        ResultActions resultActions = mockMvc
                .perform(post("/api/v1/projects").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsBytes(testProject)))
                .andDo(document("{class-name}/{method-name}",
                        preprocessRequest(prettyPrint()),
                        preprocessResponse(prettyPrint()),
                        responseFields(fieldWithPath("id").description("Идентификатор (возвращается сервером)"),
                                fieldWithPath("name").description("Название (обязательное поле)"),
                                fieldWithPath("version").description("Версия (обязательное поле)"),
                                fieldWithPath("userId").description("Автор (обязательное поле)"),
                                fieldWithPath("imported")
                                        .description("Признак - был ли проект импортирован (возвращается сервером)"),
                                fieldWithPath("importValidateMessages")
                                        .description("Сообщения об ошибках импорта проекта (возвращается сервером)"),
                                fieldWithPath("status").description("Cтатус (возвращается сервером)"),
                                fieldWithPath("description").description("Описание"),
                                fieldWithPath("createDateTime").description("Дата создания (возвращается сервером)"),
                                fieldWithPath("updateDateTime")
                                        .description("Дата последнего изменения (возвращается сервером)"))));

        resultActions.andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", equalTo(testProject.getName())))
                .andExpect(jsonPath("$.version", equalTo(testProject.getVersion())))
                .andExpect(jsonPath("$.description", equalTo(testProject.getDescription())));

    }

    @Test
    public void getAllProjectsTest() throws Exception {
        Project testProject1 = new Project();
        testProject1.setName("Тест получение всех");
        testProject1.setVersion("1");
        testProject1.setUserId("admin");

        Project testProject2 = new Project();
        testProject2.setName("Тест получение всех");
        testProject2.setVersion("2");
        testProject2.setUserId("admin");

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(post("/api/v1/projects").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsBytes(testProject1)));
        mockMvc.perform(post("/api/v1/projects").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsBytes(testProject2)));

        ResultActions resultActions = mockMvc
                .perform(get("/api/v1/projects"))
                .andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}",
                        preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())));


        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()", greaterThanOrEqualTo(2)));

    }

    @Test
    public void getOneProjectTest() throws Exception {
        Project testProject = new Project();
        testProject.setName("Тест получение одного");
        testProject.setVersion("1");
        testProject.setUserId("admin");

        ObjectMapper mapper = new ObjectMapper();

        MvcResult postResult = mockMvc.perform(post("/api/v1/projects").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsBytes(testProject))).andReturn();

        Project createdProject = mapper.readValue(postResult.getResponse().getContentAsByteArray(), Project.class);

        ResultActions resultActions = mockMvc.perform(get("/api/v1/projects/" + createdProject.getId()));

        /* Нужно дождаться, когда придет весь ответ, т.к. метод асинхронный */
        Thread.sleep(1000);

        resultActions.andDo(MockMvcRestDocumentation.document("{class-name}/{method-name}",
                preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())));
        
    }

}
