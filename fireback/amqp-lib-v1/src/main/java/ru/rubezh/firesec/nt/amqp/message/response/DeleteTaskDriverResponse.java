package ru.rubezh.firesec.nt.amqp.message.response;

import java.util.ArrayList;
import java.util.List;

public class DeleteTaskDriverResponse extends AbstractDriverResponse {

    List<String> deletedIssueIds = new ArrayList<>();

    public List<String> getDeletedIssueIds() {
        return deletedIssueIds;
    }

    public void setDeletedIssueIds(List<String> deletedIssueIds) {
        this.deletedIssueIds = deletedIssueIds;
    }
}
