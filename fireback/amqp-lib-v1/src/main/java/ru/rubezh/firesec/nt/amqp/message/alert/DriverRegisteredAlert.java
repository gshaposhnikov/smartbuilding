package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

public class DriverRegisteredAlert extends AbstractDriverAlert {

    String requestQueue;
    String alertsExchange;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DRIVER;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONNECTION;
    }
    
    public String getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(String requestQueue) {
        this.requestQueue = requestQueue;
    }

    public String getAlertsExchange() {
        return alertsExchange;
    }

    public void setAlertsExchange(String alertsExchange) {
        this.alertsExchange = alertsExchange;
    }

}
