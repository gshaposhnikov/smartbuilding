package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEntities;

/**
 * Уведомление об окончании чтения сохранённой конфигурации прибора
 * 
 * @author Андрей Лисовой
 *
 */
public class DatabaseReadingCompleted extends AbstractAlert {
    /** Идентификатор задачи чтения базы */
    private String issueId;
    /** Текущая конфигурация прибора */
    private ControlDeviceEntities currentEntities;
    /** Считанная конфигурация */
    private ControlDeviceEntities recoveredEntities;
    
    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public ControlDeviceEntities getCurrentEntities() {
        return currentEntities;
    }

    public void setCurrentEntities(ControlDeviceEntities currentEntities) {
        this.currentEntities = currentEntities;
    }

    public ControlDeviceEntities getRecoveredEntities() {
        return recoveredEntities;
    }

    public void setRecoveredEntities(ControlDeviceEntities recoveredEntities) {
        this.recoveredEntities = recoveredEntities;
    }

}
