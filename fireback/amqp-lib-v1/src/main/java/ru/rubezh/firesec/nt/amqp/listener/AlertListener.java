package ru.rubezh.firesec.nt.amqp.listener;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.alert.AbstractAlert;

/**
 * Шаблоный обработчик оповещений из очереди.
 * @author Александр Горячкин
 */
public class AlertListener extends BaseListener {

    public AlertListener(Logger logger) {
        super(logger);
    }

    /**
     * Обработчик оповещений по умолчанию.
     *
     * @param message
     *            Базовый класс оповещения.
     */
    public void handleMessage(AbstractAlert message) {
        logger.trace(
                "Unknown alert message received. Message type: {}. "
                + " routing: { level: {}, object: {}, type: {} }",
                message.getClass().getName(),
                message.getRoutingLevel(),
                message.getRoutingObject(),
                message.getRoutingType());
    }

}
