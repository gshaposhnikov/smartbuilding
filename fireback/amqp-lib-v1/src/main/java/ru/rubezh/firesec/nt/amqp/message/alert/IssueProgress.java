package ru.rubezh.firesec.nt.amqp.message.alert;

import java.util.List;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;

/**
 * Уведомление об изменении прогреса задачи.
 * @author Александр Горячкин
 *
 */
public class IssueProgress extends AbstractAlert {
    /** Идентификатор задачи в БД */
    private String issueId;
    /** Прогресс задачи */
    private double progress = 0;
    /** Новый статус задачи (если не изменился - NONE) */
    private IssueStatus status = IssueStatus.NONE;
    /** Дополнительная информация о статусе (описание ошибки) */
    private String statusMessage = "";
    /**
     * Сообщения валидации (для возврата в качестве результата выполнения задачи более одной ошибки а также
     * предупреждений)
     */
    private List<ActivationValidateMessage> validateMessages;
    

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.ISSUE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<ActivationValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public void setValidateMessages(List<ActivationValidateMessage> validateMessages) {
        this.validateMessages = validateMessages;
    }

}
