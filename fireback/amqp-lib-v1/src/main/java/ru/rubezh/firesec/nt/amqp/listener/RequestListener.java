package ru.rubezh.firesec.nt.amqp.listener;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.request.AbstractRequest;

/**
 * Шаблоный обработчик запросов из очереди.
 * @author Александр Горячкин
 */
public class RequestListener extends BaseListener {

    public RequestListener(Logger logger) {
        super(logger);
    }

    /**
     * Обработчик запросов по умолчанию.
     *
     * @param message
     *            Базовый класс запроса.
     */
    void handleMessage(AbstractRequest message) {
        if (logger != null) {
            logger.trace("Unknown request message received. Message type: {}", message.getClass().getName());
        }
    }

}
