package ru.rubezh.firesec.nt.amqp.message.alert;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.rubezh.firesec.nt.amqp.message.AbstractMessage;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;


/* TODO: добавить роутинг по классам (StateCategory) и подсистемам. */
public abstract class AbstractAlert extends AbstractMessage {

    @JsonIgnore
    public abstract RoutingLevel getRoutingLevel();

    @JsonIgnore
    public abstract RoutingObject getRoutingObject();

    @JsonIgnore
    public abstract RoutingType getRoutingType();

}
