package ru.rubezh.firesec.nt.amqp.message.response;

import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;

public class GetLicenceRestrictionsResponse extends AbstractDriverResponse {

    /** Лицензионные ограничения */
    private LicenseRestrictions restrictions;

    public LicenseRestrictions getLicenseRestrictions() {
        return restrictions;
    }

    public void setLicenseRestrictions(LicenseRestrictions restrictions) {
        this.restrictions = restrictions;
    }

}
