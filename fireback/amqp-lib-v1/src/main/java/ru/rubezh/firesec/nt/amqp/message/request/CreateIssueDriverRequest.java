package ru.rubezh.firesec.nt.amqp.message.request;

import ru.rubezh.firesec.nt.domain.v1.Issue;

public class CreateIssueDriverRequest extends AbstractRoutedDriverRequest {

    private String projectId;
    private String deviceId;
    private Issue issue;
    private String driverRequestQueueName;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public String getDriverRequestQueueName() {
        return driverRequestQueueName;
    }

    public void setDriverRequestQueueName(String driverRequestQueueName) {
        this.driverRequestQueueName = driverRequestQueueName;
    }
}
