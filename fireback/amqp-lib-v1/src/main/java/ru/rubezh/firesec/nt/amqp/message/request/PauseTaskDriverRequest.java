package ru.rubezh.firesec.nt.amqp.message.request;

/**
 * Запрос на запрет/разрешение выполнения задачи.
 *
 * @author Александр Горячкин
 */
public class PauseTaskDriverRequest extends AbstractRoutedDriverRequest {

    /** Идентификатор задачи */
    private String issueId;

    /** Запретить выполнять задачу */
    private boolean isPause;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public boolean isPause() {
        return isPause;
    }

    public void setPause(boolean pause) {
        isPause = pause;
    }
}
