package ru.rubezh.firesec.nt.amqp.message.response;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.Issue;

public class CreateIssueDriverResponse extends AbstractDriverResponse {
    /** Фактические задачи, взятые в работу */
    private List<Issue> actualIssues = new ArrayList<>();

    public List<Issue> getActualIssues() {
        return actualIssues;
    }

    public void setActualIssues(List<Issue> actualIssues) {
        this.actualIssues = actualIssues;
    }

}
