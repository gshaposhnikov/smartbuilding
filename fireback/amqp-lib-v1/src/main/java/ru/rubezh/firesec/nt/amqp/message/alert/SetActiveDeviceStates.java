package ru.rubezh.firesec.nt.amqp.message.alert;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Оповещение с полным перечнем активных на текущий момент состояний устройства.
 * 
 * @author Антон Васильев
 *
 */
public class SetActiveDeviceStates extends AbstractDriverAlert {
    
    /** Идентификатор устройства (оперативной части) */
    private String activeDeviceId = "";
    
    /** Набор идентификаторов активных состояний (согласно профилю устройства) */
    private List<String> activeStates = new ArrayList<>();

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public SetActiveDeviceStates() {
        super();
    }
    
    public SetActiveDeviceStates(String activeDeviceId, List<String> activeStates) {
        super();
        this.activeDeviceId = activeDeviceId;
        this.activeStates = activeStates;
    }

    public String getActiveDeviceId() {
        return activeDeviceId;
    }

    public void setActiveDeviceId(String activeDeviceId) {
        this.activeDeviceId = activeDeviceId;
    }

    public List<String> getActiveStates() {
        return activeStates;
    }

    public void setActiveStates(List<String> activeStates) {
        this.activeStates = activeStates;
    }
}
