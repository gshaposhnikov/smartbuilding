package ru.rubezh.firesec.nt.amqp.message.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Запрос на изменение конфигурации активного устройства
 * в подготовленной базе прибора.
 *
 * @author Александр Горячкин
 *
 */
public class SetDriverDeviceConfigRequest extends AbstractRoutedDriverRequest {
    /** Идентификатор проекта */
    private String projectId;

    /** Идентификатор устройства */
    private String deviceId;

    /** Значения параметров конфигурации в устройстве */
    private Map<String, String> activeConfigValues = new HashMap<>();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Map<String, String> getActiveConfigValues() {
        return activeConfigValues;
    }

    public void setActiveConfigValues(Map<String, String> activeConfigValues) {
        this.activeConfigValues = activeConfigValues;
    }
}
