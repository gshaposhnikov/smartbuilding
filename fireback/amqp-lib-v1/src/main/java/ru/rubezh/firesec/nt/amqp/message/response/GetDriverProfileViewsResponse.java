package ru.rubezh.firesec.nt.amqp.message.response;

import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;

public class GetDriverProfileViewsResponse extends AbstractDriverResponse {

    private List<DriverProfileView> driverProfileViews;

    public List<DriverProfileView> getDriverProfileViews() {
        return driverProfileViews;
    }

    public void setDriverProfileViews(List<DriverProfileView> driverProfileViews) {
        this.driverProfileViews = driverProfileViews;
    }

}
