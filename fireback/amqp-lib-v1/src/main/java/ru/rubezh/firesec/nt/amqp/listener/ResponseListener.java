package ru.rubezh.firesec.nt.amqp.listener;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.response.AbstractResponse;

/**
 * Шаблоный обработчик ответов из очереди.
 * @author Александр Горячкин
 */
public class ResponseListener extends BaseListener {

    public ResponseListener(Logger logger) {
        super(logger);
    }

    /**
     * Обработчик ответов по умолчанию.
     *
     * @param message
     *            Базовый класс ответа.
     */
    void handleMessage(AbstractResponse message) {
        if (logger != null) {
            logger.trace("Unknown response message received. Message type: {}."
                    + " Error: { code: {}, message: {} }",
                    message.getClass().getName(),
                    message.getError(),
                    message.getErrorString());
        }
    }

}
