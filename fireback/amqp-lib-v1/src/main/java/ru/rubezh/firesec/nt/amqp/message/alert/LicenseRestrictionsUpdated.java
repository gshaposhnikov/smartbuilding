package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.license.LicenseRestrictions;

/**
 * Уведомление об изменении лицензионных ограничений
 */
public class LicenseRestrictionsUpdated extends AbstractAlert {

    /** Лицензионные ограничения */
    private LicenseRestrictions restrictions;
    
    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DRIVER;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    public LicenseRestrictions getLicenseRestrictions() {
        return restrictions;
    }

    public void setLicenseRestrictions(LicenseRestrictions restrictions) {
        this.restrictions = restrictions;
    }

}
