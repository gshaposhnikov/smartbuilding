package ru.rubezh.firesec.nt.amqp.sender;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.request.AbstractRequest;
import ru.rubezh.firesec.nt.amqp.message.response.AbstractResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;

/**
 * Класс для отправки запросов.
 *
 * @author Александр Горячкин
 */
public class RequestSender extends AbstractSender {

    @NotNull
    protected Logger logger;
    protected RabbitTemplate rabbitTemplate;
    protected String requestQueueName;

    public RequestSender(Logger logger, RabbitTemplate rabbitTemplate, String requestQueueName) {
        this.logger = logger;
        this.rabbitTemplate = rabbitTemplate;
        this.requestQueueName = requestQueueName;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public String getRequestQueueName() {
        return requestQueueName;
    }

    public void setRequestQueueName(String requestQueueName) {
        this.requestQueueName = requestQueueName;
    }

    /**
     * Метод для отправки запросов.
     *
     * @param message
     *            Базовый класс запроса.
     */
    protected void sendRequest(AbstractRequest message) {
        if (message != null && rabbitTemplate != null && requestQueueName != null) {
            rabbitTemplate.convertAndSend("", requestQueueName, message);
        }
    }

    /**
     * Метод для отправки запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    protected AbstractResponse sendRequestAndReceiveResponse(AbstractRequest message) {
        if (message != null && rabbitTemplate != null && requestQueueName != null) {
            try {
                return (AbstractResponse) rabbitTemplate.convertSendAndReceive("", requestQueueName, message);
            } catch (ClassCastException e) {
                return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Response has bad type");
            }
        } else {
            return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Message is empty");
        }
    }

    /**
     * Метод для отправки базового запроса.
     *
     * @param message
     *            Базовый класс запроса.
     */
    public void send(AbstractRequest message) {
        logger.trace("Request message send. Message type: {}.", message.getClass().getName());

        sendRequest(message);
    }

    /**
     * Метод для отправки базового запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    public AbstractResponse sendAndReceive(AbstractRequest message) {
        logger.trace("Request message send and receive. Message type: {}.", message.getClass().getName());

        return sendRequestAndReceiveResponse(message);
    }

}
