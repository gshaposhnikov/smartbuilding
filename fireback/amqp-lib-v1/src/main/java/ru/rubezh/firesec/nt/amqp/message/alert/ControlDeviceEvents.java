package ru.rubezh.firesec.nt.amqp.message.alert;

import java.util.List;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;

public class ControlDeviceEvents extends AbstractDriverAlert {
    List<ControlDeviceEvent> controlDeviceEvents;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.EVENT;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public List<ControlDeviceEvent> getControlDeviceEvents() {
        return controlDeviceEvents;
    }

    public void setControlDeviceEvents(List<ControlDeviceEvent> controlDeviceEvents) {
        this.controlDeviceEvents = controlDeviceEvents;
    }
}
