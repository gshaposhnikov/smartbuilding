package ru.rubezh.firesec.nt.amqp.message.alert.route;

/* Внимание!!!
 * При добавлении здесь нового ключа необходимо удостовериться,
 * что на него подписаны используемые очереди оповещений.
 */
public enum RoutingObject {
    DRIVER,
    DEVICE,
    EVENT,
    ISSUE,
    PROJECT,
    ACCESS_KEY;
}
