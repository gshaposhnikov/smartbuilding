package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.domain.v1.DeviceType;

import java.util.Objects;

public class DeviceInfo{

    private int regionNo;
    private DeviceType deviceType;
    private int deviceNo;

    DeviceInfo(int regionNo, DeviceType deviceType, int deviceNo) {
        this.regionNo = regionNo;
        this.deviceType = deviceType;
        this.deviceNo = deviceNo;
    }

    public int getRegionNo() {
        return regionNo;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public int getDeviceNo() {
        return deviceNo;
    }

    public void setRegionNo(int regionNo) {
        this.regionNo = regionNo;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public void setDeviceNo(int deviceNo) {
        this.deviceNo = deviceNo;
    }

    @Override
    public String toString() {
        return "{" +
                "regionNo=" + regionNo +
                ", deviceType=" + deviceType +
                ", deviceNo=" + deviceNo +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceInfo that = (DeviceInfo) o;
        return regionNo == that.regionNo &&
                deviceNo == that.deviceNo &&
                deviceType == that.deviceType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(regionNo, deviceType, deviceNo);
    }
}
