package ru.rubezh.firesec.nt.amqp.sender;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.request.AbstractDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.response.AbstractDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;

public class DriverRequestSender extends RequestSender {

    private DriverMessagePostProcessor driverMessagePostProcessor;

    public DriverRequestSender(Logger logger, RabbitTemplate rabbitTemplate, String requestQueueName,
                               DriverMessagePostProcessor driverMessagePostProcessor) {
        super(logger, rabbitTemplate, requestQueueName);
        this.driverMessagePostProcessor = driverMessagePostProcessor;
    }

    /**
     * Метод для отправки базового запроса.
     *
     * @param message
     *            Базовый класс запроса.
     */
    public void send(AbstractDriverRequest message) {
        logger.trace("Driver request message send. Message type: {}.", message.getClass().getName());

        if (message != null && rabbitTemplate != null &&
                requestQueueName != null && driverMessagePostProcessor != null) {
            rabbitTemplate.convertAndSend(requestQueueName, message, driverMessagePostProcessor);
        }
    }

    /**
     * Метод для отправки базового запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    public AbstractDriverResponse sendAndReceive(AbstractDriverRequest message) {
        logger.trace("Request driver message send and receive. Message type: {}", message.getClass().getName());

        if (message != null && rabbitTemplate != null && requestQueueName != null
                && driverMessagePostProcessor != null) {
            try {
                return (AbstractDriverResponse) rabbitTemplate.convertSendAndReceive(requestQueueName, message, driverMessagePostProcessor);
            } catch (ClassCastException e) {
                return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Response has bad type");
            }
        } else {
            return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Sender not ready");
        }
    }

}
