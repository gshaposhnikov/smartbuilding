package ru.rubezh.firesec.nt.amqp.listener;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.alert.AbstractDriverAlert;

/**
 * Обработчик оповещений драйвера из очереди.
 * @author Александр Горячкин
 */
public class DriverAlertListener extends AlertListener {

    public DriverAlertListener(Logger logger) {
        super(logger);
    }

    void handleMessage(AbstractDriverAlert message) {
        if (logger != null) {
            logger.trace("Unknown driver alert message received. Message type: {}. "
                    + " Driver id: {}. Routing: { level: {}, object: {}, type: {} }",
                    message.getClass().getName(),
                    message.getDriverId(),
                    message.getRoutingLevel(),
                    message.getRoutingObject(),
                    message.getRoutingType());
        }
    }

}
