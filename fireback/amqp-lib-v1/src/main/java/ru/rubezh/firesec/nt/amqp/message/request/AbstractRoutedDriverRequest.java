package ru.rubezh.firesec.nt.amqp.message.request;

/**
 * Базовый класс для запросов, которые маршрутизируются бизнес-логикой к нужному
 * драйверу без изменений, кроме выставления driverId
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class AbstractRoutedDriverRequest extends AbstractDriverRequest {

}
