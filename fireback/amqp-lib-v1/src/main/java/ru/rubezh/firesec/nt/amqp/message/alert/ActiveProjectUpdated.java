package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Оповещение об изменении активного проекта пользователем.
 * 
 * @author Антон Васильев
 *
 */
public class ActiveProjectUpdated extends AbstractAlert {

    /**
     * Идентфикатор проекта.
     */
    private String projectId;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.PROJECT;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
