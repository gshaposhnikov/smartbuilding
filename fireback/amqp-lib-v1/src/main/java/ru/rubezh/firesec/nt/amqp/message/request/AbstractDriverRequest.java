package ru.rubezh.firesec.nt.amqp.message.request;

public abstract class AbstractDriverRequest extends AbstractRequest {

    protected String driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

}
