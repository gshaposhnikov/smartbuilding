package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

public class ControlDeviceInfo extends AbstractDriverAlert {

    /** Идентификатор прибора */
    private String deviceId;
    /** Глобальный универсальный идентификатор прибора */
    private String guid;
    /** Серийный номер прибора */
    private String serial;
    /** БД прибора соответствует проекту */
    private boolean databaseMatchingProject;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean isDatabaseMatchingProject() {
        return databaseMatchingProject;
    }

    public void setDatabaseMatchingProject(boolean databaseMatchingProject) {
        this.databaseMatchingProject = databaseMatchingProject;
    }

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    @Override
    public String toString() {
        return new StringBuilder("Driver id: ").append(getDriverId())
            .append(", device id: ").append(getDeviceId())
            .append(", GUID: ").append(getGuid())
            .append(", serial: ").append(getSerial())
            .append(", databaseMatchingProject: ").append(isDatabaseMatchingProject())
            .toString();
    }

}
