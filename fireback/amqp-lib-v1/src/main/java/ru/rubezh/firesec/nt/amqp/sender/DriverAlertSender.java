package ru.rubezh.firesec.nt.amqp.sender;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.alert.AbstractDriverAlert;

/**
 * Класс для отправки оповещений от драйвера в соответствующий exchange. Для
 * работы, перед вызовом одного из методов {@code send}, должны быть установлены
 * {@link #driverId}, {@link #rabbitTemplate} и {@link #alertsExchangeName}.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 * @author Александр Горячкин
 */
public class DriverAlertSender extends AlertSender {

    private String driverId;

    public DriverAlertSender(Logger logger, RabbitTemplate rabbitTemplate,
                             String alertsExchangeName, String driverId) {
        super(logger, rabbitTemplate, alertsExchangeName);
        this.driverId = driverId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public void send(AbstractDriverAlert message, String zone) {

        logger.info("Driver alert message send. Message type: {}.", message.getClass().getName());

        StringBuilder routingKey = new StringBuilder(getRoutingKey(message));
        routingKey.append('.');
        routingKey.append(driverId);

        if (zone != null && !zone.isEmpty()) {
            routingKey.append('.');
            routingKey.append(zone);
        }

        message.setDriverId(driverId);
        sendAlert(message, routingKey.toString());
    }

    public void send(AbstractDriverAlert message) {
        send(message, null);
    }

}
