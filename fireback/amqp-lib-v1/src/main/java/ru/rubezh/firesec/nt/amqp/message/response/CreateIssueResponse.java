package ru.rubezh.firesec.nt.amqp.message.response;

import ru.rubezh.firesec.nt.domain.v1.Issue;

import java.util.ArrayList;
import java.util.List;

public class CreateIssueResponse extends AbstractResponse {

    /** Созданные задачи */
    private List<Issue> issues = new ArrayList<>();

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    public void addToIssues(Issue issue) {
        issues.add(issue);
    }
}
