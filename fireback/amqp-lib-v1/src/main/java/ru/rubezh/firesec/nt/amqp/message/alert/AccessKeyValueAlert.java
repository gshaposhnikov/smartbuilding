package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent.AccessKeyValueInfo;

/** Прочитано значение ключа доступа */
public class AccessKeyValueAlert extends AbstractDriverAlert {

    /** Информация о значении ключа доступа */
    private AccessKeyValueInfo accessKeyValueInfo;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.ACCESS_KEY;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.DATA_EXCHANGE;
    }

    public AccessKeyValueInfo getAccessKeyValueInfo() {
        return accessKeyValueInfo;
    }

    public void setAccessKeyValueInfo(AccessKeyValueInfo accessKeyValueInfo) {
        this.accessKeyValueInfo = accessKeyValueInfo;
    }

}
