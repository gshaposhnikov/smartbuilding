package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Оповещение, которое бизнес-логика отправляет для уведомления о том, что
 * изменилась конфигурация устройства (активная или проектная)
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class DeviceConfigChanged extends AbstractAlert {

    /** Идентификатор проекта */
    private String projectId;

    /** Идентификатор устройства */
    private String deviceId;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
