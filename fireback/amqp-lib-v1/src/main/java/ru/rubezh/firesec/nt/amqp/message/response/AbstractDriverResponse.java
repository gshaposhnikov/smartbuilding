package ru.rubezh.firesec.nt.amqp.message.response;

public abstract class AbstractDriverResponse extends AbstractResponse {

    protected String driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

}
