package ru.rubezh.firesec.nt.amqp.message.alert.route;

public enum RoutingLevel {
    INFO("info"),
    WARN("warn"),
    ERROR("error");
    
    private String stringValue;
    
    private RoutingLevel(final String stringValue) {
        this.stringValue = stringValue;
    }
    
    @Override
    public String toString() {
        return stringValue;
    }
}
