package ru.rubezh.firesec.nt.amqp.message.response;

public enum ErrorType {
    OK,
    METOD_NOT_SUPPORTED,
    UNKNOWN_DEVICE,
    UNKNOWN_PROJECT,
    UNKNOWN_DRIVER,
    UNKNOWN_REGION,
    INTERNAL_ERROR,
    ISSUE_ACTIVE,
    UNKNOWN_TARGET,
    CONTENT_NOT_SUPPORTED,
    BAD_ISSUE_PARAMETERS,
    FORBIDDEN,
}
