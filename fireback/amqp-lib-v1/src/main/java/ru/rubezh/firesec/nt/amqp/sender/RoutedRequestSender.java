package ru.rubezh.firesec.nt.amqp.sender;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.request.AbstractRequest;
import ru.rubezh.firesec.nt.amqp.message.response.AbstractResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;

/**
 * Класс для отправки запросов, для которых заранее неизвестна очередь для отправки.
 *
 * @author Артем Седанов
 */
public class RoutedRequestSender extends AbstractSender {

    @NotNull
    protected Logger logger;
    protected RabbitTemplate rabbitTemplate;

    public RoutedRequestSender(Logger logger, RabbitTemplate rabbitTemplate) {
        this.logger = logger;
        this.rabbitTemplate = rabbitTemplate;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * Метод для отправки запросов.
     *
     * @param message
     *            Базовый класс запроса.
     */
    protected void sendRequest(String requestQueueName, AbstractRequest message) {
        if (message != null && rabbitTemplate != null && requestQueueName != null) {
            rabbitTemplate.convertAndSend("", requestQueueName, message);
        }
    }

    /**
     * Метод для отправки запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    protected AbstractResponse sendRequestAndReceiveResponse(String requestQueueName, AbstractRequest message) {
        if (message != null && rabbitTemplate != null && requestQueueName != null) {
            try {
                return (AbstractResponse) rabbitTemplate.convertSendAndReceive("", requestQueueName, message);
            } catch (ClassCastException e) {
                return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Response has bad type");
            }
        } else {
            return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Message is empty");
        }
    }

    /**
     * Метод для отправки базового запроса.
     *
     * @param message
     *            Базовый класс запроса.
     */
    public void send(String requestQueueName, AbstractRequest message) {
        logger.trace("Request message to {} send. Message type: {}.", requestQueueName, message.getClass().getName());

        sendRequest(requestQueueName, message);
    }

    /**
     * Метод для отправки базового запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    public AbstractResponse sendAndReceive(String requestQueueName, AbstractRequest message) {
        logger.trace("Request message to {} send and receive. Message type: {}.", requestQueueName, message.getClass().getName());

        return sendRequestAndReceiveResponse(requestQueueName, message);
    }

}
