package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SetEntityStates extends AbstractDriverAlert {

    /** Flag that indicates that apartment light is turned off */
    private boolean allOff;

    /** Region number where the light is off */
    private Set<Integer> offRegionNos = new HashSet<>();

    /** Pair with device info and it's value.
     * Key - info that is needed to determinate the device
     * (format: "regionNo;deviceType;serialDeviceNumber").
     * Value - number of the state.
     */
    private HashMap<String, Integer> deviceInfoWithValues = new HashMap<>();

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public Boolean isAllOff() {
        return allOff;
    }

    public HashMap<String, Integer> getDeviceInfoWithValues() {
        return deviceInfoWithValues;
    }

    public void setAllOff(Boolean allOff) {
        this.allOff = allOff;
    }

    public void addDeviceState(String regionNo, DeviceType deviceType, String deviceNo, int value){
        deviceInfoWithValues.put(regionNo + ";" + deviceType + ";" + deviceNo, value);
    }

    public void addOffRegionNo(int regionNo){
        offRegionNos.add(regionNo);
    }

    public Set<Integer> getOffRegionNos() {
        return offRegionNos;
    }
}
