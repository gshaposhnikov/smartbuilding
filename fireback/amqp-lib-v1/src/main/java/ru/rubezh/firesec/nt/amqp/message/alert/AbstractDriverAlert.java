package ru.rubezh.firesec.nt.amqp.message.alert;

public abstract class AbstractDriverAlert extends AbstractAlert {

    protected String driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

}
