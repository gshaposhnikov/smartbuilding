package ru.rubezh.firesec.nt.amqp.message.response;

import ru.rubezh.firesec.nt.amqp.message.AbstractMessage;

public abstract class AbstractResponse extends AbstractMessage {

    private ErrorType error = ErrorType.OK;
    private String errorString = null;

    public ErrorType getError() {
        return error;
    }

    public void setError(ErrorType error) {
        this.error = error;
    }

    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

}
