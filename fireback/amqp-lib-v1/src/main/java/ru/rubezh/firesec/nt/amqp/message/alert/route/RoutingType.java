package ru.rubezh.firesec.nt.amqp.message.alert.route;

public enum RoutingType {
    CONNECTION("connection"),
    CONFIGURATION("configuration"),
    MONITORING("monitoring"),
    DIAGNOSTIC("diagnostic"),
    DATA_EXCHANGE("data_exchange");
    
    private String stringValue;
    
    private RoutingType(final String stringValue) {
        this.stringValue = stringValue;
    }
    
    @Override
    public String toString() {
        return stringValue;
    }
}
