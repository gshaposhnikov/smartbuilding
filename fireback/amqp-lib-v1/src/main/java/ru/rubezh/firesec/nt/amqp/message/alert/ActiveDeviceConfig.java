package ru.rubezh.firesec.nt.amqp.message.alert;

import java.util.Map;
import java.util.Date;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.IssueAction;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;

/**
 * Оповещение, которое драйвер отправляет при чтении или после записи конфигурации активного устройства.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 *
 */
public class ActiveDeviceConfig extends AbstractDriverAlert {

    /** Идентификатор устройства */
    private String deviceId;

    /** Значения параметров в соответствии с профилем устройства */
    private Map<String, String> configPropertyValues;

    /** Дата и время записи/считывания конфигурации в/с оборудования */
    private Date performedDateTime;

    /** Версия встроенного ПО (прошивки) устройства */
    private String firmwareVersion;

    /** Заводской номер устройства */
    private String serialNumber;

    /** Версия базы данных прибора */
    private String databaseVersion;

    /** Тип выполненого действия */
    private IssueAction action;

    /** Идентификатор задачи*/
    private String issueId;

    /** Статус задачи */
    private IssueStatus status;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.CONFIGURATION;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Map<String, String> getConfigPropertyValues() {
        return configPropertyValues;
    }

    public void setConfigPropertyValues(Map<String, String> configPropertyValues) {
        this.configPropertyValues = configPropertyValues;
    }

    public Date getPerformedDateTime() {
        return performedDateTime;
    }

    public void setPerformedDateTime(Date performedDateTime) {
        this.performedDateTime = performedDateTime;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDatabaseVersion() {
        return databaseVersion;
    }

    public void setDatabaseVersion(String databaseVersion) {
        this.databaseVersion = databaseVersion;
    }

    public IssueAction getAction() {
        return action;
    }

    public void setAction(IssueAction action) {
        this.action = action;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
    }
}
