package ru.rubezh.firesec.nt.amqp.listener;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.message.AbstractMessage;

/**
 * Шаблоный обработчик сообщений из очереди.
 * @author Александр Горячкин
 */
public class BaseListener {

    @NotNull
    protected Logger logger;

    public BaseListener(Logger logger) {
        this.logger = logger;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * Обработчик сообщений по умолчанию.
     *
     * @param message
     *            Базовый класс сообщения.
     */
    void handleMessage(AbstractMessage message) {
        logger.debug("Unknown message received. Message type: {}", message.getClass().getName());
    }

}
