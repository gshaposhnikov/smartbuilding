package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;

import java.util.HashMap;
import java.util.Map;

/**
 * Оповещение с текущими значениями наблюдаемых параметров устройства.
 *
 * @author Александр Горячкин
 *
 */
public class SetActiveDeviceMonitorableValues extends AbstractDriverAlert {

    /**
     * Идентификатор устройства (оперативной части).
     * Если нет, то должны быть заданы deviceLocal, controlDeviceId,
     * controlDeviceAddress, lineNo и address.
     */
    private String deviceId = null;

    /** Локальное устройство (подключенное к прибору, с которого получено событие), или нет (подключенное к соседнему прибору) */
    private boolean deviceLocal;

    /** Идентификатор прибора, с которого получено событие*/
    private String controlDeviceId;

    /** Адрес управляющего устройства */
    private int controlDeviceAddress;

    /** Номер линии */
    private int lineNo;

    /** Адрес на линии */
    private int address;

    /** Index of a region where device is placed */
    private String regionNo;

    private DeviceType deviceType;

    private String deviceNo;

    /** Набор идентификаторов активных состояний (согласно профилю устройства) */
    private Map<String, String> monitorableValues = new HashMap<>();

    public SetActiveDeviceMonitorableValues(String regionNo, DeviceType deviceType, String deviceNo) {
        this.regionNo = regionNo;
        this.deviceType = deviceType;
        this.deviceNo = deviceNo;
    }

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public SetActiveDeviceMonitorableValues() {
        super();
    }

    public SetActiveDeviceMonitorableValues(String deviceId, Map<String, String> monitorableValues) {
        super();
        this.deviceId = deviceId;
        this.monitorableValues = monitorableValues;
    }

    public SetActiveDeviceMonitorableValues(String controlDeviceId, boolean deviceLocal,
                                            int controlDeviceAddress, int lineNo, int address,
                                            Map<String, String> monitorableValues) {
        super();
        this.controlDeviceId = controlDeviceId;
        this.deviceLocal = deviceLocal;
        this.controlDeviceAddress = controlDeviceAddress;
        this.lineNo = lineNo;
        this.address = address;
        this.monitorableValues = monitorableValues;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isDeviceLocal() {
        return deviceLocal;
    }

    public void setDeviceLocal(boolean deviceLocal) {
        this.deviceLocal = deviceLocal;
    }

    public String getControlDeviceId() {
        return controlDeviceId;
    }

    public void setControlDeviceId(String controlDeviceId) {
        this.controlDeviceId = controlDeviceId;
    }

    public int getControlDeviceAddress() {
        return controlDeviceAddress;
    }

    public void setControlDeviceAddress(int controlDeviceAddress) {
        this.controlDeviceAddress = controlDeviceAddress;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public Map<String, String> getMonitorableValues() {
        return monitorableValues;
    }

    public void setMonitorableValues(Map<String, String> monitorableValues) {
        this.monitorableValues = monitorableValues;
    }

    public String getRegionNo() {
        return regionNo;
    }

    public void setRegionNo(String regionNo) {
        this.regionNo = regionNo;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }
}
