package ru.rubezh.firesec.nt.amqp.message.alert;

import java.util.List;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;

/**
 * Уведомление со статусом валидации проекта (должно отправляться драйвером
 * после получения нового проекта)
 * 
 * @author Антон Васильев
 *
 */
public class ActiveProjectValidateStatus extends AbstractDriverAlert {

    /** Идентификатор проекта */
    private String projectId;
    /** Общий результат валидации */
    private boolean validateSucceded;
    /** Сообщения валидации */
    private List<ActivationValidateMessage> validateMessages;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public boolean isValidateSucceded() {
        return validateSucceded;
    }

    public void setValidateSucceded(boolean validateSucceded) {
        this.validateSucceded = validateSucceded;
    }

    public List<ActivationValidateMessage> getValidateMessages() {
        return validateMessages;
    }

    public void setValidateMessages(List<ActivationValidateMessage> validateMessages) {
        this.validateMessages = validateMessages;
    }

    public void addValidateMessages(List<ActivationValidateMessage> validateMessages){
        this.validateMessages.addAll(validateMessages);
    }

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.PROJECT;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.DIAGNOSTIC;
    }

}
