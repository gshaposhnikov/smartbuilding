package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Оповещение о новом возникшем активном состоянии устройства.
 * 
 * @author Антон Васильев
 *
 */
public class AddActiveDeviceState extends AbstractDriverAlert {

    /** Идентификатор устройства (оперативной части) */
    private String activeDeviceId = "";
    
    /** Идентифкатор добавляемого активного состояния */
    private String state = "";

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public AddActiveDeviceState() {
        super();
    }
    
    public AddActiveDeviceState(String activeDeviceId, String activatedState) {
        super();
        this.activeDeviceId = activeDeviceId;
        this.state = activatedState;
    }

    public String getActiveDeviceId() {
        return activeDeviceId;
    }

    public void setActiveDeviceId(String activeDeviceId) {
        this.activeDeviceId = activeDeviceId;
    }

    public String getState() {
        return state;
    }

    public void setState(String activatedState) {
        this.state = activatedState;
    }

}
