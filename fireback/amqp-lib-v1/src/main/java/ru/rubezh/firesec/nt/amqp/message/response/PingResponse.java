package ru.rubezh.firesec.nt.amqp.message.response;

import java.util.ArrayList;
import java.util.List;

public class PingResponse extends AbstractDriverResponse {

    /** Текущее время драйвера */
    private long currentTimeMs;

    /** Времена последней активности конечных автоматов */
    private List<Long> lastSMActivityTimes = new ArrayList<>();

    /** Признак режима монопольной активности (один КА работает, другие ждут) */
    private boolean exclusiveActivityMode;

    /** Время последнего получения режима монопольной активности */
    private long lastExclusiveModeAcquireTimeMs;

    /** Используемая память, байт */
    private long memoryUsage;

    public PingResponse() {
    }

    public long getCurrentTimeMs() {
        return currentTimeMs;
    }

    public void setCurrentTimeMs(long currentTimeMs) {
        this.currentTimeMs = currentTimeMs;
    }

    public List<Long> getLastSMActivityTimes() {
        return lastSMActivityTimes;
    }

    public void setLastSMActivityTimes(List<Long> lastSMActivityTimes) {
        this.lastSMActivityTimes = lastSMActivityTimes;
    }

    public boolean isExclusiveActivityMode() {
        return exclusiveActivityMode;
    }

    public void setExclusiveActivityMode(boolean exclusiveActivityMode) {
        this.exclusiveActivityMode = exclusiveActivityMode;
    }

    public long getLastExclusiveModeAcquireTimeMs() {
        return lastExclusiveModeAcquireTimeMs;
    }

    public void setLastExclusiveModeAcquireTimeMs(long lastExclusiveModeAcquireTimeMs) {
        this.lastExclusiveModeAcquireTimeMs = lastExclusiveModeAcquireTimeMs;
    }

    public long getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(long memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("currentTimeMs: ").append(currentTimeMs).append(";\n");
        stringBuilder.append("lastSMActivityTimes:");
        for (Long lastSMActivityTime : lastSMActivityTimes) {
            stringBuilder.append(" ").append(lastSMActivityTime);
        }
        stringBuilder.append(";\n");
        stringBuilder.append("exclusiveActivityMode: ").append(exclusiveActivityMode).append(";\n");
        stringBuilder.append("lastExclusiveModeAcquireTimeMs: ").append(lastExclusiveModeAcquireTimeMs).append(";\n");
        stringBuilder.append("memoryUsage: ").append(memoryUsage).append(".");

        return stringBuilder.toString();
    }

}
