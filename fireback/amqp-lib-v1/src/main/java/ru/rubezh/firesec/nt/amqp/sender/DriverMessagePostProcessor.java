package ru.rubezh.firesec.nt.amqp.sender;

import java.util.UUID;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

/**
 * Постобработчик сообщений перед отправкой - устанавливает поле replyTo равным
 * заданной очереди для ответов и генерирует correlationId.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 */
public class DriverMessagePostProcessor implements MessagePostProcessor {

    private String responseQueueName;

    public DriverMessagePostProcessor(String responseQueueName) {
        this.responseQueueName = responseQueueName;
    }

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        message.getMessageProperties().setReplyTo(responseQueueName);
        message.getMessageProperties().setCorrelationIdString(UUID.randomUUID().toString());
        return message;
    }

}
