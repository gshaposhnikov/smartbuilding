package ru.rubezh.firesec.nt.amqp.message.response;

public class ErrorResponse extends AbstractDriverResponse {

    public ErrorResponse(ErrorType error, String errorString) {
        setError(error);
        setErrorString(errorString);
    }

}
