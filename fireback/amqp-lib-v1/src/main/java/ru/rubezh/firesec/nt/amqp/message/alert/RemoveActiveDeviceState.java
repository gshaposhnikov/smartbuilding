package ru.rubezh.firesec.nt.amqp.message.alert;

/**
 * Оповещении о пропадании активного состояния устройства.
 * 
 * @author Антон Васильев
 *
 */
public class RemoveActiveDeviceState extends AddActiveDeviceState {

    public RemoveActiveDeviceState() {
        super();
    }
    
    public RemoveActiveDeviceState(String activeDeviceId, String activatedState) {
        super(activeDeviceId, activatedState);
    }
    
}
