package ru.rubezh.firesec.nt.amqp.message.request;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;

public class SetActiveProjectRequest extends AbstractDriverRequest {
    /** Идентификатор проекта */
    private String projectId;
    /** Дерево устройство проекта */
    private List<ActiveDevice> devices = new ArrayList<>();
    /** Зоны проекта */
    private List<ActiveRegion> regions = new ArrayList<>();
    /** Сценарии проекта */
    private List<ActiveScenario> scenarios = new ArrayList<>();
    /** Виртуальные состояния проекта */
    private List<VirtualState> virtualStates = new ArrayList<>();
    /** Задачи проекта */
    private List<Issue> issues = new ArrayList<>();
    /** Набор сотрудников */
    private List<Employee> employees = new ArrayList<>();
    /** Набор ключей доступа */
    private List<AccessKey> accessKeys = new ArrayList<>();
    /** Набор графиков работ */
    private List<WorkSchedule> workSchedules = new ArrayList<>();

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<ActiveDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<ActiveDevice> devices) {
        this.devices = devices;
    }

    public List<ActiveRegion> getRegions() {
        return regions;
    }

    public void setRegions(List<ActiveRegion> regions) {
        this.regions = regions;
    }

    public List<ActiveScenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<ActiveScenario> scenarios) {
        this.scenarios = scenarios;
    }

    public List<VirtualState> getVirtualStates() {
        return virtualStates;
    }

    public void setVirtualStates(List<VirtualState> virtualStates) {
        this.virtualStates = virtualStates;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<AccessKey> getAccessKeys() {
        return accessKeys;
    }

    public void setAccessKeys(List<AccessKey> accessKeys) {
        this.accessKeys = accessKeys;
    }

    public List<WorkSchedule> getWorkSchedules() {
        return workSchedules;
    }

    public void setWorkSchedules(List<WorkSchedule> workSchedules) {
        this.workSchedules = workSchedules;
    }
}
