package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Уведомление об изменении статуса проекта (оперативного статуса, присылаемого
 * драйвером)
 * 
 * @author Антон Васильев
 *
 */
public class ActiveProjectStatusUpdated extends AbstractAlert {

    /**
     * Идентфикатор проекта.
     */
    private String projectId;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.PROJECT;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.DIAGNOSTIC;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
