package ru.rubezh.firesec.nt.amqp.sender;

import java.util.Formatter;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.alert.AbstractAlert;

/**
 * Класс для отправки оповещений.
 * @author Александр Горячкин
 */
public class AlertSender extends AbstractSender {

    @NotNull
    protected Logger logger;
    protected RabbitTemplate rabbitTemplate;
    protected String alertsExchangeName;

    public AlertSender(Logger logger, RabbitTemplate rabbitTemplate, String alertsExchangeName) {
        this.logger = logger;
        this.rabbitTemplate = rabbitTemplate;
        this.alertsExchangeName = alertsExchangeName;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public RabbitTemplate getRabbitTemplate() {
        return rabbitTemplate;
    }

    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public String getAlertsExchangeName() {
        return alertsExchangeName;
    }

    public void setAlertsExchangeName(String alertsExchangeName) {
        this.alertsExchangeName = alertsExchangeName;
    }

    /**
     * Генерация заголовка оповещения.
     *
     * @param message
     *            Базовый класс оповещения.
     */
    protected String getRoutingKey(AbstractAlert message) {
        StringBuilder routingKey = new StringBuilder();
        Formatter formatter = new Formatter(routingKey);
        formatter.format("%s.%s.%s",
                message.getRoutingLevel(),
                message.getRoutingObject(),
                message.getRoutingType());
        formatter.close();
        return routingKey.toString();
    }

    /**
     * Метод для отправки оповещений.
     *
     * @param message
     *            Базовый класс оповещения.
     * @param routingKey
     *            Заголовок оповещения.
     */
    protected void sendAlert(AbstractAlert message, String routingKey) {
        if (message != null && rabbitTemplate != null &&
                alertsExchangeName != null && routingKey != null) {
            rabbitTemplate.convertAndSend(alertsExchangeName, routingKey, message);
        }
    }

    /**
     * Метод для отправки базового оповещения.
     *
     * @param message
     *            Базовый класс оповещения.
     */
    public void send(AbstractAlert message) {
        logger.trace("Alert message send. Message type: {}.", message.getClass().getName());

        sendAlert(message, getRoutingKey(message));
    }

}
