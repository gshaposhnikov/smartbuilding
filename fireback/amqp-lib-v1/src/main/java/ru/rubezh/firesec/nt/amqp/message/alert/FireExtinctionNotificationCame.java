package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;
import ru.rubezh.firesec.nt.domain.v1.FireExtinctionNotification;

import java.util.List;

/**
 * Уведомление о получении оповещений о пожаротушении
 * 
 * @author Андрей Лисовой
 *
 */
public class FireExtinctionNotificationCame extends AbstractAlert {

    /**
     * Список оповещений о пожаротушении
     */
    private List<FireExtinctionNotification> notifications;

    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public List<FireExtinctionNotification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<FireExtinctionNotification> notifications) {
        this.notifications = notifications;
    }
}
