package ru.rubezh.firesec.nt.amqp.message.alert;

import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingLevel;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingObject;
import ru.rubezh.firesec.nt.amqp.message.alert.route.RoutingType;

/**
 * Уведомление об изменении оперативного состояния устройства
 * @author Антон Васильев
 *
 */
public class ActiveDeviceUpdated extends AbstractAlert {
    /** Идентификатор устройства в БД */
    private String activeDeviceId;
    
    @Override
    public RoutingLevel getRoutingLevel() {
        return RoutingLevel.INFO;
    }

    @Override
    public RoutingObject getRoutingObject() {
        return RoutingObject.DEVICE;
    }

    @Override
    public RoutingType getRoutingType() {
        return RoutingType.MONITORING;
    }

    public String getActiveDeviceId() {
        return activeDeviceId;
    }

    public void setActiveDeviceId(String activeDeviceId) {
        this.activeDeviceId = activeDeviceId;
    }

}
