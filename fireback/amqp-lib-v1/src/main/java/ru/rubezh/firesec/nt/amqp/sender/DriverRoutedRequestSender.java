package ru.rubezh.firesec.nt.amqp.sender;

import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import ru.rubezh.firesec.nt.amqp.message.request.AbstractDriverRequest;
import ru.rubezh.firesec.nt.amqp.message.response.AbstractDriverResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorResponse;
import ru.rubezh.firesec.nt.amqp.message.response.ErrorType;

public class DriverRoutedRequestSender extends RoutedRequestSender {

    private DriverRoutedMessagePostProcessor driverRoutedMessagePostProcessor;

    public DriverRoutedRequestSender(Logger logger, RabbitTemplate rabbitTemplate,
                                     DriverRoutedMessagePostProcessor driverMessagePostProcessor) {
        super(logger, rabbitTemplate);
        this.driverRoutedMessagePostProcessor = driverMessagePostProcessor;
    }

    /**
     * Метод для отправки базового запроса.
     *
     * @param message
     *            Базовый класс запроса.
     */
    public void send(String requestQueueName, AbstractDriverRequest message) {
        logger.trace("Driver request message to {} send. Message type: {}.", requestQueueName,
                message.getClass().getName());

        if (message != null && rabbitTemplate != null &&
                requestQueueName != null && driverRoutedMessagePostProcessor != null) {
            rabbitTemplate.convertAndSend(requestQueueName, message, driverRoutedMessagePostProcessor);
        }
    }

    /**
     * Метод для отправки базового запроса с синхронным ответом.
     *
     * @param message
     *            Базовый класс запроса.
     *
     */
    public AbstractDriverResponse sendAndReceive(String requestQueueName, AbstractDriverRequest message) {
        logger.trace("Request driver message to {} send and receive. Message type: {}", requestQueueName,
                message.getClass().getName());

        if (message != null && rabbitTemplate != null && requestQueueName != null
                && driverRoutedMessagePostProcessor != null) {
            try {
                return (AbstractDriverResponse) rabbitTemplate.convertSendAndReceive(
                        requestQueueName, message, driverRoutedMessagePostProcessor);
            } catch (ClassCastException e) {
                return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Response has bad type");
            }
        } else {
            return new ErrorResponse(ErrorType.INTERNAL_ERROR, "Sender not ready");
        }
    }

}
