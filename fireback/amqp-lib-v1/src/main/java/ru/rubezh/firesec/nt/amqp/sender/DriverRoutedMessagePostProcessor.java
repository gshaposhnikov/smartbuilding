package ru.rubezh.firesec.nt.amqp.sender;

import java.util.UUID;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

/**
 * Постобработчик сообщений перед отправкой - генерирует correlationId.
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 */
public class DriverRoutedMessagePostProcessor implements MessagePostProcessor {

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        message.getMessageProperties().setCorrelationIdString(UUID.randomUUID().toString());
        return message;
    }

}
