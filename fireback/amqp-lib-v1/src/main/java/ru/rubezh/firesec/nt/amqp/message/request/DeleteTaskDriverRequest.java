package ru.rubezh.firesec.nt.amqp.message.request;

/**
 * Запрос на удаление задачи.
 *
 * @author Александр Горячкин
 */
public class DeleteTaskDriverRequest extends AbstractRoutedDriverRequest {

    /** Идентификатор задачи
     *  Если не задан, то очистить всю очередь
     */
    private String issueId;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }
}
