package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.*;

@Service
public class ScenarioDictionariesViewService {

    @Autowired
    private Logger logger;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;

    @Autowired
    private ScenarioTriggerTypeViewRepository scenarioTriggerTypeViewRepository;

    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;

    @Autowired
    private ScenarioActionTypeViewRepository scenarioActionTypeViewRepository;

    @Autowired
    private MediaRepository mediaRepository;

    public static final String SCENARIO_SENTENCE_BEGIN = "если ";
    public static final String SCENARIO_SENTENCE_ENTITY_NAME_PREPOSITION = "на ";
    public static final String SCENARIO_SENTENCE_EXPRESSION_MULTI_BEGIN = ": (";
    public static final String SCENARIO_SENTENCE_EXPRESSION_MULTI_END = ")";
    public static final String SCENARIO_SENTENCE_REGION = "зона";
    public static final String SCENARIO_SENTENCE_VRITUAL = "вирт. состояние";
    public static final String SCENARIO_SENTENCE_DEVICE = "устройство";

    private enum ScenarioTypeView {
        FIRE_FIGHTING_VIEW(ScenarioType.FIRE_FIGHTING, "#FF0000"),
        ENGINEERING_VIEW(ScenarioType.ENGINEERING, "#0000FF"),
        ENS_VIEW(ScenarioType.ENS, "#008000"),
        UNASSIGNED_VIEW(ScenarioType.UNASSIGNED, "#AAAAAA");

        private ScenarioType scenarioType;
        private String color;

        private static final HashMap<ScenarioType, ScenarioTypeView> typesByValue = new HashMap<>();
        static {
            for (ScenarioTypeView type : ScenarioTypeView.values()) {
                typesByValue.put(type.scenarioType, type);
            }
        }

        ScenarioTypeView(ScenarioType scenarioType, String color) {
            this.scenarioType = scenarioType;
            this.color = color;
        }

        public String getColor() {
            return color;
        }
    }

    private enum ScenarioPurposeView {
        EXEC_BY_LOGIC_ENABLED_VIEW(ScenarioPurpose.EXEC_BY_LOGIC, true, "#008000"),
        EXEC_BY_LOGIC_DISABLED_VIEW(ScenarioPurpose.EXEC_BY_LOGIC, false, "#808080"),
        EXEC_BY_INVOKE_ENABLED_VIEW(ScenarioPurpose.EXEC_BY_INVOKE, true, "#0000FF"),
        EXEC_BY_INVOKE_DISABLED_VIEW(ScenarioPurpose.EXEC_BY_INVOKE, false, "#808080");

        private ScenarioPurpose scenarioPurpose;
        private boolean enabled;
        private String color;

        public static ScenarioPurposeView findByPuproseAndEnabled(ScenarioPurpose purpose, boolean enabled) {
            for (ScenarioPurposeView view : ScenarioPurposeView.values()) {
                if (view.scenarioPurpose == purpose && view.enabled == enabled)
                    return view;
            }
            return null;
        }

        ScenarioPurposeView(ScenarioPurpose scenarioPurpose, boolean enabled, String color) {
            this.scenarioPurpose = scenarioPurpose;
            this.enabled = enabled;
            this.color = color;
        }

        public String getColor() {
            return color;
        }
    }

    public enum ScenarioLogicTypeView {
        AND_VIEW(ScenarioLogicBlock.LogicType.AND, "все", "[И] Если выполнены все условия", "выполняются все условия"),
        OR_VIEW(ScenarioLogicBlock.LogicType.OR, "любое", "[ИЛИ] Если выполнено хотя бы одно условие", "выполняется любое из условий"),
        NOT_AND_VIEW(ScenarioLogicBlock.LogicType.AND_NOT, "ни одно", "[И (НЕ)] Если не выполнено ни одно из условий", "не выполняется ни одно из условий"),
        NOT_OR_VIEW(ScenarioLogicBlock.LogicType.OR_NOT, "не все", "[ИЛИ (НЕ)] Если не выполено хотя бы одно условие", "выполняются не все условия");

        private ScenarioLogicBlock.LogicType logicType;
        private String name;
        private String description;
        private String sentence;

        public static final HashMap<ScenarioLogicBlock.LogicType, ScenarioLogicTypeView> typesByValue = new HashMap<>();
        static {
            for (ScenarioLogicTypeView type : ScenarioLogicTypeView.values()) {
                typesByValue.put(type.logicType, type);
            }
        }

        ScenarioLogicTypeView(ScenarioLogicBlock.LogicType logicType,
                              String name, String description, String sentence) {
            this.logicType = logicType;
            this.name = name;
            this.description = description;
            this.sentence = sentence;
        }

        public ScenarioLogicBlock.LogicType getLogicType() {
            return logicType;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getSentence() {
            return sentence;
        }
    }

    private enum ScenarioTreeItemTypeView {
        ROOT_VIEW(TreeItemType.ROOT, "actionRoot"),
        TIME_VIEW(TreeItemType.TIME, "actionTime"),
        CONDITION_VIEW(TreeItemType.CONDITION, "actionCondition"),
        IN_VIEW(TreeItemType.IN, "actionIn"),
        OUT_VIEW(TreeItemType.OUT, "actionOut"),
        TRACE_VIEW(TreeItemType.TRACE, "actionTrace"),
        NO_TRACE_VIEW(TreeItemType.NO_TRACE, "actionNoTrace"),
        ACTION_VIEW(TreeItemType.ACTION, "actionAction"),
        COMPUTER_ACTION_VIEW(TreeItemType.COMPUTER_ACTION, "actionComputerAction"),
        SHOW_MESSAGE_VIEW(TreeItemType.SHOW_MESSAGE, "actionShowMessage"),
        COUNTDOWN_VIEW(TreeItemType.COUNTDOWN, "actionCountdown");

        private TreeItemType treeItemType;
        private String mediaId;

        private static final HashMap<TreeItemType, ScenarioTreeItemTypeView> valuesByType = new HashMap<>();
        static {
            for (ScenarioTreeItemTypeView type : ScenarioTreeItemTypeView.values()) {
                valuesByType.put(type.treeItemType, type);
            }
        }

        private static final HashMap<String, ScenarioTreeItemTypeView> valuesByMediaId = new HashMap<>();
        static {
            for (ScenarioTreeItemTypeView value : ScenarioTreeItemTypeView.values()) {
                valuesByMediaId.put(value.mediaId, value);
            }
        }

        ScenarioTreeItemTypeView(TreeItemType treeItemType, String mediaId) {
            this.treeItemType = treeItemType;
            this.mediaId = mediaId;
        }

        public String getMediaId() {
            return mediaId;
        }

        public TreeItemType getTreeItemType() {
            return treeItemType;
        }
    }

    public AggregatedScenarioDictionary getAggregatedScenarioDictionaries(StringBuilder errorMessage) {
        AggregatedScenarioDictionary result = new AggregatedScenarioDictionary();

        /** Типы назначения сценариев */
        result.getPurposes().add(new IdAndNameAndDescr(ScenarioPurpose.EXEC_BY_LOGIC.toString(),"Управляющий сценарий",
                "Сценарий, запускаемый по логическому условию или по команде пользователя"));
        result.getPurposes().add(new IdAndNameAndDescr(ScenarioPurpose.EXEC_BY_INVOKE.toString(),"Исполнительный сценарий",
                "Сценарий, запускаемый из других сценариев"));
        result.getPurposes().add(new IdAndNameAndDescr(ScenarioPurpose.EXEC_BY_TACTICS.toString(),
                "Специальный сценарий", "Сценарий, определяющий тактику запуска по состоянию охранных зон"));

        /** Типы сценариев */
        result.getTypes().add(new IdAndNameAndDescr(ScenarioType.FIRE_FIGHTING.toString(),"Пожаротушение","Пожаротушение"));
        result.getTypes().add(new IdAndNameAndDescr(ScenarioType.ENGINEERING.toString(),"Инженерный","Инженерные работы"));
        result.getTypes().add(new IdAndNameAndDescr(ScenarioType.ENS.toString(),"СОУЭ","СОУЭ"));
        result.getTypes().add(new IdAndNameAndDescr(ScenarioType.UNASSIGNED.toString(),"Без указания типа","Без указания типа"));

        /** Флаги у сценариев */
        result.getFlags().add(new IdAndNameAndDescr("manualStartStopAllowed","Ручное управление из ОЗ",
                "Разрешено ручное управление (запуск/остановка/блокировка/разблокировка) из ОЗ"));
        result.getFlags().add(new IdAndNameAndDescr("stopOnGlobalReset","Выключение при глобальном сбросе",
                "Выключение сценария при их глобальном сбросе"));

        /** Типы остановки сценариев */
        result.getStopTypes().add(new IdAndNameAndDescr(Scenario.StopType.MANUAL.toString(),"Без автовыключения",
                "Выключение сценария (возврат измененных им сущностей в исходное) производится только по команде пользователя"));
        result.getStopTypes().add(new IdAndNameAndDescr(Scenario.StopType.AUTO.toString(),"При пропадании логики включения",
                "Выключение сценария (возврат измененных им сущностей в исходное) производится автоматически после пропадания логики включения"));
        result.getStopTypes().add(new IdAndNameAndDescr(Scenario.StopType.BY_LOGIC.toString(),"При сработке логики отключения",
                "Выключение сценария (возврат измененных им сущностей в исходное) производится автоматически после сработки отдельной логики выключения"));

        /** Типы сущностей, к которым относятся триггеры включения/выключения сценариев */
        result.getTriggerEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTriggerType.TriggerEntityType.DEVICE.toString(),"Устройство",""));
        result.getTriggerEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTriggerType.TriggerEntityType.REGION.toString(),"Зона",""));
        result.getTriggerEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTriggerType.TriggerEntityType.CONTROL_DEVICE.toString(),"Прибор",""));
        result.getTriggerEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTriggerType.TriggerEntityType.VIRTUAL_STATE.toString(),"Виртуальное состояние",""));

        /** Типы логики включения/выключения сценариев */
        for (ScenarioLogicTypeView logicTypeView : ScenarioLogicTypeView.values()) {
            result.getLogicTypes().add(new IdAndNameAndDescr(
                    logicTypeView.getLogicType().toString(),
                    logicTypeView.getName(),
                    logicTypeView.getDescription()));
        }

        /** Типы блоков сценариев */
        result.getTimeLineBlockTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.BlockType.EXECUTIVE.toString(),"Исполнительный",""));
        result.getTimeLineBlockTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.BlockType.TRACING.toString(),"Слежения",""));
        result.getTimeLineBlockTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.BlockType.COMPUTER_ACTION.toString(),"Действие на компьютере:",""));

        /** Типы условий исполнительных блоков сценариев */
        result.getConditionTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.ConditionType.NONE.toString(),"Отсутствует",""));
        result.getConditionTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.ConditionType.DEVICE_STATE.toString(),"По состоянию устройства",""));
        result.getConditionTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.ConditionType.VIRTUAL_STATE.toString(),"По виртуальному состоянию",""));
    
        /** Типы проверки условий исполнительных блоков сценариев */
        result.getConditionCheckTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.ConditionCheckType.IS_ACTIVE.toString(),"Проверка на активность",""));
        result.getConditionCheckTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.ConditionCheckType.IS_INACTIVE.toString(),"Проверка на неактивность",""));

        /** Типы сущностей, которыми могут выполняться действия, перечисляемые в исполнительных блоках сценариев */
        result.getActionEntityTypes().add(new IdAndNameAndDescr(
                ScenarioActionType.ActionEntityType.DEVICE.toString(),"Устройство",""));
        result.getActionEntityTypes().add(new IdAndNameAndDescr(
                ScenarioActionType.ActionEntityType.REGION.toString(),"Зона",""));
        result.getActionEntityTypes().add(new IdAndNameAndDescr(
                ScenarioActionType.ActionEntityType.SCENARIO.toString(),"Сценарий",""));
        result.getActionEntityTypes().add(new IdAndNameAndDescr(
                ScenarioActionType.ActionEntityType.VIRTUAL_STATE.toString(),"Виртуальное состояние",""));

        /** Типы сущностей блоков сценария слежения */
        result.getTracingEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.TracingEntityType.SENSOR_DEVICE.toString(),"Устройство",""));
        result.getTracingEntityTypes().add(new IdAndNameAndDescr(
                ScenarioTimeLineBlock.TracingEntityType.VIRTUAL_STATE.toString(),"Виртуальное состояние",""));

        /** Действия над сценариями */
        result.getManageActions().add(
                new IdAndNameAndDescr(ScenarioManageAction.START.toString(),"Запустить","Запустить сценарий"));
        result.getManageActions().add(
                new IdAndNameAndDescr(ScenarioManageAction.STOP.toString(),"Остановить","Остановить сценарий"));
        result.getManageActions().add(
                new IdAndNameAndDescr(ScenarioManageAction.BLOCK.toString(),"Заблокировать","Заблокировать сценарий"));
        result.getManageActions().add(
                new IdAndNameAndDescr(ScenarioManageAction.UNBLOCK.toString(),"Разблокировать","Разблокировать сценарий"));

        return result;
    }

    public List<AggregatedScenarioTriggerTypeView> getAggregatedScenarioTriggerTypeViews(StringBuilder errorMessage) {
        List<ScenarioTriggerType> triggerTypes = scenarioTriggerTypeRepository.findAll();
        Map<String, ScenarioTriggerTypeView> triggerTypeViewsById = scenarioTriggerTypeViewRepository
                .findAll().stream()
                .collect(Collectors.toMap(ScenarioTriggerTypeView::getId, typeView -> typeView));

        return triggerTypes.stream()
                .map(triggerType -> new AggregatedScenarioTriggerTypeView(triggerType,
                        triggerTypeViewsById.get(triggerType.getId())))
                .collect(Collectors.toList());
    }

    public List<AggregatedScenarioActionTypeView> getAggregatedScenarioActionTypeViews(StringBuilder errorMessage) {
        /* TODO: упростить код, как в предыдущем методе */
        List<ScenarioActionType> scenarioActionTypes = scenarioActionTypeRepository.findAllByOrderByIdAsc();
        List<ScenarioActionTypeView> scenarioActionTypeViews = scenarioActionTypeViewRepository.findAllByOrderByIdAsc();

        List<AggregatedScenarioActionTypeView> results = new ArrayList<>();
        for (int index = 0; index < scenarioActionTypes.size(); index++) {
            ScenarioActionType scenarioActionType = scenarioActionTypes.get(index);
            ScenarioActionTypeView view = scenarioActionTypeViews.get(index);

            if (scenarioActionType != null && view != null &&
                    scenarioActionType.getId().equals(view.getId())) {
                AggregatedScenarioActionTypeView result = new AggregatedScenarioActionTypeView();
                result.setId(scenarioActionType.getId());
                result.setEntityType(scenarioActionType.getEntityType());
                result.setScenarioPurpose(scenarioActionType.getScenarioPurpose());
                result.setSubsystem(scenarioActionType.getSubsystem());
                result.setDeviceCategory(scenarioActionType.getDeviceCategory());
                result.setSeverity(scenarioActionType.getSeverity());
                result.setName(view.getName());
                result.setDescription(view.getDescription());
                results.add(result);
            } else {
                logger.warn("Тип действия сценария не найден");
            }
        }
        return results;
    }

    void fillScenarioColors(Scenario scenario, AggregatedScenarioView view) {
        ScenarioTypeView scenarioTypeView = ScenarioTypeView.typesByValue.get(scenario.getScenarioType());
        view.setColor(scenarioTypeView.getColor());

        ScenarioPurposeView scenarioPurposeView = ScenarioPurposeView.findByPuproseAndEnabled(
                scenario.getScenarioPurpose(),
                scenario.isEnabled() && (scenario.getScenarioPurpose() != ScenarioPurpose.EXEC_BY_LOGIC
                        || !scenario.getStartLogic().isEmpty()));
        if (scenarioPurposeView != null)
            view.setFontColor(scenarioPurposeView.getColor());
    }

    Media getTreeItemMedia(TreeItemType treeItemType) {
        ScenarioTreeItemTypeView view = ScenarioTreeItemTypeView.valuesByType.get(treeItemType);
        return mediaRepository.findOne(view.getMediaId());
    }

    Map<TreeItemType, Media> getTreeItemMedias() {
        List<Media> medias = mediaRepository.findByIdIn(ScenarioTreeItemTypeView.valuesByMediaId.keySet());
        return medias.stream().collect(Collectors.toMap(
                media -> ScenarioTreeItemTypeView.valuesByMediaId.get(media.getId()).getTreeItemType(),
                media -> media));
    }

}
