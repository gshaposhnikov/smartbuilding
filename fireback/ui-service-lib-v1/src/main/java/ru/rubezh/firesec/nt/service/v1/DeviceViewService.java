package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceView.DevicePlanLayout;
import ru.rubezh.firesec.nt.domain.v1.representation.DeviceCreateView;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemActiveDeviceView;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DeviceViewService {
    public class CreatedAndUpdatedEntities {
        public String projectId;
        public List<TreeItemDeviceView> createdDevices = new ArrayList<>();
        public List<TreeItemDeviceView> updatedDevices = new ArrayList<>();
        public List<AggregatedScenarioView> updatedScenarios = new ArrayList<>();

        public CreatedAndUpdatedEntities() {
        }

        public CreatedAndUpdatedEntities(String projectId) {
            this.projectId = projectId;
        }
    }

    public class RemovedAndUpdatedEntities {
        public String projectId;
        public List<String> removedDeviceIds = new ArrayList<>();
        public List<TreeItemDeviceView> updatedDevices = new ArrayList<>();
        public List<IndicatorPanel> updatedIndicatorPanels = new ArrayList<>();

        public RemovedAndUpdatedEntities() {
        }

        public RemovedAndUpdatedEntities(String projectId) {
            this.projectId = projectId;
        }

    }

    public CreatedAndUpdatedEntities add(DeviceCreateView deviceInfo, String projectId, Language language,
            StringBuilder errorMessage);

    public RemovedAndUpdatedEntities remove(String projectId, String deviceId, Language language,
            StringBuilder errorMessage);

    public List<TreeItemDeviceView> getTreeItemDeviceViewsByDeviceViews(String projectId, Language language,
            List<DeviceView> deviceViews);

    public List<TreeItemDeviceView> getTreeItemDeviceViewsByProjectId(String projectId, Language language);

    public TreeItemDeviceView getDeviceTreeItemByProjectIdAndDeviceId(String projectId, String deviceId, Language language);

    public TreeItemActiveDeviceView getActiveDeviceTreeItemByProjectIdAndDeviceId(String projectId,
            String deviceId, Language language);

    public List<TreeItemActiveDeviceView> getActiveDeviceTreeItemByProjectIdAndRegionId(String projectId,
            String regionId, Language language);

    /**
     * Получить (постранично) агрегированные отображения активных устройств
     * проекта по идентификатору проекта
     *
     * @param projectId
     *            идентификатор проекта
     * @param pageSize
     *            размер страницы
     * @param pageNo
     *            номер страницы (с нуля)
     * @return список отображений устройств
     */
    List<TreeItemActiveDeviceView> getActiveDeviceTreeItemsByProjectId(String projectId, long pageSize, int pageNo);

    /**
     * Постранично получить агрегированные отображения активных устройств,
     * время обновления состояния которых позже заданного
     *
     * @param lastObservedTime последнее время проверки
     * @param pageSize размер страницы
     * @param pageNo номер страницы
     * @return список агрегированных представлений активных устройств
     */
    List<TreeItemActiveDeviceView> getActiveDeviceTreeItemsByTimeAfter(Date lastObservedTime,
                                                                       long pageSize, int pageNo);

    /**
     * Безопасно (с ограниченным кол-вом занимаемой памяти) записать
     * агрегированные отображения активных устройств в поток вывода
     * 
     * @param projectId
     *            идентификатор проекта
     * @param outputStream
     *            поток вывода для записи
     * @param objectMapper
     *            преобразователь объектов в JSON
     */
    public void safelyWriteActiveDeviceTreeItemByProjectId(String projectId, OutputStream outputStream,
                                                           ObjectMapper objectMapper) throws IOException;

    /**
     * Изменение профиля устройства.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param deviceProfileId
     *            идентификатор профиля устройства
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникает
     * @return Пару: созданные и измененные устройства в виде элементов дерева
     *         устройств - или null в случае ошибки
     */
    public CreatedAndUpdatedEntities updateDeviceProfile(String projectId, String deviceId,
            String deviceProfileId, Language language, StringBuilder errorMessage);

    /**
     * Изменить адрес устройства.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param shortAddressPath
     *            короткий адресный путь (номер линии и сам адрес или только
     *            адрес для корневого устройства)
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникает
     * @return Пару: созданные и измененные устройства в виде элементов дерева
     *         устройств - или null в случае ошибки
     */
    public CreatedAndUpdatedEntities updateDeviceAddress(String projectId, String deviceId,
            String shortAddressPath, Language language, StringBuilder errorMessage);

    /**
     * Изменить расположение устройства на планах помещений.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param planLayouts
     *            список мест расположения устройства на планах помещений
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникает
     * @return отображение устройства
     */
    public TreeItemDeviceView updateDevicePlanLayouts(String projectId, String deviceId,
            List<DevicePlanLayout> planLayouts, Language language, StringBuilder errorMessage);

    /**
     * Изменить настраиваемые свойства устройства.
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param propertyValues
     *            набор изменяемых свойств
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникнет
     * @return отображение устройства
     */
    public TreeItemDeviceView updateDeviceProperties(String projectId, String deviceId,
            Map<String, String> propertyValues, Language language, StringBuilder errorMessage);

    /**
     * Включить или выключить устройство
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param disabled
     *            флаг "устройство отключено"
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникнет
     * @return отображение устройства
     */
    public TreeItemDeviceView updateDeviceDisabled(String projectId, String deviceId,
            boolean disabled, Language language, StringBuilder errorMessage);

    /**
     * Обновить ссылки виртуального контейнера
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceId
     *            идентификатор устройства
     * @param linksUpdate
     *            обновлённый список подключенных устройств (ссылок)
     * @param language
     *            язык
     * @param errorMessage
     *            сообщение с ошибкой, если возникнет
     * @return отображения изменённых устройств
     */
    public List<TreeItemDeviceView> updateDeviceLinks(String projectId, String deviceId,
            List<String> linksUpdate, Language language, StringBuilder errorMessage);

    public boolean checkAndAdditionDeviceViewToImportProject (String projectId, List<DeviceView> DeviceViews,
            Map<String, String> oldAndNewPlansId, Map<String, String> oldAndNewDevicesId, StringBuilder errorMessage);


    /**
     * Сгенерировать отображения устройств на основе проектных сущностей
     * 
     * @param devices
     *            устройства (проектные сущности)
     * @param regions
     *            зоны (проектные сущности)
     * @param language
     *            язык
     * @return отображения элементов дерева устройств
     */
    public List<TreeItemDeviceView> generateTreeItemDeviceViews(List<Device> devices, List<Region> regions,
            Language language);

    /**
     * Добавить устройства из сохранённой конфигурации
     * 
     * @param projectId
     *            идентификатор проекта
     * @param devices
     *            устройства (проектные сущности)
     * @param regions
     *            добавленные зоны (проектные сущности)
     * @param oldToNewIds
     *            карта соответствия новых идентификаторов устройств старым (заполняется в методе)
     * @param regionIds
     *            карта соответствия новых идентификаторов зон старым
     * @param errorMessage
     *            сообщение об ошибке (заполняется в случае ошибки)
     * @param language
     *            язык
     * @return добавленные и изменённые устройства (отображения элементов дерева) или null в случае ошибки
     */
    public EntitiesContainer<TreeItemDeviceView> createByRecovery(String projectId, List<Device> devices,
            List<Region> regions, Map<String, String> oldToNewIds, Map<String, String> regionIds,
            StringBuilder errorMessage, Language language);

    /**
     * Откатить восстановленные из сохранённой конфигурации устройства
     * 
     * @param projectId
     *            идентификатор проекта
     * @param devicesUpdate
     *            добавленные и изменённые устройства
     */
    public void remove(String projectId, EntitiesContainer<TreeItemDeviceView> devicesUpdate);
    
    public void calculateAddressPaths(DeviceView deviceView);
    /**
     * Изменяет библиотеку шейпов устройства
     *
     * @param projectId идентификатор проекта
     * @param deviceId идентификатор устройства
     * @param shapeLibraryId идентификатор библиотеки шейпов
     * @param errorMessage сообщение возможной ошибки
     * @return отображение устройства
     */
    TreeItemDeviceView updateDeviceShapeLibrary(String projectId, String deviceId, String shapeLibraryId,
                                                Language language, StringBuilder errorMessage);

    /**
     * Обновить подсистему фильтрации для устройства
     *
     * @param projectId id проекта
     * @param deviceId id устройства
     * @param customSubsystem подсистема
     * @param language язык
     * @param errorMessage сообщение возможной ошибки
     * @return созданные и обновленные сущности
     */
    CreatedAndUpdatedEntities updateCustomSubsystem(String projectId, String deviceId, Subsystem customSubsystem,
                                                    Language language, StringBuilder errorMessage);

}
