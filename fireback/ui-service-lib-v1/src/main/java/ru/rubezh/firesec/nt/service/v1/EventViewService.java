package ru.rubezh.firesec.nt.service.v1;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;

import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedEventView;
import ru.rubezh.firesec.nt.domain.v1.representation.QAggregatedEventView;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@Service
public class EventViewService{

    @Autowired
    private Logger logger;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private EventTypeRepository eventTypeRepository;

    @Autowired
    private EventTypeViewRepository eventTypeViewRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private StateViewRepository stateViewRepository;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DslAggregatedEventViewRepository dslAggregatedEventViewRepository;

    // События, для которых возможна подстановка сообщений в журнале
    private enum EventsForMessageReplace {
        r2op3ExecDevicePowerOnEvent,
        r2op3ExecDevicePowerOffEvent,
        r2op3ResetFirePerformedEvent,
        r2op3AMTNormalEvent,
        r2op3AMTTriggered1Event,
        r2op3AMTTriggered2Event,
        r2op3AMTTriggeredBothEvent,
    }

    public void addEventTypeViews(List<EventTypeView> eventTypeViews) {
        BulkOperations eventTypeViewBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventTypeView.class);
        /* Сначала удаляем старые версии отображений типов событий */
        for (EventTypeView eventTypeView: eventTypeViews)
            eventTypeViewBulk.remove(new Query(Criteria.where("_id").is(eventTypeView.getId())));
        eventTypeViewBulk.execute();
        /* Теперь сохраняем новые версии отображений типов событий */
        eventTypeViewBulk.insert(eventTypeViews);
        eventTypeViewBulk.execute();
    }

    public void removeEventTypeViews(List<EventTypeView> eventTypeViews) {
        BulkOperations eventTypeViewBulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, EventTypeView.class);
        for (EventTypeView eventTypeView : eventTypeViews)
            eventTypeViewBulk.remove(Query.query(Criteria.where("_id").is(eventTypeView.getId())));
        eventTypeViewBulk.execute();
    }

    private AggregatedEventView getAggregatedEventView(
            Event event, Map<String, EventType> eventTypeMap, Map<String, EventTypeView> eventTypeViewMap,
            Map<String, DeviceView> deviceViewMap, Map<String, Region> regionMap,
            Map<String, VirtualState> virtualStateMap, Map<String, Scenario> scenarioMap,
            Map<String, Indicator> indicatorMap, Map<String, Employee> employeeMap) {
        EventType eventType = eventTypeMap.get(event.getEventTypeId());
        EventTypeView eventTypeView = eventTypeViewMap.get(event.getEventTypeId());
        StateCategoryView stateCategoryView = stateViewService
                .getStateCategoryViewById(eventType.getCategory().getId());

        AggregatedEventView eventView = new AggregatedEventView(event, eventTypeView, stateCategoryView);
        boolean availableSubsystemsAreFilled = false;
        Set<String> eventViewTags = eventView.getFilterTags().getTags();
        Subsystem eventViewSubsystem = eventView.getSubsystem();
        if (eventViewSubsystem != null && eventViewSubsystem != Subsystem.GENERAL){
            eventViewTags.add(eventViewSubsystem.toString());
            availableSubsystemsAreFilled = true;
        }
        if (eventType.isHavingControlDeviceInfo()) {
            DeviceView deviceView = deviceViewMap.get(event.getControlDeviceId());
            if (deviceView != null) {
                eventView.setControlDeviceEventInfo(new AggregatedEventView.ControlDeviceEventInfo(event, deviceView));
                if (eventType.isHavingControlDeviceStates() != null && eventType.isHavingControlDeviceStates()) {
                    List<String> stateIds = event.getControlDeviceStateIds();
                    if (stateIds != null && !stateIds.isEmpty()) {
                        List<StateView> stateViews = stateViewRepository.findByIdIn(stateIds);
                        List<String> stateNames =
                                stateViews.stream().map(StateView::getName).collect(Collectors.toList());
                        eventView.getControlDeviceEventInfo().setStates(stateNames);
                    }
                }
            }
            else {
                logger.warn("Control device not found (id: {})", event.getControlDeviceId());
            }
        }
        if (eventType.isHavingDeviceInfo() != null && eventType.isHavingDeviceInfo()) {
            DeviceView deviceView = deviceViewMap.get(event.getDeviceId());
            if (deviceView != null) {
                List<String> stateNames = new ArrayList<>();
                List<StateView> stateViews = stateViewRepository.findByIdIn(event.getActiveDeviceStateIds());
                for (StateView stateView : stateViews) {
                    stateNames.add(stateView.getName());
                }

                eventView.setDeviceEventInfo(new AggregatedEventView.DeviceEventInfo(deviceView, stateNames));

                replaceTextIfNeeded(eventView, deviceView.getDevice());

                if (!availableSubsystemsAreFilled) {
                    FilterTags deviceFilterTags = deviceViewMap
                            .get(event.getDeviceId()).getDevice().getFilterTags();
                    if (deviceFilterTags != null) {
                        eventViewTags.addAll(deviceFilterTags.chooseSubsystemTags());
                    }
                }
            }
            else {
                logger.warn("Device not found (id: {})", event.getDeviceId());
            }
        }
        if (eventType.isHavingRegionInfo() != null && eventType.isHavingRegionInfo()) {
            Region region = regionMap.get(event.getRegionId());
            if (region != null) {
                List<String> stateNames = new ArrayList<>();
                List<StateView> stateViews = stateViewRepository.findByIdIn(event.getActiveDeviceStateIds());
                for (StateView stateView : stateViews) {
                    stateNames.add(stateView.getName());
                }

                eventView.setRegionEventInfo(new AggregatedEventView.RegionEventInfo(region, stateNames));

                if (!availableSubsystemsAreFilled) {
                    eventViewTags.add(regionMap.get(event.getRegionId()).getSubsystem().toString());
                }
            }
            else {
                logger.warn("Region not found (id: {})", event.getRegionId());
            }
        }
        if (eventType.isHavingUserInfo()) {
            eventView.setUserEventInfo(new AggregatedEventView.UserEventInfo(event));
        }
        if (eventType.isHavingVirtualStateInfo() != null && eventType.isHavingVirtualStateInfo()) {
            VirtualState virtualState = virtualStateMap.get(event.getVirtualStateId());
            if (virtualState != null) {
                eventView.setVirtualStateEventInfo(new AggregatedEventView.VirtualStateEventInfo(event, virtualState));
                if (eventType.isVirtualStateOn() != null) {
                    StateCategoryView subStateCategoryView = stateViewService
                            .getStateCategoryViewById(virtualState.getStateCategoryId());
                    if (eventType.isVirtualStateOn() && !virtualState.getMessageOn().isEmpty()) {
                        eventView.setName(virtualState.getMessageOn());
                    } else if (!eventType.isVirtualStateOn() && !virtualState.getMessageOff().isEmpty()) {
                        eventView.setName(virtualState.getMessageOff());
                    }
                    if (subStateCategoryView != null) {
                        eventView.setStateCategoryId(subStateCategoryView.getId());
                        eventView.setColor(subStateCategoryView.getColor());
                        eventView.setFontColor(subStateCategoryView.getFontColor());
                        eventView.setIconName(subStateCategoryView.getIconName());
                    }
                }

                if (!availableSubsystemsAreFilled) {
                    FilterTags virtualStateFilterTags = virtualStateMap
                            .get(event.getVirtualStateId()).getFilterTags();
                    if (virtualStateFilterTags != null) {
                        eventViewTags.addAll(virtualStateFilterTags.chooseSubsystemTags());
                    }
                }
            }
            else {
                logger.warn("Virtual state not found (id: {})", event.getVirtualStateId());
            }
        }
        if (eventType.isHavingScenarioInfo() != null && eventType.isHavingScenarioInfo()) {
            Scenario scenario = scenarioMap.get(event.getScenarioId());
            List<String> stateNames = new ArrayList<>();
            List<StateView> stateViews = stateViewRepository.findByIdIn(event.getActiveDeviceStateIds());
            for (StateView stateView : stateViews) {
                stateNames.add(stateView.getName());
            }
            if (scenario != null) {
                eventView.setScenarioEventInfo(new AggregatedEventView.ScenarioEventInfo(scenario, stateNames));

                if (!availableSubsystemsAreFilled) {
                    FilterTags scenarioFilterTags = scenarioMap
                            .get(event.getScenarioId()).getFilterTags();
                    if (scenarioFilterTags != null) {
                        eventViewTags.addAll(scenarioFilterTags.chooseSubsystemTags());
                    }
                }
            }
            else {
                logger.warn("Scenario not found (id: {})", event.getScenarioId());
            }
        }
        if (eventType.isHavingLineInfo() != null && eventType.isHavingLineInfo()) {
            eventView.setLineEventInfo(new AggregatedEventView.LineEventInfo(event));
        }
        if (eventType.isHavingVirtualIndicatorInfo() != null && eventType.isHavingVirtualIndicatorInfo()) {
            Indicator indicator = indicatorMap.get(event.getIndicatorPanelId()
                    + event.getIndicatorRowNo() + event.getIndicatorColNo());
            if (indicator != null){
                eventView.setIndicatorEventInfo(new AggregatedEventView.IndicatorEventInfo(event, indicator));

                if (!availableSubsystemsAreFilled) {
                    FilterTags indicatorFilterTags = indicatorMap
                            .get(event.getIndicatorPanelId() + event.getIndicatorRowNo() + event.getIndicatorColNo())
                            .getFilterTags();
                    if (indicatorFilterTags != null) {
                        eventViewTags.addAll(indicatorFilterTags.chooseSubsystemTags());
                    }
                }
            } else {
                logger.warn("Virtual indicator not found (panelId: {}, row: {}, col: {})",
                        event.getIndicatorPanelId(), event.getIndicatorRowNo(), event.getIndicatorColNo());
            }
        }
        if (eventType.isHavingEmployeeInfo() != null && eventType.isHavingEmployeeInfo()) {
            Employee employee = employeeMap.get(event.getEmployeeId());
            if (employee != null) {
                eventView.setEmployeeEventInfo(new AggregatedEventView.EmployeeEventInfo(employee));
            }
            else{
                logger.warn("Employee not found (id: {})", event.getEmployeeId());
            }
        }

        if (eventViewTags.isEmpty()){
            eventViewTags.add(Subsystem.GENERAL.toString());
        }
        return eventView;
    }

    private String getTextProperty(Device device, String propertyName) {
        String text = device.getDevicePropertyValues().get(propertyName);
        if ((text == null) || text.isEmpty())
            return null;
        else
            return text;
    }

    private void replaceTextIfNeeded(AggregatedEventView eventView, Device device) {
        String textPropertyName = null;
        try {
            switch (EventsForMessageReplace.valueOf(eventView.getEventTypeId())) {
            case r2op3ExecDevicePowerOnEvent:
                textPropertyName = DeviceProperties.EnabledMessage.toString();
                break;
            case r2op3ExecDevicePowerOffEvent:
                textPropertyName = DeviceProperties.DisabledMessage.toString();
                break;
            case r2op3AMTNormalEvent:
            case r2op3ResetFirePerformedEvent:
                textPropertyName = DeviceProperties.NoOneTriggeredMessage.toString();
                break;
            case r2op3AMTTriggered1Event:
                textPropertyName = DeviceProperties.FirstTriggeredMessage.toString();
                break;
            case r2op3AMTTriggered2Event:
                textPropertyName = DeviceProperties.SecondTriggeredMessage.toString();
                break;
            case r2op3AMTTriggeredBothEvent:
                textPropertyName = DeviceProperties.BothTriggeredMessage.toString();
                break;
            }
        } catch (IllegalArgumentException ex) {
        }
        if (textPropertyName != null) {
            String alternateText = getTextProperty(device, textPropertyName);
            if (alternateText != null)
                eventView.setName(alternateText);
        }
    }

    public List<AggregatedEventView> commitAggregatedEventViews(List<Event> events) {
        /* выберем все необходимые типы событий и их отображения */
        List<String> typeIds = events.stream().map(Event::getEventTypeId).collect(Collectors.toList());
        Map<String, EventType> eventTypeMap = eventTypeRepository.findAllByIdIn(typeIds).
                stream().collect(Collectors.toMap(EventType::getId, eventType -> eventType));
        Map<String, EventTypeView> eventTypeViewMap = eventTypeViewRepository.findAllByIdIn(typeIds).
                stream().collect(Collectors.toMap(EventTypeView::getId, eventTypeView -> eventTypeView));

        /* выберем все необходимые отображения устройств */
        List<String> deviceIds = events.stream().map(Event::getControlDeviceId).
                filter(id -> !id.isEmpty()).collect(Collectors.toList());
        deviceIds.addAll(events.stream().map(Event::getDeviceId).
                filter(id -> !id.isEmpty()).collect(Collectors.toList()));
        Map<String, DeviceView> deviceViewMap = deviceViewRepository.findAllByDeviceIdIn(deviceIds).
                stream().collect(Collectors.toMap(deviceView -> deviceView.getDevice().getId(), deviceView -> deviceView));

        /* выберем все необходимые зоны */
        List<String> regionIds = events.stream().map(Event::getRegionId).filter(Objects::nonNull).collect(Collectors.toList());
        Map<String, Region> regionMap = regionRepository.findAllByIdIn(regionIds).
                stream().collect(Collectors.toMap(Region::getId, region -> region));

        /* выберем все необходимые вирт. состояния */
        List<String> virtualStateIds = events.stream().map(Event::getVirtualStateId).
                filter(Objects::nonNull).collect(Collectors.toList());
        Map<String, VirtualState> virtualStateMap = virtualStateRepository.findAllByIdIn(virtualStateIds).
                stream().collect(Collectors.toMap(VirtualState::getId, virtualState -> virtualState));

        /* выберем все необходимые сценарии */
        List<String> scenarioIds = events.stream().map(Event::getScenarioId).
                filter(Objects::nonNull).collect(Collectors.toList());
        Map<String, Scenario> scenarioMap = scenarioRepository.findAllByIdIn(scenarioIds).
                stream().collect(Collectors.toMap(Scenario::getId, scenario -> scenario));

        /* выберем все необходимые виртуальные индикаторы */
        Map<String, Indicator> indicatorMap = new HashMap<>();
        for (Event event: events) {
            if (!event.getIndicatorPanelId().isEmpty()){
                IndicatorPanel indicatorPanel = indicatorPanelRepository.findOne(event.getIndicatorPanelId());
                if (indicatorPanel != null && !indicatorPanel.getIndicators().isEmpty()
                        && indicatorPanel.getRowCount() > event.getIndicatorRowNo()
                        && indicatorPanel.getColCount() > event.getIndicatorColNo()) {
                    Indicator indicator = indicatorPanel.getIndicators().get(event.getIndicatorRowNo()).get(event.getIndicatorColNo());
                    indicatorMap.put(event.getIndicatorPanelId() + event.getIndicatorRowNo() + event.getIndicatorColNo(), indicator);
                }
            }
        }

        /* выберем всех необходимых сотрудников */
        List<String> employeeIds = events.stream().map(Event::getEmployeeId).
                filter(employeeId -> !employeeId.isEmpty()).collect(Collectors.toList());
        Map<String, Employee> employeeMap = employeeRepository.findAllByIdInAndRemovedIsFalse(employeeIds).
                stream().collect(Collectors.toMap(Employee::getId, employee -> employee));

        List<AggregatedEventView> savedEventViews = new ArrayList<>();
        for (Event event : events) {
            try {
                AggregatedEventView aggregatedEventView = getAggregatedEventView(event, eventTypeMap, eventTypeViewMap,
                        deviceViewMap, regionMap, virtualStateMap, scenarioMap, indicatorMap, employeeMap);
                if (aggregatedEventView != null) {
                    savedEventViews.add(aggregatedEventView);
                }
            } catch (Exception ex) {
                logger.error("Failed to commit aggregated event views: ", ex);
            }
        }

        return savedEventViews.size() > 0 ? saveEvents(savedEventViews) : null;
    }

    private List<AggregatedEventView> saveEvents(List<AggregatedEventView> events) {
        BulkOperations bulkOperations = mongoTemplate.bulkOps(BulkMode.UNORDERED, AggregatedEventView.class);
        for (AggregatedEventView event: events)
            bulkOperations.insert(event);
        bulkOperations.execute();
        /* TODO: проверять на ошибки записи в БД */
        return events;
    }

    private BooleanBuilder getPredicate(String idAfter, String idBefore, String subsystems, Date occurredAfter,
            Date receivedAfter, String name, String eventTypeIds, String controlDeviceIds, String deviceIds,
            String regionIds, String userIds, String virtualStateIds, String scenarioIds, Integer lineNo,
            String logTypeIds, String stateCategoryIds, String indicatorsInfo, String employeeIds, Date occurredBefore,
            Date receivedBefore, String tags) {
        BooleanBuilder where = new BooleanBuilder();
        
        if (idAfter != null) {
            /* Для ObjectId в сгенерированном классе нет операций gt/lt, поэтому строим предикат вручную */
            where = where.and(Expressions.predicate(Ops.GT, new PathBuilder<>(ObjectId.class, "_id"),
                    ConstantImpl.create(new ObjectId(idAfter))));
        }
        if (idBefore != null) {
            /* Для ObjectId в сгенерированном классе нет операций gt/lt, поэтому строим предикат вручную */
            where = where.and(Expressions.predicate(Ops.LT, new PathBuilder<>(ObjectId.class, "_id"),
                    ConstantImpl.create(new ObjectId(idBefore))));
        }
        if (subsystems != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.subsystem
                    .in(new HashSet<>(Arrays.stream(subsystems.split(","))
                            .map(Subsystem::valueOf).collect(Collectors.toList())
                    ))
            );
        }
        if (occurredAfter != null) {
            occurredAfter.setTime(occurredAfter.getTime() - 1000);
            where = where.and(QAggregatedEventView.aggregatedEventView.occurred.after(occurredAfter));
        }
        if (receivedAfter != null) {
            receivedAfter.setTime(receivedAfter.getTime() - 1000);
            where = where.and(QAggregatedEventView.aggregatedEventView.received.after(receivedAfter));
        }
        if (name != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.name.containsIgnoreCase(name));
        }
        if (eventTypeIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.eventTypeId
                    .in(new HashSet<>(Arrays.asList(eventTypeIds.split(",")))));
        }
        if (controlDeviceIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.controlDeviceEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(controlDeviceIds.split(",")))));
        }
        if (deviceIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.deviceEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(deviceIds.split(",")))));
        }
        if (regionIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.regionEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(regionIds.split(",")))));
        }
        if (userIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.userEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(userIds.split(",")))));
        }
        if (virtualStateIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.virtualStateEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(virtualStateIds.split(",")))));
        }
        if (scenarioIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.scenarioEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(scenarioIds.split(",")))));
        }
        if (lineNo != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.lineEventInfo.lineNo.eq(lineNo));
        }
        if (logTypeIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.logTypeId
                    .in(new HashSet<>(Arrays.asList(logTypeIds.split(",")))));
        }
        if (stateCategoryIds != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.stateCategoryId
                    .in(new HashSet<>(Arrays.asList(stateCategoryIds.split(",")))));
        }
        if (indicatorsInfo != null) {
            Set<String> indicatorsInfoSet = new HashSet<>(Arrays.asList(indicatorsInfo.split(";")));
            for (String indicatorInfo: indicatorsInfoSet) {
                List<String> indicatorParameters = new ArrayList<>(Arrays.asList(indicatorInfo.split(",")));
                if (indicatorParameters.size() == 3 && indicatorParameters.get(0) != null
                        && indicatorParameters.get(1) != null && indicatorParameters.get(1).matches("(0|[1-9]\\d*)")
                        && indicatorParameters.get(2) != null && indicatorParameters.get(2).matches("(0|[1-9]\\d*)")) {
                    where = where.and(QAggregatedEventView.aggregatedEventView.indicatorEventInfo
                                .indicatorPanelId.eq(indicatorParameters.get(0)))
                            .and(QAggregatedEventView.aggregatedEventView.indicatorEventInfo
                                    .rowNo.eq(Integer.parseInt(indicatorParameters.get(1))))
                            .and(QAggregatedEventView.aggregatedEventView.indicatorEventInfo
                                    .colNo.eq(Integer.parseInt(indicatorParameters.get(2))));
                }
            }
        }
        if (employeeIds != null){
            where = where.and(QAggregatedEventView.aggregatedEventView.employeeEventInfo.idRef
                    .in(new HashSet<>(Arrays.asList(employeeIds.split(",")))));
        }
        if (occurredBefore != null) {
            occurredBefore.setTime(occurredBefore.getTime() + 1000);
            where = where.and(QAggregatedEventView.aggregatedEventView.occurred.before(occurredBefore));
        }
        if (receivedBefore != null) {
            receivedBefore.setTime(receivedBefore.getTime() + 1000);
            where = where.and(QAggregatedEventView.aggregatedEventView.received.before(receivedBefore));
        }
        if (tags != null) {
            where = where.and(QAggregatedEventView.aggregatedEventView.filterTags.tags.any()
                    .in(new HashSet<>(Arrays.asList(tags.split(",")))));
        }
        return where;
    }

    public Page<AggregatedEventView> getAggregatedEventViews(String idAfter, String idBefore, String subsystems,
            Date occurredAfter, Date receivedAfter, String name, String eventTypeIds, String controlDeviceIds,
            String deviceIds, String regionIds, String userIds, String virtualStateIds, String scenarioIds,
            Integer lineNo, String logTypeIds, String stateCategoryIds, String indicatorsInfo, String employeeIds,
            Date occurredBefore, Date receivedBefore, String tags, Pageable pageable) {

        BooleanBuilder where = getPredicate(idAfter, idBefore, subsystems, occurredAfter, receivedAfter, name,
                eventTypeIds, controlDeviceIds, deviceIds, regionIds, userIds, virtualStateIds, scenarioIds, lineNo,
                logTypeIds, stateCategoryIds, indicatorsInfo, employeeIds, occurredBefore, receivedBefore, tags);

        return dslAggregatedEventViewRepository.findAll(where, pageable);
    }

    public long getAggregatedEventViewsCount(String idAfter, String idBefore, String subsystems, Date occurredAfter,
            Date receivedAfter, String name, String eventTypeIds, String controlDeviceIds, String deviceIds,
            String regionIds, String userIds, String virtualStateIds, String scenarioIds, Integer lineNo,
            String logTypeIds, String stateCategoryIds, String indicatorsInfo, String employeeIds, Date occurredBefore,
            Date receivedBefore, String tags) {

        BooleanBuilder where = getPredicate(idAfter, idBefore, subsystems, occurredAfter, receivedAfter, name,
                eventTypeIds, controlDeviceIds, deviceIds, regionIds, userIds, virtualStateIds, scenarioIds, lineNo,
                logTypeIds, stateCategoryIds, indicatorsInfo, employeeIds, occurredBefore, receivedBefore, tags);

        return dslAggregatedEventViewRepository.count(where);
    }
}
