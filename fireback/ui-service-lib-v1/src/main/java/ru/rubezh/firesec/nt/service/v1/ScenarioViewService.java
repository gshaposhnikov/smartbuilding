package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.*;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public interface ScenarioViewService {

    class RemovedScenarioInfo{
        public String projectId;
        public String removedScenarioId;
        public List<IndicatorPanel> updatedIndicatorPanels = new ArrayList<>();

        public RemovedScenarioInfo() {
        }

        public RemovedScenarioInfo(String projectId) {
            this.projectId = projectId;
        }
    }

    /**
     * Затянуть все отображения триггеров сценариев из отображения профиля драйвера в БД.
     * @param driverProfileView отображение профиля драйвера
     */
    void fetchScenarioTriggerTypeViews(DriverProfileView driverProfileView);
    
    /**
     * Затянуть все отображения действий сценариев из отображения профиля драйвера в БД.
     * @param driverProfileView отображение профиля драйвера
     */
    void fetchScenarioActionTypeViews(DriverProfileView driverProfileView);

    /**
     * Получение списка обобщённых отображений сценариев (в редакторе проектов)
     * @param projectId идентификатор проекта
     * @return список обобщённых отображений сценариев
     */
    List<AggregatedScenarioView> getAllAggregatedScenarioViews(String projectId);

    /**
     * Получение обобщённого отображения сценария (в редакторе проектов)
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView getAggregatedScenarioView(String projectId, String scenarioId,
                                                            StringBuilder errorMessage);

    /**
     * Получение обобщенных отображений сценариев для редактора проектов,
     * содержащих указанные устройства
     * 
     * @param projectId
     *            идентификатор проекта
     * @param deviceIds
     *            идентификаторы устройств
     * @return список обобщённых отображений сценариев
     */
    List<AggregatedScenarioView> getAggregatedScenarioViewsContainingDevices(String projectId,
            List<String> deviceIds);

    /**
     * Получение списка обобщённых отображений сценариев (в ОЗ)
     * @param projectId идентификатор проекта
     * @return список обобщённых отображений сценариев
     */
    List<AggregatedActiveScenarioView> getAllAggregatedActiveScenarioViews(String projectId);

    /**
     * Получение обобщённого отображения сценария (в ОЗ)
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedActiveScenarioView getAggregatedActiveScenarioView(String projectId, String scenarioId,
                                                                        StringBuilder errorMessage);

    /**
     * Удаление сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param errorMessage возвращаемый текст ошибки
     * @return Удаление выполнено без ошибок
     */
    RemovedScenarioInfo remove(String projectId, String scenarioId, StringBuilder errorMessage);

    /**
     * Создание сценария из базовых параметров
     * @param projectId идентификатор проекта
     * @param scenarioBasicParams набор базовых параметров
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView createByBasicParams(String projectId, ScenarioBasicParams scenarioBasicParams,
                                                      StringBuilder errorMessage);

    /**
     * Изменение базовых параметров сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param scenarioBasicParams набор базовых параметров
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView updateBasicParams(String projectId, String scenarioId,
                                                    ScenarioBasicParams scenarioBasicParams,
                                                    StringBuilder errorMessage);

    /**
     * Изменение расширенных параметров сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param scenarioAdvancedParams набор расширеных параметров
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView updateAdvancedParams(String projectId, String scenarioId,
                                                       ScenarioAdvancedParams scenarioAdvancedParams,
                                                       StringBuilder errorMessage);

    /**
     * Изменение логики запуска сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param scenarioLogicBlock параметры запуска
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView updateStartLogic(String projectId, String scenarioId,
                                                   ScenarioLogicBlock scenarioLogicBlock,
                                                   StringBuilder errorMessage);

    /**
     * Изменение логики остановки сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param scenarioLogicBlock параметры остановки
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView updateStopLogic(String projectId, String scenarioId,
                                                  ScenarioLogicBlock scenarioLogicBlock,
                                                  StringBuilder errorMessage);

    /**
     * Создание временного блока сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param scenarioTimeLineBlock параметры временного блока
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView createTimeLineBlock(String projectId, String scenarioId,
                                                      ScenarioTimeLineBlock scenarioTimeLineBlock,
                                                      StringBuilder errorMessage);

    /**
     * Изменение блока сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param blockNo порядковый номер блока сценария
     * @param scenarioTimeLineBlock параметры блока
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView updateBlock(String projectId, String scenarioId, int blockNo,
                                              ScenarioTimeLineBlock scenarioTimeLineBlock,
                                              StringBuilder errorMessage);

    /**
     * Удаление блока сценария
     * @param projectId идентификатор проекта
     * @param scenarioId идентификатор сценария
     * @param blockNo порядковый номер блока сценария
     * @param errorMessage возвращаемый текст ошибки
     * @return обобщённое отображение сценария
     */
    AggregatedScenarioView removeBlock(String projectId, String scenarioId, int blockNo,
                                              StringBuilder errorMessage);

    /**
     * Проверка и добавление сценариев импортируемого проекта
     * @param projectId id импортируемого проекта
     * @param scenarios сценарии
     * @param oldAndNewScenariosId карта старых идентификаторов сценариев к новым
     * @param oldAndNewDevicesId карта старых идентификаторов устройств к новым
     * @param oldAndNewRegionsId карта старых идентификаторов зон к новым
     * @param oldAndNewVirtualStatesId карта старых идентификаторов виртуальных состояний к новым
     * @param errorMessage сообщение возможной ошибки
     * @return true, если сценарии были вставлены, иначе false
     */
    boolean checkAndAdditionScenarioViewToImportProject(String projectId, List<Scenario> scenarios,
            Map<String, String> oldAndNewScenariosId , Map<String, String> oldAndNewDevicesId,
            Map<String, String> oldAndNewRegionsId, Map<String, String> oldAndNewVirtualStatesId,
            StringBuilder errorMessage);

    /**
     * Сгенерировать отображения сценариев на основе проектных сущностей
     * 
     * @param scenarios сценарии (проектные сущности)
     * @param devices устройства (обобщённые отображения)
     * @param regions зоны (проектные сущности)
     * @param virtualStates виртуальные состояния
     * @return обобщённые отображения сценариев
     */
    List<AggregatedScenarioView> generateAggregatedScenarioViews(List<Scenario> scenarios,
            List<TreeItemDeviceView> devices, List<Region> regions, List<VirtualState> virtualStates);

    /**
     * Сгенерировать отображения сценариев на их основе
     *
     * @param projectId идентификатор проекта
     * @param scenarios сценарии (проектные сущности)
     * @return обобщённые отображения сценариев
     */
    List<AggregatedScenarioView> generateAggregatedScenarioViews(String projectId, List<Scenario> scenarios);

    /**
     * Проверка сценария с поиском всех сущностей в базе данных
     */
    boolean validateScenario(Scenario scenario, StringBuilder errorMessage);

    /**
     * Поиск идентификаторов типов запуска в блоке логики (включая подблоки)
     */
    void getTriggerTypeIds(ScenarioLogicBlock logic, Set<String> triggerTypeIds);

    /**
     * Поиск идентификаторов типов действий в исполнительном блоке
     */
    void getActionTypeIds(List<ScenarioTimeLineBlock> timeLineBlocks, Set<String> actionTypeIds);

    /**
     * Генерация нового глобального номера сценария для проекта
     */
    int getNewGlobalNo(String projectId);

    /**
     * Формирование представления сценария на основе данных из БД
     */
    AggregatedScenarioView getAggregatedScenarioView(Scenario scenario);
    
    boolean validateScenarioBasicParams(Scenario scenario, ScenarioBasicParams params, StringBuilder errorMessage); 
    
    boolean validateScenarioTimeLineBlock(ScenarioTimeLineBlock block, StringBuilder errorMessage, 
            ScenarioPurpose scenarioPurpose, boolean ignoreScenarioActions); 
     
    boolean validateScenarioLogicBlock(String projectId,  ScenarioLogicBlock block, ScenarioPurpose scenarioPurpose, 
            StringBuilder errorMessage);

    /**
     * Безопасно (с ограниченным кол-вом занимаемой памяти) записать
     * агрегированные отображения активных сценариев в поток вывода
     *
     * @param projectId
     *            идентификатор проекта
     * @param outputStream
     *            поток вывода для записи
     * @param objectMapper
     *            преобразователь объектов в JSON
     */
    void safelyWriteActiveDeviceTreeItemByProjectId(String projectId, OutputStream outputStream,
                                                    ObjectMapper objectMapper) throws IOException;

    /**
     * Постранично получить агрегированные представления
     * активных сценариев, время обновления состояния
     * которых позже заданного
     *
     * @param lastObservedTime последнее время проверки
     * @param pageSize размер страницы
     * @param pageNo номер страницы
     * @return список агрегированных представлений активных сценриев
     */
    List<AggregatedActiveScenarioView> getAggregatedActiveScenarioViewsByTimeAfter(Date lastObservedTime,
                                                                                   int pageSize, int pageNo);

    /**
     * Получить "действия на компьютере" с агрегированного
     * представления активного сценария.
     *
     * @param aggregatedActiveScenarioView представление сценария,
     *                                     с которого берутся действия
     * @return список "действий на компьютере"
     */
    List<ScenarioComputerAction> getComputerActionsFromScenario(AggregatedActiveScenarioView aggregatedActiveScenarioView);
}
