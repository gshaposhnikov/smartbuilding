package ru.rubezh.firesec.nt.service.v1;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceShape.AnimationType;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedDeviceProfileDictionary;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;

import java.util.*;

@Service
public class DeviceProfileViewService {

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private DeviceProfileViewRepository deviceProfileViewRepository;

    @Autowired
    private StateViewRepository stateViewRepository;

    @Autowired
    private DeviceShapeLibraryRepository deviceShapeLibraryRepository;

    @Autowired
    private DeviceShapeRepository deviceShapeRepository;

    @Autowired
    private StateCategoryViewRepository stateCategoryViewRepository;

    public void add(List<DeviceProfileView> deviceProfileViews) {
        for (DeviceProfileView deviceProfileView : deviceProfileViews)
            add(deviceProfileView);
    }

    private void add(DeviceProfileView deviceProfileView) {
        deviceProfileRepository.save(deviceProfileView.getDeviceProfile());
        deviceProfileViewRepository.save(deviceProfileView);

        if (deviceProfileView.getDeviceProfile().getDeviceCategory() == DeviceCategory.CONTAINER
                || deviceProfileView.getDeviceProfile().getDeviceCategory() == DeviceCategory.TRANSPORT)
            return;
        /* Добавим библиотеку шейпов по умолчанию, если таковая отсутствует */
        Date currentDateTime = new Date();
        DeviceShapeLibrary deviceShapeLibrary = deviceShapeLibraryRepository
                .findOne(deviceProfileView.getId() + DeviceShapeLibrary.DEFAULT_LIBRARY_POSTFIX);
        if (deviceShapeLibrary == null && deviceProfileView.getDeviceProfile().getDeviceCategory() != DeviceCategory.CONTAINER
                && deviceProfileView.getDeviceProfile().getDeviceCategory() != DeviceCategory.TRANSPORT) {
            deviceShapeLibrary = new DeviceShapeLibrary();
            deviceShapeLibrary.setCreated(currentDateTime);
            deviceShapeLibrary.setUpdated(currentDateTime);
            deviceShapeLibrary.setId(deviceProfileView.getId() + DeviceShapeLibrary.DEFAULT_LIBRARY_POSTFIX);
            deviceShapeLibrary.setName(DeviceShapeLibrary.DEFAULT_NAME);
            deviceShapeLibrary.setBuiltin(true);
            deviceShapeLibrary.setDeviceProfileId(deviceProfileView.getId());

            deviceShapeLibrary = deviceShapeLibraryRepository.insert(deviceShapeLibrary);
        }
        
        /* Добавим базовый шейп, если таковой отсутствует */
        if (deviceShapeLibrary != null
                && !deviceShapeRepository.existsByDeviceShapeLibraryIdAndStateCategoryId(deviceShapeLibrary.getId(), "")) {
            String deviceShapeLibraryId = deviceShapeLibrary.getId();
            String textureMediaId = deviceProfileView.getTextureMediaId();

            DeviceShape basicDeviceShape = createDeviceShape(currentDateTime, DeviceShape.BASIC_NAME, "");
            basicDeviceShape.setDeviceShapeLibraryId(deviceShapeLibraryId);
            basicDeviceShape.setBuiltinMediaId(textureMediaId);

            /* вместе с некоторыми базовыми шейпами создаются дополнительные шейпы */
            List<DeviceShape> additionalDeviceShapes = new ArrayList<>();
            DeviceProfile deviceProfile = deviceProfileView.getDeviceProfile();
            List<StateCategoryView> stateCategoryViews = new ArrayList<>();
            if (deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL){
                stateCategoryViews.addAll(stateCategoryViewRepository.findByFireStateCategoryIsTrue());
                stateCategoryViews.add(stateCategoryViewRepository.findByAlarmStateCategoryIsTrue());
                stateCategoryViews.add(stateCategoryViewRepository.findByWarningStateCategoryIsTrue());
            } else if (deviceProfile.getSubsystem() == Subsystem.SECURITY
                    && deviceProfile.getDeviceCategory() == DeviceCategory.SENSOR){
                stateCategoryViews.add(stateCategoryViewRepository.findByAlarmStateCategoryIsTrue());
            } else if (deviceProfile.getSubsystem() == Subsystem.FIRE
                    && deviceProfile.getDeviceCategory() == DeviceCategory.SENSOR){
                stateCategoryViews.addAll(stateCategoryViewRepository.findByFireStateCategoryIsTrue());
                stateCategoryViews.add(stateCategoryViewRepository.findByWarningStateCategoryIsTrue());
            } else if (deviceProfile.getDeviceCategory() == DeviceCategory.EXECUTIVE
                    && (deviceProfile.getSubsystem() == Subsystem.GENERAL || deviceProfile.getSubsystem() == Subsystem.FIRE)){
                stateCategoryViews.add(stateCategoryViewRepository.findByAutoOffStateCategoryIsTrue());
            } else if (deviceProfile.getSubsystem() == Subsystem.SECURITY
                    && deviceProfile.getDeviceCategory() == DeviceCategory.EXECUTIVE){
                stateCategoryViews.add(stateCategoryViewRepository.findByAlarmStateCategoryIsTrue());
                stateCategoryViews.add(stateCategoryViewRepository.findByWarningStateCategoryIsTrue());
            }
            for (StateCategoryView stateCategoryView: stateCategoryViews) {
                additionalDeviceShapes.add(createDeviceShape(new Date(), stateCategoryView.getName(),
                        stateCategoryView.getStateCategory().getId()));
            }
            deviceShapeRepository.insert(basicDeviceShape);
            for (DeviceShape deviceShape : additionalDeviceShapes) {
                deviceShape.setDeviceShapeLibraryId(deviceShapeLibraryId);
                deviceShape.setBuiltinMediaId(textureMediaId);
                deviceShape.setAnimationType(AnimationType.BLINKING);
                deviceShape.setAnimationSpeed(2000);
            }
            deviceShapeRepository.insert(additionalDeviceShapes);
        }
    }

    //метод не заполняет deviceShapeLibraryId и textureMediaId
    private DeviceShape createDeviceShape(Date currentDateTime, String name, String stateCategoryId){
        DeviceShape deviceShape = new DeviceShape();
        deviceShape.setCreated(currentDateTime);
        deviceShape.setUpdated(currentDateTime);
        deviceShape.setName(name);
        deviceShape.setStateCategoryId(stateCategoryId);
        deviceShape.setAnimationType(AnimationType.NONE);
        return deviceShape;
    }

    public void remove(DeviceProfileView deviceProfileView) {
        if (deviceProfileRepository.findOne(deviceProfileView.getDeviceProfile().getId()) != null)
            deviceProfileRepository.delete(deviceProfileView.getDeviceProfile());
        deviceProfileViewRepository.delete(deviceProfileView);
    }

    private void attacheStateViews(DeviceProfileView deviceProfileView) {
        Set<String> stateIds = new HashSet<>();
        DeviceProfile deviceProfile = deviceProfileView.getDeviceProfile();
        if (deviceProfile.getDeviceLostState() != null && deviceProfile.getDeviceLostState().getId() != null)
            stateIds.add(deviceProfile.getDeviceLostState().getId());
        if (deviceProfile.getDeviceNotReadyState() != null && deviceProfile.getDeviceNotReadyState().getId() != null)
            stateIds.add(deviceProfile.getDeviceNotReadyState().getId());
        if (deviceProfile.getDeviceReadyState() != null && deviceProfile.getDeviceReadyState().getId() != null)
            stateIds.add(deviceProfileView.getDeviceProfile().getDeviceReadyState().getId());
        if (deviceProfile.isIgnorable() && deviceProfile.getDeviceIgnoreStateId() != null)
            stateIds.add(deviceProfile.getDeviceIgnoreStateId());
        for (State state : deviceProfile.getCustomBitStates()) {
            stateIds.add(state.getId());
        }
        for (State state : deviceProfile.getCustomMaskedStates()) {
            stateIds.add(state.getId());
        }
        stateIds.addAll(deviceProfile.getOtherCustomStateIds());
        deviceProfileView.setStateViews(stateViewRepository.findByIdIn(stateIds));
    }

    public List<DeviceProfileView> getByLanguage(Language language) {
        List<DeviceProfileView> deviceProfileViews = deviceProfileViewRepository.findByLanguage(language);
        for (DeviceProfileView deviceProfileView : deviceProfileViews) {
            attacheStateViews(deviceProfileView);
        }
        return deviceProfileViews;
    }

    public List<DeviceProfileView> getByLanguageAndConnectionType(Language language,
            ConnectionInterfaceType connectionType) {
        List<DeviceProfileView> result = new ArrayList<>();
        List<DeviceProfile> deviceProfiles = deviceProfileRepository.findByConnectionTypesContaining(connectionType);
        for (DeviceProfile deviceProfile : deviceProfiles) {
            DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language, deviceProfile.getId());
            attacheStateViews(deviceProfileView);
            result.add(deviceProfileView);
        }
        return result;
    }

    public DeviceProfileView getByLanguageAndDeviceProfileId(Language language, String deviceProfileId) {
        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language, deviceProfileId);
        attacheStateViews(deviceProfileView);
        return deviceProfileView;
    }

    public AggregatedDeviceProfileDictionary getDeviceProfilesDictionary() {
        AggregatedDeviceProfileDictionary deviceProfileDictionary = new AggregatedDeviceProfileDictionary();

        for (DeviceGroup deviceGroup: DeviceGroup.values()){
            deviceProfileDictionary.addDeviceGroup(new IdAndNameAndDescr(deviceGroup.name(), deviceGroup.getViewName(), ""));
        }

        return deviceProfileDictionary;
    }
}
