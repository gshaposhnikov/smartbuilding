package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.*;

@Service
public class MonitorableValueViewService {

    @Autowired
    MongoTemplate mongoTemplate;

    public void fetchValueViews(DriverProfileView driverProfileView) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, MonitorableValueProfileView.class);
        /* Сначала удаляем старые версии отображений состояний */
        for (MonitorableValueProfileView valueView: driverProfileView.getMonitorableValueProfileViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(valueView.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии отображений состояний */
        bulkOps.insert(driverProfileView.getMonitorableValueProfileViews());
        bulkOps.execute();
    }

}
