package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.LogViewRepository;
import ru.rubezh.firesec.nt.domain.v1.LogView;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;

@Service
public class LogViewService {

    @Autowired
    private LogViewRepository logViewRepository;

    private static final int MIN_RECORD_COUNT = 1;
    private static final int MAX_RECORD_COUNT = 1_000;

    public boolean add(LogView logView, StringBuilder errorMessage){
        if (checkInput(logView, errorMessage)) {
            logView.setCreateDateTime(new Date());
            logView.setPosition((int) logViewRepository.count());
            logViewRepository.insert(logView);
            return true;
        } else {
            return false;
        }
    }

    public boolean update(String id, LogView logView, StringBuilder errorMessage){
        if (checkInput(logView, errorMessage)) {
            LogView updatableLogView = logViewRepository.findOneByRemovedIsFalseAndId(id);
            if (updatableLogView == null) {
                errorMessage.append("Изменяемое представление не найдено");
                return false;
            }
            int oldPosition = updatableLogView.getPosition();
            int newPosition = logView.getPosition();
            if (oldPosition != newPosition) {
                LogView neighbourLogView = logViewRepository.findOneByRemovedIsFalseAndPosition(newPosition);
                if (neighbourLogView != null) {
                    neighbourLogView.setPosition(oldPosition);
                    neighbourLogView.setUpdateDateTime(new Date());
                    logViewRepository.save(neighbourLogView);
                    updatableLogView.setPosition(newPosition);
                }
            }
            updatableLogView.update(logView);
            logViewRepository.save(updatableLogView);
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(String id, StringBuilder errorMessage){
        LogView removableLogView = logViewRepository.findOneByRemovedIsFalseAndId(id);
        if (removableLogView == null){
            errorMessage.append("Удаляемое представление не найдено");
            return false;
        } else {
            List<LogView> tailLogViews =
                    logViewRepository.findByRemovedIsFalseAndPositionGreaterThan(removableLogView.getPosition());
            if (tailLogViews != null){
                for (LogView logView: tailLogViews) {
                    logView.setPosition(logView.getPosition() - 1);
                    logView.setUpdateDateTime(new Date());
                }
            }
            removableLogView.setRemoved(true);
            logViewRepository.save(tailLogViews);
            logViewRepository.save(removableLogView);
            return true;
        }
    }

    private boolean checkInput(LogView logView, StringBuilder errorMessage){
        if (logView.getName() == null){
            errorMessage.append("Не задано название журнала");
            return false;
        }
        if (logView.getnLastDays() < 0){
            errorMessage.append("Число дней не может быть отрицательным");
            return false;
        }
        if (logView.getRecordCount() > MAX_RECORD_COUNT){
            errorMessage.append("Максимальное число записей - 1000");
            return false;
        }
        if (logView.getRecordCount() < MIN_RECORD_COUNT){
            errorMessage.append("Минимальное число записей - 1");
            return false;
        }
        return true;
    }

    public EntitiesContainer<LogView> getChangesAndDoRemove(Date from){
        /* Заполняем изменения */
        EntitiesContainer<LogView> changes = new EntitiesContainer<>();
        changes.setCreated(logViewRepository.findByRemovedIsFalseAndCreateDateTimeAfter(from));
        changes.setUpdated(logViewRepository.findByRemovedIsFalseAndUpdateDateTimeAfter(from));
        List<String> removedLogViewIds = logViewRepository.findByRemovedIsTrue().stream()
                .map(LogView::getId).collect(Collectors.toList());
        changes.setDeletedIds(removedLogViewIds);

        /* Удаление отмеченных представлений */
        logViewRepository.deleteByIdIn(removedLogViewIds);

        return changes;
    }
}
