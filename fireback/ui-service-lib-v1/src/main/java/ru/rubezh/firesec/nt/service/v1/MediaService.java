package ru.rubezh.firesec.nt.service.v1;

import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.Media;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

import java.util.Date;

@Service
public interface MediaService {

    interface MediaObserver{
        /**
         * Проверка на использование медиафайла у "подписчика"
         *
         * @param mediaId идентифиувтор медиафайла
         * @return true, если медиафайл используется, иначе false
         */
        boolean checkIfMediaIsUsed(String mediaId);
    }

    /**
     * Добавление медиафайла.
     *
     * Добавляет медиафайл, если длина имени не превышает допустимого значения.
     *
     * @param media создаваемый медиафайл
     * @param currentUser действующий пользователь
     * @param errorMessage сообщение возможной ошибки
     * @return true если медиафайл успешно добавлен, иначе false
     */
    boolean addMedia(Media media, User currentUser, StringBuilder errorMessage);

    /**
     * Обновление медиафайла.
     *
     * Обновляет медиафайл, если он есть, его тип не изменяется
     * и длина имени не превышает допустимого значения.
     *
     * @param id идентификатор обновляемого медиафайла
     * @param media обьект с обновленными параметрами медиафайла
     * @param errorMessage сообщение возможной ошибки
     * @return true если медиафайл успешно добавлен, иначе false
     */
    boolean updateMedia(String id, Media media, StringBuilder errorMessage);

    /**
     * Удаление медиафайла.
     *
     * Удаляет медиафайл, если он есть и не используется в
     * звуковых оповещениях.
     *
     * @param id идентификатор удаляемого медиафайла
     * @param errorMessage сообщение возможной ошибки
     * @return true, если медиафайл помечен как удаленный, иначе false
     */
    boolean deleteMedia(String id, StringBuilder errorMessage);

    /**
     * Получение обьектов(или их идентификаторов) для созданных/обновленных/удаленных сущностей
     *
     * Собирает информацию по медиафайлам, которые были созданы, обновлены или удалены
     * со времени последней проверки.
     *
     * @param from дата и время последней проверки.
     * @return обьект с информацией по измененным медиафайлам.
     */
    UpdatedEntities.EntitiesContainer<Media> getChangesAndDoRemove(Date from);

    /**
     * Проверяетс идентификатор звукового файла на допустимость указать его звуковому оповещению.
     *
     * Проверяет наличие в базе медиафайла с указанным идентификатором,
     * если идентификатор не пуст
     *
     * @param id идентификатор медиафайла
     * @param errorMessage сообщение возможной ошибки
     * @return true, если существует медиафайл с указанным идентификатором,
     * идентификатор пуст или равен null. Иначе false.
     */
    boolean checkSoundMedia(String id, StringBuilder errorMessage);

    /**
     * Добавить "подписчика" на действия с медиафайлами
     *
     * @param mediaObserver
     */
    void addMediaObserver(MediaObserver mediaObserver);

    /**
     * Убрать "подписчика" на действия с медиафайлами
     *
     * @param mediaObserver
     */
    void removeMediaObserver(MediaObserver mediaObserver);

}
