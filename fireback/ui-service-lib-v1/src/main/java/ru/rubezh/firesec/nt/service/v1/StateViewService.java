package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;
import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;
import ru.rubezh.firesec.nt.domain.v1.StateView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StateViewService {

    @Autowired
    MongoTemplate mongoTemplate;

    private Map<String, StateCategoryView> stateCategoryViewsById = new HashMap<>();

    @Autowired
    public StateViewService(List<StateCategoryView> stateCategoryViews) {
        for (StateCategoryView stateCategoryView : stateCategoryViews) {
            stateCategoryViewsById.put(stateCategoryView.getId(), stateCategoryView);
        }
    }

    public void fetchStateViews(DriverProfileView driverProfileView) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, StateView.class);
        /* Сначала удаляем старые версии отображений состояний */
        for (StateView stateView: driverProfileView.getStateViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(stateView.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии отображений состояний */
        bulkOps.insert(driverProfileView.getStateViews());
        bulkOps.execute();
    }

    public StateCategoryView getStateCategoryViewById(String id) {
        return stateCategoryViewsById.get(id);
    }

    public List<StateCategoryView> getByIdIn(List<String> ids) {
        List<StateCategoryView> views = new ArrayList<>();
        for (String id: ids) {
            StateCategoryView category = stateCategoryViewsById.get(id);
            if (category != null) views.add(category);
        }
        return views;
    }

}
