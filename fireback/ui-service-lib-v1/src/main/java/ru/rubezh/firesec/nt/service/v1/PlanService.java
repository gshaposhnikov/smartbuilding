package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface PlanService {

    class PlanReferences {

        public List<TreeItemDeviceView> devices = new ArrayList<>();
        public List<RegionView> regions = new ArrayList<>();
    }
    Plan add(String projectId, Language language, Plan plan, StringBuilder errorMessage);

    Plan update(String projectId, Language language, String planId, Plan plan,
            StringBuilder errorMessage);

    PlanReferences remove(String projectId, Language language, String planId,
            StringBuilder errorMessage);

    boolean checkAndAdditionPlanToImportProject(List<Plan> plans, String projectId, Map<String, String> oldAndNewPlansId,
                                                       Map<String, String> oldAndNewPlanGroupsId, StringBuilder errorMessage);

}
