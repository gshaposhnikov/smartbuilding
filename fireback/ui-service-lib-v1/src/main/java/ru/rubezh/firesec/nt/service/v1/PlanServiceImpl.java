package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.*;

@Service
@Order(7)
public class PlanServiceImpl implements PlanService, ActivationFilterableService {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PlanGroupRepository planGroupRepository;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private RegionViewRepository regionViewRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private DeviceViewService deviceViewService;

    @Override
    public Plan add(String projectId, Language language, Plan plan, StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            if (plan.getxSize() > 5000 || plan.getySize() > 5000
                    || plan.getxSize() < 100 || plan.getySize() < 100) {
                errorMessage.append("Указан неверный размер плана");
            } else if (plan.getPlanGroupId() != null && !plan.getPlanGroupId().isEmpty()
                    && !planGroupRepository.exists(plan.getPlanGroupId())) {
                errorMessage.append("Группа планов с идентификатором ").append(plan.getPlanGroupId())
                        .append(" не найдена");
            } else {
                plan.setProjectId(projectId);
                return planRepository.insert(plan);
            }
        }

        return null;
    }

    @Override
    public Plan update(String projectId, Language language, String planId, Plan plan, StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Plan prevPlan = planRepository.findByProjectIdAndId(projectId, planId);
            if (prevPlan != null) {
                if (plan.getxSize() > 5000 || plan.getySize() > 5000
                        || plan.getxSize() < 100 || plan.getySize() < 100) {
                    errorMessage.append("Указан неверный размер плана");
                } else if (plan.getPlanGroupId() != null && !plan.getPlanGroupId().isEmpty()
                        && !planGroupRepository.exists(plan.getPlanGroupId())) {
                    errorMessage.append("Группа планов с идентификатором ").append(plan.getPlanGroupId())
                            .append(" не найдена");
                } else {
                    prevPlan.update(plan);
                    try {
                        return planRepository.save(prevPlan);
                    }
                    catch(Exception e) {
                        errorMessage.append(e.toString());
                    }
                }

            } else {
                errorMessage.append("Не найдена прежняя копия изменяемого плана");
            }
        }

        return null;
    }

    @Override
    public PlanReferences remove(String projectId, Language language, String planId, StringBuilder errorMessage) {
        PlanReferences planReferences = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
            if (plan == null) {
                errorMessage.append("План не найден");
            } else {
                planRepository.deleteByProjectIdAndId(projectId, planId);

                planReferences = new PlanReferences();

                /* Удалим ссылки на план в отображениях устройств */
                List<DeviceView> deviceViews = deviceViewRepository.findByProjectIdAndPlanLayouts_planId(projectId,
                        planId);
                for (DeviceView deviceView : deviceViews) {
                    deviceView.getPlanLayouts().removeIf(layout -> layout.getPlanId().equals(planId));
                    deviceView = deviceViewRepository.save(deviceView);
                }
                planReferences.devices = deviceViewService.getTreeItemDeviceViewsByDeviceViews(projectId,
                        language, deviceViews);

                /* Удалим ссылки на план в отображениях зон */
                List<RegionView> regionViews = regionViewRepository.findByProjectIdAndPlanLayouts_planId(projectId,
                        planId);
                for (RegionView regionView : regionViews) {
                    regionView.getPlanLayouts().removeIf(layout -> layout.getPlanId().equals(planId));
                }
                planReferences.regions = regionViewRepository.save(regionViews);
            }
        }

        return planReferences;
    }

    public boolean checkAndAdditionPlanToImportProject(List<Plan> plans, String projectId, Map<String, String> oldAndNewPlansId,
            Map<String, String> oldAndNewPlanGroupsId, StringBuilder errorMessage) {
        for (Plan plan : plans) {
            plan.setProjectId(projectId);
            String oldPlanId = plan.getId();
            if(plan.getPlanGroupId() != null) {
                if (oldAndNewPlanGroupsId.containsKey(plan.getPlanGroupId())) {
                    plan.setPlanGroupId(oldAndNewPlanGroupsId.get(plan.getPlanGroupId()));
                } else {
                    errorMessage.append("Для плана ").append(plan.getName()).append(" не найдена соответствующая группа планов");
                    return false;
                }
                if(!plan.getPlanGroupId().isEmpty()) {
                    if(!planGroupRepository.exists(plan.getPlanGroupId())){
                        errorMessage.append("Группы планов, указанной в плане ").append(plan.getName()).append(" не существует");
                        return false;
                    }
                }
            }
            if (plan.getxSize() > 5000 || plan.getySize() > 5000 || plan.getxSize() < 100 || plan.getySize() < 100) {
                errorMessage.append("Размеры плана ").append(plan.getName()).append(" вне допустимых размеров");
                return false;
            }
            plan.clearId();
            oldAndNewPlansId.put(oldPlanId, planRepository.insert(plan).getId());
        }
        return true;
    }

    @Override
    public void clearTags(String projectId) {
        List<Plan> plans = planRepository.findByProjectId(projectId);
        plans.forEach(plan -> plan.getFilterTags().getTags().clear());
        planRepository.save(plans);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<Plan> plans = planRepository.findByProjectId(projectId);
        List<DeviceView> deviceViews = deviceViewRepository.findByProjectId(projectId);
        List<RegionView> regionViews = regionViewRepository.findByProjectId(projectId);

        plans.forEach(plan -> {
            FilterTags filterTags = plan.getFilterTags();
            filterTags.getTags().addAll(getAvailableSubsystems(plan, deviceViews, regionViews));
        });

        planRepository.save(plans);
    }

    private Set<String> getAvailableSubsystems(Plan plan, List<DeviceView> deviceViews, List<RegionView> regionViews) {
        Set<String> availableSubsystems = new HashSet<>();

        deviceViews.forEach(deviceView -> {
            List<DeviceView.DevicePlanLayout> devicePlanLayouts = deviceView.getPlanLayouts();
            devicePlanLayouts.forEach(devicePlanLayout -> {
                if (devicePlanLayout.getPlanId().equals(plan.getId())){
                    availableSubsystems.addAll(deviceView.getDevice().getFilterTags().chooseSubsystemTags());
                }
            });
        });

        regionViews.forEach(regionView -> {
            List<RegionView.RegionPlanLayout> regionPlanLayouts = regionView.getPlanLayouts();
            regionPlanLayouts.forEach(regionPlanLayout -> {
                if (regionPlanLayout.getPlanId().equals(plan.getId())){
                    availableSubsystems.add(regionView.getRegion().getSubsystem().toString());
                }
            });
        });

        Subsystem planSubsystem = plan.getSubsystem();
        if (planSubsystem != Subsystem.GENERAL || availableSubsystems.isEmpty()){
            availableSubsystems.add(planSubsystem.toString());
        }

        return availableSubsystems;
    }
}
