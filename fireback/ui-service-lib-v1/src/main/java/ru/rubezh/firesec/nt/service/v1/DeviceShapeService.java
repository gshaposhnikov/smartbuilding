package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.DeviceShape;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

public interface DeviceShapeService {

    /**
     * Получить все актуальные шейпы устройств.
     * 
     * Актуальными считаются те шейпы, для которых есть актуальная библиотека и
     * зарегистрированный в системе класс состояния (либо это базовый шейп), а
     * также не отмеченные для удаления.
     * 
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return набор всех актуальных шейпов устройств или null (в случае ошибки)
     */
    List<DeviceShape> getActual(StringBuilder errorMessage);

    /**
     * Получить шейп устройства по идентификатору, если он актуален и
     * существует.
     * 
     * Актуальными считаются те шейпы, для которых есть актуальная библиотека и
     * зарегистрированный в системе класс состояния (либо это базовый шейп), а
     * также не отмеченные для удаления.
     * 
     * @param deviceShapeId
     *            идентификатор шейпа устройств
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return искомый шейп или null (в случае ошибки)
     */
    DeviceShape getIfActual(String deviceShapeId, StringBuilder errorMessage);

    /**
     * Добавить новый шейп устройства.
     * 
     * Обязательно должны быть указаны идентификатор актуальной существующей
     * библиотеки и идентификатор зарегистрированного в системе класса состояния
     * (базовый шейп этим методом добавляться не может). Также реализация метода
     * должна проверять уникальность пары этих идентификаторов.
     * 
     * Добавленному шейпу должно выставляться время создания (текущее).
     * 
     * @param deviceShape
     *            параметры создаваемого шейпа
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean add(DeviceShape deviceShape, StringBuilder errorMessage);

    /**
     * Изменить существующий шейп устройства.
     * 
     * Измененному шейпу должно выставляться время изменения (текущее).
     * 
     * @param deviceShapeId
     *            идентификатор изменяемого шейпа
     * @param deviceShape
     *            новые параметры шейпа
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean update(String deviceShapeId, DeviceShape deviceShape, StringBuilder errorMessage);

    /**
     * Удалить существующий шейп.
     * 
     * Шейп должен быть только отмечен к удалению.
     * 
     * Нельзя удалить базовый шейп.
     * 
     * @param deviceShapeId
     *            идентификатор удаляемого шейпа
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean remove(String deviceShapeId, StringBuilder errorMessage);

    /**
     * Получить изменения с указанного времени (все созданные и измененные шейпы
     * устройств, а также идентификаторы удаленных), и выполнить фактическое
     * удаление отмеченных для этого шейпов.
     * 
     * @param from
     *            момент времени, изменения от которого запрашиваются
     * @return контейнер измененных сущностей
     */
    UpdatedEntities.EntitiesContainer<DeviceShape> getChangesAndDoRemove(Date from);

}
