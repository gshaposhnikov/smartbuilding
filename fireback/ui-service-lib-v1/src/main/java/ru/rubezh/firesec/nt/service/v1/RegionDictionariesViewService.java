package ru.rubezh.firesec.nt.service.v1;

import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.SecurityRegionType;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedRegionDictionary;

@Service
public class RegionDictionariesViewService {

    public AggregatedRegionDictionary getAggregatedRegionDictionaries(StringBuilder errorMessage) {
        AggregatedRegionDictionary result = new AggregatedRegionDictionary();

        /** Типы охранной зоны */
        result.getSecurityTypes().add(new IdAndNameAndDescr(SecurityRegionType.DEFAULT.toString(),
                "Обычная","Сработка устройства в зоне активирует тревогу"));
        result.getSecurityTypes().add(new IdAndNameAndDescr(SecurityRegionType.TIMEOUT.toString(),
                "С задержкой входа/выхода",
                "После сработки устройства в зоне тревога активируется после установленного времени"));
        result.getSecurityTypes().add(new IdAndNameAndDescr(SecurityRegionType.NO_OFF.toString(),
                "Без права снятия","Нет возможности снять зону с охраны после постановки"));

        return result;
    }

}
