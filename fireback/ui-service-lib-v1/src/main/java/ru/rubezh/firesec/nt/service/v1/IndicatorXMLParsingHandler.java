package ru.rubezh.firesec.nt.service.v1;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.DefaultHandler2;

import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage;
import ru.rubezh.firesec.nt.domain.v1.Indicator;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;

/**
 * Класс для получения индикатора из импортированного файла (.FSc), проекта delphi версии, находящее в CDATA тэга Сontent.
 * @author Антон Лягин
 */

@Service
public class IndicatorXMLParsingHandler extends DefaultHandler2{
    
    /** Xml префикс.*/
    private final static String XML_PREFIX = "<?xml";
    /** количество столбцов.*/
    private static final String COLOMN_NO = "Col";
    /** количество строк.*/
    private static final String ROW_NO = "Row";
    /** Тэг индикатора.*/
    private static final String INDICATOR_TAG = "Indicator";
    
    @Autowired
    private IndicatorPropertiesXMLParsingHandler indicatorPropertiesXMLParsingHandler;
    
    @Autowired
    private IndicatorPanelService indicatorPanelService;
    /** Текущий индикатор.*/
    private Indicator currentIndicator;
    /** Текущая панель управления.*/
    private IndicatorPanel indicatorPanel;
    /** ID устройства текущей и desktop версии.*/
    private Map<String, String> deviceNewIdByDesktopId;
    /** ID зоны текущей и desktop версии.*/
    private Map<String, String> regionNewIdByDesktopId;
    /** ID сценария текущей и desktop версии.*/
    private Map<String, String> scenarioNewIdByDesktopId;
    /** Список предупреждений (ошибок) импорта свойств индикаторов.*/
    private List<ImportValidateMessage> idicatorPropertyWarningMessages;
    /** Список предупреждений (ошибок) импорта индикаторов.*/
    private List<ImportValidateMessage> indicatorWarningMessages;
    /** Пропустить индикатор.*/
    private boolean passIndicator = false;
    
    @Override 
    public void startDocument() throws SAXException {
        indicatorWarningMessages = new ArrayList<>();
    }
    
    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if(qName.equals(INDICATOR_TAG)) {
            if(atts.getValue(ROW_NO) != null && atts.getValue(COLOMN_NO) != null ) {
                idicatorPropertyWarningMessages = new ArrayList<>();
                try {
                    int rowNo = Integer.parseInt(atts.getValue(ROW_NO));
                    int colomnNo = Integer.parseInt(atts.getValue(COLOMN_NO));
                    if(rowNo <= indicatorPanel.getRowCount() && rowNo >= 0 && colomnNo <= indicatorPanel.getColCount() && colomnNo >= 0) {
                        currentIndicator = new Indicator(rowNo, colomnNo);
                    }
                } catch (Exception e) {
                    passIndicator = true;
                    String description ="Панель индикатора \"".concat(indicatorPanel.getName()).concat("\" не была добавлена") ;
                    indicatorWarningMessages.add(new ImportValidateMessage(Level.ERROR, Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, 
                            description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
                }
            }
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(!passIndicator) {
            String cdata = new String(ch, start, length);
            if (cdata.startsWith(XML_PREFIX)) {
                byte[] byteArrayFileConten;
                try {
                    byteArrayFileConten = cdata.getBytes();
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArrayFileConten);
                    SAXParserFactory spf = SAXParserFactory.newInstance();
                    SAXParser saxParser = spf.newSAXParser();
                    XMLReader xmlReader = saxParser.getXMLReader();
                    IndicatorPropertiesXMLParsingHandler handler = indicatorPropertiesXMLParsingHandler;
                    if(currentIndicator != null) {
                        handler.setCurrentIndicator(currentIndicator);
                    }
                    handler.setIndicatorPanel(indicatorPanel);
                    handler.setRegionNewIdByDesktopId(regionNewIdByDesktopId);
                    handler.setDeviceNewIdByDesktopId(deviceNewIdByDesktopId);
                    handler.setScenarioNewIdByDesktopId(scenarioNewIdByDesktopId);
                    handler.setProjectId(indicatorPanel.getProjectId());
                    xmlReader.setContentHandler(handler);
                    xmlReader.parse(new InputSource(inputStream));
                    inputStream.close();
                    currentIndicator = handler.getCurrentIndicator();
                    if(currentIndicator != null) {
                        int col = currentIndicator.getColNo();
                        int row = currentIndicator.getRowNo();
                        indicatorPanel.getIndicators().get(row).set(col, currentIndicator);
                        if(indicatorPanelService.updateIndicator(currentIndicator, indicatorPanel.getProjectId(), indicatorPanel.getId()
                                , currentIndicator.getColNo(), currentIndicator.getRowNo(), new StringBuilder()) != null) {
                            idicatorPropertyWarningMessages.addAll(handler.getIdicatorWarningMessages());
                        }else {
                            String description ="Панель индикатора \"".concat(indicatorPanel.getName()).concat("\" не прошла валидацию");
                            idicatorPropertyWarningMessages.add(new ImportValidateMessage(Level.ERROR,
                                    Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override 
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if(!passIndicator) {
            if(qName.equals(INDICATOR_TAG)) {
                int col = currentIndicator.getColNo();
                int row = currentIndicator.getRowNo();
                indicatorPanel.getIndicators().get(row).set(col, currentIndicator);
            }
        }
        passIndicator = false;
    }
    
    @Override 
    public void endDocument() throws SAXException {
        passIndicator = false;
        indicatorWarningMessages.addAll(idicatorPropertyWarningMessages);
    }

    public void setIndicatorPanel(IndicatorPanel indicatorPanel) {
        this.indicatorPanel = indicatorPanel;
    }

    public void setDeviceNewIdByDesktopId(Map<String, String> deviceNewIdByDesktopId) {
        this.deviceNewIdByDesktopId = deviceNewIdByDesktopId;
    }

    public void setRegionNewIdByDesktopId(Map<String, String> regionNewIdByDesktopId) {
        this.regionNewIdByDesktopId = regionNewIdByDesktopId;
    }

    public void setScenarioNewIdByDesktopId(Map<String, String> scenarioNewIdByDesktopId) {
        this.scenarioNewIdByDesktopId = scenarioNewIdByDesktopId;
    }

    public IndicatorPanel getIndicatorPanel() {
        return indicatorPanel;
    }

    public List<ImportValidateMessage> getIndicatorWarningMessages() {
        return indicatorWarningMessages;
    }
}
