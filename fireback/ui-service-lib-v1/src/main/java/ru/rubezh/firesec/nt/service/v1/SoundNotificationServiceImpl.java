package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.SoundNotificationRepository;
import ru.rubezh.firesec.nt.dao.v1.StateCategoryRepository;
import ru.rubezh.firesec.nt.domain.v1.SoundNotification;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SoundNotificationServiceImpl implements SoundNotificationService{

    private SoundNotificationRepository soundNotificationRepository;

    private StateCategoryRepository stateCategoryRepository;

    private MediaService mediaService;

    @Autowired
    public SoundNotificationServiceImpl(SoundNotificationRepository soundNotificationRepository,
                                        StateCategoryRepository stateCategoryRepository,
                                        MediaService mediaService){
        this.soundNotificationRepository = soundNotificationRepository;
        this.stateCategoryRepository = stateCategoryRepository;
        this.mediaService = mediaService;
        mediaService.addMediaObserver(this);
    }

    @Override
    public boolean addSoundNotification(SoundNotification soundNotification, StringBuilder errorMessage) {
        if (soundNotification.getStateCategoryId().isEmpty()){
            errorMessage.append("Для звукового оповещения должен быть задан класс состояния");
            return false;
        }
        if (!stateCategoryRepository.exists(soundNotification.getStateCategoryId())){
            errorMessage.append("Указанного класса состояния не существует");
            return false;
        }
        if (soundNotificationRepository.existsByStateCategoryId(soundNotification.getStateCategoryId())){
            errorMessage.append("Звуковое оповещение для такого класса состояний уже существует");
            return false;
        }
        if (!mediaService.checkSoundMedia(soundNotification.getMediaId(), errorMessage)){
            return false;
        }
        soundNotificationRepository.insert(soundNotification);
        return true;
    }

    @Override
    public boolean updateSoundNotification(String id, SoundNotification soundNotification, StringBuilder errorMessage) {
        SoundNotification updatableSoundNotification = soundNotificationRepository.findOne(id);
        if (updatableSoundNotification == null){
            errorMessage.append("Изменяемое звуковое оповещение не найдено");
            return false;
        }
        if (!soundNotification.getStateCategoryId().equals(updatableSoundNotification.getStateCategoryId())){
            errorMessage.append("Для звукового оповещения нельзя менять класс состояния");
            return false;
        }
        if (!mediaService.checkSoundMedia(soundNotification.getMediaId(), errorMessage)){
            return false;
        }
        updatableSoundNotification.update(soundNotification);
        soundNotificationRepository.save(updatableSoundNotification);
        return true;
    }

    @Override
    public boolean deleteSoundNotification(String id, StringBuilder errorMessage) {
        SoundNotification removableSoundNotification = soundNotificationRepository.findOne(id);
        if (removableSoundNotification == null){
            errorMessage.append("Удаляемое звуковое оповещение не найдено");
            return false;
        }
        removableSoundNotification.setRemoved(true);
        soundNotificationRepository.save(removableSoundNotification);
        return true;
    }

    @Override
    public UpdatedEntities.EntitiesContainer<SoundNotification> getChangesAndDoRemove(Date from){
        /* Заполняем изменения */
        UpdatedEntities.EntitiesContainer<SoundNotification> changes = new UpdatedEntities.EntitiesContainer<>();
        changes.setCreated(soundNotificationRepository.findByRemovedIsFalseAndCreateDateTimeAfter(from));
        changes.setUpdated(soundNotificationRepository.findByRemovedIsFalseAndUpdateDateTimeAfter(from));
        List<String> removedSoundNotificationIds = soundNotificationRepository.findByRemovedIsTrue().stream()
                .map(SoundNotification::getId).collect(Collectors.toList());
        changes.setDeletedIds(removedSoundNotificationIds);

        /* Удаление отмеченных представлений */
        soundNotificationRepository.deleteByIdIn(removedSoundNotificationIds);
        return changes;
    }

    @Override
    public boolean checkIfMediaIsUsed(String mediaId) {
        return soundNotificationRepository.existsByMediaId(mediaId);
    }
}
