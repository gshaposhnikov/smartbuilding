package ru.rubezh.firesec.nt.service.v1;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.DefaultHandler2;

import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceProfileViewRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceViewRepository;
import ru.rubezh.firesec.nt.dao.v1.IndicatorPanelRepository;
import ru.rubezh.firesec.nt.dao.v1.RegionRepository;
import ru.rubezh.firesec.nt.dao.v1.RegionViewRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioActionTypeRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioTriggerTypeRepository;
import ru.rubezh.firesec.nt.dao.v1.StateRepository;
import ru.rubezh.firesec.nt.dao.v1.VirtualStateRepository;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;
import ru.rubezh.firesec.nt.domain.v1.DeviceConfigProperty;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile.InternalDeviceItem;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfileView;
import ru.rubezh.firesec.nt.domain.v1.DeviceProperty;
import ru.rubezh.firesec.nt.domain.v1.DeviceView;
import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.PropertyType;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.RegionView;
import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioLogicBlock.LogicType;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Command;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionCheckType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.ConditionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.TracingEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioPurpose;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioType;
import ru.rubezh.firesec.nt.domain.v1.SecurityRegionType;
import ru.rubezh.firesec.nt.domain.v1.State;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanelGroup;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;
import ru.rubezh.firesec.nt.domain.v1.VirtualState;
import ru.rubezh.firesec.nt.domain.v1.representation.DeviceCreateView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.ScenarioBasicParams;

/**
 * Класс для получения всех сущностей из импортируего файла (***.FSc) проекта, delphi версии.
 * @author Антон Лягин
 *
 */
@Service
public class ImportedProjectXMLParsingHandler extends DefaultHandler2{
    /** Xml префикс, для определения xml файла.*/
    private final static String XML_PREFIX = "<?xml";
    /** Тэг DRV, содержит имя и ID устройства desktop версии.*/
    private final static String DRV_TAG = "drv";
    /** Описания зоны в тэге зона.*/
    private final static String DESCRIPTION = "desc";
    /** Тэг зоны.*/
    private final static String REGION_TAG = "zone";
    /** Тэг параметров для зоны, виртуального состояния и виртуального индикатора.*/
    private final static String PARAMETRS_TAG = "param";
    /** Тэг устройства.*/
    private final static String DEVICE_TAG = "dev";
    /** Тэг свойства устройства */
    private final static String DEVICE_PROPERTIES_TAG = "prop";
    /** Тэг содержащий данные для desktop версии, не используемые.*/
    private final static String DATABASE_INFO_TAG = "databaseInfo";
    /** Тэг виртуального состояния.*/ 
    private final static String VIRTUAL_STATE_TAG = "customstate";
    /** Пожараня подсистема сценария для зоны.*/
    private final static String SUBSYSTEM_PARAM_TYPE_FIRE = "0";
    /** Охраная подсистема сценария для зоны.*/
    private final static String SUBSYSTEM_PARAM_TYPE_SECURITY = "1";
    /** СКУД подсистема сценария для зоны.*/
    private final static String SUBSYSTEM_PARAM_TYPE_SKUD = "2";
    /** Тип охранной зоны: Обычная.*/
    private final static String SECURITY_REGION_TYPE_DEFAULT = "0";
    /** Тип охранной зоны: С задержкой входа/выхода.*/
    private final static String SECURITY_REGION_TYPE_TIMEOUT = "2";
    /** Тип охранной зоны: Без права снятия.*/
    private final static String SECURITY_REGION_TYPE_NO_OFF = "3";
    /** Тихая тревога у зоны отключена.*/
    private final static String SILENCE_ALARM_PARAM_OFF = "0";
    /** Параметр для desktop версии, которые игнорируем за ненадобностью.*/
    private final static String USELESS_PARAM_1 = "DB$IDZones";
    /** Параметр для desktop версии, которые игнорируем за ненадобностью.*/
    private final static String USELESS_PARAM_2 = "DB$IDParentZones";
    /** Индекс профиля ПК в desctop версии.*/
    private final static String PC_INDEX= "0";
    /** Индекс профиля канала 1 для устройств с deviceCategory = TRANSPORT в desctop версии.*/
    private final static String USB_CHANNEL_1_INDEX = "65";
    /** Индекс профиля канала 2 для устройств с deviceCategory = TRANSPORT в desctop версии.*/
    private final static String USB_CHANNEL_2_INDEX = "66";
    /** Индекс зоны в desktop версии.*/
    private final static String REGION_INDEX = "idx";
    /** Номер зоны в desktop версии.*/
    private final static String REGION_NUMBER = "no";
    /** Имя зоны в desktop версии.*/
    private final static String REGION_NAME = "name";
    /** Имя параметра для зон, виртуальных состояний и виртуальных индикаторов.*/
    private final static String PARAM_NAME = "name";
    /** Значение параметра для зон, виртуальных состояний и виртуальных индикаторов.*/
    private final static String PARAM_VALUE = "value";
    /** Имя устройства в тэге DRV.*/
    private final static String DRV_NAME ="name";
    /** Id устройства desktop версии в тэге "drv".*/
    private final static String DRV_ID = "id";
    /** Индекс устройства в тэге "drv".*/
    private final static String DRV_IDX = "idx";
    /** Индекс устройства в теге "dev".*/
    private final static String DEVICE_INDEX = "drv";
    /** Адрес устройства в теге "dev".*/
    private final static String DEVICE_ADDRESS = "addr";
    /** Id устройства в desktop версии в теге "dev".*/
    private final static String DEVICE_GUID = "guid";
    /** Название параметра устройства в теге "prop".*/
    private final static String DEVICE_PROPERTIES_NAME = "name";
    /** Значение параметра устройства в теге "prop".*/
    private final static String DEVICE_PROPERTIES_VALUE = "value";
    /** Название свойства устройства, если устройство не используется, выключено.*/
    private final static String NOT_USED = "NotUsed";
    /** Тэг указывающий в какую зону добавлено устройство.*/
    private final static String ADD_DEVICE_TO_REGION_TAG = "inZ";
    /** Значение парметра указывающего в какую зону добавлено устройство в desktop версии.*/
    private final static String ADD_DEVICE_TO_REGION_TAG_VALUE = "idz";
    /** Получениея первого уствойства в списке.*/
    private final static int FIRST_DEVICE = 0;
    /** Получениея первого элемента в node в списке.*/
    private final static int ROOT_ELEMENT = 0;
    /** Зачение FALSE для параметров сущностей.*/
    private final static String FALSE = "0";
    /** Поле указывающие подключеные устройства к Н.С*/
    private final static String DEVICE_CONTAIN = "contain";
    /** Максимальное значение уровня вложености в дереве устрйоств в desktop версии.*/
    private final static int MAX_DEVICE_LEVEL = 5;
    /** Разделитель уровня вложености в дереве устрйоств в desktop версии, нужен для contain (contain="0\0\1\13").*/
    private final static String LEVEL_SEPARATOR = "\\";
    /** Разделитель уровней для полного пути устройства.*/
    private final static String DEVICE_PATH_DEVIDER = ",";
    /** Название параметра, кол-во извещателей для перехода в состояния Пожар-2 для зоны.*/
    private final static String REGION_FIRE_DEVICE_COUNT = "FireDeviceCount";
    /** Тип зоны.*/
    private final static String REGION_TYPE = "ZoneType";
    /** Тип охраной зоны.*/
    private final static String REGION_GUARD_REGION_TYPE = "GuardZoneType";
    /** Название параметра, тихая тревога для зоны.*/
    private final static String REGION_SKIPPED = "Skipped";
    /** Название параметра, входная/выходная задержка для зоны.*/
    private final static String REGION_DELAY = "Delay";
    /** Название параметра, автоперевзятие для зоны.*/
    private final static String REGION_AUTO_SET = "AutoSet";
    /** Префикс в имени свойства устройства, для сво-ва устройства "Конфигурирование".*/
    private final static String CONFIG_PREFIX = "Config$";
    /** Значение по умолчанию для свойства устройства.*/
    private final static String DEVICE_PROPERTY_DEFAULT_VALUE = "-1";
    /** Номер виртуального состояния.*/
    private final static String VIRTUAL_STATE_NO = "no";
    /** Название виртуального состояния.*/
    private final static String VIRTUAL_STATE_NAME = "name";
    /** Описание виртуального состояния.*/
    private final static String VIRTUAL_STATE_DESCRIPTION = "desc";
    /** Категория состояния для виртуального состояния.*/
    private final static String VIRTUAL_STATE_STATE_CATEGORY_ID= "stateclass";
    /** Сообщение при постановке для виртуального состояния.*/
    private final static String VIRTUAL_STATE_STATE_MESSAGE_ON = "messageSet";
    /** Сообщение при пропадании для виртуального состояния.*/
    private final static String VIRTUAL_STATE_STATE_MESSAGE_OFF = "messageUnset";
    /** Название параметра для указания прибора в виртуальном состояние.*/
    private final static String VIRTUAL_STATE_DESKTOP_STATE_CATEGORY_ID_PARAM_NAME = "AssignedPanels";
    /** Название состояния виртуального состояния , снят с охраны.*/
    private final static String VIRTUAL_STATE_STATE_CATEGORY_ID_DO_NOT_GUARD = "DoNotGuard";
    /** Название состояния виртуального состояния desktop версии, пожар-1.*/
    private final static String VIRTUAL_STATE_STATE_CATEGORY_ID_FIRE_1P = "Fire1p";
    /** Название состояния виртуального состояния desktop версии, снят с охраны.*/
    private final static String VIRTUAL_STATE_STATE_CATEGORY_OFF_GUARD = "OffGuard";
    /** Название состояния виртуального состояния , пожар-1.*/
    private final static String VIRTUAL_STATE_STATE_CATEGORY_FIRE1 = "Fire1";
    /** Id сценария в desktop версии.*/
    private final static String SCENARIO_ID = "id";
    /** Тип сценария.*/
    private final static String SCENARIO_TYPE = "type";
    /** Название параметра сценария, назначение сценария.*/
    private final static String SCENARIO_PURPOSE = "role";
    /** Название параметра сценария, включение сценария.*/
    private final static String SCENARIO_ENABLED = "enabled";
    /** Тип остановки сценария, в desktop версии определяется по 2 параметрам SCENARIO_MANUAL_START_STOP_ALLOWED 
     * и SCENARIO_STOP_TYPE_MANUAL_OR_AUTO в зависимости от сочетания их значнией.
     */
    private final static String SCENARIO_MANUAL_START_STOP_ALLOWED = "enableControl";
    /** Значение для определения типа остановки сценария.*/
    private final static String SCENARIO_STOP_TYPE_MANUAL_OR_AUTO = "manualReset";
    /** Значение для определения типа остановки сценария.*/
    private final static String SCENARIO_STOP_TYPE_BY_LOGIC = "disableAutoOff";
    /** Выключение сценария при их глобальном сбросе.*/
    private final static String SCENARIO_STOP_ON_GLOBAL_RESET = "manualAbort";
    /** Описание сценария.*/
    private final static String SCENARIO_DESCRIPTION = "desc";
    /** Название сценария.*/
    private final static String SCENARIO_NAME = "caption";
    /** Идентификатор приемно-контрольного устройства (актуален и обязателен для исполнительных сценариев).*/
    private final static String SCENARIO_CONTROL_DEVICE_ID = "panel";
    /** Значение параметра "true" в сценарии.*/
    private final static String SCENARIO_PARAM_VALUE_TRUE = "1";
    /** Значение параметра "false" в сценарии.*/
    private final static String SCENARIO_PARAM_VALUE_FALSE = "0";
    /** Тип сценария "Без указания типа".*/
    private final static String SCENARIO_TYPE_UNASSIGNED = "stNone";
    /** Тип сценария "СОУЭ".*/
    private final static String SCENARIO_TYPE_ENGINEERING = "stEngeneering";
    /** Тип сценария "Инженерный".*/
    private final static String SCENARIO_TYPE_ENS = "stSOUE";
    /** Тип сценария "Пажаротушение".*/
    private final static String SCENARIO_TYPE_FIRE_FIGHTING = "stSPT";
    /** Назначение сценария "Управляющий сценарий".*/
    private final static String SCENARIO_PURPOSE_EXEC_BY_LOGIC = "srControlExt";
    /** Назначение сценария "Исполнительный сценарий".*/
    private final static String SCENARIO_PURPOSE_EXEC_BY_INVOKE = "srExecutive";
    /** Назначение сценария "Спецальный сценарий".*/
    private final static String SCENARIO_PURPOSE_EXEC_BY_TACTICS = "srSpecial";
    /** Парамтер сценария "включен".*/
    private final static int SCENARIO_PARAM_ENABLED_TRUE = -1;
    /** Рутовый тэг сценариев, для всех сценариев.*/
    private final static String SCENARIES_TAG = "scenaries";
    /** Тэг сценария.*/
    private final static String SCENARIO_TAG = "scenario";
    /** Рутовый тэг для исполнительных блоков.*/
    private final static String SCENARIO_EXECUTION_TAG = "execution";
    /** Тэг исполнительного блока.*/
    private final static String SCENARIO_BLOCK_TAG = "block";
    /** Тэг списка действий в исполнительном блоке.*/
    private final static String SCENARIO_EXECUTION_LIST_TAG = "exeList";
    /** Тэг действия в исполнительном блоке*/
    private final static String SCENARIO_ITEM_TAG = "item";
    /** Тип действия в action.*/
    private final static String SCENARIO_BLOCK_ACTION_TYPE = "action";
    /** Id Устройства desctop версии, в action исполнительного блока сценария.*/
    private final static String SCENARIO_BLOCK_ACTION_DEVICE = "uid";
    /** Id Сценария desctop версии, в action исполнительного блока сценария.*/
    private final static String SCENARIO_BLOCK_ACTION_SCENARIO = "scenarioID";
    /** Id Виртуального состояния desktop версии, в action исполнительного блока сценария.*/
    private final static String SCENARIO_BLOCK_ACTION_VIRTUAL_STATE = "customStateID";
    /** Id зоны desktop версии, в action исполнительного блока сценария.*/
    private final static String SCENARIO_BLOCK_ACTION_REGION = "zoneID";
    /** Рутовый тэг список действий на ПК  в сценарие.*/
    private final static String SCENARIO_COMPUTER_ACTION_LIST_TAG = "pcexeList";
    /** Тэг действие на ПК в сценария.*/
    private final static String SCENARIO_COMPUTER_ACTION_ITEM_TAG = "pcitem";
    /** Тэг включение логики в сценария.*/
    private final static String SCENARIO_START_LOGIC_TAG = "condition";
    /** Тэг выключение логики в сценария.*/
    private final static String SCENARIO_STOP_LOGIC_TAG = "offcondition";
    /** Название задержки в исполнительном блоке сценария.*/
    private final static String SCENARIO_BLOCK_TIME_DELAY_SEC = "delay";
    /** Тип проверки условия в исполнительном блоке. */
    private final static String SCENARIO_BLOCK_CONDITION_TYPE = "type";
    /** Тип условия в исполнительном блоке сценария.*/
    private final static String SCENARIO_BLOCK_CONDITION_CHECK_TYPE = "preset";
    /** Тип проверки "Устройство" в исполнительном блоке сценария.*/
    private final static String SCENARIO_BLOCK_CONDITION_CHECK_TYPE_DEVICE = "device";
    /** Тип проверки "Виртуальное состояние" в исполнительном блоке сценария.*/
    private final static String SCENARIO_BLOCK_CONDITION_CHECK_TYPE_VIRTUAL_STATE = "customState";
    /** Тип состояние в исполнительном блоке с условием.*/
    private final static String SCENARIO_BLOCK_CONDITION_DEVICE_STATE_ID = "stateID";
    /** Сообщение в исполнительном блоке, действия компьютера.*/
    private final static String SCENARIO_BLOCK_MESSAGE = "msg";
    /** Заголовок в исполнительном блоке, действия компьютера.*/
    private final static String SCENARIO_BLOCK_TITLE = "msgCaption";
    /** Значение (Время) в обратном отсчете действия компьютера,в исполнительном блоке.*/
    private final static String SCENARIO_BLOCK_COUNT_DOWN_TIME_SEC= "countdowndValue";
    /** Заголовок в обратном отсчете действия компьютера,в исполнительном блоке.*/
    private final static String SCENARIO_BLOCK_COUNT_DOWN_TITLE = "countdowndCaption";
    /** Тип блока "Исполнение" в исполнительном блоке сценария.*/
    private final static String SCENARIO_TIME_LINE_BLOCK_TYPE_EXECUTIVE = "btExe";
    /** Тип блока "Слежение" в исполнительном блоке сценария.*/
    private final static String SCENARIO_TIME_LINE_BLOCK_TYPE_TRACING = "btTracking";
    /** Тип блока "Действие на ПК" в исполнительном блоке сценария.*/
    private final static String SCENARIO_TIME_LINE_BLOCK_TYPE_COMPUTER_ACTION = "btPCExe";
    /** Тип блока "Остановка слежения" в исполнительном блоке сценария.*/
    private final static String SCENARIO_TIME_LINE_BLOCK_TYPE_TRACING_STOP_ACTION = "btStopTracking";
    /** Исполнительный блок с условием в исполнительном блоке сценария.*/
    private final static String SCENARIO_TIME_LINE_BLOCK_TYPE_HAVING_TRACING_BLOCK = "btIf";
    /** Тип действия "Вкл." для устройства в action спецального сценария.*/
    private final static String SCENARIO_TIME_LINE_ITEM_ACTION_TACTICS_PURPOSE_EXEC_DEVICE_ON = "satOnTacticsPurposeExecDeviceOn";
    /** Тип действия "Вкл." для устройства в action в desktop версии сценария.*/
    private final static String ACTION_EXEC_DEVICE_ON = "0";
    /** Значение "Показать сообщение" в действия компьютера,в исполнительном блоке сценария.*/
    private final static String SHOW_MESSAGE_TYPE = "0";
    /** Значение "Обратный отсчет" в действия компьютера,в исполнительном блоке сценария.*/
    private final static String COUNTDOWN_TYPE = "15";
    /** Тип блока в сценарии.*/
    private final static String SCENARIO_BLOCK_TYPE = "type";
    /** Значение тип поля "устройство" исполнительного блока сценария.*/
    private final static String SCENARIO_CONDITION_DEVICE_TYPE = "1";
    /** Значение тип поля "виртуальное устройство" исполнительного блока сценария.*/
    private final static String SCENARIO_CONDITION_VIRTUAL_STATE_TYPE = "3";
    /** Тип проверки "Проверка на активность" в исполнительном блоке сценария.*/
    private final static String SCENARIO_CONDITION_ACTIVE_TYPE = "1";
    /** Тип проверки "Проверка на не активность"  в исполнительном блоке сценария.*/
    private final static String SCENARIO_CONDITION_INACTIVE_TYPE = "0";
    //** Тэг вложения в блоке логики сценария.*/
    private final static String ITEM_TAG = "item";
    //** Тэг вложения только с заполненой логикой  в блоке логики сценария.*/
    private final static String CLAUSE_TAG = "clause";
    /** Логика по "И" в блоке логике сценария.*/
    private final static String LOGIC_TYPE_AND = "0";
    /** Логика по "ИЛИ" в блоке логике сценария*/
    private final static String LOGIC_TYPE_OR = "1";
    /** Логика по "ИЛИ (НЕ)" в блоке логике сценария*/
    private final static String LOGIC_TYPE_OR_NOT = "3";
    /** Логика по "И (НЕ)" в блоке логике сценария*/
    private final static String LOGIC_TYPE_AND_NOT = "2";
    /** Тип события в блоке логики сценария.*/
    private final static String ITEM_TRIGGER_TYPE_ID = "state";
    /** Тип логики в блоке логики для тэга Clause и Logic. сценария*/
    private final static String ITEM_OPERATOR = "operator";
    /** Тип логики в блоке логики, для тэга item, тип логики
     *  для  тэга item определяется по значениям OPERATOR и AGREGATE в desktop версии.*/
    private final static String ITEM_AGREGATE = "agregate";
    /** Значение типа логики "если"  в блоке логике сценария.*/
    private final static String ITEM_OPERATOR_IF = "0";
    /** Значение типа логики "если не" в блоке логике сценария. */
    private final static String ITEM_OPERATOR_IF_NOT = "1";
    /** Значение типа логики "или" в блоке логике сценария.*/
    private final static String ITEM_AGREGATE_OR = "or";
    /** Значение типа логики "и" в блоке логике сценария.*/
    private final static String ITEM_AGREGATE_AND = "and";
    /** Тип сущности "Устройство" в блоке логике сценария.*/
    private final static String DEVICE_DESKTOP_ID = "UID";
    /** Тип сущности "Вирутальное состояние" в блоке логике сценария.*/
    private final static String VIRTUAL_STATE_DESKTOP_ID = "ID";
    /** Тэг устройсва в блоке логики сценария.*/
    private final static String LOGIC_DEVICE_TAG = "device";
    /** Тэг группы индикаторов.*/
    private final static String INDICATOR_PANEL_GROUP_TAG = "IndList";
    /** Тэг панели индикаторов.*/
    private final static String INDICATOR_PANEL_TAG = "Page";
    /** Тэг в котором нахоядтся все индикаторы.*/
    private final static String CONTENT_TAG = "Content";
    /** Название группы панелей индикаторов.*/
    private final static String INDICATOR_PANEL_GROUP_NAME = "name";
    /** Описание группы панелей индикаторов.*/
    private final static String INDICATOR_PANEL_GROUP_DESCRIPTION = "desc";
    /** Номер(Id в desktop) группы панелей индикаторов.*/
    private final static String INDICATOR_PANEL_GROUP_NO = "no";
    /** Номер(Id в desktop) панели индикатора.*/
    private final static String INDICATOR_PANEL_NO = "no";
    /** Название панели индикатора.*/
    private final static String INDICATOR_PANEL_NAME = "name";
    /** Описание панели индикатора.*/
    private final static String INDICATOR_PANEL_DESCTIPTION = "desc";
    /** Кол-во строк в панели индикатора.*/
    private final static String INDICATOR_PANEL_PARAM_ROW_COUNT = "RowCount";
    /** Кол-во столбцо в панели индикатора.*/
    private final static String INDICATOR_PANEL_PARAM_COL_COUNT = "ColCount";
    /** Имя панели индикаторов по умолчанию.*/
    private final static String IDICATOR_PANEL_DEFAULT_NAME = "Новая панель индикаторов ";
    /** Имя группы панелей индикаторов по умолчанию.*/
    private final static String IDICATOR_PANEL_GROUP_DEFAULT_NAME = "Новая группа панелей ";
    /** Экранирование "]]" в CDATA для Delphi.*/
    private final static String NG_VERSION_ESCAPE_CHARS_IN_CDATA = "]]";
    /** Замена символов "]]" в CDATA.*/
    private final static String REPLACED_CHARS_NG_VERSION_ESCAPE_CHARS_IN_CDATA = "##";
    /** Тэги который пока еще не используются.*/
    private final static String PC_CONTENT_TAG = "pcContent"; // задел на будущее
    private final static String SURFACES_TAG = "surfaces";
    
    @Autowired
    private DeviceProfileRepository deviceProfileRepository;
    
    @Autowired
    private Logger logger;
    
    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;
    
    @Autowired
    private DeviceViewRepository deviceViewRepository;
    
    @Autowired
    private DeviceService deviceService;
    
    @Autowired
    private DeviceViewService deviceViewService;
    
    @Autowired
    private DeviceRepository deviceRepository;
    
    @Autowired
    private RegionViewRepository regionViewRepository;
    
    @Autowired
    private RegionRepository regionRepository;
    
    @Autowired
    private DeviceProfileViewRepository deviceProfileViewRepository;
    
    @Autowired
    private RegionService regionService;
    
    @Autowired
    private VirtualStateService virtualStateService;
    
    @Autowired
    private VirtualStateRepository virtualStateRepository;
    
    @Autowired
    private ScenarioViewService scenarioViewService;
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private ScenarioRepository scenarioRepository;
    
    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;
    
    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;
    
    @Autowired
    private StateRepository stateRepository;
    
    @Autowired
    private IndicatorPanelGroupService indicatorPanelGroupService;
    
    @Autowired
    private IndicatorPanelService indicatorPanelService;
    
    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;
    
    @Autowired
    private IndicatorXMLParsingHandler indicatorXMLParsingHandler;
    
    /** Название свойства устройства.*/
    private String devicePropertyName;
    /** Значение свойства устройства.*/
    private String devicePropertyValue;
    private StringBuilder errorMessage;
    /** Id проекта.*/
    private String projectId = null;
    /** Не используемый тэг.*/
    private boolean isUselessTag;
    /** Исполнительный блок находится в блоке, для блока слежения.*/
    private boolean blockInBlock;
    /** ID виртуальных состояних текущей и desktop версии.*/
    private Map<String, String> virtualStateNewIdByDesktopNo;
    /** ID устройства текущей и desktop версии.*/
    private Map<String, String> deviceNewIdByDesktopId;
    /** ID зоны текущей и desktop версии.*/
    private  Map<String, String> regionNewIdByDesktopId;
    /** ID и Index desktop версии.*/
    private Map<String, String> importedDeviceIdByIdx;
    /** Название и UID устройства desktop версии.*/
    private Map<String, String> importedDeviceNameById;
    /** Название и значение свойств "конфигурирование" устройства.*/
    private Map<String, String> importedDeviceConfigPropertyValueByName;
    /** Название и значение свойств текущего устрйоства.*/
    private Map<String, String> currentDevicePropertyValueByName;
    /** ID сценария текущей и desktop версии.*/
    private Map<String, String> scenarioNewIdByDesktopId;
    /** ID и index зоны desktop версии.*/
    private Map<String, String> regionIdbyIdx;
    /** Текущее значение индекса региона для desktop версии.*/
    private String currentRegionIdx;
    /** Список добавленых устройств по профилю устройства desktop версии.*/
    private List<Device> addedDevice;
    /** Храним имя и значение свойства "Конфигурирование" устройства.*/
    private Map<String, DeviceConfigProperty> deviceConfigPropertyValueByName;
    /** Храним имя и значение свойства "прочие" устройства.*/
    private Map<String, DeviceProperty> devicePropertyValueByName;
    /** Текущая зона.*/
    private Region currentRegion;
    /** Сущность использумоемая для создания текущего устройства.*/
    private DeviceCreateView deviceCreateView;
    /** Текущее устройство.*/
    private Device currentDevice;
    /** Текущее виртуальное состояние.*/
    private VirtualState currentVirtualState;
    /** Текущее действие в исполнительном блоке.*/
    private ScenarioTimeLineBlock.Action currentScenarioTimeLineBlockAction;
    /** Пропустить свойвства устройства.*/
    private boolean passDeviceProperty;
    /** Примененить свойства устройства.*/
    private boolean applyDevicePropertySucceded;
    /** Id устрйоства desktop версии.*/
    private String deviceDesktopId;
    /** Id текущей зоны desktop версии.*/
    private String currentRegionDesktopId;
    /** Id родителя устройства  с deviceCategory = Transport.*/
    private String transportParentId;
    /** Id родителя устройства.*/
    private String parentDeviceId;
    /** Количество внутрених устройств.*/
    private int innerDeviceNumber;
    /** Счетчик внутрених устройств.*/
    private int innerDeviceCounter;
    /** Значение свойства устройства.*/
    private String propertyValue;
    /** Устройство имеет тип deviceCategory = Control.*/
    private boolean isControl;
    /** Профиль устройства.*/
    private DeviceProfile deviceProfile;
    /** Счетчик уровней устрйоства.*/
    private int deviceLevelCounter;
    /** Колчиство устройств первого уровня для desktop версии.*/
    private int firstLevelDeviceQuantity;
    /** Колчиство устройств второго уровня для desktop версии.*/
    private int secondLevelDeviceQuantity;
    /** Колчиство устройств третьего уровня для desktop версии.*/
    private int thirdLevelDeviceQuantity;
    /** Колчиство устройств четвертого уровня для desktop версии.*/
    private int fourthLevelDeviceQuantity;
    /** Колчиство устройств пятого уровня для desktop версии.*/
    private int fifthLevelDeviceQuantity;
    /** Предыдущий тэг был устройство.*/
    private boolean isPreviosDeviceTag;
    /** Текущий тэг устройство.*/
    private boolean isCurrentDeviceTag;
    /** Путь устройтва и его профиль в desktop версии.*/
    private Map<String, String> devicePathByGUID;
    /** Путь устройства которые подключены к НС и профиль устройства в desktop версии.*/
    private Map<String, String> deviceWithDeviceLinksPathByGUID;
    /** Путь устройства.*/
    private String devicePath;
    /** Список зон у которых индекс зоны меньше 1.*/
    private List<Region> regionIndexLessOne;
    /** Список индексов зон desktop версии у которых индекс зоны меньше 1.*/
    private List<String> regionIdxIndexLessOne;
    /** Сохранили зону у которой  индекс меньше нуля.*/
    private boolean isRegionIndexLessOneSaved;
    /** Сгенерировали индекс зоны.*/
    private boolean isRegionIndexGenerated;
    /** Список добавленых виртуальных состояний.*/
    private List<VirtualState> importedVirtualState;
    /** Счетчик канала, для устройств с deviceCategory = Transport.*/
    private int channelCounter;
    /** Текущий сценарий.*/
    private Scenario currentScenario;
    /** Список действий в исполнительном блоке сценария.*/
    private List<ScenarioTimeLineBlock.Action> scenarioTimeLineBlockActions;
    /** Список действий для ПК действий в исполнительном блоке сценария.*/
    private List<ScenarioTimeLineBlock.ComputerAction> scenarioTimeLineBlockComputerActions;
    /** Текущий исполнительный блок сценария.*/
    private ScenarioTimeLineBlock currentScenarioTimeLineBlock;
    /** Список исполнительных блоков сценария.*/
    private List<ScenarioTimeLineBlock> timeLineBlocks;
    /** Действие на ПК в исполнительном блоке сценария.*/
    private ScenarioTimeLineBlock.ComputerAction computerAction;
    /** Предыдущий блок был блоком.*/
    private boolean isPreviousBlockWasBlock;
    /** Пропустить сценарий.*/
    private boolean passScenario;
    /** Список сущностей в блоке сценария.*/
    private List<String> itemEntityIds;
    /** Блок логике сценария.*/
    private ScenarioLogicBlock scenarioLogicBlock;
    /** Текущий блок логики сценария.*/
    private ScenarioLogicBlock currentScenarioLogicBlock;
    /** Предыдущий блок в логики сценария.*/
    private ScenarioLogicBlock previosScenarioLogicBlock;
    /** Рутовый блок в логике сценария.*/
    private ScenarioLogicBlock rootScenarioLogicBlock;
    /** Список вложеной логи у блока логики выше увроня.*/
    private List<ScenarioLogicBlock> previosLevelsubLogicList;
    /** Храним уровень логики и Id элемента родителя.*/
    private Map<Integer, ScenarioLogicBlock> parentElementEntityBylogiclevelCounter;
    /** Счетчик уровня логики сценария.*/
    private int logicLevelCounter;
    /** Необходимо перепроверить сценарий.*/
    private boolean recheckScenario;
    /** Текущая группу панелей индикаторов.*/
    private IndicatorPanelGroup currentIndicatorPanelGroup;
    /** Текущая панель индикаторов.*/
    private IndicatorPanel currentIndicatorPanel;
    /** внутри тэга Content.*/
    private boolean isContent = false;
    /** внутри блока логике.*/
    private boolean isScenarioLogicBlock = false;
    /** Номер группы панелей индикатора(Id в desktop) и Id группы панелей индикатора.*/
    private static Map<String, String> indicatorPageGroupNewIdByDesktopNo;
    /** Паметром панели индикатора.*/
    private boolean isPenelIndicatorParametr = false;
    /** Пропустить применение свойств панели индикаторов.*/
    private boolean passIndicatorPanel = false;
    /** Текущие данные для перепроверки сценария.*/
    private RecheckScenarioData currentRecheckScenarioData;
    /** Данные  для перепроверки сценариев */
    private static class RecheckScenarioData {
        /* Id сценария в котором необходимо перепроверить сценарии в исполнительном блоке.*/
        private String scenarioId;
        /* Номер action в блоке, так как необходимо соблюсти порядок заполнения action в блоке.*/
        private int actionNumber;
        /* Номер блока сценария.*/
        private int blockNumber;
        /* Имя типа action блоке в desktop версии. */
        private String entityType;
        /* Идентификатор сущности action который необходимо перепроверить в блоке.*/
        private String entityId;
        /* Идентификатор типа действия.*/
        private String actionType;
        
        public RecheckScenarioData(String entityType, String actionType, String entityId, int actionNumber, int blockNumber) {
             this.entityType = entityType;
             this.actionType = actionType;
             this.entityId = entityId;
             this.actionNumber = actionNumber;
             this.blockNumber = blockNumber;
        }

        public String getScenarioId() {
            return scenarioId;
        }

        public void setScenarioId(String scenarioId) {
             this.scenarioId = scenarioId;
        }

        public int getActionNumber() {
            return actionNumber;
        }

        public int getBlockNumber() {
            return blockNumber;
        }

        public String getEntityType() {
            return entityType;
        }

        public String getEntityId() {
            return entityId;
        }

        public String getActionType() {
            return actionType;
        }
    }
    /** Счетчик действий в блоке сценария.*/
    private int actionCounter;
    /** Счетчик блоков в сценарии.*/
    private int blockCounter;
    /** Список данных для перепроверки сценария в действия исполнительного блока.*/
    private List<ImportedProjectXMLParsingHandler.RecheckScenarioData> recheckScenarioDataListInAction;
    /** Список данных для перепроверки сценария исполнительного блока.*/
    private List<ImportedProjectXMLParsingHandler.RecheckScenarioData> recheckScenarioDataListInBlock;
    /** Список данных для перепроверки сценария в сценарии.*/
    private List<ImportedProjectXMLParsingHandler.RecheckScenarioData> recheckScenarioDataListInScenario;
    /** Список предупреждений (ошибок) импорта проекта.*/
    private List<ImportValidateMessage> warningMessages;
    /** Список предупреждений (ошибок) импорта проекта в исполнительном блоке.*/
    private List<ImportValidateMessage> warningMessagesInTimeLineBlock;
    
    public ImportedProjectXMLParsingHandler() {
    }
    
    @Override 
    public void startDocument() throws SAXException {
        errorMessage = new StringBuilder();
        virtualStateNewIdByDesktopNo = new HashMap<>();
        regionNewIdByDesktopId = new HashMap<>();
        deviceWithDeviceLinksPathByGUID = new HashMap<>();
        devicePathByGUID = new HashMap<>();
        deviceNewIdByDesktopId = new HashMap<>();
        regionIdbyIdx = new HashMap<>();
        importedDeviceIdByIdx = new HashMap<>();
        importedDeviceNameById = new HashMap<>();
        regionIndexLessOne = new ArrayList<>();
        regionIdxIndexLessOne = new ArrayList<>();
        importedVirtualState = new ArrayList<>();
        scenarioNewIdByDesktopId = new HashMap<>();
        indicatorPageGroupNewIdByDesktopNo = new HashMap<>();
        timeLineBlocks = new ArrayList<>();
        warningMessages = new ArrayList<>();
        firstLevelDeviceQuantity = -1;
        secondLevelDeviceQuantity = -1;
        thirdLevelDeviceQuantity = -1;
        fourthLevelDeviceQuantity = -1;
        fifthLevelDeviceQuantity = -1;
        deviceLevelCounter = -1;
        innerDeviceNumber = 0;
        innerDeviceCounter = 0;
        isRegionIndexLessOneSaved = false;
        isRegionIndexGenerated = false;
        passDeviceProperty = true;
        parentDeviceId = null;
        transportParentId = null;
        isPreviosDeviceTag = true;
        isCurrentDeviceTag = false;
        isControl = false;
        isUselessTag = false;
        channelCounter = 0;
        isPreviousBlockWasBlock = false;
        blockInBlock = false;
        passScenario = false;
        logicLevelCounter = 0;
        recheckScenario = false;
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        
        if (!isUselessTag) {
            /*
             * Для применения свойств в исполнительном блоке при заданых условиях в исполнительном блоке сценария,
             *  используем эту проверку
             */
            if (isPreviousBlockWasBlock && qName.equals(SCENARIO_START_LOGIC_TAG)) { 
                applyScenarioTimeLineBlockCondtionProperty(atts.getValue(SCENARIO_BLOCK_CONDITION_TYPE),
                        atts.getValue(SCENARIO_BLOCK_CONDITION_CHECK_TYPE),
                        atts.getValue(SCENARIO_BLOCK_CONDITION_CHECK_TYPE_DEVICE),
                        atts.getValue(SCENARIO_BLOCK_CONDITION_CHECK_TYPE_VIRTUAL_STATE),
                        atts.getValue(SCENARIO_BLOCK_CONDITION_DEVICE_STATE_ID));
            } else if (qName.equals(SCENARIO_BLOCK_TAG)) {
                isPreviousBlockWasBlock = true;
            } else {
                isPreviousBlockWasBlock = false;
            }
            switch (qName) {
            case DRV_TAG:
                importedDeviceIdByIdx.put(atts.getValue(DRV_IDX), atts.getValue(DRV_ID));
                importedDeviceNameById.put(atts.getValue(DRV_IDX), atts.getValue(DRV_NAME));
                break;
            case DATABASE_INFO_TAG:
                isUselessTag = true;
                break;
            case REGION_TAG:
                applyRegionBaseProperty(atts.getValue(REGION_INDEX), atts.getValue(REGION_NAME)
                        , atts.getValue(REGION_NUMBER), atts.getValue(DESCRIPTION));
                break;
            case PARAMETRS_TAG:
                if(atts.getValue(PARAM_NAME) != null && atts.getValue(PARAM_VALUE) != null) {
                    if(isPenelIndicatorParametr) {
                        applyIndicatorPanelParametr(atts.getValue(PARAM_NAME), atts.getValue(PARAM_VALUE));
                    }else if(atts.getValue(PARAM_NAME).equals(VIRTUAL_STATE_DESKTOP_STATE_CATEGORY_ID_PARAM_NAME)) {
                        /* в deviceId добавляем desktopDeviceId чтобы потом сравнить его
                         *  и если он совпадает заменять на актуальный deviceId
                         */
                        currentVirtualState.setDeviceId(atts.getValue(PARAM_VALUE));
                    }else {
                        applyRegionParameter(atts.getValue(PARAM_NAME), atts.getValue(PARAM_VALUE));
                    }
                }
                break;
            case DEVICE_TAG:
                if(!isRegionIndexLessOneSaved) {
                    for(int i = 0; i < regionIndexLessOne.size(); i++) {
                        if(currentRegion != null && regionIndexLessOne.get(i).getIndex() < 1) {
                            currentRegionIdx = regionIdxIndexLessOne.get(i);
                            generateRegionIndex(regionIndexLessOne.get(i));
                        }
                    }
                    regionIndexLessOne = null;
                    regionIdxIndexLessOne = null;
                    isRegionIndexLessOneSaved = true;
                }
                isCurrentDeviceTag = true;
                if(isPreviosDeviceTag && isCurrentDeviceTag) {
                    if(deviceLevelCounter == 1) {
                        firstLevelDeviceQuantity++;
                        secondLevelDeviceQuantity = -1;
                        thirdLevelDeviceQuantity = -1;
                        fourthLevelDeviceQuantity = -1;
                        fifthLevelDeviceQuantity = -1;
                        channelCounter = 0;
                    }else if(deviceLevelCounter == 2) {
                        secondLevelDeviceQuantity++;
                    }else if(deviceLevelCounter == 3) {
                        thirdLevelDeviceQuantity++;
                    }else if(deviceLevelCounter == 4) {
                        fourthLevelDeviceQuantity++;
                    }
                    deviceLevelCounter++;
                    if(deviceLevelCounter == 3) {
                        channelCounter++;
                    }
                }
                isPreviosDeviceTag = true;
                String deviceIndex = atts.getValue(DEVICE_INDEX);
                if (!(deviceIndex.equals(PC_INDEX) || deviceIndex.equals(USB_CHANNEL_1_INDEX) || deviceIndex.equals(USB_CHANNEL_2_INDEX))) {
                    passDeviceProperty = false;
                    if (!createDeviceView(deviceIndex, atts.getValue(DEVICE_ADDRESS))) {
                        break;
                    }
                    deviceDesktopId = atts.getValue(DEVICE_GUID);
                    deviceConfigPropertyValueByName = deviceProfile.getConfigProperties();
                    devicePropertyValueByName = deviceProfile.getDeviceProperties();
                    /*
                     *  если innerDeviceNumber > 0 то не добавляем устройство,
                     *   а меняем профиль внутренних устройств если это необходимо
                     */
                    if (innerDeviceNumber > 0) {
                        updateDeviceProfileForInnerDevice();
                    }else {
                        createDevice(atts.getValue(DEVICE_CONTAIN), atts.getValue(DEVICE_INDEX));
                    }
                    deviceNewIdByDesktopId.put(deviceDesktopId, currentDevice.getId());
                    if (currentDevice.getDeviceCategory() == DeviceCategory.CONTROL) {
                        addVirtualState();
                    }
                }
                break;
            case DEVICE_PROPERTIES_TAG:
                devicePropertyName = atts.getValue(DEVICE_PROPERTIES_NAME);
                devicePropertyValue = atts.getValue(DEVICE_PROPERTIES_VALUE);
                if (!passDeviceProperty && !(devicePropertyValue == null)) {
                    applyDevicePropertySucceded = false;
                    if(devicePropertyName.equals(NOT_USED)) {
                        if(devicePropertyValue.equals(FALSE)) {
                            currentDevice.setDisabled(false);
                        }else {
                            currentDevice.setDisabled(true);
                        }
                        currentDevice = deviceRepository.save(currentDevice);
                        break;
                    }
                    if(applyDeviceProperty()) {
                        break;
                    }
                    applyDeviceConfigProperty();
                }
                break;
            case ADD_DEVICE_TO_REGION_TAG:
                    if(!passDeviceProperty) {
                        String addDeviceToRegionValue = atts.getValue(ADD_DEVICE_TO_REGION_TAG_VALUE);
                        DeviceView deviceView = deviceViewRepository.findOne(currentDevice.getId());
                        String currentDeviceId = currentDevice.getId();
                        if(regionIdbyIdx.containsKey(addDeviceToRegionValue)) {
                            currentDevice = regionService.addDeviceRegionLink(currentDevice.getId(),regionIdbyIdx.get(addDeviceToRegionValue), projectId, errorMessage);
                            if(currentDevice != null) {
                                deviceRepository.save(currentDevice);
                            }else {
                                String description  = "Устройство \"".concat(deviceView.getName()).concat(" , ").concat(deviceView.getMiddleAddressPath()
                                        .concat("\" не было добавлено в зону"));
                                warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_NOT_ADDED_IN_ZONE, description, EntityType.DEVICE, currentDeviceId));
                            }
                        }else {
                            String description  = "Устройство \"".concat(deviceView.getName()).concat("\" , ").concat(deviceView.getMiddleAddressPath()
                                    .concat(" не было добавлено в зону"));
                            warningMessages.add(new ImportValidateMessage(Level.WARNING, Code.DEVICE_NOT_ADDED_IN_ZONE, description, EntityType.DEVICE, currentDeviceId));
                        }
                    }
            break;
            case VIRTUAL_STATE_TAG:
                applyVirtualStateBaseProperty(atts.getValue(VIRTUAL_STATE_NO), atts.getValue(VIRTUAL_STATE_NAME), atts.getValue(VIRTUAL_STATE_DESCRIPTION),
                        atts.getValue(VIRTUAL_STATE_STATE_CATEGORY_ID), atts.getValue(VIRTUAL_STATE_STATE_MESSAGE_ON), atts.getValue(VIRTUAL_STATE_STATE_MESSAGE_OFF));
                break;
            case SCENARIO_COMPUTER_ACTION_LIST_TAG:
                scenarioTimeLineBlockComputerActions = new ArrayList<>();
                break;
            case SCENARIO_COMPUTER_ACTION_ITEM_TAG:
                if (currentScenarioTimeLineBlock.getBlockType() == BlockType.COMPUTER_ACTION
                        && atts.getValue(SCENARIO_BLOCK_ACTION_TYPE) != null) {
                    applyScenarionTimeLineBlockActionComputerActionItem(atts.getValue(SCENARIO_BLOCK_ACTION_TYPE),
                            atts.getValue(SCENARIO_BLOCK_MESSAGE), atts.getValue(SCENARIO_BLOCK_TITLE), atts.getValue(SCENARIO_BLOCK_COUNT_DOWN_TIME_SEC), atts.getValue(SCENARIO_BLOCK_COUNT_DOWN_TITLE));
                }
                break;
            case SCENARIES_TAG:
                recheckScenarioDataListInScenario = new ArrayList<RecheckScenarioData>();
                break;
            case SCENARIO_TAG:
                applyScenarioProperty(atts.getValue(SCENARIO_ID), atts.getValue(SCENARIO_NAME),atts.getValue(SCENARIO_DESCRIPTION), atts.getValue(SCENARIO_ENABLED),
                        atts.getValue(SCENARIO_CONTROL_DEVICE_ID), atts.getValue(SCENARIO_PURPOSE), atts.getValue(SCENARIO_TYPE),
                        atts.getValue(SCENARIO_STOP_TYPE_MANUAL_OR_AUTO), atts.getValue(SCENARIO_STOP_TYPE_BY_LOGIC),atts.getValue(SCENARIO_MANUAL_START_STOP_ALLOWED), atts.getValue(SCENARIO_STOP_ON_GLOBAL_RESET));
                break;
            case SCENARIO_EXECUTION_TAG:
                if(!passScenario) {
                    recheckScenarioDataListInBlock = new ArrayList<RecheckScenarioData>();
                    blockCounter = 0;
                }
                break;
            case SCENARIO_BLOCK_TAG:
                if(!passScenario) {
                    applyScenarioTimeLineBlockProperty(atts.getValue(SCENARIO_BLOCK_TIME_DELAY_SEC), atts.getValue(SCENARIO_BLOCK_TYPE));
                }
                break;
            case SCENARIO_EXECUTION_LIST_TAG:
                if(!passScenario) {
                    scenarioTimeLineBlockActions = new ArrayList<>();
                    recheckScenarioDataListInAction = new ArrayList<>();
                    warningMessagesInTimeLineBlock = new ArrayList<>();
                    actionCounter = 0;
                }
                break;
            case SCENARIO_ITEM_TAG:
                if(!passScenario) {
                    String itemType = atts.getQName(0);
                    String actionType = atts.getValue(SCENARIO_BLOCK_ACTION_TYPE);
                    String itemValue = atts.getValue(0);
                    if(currentScenarioTimeLineBlock.getBlockType() == BlockType.EXECUTIVE && itemValue != null) {
                        applyScenarionTimeLineBlockActionExecutiveItem(itemType, actionType, itemValue);
                    }else if(currentScenarioTimeLineBlock.getBlockType() == BlockType.TRACING && itemValue != null) {
                        applyScenarioTimeLineBlockActionExecutiveTrackingItem(itemType, itemValue);
                    }
                    if(currentScenarioTimeLineBlockAction != null) {
                        actionCounter++;
                    }
                }
                break;
            
            case SCENARIO_START_LOGIC_TAG:
                if(!passScenario) {
                    isScenarioLogicBlock = true;
                }
                break;
            case SCENARIO_STOP_LOGIC_TAG:
                if(!passScenario) {
                    isScenarioLogicBlock = true;
                }
                break;
            case INDICATOR_PANEL_GROUP_TAG:
                String indicatorPanelGroupNo = atts.getValue(INDICATOR_PANEL_GROUP_NO);
                String indicatorPanelGroupName = atts.getValue(INDICATOR_PANEL_GROUP_NAME);
                String indicatorPanelGroupDescription = atts.getValue(INDICATOR_PANEL_GROUP_DESCRIPTION);
                if(indicatorPanelGroupNo != null) { 
                    currentIndicatorPanelGroup = new IndicatorPanelGroup();
                    if(indicatorPanelGroupName != null && !indicatorPanelGroupName.isEmpty()) {
                        currentIndicatorPanelGroup.setName(indicatorPanelGroupName);
                    }else {
                        currentIndicatorPanelGroup.setName(IDICATOR_PANEL_GROUP_DEFAULT_NAME + indicatorPanelGroupNo);
                    }
                    if(indicatorPanelGroupDescription != null && !indicatorPanelGroupDescription.isEmpty()) {
                        currentIndicatorPanelGroup.setDescription(indicatorPanelGroupDescription);
                    }
                    indicatorPanelGroupName = currentIndicatorPanelGroup.getName();
                    currentIndicatorPanelGroup = indicatorPanelGroupService.add(currentIndicatorPanelGroup, projectId, errorMessage);
                    if(currentIndicatorPanelGroup != null) {
                        indicatorPageGroupNewIdByDesktopNo.put(indicatorPanelGroupNo, currentIndicatorPanelGroup.getId());
                    }else {
                        String description = "Группа панелей \"".concat(indicatorPanelGroupName).concat("\" не добавлена");
                        warningMessages.add(new ImportValidateMessage(Level.ERROR,
                                Code.VIRTUAL_INDICATOR_PANEL_GROUP_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL_GROUP, null));
                        passIndicatorPanel = true;
                    }
                }
            break;
            case INDICATOR_PANEL_TAG:
                if(!passIndicatorPanel) {
                    String indicatorPanelName = atts.getValue(INDICATOR_PANEL_NAME);
                    String indicatorPanelNumber = atts.getValue(INDICATOR_PANEL_NO);
                    String indicarorPanelDescription = atts.getValue(INDICATOR_PANEL_DESCTIPTION);
                    if(indicatorPanelNumber != null) {
                        currentIndicatorPanel = new IndicatorPanel();
                        if( indicatorPanelName != null && !indicatorPanelName.isEmpty()) {
                            currentIndicatorPanel.setName(indicatorPanelName);
                        }else {
                            currentIndicatorPanel.setName(IDICATOR_PANEL_DEFAULT_NAME + indicatorPanelNumber);
                        }
                        currentIndicatorPanel.setIndicatorGroupId(currentIndicatorPanelGroup.getId());
                        if(indicarorPanelDescription != null) {
                            currentIndicatorPanel.setDescription(indicarorPanelDescription);
                        }
                        isPenelIndicatorParametr =true;
                    }
                }
                break;
            case CONTENT_TAG:
                if(!passIndicatorPanel && currentIndicatorPanel != null) {
                    String indicatorPanelName = currentIndicatorPanel.getName();
                    currentIndicatorPanel = indicatorPanelService.addNew(currentIndicatorPanel, projectId, new StringBuilder());
                    if(currentIndicatorPanel != null) {
                        isContent = true;
                    }else {
                        String description = "Панель индикаторов \"".concat(indicatorPanelName).concat("\" не добавлена");
                        warningMessages.add(new ImportValidateMessage(Level.ERROR,
                                Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
                        passIndicatorPanel = true;
                    }
                }
            default:
                break;
            }
        }
    }

    @Override 
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        
        if (qName.equals(DATABASE_INFO_TAG)) {
            isUselessTag = false;
        }
        if (!isUselessTag) {
                switch (qName) {
                case REGION_TAG:
                    if(currentRegion != null) {
                        if(currentRegion.getIndex() < 1) {
                            regionIndexLessOne.add(currentRegion);
                        }else {
                            addRegion(currentRegion);
                        }
                    }
                    break;
                case DEVICE_TAG:
                    isCurrentDeviceTag = false;
                    if(!isPreviosDeviceTag && !isCurrentDeviceTag) {
                        if(deviceLevelCounter == 5) {
                            fifthLevelDeviceQuantity=-1;
                        }else if(deviceLevelCounter == 4) {
                            fourthLevelDeviceQuantity=-1;
                        }else if(deviceLevelCounter == 3) {
                            thirdLevelDeviceQuantity = -1;
                        }
                        deviceLevelCounter--;
                    }else if(isPreviosDeviceTag && !isCurrentDeviceTag) {
                       if(deviceLevelCounter == 4) {
                           fourthLevelDeviceQuantity++;
                        }else if(deviceLevelCounter == 5) {
                            fifthLevelDeviceQuantity++;
                        }
                       if(deviceLevelCounter > 3) {
                           createDevicePath();
                       }
                    }
                    isPreviosDeviceTag = false;
                    break;
                case DEVICE_PROPERTIES_TAG:
                    break;
                case SCENARIO_ITEM_TAG:
                    if(!passScenario && currentScenarioTimeLineBlockAction != null) {
                        scenarioTimeLineBlockActions.add(currentScenarioTimeLineBlockAction);
                        if(recheckScenario) {
                            recheckScenario = false;
                        }
                    }
                    break;
                case SCENARIO_BLOCK_TAG:
                    if(!passScenario) {
                        if(currentScenarioTimeLineBlock != null && currentScenarioTimeLineBlock.getBlockType() != null && !blockInBlock) {
                            if(scenarioViewService.validateScenarioTimeLineBlock(currentScenarioTimeLineBlock, new StringBuilder(), currentScenario.getScenarioPurpose(), false)) {
                                timeLineBlocks.add(currentScenarioTimeLineBlock); 
                                blockCounter++;
                                if(recheckScenarioDataListInBlock != null && recheckScenarioDataListInAction != null && !recheckScenarioDataListInAction.isEmpty()) {
                                    recheckScenarioDataListInBlock.addAll(recheckScenarioDataListInAction);
                                }
                                if(warningMessagesInTimeLineBlock != null && !warningMessagesInTimeLineBlock.isEmpty()) {
                                    warningMessages.addAll(warningMessagesInTimeLineBlock);
                                }
                            }else {
                                recheckScenarioDataListInAction = null;
                                String description  = "Исполнительный блок не был добавлен в сценарий \"".concat(currentScenario.getName()).concat("\"");
                                warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.SCENARIO_TIME_LINE_BLOCK_NOT_ADDED, description, EntityType.SCENARIO, currentScenario.getId()));
                            }
                        }
                        blockInBlock = false;
                    }
                    break;
                case SCENARIO_EXECUTION_LIST_TAG:
                    if(!passScenario && scenarioTimeLineBlockActions != null && !scenarioTimeLineBlockActions.isEmpty()) {
                        currentScenarioTimeLineBlock.setActions(scenarioTimeLineBlockActions);
                        actionCounter = -1;
                    }
                    break;
                case SCENARIO_TAG:
                    if (!passScenario) {
                        currentScenario.setTimeLineBlocks(timeLineBlocks);
                        String scenarioName = currentScenario.getName();
                        String scenarioId = currentScenario.getId();
                        currentScenario = scenarioRepository.save(currentScenario);
                        if (currentScenario != null) {
                            for (RecheckScenarioData currentItem : recheckScenarioDataListInBlock) {
                                currentItem.setScenarioId(currentScenario.getId());
                            }
                            recheckScenarioDataListInScenario.addAll(recheckScenarioDataListInBlock);
                        } else {
                            String description  = "Сценарий \"".concat(scenarioName).concat("\"").concat(" не был добавлен");
                            warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.SCENARIO_NOT_ADDED, description, EntityType.SCENARIO, scenarioId));
                        }
                        timeLineBlocks.clear();
                    }else {
                        passScenario = false;
                    }
                    break;
                case SCENARIO_COMPUTER_ACTION_LIST_TAG:
                    if(!passScenario && currentScenarioTimeLineBlock != null)
                    currentScenarioTimeLineBlock.setComputerActions(scenarioTimeLineBlockComputerActions);
                    break;
                case SCENARIO_COMPUTER_ACTION_ITEM_TAG:
                    if(!passScenario && computerAction != null) {
                        scenarioTimeLineBlockComputerActions.add(computerAction);
                    }
                    break;
                case SCENARIO_START_LOGIC_TAG:
                    if (!passScenario) {
                        isScenarioLogicBlock = false;
                        if (rootScenarioLogicBlock != null) {
                            if(currentScenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS) {
                                scenarioLogicBlock = parentElementEntityBylogiclevelCounter.get(0).getSubLogics().get(0);
                            }else {
                                scenarioLogicBlock = parentElementEntityBylogiclevelCounter.get(ROOT_ELEMENT);
                            }
                            if (scenarioLogicBlock != null && !scenarioLogicBlock.isEmpty()) {
                                if(currentScenario.getScenarioPurpose() != ScenarioPurpose.EXEC_BY_INVOKE) {
                                    if (scenarioViewService.validateScenarioLogicBlock(projectId, scenarioLogicBlock,
                                            currentScenario.getScenarioPurpose(), new StringBuilder())) {
                                        currentScenario.setStartLogic(scenarioLogicBlock);
                                    } else {
                                        scenarioLogicBlock = null;
                                        String description  = "Логика запуска не прошла валидацию";
                                        warningMessages.add(new ImportValidateMessage(Level.ERROR,Code.SCENARIO_START_LOGIC_ERROR, description, EntityType.SCENARIO, currentScenario.getId()));
                                    }
                                }
                            }
                        }
                    }
                    break;
                case SCENARIO_STOP_LOGIC_TAG:
                    if (!passScenario) {
                        isScenarioLogicBlock = false;
                        if (rootScenarioLogicBlock != null) {
                            scenarioLogicBlock = parentElementEntityBylogiclevelCounter.get(ROOT_ELEMENT);
                            if (scenarioLogicBlock != null && !scenarioLogicBlock.isEmpty()) {
                                if(currentScenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_LOGIC) {
                                    if (scenarioViewService.validateScenarioLogicBlock(projectId, scenarioLogicBlock,
                                            currentScenario.getScenarioPurpose(), new StringBuilder())) {
                                        currentScenario.setStartLogic(scenarioLogicBlock);
                                    } else {
                                        scenarioLogicBlock = null;
                                        String description  = "Логика остановки не прошла валидацию";
                                        warningMessages.add(new ImportValidateMessage(Level.ERROR,Code.SCENARIO_START_LOGIC_ERROR, description, EntityType.SCENARIO, currentScenario.getId()));
                                    }
                                }
                            }
                        }
                    }
                    break;
                case SCENARIES_TAG:
                    recheckScenarioAdditionInTimeLineBlockAction();
                    break;
                case INDICATOR_PANEL_TAG:
                    isPenelIndicatorParametr = false;
                    passIndicatorPanel = false;
                    break;
                case CONTENT_TAG:
                    isContent = false;
                    break;
                default:
                    break;
                }
        }
    }
    
    @Override 
    public void endDocument() throws SAXException {
        applyDeviceLinks();
        deviceWithDeviceLinksPathByGUID = null;
        devicePathByGUID = null;
        deviceNewIdByDesktopId = null;
        regionIdbyIdx = null;
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(!passScenario && isScenarioLogicBlock ) {
            String text = new String(ch, start, length);
            if (text.startsWith(XML_PREFIX)) {
                InputStream inputStream = new ByteArrayInputStream(text.getBytes());
                Document document = null;
                try {
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                    document = documentBuilder.parse(inputStream);
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Element root = document.getDocumentElement();
                rootScenarioLogicBlock = new ScenarioLogicBlock();
                previosScenarioLogicBlock = rootScenarioLogicBlock;
                parentElementEntityBylogiclevelCounter = new HashMap<>();
                parentElementEntityBylogiclevelCounter.put(logicLevelCounter, rootScenarioLogicBlock);
                previosLevelsubLogicList = rootScenarioLogicBlock.getSubLogics();
                NamedNodeMap rootAttrebutes = root.getAttributes();
                /*
                получаем атрибуты для корневого элемента, тэг logic
                */
                for (int j = 0; j < rootAttrebutes.getLength(); j++) {
                    Node Node = rootAttrebutes.item(j);
                    String operator = null;
                    if (Node.getNodeName().equals(ITEM_OPERATOR)) {
                        operator = Node.getNodeValue();
                        if (operator != null) {
                            switch (operator) {
                            case LOGIC_TYPE_OR:
                                rootScenarioLogicBlock.setLogicType(LogicType.OR);
                                break;
                            case LOGIC_TYPE_AND:
                                rootScenarioLogicBlock.setLogicType(LogicType.AND);
                                break;
                            case LOGIC_TYPE_AND_NOT:
                                rootScenarioLogicBlock.setLogicType(LogicType.AND_NOT);
                                break;
                            case LOGIC_TYPE_OR_NOT:
                                rootScenarioLogicBlock.setLogicType(LogicType.OR_NOT);
                                break;
                            default:
                                break;
                            }
                        }
                    }
                }
                logicLevelCounter++;
                NodeList children = root.getChildNodes();
                parseNode(children);
            }
        } else if (isContent) {
            String cdata = new String(ch, start, length);
            if (cdata.startsWith(XML_PREFIX)) {
                cdata = cdata.replaceAll(REPLACED_CHARS_NG_VERSION_ESCAPE_CHARS_IN_CDATA, NG_VERSION_ESCAPE_CHARS_IN_CDATA);
                byte[] byteArrayFileConten;
                try {
                    byteArrayFileConten = cdata.getBytes();
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArrayFileConten);
                    SAXParserFactory spf = SAXParserFactory.newInstance();
                    SAXParser saxParser = spf.newSAXParser();
                    XMLReader xmlReader = saxParser.getXMLReader();
                    IndicatorXMLParsingHandler handler = indicatorXMLParsingHandler;
                    handler.setIndicatorPanel(currentIndicatorPanel);
                    handler.setRegionNewIdByDesktopId(regionNewIdByDesktopId);
                    handler.setDeviceNewIdByDesktopId(deviceNewIdByDesktopId);
                    handler.setScenarioNewIdByDesktopId(scenarioNewIdByDesktopId);
                    xmlReader.setContentHandler(handler);
                    xmlReader.parse(new InputSource(inputStream));
                    inputStream.close();
                    warningMessages.addAll(handler.getIndicatorWarningMessages());
                    currentIndicatorPanel = indicatorPanelRepository.save(currentIndicatorPanel);
                    if(currentIndicatorPanel!= null) {
                        indicatorPanelRepository.save(currentIndicatorPanel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
    
    public List<ImportValidateMessage> getWarningMessages() {
        return warningMessages;
    }

    private boolean applyConfigProperty() {
        Map<String, String> oldPropertyValues = currentDevice.getProjectConfigValues();
        Map<String, String> newPropertyValues = new HashMap<>();
        for (String propertyId : deviceConfigPropertyValueByName.keySet()) {
            DeviceConfigProperty property = deviceConfigPropertyValueByName.get(propertyId);
            String newPropertyValue = null;
            if (importedDeviceConfigPropertyValueByName.containsKey(propertyId)) {
                newPropertyValue = importedDeviceConfigPropertyValueByName.get(propertyId);
            }else if (oldPropertyValues.containsKey(propertyId)) {
                newPropertyValue = oldPropertyValues.get(propertyId);
            }else {
                newPropertyValue = property.getDefaultValue();
            }
            newPropertyValues.put(propertyId, newPropertyValue);
        }
        currentDevice.setProjectConfigValues(newPropertyValues);
        if(deviceRepository.save(currentDevice) != null) {
            return true;
        }else {
            return false;
        }
    }
    
    private void applyDeviceLinks() {
        String deviceGUID;
        List<String> deviceLinks = new ArrayList<>();
        for (Map.Entry<String, String> entry : deviceWithDeviceLinksPathByGUID.entrySet()) {
            String[] devicePaths = entry.getValue().split(DEVICE_PATH_DEVIDER);
            for(int i = 0; i < devicePaths.length; i++) { 
                if(devicePathByGUID.containsKey(devicePaths[i])) {
                    deviceGUID = devicePathByGUID.get(devicePaths[i]);
                    if(deviceNewIdByDesktopId.containsKey(deviceGUID)) {
                        deviceLinks.add(deviceNewIdByDesktopId.get(deviceGUID));
                    }
                }
             }
            devicePaths = null;
            deviceViewService.updateDeviceLinks(projectId, entry.getKey(), deviceLinks, language, errorMessage);
            deviceLinks.clear();
        }
    }
    
    private void updateDeviceProfileForInnerDevice() {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(
                addedDevice.get(1 + innerDeviceCounter).getProjectId(),
                addedDevice.get(1 + innerDeviceCounter).getId());
        currentDevice = deviceRepository.findByProjectIdAndId(projectId, deviceView.getId());
        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceCreateView.getDeviceProfileId());
        if (!deviceCreateView.getDeviceProfileId()
                .equals(addedDevice.get(1 + innerDeviceCounter).getDeviceProfileId())) {
            deviceService.setDeviceProfile(deviceView.getDevice(),
                    deviceCreateView.getDeviceProfileId(), errorMessage);
            deviceView.apply(deviceProfileView);
            deviceView = deviceViewRepository.save(deviceView);
            currentDevice = deviceView.getDevice();
        }
        innerDeviceCounter++;
        if (innerDeviceCounter == innerDeviceNumber) {
            innerDeviceNumber = 0;
            innerDeviceCounter = 0;
            isControl = false;
        }
    }
    
    private void createDevice(String importedDeviceContainPath, String importedDeviceIndex) {
        if(deviceProfile.getDeviceCategory() == DeviceCategory.TRANSPORT) {
            deviceCreateView.setParentDeviceId(null);
        }else if(deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL) {
            deviceCreateView.setParentDeviceId(transportParentId);
        }else {
            deviceCreateView.setParentDeviceId(parentDeviceId);
        }
        if (innerDeviceNumber <= 0) {
            addedDevice = deviceService.add(deviceCreateView, projectId, new ArrayList<String>(),
                    errorMessage);
            if(addedDevice != null && !addedDevice.isEmpty()) {
                currentDevice = addedDevice.get(FIRST_DEVICE);
                if(importedDeviceContainPath != null) { 
                    deviceWithDeviceLinksPathByGUID.put(currentDevice.getId(), importedDeviceContainPath);
                }
                for (Device savedDevice : addedDevice) {
                    DeviceProfileView deviceProfileView = deviceProfileViewRepository
                            .findByLanguageAndId(language, savedDevice.getDeviceProfileId());
                    if (deviceProfileView == null) {
                        String description = "Устройство \"".concat(importedDeviceNameById.get(importedDeviceIndex)).concat("\"не было добавлено");
                        warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_NOT_ADDED, description, EntityType.DEVICE, null));
                        break;
                    }
                    DeviceView savedDeviceView = new DeviceView(savedDevice, deviceProfileView);
                    deviceViewService.calculateAddressPaths(savedDeviceView);
                    deviceViewRepository.save(savedDeviceView); 
                }
                if (addedDevice.get(FIRST_DEVICE).getDeviceCategory() == DeviceCategory.TRANSPORT) {
                    transportParentId = currentDevice.getId();
                    parentDeviceId = currentDevice.getId();
                } else if (addedDevice.get(FIRST_DEVICE).getDeviceCategory() == DeviceCategory.CONTROL) {
                    parentDeviceId = currentDevice.getId();
                    isControl = true;
                }
                DeviceProfile currentDeviceProfile = deviceProfileRepository
                        .findOne(currentDevice.getDeviceProfileId());
                if (currentDeviceProfile.getInternalDevices() != null
                        | !currentDeviceProfile.getInternalDevices().isEmpty()) {
                    for (InternalDeviceItem internalDevice : currentDeviceProfile.getInternalDevices()) {
                        innerDeviceNumber += internalDevice.getnDevices();
                    }
                }
            }
        }
    }
    
    private void applyRegionParameter(String parameterName, String parameterValue) {
        if (!parameterName.equals(USELESS_PARAM_1) && !parameterName.equals(USELESS_PARAM_2)) {
            switch (parameterName) {
            case REGION_FIRE_DEVICE_COUNT:
                currentRegion.setFireEventCount(Integer.parseInt(parameterValue));
                break;
            case REGION_TYPE:
                switch (parameterValue) {
                case SUBSYSTEM_PARAM_TYPE_FIRE:
                    currentRegion.setSubsystem(Subsystem.FIRE);
                    break;
                case SUBSYSTEM_PARAM_TYPE_SECURITY:
                    currentRegion.setSubsystem(Subsystem.SECURITY);
                    break;
                case SUBSYSTEM_PARAM_TYPE_SKUD:
                    currentRegion.setSubsystem(Subsystem.SKUD);
                    break;
                default:
                    break;
                }
                break;
            case REGION_SKIPPED:
                if (parameterValue.equals(SILENCE_ALARM_PARAM_OFF)) {
                    currentRegion.setSilenceAlarm(false);
                } else {
                    currentRegion.setSilenceAlarm(true);
                }
                break;
            case REGION_AUTO_SET:
                try {
                    currentRegion.setAutoRelock(Integer.parseInt(parameterValue));
                } catch (Exception e) {
                    logger.warn("Could not parse the imported file attribute");
                }
                break;
            case REGION_DELAY:
                try {
                    currentRegion.setInputOutputTimeout(Integer.parseInt(parameterValue));
                } catch (Exception e) {
                    logger.warn("Could not parse the imported file attribute");
                }
                break;
            case REGION_GUARD_REGION_TYPE:
                switch (parameterValue) {
                case SECURITY_REGION_TYPE_DEFAULT:
                    currentRegion.setSecurityRegionType(SecurityRegionType.DEFAULT);
                    break;
                case SECURITY_REGION_TYPE_TIMEOUT:
                    currentRegion.setSecurityRegionType(SecurityRegionType.TIMEOUT);
                    break;
                case SECURITY_REGION_TYPE_NO_OFF:
                    currentRegion.setSecurityRegionType(SecurityRegionType.NO_OFF);
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
    }

    private void applyRegionBaseProperty(String regionIndex, String regionName, String regionNumber, String regionDescription) {

        if(Integer.parseInt(regionIndex) >= 0) {
            currentRegion = new Region();
            /*
             * regionNumber нужен для привязки устройства к зоне
             * regionNumber равен индексу и no(который будет нашем Id)
             */
            try {
                currentRegion.setIndex(Integer.parseInt(regionNumber));
            } catch (Exception e) {
                logger.warn("Could not parse the imported file attribute");
            }
            currentRegion.setName(regionName);
            currentRegion.setDescription(regionDescription);
            currentRegionDesktopId = regionNumber;
            try {
                if (Integer.parseInt(regionNumber) < 1) {
                    regionIdxIndexLessOne.add(regionIndex);
                } else {
                    currentRegionIdx = regionIndex;
                }
            } catch (Exception e) {
                logger.warn("Could not parse the imported file attribute");
            }
        }
    }
    
    private boolean createDeviceView(String deviceIndex, String deviceAddress) {
        deviceCreateView = new DeviceCreateView();
        if (importedDeviceIdByIdx.containsKey(deviceIndex)) {
            deviceProfile = deviceProfileRepository.findByDesktopFSId(importedDeviceIdByIdx.get(deviceIndex));
            if (deviceProfile != null) {
                deviceCreateView.setDeviceProfileId(deviceProfile.getId());
                if (innerDeviceCounter > 2 & innerDeviceCounter < 5 & isControl) {
                    deviceProfile = deviceProfileRepository.findByChangeDeviceProfile(true);
                    deviceCreateView.setDeviceProfileId(deviceProfile.getId());
                } else {
                    deviceCreateView.setDeviceProfileId(deviceProfile.getId());
                }
            } else {
                String description;
                if (Integer.parseInt(deviceAddress) <= 256) {
                    description = "Устройство \"".concat(importedDeviceNameById.get(deviceIndex)
                            .concat("\" , " + (Integer.parseInt(deviceAddress) & 0xFF)).concat(" не  добавлено"));
                } else {
                    description = "устройство ".concat(importedDeviceNameById.get(deviceIndex)
                            .concat(" , " + ((Integer.parseInt(deviceAddress) & 0xFF00) >> 8)).concat(".")
                            + (Integer.parseInt(deviceAddress) & 0xFF)).concat(" не  добавлено");
                }
                warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_NOT_ADDED, description, EntityType.DEVICE, null));
                currentDevice = null;
                passDeviceProperty = true;
                return false;
            }
        } else {
            warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.UNKNOWN_DEVICE_NOT_ADDED , EntityType.DEVICE, null));
            currentDevice = null;
            passDeviceProperty = true;
            return false;
        }
        try {
            if(deviceProfile.getDeviceCategory() == DeviceCategory.TRANSPORT) {
                deviceCreateView.setLineAddress(Integer.parseInt(deviceAddress));
            }else {
                deviceCreateView.setLineAddress(Integer.parseInt(deviceAddress) & 0xFF);
            }
            if(deviceProfile.getDeviceCategory() == DeviceCategory.CONTROL) {
                deviceCreateView.setLineNo(channelCounter);
            }else {
                deviceCreateView.setLineNo((Integer.parseInt(deviceAddress) & 0xFF00) >> 8);
            }
        } catch (Exception e) {
            logger.warn("Could not parse the imported file attribute");
        }
        return true;
    }

    private boolean applyDeviceProperty () {
        for (String propertyName : devicePropertyValueByName.keySet()) {
            DeviceProperty property = devicePropertyValueByName.get(propertyName);
            if (property.getDesktopFSPropertyId() != null && !property.getDesktopFSPropertyId().isEmpty()
                    && property.getDesktopFSPropertyId().equals(devicePropertyName)) {
                if (property.getType() == PropertyType.FLAG) {
                    if (devicePropertyValue.equals(FALSE)) {
                        propertyValue = String.valueOf(false);
                    } else {
                        propertyValue = String.valueOf(true);
                    }
                } else if (property.getType() == PropertyType.ENTITY_ID) {
                    if (devicePropertyValue.equals(DEVICE_PROPERTY_DEFAULT_VALUE)) {
                        propertyValue = property.getDefaultValue();
                    }else if (regionNewIdByDesktopId.containsKey(devicePropertyValue)) {
                        propertyValue = regionNewIdByDesktopId.get(devicePropertyValue);
                    }else {
                        propertyValue = property.getDefaultValue();
                    }
                } else {
                    propertyValue = devicePropertyValue;
                }
                currentDevicePropertyValueByName = new HashMap<>();
                currentDevicePropertyValueByName.put(propertyName, propertyValue);
                applyDevicePropertySucceded = true;
                if (!property.checkValue(propertyValue)) {
                    String nameOfProperty= propertyName;
                    DeviceProfileView deviceProfileView = deviceProfileViewRepository.findOne(currentDevice.getDeviceProfileId());
                    if(deviceProfileView.getPropertyViews().containsKey(propertyName)) {
                        nameOfProperty = deviceProfileView.getPropertyViews().get(propertyName).getName();
                    }
                    String description = "Параметр \"" 
                            + nameOfProperty + "\" был изменен на значение по умолчанию в свойствах \"Прочие\"";
                    warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_PARAMETR_WAS_CHANCHED_TO_DEFAULT_VALUE_IN_OTHER_PROPERTY, description, EntityType.DEVICE, currentDevice.getId()));
                    applyDevicePropertySucceded = false;
                    return false;
                }
            }
            if (applyDevicePropertySucceded) {
                for (String keyStr : currentDevicePropertyValueByName.keySet()) {
                    currentDevice.getDevicePropertyValues().put(keyStr, currentDevicePropertyValueByName.get(keyStr));
                }
                deviceRepository.save(currentDevice);
                return true;
            }
        }
        return false;
    }
    
    private void applyDeviceConfigProperty() {
        importedDeviceConfigPropertyValueByName = new HashMap<>();
        if (devicePropertyName.startsWith(CONFIG_PREFIX)) {
            devicePropertyName = devicePropertyName.substring(CONFIG_PREFIX.length());
        } else {
            return;
        }
        importedDeviceConfigPropertyValueByName.put(devicePropertyName, devicePropertyValue);
        applyDevicePropertySucceded = false;
        if(deviceConfigPropertyValueByName.containsKey(devicePropertyName)) {
            applyDevicePropertySucceded = false;
            DeviceConfigProperty configProperty = deviceConfigPropertyValueByName.get(devicePropertyName);
            if (configProperty != null) {
                applyDevicePropertySucceded = true;
                if(configProperty.isHidden() == false) {
                    if (!configProperty.checkValue(devicePropertyValue)) {
                        String nameOfProperty = devicePropertyName;
                        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findOne(currentDevice.getDeviceProfileId());
                        if(deviceProfileView.getConfigPropertyViews().containsKey(devicePropertyName)) {
                            nameOfProperty = deviceProfileView.getConfigPropertyViews().get(devicePropertyName).getName();
                        }
                        String description = "Параметр \"".concat(nameOfProperty).concat("\" со значение \"" +devicePropertyValue + "\" был изменен на значение по умолчанию в свойствах \"Конфигурирование\"");
                        warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_PARAMETR_WAS_CHANCHED_TO_DEFAULT_VALUE_IN_CONFIGURATION_PROPERTY, description , EntityType.DEVICE, currentDevice.getId()));
                        applyDevicePropertySucceded = false;
                        return;
                    }
                }
            }
        }
        if (applyDevicePropertySucceded) {
            if (applyConfigProperty()) {
                return;
            }
        }
    }
    /*
     *  путь нужен для добавления устройств в НС и исполнительный блок, так как добавление,
     *  в desktop версии, происходи по пути устройства.
     */
    private void createDevicePath() {
        StringBuilder devicePathMaker = new StringBuilder();
        devicePathMaker.delete(0, devicePathMaker.length());
        if (deviceLevelCounter == MAX_DEVICE_LEVEL) {
            devicePath = devicePathMaker.append(firstLevelDeviceQuantity).append(LEVEL_SEPARATOR)
                    .append(secondLevelDeviceQuantity).append(LEVEL_SEPARATOR).append(thirdLevelDeviceQuantity)
                    .append(LEVEL_SEPARATOR).append(fourthLevelDeviceQuantity).append(LEVEL_SEPARATOR)
                    .append(fifthLevelDeviceQuantity).toString();
        } else {
            devicePath = devicePathMaker.append(firstLevelDeviceQuantity).append(LEVEL_SEPARATOR)
                    .append(secondLevelDeviceQuantity).append(LEVEL_SEPARATOR).append(thirdLevelDeviceQuantity)
                    .append(LEVEL_SEPARATOR).append(fourthLevelDeviceQuantity).toString();
        }
        devicePathByGUID.put(devicePath, deviceDesktopId);
    }

    private void generateRegionIndex(Region region) {
        int minRegionIndex = 1;
        int maxRegionIndex = 65535;
        int result = 0;
        boolean valid = false;
        Random indexGenarator = new Random();
        isRegionIndexGenerated = false;
        currentRegionDesktopId = String.valueOf(region.getIndex());
        if(checkRegionSubsystem(region)) {
            while (!valid) {
                result = indexGenarator.nextInt(maxRegionIndex - minRegionIndex) + minRegionIndex;
                region.setIndex(result);
                isRegionIndexGenerated = true;
                valid = addRegion(region);
            }
        }else {
           
            String description  = "Зона \"".concat(region.getName()).concat("\" не была добавлена");
            warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.REGION_NOT_ADDED, description, EntityType.REGION, null));
        }
    }

    private boolean addRegion(Region region) {
        Region savedRegion = regionService.add(projectId, region, errorMessage);
        if (savedRegion != null) {
            RegionView regionView = new RegionView(savedRegion);
            regionView = regionViewRepository.insert(regionView);
            if(regionView != null) {
                regionNewIdByDesktopId.put(currentRegionDesktopId, savedRegion.getId());
                regionIdbyIdx.put(currentRegionIdx, savedRegion.getId());
                return true;
            }else {
                String description = "Зона \"".concat(currentRegion.getName()).concat("\" не была добавлена");
                warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.REGION_NOT_ADDED, description, EntityType.REGION, null));
                return false;
            }
        }
        if(!isRegionIndexGenerated) {
            String description = "Зона \"".concat(currentRegion.getName()).concat("\" не была добавлена");
            warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.REGION_NOT_ADDED, description, EntityType.REGION, null));
        }
        return false;
    }
    
    private boolean checkRegionSubsystem(Region region) {
        boolean valid = true;
        switch (region.getSubsystem()) {
            case UNKNOWN:
                valid = false;
                break;
            case FIRE:
                if (region.getFireEventCount() <= 0) {
                    valid = false;
                }
                break;
            case GENERAL:
                valid = false;
                break;
            default:
                break;
        }
        return valid;
    }
    
    private void addVirtualState() {
        VirtualState savedVirtualState = new VirtualState();
        for (VirtualState virtualState : importedVirtualState) {
            if (virtualState.getDeviceId().equals(deviceDesktopId)) {
                virtualState.setDeviceId(currentDevice.getId());
                savedVirtualState = virtualStateService.create(virtualState, errorMessage);
                if (savedVirtualState == null) {
                    String description  = "Виртуальное состояние \"".concat(virtualState.getName().concat("\" не было добавлено"));
                    warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.VIRTUAL_STATE_NOT_ADDED, description, EntityType.VIRTUAL_STATE, null));
                }else {
                    virtualStateNewIdByDesktopNo.put(String.valueOf(savedVirtualState.getGlobalNo()), savedVirtualState.getId());
                }
            }
        }
    }

    private void applyVirtualStateBaseProperty(String number, String name, String description, String stateCategoryId,
            String messageOn, String messageOff) {
        currentVirtualState = new VirtualState();
        importedVirtualState.add(currentVirtualState);
        try {
            currentVirtualState.setGlobalNo(Integer.parseInt(number));
        } catch (Exception e) {
            logger.warn("Could not parse the imported file attribute");
        }
        currentVirtualState.setName(name);
        if (description != null) {
            currentVirtualState.setDescription(description);
        }
        try {
            if (stateCategoryId.equals(VIRTUAL_STATE_STATE_CATEGORY_ID_DO_NOT_GUARD)) {
                currentVirtualState.setStateCategoryId(VIRTUAL_STATE_STATE_CATEGORY_OFF_GUARD);
            }else if (stateCategoryId.equals(VIRTUAL_STATE_STATE_CATEGORY_ID_FIRE_1P)){
                currentVirtualState.setStateCategoryId(VIRTUAL_STATE_STATE_CATEGORY_FIRE1);
            }else {
                currentVirtualState.setStateCategoryId(stateCategoryId);
            }
        } catch (Exception e) {
            logger.warn("Could not parse the imported file attribute");
        }
        if (messageOn != null) {
            currentVirtualState.setMessageOn(messageOn);
        }
        if (messageOff != null) {
            currentVirtualState.setMessageOff(messageOff);
        }
        currentVirtualState.setProjectId(projectId);
    }
    
    private Scenario createScenario(String projectId, ScenarioBasicParams scenarioBasicParams, boolean manualStartStopStatus,
            boolean stopOnGlobalReset, StopType stopType) {
        if ( scenarioBasicParams.getScenarioPurpose() != null && projectService.checkProjectIsEditable(projectId, errorMessage) &&
                scenarioViewService.validateScenarioBasicParams(null, scenarioBasicParams, errorMessage)) {
            Scenario scenario = new Scenario();
            scenario.setProjectId(projectId);
            scenario.setName(scenarioBasicParams.getName());
            scenario.setDescription(scenarioBasicParams.getDescription());
            scenario.setScenarioPurpose(scenarioBasicParams.getScenarioPurpose());
            scenario.setScenarioType(scenarioBasicParams.getScenarioType());
            scenario.setControlDeviceId(scenarioBasicParams.getControlDeviceId());
            scenario.setEnabled(scenarioBasicParams.isEnabled());
            scenario.setGlobalNo(getNewGlobalNo(projectId));
            
            scenario.setManualStartStopAllowed(manualStartStopStatus);
            scenario.setStopOnGlobalReset(stopOnGlobalReset);
            scenario.setStopType(stopType);
            scenario = scenarioRepository.insert(scenario);
            if(scenario != null) {
                return scenario;
            }else {
                String description  = "Сценарий \"".concat(scenarioBasicParams.getName().concat("\" не был добавлен"));
                warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.SCENARIO_NOT_ADDED, description, EntityType.SCENARIO, null));
            }
        }
        return null;
    }
    
    private int getNewGlobalNo(String projectId) {
        Scenario scenario = scenarioRepository.findTopByProjectIdOrderByGlobalNoDesc(projectId);
        int result = 1;
        if (scenario != null)
            result += scenario.getGlobalNo();
        return result;
    }
    
    private void applyScenarioProperty(String scenarioDesktopId, String scenarioName, String scenarioDescription, String scenarioEnabled, String scenarioControlDeviceId, String scenarioPurpose, String scenarioType
            , String scenarioStopTypeManualOrAuto, String scenarioStopTypeByLogic, String scenarioManualStartStopAllowed, String scenarioStopOmGlobalReset) {
        StopType stopType = StopType.AUTO;
        boolean manualStartStopAllowed = false;
        boolean stopOnGlobalReset = false;
        ScenarioBasicParams scenarioBasicParams = new ScenarioBasicParams();
        if(scenarioName != null) {
            scenarioBasicParams.setName(scenarioName);
        }
        scenarioBasicParams.setDescription(scenarioDescription);
        if(scenarioEnabled != null) {
            try {
                if(Integer.parseInt(scenarioEnabled) == SCENARIO_PARAM_ENABLED_TRUE) {
                    scenarioBasicParams.setEnabled(true);
                }else {
                    scenarioBasicParams.setEnabled(false);
                }
            } catch (Exception e) {
                logger.warn("Could not parse the imported file attribute");
            }
        }
        if(scenarioPurpose != null) {
            if(scenarioPurpose.equals(SCENARIO_PURPOSE_EXEC_BY_LOGIC)) {
                scenarioBasicParams.setScenarioPurpose(ScenarioPurpose.EXEC_BY_LOGIC);
            }else if(scenarioPurpose.equals(SCENARIO_PURPOSE_EXEC_BY_INVOKE)){
                scenarioBasicParams.setScenarioPurpose(ScenarioPurpose.EXEC_BY_INVOKE);
            }else if(scenarioPurpose.equals(SCENARIO_PURPOSE_EXEC_BY_TACTICS)) {
                scenarioBasicParams.setScenarioPurpose(ScenarioPurpose.EXEC_BY_TACTICS);
            }else {
                scenarioBasicParams.setScenarioPurpose(null);
            }
        }
        if(scenarioType != null) {
            if(scenarioType.equals(SCENARIO_TYPE_UNASSIGNED)) {
                scenarioBasicParams.setScenarioType(ScenarioType.UNASSIGNED);
            }else if (scenarioType.equals(SCENARIO_TYPE_ENGINEERING)) {
                scenarioBasicParams.setScenarioType(ScenarioType.ENGINEERING);
            }else if (scenarioType.equals(SCENARIO_TYPE_ENS)) {
                scenarioBasicParams.setScenarioType(ScenarioType.ENS);
            }else if (scenarioType.equals(SCENARIO_TYPE_FIRE_FIGHTING)) {
                scenarioBasicParams.setScenarioType(ScenarioType.FIRE_FIGHTING);
            }
        }
        if(scenarioControlDeviceId != null) {
            if(deviceNewIdByDesktopId.containsKey(scenarioControlDeviceId)){
                scenarioBasicParams.setControlDeviceId(deviceNewIdByDesktopId.get(scenarioControlDeviceId));
            }
        }
        if(scenarioStopTypeManualOrAuto != null && scenarioStopTypeByLogic != null) {
            if(scenarioStopTypeManualOrAuto.equals(SCENARIO_PARAM_VALUE_FALSE) & scenarioStopTypeByLogic.equals(SCENARIO_PARAM_VALUE_FALSE)) {
                stopType = StopType.AUTO;
            }else if(scenarioStopTypeManualOrAuto.equals(SCENARIO_PARAM_VALUE_FALSE) & scenarioStopTypeByLogic.equals(SCENARIO_PARAM_VALUE_TRUE)) {
                stopType = StopType.MANUAL;
            }else if(scenarioStopTypeManualOrAuto.equals(SCENARIO_PARAM_VALUE_TRUE) & scenarioStopTypeByLogic.equals(SCENARIO_PARAM_VALUE_FALSE)) {
                stopType = StopType.BY_LOGIC;
            }
        }
        if(scenarioManualStartStopAllowed != null) {
            if(scenarioManualStartStopAllowed.equals(SCENARIO_PARAM_VALUE_TRUE)) {
                manualStartStopAllowed = true;
            }else if(scenarioManualStartStopAllowed.equals(SCENARIO_PARAM_VALUE_FALSE)) {
                manualStartStopAllowed = false;
            }
        }
        if(scenarioStopOmGlobalReset != null) {
            if(scenarioStopOmGlobalReset.equals(SCENARIO_PARAM_VALUE_TRUE)) {
                stopOnGlobalReset = true;
            }else if(scenarioStopOmGlobalReset.equals(SCENARIO_PARAM_VALUE_FALSE)) {
                stopOnGlobalReset = false;
            }
        }
        currentScenario = createScenario(projectId, scenarioBasicParams, manualStartStopAllowed, stopOnGlobalReset, stopType);
        if (currentScenario != null) {
            scenarioNewIdByDesktopId.put(scenarioDesktopId, currentScenario.getId());
        }else {
            passScenario = true;
        }
    }
    
    private void applyScenarioTimeLineBlockProperty(String timeDelay, String type) {
        
        if(type != null ) {
           
            if(type.equals(SCENARIO_TIME_LINE_BLOCK_TYPE_TRACING_STOP_ACTION)) {
                currentScenarioTimeLineBlock.setTracingPeriodSec(Integer.parseInt(timeDelay) - currentScenarioTimeLineBlock.getTimeDelaySec());
                blockInBlock = true;
            }else {
                currentScenarioTimeLineBlock = new ScenarioTimeLineBlock();
                try {
                    if(Integer.parseInt(timeDelay) >= 0 ) {
                        currentScenarioTimeLineBlock.setTimeDelaySec(Integer.parseInt(timeDelay));
                    }
                }catch (Exception e) {
                    logger.warn("Could not parse the imported file attribute");
                }
                if(type.equals(SCENARIO_TIME_LINE_BLOCK_TYPE_EXECUTIVE) | type.equals(SCENARIO_TIME_LINE_BLOCK_TYPE_HAVING_TRACING_BLOCK)) {
                    currentScenarioTimeLineBlock.setBlockType(BlockType.EXECUTIVE);
                }else if(type.equals(SCENARIO_TIME_LINE_BLOCK_TYPE_TRACING)){
                    currentScenarioTimeLineBlock.setBlockType(BlockType.TRACING);
                }else if(type.equals(SCENARIO_TIME_LINE_BLOCK_TYPE_COMPUTER_ACTION)) {
                    currentScenarioTimeLineBlock.setBlockType(BlockType.COMPUTER_ACTION);
                }
            }
        }
    }
    /*
     * добавление устройства в исполнительном блоке в действие, в desktop версии, происходит не по UID устройства,
     * а профилям устройств и полному адресу.
     *  профиль_мс:адрес_мс;профиль_канала:адрес_канала;профиль_прибора:адрес_прибора;профиль_устройства:адрес_устройства
     */
    private String applyScenarioTimeLineBlockDeviceItem(String itemPath) {
        Device device;
        StringBuilder adressPath = new StringBuilder();
        String[] devidedDevicePath = itemPath.split(";");
        String desktopFSId = "";
        String deviceAddress = "";
        String[] deviceProfileAndAddress;
        int containerAdress = 0;
        int containerlineNo = 0;
        DeviceProfile deviceProfile = null;
        boolean passDeviceProfile = false;
        boolean isConteinerDevice = false;
        for (int i = devidedDevicePath.length - 1; i > -1; i--) {
            deviceProfileAndAddress = devidedDevicePath[i].split(":");
            desktopFSId = deviceProfileAndAddress[0];
            deviceAddress = deviceProfileAndAddress[1];
            if (i == devidedDevicePath.length - 1) {
                adressPath = adressPath.append(",");
                continue;
            }
            if(!passDeviceProfile) {
                deviceProfile = deviceProfileRepository.findByDesktopFSId(desktopFSId);
                if(deviceProfile != null) {
                    if(deviceProfile.getDeviceCategory()== DeviceCategory.TRANSPORT) {
                        adressPath =adressPath.append(deviceAddress + ",");
                        passDeviceProfile = true;
                    }else if(deviceProfile.getDeviceCategory()== DeviceCategory.CONTROL) {
                        adressPath =adressPath.append(deviceAddress + ",");
                    }else {
                        int lineNo =(Integer.parseInt(deviceAddress) & 0xFF00) >> 8;
                        int deviceAdress = Integer.parseInt(deviceAddress) & 0xFF;
                        // если устройство генерируемое или в контейнере
                        if(deviceProfile.getLineNumberSubstitution()!= null && deviceProfile.getLineNumberSubstitution() > 0) {
                            if(deviceProfile.getLineAddressSubstitution() != null && deviceProfile.getLineAddressSubstitution() > 0) {
                                adressPath=adressPath.append(deviceProfile.getLineNumberSubstitution()+"," + deviceProfile.getLineAddressSubstitution());
                            }else {;
                                adressPath=adressPath.append(deviceProfile.getLineNumberSubstitution()+"," +deviceAddress);
                            }
                        }else {
                            if(i!=0) {
                                adressPath=adressPath.append(lineNo+"," +deviceAdress+",");
                                containerAdress = deviceAdress;
                                containerlineNo = lineNo;
                                isConteinerDevice = true;
                            }else {
                                if(isConteinerDevice) {
                                    adressPath=adressPath.append(containerlineNo+"," + (deviceAdress + containerAdress));
                                    isConteinerDevice = false;
                                }else {
                                    adressPath=adressPath.append(lineNo+"," + deviceAdress);
                                }
                            }
                        }
                    }
                }
            }else {
                passDeviceProfile = false;
                adressPath =adressPath.append(deviceAddress+",");
            }
        }
        device = deviceRepository.findByProjectIdAndAddressPath(projectId, String.valueOf(adressPath));
        if(device != null) {
            return device.getId();
        }else {
            String description;
            if(deviceProfile != null) {
                DeviceProfileView deviceProfileView = deviceProfileViewRepository.findOne(deviceProfile.getId());
                description  = "Устройство \"".concat(deviceProfileView.getName().concat("\" не было добавлено в исполнительный блок сценария \""
                        .concat(currentScenario.getName()).concat("\"")));
            }else {
                description  = "Устройство не было добавлено в исполнительный блок сценария\""
                        .concat(currentScenario.getName()).concat("\"");
            }
            warningMessagesInTimeLineBlock.add(new ImportValidateMessage(Level.WARNING,Code.DEVICE_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK, description, EntityType.DEVICE, null));
            return null;
        }
    }
    
    private String applyScenarioTimeLineBlockScenarioItem(String item) {
        Scenario scenario = null;
        if(scenarioNewIdByDesktopId.containsKey(item)) {
            scenario = scenarioRepository.findOne(scenarioNewIdByDesktopId.get(item));
            if(scenario != null) {
                return scenario.getId();
            }
        }
        return null;
    }
    
    private String applyScenarioTimeLineBlockRegionItem(String item) {
        Region region = null;
        if(regionNewIdByDesktopId.containsKey(item)) {
            region = regionRepository.findOne(regionNewIdByDesktopId.get(item));
            if(region != null) {
                return region.getId();
            }
        }
        String description  = "Зона не была добавлена в исполнительный блок сценария \"".concat(currentScenario.getName().concat("\""));
        warningMessagesInTimeLineBlock.add(new ImportValidateMessage(Level.WARNING,Code.REGIO_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK, description, EntityType.REGION, null));
        return null;
    }
    
    private String applyScenarioTimeLineBlockVirtualStateItem(String item) {
        VirtualState virtualState = null;
        if(virtualStateNewIdByDesktopNo.containsKey(item)) {
            virtualState = virtualStateRepository.findOne(virtualStateNewIdByDesktopNo.get(item));
            if(virtualState != null) {
                return virtualState.getId();
            }
        }
        String description  = "Виртуальное состояние не было добавлено в исполнительный блок сценария \"".concat(currentScenario.getName().concat("\""));
        warningMessagesInTimeLineBlock.add(new ImportValidateMessage(Level.WARNING,Code.VIRTUAL_STATE_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK, description, EntityType.VIRTUAL_STATE, null));
        return null;
    }
    private void applyScenarionTimeLineBlockActionExecutiveItem(String itemType, String actionTypeNumber, String itemValue) {
        currentScenarioTimeLineBlockAction = null;
        String actionType = null;
        String actionNumber = actionTypeNumber;
        String actionId = null;
        ScenarioActionType scenarioActionType;
        if (itemType.equals(SCENARIO_BLOCK_ACTION_DEVICE)) {
            actionId = applyScenarioTimeLineBlockDeviceItem(itemValue);
            if (actionId != null) {
                if(actionId == ACTION_EXEC_DEVICE_ON && (currentScenario.getScenarioPurpose().equals(ScenarioPurpose.EXEC_BY_TACTICS))){
                    actionType = SCENARIO_TIME_LINE_ITEM_ACTION_TACTICS_PURPOSE_EXEC_DEVICE_ON;
                }else {
                    scenarioActionType = scenarioActionTypeRepository.findByDesktopActionNumberAndEntityType(actionNumber, ActionEntityType.DEVICE);
                    if(scenarioActionType != null) {
                            actionType = scenarioActionType.getId();
                    }
                }
            }
        }
        if (itemType.equals(SCENARIO_BLOCK_ACTION_SCENARIO)) {
            actionId = applyScenarioTimeLineBlockScenarioItem(itemValue);
            if (actionId != null) {
                scenarioActionType = scenarioActionTypeRepository.findByDesktopActionNumberAndEntityType(actionNumber, ActionEntityType.SCENARIO);
                if (scenarioActionType != null) {
                    actionType = scenarioActionType.getId();
                }
            }else {
                recheckScenario = true;
                currentRecheckScenarioData = new RecheckScenarioData(itemType, actionTypeNumber, itemValue, actionCounter, blockCounter);
                recheckScenarioDataListInAction.add(currentRecheckScenarioData);
            }
        }
        if (itemType.equals(SCENARIO_BLOCK_ACTION_VIRTUAL_STATE)) {
            actionId = applyScenarioTimeLineBlockVirtualStateItem(itemValue);
            if (actionId != null) {
                scenarioActionType = scenarioActionTypeRepository.findByDesktopActionNumberAndEntityType(actionNumber, ActionEntityType.VIRTUAL_STATE);
                if (scenarioActionType != null) {
                    actionType = scenarioActionType.getId();
                }
            }
        }
        if (itemType.equals(SCENARIO_BLOCK_ACTION_REGION)) {
            actionId = applyScenarioTimeLineBlockRegionItem(itemValue);
            if (actionId != null) {
                scenarioActionType = scenarioActionTypeRepository.findByDesktopActionNumberAndEntityType(actionNumber, ActionEntityType.REGION);
                if (scenarioActionType != null) {
                    actionType = scenarioActionType.getId();
                }
            }
        }
        if (actionType != null && actionId != null) {
            currentScenarioTimeLineBlockAction = new ScenarioTimeLineBlock.Action();
            currentScenarioTimeLineBlockAction.setActionTypeId(actionType);
            currentScenarioTimeLineBlockAction.setEntityId(actionId);
            if (!validateScenarioTimeLineBlockExecutiveActionItem(currentScenarioTimeLineBlockAction, false)) {
                currentScenarioTimeLineBlockAction = null;
            } else {
            }
        }
    }

    private void applyScenarionTimeLineBlockActionComputerActionItem(String actionType, String message, String title, String countdownTimeSec, String countDowndTitle) {
        computerAction= new ScenarioTimeLineBlock.ComputerAction();
        if(actionType.equals(SHOW_MESSAGE_TYPE)) {
            computerAction.setCommand(Command.SHOW_MESSAGE);
            computerAction.setMessage(message);
            computerAction.setTitle(title);
        }else if (actionType.equals(COUNTDOWN_TYPE)) {
            computerAction.setCommand(Command.COUNTDOWN);
            computerAction.setCountdownTimeSec(Integer.parseInt(countdownTimeSec));
            computerAction.setMessage(countDowndTitle);
        }else {
            computerAction = null;
        }
        if(computerAction == null || !validateScenarioTimeLineBlockComputerActionItem(computerAction)) {
            computerAction = null;
        }
    }

    private boolean validateScenarioTimeLineBlockComputerActionItem(ScenarioTimeLineBlock.ComputerAction computerAction) {
        boolean result = true;
        int COMPUTER_BLOCK_MAX_TITLE_LENGTH = 50;
        int COMPUTER_BLOCK_MAX_MESSAGE_LENGTH = 200;
        if (computerAction.getMessage().length() < 1
                || computerAction.getMessage().length() > COMPUTER_BLOCK_MAX_MESSAGE_LENGTH) {
            result = false;
        }
        if (computerAction.getCommand() == Command.SHOW_MESSAGE) {
            if (computerAction.getTitle().length() < 1
                    || computerAction.getTitle().length() > COMPUTER_BLOCK_MAX_TITLE_LENGTH) {
                result = false;
            }
        } else {
            if (computerAction.getCountdownTimeSec() < 0) {
                result = false;
            }
        }
        return result;
    }

    private boolean validateScenarioTimeLineBlockExecutiveActionItem(ScenarioTimeLineBlock.Action action, boolean ignoreScenarioActions) {
        boolean result = true;
        if (result) {
            ScenarioActionType actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
            if (actionType == null) {;
                result = false;
            } else {
                switch (actionType.getEntityType()) {
                case DEVICE:
                    if (!deviceRepository.existsByIdAndDisabled(action.getEntityId(), false)) {
                        result = false;
                    }
                    break;
                case REGION:
                    if (regionRepository.findOne(action.getEntityId()) == null) {
                        result = false;
                    }
                    break;
                case SCENARIO: {
                    if (!ignoreScenarioActions) {
                        Scenario scenario = scenarioRepository.findOne(action.getEntityId());
                        if (scenario == null) {
                            result = false;
                        } else if (!scenario.isEnabled()) {
                            result = false;
                        }
                    }
                }
                break;
                case VIRTUAL_STATE:
                    if (virtualStateRepository.findOne(action.getEntityId()) == null) {
                        result = false;
                    }
                    break;
                default:
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    private void applyScenarioTimeLineBlockActionExecutiveTrackingItem(String itemType, String itemValue) {
        
        if(itemType.equals(SCENARIO_BLOCK_ACTION_DEVICE)) {
            currentScenarioTimeLineBlock.setTracingEntityType(TracingEntityType.SENSOR_DEVICE);
            currentScenarioTimeLineBlock.setTracingEntityId(applyScenarioTimeLineBlockDeviceItem(itemValue));
        }else if(itemType.equals(SCENARIO_BLOCK_ACTION_VIRTUAL_STATE)) {
            currentScenarioTimeLineBlock.setTracingEntityType(TracingEntityType.VIRTUAL_STATE);
            currentScenarioTimeLineBlock.setTracingEntityId(applyScenarioTimeLineBlockVirtualStateItem(itemValue));
        }
    }
    
    private void applyScenarioTimeLineBlockCondtionProperty(String conditionType, String checkType, String itemTypeDevice, String itemTypeVirtualState, String deviceStateId) {
        
        if(conditionType.equals(SCENARIO_CONDITION_DEVICE_TYPE)) {
            currentScenarioTimeLineBlock.setConditionType(ConditionType.DEVICE_STATE);
        }else if(conditionType.equals(SCENARIO_CONDITION_VIRTUAL_STATE_TYPE)) {
            currentScenarioTimeLineBlock.setConditionType(ConditionType.VIRTUAL_STATE);
        }
        if(checkType.equals(SCENARIO_CONDITION_ACTIVE_TYPE)) {
            currentScenarioTimeLineBlock.setConditionCheckType(ConditionCheckType.IS_ACTIVE);
        }else if (checkType.equals(SCENARIO_CONDITION_INACTIVE_TYPE)) {
            currentScenarioTimeLineBlock.setConditionCheckType(ConditionCheckType.IS_INACTIVE);
        }
        if(currentScenarioTimeLineBlock.getConditionType()== ConditionType.DEVICE_STATE && itemTypeDevice != null) {
            currentScenarioTimeLineBlock.setConditionEntityId(applyScenarioTimeLineBlockDeviceItem(itemTypeDevice));
            if(deviceStateId != null) {
                State state = stateRepository.findByDesktopStateName(deviceStateId);
                if(state != null) {
                    currentScenarioTimeLineBlock.setConditionDeviceStateId(state.getId());
                }
            }
        }else if (currentScenarioTimeLineBlock.getConditionType()== ConditionType.VIRTUAL_STATE && itemTypeVirtualState != null) {
            if(virtualStateNewIdByDesktopNo.containsKey(itemTypeVirtualState)) {
                currentScenarioTimeLineBlock.setConditionEntityId(virtualStateNewIdByDesktopNo.get(itemTypeVirtualState));
            }
        }
    }

    private void parseNode(NodeList nodeList){
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            previosLevelsubLogicList = previosScenarioLogicBlock.getSubLogics();
            getNodeAttrebutes(node);
        }
        if(itemEntityIds != null && !itemEntityIds.isEmpty()) {
            currentScenarioLogicBlock.setEntityIds(itemEntityIds);
            itemEntityIds = null;
        }
        if(!previosLevelsubLogicList.isEmpty() && previosLevelsubLogicList != null)
        previosScenarioLogicBlock.setSubLogics(previosLevelsubLogicList);
        previosLevelsubLogicList = null;
        findChildren(nodeList);
        logicLevelCounter--;
    }

    private void getNodeAttrebutes(Node node) {
        String operator = null;
        String agregate = null;
        String state = null;
        String uid = null;
        String id = null;
        ScenarioTriggerType scenarioTriggerType;
        if (node.getNodeType() != Node.TEXT_NODE) {
            NamedNodeMap attributeList = node.getAttributes();
            for (int j = 0;j < attributeList.getLength(); j++) {
              Node attribute = attributeList.item(j);
              if(attribute.getNodeName().equals(ITEM_OPERATOR)) {
                  operator = attribute.getNodeValue();
              }else if (attribute.getNodeName().equals(ITEM_AGREGATE)) {
                  agregate = attribute.getNodeValue();
              }else if(attribute.getNodeName().equals(ITEM_TRIGGER_TYPE_ID)) {
                  state = attribute.getNodeValue();
              }else if(attribute.getNodeName().equals(DEVICE_DESKTOP_ID)) {
                  uid = attribute.getNodeValue();
              }else if(attribute.getNodeName().equals(VIRTUAL_STATE_DESKTOP_ID)) {
                  id = attribute.getNodeValue();
              }
            }
            switch (node.getNodeName()) {
            case CLAUSE_TAG:
                currentScenarioLogicBlock = new ScenarioLogicBlock();
                if(operator != null) {
                    switch (operator) {
                    case LOGIC_TYPE_OR:
                        currentScenarioLogicBlock.setLogicType(LogicType.OR);
                        break;
                    case LOGIC_TYPE_AND:
                        currentScenarioLogicBlock.setLogicType(LogicType.AND);
                        break;
                    case LOGIC_TYPE_AND_NOT:
                        currentScenarioLogicBlock.setLogicType(LogicType.AND_NOT);
                        break;
                    case LOGIC_TYPE_OR_NOT:
                        currentScenarioLogicBlock.setLogicType(LogicType.OR_NOT);
                        break;
                    default:
                        break;
                    }
                }
                previosLevelsubLogicList.add(currentScenarioLogicBlock);
                break;
            case ITEM_TAG:
                currentScenarioLogicBlock = new ScenarioLogicBlock();
                if (operator != null && agregate != null && state != null) {
                    if (operator.equals(ITEM_OPERATOR_IF) && agregate.equals(ITEM_AGREGATE_OR)) {
                        currentScenarioLogicBlock.setLogicType(LogicType.OR);
                    } else if (operator.equals(ITEM_OPERATOR_IF) && agregate.equals(ITEM_AGREGATE_AND)) {
                        currentScenarioLogicBlock.setLogicType(LogicType.AND);
                    } else if (operator.equals(ITEM_OPERATOR_IF_NOT) && agregate.equals(ITEM_AGREGATE_OR)) {
                        currentScenarioLogicBlock.setLogicType(LogicType.OR_NOT);
                    } else if (operator.equals(ITEM_OPERATOR_IF_NOT) && agregate.equals(ITEM_AGREGATE_AND)) {
                        currentScenarioLogicBlock.setLogicType(LogicType.AND_NOT);
                    }
                    scenarioTriggerType = scenarioTriggerTypeRepository.findByDesktopTriggerTypeName(state);
                    if(scenarioTriggerType != null) {
                        currentScenarioLogicBlock.setTriggerTypeId(scenarioTriggerType.getId());;
                    }
                }
                previosLevelsubLogicList.add(currentScenarioLogicBlock);
                break;
            case LOGIC_DEVICE_TAG:
                if(uid != null) {
                    if(deviceNewIdByDesktopId.containsKey(uid)) {
                        itemEntityIds.add(deviceNewIdByDesktopId.get(uid));
                    }
                }
                break;
            case VIRTUAL_STATE_TAG:
                if(id != null) {
                    if(virtualStateNewIdByDesktopNo.containsKey(id)) {
                        itemEntityIds.add(virtualStateNewIdByDesktopNo.get(id));
                    }
                }
                break;
            case REGION_TAG:
                    String regionId = node.getTextContent().replace("\n", "").trim();
                    if(regionId != null && !regionId.isEmpty()) {
                        if(regionNewIdByDesktopId.containsKey(regionId)) {
                            itemEntityIds.add(regionNewIdByDesktopId.get(regionId));
                        }
                    }
                break;
            default:
                break;
            }
        }
    }
    
    private void findChildren(NodeList nodelist) {
        int counter = -1;
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node node = nodelist.item(i);
            if (node.hasChildNodes()) {
                if (node.getNodeType() != Node.TEXT_NODE && !node.getNodeName().equals(REGION_TAG)) {
                    counter++;
                    if (node.getNodeName().equals(ITEM_TAG)) {
                        currentScenarioLogicBlock = parentElementEntityBylogiclevelCounter.get(logicLevelCounter - 1).getSubLogics().get(counter);
                        itemEntityIds = new ArrayList<>();
                    } else if (node.getNodeName().equals(CLAUSE_TAG)) {
                            parentElementEntityBylogiclevelCounter.put(logicLevelCounter, parentElementEntityBylogiclevelCounter.get(logicLevelCounter - 1).getSubLogics().get(counter));
                            if (parentElementEntityBylogiclevelCounter.containsKey(logicLevelCounter - 1)) {
                                previosScenarioLogicBlock = parentElementEntityBylogiclevelCounter.get(logicLevelCounter - 1).getSubLogics().get(counter);
                            }
                    }
                    logicLevelCounter++;
                    parseNode(node.getChildNodes());
                }
            }
        }
    }
    /*
     * В исполнительных блоках сценария, в действиях необходимо перепроверить ID сценариев если они не были найдены,
     * так как могут быть случае когда при обработки сценария, у него действиях в исполнительном блоке может происходить какое-то действия
     * со сценарием который еще не был создан и добавлен, поэтому проверям после создания и добавления всех сценариев проекта.
     */
    private void recheckScenarioAdditionInTimeLineBlockAction() {
        if (recheckScenarioDataListInScenario != null && !recheckScenarioDataListInScenario.isEmpty()) {
            for (RecheckScenarioData currentData : recheckScenarioDataListInScenario) {
                currentScenario = scenarioRepository.findByProjectIdAndId(projectId, currentData.getScenarioId());
                if (currentScenario != null) {
                    timeLineBlocks = currentScenario.getTimeLineBlocks();
                    scenarioTimeLineBlockActions = timeLineBlocks.get(currentData.getBlockNumber()).getActions();
                    applyScenarionTimeLineBlockActionExecutiveItem(currentData.getEntityType(),
                            currentData.getActionType(), currentData.getEntityId());
                    scenarioTimeLineBlockActions.add(currentData.getActionNumber(), currentScenarioTimeLineBlockAction);
                    timeLineBlocks.get(currentData.getBlockNumber()).setActions(scenarioTimeLineBlockActions);
                    for (ScenarioTimeLineBlock scenarioTimeLineBlock : timeLineBlocks) {
                        if (scenarioViewService.validateScenarioTimeLineBlock(scenarioTimeLineBlock,
                                new StringBuilder(), currentScenario.getScenarioPurpose(), false)) {
                            currentScenario.setTimeLineBlocks(timeLineBlocks);
                            scenarioRepository.save(currentScenario);
                        } else {
                            String description  = "Cценарий не был добавлен в исполнительный блок сценария \""
                                    .concat(currentScenario.getName()).concat("\"");
                            warningMessages.add(new ImportValidateMessage(Level.WARNING,Code.SCENARIO_NOT_ADDED_IN_SCENARIO_TIME_LINE_BLOCK, description, EntityType.SCENARIO, null));
                        }
                    }
                }
            }
        }
    }
    
    private void applyIndicatorPanelParametr(String paramName, String paramValue) {
        int MIN = 3;
        int MAX_ROW = 100;
        int MAX_COL = 20;
        String description = "Индикатор панелей \"" .concat(currentIndicatorPanel.getName()).concat("\" не добавлен");;
        try {
            if (paramName.equals(INDICATOR_PANEL_PARAM_COL_COUNT)) {
                int colCount = Integer.parseInt(paramValue);
                if (colCount >= MIN && colCount <= MAX_COL) {
                    currentIndicatorPanel.setColCount(colCount);
                }else {
                    passIndicatorPanel = true;
                    warningMessages.add(new ImportValidateMessage(Level.ERROR,Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
                    return;
                }
            } else if (paramName.equals(INDICATOR_PANEL_PARAM_ROW_COUNT)) {
                int rowCount = Integer.parseInt(paramValue);
                if (rowCount >= MIN && rowCount <= MAX_ROW) {
                    currentIndicatorPanel.setRowCount(rowCount);
                }else {
                    passIndicatorPanel = true;
                    warningMessages.add(new ImportValidateMessage(Level.ERROR,Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
                    return;
                }
            }
        } catch (Exception e) {
            passIndicatorPanel = true;
            warningMessages.add(new ImportValidateMessage(Level.ERROR,Code.VIRTUAL_INDICATOR_PANEL_NOT_ADDED, description, EntityType.VIRTUAL_INDICATOR_PANEL, null));
            return;
        }
    }
}