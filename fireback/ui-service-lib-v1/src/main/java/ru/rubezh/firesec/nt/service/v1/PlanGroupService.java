package ru.rubezh.firesec.nt.service.v1;

import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Plan;
import ru.rubezh.firesec.nt.domain.v1.PlanGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface PlanGroupService {

    public PlanGroup add(String projectId, Language language, PlanGroup planGroup,
            StringBuilder errorMessage);

    public PlanGroup update(String projectId, Language language, String planGroupId, PlanGroup planGroup,
            StringBuilder errorMessage);

    public class PlanGroupReferences {
        public List<Plan> plans = new ArrayList<>();
    }
    public PlanGroupReferences remove(String projectId, Language language, String planGroupId,
            StringBuilder errorMessage);

    public boolean checkAndAdditionPlanGroupToImportProject(String projectId, List<PlanGroup> planGroups, Map<String, String> oldAndNewPlanGroupsId);

}
