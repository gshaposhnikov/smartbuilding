package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceView.DevicePlanLayout;
import ru.rubezh.firesec.nt.domain.v1.representation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Order(1)
public class DeviceViewServiceImpl implements DeviceViewService, DeviceShapeLibraryService.DeviceShapeLibraryObserver,
        ActivationFilterableService {

    @Autowired
    private Logger logger;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceProfileViewRepository deviceProfileViewRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private ActiveDeviceRepository activeDeviceRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private MonitorableValueProfileViewRepository monitorableValueProfileViewRepository;

    @Autowired
    private ScenarioViewService scenarioViewService;

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private StateViewRepository stateViewRepository;

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private IndicatorPanelService indicatorPanelService;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    private DeviceShapeLibraryRepository deviceShapeLibraryRepository;

    private static final String DEFAULT_PARAMETER = "default";

    private static final String DEVICE_VIEW_NOT_FOUND_MSG = "Отображение устройства не найдено";

    /** Максимальное кол-во устройств, извлекаемое mongo-скриптом */
    public final static int AGGREGATED_ACTIVE_DEVICES_SAFE_PAGE_SIZE = 1000;

    /** Имя mongo-скрипта для получения активных представлений устройств
     * (элементов дерева) через id проекта и время их последнего обновления */
    private static final String GET_ACTIVE_DEVICES_SCRIPT_NAME =
            "getTreeItemActiveDeviceViewsByProjectIdAndTimeAfter";

    // Состояния, для которых возможна подстановка сообщений в ОЗ
    private static enum StatesForMessageReplace {
        am4OnOffState,
        am4OffOnState,
        am4OnOnState,
        run,
    }

    DeviceViewServiceImpl(){}

    @Autowired
    DeviceViewServiceImpl(
                          DeviceShapeLibraryService deviceShapeLibraryService,
                          DeviceShapeLibraryRepository deviceShapeLibraryRepository){
        this.deviceShapeLibraryRepository = deviceShapeLibraryRepository;
        deviceShapeLibraryService.addObserver(this);
    }

    private void calculateParentFullAddressPath(DeviceView deviceView, String projectId,
            Device device, StringBuilder fullAddressPath) {
        Device parentDevice = null;
        if (device.getParentDeviceId() != null && !device.getParentDeviceId().isEmpty())
            parentDevice = deviceRepository.findByProjectIdAndId(projectId, device.getParentDeviceId());
        if (parentDevice == null) {
            fullAddressPath.append(device.getLineAddress());
        } else {
            calculateParentFullAddressPath(deviceView, projectId, parentDevice, fullAddressPath);
            if (device.getDeviceCategory() != DeviceCategory.CONTAINER) {
                switch (device.getAddressType()) {
                case GENERIC:
                default:
                    fullAddressPath.append(".").append(device.getLineNo());
                    fullAddressPath.append(".").append(device.getLineAddress());
                    break;
                case INTERNAL:
                    fullAddressPath.append(".").append(device.getLineAddress());
                    break;
                case LACK:
                    fullAddressPath.append(".").append(deviceView.getName());
                    break;
                }
            }
        }
    }

    private void calculateRootDeviceAddressPaths(DeviceView deviceView) {
        DeviceProfileView deviceProfileView = deviceProfileViewRepository
                .findOne(deviceView.getDevice().getDeviceProfileId());
        assert deviceProfileView != null;

        String prefixedAddressPath = deviceProfileView.getRootAddressPathPrefix()
                + deviceView.getDevice().getLineAddress();
        deviceView.setFullAddressPath(prefixedAddressPath);
        deviceView.setMiddleAddressPath(prefixedAddressPath);
    }

    private void calculateNonRootDeviceFullAddressPath(DeviceView deviceView, String projectId) {
        assert (deviceView.getParentDeviceId() != null && !deviceView.getParentDeviceId().isEmpty());

        StringBuilder fullAddressPath = new StringBuilder();
        Device device = deviceView.getDevice();

        Device parentDevice = null;
        if (device.getParentDeviceId() != null && !device.getParentDeviceId().isEmpty())
            parentDevice = deviceRepository.findByProjectIdAndId(projectId, device.getParentDeviceId());
        if (parentDevice == null) {
            fullAddressPath.append(device.getLineAddress());
        } else {
            calculateParentFullAddressPath(deviceView, projectId, parentDevice, fullAddressPath);
            switch (device.getAddressType()) {
            case GENERIC:
            default:
                fullAddressPath.append(".").append(device.getLineNo());
                fullAddressPath.append(".").append(device.getLineAddress());
                break;
            case INTERNAL:
                fullAddressPath.append(".").append(device.getLineAddress());
                break;
            case LACK:
                fullAddressPath.append(".").append(deviceView.getName());
                break;
            }
        }

        deviceView.setFullAddressPath(fullAddressPath.toString());
    }

    private void calculateNonRootDeviceMiddleAddressPath(DeviceView deviceView, String projectId) {
        assert (deviceView.getParentDeviceId() != null && !deviceView.getParentDeviceId().isEmpty());

        Device device = deviceView.getDevice();

        switch (device.getAddressType()) {
        case GENERIC:
        default:
            deviceView.setMiddleAddressPath(
                    Integer.toString(device.getParentLineAddress()) + "."
                    + Integer.toString(device.getLineNo()) + "."
                    + Integer.toString(device.getLineAddress()));
            break;
        case INTERNAL:
            deviceView.setMiddleAddressPath(
                    Integer.toString(device.getParentLineAddress()) + "."
                    + Integer.toString(device.getLineAddress()));
            break;
        case LACK:
            deviceView.setMiddleAddressPath(
                    Integer.toString(device.getParentLineAddress()));
            break;
        }
    }

    private void calculateNonRootDeviceShortAddressPath(DeviceView deviceView, String projectId) {
        assert (deviceView.getParentDeviceId() != null && !deviceView.getParentDeviceId().isEmpty());

        Device device = deviceView.getDevice();

        if (device.getDeviceCategory() != DeviceCategory.CONTAINER) {
            switch (device.getAddressType()) {
            case GENERIC:
            default:
                deviceView.setShortAddressPath(
                        Integer.toString(device.getLineNo()) + "."
                        + Integer.toString(device.getLineAddress()));
                break;
            case INTERNAL:
                deviceView.setShortAddressPath(
                        Integer.toString(device.getLineAddress()));
                break;
            case LACK:
                deviceView.setShortAddressPath("");
                break;
            }
        } else {
            int nChilds = deviceRepository.countByProjectIdAndParentDeviceId(projectId, deviceView.getId());
            deviceView.setShortAddressPath(Integer.toString(device.getLineNo()) + "."
                    + Integer.toString(device.getLineAddress()) + " - "
                    + Integer.toString(device.getLineNo()) + "."
                    + Integer.toString(device.getLineAddress() + nChilds - 1));
        }
    }

    @Override
    public void calculateAddressPaths(DeviceView deviceView) {
        String parentDeviceId = deviceView.getParentDeviceId();
        String projectId = deviceView.getProjectId();

        if (parentDeviceId != null && !parentDeviceId.isEmpty()) {
            calculateNonRootDeviceFullAddressPath(deviceView, projectId);
            calculateNonRootDeviceMiddleAddressPath(deviceView, projectId);
            calculateNonRootDeviceShortAddressPath(deviceView, projectId);
        } else {
            calculateRootDeviceAddressPaths(deviceView);
        }
    }

    @Override
    public CreatedAndUpdatedEntities add(DeviceCreateView deviceInfo, String projectId, Language language,
            StringBuilder errorMessage) {
        CreatedAndUpdatedEntities createdAndUpdatedEntities = null;
        List<String> modifiedDeviceIds = new ArrayList<>();
        List<Device> savedDevices = deviceService.add(deviceInfo, projectId, modifiedDeviceIds, errorMessage);
        if (savedDevices != null) {
            createdAndUpdatedEntities = new CreatedAndUpdatedEntities(projectId);
            List<DeviceView> savedDeviceViews = new ArrayList<>();
            for (Device savedDevice : savedDevices) {
                DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                        savedDevice.getDeviceProfileId());
                if (deviceProfileView == null) {
                    logger.error("Профиль устройства не найден (profile id: {}, language: {})",
                            savedDevice.getDeviceProfileId(), language.toString());
                    continue;
                }
                DeviceView savedDeviceView = new DeviceView(savedDevice, deviceProfileView);
                calculateAddressPaths(savedDeviceView);
                savedDeviceView = deviceViewRepository.insert(savedDeviceView);
                savedDeviceViews.add(savedDeviceView);
            }
            createdAndUpdatedEntities.createdDevices = getTreeItemDeviceViewsByDeviceViews(projectId, language,
                    savedDeviceViews);
            if (!modifiedDeviceIds.isEmpty()) {
                createdAndUpdatedEntities.updatedDevices =
                        getTreeItemDeviceViewsByIds(projectId, modifiedDeviceIds, language);
            }
        }
        return createdAndUpdatedEntities;
    }

    @Override
    public RemovedAndUpdatedEntities remove(String projectId, String deviceId, Language language,
            StringBuilder errorMessage) {
        RemovedAndUpdatedEntities removedAndUpdatedEntities = null;
        List<String> updatedDeviceIds = new ArrayList<>();
        List<String> removedDeviceIds = deviceService.remove(projectId, deviceId, updatedDeviceIds, errorMessage);
        if (removedDeviceIds != null) {
            if (deviceViewRepository.deleteByProjectIdAndIdIn(projectId, removedDeviceIds) != removedDeviceIds.size()) {
                logger.warn("Count of deleted devices and views are not equal");
            }
            removedAndUpdatedEntities = new RemovedAndUpdatedEntities(projectId);
            removedAndUpdatedEntities.removedDeviceIds = removedDeviceIds;
            removedAndUpdatedEntities.updatedDevices =
                    getTreeItemDeviceViewsByIds(projectId, updatedDeviceIds, language);
            removedAndUpdatedEntities.updatedIndicatorPanels =
                    indicatorPanelService.removeEntityIdsFromIndicatorPanels(projectId,
                            removedDeviceIds, EntityType.DEVICE);
        }
        return removedAndUpdatedEntities;
    }

    private TreeItemDeviceView getTreeItemDeviceView(String projectId, Language language,
            DeviceView deviceView, DeviceProfileView deviceProfileView) {
        Device device = deviceView.getDevice();
        Region region = regionRepository.findByProjectIdAndId(projectId, device.getRegionId());
        List<DeviceProfileView> acceptableDeviceProfileViews = deviceProfileViewRepository
                .findByLanguageAndIdInOrderByIdAsc(language, device.getAcceptableDeviceProfileIds());

        List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews =
                getConfigPropertyViews(deviceProfileView, device, null);

        List<AggregatedActiveDeviceConfigPropertyView> otherPropertyViews =
                getOtherPropertyViews(deviceProfileView, device);

        Media alternateIcon = getIconProperty(device, DeviceProperties.AlternateIcon.toString());
        if (alternateIcon != null)
            deviceView.setIconMedia(alternateIcon);

        TreeItemDeviceView treeItemDeviceView = new TreeItemDeviceView(deviceView, deviceProfileView, region,
                acceptableDeviceProfileViews, configPropertyViews, otherPropertyViews);

        return treeItemDeviceView;
    }

    private TreeItemDeviceView getTreeItemDeviceView(String projectId, Language language,
            DeviceView deviceView) {
        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceView.getDevice().getDeviceProfileId());
        return getTreeItemDeviceView(projectId, language, deviceView, deviceProfileView);
    }

    private List<TreeItemDeviceView> getTreeItemDeviceViewsByIds(String projectId, List<String> deviceIds,
            Language language) {
        List<DeviceView> deviceViews = deviceViewRepository.findByProjectIdAndIdIn(projectId, deviceIds);
        return getTreeItemDeviceViewsByDeviceViews(projectId, language, deviceViews);
    }

    @Override
    public List<TreeItemDeviceView> getTreeItemDeviceViewsByDeviceViews(String projectId, Language language,
            List<DeviceView> deviceViews) {
        List<TreeItemDeviceView> treeItemDeviceViews = new ArrayList<>();

        // поиск зон, отображений профилей устройств и альтернативных иконок в базе
        Set<String> deviceProfileIds = new HashSet<>();
        Set<String> regionIds = new HashSet<>();
        Set<String> iconIds = new HashSet<>();
        for (DeviceView deviceView : deviceViews) {
            Device device = deviceView.getDevice();
            deviceProfileIds.add(device.getDeviceProfileId());
            deviceProfileIds.addAll(device.getAcceptableDeviceProfileIds());
            if (device.getRegionId() != null)
                regionIds.add(device.getRegionId());
            String alternateIconId = DeviceProperty.getString(device, DeviceProperties.AlternateIcon);
            if (alternateIconId != null)
                iconIds.add(alternateIconId);
        }
        Map<String, DeviceProfileView> deviceProfileViewsById =
                deviceProfileViewRepository.findByLanguageAndIdIn(language, deviceProfileIds)
                .stream().collect(Collectors.toMap(DeviceProfileView::getId, view -> view));
        Map<String, Region> regionsById = regionRepository.findByProjectIdAndIdIn(projectId, regionIds)
                .stream().collect(Collectors.toMap(Region::getId, region -> region));
        Map<String, Media> iconsById = mediaRepository.findByIdIn(iconIds)
                .stream().collect(Collectors.toMap(Media::getId, icon -> icon));

        // формирование отображений элементов дерева устройств
        List<Device> devices = deviceViews.stream().map(DeviceView::getDevice).collect(Collectors.toList());
        for (DeviceView deviceView : deviceViews) {
            TreeItemDeviceView treeItemDeviceView = getTreeItemDeviceView(deviceView,
                    deviceProfileViewsById, regionsById, iconsById, devices);
            if (treeItemDeviceView != null)
                treeItemDeviceViews.add(treeItemDeviceView);
        }

        return treeItemDeviceViews;
    }

    public TreeItemDeviceView getTreeItemDeviceView(DeviceView deviceView,
                                                    Map<String, DeviceProfileView> deviceProfileViewsById,
                                                    Map<String, Region> regionsById, Map<String, Media> iconsById,
                                                    List<Device> devices) {
        Device device = deviceView.getDevice();

        // отображение профиля
        String deviceProfileId = device.getDeviceProfileId();
        DeviceProfileView deviceProfileView = deviceProfileViewsById.get(deviceProfileId);
        if (deviceProfileView == null) {
            logger.error("Отображение профиля устройства не найдено (profile id: {})", deviceProfileId);
            return null;
        }

        // зона, если есть
        String regionId = device.getRegionId();
        Region region = null;
        if (regionId != null) {
            region = regionsById.get(regionId);
            if (region == null) {
                logger.error("Зона не найдена (region id: {})", regionId);
            }
        }

        // отображения профилей из списка допустимых замен
        Set<String> acceptableDeviceProfileIds = device.getAcceptableDeviceProfileIds();
        acceptableDeviceProfileIds.retainAll(deviceProfileViewsById.keySet());
        List<DeviceProfileView> acceptableDeviceProfileViews = new ArrayList<>();
        for (String acceptableDeviceProfileId : acceptableDeviceProfileIds)
            acceptableDeviceProfileViews.add(deviceProfileViewsById.get(acceptableDeviceProfileId));

        // отображения папарметров
        List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews =
                getConfigPropertyViews(deviceProfileView, device, null);
        List<AggregatedActiveDeviceConfigPropertyView> otherPropertyViews =
                getOtherPropertyViews(deviceProfileView, device);

        // альтернативная иконка, если есть
        String alternateIconId = DeviceProperty.getString(device, DeviceProperties.AlternateIcon);
        if (alternateIconId != null) {
            Media alternateIcon = iconsById.get(alternateIconId);
            if (alternateIcon != null) {
                deviceView.setIconMedia(alternateIcon);
            } else {
                logger.warn("Не найдена альтернативная иконка (media id: {})", alternateIconId);
            }
        }

        // элемент дерева
        return new TreeItemDeviceView(deviceView, deviceProfileView, region,
                acceptableDeviceProfileViews, configPropertyViews, otherPropertyViews);
    }

    @Override
    public TreeItemDeviceView getDeviceTreeItemByProjectIdAndDeviceId(String projectId, String deviceId, Language language) {
        TreeItemDeviceView result = null;

        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView != null) {
            result = getTreeItemDeviceView(projectId, language, deviceView);
        }

        return result;
    }

    @Override
    public List<TreeItemDeviceView> getTreeItemDeviceViewsByProjectId(String projectId, Language language) {
        /* Два запроса представления устройств(без устройств) и устройств работают
           быстрее чем один запрос представлений устройств(включающий устройства) */
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("projectId").is(projectId)),
                Aggregation.project("projectId", "parentDeviceId", "addressPath", "addressLevel",
                        "fullAddressPath", "middleAddressPath", "shortAddressPath",
                        "iconMedia", "textureMedia", "planLayouts", "deviceShapeLibraryId"));
        AggregationResults<DeviceView> aggResults = mongoTemplate.aggregate(aggregation, "device_views", DeviceView.class);
        List<DeviceView> deviceViews = aggResults.getMappedResults();
        Map<String,Device> devices = deviceRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Device::getId, Function.identity()));
        for (DeviceView deviceView: deviceViews) {
            deviceView.setDevice(devices.get(deviceView.getId()));
        }
        return getTreeItemDeviceViewsByDeviceViews(projectId, language, deviceViews);
    }

    @Override
    public TreeItemActiveDeviceView getActiveDeviceTreeItemByProjectIdAndDeviceId(String projectId,
            String deviceId, Language language) {
        TreeItemActiveDeviceView  result = null;
        ActiveDevice activeDevice = activeDeviceRepository.findOne(deviceId);

        if (activeDevice != null) {
            Device device = activeDevice.getDeviceProject();

            DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, activeDevice.getId());

            Region region = regionRepository.findByProjectIdAndId(projectId, device.getRegionId());

            List<DeviceProfileView> acceptableDeviceProfileViews = deviceProfileViewRepository
                    .findByLanguageAndIdIn(language, device.getAcceptableDeviceProfileIds());
            DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                    device.getDeviceProfileId());

            boolean statePolling = true;
            if (deviceProfileView.getDeviceProfile().isIgnorable()
                    && deviceProfileView.getDeviceProfile().getDeviceIgnoreStateId() != null) {
                statePolling = !activeDevice.getActiveStates()
                        .contains(deviceProfileView.getDeviceProfile().getDeviceIgnoreStateId());
            }

            StateCategoryView generalStateCategoryView = stateViewService
                    .getStateCategoryViewById(activeDevice.getGeneralStateCategoryId());

            List<StateView> activeStateViews = stateViewRepository.findByIdIn(activeDevice.getActiveStates());
            List<AggregatedStateView> aggregatedStateViews = getAggregatedActiveStateViews(activeStateViews,
                    device.getDevicePropertyValues());

            List<MonitorableValueProfileView> monitorableValueProfileViews = monitorableValueProfileViewRepository
                    .findByIdIn(activeDevice.getMonitorableValues().keySet());
            List<TreeItemActiveDeviceView.ItemMonitorableValue> aggregatedMonitorableValueViews = getAggregatedMonitorableValueViews(
                    monitorableValueProfileViews, activeDevice.getMonitorableValues());

            List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews =
                    getConfigPropertyViews(deviceProfileView, device, activeDevice);
            List<AggregatedActiveDeviceConfigPropertyView> otherPropertyViews = getOtherPropertyViews(deviceProfileView, device);

            result = new TreeItemActiveDeviceView(deviceView, deviceProfileView, region,
                    acceptableDeviceProfileViews, statePolling, generalStateCategoryView,
                    aggregatedStateViews, aggregatedMonitorableValueViews, configPropertyViews, otherPropertyViews,
                    activeDevice.getMonitorableValuesTimestamp(), activeDevice.getNotes());

        }

        return result;
    }

    private List<TreeItemActiveDeviceView.ItemMonitorableValue> getAggregatedMonitorableValueViews(
            List<MonitorableValueProfileView> monitorableValueProfileViews,
            Map<String, String> deviceMonitorableValues) {

        List<TreeItemActiveDeviceView.ItemMonitorableValue> aggregatedMonitorableValueViews = new ArrayList<>();
        for (MonitorableValueProfileView monitorableValueProfileView : monitorableValueProfileViews) {
            aggregatedMonitorableValueViews.add(new TreeItemActiveDeviceView.ItemMonitorableValue(
                    monitorableValueProfileView.getId(), monitorableValueProfileView.getName(),
                    monitorableValueProfileView.getDescription(), monitorableValueProfileView.getUnit(),
                    deviceMonitorableValues.get(monitorableValueProfileView.getId())));
        }
        return aggregatedMonitorableValueViews;
    }

    private List<AggregatedStateView> getAggregatedActiveStateViews(List<StateView> activeStateViews,
            Map<String, String> devicePropertyValues) {
        List<AggregatedStateView> aggregatedStateViews = new ArrayList<>();
        for (StateView stateView : activeStateViews) {
            State state = stateView.getState();
            if (state.getSubsystem() == Subsystem.UNKNOWN) {
                state.setSubsystem(state.getStateCategory().getSubsystem());
            }
            replaceTextIfNeeded(stateView, devicePropertyValues);
            aggregatedStateViews.add(new AggregatedStateView(stateView,
                    stateViewService.getStateCategoryViewById(state.getStateCategory().getId())));
        }
        return aggregatedStateViews;
    }

    private Media getIconProperty(Device device, String propertyName) {
        String iconMediaId = device.getDevicePropertyValues().get(propertyName);
        if ((iconMediaId == null) || iconMediaId.isEmpty())
            return null;
        else
            return mediaRepository.findOne(iconMediaId);
    }

    private String getTextProperty(String propertyName, Map<String, String> properties) {
        String text = properties.get(propertyName);
        if ((text == null) || text.isEmpty())
            return null;
        else
            return text;
    }

    private void replaceTextIfNeeded(StateView stateView, Map<String, String> properties) {
        String textPropertyName = null;
        try {
            switch (StatesForMessageReplace.valueOf(stateView.getState().getId())) {
            case am4OnOffState:
                textPropertyName = DeviceProperties.FirstTriggeredMessage.toString();
                break;
            case am4OffOnState:
                textPropertyName = DeviceProperties.SecondTriggeredMessage.toString();
                break;
            case am4OnOnState:
                textPropertyName = DeviceProperties.BothTriggeredMessage.toString();
                break;
            case run:
                textPropertyName = DeviceProperties.EnabledMessage.toString();
                break;
            default:
                break;
            }
        } catch (IllegalArgumentException ex) {
        }
        if (textPropertyName != null) {
            String alternateText = getTextProperty(textPropertyName, properties);
            if (alternateText != null)
                stateView.setName(alternateText);
        }
    }

    @Override
    public List<TreeItemActiveDeviceView> getActiveDeviceTreeItemByProjectIdAndRegionId(String projectId,
            String regionId, Language language) {
        List<TreeItemActiveDeviceView> results = new ArrayList<>();

        // Получение устройств, подключенных к зоне
        List<ActiveDevice> activeDevices = activeDeviceRepository.findByProjectIdAndRegionId(projectId, regionId);

        // Выявление адресов родительских устройств
        Set<String> paths = new HashSet<>();
        for (ActiveDevice activeDevice : activeDevices) {
            String[] addressPath = activeDevice.getAddressPath().split(",");
            String path = "";
            for (int i = 0; i + 1 < addressPath.length; i += 2) {
                if (i > 0)
                    path += "," + addressPath[i];
                path += "," + addressPath[i + 1];
                if (!path.equals(activeDevice.getAddressPath()))
                    paths.add(path);
            }
        }

        // Получение родительских устройств
        activeDevices.addAll(activeDeviceRepository.findByAddressPathIn(paths));

        // Формирование представлений
        for (ActiveDevice activeDevice : activeDevices) {
            results.add(getActiveDeviceTreeItemByProjectIdAndDeviceId(projectId, activeDevice.getId(), language));
        }

        return results;
    }

    @Override
    public List<TreeItemActiveDeviceView> getActiveDeviceTreeItemsByProjectId(String projectId,
                                                                              long pageSize, int pageNo) {
        return getActiveDeviceTreeItemsByProjectIdAndTimeAfter(projectId, null, pageSize, pageNo);
    }

    @Override
    public List<TreeItemActiveDeviceView> getActiveDeviceTreeItemsByTimeAfter(Date lastObservedTime,
                                                                              long pageSize, int pageNo) {
        return getActiveDeviceTreeItemsByProjectIdAndTimeAfter(null, lastObservedTime, pageSize, pageNo);
    }

    private List<TreeItemActiveDeviceView> getActiveDeviceTreeItemsByProjectIdAndTimeAfter(String projectId,
                                                                                          Date lastObservedTime,
                                                                                          long pageSize, int pageNo) {
        List<TreeItemActiveDeviceViewFromScript> deviceViewsFromScript = new ArrayList<>();
        try {
            /* Перенаправляем запрос в mongo-скрипт */
            ScriptOperations scriptOperations = mongoTemplate.scriptOps();
            String lastObservedTimeString = null;
            if (lastObservedTime != null){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                lastObservedTimeString = simpleDateFormat.format(lastObservedTime);
            }
            BasicDBList dbList = (BasicDBList) scriptOperations.call(GET_ACTIVE_DEVICES_SCRIPT_NAME,
                    projectId, lastObservedTimeString, pageSize, pageNo);
            for (Object dbObject : dbList) {
                deviceViewsFromScript.add(mongoTemplate.getConverter().read(TreeItemActiveDeviceViewFromScript.class,
                        (DBObject) dbObject));
            }
        } catch (Exception e) {
            logger.error("Failed get active devices tree items via mongo-script, exception: ", e);
            throw e;
        }

        /*
         * Дозаполним сложные поля с использованием временных полей - помощников
         */
        for (TreeItemActiveDeviceViewFromScript deviceView : deviceViewsFromScript) {
            deviceView.setActiveStateViews(getAggregatedActiveStateViews(deviceView.getRawActiveStateViews(),
                    deviceView.getDevicePropertyValues()));
            deviceView.setMonitorableValueViews(getAggregatedMonitorableValueViews(
                    deviceView.getMonitorableValueProfileViews(), deviceView.getDeviceMonitorableValues()));

            deviceView.setConfigPropertyViews(getConfigPropertyViews(deviceView.getDeviceConfigProperties(),
                    deviceView.getDeviceConfigPropertyViews(), deviceView.getProjectDeviceConfigValues(),
                    deviceView.getActiveDeviceConfigValues()));

            deviceView.setAggregatedPropertyViews(getOtherPropertyViews(deviceView.getDeviceOtherProperties(),
                    deviceView.getDeviceOtherPropertyViews(), deviceView.getProjectDeviceOtherValues()));

            deviceView.setLinkedDeviceIds(deviceView.getLinkedDeviceIdsMap().values()
                    .stream().flatMap(Collection::stream).collect(Collectors.toList()));

            deviceView.setAggregatedSupportedActions(TreeItemActiveDeviceView.createAggregatedActions(
                    deviceView.getSupportedActions(),
                    deviceView.getSupportedActionViews()));
        }

        return new ArrayList<>(deviceViewsFromScript);
    }

    @Override
    public void safelyWriteActiveDeviceTreeItemByProjectId(String projectId, OutputStream outputStream,
                                                           ObjectMapper objectMapper) throws IOException {
        long nDevicePages = (activeDeviceRepository.countByProjectId(projectId)
                / AGGREGATED_ACTIVE_DEVICES_SAFE_PAGE_SIZE) + 1;
        outputStream.write("[".getBytes());
        for (int pageNo = 0; pageNo < nDevicePages; ++pageNo) {
            List<TreeItemActiveDeviceView> devices = getActiveDeviceTreeItemsByProjectId(projectId,
                    AGGREGATED_ACTIVE_DEVICES_SAFE_PAGE_SIZE, pageNo);
            for (int deviceNo = 0; deviceNo < devices.size(); ++deviceNo) {
                TreeItemActiveDeviceView device = devices.get(deviceNo);
                outputStream.write(objectMapper.writeValueAsBytes(device));
                if (pageNo < (nDevicePages - 1) || deviceNo < (devices.size() - 1)) {
                    outputStream.write(",".getBytes());
                }
            }
            outputStream.flush();
        }
        outputStream.write("]".getBytes());
    }

    private List<AggregatedActiveDeviceConfigPropertyView> getConfigPropertyViews(
            DeviceProfileView deviceProfileView, Device device, ActiveDevice activeDevice) {
        if (device != null) {
            return getConfigPropertyViews(
                    deviceProfileView.getDeviceProfile().getConfigProperties(),
                    deviceProfileView.getConfigPropertyViews(), device.getProjectConfigValues(),
                    (activeDevice != null) ? activeDevice.getActiveConfigValues() : null);
        } else {
            return null;
        }
    }

    private List<AggregatedActiveDeviceConfigPropertyView> getConfigPropertyViews(
            Map<String, DeviceConfigProperty> configProperties,
            Map<String, DeviceConfigPropertyView> configPropertyViews, Map<String, String> projectConfigValues,
            Map<String, String> activeConfigValues) {

        List<AggregatedActiveDeviceConfigPropertyView> properties = new ArrayList<>();
        if (configProperties != null) {
            for (String propertyId : configProperties.keySet()) {
                DeviceConfigProperty property = configProperties.get(propertyId);
                if (!property.isHidden()) {
                    DeviceConfigPropertyView view = configPropertyViews.get(propertyId);
                    String activeValue = (activeConfigValues != null) ? activeConfigValues.get(propertyId) : null;
                    String projectValue = projectConfigValues.get(propertyId);
                    AggregatedActiveDeviceConfigPropertyView aggregatedView = new AggregatedActiveDeviceConfigPropertyView(
                            propertyId, property, view, activeValue, projectValue);
                    properties.add(aggregatedView);
                }
            }
        }

        return properties;
    }

    private List<AggregatedActiveDeviceConfigPropertyView> getOtherPropertyViews(
            Map<String, DeviceProperty> otherProperties, Map<String, DeviceConfigPropertyView> otherPropertyViews,
            Map<String, String> projectValues) {

        List<AggregatedActiveDeviceConfigPropertyView> properties = new ArrayList<>();
        for (String propertyId : otherProperties.keySet())
            properties.add(new AggregatedActiveDeviceConfigPropertyView(propertyId,
                    otherProperties.get(propertyId), otherPropertyViews.get(propertyId), null, // activeValue
                    projectValues.get(propertyId)));
        return properties;
    }

    private List<AggregatedActiveDeviceConfigPropertyView> getOtherPropertyViews(
            DeviceProfileView deviceProfileView, Device device) {
        return getOtherPropertyViews(
                deviceProfileView.getDeviceProfile().getDeviceProperties(),
                deviceProfileView.getPropertyViews(),
                device.getDevicePropertyValues());
    }

    @Override
    public CreatedAndUpdatedEntities updateDeviceProfile(String projectId, String deviceId,
            String deviceProfileId, Language language, StringBuilder errorMessage) {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append("Устройство с идентификатором ").append(deviceId)
                    .append(" или его отображение не найдены");
            return null;
        }

        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceProfileId);
        if (deviceProfileView == null) {
            errorMessage.append("Отображение профиля устройства не найдено");
            return null;
        }

        if (!deviceService.setDeviceProfile(deviceView.getDevice(), deviceProfileId, errorMessage)) {
            return null;
        }

        deviceView.apply(deviceProfileView);
        CreatedAndUpdatedEntities createdAndUpdatedEntities = new CreatedAndUpdatedEntities(projectId);
        deviceView = deviceViewRepository.save(deviceView);
        createdAndUpdatedEntities.updatedDevices
                .add(getTreeItemDeviceView(projectId, language, deviceView));
        return createdAndUpdatedEntities;
    }

    @Override
    public CreatedAndUpdatedEntities updateDeviceAddress(String projectId, String deviceId,
            String shortAddressPath, Language language, StringBuilder errorMessage) {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append("Устройство с идентификатором ").append(deviceId)
                    .append(" или его отображение не найдены");
            return null;
        }

        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceView.getDevice().getDeviceProfileId());
        if (deviceProfileView == null) {
            errorMessage.append("Профиль устройства или отображение профиля не найдены");
            return null;
        }

        if (deviceView.getParentDeviceId() == null || deviceView.getParentDeviceId().isEmpty()) {
            /* Корневое устройство */
            return updateRootDeviceAddress(projectId, deviceView,
                    shortAddressPath, language, errorMessage);
        } else {
            /* Устройство с родителем */
            return updateNonRootDeviceAddress(projectId, deviceView,
                    shortAddressPath, language, errorMessage);
        }
    }

    private CreatedAndUpdatedEntities updateNonRootDeviceAddress(String projectId, DeviceView deviceView,
            String shortAddressPath, Language language, StringBuilder errorMessage) {

        String[] addressPathTokens = shortAddressPath.split("\\.");
        if (addressPathTokens.length == 2) {
            try {
                int lineNo = Integer.valueOf(addressPathTokens[0]);
                int address = Integer.valueOf(addressPathTokens[1]);

                Device updatedDevice = deviceService.updateNonRootDeviceAddress(deviceView.getDevice(),
                        lineNo, address, errorMessage);
                if (updatedDevice != null) {
                    /*
                     * Обновляем отображение устройства, у которого изменили
                     * адрес
                     */
                    updateAddressPathFromLogicEntity(deviceView, updatedDevice);
                    deviceView = deviceViewRepository.save(deviceView);

                    /* Обновляем отображения устройств - потомков */
                    List<DeviceView> updatedChildDeviceViews = new ArrayList<>();
                    updatedChildDeviceViews
                            .addAll(updateChildDeviceViewsAddressPaths(deviceView));
                    updatedChildDeviceViews = deviceViewRepository.save(updatedChildDeviceViews);

                    /*
                     * Получаем отображение устройства - предка (у него должны
                     * обновиться диапазоны свободных адресов)
                     */
                    DeviceView parentDeviceView = deviceViewRepository.findByProjectIdAndId(projectId,
                            updatedDevice.getParentDeviceId());

                    /*
                     * Формируем список всех измененных элементов для
                     * отображения дерева
                     */
                    List<DeviceView> allUpdatedDeviceViews = new ArrayList<>();
                    /* Измененное устройство */
                    allUpdatedDeviceViews.add(deviceView);
                    /* Потомки измененного устройства */
                    allUpdatedDeviceViews.addAll(updatedChildDeviceViews);
                    /* Предок измененного устройства */
                    allUpdatedDeviceViews.add(parentDeviceView);

                    CreatedAndUpdatedEntities createdAndUpdatedEntities = new CreatedAndUpdatedEntities(projectId);
                    createdAndUpdatedEntities.updatedDevices = getTreeItemDeviceViewsByDeviceViews(projectId,
                            language, allUpdatedDeviceViews);

                    /*
                     * Формируем список сценариев, которые ссылались на
                     * измененные устройства
                     */
                    List<String> updatedDeviceIds = new ArrayList<>();
                    for (DeviceView updatedDeviceView : allUpdatedDeviceViews) {
                        updatedDeviceIds.add(updatedDeviceView.getId());
                    }
                    createdAndUpdatedEntities.updatedScenarios = scenarioViewService
                            .getAggregatedScenarioViewsContainingDevices(projectId, updatedDeviceIds);

                    return createdAndUpdatedEntities;
                }
            } catch (NumberFormatException numberFormatException) {
                errorMessage.append("Ошибка преобразования строки к числу");
            }
        } else {
            errorMessage.append("В новом адресе должны быть заданы 2 числа через точку: номер линии и адрес на линии");
        }

        return null;
    }

    private void updateAddressPathFromLogicEntity(DeviceView deviceView, Device logicEntity) {
        String projectId = deviceView.getProjectId();

        /* Обновляем адресацию, наследуемую от логической сущности */
        deviceView.apply(logicEntity);

        /* Вычисляем новые адресные пути отображения */
        calculateNonRootDeviceFullAddressPath(deviceView, projectId);
        calculateNonRootDeviceMiddleAddressPath(deviceView, projectId);
        calculateNonRootDeviceShortAddressPath(deviceView, projectId);
    }

    private CreatedAndUpdatedEntities updateRootDeviceAddress(String projectId, DeviceView deviceView,
            String shortAddressPath, Language language, StringBuilder errorMessage) {
        try {
            /* Обновляем адресацию на уровне логики */
            Device device = deviceService.updateRootDeviceAddress(deviceView.getDevice(),
                    Integer.valueOf(shortAddressPath), errorMessage);
            if (device != null) {
                /*
                 * Применяем новую адресацию к отображению корневого устройства
                 */
                deviceView.apply(device);
                calculateRootDeviceAddressPaths(deviceView);

                /* Обновляем адресацию всех потомков */
                List<DeviceView> updatedDeviceViews = updateChildDeviceViewsAddressPaths(deviceView);

                /*
                 * Сохраняем отображения корневого устройства и всех измененных
                 * потомков
                 */
                deviceView = deviceViewRepository.save(deviceView);
                updatedDeviceViews = deviceViewRepository.save(updatedDeviceViews);

                /*
                 * Формируем список всех измененных элементов для отображения
                 * дерева
                 */
                List<DeviceView> allUpdatedDeviceViews = new ArrayList<>();
                allUpdatedDeviceViews.add(deviceView);
                allUpdatedDeviceViews.addAll(updatedDeviceViews);

                CreatedAndUpdatedEntities createdAndUpdatedEntities = new CreatedAndUpdatedEntities(projectId);
                createdAndUpdatedEntities.updatedDevices = getTreeItemDeviceViewsByDeviceViews(projectId,
                        language, allUpdatedDeviceViews);

                List<String> updatedDeviceIds = new ArrayList<>();
                for (DeviceView updatedDeviceView : allUpdatedDeviceViews) {
                    updatedDeviceIds.add(updatedDeviceView.getId());
                }
                createdAndUpdatedEntities.updatedScenarios = scenarioViewService
                        .getAggregatedScenarioViewsContainingDevices(projectId, updatedDeviceIds);

                return createdAndUpdatedEntities;
            }
        } catch (NumberFormatException numberFormatException) {
            errorMessage.append("Адрес корневого устройства должен быть числом");
        }
        return null;
    }

    private List<DeviceView> updateChildDeviceViewsAddressPaths(DeviceView parentDeviceView) {
        List<DeviceView> updatedDeviceViews = new ArrayList<>();
        List<DeviceView> childDeviceViews = deviceViewRepository
                .findByProjectIdAndParentDeviceId(parentDeviceView.getProjectId(), parentDeviceView.getId());

        for (DeviceView childDeviceView : childDeviceViews) {
            /* Обновляем адресацию, наследуемую от логической сущности */
            updateAddressPathFromLogicEntity(childDeviceView, childDeviceView.getDevice());

            /* Спускаемся на следующий уровень дерева */
            updatedDeviceViews.addAll(updateChildDeviceViewsAddressPaths(childDeviceView));
        }
        updatedDeviceViews.addAll(childDeviceViews);
        return updatedDeviceViews;
    }


    @Override
    public TreeItemDeviceView updateDevicePlanLayouts(String projectId, String deviceId,
            List<DevicePlanLayout> planLayouts, Language language, StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
            if (deviceView != null) {
                boolean allPlansExist = true;
                Map<String, Plan> projectPlans = new HashMap<>();

                for (DevicePlanLayout devicePlanLayout : planLayouts) {
                    String planId = devicePlanLayout.getPlanId();
                    if (!projectPlans.containsKey(planId)) {
                        Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
                        if (plan != null) {
                            projectPlans.put(planId, plan);
                        } else {
                            errorMessage.append("Не найден план с идентификатором ").append(planId);
                            allPlansExist = false;
                            break;
                        }
                    }

                    CoordinatePoint deviceCoordinatePoint = devicePlanLayout.getCoordinatePoint();
                    if (
                        deviceCoordinatePoint.getX() < 0.0 ||
                        deviceCoordinatePoint.getX() > projectPlans.get(planId).getxSize() ||
                        deviceCoordinatePoint.getY() < 0.0 ||
                        deviceCoordinatePoint.getY() > projectPlans.get(planId).getySize()
                    ) {
                        errorMessage.append("Устройство выходит за пределы плана");
                        allPlansExist = false;
                        break;
                    }
                }
                if (allPlansExist) {
                    deviceView.setPlanLayouts(planLayouts);
                    deviceView = deviceViewRepository.save(deviceView);
                    return getTreeItemDeviceView(projectId, language, deviceView);
                }
            } else {
                errorMessage.append("Устройство не найдено в проекте");
            }
        }

        return null;
    }

    private boolean updateIconMediaProperty(DeviceView deviceView, DeviceProfileView deviceProfileView,
            String propertyValue, StringBuilder errorMessage) {
        String iconMediaId = deviceProfileView.getIconMediaId();
        if (propertyValue != null && !propertyValue.isEmpty()) {
            iconMediaId = propertyValue;
        }
        Media iconMedia = mediaRepository.findOne(iconMediaId);
        if (iconMedia == null) {
            errorMessage.append("Не найден медиа-ресурс: ").append(iconMediaId);
            return false;
        }
        if (iconMedia.getMediaType() != MediaType.ICON) {
            errorMessage.append("Недопустимый тип ").append(iconMedia.getMediaType().toString())
                    .append("медиа-ресурса ").append(iconMediaId);
            return false;
        }
        deviceView.setIconMedia(iconMedia);
        return true;
    }

    private boolean updateNameProperty(DeviceView deviceView, DeviceProfileView deviceProfileView, String propertyValue,
            StringBuilder errorMessage) {
        String deviceAlias = deviceProfileView.getName();
        if (propertyValue != null && !propertyValue.isEmpty()) {
            deviceAlias = propertyValue;
        }
        deviceView.setName(deviceAlias);
        return true;
    }

    @Override
    public TreeItemDeviceView updateDeviceProperties(String projectId, String deviceId,
            Map<String, String> propertyValues, Language language, StringBuilder errorMessage) {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        boolean deviceViewUpdated = false;
        /* Проверим среди изменяемых совойств наличие свойств отображения */
        if (propertyValues.containsKey(DeviceProperties.AlternateIcon.toString())
                || propertyValues.containsKey(DeviceProperties.AlternateName.toString())) {
            DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                    deviceView.getDevice().getDeviceProfileId());

            if (propertyValues.containsKey(DeviceProperties.AlternateIcon.toString())) {
                if (!updateIconMediaProperty(deviceView, deviceProfileView,
                        propertyValues.get(DeviceProperties.AlternateIcon.toString()), errorMessage)) {
                    return null;
                }
            }

            if (propertyValues.containsKey(DeviceProperties.AlternateName.toString())) {
                if (!updateNameProperty(deviceView, deviceProfileView,
                        propertyValues.get(DeviceProperties.AlternateName.toString()), errorMessage)) {
                    return null;
                }
            }
            deviceViewUpdated = true;
        }
        Device device = deviceService.setDeviceProperties(deviceId, projectId, propertyValues, errorMessage);
        if (device != null) {
            /* Актуализируем сущность устройства внутри отображения */
            deviceView.setDevice(device);
            if (deviceViewUpdated) {
                /*
                 * Сохраняем отображение устройства только после успешного
                 * обновления самого устройства
                 */
                deviceView = deviceViewRepository.save(deviceView);
            }
            return getTreeItemDeviceView(projectId, language, deviceView);
        }
        return null;
    }

    @Override
    public TreeItemDeviceView updateDeviceDisabled(String projectId, String deviceId,
            boolean disabled, Language language, StringBuilder errorMessage) {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append("Отображение устройства не найдено");
            return null;
        }
        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceView.getDevice().getDeviceProfileId());
        if (deviceProfileView == null) {
            errorMessage.append("Отображение профиля устройства не найдено");
            return null;
        }
        Device device = deviceService.setDeviceDisabled(deviceView.getDevice(), deviceProfileView.getDeviceProfile(),
                disabled, errorMessage);
        if (device == null) {
            return null;
        }
        deviceView.setDevice(device);
        return getTreeItemDeviceView(projectId, language, deviceView, deviceProfileView);
    }

    @Override
    public List<TreeItemDeviceView> updateDeviceLinks(String projectId, String deviceId,
            List<String> linksUpdate, Language language, StringBuilder errorMessage) {
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append(DEVICE_VIEW_NOT_FOUND_MSG);
            return null;
        }
        DeviceProfileView deviceProfileView = deviceProfileViewRepository.findByLanguageAndId(language,
                deviceView.getDevice().getDeviceProfileId());
        if (deviceProfileView == null) {
            errorMessage.append("Отображение профиля устройства не найдено");
            return null;
        }
        List<String> modifiedDeviceIds = deviceService.updateLinks(
                deviceView.getDevice(), deviceProfileView.getDeviceProfile(), linksUpdate, errorMessage);
        if (modifiedDeviceIds == null) {
            return null;
        }
        return getTreeItemDeviceViewsByIds(projectId, modifiedDeviceIds, language);
    }

    public boolean checkAndAdditionDeviceViewToImportProject(String projectId, List<DeviceView> deviceViews,
            Map<String, String> oldAndNewPlansId, Map<String, String> oldAndNewDevicesId, StringBuilder errorMessage) {
        Map<String, Plan> projectPlans = new HashMap<>();
        for (DeviceView deviceView : deviceViews) {
            deviceView.apply(deviceRepository.findByProjectIdAndId(projectId,
                    oldAndNewDevicesId.get(deviceView.getDevice().getId())));
            for (DevicePlanLayout devicePlanLayout : deviceView.getPlanLayouts()) {
                if ((devicePlanLayout.getPlanId() != null)
                        && oldAndNewPlansId.containsKey(devicePlanLayout.getPlanId())) {
                    devicePlanLayout.setPlanId(oldAndNewPlansId.get(devicePlanLayout.getPlanId()));
                }
            }
            if (deviceView.getParentDeviceId() != null && !deviceView.getParentDeviceId().isEmpty()) {
                calculateNonRootDeviceFullAddressPath(deviceView, projectId);
                calculateNonRootDeviceMiddleAddressPath(deviceView, projectId);
                calculateNonRootDeviceShortAddressPath(deviceView, projectId);
            }
            deviceViewRepository.insert(deviceView);
            for (DevicePlanLayout devicePlanLayout : deviceView.getPlanLayouts()) {
                String planId = devicePlanLayout.getPlanId();
                if (!projectPlans.containsKey(planId)) {
                    Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
                    if (plan != null) {
                        projectPlans.put(planId, plan);
                    } else {
                        errorMessage.append("Для отображения устройства с адресом ").append(deviceView.getAddressPath())
                                .append(" не найден соответствующие план");
                        return false;
                    }
                }
                CoordinatePoint deviceCoordinatePoint = devicePlanLayout.getCoordinatePoint();
                if (deviceCoordinatePoint.getX() < 0.0
                        || deviceCoordinatePoint.getX() > projectPlans.get(planId).getxSize()
                        || deviceCoordinatePoint.getY() < 0.0
                        || deviceCoordinatePoint.getY() > projectPlans.get(planId).getySize()) {
                    errorMessage.append("Для отображения устройства с адресом ").append(deviceView.getAddressPath())
                            .append(" неправильно заданы координаты");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public List<TreeItemDeviceView> generateTreeItemDeviceViews(List<Device> devices, List<Region> regions,
            Language language) {
        // поиск профилей устройств
        Map<String, DeviceProfileView> profileViewsById = findRelatedDeviceProfileViews(devices, language);
        if (profileViewsById == null)
            // TODO: отправлять наверх "не найден профиль устройства"
            return null;

        // генерация отображений устройств
        List<DeviceView> deviceViews = generateDeviceViews(devices, profileViewsById);

        // генерация элементов дерева устройств
        return getTreeItemDeviceViews(deviceViews, profileViewsById, regions);
    }

    /**
     * Поиск отображений профилей устройств по списку и устройств, на которые их можно заменить
     * @param devices список устройств
     * @return карта профилей устройств или null если не был найден профиль хотя бы одного устройства из списка
     */
    private Map<String, DeviceProfileView> findRelatedDeviceProfileViews(List<Device> devices, Language language) {
        Set<String> deviceProfileIds = new HashSet<>();
        Set<String> allDeviceProfileIds = new HashSet<>();
        for (Device device : devices) {
            deviceProfileIds.add(device.getDeviceProfileId());
            allDeviceProfileIds.addAll(device.getAcceptableDeviceProfileIds());
        }
        allDeviceProfileIds.addAll(deviceProfileIds);
        Map<String, DeviceProfileView> profileViewsById =
                deviceProfileViewRepository.findByLanguageAndIdIn(language, allDeviceProfileIds)
                .stream().collect(Collectors.toMap(DeviceProfileView::getId, view -> view));
        if (!profileViewsById.keySet().containsAll(deviceProfileIds))
            return null;
        return profileViewsById;
    }

    private List<TreeItemDeviceView> getTreeItemDeviceViews(List<DeviceView> deviceViews,
            Map<String, DeviceProfileView> profileViewsById, List<Region> regions) {
        List<TreeItemDeviceView> treeItemViews = new ArrayList<>();

        // карта зон
        Map<String, Region> regionsById = regions.stream().collect(Collectors.toMap(Region::getId, region -> region));

        // поиск альтернативных иконок в базе
        Set<String> iconIds = new HashSet<>();
        for (DeviceView deviceView : deviceViews) {
            Device device = deviceView.getDevice();
            String alternateIconId = DeviceProperty.getString(device, DeviceProperties.AlternateIcon);
            if (alternateIconId != null)
                iconIds.add(alternateIconId);
        }
        Map<String, Media> iconsById = mediaRepository.findByIdIn(iconIds)
                .stream().collect(Collectors.toMap(Media::getId, icon -> icon));

        // формирование отображений элементов дерева устройств
        List<Device> devices = deviceViews.stream().map(DeviceView::getDevice).collect(Collectors.toList());
        for (DeviceView deviceView : deviceViews) {
            TreeItemDeviceView treeItemDeviceView = getTreeItemDeviceView(deviceView,
                    profileViewsById, regionsById, iconsById, devices);
            if (treeItemDeviceView != null)
                treeItemViews.add(treeItemDeviceView);
        }

        return treeItemViews;
    }

    private List<DeviceView> generateDeviceViews(List<Device> devices,
            Map<String, DeviceProfileView> profileViewsById) {
        List<DeviceView> deviceViews = new ArrayList<>();

        // генерация отображений устройств
        for (Device device : devices) {
            String profileId = device.getDeviceProfileId();
            DeviceProfileView profileView = profileViewsById.get(profileId);
            assert profileView != null : "Передана неполная карта профилей устройств";
            DeviceView deviceView = new DeviceView(device, profileView);
            calculateAddressPaths(deviceView);
            deviceViews.add(deviceView);
        }

        return deviceViews;
    }

    @Override
    public EntitiesContainer<TreeItemDeviceView> createByRecovery(String projectId, List<Device> devices,
            List<Region> regions, Map<String, String> oldToNewIds, Map<String, String> regionIds,
            StringBuilder errorMessage, Language language) {

        // поиск отображений профилей добавляемых устройств
        Map<String, DeviceProfileView> profileViewsById = findRelatedDeviceProfileViews(devices, language);
        if (profileViewsById == null) {
            errorMessage.append("Не найдено отображение профиля добавляемого устройства");
            return null;
        }

        // добавление устройств
        List<Device> updatedDevices =
                deviceService.createByRecovery(projectId, devices, regionIds, oldToNewIds, errorMessage);
        if (updatedDevices == null)
            return null;
        List<String> updatedDeviceIds = updatedDevices.stream().map(Device::getId).collect(Collectors.toList());

        // генерация и запись в базу отображений устройств
        List<DeviceView> createdDeviceViews = generateDeviceViews(devices, profileViewsById);
        createdDeviceViews = deviceViewRepository.insert(createdDeviceViews);

        // генерация элементов дерева устройств
        EntitiesContainer<TreeItemDeviceView> devicesUpdate = new EntitiesContainer<>();
        devicesUpdate.setCreated(getTreeItemDeviceViews(createdDeviceViews, profileViewsById, regions));
        devicesUpdate.setUpdated(getTreeItemDeviceViewsByIds(projectId, updatedDeviceIds, language));

        return devicesUpdate;
    }

    @Override
    public void remove(String projectId, EntitiesContainer<TreeItemDeviceView> devicesUpdate) {
        // удаление добавленных устройств и их отображений
        List<String> createdDeviceIds =
                devicesUpdate.getCreated().stream().map(TreeItemDeviceView::getId).collect(Collectors.toList());
        deviceViewRepository.deleteByProjectIdAndIdIn(projectId, createdDeviceIds);
        deviceRepository.deleteByProjectIdAndIdIn(projectId, createdDeviceIds);
        // обновление карты свободных адресов изменённых устройств
        List<String> updatedDeviceIds =
                devicesUpdate.getUpdated().stream().map(TreeItemDeviceView::getId).collect(Collectors.toList());
        deviceService.updateLineAddressRanges(projectId, updatedDeviceIds);
    }

    @Override
    public TreeItemDeviceView updateDeviceShapeLibrary(String projectId, String deviceId,
                                            String shapeLibraryId, Language language, StringBuilder errorMessage) {
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)){
            return null;
        }
        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append(DEVICE_VIEW_NOT_FOUND_MSG);
            return null;
        }
        if (shapeLibraryId.equals(DEFAULT_PARAMETER)){
            deviceView.setDeviceShapeLibraryId("");
        } else {
            if (!deviceShapeLibraryRepository.existsByRemovedIsFalseAndIdAndDeviceProfileId(shapeLibraryId,
                    deviceView.getDevice().getDeviceProfileId())){
                errorMessage.append("Библиотека шейпов не найдена");
                return null;
            }
            deviceView.setDeviceShapeLibraryId(shapeLibraryId);
        }
        deviceView = deviceViewRepository.save(deviceView);
        return getTreeItemDeviceView(projectId, language, deviceView);
    }

    @Override
    public boolean handleDeviceShapeLibraryDeletion(String deviceShapeLibraryId, StringBuilder errorMessage) {
        List<DeviceView> deviceViews = deviceViewRepository.findAllByDeviceShapeLibraryId(deviceShapeLibraryId);
        Project activeProject = projectService.getActive();
        if (deviceViews.stream().anyMatch(deviceView -> activeProject.getId().equals(deviceView.getProjectId()))){
            errorMessage.append("Библиотека шейпов используется устройством, которое находится в активном проекте");
            return false;
        }
        for (DeviceView deviceView: deviceViews) {
            deviceView.setDeviceShapeLibraryId("");
        }
        deviceViewRepository.save(deviceViews);
        return true;
    }

    @Override
    public CreatedAndUpdatedEntities updateCustomSubsystem(String projectId, String deviceId, Subsystem customSubsystem,
                                                           Language language, StringBuilder errorMessage){

        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }

        if (customSubsystem != null && !customSubsystem.isAFilterTag()){
            errorMessage.append("Подсистема не может быть использована для фильтрации");
            return null;
        }

        DeviceView deviceView = deviceViewRepository.findByProjectIdAndId(projectId, deviceId);
        if (deviceView == null) {
            errorMessage.append(DEVICE_VIEW_NOT_FOUND_MSG);
            return null;
        }
        List<Device> childDevices = deviceRepository.findByProjectIdAndParentDeviceIdAndDisabled(projectId, deviceId, false);

        DeviceProfile deviceProfile = deviceProfileRepository.findById(deviceView.getDevice().getDeviceProfileId());
        if (deviceProfile == null){
            errorMessage.append("В системе отсутствует представление профиля данного устройства");
            return null;
        }
        List<DeviceProfile> childDevicesProfiles = deviceProfileRepository
                .findByIdIn(childDevices.stream().map(Device::getDeviceProfileId).collect(Collectors.toList()));

        if (deviceProfile.getSubsystem() != Subsystem.GENERAL) {
            errorMessage.append("Профиль устройства должен принадлежать общей подсистеме");
            return null;
        }

        if(childDevicesProfiles
                .stream().anyMatch(childDeviceProfile -> childDeviceProfile.getSubsystem() != Subsystem.GENERAL)){
            errorMessage.append("Для усройства есть доступные подсистемы для фильтрации");
            return null;
        }

        Device device = deviceView.getDevice();
        device.setCustomSubsystem(customSubsystem);
        deviceRepository.save(device);

        CreatedAndUpdatedEntities createdAndUpdatedEntities = new CreatedAndUpdatedEntities(projectId);
        createdAndUpdatedEntities.updatedDevices.add(getTreeItemDeviceView(projectId, language, deviceView));
        return createdAndUpdatedEntities;
    }

    @Override
    public void clearTags(String projectId){
        List<Device> devices = deviceRepository.findByProjectId(projectId);
        devices.forEach(device -> device.getFilterTags().getTags().clear());
        deviceRepository.save(devices);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<Device> devices = deviceRepository.findByProjectId(projectId);
        List<String> deviceProfileIds = devices
                .stream().map(Device::getDeviceProfileId)
                .collect(Collectors.toList());
        Map<String, DeviceProfile> deviceProfileById = deviceProfileRepository
                .findByIdIn(deviceProfileIds)
                .stream()
                .collect(Collectors.toMap(DeviceProfile::getId, Function.identity()));

        for (Device device : devices){
            FilterTags filterTags = device.getFilterTags();
            List<Device> childDevices = new ArrayList<>();
            devices.forEach(possibleChildDevice -> {
                String parentDeviceId = possibleChildDevice.getParentDeviceId();
                if (parentDeviceId != null && parentDeviceId.equals(device.getId())){
                    childDevices.add(possibleChildDevice);
                }
            });

            filterTags.getTags().addAll(getSubsystemTagsForDevice(device,
                    deviceProfileById.get(device.getDeviceProfileId()), deviceProfileById, childDevices));
        }

        deviceRepository.save(devices);
    }

    private Set<String> getSubsystemTagsForDevice(Device device, DeviceProfile deviceProfile,
                                               Map<String, DeviceProfile> deviceProfileById,
                                               List<Device> childDevices){
        Set<String> availableSubsystems = new HashSet<>();
        /*
         * - если есть customSubsystem -> добавить customSubsystem
         * - если в профиле GENERAL и есть дочерние устройства -> по профилям дочерних устройств,
         *  а если все дочерние подсистемы - GENERAL -> customSubsystem дочернего или GENERAL
         * - если в профиле GENERAL и дочерних устройств нет -> оставить как есть
         * - если в профиле не GENERAL -> добавить, если подсистема - метка
         * - добавить additionalSubsystems
         */
        Subsystem profileSubsystem = deviceProfile.getSubsystem();
        Subsystem customSubsystem = device.getCustomSubsystem();
        if (customSubsystem != null){
            availableSubsystems.add(customSubsystem.toString());
        } else if (profileSubsystem == Subsystem.GENERAL){
            if (!childDevices.isEmpty()) {
                childDevices.forEach(childDevice -> {
                    DeviceProfile childDeviceProfile = deviceProfileById.get(childDevice.getDeviceProfileId());
                    if (childDeviceProfile != null && !childDevice.isDisabled()) {
                        Subsystem childSubsystem = childDeviceProfile.getSubsystem();
                        if (childSubsystem.isAFilterTag()) {
                            availableSubsystems.add(childSubsystem.toString());
                        }
                        if (childSubsystem == Subsystem.GENERAL){
                            if (childDevice.getCustomSubsystem() != null){
                                availableSubsystems.add(childDevice.getCustomSubsystem().toString());
                            } else {
                                availableSubsystems.add(childSubsystem.toString());
                            }
                        }
                    }
                });
            } else {
                availableSubsystems.add(profileSubsystem.toString());
            }
        } else if (profileSubsystem.isAFilterTag()){
            availableSubsystems.add(profileSubsystem.toString());
        }
        deviceProfile.getAdditionalSubsystems().forEach(subsystem -> availableSubsystems.add(subsystem.toString()));

        return availableSubsystems;
    }
}
