package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.RegionView;
import ru.rubezh.firesec.nt.domain.v1.RegionView.RegionPlanLayout;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedActiveRegionView;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface RegionViewService {
    
    public class EntitiesUpdatedByRegion {
         public List<AggregatedScenarioView> updatedScenarios = new ArrayList<>();
         public List<TreeItemDeviceView> updatedDevices = new ArrayList<>();
         public List<IndicatorPanel> indicatorPanels = new ArrayList<>();
    }

    public RegionView add(String projectId, Region region, StringBuilder errorMessage);

    /*
     * TODO: Реализовать обновление сущностей, ссылающихся на зону, и возврат их
     */
    public EntitiesUpdatedByRegion remove(String projectId, String regionId, StringBuilder errorMessage);

    public RegionView update(String projectId, String regionId, Region region, StringBuilder errorMessage);

    public RegionView updateRegionPlanLayouts(String projectId, String regionId, List<RegionPlanLayout> planLayouts,
            StringBuilder errorMessage);

    public List<AggregatedActiveRegionView> getAggregatedActiveRegionViews(String projectId);

    public AggregatedActiveRegionView getAggregatedActiveRegionViewByRegionId(String projectId, String regionId);

    public boolean checkAndAdditionRegionViewToImportProject(String projectId, List<RegionView> regionViews,
            Map<String, String> oldAndNewPlansId, Map<String, String> oldAndNewRegionsId, StringBuilder errorMessage);

    public List<RegionView> generateRegionViews(List<Region> regions);

    /**
     * Добавить зоны из сохранённой конфигурации
     * 
     * @param projectId
     *            идентификатор проекта
     * @param regions
     *            зоны (проектные сущности)
     * @param oldToNewIds
     *            карта соответствия новых идентификаторов зон старым (заполняется в методе)
     * @param errorMessage
     *            сообщение об ошибке (заполняется в случае ошибки)
     * @return добавленные зоны (отображения) или null в случае ошибки
     */
    public List<RegionView> createByRecovery(String projectId, List<Region> regions, Map<String, String> oldToNewIds,
            StringBuilder errorMessage);

    /**
     * Откатить восстановленные из сохранённой конфигурации зоны
     * 
     * @param projectId
     *            идентификатор проекта
     * @param regionViews
     *            добавленные зоны (отображения)
     */
    public void remove(String projectId, List<RegionView> regionViews);

    /**
     * Безопасно (с ограниченным кол-вом занимаемой памяти) записать
     * агрегированные отображения активных зон в поток вывода
     *
     * @param projectId
     *            идентификатор проекта
     * @param outputStream
     *            поток вывода для записи
     * @param objectMapper
     *            преобразователь объектов в JSON
     */
    void safelyWriteAggregatedActiveRegionsByProjectId(String projectId, OutputStream outputStream,
                                                       ObjectMapper objectMapper) throws IOException;

    /**
     * Получить (постранично) агрегированные отображения активных зон
     * проекта по идентификатору проекта
     *
     * @param projectId
     *            идентификатор проекта
     * @param pageSize
     *            размер страницы
     * @param pageNo
     *            номер страницы (с нуля)
     * @return список отображений устройств
     */
    List<AggregatedActiveRegionView> getAggregatedActiveRegionViewsByProjectId(String projectId, long pageSize, int pageNo);

    /**
     * Постранично получить агрегированные отображения активных зон,
     * время обновления состояния которых позже заданного
     *
     * @param lastObservedTime последнее время проверки
     * @param pageSize размер страницы
     * @param pageNo номер страницы
     * @return список агрегированных представлений активных зон
     */
    List<AggregatedActiveRegionView> getActiveRegionViewsByTimeAfter(Date lastObservedTime, long pageSize,
                                                                                 int pageNo);

}
