package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.DeviceShapeRepository;
import ru.rubezh.firesec.nt.dao.v1.StateCategoryRepository;
import ru.rubezh.firesec.nt.domain.v1.DeviceShape;
import ru.rubezh.firesec.nt.domain.v1.DeviceShapeLibrary;
import ru.rubezh.firesec.nt.domain.v1.StateCategory;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;

@Service
public class DeviceShapeServiceImpl implements DeviceShapeService {

    @Autowired
    private DeviceShapeLibraryService deviceShapeLibraryService;

    @Autowired
    private DeviceShapeRepository deviceShapeRepository;

    @Autowired
    private StateCategoryRepository stateCategoryRepository;

    @Override
    public List<DeviceShape> getActual(StringBuilder errorMessage) {
        List<DeviceShapeLibrary> actualDeviceShapeLibraries = deviceShapeLibraryService.getActual(errorMessage);
        if (actualDeviceShapeLibraries == null) {
            return null;
        }

        List<String> actualDeviceShapeLibraryIds = actualDeviceShapeLibraries.stream().map(deviceShapeLibrary -> {
            return deviceShapeLibrary.getId();
        }).collect(Collectors.toList());

        List<String> existingStateCategoryIds = stateCategoryRepository.findAll().stream().map(stateCategory -> {
            return stateCategory.getId();
        }).collect(Collectors.toList());
        existingStateCategoryIds.add("");//Пустой идентификатор класса состояния у базовых шейпов
        
        List<DeviceShape> actualDeviceShapes = deviceShapeRepository
                .findByRemovedIsFalseAndDeviceShapeLibraryIdInAndStateCategoryIdInOrderByDeviceShapeLibraryId(
                        actualDeviceShapeLibraryIds,
                        existingStateCategoryIds);

        return actualDeviceShapes;
    }

    @Override
    public DeviceShape getIfActual(String deviceShapeId, StringBuilder errorMessage) {
        DeviceShape deviceShape = deviceShapeRepository.findOneByRemovedIsFalseAndId(deviceShapeId);
        if (deviceShape == null) {
            errorMessage.append("Шейп не найден");
            return null;
        }

        DeviceShapeLibrary deviceShapeLibrary = deviceShapeLibraryService
                .getIfActual(deviceShape.getDeviceShapeLibraryId(), errorMessage);
        if (deviceShapeLibrary == null) {
            errorMessage.setLength(0);
            errorMessage.append("Шейп относится к неактуальной библиотеке шейпов");
            return null;
        }

        if (!deviceShape.getStateCategoryId().isEmpty()) {
            StateCategory stateCategory = stateCategoryRepository.findOne(deviceShape.getStateCategoryId());
            if (stateCategory == null) {
                errorMessage.append("Шейп не актуален, т.к. ссылается на неизвестный класс состояния");
                return null;
            }
        }

        return deviceShape;
    }

    @Override
    public boolean add(DeviceShape deviceShape, StringBuilder errorMessage) {
        if (deviceShape.getDeviceShapeLibraryId() == null || deviceShape.getDeviceShapeLibraryId().isEmpty()) {
            errorMessage.append("Не указана библиотека шейпов устройств");
            return false;
        }

        if (deviceShape.getStateCategoryId() == null || deviceShape.getStateCategoryId().isEmpty()) {
            errorMessage.append("Не указан класс состояния");
            return false;
        }

        if (deviceShapeRepository.existsByDeviceShapeLibraryIdAndStateCategoryId(deviceShape.getDeviceShapeLibraryId(),
                deviceShape.getStateCategoryId())) {
            errorMessage.append("В указанной библиотеке для указанного класса состояния уже определен шейп");
            return false;
        }

        if ((deviceShape.getBuiltinMediaId() == null || deviceShape.getBuiltinMediaId().isEmpty())
                && (deviceShape.getSvgContent() == null || deviceShape.getSvgContent().isEmpty())) {
            errorMessage.append("Не задано изображение");
            return false;
        }

        if (deviceShape.getAnimationSpeed() < 500 || deviceShape.getAnimationSpeed() > 5000) {
            errorMessage.append("Скорость анимации должна быть от 500мс до 5000мс");
            return false;
        }

        /*
         * TODO: от сюда и до выхода нужно заблокировать EntityNotifier в
         * фронтенд-сервере, чтобы не было вероятности, что некоторые
         * создаваемые сущности не будут отправлены по WebSockets.
         */

        Date currentDateTime = new Date();
        deviceShape.setCreated(currentDateTime);
        deviceShapeRepository.insert(deviceShape);
        return true;
    }

    @Override
    public boolean update(String deviceShapeId, DeviceShape deviceShape, StringBuilder errorMessage) {
        DeviceShape destDeviceShape = deviceShapeRepository.findOneByRemovedIsFalseAndId(deviceShapeId);
        if (destDeviceShape == null) {
            errorMessage.append("Изменяемый шейп не найден");
            return false;
        }

        if ((deviceShape.getBuiltinMediaId() == null || deviceShape.getBuiltinMediaId().isEmpty())
                && (deviceShape.getSvgContent() == null || deviceShape.getSvgContent().isEmpty())) {
            errorMessage.append("Не задано изображение");
            return false;
        }

        if (deviceShape.getAnimationSpeed() < 500 || deviceShape.getAnimationSpeed() > 5000) {
            errorMessage.append("Скорость анимации должна быть от 500мс до 5000мс");
            return false;
        }

        /*
         * TODO: от сюда и до выхода нужно заблокировать EntityNotifier в
         * фронтенд-сервере, чтобы не было вероятности, что некоторые
         * создаваемые сущности не будут отправлены по WebSockets.
         */

        destDeviceShape.update(deviceShape);
        deviceShapeRepository.save(destDeviceShape);
        return true;
    }

    @Override
    public boolean remove(String deviceShapeId, StringBuilder errorMessage) {
        DeviceShape removingDeviceShape = deviceShapeRepository.findOneByRemovedIsFalseAndId(deviceShapeId);
        if (removingDeviceShape == null) {
            errorMessage.append("Удаляемый шейп не найден");
            return false;
        }

        if (removingDeviceShape.getStateCategoryId().isEmpty()) {
            errorMessage.append("Нельзя удалить базовый шейп");
            return false;
        }

        removingDeviceShape.setRemoved(true);
        deviceShapeRepository.save(removingDeviceShape);
        return true;
    }

    @Override
    public EntitiesContainer<DeviceShape> getChangesAndDoRemove(Date from) {
        /* Заполняем изменения */
        EntitiesContainer<DeviceShape> changes = new EntitiesContainer<>();
        changes.setCreated(deviceShapeRepository.findByRemovedIsFalseAndCreatedAfter(from));
        changes.setUpdated(deviceShapeRepository.findByRemovedIsFalseAndUpdatedAfter(from));
        List<String> removedShapeIds = deviceShapeRepository.findByRemovedIsTrue().stream()
                .map(deviceShape -> {
                    return deviceShape.getId();
                }).collect(Collectors.toList());
        changes.setDeletedIds(removedShapeIds);

        /* Удаляем шейпы, отмеченные на удаление */
        deviceShapeRepository.deleteByIdIn(removedShapeIds);

        return changes;
    }

}
