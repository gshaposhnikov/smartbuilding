package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.ScenarioTriggerTypeRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioActionTypeRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioRepository;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Action;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.BlockType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;

@Service
public class ScenarioRecoveryServiceImpl implements ScenarioRecoveryService {

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;

    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;

    @Autowired
    private ScenarioViewService scenarioViewService;

    private List<String> getIdsReplacement(List<String> oldIds, Map<String, String> idsMap) {
        List<String> newIds =
                oldIds.stream().map(id -> idsMap.get(id)).filter(Objects::nonNull).collect(Collectors.toList());
        return newIds.size() == oldIds.size() ? newIds : null;
    }

    /**
     * Замена ссылок в блоке логики сценария (рекурсивная)
     */
    private boolean replaceLogicEntityIds(ScenarioLogicBlock logic, Map<String, ScenarioTriggerType> triggerTypesById,
            Map<String, String> regionIds, Map<String, String> deviceIds, Map<String, String> virtualStateIds) {
        if (logic != null) {
            if (logic.getTriggerTypeId() != null && !logic.getTriggerTypeId().isEmpty()) {
                List<String> oldEntityIds = logic.getEntityIds();
                List<String> newEntityIds = null;
                TriggerEntityType entityType = triggerTypesById.get(logic.getTriggerTypeId()).getEntityType();
                switch (entityType) {
                case DEVICE:
                case CONTROL_DEVICE:
                    newEntityIds = getIdsReplacement(oldEntityIds, deviceIds);
                    break;
                case REGION:
                    newEntityIds = getIdsReplacement(oldEntityIds, regionIds);
                    break;
                case VIRTUAL_STATE:
                    newEntityIds = getIdsReplacement(oldEntityIds, virtualStateIds);
                    break;
                default:
                    break;
                }
                if (newEntityIds == null)
                    return false;
                logic.setEntityIds(newEntityIds);
            }
            // вложенные блоки логики
            for (ScenarioLogicBlock sublogic : logic.getSubLogics())
                if (!replaceLogicEntityIds(sublogic, triggerTypesById,
                        regionIds, deviceIds, virtualStateIds))
                    return false;
        }
        return true;
    }

    /**
     * Замена ссылки на отслеживаемую сущность временного блока сценария
     */
    private boolean replaceTracingEntityId(ScenarioTimeLineBlock timeLineBlock,
            Map<String, String> deviceIds, Map<String, String> virtualStateIds) {
        String oldEntityId = timeLineBlock.getTracingEntityId();
        String newEntityId = null;
        switch (timeLineBlock.getTracingEntityType()) {
        case SENSOR_DEVICE:
            newEntityId = deviceIds.get(oldEntityId);
            break;
        case VIRTUAL_STATE:
            newEntityId = virtualStateIds.get(oldEntityId);
            break;
        default:
            break;
        }
        if (newEntityId == null)
            return false;
        timeLineBlock.setTracingEntityId(newEntityId);
        return true;
    }

    /**
     * Замена ссылок в условии временного блока сценария
     */
    private boolean replaceConditionEntityId(ScenarioTimeLineBlock timeLineBlock,
            Map<String, String> deviceIds, Map<String, String> virtualStateIds) {
        String oldEntityId = timeLineBlock.getConditionEntityId();
        String newEntityId = null;
        switch (timeLineBlock.getConditionType()) {
        case DEVICE_STATE:
            newEntityId = deviceIds.get(oldEntityId);
            break;
        case VIRTUAL_STATE:
            newEntityId = virtualStateIds.get(oldEntityId);
            break;
        case NONE:
            return true;
        default:
            break;
        }
        if (newEntityId == null)
            return false;
        timeLineBlock.setConditionEntityId(newEntityId);
        return true;
    }

    /**
     * Замена ссылок в действии временного блока сценария
     */
    private boolean replaceActionEntityId(Action action, Map<String, ScenarioActionType> actionTypesById,
            Map<String, String> regionIds, Map<String, String> deviceIds,
            Map<String, String> virtualStateIds, Map<String, String> scenarioIds) {
        String oldEntityId = action.getEntityId();
        String newEntityId = null;
        ActionEntityType entityType = actionTypesById.get(action.getActionTypeId()).getEntityType();
        switch (entityType) {
        case DEVICE:
            newEntityId = deviceIds.get(oldEntityId);
            break;
        case REGION:
            newEntityId = regionIds.get(oldEntityId);
            break;
        case VIRTUAL_STATE:
            newEntityId = virtualStateIds.get(oldEntityId);
            break;
        case SCENARIO:
            newEntityId = scenarioIds.get(oldEntityId);
            break;
        default:
            break;
        }
        if (newEntityId == null)
            return false;
        action.setEntityId(newEntityId);
        return true;
    }

    /**
     * Замена ссылки на сценарии в действии временного блока сценария
     */
    private void replaceActionScenarioId(Action action, Map<String, ScenarioActionType> actionTypesById,
            Map<String, String> scenarioIds) {
        ActionEntityType entityType = actionTypesById.get(action.getActionTypeId()).getEntityType();
        if (entityType == ActionEntityType.SCENARIO) {
            String newEntityId = scenarioIds.get(action.getEntityId());
            assert newEntityId != null : "передана неполная карта сценариев";
            action.setEntityId(newEntityId);
        }
    }

    /**
     * Замена ссылок во временном блоке сценария
     */
    private boolean replaceExecEntityIds(ScenarioTimeLineBlock timeLineBlock,
            Map<String, ScenarioActionType> actionTypesById,
            Map<String, String> regionIds, Map<String, String> deviceIds,
            Map<String, String> virtualStateIds, Map<String, String> scenarioIds) {
        switch (timeLineBlock.getBlockType()) {
        case TRACING:
            if (!replaceTracingEntityId(timeLineBlock, deviceIds, virtualStateIds))
                return false;
            break;
        case EXECUTIVE:
            if (!replaceConditionEntityId(timeLineBlock, deviceIds, virtualStateIds))
                return false;
            for (Action action : timeLineBlock.getActions())
                if (!replaceActionEntityId(action, actionTypesById,
                        regionIds, deviceIds, virtualStateIds, scenarioIds))
                    return false;
            break;
        case COMPUTER_ACTION:
        default:
            break;
        }
        return true;
    }

    /**
     * Поиск и составление карт всех необходимых типов триггеров сценариев
     */
    private Map<String, ScenarioTriggerType> getScenarioTriggerTypeMaps(List<Scenario> scenarios,
            StringBuilder errorMessage) {
        Set<String> triggerTypeIds = new HashSet<>();
        for (Scenario scenario : scenarios) {
            // поиск используемых типов триггеров
            scenarioViewService.getTriggerTypeIds(scenario.getStartLogic(), triggerTypeIds);
            scenarioViewService.getTriggerTypeIds(scenario.getStopLogic(), triggerTypeIds);
        }
        // триггеры
        List<ScenarioTriggerType> triggerTypes = scenarioTriggerTypeRepository.findByIdIn(triggerTypeIds);
        if (triggerTypes.size() != triggerTypeIds.size()) {
            errorMessage.append("Не найден тип триггера сценария");
            return null;
        }
        return triggerTypes.stream().collect(
                Collectors.toMap(ScenarioTriggerType::getId, triggerType -> triggerType));
    }

    /**
     * Поиск и составление карт всех необходимых типов действий сценариев
     */
    private Map<String, ScenarioActionType> getScenarioActionTypeMaps(List<Scenario> scenarios,
            StringBuilder errorMessage) {
        Set<String> actionTypeIds = new HashSet<>();
        for (Scenario scenario : scenarios) {
            // поиск используемых типов действий
            scenarioViewService.getActionTypeIds(scenario.getTimeLineBlocks(), actionTypeIds);
        }
        // действия
        List<ScenarioActionType> actionTypes = scenarioActionTypeRepository.findByIdIn(actionTypeIds);
        if (actionTypes.size() != actionTypeIds.size()) {
            errorMessage.append("Не найден тип действия сценария");
            return null;
        }
        return actionTypes.stream().collect(
                Collectors.toMap(ScenarioActionType::getId, actionType -> actionType));
    }

    /**
     * Замена ссылок на все сущности по соответствующим картам
     */
    private boolean replaceEntityIds(List<Scenario> scenarios,
            Map<String, String> regionIds, Map<String, String> deviceIds,
            Map<String, String> virtualStateIds, Map<String, String> scenarioIds,
            Map<String, ScenarioTriggerType> triggerTypesById, Map<String, ScenarioActionType> actionTypesById,
            StringBuilder errorMessage) {
        for (Scenario scenario : scenarios) {
            // ссылка на прибор
            String controlDeviceId = scenario.getControlDeviceId();
            if (controlDeviceId != null) {
                String newId = deviceIds.get(controlDeviceId);
                if (newId == null) {
                    errorMessage.append("Не найден прибор исполнительного сценария " + scenario.getId());
                    return false;
                }
                scenario.setControlDeviceId(newId);
            }
            // замена идентификаторов сущностей блоков логики
            if (!replaceLogicEntityIds(scenario.getStartLogic(), triggerTypesById,
                    regionIds, deviceIds, virtualStateIds)) {
                errorMessage.append("Не найдена сущность блока логики включения сценария " + scenario.getId());
                return false;
            }
            if (!replaceLogicEntityIds(scenario.getStopLogic(), triggerTypesById,
                    regionIds, deviceIds, virtualStateIds)) {
                errorMessage.append("Не найдена сущность блока логики выключения сценария " + scenario.getId());
                return false;
            }
            // замена идентификаторов сущностей исполнительных блоков
            for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks())
                if (!replaceExecEntityIds(timeLineBlock, actionTypesById,
                        regionIds, deviceIds, virtualStateIds, scenarioIds)) {
                    errorMessage.append("Не найдена сущность исполнительного блока сценария " + scenario.getId());
                    return false;
                }
        }
        return true;
    }

    @Override
    public List<AggregatedScenarioView> createByRecovery(String projectId, List<Scenario> scenarios,
            Map<String, String> regionIds, Map<String, String> deviceIds, Map<String, String> virtualStateIds,
            StringBuilder errorMessage) {
        // поиск необходимых справочных сущностей
        Map<String, ScenarioTriggerType> triggerTypesById = getScenarioTriggerTypeMaps(scenarios, errorMessage);
        if (triggerTypesById == null)
            return null;
        Map<String, ScenarioActionType> actionTypesById = getScenarioActionTypeMaps(scenarios, errorMessage);
        if (actionTypesById == null)
            return null;

        // заполнение вспомогательных карт
        Map<Integer, String> globalNosToOldIds = new HashMap<>();
        Map<String, String> scenarioIds = new HashMap<>();
        int nextGlobalNo = scenarioViewService.getNewGlobalNo(projectId);
        for (Scenario scenario : scenarios) {
            // назначение нового идентификатора проекта
            scenario.setProjectId(projectId);
            // назначение глобального номера
            scenario.setGlobalNo(nextGlobalNo++);
            // заполнение карты индексов
            scenarioIds.put(scenario.getId(), scenario.getId());
            // сохранение старых идентификаторов
            globalNosToOldIds.put(scenario.getGlobalNo(), scenario.getId());
        }

        // замена идентификаторов
        if (!replaceEntityIds(scenarios, regionIds, deviceIds, virtualStateIds, scenarioIds,
                triggerTypesById, actionTypesById, errorMessage))
            return null;

        // запись в базу
        scenarios.forEach(Scenario::clearId);
        scenarios = scenarioRepository.insert(scenarios);

        // обновление карты индексов
        for (Scenario newScenario : scenarios)
            scenarioIds.put(
                    globalNosToOldIds.get(newScenario.getGlobalNo()),
                    newScenario.getId());

        // замена идентификаторов сценариев в исполнительных блоках
        for (Scenario scenario : scenarios)
            for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks())
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE)
                    for (Action action : timeLineBlock.getActions())
                        replaceActionScenarioId(action, actionTypesById, scenarioIds);

        // обновление базы
        scenarios = scenarioRepository.save(scenarios);

        // валидация с использованием базы
        for (Scenario scenario : scenarios)
            if (!scenarioViewService.validateScenario(scenario, errorMessage)) {
                scenarioRepository.deleteByProjectIdAndIdIn(projectId, scenarioIds.values());
                return null;
            }

        // генерация отображений
        List<AggregatedScenarioView> scenarioViews = new ArrayList<>();
        for (Scenario scenario: scenarios) {
            scenarioViews.add(scenarioViewService.getAggregatedScenarioView(scenario));
        }
        return scenarioViews;
    }

}
