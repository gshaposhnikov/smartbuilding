package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

import ru.rubezh.firesec.nt.dao.v1.DeviceRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceViewRepository;
import ru.rubezh.firesec.nt.dao.v1.RegionRepository;
import ru.rubezh.firesec.nt.dao.v1.ScenarioRepository;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;
import ru.rubezh.firesec.nt.domain.v1.DeviceView;
import ru.rubezh.firesec.nt.domain.v1.EntityType;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage;
import ru.rubezh.firesec.nt.domain.v1.Indicator;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.ImportValidateMessage.Code;
import ru.rubezh.firesec.nt.domain.v1.Indicator.Style;
import ru.rubezh.firesec.nt.domain.v1.ValidateMessage.Level;

/**
 * Класс для получения свойств индикатора из импортированного файла (.FSc), проекта delphi версии,
 * находящее в CDATA тега Indicator.
 * @author Антон Лягин
 */

@Service
public class IndicatorPropertiesXMLParsingHandler extends DefaultHandler2{
    /** Название индикатора. */
    private static final String NAME = "caption";
    /** Запроса пароля при управлении объектом через индикатор. */
    private static final String PASSWORD_NEEDED = "requestPassword";
    /** Тип сущности индикатора. */
    private static final String ENTITY_TYPE = "type";
    /** Тэг свойства индикатора. */
    private static final String INDICATOR_PROPERTIES_TAG = "IndicatorProperties";
    /** Тэг шрифта(стиль). */
    private static final String FONT_TAG = "Font";
    /** Тэг зоны. */
    private static final String REGION_TAG = "zone";
    /** Тэг устройства. */
    private static final String DEVICE_TAG = "device";
    /** Размер шрифта. */
    private static final String FONT_SIZE = "Size";
    /** Толщина шрифта. */
    private static final String FONT_WEIGHT = "Bold";
    /** Стиль написания. */
    private static final String FONT_STYLE = "Italic";
    /** Оформление текста. */
    private static final String FONT_TEXT_DECORATION = "Underline";
    /** Ориентация текста. */
    private static final String FONT_TEXT_ALIGN = "Align";
    /** Id устройства(desktop версии). */
    private static final String DEVICE_UID = "UID";
    /** Id cценария (desktop версии). */
    private static final String SCENARIO = "scenario";
    /** Тип сущности - устройство. */
    private static final String ENTITY_TYPE_DEVICE = "1";
    /** Тип сущности - зона. */
    private static final String ENTITY_TYPE_REGION = "0";
    /** Тип сущности - сценарий. */
    private static final String ENTITY_TYPE_SCENARIO = "2";
    /** Отсутствует необходимость запроса пароля при управлении объектом через индикатор. */
    private static final String PASSWORD_NEEDED_FALSE = "0";
    /** Минимально допустимый размер шрифта. */
    private static final int FONT_SIZE_MIN = 8;
    /** Максимально допустимый размер шрифта.*/
    private static final int FONT_SIZE_MAX = 36;
    /** Оформление текста подчеркивание. */
    private static final String FONT_TEXT_DECORATION_UNDERLINE = "underline";
    /** Толщина шрифта "Жирный". */
    private static final String FONT_TEXT_WEIGHT_BOLD = "bold";
    /** Стиль написания "Курсивный". */
    private static final String FONT_TEXT_STYLE_ITALIC ="italic";
    /** Параметр стиля шрифта не выбран. */
    private static final String FONT_TEXT_STYLE_PARAMETR_SET_FALSE ="0";
    /** Ориентация текста справо. */
    private static final String TEXT_ALIGN_RIGHT = "Right";
    /** Ориентация текста слево. */
    private static final String TEXT_ALIGN_LEFT = "Left";
    /** Ориентация текста поцентру. */
    private static final String TEXT_ALIGN_CENTER = "Center";
    /** Размер шрифта по умолчанию. */
    private static final int DEFAULT_FONT_SIZE = 12;
    
    @Autowired
    private ScenarioRepository scenarioRepository;
    
    @Autowired
    private RegionRepository regionRepository;
    
    @Autowired
    private DeviceRepository deviceRepository;
    
    @Autowired
    private DeviceViewRepository deviceViewRepository;
    /** Текущий индикатор.*/
    private Indicator currentIndicator;
    /** Текущая панель управления.*/
    private IndicatorPanel indicatorPanel;
    /** набор идентификаторов сущностей.*/
    private List<String> entityIds;
    /** ID устройства текущей и desktop версии.*/
    private Map<String, String> deviceNewIdByDesktopId;
    /** ID зоны текущей и desktop версии.*/
    private Map<String, String> regionNewIdByDesktopId;
    /** ID сценария текущей и desktop версии.*/
    private Map<String, String> scenarioNewIdByDesktopId;
    /** Является тэгом зоны.*/
    private boolean isRegionTag = false;
    /** Id проекта.*/
    private String projectId;
    /** Список предупреждений (ошибок) импорта проекта.*/
    private List<ImportValidateMessage> idicatorWarningMessages;
    /** Префикс описание ошибки индикатора у которого задано имя.*/
    private String descriptionWithIndicatorNamePrefix;
    /** Префикс описание ошибки индикатора у которого не задано имя.*/
    private String descriptionWithOutIndicatorNamePrefix;
    
    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        switch (qName) {
        case INDICATOR_PROPERTIES_TAG:
            String name = atts.getValue(NAME);
            String entityType = atts.getValue(ENTITY_TYPE);
            String passwordNeeded = atts.getValue(PASSWORD_NEEDED);
            String scenario = atts.getValue(SCENARIO);
            if(entityType != null && passwordNeeded != null) {
                idicatorWarningMessages = new ArrayList<>();
                entityIds = new ArrayList<String>();
                if(name != null && !name.isEmpty()) {
                    currentIndicator.setName(name);
                }
                applyIndicatorPrefixWarningMessages(currentIndicator);
                switch (entityType) {
                case ENTITY_TYPE_REGION:
                    currentIndicator.setEntityType(EntityType.REGION);
                    break;
                case ENTITY_TYPE_DEVICE:
                    currentIndicator.setEntityType(EntityType.DEVICE);
                    break;
                case ENTITY_TYPE_SCENARIO:
                    currentIndicator.setEntityType(EntityType.SCENARIO);
                    if (scenario != null && scenarioNewIdByDesktopId.containsKey(scenario)) {
                        if (validaeEntity(EntityType.SCENARIO, scenarioNewIdByDesktopId.get(scenario), projectId, new ArrayList<>())) {
                            entityIds.add(scenarioNewIdByDesktopId.get(scenario));
                        }
                    } else {
                        String description;
                        if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty()) {
                            description = descriptionWithIndicatorNamePrefix.concat("Сценарий не добавлен");
                        }else {
                            description = descriptionWithOutIndicatorNamePrefix.concat("Сценарий не добавлен");
                        }
                        idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                                Code.SCENARIO_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.SCENARIO, null));
                    }
                default:
                    break;
                }
                if(passwordNeeded.equals(PASSWORD_NEEDED_FALSE)) {
                    currentIndicator.setPasswordNeeded(false);
                }else {
                    currentIndicator.setPasswordNeeded(true);
                }
            }
            break;
        case FONT_TAG:
            String textAlign = atts.getValue(FONT_TEXT_ALIGN);
            String decaration = atts.getValue(FONT_TEXT_DECORATION);
            String fontStyle = atts.getValue(FONT_STYLE);
            String weight = atts.getValue(FONT_WEIGHT);
            String size = atts.getValue(FONT_SIZE);
            if(textAlign != null && decaration != null && fontStyle != null && weight != null && size != null) {
                Style style = new Style();
                int fontSize = 0;
                try {
                    fontSize = Integer.parseInt(size);
                } catch (Exception e) {
                    fontSize = DEFAULT_FONT_SIZE;
                }
                if(fontSize >= FONT_SIZE_MIN && fontSize <= FONT_SIZE_MAX) {
                    style.setFontSize(fontSize);
                }else {
                    style.setFontSize(DEFAULT_FONT_SIZE);
                }
                switch (textAlign) {
                case TEXT_ALIGN_RIGHT:
                    style.setTextAlign(TEXT_ALIGN_RIGHT.toLowerCase());
                    break;
                case TEXT_ALIGN_LEFT:
                    style.setTextAlign(TEXT_ALIGN_LEFT.toLowerCase());
                    break;
                case TEXT_ALIGN_CENTER:
                    style.setTextAlign(TEXT_ALIGN_CENTER.toLowerCase());
                    break;
                default:
                    break;
                }
                if(!decaration.equals(FONT_TEXT_STYLE_PARAMETR_SET_FALSE)) {
                    style.setTextDecoration(FONT_TEXT_DECORATION_UNDERLINE);
                }
                if(!fontStyle.equals(FONT_TEXT_STYLE_PARAMETR_SET_FALSE)) {
                    style.setFontStyle(FONT_TEXT_STYLE_ITALIC);
                }
                if(!weight.equals(FONT_TEXT_STYLE_PARAMETR_SET_FALSE)) {
                    style.setFontWeight(FONT_TEXT_WEIGHT_BOLD);
                }
                currentIndicator.setStyles(style);
            }
            break;
        case DEVICE_TAG:
            String device_UID = atts.getValue(DEVICE_UID);
            if(device_UID != null) {
                if(deviceNewIdByDesktopId.containsKey(device_UID)){
                    if(validaeEntity(EntityType.DEVICE, deviceNewIdByDesktopId.get(device_UID), projectId, new ArrayList<>())){
                            entityIds.add(deviceNewIdByDesktopId.get(device_UID));
                    }
                }else {
                    String description;
                    if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty()) {
                            description = descriptionWithIndicatorNamePrefix.concat("Устройство не добавлено");
                            idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                                    Code.DEVICE_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.DEVICE, null));
                    }else {
                            description = descriptionWithOutIndicatorNamePrefix.concat("Устройство не добавлено");
                            idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                                    Code.DEVICE_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.DEVICE, null));
                    }
                }
            }
            break;
        case REGION_TAG:
            isRegionTag = true;
            break;
        default:
            break;
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(isRegionTag) {
            String regionId  = new String(ch, start, length).replace("\n", "").trim();
            if(regionNewIdByDesktopId.containsKey(regionId)) {
                if(validaeEntity(EntityType.REGION, regionNewIdByDesktopId.get(regionId), projectId, entityIds)) {
                    entityIds.add(regionNewIdByDesktopId.get(regionId));
                }
            }else {
                
            }
        }
    }
    
    @Override 
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        switch (qName) {
        case REGION_TAG:
            isRegionTag = false;
            break;
        case INDICATOR_PROPERTIES_TAG:
            if(entityIds != null && !entityIds.isEmpty() ) {
                currentIndicator.setEntityIds(entityIds);
            }
            break;

        default:
            break;
        }
    }
    
    private boolean validaeEntity(EntityType entityType, String entity, String projectId, List<String> entityIds) {
        String description;
        if (entityType == EntityType.SCENARIO) {
            /* сценарий может быть только один */
            if (scenarioRepository.existsByProjectIdAndId(projectId, entity)) {
                return true;
            } else {
                Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, entity);
                if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty()) {
                    description = descriptionWithIndicatorNamePrefix.concat("Сценарий \"").concat(scenario.getName())
                            .concat("\" не добавлен");
                }else {
                    description = descriptionWithOutIndicatorNamePrefix.concat("Сценарий \"")
                            .concat(scenario.getName()).concat("\" не добавлен");
                }
                idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                        Code.SCENARIO_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.SCENARIO, null));
                return false;
            }
        } else if (entityType == EntityType.DEVICE) {
            Device device = deviceRepository.findByProjectIdAndId(projectId, entity);
            if (device != null) {
                DeviceCategory deviceCategory = device.getDeviceCategory();
                if (deviceCategory == DeviceCategory.CONTROL || deviceCategory == DeviceCategory.TRANSPORT
                        || deviceCategory == DeviceCategory.CONTAINER) {
                    DeviceView deviceView = deviceViewRepository.findByDeviceId(entity);
                    System.out.println("deviceView" +deviceView);
                    String deviceNameAndMiddlepath = deviceView.getName().concat(" (")
                                .concat(deviceView.getMiddleAddressPath()).concat(")");
                    if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty() ) {
                        description = descriptionWithIndicatorNamePrefix.concat("устройство \"").concat(deviceNameAndMiddlepath).concat("\" не добавлено");
                    } else {
                        description = descriptionWithOutIndicatorNamePrefix.concat("устройство \"").concat(deviceNameAndMiddlepath).concat("\" не добавлено");
                    }
                    idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                            Code.DEVICE_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.DEVICE, null));
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } else if (entityType == EntityType.REGION) {
            Region region = regionRepository.findByProjectIdAndId(projectId, entity);
            if (region == null) {
                if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty()) {
                    description = descriptionWithIndicatorNamePrefix.concat("Зона не добавлена");
                }else {
                    description = descriptionWithOutIndicatorNamePrefix.concat("Зона не добавлена");
                }
                idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                        Code.REGION_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.REGION, null));
                return false;
            }
            if (entityIds != null && !entityIds.isEmpty()) {
                List<Region> regions = regionRepository.findByProjectIdAndIdIn(projectId, entityIds);
                if (regions.get(0).getSubsystem() != region.getSubsystem()) {
                    if (currentIndicator.getName() != null && !currentIndicator.getName().isEmpty()) {
                        description = descriptionWithIndicatorNamePrefix.concat("Зона \"").concat(region.getName()).concat("\" не добавлена");
                    }else {
                        description = descriptionWithOutIndicatorNamePrefix.concat("Зона \"").concat(region.getName()).concat("\" не добавлена");
                    }
                    idicatorWarningMessages.add(new ImportValidateMessage(Level.WARNING,
                            Code.REGION_NOT_ADDED_TO_VIRTUAL_INDICATOR, description, EntityType.REGION, null));
                    return false;
                }
            }
        }
        return true;
    }

    public void setCurrentIndicator(Indicator currentIndicator) {
        this.currentIndicator = currentIndicator;
    }
    
    private void applyIndicatorPrefixWarningMessages(Indicator currentIndicator) {
        descriptionWithIndicatorNamePrefix ="Панель индикаторов \"".concat(indicatorPanel.getName()).concat("\", Индикатор \"").concat(currentIndicator.getName()).concat("\" ")
                .concat("столбец ").concat(String.valueOf(currentIndicator.getColNo() + 1)).concat(" строка ")
                .concat(String.valueOf(currentIndicator.getRowNo() + 1)).concat(" : ");
        descriptionWithOutIndicatorNamePrefix ="Панель индикаторов /".concat(indicatorPanel.getName()).concat("\", Индикатор ").concat("столбец ")
                .concat(String.valueOf(currentIndicator.getColNo() + 1)).concat(" строка ")
                .concat(String.valueOf(currentIndicator.getRowNo() + 1)).concat(" : ");
    }
    
    public Indicator getCurrentIndicator() {
        return currentIndicator;
    }

    public void setDeviceNewIdByDesktopId(Map<String, String> deviceNewIdByDesktopId) {
        this.deviceNewIdByDesktopId = deviceNewIdByDesktopId;
    }

    public void setRegionNewIdByDesktopId(Map<String, String> regionNewIdByDesktopId) {
        this.regionNewIdByDesktopId = regionNewIdByDesktopId;
    }

    public void setScenarioNewIdByDesktopId(Map<String, String> scenarioNewIdByDesktopId) {
        this.scenarioNewIdByDesktopId = scenarioNewIdByDesktopId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<ImportValidateMessage> getIdicatorWarningMessages() {
        return idicatorWarningMessages;
    }

    public void setIndicatorPanel(IndicatorPanel indicatorPanel) {
        this.indicatorPanel = indicatorPanel;
    }
}
