package ru.rubezh.firesec.nt.service.v1;

import java.util.List;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;

public interface ScenarioRecoveryService {

    /**
     * Добавление сценариев из сохранённой конфигурации
     * @param projectId идентификатор проекта
     * @param scenarios сценарии (проектные сущности)
     * @param regionIds отображение старых идентификаторов зон на новые
     * @param deviceIds отображение старых идентификаторов устройств на новые
     * @param virtualStateIds отображение старых идентификаторов виртуальных состояний на новые
     * @param errorMessage возвращаемый текст ошибки
     * @return список обобщённых отображений сценариев или null в случае ошибки
     */
    List<AggregatedScenarioView> createByRecovery(String projectId, List<Scenario> scenarios,
            Map<String, String> regionIds, Map<String, String> deviceIds, Map<String, String> virtualStateIds,
            StringBuilder errorMessage);
}
