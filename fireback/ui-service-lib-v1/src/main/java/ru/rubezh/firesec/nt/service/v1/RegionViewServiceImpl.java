package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.RegionView.RegionPlanLayout;
import ru.rubezh.firesec.nt.domain.v1.representation.AggregatedActiveRegionView;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Order(2)
public class RegionViewServiceImpl implements RegionViewService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Logger logger;

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private ActiveRegionRepository activeRegionRepository;

    @Autowired
    private RegionViewRepository regionViewRepository;

    @Autowired
    private RegionService regionService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PlanRepository planRepository;
    
    @Autowired
    private ScenarioViewService scenarioViewService;
    
    @Autowired
    private DeviceViewService deviceViewService;
    
    @Autowired
    DeviceViewRepository deviceViewRepository;
    
    @Autowired
    RegionRepository regionRepository;

    @Autowired
    private IndicatorPanelService indicatorPanelService;
    
    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    /** Максимальное число зон, забираемое mongo-скриптом за раз */
    public static final int AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE = 500;

    /** Имя mongo-скрипта для получения агрегированных представлений активных зон */
    private static final String GET_AGGREGATED_ACTIVE_REGIONS_SCRIPT_NAME =
            "getAggregatedActiveRegionViewsByProjectIdAndTimeAfter";


    @Override
    public RegionView add(String projectId, Region region, StringBuilder errorMessage) {
        Region savedRegion = regionService.add(projectId, region, errorMessage);
        if (savedRegion == null) {
            return null;
        }
        RegionView regionView = new RegionView(savedRegion);
        regionView = regionViewRepository.insert(regionView);
        return regionView;
    }

    @Override
    public EntitiesUpdatedByRegion remove(String projectId, String regionId, StringBuilder errorMessage) {
        if (regionViewRepository.deleteByProjectIdAndId(projectId, regionId) == 0) {
            errorMessage.append("Зона не найдена");
            return null;
        }
        EntitiesUpdatedByRegion entitiesUpdatedByRegion = new EntitiesUpdatedByRegion();
        UpdatedScenariosAndDevices updatedScenariosAndDevices = regionService.remove(projectId, regionId, errorMessage);
        for (String currentDeviceId : updatedScenariosAndDevices.getUpdatedDeviceIds()) {
            entitiesUpdatedByRegion.updatedDevices.add(deviceViewService.getDeviceTreeItemByProjectIdAndDeviceId(projectId, currentDeviceId, language));
        }
        for (String currentScenarioId : updatedScenariosAndDevices.getUpdatedScenarioIds()) {
            entitiesUpdatedByRegion.updatedScenarios.add(scenarioViewService.getAggregatedScenarioView(projectId, currentScenarioId, errorMessage));
        }
        entitiesUpdatedByRegion.indicatorPanels =
                indicatorPanelService.removeEntityIdsFromIndicatorPanels(projectId,
                        Arrays.asList(regionId), EntityType.REGION);
        return entitiesUpdatedByRegion;
    }


    @Override
    public RegionView update(String projectId, String regionId, Region region, StringBuilder errorMessage) {
        Region updatedRegion = regionService.update(projectId, regionId, region, errorMessage);
        if (updatedRegion != null) {
            RegionView regionView = regionViewRepository.findByProjectIdAndId(projectId, regionId);
            /*
             * Необходимо вызвать метод setRegion, т.к. в нем могут обновляться
             * дополнительные поля отображения измененной зоны
             */
            regionView.setRegion(updatedRegion);
            return regionViewRepository.save(regionView);
        }

        return null;
    }

    @Override
    public RegionView updateRegionPlanLayouts(String projectId, String regionId, List<RegionPlanLayout> planLayouts,
            StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            RegionView regionView = regionViewRepository.findByProjectIdAndId(projectId, regionId);
            if (regionView != null) {
                boolean allPlansExist = true;
                Map<String, Plan> projectPlans = new HashMap<>();

                for (RegionPlanLayout planLayout : planLayouts) {
                    String planId = planLayout.getPlanId();
                    if (!projectPlans.containsKey(planId)) {
                        Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
                        if (plan != null) {
                            projectPlans.put(planId, plan);
                        } else {
                            errorMessage.append("Не найден план с идентификатором ").append(planId);
                            allPlansExist = false;
                            break;
                        }
                    }
                    if (
                        planLayout.getMinX() < 0.0 ||
                        planLayout.getMaxX() > projectPlans.get(planId).getxSize() ||
                        planLayout.getMinY() < 0.0 ||
                        planLayout.getMaxY() > projectPlans.get(planId).getySize()
                    ) {
                        errorMessage.append("Зона выходит за пределы плана").append(planLayout.getMinX());
                        allPlansExist = false;
                        break;
                    }
                }
                if (allPlansExist) {
                    regionView.setPlanLayouts(planLayouts);
                    return regionViewRepository.save(regionView);
                }

            } else {
                errorMessage.append("Зона не найдена в проекте");
            }
        }

        return null;
    }

    private AggregatedActiveRegionView getAggregatedActiveRegionViewByActiveRegion(String projectId,
            ActiveRegion activeRegion) {
        AggregatedActiveRegionView result = null;
        if (activeRegion != null) {
            RegionView regionView = regionViewRepository.findByProjectIdAndId(projectId, activeRegion.getId());
            StateCategoryView generalStateCategoryView = stateViewService
                    .getStateCategoryViewById(activeRegion.getGeneralStateCategoryId());

            result = new AggregatedActiveRegionView(regionView);
            result.setGeneralStateCategoryView(generalStateCategoryView);
            result.setOnGuard(activeRegion.isOnGuard());
            result.setFilterTags(activeRegion.getFilterTags());
        }
        return result;
    }

    @Override
    public List<AggregatedActiveRegionView> getAggregatedActiveRegionViews(String projectId) {
        List<AggregatedActiveRegionView> result = new ArrayList<>();

        int nRegionPages = (activeRegionRepository.countByProjectId(projectId)
                / AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE) + 1;
        for (int pageNo = 0; pageNo < nRegionPages; ++pageNo) {
            result.addAll(getAggregatedActiveRegionViewsByProjectId(projectId, AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE, pageNo));
        }
        return result;
    }

    @Override
    public AggregatedActiveRegionView getAggregatedActiveRegionViewByRegionId(String projectId, String regionId) {
        ActiveRegion activeRegion = activeRegionRepository.findOne(regionId);
        return getAggregatedActiveRegionViewByActiveRegion(projectId, activeRegion);
    }

    public boolean checkAndAdditionRegionViewToImportProject(String projectId, List<RegionView> regionViews,
            Map<String, String> oldAndNewPlansId, Map<String, String> oldAndNewRegionsId, StringBuilder errorMessage) {

        Map<String, Plan> projectPlans = new HashMap<>();
        for (RegionView regionView : regionViews) {
            regionView.setRegion(regionRepository.findByProjectIdAndId(projectId,
                    oldAndNewRegionsId.get(regionView.getRegion().getId())));
            for (RegionPlanLayout regionPlanLayout : regionView.getPlanLayouts()) {
                if ((regionPlanLayout.getPlanId() != null)
                        && oldAndNewPlansId.containsKey(regionPlanLayout.getPlanId())) {
                    regionPlanLayout.setPlanId(oldAndNewPlansId.get(regionPlanLayout.getPlanId()));
                } else {
                    errorMessage.append("У отображения зоны ").append(regionView.getName()).append(" нет идентификатора плана");
                    return false;
                }
            }
            regionViewRepository.insert(regionView);
            for (RegionPlanLayout planLayout : regionView.getPlanLayouts()) {
                String planId = planLayout.getPlanId();
                if (!projectPlans.containsKey(planId)) {
                    Plan plan = planRepository.findByProjectIdAndId(projectId, planId);
                    if (plan != null) {
                        projectPlans.put(planId, plan);
                    } else {
                        errorMessage.append("План, к которому привязана зона ").append(regionView.getName()).append(" не сущесвует");
                        return false;
                    }
                }
                if (planLayout.getMinX() < 0.0 || planLayout.getMaxX() > projectPlans.get(planId).getxSize()
                        || planLayout.getMinY() < 0.0 || planLayout.getMaxY() > projectPlans.get(planId).getySize()) {
                    errorMessage.append("Координаты зоны ").append(regionView.getName()).append(" указаны неправильно");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public List<RegionView> generateRegionViews(List<Region> regions) {
        List<RegionView> regionViews = new ArrayList<>();
        for (Region region : regions)
            regionViews.add(new RegionView(region));
        return regionViews;
    }

    @Override
    public List<RegionView> createByRecovery(String projectId, List<Region> regions, Map<String, String> oldToNewIds,
            StringBuilder errorMessage) {
        List<Region> createdRegions = regionService.createByRecovery(projectId, regions, oldToNewIds, errorMessage);
        if (createdRegions == null)
            return null;
        return regionViewRepository.insert(generateRegionViews(createdRegions));
    }

    @Override
    public void remove(String projectId, List<RegionView> regionViews) {
        List<String> regionIds = regionViews.stream().map(RegionView::getId).collect(Collectors.toList());
        regionViewRepository.deleteByProjectIdAndIdIn(projectId, regionIds);
        regionRepository.deleteByProjectIdAndIdIn(projectId, regionIds);
    }

    @Override
    public void safelyWriteAggregatedActiveRegionsByProjectId(String projectId, OutputStream outputStream,
                                                           ObjectMapper objectMapper) throws IOException {
        long nRegionPages = (activeRegionRepository.countByProjectId(projectId)
                / AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE) + 1;
        outputStream.write("[".getBytes());
        for (int pageNo = 0; pageNo < nRegionPages; ++pageNo) {
            List<AggregatedActiveRegionView> regions = getAggregatedActiveRegionViewsByProjectId(projectId,
                    AGGREGATED_ACTIVE_REGION_SAFE_PAGE_SIZE, pageNo);
            for (int regionNo = 0; regionNo < regions.size(); ++regionNo) {
                AggregatedActiveRegionView region = regions.get(regionNo);
                outputStream.write(objectMapper.writeValueAsBytes(region));
                if (pageNo < (nRegionPages - 1) || regionNo < (regions.size() - 1)) {
                    outputStream.write(",".getBytes());
                }
            }
            outputStream.flush();
        }
        outputStream.write("]".getBytes());
    }

    @Override
    public List<AggregatedActiveRegionView> getAggregatedActiveRegionViewsByProjectId(String projectId,
                                                                                      long pageSize, int pageNo) {
        return getActiveRegionViewsByProjectIdAndTimeAfter(projectId, null, pageSize, pageNo);
    }

    @Override
    public List<AggregatedActiveRegionView> getActiveRegionViewsByTimeAfter(Date lastObserverTime,
                                                                            long pageSize, int pageNo) {
        return getActiveRegionViewsByProjectIdAndTimeAfter(null, lastObserverTime, pageSize, pageNo);
    }

    private List<AggregatedActiveRegionView> getActiveRegionViewsByProjectIdAndTimeAfter(String projectId,
                                                                                         Date lastObservedTime,
                                                                                         long pageSize, int pageNo) {
        List<AggregatedActiveRegionView> regionViewsFromScript = new ArrayList<>();
        try {
            /* Перенаправляем запрос в mongo-скрипт */
            ScriptOperations scriptOperations = mongoTemplate.scriptOps();
            String lastObservedTimeString = null;
            if (lastObservedTime != null){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                lastObservedTimeString = simpleDateFormat.format(lastObservedTime);
            }
            BasicDBList dbList = (BasicDBList) scriptOperations
                    .call(GET_AGGREGATED_ACTIVE_REGIONS_SCRIPT_NAME, projectId, lastObservedTimeString,
                            pageSize, pageNo);
            for (Object dbObject : dbList) {
                regionViewsFromScript.add(mongoTemplate.getConverter().read(AggregatedActiveRegionView.class,
                        (DBObject) dbObject));
            }
        } catch (Exception e) {
            logger.error("Failed to get aggregated active region views via mongo-script, exception: ", e);
            throw e;
        }

        return regionViewsFromScript;
    }
}
