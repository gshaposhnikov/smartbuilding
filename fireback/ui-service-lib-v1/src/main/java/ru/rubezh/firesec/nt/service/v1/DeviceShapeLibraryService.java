package ru.rubezh.firesec.nt.service.v1;

import java.util.Date;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.DeviceShapeLibrary;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

public interface DeviceShapeLibraryService {

    interface DeviceShapeLibraryObserver{
        /**
         * Обновить состояние "подписчика" библиотеки шейпов
         * @param deviceShapeLibraryId идентификатор библиотеки шейпов,
         *                            используемый у подписчика
         * @param errorMessage сообщение возможной ошибки
         * @return true, если "подписчик" разрешает удаление библиотеки
         * шейпов, иначе false
         */
        boolean handleDeviceShapeLibraryDeletion(String deviceShapeLibraryId, StringBuilder errorMessage);
    }
    /**
     * Получить актуальные библиотеки шейпов.
     * 
     * Актуальными считаются те библиотеки, для которых в системе
     * зарегистрирован соответствующий профиль устройства, а также не отмеченные
     * для удаления.
     * 
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return набор актуальных библиотек шейпов устройств или null (в случае
     *         ошибки)
     */
    List<DeviceShapeLibrary> getActual(StringBuilder errorMessage);

    /**
     * Получить библиотеку шейпов устройств по идентификатору, если она
     * актуальна и существует.
     * 
     * Актуальными считаются те библиотеки, для которых в системе
     * зарегистрирован соответствующий профиль устройства, а также не отмеченные
     * для удаления.
     * 
     * @param deviceShapeLibraryId
     *            идентификатор библиотеки шейпов устройств
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return искомая библиотека или null (в случае ошибки)
     */
    DeviceShapeLibrary getIfActual(String deviceShapeLibraryId, StringBuilder errorMessage);

    /**
     * Добавить новую библиотеку шейпов устройств.
     * 
     * Для библиотеки обязательно должен быть указан идентификатор существующего
     * профиля устройств.
     * 
     * Название библиотеки в паре с идентификатором профиля устройств должны
     * быть уникальными.
     * 
     * Должен автоматически добавляться и базовый шейп - копия шейпа библиотеки
     * по умолчанию для данного (обязательно указанного в параметрах библиотеки)
     * профиля устройств.
     * 
     * Добавленной библиотеке должно выставляться время создания (текущее).
     * 
     * @param deviceShapeLibrary
     *            параметры создаваемой библиотеки
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean add(DeviceShapeLibrary deviceShapeLibrary, StringBuilder errorMessage);

    /**
     * Изменить существующую библиотеку шейпов устройств.
     * 
     * Измененной библиотеке должно выставляться время изменения (текущее).
     * 
     * @param deviceShapeLibraryId
     *            идентификатор изменяемой библиотеки
     * @param deviceShapeLibrary
     *            новые параметры библиотеки
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean update(String deviceShapeLibraryId, DeviceShapeLibrary deviceShapeLibrary,
            StringBuilder errorMessage);

    /**
     * Удалить существующую библиотеку шейпов.
     * 
     * Библиотека должна быть только отмеченна к удалению.
     * 
     * Нельзя удалить встроенную библиотеку.
     * 
     * @param deviceShapeLibraryId
     *            идентификатор удаляемой библиотеки
     * @param errorMessage
     *            сообщение с ошибкой в случае ошибки
     * @return true в случае успеха, иначе false
     */
    boolean remove(String deviceShapeLibraryId, StringBuilder errorMessage);

    /**
     * Получить изменения с указанного времени (все созданные и измененные
     * библиотеки шейпов устройств, а также идентификаторы удаленных), и
     * выполнить фактическое удаление отмеченных для этого библиотек, а также
     * отметить на удаление все относящиеся к ним шейпы.
     * 
     * @param from
     *            момент времени, изменения от которого запрашиваются
     * @return контейнер измененных сущностей
     */
    UpdatedEntities.EntitiesContainer<DeviceShapeLibrary> getChangesAndDoRemove(Date from);

    /**
     * Добавить "подписчика" на изменения библиотеки шейпов устройств
     *
     * @param deviceShapeLibraryObserver "подписчик" на изменения
     */
    void addObserver(DeviceShapeLibraryObserver deviceShapeLibraryObserver);

    /**
     * Убрать "подписчика" на изменения библиотеки шейпов устройств
     *
     * @param deviceShapeLibraryObserver "подписчик" на изменения
     */
    void removeObserver(DeviceShapeLibraryObserver deviceShapeLibraryObserver);

    /**
     * Оповестить "подписчиков" об изменениях в библиотеке шейпов устройств
     *
     * @param deviceShapeLibraryId идентификатор удялемой библиотеки шейпов
     * @param errorMessage сообщение возможной ошибки
     * @return true, если "подписчики" опрошены и операция с библиотекой
     * устройств разрешена, иначе false
     */
    boolean enquireObservers(String deviceShapeLibraryId, StringBuilder errorMessage);
}
