package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.*;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.*;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Order(3)
public class ScenarioViewServiceImpl implements ScenarioViewService, ActivationFilterableService {

    @Value("#{'${server.language:RUSSIAN}'}")
    private Language language;

    @Autowired
    private Logger logger;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ScenarioRepository scenarioRepository;

    @Autowired
    private ActiveScenarioRepository activeScenarioRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ScenarioDictionariesViewService scenarioDictionariesViewService;

    @Autowired
    private ScenarioTriggerTypeRepository scenarioTriggerTypeRepository;

    @Autowired
    private ScenarioActionTypeRepository scenarioActionTypeRepository;

    @Autowired
    private ScenarioActionTypeViewRepository scenarioActionTypeViewRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private ScenarioTriggerTypeViewRepository scenarioTriggerTypeViewRepository;

    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private DeviceProfileViewService deviceProfileViewService;

    @Autowired
    private StateViewRepository stateViewRepository;

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private IndicatorPanelService indicatorPanelService;

    @Autowired
    private ActiveScenarioService activeScenarioService;

    private static final int SCENARIO_SUBLOGIC_MAX_LEVEL = 15;

    private static final int COMPUTER_BLOCK_MAX_TITLE_LENGTH = 50;

    private static final int COMPUTER_BLOCK_MAX_MESSAGE_LENGTH = 200;

    private static final String SECONDS_POSTFIX = " секунд";

    /** Максимальное число сценариев, извлекаемое за раз mongo-скриптом*/
    public static final int AGGREGATED_ACTIVE_SCENARIOS_SAFE_PAGE_SIZE = 300;

    private static final String GET_AGGREGATED_ACTIVE_SCENARIOS_SCRIPT_NAME =
            "getAggregatedActiveScenarioViewsByProjectIdAndTimeAfter";

    @Override
    public void fetchScenarioTriggerTypeViews(DriverProfileView driverProfileView) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, ScenarioTriggerTypeView.class);
        /* Сначала удаляем старые версии отображений типов триггеров */
        for (ScenarioTriggerTypeView triggerTypeView: driverProfileView.getScenarioTriggerTypeViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(triggerTypeView.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии типов триггеров */
        bulkOps.insert(driverProfileView.getScenarioTriggerTypeViews());
        bulkOps.execute();
    }

    @Override
    public void fetchScenarioActionTypeViews(DriverProfileView driverProfileView) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, ScenarioActionTypeView.class);
        /* Сначала удаляем старые версии отображений типов действий */
        for (ScenarioActionTypeView actionTypeView: driverProfileView.getScenarioActionTypeViews())
            bulkOps.remove(new Query(Criteria.where("_id").is(actionTypeView.getId())));
        bulkOps.execute();
        /* Теперь сохраняем новые версии типов действий */
        bulkOps.insert(driverProfileView.getScenarioActionTypeViews());
        bulkOps.execute();
    }

    private void addNewLineAndTabs(StringBuilder sentence, int level) {
        sentence.append("\n");
        for (int i = 0; i < level; i++) {
            sentence.append("  ");
        }
    }

    private void generateLogicSentence(String projectId, ScenarioLogicBlock block, int level, StringBuilder sentence) {
        sentence.append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_BEGIN)
            .append(ScenarioDictionariesViewService.ScenarioLogicTypeView.
                typesByValue.get(block.getLogicType()).getSentence())
            .append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_EXPRESSION_MULTI_BEGIN);

        if (!block.getTriggerTypeId().isEmpty() && !block.getEntityIds().isEmpty()) {
            ScenarioTriggerTypeView scenarioTriggerTypeView =
                    scenarioTriggerTypeViewRepository.findOne(block.getTriggerTypeId());
            ScenarioTriggerType scenarioTriggerType =
                    scenarioTriggerTypeRepository.findOne(block.getTriggerTypeId());
            List<String> entityNames = getEntityNames(projectId, block, scenarioTriggerType);
            for (String entityName: entityNames) {
                addNewLineAndTabs(sentence, level + 1);
                sentence.append(scenarioTriggerTypeView.getName())
                    .append(" ")
                    .append(entityName);
            }
        }
        if (!block.getSubLogics().isEmpty()) {
            for (ScenarioLogicBlock logicBlock: block.getSubLogics()) {
                addNewLineAndTabs(sentence, level + 1);
                generateLogicSentence(projectId, logicBlock, level + 1, sentence);
            }
        }
        addNewLineAndTabs(sentence, level);
        sentence.append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_EXPRESSION_MULTI_END);
    }

    private List<String> getEntityNames(String projectId, ScenarioLogicBlock block, ScenarioTriggerType scenarioTriggerType) {
        List<String> entityNames = new ArrayList<>();
        switch (scenarioTriggerType.getEntityType()) {
        case REGION:
            List<Region> regions = regionRepository.findByProjectIdAndIdIn(projectId, block.getEntityIds());
            for(Region region: regions) {
                entityNames.add(ScenarioDictionariesViewService.SCENARIO_SENTENCE_ENTITY_NAME_PREPOSITION
                        + region.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_REGION
                        + " " + Integer.toString(region.getIndex()) + ")");
            }
            break;
        case VIRTUAL_STATE:
            List<VirtualState> virtualStates = virtualStateRepository.findByProjectIdAndIdIn(projectId, block.getEntityIds());
            for(VirtualState virtualState: virtualStates) {
                entityNames.add(virtualState.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_VRITUAL
                        + " " + Integer.toString(virtualState.getGlobalNo()) + ")");
            }
            break;
        case CONTROL_DEVICE:
        case DEVICE:
            List<DeviceView> deviceViews = deviceViewRepository.findByProjectIdAndIdIn(projectId, block.getEntityIds());
            for(DeviceView deviceView: deviceViews) {
                String shortAddressPath = String.valueOf(deviceView.getDevice().getLineAddress());

                if (deviceView.getParentDeviceId() != null && !deviceView.getParentDeviceId().isEmpty()) {
                    shortAddressPath = deviceView.getDevice().getParentLineAddress()
                            + "." + deviceView.getDevice().getLineNo()
                            + "." + shortAddressPath;
                }
                entityNames.add(ScenarioDictionariesViewService.SCENARIO_SENTENCE_ENTITY_NAME_PREPOSITION
                        + deviceView.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_DEVICE
                        + " " + shortAddressPath + ")");
            }
            break;
        default:
            logger.warn("Не обрабатываемый тип сущности: {}", scenarioTriggerType.getEntityType().toString());
            break;
        }
        return entityNames;
    }

    private void fillDeviceIdListByLogicBlock(ScenarioLogicBlock logicBlock, Set<String> deviceIds,
            Map<String, ScenarioTriggerType> triggerTypes) {
        if (logicBlock.getEntityIds().size() > 0) {
            ScenarioTriggerType triggerType = triggerTypes.get(logicBlock.getTriggerTypeId());
            if (triggerType == null) {
                triggerType = scenarioTriggerTypeRepository.findOne(logicBlock.getTriggerTypeId());
                triggerTypes.put(triggerType.getId(), triggerType);
            }

            if (triggerType.getEntityType() == TriggerEntityType.CONTROL_DEVICE ||
                    triggerType.getEntityType() == TriggerEntityType.DEVICE) {
                deviceIds.addAll(logicBlock.getEntityIds());
            }

            for (ScenarioLogicBlock subLogicBlock: logicBlock.getSubLogics()) {
                fillDeviceIdListByLogicBlock(subLogicBlock, deviceIds, triggerTypes);
            }
        }
    }

    private void setTLBlockMediaByTreeItemType(TreeItemScenarioTimeLineBlockView treeItem) {
        Media media = scenarioDictionariesViewService.getTreeItemMedia(treeItem.getItemType());
        if (media != null) {
            treeItem.setActionIconName(media.getPath());
            treeItem.setActionIconColor(media.getFillColor());
        }
    }
    
    private TreeItemScenarioTimeLineBlockView getRootTimeItem(List<TreeItemScenarioTimeLineBlockView> tree, int time) {
        TreeItemScenarioTimeLineBlockView timeItem = null;
        for (TreeItemScenarioTimeLineBlockView item: tree) {
            if (item.getTimeDelaySec() == time) {
                timeItem = item;
                break;
            }
        }
        if (timeItem == null) {
            timeItem = new TreeItemScenarioTimeLineBlockView();
            timeItem.setTimeDelaySec(time);
            timeItem.setItemType(TreeItemType.TIME);
            setTLBlockMediaByTreeItemType(timeItem);
            tree.add(timeItem);
        }
        return timeItem;
    }

    private TreeItemDeviceView findDeviceViewById(List<TreeItemDeviceView> treeItemDeviceViews, String id) {
        for (TreeItemDeviceView treeItemDeviceView : treeItemDeviceViews) {
            if (treeItemDeviceView.getId().equals(id)) {
                return treeItemDeviceView;
            }
        }
        return null;
    }
    
    private List<TreeItemScenarioTimeLineBlockView> getTimeLineBlocksTree(Scenario scenario) {
        List<TreeItemScenarioTimeLineBlockView> tree = new ArrayList<>();
        List<TreeItemDeviceView> treeItemDeviceViews = deviceViewService
                .getTreeItemDeviceViewsByProjectId(scenario.getProjectId(), language);

        /* добавим корневой элемент - вход в сценарий */
        TreeItemScenarioTimeLineBlockView headItem = new TreeItemScenarioTimeLineBlockView();
        headItem.setDescription(scenario.getName());
        headItem.setTimeDelaySec(-1);
        headItem.setItemType(TreeItemType.ROOT);
        setTLBlockMediaByTreeItemType(headItem);
        tree.add(headItem);
        /* добавим все начала и окончания слежений */
        gatherTracingAndNoTracingTreeItems(scenario, tree, treeItemDeviceViews);
        /* добавим все исполнительные блоки */
        gatherExecutiveTreeItems(scenario, tree, treeItemDeviceViews);
        /* добавим все блоки "действие на компьютере" */
        gatherComputerActionTreeItems(scenario, tree);
        /* упорядочим дерево */
        arrangeTree(tree);
        /* добавляем элемент выхода из сценария */
        TreeItemScenarioTimeLineBlockView exitItem = new TreeItemScenarioTimeLineBlockView();
        exitItem.setItemType(TreeItemType.OUT);
        setTLBlockMediaByTreeItemType(exitItem);
        tree.add(exitItem);

        return tree;
    }
    
    private void arrangeTree(List<TreeItemScenarioTimeLineBlockView> tree) {
        /* Рекурсивный спуск */
        for (TreeItemScenarioTimeLineBlockView item: tree) {
            if (!item.getChildren().isEmpty()) {
                arrangeTree(item.getChildren());
            }
        }
        /* отсортируем по времени задержки */
        Collections.sort(tree);
    }

    private void gatherComputerActionTreeItems(Scenario scenario, List<TreeItemScenarioTimeLineBlockView> tree){
        for (int index = 0; index < scenario.getTimeLineBlocks().size(); index++) {
            ScenarioTimeLineBlock block = scenario.getTimeLineBlocks().get(index);

            if (block.getBlockType() == BlockType.COMPUTER_ACTION){
                // найдём корневой элемент
                TreeItemScenarioTimeLineBlockView rootItem = getRootTimeItem(tree, block.getTimeDelaySec());
                // создадим элемент для самого блока
                TreeItemScenarioTimeLineBlockView blockItem = new TreeItemScenarioTimeLineBlockView();
                blockItem.setNumber(index);
                fillComputerActionBlockTreeItem(index, block, blockItem);
                // добавим блок к родителю
                rootItem.getChildren().add(blockItem);
                /* добавляем элемент выхода из блока */
                TreeItemScenarioTimeLineBlockView endItem = new TreeItemScenarioTimeLineBlockView();
                endItem.setNumber(index);
                endItem.setItemType(TreeItemType.OUT);
                endItem.setTimeDelaySec(block.getTimeDelaySec());
                setTLBlockMediaByTreeItemType(endItem);
                rootItem.getChildren().add(endItem);
            }
        }
    }

    private void gatherExecutiveTreeItems(Scenario scenario, List<TreeItemScenarioTimeLineBlockView> tree,
            List<TreeItemDeviceView> treeItemDeviceViews) {
        for (int index = 0; index < scenario.getTimeLineBlocks().size(); index++) {
            ScenarioTimeLineBlock block = scenario.getTimeLineBlocks().get(index);

            if (block.getBlockType() == BlockType.EXECUTIVE) {
                // найдём корневой элемент
                TreeItemScenarioTimeLineBlockView rootItem = getRootTimeItem(tree, block.getTimeDelaySec());
                // создадим элемент для самого блока
                TreeItemScenarioTimeLineBlockView blockItem = new TreeItemScenarioTimeLineBlockView();
                blockItem.setNumber(index);
                fillExecutiveBlockTreeItem(treeItemDeviceViews, index, block, blockItem);
                // добавим блок к родителю
                rootItem.getChildren().add(blockItem);
                /* добавляем элемент выхода из блока */
                TreeItemScenarioTimeLineBlockView endItem = new TreeItemScenarioTimeLineBlockView();
                endItem.setNumber(index);
                endItem.setItemType(TreeItemType.OUT);
                endItem.setTimeDelaySec(block.getTimeDelaySec());
                setTLBlockMediaByTreeItemType(endItem);
                rootItem.getChildren().add(endItem);
            }
        }
    }

    private void gatherTracingAndNoTracingTreeItems(Scenario scenario, List<TreeItemScenarioTimeLineBlockView> tree,
            List<TreeItemDeviceView> treeItemDeviceViews) {
        for (int index = 0; index < scenario.getTimeLineBlocks().size(); index++) {
            ScenarioTimeLineBlock block = scenario.getTimeLineBlocks().get(index);

            if (block.getBlockType() == BlockType.TRACING) {
                /* добавляем элемент начала слежения */
                TreeItemScenarioTimeLineBlockView startItem = new TreeItemScenarioTimeLineBlockView();
                startItem.setNumber(index);
                fillTracingBlockTreeItem(treeItemDeviceViews, block, startItem);
                TreeItemScenarioTimeLineBlockView rootItem = getRootTimeItem(tree, block.getTracingStartSec());
                rootItem.getChildren().add(startItem);
                /* добавляем элемент окончания слежения */
                TreeItemScenarioTimeLineBlockView stopItem = new TreeItemScenarioTimeLineBlockView();
                stopItem.setNumber(index);
                fillNoTracingBlockTreeItem(treeItemDeviceViews, block, stopItem);
                rootItem = getRootTimeItem(tree, block.getTracingStartSec() + block.getTracingPeriodSec());
                rootItem.getChildren().add(stopItem);
            }
        }
    }

    private void fillComputerActionBlockTreeItem(int index, ScenarioTimeLineBlock block,
                                                 TreeItemScenarioTimeLineBlockView blockItem){
        blockItem.setItemType(TreeItemType.COMPUTER_ACTION);
        blockItem.setTimeDelaySec(block.getTimeDelaySec());
        setTLBlockMediaByTreeItemType(blockItem);
        blockItem.setActionName(block.getName());
        fillComputerActionTreeItems(index, block, blockItem);
    }

    private void fillTracingBlockTreeItem(List<TreeItemDeviceView> treeItemDeviceViews, ScenarioTimeLineBlock block,
            TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTracingStartSec());
        blockItem.setItemType(TreeItemType.TRACE);
        blockItem.setActionName(block.getName() + " (вкл.)");
        fillGeneralTracingBlockTreeItemInfo(treeItemDeviceViews, block, blockItem);
    }

    private void fillNoTracingBlockTreeItem(List<TreeItemDeviceView> treeItemDeviceViews, ScenarioTimeLineBlock block,
            TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTracingStartSec() + block.getTracingPeriodSec());
        blockItem.setItemType(TreeItemType.NO_TRACE);
        blockItem.setActionName(block.getName() + " (выкл.)");
        fillGeneralTracingBlockTreeItemInfo(treeItemDeviceViews, block, blockItem);
    }

    private void fillGeneralTracingBlockTreeItemInfo(List<TreeItemDeviceView> treeItemDeviceViews,
                                                     ScenarioTimeLineBlock block,
                                                     TreeItemScenarioTimeLineBlockView blockItem){
        setTLBlockMediaByTreeItemType(blockItem);
        blockItem.setEntityId(block.getTracingEntityId());
        switch (block.getTracingEntityType()) {
            case SENSOR_DEVICE: {
                TreeItemDeviceView deviceView = findDeviceViewById(treeItemDeviceViews, block.getTracingEntityId());
                blockItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
                blockItem.setDescription(" " + deviceView.getName()
                        + " (" + deviceView.getShortAddressPath() + ")");
                blockItem.setEntityType(EntityType.DEVICE);
                break;
            }
            case VIRTUAL_STATE: {
                VirtualState virtualState = virtualStateRepository.findOne(block.getTracingEntityId());
                blockItem.setDescription("Вирт. состояние: " + virtualState.getName());
                blockItem.setEntityType(EntityType.VIRTUAL_STATE);
                break;
            }
            default:
                break;
        }
    }
    
    private void fillExecutiveBlockTreeItem(List<TreeItemDeviceView> treeItemDeviceViews, int index,
            ScenarioTimeLineBlock block, TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTimeDelaySec());
        blockItem.setItemType(block.getConditionType() == ConditionType.NONE
                ? TreeItemType.IN : TreeItemType.CONDITION);
        setTLBlockMediaByTreeItemType(blockItem);
        blockItem.setActionName(block.getName());
        blockItem.setEntityId(block.getConditionEntityId());
        switch (block.getConditionType()) {
            case DEVICE_STATE: {
                TreeItemDeviceView deviceView = findDeviceViewById(treeItemDeviceViews, block.getConditionEntityId());
                StateView stateView = stateViewRepository.findOne(block.getConditionDeviceStateId());
                blockItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
                blockItem.setDescription(deviceView.getName() + " (" + deviceView.getShortAddressPath() + ")" 
                        + (block.getConditionCheckType() == ConditionCheckType.IS_ACTIVE ? " активно " : " не активно ")
                        + "'" + stateView.getName() + "'");
                blockItem.setEntityType(EntityType.DEVICE);
                break;
            }
            case VIRTUAL_STATE: {
                VirtualState virtualState = virtualStateRepository.findOne(block.getConditionEntityId());
                blockItem.setDescription("Если" + (block.getConditionCheckType() == ConditionCheckType.IS_ACTIVE ? " активно " : " не активно ")
                        + "вирт. состояние '" + virtualState.getName() + "'");
                blockItem.setEntityType(EntityType.VIRTUAL_STATE);
                break;
            }
            default:
                break;
        }
        fillActionTreeItems(treeItemDeviceViews, index, block, blockItem);
    }

    private void fillComputerActionTreeItems(int index, ScenarioTimeLineBlock block, TreeItemScenarioTimeLineBlockView blockItem){
        for (ComputerAction computerAction: block.getComputerActions()){
            TreeItemScenarioTimeLineBlockView computerActionItem = new TreeItemScenarioTimeLineBlockView();
            computerActionItem.setNumber(index);
            StringBuilder computerActionDescription = new StringBuilder(computerAction.getMessage());
            if (computerAction.getCommand() == Command.COUNTDOWN){
                computerActionItem.setItemType(TreeItemType.COUNTDOWN);
                computerActionDescription.append(": ").append(computerAction.getCountdownTimeSec()).append(SECONDS_POSTFIX);
            } else {
                computerActionItem.setItemType(TreeItemType.SHOW_MESSAGE);
            }

            computerActionItem.setActionName(computerAction.getCommand().getLabel());
            setTLBlockMediaByTreeItemType(computerActionItem);

            computerActionItem.setDescription(computerActionDescription.toString());
            blockItem.getChildren().add(computerActionItem);
        }
    }

    private void fillActionTreeItems(List<TreeItemDeviceView> treeItemDeviceViews, int index, ScenarioTimeLineBlock block,
            TreeItemScenarioTimeLineBlockView blockItem) {
        // добавим элементы действий
        for (ScenarioTimeLineBlock.Action action: block.getActions()) {
            TreeItemScenarioTimeLineBlockView actionItem = new TreeItemScenarioTimeLineBlockView();
            actionItem.setNumber(index);
            actionItem.setItemType(TreeItemType.ACTION);
            actionItem.setEntityId(action.getEntityId());

            ScenarioActionType actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
            ScenarioActionTypeView actionTypeView = scenarioActionTypeViewRepository.findOne(action.getActionTypeId());

            actionItem.setActionName(actionTypeView.getName());
            actionItem.setActionIconName(actionTypeView.getIconMedia().getPath());
            actionItem.setActionIconColor(actionTypeView.getIconMedia().getFillColor());
            actionItem.setEntityType(EntityType.valueOf(actionType.getEntityType().toString()));

            switch (actionType.getEntityType()) {
            case DEVICE:
                TreeItemDeviceView deviceView = findDeviceViewById(treeItemDeviceViews, action.getEntityId());
                actionItem.setDescription(deviceView.getName() + " (" + deviceView.getShortAddressPath() + ")");
                actionItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
                break;
            case REGION:
                Region region = regionRepository.findOne(action.getEntityId());
                actionItem.setDescription(region.getName() + " (No: " + region.getIndex() + ")");
                break;
            case SCENARIO:
                Scenario subScenario = scenarioRepository.findOne(action.getEntityId());
                actionItem.setDescription(subScenario.getName() + " (No: " + subScenario.getGlobalNo() + ")");
                break;
            case VIRTUAL_STATE:
                VirtualState virtualState = virtualStateRepository.findOne(action.getEntityId());
                actionItem.setDescription(virtualState.getName() + " (No: " + virtualState.getGlobalNo() + ")");
                break;
            default:
                break;
            }

            blockItem.getChildren().add(actionItem);
        }
    }

    private Set<String> getDeviceIds(Scenario scenario) {
        Set<String> deviceIds = new HashSet<>();
        Map<String, ScenarioTriggerType> triggerTypes = new HashMap<>();
            fillDeviceIdListByLogicBlock(scenario.getStartLogic(), deviceIds, triggerTypes);
        if (scenario.getStopLogic() != null)
            fillDeviceIdListByLogicBlock(scenario.getStopLogic(), deviceIds, triggerTypes);

        Map<String, ScenarioActionType> actionTypes = new HashMap<>();
        for (ScenarioTimeLineBlock timeLineBlock: scenario.getTimeLineBlocks()) {
            switch (timeLineBlock.getBlockType()) {
            case TRACING:
                if (timeLineBlock.getTracingEntityType() == ScenarioTimeLineBlock.TracingEntityType.SENSOR_DEVICE)
                    deviceIds.add(timeLineBlock.getTracingEntityId());
                break;
            case EXECUTIVE:
                if (timeLineBlock.getConditionType() == ScenarioTimeLineBlock.ConditionType.DEVICE_STATE)
                    deviceIds.add(timeLineBlock.getConditionEntityId());
                for (Action action : timeLineBlock.getActions()) {
                    ScenarioActionType actionType = actionTypes.get(action.getActionTypeId());
                    if (actionType == null) {
                        actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
                        actionTypes.put(action.getActionTypeId(), actionType);
                    }
                    if (actionType.getEntityType() == ActionEntityType.DEVICE) {
                        deviceIds.add(action.getEntityId());
                    }
                }
                break;
            case COMPUTER_ACTION:
            default:
                break;
            }
        }

        return deviceIds;
    }

    public AggregatedScenarioView getAggregatedScenarioView(Scenario scenario) {
        AggregatedScenarioView scenarioView = new AggregatedScenarioView(scenario);
        scenarioDictionariesViewService.fillScenarioColors(scenario, scenarioView);
        scenarioView.setDeviceIds(new ArrayList<>(getDeviceIds(scenario)));

        ScenarioBasicParams scenarioBasicParams = new ScenarioBasicParams(scenario);
        scenarioView.setBasicParams(scenarioBasicParams);

        ScenarioAdvancedParams scenarioAdvancedParams = new ScenarioAdvancedParams(scenario);
        StringBuilder sentence;
        if (scenario.getStartLogic() != null) {
            sentence = new StringBuilder();
            generateLogicSentence(scenario.getProjectId(), scenario.getStartLogic(), 0, sentence);
            scenarioAdvancedParams.setStartLogicSentence(sentence.toString());
        }
        if (scenario.getStopLogic() != null) {
            sentence = new StringBuilder();
            generateLogicSentence(scenario.getProjectId(), scenario.getStopLogic(), 0, sentence);
            scenarioAdvancedParams.setStopLogicSentence(sentence.toString());
        }
        scenarioView.setAdvancedParams(scenarioAdvancedParams);

        scenarioView.setTimeLineBlocksTree(getTimeLineBlocksTree(scenario));

        return scenarioView;
    }

    private AggregatedActiveScenarioView getAggregatedActiveScenarioView(ActiveScenario activeScenario) {
        StateCategoryView generalStateCategoryView =
                stateViewService.getStateCategoryViewById(activeScenario.getGeneralStateCategoryId());
        AggregatedScenarioView aggregatedScenarioView =
                getAggregatedScenarioView(activeScenario.getScenarioProject());
        AggregatedActiveScenarioView aggregatedActiveScenarioView =
                new AggregatedActiveScenarioView(aggregatedScenarioView);
        aggregatedActiveScenarioView.setGeneralStateCategoryView(generalStateCategoryView);
        aggregatedActiveScenarioView.setFilterTags(activeScenario.getScenarioProject().getFilterTags());
        return aggregatedActiveScenarioView;
    }

    private void updateScenarioByScenarioBasicParams(Scenario scenario, ScenarioBasicParams params) {
        scenario.setName(params.getName());
        scenario.setDescription(params.getDescription());
        scenario.setScenarioPurpose(params.getScenarioPurpose());
        scenario.setScenarioType(params.getScenarioType());
        scenario.setControlDeviceId(params.getControlDeviceId());
        scenario.setEnabled(params.isEnabled());
    }
    
    @Override
    public boolean validateScenarioBasicParams(Scenario scenario, ScenarioBasicParams params, StringBuilder errorMessage) {
        boolean valid = true;
        if (params.getName() == null || params.getName().isEmpty()) {
            errorMessage.append("Не задано имя сценария");
            valid = false;
        }
        if (valid && params.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_INVOKE
                && (params.getControlDeviceId() == null ||
                        !deviceRepository.existsByIdAndDisabled(params.getControlDeviceId(), false))) {
            errorMessage.append("Прибор, заданный для исполнительного сценария не найден или отключен");
            valid = false;
        }
        if (valid && scenario != null && scenario.isEnabled() && !params.isEnabled()
                && !checkScenarioInvocationsAbsent(scenario.getProjectId(), scenario.getId(), errorMessage)) {
            valid = false;
        }
        return valid;
    }

    private boolean validateScenarioLogicFullness(ScenarioLogicBlock block,
                                               StringBuilder errorMessage, int scenarioSubLogicLevelCounter) {
        if (block.getLogicType() == null) {
            errorMessage.append("Для логического блока не задан тип логики");
            return false;
        }
        if (block.getSubLogics() != null && !block.getSubLogics().isEmpty()) {
            scenarioSubLogicLevelCounter++;
            if(scenarioSubLogicLevelCounter > SCENARIO_SUBLOGIC_MAX_LEVEL) {
                errorMessage.append("Для логического блока превышено количество уровней логики");
                return false;
            }
            for (ScenarioLogicBlock logicBlock: block.getSubLogics()) {
                if (!validateScenarioLogicFullness(logicBlock, errorMessage, scenarioSubLogicLevelCounter)) {
                    return false;
                }
            }
            return true;
        }
        if (block.getTriggerTypeId() == null || block.getTriggerTypeId().isEmpty()) {
            errorMessage.append("Для логического блока не задано событие");
            return false;
        }
        if (block.getEntityIds() == null || block.getEntityIds().isEmpty()) {
            errorMessage.append("Для логического блока не задан ни один объект");
            return false;
        }
        return true;
    }

    private boolean validateScenarioLogicReferences(String projectId,
                                               ScenarioLogicBlock block,
                                               ScenarioPurpose scenarioPurpose,
                                               StringBuilder errorMessage) {
        if (block.getTriggerTypeId() != null && !block.getTriggerTypeId().isEmpty()) {
            ScenarioTriggerType trigger = scenarioTriggerTypeRepository.findOne(block.getTriggerTypeId());
            if (trigger == null) {
                errorMessage.append("Тип события логического блока не найден");
                return false;
            }
            if (trigger.getScenarioPurpose() != scenarioPurpose){
                errorMessage.append("Используется тип события, недопустимый для сценария с таким назначением");
                return false;
            }
            if (block.getEntityIds() != null && !block.getEntityIds().isEmpty()) {
                switch (trigger.getEntityType()) {
                case DEVICE:
                case CONTROL_DEVICE:
                    if (deviceRepository.countByProjectIdAndIdInAndDisabled(projectId, block.getEntityIds(), false) !=
                            block.getEntityIds().size()) {
                        errorMessage.append("Не найдено или отключено устройство, на которое ссылается логика");
                        return false;
                    }
                    break;
                case REGION:
                    if (regionRepository.countByProjectIdAndIdIn(projectId, block.getEntityIds()) != block.getEntityIds().size()) {
                        errorMessage.append("Найдены не все зоны, на которые ссылается логика");
                        return false;
                    }
                    break;
                case VIRTUAL_STATE:
                    if (virtualStateRepository.countByProjectIdAndIdIn(projectId, block.getEntityIds()) != block.getEntityIds().size()) {
                        errorMessage.append("Найдены не все виртуальные состояния, на которые ссылается логика");
                        return false;
                    }
                    break;
                default:
                    errorMessage.append("Тип события логического блока ссылается на неизвестный тип объекта");
                    return false;
                }
            }
        }
        for (ScenarioLogicBlock subLogic: block.getSubLogics()) {
            if (!validateScenarioLogicReferences(projectId, subLogic, scenarioPurpose, errorMessage)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean validateScenarioLogicBlock(String projectId,
                                               ScenarioLogicBlock block,
                                               ScenarioPurpose scenarioPurpose,
                                               StringBuilder errorMessage) {
        int scenarioSubLogicLevelCounter = 0;
        if (!validateScenarioLogicFullness(block, errorMessage, scenarioSubLogicLevelCounter)) {
            return false;
        }
        return validateScenarioLogicReferences(projectId, block, scenarioPurpose, errorMessage);
    }
    
    @Override
    public boolean validateScenarioTimeLineBlock(ScenarioTimeLineBlock block, StringBuilder errorMessage,
                                                  ScenarioPurpose scenarioPurpose, boolean ignoreScenarioActions) {
        boolean result = false;
        if (block.getTimeDelaySec() < 0) {
            errorMessage.append("Задержка не может быть меньше нуля");
        } else {
            switch (block.getBlockType()) {
            case TRACING:
                if (block.getTracingPeriodSec() < 0) {
                    errorMessage.append("Период слежения не может быть меньше нуля");
                } else {
                    switch (block.getTracingEntityType()) {
                    case SENSOR_DEVICE:
                        if (!deviceRepository.existsByIdAndDisabled(block.getTracingEntityId(), false)) { 
                            errorMessage.append("Не найдено или отключено устройство отслеживания (")
                            .append(block.getTracingEntityId()).append(")");
                        } else {
                            result = true;
                        }
                        break;
                    case VIRTUAL_STATE:
                        if (!virtualStateRepository.exists(block.getTracingEntityId())) {
                            errorMessage.append("Не найдено вирт. состояние отслеживания (")
                            .append(block.getTracingEntityId()).append(")");
                        } else {
                            result = true;
                        }
                        break;
                    }
                }
                break;
            case EXECUTIVE:
                switch (block.getConditionType()) {
                case DEVICE_STATE:
                    Device device = deviceRepository.findOne(block.getConditionEntityId());
                    if (device == null) {
                        errorMessage.append("Не найдено устройство для проверки условия (")
                        .append(block.getConditionEntityId()).append(")");
                    } else if (device.isDisabled()) {
                        errorMessage.append("Устройство для проверки условия (")
                        .append(block.getConditionEntityId()).append(") отключено");
                    } else {
                        DeviceProfileView deviceProfileView = deviceProfileViewService
                                .getByLanguageAndDeviceProfileId(language, device.getDeviceProfileId());
                        for (StateView stateView : deviceProfileView.getStateViews()) {
                            if (stateView.getId().equals(block.getConditionDeviceStateId())) {
                                if (stateView.getState().isAcceptableAsScenarioCondition()) {
                                    result = true;
                                    break;
                                } else {
                                    errorMessage.append("Состояние устройства ").append(stateView.getName()).append(
                                            " не может быть использовано в качестве условия к блоку сценария");
                                }
                            }
                        }
                        if (!result) {
                            errorMessage.append("Состояние устройства ").append(block.getConditionDeviceStateId())
                            .append(" не найдено в профиле указанного устройства");
                        }
                    }
                    break;
                case VIRTUAL_STATE:
                    if (!virtualStateRepository.exists(block.getConditionEntityId())) {
                        errorMessage.append("Не найдено вирт. состояние для проверки условия (")
                        .append(block.getConditionEntityId()).append(")");
                    } else {
                        result = true;
                    }
                    break;
                case NONE:
                default:
                    result = true;
                    break;
                }
                if (result) {
                    for (Action action: block.getActions()) {
                        ScenarioActionType actionType = scenarioActionTypeRepository.findOne(
                                action.getActionTypeId());
                        if (actionType == null) {
                            result = false;
                            errorMessage.append("Тип действия не найден (")
                                    .append(action.getActionTypeId()).append(")");
                        } else if (actionType.getScenarioPurpose() != null
                                && actionType.getScenarioPurpose() != scenarioPurpose){
                            result = false;
                            errorMessage.append("В блоке сценария указано действие с неподходящим типом назначения");
                        } else {
                            switch (actionType.getEntityType()) {
                            case DEVICE:
                                if (!deviceRepository.existsByIdAndDisabled(action.getEntityId(), false)) {
                                    result = false;
                                    errorMessage.append("Не найдено или отключено устройство (")
                                    .append(action.getEntityId()).append(")");
                                }
                                break;
                            case REGION:
                                if (regionRepository.findOne(action.getEntityId()) == null) {
                                    result = false;
                                    errorMessage.append("Не найдена зона (")
                                    .append(action.getEntityId()).append(")");
                                }
                                break;
                            case SCENARIO: {
                                if (!ignoreScenarioActions) {
                                    Scenario scenario = scenarioRepository.findOne(action.getEntityId());
                                    if (scenario == null) {
                                        result = false;
                                        errorMessage.append("Не найден сценарий (").append(action.getEntityId())
                                                .append(")");
                                    } else if (!scenario.isEnabled()) {
                                        result = false;
                                        errorMessage.append("Сценарий ").append(scenario.getName()).append(" выключен");
                                    }
                                }
                                break;
                            }
                            case VIRTUAL_STATE:
                                if (virtualStateRepository.findOne(action.getEntityId()) == null) {
                                    result = false;
                                    errorMessage.append("Не найдено вирт. состояние (")
                                    .append(action.getEntityId()).append(")");
                                }
                                break;
                            }
                        }
                        if (!result) break;
                    }
                }
                break;
            case COMPUTER_ACTION:
                result = true;
                for (ComputerAction computerAction: block.getComputerActions()) {
                    if (computerAction.getMessage().length() < 1 || computerAction.getMessage().length() > COMPUTER_BLOCK_MAX_MESSAGE_LENGTH){
                        result = false;
                        errorMessage.append("Сообщение должны быть не пустым и содержать не более 200 символов");
                    }
                    if (computerAction.getCommand() == Command.SHOW_MESSAGE ){
                        if (computerAction.getTitle().length() < 1 || computerAction.getTitle().length() > COMPUTER_BLOCK_MAX_TITLE_LENGTH){
                            result = false;
                            errorMessage.append("Заголовок должен быть не пустым и содержать не более 50 символов");
                        }
                    } else {
                        if (computerAction.getCountdownTimeSec() < 0){
                            result = false;
                            errorMessage.append("Время обратного отсчета не может быть отрицательным");
                        }
                    }
                    if (!result) break;
                }
                break;
            default:
                errorMessage.append("Неизвестный тип блока");
            }
        }
        return result;
    }

    public boolean validateTacticsScenarioActions(Scenario scenario, ScenarioTimeLineBlock block, StringBuilder errorMessage){
        ScenarioTriggerType scenarioTriggerType = scenarioTriggerTypeRepository
                .findOne(scenario.getStartLogic().getTriggerTypeId());
        if (scenarioTriggerType != null && scenarioTriggerType.getForcedScenarioActionTypeId() != null){
            for (Action action: block.getActions()) {
                if (!action.getActionTypeId().equals(scenarioTriggerType.getForcedScenarioActionTypeId())){
                    errorMessage.append("Неправильно задано действие в блоке");
                    return false;
                }
            }
        }
        return true;
    }

    public int getNewGlobalNo(String projectId) {
        Scenario scenario = scenarioRepository.findTopByProjectIdOrderByGlobalNoDesc(projectId);
        int result = 1;
        if (scenario != null)
            result += scenario.getGlobalNo();

        return result;
    }

    @Override
    public List<AggregatedScenarioView> getAllAggregatedScenarioViews(String projectId) {
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        return generateAggregatedScenarioViews(projectId, scenarios);
    }

    @Override
    public AggregatedScenarioView getAggregatedScenarioView(String projectId, String scenarioId,
            StringBuilder errorMessage) {
        Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
        if (scenario == null) {
            errorMessage.append("Сценарий не найден");
            return null;
        }
        return getAggregatedScenarioView(scenario);
    }

    @Override
    public List<AggregatedScenarioView> getAggregatedScenarioViewsContainingDevices(String projectId,
            List<String> deviceIds) {
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);

        Iterator<Scenario> scenariosToCheck = scenarios.iterator();
        while (scenariosToCheck.hasNext()) {
            Scenario scenario = scenariosToCheck.next();
            Set<String> containingDeviceIds = getDeviceIds(scenario);
            containingDeviceIds.retainAll(deviceIds);
            if (containingDeviceIds.isEmpty())
                scenariosToCheck.remove();
        }

        return generateAggregatedScenarioViews(projectId, scenarios);
    }

    @Override
    public List<AggregatedActiveScenarioView> getAllAggregatedActiveScenarioViews(String projectId) {
        List<ActiveScenario> activeScenarios = activeScenarioRepository.findByProjectId(projectId);
        List<AggregatedActiveScenarioView> results = new ArrayList<>();
        for (ActiveScenario activeScenario: activeScenarios) {
            results.add(getAggregatedActiveScenarioView(activeScenario));
        }
        return results;
    }

    @Override
    public AggregatedActiveScenarioView getAggregatedActiveScenarioView(String projectId, String scenarioId,
            StringBuilder errorMessage) {
        ActiveScenario activeScenario = activeScenarioRepository.findByProjectIdAndId(projectId, scenarioId);
        if (activeScenario == null) {
            errorMessage.append("Сценарий не найден: ").append(scenarioId);
            return null;
        }
        return getAggregatedActiveScenarioView(activeScenario);
    }

    @Override
    public RemovedScenarioInfo remove(String projectId, String scenarioId, StringBuilder errorMessage) {
        
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }
        
        /*  Проверим, что сценарий не используется в другом сценарии */
        if (!checkScenarioInvocationsAbsent(projectId, scenarioId, errorMessage)) {
            return null;
        }
        
        if (scenarioRepository.deleteById(scenarioId) == 0) {
            errorMessage.append("Сценарий не найден");
            return null;
        }
        RemovedScenarioInfo removedScenarioInfo = new RemovedScenarioInfo(projectId);
        removedScenarioInfo.removedScenarioId = scenarioId;
        removedScenarioInfo.updatedIndicatorPanels =
                indicatorPanelService.removeEntityIdsFromIndicatorPanels(projectId,
                        Arrays.asList(scenarioId), EntityType.SCENARIO);
        return removedScenarioInfo;
    }

    private boolean checkScenarioInvocationsAbsent(String projectId, String scenarioId, StringBuilder errorMessage) {
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        Map<String, ScenarioActionType> actionTypes = new HashMap<>();
        for (Scenario scenario: scenarios) {
            for (ScenarioTimeLineBlock timeLineBlock: scenario.getTimeLineBlocks()) {
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE) {
                    for (Action action: timeLineBlock.getActions()) {
                        ScenarioActionType actionType = actionTypes.get(action.getActionTypeId());
                        if (actionType == null) {
                            actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
                            actionTypes.put(actionType.getId(), actionType);
                        }
                        if (actionType.getEntityType() == ActionEntityType.SCENARIO
                                && action.getEntityId().equals(scenarioId)) {
                            errorMessage.append("Сценарий используется в сценарии \"").append(scenario.getName())
                                    .append("\"");
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public AggregatedScenarioView createByBasicParams(String projectId, ScenarioBasicParams scenarioBasicParams,
            StringBuilder errorMessage) {
        AggregatedScenarioView result = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage) &&
                validateScenarioBasicParams(null, scenarioBasicParams, errorMessage)) {
            Scenario scenario = new Scenario();
            scenario.setProjectId(projectId);
            updateScenarioByScenarioBasicParams(scenario, scenarioBasicParams);
            scenario.setGlobalNo(getNewGlobalNo(projectId));
            scenario = scenarioRepository.insert(scenario);
            result = getAggregatedScenarioView(scenario);
        }
        return result;
    }

    @Override
    public AggregatedScenarioView updateBasicParams(String projectId, String scenarioId,
            ScenarioBasicParams scenarioBasicParams,
            StringBuilder errorMessage) {
        AggregatedScenarioView result = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
            if (scenario == null) {
                errorMessage.append("Сценарий не найден");
            } else if (validateScenarioBasicParams(scenario, scenarioBasicParams, errorMessage)) {
                updateScenarioByScenarioBasicParams(scenario, scenarioBasicParams);
                scenario = scenarioRepository.save(scenario);
                result = getAggregatedScenarioView(scenario);
            }
        }
        return result;
    }

    @Override
    public AggregatedScenarioView updateAdvancedParams(String projectId, String scenarioId,
            ScenarioAdvancedParams scenarioAdvancedParams,
            StringBuilder errorMessage) {
        AggregatedScenarioView result = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
            if (scenario == null) {
                errorMessage.append("Сценарий не найден");
            } else {
                scenario.setManualStartStopAllowed(scenarioAdvancedParams.isManualStartStopAllowed());
                scenario.setStopOnGlobalReset(scenarioAdvancedParams.isStopOnGlobalReset());
                scenario.setStopType(scenarioAdvancedParams.getStopType());
                scenario = scenarioRepository.save(scenario);
                result = getAggregatedScenarioView(scenario);
            }
        }
        return result;
    }

    private AggregatedScenarioView updateStartStopLogic(String projectId, String scenarioId,
            ScenarioLogicBlock scenarioLogicBlock, boolean isStopLogic, StringBuilder errorMessage) {
        AggregatedScenarioView result = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
            if (scenario == null) {
                errorMessage.append("Сценарий не найден");
            } else if (validateScenarioLogicBlock(projectId, scenarioLogicBlock,
                    scenario.getScenarioPurpose(), errorMessage)) {
                boolean needsExecutiveBlock = false;
                ScenarioTriggerType scenarioTriggerType = scenarioTriggerTypeRepository.findOne(scenarioLogicBlock.getTriggerTypeId());
                /* Если создается блок логики тактического сценария, то исполнительный блок должен создаваться автоматически */
                if (scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS){
                    if ((scenario.getTimeLineBlocks().isEmpty()
                                || !scenario.getStartLogic().getTriggerTypeId().equals(scenarioLogicBlock.getTriggerTypeId()))
                            && scenarioTriggerType.getForcedScenarioActionTypeId() != null) {
                        needsExecutiveBlock = true;
                    }
                }
                if (scenarioLogicBlock.getLogicType() != null && scenarioTriggerType != null &&
                        scenarioTriggerType.getForcedScenarioLogicType() != null) {
                    scenarioLogicBlock.setLogicType(scenarioTriggerType.getForcedScenarioLogicType());
                }
                if (isStopLogic) {
                    scenario.setStopLogic(scenarioLogicBlock);
                } else {
                    scenario.setStartLogic(scenarioLogicBlock);
                }
                if (needsExecutiveBlock){
                    scenario.getTimeLineBlocks().clear();
                    ScenarioTimeLineBlock tlBlock = new ScenarioTimeLineBlock();
                    tlBlock.setBlockType(BlockType.EXECUTIVE);
                    tlBlock.setTimeDelaySec(0);
                    processTimeLineBlock(scenario, tlBlock, errorMessage);
                }
                scenario = scenarioRepository.save(scenario);
                result = getAggregatedScenarioView(scenario);
            }
        }
        return result;
    }

    private boolean processTimeLineBlock(Scenario scenario, ScenarioTimeLineBlock newBlock, StringBuilder errorMessage){
        if (scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && !validateTacticsScenarioActions(scenario, newBlock, errorMessage)) {
            return false;
        }
        if (validateScenarioTimeLineBlock(newBlock, errorMessage, scenario.getScenarioPurpose(), false)) {

            scenario.getTimeLineBlocks().add(newBlock);
            return true;
        }
        return false;
    }

    @Override
    public AggregatedScenarioView updateStartLogic(String projectId, String scenarioId,
            ScenarioLogicBlock scenarioLogicBlock, StringBuilder errorMessage) {
        return updateStartStopLogic(projectId, scenarioId, scenarioLogicBlock, false, errorMessage);
    }

    @Override
    public AggregatedScenarioView updateStopLogic(String projectId, String scenarioId,
            ScenarioLogicBlock scenarioLogicBlock, StringBuilder errorMessage) {
        return updateStartStopLogic(projectId, scenarioId, scenarioLogicBlock, true, errorMessage);
    }

    @Override
    public AggregatedScenarioView createTimeLineBlock(String projectId, String scenarioId,
            ScenarioTimeLineBlock newBlock, StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
            if (scenario == null) {
                errorMessage.append("Сценарий не найден");
            } else if (processTimeLineBlock(scenario, newBlock, errorMessage)){
                scenarioRepository.save(scenario);
                return getAggregatedScenarioView(scenario);
            }
        }
        return null;
    }

    @Override
    public AggregatedScenarioView updateBlock(String projectId, String scenarioId, int blockNo,
            ScenarioTimeLineBlock newBlock,
            StringBuilder errorMessage) {
        AggregatedScenarioView updatedScenarioView;
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }
        Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
        if (scenario == null) {
            errorMessage.append("Сценарий не найден");
            return null;
        }
        if (scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS
                && !validateTacticsScenarioActions(scenario, newBlock, errorMessage)){
                return null;
        }
        if (!validateScenarioTimeLineBlock(newBlock, errorMessage, scenario.getScenarioPurpose(), false)) {
            return null;
        }
        ScenarioTimeLineBlock oldBlock = scenario.getTimeLineBlocks().get(blockNo);
        if (oldBlock == null) {
            errorMessage.append("Блок сценария не найден");
            return null;
        }
        if (oldBlock.getBlockType() != newBlock.getBlockType()) {
            errorMessage.append("Нельзя изменить тип блока сценария");
            return null;
        }

        scenario.getTimeLineBlocks().set(blockNo, newBlock);

        scenario = scenarioRepository.save(scenario);
        updatedScenarioView = getAggregatedScenarioView(scenario);
        return updatedScenarioView;
    }

    @Override
    public AggregatedScenarioView removeBlock(String projectId, String scenarioId, int blockNo,
            StringBuilder errorMessage) {
        AggregatedScenarioView result = null;
        if (!projectService.checkProjectIsEditable(projectId, errorMessage)) {
            return null;
        }
        Scenario scenario = scenarioRepository.findByProjectIdAndId(projectId, scenarioId);
        if (scenario == null) {
            errorMessage.append("Сценарий не найден");
            return null;
        }
        ScenarioTimeLineBlock blockToRemove = scenario.getTimeLineBlocks().get(blockNo);
        if (blockToRemove == null) {
            errorMessage.append("Блок сценария не найден");
            return null;
        }
        if (blockNo == 0 && scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_TACTICS){
            errorMessage.append("Первый блок тактического сценария является обязательным");
            return null;
        }

        scenario.getTimeLineBlocks().remove(blockNo);

        scenario = scenarioRepository.save(scenario);
        result = getAggregatedScenarioView(scenario);
        return result;
    }
    
    private boolean changeScenarioLogicBlockIds(ScenarioLogicBlock block,
            Map<String, String> oldAndNewDevicesId, Map<String, String> oldAndNewRegionsId,
            Map<String, String> oldAndNewVirtualStatesId, String scenarioName, StringBuilder errorMessage) {
        if (block != null) {
            if (block.getTriggerTypeId() != null && !block.getTriggerTypeId().isEmpty()) {
                ScenarioTriggerType trigger = scenarioTriggerTypeRepository.findOne(block.getTriggerTypeId());
                if (trigger == null){
                    errorMessage.append("Для сценария ").append(scenarioName).append(" не найден указанный триггер");
                    return false;
                }
                if (block.getEntityIds() != null && !block.getEntityIds().isEmpty()) {
                    switch (trigger.getEntityType()) {
                    case DEVICE:
                    case CONTROL_DEVICE:
                        List<String> newDevicesIdInLogicBlock = new ArrayList<>();
                        for (String blockEntityId : block.getEntityIds()) {
                            if (oldAndNewDevicesId.containsKey(blockEntityId)) {
                                newDevicesIdInLogicBlock.add(oldAndNewDevicesId.get(blockEntityId));
                            } else {
                                errorMessage.append("В блоке сценария ").append(scenarioName)
                                        .append(" указана ссылка на несуществующее устройство");
                                return false;
                            }
                        }
                        if (!newDevicesIdInLogicBlock.isEmpty())
                            block.setEntityIds(newDevicesIdInLogicBlock);
                        break;
                    case REGION:
                        List<String> newRegionsIdIdInLogicBlock = new ArrayList<>();
                        for (String blockEntityId : block.getEntityIds()) {
                            if (oldAndNewRegionsId.containsKey(blockEntityId)) {
                                newRegionsIdIdInLogicBlock.add(oldAndNewRegionsId.get(blockEntityId));
                            } else {
                                errorMessage.append("В блоке сценария ").append(scenarioName)
                                        .append(" указана ссылка на несуществующую зону");
                                return false;
                            }
                        }
                        if (!newRegionsIdIdInLogicBlock.isEmpty()) {
                            block.setEntityIds(newRegionsIdIdInLogicBlock);
                        }
                        break;
                    case VIRTUAL_STATE:
                        List<String> newVirtualStatesIdInLogicBlock = new ArrayList<>();
                        for (String blockEntityId : block.getEntityIds()) {
                            if (oldAndNewVirtualStatesId.containsKey(blockEntityId)) {
                                newVirtualStatesIdInLogicBlock.add(oldAndNewVirtualStatesId.get(blockEntityId));
                            } else {
                                errorMessage.append("В блоке сценария ").append(scenarioName)
                                        .append(" указана ссылка на несуществующее виртуальное состояние");
                                return false;
                            }
                        }
                        if (!newVirtualStatesIdInLogicBlock.isEmpty()) {
                            block.setEntityIds(newVirtualStatesIdInLogicBlock);
                        }
                        break;
                    default:
                    }
                }
                for (ScenarioLogicBlock subLogic : block.getSubLogics()) {
                    if (!changeScenarioLogicBlockIds(subLogic, oldAndNewDevicesId, oldAndNewRegionsId,
                            oldAndNewVirtualStatesId, scenarioName, errorMessage)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean checkAndAdditionScenarioViewToImportProject(String projectId, List<Scenario> scenarios,
            Map<String, String> oldAndNewScenariosId, Map<String, String> oldAndNewDevicesId, Map<String, String> oldAndNewRegionsId,
            Map<String, String> oldAndNewVirtualStatesId, StringBuilder errorMessage) {
        List<Scenario> scenariosWithNewId = new ArrayList<>();
        List<String> oldScenarioIds = new ArrayList<>();
        Set<String> actionTypeIdsOnScenarioAction = new HashSet<>();
        for (Scenario scenario : scenarios) {
            if (!checkAndSetScenarioReferenceIds(projectId, scenario, oldAndNewDevicesId, oldAndNewRegionsId,
                     oldAndNewVirtualStatesId, actionTypeIdsOnScenarioAction, errorMessage)){
                return false;
            }

            if (scenario.getStartLogic() != null
                    && !changeScenarioLogicBlockIds(scenario.getStartLogic(), oldAndNewDevicesId,
                    oldAndNewRegionsId, oldAndNewVirtualStatesId, scenario.getName(), errorMessage)) {
                return false;
            }
            if (scenario.getStopLogic() != null
                    && !changeScenarioLogicBlockIds(scenario.getStopLogic(), oldAndNewDevicesId,
                    oldAndNewRegionsId, oldAndNewVirtualStatesId, scenario.getName(), errorMessage)) {
                return false;
            }

            if (!validateImportedScenario(projectId, scenario, errorMessage)){
                return false;
            }

            oldScenarioIds.add(scenario.getId());
            scenario.clearId();
            scenariosWithNewId.add(scenario);
        }

        scenariosWithNewId = scenarioRepository.insert(scenariosWithNewId);
        Iterator<String> itOnOld = oldScenarioIds.iterator();
        Iterator<String> itOnNew = scenariosWithNewId.stream().map(Scenario::getId).iterator();
        while (itOnOld.hasNext() && itOnNew.hasNext()){
            oldAndNewScenariosId.put(itOnOld.next(), itOnNew.next());
        }

        // Обновление идентификаторов сценариев в действиях сценариев
        if (!actionTypeIdsOnScenarioAction.isEmpty()){
            List<Scenario> scenariosToUpdate = new ArrayList<>();
            for (Scenario scenarioWithNewId: scenariosWithNewId){
                boolean shouldBeUpdated = false;
                for (ScenarioTimeLineBlock scenarioTimeLineBlock: scenarioWithNewId.getTimeLineBlocks()){
                    for (Action action: scenarioTimeLineBlock.getActions()){
                        if (actionTypeIdsOnScenarioAction.contains(action.getActionTypeId())){
                            String newScenarioId = oldAndNewScenariosId.get(action.getEntityId());
                            if (newScenarioId == null){
                                errorMessage.append("В действиях сценария ")
                                        .append(scenarioWithNewId.getName()).append(" указан несуществующий сценарий ").append(action.getEntityId());
                                return false;
                            }
                            action.setEntityId(newScenarioId);
                            shouldBeUpdated = true;
                        }
                    }
                }
                if (shouldBeUpdated){
                    scenariosToUpdate.add(scenarioWithNewId);
                }
            }
            scenarioRepository.save(scenariosToUpdate);
        }


        return true;
    }

    private boolean checkAndSetScenarioReferenceIds(String projectId,
                                                    Scenario scenario,
                                                    Map<String, String> oldAndNewDevicesId,
                                                    Map<String, String> oldAndNewRegionsId,
                                                    Map<String, String> oldAndNewVirtualStatesId,
                                                    Set<String> actionTypeIdsOnScenarioAction,
                                                    StringBuilder errorMessage) {
        scenario.setProjectId(projectId);
        if(scenario.getControlDeviceId() != null) {
            if(!oldAndNewDevicesId.containsKey(scenario.getControlDeviceId())) {
                errorMessage.append("В сценарии ").append(scenario.getName()).append(" не найден указанный прибор");
                return false;
            }
            scenario.setControlDeviceId(oldAndNewDevicesId.get(scenario.getControlDeviceId()));
        }
        for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks()) {
            if (timeLineBlock.getTimeDelaySec() >= 0) {
                switch (timeLineBlock.getBlockType()) {
                    case TRACING:
                        if (timeLineBlock.getTracingEntityType() == TracingEntityType.SENSOR_DEVICE) {
                            if (!oldAndNewDevicesId.containsKey(timeLineBlock.getTracingEntityId())) {
                                errorMessage.append("В блоке слежения сценария ").append(scenario.getName())
                                        .append(" не найдено указанное устройство");
                                return false;
                            }
                            timeLineBlock
                                    .setTracingEntityId(oldAndNewDevicesId.get(timeLineBlock.getTracingEntityId()));
                        } else {
                            if (!oldAndNewVirtualStatesId.containsKey(timeLineBlock.getTracingEntityId())) {
                                errorMessage.append("В блоке слежения сценария ").append(scenario.getName())
                                        .append(" не найдено указанное виртуальное состояние");
                                return false;
                            }
                            timeLineBlock.setTracingEntityId(
                                    oldAndNewVirtualStatesId.get(timeLineBlock.getTracingEntityId()));
                        }
                        break;
                    case EXECUTIVE:
                        switch (timeLineBlock.getConditionType()) {
                            case DEVICE_STATE:
                                if (oldAndNewDevicesId.containsKey(timeLineBlock.getConditionEntityId())) {
                                    timeLineBlock.setConditionEntityId(
                                            oldAndNewDevicesId.get(timeLineBlock.getConditionEntityId()));
                                } else {
                                    errorMessage.append("В исполнительном блоке сценария ")
                                            .append(scenario.getName())
                                            .append(" не найдено устройство для условия");
                                    return false;
                                }
                                break;
                            case VIRTUAL_STATE:
                                if (oldAndNewVirtualStatesId.containsKey(timeLineBlock.getConditionEntityId())) {
                                    timeLineBlock.setConditionEntityId(
                                            oldAndNewVirtualStatesId.get(timeLineBlock.getConditionEntityId()));
                                } else {
                                    errorMessage.append("В исполнительном блоке сценария ")
                                            .append(scenario.getName())
                                            .append(" не найдено виртуальное состояние для условия");
                                    return false;
                                }
                                break;
                            case NONE:
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            for (ScenarioTimeLineBlock.Action action : timeLineBlock.getActions()) {
                ScenarioActionType actionType = scenarioActionTypeRepository.findOne(action.getActionTypeId());
                if (actionType != null) {
                    switch (actionType.getEntityType()) {
                        case DEVICE:
                            if (!oldAndNewDevicesId.containsKey(action.getEntityId())) {
                                errorMessage.append("В действиях сценария ").append(scenario.getName())
                                        .append(" указано несуществующее устройство");
                                return false;
                            }
                            action.setEntityId((oldAndNewDevicesId.get(action.getEntityId())));
                            break;
                        case REGION:
                            if (!oldAndNewRegionsId.containsKey(action.getEntityId())) {
                                errorMessage.append("В действиях сценария ").append(scenario.getName())
                                        .append(" указано несуществующее устройство");
                                return false;
                            }
                            action.setEntityId((oldAndNewRegionsId.get(action.getEntityId())));
                            break;
                        case SCENARIO:
                            /* Обновлять идентификаторы действия в этом случае нужно после вставки всех сценариев в базу.
                             * Для этого нужно запомнить id действий.
                             */
                            actionTypeIdsOnScenarioAction.add(action.getActionTypeId());
                            break;
                        case VIRTUAL_STATE:
                            if (!oldAndNewVirtualStatesId.containsKey(action.getEntityId())) {
                                errorMessage.append("В действиях сценария ").append(scenario.getName())
                                        .append(" указано несуществующее виртуальное состояние");
                                return false;
                            }
                            action.setEntityId((oldAndNewVirtualStatesId.get(action.getEntityId())));
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return true;
    }

    private boolean validateImportedScenario(String projectId, Scenario scenario,
                                             StringBuilder errorMessage) {
        if (scenario.getScenarioPurpose() == ScenarioPurpose.EXEC_BY_INVOKE
                && (scenario.getControlDeviceId() == null ||
                !deviceRepository.existsByIdAndDisabled(scenario.getControlDeviceId(), false))) {
            errorMessage.append("В сценарии ").append(scenario.getName()).append(" указан несуществующий прибор");
            return false;
        }
        for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks()) {
            if (!validateScenarioTimeLineBlock(timeLineBlock, errorMessage, scenario.getScenarioPurpose(), true)) {
                return false;
            }
        }
        if (scenario.getScenarioPurpose() != ScenarioPurpose.EXEC_BY_INVOKE) {
            if (scenario.getStartLogic() != null && !scenario.getStartLogic().isEmpty()
                    && !validateScenarioLogicBlock(projectId, scenario.getStartLogic(),
                    scenario.getScenarioPurpose(), errorMessage)) {
                return false;

            }
            if (scenario.getStopLogic() != null && scenario.getStopType() == StopType.BY_LOGIC
                    && !scenario.getStopLogic().isEmpty()
                    && !validateScenarioLogicBlock(projectId, scenario.getStartLogic(),
                    scenario.getScenarioPurpose(), errorMessage)) {
                return false;
            }
        }
        return true;
    }

    public List<AggregatedScenarioView> generateAggregatedScenarioViews(List<Scenario> scenarios,
            List<TreeItemDeviceView> devices, List<Region> regions, List<VirtualState> virtualStates) {
        List<AggregatedScenarioView> scenarioViews = new ArrayList<>();

        // поиск идентификаторов используемых справочных сущностей
        ScenarioReferenceEntityIds refEntityIds = new ScenarioReferenceEntityIds();
        for (Scenario scenario : scenarios) {
            getTriggerTypeIds(scenario.getStartLogic(), refEntityIds.triggerTypes);
            getTriggerTypeIds(scenario.getStopLogic(), refEntityIds.triggerTypes);
            getActionTypeIds(scenario.getTimeLineBlocks(), refEntityIds.actionTypes);
            getStateIds(scenario.getTimeLineBlocks(), refEntityIds.states);
        }

        // поиск в базе необходимых справочных сущностей
        ScenarioReferenceEntitiesById refEntitiesById = new ScenarioReferenceEntitiesById(refEntityIds);
        if (!refEntitiesById.containsAll(refEntityIds)) {
            refEntityIds.removeAll(refEntitiesById);
            logger.error("Не найдена справочная сущность сценария: {}", refEntityIds);
            return null;
        }

        // поиск идентификаторов используемых сущностей
        ScenarioEntityIds entityIds = new ScenarioEntityIds();
        for (Scenario scenario: scenarios) {
            getScenarioEntityIds(scenario, refEntitiesById, entityIds);
        }

        // поиск в коллекциях сущностей для построения отображений
        ScenarioEntitiesById entitiesById = new ScenarioEntitiesById(scenarios, devices, regions, virtualStates);
        if (!entitiesById.containsAll(entityIds)) {
            entityIds.removeAll(entitiesById);
            logger.error("В коллекциях не найдена сущность сценария: {}", entityIds);
            return null;
        }

        // построение отображений сценариев
        for (Scenario scenario: scenarios) {
            scenarioViews.add(generateAggregatedScenarioView(scenario, entitiesById, refEntitiesById));
        }

        return scenarioViews;
    }

    @Override
    public List<AggregatedScenarioView> generateAggregatedScenarioViews(String projectId, List<Scenario> scenarios) {
        List<AggregatedScenarioView> scenarioViews = new ArrayList<>();

        // поиск идентификаторов используемых справочных сущностей
        ScenarioReferenceEntityIds refEntityIds = new ScenarioReferenceEntityIds();
        for (Scenario scenario : scenarios) {
            getTriggerTypeIds(scenario.getStartLogic(), refEntityIds.triggerTypes);
            getTriggerTypeIds(scenario.getStopLogic(), refEntityIds.triggerTypes);
            getActionTypeIds(scenario.getTimeLineBlocks(), refEntityIds.actionTypes);
            getStateIds(scenario.getTimeLineBlocks(), refEntityIds.states);
        }

        // поиск в базе необходимых справочных сущностей
        ScenarioReferenceEntitiesById refEntitiesById = new ScenarioReferenceEntitiesById(refEntityIds);
        if (!refEntitiesById.containsAll(refEntityIds)) {
            refEntityIds.removeAll(refEntitiesById);
            logger.error("Не найдена справочная сущность сценария: {}", refEntityIds);
            return null;
        }

        // поиск идентификаторов используемых сущностей
        ScenarioEntityIds entityIds = new ScenarioEntityIds();
        for (Scenario scenario: scenarios) {
            getScenarioEntityIds(scenario, refEntitiesById, entityIds);
        }

        // поиск в базе данных сущностей для построения отображений
        ScenarioEntitiesById entitiesById = new ScenarioEntitiesById(projectId, entityIds);
        if (!entitiesById.containsAll(entityIds)) {
            entityIds.removeAll(entitiesById);
            logger.error("В базе не найдена сущность сценария: {}", entityIds);
            return null;
        }

        // построение отображений сценариев
        for (Scenario scenario: scenarios) {
            scenarioViews.add(generateAggregatedScenarioView(scenario, entitiesById, refEntitiesById));
        }

        return scenarioViews;
    }

    private AggregatedScenarioView generateAggregatedScenarioView(Scenario scenario,
            ScenarioEntitiesById entitiesById,
            ScenarioReferenceEntitiesById refEntitiesById) {
        AggregatedScenarioView scenarioView = new AggregatedScenarioView(scenario);
        return generateAggregatedScenarioView(scenarioView, scenario, entitiesById, refEntitiesById);
    }

    private AggregatedActiveScenarioView generateAggregatedActiveScenarioView(Scenario scenario,
                                                                              ScenarioEntitiesById entitiesById,
                                                                              ScenarioReferenceEntitiesById refEntitiesById) {
        AggregatedScenarioView scenarioView = new AggregatedScenarioView(scenario);
        AggregatedActiveScenarioView activeScenarioView = new AggregatedActiveScenarioView(scenarioView);
        return (AggregatedActiveScenarioView)generateAggregatedScenarioView(activeScenarioView, scenario, entitiesById,
                refEntitiesById);
    }

    private AggregatedScenarioView generateAggregatedScenarioView(AggregatedScenarioView scenarioView,
                                                                  Scenario scenario,
                                                                  ScenarioEntitiesById entitiesById,
                                                                  ScenarioReferenceEntitiesById refEntitiesById){
        scenarioDictionariesViewService.fillScenarioColors(scenario, scenarioView);
        scenarioView.setDeviceIds(new ArrayList<>(getDeviceIds(scenario)));

        ScenarioBasicParams scenarioBasicParams = new ScenarioBasicParams(scenario);
        scenarioView.setBasicParams(scenarioBasicParams);

        ScenarioAdvancedParams scenarioAdvancedParams = new ScenarioAdvancedParams(scenario);
        if (scenario.getStartLogic() != null) {
            StringBuilder sentence = new StringBuilder();
            generateLogicSentence(scenario.getStartLogic(), entitiesById, refEntitiesById, 0, sentence);
            scenarioAdvancedParams.setStartLogicSentence(sentence.toString());
        }
        if (scenario.getStopLogic() != null) {
            StringBuilder sentence = new StringBuilder();
            generateLogicSentence(scenario.getStopLogic(), entitiesById, refEntitiesById, 0, sentence);
            scenarioAdvancedParams.setStopLogicSentence(sentence.toString());
        }
        scenarioView.setAdvancedParams(scenarioAdvancedParams);

        scenarioView.setTimeLineBlocksTree(generateTimeLineBlocksTree(scenario, entitiesById, refEntitiesById));

        return scenarioView;
    }

    private List<TreeItemScenarioTimeLineBlockView> generateTimeLineBlocksTree(Scenario scenario,
            ScenarioEntitiesById entitiesById, ScenarioReferenceEntitiesById refEntitiesById) {
        List<TreeItemScenarioTimeLineBlockView> tree = new ArrayList<>();

        /* добавим корневой элемент - вход в сценарий */
        TreeItemScenarioTimeLineBlockView headItem = new TreeItemScenarioTimeLineBlockView();
        headItem.setDescription(scenario.getName());
        headItem.setTimeDelaySec(-1);
        headItem.setItemType(TreeItemType.ROOT);
        setTLBlockMediaByTreeItemType(headItem, refEntitiesById.treeItemMedias);
        tree.add(headItem);

        for (int index = 0; index < scenario.getTimeLineBlocks().size(); index++) {
            ScenarioTimeLineBlock block = scenario.getTimeLineBlocks().get(index);
            switch (block.getBlockType()) {
            case TRACING:
                /* добавим все начала и окончания слежений */
                generateTracingAndNoTracingTreeItems(block, index, tree, entitiesById, refEntitiesById);
                break;
            case EXECUTIVE:
                /* добавим все исполнительные блоки */
                generateExecutiveTreeItems(block, index, tree, entitiesById, refEntitiesById);
                break;
            case COMPUTER_ACTION:
                /* добавим все блоки "действие на компьютере" */
                gatherComputerActionTreeItems(block, index, tree, refEntitiesById);
                break;
            default:
                break;
            }
        }

        /* упорядочим дерево */
        arrangeTree(tree);

        /* добавляем элемент выхода из сценария */
        TreeItemScenarioTimeLineBlockView exitItem = new TreeItemScenarioTimeLineBlockView();
        exitItem.setItemType(TreeItemType.OUT);
        setTLBlockMediaByTreeItemType(exitItem, refEntitiesById.treeItemMedias);
        tree.add(exitItem);

        return tree;
    }

    private void gatherComputerActionTreeItems(ScenarioTimeLineBlock block, int index,
            List<TreeItemScenarioTimeLineBlockView> tree, ScenarioReferenceEntitiesById refEntitiesById) {
        // найдём корневой элемент
        TreeItemScenarioTimeLineBlockView rootItem = getOrCreateRootTimeItem(tree, block.getTimeDelaySec(),
                refEntitiesById.treeItemMedias);
        // создадим элемент для самого блока
        TreeItemScenarioTimeLineBlockView blockItem = new TreeItemScenarioTimeLineBlockView();
        blockItem.setNumber(index);
        fillComputerActionBlockTreeItem(index, block, refEntitiesById, blockItem);
        // добавим блок к родителю
        rootItem.getChildren().add(blockItem);
        /* добавляем элемент выхода из блока */
        TreeItemScenarioTimeLineBlockView endItem = new TreeItemScenarioTimeLineBlockView();
        endItem.setNumber(index);
        endItem.setItemType(TreeItemType.OUT);
        endItem.setTimeDelaySec(block.getTimeDelaySec());
        setTLBlockMediaByTreeItemType(endItem, refEntitiesById.treeItemMedias);
        rootItem.getChildren().add(endItem);
    }

    private void generateExecutiveTreeItems(ScenarioTimeLineBlock block, int index,
                                            List<TreeItemScenarioTimeLineBlockView> tree,
                                            ScenarioEntitiesById entitiesById,
                                            ScenarioReferenceEntitiesById refEntitiesById) {
        // найдём корневой элемент
        TreeItemScenarioTimeLineBlockView rootItem = getOrCreateRootTimeItem(tree, block.getTimeDelaySec(),
                refEntitiesById.treeItemMedias);
        // создадим элемент для самого блока
        TreeItemScenarioTimeLineBlockView blockItem = new TreeItemScenarioTimeLineBlockView();
        blockItem.setNumber(index);
        fillExecutiveBlockTreeItem(entitiesById, refEntitiesById, index, block, blockItem);
        // добавим блок к родителю
        rootItem.getChildren().add(blockItem);
        /* добавляем элемент выхода из блока */
        TreeItemScenarioTimeLineBlockView endItem = new TreeItemScenarioTimeLineBlockView();
        endItem.setNumber(index);
        endItem.setItemType(TreeItemType.OUT);
        endItem.setTimeDelaySec(block.getTimeDelaySec());
        setTLBlockMediaByTreeItemType(endItem, refEntitiesById.treeItemMedias);
        rootItem.getChildren().add(endItem);
    }

    private void generateTracingAndNoTracingTreeItems(ScenarioTimeLineBlock block, int index,
            List<TreeItemScenarioTimeLineBlockView> tree, ScenarioEntitiesById entitiesById,
            ScenarioReferenceEntitiesById refEntitiesById) {
        /* добавляем элемент начала слежения */
        TreeItemScenarioTimeLineBlockView startItem = new TreeItemScenarioTimeLineBlockView();
        startItem.setNumber(index);
        fillTracingBlockTreeItem(entitiesById, refEntitiesById, block, startItem);
        TreeItemScenarioTimeLineBlockView rootItem = getOrCreateRootTimeItem(tree, block.getTracingStartSec(),
                refEntitiesById.treeItemMedias);
        rootItem.getChildren().add(startItem);
        /* добавляем элемент окончания слежения */
        TreeItemScenarioTimeLineBlockView stopItem = new TreeItemScenarioTimeLineBlockView();
        stopItem.setNumber(index);
        fillNoTracingBlockTreeItem(entitiesById, refEntitiesById, block, stopItem);
        rootItem = getOrCreateRootTimeItem(tree, block.getTracingStartSec() + block.getTracingPeriodSec(),
                refEntitiesById.treeItemMedias);
        rootItem.getChildren().add(stopItem);
    }

    private void fillComputerActionBlockTreeItem(int index, ScenarioTimeLineBlock block,
            ScenarioReferenceEntitiesById refEntitiesById, TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setItemType(TreeItemType.COMPUTER_ACTION);
        blockItem.setTimeDelaySec(block.getTimeDelaySec());
        setTLBlockMediaByTreeItemType(blockItem, refEntitiesById.treeItemMedias);
        blockItem.setActionName(block.getName());
        fillComputerActionTreeItems(index, block, refEntitiesById, blockItem);
    }

    private void fillComputerActionTreeItems(int index, ScenarioTimeLineBlock block,
            ScenarioReferenceEntitiesById refEntitiesById, TreeItemScenarioTimeLineBlockView blockItem){
        for (ComputerAction computerAction: block.getComputerActions()){
            TreeItemScenarioTimeLineBlockView computerActionItem = new TreeItemScenarioTimeLineBlockView();
            computerActionItem.setNumber(index);
            StringBuilder computerActionDescription = new StringBuilder(computerAction.getMessage());
            if (computerAction.getCommand() == Command.COUNTDOWN){
                computerActionItem.setItemType(TreeItemType.COUNTDOWN);
                computerActionDescription.append(": ").append(computerAction.getCountdownTimeSec()).append(SECONDS_POSTFIX);
            } else {
                computerActionItem.setItemType(TreeItemType.SHOW_MESSAGE);
            }

            computerActionItem.setActionName(computerAction.getCommand().getLabel());
            setTLBlockMediaByTreeItemType(computerActionItem, refEntitiesById.treeItemMedias);

            computerActionItem.setDescription(computerActionDescription.toString());
            blockItem.getChildren().add(computerActionItem);
        }
    }

    private void fillTracingBlockTreeItem(ScenarioEntitiesById entitiesById,
            ScenarioReferenceEntitiesById refEntitiesById,
            ScenarioTimeLineBlock block, TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTracingStartSec());
        blockItem.setItemType(TreeItemType.TRACE);
        blockItem.setActionName(block.getName() + " (вкл.)");
        fillGeneralTracingBlockTreeItemInfo(entitiesById, refEntitiesById, block, blockItem);
    }

    private void fillNoTracingBlockTreeItem(ScenarioEntitiesById entitiesById,
            ScenarioReferenceEntitiesById refEntitiesById,
            ScenarioTimeLineBlock block, TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTracingStartSec() + block.getTracingPeriodSec());
        blockItem.setItemType(TreeItemType.NO_TRACE);
        blockItem.setActionName(block.getName() + " (выкл.)");
        fillGeneralTracingBlockTreeItemInfo(entitiesById, refEntitiesById, block, blockItem);
    }

    private void fillGeneralTracingBlockTreeItemInfo(ScenarioEntitiesById entitiesById,
                                                     ScenarioReferenceEntitiesById refEntitiesById,
                                                     ScenarioTimeLineBlock block,
                                                     TreeItemScenarioTimeLineBlockView blockItem){
        setTLBlockMediaByTreeItemType(blockItem, refEntitiesById.treeItemMedias);
        blockItem.setEntityId(block.getTracingEntityId());
        switch (block.getTracingEntityType()) {
            case SENSOR_DEVICE: {
                ScenarioDeviceView deviceView = entitiesById.devices.get(block.getTracingEntityId());
                blockItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
                blockItem.setDescription(" " + deviceView.getName()
                        + " (" + deviceView.getShortAddressPath() + ")");
                blockItem.setEntityType(EntityType.DEVICE);
                break;
            }
            case VIRTUAL_STATE: {
                VirtualState virtualState = entitiesById.virtualStates.get(block.getTracingEntityId());
                blockItem.setDescription("Вирт. состояние: " + virtualState.getName());
                blockItem.setEntityType(EntityType.VIRTUAL_STATE);
                break;
            }
            default:
                break;
        }
    }

    private void fillExecutiveBlockTreeItem(ScenarioEntitiesById entitiesById, ScenarioReferenceEntitiesById refEntitiesById,
            int index, ScenarioTimeLineBlock block, TreeItemScenarioTimeLineBlockView blockItem) {
        blockItem.setTimeDelaySec(block.getTimeDelaySec());
        blockItem.setItemType(block.getConditionType() == ConditionType.NONE
                ? TreeItemType.IN : TreeItemType.CONDITION);
        setTLBlockMediaByTreeItemType(blockItem, refEntitiesById.treeItemMedias);
        blockItem.setActionName(block.getName());
        blockItem.setEntityId(block.getConditionEntityId());
        switch (block.getConditionType()) {
            case DEVICE_STATE: {
                ScenarioDeviceView deviceView = entitiesById.devices.get(block.getConditionEntityId());
                StateView stateView = refEntitiesById.stateViews.get(block.getConditionDeviceStateId());
                blockItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
                blockItem.setDescription(deviceView.getName() + " (" + deviceView.getShortAddressPath() + ")" 
                        + (block.getConditionCheckType() == ConditionCheckType.IS_ACTIVE ? " активно " : " не активно ")
                        + "'" + stateView.getName() + "'");
                blockItem.setEntityType(EntityType.DEVICE);
                break;
            }
            case VIRTUAL_STATE: {
                VirtualState virtualState = entitiesById.virtualStates.get(block.getConditionEntityId());
                blockItem.setDescription("Если" + (block.getConditionCheckType() == ConditionCheckType.IS_ACTIVE ? " активно " : " не активно ")
                        + "вирт. состояние '" + virtualState.getName() + "'");
                blockItem.setEntityType(EntityType.VIRTUAL_STATE);
                break;
            }
            default:
                break;
        }
        for (ScenarioTimeLineBlock.Action action: block.getActions())
            fillActionTreeItem(entitiesById, refEntitiesById, index, action, blockItem);
    }

    private void fillActionTreeItem(ScenarioEntitiesById entitiesById, ScenarioReferenceEntitiesById refEntitiesById,
            int index, ScenarioTimeLineBlock.Action action,
            TreeItemScenarioTimeLineBlockView blockItem) {
        // добавим элементы действий
        TreeItemScenarioTimeLineBlockView actionItem = new TreeItemScenarioTimeLineBlockView();
        actionItem.setNumber(index);
        actionItem.setItemType(TreeItemType.ACTION);
        actionItem.setEntityId(action.getEntityId());

        ScenarioActionType actionType = refEntitiesById.actionTypes.get(action.getActionTypeId());
        ScenarioActionTypeView actionTypeView = refEntitiesById.actionTypeViews.get(action.getActionTypeId());

        actionItem.setActionName(actionTypeView.getName());
        actionItem.setActionIconName(actionTypeView.getIconMedia().getPath());
        actionItem.setActionIconColor(actionTypeView.getIconMedia().getFillColor());
        actionItem.setEntityType(EntityType.valueOf(actionType.getEntityType().toString()));

        switch (actionType.getEntityType()) {
        case DEVICE:
            ScenarioDeviceView deviceView = entitiesById.devices.get(action.getEntityId());
            actionItem.setDescription(deviceView.getName() + " (" + deviceView.getShortAddressPath() + ")");
            actionItem.setDescriptionIconName(deviceView.getIconMedia().getPath());
            break;
        case REGION:
            Region region = entitiesById.regions.get(action.getEntityId());
            actionItem.setDescription(region.getName() + " (No: " + region.getIndex() + ")");
            break;
        case SCENARIO:
            Scenario scenario = entitiesById.scenarios.get(action.getEntityId());
            actionItem.setDescription(scenario.getName() + " (No: " + scenario.getGlobalNo() + ")");
            break;
        case VIRTUAL_STATE:
            VirtualState virtualState = entitiesById.virtualStates.get(action.getEntityId());
            actionItem.setDescription(virtualState.getName() + " (No: " + virtualState.getGlobalNo() + ")");
            break;
        default:
            break;
        }

        blockItem.getChildren().add(actionItem);
    }

    private void generateLogicSentence(ScenarioLogicBlock logicBlock, ScenarioEntitiesById entitiesById,
            ScenarioReferenceEntitiesById refEntitiesById, int level, StringBuilder sentence) {
        sentence.append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_BEGIN)
            .append(ScenarioDictionariesViewService.ScenarioLogicTypeView.
                typesByValue.get(logicBlock.getLogicType()).getSentence())
            .append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_EXPRESSION_MULTI_BEGIN);

        if (!logicBlock.getTriggerTypeId().isEmpty() && !logicBlock.getEntityIds().isEmpty()) {
            ScenarioTriggerType triggerType = refEntitiesById.triggerTypes.get(logicBlock.getTriggerTypeId());
            String triggerTypeName = refEntitiesById.triggerTypeViews.get(logicBlock.getTriggerTypeId()).getName();
            List<String> logicBlockEntityNames = getLogicBlockEntityNames(logicBlock, triggerType, entitiesById);
            for (String entityName: logicBlockEntityNames) {
                addNewLineAndTabs(sentence, level + 1);
                sentence.append(triggerTypeName).append(" ").append(entityName);
            }
        }
        if (!logicBlock.getSubLogics().isEmpty()) {
            for (ScenarioLogicBlock sublogicBlock: logicBlock.getSubLogics()) {
                addNewLineAndTabs(sentence, level + 1);
                generateLogicSentence(sublogicBlock, entitiesById, refEntitiesById, level + 1, sentence);
            }
        }
        addNewLineAndTabs(sentence, level);
        sentence.append(ScenarioDictionariesViewService.SCENARIO_SENTENCE_EXPRESSION_MULTI_END);
    }

    private List<String> getLogicBlockEntityNames(ScenarioLogicBlock logicBlock,
            ScenarioTriggerType triggerType, ScenarioEntitiesById entitiesById) {
        List<String> entityNames = new ArrayList<>();

        for (String entityId : logicBlock.getEntityIds()) {
            switch (triggerType.getEntityType()) {
            case REGION:
                Region region = entitiesById.regions.get(entityId);
                entityNames.add(ScenarioDictionariesViewService.SCENARIO_SENTENCE_ENTITY_NAME_PREPOSITION
                        + region.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_REGION
                        + " " + Integer.toString(region.getIndex()) + ")");
                break;
            case VIRTUAL_STATE:
                VirtualState virtualState = entitiesById.virtualStates.get(entityId);
                entityNames.add(virtualState.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_VRITUAL
                        + " " + Integer.toString(virtualState.getGlobalNo()) + ")");
                break;
            case CONTROL_DEVICE:
            case DEVICE:
                ScenarioDeviceView deviceView = entitiesById.devices.get(entityId);
                entityNames.add(ScenarioDictionariesViewService.SCENARIO_SENTENCE_ENTITY_NAME_PREPOSITION
                        + deviceView.getName()
                        + " (" + ScenarioDictionariesViewService.SCENARIO_SENTENCE_DEVICE
                        + " " + deviceView.getMiddleAddressPath() + ")");
                break;
            default:
                logger.warn("Не обрабатываемый тип сущности: {}", triggerType.getEntityType().toString());
                break;
            }
        }

        return entityNames;
    }

    private void setTLBlockMediaByTreeItemType(TreeItemScenarioTimeLineBlockView treeItem,
            Map<TreeItemType, Media> treeItemMedias) {
        Media media = treeItemMedias.get(treeItem.getItemType());
        if (media != null) {
            treeItem.setActionIconName(media.getPath());
            treeItem.setActionIconColor(media.getFillColor());
        } else if (treeItem.getItemType() != TreeItemType.OUT) {
            logger.warn("В базе отсутствует иконка элемента дерева сценария: {}", treeItem.getItemType());
        }
    }

    private TreeItemScenarioTimeLineBlockView getOrCreateRootTimeItem(List<TreeItemScenarioTimeLineBlockView> tree,
            int time, Map<TreeItemType, Media> treeItemMedias) {
        TreeItemScenarioTimeLineBlockView timeItem = null;
        for (TreeItemScenarioTimeLineBlockView item: tree) {
            if (item.getTimeDelaySec() == time) {
                timeItem = item;
                break;
            }
        }
        if (timeItem == null) {
            timeItem = new TreeItemScenarioTimeLineBlockView();
            timeItem.setTimeDelaySec(time);
            timeItem.setItemType(TreeItemType.TIME);
            setTLBlockMediaByTreeItemType(timeItem, treeItemMedias);
            tree.add(timeItem);
        }
        return timeItem;
    }

    /**
     * Идентификаторы справочных сущностей, используемых в сценарии
     */
    public class ScenarioReferenceEntityIds {
        /** Типы запуска */
        public Set<String> triggerTypes = new HashSet<>();
        /** Типы действий */
        public Set<String> actionTypes = new HashSet<>();
        /** Состояния */
        public Set<String> states = new HashSet<>();

        /** Удаление идентификаторов всех сущностей из представленного набора сущностей */
        public void removeAll(ScenarioReferenceEntitiesById refEntities) {
            triggerTypes.removeAll(refEntities.triggerTypeViews.keySet());
            actionTypes.removeAll(refEntities.actionTypeViews.keySet());
            states.removeAll(refEntities.stateViews.keySet());
        }

        @Override
        public String toString() {
            StringBuilder message = new StringBuilder();
            if (!triggerTypes.isEmpty()) {
                message.append(" Типы запуска: ").append(triggerTypes);
            }
            if (!actionTypes.isEmpty()) {
                message.append(" Типы действий: ").append(actionTypes);
            }
            if (!states.isEmpty()) {
                message.append(" Состояния: ").append(states);
            }
            return message.toString();
        }
    }

    /**
     * Справочные сущности, используемые в сценарии
     */
    public class ScenarioReferenceEntitiesById {
        /** Типы запуска */
        public Map<String, ScenarioTriggerType> triggerTypes;
        /** Типы действий */
        public Map<String, ScenarioActionType> actionTypes;
        /** Отображения типов запуска */
        public Map<String, ScenarioTriggerTypeView> triggerTypeViews;
        /** Отображения типов действий */
        public Map<String, ScenarioActionTypeView> actionTypeViews;
        /** Иконки элементов дерева сценариев */
        public Map<TreeItemType, Media> treeItemMedias;
        /** Отображения состояний */
        public Map<String, StateView> stateViews;

        public ScenarioReferenceEntitiesById(Map<String, ScenarioTriggerType> triggerTypes,
                                             Map<String, ScenarioActionType> actionTypes,
                                             Map<String, ScenarioTriggerTypeView> triggerTypeViews,
                                             Map<String, ScenarioActionTypeView> actionTypeViews,
                                             Map<TreeItemType, Media> treeItemMedias){
            this.triggerTypes = triggerTypes;
            this.actionTypes = actionTypes;
            this.triggerTypeViews = triggerTypeViews;
            this.actionTypeViews = actionTypeViews;
            this.treeItemMedias = treeItemMedias;
        }

        public ScenarioReferenceEntitiesById(ScenarioReferenceEntityIds refEntityIds) {
            triggerTypes = scenarioTriggerTypeRepository.findByIdIn(refEntityIds.triggerTypes)
                .stream().collect(Collectors.toMap(ScenarioTriggerType::getId, triggerType -> triggerType));
            actionTypes = scenarioActionTypeRepository.findByIdIn(refEntityIds.actionTypes)
                .stream().collect(Collectors.toMap(ScenarioActionType::getId, actionType -> actionType));
            triggerTypeViews = scenarioTriggerTypeViewRepository.findByIdIn(refEntityIds.triggerTypes)
                .stream().collect(Collectors.toMap(ScenarioTriggerTypeView::getId, triggerTypeView -> triggerTypeView));
            actionTypeViews = scenarioActionTypeViewRepository.findByIdIn(refEntityIds.actionTypes)
                .stream().collect(Collectors.toMap(ScenarioActionTypeView::getId, actionTypeView -> actionTypeView));
            treeItemMedias = scenarioDictionariesViewService.getTreeItemMedias();
            stateViews = stateViewRepository.findByIdIn(refEntityIds.states)
                    .stream().collect(Collectors.toMap(StateView::getId, stateView -> stateView));
        }

        public boolean containsAll(ScenarioReferenceEntityIds refEntityIds) {
            return triggerTypes.keySet().containsAll(refEntityIds.triggerTypes) &&
                    actionTypes.keySet().containsAll(refEntityIds.actionTypes) &&
                    triggerTypeViews.keySet().containsAll(refEntityIds.triggerTypes) &&
                    actionTypeViews.keySet().containsAll(refEntityIds.actionTypes) &&
                    /* NOTE: в базе нет некоторых иконок дерева сценария (v0.17)
                    treeItemMedias.keySet().containsAll(EnumSet.allOf(TreeItemType.class)) && */
                    stateViews.keySet().containsAll(refEntityIds.states);
        }

        public void setStateViews(Map<String, StateView> stateViews, List<ScenarioTimeLineBlock> timeLineBlocks) {
            Map<String, StateView> stateViewMap = new HashMap<>();
            for (ScenarioTimeLineBlock timeLineBlock : timeLineBlocks)
                if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE &&
                        timeLineBlock.getConditionType() == ConditionType.DEVICE_STATE) {
                    String stateId = timeLineBlock.getConditionDeviceStateId();
                    stateViewMap.put(stateId, stateViews.get(stateId));
                }
            this.stateViews = stateViewMap;
        }
    }

    /**
     * Идентификаторы сущностей, используемых в сценарии
     */
    public class ScenarioEntityIds {
        /** Устройства (включая прибор) */
        public Set<String> devices = new HashSet<>();
        /** Виртуальные состояния */
        public Set<String> virtualStates = new HashSet<>();
        /** Сценарии */
        public Set<String> scenarios = new HashSet<>();
        /** Зоны */
        public Set<String> regions = new HashSet<>();

        /** Удаление идентификаторов всех сущностей из представленного набора сущностей */
        public void removeAll(ScenarioEntitiesById entities) {
            devices.removeAll(entities.devices.keySet());
            virtualStates.removeAll(entities.virtualStates.keySet());
            scenarios.removeAll(entities.scenarios.keySet());
            regions.removeAll(entities.regions.keySet());
        }

        @Override
        public String toString() {
            StringBuilder message = new StringBuilder();
            if (!devices.isEmpty()) {
                message.append(" Устройства: ").append(devices);
            }
            if (!virtualStates.isEmpty()) {
                message.append(" Виртуальные состояния: ").append(virtualStates);
            }
            if (!scenarios.isEmpty()) {
                message.append(" Сценарии: ").append(scenarios);
            }
            if (!regions.isEmpty()) {
                message.append(" Зоны: ").append(regions);
            }
            return message.toString();
        }
    }

    /**
     * Сущности, используемые в сценарии
     */
    public class ScenarioEntitiesById {
        /** Устройства (включая прибор) */
        public Map<String, ScenarioDeviceView> devices;
        /** Виртуальные состояния */
        public Map<String, VirtualState> virtualStates;
        /** Сценарии */
        public Map<String, Scenario> scenarios;
        /** Зоны */
        public Map<String, Region> regions;

        private void InitializeMaps(List<Scenario> scenarios, List<ScenarioDeviceView> devices,
                List<Region> regions, List<VirtualState> virtualStates) {
            this.scenarios = scenarios.stream().collect(Collectors.toMap(Scenario::getId, scenario -> scenario));
            this.devices = devices.stream().collect(Collectors.toMap(ScenarioDeviceView::getId, device -> device));
            this.regions = regions.stream().collect(Collectors.toMap(Region::getId, region -> region));
            this.virtualStates =
                    virtualStates.stream().collect(Collectors.toMap(VirtualState::getId, virtualState -> virtualState));
        }

        private void InitializeMaps(Set<Scenario> scenarios, Set<ScenarioDeviceView> devices,
                                    Set<Region> regions, Set<VirtualState> virtualStates) {
            this.scenarios = scenarios.stream().collect(Collectors.toMap(Scenario::getId, scenario -> scenario));
            this.devices = devices.stream().collect(Collectors.toMap(ScenarioDeviceView::getId, device -> device));
            this.regions = regions.stream().collect(Collectors.toMap(Region::getId, region -> region));
            this.virtualStates =
                    virtualStates.stream().collect(Collectors.toMap(VirtualState::getId, virtualState -> virtualState));
        }

        private List<ScenarioDeviceView> toScenarioDeviceViews(List<TreeItemDeviceView> deviceViews) {
            return deviceViews.stream().map(view -> new ScenarioDeviceView(view)).collect(Collectors.toList());
        }

        private Set<ScenarioDeviceView> toScenarioDeviceViews(Set<DeviceView> deviceViews) {
            return deviceViews.stream().map(view -> new ScenarioDeviceView(view)).collect(Collectors.toSet());
        }

        public ScenarioEntitiesById(List<Scenario> scenarios, List<TreeItemDeviceView> devices,
                List<Region> regions, List<VirtualState> virtualStates) {
            InitializeMaps(scenarios, toScenarioDeviceViews(devices),
                    regions, virtualStates);
        }

        public ScenarioEntitiesById(String projectId, ScenarioEntityIds entityIds) {
            InitializeMaps(scenarioRepository.findByProjectIdAndIdIn(projectId, entityIds.scenarios),
                    deviceViewRepository.findByProjectIdAndIdIn(projectId, entityIds.devices)
                            .stream().map(device -> new ScenarioDeviceView(device)).collect(Collectors.toList()),
                    regionRepository.findByProjectIdAndIdIn(projectId, entityIds.regions),
                    virtualStateRepository.findByProjectIdAndIdIn(projectId, entityIds.virtualStates));
        }

        public ScenarioEntitiesById(Set<Scenario> scenarios, Set<DeviceView> deviceViews,
                                    Set<Region> regions, Set<VirtualState> virtualStates) {
            InitializeMaps(scenarios, toScenarioDeviceViews(deviceViews),
                    regions, virtualStates);
        }

        public boolean containsAll(ScenarioEntityIds entityIds) {
            return devices.keySet().containsAll(entityIds.devices) &&
                    virtualStates.keySet().containsAll(entityIds.virtualStates) &&
                    scenarios.keySet().containsAll(entityIds.scenarios) &&
                    regions.keySet().containsAll(entityIds.regions);
        }
    }

    /**
     * Сбор идентификаторов сущностей сценария
     */
    private void getScenarioEntityIds(Scenario scenario,
            ScenarioReferenceEntitiesById refEntitiesById,
            ScenarioEntityIds entityIds) {
        if (scenario == null)
            return;

        getControlDeviceId(scenario, entityIds);

        getLogicEntityIds(scenario.getStartLogic(), refEntitiesById.triggerTypes, entityIds);
        getLogicEntityIds(scenario.getStopLogic(), refEntitiesById.triggerTypes, entityIds);

        for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks())
            getExecEntityIds(timeLineBlock, refEntitiesById.actionTypes, entityIds);
    }

    private void getControlDeviceId(Scenario scenario,
            ScenarioEntityIds entityIds) {
        String controlDeviceId = scenario.getControlDeviceId();
        if (controlDeviceId != null)
            entityIds.devices.add(controlDeviceId);
    }

    private void getLogicEntityIds(ScenarioLogicBlock logicBlock,
            Map<String, ScenarioTriggerType> triggerTypesById,
            ScenarioEntityIds entityIds) {
        if (logicBlock == null)
            return;
        ScenarioTriggerType triggerType = triggerTypesById.get(logicBlock.getTriggerTypeId());
        if (triggerType != null) {
            switch (triggerType.getEntityType()) {
            case DEVICE:
            case CONTROL_DEVICE:
                entityIds.devices.addAll(logicBlock.getEntityIds());
                break;
            case REGION:
                entityIds.regions.addAll(logicBlock.getEntityIds());
                break;
            case VIRTUAL_STATE:
                entityIds.virtualStates.addAll(logicBlock.getEntityIds());
                break;
            default:
                break;
            }
        }
        for (ScenarioLogicBlock sublogic : logicBlock.getSubLogics())
            getLogicEntityIds(sublogic, triggerTypesById, entityIds);
    }

    private void getExecEntityIds(ScenarioTimeLineBlock timeLineBlock,
            Map<String, ScenarioActionType> actionTypesById,
            ScenarioEntityIds entityIds) {
        if (timeLineBlock == null)
            return;
        switch (timeLineBlock.getBlockType()) {
        case TRACING:
            getTracingEntityId(timeLineBlock, entityIds);
            break;
        case EXECUTIVE:
            getConditionEntityId(timeLineBlock, entityIds);
            for (Action action : timeLineBlock.getActions())
                getActionEntityId(action, actionTypesById, entityIds);
            break;
        case COMPUTER_ACTION:
        default:
            break;
        }
    }

    private void getTracingEntityId(ScenarioTimeLineBlock timeLineBlock,
            ScenarioEntityIds entityIds) {
        switch (timeLineBlock.getTracingEntityType()) {
        case SENSOR_DEVICE:
            entityIds.devices.add(timeLineBlock.getTracingEntityId());
            break;
        case VIRTUAL_STATE:
            entityIds.virtualStates.add(timeLineBlock.getTracingEntityId());
            break;
        default:
            break;
        }
    }

    private void getConditionEntityId(ScenarioTimeLineBlock timeLineBlock,
            ScenarioEntityIds entityIds) {
        switch (timeLineBlock.getConditionType()) {
        case DEVICE_STATE:
            entityIds.devices.add(timeLineBlock.getConditionEntityId());
            break;
        case VIRTUAL_STATE:
            entityIds.virtualStates.add(timeLineBlock.getConditionEntityId());
            break;
        case NONE:
        default:
            break;
        }
    }

    private void getActionEntityId(Action action,
            Map<String, ScenarioActionType> actionTypesById,
            ScenarioEntityIds entityIds) {
        if (action == null)
            return;
        ScenarioActionType actionType = actionTypesById.get(action.getActionTypeId());
        if (actionType != null) {
            String actionEntityId = action.getEntityId();
            switch (actionType.getEntityType()) {
            case DEVICE:
                entityIds.devices.add(actionEntityId);
                break;
            case REGION:
                entityIds.regions.add(actionEntityId);
                break;
            case VIRTUAL_STATE:
                entityIds.virtualStates.add(actionEntityId);
                break;
            case SCENARIO:
                entityIds.scenarios.add(actionEntityId);
                break;
            default:
                break;
            }
        }
    }

    public void getTriggerTypeIds(ScenarioLogicBlock logic, Set<String> triggerTypeIds) {
        if (logic != null) {
            String triggerTypeId = logic.getTriggerTypeId();
            if (triggerTypeId != null && !triggerTypeId.isEmpty())
                triggerTypeIds.add(triggerTypeId);
            for (ScenarioLogicBlock sublogic : logic.getSubLogics())
                getTriggerTypeIds(sublogic, triggerTypeIds);
        }
    }

    public void getActionTypeIds(List<ScenarioTimeLineBlock> timeLineBlocks, Set<String> actionTypeIds) {
        for (ScenarioTimeLineBlock timeLineBlock : timeLineBlocks)
            for (Action action : timeLineBlock.getActions())
                if (action != null)
                    actionTypeIds.add(action.getActionTypeId());
    }

    private void getStateIds(List<ScenarioTimeLineBlock> timeLineBlocks, Set<String> stateIds) {
        for (ScenarioTimeLineBlock timeLineBlock : timeLineBlocks)
            if (timeLineBlock.getBlockType() == BlockType.EXECUTIVE &&
                    timeLineBlock.getConditionType() == ConditionType.DEVICE_STATE)
                stateIds.add(timeLineBlock.getConditionDeviceStateId());
    }

    public boolean validateScenario(Scenario scenario, StringBuilder errorMessage) {
        String projectId = scenario.getProjectId();
        if (!validateScenarioBasicParams(scenario, new ScenarioBasicParams(scenario), errorMessage))
            return false;
        if (scenario.getScenarioPurpose() != ScenarioPurpose.EXEC_BY_INVOKE) {
            if (scenario.getStartLogic() != null &&
                    !validateScenarioLogicBlock(projectId, scenario.getStartLogic(), scenario.getScenarioPurpose(),
                    errorMessage))
                return false;
            if (scenario.getStopLogic() != null &&
                    !validateScenarioLogicBlock(projectId, scenario.getStopLogic(), scenario.getScenarioPurpose(),
                    errorMessage))
                return false;
        }
        for (ScenarioTimeLineBlock timeLineBlock : scenario.getTimeLineBlocks())
            if (!validateScenarioTimeLineBlock(timeLineBlock, errorMessage, scenario.getScenarioPurpose(), false))
                return false;
        return true;
    }

    @Override
    public void clearTags(String projectId) {
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);
        scenarios.forEach(scenario -> scenario.getFilterTags().getTags().clear());
        scenarioRepository.save(scenarios);
    }

    @Override
    public void addSubsystemTags(String projectId){
        List<Scenario> scenarios = scenarioRepository.findByProjectId(projectId);

        Map<String, FilterTags> scenarioIdToFilterTags = new HashMap<>();
        scenarios.forEach(scenario -> scenarioIdToFilterTags.put(scenario.getId(), scenario.getFilterTags()));
        Map<String, FilterTags> deviceIdToFilterTags = deviceRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Device::getId, Device::getFilterTags));
        Map<String, Subsystem> regionIdToSubsystem = regionRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(Region::getId, Region::getSubsystem));
        Map<String, FilterTags> virtualStateIdToFilterTags = virtualStateRepository.findByProjectId(projectId)
                .stream()
                .collect(Collectors.toMap(VirtualState::getId, VirtualState::getFilterTags));

        scenarios.forEach(scenario -> {
            FilterTags filterTags = scenario.getFilterTags();

            setSubsystemsFromLogicBlocks(scenario, filterTags.getTags(), deviceIdToFilterTags, regionIdToSubsystem,
                    virtualStateIdToFilterTags);
            setSubsystemsFromTLBlocks(scenario, filterTags.getTags(), deviceIdToFilterTags, regionIdToSubsystem,
                    virtualStateIdToFilterTags, scenarioIdToFilterTags);
        });

        scenarioRepository.save(scenarios);
    }

    private void setSubsystemsFromLogicBlocks(Scenario scenario, Set<String> scenarioTags,
                                              Map<String, FilterTags> deviceIdToFilterTags,
                                              Map<String, Subsystem> regionIdToSubsystem,
                                              Map<String, FilterTags> virtualStateIdToFilterTags) {
        Map<String, List<String>> triggerTypeIdToEntityIds = new HashMap<>();
        setTriggerTypeIdToEntityIds(scenario.getStartLogic(), triggerTypeIdToEntityIds);
        ScenarioLogicBlock stopLogicBlock = scenario.getStopLogic();
        if (stopLogicBlock != null)
            setTriggerTypeIdToEntityIds(stopLogicBlock, triggerTypeIdToEntityIds);

        List<ScenarioTriggerType> scenarioTriggerTypes = scenarioTriggerTypeRepository
                .findByIdIn(triggerTypeIdToEntityIds.keySet());
        Map<ScenarioTriggerType.TriggerEntityType, List<String>> triggerEntityTypeToEntityIds =
                new EnumMap<>(ScenarioTriggerType.TriggerEntityType.class);
        for (ScenarioTriggerType.TriggerEntityType triggerEntityType: ScenarioTriggerType.TriggerEntityType.values()) {
            triggerEntityTypeToEntityIds.put(triggerEntityType, new ArrayList<>());
        }

        scenarioTriggerTypes.forEach(scenarioTriggerType -> triggerEntityTypeToEntityIds
                .get(scenarioTriggerType.getEntityType())
                .addAll(triggerTypeIdToEntityIds.get(scenarioTriggerType.getId())));

        triggerEntityTypeToEntityIds.keySet().forEach(triggerEntityType -> {
            List<String> entityIds = triggerEntityTypeToEntityIds.get(triggerEntityType);
            switch (triggerEntityType){
                case DEVICE:
                case CONTROL_DEVICE:
                    inheritSubsystemFilterTags(deviceIdToFilterTags, scenarioTags, entityIds);
                    break;
                case REGION:
                    entityIds.forEach(regionId -> scenarioTags
                            .add(regionIdToSubsystem.get(regionId).toString()));
                    break;
                case VIRTUAL_STATE:
                    inheritSubsystemFilterTags(virtualStateIdToFilterTags, scenarioTags, entityIds);
                    break;
            }
        });
    }

    private void setSubsystemsFromTLBlocks(Scenario scenario, Set<String> scenarioTags,
                                           Map<String, FilterTags> deviceIdToFilterTags,
                                           Map<String, Subsystem> regionIdToSubsystem,
                                           Map<String, FilterTags> virtualStateIdToFilterTags,
                                           Map<String, FilterTags> scenarioIdToFilterTags) {

        /* добавление меток подсистем из условий исполнительных блоков и типов у блоков слежения */
        scenario.getTimeLineBlocks().forEach(tlBlock -> {
            if (tlBlock.getBlockType() == BlockType.EXECUTIVE && tlBlock.getConditionType() != ConditionType.NONE) {
                if (tlBlock.getConditionType() == ConditionType.DEVICE_STATE) {
                    scenarioTags.addAll(deviceIdToFilterTags.get(tlBlock.getConditionEntityId()).chooseSubsystemTags());
                } else {
                    scenarioTags.addAll(virtualStateIdToFilterTags
                            .get(tlBlock.getConditionEntityId()).chooseSubsystemTags());
                }
            } else if (tlBlock.getBlockType() == BlockType.TRACING){
                if (tlBlock.getTracingEntityType() == TracingEntityType.SENSOR_DEVICE){
                    scenarioTags.addAll(deviceIdToFilterTags.get(tlBlock.getTracingEntityId()).chooseSubsystemTags());
                } else {
                    scenarioTags.addAll(virtualStateIdToFilterTags
                            .get(tlBlock.getTracingEntityId()).chooseSubsystemTags());
                }
            }
        });

        /* добавление меток подсистем из действий блоков */
        Map<String, String> actionTypeIdToEntityId = new HashMap<>();
        Map<ScenarioActionType.ActionEntityType, List<String>> entityTypeToEntityIds
                = new EnumMap<>(ScenarioActionType.ActionEntityType.class);
        for (ScenarioActionType.ActionEntityType actionEntityType : ScenarioActionType.ActionEntityType.values()){
            entityTypeToEntityIds.put(actionEntityType, new ArrayList<>());
        }
        setActionTypeIdToEntityId(scenario.getTimeLineBlocks(), actionTypeIdToEntityId);
        List<ScenarioActionType> actionTypes = scenarioActionTypeRepository
                .findByIdIn(actionTypeIdToEntityId.keySet());

        actionTypes.forEach(actionType -> entityTypeToEntityIds.get(actionType.getEntityType())
                .add(actionTypeIdToEntityId.get(actionType.getId())));

        entityTypeToEntityIds.keySet().forEach(actionEntityType -> {
            List<String> entityIds = entityTypeToEntityIds.get(actionEntityType);
            switch (actionEntityType){
                case DEVICE:
                    inheritSubsystemFilterTags(deviceIdToFilterTags, scenarioTags, entityIds);
                    break;
                case REGION:
                    entityIds.forEach(regionId -> scenarioTags
                            .add(regionIdToSubsystem.get(regionId).toString()));
                    break;
                case VIRTUAL_STATE:
                    inheritSubsystemFilterTags(virtualStateIdToFilterTags, scenarioTags, entityIds);
                    break;
                case SCENARIO:
                    inheritSubsystemFilterTags(scenarioIdToFilterTags, scenarioTags, entityIds);
                    break;
            }
        });
    }

    private void setTriggerTypeIdToEntityIds(ScenarioLogicBlock scenarioLogicBlock,
                                             Map<String, List<String>> triggerTypeIdToEntityIds){
        triggerTypeIdToEntityIds.put(scenarioLogicBlock.getTriggerTypeId(), scenarioLogicBlock.getEntityIds());
        if (!scenarioLogicBlock.getSubLogics().isEmpty()){
            for (ScenarioLogicBlock subLogic: scenarioLogicBlock.getSubLogics()){
                setTriggerTypeIdToEntityIds(subLogic, triggerTypeIdToEntityIds);
            }
        }
    }

    private void setActionTypeIdToEntityId(List<ScenarioTimeLineBlock> scenarioTimeLineBlocks,
                                           Map<String, String> actionTypeIdToEntityId){
        scenarioTimeLineBlocks.forEach(scenarioTimeLineBlock ->
                scenarioTimeLineBlock.getActions().forEach(action ->
                        actionTypeIdToEntityId.put(action.getActionTypeId(), action.getEntityId())
                )
        );
    }

    private void inheritSubsystemFilterTags(Map<String, FilterTags> entityIdToFilterTags,
                                            Set<String> scenarioTags, List<String> entityIds){
        entityIds.forEach(entityId -> {
            FilterTags filterTags = entityIdToFilterTags.get(entityId);
            if (filterTags != null) {
                scenarioTags
                        .addAll(entityIdToFilterTags.get(entityId).chooseSubsystemTags());
            }
        });
    }

    @Override
    public void safelyWriteActiveDeviceTreeItemByProjectId(String projectId, OutputStream outputStream,
                                                           ObjectMapper objectMapper) throws IOException {

        int nScenarioPages = (activeScenarioRepository.countByProjectId(projectId)
                / AGGREGATED_ACTIVE_SCENARIOS_SAFE_PAGE_SIZE) + 1;
        outputStream.write("[".getBytes());

        /* Для создания этого обьекта заране известны все поля, кроме stateViews.
           Оно будет заполняться при рассмотрении отдельного сценария  */
        ScenarioReferenceEntitiesById referenceEntitiesById = initScenarioReferenceEntitiesWithoutStateViews();

        for (int pageNo = 0; pageNo < nScenarioPages; ++pageNo) {
            List<AggregatedActiveScenarioView> scenarios = getAggregatedActiveScenarioViews(projectId, null,
                    AGGREGATED_ACTIVE_SCENARIOS_SAFE_PAGE_SIZE, pageNo, referenceEntitiesById);
            for (int scenarioNo = 0; scenarioNo < scenarios.size(); ++scenarioNo) {
                AggregatedActiveScenarioView scenario = scenarios.get(scenarioNo);
                outputStream.write(objectMapper.writeValueAsBytes(scenario));
                if (pageNo < (nScenarioPages - 1) || scenarioNo < (scenarios.size() - 1)) {
                    outputStream.write(",".getBytes());
                }
            }
            outputStream.flush();
        }
        outputStream.write("]".getBytes());
    }

    private ScenarioReferenceEntitiesById initScenarioReferenceEntitiesWithoutStateViews(){
        Map<String, ScenarioTriggerType> triggerTypeMap = scenarioTriggerTypeRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(ScenarioTriggerType::getId, Function.identity()));
        Map<String, ScenarioTriggerTypeView> triggerTypeViewMap = scenarioTriggerTypeViewRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(ScenarioTriggerTypeView::getId, Function.identity()));
        Map<String, ScenarioActionType> actionTypeMap = scenarioActionTypeRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(ScenarioActionType::getId, Function.identity()));
        Map<String, ScenarioActionTypeView> actionTypeViewMap = scenarioActionTypeViewRepository
                .findAll()
                .stream()
                .collect(Collectors.toMap(ScenarioActionTypeView::getId, Function.identity()));
        Map<TreeItemType, Media>treeItemMedias = scenarioDictionariesViewService.getTreeItemMedias();

        ScenarioReferenceEntitiesById referenceEntitiesById =
                new ScenarioReferenceEntitiesById(triggerTypeMap, actionTypeMap,
                        triggerTypeViewMap, actionTypeViewMap, treeItemMedias);
        return referenceEntitiesById;
    }

    @Override
    public List<AggregatedActiveScenarioView> getAggregatedActiveScenarioViewsByTimeAfter(Date lastObservedTime,
                                                                                          int pageSize, int pageNo) {
        /* Создавать справочный обьект на каждую страницу не очень хорошо,
           но создавать его снаружи специально перед вызовом этого метода еще хуже */
        ScenarioReferenceEntitiesById referenceEntitiesById = initScenarioReferenceEntitiesWithoutStateViews();
        return getAggregatedActiveScenarioViews(null, lastObservedTime, pageSize, pageNo,
                referenceEntitiesById);
    }

    private List<AggregatedActiveScenarioView> getAggregatedActiveScenarioViews(String projectId,
                                                                                Date lastObservedTime,
                                                                                int pageSize, int pageNo,
                                                                                ScenarioReferenceEntitiesById referenceEntitiesById){
        List<AggregatedActiveScenarioViewFromScript> scenarioViewsFromScript = new ArrayList<>();
        try {
            /* Перенаправляем запрос в mongo-скрипт */
            ScriptOperations scriptOperations = mongoTemplate.scriptOps();
            String lastObservedTimeString = null;
            if (lastObservedTime != null){
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                lastObservedTimeString = simpleDateFormat.format(lastObservedTime);
            }
            BasicDBList dbList = (BasicDBList) scriptOperations
                    .call(GET_AGGREGATED_ACTIVE_SCENARIOS_SCRIPT_NAME, projectId, lastObservedTimeString, pageSize, pageNo);
            for (Object dbObject : dbList) {
                scenarioViewsFromScript.add(mongoTemplate.getConverter()
                        .read(AggregatedActiveScenarioViewFromScript.class, (DBObject) dbObject));
            }
        } catch (Exception e) {
            logger.error("Failed get active scenario tree items via mongo-script, exception: ", e);
            throw e;
        }

        List<AggregatedActiveScenarioView> activeScenarioViews = new ArrayList<>();
        for (AggregatedActiveScenarioViewFromScript scenarioViewFromScript : scenarioViewsFromScript) {

            Set<DeviceView> deviceViews = new HashSet<>();
            Set<Region> regions = new HashSet<>();
            Set<Scenario> scenarios = new HashSet<>();
            Set<VirtualState> virtualStates = new HashSet<>();

            Scenario scenario = scenarioViewFromScript.getScenario();
            ScenarioLogicBlock startLogic = scenario.getStartLogic();
            ScenarioLogicBlock stopLogic = scenario.getStopLogic();

            /* Удаление обьектов, не используемых в блоках логики и блоках времени*/
            removeUnusedLogicBlockEntities(scenarioViewFromScript.getLogicBlockDeviceViews(), startLogic,
                   stopLogic, referenceEntitiesById.triggerTypes, TriggerEntityType.DEVICE, TriggerEntityType.CONTROL_DEVICE);
            removeUnusedLogicBlockEntities(scenarioViewFromScript.getLogicBlockRegions(), startLogic,
                   stopLogic, referenceEntitiesById.triggerTypes, TriggerEntityType.REGION);
            removeUnusedLogicBlockEntities(scenarioViewFromScript.getLogicBlockVirtualStates(), startLogic,
                    stopLogic, referenceEntitiesById.triggerTypes, TriggerEntityType.VIRTUAL_STATE);
            deviceViews.addAll(scenarioViewFromScript.getLogicBlockDeviceViews());
            regions.addAll(scenarioViewFromScript.getLogicBlockRegions());
            virtualStates.addAll(scenarioViewFromScript.getLogicBlockVirtualStates());

            List<ScenarioTimeLineBlock> tlBlocks = scenario.getTimeLineBlocks();
            removeUnusedActionEntities(scenarioViewFromScript.getActionDeviceViews(), tlBlocks,
                    referenceEntitiesById.actionTypes, ActionEntityType.DEVICE);
            removeUnusedActionEntities(scenarioViewFromScript.getActionRegions(), tlBlocks,
                    referenceEntitiesById.actionTypes, ActionEntityType.REGION);
            removeUnusedActionEntities(scenarioViewFromScript.getActionScenarios(), tlBlocks,
                    referenceEntitiesById.actionTypes, ActionEntityType.SCENARIO);
            removeUnusedActionEntities(scenarioViewFromScript.getActionVirtualStates(), tlBlocks,
                    referenceEntitiesById.actionTypes, ActionEntityType.VIRTUAL_STATE);
            deviceViews.addAll(scenarioViewFromScript.getActionDeviceViews());
            regions.addAll(scenarioViewFromScript.getActionRegions());
            scenarios.addAll(scenarioViewFromScript.getActionScenarios());
            virtualStates.addAll(scenarioViewFromScript.getActionVirtualStates());

            removeUnusedTlBlockEntities(scenarioViewFromScript.getTlBlockDeviceViews(), tlBlocks,
                    TracingEntityType.SENSOR_DEVICE, ConditionType.DEVICE_STATE);
            removeUnusedTlBlockEntities(scenarioViewFromScript.getTlBlockVirtualStates(), tlBlocks,
                    TracingEntityType.VIRTUAL_STATE, ConditionType.VIRTUAL_STATE);
            deviceViews.addAll(scenarioViewFromScript.getTlBlockDeviceViews());
            virtualStates.addAll(scenarioViewFromScript.getTlBlockVirtualStates());

            /* Добавление обьектов из блоков логики с уровнем вложенности больше 3 */
            addEntitiesFromLogicLevelGreaterThan3(startLogic, stopLogic, deviceViews, regions, virtualStates,
                    referenceEntitiesById.triggerTypes);

            /* Создание обьектов для генерации агрегированного представления сценария */
            Map<String, StateView> stateViewMap = scenarioViewFromScript.getTlBlockConditionStateViews()
                    .stream()
                    .collect(Collectors.toMap(StateView::getId, Function.identity()));
            referenceEntitiesById.setStateViews(stateViewMap, tlBlocks);
            ScenarioEntitiesById scenarioEntitiesById = new ScenarioEntitiesById(scenarios, deviceViews, regions,
                    virtualStates);

            AggregatedActiveScenarioView activeScenarioView = generateAggregatedActiveScenarioView(scenario,
                    scenarioEntitiesById, referenceEntitiesById);
            activeScenarioView.setFilterTags(scenario.getFilterTags());
            activeScenarioView.setGeneralStateCategoryView(scenarioViewFromScript.getGeneralStateCategoryView());
            activeScenarioViews.add(activeScenarioView);
        }

        return activeScenarioViews;
    }

    private void removeUnusedLogicBlockEntities(Set<? extends BasicEntity> entities, ScenarioLogicBlock startLogic,
                                                ScenarioLogicBlock stopLogic,
                                                Map<String, ScenarioTriggerType> triggerTypeMap,
                                                TriggerEntityType... entityTypes) {
        entities.removeIf(entity ->
                !(checkEntityIsInLogicBlocks(entity, startLogic, triggerTypeMap, entityTypes)
                        || checkEntityIsInLogicBlocks(entity, stopLogic, triggerTypeMap, entityTypes)));
    }

    private boolean checkEntityIsInLogicBlocks(BasicEntity entity, ScenarioLogicBlock logicBlock,
                                               Map<String, ScenarioTriggerType> triggerTypeMap,
                                               TriggerEntityType... entityTypes){
        if (logicBlock != null){
            if (logicBlock.getEntityIds().contains(entity.getId())){
                ScenarioTriggerType triggerType = triggerTypeMap.get(logicBlock.getTriggerTypeId());
                for (TriggerEntityType entityType: entityTypes) {
                    if (triggerType.getEntityType().equals(entityType)){
                        return true;
                    }
                }
            }
            if (!logicBlock.getSubLogics().isEmpty()){
                for (ScenarioLogicBlock subLogic: logicBlock.getSubLogics()) {
                    if (checkEntityIsInLogicBlocks(entity, subLogic, triggerTypeMap, entityTypes)){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void removeUnusedTlBlockEntities(Set<? extends BasicEntity> entities,
                                             List<ScenarioTimeLineBlock> tlBlocks,
                                             TracingEntityType tracingEntityType,
                                             ConditionType conditionType) {
        entities.removeIf(entity -> {
            for (ScenarioTimeLineBlock tlBlock: tlBlocks){
                if ((tlBlock.getTracingEntityId().equals(entity.getId())
                        && tracingEntityType.equals(tlBlock.getTracingEntityType()))
                    || (tlBlock.getConditionEntityId().equals(entity.getId()) &&
                        tlBlock.getConditionType().equals(conditionType))){
                    return false;
                }
            }
            return true;
        });
    }

    private void removeUnusedActionEntities(Set<? extends BasicEntity> entities,
                                            List<ScenarioTimeLineBlock> tlBlocks,
                                            Map<String, ScenarioActionType> scenarioActionTypeMap,
                                            ActionEntityType actionType){
        entities.removeIf(entity -> {
            for (ScenarioTimeLineBlock tlBlock: tlBlocks){
                for(Action action: tlBlock.getActions()){
                    if (action.getEntityId().equals(entity.getId())
                            && scenarioActionTypeMap.get(action.getActionTypeId()).getEntityType()
                            == actionType){
                        return false;
                    }
                }
            }
            return true;
        });
    }

    private void addEntitiesFromLogicLevelGreaterThan3(ScenarioLogicBlock startLogic, ScenarioLogicBlock stopLogic,
                                                       Set<DeviceView> deviceViews, Set<Region> regions,
                                                       Set<VirtualState> virtualStates,
                                                       Map<String, ScenarioTriggerType> triggerTypeMap){
        /* Т.к. в скрипте рассматриваются блоки логики с максимальной глубиной 3,
                    нужно проверить остальные уровни и добавить недостающие обьекты */
        List<ScenarioLogicBlock> nestedBlocks = new ArrayList<>(getLogicBlocksWithLevelGreaterThan3(startLogic));
        nestedBlocks.addAll(getLogicBlocksWithLevelGreaterThan3(stopLogic));
        Map<TriggerEntityType, List<String>> entityTypeSetMap = new HashMap<>();
        if (!nestedBlocks.isEmpty()){
            nestedBlocks.forEach(nestedBlock ->
                    addEntityIdsWithLevelGreaterThan3(entityTypeSetMap, nestedBlock, triggerTypeMap));
        }
        entityTypeSetMap.keySet().forEach(key -> {
            switch (key){
                case CONTROL_DEVICE:
                case DEVICE:
                    deviceViews.addAll(deviceViewRepository.findAllByDeviceIdIn(entityTypeSetMap.get(key)));
                    break;
                case REGION:
                    regions.addAll(regionRepository.findAllByIdIn(entityTypeSetMap.get(key)));
                    break;
                case VIRTUAL_STATE:
                    virtualStates.addAll(virtualStateRepository.findAllByIdIn(entityTypeSetMap.get(key)));
                    break;
                default:
                    break;
            }
        });
    }

    private List<ScenarioLogicBlock> getLogicBlocksWithLevelGreaterThan3(ScenarioLogicBlock logicBlock) {
        List<ScenarioLogicBlock> nestedBlocks = new ArrayList<>();
        if (logicBlock != null && !logicBlock.getSubLogics().isEmpty()){
            for (ScenarioLogicBlock subLogic: logicBlock.getSubLogics()){
                if (!subLogic.getSubLogics().isEmpty()){
                    for (ScenarioLogicBlock subSubLogic: subLogic.getSubLogics()){
                        if (!subSubLogic.getSubLogics().isEmpty()){
                            nestedBlocks.addAll(subSubLogic.getSubLogics());
                        }
                    }
                }
            }
        }
        return nestedBlocks;
    }

    private void addEntityIdsWithLevelGreaterThan3(Map<TriggerEntityType, List<String>> entityTypeSetMap,
                                                   ScenarioLogicBlock logicBlock,
                                                   Map<String, ScenarioTriggerType> triggerTypeMap) {
        if (logicBlock != null){
            entityTypeSetMap.put(triggerTypeMap.get(logicBlock.getTriggerTypeId()).getEntityType(),
                    logicBlock.getEntityIds());
            if (!logicBlock.getSubLogics().isEmpty()){
                logicBlock.getSubLogics().forEach(subLogic -> addEntityIdsWithLevelGreaterThan3(entityTypeSetMap,
                        subLogic, triggerTypeMap));
            }
        }
    }

    public List<ScenarioComputerAction> getComputerActionsFromScenario(AggregatedActiveScenarioView scenarioView){
        List<ScenarioComputerAction> scenarioComputerActions = new ArrayList<>();
        if (scenarioView.getGeneralStateCategoryView().getStateCategory().isScenarioRunning()
                && !scenarioView.getTimeLineBlocks().isEmpty()) {
            for (ScenarioTimeLineBlock tlBlock : scenarioView.getTimeLineBlocks()) {
                if (tlBlock.getBlockType() == BlockType.COMPUTER_ACTION) {
                    for (ComputerAction computerAction : tlBlock.getComputerActions()) {
                        ScenarioComputerAction scenarioComputerAction = new ScenarioComputerAction(tlBlock,
                                computerAction);
                        scenarioComputerActions.add(scenarioComputerAction);
                    }
                }
            }
        }
        return scenarioComputerActions;
    }

}
