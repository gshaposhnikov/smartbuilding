package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.ActiveSubsystemRepository;
import ru.rubezh.firesec.nt.domain.v1.ActiveSubsystem;
import ru.rubezh.firesec.nt.domain.v1.ManualResetStateGroup;
import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;
import ru.rubezh.firesec.nt.domain.v1.representation.SubsystemView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SubsystemViewService {

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private ActiveSubsystemRepository activeSubsystemRepository;

    public SubsystemView getActiveSubsystem(Subsystem subsystem) {
        Map<String, SubsystemView.Counter> counters = new HashMap<>();
        ActiveSubsystem activeSubsystem = activeSubsystemRepository.findOneBySubsystem(subsystem);
        SubsystemView result = null;

        if (activeSubsystem != null) {
            for (String stateCategoryId: activeSubsystem.getCounters().keySet()) {

                SubsystemView.Counter counter = counters.get(stateCategoryId);
                if (counter == null) {
                    StateCategoryView view = stateViewService.getStateCategoryViewById(stateCategoryId);
                    counter = new SubsystemView.Counter();
                    counter.setName(view.getName());
                    counter.setColor(view.getColor());
                    counter.setFontColor(view.getFontColor());
                }
                ActiveSubsystem.Counter activeSubsystemCounter = activeSubsystem.getCounters().get(stateCategoryId);
                counter.setValue(counter.getValue() + activeSubsystemCounter.getValue());
                counter.getActiveEntitiesIds().addAll(activeSubsystemCounter.getActiveEntitiesIds());

                counters.put(stateCategoryId, counter);
            }

            StateCategoryView view = stateViewService
                    .getStateCategoryViewById(activeSubsystem.getGeneralStateCategoryId());
            SubsystemView.Counter priorityCounter = new SubsystemView.Counter();
            priorityCounter.setName(view.getName());
            priorityCounter.setColor(view.getColor());
            priorityCounter.setFontColor(view.getFontColor());

            result = new SubsystemView(activeSubsystem, priorityCounter,
                    ManualResetStateGroup.groupsBySubsystem(activeSubsystem.getSubsystem()));
            result.setCounters(counters);
        }

        return result;
    }

    public List<SubsystemView> getAll() {
        List<SubsystemView> results = new ArrayList<>();

        for (Subsystem subsystem: Subsystem.values()) {
            if (subsystem == Subsystem.UNKNOWN) continue;
            SubsystemView result = getActiveSubsystem(subsystem);
            if (result != null) results.add(result);
        }

        return results;
    }

}
