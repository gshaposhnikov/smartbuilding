package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import ru.rubezh.firesec.nt.dao.v1.*;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.*;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;
import ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;
import ru.rubezh.firesec.nt.service.v1.skud.AccessKeyService;
import ru.rubezh.firesec.nt.service.v1.skud.EmployeeService;
import ru.rubezh.firesec.nt.service.v1.skud.WorkScheduleService;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectViewServiceImpl implements ProjectViewService {
    
    /** Экранирование "]]]]" в CDATA для Delphi*/
    private final static String NG_VERSION_ESCAPE_CHARS_IN_CDATA = "]]]]";
    /** Замена символами "##" кастомного символа экранирования "]]]]" в CDATA*/
    private final static String REPLACED_CHARS_NG_VERSION_ESCAPE_CHARS_IN_CDATA = "##";
    /** Экранирование "<![CDATA[>" в CDATA для Delphi*/
    private final static String NG_VERSION_ESCAPE_CDATA_IN_CDATA = "<!\\[CDATA\\[>";
    
    @Autowired
    private Logger logger;
    
    @Autowired
    private DeviceViewRepository deviceViewRepository;

    @Autowired
    private DeviceViewService deviceViewService;

    @Autowired
    private RegionViewRepository regionViewRepository;

    @Autowired
    private RegionViewService regionViewService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PlanGroupRepository planGroupRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ActiveProjectRepository activeProjectRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ScenarioViewService scenarioViewService;

    @Autowired
    private ScenarioRecoveryService scenarioRecoveryService;

    @Autowired
    private VirtualStateService virtualStateService;

    @Autowired
    private VirtualStateRepository virtualStateRepository;

    @Autowired
    private SubsystemViewService subsystemViewService;

    @Autowired
    private IndicatorPanelRepository indicatorPanelRepository;

    @Autowired
    private IndicatorPanelGroupRepository indicatorPanelGroupRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private AccessKeyService accessKeyService;

    @Autowired
    private WorkScheduleService workScheduleService;

    @Autowired
    private ImportedProjectXMLParsingHandler importedProjectXMLParsingHandler;

    @Autowired
    private List<ActivationFilterableService> activationFilterableServices;
    

    @Override
    public Project add(Project project, StringBuilder errorMessage) {
        return projectService.add(project, errorMessage);
    }

    @Override
    public boolean remove(String projectId, StringBuilder errorMessage) {
        boolean success = false;
        if (projectService.remove(projectId, errorMessage)) {
            deviceViewRepository.deleteByProjectId(projectId);
            regionViewRepository.deleteByProjectId(projectId);
            planRepository.deleteByProjectId(projectId);
            planGroupRepository.deleteByProjectId(projectId);
            indicatorPanelGroupRepository.deleteByProjectId(projectId);
            indicatorPanelRepository.deleteByProjectId(projectId);
            success = true;
        }
        return success;
    }

    @Override
    public Project update(String projectId, Project project, StringBuilder errorMessage) {
        return projectService.update(projectId, project, errorMessage);
    }

    @Override
    public BuildProjectEntitiesStreamSerializer getProjectEntitiesSerializer(String projectId, Language language,
            StringBuilder errorMessage) {
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден");
            return null;
        }

        return new BuildProjectEntitiesStreamSerializer() {

            @Override
            public void writeProject(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"project\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(project));
            }

            @Override
            public void writeVirtualStates(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<VirtualState> virtualStates = virtualStateRepository.findByProjectId(projectId);
                outputStream.write("\"virtualStates\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(virtualStates));
            }

            @Override
            public void writeRegions(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<RegionView> regions = regionViewRepository.findByProjectId(projectId);
                outputStream.write("\"regions\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(regions));
            }

            @Override
            public void writePlans(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<Plan> plans = planRepository.findByProjectId(projectId);
                outputStream.write("\"plans\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(plans));
            }

            @Override
            public void writePlanGroups(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<PlanGroup> planGroups = planGroupRepository.findByProjectId(projectId);
                outputStream.write("\"planGroups\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(planGroups));
            }

            @Override
            public void writeScenarios(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<AggregatedScenarioView> scenarios = scenarioViewService.getAllAggregatedScenarioViews(projectId);
                outputStream.write("\"scenarios\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(scenarios));
            }

            @Override
            public void writeDevices(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<TreeItemDeviceView> devices = deviceViewService.getTreeItemDeviceViewsByProjectId(projectId,
                        language);
                outputStream.write("\"devices\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(devices));
            }

            @Override
            public void writeIndicatorPanels(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);
                outputStream.write("\"indicatorPanels\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(indicatorPanels));
            }

            @Override
            public void writeIndicatorPanelGroups (OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<IndicatorPanelGroup> indicatorPanelGroups = indicatorPanelGroupRepository.findByProjectId(projectId);
                outputStream.write("\"indicatorPanelGroups\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(indicatorPanelGroups));
            }

            @Override
            public void writeEmployees(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"employees\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<Employee> employees = employeeService.get(projectId, localErrorMessage);
                if (employees != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(employees));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }

            @Override
            public void writeAccessKeys(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"accessKeys\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<AccessKey> accessKeys = accessKeyService.get(projectId, localErrorMessage);
                if (accessKeys != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(accessKeys));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }

            @Override
            public void writeWorkSchedules(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"workSchedules\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<WorkSchedule> workSchedules = workScheduleService.get(projectId, localErrorMessage);
                if (workSchedules != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(workSchedules));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }
           
            @Override
            public void writeAll(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("{".getBytes());
                writeProject(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeDevices(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeRegions(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeVirtualStates(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeScenarios(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writePlans(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writePlanGroups(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeIndicatorPanels(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeIndicatorPanelGroups(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeEmployees(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeAccessKeys(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeWorkSchedules(outputStream, objectMapper);
                outputStream.write("}".getBytes());
                outputStream.flush();
            }
        };
    }

    @Override
    public ActiveProjectEntitiesStreamSerializer getActiveProjectEntitiesSerializer(String projectId, Language language,
            StringBuilder errorMessage) {
        Project project = projectRepository.findOne(projectId);
        if (project == null) {
            errorMessage.append("Проект не найден");
            return null;
        }
        if (project.getStatus() != ProjectStatus.ACTIVE) {
            errorMessage.append("Проект не активен");
            return null;
        }
        return new ActiveProjectEntitiesStreamSerializer() {

            @Override
            public void writeProject(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"project\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(project));
                outputStream.flush();
            }

            @Override
            public void writeVirtualStates(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<VirtualState> virtualStates = virtualStateRepository.findByProjectId(projectId);
                outputStream.write("\"virtualStates\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(virtualStates));
                outputStream.flush();
            }

            @Override
            public void writePlans(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<Plan> plans = planRepository.findByProjectId(projectId);
                outputStream.write("\"plans\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(plans));
                outputStream.flush();
            }

            @Override
            public void writePlanGroups(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<PlanGroup> planGroups = planGroupRepository.findByProjectId(projectId);
                outputStream.write("\"planGroups\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(planGroups));
                outputStream.flush();
            }

            @Override
            public void writeSubsystems(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<SubsystemView> subsystems = subsystemViewService.getAll();
                outputStream.write("\"subsystems\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(subsystems));
                outputStream.flush();
            }

            @Override
            public void writeScenarios(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"scenarios\":".getBytes());
                scenarioViewService.safelyWriteActiveDeviceTreeItemByProjectId(projectId, outputStream, objectMapper);
            }

            @Override
            public void writeRegions(OutputStream outputStream, ObjectMapper objectMapper)
                    throws IOException {
                outputStream.write("\"regions\":".getBytes());
                regionViewService.safelyWriteAggregatedActiveRegionsByProjectId(projectId, outputStream, objectMapper);
            }

            @Override
            public void writeActiveProject(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                ActiveProject activeProject = activeProjectRepository.findOne(projectId);
                outputStream.write("\"activeProject\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(activeProject));
                outputStream.flush();
            }

            @Override
            public void writeDevices(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"devices\":".getBytes());
                deviceViewService.safelyWriteActiveDeviceTreeItemByProjectId(projectId, outputStream, objectMapper);
            }

            @Override
            public void writeIndicatorPanels(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<IndicatorPanel> indicatorPanels = indicatorPanelRepository.findByProjectId(projectId);
                outputStream.write("\"indicatorPanels\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(indicatorPanels));
                outputStream.flush();
            }

            @Override
            public void writeIndicatorPanelGroups(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                List<IndicatorPanelGroup> indicatorPanelGroups = indicatorPanelGroupRepository.findByProjectId(projectId);
                outputStream.write("\"indicatorPanelGroups\":".getBytes());
                outputStream.write(objectMapper.writeValueAsBytes(indicatorPanelGroups));
                outputStream.flush();
            }

            @Override
            public void writeEmployees(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"employees\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<Employee> employees = employeeService.get(projectId, localErrorMessage);
                if (employees != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(employees));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }

            @Override
            public void writeAccessKeys(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"accessKeys\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<AccessKey> accessKeys = accessKeyService.get(projectId, localErrorMessage);
                if (accessKeys != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(accessKeys));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }

            @Override
            public void writeWorkSchedules(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("\"workSchedules\":".getBytes());
                StringBuilder localErrorMessage = new StringBuilder();
                List<WorkSchedule> workSchedules = workScheduleService.get(projectId, localErrorMessage);
                if (workSchedules != null) {
                    outputStream.write(objectMapper.writeValueAsBytes(workSchedules));
                } else {
                    outputStream.write("[]".getBytes());
                    logger.error(localErrorMessage);
                }
            }
            
            @Override
            public void writeAll(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
                outputStream.write("{".getBytes());
                writeProject(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeActiveProject(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeDevices(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeRegions(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeSubsystems(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeVirtualStates(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeScenarios(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writePlans(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writePlanGroups(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeIndicatorPanels(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                writeIndicatorPanelGroups(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeEmployees(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeAccessKeys(outputStream, objectMapper);
                outputStream.write(",".getBytes());
                outputStream.flush();
                writeWorkSchedules(outputStream, objectMapper);
                outputStream.write("}".getBytes());
            }

        };
    }

    @Override
    public ControlDeviceEntitiesView getControlDeviceEntitiesView(ControlDeviceEntities entities, Language language) {
        List<TreeItemDeviceView> deviceViews =
                deviceViewService.generateTreeItemDeviceViews(entities.getDevices(), entities.getRegions(), language);
        List<AggregatedScenarioView> scenarioViews =
                scenarioViewService.generateAggregatedScenarioViews(entities.getScenarios(), deviceViews,
                entities.getRegions(), entities.getVirtualStates());
        ControlDeviceEntitiesView entitiesView = new ControlDeviceEntitiesView(
                deviceViews != null ? deviceViews : Collections.emptyList(),
                entities.getVirtualStates(),
                scenarioViews != null ? scenarioViews : Collections.emptyList(),
                regionViewService.generateRegionViews(entities.getRegions()),
                entities.getCreated());
        return entitiesView;
    }
    
    @Override
    public Project addImpotredNgProjectView(String fileName, ImportedXmlProjectWrapper importedFile,
            StringBuilder errorMessage) {
        /*
         * Получив файл импорта NG версии проекта, необходимо заменить, кастомные "<!\\[CDATA\\[>" и "]]]]",
         *  экранированые символы которые находятся в CDATA тэга Content для корректного чтения парсером импортированного файла.
         */
        Project importedProject = new Project();
        String projectName = fileName.substring(0, fileName.length() - 4);
        String file  = importedFile.getImportedFile().replaceAll(NG_VERSION_ESCAPE_CHARS_IN_CDATA, REPLACED_CHARS_NG_VERSION_ESCAPE_CHARS_IN_CDATA);
        file  = file.replaceAll(NG_VERSION_ESCAPE_CDATA_IN_CDATA, "");
        importedProject.setName(projectName);
        importedProject.setVersion(projectName);
        Project projectSaved = add(importedProject, errorMessage);
        if (projectSaved != null) {
            byte[] byteArrayFileConten;
            try {
                byteArrayFileConten = file.getBytes();
                ByteArrayInputStream inputStream = new ByteArrayInputStream(byteArrayFileConten);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser saxParser = spf.newSAXParser();
                XMLReader xmlReader = saxParser.getXMLReader();
                ImportedProjectXMLParsingHandler handler = importedProjectXMLParsingHandler;
                String projectId = projectSaved.getId();
                handler.setProjectId(projectId);
                xmlReader.setContentHandler(handler);
                xmlReader.parse(new InputSource(inputStream));
                projectSaved.setImported(true);
                projectSaved.setImportValidateMessages(handler.getWarningMessages());
                projectSaved = projectRepository.save(projectSaved);
                if(projectSaved != null) {
                    return projectSaved;
                }else {
                    remove(projectId, errorMessage);
                    errorMessage.append("Не удалось импортировать проект, произошла ошибка при импорте проекта");
                }
                
            } catch (Exception e) {
                remove(projectSaved.getId(), errorMessage);
                errorMessage.append("Не удалось импортировать проект, файл не прошел валидацию");
                e.printStackTrace();
            }
        }
        return projectSaved;
    }

    @Override
    public UpdatedEntities addEntities(String projectId, ControlDeviceEntities entities, Language language,
            StringBuilder errorMessage) {

        if (!projectService.checkProjectIsEditable(projectId, errorMessage))
            return null;

        UpdatedEntities updatedEntities = new UpdatedEntities();

        // зоны
        Map<String, String> regionIds = new HashMap<>();
        List<RegionView> createdRegions =
                regionViewService.createByRecovery(projectId, entities.getRegions(), regionIds, errorMessage);
        if (createdRegions == null)
            return null;
        EntitiesContainer<RegionView> regionsUpdate = new EntitiesContainer<>();
        regionsUpdate.setCreated(createdRegions);
        updatedEntities.put("regions", regionsUpdate);

        // устройства
        Map<String, String> deviceIds = new HashMap<>();
        EntitiesContainer<TreeItemDeviceView> devicesUpdate =
                deviceViewService.createByRecovery(projectId, entities.getDevices(),
                createdRegions.stream().map(RegionView::getRegion).collect(Collectors.toList()),
                deviceIds, regionIds, errorMessage, language);
        if (devicesUpdate == null) {
            regionViewService.remove(projectId, createdRegions);
            return null;
        }
        updatedEntities.put("devices", devicesUpdate);

        // виртуальные состояния
        Map<String, String> virtualStateIds = new HashMap<>();
        List<VirtualState> createdVirtualStates = virtualStateService.createByRecovery(projectId,
                entities.getVirtualStates(), virtualStateIds, deviceIds, errorMessage);
        if (createdVirtualStates == null) {
            regionViewService.remove(projectId, createdRegions);
            deviceViewService.remove(projectId, devicesUpdate);
            return null;
        }
        EntitiesContainer<VirtualState> virtualStatesUpdate = new EntitiesContainer<>();
        virtualStatesUpdate.setCreated(createdVirtualStates);
        updatedEntities.put("virtualStates", virtualStatesUpdate);

        // сценарии
        List<AggregatedScenarioView> createdScenarios = scenarioRecoveryService.createByRecovery(projectId,
                entities.getScenarios(), regionIds, deviceIds, virtualStateIds, errorMessage);
        if (createdScenarios == null) {
            regionViewService.remove(projectId, createdRegions);
            deviceViewService.remove(projectId, devicesUpdate);
            virtualStateService.remove(projectId, createdVirtualStates);
            return null;
        }
        EntitiesContainer<AggregatedScenarioView> scenariosUpdate = new EntitiesContainer<>();
        scenariosUpdate.setCreated(createdScenarios);
        updatedEntities.put("scenarios", scenariosUpdate);

        return updatedEntities;
    }

    /* TODO: убрать анотации @Order на сервисах-имплементаторах, т.к. порядковой зависимости быть не должно.
     *  Чтобы этого избежать, можно выполнять цикл 2 раза. Сделать это можно будет после оптимизации
     *  проставления меток  */
    @Override
    public void setFilterTagsOnEntities(String projectId) {
        // Так как у обьектов зависимость может быть разная, необходимо две итерации.
        for (int i = 0; i < 1; i++) {
            activationFilterableServices.forEach(filterableService -> {
                // Перед заполнением тэгов необходимо удалить все старые тэги
                filterableService.clearTags(projectId);
                filterableService.addSubsystemTags(projectId);
            });
        }
    }
}
