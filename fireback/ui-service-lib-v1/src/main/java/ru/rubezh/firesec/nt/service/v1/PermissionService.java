package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.Permission;

import java.util.List;

@Service
public class PermissionService {

    private List<Permission> permissions;

    @Autowired
    private List<Permission.Target> permissionTargets;

    @Autowired
    private List<Permission.AccessLevel> permissionAccessLevels;

    public List<Permission> getPermissions() {
        return permissions;
    }

    @Autowired
    public void setPermissions(List<Permission> permissions) {
        for (Permission p: permissions) {
            if (p.getId() == null)
                p.setId(getPermissionId(p.getTargetId(), p.getAccessLevelId()));
        }
        this.permissions = permissions;
    }

    public List<Permission.Target> getPermissionTargets() {
        return permissionTargets;
    }

    public List<Permission.AccessLevel> getPermissionAccessLevels() {
        return permissionAccessLevels;
    }

    public static String getPermissionId(String targetId, String levelId) {
        return targetId + "_" + levelId;
    }
}
