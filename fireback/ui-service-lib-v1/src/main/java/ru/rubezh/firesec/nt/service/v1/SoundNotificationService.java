package ru.rubezh.firesec.nt.service.v1;

import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.SoundNotification;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

import java.util.Date;

@Service
public interface SoundNotificationService extends MediaService.MediaObserver {

    /**
     * Добавление звукового оповещения.
     *
     * Добавлет звуковое оповещение если заданы все необходимые парметры,
     * указанный класс состояния не используется в другом оповещении
     * и звук доступен для использования.
     *
     * @param soundNotification добавляемое звуковое оповещение
     * @param errorMessage сообщение возможной ошибки
     * @return true если успешно добавлено, иначе false
     */
    boolean addSoundNotification(SoundNotification soundNotification, StringBuilder errorMessage);

    /**
     * Обновление звукового оповещения.
     *
     * Изменяет параметры звукового оповещения, если оно есть,
     * класс состояния не изменяется и звук доступен для использования
     *
     * @param id идентификатор обновляемого звукового оповещения
     * @param soundNotification звуковое оповещение с новыми параметрами
     * @param errorMessage сообщение возможной ошибки
     * @return true, если успешно добавлено, иначе false
     */
    boolean updateSoundNotification(String id, SoundNotification soundNotification, StringBuilder errorMessage);

    /**
     * Удаление звукового оповещения.
     *
     * Помечает звуковое оповещение как удаленное, если оно есть.
     *
     * @param id идентификатор удаляемого опоовещения
     * @param errorMessage сообщение возможной ошибки
     * @return true, если звуковое оповещение помечено как удаленное, иначе false
     */
    boolean deleteSoundNotification(String id, StringBuilder errorMessage);

    /**
     * Получение обьектов(или их идентификаторов) для созданных/обновленных/удаленных сущностей.
     *
     * Собирает информацию по звуковым оповещениям, которые были созданы,
     * обновлены или удалены со времени последней проверки.
     *
     * @param from дата и время последней проверки.
     * @return обьект с информацией по измененным медиафайлам.
     */
    UpdatedEntities.EntitiesContainer<SoundNotification> getChangesAndDoRemove(Date from);
}
