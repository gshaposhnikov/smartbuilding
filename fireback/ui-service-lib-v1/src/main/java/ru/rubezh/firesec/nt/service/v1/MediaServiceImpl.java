package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.MediaRepository;
import ru.rubezh.firesec.nt.domain.v1.Media;
import ru.rubezh.firesec.nt.domain.v1.MediaType;
import ru.rubezh.firesec.nt.domain.v1.User;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

@Service
public class MediaServiceImpl implements MediaService {

    private static final int MAX_MEDIA_NAME_LENGTH = 30;

    @Autowired
    private MediaRepository mediaRepository;

    private List<MediaObserver> mediaObservers = new ArrayList<>();

    @Override
    public boolean addMedia(Media media, User currentUser, StringBuilder errorMessage) {
        if (!checkNameLength(media.getName().length(), errorMessage)){
            return false;
        }
        if (currentUser != null){
            media.setUserId(currentUser.getId());
        }
        mediaRepository.insert(media);
        return true;
    }

    @Override
    public boolean updateMedia(String id, Media media, StringBuilder errorMessage) {
        Media updatableMedia = mediaRepository.findOne(id);
        if (updatableMedia == null){
            errorMessage.append("Медиафайл не найден");
            return false;
        }
        if (!checkNameLength(media.getName().length(), errorMessage)){
            return false;
        }
        if (media.getMediaType() != updatableMedia.getMediaType()){
            errorMessage.append("Нельзя менять тип медиафайла");
            return false;
        }
        updatableMedia.update(media);
        mediaRepository.save(updatableMedia);
        return true;
    }

    private boolean checkNameLength(int nameLength, StringBuilder errorMessage){
        if (nameLength == 0){
            errorMessage.append("Имя медиафайла не может быть пустым");
            return false;
        }
        if (nameLength > MAX_MEDIA_NAME_LENGTH){
            errorMessage.append("Имя медиафайла слишком длинное");
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteMedia(String id, StringBuilder errorMessage) {
        Media removableMedia = mediaRepository.findOne(id);
        if (removableMedia == null){
            errorMessage.append("Медиафайл не найден");
            return false;
        }
        if (!checkMediaDeletionAvailability(removableMedia.getId())){
            errorMessage.append("Медиафайл используется в звуковых оповещениях");
            return false;
        }
        removableMedia.setRemoved(true);
        mediaRepository.save(removableMedia);
        return true;
    }

    @Override
    public UpdatedEntities.EntitiesContainer<Media> getChangesAndDoRemove(Date from){
        /* Заполняем изменения */
        UpdatedEntities.EntitiesContainer<Media> changes = new UpdatedEntities.EntitiesContainer<>();
        changes.setCreated(mediaRepository.findByRemovedIsFalseAndCreateDateTimeAfter(from));
        changes.setUpdated(mediaRepository.findByRemovedIsFalseAndUpdateDateTimeAfter(from));
        List<String> removedMediaIds = mediaRepository.findByRemovedIsTrue().stream()
                .map(Media::getId).collect(Collectors.toList());
        changes.setDeletedIds(removedMediaIds);

        /* Удаление отмеченных представлений */
        mediaRepository.deleteByIdIn(removedMediaIds);
        return changes;
    }

    @Override
    public boolean checkSoundMedia(String id, StringBuilder errorMessage) {
        if (id != null && !id.isEmpty() && !mediaRepository.existsByIdAndMediaType(id, MediaType.SOUND)){
            errorMessage.append("Медиафайл не найден");
            return false;
        }
        return true;
    }

    @Override
    public void addMediaObserver(MediaObserver mediaObserver) {
        mediaObservers.add(mediaObserver);
    }

    @Override
    public void removeMediaObserver(MediaObserver mediaObserver) {
        mediaObservers.remove(mediaObserver);
    }

    private boolean checkMediaDeletionAvailability(String mediaId){
        for (MediaObserver mediaObserver: mediaObservers) {
            if (mediaObserver.checkIfMediaIsUsed(mediaId))
                return false;
        }
        return true;
    }

}
