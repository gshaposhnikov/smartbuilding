package ru.rubezh.firesec.nt.service.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import ru.rubezh.firesec.nt.dao.v1.DeviceProfileRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceShapeLibraryRepository;
import ru.rubezh.firesec.nt.dao.v1.DeviceShapeRepository;
import ru.rubezh.firesec.nt.domain.v1.DeviceShape;
import ru.rubezh.firesec.nt.domain.v1.DeviceShapeLibrary;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities.EntitiesContainer;

@Service
public class DeviceShapeLibraryServiceImpl implements DeviceShapeLibraryService {

    @Autowired
    private DeviceShapeLibraryRepository deviceShapeLibraryRepository;

    @Autowired
    private DeviceProfileRepository deviceProfileRepository;

    @Autowired
    private DeviceShapeRepository deviceShapeRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private List<DeviceShapeLibraryObserver> deviceShapeLibraryObservers = new ArrayList<>();

    @Override
    public List<DeviceShapeLibrary> getActual(StringBuilder errorMessage) {
        /*
         * Нужно отфильтровать библиотеки, для которых отсутствует профиль
         * устройства
         */
        LookupOperation lookupOperation = LookupOperation.newLookup()
                .from("device_profiles")
                .localField("deviceProfileId")
                .foreignField("_id")
                .as("deviceProfiles");
        MatchOperation filterDevProfileAbsence = Aggregation.match(new Criteria("deviceProfiles").not().size(0));
        /* Также нужно отфильтровать библиотеки, отмеченные к удалению */
        MatchOperation filterPreparedToRemove = Aggregation.match(new Criteria("removed").is(false));
        Aggregation aggregation = Aggregation.newAggregation(
                lookupOperation,
                filterDevProfileAbsence,
                filterPreparedToRemove);
        return mongoTemplate.aggregate(aggregation, DeviceShapeLibrary.class, DeviceShapeLibrary.class)
                .getMappedResults();
    }

    @Override
    public DeviceShapeLibrary getIfActual(String deviceShapeLibraryId, StringBuilder errorMessage) {
        DeviceShapeLibrary deviceShapeLibrary = deviceShapeLibraryRepository
                .findOneByRemovedIsFalseAndId(deviceShapeLibraryId);
        if (deviceShapeLibrary == null) {
            errorMessage.append("Библиотека шейпов устройств не найдена");
            return null;
        }

        if (!deviceProfileRepository.exists(deviceShapeLibrary.getDeviceProfileId())) {
            errorMessage.append("Библиотека шейпов устройств не актуальна");
            return null;
        }

        return deviceShapeLibrary;
    }

    @Override
    public boolean add(DeviceShapeLibrary deviceShapeLibrary, StringBuilder errorMessage) {
        if (deviceShapeLibrary.getDeviceProfileId() == null || deviceShapeLibrary.getDeviceProfileId().isEmpty()) {
            errorMessage.append("Не задан тип устройства");
            return false;
        }

        if (!deviceProfileRepository.exists(deviceShapeLibrary.getDeviceProfileId())) {
            errorMessage.append("Не найден указанный профиль устройства");
            return false;
        }

        if (deviceShapeLibraryRepository.existsByDeviceProfileIdAndName(deviceShapeLibrary.getDeviceProfileId(),
                deviceShapeLibrary.getName())) {
            errorMessage.append("Заданное имя уже используется другой библиотекой того же типа устройств");
            return false;
        }

        DeviceShape defaultBasicDeviceShape = deviceShapeRepository
                .findOneByRemovedIsFalseAndDeviceShapeLibraryIdAndStateCategoryId(
                        deviceShapeLibrary.getDeviceProfileId() + DeviceShapeLibrary.DEFAULT_LIBRARY_POSTFIX, "");
        if (defaultBasicDeviceShape == null) {
            errorMessage.append("Не найден базовый шейп библиотеки по умолчанию того же типа устройств");
            return false;
        }

        /*
         * TODO: от сюда и до выхода нужно заблокировать EntityNotifier в
         * фронтенд-сервере, чтобы не было вероятности, что некоторые
         * создаваемые сущности не будут отправлены по WebSockets.
         */

        Date currentDateTime = new Date();
        deviceShapeLibrary.setCreated(currentDateTime);
        deviceShapeLibrary = deviceShapeLibraryRepository.insert(deviceShapeLibrary);

        defaultBasicDeviceShape.setId(null);
        defaultBasicDeviceShape.setDeviceShapeLibraryId(deviceShapeLibrary.getId());
        defaultBasicDeviceShape.setCreated(currentDateTime);
        deviceShapeRepository.insert(defaultBasicDeviceShape);
        return true;

    }

    @Override
    public boolean update(String deviceShapeLibraryId, DeviceShapeLibrary deviceShapeLibrary,
            StringBuilder errorMessage) {
        DeviceShapeLibrary destDeviceShapeLibrary = deviceShapeLibraryRepository
                .findOneByRemovedIsFalseAndId(deviceShapeLibraryId);
        if (destDeviceShapeLibrary == null) {
            errorMessage.append("Не найдена изменяемая библиотека шейпов устройств");
            return false;
        }

        if (!deviceProfileRepository.exists(destDeviceShapeLibrary.getDeviceProfileId())) {
            errorMessage.append("Изменяемая библиотека не актуальна");
            return false;
        }

        if (deviceShapeLibraryRepository.existsByIdNotAndDeviceProfileIdAndName(destDeviceShapeLibrary.getId(),
                destDeviceShapeLibrary.getDeviceProfileId(), deviceShapeLibrary.getName())) {
            errorMessage.append("Заданное имя уже используется другой библиотекой того же типа устройств");
            return false;
        }

        /*
         * TODO: от сюда и до выхода нужно заблокировать EntityNotifier в
         * фронтенд-сервере, чтобы не было вероятности, что некоторые
         * создаваемые сущности не будут отправлены по WebSockets.
         */

        destDeviceShapeLibrary.update(deviceShapeLibrary);
        deviceShapeLibraryRepository.save(destDeviceShapeLibrary);
        return true;
    }

    @Override
    public boolean remove(String deviceShapeLibraryId, StringBuilder errorMessage) {
        DeviceShapeLibrary deviceShapeLibrary = deviceShapeLibraryRepository
                .findOneByRemovedIsFalseAndId(deviceShapeLibraryId);
        if (deviceShapeLibrary == null) {
            errorMessage.append("Не найдена удаляемая библиотека шейпов устройств");
            return false;
        }

        if (deviceShapeLibrary.isBuiltin()) {
            errorMessage.append("Нельзя удалить библиотеку по умолчанию");
            return false;
        }
        if (!enquireObservers(deviceShapeLibraryId, errorMessage)){
            return false;
        }
        deviceShapeLibrary.setRemoved(true);
        deviceShapeLibraryRepository.save(deviceShapeLibrary);
        return true;
    }

    @Override
    public EntitiesContainer<DeviceShapeLibrary> getChangesAndDoRemove(Date from) {

        /* Заполняем изменения */
        EntitiesContainer<DeviceShapeLibrary> changes = new EntitiesContainer<>();
        changes.setCreated(deviceShapeLibraryRepository.findByRemovedIsFalseAndCreatedAfter(from));
        changes.setUpdated(deviceShapeLibraryRepository.findByRemovedIsFalseAndUpdatedAfter(from));
        List<String> removedLibraryIds = deviceShapeLibraryRepository.findByRemovedIsTrue().stream()
                .map(DeviceShapeLibrary::getId).collect(Collectors.toList());
        changes.setDeletedIds(removedLibraryIds);

        /* Отмечем к удалению шейпы удаленных библиотек */
        List<DeviceShape> deviceShapesToRemove = deviceShapeRepository.findByDeviceShapeLibraryIdIn(removedLibraryIds);
        deviceShapesToRemove.stream().forEach(deviceShape -> {
            deviceShape.setRemoved(true);
        });
        deviceShapeRepository.save(deviceShapesToRemove);
        /* Удаляем библиотеки, отмеченные на удаление */
        deviceShapeLibraryRepository.deleteByIdIn(removedLibraryIds);

        return changes;
    }

    @Override
    public void addObserver(DeviceShapeLibraryObserver deviceShapeLibraryObserver) {
        deviceShapeLibraryObservers.add(deviceShapeLibraryObserver);
    }

    @Override
    public void removeObserver(DeviceShapeLibraryObserver deviceShapeLibraryObserver) {
        deviceShapeLibraryObservers.remove(deviceShapeLibraryObserver);
    }

    @Override
    public boolean enquireObservers(String deviceShapeLibraryId, StringBuilder errorMessage) {
        for (DeviceShapeLibraryObserver deviceShapeLibraryObserver: deviceShapeLibraryObservers) {
            if (!deviceShapeLibraryObserver.handleDeviceShapeLibraryDeletion(deviceShapeLibraryId, errorMessage)){
                return false;
            }
        }
        return true;
    }

}
