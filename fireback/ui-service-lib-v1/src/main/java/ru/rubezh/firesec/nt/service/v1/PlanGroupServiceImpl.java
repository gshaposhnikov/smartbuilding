package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.PlanGroupRepository;
import ru.rubezh.firesec.nt.dao.v1.PlanRepository;
import ru.rubezh.firesec.nt.domain.v1.FilterTags;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Plan;
import ru.rubezh.firesec.nt.domain.v1.PlanGroup;

import java.util.*;

@Service
@Order(8)
public class PlanGroupServiceImpl implements PlanGroupService, ActivationFilterableService {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PlanGroupRepository planGroupRepository;

    @Override
    public PlanGroup add(String projectId, Language language, PlanGroup planGroup, StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            planGroup.setProjectId(projectId);
            return planGroupRepository.insert(planGroup);
        }

        return null;
    }

    @Override
    public PlanGroup update(String projectId, Language language, String planGroupId, PlanGroup planGroup,
            StringBuilder errorMessage) {
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            PlanGroup prevPlanGroup = planGroupRepository.findByProjectIdAndId(projectId, planGroupId);
            if (prevPlanGroup != null) {
                prevPlanGroup.update(planGroup);
                return planGroupRepository.save(prevPlanGroup);
            } else {
                errorMessage.append("Не найдена прежняя копия изменяемой группы планов");
            }
        }

        return null;
    }

    @Override
    public PlanGroupReferences remove(String projectId, Language language, String planGroupId,
            StringBuilder errorMessage) {
        PlanGroupReferences planGroupReferences = null;
        if (projectService.checkProjectIsEditable(projectId, errorMessage)) {
            PlanGroup planGroup = planGroupRepository.findByProjectIdAndId(projectId, planGroupId);
            if (planGroup == null) {
                errorMessage.append("Группа планов не найдена");
            } else {
                planGroupRepository.deleteByProjectIdAndId(projectId, planGroupId);

                /* Обновляем планы, ссылавшиеся на удаленную группу */
                List<Plan> plans = planRepository.findByProjectIdAndPlanGroupId(projectId, planGroupId);
                for (Plan plan : plans) {
                    plan.setPlanGroupId(null);
                }
                planGroupReferences = new PlanGroupReferences();
                planGroupReferences.plans = planRepository.save(plans);
            }
        }

        return planGroupReferences;
    }
    
    public boolean checkAndAdditionPlanGroupToImportProject(String projectId, List<PlanGroup> planGroups, Map<String, String> oldAndNewPlanGroupsId) {
        for (PlanGroup planGroup : planGroups) {
            planGroup.setProjectId(projectId);
            String oldPlanGroupId = planGroup.getId();
            planGroup.clearId();
            oldAndNewPlanGroupsId.put(oldPlanGroupId, planGroupRepository.insert(planGroup).getId());
        }
        return true;
    }

    @Override
    public void clearTags(String projectId) {
        List<PlanGroup> planGroups = planGroupRepository.findByProjectId(projectId);
        planGroups.forEach(planGroup -> planGroup.getFilterTags().getTags().clear());
        planGroupRepository.save(planGroups);
    }

    @Override
    public void addSubsystemTags(String projectId) {
        List<PlanGroup> planGroups = planGroupRepository.findByProjectId(projectId);
        List<Plan> plans = planRepository.findByProjectId(projectId);

        planGroups.forEach(planGroup -> {
            FilterTags filterTags = planGroup.getFilterTags();

            for (Plan plan : plans){
                if (plan.getPlanGroupId() != null && plan.getPlanGroupId().equals(planGroup.getId())){
                    filterTags.getTags().addAll(plan.getFilterTags().chooseSubsystemTags());
                }
            }
        });

        planGroupRepository.save(planGroups);
    }
}
