package ru.rubezh.firesec.nt.service.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEntities;
import ru.rubezh.firesec.nt.domain.v1.Language;
import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.domain.v1.representation.ControlDeviceEntitiesView;
import ru.rubezh.firesec.nt.domain.v1.representation.ImportedXmlProjectWrapper;
import ru.rubezh.firesec.nt.domain.v1.representation.UpdatedEntities;

import java.io.IOException;
import java.io.OutputStream;

@Service
public interface ProjectViewService {

    public interface BasicProjectEntitiesStreamSerializer {
        public void writeProject(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeDevices(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeRegions(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeScenarios(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeVirtualStates(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writePlans(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writePlanGroups(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeIndicatorPanels(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeIndicatorPanelGroups(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeEmployees(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeAccessKeys(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeWorkSchedules(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeAll(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;
    }

    public interface BuildProjectEntitiesStreamSerializer extends BasicProjectEntitiesStreamSerializer {
    }

    public interface ActiveProjectEntitiesStreamSerializer extends BasicProjectEntitiesStreamSerializer {

        public void writeActiveProject(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

        public void writeSubsystems(OutputStream outputStream, ObjectMapper objectMapper) throws IOException;

    }

    public Project add(Project project, StringBuilder errorMessage);

    public boolean remove(String projectId, StringBuilder errorMessage);

    public Project update(String projectId, Project project, StringBuilder errorMessage);

    /**
     * Получить полный набор сущностей неактивного проекта
     * 
     * @param projectId
     *            идентификатор проекта
     * @param language
     *            язык интерфейса
     * @param errorMessage
     *            сообщение, заполняемое в случае ошибки
     * @return набор сущностей проекта или null в случае ошибки
     */
    public BuildProjectEntitiesStreamSerializer getProjectEntitiesSerializer(String projectId, Language language,
            StringBuilder errorMessage);

    /**
     * Получить сериализатор полного набора сущностей активного проекта в поток
     * вывода
     * 
     * @param projectId
     *            идентификатор проекта
     * @param language
     *            язык интерфейса
     * @param errorMessage
     *            сообщение, заполняемое в случае ошибки
     * @return набор сущностей активного проекта или null в случае ошибки
     */
    public ActiveProjectEntitiesStreamSerializer getActiveProjectEntitiesSerializer(String projectId, Language language,
            StringBuilder errorMessage);

    /**
     * Сгенерировать отображение сущностей прибора
     * 
     * @param entities
     *            сущности прибора
     * @param language
     *            язык интерфейса
     * @return отображение сущностей прибора
     */
    public ControlDeviceEntitiesView getControlDeviceEntitiesView(ControlDeviceEntities entities, Language language);

    public Project addImpotredNgProjectView(String fileName, ImportedXmlProjectWrapper importedFile, StringBuilder errorMessage);

    /**
     * Добавить сущности прибора
     * 
     * @param projectId
     *            идентификатор проекта
     * @param entities
     *            сущности прибора
     * @param language
     *            язык интерфейса
     * @return отображение сущностей прибора
     */
    public UpdatedEntities addEntities(String projectId, ControlDeviceEntities entities, Language language,
            StringBuilder errorMessage);

    /**
     * Поставить метки фильтрации на обьекты
     *
     * @param projectId идентификатор проекта
     */
    void setFilterTagsOnEntities(String projectId);
}
