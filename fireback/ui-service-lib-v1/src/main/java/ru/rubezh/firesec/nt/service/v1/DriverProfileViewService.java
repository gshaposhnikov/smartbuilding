package ru.rubezh.firesec.nt.service.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rubezh.firesec.nt.dao.v1.DriverProfileViewRepository;
import ru.rubezh.firesec.nt.domain.v1.DriverProfileView;

import java.util.List;

@Service
public class DriverProfileViewService {

    @Autowired
    private DriverProfileService driverProfileService;

    @Autowired
    private DriverProfileViewRepository driverProfileViewRepository;

    @Autowired
    private DeviceProfileViewService deviceProfileViewService;

    @Autowired
    private StateService stateService;

    @Autowired
    private MonitorableValueProfileService monitorableValueProfileService;

    @Autowired
    private StateViewService stateViewService;

    @Autowired
    private MonitorableValueViewService monitorableValueViewService;

    @Autowired
    private EventViewService eventViewService;
    
    @Autowired
    private ScenarioService scenarioService;
    
    @Autowired
    private ScenarioViewService scenarioViewService;

    private void add(DriverProfileView driverProfileView) {
        driverProfileService.add(driverProfileView.getDriverProfile());
        deviceProfileViewService.add(driverProfileView.getDeviceProfileViews());
        eventViewService.addEventTypeViews(driverProfileView.getEventTypeViews());

        stateService.fetchStates(driverProfileView);
        stateViewService.fetchStateViews(driverProfileView);

        monitorableValueProfileService.fetchValues(driverProfileView);
        monitorableValueViewService.fetchValueViews(driverProfileView);
//
//        scenarioService.fetchScenarioTriggerTypes(driverProfileView.getDriverProfile());
//        scenarioService.fetchScenarioActionTypes(driverProfileView.getDriverProfile());
//        scenarioViewService.fetchScenarioTriggerTypeViews(driverProfileView);
//        scenarioViewService.fetchScenarioActionTypeViews(driverProfileView);
        
        driverProfileViewRepository.save(driverProfileView);
    }

    public void add(List<DriverProfileView> driverProfileViews) {
        /* TODO: сохранять не все, а только текущий язык */
        for (DriverProfileView driverProfileView: driverProfileViews)
            add(driverProfileView);
    }

}
