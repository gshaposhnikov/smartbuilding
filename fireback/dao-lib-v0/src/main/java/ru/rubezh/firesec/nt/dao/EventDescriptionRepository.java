package ru.rubezh.firesec.nt.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import ru.rubezh.firesec.nt.domain.v0.event.EventCode;
import ru.rubezh.firesec.nt.domain.v0.event.EventDescription;

//http://docs.spring.io/spring-data/mongodb/docs/current/reference/html

public interface EventDescriptionRepository extends MongoRepository<EventDescription, String> {

    public EventDescription findOneByEventCode(EventCode eventCode);

}
