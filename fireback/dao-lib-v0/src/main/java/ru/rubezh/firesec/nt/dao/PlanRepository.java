package ru.rubezh.firesec.nt.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import ru.rubezh.firesec.nt.domain.v0.Plan;

public interface PlanRepository extends MongoRepository<Plan, String> {

    @Query(value="{ 'adminStatus' : 'on' }")
    Plan findByAdminStatusIsOn();

    @Query(value="{ 'adminStatus' : 'off' }")
    Page<Plan> findByAdminStatusIsOff(Pageable pageable);

    public int countById(String id);
}
