package ru.rubezh.firesec.nt.dao;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v0.Device;
import ru.rubezh.firesec.nt.domain.v0.DeviceStatus;
import ru.rubezh.firesec.nt.domain.v0.FireStatus;

//http://docs.spring.io/spring-data/mongodb/docs/current/reference/html

@Repository("deviceRepository_v0")
public interface DeviceRepository extends MongoRepository<Device, String> {

    public List<Device> findByPlanId(String planId);
    public Page<Device> findByPlanIdAndRegionNo(String planId, Integer regionNo, Pageable pageable);

    @Cacheable("findByPlanIdAndParentDeviceIdPageable")
    public Page<Device> findByPlanIdAndParentDeviceId(String planId, String parentDeviceId, Pageable pageable);
    @Cacheable("findByPlanIdAndParentDeviceId")
    public List<Device> findByPlanIdAndParentDeviceId(String planId, String parentDeviceId);

    @Query("{ $and: [{parentDeviceId: {$eq: ?0}}, {lineNo: {$eq: ?1}}, {address: {$eq: ?2}}]}")
    public Device findOneByParentDeviceIdAndLineNoAndAddress(String parentDeviceId, Integer lineNo, Integer address);
    public List<Device> findByParentDeviceIdAndLineNo(String parentDeviceId, Integer lineNo);

    public int countByIdAndPlanId(String id, String planId);

    public int countByPlanIdAndDeviceStatus(String planId, DeviceStatus deviceStatus);
    public int countByPlanIdAndFireStatus(String planId, FireStatus fireStatus);
    public int countByPlanIdAndDeviceStatusAndFireStatus(String planId, DeviceStatus deviceStatus, FireStatus fireStatus);
    public int countByPlanIdAndDeviceStatusOrFireStatus(String planId, DeviceStatus deviceStatus, FireStatus fireStatus);
    public int countByPlanIdAndRegionNoAndDeviceStatus(String planId, Integer regionNo, DeviceStatus deviceStatus);
    public int countByPlanIdAndRegionNoAndFireStatus(String planId, Integer regionNo, FireStatus fireStatus);

    public int countByPlanIdAndParentDeviceId(String planId, String parentDeviceId);
}
