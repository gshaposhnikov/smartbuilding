package ru.rubezh.firesec.nt.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import ru.rubezh.firesec.nt.domain.v0.LogRecord;

public interface LogRecordRepository extends MongoRepository<LogRecord, String> {

    public Page<LogRecord> findByDateTimeAfter(Date date, Pageable pageable);
    public Page<LogRecord> findByDateTimeBetween(Date from, Date to, Pageable pageable);

    public List<LogRecord> findByRecordNoBetween(long from, long to);
    public LogRecord findTopByOrderByRecordNoDesc();
}
