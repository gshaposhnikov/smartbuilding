package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.ConnectionInterfaceType;
import ru.rubezh.firesec.nt.domain.v1.DeviceType;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;

import java.util.Collection;
import java.util.List;

@Repository
public interface DeviceProfileRepository extends MongoRepository<DeviceProfile, String> {

    DeviceProfile findById(String deviceProfileId);

    List<DeviceProfile> findByIdIn(Collection<String> deviceProfileIds);

    List<DeviceProfile> findByConnectionTypesContaining(ConnectionInterfaceType connectionInterfaceType);

    DeviceProfile findByDesktopFSId(String desktopFSId);
    
    DeviceProfile findByChangeDeviceProfile(Boolean ChangeDeviceProfile);

    List<DeviceProfile> findByIdInAndAccessPointIsTrue(List<String> ids);

    DeviceProfile findByDeviceType(DeviceType deviceType);
}
