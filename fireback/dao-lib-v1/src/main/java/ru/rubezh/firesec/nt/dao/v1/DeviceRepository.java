package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;

import java.util.List;

@Repository
public interface DeviceRepository extends MongoRepository<Device, String> {

    public List<Device> findByProjectIdAndAddressPathRegex(String projectId, String addressPathMask);

    public List<Device> findByProjectId(String projectId);

    public List<Device> findByProjectIdAndParentDeviceId(String projectId, String parentDeviceId);

    public List<Device> findByProjectIdAndParentDeviceIdAndLineNoOrderByLineAddressAsc(String projectId,
            String parentDeviceId, int lineNo);

    public List<Device> findByProjectIdAndParentDeviceIdAndLineNoAndDeviceCategory(String projectId,
            String parentDeviceId, int lineNo, DeviceCategory deviceCategory);

    public Device findByProjectIdAndId(String projectId, String id);

    public List<Device> findByProjectIdAndRegionId(String projectId, String regionId);

    public List<Device> findByProjectIdAndIdInAndDisabled(String projectId, List<String> deviceIds, boolean disabled);

    public List<Device> findByProjectIdAndIdIn(String projectId, List<String> deviceIds);

    public int deleteByProjectId(String projectId);

    public void deleteByProjectIdAndIdIn(String projectId, List<String> removedDeviceIds);

    public Device findByProjectIdAndParentDeviceIdAndLineNoAndLineAddress(String projectId, String parentDeviceId, int lineNo, int lineAddress);

    public long countByProjectIdAndIdInAndDisabled(String projectId, List<String> ids, boolean disabled);

    public int countByProjectIdAndParentDeviceId(String projectId, String parentDeviceId);

    public int countByProjectIdAndAddressPath(String projectId, String addressPath);

    public int countByProjectIdAndAddressPathRegexAndDeviceCategoryNot(String projectId, String addressPathMask,
            DeviceCategory exceptDeviceCategory);

    public boolean existsByProjectIdAndDeviceCategoryAndId(String projectId, DeviceCategory deviceCategory, String id);

    public boolean existsByIdAndDisabled(String id, boolean disabled);
    
    public List<Device> findAllByProjectIdAndIdIn(String projectId, List<String> entityIds);

    public Device findByProjectIdAndIdAndDisabled(String projectId, String entityId, boolean disabled);

    public List<Device> findByProjectIdAndParentDeviceIdAndDisabled(String projectId, String parentDeviceId, boolean disabled);
    
    public Device findByProjectIdAndAddressPath(String projectId, String addressPath);

    List<Device> findByProjectIdAndDisabledIsFalse(String projectId, Pageable pageable);

    long countByProjectIdAndDisabledIsFalse(String projectId);

    Device findByRegionIdAndDeviceProfileIdAndLineAddress(String regionId, String deviceProfileId, int lineAddress);
}
