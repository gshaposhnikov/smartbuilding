package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.ActiveRegion;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface ActiveRegionRepository extends MongoRepository<ActiveRegion, String> {
    
    List<ActiveRegion> findByProjectId(String projectId);

    ActiveRegion findByProjectIdAndId(String projectId, String regionId);

    List<ActiveRegion> findAllByProjectIdAndIndexIn(String projectId, List<Integer> indexes);

    List<ActiveRegion> findByProjectIdAndIdIn(String projectId, Collection<String> ids);

    boolean existsAllByIdIn(List<String> regionIds);

    boolean existsByIdAndOnGuardIsTrue(String regionId);

    int countByProjectId(String projectId);

    List<ActiveRegion> findByProjectId(String projectId, Pageable pageable);

    int countByStateGetDateTimeAfter(Date lastObservedTime);

    List<ActiveRegion> findByIndexIn(Set<Integer> indexes);

    ActiveRegion findByIndex(int regionNo);
}
