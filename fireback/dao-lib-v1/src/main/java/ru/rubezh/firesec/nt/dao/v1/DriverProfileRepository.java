package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DriverProfile;

@Repository
public interface DriverProfileRepository extends MongoRepository<DriverProfile, String> {

    public int countByDeviceProfileIdsContaining(String deviceProfileId);

    public DriverProfile findOneByDeviceProfileIdsContaining(String deviceProfileId);

}
