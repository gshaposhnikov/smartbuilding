package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.Scenario;

import java.util.List;
import java.util.Collection;

@Repository
public interface ScenarioRepository extends MongoRepository<Scenario, String> {

    List<Scenario> findByProjectId(String projectId);

    List<Scenario> findByProjectIdAndEnabledIsTrue(String projectId, Pageable pageable);

    Scenario findByProjectIdAndId(String projectId, String id);

    List<Scenario> findByProjectIdAndIdIn(String projectId, Collection<String> ids);

    Scenario findTopByProjectIdOrderByGlobalNoDesc(String projectId);

    void deleteByProjectId(String projectId);

    Long deleteById(String id);

    List<Scenario> findAllByIdIn(List<String> ids);

    boolean existsByProjectIdAndId(String projectId, String id);

    void deleteByProjectIdAndIdIn(String projectId, Collection<String> ids);

    boolean existsAllByIdIn(List<String> entityIds);

    long countByProjectIdAndEnabledIsTrue(String projectId);
}
