package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@Repository
public interface EmployeeRepository extends ProjectEntityRepository<Employee> {

    Employee findTopByProjectIdOrderByIndexDesc(String projectId);

    boolean existsByProjectIdAndIdAndRemovedIsFalse(String projectId, String id);

    List<Employee> findByProjectIdAndAccessMap_AccessPointDeviceIdsAndRemovedIsFalse(String projectId, String deviceId);

    List<Employee> findByProjectIdAndAccessMap_WorkRegionIdsAndRemovedIsFalse(String projectId, String regionId);

    List<Employee> findByProjectIdAndAccessMap_WorkScheduleIdAndRemovedIsFalse(String projectId, String workScheduleId);

    List<Employee> findAllByIdInAndRemovedIsFalse(List<String> employeeIds);

    List<Employee> findAllByProjectIdAndIndexInAndRemovedIsFalse(String id, List<Integer> employeeIds);
}
