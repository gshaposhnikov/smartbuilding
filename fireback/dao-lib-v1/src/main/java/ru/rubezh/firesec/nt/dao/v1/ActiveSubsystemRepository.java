package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ActiveSubsystem;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;

@Repository
public interface ActiveSubsystemRepository extends MongoRepository<ActiveSubsystem, String> {

    public ActiveSubsystem findOneBySubsystem(Subsystem subsystem);

    public List<ActiveSubsystem> findByStateGetDateTimeAfter(Date date);

}
