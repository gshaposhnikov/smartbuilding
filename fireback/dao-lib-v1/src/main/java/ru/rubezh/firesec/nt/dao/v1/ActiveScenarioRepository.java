package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.ActiveScenario;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
public interface ActiveScenarioRepository extends MongoRepository<ActiveScenario, String> {

    public List<ActiveScenario> findByProjectId(String projectId);

    public ActiveScenario findByProjectIdAndId(String projectId, String id);

    public List<ActiveScenario> findByProjectIdAndIdIn(String projectId, Collection<String> ids);

    public List<ActiveScenario> findAllByProjectIdAndGlobalNoIn(String projectId, List<Integer> globalNos);

    ActiveScenario findByProjectIdAndGlobalNo(String projectId, int scenarioNo);

    int countByProjectId(String projectId);

    int countByStateGetDateTimeAfter(Date lastObservedTime);
}
