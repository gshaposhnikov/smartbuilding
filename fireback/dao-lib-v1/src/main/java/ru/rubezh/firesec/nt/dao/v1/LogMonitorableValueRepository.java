package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.LogMonitorableValue;

import java.util.Date;

@Repository
public interface LogMonitorableValueRepository extends MongoRepository<LogMonitorableValue, String> {

    LogMonitorableValue findFirstByProjectIdAndDeviceIdAndProfileIdOrderByReceivedDesc(
            String projectId, String deviceId, String profileId);

    Page<LogMonitorableValue> findAllByProjectIdAndDeviceIdAndReceivedBetweenOrderByReceivedAsc(
            String projectId, String deviceId, Date from, Date to, Pageable pageable);

    void deleteAllByReceivedBefore(Date received);
}
