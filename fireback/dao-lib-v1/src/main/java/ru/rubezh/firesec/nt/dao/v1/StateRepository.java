package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.State;

import java.util.Collection;
import java.util.List;

@Repository
public interface StateRepository extends MongoRepository<State, String> {

    List<State> findByIdIn(Collection<String> ids);
    
    State findByDesktopStateName(String desktopStateName);

    State findByOffStateIsTrue();

    State findByNumber(int number);
}
