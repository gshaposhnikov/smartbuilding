package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanelGroup;

import java.util.List;
import java.util.Set;

public interface IndicatorPanelGroupRepository extends MongoRepository<IndicatorPanelGroup, String> {
    List<IndicatorPanelGroup> findByProjectId(String projectId);

    IndicatorPanelGroup findByProjectIdAndId(String projectId, String id);

    Long deleteByProjectIdAndId(String projectId, String id);

    void deleteByProjectId(String projectId);

    List<IndicatorPanelGroup> findByProjectIdAndIdIn(String projectId, Set<String> ids);
}
