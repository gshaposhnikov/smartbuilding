package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEvent;

@Repository
public interface ControlDeviceEventRepository extends MongoRepository<ControlDeviceEvent, String> {

    List<ControlDeviceEvent> findFirst100ByOrderByIdAsc();

}
