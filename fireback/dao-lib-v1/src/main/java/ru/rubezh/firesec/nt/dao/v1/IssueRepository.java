package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.Issue;
import ru.rubezh.firesec.nt.domain.v1.IssueStatus;

import java.util.Date;
import java.util.List;

@Repository
public interface IssueRepository extends MongoRepository<Issue, String> {

    List<Issue> findAllByOrderByCreateDateTimeDesc();

    void removeByStatusNotInAndFinishDateTimeBefore(IssueStatus status, Date finishDateTime);

    List<Issue> deleteByStatus(IssueStatus status);

    List<Issue> deleteByIdIn(List<String> ids);

    List<Issue> deleteByProjectId(String projectId);

    List<Issue> findByProjectIdAndStatusIn(String projectId, List<IssueStatus> statuses);
}
