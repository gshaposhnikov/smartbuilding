package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rubezh.firesec.nt.domain.v1.IndicatorPanel;

import java.util.List;

public interface IndicatorPanelRepository extends MongoRepository<IndicatorPanel, String> {

    List<IndicatorPanel> findByProjectId(String projectId);

    IndicatorPanel findByProjectIdAndId(String projectId, String id);

    Long deleteByProjectIdAndId(String projectId, String id);

    List<IndicatorPanel> deleteByIndicatorGroupId(String indicatorGroupId);

    void deleteByProjectId(String projectId);
}
