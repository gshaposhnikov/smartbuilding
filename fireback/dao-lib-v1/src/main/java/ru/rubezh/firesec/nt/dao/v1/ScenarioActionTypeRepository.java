package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;

import java.util.List;
import java.util.Collection;

@Repository
public interface ScenarioActionTypeRepository extends MongoRepository<ScenarioActionType, String> {

    List<ScenarioActionType> findAllByOrderByIdAsc();

    List<ScenarioActionType> findByIdIn(Collection<String> ids);
    
    ScenarioActionType findByDesktopActionNumberAndEntityType(String desktopActionNumber, ActionEntityType entityType);
}
