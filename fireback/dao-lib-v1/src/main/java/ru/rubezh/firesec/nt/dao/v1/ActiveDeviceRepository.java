package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface ActiveDeviceRepository extends MongoRepository<ActiveDevice, String> {

    List<ActiveDevice> findByProjectId(String projectId);

    ActiveDevice findOneByProjectIdAndId(String projectId, String id);

    ActiveDevice findByProjectIdAndAddressPath(String projectId, String addressPath);

    ActiveDevice findOneByAddressPath(String addressPath);

    List<ActiveDevice> findByAddressPathIn(Collection<String> paths);

    List<ActiveDevice> findByIdIn(Collection<String> ids);

    List<ActiveDevice> findByProjectIdAndRegionId(String projectId, String regionId);

    List<ActiveDevice> findByProjectIdAndRegionIdIn(String projectId, Collection<String> regionIds);

    List<ActiveDevice> findByProjectIdAndParentDeviceId(String projectId, String parentDeviceId);

    ActiveDevice findByProjectIdAndId(String projectId, String deviceId);

    long countByProjectId(String projectId);

    List<ActiveDevice> findAllByProjectIdAndIdIn(String projectId, Iterable<String> ids);

    boolean existsAllByIdIn(List<String> entityIds);

    List<ActiveDevice> findByProjectIdAndStateGetDateTimeAfter(String projectId, Date from);

    List<ActiveDevice> findByProjectIdAndRegionIdInAndIdNotIn(String projectId, Collection<String> regionIds,
            Collection<String> deviceIds);

    List<ActiveDevice> findByProjectId(String projectId, Pageable pageable);

    int countByStateGetDateTimeAfter(Date lastObservedTime);

    List<ActiveDevice> findByRegionIdIn(Set<String> offStateRegionIds);
}
