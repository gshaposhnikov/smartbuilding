package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.Region;

import java.util.Collection;
import java.util.List;

@Repository
public interface RegionRepository extends MongoRepository<Region, String> {

    public List<Region> findByProjectId(String projectId);

    public Region findByProjectIdAndId(String projectId, String id);

    public void deleteByProjectId(String projectId);

    public void deleteByProjectIdAndId(String projectId, String id);

    public void deleteByProjectIdAndIdIn(String projectId, List<String> ids);

    public Long countByProjectIdAndIndexAndIdNot(String projectId, int index, String id);
    
    public long countByProjectIdAndIdIn(String projectId, List<String> ids);
    
    public List<Region> findByProjectIdAndIdIn(String projectId, Collection<String> ids);

    public List<Region> findAllByIdIn(List<String> ids);

    List<Region> findAllByProjectIdAndIdIn(String projectId, List<String> entityIds);

    boolean existsByProjectIdAndId(String projectId, String entityId);

    List<Region> findByProjectId(String projectId, Pageable pageable);

    long countByProjectId(String projectId);
}
