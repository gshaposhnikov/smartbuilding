package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.EventProfile;

@Repository
public interface EventProfileRepository extends MongoRepository<EventProfile, String> {

    public List<EventProfile> findByIdIn(List<String> ids);

}
