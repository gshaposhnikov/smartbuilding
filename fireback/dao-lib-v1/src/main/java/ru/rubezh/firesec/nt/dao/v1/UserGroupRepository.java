package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.UserGroup;

@Repository
public interface UserGroupRepository extends MongoRepository<UserGroup, String> {

    List<UserGroup> findByRemovedIsFalseAndIdIn(List<String> ids);

    List<UserGroup> findByRemovedIsFalseAndUpdateDateTimeAfter(Date updateDateTime);

    List<UserGroup> findByRemovedIsFalseAndCreateDateTimeAfter(Date createDateTime);

    List<UserGroup> findByRemovedIsTrue();

    void deleteByIdIn(List<String> ids);

    List<UserGroup> findAllByRemovedIsFalse();

    UserGroup findByRemovedIsFalseAndId(String id);

    int countAllByIdIn(List<String> ids);
}
