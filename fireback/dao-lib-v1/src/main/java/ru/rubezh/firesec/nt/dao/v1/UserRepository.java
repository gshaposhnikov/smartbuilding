package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findByName(String name);

    boolean existsByUserGroupIdsContaining(String groupId);

    List<User> findByUpdateDateTimeAfter(Date updateDateTime);

    int countAllByIdIn(List<String> ids);
}
