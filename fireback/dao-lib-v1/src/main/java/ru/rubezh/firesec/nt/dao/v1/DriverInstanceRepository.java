package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DriverInstance;

@Repository
public interface DriverInstanceRepository extends MongoRepository<DriverInstance, String> {

    public DriverInstance findOneByProfileId(String profileId);

}
