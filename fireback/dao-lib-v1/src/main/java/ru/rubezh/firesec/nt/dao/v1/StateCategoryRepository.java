package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.StateCategory;

@Repository
public interface StateCategoryRepository extends MongoRepository<StateCategory, String> {
    /** Получить класс состояния по умолчанию */
    StateCategory findOneByDefaultCategoryIsTrue();
}
