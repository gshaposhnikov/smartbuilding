package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.DecodableEventStateSet;

@Repository
public interface DecodableEventStateSetRepository extends MongoRepository<DecodableEventStateSet, String> {

    public List<DecodableEventStateSet> findByIdIn(List<String> ids);

}
