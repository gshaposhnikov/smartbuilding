package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.EntityObserveMutex;

@Repository
public interface EntityObserveMutexRepository extends MongoRepository<EntityObserveMutex, String> {

    void deleteByIdAndLockedIsTrueAndLockExpireBefore(String id, Date lockExpire);

}
