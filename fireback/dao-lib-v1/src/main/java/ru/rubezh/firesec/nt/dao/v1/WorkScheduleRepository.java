package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;

@Repository
public interface WorkScheduleRepository extends ProjectEntityRepository<WorkSchedule> {

    long countByProjectIdAndIdAndRemovedIsFalse(String projectId, String id);

    WorkSchedule findTopByProjectIdOrderByIndexDesc(String projectId);

    WorkSchedule findTopByProjectIdOrderByDays_IndexDesc(String projectId);

    WorkSchedule findTopByProjectIdOrderByDays_AccessGrantedTimes_IndexDesc(String projectId);

}
