package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType;

import java.util.Collection;
import java.util.List;

@Repository
public interface ScenarioTriggerTypeRepository extends MongoRepository<ScenarioTriggerType, String> {

    List<ScenarioTriggerType> findAllByOrderByIdAsc();

    List<ScenarioTriggerType> findByIdIn(Collection<String> ids);
    
    ScenarioTriggerType findByDesktopTriggerTypeName(String desktopTriggerTypeName);
}
