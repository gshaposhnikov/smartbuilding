package ru.rubezh.firesec.nt.dao.v1;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.Event;

@Repository
public interface EventRepository extends MongoRepository<Event, String> {

    List<Event> findFirst100ByIdAfterOrderByIdAsc(ObjectId id);

    List<Event> findByControlDeviceIdInAndRecordNoIn(Collection<String> controlDeviceId, Collection<Long> recordNos);

    /* Нужен Query, чтобы задать OR */
    @Query("{_id: {$gt: ?0}, online: true, $or: [{deviceId: {$ne: null, $ne: \"\"}}, {regionId: {$ne: null, $ne: \"\"}}, {controlDeviceStateIds: {$ne: null, $not: {$size: 0}}}]}")
    List<Event> findByIdAfterAndOnlineIsTrueAnd_DeviceIdOrRegionIdOrControlDeviceStateIdsNotEmpty(ObjectId from,
            Sort sort);

    /* Нужен Query, чтобы проверить scenarioId и на null, и на пустую строку */
    @Query("{_id: {$gt: ?0}, online: true, scenarioId: {$ne: null, $ne: \"\"}}")
    List<Event> findByIdAfterAndOnlineIsTrueAndScenarioIdNotNull(ObjectId from, Sort sort);

    List<Event> findByIdAfterAndOnlineIsTrueAndFireExtinctionEventNotNull(ObjectId from, Sort sort);

}
