package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.domain.v1.ProjectStatus;

@Repository
public interface ProjectRepository extends MongoRepository<Project, String> {

    public List<Project> findByStatus(ProjectStatus status);

    public Project findOneByStatus(ProjectStatus status);

    public boolean existsByNameAndVersion(String name, String version);

    public boolean existsByNameAndVersionAndIdNot(String name, String version, String Id);
    
    public long countByNameAndVersion(String name, String version);
    
    public long deleteByIdNotIn(Iterable<String> ids);

}
