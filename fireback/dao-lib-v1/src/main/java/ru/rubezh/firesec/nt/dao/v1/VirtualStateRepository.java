package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.rubezh.firesec.nt.domain.v1.VirtualState;

import java.util.Collection;
import java.util.List;

@Repository
public interface VirtualStateRepository extends MongoRepository<VirtualState, String> {

    VirtualState findByIdAndProjectId(String id, String projectId);

    List<VirtualState> findAllByProjectIdAndGlobalNoIn(String projectId, List<Integer> nos);

    Long deleteByIdAndProjectId(String id, String projectId);

    void deleteByProjectId(String projectId);

    void deleteByProjectIdAndIdIn(String projectId, List<String> ids);

    List<VirtualState> findByProjectId(String projectId);

    List<VirtualState> findByProjectIdAndIdIn(String projectId, Collection<String> ids);
    
    long countByProjectIdAndIdIn(String projectId, List<String> ids);

    List<VirtualState> findAllByIdIn(List<String> ids);

    Integer countByProjectIdAndGlobalNoAndIdNot(String projectId, int globalNo, String id);
}
