package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

@Repository
public interface AccessKeyRepository extends ProjectEntityRepository<AccessKey> {

    List<AccessKey> findByProjectIdAndEmployeeIdAndRemovedIsFalse(String projectId, String employeeId);

    AccessKey findTopByProjectIdAndKeyTypeOrderByIndexDesc(String projectId,
            AccessKey.KeyType keyType);

    boolean existsByProjectIdAndKeyTypeAndKeyValue(String projectId, AccessKey.KeyType keyType, String keyValue);

    boolean existsByProjectIdAndIdNotAndKeyTypeAndKeyValue(String projectId, String id, AccessKey.KeyType keyType,
            String keyValue);

    List<AccessKey> findByProjectIdAndAccessMap_AccessPointDeviceIdsAndRemovedIsFalse(String projectId,
            String deviceId);

    List<AccessKey> findByProjectIdAndAccessMap_WorkRegionIdsAndRemovedIsFalse(String projectId, String regionId);

    List<AccessKey> findByProjectIdAndAccessMap_WorkScheduleIdAndRemovedIsFalse(String projectId,
            String workScheduleId);

    List<AccessKey> findByProjectIdAndEmployeeId(String projectId, String employeeId);

    List<Employee> findByProjectId(String projectId);
}
