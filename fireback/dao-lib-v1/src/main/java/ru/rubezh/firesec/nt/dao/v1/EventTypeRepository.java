package ru.rubezh.firesec.nt.dao.v1;

import java.util.Collection;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.EventType;
import ru.rubezh.firesec.nt.domain.v1.IssueAction;

@Repository
public interface EventTypeRepository extends MongoRepository<EventType, String> {

    List<EventType> findByIdIn(List<String> ids);

    List<EventType> findAllByIdIn(Collection<String> ids);

    List<EventType> findByIssueAction(IssueAction issueAction);

    List<EventType> findByPermissionId(String permissionId);

    EventType findOneByDeviceActionKeyAndIssueAction(String deviceActionKey, IssueAction issueAction);
}
