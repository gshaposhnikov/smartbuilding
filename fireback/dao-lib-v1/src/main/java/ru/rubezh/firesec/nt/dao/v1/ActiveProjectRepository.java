package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.ActiveProject;

@Repository
public interface ActiveProjectRepository extends MongoRepository<ActiveProject, String> {

    List<ActiveProject> findByStateGetDateTimeAfter(Date date);

    ActiveProject findOneByOrderByStateGetDateTimeAsc();

}
