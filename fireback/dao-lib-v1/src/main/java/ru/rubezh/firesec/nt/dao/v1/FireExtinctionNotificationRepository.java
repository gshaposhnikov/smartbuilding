package ru.rubezh.firesec.nt.dao.v1;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.FireExtinctionNotification;

@Repository
public interface FireExtinctionNotificationRepository extends MongoRepository<FireExtinctionNotification, String> {

    /* Нужен Query, потому что генерируемый between ищет с $lt */
    @Query("{_id: {$gt: ?0, $lte: ?1}}")
    List<FireExtinctionNotification> findByIdBetween(ObjectId from, ObjectId to, Sort sort);

}
