package ru.rubezh.firesec.nt.dao.v1;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import ru.rubezh.firesec.nt.domain.v1.BasicEntityWithDate;

@NoRepositoryBean
public interface ProjectEntityRepository<T extends BasicEntityWithDate> extends MongoRepository<T, String> {

    List<T> findByProjectIdAndRemovedIsFalse(String projectId);

    T findByProjectIdAndIdAndRemovedIsFalse(String projectId, String id);

    List<T> findByRemovedIsFalseAndCreateDateTimeBetween(Date from, Date upTo);

    List<T> findByRemovedIsFalseAndUpdateDateTimeBetween(Date from, Date upTo);

    List<T> findByRemovedIsTrueAndUpdateDateTimeBetween(Date from, Date upTo);

    void deleteByRemovedIsTrueAndUpdateDateTimeBefore(Date upTo);

    void deleteByRemovedIsTrueAndProjectIdAndUpdateDateTimeBefore(String projectId, Date upTo);

    void deleteByProjectId(String projectId);
}
