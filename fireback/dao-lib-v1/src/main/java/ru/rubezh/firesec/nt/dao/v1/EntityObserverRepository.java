package ru.rubezh.firesec.nt.dao.v1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import ru.rubezh.firesec.nt.domain.v1.EntityObserver;

@Repository
public interface EntityObserverRepository extends MongoRepository<EntityObserver, String> {
    public void deleteByIdRegex(String idRegex);
}
