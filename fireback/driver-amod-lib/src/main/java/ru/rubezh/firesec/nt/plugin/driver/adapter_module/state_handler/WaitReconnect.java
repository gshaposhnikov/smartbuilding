package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;

public class WaitReconnect extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.WAIT_RECONNECT;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        WaitReconnect handler = new WaitReconnect();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }
    
    @Override
    public State onTimer(long currentTimeMs) {
        stateMachineContext.getActionExecutor().connect(currentTimeMs);
        return State.CONNECTING;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetTimer(currentTimeMs);
        return State.IDLE;
    }
}
