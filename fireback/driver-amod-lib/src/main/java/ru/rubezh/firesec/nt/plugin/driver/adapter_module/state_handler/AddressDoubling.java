package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;

public class AddressDoubling extends Ready {

    @Override
    public State getCurrentState() {
        return State.ADDRESS_DOUBLING;
    }

    @Override
    public State onAddressDoublingFlagsReceived(long currentTimeMs, boolean[] addressDoublingFlags) {
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getPingInterval());
        if (!addressDoublingFlags[stateMachineContext.getLineNo()]) {
            stateMachineContext.getStateMachineHooks().onRS485AddressDoublingFixed(currentTimeMs, stateMachineContext.getLineNo());
            return State.READY;
        }
        else
            return getCurrentState();
    }
}
