package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import java.util.EnumMap;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;

public class StateRelay implements StateHandler {
    StateMachineContext stateMachineContext;

    State state = State.IDLE;
    private EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);

    private State setState(State state) {
        if (state != this.state) {
            stateMachineContext.getLogger().info("Adapter module (address path: {}) state transition: {} -> {}",
                    stateMachineContext.getAdapterModuleDevice().getAddressPath(),
                    this.state, state);
            this.state = state;
        }
        return this.state;
    }

    public StateRelay(StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
        for (State state: State.values()) {
            if (state != State.NONE)
                handlersByState.put(state, State.getHandler(state).cloneBinded(stateMachineContext));
        }
    }

    @Override
    public State getCurrentState() {
        return state;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        return new StateRelay(stateMachineContext);
    }

    @Override
    public boolean isConnected() {
        return handlersByState.get(state).isConnected();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnectRequest(currentTimeMs));
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDisconnectRequest(currentTimeMs));
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnected(currentTimeMs));
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConnectFailure(currentTimeMs));
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onDisconnected(currentTimeMs));
    }

    @Override
    public State onConfigReceived(long currentTimeMs, RS485Configuration config) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onConfigReceived(currentTimeMs, config));
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onExceptionReceived(currentTimeMs, exception));
    }

    @Override
    public State onSetConfigConfirmed(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onSetConfigConfirmed(currentTimeMs));
    }

    @Override
    public State onAddressDoublingFlagsReceived(long currentTimeMs, boolean[] addressDoublingFlags) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onAddressDoublingFlagsReceived(currentTimeMs, addressDoublingFlags));
    }

    @Override
    public State onExtendedStatusEnabled(long currentTimeMs) {
        stateMachineContext.getLogger().debug("Enter");
        return setState(handlersByState.get(state).onExtendedStatusEnabled(currentTimeMs));
    }

    @Override
    public State onTimer(long currentTimeMs) {
        stateMachineContext.getLogger().traceEntry();
        return setState(handlersByState.get(state).onTimer(currentTimeMs));
    }

}
