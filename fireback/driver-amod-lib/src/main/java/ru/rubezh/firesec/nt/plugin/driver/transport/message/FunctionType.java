package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum FunctionType {
    GET_PARAMETER(0x01),
    SET_PARAMETER(0x02);
    
    private final int value;
    private FunctionType(int value) {
        this.value = value;
    }

    private static final Map<Integer, FunctionType> typesByValue = new HashMap<>();
    static {
        for (FunctionType type : FunctionType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<FunctionType, Integer> valuesByType = new EnumMap<>(FunctionType.class);
    static {
        for (FunctionType type : FunctionType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static FunctionType fromInteger(int value) {
        return typesByValue.get(value);
    }
    
    public static Integer toInteger(FunctionType type) {
        return valuesByType.get(type);
    }
}
