package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class GetRS485AddressesDoublingFlagsRS extends CommonRS {
    private boolean[] isDoubling = {false, false};
    
    public boolean[] getDoublingFlags() {
        return isDoubling;
    }

    public boolean isDoubling(int lineNo) {
        return isDoubling[lineNo];
    }
    
    public void setIsDoubling(int lineNo, boolean isDoubling) {
        assert lineNo < this.isDoubling.length;
        
        this.isDoubling[lineNo] = isDoubling;
    }
    
    public GetRS485AddressesDoublingFlagsRS() {
        super(FunctionType.GET_PARAMETER);
    }
}
