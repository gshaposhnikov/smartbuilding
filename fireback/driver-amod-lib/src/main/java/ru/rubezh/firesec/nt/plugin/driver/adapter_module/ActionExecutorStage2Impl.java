package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class ActionExecutorStage2Impl extends ActionExecutorStage1Impl {

    abstract protected void responseGetRS485ConfigReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData);
    abstract protected void responseSetRS485ConfigReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData);
    abstract protected void responseGetRS485AddressesDoublingFlagsReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData);
    abstract protected void responseEnableExtendedStatusReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData);

    public ActionExecutorStage2Impl(StateMachineContext stateMachineContext) {
        super(stateMachineContext);
    }

    @Override
    protected void connected(long currentTimeMs, boolean isSuccess) {
        if (isSuccess)
            stateMachineContext.getStateHandler().onConnected(currentTimeMs);
        else
            stateMachineContext.getStateHandler().onConnectFailure(currentTimeMs);
    }

    @Override
    protected void disconnected(long currentTimeMs) {
        stateMachineContext.getLogger().warn("Disconnected, adapter module address path: {}",
                stateMachineContext.getAdapterModuleDevice().getAddressPath());
        stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
    }

    @Override
    protected void sendReceiveFailed(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getLogger().warn("Send/receive failed, adapter module address path: {}",
                stateMachineContext.getAdapterModuleDevice().getAddressPath());
        /*
         * В случае ошибки приемо-передачи необходимо принудительно порвать связь на транспортном уровне, чтобы начать
         * процедуру переподключения
         */
        try {
            stateMachineContext.getTransport().disconnect(currentTimeMs);
        } catch (IOException e) {
            stateMachineContext.getLogger().error("IO exception with message: {}", e.getMessage());
        }
        stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
    }

    @Override
    protected void requestGetRS485ConfigSended(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getTransport().receive(currentTimeMs, sequenceNo, this::responseGetRS485ConfigReceived, this::sendReceiveFailed);
    }

    @Override
    protected void requestSetRS485ConfigSended(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getTransport().receive(currentTimeMs, sequenceNo, this::responseSetRS485ConfigReceived, this::sendReceiveFailed);
    }

    @Override
    protected void timerHandler(long currentTimeMs) {
        stateMachineContext.getStateHandler().onTimer(currentTimeMs);
    }

    @Override
    protected void requestGetRS485AddressesDoublingFlagsSended(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getTransport().receive(currentTimeMs, sequenceNo, this::responseGetRS485AddressesDoublingFlagsReceived, this::sendReceiveFailed);
    }

    @Override
    protected void requestEnableExtendedStatusSended(long currentTimeMs, int sequenceNo) {
        stateMachineContext.getTransport().receive(currentTimeMs, sequenceNo,
                this::responseEnableExtendedStatusReceived, this::sendReceiveFailed);
    }

}
