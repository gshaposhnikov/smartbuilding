package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hid4java.HidDevice;
import org.hid4java.HidServices;

/**
 * Сканер HID-устройств.
 * 
 * Hid4Java имеет свой сканер, однако он корректно работает только через подписку.
 * 
 * @author Антон Васильев
 *
 */
public class HidDeviceScanner {

    HidServices hidServices;

    Map<String, HidDevice> hidDevicesByIds = new HashMap<>();

    public HidDeviceScanner(HidServices hidServices) {
        hidServices.stop();
        this.hidServices = hidServices;
    }

    public synchronized void scan() {
        List<HidDevice> devices = hidServices.getAttachedHidDevices();
        for (HidDevice hidDevice : devices) {
            if (!hidDevicesByIds.containsKey(hidDevice.getId())) {
                hidDevicesByIds.put(hidDevice.getId(), hidDevice);
            }
        }
        List<String> deviceIdsToRemove = new ArrayList<>();
        for (Map.Entry<String, HidDevice> entry : hidDevicesByIds.entrySet()) {
            if (!devices.contains(entry.getValue()))
                deviceIdsToRemove.add(entry.getKey());
        }
        if (!deviceIdsToRemove.isEmpty())
            hidDevicesByIds.keySet().removeAll(deviceIdsToRemove);
    }

    public synchronized List<HidDevice> getAttachedHidDevices() {
        return new ArrayList<>(hidDevicesByIds.values());
    }

}
