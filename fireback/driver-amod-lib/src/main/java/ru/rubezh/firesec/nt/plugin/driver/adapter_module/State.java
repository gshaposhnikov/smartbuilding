package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.util.EnumMap;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler.*;

//TODO: добавить состояния для поддержки более 1-ого интерфейса RS485
public enum State {
    NONE(null),
    IDLE(new Idle()),
    FIRST_TIME_CONNECTING(new FirstTimeConnecting()),
    CONNECTING(new Connecting()),
    ADJUSTMENT(new Adjustment()),
    WAIT_RECONNECT(new WaitReconnect()),
    WAIT_CONFIG(new WaitConfig()),
    SETTING_CONFIG(new SettingConfig()),
    WAIT_RECONFIG(new WaitReconfig()),
    READY(new Ready()),
    ADDRESS_DOUBLING(new AddressDoubling());
    
    private final StateHandler handler;
    private State(StateHandler handler) {
        this.handler = handler;
    }

    private static final EnumMap<State, StateHandler> handlersByState = new EnumMap<>(State.class);
    static {
        for (State state : State.values()) {
            handlersByState.put(state, state.handler);
        }
    }

    public static StateHandler getHandler(State state) {
        return handlersByState.get(state);
    }
}
