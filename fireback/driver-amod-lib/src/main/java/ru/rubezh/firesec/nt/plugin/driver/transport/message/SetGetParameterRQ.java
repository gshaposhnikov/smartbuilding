package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public interface SetGetParameterRQ extends Message {
    public ParameterType getParameterType();
}
