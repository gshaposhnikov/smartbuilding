package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum ExceptionType {
    NONE(0x00),
    ILLEGAL_FUNCTION(0x01),
    ILLEGAL_DATA_ADDRESS(0x02),
    ILLEGAL_DATA_VALUE(0x03),
    FAILURE_IN_ASSOCIATED_DEVICE(0x04),
    ACKNOWLEDGE(0x05),
    SLAVE_DEVICE_BUSY(0x06),
    NAK_NEGATIVE_ACKNOWLEDGMENT(0x07),
    DENIAL_OF_SERVICE(0x08),
    INVALID_DEVICE_REPLY(0x09),
    FAILURE_WRITE_RSR_DEVICE(0x0A);

    private final int value;

    private ExceptionType(int value) {
        this.value = value;
    }

    private static final Map<Integer, ExceptionType> typesByValue = new HashMap<>();
    static {
        for (ExceptionType type : ExceptionType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<ExceptionType, Integer> valuesByType = new EnumMap<>(ExceptionType.class);
    static {
        for (ExceptionType type : ExceptionType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static ExceptionType fromInteger(int value) {
        return typesByValue.get(value);
    }

    public static Integer toInteger(ExceptionType type) {
        return valuesByType.get(type);
    }
}
