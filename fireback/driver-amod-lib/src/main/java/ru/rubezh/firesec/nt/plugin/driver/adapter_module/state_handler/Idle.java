package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;

public class Idle extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.IDLE;
    }
    
    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        Idle handler = new Idle();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().connect(currentTimeMs);
        return State.FIRST_TIME_CONNECTING;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        return State.IDLE;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return getCurrentState();
    }

}
