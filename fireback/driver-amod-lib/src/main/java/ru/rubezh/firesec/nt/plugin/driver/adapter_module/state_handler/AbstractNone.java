package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;

abstract public class AbstractNone implements StateHandler {

    protected StateMachineContext stateMachineContext;

    @Override
    public boolean isConnected() {
        return false;
    }

    private String getLogMessage() {
        StringBuilder message = new StringBuilder()
                .append("Adapter module (address path: ")
                .append(stateMachineContext.getAdapterModuleDevice().getAddressPath())
                .append(") wrong event for the state ")
                .append(getCurrentState().toString());
        return message.toString();
    }

    @Override
    public State onConnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onConfigReceived(long currentTimeMs, RS485Configuration config) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onSetConfigConfirmed(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onAddressDoublingFlagsReceived(long currentTimeMs, boolean[] addressDoublingFlags) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onExtendedStatusEnabled(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }

    @Override
    public State onTimer(long currentTimeMs) {
        stateMachineContext.getLogger().error(getLogMessage());
        return getCurrentState();
    }
}
