package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;

public interface StateHandler {
    public State getCurrentState();
    public StateHandler cloneBinded(StateMachineContext stateMachineContext);
    public boolean isConnected();
    
    public State onConnectRequest(long currentTimeMs);
    public State onDisconnectRequest(long currentTimeMs);
    public State onConnected(long currentTimeMs);
    public State onConnectFailure(long currentTimeMs);
    public State onDisconnected(long currentTimeMs);
    public State onConfigReceived(long currentTimeMs, RS485Configuration config);
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception);
    public State onSetConfigConfirmed(long currentTimeMs);
    public State onAddressDoublingFlagsReceived(long currentTimeMs, boolean[] addressDoublingFlags);
    public State onExtendedStatusEnabled(long currentTimeMs);
    public State onTimer(long currentTimeMs);
}
