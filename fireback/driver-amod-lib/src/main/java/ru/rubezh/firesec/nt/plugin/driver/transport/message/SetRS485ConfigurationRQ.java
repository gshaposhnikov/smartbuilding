package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class SetRS485ConfigurationRQ extends SetParameterRQ {

    @Override
    public DestinationType getDestinationType() {
        return DestinationType.AM;
    }

    @Override
    public FunctionType getFunctionType() {
        return FunctionType.SET_PARAMETER;
    }

    @Override
    public ParameterType getParameterType() {
        switch (lineNo) {
        case 1:
            return ParameterType.RS485Line1Configuration;
        default:
            return ParameterType.RS485Line2Configuration;
        }
    }
    
    private int lineNo;
    private RS485Configuration rs485Configuration;

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public RS485Configuration getRs485Configuration() {
        return rs485Configuration;
    }

    public void setRs485Configuration(RS485Configuration rs485Configuration) {
        this.rs485Configuration = rs485Configuration;
    }
}
