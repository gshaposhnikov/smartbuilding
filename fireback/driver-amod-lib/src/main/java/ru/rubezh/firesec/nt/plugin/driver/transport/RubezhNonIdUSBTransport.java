package ru.rubezh.firesec.nt.plugin.driver.transport;

import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

/**
 * Реализация взаимодействия по USB с HID-устройствами "КБ пожарной автоматики Рубеж" без идентификации пакетов.
 * 
 * @author Антон Васильев
 *
 */
public class RubezhNonIdUSBTransport extends RubezhUSBTransportBase {

    public RubezhNonIdUSBTransport(Logger logger, HidDeviceScanner hidDeviceScanner, int vendorId,
            int productId, String serialNumber, String product) {
        super(logger, hidDeviceScanner, vendorId, productId, serialNumber, product);
    }

    /* Подключение */

    @Override
    protected void processConnect(long currentTimeMs) {
        assert !needCloseDevice;

        if (this.connectHandler == null)
            return;

        if (hidDevice == null)
            checkAttachedDevices();

        if (hidDevice == null) {
            needCloseDevice = true;
            return;
        }

        if (!hidDevice.isOpen() && !hidDevice.open()) {
            logger.error("Failed open device (device: {}), last error message: {}", hidDevice,
                    hidDevice.getLastErrorMessage());
            needCloseDevice = true;
            return;
        }

        hidDevice.setNonBlocking(true);
        ConnectHandler connectHandler = this.connectHandler;
        this.connectHandler = null;
        connectHandler.onConnected(currentTimeMs, true);
    }

    /* Отправка данных */

    long sendRequestedTimeMs;
    int sequenceNo;
    ByteBuffer encodedDataToSend;
    SendHandler sendHandler;
    SendReceiveFailedHandler sendFailedHandler;

    /**
     * Запрос на отправку данных.
     * 
     * Метод может быть вызван, только если была завершена предыдущая отправка.
     * 
     */
    @Override
    public int send(long currentTimeMs, ByteBuffer rawData, SendHandler sendHandler,
            SendReceiveFailedHandler sendFailedHandler) {
        lastActivityTime = currentTimeMs;
        if (this.sendHandler != null)
            throw new AssertionError("Allready sending", null);

        ++this.sequenceNo;
        this.sendRequestedTimeMs = currentTimeMs;
        this.encodedDataToSend = getEncodedData(rawData.duplicate());
        this.sendHandler = sendHandler;
        this.sendFailedHandler = sendFailedHandler;

        return this.sequenceNo;
    }

    @Override
    protected void processSend(long currentTimeMs) {
        if (sendHandler == null)
            return;

        if (hidDevice != null && hidDevice.isOpen() && sendPacket(encodedDataToSend)) {
            SendHandler sendHandler = this.sendHandler;
            int seqNo = this.sequenceNo;
            this.sendHandler = null;

            sendHandler.onSended(currentTimeMs, seqNo);
        }

        if (hidDevice == null || !hidDevice.isOpen()
                || (sendHandler != null && (sendRequestedTimeMs + getMaxWaitSendTimeMs()) <= currentTimeMs)) {
            SendReceiveFailedHandler sendFailedHandler = this.sendFailedHandler;
            int seqNo = this.sequenceNo;
            this.sendHandler = null;

            logger.warn("Send failed (sequenceNo: {}), calling fail handler", seqNo);
            sendFailedHandler.onFailed(currentTimeMs, seqNo);
        }
    }

    /* Получение данных */

    long receiveRequestedTimeMs;
    int receiveRequestedSequenceNo;
    ReceiveHandler<ByteBuffer> receiveHandler;
    SendReceiveFailedHandler receiveFailedHandler;
    SendReceiveFailedHandler receiveTimeoutHandler;
    ByteBuffer receivedPacket;

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failedHandler) {
        lastActivityTime = currentTimeMs;
        if (this.receiveHandler != null)
            throw new AssertionError("Allready receiving", null);

        this.receiveRequestedTimeMs = currentTimeMs;
        this.receiveRequestedSequenceNo = sequenceNo;
        this.receiveHandler = receiveHandler;
        this.receiveFailedHandler = failedHandler;
        this.receiveTimeoutHandler = null;
        this.receivedPacket = null;
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failedHandler, SendReceiveFailedHandler timeoutHandler) {
        lastActivityTime = currentTimeMs;
        if (this.receiveHandler != null)
            throw new AssertionError("Allready receiving", null);

        this.receiveRequestedTimeMs = currentTimeMs;
        this.receiveRequestedSequenceNo = sequenceNo;
        this.receiveHandler = receiveHandler;
        this.receiveFailedHandler = failedHandler;
        this.receiveTimeoutHandler = timeoutHandler;
        this.receivedPacket = null;
    }

    @Override
    protected void rememberReceivedPacket(long currentTimeMs, ByteBuffer receivedData) {
        if (receiveHandler != null)
            receivedPacket = receivedData;
    }

    @Override
    protected int getServiceBytesCount() {
        return 1;
    }

    @Override
    protected void handleServiceByte(int byteNo, byte byteValue) {
        assert byteNo == 0;

        if ((byteValue & ID_MODE_MASK) != 0) {
            needCloseDevice = true; // устройство находится в режиме работы по ID, необходимо произвести отключение
        }
    }

    @Override
    protected void processReceive(long currentTimeMs) {
        if (receiveHandler == null)
            return;

        if (sequenceNo != receiveRequestedSequenceNo) {
            SendReceiveFailedHandler receiveFailedHandler = this.receiveFailedHandler;
            receiveHandler = null;

            logger.warn("Wrong receive sequenceNo: {}, calling fail handler", receiveRequestedSequenceNo);
            receiveFailedHandler.onFailed(currentTimeMs, receiveRequestedSequenceNo);
            return;
        }

        if (receivedPacket != null) {
            ReceiveHandler<ByteBuffer> receiveHandler = this.receiveHandler;
            ByteBuffer receivedPacket = this.receivedPacket;
            this.receiveHandler = null;
            this.receivedPacket = null;

            receiveHandler.onReceived(currentTimeMs, receiveRequestedSequenceNo, receivedPacket);
            return;
        }

        if (hidDevice == null || !hidDevice.isOpen()
                || (receiveRequestedTimeMs + getMaxWaitReceiveTimeMs()) <= currentTimeMs) {
            receiveHandler = null;
            SendReceiveFailedHandler failedHandler;

            if (receiveTimeoutHandler != null) {
                logger.warn("Receive timeout (sequenceNo: {}), calling timeout handler", receiveRequestedSequenceNo);
                failedHandler = receiveTimeoutHandler;
            } else {
                failedHandler = receiveFailedHandler;
                logger.warn("Receive timeout (sequenceNo: {}), calling fail handler", receiveRequestedSequenceNo);
            }
            failedHandler.onFailed(currentTimeMs, receiveRequestedSequenceNo);
        }

    }

}
