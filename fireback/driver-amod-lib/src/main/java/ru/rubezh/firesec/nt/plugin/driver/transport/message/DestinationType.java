package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum DestinationType {
    AM(0x01),
    LOCAL_DEVICE(0x02),
    LINE1(0x03),
    LINE2(0x04);
    
    private final int value;
    private DestinationType(int value) {
        this.value = value;
    }

    private static final Map<Integer, DestinationType> typesByValue = new HashMap<>();
    static {
        for (DestinationType type : DestinationType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<DestinationType, Integer> valuesByType = new EnumMap<>(DestinationType.class);
    static {
        for (DestinationType type : DestinationType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static DestinationType fromInteger(int value) {
        return typesByValue.get(value);
    }
    
    public static Integer toInteger(DestinationType type) {
        return valuesByType.get(type);
    }
}
