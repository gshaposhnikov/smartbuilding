package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.nio.ByteBuffer;
import java.util.*;

import org.apache.commons.codec.binary.Hex;
import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.DestinationType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.MessageCodec;

public class RubezhIdUSBTransport extends RubezhUSBTransportBase {

    /* Константы */

    /*
     * По документу есть 2 режима: ID по USB и ID по RS485. На самом деле
     * работает только ID по USB. По RS485 было требование единственного объекта
     * (атомной станции), и больше этот режим нигде не используется. ID по USB -
     * это значит МС-ка получает по USB пакеты с идентификатором, но в RS485 уже
     * отправляет без оных.
     * 
     * Однако в служебной информации мы анализируем биты обоих режимов, т.к. по
     * видимому они перепутаны (приходит 0x20 - ID по RS485, хотя включаем мы ID
     * по USB).
     */
    private static final byte[] SET_ID_MODE_REQUEST = {PACKET_BEGIN_MARKER, 0x01, 0x02, 0x34, 0x01, PACKET_END_MARKER};

    /* Конфигурация */

    /** Кодек сообщений (для извлечения маршрута сообщения) */
    private MessageCodec messageCodec;

    /** Расширенный статус (два сервисных байта вместо одного) */
    private boolean extendedStatus = true;
    /** Максимальное время ожидания подключения */
    private int maxWaitConnectTimeMs = 10000;

    public boolean isExtendedStatus() {
        return extendedStatus;
    }

    public void setExtendedStatus(boolean extendedStatus) {
        this.extendedStatus = extendedStatus;
    }

    public int getMaxWaitConnectTimeMs() {
        return maxWaitConnectTimeMs;
    }

    public void setMaxWaitConnectTimeMs(int maxWaitConnectTimeMs) {
        this.maxWaitConnectTimeMs = maxWaitConnectTimeMs;
    }

    public RubezhIdUSBTransport(Logger logger, HidDeviceScanner hidDeviceScanner, MessageCodec messageCodec,
            int vendorId,
            int productId, String serialNumber, String product) {
        super(logger, hidDeviceScanner, vendorId, productId, serialNumber, product);
        this.messageCodec = messageCodec;
        initSendReady();
    }

    /* Подключение */

    private long connectRequestTimeMs;
    private Boolean idMode;
    private boolean setIdModeRQSent;

    @Override
    public void connect(long currentTimeMs, ConnectHandler connectHandler, DisconnectHandler disconnectHandler) {
        super.connect(currentTimeMs, connectHandler, disconnectHandler);
        connectRequestTimeMs = currentTimeMs;
    }

    @Override
    protected void processConnect(long currentTimeMs) {
        assert !needCloseDevice;

        if (this.connectHandler == null)
            return;

        if ((connectRequestTimeMs + maxWaitConnectTimeMs) < currentTimeMs) {
            needCloseDevice = true;
            return;
        }

        /* 1 - жидаем подключенное HID-устройство */
        if (hidDevice == null)
            checkAttachedDevices();

        if (hidDevice == null) {
            return;
        }

        if (!hidDevice.isOpen()) {
            if (!hidDevice.open()) {
                logger.error("Failed open device (device: {}), last error message: {}", hidDevice,
                        hidDevice.getLastErrorMessage());
                needCloseDevice = true;
                return;
            }

            hidDevice.setNonBlocking(true);
            idMode = null;
            initSendReady();
            serviceFlags[0] = serviceFlags[1] = (byte) (0xFF & 0xFF);
            setIdModeRQSent = false;
            return;
        }

        /* 2 - ожидаем прочтения текущего режима ID */
        if (idMode == null)
            return;

        /* 3 - ожидаем утановки режима ID */
        if (idMode) {
            connectedSuccessfully(currentTimeMs);
            return;
        }

        if (!setIdModeRQSent) {
            if (!sendPacket(ByteBuffer.wrap(SET_ID_MODE_REQUEST))) {
                logger.error("Failed sending set ID-mode request (device: {}, last error message: {})", hidDevice,
                        hidDevice.getLastErrorMessage());
                needCloseDevice = true;
                return;
            }

            setIdModeRQSent = true;
            return;
        }
    }

    private void connectedSuccessfully(long currentTimeMs) {
        ConnectHandler connectHandler = this.connectHandler;
        this.connectHandler = null;
        connectHandler.onConnected(currentTimeMs, true);
    }

    private class SendRequest {
        long requestTimeMs;
        int sequenceNo;
        ByteBuffer dataToSend;
        DestinationType destinationType;
        SendHandler sendHandler;
        SendReceiveFailedHandler failedHandler;

        SendRequest(long requestTimeMs, int sequenceNo, DestinationType destinationType, ByteBuffer dataToSend,
                SendHandler sendHandler, SendReceiveFailedHandler failedHandler) {
            this.requestTimeMs = requestTimeMs;
            this.sequenceNo = sequenceNo;
            this.destinationType = destinationType;
            this.dataToSend = dataToSend;
            this.sendHandler = sendHandler;
            this.failedHandler = failedHandler;
        }
    }

    private Queue<SendRequest> sendRequests = new LinkedList<>();
    private int sequenceNo = 0;
    private EnumMap<DestinationType, Boolean> sendReadyByDestinationType = new EnumMap<>(DestinationType.class);

    private void initSendReady() {
        sendReadyByDestinationType = new EnumMap<>(DestinationType.class);
        for (DestinationType destinationType : DestinationType.values()) {
            sendReadyByDestinationType.put(destinationType, false);
        }
    }

    private void updateSendReady() {
        if (idMode == null || !idMode)
            return; // Обновляем флаги готовности к отправке только после установки режима с идентификаторами
        /*
         * Описание служебной информации в репорте:
         * Байт 1:
         * [7, 4] — режим прибора*;
         * [6] – включен ID по RS485;
         * [5] – включен ID по USB;
         * [3] – очереди пусты;
         * [2] – невозможно захватить маркер по каналу 2;
         * [1] – невозможно захватить маркер по каналу 1;
         * [0] – входной буфер полон.
         * * — копия битов [3-1] параметра 0x01 Приложения А, с наложенной маской 0x9.
         *  Байт 2:
         * [7] – не используется, всегда «1»;
         * [6] – очередь канала 2 пуста;
         * [5] – очередь канала 1 пуста;
         * [4] – невозможно сформировать запрос в канале 2;
         * [3] – невозможно сформировать запрос в канале 1;
         * [2] – куча (область памяти, выделенная для хранения поступающих от хоста пакетов) заполнена;
         * [1] – очередь сообщений канала 2 заполнена;
         * [0] – очередь сообщений канала 1 заполнена;
         */
        
        /* Проверяем: "входной буфер полон" и "куча заполнена" */
        sendReadyByDestinationType.put(DestinationType.AM,
                (serviceFlags[0] & 0x01) == 0 && (serviceFlags[1] & 0x04) == 0);
        /*
         * Проверяем: "невозможно захватить маркер по каналу 1", "куча заполнена" и
         * "очередь сообщений канала 1 заполнена"
         */
        sendReadyByDestinationType.put(DestinationType.LINE1,
                (serviceFlags[0] & 0x02) == 0 && (serviceFlags[1] & 0x05) == 0);
        /*
         * Проверяем: "невозможно захватить маркер по каналу 2", "куча заполнена" и
         * "очередь сообщений канала 2 заполнена"
         */
        sendReadyByDestinationType.put(DestinationType.LINE2,
                (serviceFlags[0] & 0x04) == 0 && (serviceFlags[1] & 0x06) == 0);

        /* Проверяем: "входной буфер полон" и "куча заполнена" */
        sendReadyByDestinationType.put(DestinationType.LOCAL_DEVICE,
                (serviceFlags[0] & 0x01) == 0 && (serviceFlags[1] & 0x04) == 0);

    }

    private void updateIdMode() {
        idMode = (serviceFlags[0] & ID_MODE_MASK) != 0;
    }

    @Override
    public int send(long currentTimeMs, ByteBuffer rawData, SendHandler sendHandler,
            SendReceiveFailedHandler failHandler) {
        lastActivityTime = currentTimeMs;
        int sequenceNo = ++this.sequenceNo;
        ByteBuffer rawDataWithSeqNo = ByteBuffer.allocateDirect(4 + rawData.remaining());
        rawDataWithSeqNo.putInt(sequenceNo);
        rawDataWithSeqNo.put(rawData);
        rawDataWithSeqNo.flip();
        sendRequests
                .add(new SendRequest(currentTimeMs, sequenceNo, messageCodec.getDestinationTypeFromEncoded(rawData),
                        getEncodedData(rawDataWithSeqNo), sendHandler, failHandler));
        return sequenceNo;
    }

    @Override
    protected void processSend(long currentTimeMs) {
        if (!sendRequests.isEmpty()) {
            Iterator<SendRequest> sendRequestIt = sendRequests.iterator();
            while (sendRequestIt.hasNext()) {
                SendRequest sendRequest = sendRequestIt.next();
                boolean sendReady = sendReadyByDestinationType.get(sendRequest.destinationType);
                if (sendReady && hidDevice != null && hidDevice.isOpen() && sendPacket(sendRequest.dataToSend)) {
                    /* за одну итерацию осуществляем отправку только одного пакета */
                    sendRequestIt.remove();
                    sendRequest.sendHandler.onSended(currentTimeMs, sendRequest.sequenceNo);
                    break;
                }
            }

            /* также удаляем все просроченные запросы, если мы их не успели обработать */
            Queue<SendRequest> removedRequests = new LinkedList<>();
            sendRequestIt = sendRequests.iterator();
            while (sendRequestIt.hasNext()) {
                SendRequest sendRequest = sendRequestIt.next();
                if (hidDevice == null || !hidDevice.isOpen()
                        || (sendRequest.requestTimeMs + getMaxWaitSendTimeMs()) <= currentTimeMs) {
                    sendRequestIt.remove();
                    removedRequests.add(sendRequest);
                }
            }
            /*
             * Обработчики ошибок отправки вызываем в отдельном цикле, т.к. они могут добавить новые запросы
             */
            for (SendRequest removedRequest : removedRequests) {
                logger.warn("Send timeout (sequenceNo: {}), calling fail handler", removedRequest.sequenceNo);
                removedRequest.failedHandler.onFailed(currentTimeMs, removedRequest.sequenceNo);
            }
        }
    }

    private class ReceiveRequest {
        long requestTimeMs;
        int sequenceNo;
        ReceiveHandler<ByteBuffer> receiveHandler;
        SendReceiveFailedHandler failedHandler;
        SendReceiveFailedHandler timeoutHandler;
        ReceivedPacket receivedPacket;

        ReceiveRequest(long requestTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
                SendReceiveFailedHandler failedHandler, SendReceiveFailedHandler timeoutHandler) {
            this.requestTimeMs = requestTimeMs;
            this.sequenceNo = sequenceNo;
            this.receiveHandler = receiveHandler;
            this.failedHandler = failedHandler;
            this.timeoutHandler = timeoutHandler;
        }
    }

    private class ReceivedPacket {
        long receivedTimeMs;
        int sequenceNo;
        ByteBuffer receivedData;

        ReceivedPacket(long receivedTimeMs, int sequenceNo, ByteBuffer receivedData) {
            this.receivedTimeMs = receivedTimeMs;
            this.sequenceNo = sequenceNo;
            this.receivedData = receivedData;
        }
    }

    private Queue<ReceiveRequest> receiveRequests = new LinkedList<>();
    private Queue<ReceivedPacket> receivedPackets = new ArrayDeque<>();
    /** Два байта со служебными флагами */
    private byte[] serviceFlags = new byte[2];

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failHandler) {
        lastActivityTime = currentTimeMs;
        receiveRequests.add(new ReceiveRequest(currentTimeMs, sequenceNo, receiveHandler, failHandler, null));
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<ByteBuffer> receiveHandler,
            SendReceiveFailedHandler failHandler, SendReceiveFailedHandler timeoutHandler) {
        lastActivityTime = currentTimeMs;
        receiveRequests.add(new ReceiveRequest(currentTimeMs, sequenceNo, receiveHandler, failHandler, timeoutHandler));
    }

    @Override
    protected void processReceive(long currentTimeMs) {
        /* Ищем полученные пакеты, ожидаемые своим получателем */
        Queue<ReceiveRequest> requestsToCallback = new LinkedList<>();
        for (Iterator<ReceiveRequest> receiveRequestIt = receiveRequests.iterator(); receiveRequestIt.hasNext();) {
            ReceiveRequest receiveRequest = receiveRequestIt.next();
            for (Iterator<ReceivedPacket> receivedPacketIt = receivedPackets.iterator(); receivedPacketIt.hasNext();) {
                ReceivedPacket receivedPacket = receivedPacketIt.next();
                if (receiveRequest.sequenceNo == receivedPacket.sequenceNo) {
                    receiveRequestIt.remove();
                    receivedPacketIt.remove();
                    receiveRequest.receivedPacket = receivedPacket;
                    requestsToCallback.add(receiveRequest);
                    break;
                }
            }
        }
        /*
         * Обработчики приема вызываем в отдельном цикле, т.к. они могут добавить новые запросы
         */
        for (ReceiveRequest receiveRequest : requestsToCallback) {
            receiveRequest.receiveHandler.onReceived(currentTimeMs, receiveRequest.sequenceNo,
                    receiveRequest.receivedPacket.receivedData);
        }

        /* удаляем просроченные запросы на ожидание данных */
        requestsToCallback.clear();
        for (Iterator<ReceiveRequest> receiveRequestIt = receiveRequests.iterator(); receiveRequestIt.hasNext();) {
            ReceiveRequest receiveRequest = receiveRequestIt.next();
            if (hidDevice == null || !hidDevice.isOpen()
                    || (receiveRequest.requestTimeMs + getMaxWaitReceiveTimeMs()) <= currentTimeMs) {
                receiveRequestIt.remove();
                requestsToCallback.add(receiveRequest);
            }
        }
        /* Аналогично: обработчики в отдельном цикле */
        for (ReceiveRequest receiveRequest : requestsToCallback) {
            if (receiveRequest.timeoutHandler != null) {
                logger.warn("Receive timeout (sequenceNo: {}), calling timeout handler", receiveRequest.sequenceNo);
                receiveRequest.timeoutHandler.onFailed(currentTimeMs, receiveRequest.sequenceNo);
            } else {
                logger.warn("Receive timeout (sequenceNo: {}), calling fail handler", receiveRequest.sequenceNo);
                receiveRequest.failedHandler.onFailed(currentTimeMs, receiveRequest.sequenceNo);
            }
        }

        /* удаляем просроченные данные */
        for (Iterator<ReceivedPacket> receivedPacketIt = receivedPackets.iterator(); receivedPacketIt.hasNext();) {
            ReceivedPacket receivedPacket = receivedPacketIt.next();
            if (hidDevice == null || !hidDevice.isOpen()
                    || (receivedPacket.receivedTimeMs + getMaxWaitReceiveTimeMs()) <= currentTimeMs) {
                {
                    ByteBuffer bb = receivedPacket.receivedData.duplicate();
                    bb.flip();
                    byte[] b = new byte[bb.remaining()];
                    bb.get(b);
                    logger.warn("Non expected data received (sequenceNo: {}, dataBuffer: {}), recycling",
                            receivedPacket.sequenceNo, Hex.encodeHexString(b));
                }
                receivedPacketIt.remove();
            }
        }
    }

    @Override
    protected void rememberReceivedPacket(long currentTimeMs, ByteBuffer receivedData) {
        if (idMode != null && idMode)
            receivedPackets.add(new ReceivedPacket(currentTimeMs, receivedData.getInt(), receivedData));
    }

    @Override
    protected int getServiceBytesCount() {
        if (extendedStatus)
            return 2;
        else
            return 1;
    }

    @Override
    protected void handleServiceByte(int byteNo, byte byteValue) {
        if (byteValue != serviceFlags[byteNo]) {
            logger.info("Service flag byte changed. №: {}, value: 0x{}", byteNo, Integer.toHexString(byteValue & 0xFF));
            serviceFlags[byteNo] = byteValue;
            updateSendReady();
            updateIdMode();
        }
    }

}
