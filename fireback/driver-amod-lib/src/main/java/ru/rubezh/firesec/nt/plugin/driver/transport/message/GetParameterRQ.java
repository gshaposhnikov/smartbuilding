package ru.rubezh.firesec.nt.plugin.driver.transport.message;

abstract public class GetParameterRQ implements SetGetParameterRQ {
    @Override
    public FunctionType getFunctionType() {
        return FunctionType.GET_PARAMETER;
    }
}
