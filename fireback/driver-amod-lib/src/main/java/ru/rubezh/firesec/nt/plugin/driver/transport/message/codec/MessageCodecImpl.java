package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration.DeviceDescriptor;

public class MessageCodecImpl implements MessageCodec {

    /** Системный журнал */
    private Logger logger;
    /** Размер буфера для кодируемых сообщений */
    private static final int BUFFER_SIZE = 512;
    /** Размер листов */
    private static final int DEVICE_LIST_SIZE = 64;
    /** Индекс байта с типом маршрута */
    private static final int DESTINATION_TYPE_BYTE_INDEX = 0;

    /**
     * Запуск кодирования сообщения, предназначенного для самой МС.
     *
     * - создает новый буфер;
     * - кладет байт кода маршрута;
     * - кладет байт кода функции.
     *
     * @param message кодируемое сообщение
     * @return байтовый буфер
     */
    private ByteBuffer startEncodeAMMessage(Message message) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
        byteBuffer.put((byte)(DestinationType.toInteger(message.getDestinationType()) & 0xFF));
        byteBuffer.put((byte)(FunctionType.toInteger(message.getFunctionType()) & 0xFF));
        return byteBuffer;
    }

    /**
     * Кодирование запроса на получение какого-либо параметра МС
     *
     * @param message кодируемое сообщение
     * @return байтовый буфер с позицией в конце закодированного сообщения (необходимо сделать flip перед отправкой)
     */
    @Override
    public ByteBuffer encodeForAM(GetParameterRQ message) {
        ByteBuffer byteBuffer = startEncodeAMMessage(message);
        byteBuffer.put((byte)(ParameterType.toInteger(message.getParameterType()) & 0xFF));
        return byteBuffer;
    }

    /**
     * Кодирование запроса на запись конфигурации МС
     *
     * @param message кодируемое сообщение. Должно содрежать конфигурацию с отсортированным по адресу списку дескрипторов устройств, включающему и служебный адрес.
     * @return байтовый буфер с позицией в конце закодированного сообщения (необходимо сделать flip перед отправкой)
     */
    @Override
    public ByteBuffer encodeForAM(SetRS485ConfigurationRQ message) {
        ByteBuffer byteBuffer = startEncodeAMMessage(message);
        byteBuffer.put((byte)(ParameterType.toInteger(message.getParameterType()) & 0xFF));
        /* записываем сначала адрес МС */
        byteBuffer.put((byte)(message.getRs485Configuration().getAdapterAddress() & 0xFF));
        /* записываем скорость RS485 */
        byteBuffer.put((byte)(message.getRs485Configuration().getRate() & 0xFF));
        /* адресный лист должен быть отсортирован по возрастанию значения адреса,
         * а типы устройств должны быть в тех же позициях, что и их адреса */
        List<DeviceDescriptor> devices = new ArrayList<>(message.getRs485Configuration().getDevices());
        for (int i = 0; i < DEVICE_LIST_SIZE; ++i) {
            if (i < devices.size())
                byteBuffer.put((byte)(devices.get(i).getAddress() & 0xFF));
            else
                byteBuffer.put((byte)0x00); /* лишние байты адресного листа должны быть заполнены нулями */
        }
        for (int i = 0; i < DEVICE_LIST_SIZE; ++i) {
            if (i < devices.size())
                byteBuffer.put((byte)(devices.get(i).getType() & 0xFF));
            else
                byteBuffer.put((byte)0x00);
        }
        return byteBuffer;
    }

    @Override
    public ByteBuffer encodeForAM(EnableExtendedStatusRQ message) {
        ByteBuffer byteBuffer = startEncodeAMMessage(message);
        byteBuffer.put((byte)(ParameterType.toInteger(message.getParameterType()) & 0xFF));
        return byteBuffer;
    }

    @Override
    public ByteBuffer encodeForLine(RS485ProxyRQ message) {
        /* создаем новый буфер */
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
        /* добавляем маршрут */
        byteBuffer.put((byte)(DestinationType.toInteger(message.getDestinationType()) & 0xFF));
        /* добавляем адрес устройства на линии */
        byteBuffer.put((byte)(message.getRs485Address() & 0xFF));
        /* добавляем контекст функции, если есть */
        if (message.getFunctionContextData() != null)
            byteBuffer.put(message.getFunctionContextData());
        return byteBuffer;
    }

    @Override
    public DestinationType getDestinationTypeFromEncoded(ByteBuffer encodedMessage) {
        return DestinationType.fromInteger(encodedMessage.get(DESTINATION_TYPE_BYTE_INDEX));
    }

    /* маски для выделения частей функционального байта */
    private static final byte EXCEPTION_FLAG_MASK = (byte)(0x80 & 0xFF);
    private static final byte FUNCTION_MASK = 0x3F;

    private void decode(ByteBuffer byteBuffer, GetRS485ConfigurationRS response) throws BufferUnderflowException {

        RS485Configuration rs485Configuration = new RS485Configuration();
        rs485Configuration.setAdapterAddress(Byte.toUnsignedInt(byteBuffer.get()));
        rs485Configuration.setRate(Byte.toUnsignedInt(byteBuffer.get()));
        List<DeviceDescriptor> deviceDescriptors = new ArrayList<>();
        /* читаем 64 байта адресного листа */
        for (int i = 0; i < 64; ++i) {
            byte address = byteBuffer.get();
            if (address != (byte)0) {
                DeviceDescriptor deviceDescriptor = rs485Configuration.new DeviceDescriptor();
                deviceDescriptor.setAddress(Byte.toUnsignedInt(address));
                deviceDescriptors.add(deviceDescriptor);
            }
        }
        for (int i = 0; i < deviceDescriptors.size(); ++i) {
            byte typeCode = byteBuffer.get();
            deviceDescriptors.get(i).setType(Byte.toUnsignedInt(typeCode));
        }
        rs485Configuration.setDevices(deviceDescriptors);
        response.setRs485Configuration(rs485Configuration);
    }

    private void decode(ByteBuffer byteBuffer, GetRS485AddressesDoublingFlagsRS response) throws BufferUnderflowException {
        byte flags = byteBuffer.get();
        if ((flags & 0x01) == 0)
            response.setIsDoubling(0, false);
        else
            response.setIsDoubling(0, true);
        if ((flags & 0x02) == 0)
            response.setIsDoubling(1, false);
        else
            response.setIsDoubling(1, true);
    }

    @Override
    public RS485ProxyRS decodeRS485ProxyRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        DestinationType destinationType = DestinationType.fromInteger(byteBuffer.get());
        RS485ProxyRS rs485ProxyRS = new RS485ProxyRS(destinationType);
        if (destinationType == DestinationType.AM) {
            byte function = byteBuffer.get();
            if ((function & EXCEPTION_FLAG_MASK) != 0) {
                ExceptionType exception = ExceptionType.fromInteger(function & FUNCTION_MASK);
                rs485ProxyRS.setException(exception);
            }
        } else {
            int rs485Address = byteBuffer.get();
            rs485ProxyRS.setRs485Address(rs485Address);
        }
        ByteBuffer contextData = ByteBuffer.allocateDirect(byteBuffer.remaining());
        contextData.put(byteBuffer);
        contextData.flip();
        rs485ProxyRS.setFunctionContextData(contextData);
        return rs485ProxyRS;
    }

    @Override
    public CommonRS decodeGetRS485ConfigurationRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        CommonRS response = null;
        DestinationType destinationType = DestinationType.fromInteger(byteBuffer.get());
        if (destinationType == DestinationType.AM) {
            /* читаем поле функции */
            byte functionCode = byteBuffer.get();
            if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
                /* нет исключительной */
                FunctionType functionType = FunctionType.fromInteger(functionCode & FUNCTION_MASK);
                if (functionType != null && functionType == FunctionType.GET_PARAMETER) {
                    /* читаем конфигурацию */
                    GetRS485ConfigurationRS getRS485ConfigurationRS = new GetRS485ConfigurationRS();
                    decode(byteBuffer, getRS485ConfigurationRS);
                    response = getRS485ConfigurationRS;
                }
                else
                    logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
            else {
                /* есть исключительная */
                response = new CommonRS(FunctionType.fromInteger(functionCode & FUNCTION_MASK));
                response.setSuccess(false);
                /* читаем код исключительной */
                response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
            }
        }
        else
            logger.warn("Wrong destination type in response: {}",
                        (destinationType != null ? destinationType.toString() : destinationType));

        return response;
    }

    @Override
    public CommonRS decodeSetAMParameterRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        CommonRS response = null;
        DestinationType destinationType = DestinationType.fromInteger(byteBuffer.get());
        if (destinationType == DestinationType.AM) {
            /* читаем поле функции */
            byte functionCode = byteBuffer.get();
            if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
                /* нет исключительной */
                FunctionType functionType = FunctionType.fromInteger(functionCode & FUNCTION_MASK);
                if (functionType != null && functionType == FunctionType.SET_PARAMETER) {
                    /* создаем объект успешного ответа */
                    response = new CommonRS(FunctionType.fromInteger(functionCode & FUNCTION_MASK));
                }
                else
                    logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
            else {
                /* есть исключительная */
                response = new CommonRS(FunctionType.fromInteger(functionCode & FUNCTION_MASK));
                response.setSuccess(false);
                /* читаем код исключительной */
                response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
            }
        }
        else
            logger.warn("Wrong destination type in response: {}",
                        (destinationType != null ? destinationType.toString() : destinationType));

        return response;
    }

    @Override
    public CommonRS decodeGetRS485AddressDoublingFlagsRS(ByteBuffer byteBuffer) throws BufferUnderflowException {
        CommonRS response = null;
        DestinationType destinationType = DestinationType.fromInteger(byteBuffer.get());
        if (destinationType == DestinationType.AM) {
            /* читаем поле функции */
            byte functionCode = byteBuffer.get();
            if ((functionCode & EXCEPTION_FLAG_MASK) == 0) {
                /* нет исключительной */
                FunctionType functionType = FunctionType.fromInteger(functionCode & FUNCTION_MASK);
                if (functionType != null && functionType == FunctionType.GET_PARAMETER) {
                    /* читаем конфигурацию */
                    GetRS485AddressesDoublingFlagsRS getRS485AddressesDoublingFlagsRS = new GetRS485AddressesDoublingFlagsRS();
                    decode(byteBuffer, getRS485AddressesDoublingFlagsRS);
                    response = getRS485AddressesDoublingFlagsRS;
                }
                else
                    logger.warn("Wrong function code in response: {}", functionCode & FUNCTION_MASK);
            }
            else {
                /* есть исключительная */
                response = new CommonRS(FunctionType.fromInteger(functionCode & FUNCTION_MASK));
                response.setSuccess(false);
                /* читаем код исключительной */
                response.setExceptionType(ExceptionType.fromInteger(byteBuffer.get()));
            }
        }
        else
            logger.warn("Wrong destination type in response: {}",
                        (destinationType != null ? destinationType.toString() : destinationType));

        return response;
    }

    public MessageCodecImpl(Logger logger) {
        this.logger = logger;
    }
}
