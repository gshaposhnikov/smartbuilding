package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;

public class FirstTimeConnecting extends Connecting {

    @Override
    public State getCurrentState() {
        return State.FIRST_TIME_CONNECTING;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        FirstTimeConnecting handler = new FirstTimeConnecting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return super.onConnectFailure(currentTimeMs);
    }

}
