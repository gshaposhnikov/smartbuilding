package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.io.IOException;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.ActivationValidateMessage;
import ru.rubezh.firesec.nt.plugin.driver.StateMachine;

public class StateMachineImpl implements StateMachine {

    StateMachineContext stateMachineContext;
    private long startTimeMs;

    public StateMachineImpl(StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    @Override
    public boolean start(long currentTimeMs, List<ActivationValidateMessage> validateMessages) {
        startTimeMs = currentTimeMs;
        stateMachineContext.getStateHandler().onConnectRequest(currentTimeMs);
        return true;
    }

    @Override
    public void step(long currentTimeMs) {
        stateMachineContext.getClock().process(currentTimeMs);
        try {
            stateMachineContext.getTransport().process(currentTimeMs);
        } catch (IOException e) {
            stateMachineContext.getLogger().error("IO exception with message: {}", e.getMessage());
        }
    }

    @Override
    public void stop(long currentTimeMs) {
        stateMachineContext.getStateHandler().onDisconnectRequest(currentTimeMs);
    }

    @Override
    public long getLastActivityTime() {
        long lastActivityTimeMs = stateMachineContext.getTransport().getLastActivityTime();
        if (lastActivityTimeMs == 0)
            lastActivityTimeMs = startTimeMs;
        return lastActivityTimeMs;
    }

}
