package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.amqp.message.alert.AddActiveDeviceState;
import ru.rubezh.firesec.nt.amqp.message.alert.RemoveActiveDeviceState;
import ru.rubezh.firesec.nt.amqp.message.alert.SetActiveDeviceStates;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class StateMachineHooksImpl implements StateMachineHooks {

    private StateMachineContext stateMachineContext;

    @Override
    public StateMachineContext getStateMachineContext() {
        return stateMachineContext;
    }

    @Override
    public void setStateMachineContext(StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }

    @Override
    public void onConnected(long currentTimeMs) {
        ru.rubezh.firesec.nt.domain.v1.State deviceLostState = stateMachineContext.getAdapterModuleDeviceProfile()
                .getDeviceLostState();
        if (deviceLostState != null && deviceLostState.getId() != null) {
            RemoveActiveDeviceState alert = new RemoveActiveDeviceState(
                    stateMachineContext.getAdapterModuleDevice().getId(), deviceLostState.getId());
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onDisconnected(long currentTimeMs) {
        ru.rubezh.firesec.nt.domain.v1.State deviceLostState = stateMachineContext.getAdapterModuleDeviceProfile()
                .getDeviceLostState();
        if (deviceLostState != null && deviceLostState.getId() != null) {
            AddActiveDeviceState alert = new AddActiveDeviceState(stateMachineContext.getAdapterModuleDevice().getId(),
                    deviceLostState.getId());
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onConnectFailure(long currentTimeMs) {
        stateMachineContext.getLogger().info("Connect failure to adaptaer module {}",
                stateMachineContext.getAdapterModuleDevice().getAddressPath());
    }

    @Override
    public void onRS485Configured(long currentTimeMs, int lineNo) {
        ru.rubezh.firesec.nt.domain.v1.State deviceReadyState = stateMachineContext.getAdapterModuleDeviceProfile()
                .getDeviceReadyState();
        if (deviceReadyState != null && deviceReadyState.getId() != null) {
            List<String> states = new ArrayList<>();
            states.add(deviceReadyState.getId());
            SetActiveDeviceStates alert = new SetActiveDeviceStates(
                    stateMachineContext.getAdapterModuleDevice().getId(), states);
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onRS485ConfigureFailed(long currentTimeMs, int lineNo, ExceptionType exceptionType) {
        ru.rubezh.firesec.nt.domain.v1.State deviceNotReadyState = stateMachineContext.getAdapterModuleDeviceProfile()
                .getDeviceNotReadyState();
        if (deviceNotReadyState != null && deviceNotReadyState.getId() != null) {
            List<String> states = new ArrayList<>();
            states.add(deviceNotReadyState.getId());
            SetActiveDeviceStates alert = new SetActiveDeviceStates(
                    stateMachineContext.getAdapterModuleDevice().getId(), states);
            stateMachineContext.getDriverAlertSender().send(alert);
        }
    }

    @Override
    public void onRS485AddressDoublingDetected(long currentTimeMs, int lineNo) {
        stateMachineContext.getLogger().warn("RS 485 address doubling detected on adaptaer module {}, line {}",
                stateMachineContext.getAdapterModuleDevice().getId(),
                lineNo);
        //TODO: send state
    }

    @Override
    public void onRS485AddressDoublingFixed(long currentTimeMs, int lineNo) {
        stateMachineContext.getLogger().info("RS 485 address doubling fixed on adaptaer module {}, line {}",
                stateMachineContext.getAdapterModuleDevice().getId(),
                lineNo);
        //TODO: send state
    }
}
