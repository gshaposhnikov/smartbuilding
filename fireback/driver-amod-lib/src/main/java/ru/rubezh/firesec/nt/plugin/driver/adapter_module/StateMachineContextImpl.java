package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.nio.ByteBuffer;
import java.util.*;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.domain.v1.DeviceProperty;
import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler.StateHandler;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler.StateRelay;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration.DeviceDescriptor;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.MessageCodec;

public class StateMachineContextImpl implements StateMachineContext {
    private Logger logger;

    private Transport<ByteBuffer> transport;
    private MessageCodec messageCodec;
    private Clock сlock;
    StateHandler stateHandler;
    private ActionExecutor actionExecutor;
    private StateMachineHooks stateMachineHooks;
    private DriverAlertSender driverAlertSender;

    /* конфигурация */
    private DeviceProfile adapterModuleDeviceProfile;
    private Map<String, DeviceProfile> supportedDeviceProfiles;
    private int reconnectInterval = 10000;
    private int pingInterval = 5000;
    private int reconfigInterval = 20000;

    /* модель данных */
    final private int ADAPTER_MODULE_LINE_ADDRESS = 33;
    final private int DEFAULT_RS485_BITRATE = 3;
    /* TODO: адрес МС должен настраиваться пользователем отдельным параметром */
    private ActiveDevice adapterModuleDevice;
    private List<ActiveDevice> childDevices;
    private int lineNo;

    public StateMachineContextImpl(Logger logger,
            DeviceProfile adapterModuleDeviceProfile,
            Map<String, DeviceProfile> supportedDeviceProfiles,
            DriverAlertSender driverAlertSender,
            Transport<ByteBuffer> transport,
            MessageCodec messageCodec,
            Clock сlock,
            List<ActiveDevice> childDevices,
            ActiveDevice adapterModuleDevice,
            int lineNo,
            StateMachineHooks stateMachineHooks) {
        this.logger = logger;
        this.adapterModuleDeviceProfile = adapterModuleDeviceProfile;
        this.supportedDeviceProfiles = supportedDeviceProfiles;
        this.driverAlertSender = driverAlertSender;
        this.transport = transport;
        this.messageCodec = messageCodec;
        this.сlock = сlock;
        this.childDevices = childDevices;
        this.adapterModuleDevice = adapterModuleDevice;
        this.lineNo = lineNo;
        this.stateMachineHooks = stateMachineHooks;
        stateMachineHooks.setStateMachineContext(this);

        actionExecutor = new ActionExecutorStage3Impl(this);
        stateHandler = new StateRelay(this);
    }

    public void setReconfigInterval(int reconfigInterval) {
        this.reconfigInterval = reconfigInterval;
    }

    public void setPingInterval(int pingInterval) {
        this.pingInterval = pingInterval;
    }

    public void setReconnectInterval(int reconnectInterval) {
        this.reconnectInterval = reconnectInterval;
    }

    @Override
    public StateHandler getStateHandler() {
        return stateHandler;
    }

    @Override
    public ActionExecutor getActionExecutor() {
        return actionExecutor;
    }

    @Override
    public DriverAlertSender getDriverAlertSender() {
        return driverAlertSender;
    }

    @Override
    public DeviceProfile getAdapterModuleDeviceProfile() {
        return adapterModuleDeviceProfile;
    }

    public void setAdapterModuleDeviceProfile(DeviceProfile adapterModuleDeviceProfile) {
        this.adapterModuleDeviceProfile = adapterModuleDeviceProfile;
    }

    @Override
    public ActiveDevice getAdapterModuleDevice() {
        return adapterModuleDevice;
    }

    @Override
    public void setAdapterModuleDevice(ActiveDevice adapterModuleDevice) {
        this.adapterModuleDevice = adapterModuleDevice;
    }

    @Override
    public List<ActiveDevice> getChildDevices() {
        return childDevices;
    }

    @Override
    public int getLineNo() {
        return lineNo;
    }

    @Override
    public Transport<ByteBuffer> getTransport() {
        return transport;
    }

    @Override
    public Clock getClock() {
        return сlock;
    }

    @Override
    public MessageCodec getMessageCodec() {
        return messageCodec;
    }

    @Override
    public StateMachineHooks getStateMachineHooks() {
        return stateMachineHooks;
    }

    @Override
    public void setStateMachineHooks(StateMachineHooks stateMachineHooks) {
        this.stateMachineHooks = stateMachineHooks;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public int getReconnectInterval() {
        return reconnectInterval;
    }

    @Override
    public int getPingInterval() {
        return pingInterval;
    }

    @Override
    public int getReconfigInterval() {
        return reconfigInterval;
    }

    @Override
    public RS485Configuration generateLineConfiguration(int lineNo) {
        /* создаем конфигурацию линии */
        RS485Configuration rs485Configuration = new RS485Configuration();
        /* записываем адрес самого адаптера */
        rs485Configuration.setAdapterAddress(ADAPTER_MODULE_LINE_ADDRESS);
        /* записываем скорость RS485 */
        DeviceProperty bitrateProperty = adapterModuleDeviceProfile.getDeviceProperties().get(DeviceProperties.RS485Bitrate.toString());
        String bitrateValue = adapterModuleDevice.getDeviceProject().getDevicePropertyValues().get(DeviceProperties.RS485Bitrate.toString());
        if (bitrateProperty != null)
            rs485Configuration.setRate(DeviceProperty.getIntegerOrDefault(bitrateValue, bitrateProperty));
        else
            rs485Configuration.setRate(DEFAULT_RS485_BITRATE);
        /* создаем массив с адресами и типами устройств на линии */
        List<DeviceDescriptor> lineDevices = new ArrayList<>();
        /* инициализируем и добавляем адрес и тип самого модуля сопряжения */
        DeviceDescriptor selfDescriptor = rs485Configuration.new DeviceDescriptor();
        selfDescriptor.setAddress(ADAPTER_MODULE_LINE_ADDRESS);
        /* тип МС-ки должен остаться нулевым */
        selfDescriptor.setType(0);
        lineDevices.add(selfDescriptor);

        // TODO: добавить адрес резервной МС с 0-вым типом, если она настроена

        /* инициализируем и добавляем служебный адрес */
        DeviceDescriptor serviceDescriptor = rs485Configuration.new DeviceDescriptor();
        serviceDescriptor.setAddress(0xFF);
        serviceDescriptor.setType(0);
        lineDevices.add(serviceDescriptor);
        /* добавляем адреса и типы всех дочерних устройств */
        for (ActiveDevice device : childDevices) {
            if (device.getDeviceProject().getLineNo() == lineNo) {
                DeviceDescriptor deviceDescriptor = rs485Configuration.new DeviceDescriptor();
                deviceDescriptor.setAddress(device.getDeviceProject().getLineAddress());
                deviceDescriptor.setType(supportedDeviceProfiles
                        .getOrDefault(device.getDeviceProject().getDeviceProfileId(), new DeviceProfile())
                        .getAmodDeviceType());
                lineDevices.add(deviceDescriptor);
            }
        }
        /* сортируем по возрастанию адресов */
        Collections.sort(lineDevices);
        rs485Configuration.setDevices(lineDevices);
        return rs485Configuration;
    }

    @Override
    public Map<String, DeviceProfile> getSupportedDeviceProfiles() {
        return supportedDeviceProfiles;
    }
}
