package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.nio.ByteBuffer;

public class RS485ProxyRQ {
    private DestinationType destinationType;
    private int rs485Address;
    private ByteBuffer functionContextData;

    public RS485ProxyRQ(DestinationType destinationType, int rs485Address) {
        assert rs485Address >= 0 && rs485Address <= 0xFF;

        this.destinationType = destinationType;
        this.rs485Address = rs485Address;
    }

    public int getRs485Address() {
        return rs485Address;
    }

    public ByteBuffer getFunctionContextData() {
        return functionContextData;
    }

    public void setFunctionContextData(ByteBuffer incapsulatedData) {
        this.functionContextData = incapsulatedData;
    }

    public DestinationType getDestinationType() {
        return destinationType;
    }

}
