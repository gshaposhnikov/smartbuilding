package ru.rubezh.firesec.nt.plugin.driver.transport.message;

abstract public class SetParameterRQ implements SetGetParameterRQ {
    @Override
    public FunctionType getFunctionType() {
        return FunctionType.SET_PARAMETER;
    }
}
