package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class GetRS485AddressesDoublingFlagsRQ extends GetParameterRQ {

    @Override
    public DestinationType getDestinationType() {
        return DestinationType.AM;
    }

    @Override
    public ParameterType getParameterType() {
        return ParameterType.RS485AddressesDoublingFlags;
    }
}
