package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class GetRS485ConfigurationRS extends CommonRS {

    private RS485Configuration rs485Configuration;

    public RS485Configuration getRs485Configuration() {
        return rs485Configuration;
    }

    public void setRs485Configuration(RS485Configuration rs485Configuration) {
        this.rs485Configuration = rs485Configuration;
    }
    
    public GetRS485ConfigurationRS() {
        super(FunctionType.GET_PARAMETER);
    }
}
