package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

/**
 * Набор методов, в которых должна реализовываться реакция на конкретные результаты работы конечного автомата драйвера модуля сопряжения,
 * но которые не должны при этом влиять на работу самого автомата.
 * 
 * @author Антон Васильев
 *
 */
public interface StateMachineHooks {
    public void onConnected(long currentTimeMs);
    public void onDisconnected(long currentTimeMs);
    public void onConnectFailure(long currentTimeMs);

    public void onRS485Configured(long currentTimeMs, int lineNo);
    public void onRS485ConfigureFailed(long currentTimeMs, int lineNo, ExceptionType exceptionType);

    public void onRS485AddressDoublingDetected(long currentTimeMs, int lineNo);
    public void onRS485AddressDoublingFixed(long currentTimeMs, int lineNo);
    
    /* Доступ к контексту конечного автомата */
    public StateMachineContext getStateMachineContext();
    public void setStateMachineContext(StateMachineContext stateMachineContext); 
}
