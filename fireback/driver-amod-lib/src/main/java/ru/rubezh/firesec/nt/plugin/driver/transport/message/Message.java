package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public interface Message {
    public DestinationType getDestinationType();
    public FunctionType getFunctionType();
}
