package ru.rubezh.firesec.nt.plugin.driver.transport.message;

/**
 * Запрос на включение режима "расширенного статуса"
 *
 * Статус МС состоит из одного или двух байт - в зависимости от данной настройки.
 * Старый ФС всегда включает этот режим, а выключить его (наверное) нельзя.
 * Чтобы не утруждать пользователя сбросом питания МС, решено этот режим включать и у нас.
 * Статус МС отбрасывается нашим транспортом, однако ему надо знать его размер. Для этого
 * предусмотрен флаг extendedStatus в конструкторе транспорта.
 */
public class EnableExtendedStatusRQ extends SetParameterRQ {

    @Override
    public DestinationType getDestinationType() {
        return DestinationType.AM;
    }

    @Override
    public FunctionType getFunctionType() {
        return FunctionType.SET_PARAMETER;
    }

    @Override
    public ParameterType getParameterType() {
        return ParameterType.extendedStatusEnable;
    }
    
}
