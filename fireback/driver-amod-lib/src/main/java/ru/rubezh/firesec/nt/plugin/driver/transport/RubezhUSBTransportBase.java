package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hid4java.HidDevice;

import com.sun.jna.Platform;

/**
 * Реализация базового функционала для взаимодействия по USB с HID-устройствами "КБ пожарной автоматики Рубеж".
 * 
 * Взаимодействие по USB реализовано через библиотеку Hid4Java.
 * 
 * В базовом классе реализовано:
 *  - работа с Hid4Java по обнаружению HID-устройства;
 *  - кодирование/декодирование данных транспортного уровня (добавление маркеров и ESC-символов);
 *  - работа с HID4Java по отправке данных;
 *  - работа с Hid4Java по приему данных;
 *  - общая фоновая процедура;
 *  - отключение с закрытием дескриптора HID-устройства и высвобождением других ресурсов.
 *  
 * Дочерние классы должны реализовать:
 *  - публичные методы интерфейса Transport, обрабатывающие запросы на отправку/получение данных;
 *  - абстрактный метод по вполнению итерационной процедуры подключения;
 *  - абстрактный метод по итерационному выполнению запросов на отправку данных;
 *  - абстрактный метод по итерационной обработке принятых данных;
 *  - некоторые другие вспомогательные методы (см. ниже).
 *  
 * Дочерние классы должны использовать hidDevice в качестве дескриптора HID-устройства
 *  и вызывать checkAttachedDevices для поиска подходящего подключенного при выполнении процедуры подключения.
 * Дочерние классы должны записывать true в needCloseDevice, чтобы выполнилась итерация по отключению
 *  (закрытию HID-устройства и вызову соответствующих call-back-ов).
 * Дочерние классы должны записывать текущее время в lastActivityTime в определяемых ими контрольных точках Watchdog-а.
 *  
 * @author Антон Васильев
 *
 */
public abstract class RubezhUSBTransportBase implements Transport<ByteBuffer> {

    /* Методы для обязательного переопределения */

    /**
     * Выполнение итерации подключения.
     * 
     * Вызывается из общей фоновой процедуры, если был запрос на подключение (connectHandler != null) и не был
     * установлен флаг о необходимости отключения (needCloseDevice == false).
     * 
     * @param currentTimeMs текущее время
     */
    abstract protected void processConnect(long currentTimeMs);

    /**
     * Выполнение итерации по отправке данных.
     * 
     * Вызывается из общей фоновой процедуры.
     * 
     * @param currentTimeMs текущее время
     */
    abstract protected void processSend(long currentTimeMs);

    /**
     * Выполнение итерации по обработке принятых данных.
     * 
     * Вызывается из общей фоновой процедуры.
     * 
     * @param currentTimeMs текущее время
     */
    abstract protected void processReceive(long currentTimeMs);

    /**
     * Запоминание (буферизация) принятого с USB, но не отданного получателю пакета данных.
     * 
     * @param currentTimeMs текущее время
     * @param receivedData полученный по USB и расшифрованный (убраны маркеры и экранирующие символы) пакет данных
     */
    abstract protected void rememberReceivedPacket(long currentTimeMs, ByteBuffer receivedData);

    /**
     * Кол-во байт служебной информации в USB-репорте.
     * 
     * @return
     */
    abstract protected int getServiceBytesCount();

    /**
     * Обработка байта служебной информации.
     * 
     * @param byteNo номер байта в USB-репорте
     * @param byteValue значение байта
     */
    abstract protected void handleServiceByte(int byteNo, byte byteValue);

    /**
     * Базовый конструктор.
     * 
     * @param logger логгер
     * @param hidDeviceScanner сканер HID-устройств
     * @param vendorId идентификатор вендора ожидаемого HID-устройства
     * @param productId идентификатор изделия ожидаемого HID-устройства
     * @param serialNumber серийный номер ожидаемого HID-устройства (может быть null)
     * @param product название изделия ожидаемого HID-устройства
     */
    public RubezhUSBTransportBase(Logger logger, HidDeviceScanner hidDeviceScanner, int vendorId, int productId,
            String serialNumber, String product) {
        this.logger = logger;
        this.hidDeviceScanner = hidDeviceScanner;
        this.vendorId = vendorId;
        this.productId = productId;
        this.serialNumber = serialNumber;
        this.product = product;
    }

    /* Конфигурация */

    private int maxWaitReceiveTimeMs = 10000;
    private int maxWaitSendTimeMs = 60000;

    public int getMaxWaitReceiveTimeMs() {
        return maxWaitReceiveTimeMs;
    }

    public void setMaxWaitReceiveTimeMs(int maxWaitReceiveTimeMs) {
        this.maxWaitReceiveTimeMs = maxWaitReceiveTimeMs;
    }

    public int getMaxWaitSendTimeMs() {
        return maxWaitSendTimeMs;
    }

    public void setMaxWaitSendTimeMs(int maxWaitSendTimeMs) {
        this.maxWaitSendTimeMs = maxWaitSendTimeMs;
    }

    /* Общесистемные компоненты (бины) */

    protected Logger logger;
    protected HidDeviceScanner hidDeviceScanner;

    /* Общие константы транспортного уровня */

    protected static final int PACKET_LENGTH = 64;
    protected static final byte PACKET_BEGIN_MARKER = 0x7E;
    protected static final byte PACKET_END_MARKER = 0x3E;
    protected static final byte BEGIN_ESCAPE_MARKER = 0x7D;
    protected static final byte END_ESCAPE_MARKER = 0x3D;
    protected static final byte ESCAPE_XOR_MASK = 0x20;
    /** Маска для проверки в байте служебной информации одного из режимов: ID по USB или ID по RS485 */
    protected static final byte ID_MODE_MASK = 0x60;

    /* Параметры идентификации экземпляра транспорта */

    protected int vendorId;
    protected int productId;
    protected String serialNumber;
    protected String product;

    /* Watchdog */

    /** Время последней полезной операции */
    protected long lastActivityTime;

    @Override
    public long getLastActivityTime() {
        return lastActivityTime;
    }

    /* Подключение */

    protected HidDevice hidDevice;
    protected boolean needCloseDevice = false;

    protected ConnectHandler connectHandler = null;
    protected DisconnectHandler disconnectHandler = null;

    /**
     * Запрос на подключение к HID-устройству.
     *
     * Возможен только один источник запросов на подключение, и не допустимо
     * выполнять сразу несколько попыток подключения. Т.е., пока по одному
     * запросу не получен ответ о результатах подключения, другие запросы будут
     * считаться ошибкой программы. Также не допустимо выполнять запрос на
     * подключение, если оно уже было успешно выполнено и не было потери связи.
     */
    @Override
    public void connect(long currentTimeMs, ConnectHandler connectHandler,
            DisconnectHandler disconnectHandler) {
        lastActivityTime = currentTimeMs;
        assert this.connectHandler == null;
        assert hidDevice == null || !hidDevice.isOpen();

        this.connectHandler = connectHandler;
        this.disconnectHandler = disconnectHandler;
    }

    private boolean checkHidDeviceProperties(HidDevice hidDevice) {
        assert (hidDevice != null);

        return (vendorId == 0 || hidDevice.getVendorId() == vendorId)
                && (productId == 0 || hidDevice.getProductId() == productId)
                && (serialNumber == null || serialNumber.isEmpty() || serialNumber.equals(hidDevice.getSerialNumber()))
                && (product == null || product.equals(hidDevice.getProduct()));
    }

    protected void checkAttachedDevices() {
        List<HidDevice> devices = hidDeviceScanner.getAttachedHidDevices();
        for (HidDevice hidDevice : devices) {
            if (hidDevice != null && checkHidDeviceProperties(hidDevice)) {
                this.hidDevice = hidDevice;
                break;
            }
        }
    }

    /* Отправка данных */

    protected void encodeAndPutDataByte(byte dataByte, ByteBuffer dataToSend) {
        switch (dataByte) {
        case PACKET_BEGIN_MARKER:
        case BEGIN_ESCAPE_MARKER:
            dataToSend.put(BEGIN_ESCAPE_MARKER);
            dataToSend.put((byte) (dataByte ^ ESCAPE_XOR_MASK));
            break;
        case PACKET_END_MARKER:
        case END_ESCAPE_MARKER:
            dataToSend.put(END_ESCAPE_MARKER);
            dataToSend.put((byte) (dataByte ^ ESCAPE_XOR_MASK));
            break;
        default:
            dataToSend.put(dataByte);
            break;
        }
    }

    protected ByteBuffer getEncodedData(ByteBuffer rawData) {
        ByteBuffer encodedData = ByteBuffer.allocateDirect(rawData.remaining() * 2 + 5);
        /*
         * Кодируем данные для отправки, включая маркеры начала и конца и
         * экранируемые байты
         */
        encodedData.put(PACKET_BEGIN_MARKER);
        while (rawData.hasRemaining())
            encodeAndPutDataByte(rawData.get(), encodedData);
        encodedData.put(PACKET_END_MARKER);
        /*
         * Добавляем 3 байта с мусором, т.к. у HID-драйвер какие-то проблемы с
         * отправкой одиночного байта, а такие ситуации случаются (когда 3e
         * оказывается в начале нового пакета)
         */
        encodedData.put((byte) (0xFF & 0xFF));
        encodedData.put((byte) (0xFF & 0xFF));
        encodedData.put((byte) (0xFF & 0xFF));
        encodedData.flip();

        return encodedData;
    }

    protected static byte reportId = (byte) 0x00;
    static {
        if (Platform.isWindows()) {
            String osVersion = System.getProperty("os.version");
            if (osVersion.equals("6.2") || osVersion.equals("6.3") || osVersion.equals("10.0")) {
                /*
                 * На Windows 8 и 10 report id должен быть не равен нулю, т.к. в
                 * HID4JAVA в этом случае не добавляется этот байт и данные не
                 * отправляются либо отправляются с ошибкой.
                 */
                reportId = (byte) 0x01;
            }
        }
    }

    protected boolean sendPacket(ByteBuffer packet) {
        boolean sendingFailed = false;
        byte[] sendBuffer = new byte[PACKET_LENGTH];
        while (packet.hasRemaining()) {
            int length = packet.remaining() > sendBuffer.length ? sendBuffer.length
                    : packet.remaining();
            packet.get(sendBuffer, 0, length);
            int nSendedBytes = hidDevice.write(sendBuffer, length, reportId);
            if (nSendedBytes < 0) {
                logger.error("Failed sending message ({})", hidDevice.getLastErrorMessage());
                needCloseDevice = true;
                sendingFailed = true;
                break;
            }
        }
        return !sendingFailed;
    }

    /* Получение данных */

    protected ByteBuffer receivingData;
    protected boolean receivingEscape;
    protected byte receiveBuffer[] = new byte[PACKET_LENGTH * 100];

    protected void receivePackets(long currentTimeMs) {
        assert hidDevice != null;

        int val = hidDevice.read(receiveBuffer);
        while (val > 0) {
            int i = 0;
            /* Проверка сервисных байтов */
            for (; i < val && i < getServiceBytesCount(); ++i) {
                handleServiceByte(i, receiveBuffer[i]);
            }
            /* Поиск и декодирование данных */
            for (; i < val && i < receiveBuffer.length; ++i) {
                if (receivingData == null) {
                    if (receiveBuffer[i] == PACKET_BEGIN_MARKER) {
                        receivingData = ByteBuffer.allocateDirect(PACKET_LENGTH * 5);
                        receivingEscape = false;
                    }
                } else {
                    try {
                        if (receivingEscape) {
                            receivingData.put((byte) (receiveBuffer[i] ^ ESCAPE_XOR_MASK));
                            receivingEscape = false;
                        } else {
                            switch (receiveBuffer[i]) {
                            case BEGIN_ESCAPE_MARKER:
                            case END_ESCAPE_MARKER:
                                receivingEscape = true;
                                break;
                            case PACKET_END_MARKER:
                                receivingData.flip();
                                rememberReceivedPacket(currentTimeMs, receivingData);
                                receivingData = null;
                                break;
                            case PACKET_BEGIN_MARKER:
                                logger.warn(
                                        "Begin marker is in the middle of packet, possibly some reports were missed. Starting packet collection again");
                                receivingData.clear();
                                break;
                            default:
                                receivingData.put(receiveBuffer[i]);
                                break;
                            }
                        }
                    } catch (BufferOverflowException e) {
                        logger.error("Too big packet received ({}), truncating", receivingData);
                        receivingData.flip();
                        rememberReceivedPacket(currentTimeMs, receivingData);
                        receivingData = null;
                    }
                }
            }
            val = hidDevice.read(receiveBuffer);
        }
        if (val < 0) {
            logger.error("Read error (HID device: {}), message: {}", hidDevice, hidDevice.getLastErrorMessage());
            needCloseDevice = true;
        }
    }

    /* Отключение */

    @Override
    public void disconnect(long currentTimeMs) throws IOException {
        lastActivityTime = currentTimeMs;
        needCloseDevice = true;
    }

    protected void processDisconnect(long currentTimeMs) {
        assert needCloseDevice;

        if (hidDevice != null && hidDevice.isOpen())
            hidDevice.close();
        hidDevice = null;
        if (connectHandler != null) {
            ConnectHandler connectHandler = this.connectHandler;
            this.connectHandler = null;
            connectHandler.onConnected(currentTimeMs, false);
        } else if (disconnectHandler != null) {
            DisconnectHandler disconnectHandler = this.disconnectHandler;
            this.disconnectHandler = null;
            disconnectHandler.onDisconnected(currentTimeMs);
        }
    }

    /* Общая фоновая процедура */

    @Override
    public void process(long currentTimeMs) throws IOException {

        /* Далее выполняем чтение пакетов, в этом методе также определяется доступность устройства */
        if (!needCloseDevice && hidDevice != null && hidDevice.isOpen())
            receivePackets(currentTimeMs);

        if (needCloseDevice) {
            processDisconnect(currentTimeMs);
            needCloseDevice = false;
        } else
            processConnect(currentTimeMs);

        /*
         * Обработку запросов на прием и передачу делаем независимо от наличия подключения: если его нет, то инициатор
         * запроса должен получить уведомление о неуспехе
         */
        processSend(currentTimeMs);
        processReceive(currentTimeMs);
    }

}
