package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import ru.rubezh.firesec.nt.amqp.sender.DriverAlertSender;
import ru.rubezh.firesec.nt.domain.v1.ActiveDevice;
import ru.rubezh.firesec.nt.domain.v1.DeviceProfile;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler.StateHandler;
import ru.rubezh.firesec.nt.plugin.driver.clock.Clock;
import ru.rubezh.firesec.nt.plugin.driver.transport.Transport;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.codec.MessageCodec;

public interface StateMachineContext {
    public StateHandler getStateHandler();

    public ActionExecutor getActionExecutor();

    public DriverAlertSender getDriverAlertSender();

    public DeviceProfile getAdapterModuleDeviceProfile();

    public ActiveDevice getAdapterModuleDevice();

    public void setAdapterModuleDevice(ActiveDevice device);

    public List<ActiveDevice> getChildDevices();

    public int getLineNo();

    public Transport<ByteBuffer> getTransport();

    public Clock getClock();

    public MessageCodec getMessageCodec();

    public StateMachineHooks getStateMachineHooks();
    public void setStateMachineHooks(StateMachineHooks stateMachineHooks);

    public Logger getLogger();

    public int getReconnectInterval();

    public int getPingInterval();

    public int getReconfigInterval();

    public RS485Configuration generateLineConfiguration(int lineNo);

    public Map<String, DeviceProfile> getSupportedDeviceProfiles();
}
