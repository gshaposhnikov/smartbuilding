package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class CommonRS implements Message {
    private FunctionType functionType;
    private boolean isSuccess = true;
    private ExceptionType exceptionType = ExceptionType.NONE;
    
    public CommonRS(FunctionType functionType) {
        this.functionType = functionType;
    }
    
    @Override
    public DestinationType getDestinationType() {
        return DestinationType.AM;
    }

    @Override
    public FunctionType getFunctionType() {
        return functionType;
    }
    
    public boolean isSuccess() {
        return isSuccess;
    }
    
    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }
}
