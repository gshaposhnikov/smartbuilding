package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;

public class WaitReconfig extends AbstractConnected {
    @Override
    public State getCurrentState() {
        return State.WAIT_RECONFIG;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        WaitReconfig handler = new WaitReconfig();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }
    
    @Override
    public State onTimer(long currentTimeMs) {
        stateMachineContext.getActionExecutor().requestConfig(currentTimeMs, stateMachineContext.getLineNo());
        return State.WAIT_CONFIG;
    }
}
