package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class Adjustment extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.ADJUSTMENT;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        Adjustment handler = new Adjustment();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onExtendedStatusEnabled(long currentTimeMs) {
        stateMachineContext.getActionExecutor().requestConfig(currentTimeMs, stateMachineContext.getLineNo());
        return State.WAIT_CONFIG;
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error(
                "Adjustment failed, disconnecting (AM serial number: {}, lineNo: {}, exception: {})",
                stateMachineContext.getAdapterModuleDevice().getDeviceProject().getDevicePropertyValues()
                        .get(DeviceProperties.USBSerialNumber.toString()),
                stateMachineContext.getLineNo(), exception);
        stateMachineContext.getStateMachineHooks().onRS485ConfigureFailed(currentTimeMs,
                stateMachineContext.getLineNo(), exception);
        return onDisconnected(currentTimeMs);
    }

}
