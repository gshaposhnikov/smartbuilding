package ru.rubezh.firesec.nt.plugin.driver.transport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Транспорт-мультиплексор
 *
 * Реализует connect как подписку для нескольких ConnectHandler/DisconnectHandler
 * Перенаправляет onConnected/onDisconnected всем подписчикам
 * Перенаправляет read/write/disconnect/process в первичный транспорт
 */
public class MuxTransport<R> implements Transport<R>, Transport.ConnectHandler, Transport.DisconnectHandler {

    /* Первичный транспорт */
    Transport<R> transport;

    /* Соединение инициировано */
    boolean connecting = false;
    /* Соединение установлено */
    boolean connected = false;

    /* Подписчики на изменения статуса соединения */
    private List<ConnectHandler> connectHandlers = new ArrayList<>();
    private List<ConnectHandler> postponedConnectHandlers = new ArrayList<>();
    private List<DisconnectHandler> disconnectHandlers = new ArrayList<>();

    public MuxTransport(Transport<R> transport) {
        this.transport = transport;
    }

    @Override
    public void connect(long currentTimeMs, ConnectHandler connectHandler, DisconnectHandler disconnectHandler) {
        if (connected)
            postponedConnectHandlers.add(connectHandler);
        else {
            connectHandlers.add(connectHandler);
            if (!connecting) {
                connecting = true;
                transport.connect(currentTimeMs, this, this);
            }
        }
        disconnectHandlers.add(disconnectHandler);
    }

    @Override
    public void disconnect(long currentTimeMs) throws IOException {
        transport.disconnect(currentTimeMs);
    }

    @Override
    public int send(long currentTimeMs, R rawData, SendHandler sendHandler, SendReceiveFailedHandler failedHandler) {
        return transport.send(currentTimeMs, rawData, sendHandler, failedHandler);
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<R> receiveHandler,
            SendReceiveFailedHandler failedHandler) {
        transport.receive(currentTimeMs, sequenceNo, receiveHandler, failedHandler);
    }

    @Override
    public void receive(long currentTimeMs, int sequenceNo, ReceiveHandler<R> receiveHandler,
            SendReceiveFailedHandler failedHandler, SendReceiveFailedHandler timeoutHandler) {
        transport.receive(currentTimeMs, sequenceNo, receiveHandler, failedHandler, timeoutHandler);
    }

    @Override
    public void process(long currentTimeMs) throws IOException {
        for (ConnectHandler connectHandler : postponedConnectHandlers)
            connectHandler.onConnected(currentTimeMs, true);
        postponedConnectHandlers.clear();
        transport.process(currentTimeMs);
    }

    @Override
    public long getLastActivityTime() {
        return transport.getLastActivityTime();
    }

    @Override
    public void onConnected(long currentTimeMs, boolean isSuccess) {
        for (ConnectHandler connectHandler : connectHandlers)
            connectHandler.onConnected(currentTimeMs, isSuccess);
        connectHandlers.clear();
        connecting = false;
        if (isSuccess)
            connected = true;
        else
            disconnectHandlers.clear();
    }

    @Override
    public void onDisconnected(long currentTimeMs) {
        for (DisconnectHandler disconnectHandler : disconnectHandlers)
            disconnectHandler.onDisconnected(currentTimeMs);
        disconnectHandlers.clear();
        connected = false;
    }

}
