package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.List;

public class RS485Configuration {
    public class DeviceDescriptor implements Comparable<DeviceDescriptor> {
        private int address;
        private int type;

        public int getAddress() {
            return address;
        }

        public void setAddress(int address) {
            this.address = address;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        @Override
        public int compareTo(DeviceDescriptor deviceDescriptor) {
            return address - deviceDescriptor.address;
        }

        @Override
        public boolean equals(Object obj) {
            boolean result = (obj != null) && (obj instanceof DeviceDescriptor);

            if (result) {
                DeviceDescriptor deviceDescriptor = (DeviceDescriptor)obj;

                result = (this.address == deviceDescriptor.getAddress())
                    && (this.type == deviceDescriptor.getType());
            }

            return result;
        }

        @Override
        public int hashCode(){
            return super.hashCode();
        }
    }

    private int adapterAddress;
    private int rate;
    private List<DeviceDescriptor> devices;

    public int getAdapterAddress() {
        return adapterAddress;
    }

    public void setAdapterAddress(int adapterAddress) {
        this.adapterAddress = adapterAddress;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public List<DeviceDescriptor> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceDescriptor> devices) {
        this.devices = devices;
    }

    public boolean equal(RS485Configuration rs485Configuration) {
        boolean result = this.adapterAddress == rs485Configuration.getAdapterAddress()
            && this.rate == rs485Configuration.getRate()
            && this.devices.size() == rs485Configuration.getDevices().size();

        for (int i = 0; i < this.devices.size() && result; ++i)
            result = this.devices.get(i).equals(rs485Configuration.getDevices().get(i));

        return result;
    }
}
