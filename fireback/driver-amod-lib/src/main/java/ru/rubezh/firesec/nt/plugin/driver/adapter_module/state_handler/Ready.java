package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class Ready extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.READY;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        Ready handler = new Ready();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn("Get RS485 address doubling flags failed (AM serial number: {}, lineNo: {}, exception: {})",
                stateMachineContext.getAdapterModuleDevice().getDeviceProject().getDevicePropertyValues().get(DeviceProperties.USBSerialNumber.toString()),
                stateMachineContext.getLineNo(), exception);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getPingInterval());
        return getCurrentState();
    }

    @Override
    public State onTimer(long currentTimeMs) {
        stateMachineContext.getActionExecutor().requestAddressDoubling(currentTimeMs);
        return getCurrentState();
    }

    @Override
    public State onAddressDoublingFlagsReceived(long currentTimeMs, boolean[] addressDoublingFlags) {
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getPingInterval());
        if (addressDoublingFlags[stateMachineContext.getLineNo() - 1]) {
            stateMachineContext.getStateMachineHooks().onRS485AddressDoublingDetected(currentTimeMs, stateMachineContext.getLineNo());
            return State.ADDRESS_DOUBLING;
        }
        else
            return getCurrentState();
    }
}
