package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public enum ParameterType {
    deviceMode(0x01),
    RS485Line1Configuration(0x03),
    RS485Line2Configuration(0x04),
    firmwareVersion(0x12),
    serialNumber(0x32),
    RS485AddressesDoublingFlags(0x36),
    extendedStatusEnable(0x37),
    line1DevicesTypesList(0x14),
    line2DevicesTypesList(0x15);

    
    private final int value;
    private ParameterType(int value) {
        this.value = value;
    }

    private static final Map<Integer, ParameterType> typesByValue = new HashMap<>();
    static {
        for (ParameterType type : ParameterType.values()) {
            typesByValue.put(type.value, type);
        }
    }

    private static final EnumMap<ParameterType, Integer> valuesByType = new EnumMap<>(ParameterType.class);
    static {
        for (ParameterType type : ParameterType.values()) {
            valuesByType.put(type, type.value);
        }
    }

    public static ParameterType fromInteger(int value) {
        return typesByValue.get(value);
    }
    
    public static Integer toInteger(ParameterType type) {
        return valuesByType.get(type);
    }
}
