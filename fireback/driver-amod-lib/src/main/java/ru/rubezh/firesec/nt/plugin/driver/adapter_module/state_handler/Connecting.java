package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;

public class Connecting extends AbstractNone {

    @Override
    public State getCurrentState() {
        return State.CONNECTING;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        Connecting handler = new Connecting();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }
    
    @Override
    public State onConnectRequest(long currentTimeMs) {
        return getCurrentState();
    }

    @Override
    public State onConnected(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onConnected(currentTimeMs);
        stateMachineContext.getActionExecutor().enableExtendedStatus(currentTimeMs);
        return State.ADJUSTMENT;
    }

    @Override
    public State onConnectFailure(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onConnectFailure(currentTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getReconnectInterval());
        return State.WAIT_RECONNECT;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().disconnect(currentTimeMs);
        return State.IDLE;
    }
}
