package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.nio.ByteBuffer;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.CommonRS;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.GetRS485AddressesDoublingFlagsRS;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.GetRS485ConfigurationRS;

public class ActionExecutorStage3Impl extends ActionExecutorStage2Impl {

    public ActionExecutorStage3Impl(StateMachineContext stateMachineContext) {
        super(stateMachineContext);
    }

    @Override
    protected void responseGetRS485ConfigReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        if (requests.containsKey(sequenceNo)) {
            try {
                /* удаляем запрос из ожидаемых */
                requests.remove(sequenceNo);
                /* добываем ответ */
                CommonRS response = stateMachineContext.getMessageCodec().decodeGetRS485ConfigurationRS(rawData);
                if (response.isSuccess()) {
                    /* возвращаем полученную конфигурацию */
                    GetRS485ConfigurationRS getRS485ConfigurationRS = (GetRS485ConfigurationRS) response;
                    stateMachineContext.getStateHandler().onConfigReceived(currentTimeMs, getRS485ConfigurationRS.getRs485Configuration());
                }
                else {
                    /* возвращаем полученную исключительную ситуацию */
                    stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs, response.getExceptionType());
                }
            } catch (ClassCastException e) {
                stateMachineContext.getLogger().error(
                        "Wrong request or response type (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
            catch (NullPointerException e) {
                stateMachineContext.getLogger().error(
                        "Couldn't decode response (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
        }
    }

    @Override
    protected void responseSetRS485ConfigReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        if (checkResponse(currentTimeMs, getEmptySetParameterResponse(currentTimeMs, sequenceNo, rawData))) {
            /* подтверждаем успешное завершение конфигурирования */
            stateMachineContext.getStateHandler().onSetConfigConfirmed(currentTimeMs);
        }
    }

    private boolean checkResponse(long currentTimeMs, CommonRS response) {
        if (response == null)
            return false;
        if (response.isSuccess()) {
            /* подтверждаем успешное выполнение запроса */
            return true;
        } else {
            /* возвращаем полученную исключительную ситуацию */
            stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs, response.getExceptionType());
            return false;
        }
    }

    private CommonRS getEmptySetParameterResponse(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        if (requests.containsKey(sequenceNo)) {
            try {
                /* удаляем запрос из ожидаемых */
                requests.remove(sequenceNo);
                /* добываем ответ */
                return stateMachineContext.getMessageCodec().decodeSetAMParameterRS(rawData);
            } catch (ClassCastException e) {
                stateMachineContext.getLogger().error(
                        "Wrong request or response type (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
            catch (NullPointerException e) {
                stateMachineContext.getLogger().error(
                        "Couldn't decode response (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
        }
        return null;
    }

    @Override
    protected void responseGetRS485AddressesDoublingFlagsReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        if (requests.containsKey(sequenceNo)) {
            try {
                /* удаляем запрос из ожидаемых */
                requests.remove(sequenceNo);
                /* добываем ответ */
                CommonRS response = stateMachineContext.getMessageCodec().decodeGetRS485AddressDoublingFlagsRS(rawData);
                if (response.isSuccess()) {
                    /* возвращаем полученные флаги дублирования адресов */
                    GetRS485AddressesDoublingFlagsRS getRS485AddressesDoublingFlagsRS = (GetRS485AddressesDoublingFlagsRS) response;
                    stateMachineContext.getStateHandler().onAddressDoublingFlagsReceived(currentTimeMs, getRS485AddressesDoublingFlagsRS.getDoublingFlags());
                }
                else {
                    /* возвращаем полученную исключительную ситуацию */
                    stateMachineContext.getStateHandler().onExceptionReceived(currentTimeMs, response.getExceptionType());
                }
            }
            catch(ClassCastException e) {
                stateMachineContext.getLogger().error(
                        "Wrong request or response type (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
            catch (NullPointerException e) {
                stateMachineContext.getLogger().error(
                        "Couldn't decode response (exception message: {}, adapter module address path: {})",
                        e.getMessage(), stateMachineContext.getAdapterModuleDevice().getAddressPath());
                stateMachineContext.getStateHandler().onDisconnected(currentTimeMs);
            }
        }
    }

    @Override
    protected void responseEnableExtendedStatusReceived(long currentTimeMs, int sequenceNo, ByteBuffer rawData) {
        if (checkResponse(currentTimeMs, getEmptySetParameterResponse(currentTimeMs, sequenceNo, rawData))) {
            /* подтверждаем успешное включение режима расширенного статуса */
            stateMachineContext.getStateHandler().onExtendedStatusEnabled(currentTimeMs);
        }
    }

}
