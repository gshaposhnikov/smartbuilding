package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;

public class WaitConfig extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.WAIT_CONFIG;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        WaitConfig handler = new WaitConfig();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onConfigReceived(long currentTimeMs, RS485Configuration config) {
        RS485Configuration generatedConfig = stateMachineContext.generateLineConfiguration(stateMachineContext.getLineNo());
        if (generatedConfig.equal(config)) {
            stateMachineContext.getStateMachineHooks().onRS485Configured(currentTimeMs, stateMachineContext.getLineNo());
            stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getPingInterval());
            return State.READY;
        }
        else {
            stateMachineContext.getActionExecutor().setConfig(currentTimeMs, stateMachineContext.getLineNo(), generatedConfig);
            return State.SETTING_CONFIG;
        }
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().warn(
                "Get RS485 configuration failed (AM serial number: {}, lineNo: {}, exception: {})",
                stateMachineContext.getAdapterModuleDevice().getDeviceProject().getDevicePropertyValues().get(DeviceProperties.USBSerialNumber.toString()),
                stateMachineContext.getLineNo(), exception);
        RS485Configuration generatedConfig = stateMachineContext.generateLineConfiguration(stateMachineContext.getLineNo());
        stateMachineContext.getActionExecutor().setConfig(currentTimeMs, stateMachineContext.getLineNo(), generatedConfig);
        return State.SETTING_CONFIG;
    }
}
