package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;

public abstract class AbstractConnected extends AbstractNone {
    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public State onDisconnectRequest(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetRequests(currentTimeMs);
        stateMachineContext.getActionExecutor().resetTimer(currentTimeMs);
        stateMachineContext.getActionExecutor().disconnect(currentTimeMs);
        return State.IDLE;
    }

    @Override
    public State onDisconnected(long currentTimeMs) {
        stateMachineContext.getActionExecutor().resetRequests(currentTimeMs);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getReconnectInterval());
        stateMachineContext.getStateMachineHooks().onDisconnected(currentTimeMs);
        return State.WAIT_RECONNECT;
    }
}
