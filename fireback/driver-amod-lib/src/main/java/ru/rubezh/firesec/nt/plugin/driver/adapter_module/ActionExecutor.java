package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.RS485Configuration;

public interface ActionExecutor {
    public void connect(long currentTimeMs);
    public void disconnect(long currentTimeMs);
    public void requestConfig(long currentTimeMs, int lineNo);
    public void setConfig(long currentTimeMs, int lineNo, RS485Configuration config);
    public void requestAddressDoubling(long currentTimeMs);
    public void resetRequests(long currentTimeMs);
    public void enableExtendedStatus(long currentTimeMs);
    public void setTimer(long currentTimeMs, int interval);
    public void resetTimer(long currentTimeMs);
}
