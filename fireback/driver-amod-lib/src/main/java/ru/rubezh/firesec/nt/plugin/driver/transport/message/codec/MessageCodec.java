package ru.rubezh.firesec.nt.plugin.driver.transport.message.codec;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;

public interface MessageCodec {
    public ByteBuffer encodeForAM(GetParameterRQ message);
    public ByteBuffer encodeForAM(SetRS485ConfigurationRQ message);
    public ByteBuffer encodeForAM(EnableExtendedStatusRQ message);
    public ByteBuffer encodeForLine(RS485ProxyRQ message);

    public DestinationType getDestinationTypeFromEncoded(ByteBuffer encodedMessage);

    public RS485ProxyRS decodeRS485ProxyRS(ByteBuffer byteBuffer) throws BufferUnderflowException;

    /**
     * Получить из байтового буфера объект с ответом на запрос конфигурации линии RS485
     *
     * @param byteBuffer байтовый буфер
     * @return
     * <p>
     * объект CommonRS, если в буфере - ответ с исключительной ситуацией
     * </p>
     * <p>
     * объект GetRS485ConfigurationRS, если в буфере - ожидаемый нормальный ответ
     * </p>
     * <p>
     * null - если поле маршрута или поле с кодом функции не соответствуют ожиданиям
     * </p>
     * @throws BufferUnderflowException если при разборе буфера произошел неожиданный выход за его границы
     */
    public CommonRS decodeGetRS485ConfigurationRS(ByteBuffer byteBuffer) throws BufferUnderflowException;

    /**
     * Получить из байтового буфера объект с ответом на запрос установки параметра МС
     *
     * @param byteBuffer байтовый буфер
     * @return
     * <p>
     * объект CommonRS, если в буфере - ожидаемый ответ (с или без исключительной ситуации)
     * </p>
     * <p>
     * null - если поле маршрута или поле с кодом функции не соответствуют ожиданиям
     * </p>
     * @throws BufferUnderflowException если при разборе буфера произошел неожиданный выход за его границы
     */
    public CommonRS decodeSetAMParameterRS(ByteBuffer byteBuffer) throws BufferUnderflowException;

    /**
     * Получить из байтового буфера объект с ответом на запрос конфигурации линии RS485
     *
     * @param byteBuffer байтовый буфер
     * @return
     * <p>
     * объект CommonRS, если в буфере - ответ с исключительной ситуацией
     * </p>
     * <p>
     * объект GetRS485AddressDoublingFlagsRS, если в буфере - ожидаемый нормальный ответ
     * </p>
     * <p>
     * null - если поле маршрута или поле с кодом функции не соответствуют ожиданиям
     * </p>
     * @throws BufferUnderflowException если при разборе буфера произошел неожиданный выход за его границы
     */
    public CommonRS decodeGetRS485AddressDoublingFlagsRS(ByteBuffer byteBuffer) throws BufferUnderflowException;
}
