package ru.rubezh.firesec.nt.plugin.driver.transport.message;

public class GetRS485ConfigurationRQ extends GetParameterRQ {

    private int lineNo;
    
    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    
    @Override
    public DestinationType getDestinationType() {
        return DestinationType.AM;
    }

    @Override
    public ParameterType getParameterType() {
        switch (lineNo) {
        case 1:
            return ParameterType.RS485Line1Configuration;
        default:
            return ParameterType.RS485Line2Configuration;
        }
    }
}
