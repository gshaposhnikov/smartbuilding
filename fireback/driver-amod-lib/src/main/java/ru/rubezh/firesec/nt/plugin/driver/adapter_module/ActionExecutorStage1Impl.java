package ru.rubezh.firesec.nt.plugin.driver.adapter_module;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import ru.rubezh.firesec.nt.plugin.driver.transport.message.*;

abstract public class ActionExecutorStage1Impl implements ActionExecutor {

    abstract protected void connected(long currentTimeMs, boolean isSuccess);
    abstract protected void disconnected(long currentTimeMs);
    abstract protected void sendReceiveFailed(long currentTimeMs, int sequenceNo);
    abstract protected void requestGetRS485ConfigSended(long currentTimeMs, int sequenceNo);
    abstract protected void requestSetRS485ConfigSended(long currentTimeMs, int sequenceNo);
    abstract protected void timerHandler(long currentTimeMs);
    abstract protected void requestGetRS485AddressesDoublingFlagsSended(long currentTimeMs, int sequenceNo);
    abstract protected void requestEnableExtendedStatusSended(long currentTimeMs, int sequenceNo);

    protected StateMachineContext stateMachineContext;
    protected Map<Integer, Message> requests = new HashMap<>();
    
    
    
    public ActionExecutorStage1Impl(StateMachineContext stateMachineContext) {
        this.stateMachineContext = stateMachineContext;
    }
    
    @Override
    public void connect(long currentTimeMs) {
        stateMachineContext.getTransport().connect(currentTimeMs, this::connected, this::disconnected);
    }

    @Override
    public void disconnect(long currentTimeMs) {
        try {
            stateMachineContext.getTransport().disconnect(currentTimeMs);
        }
        catch (IOException e) {
            stateMachineContext.getLogger().error("IO exception with message: {}", e.getMessage());
        }
    }

    @Override
    public void requestConfig(long currentTimeMs, int lineNo) {
        GetRS485ConfigurationRQ request = new GetRS485ConfigurationRQ();
        request.setLineNo(lineNo);
        ByteBuffer rawRequest = stateMachineContext.getMessageCodec().encodeForAM(request);
        rawRequest.flip();
        requests.put(stateMachineContext.getTransport().send(currentTimeMs, rawRequest, this::requestGetRS485ConfigSended, this::sendReceiveFailed), request);
    }

    @Override
    public void setConfig(long currentTimeMs, int lineNo, RS485Configuration config) {
        SetRS485ConfigurationRQ request = new SetRS485ConfigurationRQ();
        request.setLineNo(lineNo);
        request.setRs485Configuration(config);
        ByteBuffer rawRequest = stateMachineContext.getMessageCodec().encodeForAM(request);
        rawRequest.flip();
        requests.put(stateMachineContext.getTransport().send(currentTimeMs, rawRequest, this::requestSetRS485ConfigSended, this::sendReceiveFailed), request);
    }

    @Override
    public void setTimer(long currentTimeMs, int interval) {
        stateMachineContext.getClock().setTimer(currentTimeMs, interval, this::timerHandler);
    }

    @Override
    public void requestAddressDoubling(long currentTimeMs) {
        GetRS485AddressesDoublingFlagsRQ request = new GetRS485AddressesDoublingFlagsRQ();
        ByteBuffer rawRequest = stateMachineContext.getMessageCodec().encodeForAM(request);
        rawRequest.flip();
        requests.put(stateMachineContext.getTransport().send(currentTimeMs, rawRequest, this::requestGetRS485AddressesDoublingFlagsSended, this::sendReceiveFailed), request);
    }

    @Override
    public void enableExtendedStatus(long currentTimeMs) {
        EnableExtendedStatusRQ request = new EnableExtendedStatusRQ();
        ByteBuffer rawRequest = stateMachineContext.getMessageCodec().encodeForAM(request);
        rawRequest.flip();
        requests.put(stateMachineContext.getTransport().send(currentTimeMs, rawRequest,
                this::requestEnableExtendedStatusSended, this::sendReceiveFailed), request);
    }

    @Override
    public void resetRequests(long currentTimeMs) {
        requests.clear();
    }

    @Override
    public void resetTimer(long currentTimeMs) {
        stateMachineContext.getClock().resetAllTimers();
    }

}
