package ru.rubezh.firesec.nt.plugin.driver.transport.message;

import java.nio.ByteBuffer;

public class RS485ProxyRS {
    private DestinationType destinationType;
    private int rs485Address;
    private ByteBuffer functionContextData;
    private ExceptionType exception;

    public RS485ProxyRS(DestinationType destinationType) {
        this.destinationType = destinationType;
    }

    public DestinationType getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(DestinationType destinationType) {
        this.destinationType = destinationType;
    }

    public int getRs485Address() {
        return rs485Address;
    }

    public void setRs485Address(int rs485Address) {
        this.rs485Address = rs485Address;
    }

    public ByteBuffer getFunctionContextData() {
        return functionContextData;
    }

    public void setFunctionContextData(ByteBuffer functionContextData) {
        this.functionContextData = functionContextData;
    }

    public ExceptionType getException() {
        return exception;
    }

    public void setException(ExceptionType exception) {
        this.exception = exception;
    }
}
