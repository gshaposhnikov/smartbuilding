package ru.rubezh.firesec.nt.plugin.driver.adapter_module.state_handler;

import ru.rubezh.firesec.nt.domain.v1.DeviceProperties;

import ru.rubezh.firesec.nt.plugin.driver.adapter_module.State;
import ru.rubezh.firesec.nt.plugin.driver.adapter_module.StateMachineContext;
import ru.rubezh.firesec.nt.plugin.driver.transport.message.ExceptionType;

public class SettingConfig extends AbstractConnected {

    @Override
    public State getCurrentState() {
        return State.SETTING_CONFIG;
    }

    @Override
    public StateHandler cloneBinded(StateMachineContext stateMachineContext) {
        SettingConfig handler = new SettingConfig();
        handler.stateMachineContext = stateMachineContext;
        return handler;
    }

    @Override
    public State onSetConfigConfirmed(long currentTimeMs) {
        stateMachineContext.getStateMachineHooks().onRS485Configured(currentTimeMs, stateMachineContext.getLineNo());
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getPingInterval());
        return State.READY;
    }

    @Override
    public State onExceptionReceived(long currentTimeMs, ExceptionType exception) {
        stateMachineContext.getLogger().error(
                "Set RS485 configuration failed (AM serial number: {}, lineNo: {}, exception: {})",
                stateMachineContext.getAdapterModuleDevice().getDeviceProject().getDevicePropertyValues().get(DeviceProperties.USBSerialNumber.toString()),
                stateMachineContext.getLineNo(), exception);
        stateMachineContext.getStateMachineHooks().onRS485ConfigureFailed(currentTimeMs, stateMachineContext.getLineNo(), exception);
        stateMachineContext.getActionExecutor().setTimer(currentTimeMs, stateMachineContext.getReconfigInterval());
        return State.WAIT_RECONFIG;
    }
}
