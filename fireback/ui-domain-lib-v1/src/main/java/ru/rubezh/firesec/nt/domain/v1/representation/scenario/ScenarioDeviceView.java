package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.DeviceView;
import ru.rubezh.firesec.nt.domain.v1.Media;
import ru.rubezh.firesec.nt.domain.v1.representation.TreeItemDeviceView;

/**
 * Минимальное отображение устройства для построения отображения сценария
 */
public class ScenarioDeviceView {
    /** Идентификатор */
    private String id;
    /** Название */
    private String name;
    /** Иконка */
    private Media iconMedia;
    /** средний адресный путь в дереве устройств */
    private String middleAddressPath;
    /** короткий адресный путь в дереве устройств */
    private String shortAddressPath;

    public ScenarioDeviceView(TreeItemDeviceView deviceView) {
        id = deviceView.getId();
        name = deviceView.getName();
        iconMedia = deviceView.getIconMedia();
        middleAddressPath = deviceView.getMiddleAddressPath();
        shortAddressPath = deviceView.getShortAddressPath();
    }

    public ScenarioDeviceView(DeviceView deviceView) {
        id = deviceView.getId();
        name = deviceView.getName();
        iconMedia = deviceView.getIconMedia();
        middleAddressPath = deviceView.getMiddleAddressPath();
        shortAddressPath = deviceView.getShortAddressPath();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Media getIconMedia() {
        return iconMedia;
    }

    public String getShortAddressPath() {
        return shortAddressPath;
    }

    public String getMiddleAddressPath() {
        return middleAddressPath;
    }
}
