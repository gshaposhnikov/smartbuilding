package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип отображаемого контента в виджете "окно".
 * 
 * @author Александр Горячкин
 */
public enum WidgetContentType {
    /** Дерево устройств */
    deviceTree,
    /** Состояние устройства */
    deviceStatus,
    /** Информация о пользователе события */
    eventUserInfo,
    /** Журнал событий */
    eventLog,
    /** Подробная информация о событии */
    eventRecordInfo,
    /** Очередь сервера */
    serverQueue,
    /** Планы */
    plans,
    /** Отключенные устройства */
    disabledDevices,
    /** Параметры устройств */
    options,
    /** Сценарии */
    scripts,
    /** Индикатор */
    indicator,
    /** Персонал */
    staff,
    /** Список состояний, общих для всех подсистем */
    listStates,
    /** Компонент пожарная подсистема */
    fireSubsystem,
    /** Компонент охранная подсистема */
    securitySubsystem,
    /** Компонент СКУД */
    SKUDSubsystem,
    /** Компонент Автоматика */
    automationSubsystem,
    /** Закладка */
    bookmark,
    /** Разделитель */
    delimiter,
    /** Метка */
    label
}
