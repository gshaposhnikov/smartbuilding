package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.DeviceCategory;
import ru.rubezh.firesec.nt.domain.v1.ScenarioActionType.ActionEntityType;
import ru.rubezh.firesec.nt.domain.v1.ScenarioPurpose;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;

/**
 * Сборочное отображение типов действий сценариев, для REST-API.
 * @author Антон Васильев
 *
 */
public class AggregatedScenarioActionTypeView extends IdAndNameAndDescr {
    /** Тип сущности */
    private ActionEntityType entityType;
    /** Категория устройств, к которым применимо данное действие (если действие применимо к устройствам) */
    private DeviceCategory deviceCategory;
    /** Тип назначения сценария */
    private ScenarioPurpose scenarioPurpose;
    /** Подсистема, к которой применимо данное действие (если действие применимо к зонам) */
    private Subsystem subsystem;
    /** Значимость действия (нужно для установки действия по умолчанию) */
    private int severity = 0;

    public ActionEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(ActionEntityType entityType) {
        this.entityType = entityType;
    }

    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

}
