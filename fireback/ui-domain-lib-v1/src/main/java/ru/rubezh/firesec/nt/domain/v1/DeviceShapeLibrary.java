package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Библиотека шейпов устройств.
 * 
 * Набор шейпов для одного конкретного типа устройства с уникальными связями с состояниями в рамках этого набора,
 * обозначенный заданным пользователем именем, уникальным в рамках одного типа устройства. В редакторе планов для
 * каждого конкретного устройства пользователь может выбрать такую библиотеку (предварительно ее создав), и она будет
 * использоваться на планах в ОЗ вместо набора шейпов по умолчанию для данного устройства.
 * 
 * @author Антон Васильев
 * @ingroup UIDomain
 */
@Document(collection = "device_shape_libraries")
@TypeAlias(value = "DeviceShapeLibrary")
@CompoundIndexes({
    @CompoundIndex(name = "name_devprofid", def = "{'name':1, 'deviceProfileId':1, 'removed':1}", unique = true)
})
public class DeviceShapeLibrary extends ReferenceEntityView {

    /** Суффикс идентификатора библиотеки по умолчанию */
    public static final String DEFAULT_LIBRARY_POSTFIX = "_default_shlib";
    /** Название библиотеки по умолчанию */
    public static final String DEFAULT_NAME = "По умолчанию";

    /** Идентификатор профиля устройств (тип устройств) */
    private String deviceProfileId;

    /** Дата и время создания библиотеки */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date created = new Date();

    /** Дата и время последнего изменения библиотеки (имени или описания) */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updated = new Date(0);

    /** Подготовлена для удаления */
    @JsonIgnore
    private boolean removed = false;

    /** Встроенная библиотека (создаваемая миграцией, но не пользователем) */
    private boolean builtin = false;

    public void update(DeviceShapeLibrary deviceShapeLibrary) {
        setName(deviceShapeLibrary.getName());
        setDescription(deviceShapeLibrary.getDescription());
        setUpdated(new Date());
    }

    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isBuiltin() {
        return builtin;
    }

    public void setBuiltin(boolean builtin) {
        this.builtin = builtin;
    }

}
