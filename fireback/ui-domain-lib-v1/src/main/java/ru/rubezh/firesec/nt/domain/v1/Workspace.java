package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Рабочее пространство.
 * 
 * Определяет вид окна "оперативная задача", размеры иконок, масштаб отображения плана. Состоит из виджетов.
 * 
 * @author Александр Горячкин
 * @todo Рабочее пространство сделать вложенным объектом в сущность пользователя.
 */
@Document(collection = "workspaces")
@TypeAlias(value = "Workspace")
public class Workspace {

    @Id private String id;

    /** название */
    private String name;
    /** масштаб иконок */
    private double iconZoom = 1;
    /** масштаб плана */
    private double planZoom = 1;

    /** список виджетов */
    private List<Widget> widgets = new ArrayList<>();

    /** проект */
    @JsonIgnore private String projectId = "";
    @Transient private Project project;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getIconZoom() {
        return iconZoom;
    }

    public void setIconZoom(double iconZoom) {
        this.iconZoom = iconZoom;
    }

    public double getPlanZoom() {
        return planZoom;
    }

    public void setPlanZoom(double planZoom) {
        this.planZoom = planZoom;
    }

    public List<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
