package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Базовый класс для определения отображений справочных сущностей, содержащих название и описание.
 * 
 * @author Антон Васильев
 *
 */
public class ReferenceEntityView extends ReferenceEntity {
    /** Название */
    @Indexed
    private String name = "";
   
    /** Описание */
    private String description = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
