package ru.rubezh.firesec.nt.domain.v1;

/**
 * Виджет "окно".
 * 
 * Представляет собой окно с заданным типом содержимого.
 * 
 * @author Александр Горячкин
 */
public class WidgetWindow extends Widget {

    /** тип выводимой информации */
    private WidgetContentType contentType;

    public WidgetContentType getContentType() {
        return contentType;
    }

    public void setContentType(WidgetContentType contentType) {
        this.contentType = contentType;
    }
}
