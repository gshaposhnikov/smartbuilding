package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.List;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import  ru.rubezh.firesec.nt.domain.v1.VirtualState;
import  ru.rubezh.firesec.nt.domain.v1.representation.scenario.AggregatedScenarioView;
import  ru.rubezh.firesec.nt.domain.v1.RegionView;

/**
 * Отображение сущностей прибора
 * 
 * @author Андрей Лисовой
 *
 */
public class ControlDeviceEntitiesView {
    /** Отображения устройств (включая прибор) */
    private List<TreeItemDeviceView> devices;
    /** Виртуальные состояния */
    private List<VirtualState> virtualStates;
    /** Сценарии */
    private List<AggregatedScenarioView> scenarios;
    /** Зоны */
    private List<RegionView> regions;
    /** Дата создания */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private Date created;

    public ControlDeviceEntitiesView(
            List<TreeItemDeviceView> devices,
            List<VirtualState> virtualStates,
            List<AggregatedScenarioView> scenarios,
            List<RegionView> regions,
            Date created) {
        this.devices = devices;
        this.virtualStates = virtualStates;
        this.scenarios = scenarios;
        this.regions = regions;
        this.created = created;
    }

    public List<TreeItemDeviceView> getDevices() {
        return devices;
    }

    public void setDevices(List<TreeItemDeviceView> devices) {
        this.devices = devices;
    }

    public List<VirtualState> getVirtualStates() {
        return virtualStates;
    }

    public void setVirtualStates(List<VirtualState> virtualStates) {
        this.virtualStates = virtualStates;
    }

    public List<AggregatedScenarioView> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<AggregatedScenarioView> scenarios) {
        this.scenarios = scenarios;
    }

    public List<RegionView> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionView> regions) {
        this.regions = regions;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
