package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.ArrayList;
import java.util.List;

/**
 * Справочник перечисляемых данных функционала зон.
 * @author Александр Горячкин
 *
 */
public class AggregatedRegionDictionary {
    /** Типы назначения зон */
    private List<IdAndNameAndDescr> securityTypes = new ArrayList<>();

    public List<IdAndNameAndDescr> getSecurityTypes() {
        return securityTypes;
    }

    public void setSecurityTypes(List<IdAndNameAndDescr> securityTypes) {
        this.securityTypes = securityTypes;
    }
}
