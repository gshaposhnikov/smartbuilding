package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Отображение журнала событий.
 * 
 * Используется для хранения параметров выборки и отображения событий в режиме реального времени.
 * 
 * @author Александр Горячкин, Антон Караваев
 * @ingroup UIDomain
 */
@Document(collection = "log_views")
@TypeAlias(value = "LogView")
public class LogView extends BasicEntityWithDate{

    /** название */
    @Indexed
    private String name = "";
    /** кол-во событий */
    private int recordCount;
    /** период (число дней) */
    private int nLastDays;
    /** статус(вкл/выкл)*/
    private boolean active = true;
    /** порядковый номер представления*/
    private int position;

    /** список подсистем */
    private List<Subsystem> subsystems = new ArrayList<>();

    /** список классов состояний */
    private List<String> stateCategoryIds = new ArrayList<>();

    public void update(LogView logView){
        this.updateDateTime = new Date();
        this.setName(logView.getName());
        this.setStateCategoryIds(logView.getStateCategoryIds());
        this.setActive(logView.isActive());
        this.setnLastDays(logView.getnLastDays());
        this.setRecordCount(logView.getRecordCount());
        this.setSubsystems(logView.getSubsystems());
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    public List<String> getStateCategoryIds() {
        return stateCategoryIds;
    }

    public void setStateCategoryIds(List<String> stateCategoryIds) {
        this.stateCategoryIds = stateCategoryIds;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Subsystem> getSubsystems() {
        return subsystems;
    }

    public void setSubsystems(List<Subsystem> subsystems) {
        this.subsystems = subsystems;
    }

    public int getnLastDays() {
        return nLastDays;
    }

    public void setnLastDays(int nLastDays) {
        this.nLastDays = nLastDays;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
