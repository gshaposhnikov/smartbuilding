package ru.rubezh.firesec.nt.domain.v1;

/**
 * Базовый класс, используемый для определения несамостоятельных сущностей, т.е. являющихся встроенными объектами.
 * 
 * @author Антон Васильев
 *
 */
public class AdjectiveEntityView extends AdjectiveEntity {
    /** Название */
    private String name = "";
    
    /** Описание */
    private String description = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
