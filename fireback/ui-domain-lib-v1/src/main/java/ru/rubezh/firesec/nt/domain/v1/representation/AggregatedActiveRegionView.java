package ru.rubezh.firesec.nt.domain.v1.representation;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.RegionView.RegionPlanLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Обобщённое отображение зоны для ОЗ.
 * @author Александр Горячкин
 */
public class AggregatedActiveRegionView {

    /** Зона */
    private Region region;
        /* Базовая информация о зоне */
    /** Идентификатор зоны */
    private String id;
    /** Название */
    private String name;
    /** Описание */
    private String description;

    /** Номер (в рамках проекта) */
    private int index;
    /** Подсистема */
    private Subsystem subsystem;

    /** Список расположений зоны на планах (помещениях) */
    List<RegionPlanLayout> planLayouts = new ArrayList<>();

        /* Охранная зона: */
    /** Текущее состояние постановки/снятия на охрану */
    private boolean onGuard;

        /* Активная зона */
    /** Текущий класс состояния общей подсистемы */
    private StateCategoryView generalStateCategoryView;

    private FilterTags filterTags = new FilterTags();

    public AggregatedActiveRegionView(){}

    public AggregatedActiveRegionView(RegionView regionView) {
        this.region = regionView.getRegion();
        this.id = regionView.getId();
        this.name = regionView.getName();
        this.description = regionView.getDescription();

        this.index = regionView.getRegion().getIndex();
        this.subsystem = regionView.getRegion().getSubsystem();
        this.planLayouts = regionView.getPlanLayouts();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public List<RegionPlanLayout> getPlanLayouts() {
        return planLayouts;
    }

    public void setPlanLayouts(List<RegionPlanLayout> planLayouts) {
        this.planLayouts = planLayouts;
    }

    public boolean isOnGuard() {
        return onGuard;
    }

    public void setOnGuard(boolean onGuard) {
        this.onGuard = onGuard;
    }

    public StateCategoryView getGeneralStateCategoryView() {
        return generalStateCategoryView;
    }

    public void setGeneralStateCategoryView(StateCategoryView generalStateCategoryView) {
        this.generalStateCategoryView = generalStateCategoryView;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

    public Region getRegion(){
        return region;
    }

    public void setRegion(Region region){
        this.region = region;
    }
}
