package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Отображение типа события.
 *
 * @author Антон Васильев.
 * @author Артем Седанов.
 * @ingroup UIReferences
 */
@Document(collection = "event_type_views")
@TypeAlias(value = "EventTypeView")
public class EventTypeView extends ReferenceEntityView {

}
