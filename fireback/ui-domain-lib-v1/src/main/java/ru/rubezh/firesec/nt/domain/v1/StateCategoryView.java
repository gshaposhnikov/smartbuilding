package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * Отображение класса состояния.
 * 
 * Используется для связи класса состояния и параметров отображения различных объектов, находящихся в нем.
 * 
 * @author Александр Горячкин, Антон Васильев
 * @ingroup UIReferences
 */
@Document(collection = "state_category_views")
@TypeAlias(value = "StateCategoryView")
public class StateCategoryView extends ReferenceEntityView {

    /** Цвет в формате "#RRGGBB"*/
    private String color;

    /** Цвет текста в формате "#RRGGBB"*/
    private String fontColor;

    /** Имя иконки */
    private String iconName;

    /** Прозрачность окрашивания зон на плане, от 0 до 1 */
    private double regionFillAlpha = 0.3;

    /** Класс состояния */
    @DBRef private StateCategory stateCategory;

    private boolean fireStateCategory;

    private boolean alarmStateCategory;

    private boolean warningStateCategory;

    private boolean autoOffStateCategory;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public double getRegionFillAlpha() {
        return regionFillAlpha;
    }

    public void setRegionFillAlpha(double regionFillAlpha) {
        this.regionFillAlpha = regionFillAlpha;
    }

    public StateCategory getStateCategory() {
        return stateCategory;
    }

    public void setStateCategory(StateCategory stateCategory) {
        this.stateCategory = stateCategory;
        this.id = stateCategory.getId();
    }

    public boolean isFireStateCategory() {
        return fireStateCategory;
    }

    public void setFireStateCategory(boolean fireStateCategory) {
        this.fireStateCategory = fireStateCategory;
    }

    public boolean isWarningStateCategory() {
        return warningStateCategory;
    }

    public void setWarningStateCategory(boolean warningStateCategory) {
        this.warningStateCategory = warningStateCategory;
    }

    public boolean isAlarmStateCategory() {
        return alarmStateCategory;
    }

    public void setAlarmStateCategory(boolean alarmStateCategory) {
        this.alarmStateCategory = alarmStateCategory;
    }

    public boolean isAutoOffStateCategory() {
        return autoOffStateCategory;
    }

    public void setAutoOffStateCategory(boolean autoOffStateCategory) {
        this.autoOffStateCategory = autoOffStateCategory;
    }
}
