package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.ArrayList;
import java.util.List;

/**
 * Справочник перечисляемых данных функционала устройств
 * @author Антон Караваев
 */

public class AggregatedDeviceProfileDictionary {

    private List<IdAndNameAndDescr> deviceGroups = new ArrayList<>();

    public void addDeviceGroup(IdAndNameAndDescr deviceGroup){
        deviceGroups.add(deviceGroup);
    }

    public List<IdAndNameAndDescr> getDeviceGroups() {
        return deviceGroups;
    }
}
