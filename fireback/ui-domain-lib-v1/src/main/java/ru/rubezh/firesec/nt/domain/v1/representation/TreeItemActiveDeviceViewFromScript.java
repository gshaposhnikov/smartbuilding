package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.rubezh.firesec.nt.domain.v1.*;

/**
 * Отображение устройства как элемента дерева для оперативной задачи, получаемое
 * от Mongo-скрипта.
 * 
 * Отличается от своего родительского класса наличием полей - помощников,
 * возвращаемых mongo-скриптом для вычисления сложных аггрегированных полей.
 * 
 * @author Антон Васильев
 *
 */
public class TreeItemActiveDeviceViewFromScript extends TreeItemActiveDeviceView {

    /** Сырые (не агрегированные) отображения активных состояний */
    private List<StateView> rawActiveStateViews;
    /** Значения свойств устройства (копия из сущности устройства) */
    private Map<String, String> devicePropertyValues;

    /** Набор профилей наблюдаемых величин (согласно профилю устройства) */
    private List<MonitorableValueProfileView> monitorableValueProfileViews;

    /** Набор значений наблюдаемых величин (согласно профилю устройства) */
    private Map<String, String> deviceMonitorableValues;

    /** Набор конфигурационных параметров устройства */
    private Map<String, DeviceConfigProperty> deviceConfigProperties;
    /** Набор отображений конфигурационных параметров устройства */
    private Map<String, DeviceConfigPropertyView> deviceConfigPropertyViews;
    /** Набор проектных значений конфигурационных параметров устройства */
    private Map<String, String> projectDeviceConfigValues;
    /** Набор прочитанных значений конфигурационных параметров устройства */
    private Map<String, String> activeDeviceConfigValues;

    /** Набор "прочих" параметров устройства */
    private Map<String, DeviceProperty> deviceOtherProperties;
    /** Набор отображений "прочих" параметров устройства */
    private Map<String, DeviceConfigPropertyView> deviceOtherPropertyViews;
    /** Набор проектных значений "прочих" параметров устройства */
    private Map<String, String> projectDeviceOtherValues;

    /** Отображение Id профилей устройств на списки Id связанных устройств */
    private Map<String, Set<String>> linkedDeviceIdsMap;

    /** Поддерживаемые действия устройства в виде пар "идентификатор действия: его описание" */
    private Map<String, DeviceAction> supportedActions;
    /** Отображение поддерживаемых действий устройства в виде пар "идентификатор действия: его отображение" */
    private Map<String, DeviceActionView> supportedActionViews;

    @JsonIgnore
    public List<StateView> getRawActiveStateViews() {
        return rawActiveStateViews;
    }

    public void setRawActiveStateViews(List<StateView> rawActiveStateViews) {
        this.rawActiveStateViews = rawActiveStateViews;
    }

    @JsonIgnore
    public Map<String, String> getDevicePropertyValues() {
        return devicePropertyValues;
    }

    public void setDevicePropertyValues(Map<String, String> devicePropertyValues) {
        this.devicePropertyValues = devicePropertyValues;
    }

    @JsonIgnore
    public List<MonitorableValueProfileView> getMonitorableValueProfileViews() {
        return monitorableValueProfileViews;
    }

    public void setMonitorableValueProfileViews(List<MonitorableValueProfileView> monitorableValueProfileViews) {
        this.monitorableValueProfileViews = monitorableValueProfileViews;
    }

    @JsonIgnore
    public Map<String, String> getDeviceMonitorableValues() {
        return deviceMonitorableValues;
    }

    public void setDeviceMonitorableValues(Map<String, String> deviceMonitorableValues) {
        this.deviceMonitorableValues = deviceMonitorableValues;
    }

    @JsonIgnore
    public Map<String, DeviceConfigProperty> getDeviceConfigProperties() {
        return deviceConfigProperties;
    }

    public void setDeviceConfigProperties(Map<String, DeviceConfigProperty> deviceConfigProperties) {
        this.deviceConfigProperties = deviceConfigProperties;
    }

    @JsonIgnore
    public Map<String, DeviceConfigPropertyView> getDeviceConfigPropertyViews() {
        return deviceConfigPropertyViews;
    }

    public void setDeviceConfigPropertyViews(Map<String, DeviceConfigPropertyView> deviceConfigPropertyViews) {
        this.deviceConfigPropertyViews = deviceConfigPropertyViews;
    }

    @JsonIgnore
    public Map<String, String> getProjectDeviceConfigValues() {
        return projectDeviceConfigValues;
    }

    public void setProjectDeviceConfigValues(Map<String, String> projectDeviceConfigValues) {
        this.projectDeviceConfigValues = projectDeviceConfigValues;
    }

    @JsonIgnore
    public Map<String, String> getActiveDeviceConfigValues() {
        return activeDeviceConfigValues;
    }

    public void setActiveDeviceConfigValues(Map<String, String> activeDeviceConfigValues) {
        this.activeDeviceConfigValues = activeDeviceConfigValues;
    }

    @JsonIgnore
    public Map<String, DeviceProperty> getDeviceOtherProperties() {
        return deviceOtherProperties;
    }

    public void setDeviceOtherProperties(Map<String, DeviceProperty> deviceOtherProperties) {
        this.deviceOtherProperties = deviceOtherProperties;
    }

    @JsonIgnore
    public Map<String, DeviceConfigPropertyView> getDeviceOtherPropertyViews() {
        return deviceOtherPropertyViews;
    }

    public void setDeviceOtherPropertyViews(Map<String, DeviceConfigPropertyView> deviceOtherPropertyViews) {
        this.deviceOtherPropertyViews = deviceOtherPropertyViews;
    }

    @JsonIgnore
    public Map<String, String> getProjectDeviceOtherValues() {
        return projectDeviceOtherValues;
    }

    public void setProjectDeviceOtherValues(Map<String, String> projectDeviceOtherValues) {
        this.projectDeviceOtherValues = projectDeviceOtherValues;
    }

    @JsonIgnore
    public Map<String, Set<String>> getLinkedDeviceIdsMap() {
        return linkedDeviceIdsMap;
    }

    public void setLinkedDeviceIdsMap(Map<String, Set<String>> linkedDeviceIdsMap) {
        this.linkedDeviceIdsMap = linkedDeviceIdsMap;
    }

    @JsonIgnore
    public Map<String, DeviceAction> getSupportedActions() {
        return supportedActions;
    }

    public void setSupportedActions(Map<String, DeviceAction> supportedActions_) {
        supportedActions = supportedActions_;
    }

    @JsonIgnore
    public Map<String, DeviceActionView> getSupportedActionViews() {
        return supportedActionViews;
    }

    public void setSupportedActionViews(Map<String, DeviceActionView> supportedActionViews_) {
        supportedActionViews = supportedActionViews_;
    }

}
