package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.rubezh.firesec.nt.domain.v1.*;

/**
 * Общая сущность обновления набора данных, запускаемых периодически (sсheduler)
 * 
 * ВНИМАНИЕ!!! Если для проектной сущности реализован сервис с интерфейсом
 * ProjectEntityService, то сюда и в EntityNotifier ничего добавлять не
 * требуется!
 * 
 * TODO: реализовать SystemEntityService по аналогии с ProjectEntityService
 * TODO: перевести хардкодные поля на SystemEntityService
 * 
 * @author Александр Горячкин
 */
public class UpdatedEntities extends HashMap<String, Object> {

    /* Чтобы не было предупреждения */
    public static final long serialVersionUID = 1827923413;
    
    /** 
     * Контейнер для передачи набора системных сущностей по категориям
     * @param <T> класс системной сущности
     */
    public static class EntitiesContainer<T> {

        /** Список созданных сущностей */
        List<T> created = null;
        /** Список изменёных сущностей */
        List<T> updated = null;
        /** Список идентификаторов удалённых сущностей */
        List<String> deletedIds = null;

        @JsonIgnore
        public boolean isEmpty() {
            return (created == null || created.isEmpty())
                    && (updated == null || updated.isEmpty())
                    && (deletedIds == null || deletedIds.isEmpty());
        }

        public List<T> getCreated() {
            return created;
        }

        public void setCreated(List<T> created) {
            this.created = created;
        }

        public List<T> getUpdated() {
            return updated;
        }

        public void setUpdated(List<T> updated) {
            this.updated = updated;
        }

        public List<String> getDeletedIds() {
            return deletedIds;
        }

        public void setDeletedIds(List<String> deletedIds) {
            this.deletedIds = deletedIds;
        }
    }

    /**
     * Контейнер для передачи набора проектных сущностей по категориям
     * @author Антон Васильев
     *
     * @param <T> класс проектной сущности
     */
    public static class ProjectEntitiesContainer<T> {

        /** Список созданных сущностей */
        private List<T> created = null;
        /** Список изменёных сущностей */
        private List<T> updated = null;

        /** Список удалённых сущностей */
        private List<T> deleted = null;
        
        public boolean isEmpty() {
            return (created == null || created.isEmpty())
                    && (updated == null || updated.isEmpty())
                    && (deleted == null || deleted.isEmpty());
        }

        public List<T> getCreated() {
            return created;
        }

        public void setCreated(List<T> created) {
            this.created = created;
        }

        public List<T> getUpdated() {
            return updated;
        }

        public void setUpdated(List<T> updated) {
            this.updated = updated;
        }

        public List<T> getDeleted() {
            return deleted;
        }

        public void setDeleted(List<T> deleted) {
            this.deleted = deleted;
        }

    }

    public List<?> getEvents() {
        return (List<?>) get("events");
    }

    public void setEvents(List<AggregatedEventView> events) {
        if (events != null)
            put("events", events);
    }

    public List<?> getUsers() {
        return (List<?>) get("users");
    }

    public void setUsers(List<User> users) {
        if (users != null)
            put("users", users);
    }

    public EntitiesContainer<?> getUserGroups() {
        return (EntitiesContainer<?>) get("userGroups");
    }

    public void setUserGroups(EntitiesContainer<UserGroup> userGroups) {
        if (userGroups != null && !userGroups.isEmpty())
            put("userGroups", userGroups);
    }

    public EntitiesContainer<?> getDeviceShapeLibraries() {
        return (EntitiesContainer<?>) get("deviceShapeLibraries");
    }

    public void setDeviceShapeLibraries(EntitiesContainer<DeviceShapeLibrary> deviceShapeLibraries) {
        if (deviceShapeLibraries != null && !deviceShapeLibraries.isEmpty())
            put("deviceShapeLibraries", deviceShapeLibraries);
    }

    public EntitiesContainer<?> getDeviceShapes() {
        return (EntitiesContainer<?>) get("deviceShapes");
    }

    public void setDeviceShapes(EntitiesContainer<DeviceShape> deviceShapes) {
        if (deviceShapes != null && !deviceShapes.isEmpty())
            put("deviceShapes", deviceShapes);
    }

    public EntitiesContainer<?> getLogViews() {
        return (EntitiesContainer<?>) get("logViews");
    }

    public void setLogViews(EntitiesContainer<LogView> logViews) {
        if (logViews != null && !logViews.isEmpty())
            put("logViews", logViews);
    }

    public EntitiesContainer<?> getMedias() {
        return (EntitiesContainer<?>) get("medias");
    }

    public void setMedias(EntitiesContainer<Media> medias) {
        if (medias != null && !medias.isEmpty())
            put("medias", medias);
    }

    public EntitiesContainer<?> getSoundNotifications() {
        return (EntitiesContainer<?>) get("soundNotifications");
    }

    public void setSoundNotifications(EntitiesContainer<SoundNotification> soundNotifications) {
        if (soundNotifications != null && !soundNotifications.isEmpty())
            put("soundNotifications", soundNotifications);
    }

}
