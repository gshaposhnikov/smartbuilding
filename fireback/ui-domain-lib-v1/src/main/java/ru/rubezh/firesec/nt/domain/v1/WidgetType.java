package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип виджета.
 * 
 * @author Александр Горячкин
 */
public enum WidgetType {
    /** окно с содержимым */
    window,
    /** панель для хранения виджетов */
    panel
}
