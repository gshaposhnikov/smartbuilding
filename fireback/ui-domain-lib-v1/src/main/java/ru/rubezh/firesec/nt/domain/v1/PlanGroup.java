package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Группа планов.
 * 
 * Используется для группировки планов.
 * 
 * @author Александр Горячкин, Антон Васильев.
 * @ingroup UIDomain
 */
@Document(collection = "plan_groups")
@TypeAlias(value = "PlanGroup")
public class PlanGroup extends BasicEntityView {

    /** Проект */
    @Indexed
    private String projectId = "";

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    public void update(PlanGroup planGroup) {
        updateBasic(planGroup);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
