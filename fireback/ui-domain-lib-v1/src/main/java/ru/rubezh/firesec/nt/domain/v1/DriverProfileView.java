package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Профиль отображений драйвера.
 * 
 * Содержит отображения моделей всех справочных сущностей драйвера.
 *
 * Оно необходимо, чтобы в максимально сжатой XML-конфигурации настраивать отображения сущностей, поддерживаемых
 * драйвером, под различные типы интерфейсов, под различные языковые требования и т.д.
 *
 * Все сущности отображения должны ссылаться на сущности профиля драйвера.
 *
 * У драйвера может быть одновременно несколько отображений, например, для возможности выбирать один из поддерживаемых
 * языков.
 * 
 * @author Антон Васильев
 * @author Александр Горячкин
 * @ingroup UIReferences
 */
@Document(collection = "driver_profile_views")
@TypeAlias(value = "DriverProfileView")
public class DriverProfileView extends ReferenceEntityView {

    @DBRef private DriverProfile driverProfile = new DriverProfile();

    /** Отображения состояний */
    @DBRef private List<StateView> stateViews = new ArrayList<>();

    /** Отображения наблюдаемых значений */
    @DBRef private List<MonitorableValueProfileView> monitorableValueProfileViews = new ArrayList<>();

    /** Отображения типов событий */
    @DBRef private List<EventTypeView> eventTypeViews = new ArrayList<>();

    /** Отображения профилей (типов) устройств */
    @DBRef private List<DeviceProfileView> deviceProfileViews = new ArrayList<>();
    
    /** Отображения триггеров сценариев, поддерживаемых драйвером */
    @DBRef private List<ScenarioTriggerTypeView> scenarioTriggerTypeViews = new ArrayList<>();
    
    /** Отображения действий сценариев, поддерживаемых драйвером */
    @DBRef private List<ScenarioActionTypeView> scenarioActionTypeViews = new ArrayList<>();

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
    }

    public List<StateView> getStateViews() {
        return stateViews;
    }

    public void setStateViews(List<StateView> stateViews) {
        this.stateViews = stateViews;
    }

    public List<MonitorableValueProfileView> getMonitorableValueProfileViews() {
        return monitorableValueProfileViews;
    }

    public void setMonitorableValueProfileViews(List<MonitorableValueProfileView> monitorableValueProfileViews) {
        this.monitorableValueProfileViews = monitorableValueProfileViews;
    }

    public List<EventTypeView> getEventTypeViews() {
        return eventTypeViews;
    }

    public void setEventTypeViews(List<EventTypeView> eventTypeViews) {
        this.eventTypeViews = eventTypeViews;
    }

    public List<DeviceProfileView> getDeviceProfileViews() {
        return deviceProfileViews;
    }

    public void setDeviceProfileViews(List<DeviceProfileView> deviceProfileViews) {
        this.deviceProfileViews = deviceProfileViews;
    }

    public List<ScenarioTriggerTypeView> getScenarioTriggerTypeViews() {
        return scenarioTriggerTypeViews;
    }
    
    public void setScenarioTriggerTypeViews(List<ScenarioTriggerTypeView> scenarioTriggerTypeViews) {
        this.scenarioTriggerTypeViews = scenarioTriggerTypeViews;
    }

    public List<ScenarioActionTypeView> getScenarioActionTypeViews() {
        return scenarioActionTypeViews;
    }
    
    public void setScenarioActionTypeViews(List<ScenarioActionTypeView> scenarioActionTypeViews) {
        this.scenarioActionTypeViews = scenarioActionTypeViews;
    }
}
