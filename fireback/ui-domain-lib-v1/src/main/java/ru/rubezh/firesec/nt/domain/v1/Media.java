package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Медиафайл (иконка, изображение, звук, подложка плана).
 * 
 * Хранит тип и путь к медиафайлу. Является элементом библиотеки медиафайлов.
 * 
 * @author Александр Горячкин
 * @ingroup UIReferences
 * @ingroup UIDomain
 */
@Document(collection = "medias")
@TypeAlias(value = "Media")
public class Media extends ReferenceEntityView {
    /**
     * Путь к файлу.
     * 
     * Поле не актуально, если есть content, т.е. если содержимое файла хранится непосредственно в БД.
     */
    private String path = "";
    /** Тип медиафайла */
    private MediaType mediaType = MediaType.ICON;
    /** Цвет заливки */
    private String fillColor = "#000000";
    /**
     * Содержимое медиа
     */
    private String content;

    private String userId;

    /** Дата и время создания */
    @JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createDateTime = new Date();

    /** Дата и время обновления */
    @JsonIgnore
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updateDateTime = null;

    /** Подготовлен для удаления */
    @JsonIgnore
    private boolean removed = false;

    public void update(Media media){
        setName(media.getName());
        setDescription(media.getDescription());
        this.updateDateTime = new Date();
        this.fillColor = media.fillColor;
        this.content = media.content;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
