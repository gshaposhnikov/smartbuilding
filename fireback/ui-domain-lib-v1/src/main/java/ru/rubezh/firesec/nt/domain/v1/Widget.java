package ru.rubezh.firesec.nt.domain.v1;

/**
 * Виджет.
 * 
 * Используется для формирования интерфейса пользователя.
 * 
 * @author Александр Горячкин
 */
public class Widget {

    /** идентификатор */
    private int id;
    /** контейнер */
    private int parentId;

    /** тип виджета */
    private WidgetType widgetType;
    /** размер */
    private int size;
    /** порядковый номер в контейнере */
    private int order;
    /** редактируемый ли в окне ОЗ */
    private boolean fixed;
    /** возможность открепить */
    private boolean detachable;
    /** дополнительные параметры отображения */
    private String params;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public WidgetType getWidgetType() {
        return widgetType;
    }

    public void setWidgetType(WidgetType widgetType) {
        this.widgetType = widgetType;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean getFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public boolean getDetachable() {
        return detachable;
    }

    public void setDetachable(boolean detachable) {
        this.detachable = detachable;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
