package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import java.util.ArrayList;
import java.util.List;

import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;

/**
 * Справочник перечисляемых данных функционала сценариев.
 * @author Антон Васильев
 *
 */
public class AggregatedScenarioDictionary {
    /** Типы назначения сценариев */
    private List<IdAndNameAndDescr> purposes = new ArrayList<>();
    /** Типы сценариев */
    private List<IdAndNameAndDescr> types = new ArrayList<>();
    /** Флаги у сценариев */
    private List<IdAndNameAndDescr> flags = new ArrayList<>();
    /** Типы остановки сценариев */
    private List<IdAndNameAndDescr> stopTypes = new ArrayList<>();
    /** Типы сущностей, к которым относятся триггеры включения/выключения сценариев */
    private List<IdAndNameAndDescr> triggerEntityTypes = new ArrayList<>();
    /** Типы логики включения/выключения сценариев */
    private List<IdAndNameAndDescr> logicTypes = new ArrayList<>();
    /** Типы блоков сценариев */
    private List<IdAndNameAndDescr> timeLineBlockTypes = new ArrayList<>();
    /** Типы условий исполнительных блоков сценариев */
    private List<IdAndNameAndDescr> conditionTypes = new ArrayList<>();
    /** Типы проверки условий исполнительных блоков сценариев */
    private List<IdAndNameAndDescr> conditionCheckTypes = new ArrayList<>();
    /** Типы сущностей, которыми могут выполняться действия, перечисляемые в исполнительных блоках сценариев */
    private List<IdAndNameAndDescr> actionEntityTypes = new ArrayList<>();
    /** Типы сущностей блоков сценария слежения */
    private List<IdAndNameAndDescr> tracingEntityTypes = new ArrayList<>();
    /** Действия над сценариями */
    private List<IdAndNameAndDescr> manageActions = new ArrayList<>();

    public List<IdAndNameAndDescr> getPurposes() {
        return purposes;
    }

    public void setPurposes(List<IdAndNameAndDescr> purposes) {
        this.purposes = purposes;
    }

    public List<IdAndNameAndDescr> getTypes() {
        return types;
    }

    public void setTypes(List<IdAndNameAndDescr> types) {
        this.types = types;
    }

    public List<IdAndNameAndDescr> getFlags() {
        return flags;
    }

    public void setFlags(List<IdAndNameAndDescr> flags) {
        this.flags = flags;
    }

    public List<IdAndNameAndDescr> getStopTypes() {
        return stopTypes;
    }

    public void setStopTypes(List<IdAndNameAndDescr> stopTypes) {
        this.stopTypes = stopTypes;
    }

    public List<IdAndNameAndDescr> getTriggerEntityTypes() {
        return triggerEntityTypes;
    }

    public void setTriggerEntityTypes(List<IdAndNameAndDescr> triggerEntityTypes) {
        this.triggerEntityTypes = triggerEntityTypes;
    }

    public List<IdAndNameAndDescr> getLogicTypes() {
        return logicTypes;
    }

    public void setLogicTypes(List<IdAndNameAndDescr> logicTypes) {
        this.logicTypes = logicTypes;
    }

    public List<IdAndNameAndDescr> getTimeLineBlockTypes() {
        return timeLineBlockTypes;
    }

    public void setTimeLineBlockTypes(List<IdAndNameAndDescr> timeLineBlockTypes) {
        this.timeLineBlockTypes = timeLineBlockTypes;
    }

    public List<IdAndNameAndDescr> getConditionTypes() {
        return conditionTypes;
    }

    public void setConditionTypes(List<IdAndNameAndDescr> conditionTypes) {
        this.conditionTypes = conditionTypes;
    }

    public List<IdAndNameAndDescr> getConditionCheckTypes() {
        return conditionCheckTypes;
    }

    public void setConditionCheckTypes(List<IdAndNameAndDescr> conditionCheckTypes) {
        this.conditionCheckTypes = conditionCheckTypes;
    }

    public List<IdAndNameAndDescr> getActionEntityTypes() {
        return actionEntityTypes;
    }

    public void setActionEntityTypes(List<IdAndNameAndDescr> actionEntityTypes) {
        this.actionEntityTypes = actionEntityTypes;
    }

    public List<IdAndNameAndDescr> getTracingEntityTypes() {
        return tracingEntityTypes;
    }

    public void setTracingEntityTypes(List<IdAndNameAndDescr> tracingEntityTypes) {
        this.tracingEntityTypes = tracingEntityTypes;
    }


    public List<IdAndNameAndDescr> getManageActions() {
        return manageActions;
    }

    public void setManageActions(List<IdAndNameAndDescr> manageActions) {
        this.manageActions = manageActions;
    }

}
