package ru.rubezh.firesec.nt.domain.v1.representation;

import ru.rubezh.firesec.nt.domain.v1.Permission;

import java.util.List;

/**
 * Отображение прав доступа.
 * Включает списки уровней доступа, целей и конечных прав.
 * @author Александр Горячкин
 */
public class PermissionsView {

    private List<Permission> permissions;
    private List<Permission.Target> targets;
    private List<Permission.AccessLevel> accessLevels;

    public PermissionsView(List<Permission> permissions, List<Permission.Target> targets,
                           List<Permission.AccessLevel> accessLevels) {
        this.permissions = permissions;
        this.targets = targets;
        this.accessLevels = accessLevels;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<Permission.Target> getTargets() {
        return targets;
    }

    public void setTargets(List<Permission.Target> targets) {
        this.targets = targets;
    }

    public List<Permission.AccessLevel> getAccessLevels() {
        return accessLevels;
    }

    public void setAccessLevels(List<Permission.AccessLevel> accessLevels) {
        this.accessLevels = accessLevels;
    }
}
