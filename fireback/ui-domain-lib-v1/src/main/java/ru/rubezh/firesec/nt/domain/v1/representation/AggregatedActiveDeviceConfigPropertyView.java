package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.HashMap;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.*;

/**
 * Обобщенное отображение одного параметра конфигурации устройства в ОЗ
 *
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 */
public class AggregatedActiveDeviceConfigPropertyView {
    /** Идентификатор */
    private String id;

    /** Название */
    private String name;

    /** Описание */
    private String description;

    /** Тип (из PropertyType) */
    private String type;

    /** Значение на устройстве */
    private String activeValue;

    /** Значение в проекте */
    private String projectValue;

    /** Значение по умолчанию (заводское значение) */
    private String defaultValue;

    /** Минимальное допустимое значение или null */
    private String min;

    /** Максимальное допустимое значение или null */
    private String max;

    /** Является ли этот параметр ненастраиваемым пользователем */
    private boolean readOnly;

    /** Идентификатор раздела */
    private String section;

    /** Тип сущности (для типа параметра - идентификатор сущности) */
    private EntityType entityType;

    /** Подсистема сущностий (для типа параметра - идентификатор сущности) */
    private Subsystem entitySubsystem;

    /**
     * Список возможных значений (в виде пары "значение": "имя") или null, если
     * возможен весь диапазон от min до max
     */
    private Map<String, String> possibleValues;

    public AggregatedActiveDeviceConfigPropertyView() {
    }

    public AggregatedActiveDeviceConfigPropertyView(String id, DeviceProperty property, DeviceConfigPropertyView view,
            String activeValue, String projectValue) {
        this.id = id;
        this.type = PropertyType.getValue(property.getType());
        this.activeValue = activeValue;
        this.projectValue = projectValue;
        if (property.isViewInit() && (view != null)) {
            this.defaultValue = view.getDefaultValue();
            if (this.projectValue == null) 
                this.projectValue = this.defaultValue;
        } else
            this.defaultValue = property.getDefaultValue();
        this.min = property.getMin();
        this.max = property.getMax();
        this.readOnly = property.isReadOnly();
        this.section = property.getSection();
        this.entityType = property.getEntityType();
        this.entitySubsystem = property.getEntitySubsystem();

        if (view != null) {
            this.name = view.getName();
            this.description = view.getDescription();
        } else {
            this.name = id;
            this.description = "";
        }
        if (property.getPossibleValues() != null) {
            this.possibleValues = new HashMap<>();
            for (String value : property.getPossibleValues())
                if (view != null && view.getPossibleValueNames() != null)
                    this.possibleValues.put(value, view.getPossibleValueNames().getOrDefault(value, value));
                else
                    this.possibleValues.put(value, value);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActiveValue() {
        return activeValue;
    }

    public void setActiveValue(String activeValue) {
        this.activeValue = activeValue;
    }

    public String getProjectValue() {
        return projectValue;
    }

    public void setProjectValue(String projectValue) {
        this.projectValue = projectValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section_) {
        section = section_;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Subsystem getEntitySubsystem() {
        return entitySubsystem;
    }

    public void setEntitySubsystem(Subsystem entitySubsystem) {
        this.entitySubsystem = entitySubsystem;
    }

    public Map<String, String> getPossibleValues() {
        return possibleValues;
    }

    public void setPossibleValues(Map<String, String> possibleValues) {
        this.possibleValues = possibleValues;
    }
}
