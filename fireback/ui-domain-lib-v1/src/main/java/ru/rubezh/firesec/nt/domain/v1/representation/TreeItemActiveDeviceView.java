package ru.rubezh.firesec.nt.domain.v1.representation;

import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Отображение устройства (оперативной части) как элемента дерева.
 * @author Александр Горячкин, Антон Васильев
 */
public class TreeItemActiveDeviceView extends TreeItemDeviceView {

    /** Опрос состояния включен/отключен */
    private boolean statePolling = true;

    /** Текущий класс состояния */
    private StateCategoryView generalStateCategoryView;

    /** Набор активных состояний (согласно профилю устройства) */
    private List<AggregatedStateView> activeStateViews = new ArrayList<>();

    /** Набор наблюдаемых значений (согласно профилю устройства) */
    private List<ItemMonitorableValue> monitorableValueViews = new ArrayList<>();

    /** Время последнего обновления наблюдаемых значений */
    private Date monitorableValuesTimestamp;

    /** Набор записей по устройству из Блокнота*/
    private List<ActiveDevice.Note> notes = new ArrayList<>();

    /** Cписок допустимых действий */
    private List<AggregatedDeviceAction> aggregatedSupportedActions;

    public static class ItemMonitorableValue {
        public String id;
        public String name;
        public String description;
        public String unit;
        public String value = "0";

        public ItemMonitorableValue(String id, String name, String description, String unit, String value) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.unit = unit;
            if (value != null) {
                this.value = value;
            }
        }
    }

    public TreeItemActiveDeviceView() {

    }

    public TreeItemActiveDeviceView(DeviceView deviceView,
                                    DeviceProfileView deviceProfileView,
                                    Region region,
                                    List<DeviceProfileView> acceptableDeviceProfileViews,
                                    boolean statePolling,
                                    StateCategoryView generalStateCategoryView,
                                    List<AggregatedStateView> activeStateViews,
                                    List<ItemMonitorableValue> monitorableValueViews,
                                    List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews,
                                    List<AggregatedActiveDeviceConfigPropertyView> otherPropertyViews,
                                    Date monitorableValuesTimestamp,
                                    List<ActiveDevice.Note> notes) {
        super(deviceView, deviceProfileView, region, acceptableDeviceProfileViews, configPropertyViews,
                otherPropertyViews);

        /*
         * Для активного проекта имя устройства необходимо присвоить повторно,
         * взяв псевдоним либо значение по умолчанию, хранящееся в сущности
         * отображения.
         */
        setName(deviceView.getName());

        setStatePolling(statePolling);
        setGeneralStateCategoryView(generalStateCategoryView);
        setActiveStateViews(activeStateViews);
        setMonitorableValueViews(monitorableValueViews);
        setMonitorableValuesTimestamp(monitorableValuesTimestamp);
        setNotes(notes);
        setAggregatedSupportedActions(createAggregatedActions(
                deviceProfileView.getDeviceProfile().getSupportedActions(),
                deviceProfileView.getSupportedActionViews()));
    }

    public static List<AggregatedDeviceAction> createAggregatedActions(
            Map<String, DeviceAction> supportedActions,
            Map<String, DeviceActionView> supportedActionViews) {
        List<AggregatedDeviceAction> aggregatedActions = null;
        if (supportedActions != null && !supportedActions.isEmpty()) {
            aggregatedActions = new ArrayList<>();
            for (String actionId : supportedActions.keySet()) {
                DeviceAction deviceAction = supportedActions.get(actionId);
                DeviceActionView deviceActionView = null;
                if (supportedActionViews != null)
                    deviceActionView = supportedActionViews.get(actionId);
                aggregatedActions.add(new AggregatedDeviceAction(
                        actionId, deviceAction, deviceActionView
                ));
            }
        }
        return aggregatedActions;
    }

    public boolean isStatePolling() {
        return statePolling;
    }

    public void setStatePolling(boolean statePolling) {
        this.statePolling = statePolling;
    }

    public StateCategoryView getGeneralStateCategoryView() {
        return generalStateCategoryView;
    }

    public void setGeneralStateCategoryView(StateCategoryView stateCategoryView) {
        this.generalStateCategoryView = stateCategoryView;
    }

    public List<AggregatedStateView> getActiveStateViews() {
        return activeStateViews;
    }

    public void setActiveStateViews(List<AggregatedStateView> activeStateViews) {
        this.activeStateViews = activeStateViews;
    }

    public List<ItemMonitorableValue> getMonitorableValueViews() {
        return monitorableValueViews;
    }

    public void setMonitorableValueViews(List<ItemMonitorableValue> monitorableValueViews) {
        this.monitorableValueViews = monitorableValueViews;
    }

    public Date getMonitorableValuesTimestamp() {
        return monitorableValuesTimestamp;
    }

    public void setMonitorableValuesTimestamp(Date monitorableValuesTimestamp) {
        this.monitorableValuesTimestamp = monitorableValuesTimestamp;
    }

    public List<ActiveDevice.Note> getNotes() {
        return notes;
    }

    public void setNotes(List<ActiveDevice.Note> notes) {
        this.notes = notes;
    }

    public List<AggregatedDeviceAction> getAggregatedSupportedActions() {
        return aggregatedSupportedActions;
    }

    public void setAggregatedSupportedActions(List<AggregatedDeviceAction> aggregatedSupportedActions_) {
        aggregatedSupportedActions = aggregatedSupportedActions_;
    }

}
