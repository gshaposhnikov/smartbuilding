package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import ru.rubezh.firesec.nt.domain.v1.DeviceAction;
import ru.rubezh.firesec.nt.domain.v1.DeviceActionView;
import ru.rubezh.firesec.nt.domain.v1.DeviceConfigProperty;
import ru.rubezh.firesec.nt.domain.v1.DeviceConfigPropertyView;

/**
 * Объединённое действие устройства
 *
 * @author Андрей Лисовой
 *
 */
public class AggregatedDeviceAction extends IdAndNameAndDescr {

    /** Объединённые параметры конфигурации действия */
    private List<AggregatedActiveDeviceConfigPropertyView> properties;

    public AggregatedDeviceAction() {
    }

    public AggregatedDeviceAction(String actionId, DeviceAction deviceAction, DeviceActionView deviceActionView) {
        setId(actionId);
        if (deviceActionView != null) {
            setName(deviceActionView.getName());
            setDescription(deviceActionView.getDescription());
        } else {
            setName(actionId);
            setDescription("");
        }
        properties = AggregatedDeviceAction.createAggregatedProperties(deviceAction, deviceActionView);
    }

    private static List<AggregatedActiveDeviceConfigPropertyView> createAggregatedProperties(
            DeviceAction deviceAction, DeviceActionView deviceActionView) {
        List<AggregatedActiveDeviceConfigPropertyView> aggregatedProperties = null;
        Map<String, DeviceConfigProperty> actionConfigProperties = null;
        if (deviceAction.getExtension() != null)
            actionConfigProperties = deviceAction.getExtension().getConfigProperties();
        if (actionConfigProperties != null && !actionConfigProperties.isEmpty()) {
            Map<String, DeviceConfigPropertyView> actionConfigPropertyViews = null;
            if (deviceActionView != null)
                actionConfigPropertyViews = deviceActionView.getConfigPropertyViews();
            aggregatedProperties = new ArrayList<>();
            for (String propertyId : actionConfigProperties.keySet()) {
                DeviceConfigProperty property = actionConfigProperties.get(propertyId);
                if (property.isHidden())
                    continue;
                DeviceConfigPropertyView propertyView = null;
                if (actionConfigPropertyViews != null)
                    propertyView = actionConfigPropertyViews.get(propertyId);
                aggregatedProperties.add(new AggregatedActiveDeviceConfigPropertyView(
                        propertyId, property, propertyView, null, null
                ));
            }
        }
        return aggregatedProperties;
    }

    public List<AggregatedActiveDeviceConfigPropertyView> getProperties() {
        return properties;
    }

    public void setProperties(List<AggregatedActiveDeviceConfigPropertyView> properties_) {
        properties = properties_;
    }

}
