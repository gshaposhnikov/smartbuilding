package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Отображение состояния.
 * 
 * @author Антон Васильев
 * @ingroup UIReferences
 */
@Document(collection = "state_views")
@TypeAlias(value = "StateView")
public class StateView extends BasicEntityView {

    @DBRef
    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        this.id = state.getId();
    }

}
