package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Профиль отображения наблюдаемой величины.
 * 
 * @author Александр Горячкин
 * @ingroup UIReferences
 */
@Document(collection = "monitorable_value_profile_views")
@TypeAlias(value = "MonitorableValueProfileView")
public class MonitorableValueProfileView extends BasicEntityView {

    @DBRef
    private MonitorableValueProfile value;

    /** Единица измерения */
    private String unit;

    public MonitorableValueProfile getValue() {
        return value;
    }

    public void setValue(MonitorableValueProfile value) {
        this.value = value;
        this.id = value.getId();
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
