package ru.rubezh.firesec.nt.domain.v1.representation;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ru.rubezh.firesec.nt.domain.v1.Device;
import ru.rubezh.firesec.nt.domain.v1.DeviceView;
import ru.rubezh.firesec.nt.domain.v1.Plan;
import ru.rubezh.firesec.nt.domain.v1.PlanGroup;
import ru.rubezh.firesec.nt.domain.v1.Project;
import ru.rubezh.firesec.nt.domain.v1.Region;
import ru.rubezh.firesec.nt.domain.v1.RegionView;
import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.VirtualState;
import ru.rubezh.firesec.nt.domain.v1.skud.AccessKey;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;
import ru.rubezh.firesec.nt.domain.v1.skud.WorkSchedule;

public class ProjectWrapper {
    private String projectBuild;
    private Project project;
    private List<DeviceView> deviceViews = new ArrayList<>();
    private List<Region> regions = new ArrayList<>();
    private List<RegionView> regionViews = new ArrayList<>();
    private List<Device> devices = new ArrayList<>();
    private List<Scenario> scenarios = new ArrayList<>();
    private List<VirtualState> virtualStates = new ArrayList<>();
    private List<Plan> plans = new ArrayList<>();
    private List<PlanGroup> planGroups = new ArrayList<>();
    private List<WorkSchedule> workSchedules = new ArrayList<>();
    private List<Employee> employees = new ArrayList<>();
    private List<AccessKey> accessKeys = new ArrayList<>();

    public ProjectWrapper() {
        super();
    }
    public ProjectWrapper(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            ProjectWrapper projectWrapper = objectMapper.readValue(json, ProjectWrapper.class);
            this.projectBuild = projectWrapper.projectBuild;
            this.project = projectWrapper.project;
            this.devices = projectWrapper.devices;
            this.regions = projectWrapper.regions;
            this.scenarios = projectWrapper.scenarios;
            this.virtualStates = projectWrapper.virtualStates;
            this.plans = projectWrapper.plans;
            this.planGroups = projectWrapper.planGroups;
            this.regionViews = projectWrapper.regionViews;
            this.deviceViews = projectWrapper.deviceViews;
            this.workSchedules = projectWrapper.workSchedules;
            this.employees = projectWrapper.employees;
            this.accessKeys = projectWrapper.accessKeys;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /* TODO: Добавить сюда индикаторы когда они будут сделаны */


    public Project getProject() {
        return project;
    }

    public String getProjectBuild() {
        return projectBuild;
    }

    public void setProjectBuild(String projectBuild) {
        this.projectBuild = projectBuild;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<DeviceView> getDeviceViews() {
        return deviceViews;
    }

    public void setDeviceViews(List<DeviceView> deviceViews) {
        this.deviceViews = deviceViews;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public List<RegionView> getRegionViews() {
        return regionViews;
    }

    public void setRegionViews(List<RegionView> regionViews) {
        this.regionViews = regionViews;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }

    public List<VirtualState> getVirtualStates() {
        return virtualStates;
    }

    public void setVirtualStates(List<VirtualState> virtualStates) {
        this.virtualStates = virtualStates;
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public void setPlans(List<Plan> plans) {
        this.plans = plans;
    }

    public List<PlanGroup> getPlanGroups() {
        return planGroups;
    }

    public void setPlanGroups(List<PlanGroup> planGroups) {
        this.planGroups = planGroups;
    }

    public List<WorkSchedule> getWorkSchedules() {
        return workSchedules;
    }

    public void setWorkSchedules(List<WorkSchedule> workSchedules) {
        this.workSchedules = workSchedules;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<AccessKey> getAccessKeys() {
        return accessKeys;
    }

    public void setAccessKeys(List<AccessKey> accessKeys) {
        this.accessKeys = accessKeys;
    }
}