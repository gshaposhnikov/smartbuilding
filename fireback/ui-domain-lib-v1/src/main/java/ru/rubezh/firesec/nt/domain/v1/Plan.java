package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * План помещения.
 * 
 * Представляет собой интерактивное изображение (чертеж) некоторой территории (помещения).
 *
 * @author Александр Горячкин, Антон Васильев.
 * @ingroup UIDomain
 */
@Document(collection = "plans")
@TypeAlias(value = "Plan")
public class Plan extends BasicEntityView {

    /** Идентификатор проекта */
    @Indexed
    private String projectId = "";

    /** Идентификатор группы планов (если план входит в группу) */
    private String planGroupId;

    /** Подсистема */
    private Subsystem subsystem = Subsystem.GENERAL;

    /** Ширина */
    private double xSize = 0;

    /** Высота */
    private double ySize = 0;

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

    /** Подложка плана. */
    public static class Background {
        private CoordinatePoint topLeftPoint = new CoordinatePoint();

        private String svgContent;

        public CoordinatePoint getTopLeftPoint() {
            return topLeftPoint;
        }

        public void setTopLeftPoint(CoordinatePoint topLeftPoint) {
            this.topLeftPoint = topLeftPoint;
        }

        public String getSvgContent() {
            return svgContent;
        }

        public void setSvgContent(String svgContent) {
            this.svgContent = svgContent;
        }
    }

    /** Фоновые рисунки (в формате SVG) */
    private List<Background> backgrounds = new ArrayList<>();

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags = new FilterTags();

    public void update(Plan plan) {
        updateBasic(plan);

        setPlanGroupId(plan.getPlanGroupId());
        setSubsystem(plan.getSubsystem());
        setxSize(plan.getxSize());
        setySize(plan.getySize());
        setBackgrounds(plan.getBackgrounds());
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getPlanGroupId() {
        return planGroupId;
    }

    public void setPlanGroupId(String planGroupId) {
        this.planGroupId = planGroupId;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public double getxSize() {
        return xSize;
    }

    public void setxSize(double xSize) {
        this.xSize = xSize;
    }

    public double getySize() {
        return ySize;
    }

    public void setySize(double ySize) {
        this.ySize = ySize;
    }

    public List<Background> getBackgrounds() {
        return backgrounds;
    }

    public void setBackgrounds(List<Background> backgrounds) {
        this.backgrounds = backgrounds;
    }

    public void emptyBackgrounds() {
        backgrounds.clear();
    }

}
