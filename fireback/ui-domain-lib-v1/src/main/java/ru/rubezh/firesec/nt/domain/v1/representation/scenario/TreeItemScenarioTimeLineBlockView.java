package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.EntityType;

import java.util.ArrayList;
import java.util.List;

/**
 * Элемент дерева отображения блоков сценариев с действиями.
 * @author Антон Васильев
 *
 */
public class TreeItemScenarioTimeLineBlockView implements Comparable<TreeItemScenarioTimeLineBlockView> {
    /** Тип элемента */
    private TreeItemType itemType = TreeItemType.CONDITION;
    /** Порядковый номер блока в сценарии */
    private int number;
    /** Задержка по времени, в секундах */
    private int timeDelaySec;
    /** Не отображать время */
    private boolean hiddenTime;
    
    /** Название действия */
    private String actionName = "";
    /** Название иконки действия */
    private String actionIconName = "null.svg";
    /** Цвет иконки действия */
    private String actionIconColor = "#000000";
    /** Описание */
    private String description = "";
    /** Иконка строки описания */
    private String descriptionIconName = "null.svg";
    /** Тип сущности, используемой в блоке */
    private EntityType entityType = EntityType.NONE;
    /** Идентификатор сущности, используемой в блоке */
    private String entityId = "";

    /* TODO: параметры действия */

    /** Дочерние элементы */
    private List<TreeItemScenarioTimeLineBlockView> children = new ArrayList<>();

    public TreeItemType getItemType() {
        return itemType;
    }

    public void setItemType(TreeItemType itemType) {
        this.itemType = itemType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTimeDelaySec() {
        return timeDelaySec;
    }

    public void setTimeDelaySec(int timeDelaySec) {
        this.timeDelaySec = timeDelaySec;
    }

    public boolean isHiddenTime() {
        return hiddenTime;
    }
    
    public void setHiddenTime(boolean hiddenTime) {
        this.hiddenTime = hiddenTime;
    }
    
    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionIconName() {
        return actionIconName;
    }

    public void setActionIconName(String actionIconName) {
        this.actionIconName = actionIconName;
    }

    public String getActionIconColor() {
        return actionIconColor;
    }

    public void setActionIconColor(String actionIconColor) {
        this.actionIconColor = actionIconColor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionIconName() {
        return descriptionIconName;
    }

    public void setDescriptionIconName(String descriptionIconName) {
        this.descriptionIconName = descriptionIconName;
    }

    public List<TreeItemScenarioTimeLineBlockView> getChildren() {
        return children;
    }

    public void setChildren(List<TreeItemScenarioTimeLineBlockView> children) {
        this.children = children;
    }
    
    @Override
    public int compareTo(TreeItemScenarioTimeLineBlockView item) {
        return ((this.getItemType() == TreeItemType.NO_TRACE ? 1000 : 0)
                    + this.timeDelaySec * 100 + this.getNumber() * 10 + this.getItemType().ordinal() * 1)
                - ((item.getItemType() == TreeItemType.NO_TRACE ? 1000 : 0)
                    + item.timeDelaySec * 100 + item.getNumber() * 10 + item.getItemType().ordinal() * 1);
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
