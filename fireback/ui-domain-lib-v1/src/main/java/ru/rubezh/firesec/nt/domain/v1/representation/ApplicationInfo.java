package ru.rubezh.firesec.nt.domain.v1.representation;

/**
 * Информация о приложении.
 * @author Александр Горячкин
 */
public class ApplicationInfo {

    /** Версия приложения */
    private String version;

    public ApplicationInfo(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
