package ru.rubezh.firesec.nt.domain.v1.representation;

import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;
import ru.rubezh.firesec.nt.domain.v1.StateView;

/**
 * Обобщённое отображение состояния устройства для ОЗ.
 * @author Александр Горячкин
 *
 */
public class AggregatedStateView extends IdAndNameAndDescr {

    /** Цвет в формате "#RRGGBB"*/
    private String color;

    /** Цвет текста в формате "#RRGGBB"*/
    private String fontColor;

    /** Имя иконки */
    private String iconName;


    public AggregatedStateView(StateView stateView, StateCategoryView stateCategoryView) {
        setId(stateView.getId());
        setName(stateView.getName());
        setDescription(stateView.getDescription());
        setColor(stateCategoryView.getColor());
        setFontColor(stateCategoryView.getFontColor());
        setIconName(stateCategoryView.getIconName());
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

}
