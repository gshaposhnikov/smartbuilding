package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

/**
 * Тип элемента дерева сценариев
 * @author Александр Горячкин
 */
public enum TreeItemType {
    /** корневой элемент - непосредственно сам сценарий */
    ROOT,
    /** временная метка - элемент для группировки блоков по времени */
    TIME,
    /** Слежение */
    TRACE,
    /** условие блока */
    CONDITION,
    /** Вход в блок */
    IN,
    /** Выход из блока */
    OUT,
    /** Нет слежения */
    NO_TRACE,
    /** действие блока */
    ACTION,
    /** Действия на компьютере */
    COMPUTER_ACTION,
    /** Показать сообщение */
    SHOW_MESSAGE,
    /** Обратный отсчет */
    COUNTDOWN
}
