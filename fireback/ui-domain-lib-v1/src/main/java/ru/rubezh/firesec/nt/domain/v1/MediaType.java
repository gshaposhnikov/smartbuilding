package ru.rubezh.firesec.nt.domain.v1;

/**
 * Тип медиафайла.
 * 
 * @author Александр Горячкин
 * @ingroup UIReferences
 */
public enum MediaType {
    /** Иконка */
    ICON,
    /** Встроенная иконка библиотеки antd */
    ANTD_ICON,
    /** Подложка */
    BACKGROUND,
    /** Звуковой файл */
    SOUND,
    /**
     * Содержимое SVG.
     * 
     * Когда в медиа хранится не путь к файлу, а само содержимое SVG-файла.
     */
    SVG_CONTENT
}
