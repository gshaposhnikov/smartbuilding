package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Шейп устройства.
 * 
 * Изображение (иконка, фигура), привязанное к конкретному состоянию конкретного типа устройства и с выбранным для него
 * типом анимации.
 * 
 * @author Антон Васильев
 * @ingroup UIDomain
 */
@Document(collection = "device_shapes")
@TypeAlias(value = "DeviceShape")
@CompoundIndexes({
    @CompoundIndex(name = "dshlib_scat", def = "{'deviceShapeLibraryId':1, 'stateCategoryId':1, 'removed':1}", unique = true),
})
public class DeviceShape extends ReferenceEntityView {

    /** Название базового шейпа */
    public static String BASIC_NAME = "Базовый шейп";

    /** Идентификатор библиотеки шейпов устройств */
    private String deviceShapeLibraryId;

    /**
     * Идентификатор категории состояния. Если пустая строка - это базовый шейп
     * в библиотеке.
     */
    private String stateCategoryId = "";

    /** Идентификатор встроенной картинки */
    private String builtinMediaId;

    /**
     * Пользовательская SVG (поле актуально если не задан идентификатор
     * встроенной картинки)
     */
    private String svgContent;

    /** Дата и время создания шейпа */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date created = new Date();

    /** Дата и время последнего изменения шейпа */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updated = new Date(0);

    /** Подготовлен для удаления */
    @JsonIgnore
    private boolean removed = false;

    public static enum AnimationType {
        /** Нет анимации */
        NONE,
        /** Мерцание */
        BLINKING,
        /** Изменение размера */
        RESIZING,
        /** Вращение */
        ROTATING
    }

    /** Тип анимации */
    private AnimationType animationType = AnimationType.NONE;

    /** Скорость анимации */
    private int animationSpeed = 500;

    public void update(DeviceShape deviceShape) {
        setName(deviceShape.getName());
        setDescription(deviceShape.getDescription());
        setBuiltinMediaId(deviceShape.getBuiltinMediaId());
        setSvgContent(deviceShape.getSvgContent());
        setAnimationType(deviceShape.getAnimationType());
        setAnimationSpeed(deviceShape.getAnimationSpeed());
        setUpdated(new Date());
    }

    public String getDeviceShapeLibraryId() {
        return deviceShapeLibraryId;
    }

    public void setDeviceShapeLibraryId(String deviceShapeLibraryId) {
        this.deviceShapeLibraryId = deviceShapeLibraryId;
    }

    public String getStateCategoryId() {
        return stateCategoryId;
    }

    public void setStateCategoryId(String stateCategoryId) {
        this.stateCategoryId = stateCategoryId;
    }

    public String getBuiltinMediaId() {
        return builtinMediaId;
    }

    public void setBuiltinMediaId(String builtinMediaId) {
        this.builtinMediaId = builtinMediaId;
    }

    public String getSvgContent() {
        return svgContent;
    }

    public void setSvgContent(String svgContent) {
        this.svgContent = svgContent;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public AnimationType getAnimationType() {
        return animationType;
    }

    public void setAnimationType(AnimationType animationType) {
        this.animationType = animationType;
    }

    public int getAnimationSpeed() {
        return animationSpeed;
    }

    public void setAnimationSpeed(int animationSpeed) {
        this.animationSpeed = animationSpeed;
    }

}
