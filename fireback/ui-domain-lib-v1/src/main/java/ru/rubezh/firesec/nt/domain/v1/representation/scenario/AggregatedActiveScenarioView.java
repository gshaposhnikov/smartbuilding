package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.FilterTags;
import ru.rubezh.firesec.nt.domain.v1.StateCategoryView;

public class AggregatedActiveScenarioView extends AggregatedScenarioView {

    /** Текущий класс состояния общей подсистемы */
    private StateCategoryView generalStateCategoryView;

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags;

    public AggregatedActiveScenarioView() {
    }

    public AggregatedActiveScenarioView(AggregatedScenarioView scenarioView) {
        setId(scenarioView.getId());
        setName(scenarioView.getName());
        setDescription(scenarioView.getDescription());
        setGlobalNo(scenarioView.getGlobalNo());
        setHint(scenarioView.getHint());
        setScenarioPurpose(scenarioView.getScenarioPurpose());
        setScenarioType(scenarioView.getScenarioType());
        setStartLogic(scenarioView.getStartLogic());
        setStopLogic(scenarioView.getStopLogic());
        setTimeLineBlocks(scenarioView.getTimeLineBlocks());
        setColor(scenarioView.getColor());
        setFontColor(scenarioView.getFontColor());
        setDeviceIds(scenarioView.getDeviceIds());
        setBasicParams(scenarioView.getBasicParams());
        setAdvancedParams(scenarioView.getAdvancedParams());
        setTimeLineBlocksTree(scenarioView.getTimeLineBlocksTree());
    }

    public StateCategoryView getGeneralStateCategoryView() {
        return generalStateCategoryView;
    }

    public void setGeneralStateCategoryView(StateCategoryView generalStateCategoryView) {
        this.generalStateCategoryView = generalStateCategoryView;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
