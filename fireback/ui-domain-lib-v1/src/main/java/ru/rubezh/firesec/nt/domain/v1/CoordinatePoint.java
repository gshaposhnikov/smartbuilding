package ru.rubezh.firesec.nt.domain.v1;

/**
 * Координата.
 * 
 * Используется для хранения местоположения в некоторой системе координат
 * 
 * @author Александр Горячкин
 */
public class CoordinatePoint {

    private double x;
    private double y;

    public CoordinatePoint() {
    }

    public CoordinatePoint(double x, double y) {
        setX(x);
        setY(y);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
