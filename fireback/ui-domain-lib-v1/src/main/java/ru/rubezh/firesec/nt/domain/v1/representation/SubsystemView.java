package ru.rubezh.firesec.nt.domain.v1.representation;


import ru.rubezh.firesec.nt.domain.v1.ActiveSubsystem;
import ru.rubezh.firesec.nt.domain.v1.FilterTags;
import ru.rubezh.firesec.nt.domain.v1.Subsystem;

import java.util.*;

/**
 * Отображение состояния подсистемы.
 * @author Александр Горячкин
 */
public class SubsystemView {

    public static class Counter {

        /** Название категории состояния */
        private String name = "";

        /** Значение счётчика */
        private int value = 0;

        /** Цвет в формате "#RRGGBB"*/
        private String color = "";

        /** Цвет текста в формате "#RRGGBB"*/
        private String fontColor;

        /** Идентификаторы устройств с данной категорией состояния */
        private Set<String> activeEntitiesIds = new HashSet<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getFontColor() {
            return fontColor;
        }

        public void setFontColor(String fontColor) {
            this.fontColor = fontColor;
        }

        public Set<String> getActiveEntitiesIds() {
            return activeEntitiesIds;
        }

        public void setActiveEntitiesIds(Set<String> activeEntitiesIds) {
            this.activeEntitiesIds = activeEntitiesIds;
        }
    }

    /** Подсистема */
    private Subsystem subsystem = Subsystem.UNKNOWN;

        /* Отображение категории состояния */
    /** Название приоритетной категории */
    protected String name = "";

    /** Цвет приоритетной категории в формате "#RRGGBB"*/
    private String color = "";

    /** Цвет текста в формате "#RRGGBB"*/
    private String fontColor;

    /** Счётчики состояний (по идентификатору категории состояния) */
    private Map<String, Counter> counters = new HashMap<>();

    /**
     * Список событий для ручного сброса
     * <команда_для_сброса, название_состояния>
     */
    private Map<String, String> resetActions = new HashMap<>();

    private FilterTags filterTags = new FilterTags();

    public SubsystemView(ActiveSubsystem activeSubsystem,
                         Counter priorityCounter,
                         Map<String, String> resetActions) {
        this.subsystem = activeSubsystem.getSubsystem();
        this.name = priorityCounter.getName();
        this.color = priorityCounter.getColor();
        this.fontColor = priorityCounter.getFontColor();
        this.resetActions = resetActions;
        this.filterTags = activeSubsystem.getFilterTags();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public Map<String, Counter> getCounters() {
        return counters;
    }

    public void setCounters(Map<String, Counter> counters) {
        this.counters = counters;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public Map<String, String> getResetActions() {
        return resetActions;
    }

    public void setResetActions(Map<String, String> resetActions) {
        this.resetActions = resetActions;
    }


    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }
}
