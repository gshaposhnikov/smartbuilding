package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Отображение устройства.
 * 
 * Сожержит хранимые в БД данные, необходимые сугубо для отображение конкретного устройства на различных формах.
 * 
 * @author Александр Горячкин
 * @ingroup UIDomain
 */
@Document(collection = "device_views")
@TypeAlias(value = "DeviceView")
public class DeviceView extends BasicEntityView {

    /** устройство */
    @DBRef private Device device = new Device();

    /** проект (идентификатор) */
    @JsonIgnore
    @Indexed
    private String projectId = "";
    /** идентификатор родительского устройства */
    @JsonIgnore
    @Indexed
    private String parentDeviceId = "";
    /** поисковый адресный путь в дереве устройств */
    @JsonIgnore
    @Indexed
    private String addressPath = "";
    /** Уровень адреса (кол-во узлов в адресе) */
    @JsonIgnore
    @Indexed
    private int addressLevel = 1;

    /** полный адресный путь в дереве устройств */
    private String fullAddressPath = "";
    /** средний адресный путь в дереве устройств */
    private String middleAddressPath = "";
    /** короткий адресный путь в дереве устройств */
    private String shortAddressPath = "";

    /** Иконка */
    private Media iconMedia = new Media();

    /**
     * Текстура для отображения на плане (поле актуально только для датчиков,
     * исполнительных устройств и приборов)
     */
    private Media textureMedia;

    /** Расположение на плане */
    public static class DevicePlanLayout extends AdjectiveEntity {
        /** Идентификатор плана */
        private String planId = "";
        /** Координата центра иконки устройства на плане */
        private CoordinatePoint coordinatePoint = new CoordinatePoint();

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public CoordinatePoint getCoordinatePoint() {
            return coordinatePoint;
        }

        public void setCoordinatePoint(CoordinatePoint coordinatePoint) {
            this.coordinatePoint = coordinatePoint;
        }
    }

    /** Список расположений устройства на планах (помещениях) */
    private List<DevicePlanLayout> planLayouts = new ArrayList<>();

    /** Идентификатор библиотеки шейпов устройства */
    private String deviceShapeLibraryId = "";

    public DeviceView() {
    }

    public DeviceView(Device device, DeviceProfileView deviceProfileView) {
        apply(device);
        apply(deviceProfileView);
    }

    public void apply(DeviceProfileView deviceProfileView) {
        this.setName(deviceProfileView.getName());
        this.setDescription(deviceProfileView.getDescription());
        this.setIconMedia(deviceProfileView.getIconMedia());
        this.setTextureMedia(deviceProfileView.getTextureMedia());
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public void apply(Device device) {
        this.device = device;
        this.id = device.getId();
        this.projectId = device.getProjectId();
        this.parentDeviceId = device.getParentDeviceId();
        this.addressPath = device.getAddressPath();
        this.addressLevel = device.getAddressLevel();
        /* По умолчанию все виды отображаемых адресных путей содержат только одну цифру - адресе самого устройства. */
        this.fullAddressPath = this.middleAddressPath = this.shortAddressPath = Integer.toString(device.getLineAddress());
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public String getAddressPath() {
        return addressPath;
    }

    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    public int getAddressLevel() {
        return addressLevel;
    }

    public void setAddressLevel(int addressLevel) {
        this.addressLevel = addressLevel;
    }

    public String getFullAddressPath() {
        return fullAddressPath;
    }

    public void setFullAddressPath(String fullAddressPath) {
        this.fullAddressPath = fullAddressPath;
    }

    public String getMiddleAddressPath() {
        return middleAddressPath;
    }

    public void setMiddleAddressPath(String middleAddressPath) {
        this.middleAddressPath = middleAddressPath;
    }

    public String getShortAddressPath() {
        return shortAddressPath;
    }

    public void setShortAddressPath(String shortAddressPath) {
        this.shortAddressPath = shortAddressPath;
    }

    public Media getIconMedia() {
        return iconMedia;
    }

    public void setIconMedia(Media iconMedia) {
        this.iconMedia = iconMedia;
    }

    public Media getTextureMedia() {
        return textureMedia;
    }

    public void setTextureMedia(Media textureMedia) {
        this.textureMedia = textureMedia;
    }

    public List<DevicePlanLayout> getPlanLayouts() {
        return planLayouts;
    }

    public void setPlanLayouts(List<DevicePlanLayout> planLayouts) {
        this.planLayouts = planLayouts;
    }

    public String getDeviceShapeLibraryId() {
        return deviceShapeLibraryId;
    }

    public void setDeviceShapeLibraryId(String deviceShapeLibraryId) {
        this.deviceShapeLibraryId = deviceShapeLibraryId;
    }

}
