package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;

import java.util.ArrayList;
import java.util.List;

public class AggregatedScenarioView extends IdAndNameAndDescr {
    /* Поля для отрисовки в главной таблице */
    
    /** Номер сценария (глобальный на проект) */
    private int globalNo;
    /** Цвет */
    private String color;
    /**Цвет шрифта */
    private String fontColor;
    /** Подсказка */
    private String hint;
    
    /* Поля для фильтрации главной таблицы */
    
    /** Назначение сценария */
    private ScenarioPurpose scenarioPurpose = ScenarioPurpose.EXEC_BY_LOGIC;
    /** Тип сценария */
    private ScenarioType scenarioType = ScenarioType.UNASSIGNED;
    /** Идентификаторы устройств. задействованных в сценарии */
    private List<String> deviceIds = new ArrayList<>();
    
    /* Подструктуры для отдельного (внетабличного) отображения и для редактирования */

    /** Базовые параметры сценария */
    private ScenarioBasicParams basicParams;
    /** Расширенные параметры сценария */
    private ScenarioAdvancedParams advancedParams;
    /** Логика включения */
    private ScenarioLogicBlock startLogic;
    /** Логика выключения */
    private ScenarioLogicBlock stopLogic;
    /** Блоки сценария для редактирования */
    private List<ScenarioTimeLineBlock> timeLineBlocks; 
    /** Блоки сценария для отображения в виде дерева */
    private List<TreeItemScenarioTimeLineBlockView> timeLineBlocksTree;
    
    public AggregatedScenarioView() {
    }

    public AggregatedScenarioView(Scenario scenario) {
        setId(scenario.getId());
        setName(scenario.getName());
        setDescription(scenario.getDescription());
        setGlobalNo(scenario.getGlobalNo());
        setHint(scenario.getGlobalNo(), scenario.getName());
        setScenarioPurpose(scenario.getScenarioPurpose());
        setScenarioType(scenario.getScenarioType());
        setStartLogic(scenario.getStartLogic());
        setStopLogic(scenario.getStopLogic());
        setTimeLineBlocks(scenario.getTimeLineBlocks());
    }

    public int getGlobalNo() {
        return globalNo;
    }
    
    public void setGlobalNo(int globalNo) {
        this.globalNo = globalNo;
    }
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public String getFontColor() {
        return fontColor;
    }
    
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }
    
    public String getHint() {
        return hint;
    }
    
    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setHint(int globalNo, String name){
        this.hint = globalNo + ". " + name;
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public ScenarioType getScenarioType() {
        return scenarioType;
    }

    public void setScenarioType(ScenarioType scenarioType) {
        this.scenarioType = scenarioType;
    }

    public List<String> getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(List<String> deviceIds) {
        this.deviceIds = deviceIds;
    }

    public ScenarioBasicParams getBasicParams() {
        return basicParams;
    }

    public void setBasicParams(ScenarioBasicParams basicParams) {
        this.basicParams = basicParams;
    }

    public ScenarioAdvancedParams getAdvancedParams() {
        return advancedParams;
    }

    public void setAdvancedParams(ScenarioAdvancedParams advancedParams) {
        this.advancedParams = advancedParams;
    }

    public ScenarioLogicBlock getStartLogic() {
        return startLogic;
    }

    public void setStartLogic(ScenarioLogicBlock startLogic) {
        this.startLogic = startLogic;
    }

    public ScenarioLogicBlock getStopLogic() {
        return stopLogic;
    }

    public void setStopLogic(ScenarioLogicBlock stopLogic) {
        this.stopLogic = stopLogic;
    }

    public List<ScenarioTimeLineBlock> getTimeLineBlocks() {
        return timeLineBlocks;
    }

    public void setTimeLineBlocks(List<ScenarioTimeLineBlock> timeLineBlocks) {
        this.timeLineBlocks = timeLineBlocks;
    }

    public List<TreeItemScenarioTimeLineBlockView> getTimeLineBlocksTree() {
        return timeLineBlocksTree;
    }

    public void setTimeLineBlocksTree(List<TreeItemScenarioTimeLineBlockView> timeLineBlocksTree) {
        this.timeLineBlocksTree = timeLineBlocksTree;
    }
    
}
