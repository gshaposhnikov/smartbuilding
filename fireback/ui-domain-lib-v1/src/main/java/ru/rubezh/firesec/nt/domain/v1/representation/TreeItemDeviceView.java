package ru.rubezh.firesec.nt.domain.v1.representation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.DeviceView.DevicePlanLayout;

/**
 * Отображение устройства (проектной части) как элемента дерева.
 * @author Александр Горячкин
 */
public class TreeItemDeviceView {

        /* Базовая информация об устройстве */
    /** Идентификатор устройства */
    private String id;
    /** Идентификатор профиля устройства */
    private String deviceProfileId;
    /** Подсистема */
    private Subsystem subsystem;
    /** Категория устройства */
    private DeviceCategory deviceCategory;
    /** Идентификатор проекта */
    private String projectId;
    /**
     * Устройство может использоваться в условиях исполнительных блоков
     * сценариев или в блоках слежения сценариев
     */
    private boolean acceptableAsScenarioCondition = false;

        /* Информация о зоне, к которой прикреплено устройство */
    /** Можно ли привязать к зоне */
    private boolean attachableToRegion;
    /** Идентификатор зоны */
    private String regionId;
    /** Название зоны */
    private String regionName;

        /* Адресация в дереве устройств  */
    /** Идентификатор родительского устройства */
    private String parentDeviceId;
    /** Поисковый адресный путь в дереве устройств */
    private String addressPath;
    /** Полный адресный путь в дереве устройств */
    private String fullAddressPath;
    /** Средний адресный путь в дереве устройств (с адресом предка) */
    private String middleAddressPath;
    /** Короткий адресный путь в дереве устройств (до предка) */
    private String shortAddressPath;
    /** Уровень адреса в дереве */
    private int addressLevel;
    /** Диапазоны свободных адресов (свой список диапазонов на каждую линию) */
    private List<List<AddressRange>> lineAddressRanges = new ArrayList<>();
    /** Возможность редактировать адрес устройства */
    private boolean editableAddress;
    /** Тип адреса устройства */
    private AddressType addressType;

        /* Атрибуты виртуальных контейнеров */
    /** Список Id связанных устройств (виртуального контейнера) */
    private List<String> linkedDeviceIds;
    /** Идентификатор виртуального контейнера, содержащего ссылку на устройство */
    private String virtualContainerId;

        /* Отображение профиля устройства */
    /** Название */
    private String name;
    /** Описание */
    private String description;
    /** Иконка */
    private Media iconMedia;
    /**
     * Текстура для отображения на плане (поле актуально только для датчиков,
     * исполнительных устройств и приборов)
     */
    private Media textureMedia;

        /* Отключение устройства */
    /** Отключаемое устройство */
    private boolean canBeDisabled;
    /** Отключение устройства запрещено */
    private boolean disableNotAllowed;
    /** Устройство отключено */
    private boolean disabled;
    /** Встроенное устройство (такое устройство нельзя удалить) */
    private boolean embedded;

    /** Список расположений устройства на планах (помещениях) */
    private List<DevicePlanLayout> planLayouts = new ArrayList<>();

    /** Метки для фильтрации по сущностям */
    private FilterTags filterTags;
    /** Кастомная подсистема */
    private Subsystem customSubsystem;

    public class DeviceProfileIdAndName {

        private String deviceProfileId;

        private String deviceProfileName;

        public DeviceProfileIdAndName(String deviceProfileId, String deviceProfileName) {
            super();
            this.deviceProfileId = deviceProfileId;
            this.deviceProfileName = deviceProfileName;
        }

        public String getDeviceProfileId() {
            return deviceProfileId;
        }

        public void setDeviceProfileId(String deviceProfileId) {
            this.deviceProfileId = deviceProfileId;
        }

        public String getDeviceProfileName() {
            return deviceProfileName;
        }

        public void setDeviceProfileName(String deviceProfileName) {
            this.deviceProfileName = deviceProfileName;
        }

    }

    private List<DeviceProfileIdAndName> acceptableDeviceProfiles = new ArrayList<>();

    /** Отображения параметров */
    private List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews;
    private List<AggregatedActiveDeviceConfigPropertyView> aggregatedPropertyViews = new ArrayList<>();

    /** Идентификатор библиотеки шейпов устройства */
    private String deviceShapeLibraryId;

    public TreeItemDeviceView() {

    }

    public TreeItemDeviceView(DeviceView deviceView,
                              DeviceProfileView deviceProfileView,
                              Region region,
                              List<DeviceProfileView> acceptableDeviceProfileViews,
                              List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews,
                              List<AggregatedActiveDeviceConfigPropertyView> otherPropertyViews) {

        Device deviceProject = deviceView.getDevice();
        DeviceProfile deviceProfile = deviceProfileView.getDeviceProfile();

        setId(deviceView.getId());
        setDeviceProfileId(deviceProfile.getId());
        setSubsystem(deviceProfile.getSubsystem());
        setDeviceCategory(deviceProfile.getDeviceCategory());
        setProjectId(deviceView.getProjectId());
        setAcceptableAsScenarioCondition(deviceProfile.isAcceptableAsScenarioCondition());

        setCanBeDisabled(deviceProfile.getCanBeDisabled());
        setDisableNotAllowed(deviceProject.isDisableNotAllowed());
        setDisabled(deviceProject.isDisabled());
        setEmbedded(deviceProject.isEmbedded());

        setAttachableToRegion(deviceProfile.isAttachableToRegion());
        if (isAttachableToRegion() && region != null) {
            setRegionId(region.getId());
            setRegionName(region.getName());
        }

        setParentDeviceId(deviceView.getParentDeviceId());
        setAddressPath(deviceView.getAddressPath());
        setAddressLevel(deviceView.getAddressLevel());
        setFullAddressPath(deviceView.getFullAddressPath());
        setMiddleAddressPath(deviceView.getMiddleAddressPath());
        setShortAddressPath(deviceView.getShortAddressPath());
        setLineAddressRanges(deviceProject.getLineAddressRanges());
        setEditableAddress(deviceProject.isEditableAddress());
        setAddressType(deviceProject.getAddressType());

        setLinkedDeviceIds(deviceProject.getLinkedDeviceIdsMap().values()
                .stream().flatMap(set -> set.stream()).collect(Collectors.toList()));
        setVirtualContainerId(deviceProject.getVirtualContainerId());

        /*
         * Для неактивного проекта имя устройства в дереве устройств всегда
         * должно отображаться по умолчанию (из профиля)
         */
        setName(deviceProfileView.getName());

        setDescription(deviceView.getDescription());
        setIconMedia(deviceView.getIconMedia());
        setTextureMedia(deviceView.getTextureMedia());
        setPlanLayouts(deviceView.getPlanLayouts());
        setDeviceShapeLibraryId(deviceView.getDeviceShapeLibraryId());

        if (acceptableDeviceProfileViews != null) {
            for (DeviceProfileView acceptableDeviceProfileView : acceptableDeviceProfileViews) {
                acceptableDeviceProfiles.add(new DeviceProfileIdAndName(acceptableDeviceProfileView.getId(),
                        acceptableDeviceProfileView.getName()));
            }
        }

        setConfigPropertyViews(configPropertyViews);
        setAggregatedPropertyViews(otherPropertyViews);

        setFilterTags(deviceProject.getFilterTags());
        setCustomSubsystem(deviceProject.getCustomSubsystem());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public DeviceCategory getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(DeviceCategory deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public boolean isAcceptableAsScenarioCondition() {
        return acceptableAsScenarioCondition;
    }

    public void setAcceptableAsScenarioCondition(boolean acceptableAsScenarioCondition) {
        this.acceptableAsScenarioCondition = acceptableAsScenarioCondition;
    }

    public boolean isAttachableToRegion() {
        return attachableToRegion;
    }

    public void setAttachableToRegion(boolean attachableToRegion) {
        this.attachableToRegion = attachableToRegion;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public String getAddressPath() {
        return addressPath;
    }

    public void setAddressPath(String addressPath) {
        this.addressPath = addressPath;
    }

    public int getAddressLevel() {
        return addressLevel;
    }

    public void setAddressLevel(int addressLevel) {
        this.addressLevel = addressLevel;
    }

    public List<List<AddressRange>> getLineAddressRanges() {
        return lineAddressRanges;
    }

    public void setLineAddressRanges(List<List<AddressRange>> lineAddressRanges) {
        this.lineAddressRanges = lineAddressRanges;
    }


    public String getFullAddressPath() {
        return fullAddressPath;
    }

    public void setFullAddressPath(String fullAddressPath) {
        this.fullAddressPath = fullAddressPath;
    }

    public String getMiddleAddressPath() {
        return middleAddressPath;
    }

    public void setMiddleAddressPath(String middleAddressPath) {
        this.middleAddressPath = middleAddressPath;
    }

    public String getShortAddressPath() {
        return shortAddressPath;
    }

    public void setShortAddressPath(String shortAddressPath) {
        this.shortAddressPath = shortAddressPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Media getIconMedia() {
        return iconMedia;
    }

    public void setIconMedia(Media iconMedia) {
        this.iconMedia = iconMedia;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Media getTextureMedia() {
        return textureMedia;
    }

    public void setTextureMedia(Media textureMedia) {
        this.textureMedia = textureMedia;
    }

    public List<DevicePlanLayout> getPlanLayouts() {
        return planLayouts;
    }

    public void setPlanLayouts(List<DevicePlanLayout> planLayouts) {
        this.planLayouts = planLayouts;
    }

    public List<DeviceProfileIdAndName> getAcceptableDeviceProfiles() {
        return acceptableDeviceProfiles;
    }

    public void setAcceptableDeviceProfiles(List<DeviceProfileIdAndName> acceptableDeviceProfiles) {
        this.acceptableDeviceProfiles = acceptableDeviceProfiles;
    }

    public void setEditableAddress(boolean editableAddress) {
        this.editableAddress = editableAddress;
    }

    public boolean isEditableAddress() {
        return editableAddress;
    }

    public boolean isEmbedded() {
        return embedded;
    }

    public void setEmbedded(boolean embedded) {
        this.embedded = embedded;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public List<String> getLinkedDeviceIds() {
        return linkedDeviceIds;
    }

    public void setLinkedDeviceIds(List<String> linkedDeviceIds) {
        this.linkedDeviceIds = linkedDeviceIds;
    }

    public List<AggregatedActiveDeviceConfigPropertyView> getConfigPropertyViews() {
        return configPropertyViews;
    }

    public void setConfigPropertyViews(List<AggregatedActiveDeviceConfigPropertyView> configPropertyViews) {
        this.configPropertyViews = configPropertyViews;
    }

    public List<AggregatedActiveDeviceConfigPropertyView> getAggregatedPropertyViews() {
        return aggregatedPropertyViews;
    }

    public void setAggregatedPropertyViews(List<AggregatedActiveDeviceConfigPropertyView> aggregatedPropertyViews_) {
        this.aggregatedPropertyViews = aggregatedPropertyViews_;
    }

    public boolean getCanBeDisabled() {
        return canBeDisabled;
    }

    public void setCanBeDisabled(boolean canBeDisabled) {
        this.canBeDisabled = canBeDisabled;
    }

    public boolean isDisableNotAllowed() {
        return disableNotAllowed;
    }

    public void setDisableNotAllowed(boolean disableNotAllowed) {
        this.disableNotAllowed = disableNotAllowed;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getVirtualContainerId() {
        return virtualContainerId;
    }

    public void setVirtualContainerId(String virtualContainerId) {
        this.virtualContainerId = virtualContainerId;
    }


    public String getDeviceShapeLibraryId() {
        return deviceShapeLibraryId;
    }

    public void setDeviceShapeLibraryId(String deviceShapeLibraryId) {
        this.deviceShapeLibraryId = deviceShapeLibraryId;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

    public Subsystem getCustomSubsystem() {
        return customSubsystem;
    }

    public void setCustomSubsystem(Subsystem customSubsystem) {
        this.customSubsystem = customSubsystem;
    }
}
