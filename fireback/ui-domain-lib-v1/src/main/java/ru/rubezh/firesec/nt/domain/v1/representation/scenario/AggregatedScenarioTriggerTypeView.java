package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTriggerType.TriggerEntityType;
import ru.rubezh.firesec.nt.domain.v1.representation.IdAndNameAndDescr;

/**
 * Сборочное отображение типов триггеров сценариев, для REST-API.
 * @author Антон Васильев
 *
 */
public class AggregatedScenarioTriggerTypeView extends IdAndNameAndDescr {
    /** Тип сущности триггера. */
    private TriggerEntityType entityType;
    /** Тип подсистемы (актуально только для триггеров по зоне) */
    private Subsystem subsystem;
    /** Выставляемый тип логики */
    private ScenarioLogicBlock.LogicType forcedScenarioLogicType;
    /** Идентификатор типа действия для исполнительных блоков */
    private String forcedScenarioActionTypeId;
    /** Тип назначения сценария, для которого разрешено использовать триггер. */
    private ScenarioPurpose scenarioPurpose;

    public AggregatedScenarioTriggerTypeView() {
    }

    public AggregatedScenarioTriggerTypeView(ScenarioTriggerType triggerType, ScenarioTriggerTypeView triggerTypeView) {
        setId(triggerType.getId());
        setEntityType(triggerType.getEntityType());
        setSubsystem(triggerType.getSubsystem());
        setForcedScenarioLogicType(triggerType.getForcedScenarioLogicType());
        setForcedScenarioActionTypeId(triggerType.getForcedScenarioActionTypeId());
        setScenarioPurpose(triggerType.getScenarioPurpose());

        setName(triggerTypeView.getName());
        setDescription(triggerTypeView.getDescription());
    }

    public TriggerEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(TriggerEntityType entityType) {
        this.entityType = entityType;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public ScenarioLogicBlock.LogicType getForcedScenarioLogicType() {
        return forcedScenarioLogicType;
    }

    public void setForcedScenarioLogicType(ScenarioLogicBlock.LogicType forcedScenarioLogicType) {
        this.forcedScenarioLogicType = forcedScenarioLogicType;
    }

    public String getForcedScenarioActionTypeId() {
        return forcedScenarioActionTypeId;
    }

    public void setForcedScenarioActionTypeId(String forcedScenarioActionTypeId) {
        this.forcedScenarioActionTypeId = forcedScenarioActionTypeId;
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

}
