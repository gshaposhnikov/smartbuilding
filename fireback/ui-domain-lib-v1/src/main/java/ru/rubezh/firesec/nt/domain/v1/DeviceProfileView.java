package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Профиль отображения устройства.
 * 
 * Используется для определения параметров отображения устройств соответствующего типа.
 *
 * @author Александр Горячкин, Антон Васильев
 * @ingroup UIReferences
 */
@Document(collection = "device_profile_views")
@TypeAlias(value = "DeviceProfileView")
public class DeviceProfileView extends ReferenceEntityView {

    /**
     * Профиль устройства
     * 
     * Идентификатор профиля устройства и его отображения должны совпадать.
     * 
     * Автоматические ссылки на объекты использовать не рекомендуется
     */
    @DBRef
    @Deprecated
    private DeviceProfile deviceProfile = new DeviceProfile();

    /**
     * Иконка
     * 
     * Автоматические ссылки на объекты использовать не рекомендуется
     */
    @DBRef
    @Deprecated
    private Media iconMedia = new Media();
    /** Идентификатор иконки */
    private String iconMediaId = "";

    /**
     * Текстура, изображаемая на плане на пиктограмме устройства (поле актуально
     * только для датчиков, исполнительных устройств и приборов)
     * 
     * Автоматические ссылки на объекты использовать не рекомендуется
     */
    @DBRef
    @Deprecated
    private Media textureMedia;
    /**
     * Идентификатор текстуры, изображаемой на плане на пиктограмме устройства
     * (поле актуально только для датчиков, исполнительных устройств и приборов)
     */
    private String textureMediaId = "";

    private Language language = Language.RUSSIAN;

    /** Отображение параметров конфигурации в виде пар "идентификатор параметра: его отображение" */
    private Map<String, DeviceConfigPropertyView> configPropertyViews = new HashMap<>();

    /** Отображение параметров устройства в виде пар "идентификатор параметра: его отображение" */
    private Map<String, DeviceConfigPropertyView> propertyViews = new HashMap<>();

    /** Отображение поддерживаемых действий устройства в виде пар "идентификатор действия: его отображение" */
    private Map<String, DeviceActionView> supportedActionViews;

    /** Отображения состояний, которые могут возникать на устройстве (должно заполняться сервисом при получении данных из БД) */
    @Transient
    private List<StateView> stateViews = new ArrayList<>();

    /** Псевдоним номера линии */
    private String lineNoAlias = "Номер линии";

    /** Псевдоним адреса */
    private String addressAlias = "Адрес";

    /** Псевдоним кол-ва */
    private String countAlias = "Кол-во";

    /** Отображение имён пользователей устройства */
    private Map<String, String> deviceUserNames = new HashMap<>();

    /** Описание требований к паролю пользователя */
    private String userPasswordDescription = "";

    /** Префикс адресного пути (полного и среднего), отображаемый для корневых устройств */
    private String rootAddressPathPrefix = "";

    public DeviceProfile getDeviceProfile() {
        return deviceProfile;
    }

    public void setDeviceProfile(DeviceProfile deviceProfile) {
        this.deviceProfile = deviceProfile;
        this.id = deviceProfile.getId();
    }

    public Media getIconMedia() {
        return iconMedia;
    }

    public void setIconMedia(Media iconMedia) {
        this.iconMedia = iconMedia;
        this.iconMediaId = iconMedia.getId();
    }

    public String getIconMediaId() {
        return iconMediaId;
    }

    public void setIconMediaId(String iconMediaId) {
        this.iconMediaId = iconMediaId;
    }

    public Media getTextureMedia() {
        return textureMedia;
    }

    public void setTextureMedia(Media textureMedia) {
        this.textureMedia = textureMedia;
        if (textureMedia != null) {
            this.textureMediaId = textureMedia.getId();
        } else {
            textureMediaId = "";
        }
    }

    public String getTextureMediaId() {
        return textureMediaId;
    }

    public void setTextureMediaId(String textureMediaId) {
        this.textureMediaId = textureMediaId;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Map<String, DeviceConfigPropertyView> getConfigPropertyViews() {
        return configPropertyViews;
    }

    public void setConfigPropertyViews(Map<String, DeviceConfigPropertyView> configPropertyViews) {
        this.configPropertyViews = configPropertyViews;
    }

    public Map<String, DeviceConfigPropertyView> getPropertyViews() {
        return propertyViews;
    }

    public void setPropertyViews(Map<String, DeviceConfigPropertyView> propertyViews) {
        this.propertyViews = propertyViews;
    }

    public Map<String, DeviceActionView> getSupportedActionViews() {
        return supportedActionViews;
    }

    public void setSupportedActionViews(Map<String, DeviceActionView> supportedActionViews_) {
        supportedActionViews = supportedActionViews_;
    }

    public Map<String, String> getDeviceUserNames() {
        return deviceUserNames;
    }

    public void setDeviceUserNames(Map<String, String> deviceUserNames) {
        this.deviceUserNames = deviceUserNames;
    }

    public String getUserPasswordDescription() {
        return userPasswordDescription;
    }

    public void setUserPasswordDescription(String userPasswordDescription) {
        this.userPasswordDescription = userPasswordDescription;
    }

    public void setStateViews(List<StateView> stateViews) {
        this.stateViews = stateViews;
    }

    public List<StateView> getStateViews() {
        return stateViews;
    }

    public String getLineNoAlias() {
        return lineNoAlias;
    }

    public void setLineNoAlias(String lineNoAlias) {
        this.lineNoAlias = lineNoAlias;
    }

    public String getAddressAlias() {
        return addressAlias;
    }

    public void setAddressAlias(String addressAlias) {
        this.addressAlias = addressAlias;
    }

    public String getCountAlias() {
        return countAlias;
    }

    public void setCountAlias(String countAlias) {
        this.countAlias = countAlias;
    }

    public String getRootAddressPathPrefix() {
        return rootAddressPathPrefix;
    }

    public void setRootAddressPathPrefix(String rootAddressPathPrefix) {
        this.rootAddressPathPrefix = rootAddressPathPrefix;
    }

}
