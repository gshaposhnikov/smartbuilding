package ru.rubezh.firesec.nt.domain.v1.representation;

public class ImportedXmlProjectWrapper {

    private String importedFile = null;

    public ImportedXmlProjectWrapper(String importedFile) {
        this.importedFile = importedFile;
    }

    public String getImportedFile() {
        return importedFile;
    }

    public void setImportedFile(String importedFile) {
        this.importedFile = importedFile;
    }
}
