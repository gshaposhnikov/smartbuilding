package ru.rubezh.firesec.nt.domain.v1;

import java.util.Map;

/**
 * Отображение команды устройству.
 *
 * @author Андрей Лисовой
 * @ingroup UIReferences
 */
public class DeviceActionView extends AdjectiveEntityView {

    /** Отображение параметров конфигурации действия в виде пар "идентификатор параметра: его отображение" */
    private Map<String, DeviceConfigPropertyView> configPropertyViews;

    public Map<String, DeviceConfigPropertyView> getConfigPropertyViews() {
        return configPropertyViews;
    }

    public void setConfigPropertyViews(Map<String, DeviceConfigPropertyView> configPropertyViews_) {
        configPropertyViews = configPropertyViews_;
    }
}
