package ru.rubezh.firesec.nt.domain.v1;

/**
 * Список поддерживаемых языков в системе
 * @author Александр Горячкин
 */
public enum Language {
    /** Русский */
    RUSSIAN,
    /** Английский */
    ENGLISH,
    /** Немецкий */
    DEUTCHE
}
