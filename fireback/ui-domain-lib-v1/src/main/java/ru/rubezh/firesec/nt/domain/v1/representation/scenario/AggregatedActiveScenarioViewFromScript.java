package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AggregatedActiveScenarioViewFromScript{

    private Scenario scenario;
    private StateCategoryView generalStateCategoryView;

    /** Cостояния, возникающие на устройствах, для проверки условия */
    private List<StateView> tlBlockConditionStateViews = new ArrayList<>();

    /* В коллекциях ниже могут находиться ненужные обьекты,
        т.к. из базы они запрашиваются только по entityId действия (тип сущности не проверяется) */

    /** Представления устройств из действий блоков времени */
    private Set<DeviceView> actionDeviceViews = new HashSet<>();
    /** Зоны из действий блоков времени */
    private Set<Region> actionRegions = new HashSet<>();
    /** Сценарии из действий блоков времени */
    private Set<Scenario> actionScenarios = new HashSet<>();
    /** Виртуальные состояния из действий блоков времени */
    private Set<VirtualState> actionVirtualStates = new HashSet<>();

    /** Представления отслеживаемых устройств и устройств из
     * условий исполнительных блоков */
    private Set<DeviceView> tlBlockDeviceViews = new HashSet<>();
    /** Отслеживаемые виртуальные состояния и вирт. состояния
     * из условий исполнительных блоков*/
    private Set<VirtualState> tlBlockVirtualStates = new HashSet<>();

    /** Представления устройств из блоков логики */
    private Set<DeviceView> logicBlockDeviceViews = new HashSet<>();
    /** Зоны из блоков логики */
    private Set<Region> logicBlockRegions = new HashSet<>();
    /** Виртуальные состояния из блоков логики */
    private Set<VirtualState> logicBlockVirtualStates = new HashSet<>();

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public StateCategoryView getGeneralStateCategoryView() {
        return generalStateCategoryView;
    }

    public void setGeneralStateCategoryView(StateCategoryView generalStateCategoryView) {
        this.generalStateCategoryView = generalStateCategoryView;
    }

    public List<StateView> getTlBlockConditionStateViews() {
        return tlBlockConditionStateViews;
    }

    public void setTlBlockConditionStateViews(List<StateView> tlBlockConditionStateViews) {
        this.tlBlockConditionStateViews = tlBlockConditionStateViews;
    }

    public Set<DeviceView> getActionDeviceViews() {
        return actionDeviceViews;
    }

    public void setActionDeviceViews(Set<DeviceView> actionDeviceViews) {
        this.actionDeviceViews = actionDeviceViews;
    }

    public Set<Region> getActionRegions() {
        return actionRegions;
    }

    public void setActionRegions(Set<Region> actionRegions) {
        this.actionRegions = actionRegions;
    }

    public Set<Scenario> getActionScenarios() {
        return actionScenarios;
    }

    public void setActionScenarios(Set<Scenario> actionScenarios) {
        this.actionScenarios = actionScenarios;
    }

    public Set<VirtualState> getActionVirtualStates() {
        return actionVirtualStates;
    }

    public void setActionVirtualStates(Set<VirtualState> actionVirtualStates) {
        this.actionVirtualStates = actionVirtualStates;
    }

    public Set<DeviceView> getTlBlockDeviceViews() {
        return tlBlockDeviceViews;
    }

    public void setTlBlockDeviceViews(Set<DeviceView> tlBlockDeviceViews) {
        this.tlBlockDeviceViews = tlBlockDeviceViews;
    }

    public Set<VirtualState> getTlBlockVirtualStates() {
        return tlBlockVirtualStates;
    }

    public void setTlBlockVirtualStates(Set<VirtualState> tlBlockVirtualStates) {
        this.tlBlockVirtualStates = tlBlockVirtualStates;
    }

    public Set<DeviceView> getLogicBlockDeviceViews() {
        return logicBlockDeviceViews;
    }

    public void setLogicBlockDeviceViews(Set<DeviceView> logicBlockDeviceViews) {
        this.logicBlockDeviceViews = logicBlockDeviceViews;
    }

    public Set<Region> getLogicBlockRegions() {
        return logicBlockRegions;
    }

    public void setLogicBlockRegions(Set<Region> logicBlockRegions) {
        this.logicBlockRegions = logicBlockRegions;
    }

    public Set<VirtualState> getLogicBlockVirtualStates() {
        return logicBlockVirtualStates;
    }

    public void setLogicBlockVirtualStates(Set<VirtualState> logicBlockVirtualStates) {
        this.logicBlockVirtualStates = logicBlockVirtualStates;
    }
}
