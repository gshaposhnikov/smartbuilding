package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.ScenarioPurpose;
import ru.rubezh.firesec.nt.domain.v1.ScenarioType;

/**
 * Базовые параметры сценариев.
 * @author Антон Васильев
 *
 */
public class ScenarioBasicParams {
    /** Название сценария (передаваемое в прибор) */
    private String name;
    /** Описание (примечание) сценария */
    private String description;
    /** Назначение сценария */
    private ScenarioPurpose scenarioPurpose;
    /** Тип сценария */
    private ScenarioType scenarioType;
    /** Идентификатор прибора (только для исполнительных сценариев) */
    private String controlDeviceId;
    /** Включен или выключен (заблокирован) сценарий */
    private boolean enabled;

    public ScenarioBasicParams() {
    }

    public ScenarioBasicParams(Scenario scenario) {
        setName(scenario.getName());
        setDescription(scenario.getDescription());
        setScenarioPurpose(scenario.getScenarioPurpose());
        setScenarioType(scenario.getScenarioType());
        setControlDeviceId(scenario.getControlDeviceId());
        setEnabled(scenario.isEnabled());
    }

    public ScenarioPurpose getScenarioPurpose() {
        return scenarioPurpose;
    }

    public void setScenarioPurpose(ScenarioPurpose scenarioPurpose) {
        this.scenarioPurpose = scenarioPurpose;
    }

    public ScenarioType getScenarioType() {
        return scenarioType;
    }

    public void setScenarioType(ScenarioType scenarioType) {
        this.scenarioType = scenarioType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControlDeviceId() {
        return controlDeviceId;
    }
    
    public void setControlDeviceId(String controlDeviceId) {
        this.controlDeviceId = controlDeviceId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
