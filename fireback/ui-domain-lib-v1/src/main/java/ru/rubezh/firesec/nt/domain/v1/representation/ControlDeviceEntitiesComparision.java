package ru.rubezh.firesec.nt.domain.v1.representation;

import ru.rubezh.firesec.nt.domain.v1.ControlDeviceEntities;

/**
 * Сравнение сущностей прибора
 * 
 * @author Андрей Лисовой
 *
 */
public class ControlDeviceEntitiesComparision {
    /** Идентификатор задачи чтения базы */
    private String issueId;
    /** Текущая конфигурация прибора */
    private ControlDeviceEntities currentEntities;
    /** Считанная конфигурация */
    private ControlDeviceEntities recoveredEntities;
    /** Отображение текущей конфигурации прибора */
    private ControlDeviceEntitiesView currentEntitiesView;
    /** Отображение считанной конфигурации */
    private ControlDeviceEntitiesView recoveredEntitiesView;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public ControlDeviceEntities getCurrentEntities() {
        return currentEntities;
    }

    public void setCurrentEntities(ControlDeviceEntities currentEntities) {
        this.currentEntities = currentEntities;
    }

    public ControlDeviceEntities getRecoveredEntities() {
        return recoveredEntities;
    }

    public void setRecoveredEntities(ControlDeviceEntities recoveredEntities) {
        this.recoveredEntities = recoveredEntities;
    }

    public ControlDeviceEntitiesView getCurrentEntitiesView() {
        return currentEntitiesView;
    }

    public void setCurrentEntitiesView(ControlDeviceEntitiesView currentEntitiesView) {
        this.currentEntitiesView = currentEntitiesView;
    }

    public ControlDeviceEntitiesView getRecoveredEntitiesView() {
        return recoveredEntitiesView;
    }

    public void setRecoveredEntitiesView(ControlDeviceEntitiesView recoveredEntitiesView) {
        this.recoveredEntitiesView = recoveredEntitiesView;
    }

}
