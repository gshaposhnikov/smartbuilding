package ru.rubezh.firesec.nt.domain.v1;

/**
 * Направление расположения виджетов внутри контейнера.
 * 
 * @author Александр Горячкин
 */
public enum WidgetLayout {
    /** горизонтально */
    horizontal,
    /** вертикально */
    vertical,
    /** во вкладке горизонтально */
    tabHorizontal,
    /** во вкладке вертикально */
    tabVertical
}
