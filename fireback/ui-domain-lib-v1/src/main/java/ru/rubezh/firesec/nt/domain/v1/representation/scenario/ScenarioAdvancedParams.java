package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.Scenario;
import ru.rubezh.firesec.nt.domain.v1.Scenario.StopType;

/**
 * Расширенные параметры сценариев.
 * @author Антон Васильев
 *
 */
public class ScenarioAdvancedParams {
    /** Разрешено ручное управление (запуск/остановка/блокировка/разблокировка) из ОЗ */
    private boolean manualStartStopAllowed;
    /** Выключение сценария при их глобальном сбросе */
    private boolean stopOnGlobalReset;
    /** Тип остановки сценария */
    private StopType stopType;
    /** Фраза описания логики включения. */
    private String startLogicSentence;
    /** Фраза описания логики выключения. */
    private String stopLogicSentence;

    public ScenarioAdvancedParams() {
    }

    public ScenarioAdvancedParams(Scenario scenario) {
        setManualStartStopAllowed(scenario.isManualStartStopAllowed());
        setStopOnGlobalReset(scenario.isStopOnGlobalReset());
        setStopType(scenario.getStopType());
    }

    public boolean isManualStartStopAllowed() {
        return manualStartStopAllowed;
    }

    public void setManualStartStopAllowed(boolean manualStartStopAllowed) {
        this.manualStartStopAllowed = manualStartStopAllowed;
    }

    public boolean isStopOnGlobalReset() {
        return stopOnGlobalReset;
    }

    public void setStopOnGlobalReset(boolean stopOnGlobalReset) {
        this.stopOnGlobalReset = stopOnGlobalReset;
    }

    public StopType getStopType() {
        return stopType;
    }

    public void setStopType(StopType stopType) {
        this.stopType = stopType;
    }

    public String getStartLogicSentence() {
        return startLogicSentence;
    }

    public void setStartLogicSentence(String startLogicSentence) {
        this.startLogicSentence = startLogicSentence;
    }

    public String getStopLogicSentence() {
        return stopLogicSentence;
    }

    public void setStopLogicSentence(String stopLogicSentence) {
        this.stopLogicSentence = stopLogicSentence;
    }

}
