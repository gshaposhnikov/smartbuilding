package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Базовый класс для определения отображений сущностей, содержащих название и описание.
 * 
 * @author Антон Васильев
 *
 */
public class BasicEntityView extends BasicEntity {
    /** Название */
    @Indexed
    protected String name = "";
   
    /** Описание */
    protected String description = "";

    void updateBasic(BasicEntityView entityView) {
        setName(entityView.getName());
        setDescription(entityView.getDescription());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
