package ru.rubezh.firesec.nt.domain.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Отображение зоны.
 * 
 * Параметры зоны, хранимые в БД, необходимые сугубо для ее отображения.
 * 
 * @author Александр Горячкин
 * @ingroup UIDomain
 */
@Document(collection = "region_views")
@TypeAlias(value = "RegionView")
public class RegionView extends BasicEntityView {
    
    public  RegionView() {
        super();
    }

    /** Зона */
    @DBRef private Region region = new Region();

    /* Дублирование полей зоны для оптимизации поиска в БД */
    /** Проект (идентификатор) */
    @Indexed
    private String projectId = "";

    /** Расположение зоны на плане. */
    public static class RegionPlanLayout {
        /** Идентификатор плана */
        private String planId = "";
        /** Флаг для интерпретации фигуры в качестве прямоугольника */
        private boolean rect;
        /**
         * Вершины многоугольника (верхний левый и нижний правый углы для
         * прямоугольника)
         */
        private List<CoordinatePoint> points = new ArrayList<>();

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public List<CoordinatePoint> getPoints() {
            return points;
        }

        public void setPoints(List<CoordinatePoint> points) {
            this.points = points;
        }

        public boolean isRect() {
            return rect;
        }

        public void setRect(boolean rect) {
            this.rect = rect;
        }

        @JsonIgnore
        public double getMinX() {
            double minX = 0.0;
            for (CoordinatePoint point : points) {
                if (minX > point.getX()) {
                    minX = point.getX();
                }
            }
            return minX;
        }

        @JsonIgnore
        public double getMaxX() {
            double maxX = 0.0;
            for (CoordinatePoint point : points) {
                if (maxX < point.getX()) {
                    maxX = point.getX();
                }
              }
            return maxX;
        }

        @JsonIgnore
        public double getMinY() {
            double minY = 0.0;
            for (CoordinatePoint point : points) {
                if (minY > point.getY()) {
                    minY = point.getY();
                }
            }
            return minY;
        }

        @JsonIgnore
        public double getMaxY() {
            double maxY = 0.0;
            for (CoordinatePoint point : points) {
                if (maxY < point.getY()) {
                    maxY = point.getY();
                }
            }
            return maxY;
        }

    }

    /** Список расположений зоны на планах (помещениях) */
    List<RegionPlanLayout> planLayouts = new ArrayList<>();

    public RegionView(Region region) {
        this.setRegion(region);
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.id = region.getId();
        this.name = region.getName();
        this.description = region.getDescription();
        this.projectId = region.getProjectId();
        this.region = region;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public List<RegionPlanLayout> getPlanLayouts() {
        return planLayouts;
    }

    public void setPlanLayouts(List<RegionPlanLayout> planLayouts) {
        this.planLayouts = planLayouts;
    }

}
