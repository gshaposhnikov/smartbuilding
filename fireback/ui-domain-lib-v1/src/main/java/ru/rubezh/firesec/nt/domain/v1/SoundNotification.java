package ru.rubezh.firesec.nt.domain.v1;

import java.util.Date;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Звуковое оповещение.
 * 
 * Используется для звукового оповещения при выявлении состояния.
 * 
 * @author Александр Горячкин, Антон Караваев.
 * @ingroup UIDomain
 */
@Document(collection = "sound_notifications")
@TypeAlias(value = "SoundNotification")
public class SoundNotification extends BasicEntityWithDate{

    /** проигрывать непрерывано */
    private boolean unstoppable;

    /** класс состояния */
    private String stateCategoryId = "";

    /** медиафайл (тип "звук") */
    private String mediaId = "";

    public void update(SoundNotification soundNotification) {
        updateDateTime = new Date();
        unstoppable = soundNotification.unstoppable;
        mediaId = soundNotification.mediaId;
    }

    public String getMediaId(){
        return mediaId;
    }

    public void setMediaId(String mediaId){
        this.mediaId = mediaId;
    }

    public boolean getUnstoppable() {
        return unstoppable;
    }

    public void setUnstoppable(boolean unstoppable) {
        this.unstoppable = unstoppable;
    }

    public String getStateCategoryId() {
        return stateCategoryId;
    }

    public void setStateCategoryId(String stateCategoryId) {
        this.stateCategoryId = stateCategoryId;
    }
}
