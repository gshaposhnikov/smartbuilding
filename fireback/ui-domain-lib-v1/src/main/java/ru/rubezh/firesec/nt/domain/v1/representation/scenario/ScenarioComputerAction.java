package ru.rubezh.firesec.nt.domain.v1.representation.scenario;

import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock;
import ru.rubezh.firesec.nt.domain.v1.ScenarioTimeLineBlock.Command;

public class ScenarioComputerAction {

    /** Название блока */
    private String name = "";

    /** Задержка по времени, в секундах */
    private int timeDelaySec;

    /** Заголовок для всплывающего окна (только для @code{Command.SHOW_MESSAGE}) */
    private String title;

    /** Сообщение */
    private String message = "";

    /** Время обратного отсчета (только для @code{Command.COUNTDOWN}) */
    private int countdownTimeSec;

    /** Команда */
    private Command command;

    ScenarioComputerAction(){}

    public ScenarioComputerAction(ScenarioTimeLineBlock tlBlock, ScenarioTimeLineBlock.ComputerAction computerAction) {
        name = tlBlock.getName();
        timeDelaySec = tlBlock.getTimeDelaySec();
        message = computerAction.getMessage();
        if (!computerAction.getTitle().isEmpty()){
            title = computerAction.getTitle();
        } else if (computerAction.getCountdownTimeSec() != 0){
            countdownTimeSec = computerAction.getCountdownTimeSec();
        }
        command = computerAction.getCommand();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTimeDelaySec() {
        return timeDelaySec;
    }

    public void setTimeDelaySec(int timeDelaySec) {
        this.timeDelaySec = timeDelaySec;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCountdownTimeSec() {
        return countdownTimeSec;
    }

    public void setCountdownTimeSec(int countdownTimeSec) {
        this.countdownTimeSec = countdownTimeSec;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
