package ru.rubezh.firesec.nt.domain.v1.representation;

/**
 * Информация для подключения устройств.
 * @author Александр Горячкин
 */
public class DeviceCreateView {

    /** Тип (идентификатор профиля) устройства*/
    private String deviceProfileId;

    /** Идентификатор родительского устройства */
    private String parentDeviceId = null;

    /** Номер линии (родительского устройства)
     * [1 .. deviceProfile.getLineCount()]
     */
    private int lineNo = 1;

    /** Собственный адрес (на линии)
     * [1 .. deviceProfile.getLineDeviceCount()]
     */
    private int lineAddress = 1;

    /** Кол-во создаваемых устройств */
    private int count = 1;


    public String getDeviceProfileId() {
        return deviceProfileId;
    }

    public void setDeviceProfileId(String deviceProfileId) {
        this.deviceProfileId = deviceProfileId;
    }

    public String getParentDeviceId() {
        return parentDeviceId;
    }

    public void setParentDeviceId(String parentDeviceId) {
        this.parentDeviceId = parentDeviceId;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public int getLineAddress() {
        return lineAddress;
    }

    public void setLineAddress(int lineAddress) {
        this.lineAddress = lineAddress;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
