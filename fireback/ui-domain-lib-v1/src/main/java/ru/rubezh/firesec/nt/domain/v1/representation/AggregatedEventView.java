package ru.rubezh.firesec.nt.domain.v1.representation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import ru.rubezh.firesec.nt.domain.v1.*;
import ru.rubezh.firesec.nt.domain.v1.skud.Employee;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Агрегирующее отображение события.
 *
 * Агрегирует в себе информацию по связанным сущностям.
 *
 * @author Антон Васильев
 * @ingroup UIDomain
 */
@QueryEntity
@Document(collection = "aggregated_event_views")
@TypeAlias(value = "AggregatedEventView")
@CompoundIndexes({
        @CompoundIndex(name = "indicator_idx", def = "{'indicatorPanelId':1, 'rowNo':1, 'colNo':1}"),
        @CompoundIndex(name = "tags_idx", def = "{'filterTags.tags':1}")
})
public class AggregatedEventView extends BasicEntityView {

    public EmployeeEventInfo getEmployeeEventInfo() {
        return employeeEventInfo;
    }

    public void setEmployeeEventInfo(EmployeeEventInfo employeeEventInfo) {
        this.employeeEventInfo = employeeEventInfo;
    }

    public static class ControlDeviceEventInfo {

        /** Идентификатор прибора (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";

        /** Название */
        private String name = "";

        /** Описание */
        private String description = "";

        /** Номер записи в журнале событий прибора */
        private long recordNo;

        /** Адрес прибора */
        private String address = "";

        /** Перечень активных состояний прибора */
        private List<String> states;

        public ControlDeviceEventInfo() {

        }

        public ControlDeviceEventInfo(Event event, DeviceView deviceView) {
            this.idRef = deviceView.getId();
            this.recordNo = event.getRecordNo();

            this.name = deviceView.getName();
            this.description = deviceView.getDescription();
            this.address = deviceView.getMiddleAddressPath();
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public long getRecordNo() {
            return recordNo;
        }

        public void setRecordNo(long recordNo) {
            this.recordNo = recordNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public List<String> getStates() {
            return states;
        }

        public void setStates(List<String> states) {
            this.states = states;
        }

    }

    public static class DeviceEventInfo{

        /** Идентификатор устройства (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";

        /** Название */
        private String name = "";

        /** Описание */
        private String description = "";

        /** Адрес прибора */
        private String address = "";

        /** Перечень активных состояний устройства */
        private List<String> states = new ArrayList<>();

        public DeviceEventInfo() {

        }

        public DeviceEventInfo(DeviceView deviceView, List<String> states) {
            this.idRef = deviceView.getId();
            this.states = states;

            Device device = deviceView.getDevice();

            String alternateName = device.getDevicePropertyValues().get(DeviceProperties.AlternateName.toString());
            if ((alternateName != null) && !alternateName.isEmpty())
                this.name = alternateName;
            else
                this.name = deviceView.getName();
            this.description = deviceView.getDescription();

            this.address = String.valueOf(device.getLineAddress());
            if(device.getParentDeviceId() != null) {
                this.address = device.getParentLineAddress() + "." +
                        device.getLineNo() + "." +
                        device.getLineAddress();
            }
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public List<String> getStates() {
            return states;
        }

        public void setStates(List<String> states) {
            this.states = states;
        }

    }

    public static class RegionEventInfo {

        /** Идентификатор зоны (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";

        /** Название */
        private String name = "";

        /** Описание */
        private String description = "";

        /** Перечень активных состояний зоны */
        private List<String> states = new ArrayList<>();

        public RegionEventInfo() {
        }

        public RegionEventInfo(Region region, List<String> states) {
            this.idRef = region.getId();
            this.states = states;

            this.name = region.getName();
            this.description = region.getDescription();
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getStates() {
            return states;
        }

        public void setStates(List<String> states) {
            this.states = states;
        }

    }

    public static class UserEventInfo {

        /** Идентификатор пользователя (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";

        /** Имя пользователя */
        private String name = "";

        public UserEventInfo() {
        }

        public UserEventInfo(Event event) {
            this.idRef = event.getUserId();
            this.name = event.getUserName();
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class VirtualStateEventInfo {

        /** Идентификатор вирт. состояния (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";
        /** Название */
        private String name = "";
        /** Описание */
        private String description = "";
        /** Номер в проекте */
        private int globalNo = 0;
        /** Идентификатор прибора */
        private String deviceId = "";

        public VirtualStateEventInfo() {
        }

        public VirtualStateEventInfo(Event event, VirtualState virtualState) {
            this.idRef = virtualState.getId();
            this.name = virtualState.getName();
            this.description = virtualState.getDescription();
            this.globalNo = virtualState.getGlobalNo();
            this.deviceId = virtualState.getDeviceId();
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getGlobalNo() {
            return globalNo;
        }

        public void setGlobalNo(int globalNo) {
            this.globalNo = globalNo;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }
    }

    public static class ScenarioEventInfo {

        /** Идентификатор сценария (добавлено Ref из-за проблемы: https://github.com/querydsl/querydsl/issues/1525) */
        @Indexed
        private String idRef = "";
        /** Название */
        private String name = "";
        /** Описание */
        private String description = "";
        /** Номер в проекте */
        private int globalNo = 0;
        /** Перечень активных состояний сценария */
        private List<String> states = new ArrayList<>();

        public ScenarioEventInfo() {
        }

        public ScenarioEventInfo(Scenario scenario, List<String> states) {
            this.idRef = scenario.getId();
            this.name = scenario.getName();
            this.description = scenario.getDescription();
            this.globalNo = scenario.getGlobalNo();
            this.states = states;
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getGlobalNo() {
            return globalNo;
        }

        public void setGlobalNo(int globalNo) {
            this.globalNo = globalNo;
        }

        public List<String> getStates() {
            return states;
        }

        public void setStates(List<String> states) {
            this.states = states;
        }
    }

    public static class LineEventInfo {

        /** Номер линии */
        @Indexed
        private int lineNo = 0;

        public LineEventInfo() {
        }

        public LineEventInfo(Event event) {
            this.lineNo = event.getLineNo();
        }

        public int getLineNo() {
            return lineNo;
        }

        public void setLineNo(int lineNo) {
            this.lineNo = lineNo;
        }
    }

    public static class IndicatorEventInfo {

        private String name = "";

        private String description = "";

        private String indicatorPanelId = "";

        private int rowNo;

        private int colNo;

        public IndicatorEventInfo(){}

        public IndicatorEventInfo(Event event, Indicator indicator){
            this.indicatorPanelId = event.getIndicatorPanelId();
            this.rowNo = indicator.getRowNo();
            this.colNo = indicator.getColNo();
            this.name = indicator.getName();
            this.description = indicator.getDescription();
        }

        public String getIndicatorPanelId() {
            return indicatorPanelId;
        }

        public int getRowNo() {
            return rowNo;
        }

        public void setRowNo(int rowNo) {
            this.rowNo = rowNo;
        }

        public int getColNo() {
            return colNo;
        }

        public void setColNo(int colNo) {
            this.colNo = colNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class EmployeeEventInfo {
        /** Идентификатор сотрудника */
        @Indexed
        private String idRef = "";
        /** Фамилия и инициалы */
        private String name = "";
        /** Имя */
        private String firstName = "";
        /** Отчество */
        private String middleName = "";
        /** Фамилия */
        private String lastName = "";

        public EmployeeEventInfo(){}

        public EmployeeEventInfo(Employee employee){
            idRef = employee.getId();
            name = employee.getName();
            firstName = employee.getFirstName();
            middleName = employee.getMiddleName();
            lastName = employee.getLastName();
        }

        public String getIdRef() {
            return idRef;
        }

        public void setIdRef(String idRef) {
            this.idRef = idRef;
        }

        public String getName() {
            return name;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }

    @JsonIgnore
    @Indexed
    private String eventTypeId = "";

    /** Подсистема события */
    @Indexed
    private Subsystem subsystem = Subsystem.UNKNOWN;

    /** Журнал, из которого получено событие */
    @Indexed
    private String logTypeId = null;

    /** Время возникновения события */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @Indexed
    private Date occurred = null;

    /** Время получения события с прибора (системное время сервера) */
    @DateTimeFormat(iso = ISO.DATE_TIME)
    @Indexed
    private Date received = null;

        /* Отображение класса состояния */
    /** Идентификатор */
    @Indexed
    private String stateCategoryId = "";

    /** Цвет */
    private String color;

    /** Цвет фона */
    private String fontColor;

    /** Имя иконки */
    private String iconName;

        /* Отображение панели */
    private ControlDeviceEventInfo controlDeviceEventInfo;

        /* Отображение устройства */
    private DeviceEventInfo deviceEventInfo;

        /* Отображение зоны */
    private RegionEventInfo regionEventInfo;

        /* Пользователь */
    private UserEventInfo userEventInfo;

        /* Виртуальное состояние */
    private VirtualStateEventInfo virtualStateEventInfo;

        /* Сценарий */
    private ScenarioEventInfo scenarioEventInfo;

        /* Линия */
    private LineEventInfo lineEventInfo;

        /* Виртуальный индикатор */
    private IndicatorEventInfo indicatorEventInfo;

        /* Сотрудник */
    private EmployeeEventInfo employeeEventInfo;

        /* Метки фильтрации */
    private FilterTags filterTags = new FilterTags();

    public AggregatedEventView() {
    }

    public AggregatedEventView(Event event, EventTypeView eventTypeView, StateCategoryView stateCategoryView) {
        setId(event.getId());
        setEventTypeId(eventTypeView.getId());
        setName(eventTypeView.getName());
        setDescription(eventTypeView.getDescription());

        setSubsystem(event.getSubsystem());
        setLogTypeId(event.getLogTypeId());
        setOccurred(event.getOccurred());
        setReceived(event.getReceived());

        setStateCategoryId(stateCategoryView.getId());
        setColor(stateCategoryView.getColor());
        setFontColor(stateCategoryView.getFontColor());
        setIconName(stateCategoryView.getIconName());
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public String getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(String logTypeId) {
        this.logTypeId = logTypeId;
    }

    public Date getOccurred() {
        return occurred;
    }

    public void setOccurred(Date occurred) {
        this.occurred = occurred;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public String getStateCategoryId() {
        return stateCategoryId;
    }

    public void setStateCategoryId(String stateCategoryId) {
        this.stateCategoryId = stateCategoryId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public ControlDeviceEventInfo getControlDeviceEventInfo() {
        return controlDeviceEventInfo;
    }

    public void setControlDeviceEventInfo(ControlDeviceEventInfo controlDeviceEventInfo) {
        this.controlDeviceEventInfo = controlDeviceEventInfo;
    }

    public DeviceEventInfo getDeviceEventInfo() {
        return deviceEventInfo;
    }

    public void setDeviceEventInfo(DeviceEventInfo deviceEventInfo) {
        this.deviceEventInfo = deviceEventInfo;
    }

    public RegionEventInfo getRegionEventInfo() {
        return regionEventInfo;
    }

    public void setRegionEventInfo(RegionEventInfo regionEventInfo) {
        this.regionEventInfo = regionEventInfo;
    }

    public UserEventInfo getUserEventInfo() {
        return userEventInfo;
    }

    public void setUserEventInfo(UserEventInfo userEventInfo) {
        this.userEventInfo = userEventInfo;
    }

    public VirtualStateEventInfo getVirtualStateEventInfo() {
        return virtualStateEventInfo;
    }

    public void setVirtualStateEventInfo(VirtualStateEventInfo virtualStateEventInfo) {
        this.virtualStateEventInfo = virtualStateEventInfo;
    }

    public ScenarioEventInfo getScenarioEventInfo() {
        return scenarioEventInfo;
    }

    public void setScenarioEventInfo(ScenarioEventInfo scenarioEventInfo) {
        this.scenarioEventInfo = scenarioEventInfo;
    }

    public LineEventInfo getLineEventInfo() {
        return lineEventInfo;
    }

    public void setLineEventInfo(LineEventInfo lineEventInfo) {
        this.lineEventInfo = lineEventInfo;
    }

    public IndicatorEventInfo getIndicatorEventInfo() {
        return indicatorEventInfo;
    }

    public void setIndicatorEventInfo(IndicatorEventInfo indicatorEventInfo) {
        this.indicatorEventInfo = indicatorEventInfo;
    }

    public FilterTags getFilterTags() {
        return filterTags;
    }

    public void setFilterTags(FilterTags filterTags) {
        this.filterTags = filterTags;
    }

}
