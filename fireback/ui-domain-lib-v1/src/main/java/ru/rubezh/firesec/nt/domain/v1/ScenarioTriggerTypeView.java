package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Отображение типа триггера сценария.
 * 
 * @author Антон Васильев
 * @ingroup UIReferences
 */
@Document(collection="scenario_trigger_type_views")
@TypeAlias(value="ScenarioTriggerTypeView")
@JsonTypeName("ScenarioTriggerTypeView")
public class ScenarioTriggerTypeView extends ReferenceEntityView {

}
