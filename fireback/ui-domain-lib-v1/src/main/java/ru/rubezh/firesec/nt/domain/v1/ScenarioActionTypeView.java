package ru.rubezh.firesec.nt.domain.v1;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Отображение типа действия, выполняемого сценарием.
 * 
 * @author Антон Васильев
 * @ingroup UIReferences
 */
@Document(collection="scenario_action_type_views")
@TypeAlias(value="ScenarioActionTypeView")
@JsonTypeName("ScenarioActionTypeView")
public class ScenarioActionTypeView extends ReferenceEntityView {
    /** Иконка */
    @DBRef
    private Media iconMedia = new Media();

    public Media getIconMedia() {
        return iconMedia;
    }
    
    public void setIconMedia(Media iconMedia) {
        this.iconMedia = iconMedia;
    }
    
}
