package ru.rubezh.firesec.nt.domain.v1;

import java.util.Map;

/**
 * Отображение конфигурируемого параметра.
 * 
 * @author Artem Sedanov (artem.sedanov@satellite-soft.ru)
 * @ingroup UIReferences
 */
public class DeviceConfigPropertyView extends AdjectiveEntityView {
    /** Имена возможных значений параметра */
    private Map<String, String> possibleValueNames;

    /** Значение по умолчанию */
    private String defaultValue;

    public Map<String, String> getPossibleValueNames() {
        return possibleValueNames;
    }

    public void setPossibleValueNames(Map<String, String> possibleValueNames) {
        this.possibleValueNames = possibleValueNames;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
