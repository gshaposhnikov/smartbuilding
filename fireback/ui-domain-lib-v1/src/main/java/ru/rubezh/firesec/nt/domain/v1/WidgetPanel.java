package ru.rubezh.firesec.nt.domain.v1;

/**
 * Виджет "панель".
 * 
 * Представляет собой контейнер для других виджетов (задаёт направление расположения элементов внутри).
 * 
 * @author Александр Горячкин
 */
public class WidgetPanel extends Widget {

    /** расположение внутренних виджетов */
    private WidgetLayout layout;

    public WidgetLayout getLayout() {
        return layout;
    }

    public void setLayout(WidgetLayout layout) {
        this.layout = layout;
    }
}
