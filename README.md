Инструкция по разворачиванию проекта FireSec
============================================


Подготовка Ubuntu 16.04 LTS x64
-------------------------------

### Установить Java 8 от Oracle

Добавить репозиторий сторонних скриптов установки java

    sudo add-apt-repository ppa:webupd8team/java
    sudo apt update

*Опционально*: автоматическое принятие лицензии Oracle

    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

Установка Java 8

    sudo apt install -y oracle-java8-installer

Автоматическая установка переменных среды

    sudo apt install -y oracle-java8-set-default

### Установить и настроить MongoDB 3.4

Подключить репозиторий и установить MongoDB

    echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    sudo apt update
    sudo apt install -y mongodb-org

*Опционально*: для подключения к MongoDB с удаленного сервера указать в файле `/etc/mongod.conf` в параметре `net.bindIp` прослушиваемый интерфейс (например: `0.0.0.0` - все интерфейсы)

Настроить автозапуск и запустить MongoDB

    sudo systemctl enable mongod.service
    sudo systemctl start mongod.service

### Установить Tomcat 8.5

Создать пользователя и группу

    sudo groupadd tomcat
    sudo mkdir /opt/tomcat
    sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

Скачать и установить дистрибутив (например версии `8.5.29`)

    TOMCAT_VERSION=8.5.29
    wget http://archive.apache.org/dist/tomcat/tomcat-8/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
    sudo tar xzf apache-tomcat-$TOMCAT_VERSION.tar.gz -C /opt/tomcat --strip-components=1
    rm apache-tomcat-$TOMCAT_VERSION.tar.gz

Перейти в каталог Tomcat

    pushd /opt/tomcat/

Скопировать путь до jre и установить в переменную `JAVA_HOME`:

    update-java-alternatives -l
    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

Настроить сервис, установив в переменную `JAVA_HOME` скопированный путь

    cat << EOF | sudo tee /etc/systemd/system/tomcat.service
    [Unit]
    Description=Apache Tomcat Web Application Container
    After=network.target

    [Service]
    WorkingDirectory=/opt/tomcat

    Type=forking

    Environment=JAVA_HOME=/usr/lib/jvm/java-8-oracle
    Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
    Environment=CATALINA_HOME=/opt/tomcat
    Environment=CATALINA_BASE=/opt/tomcat
    Environment=UMASK=0022
    Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
    Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

    ExecStart=/opt/tomcat/bin/startup.sh
    ExecStop=/opt/tomcat/bin/shutdown.sh

    User=tomcat
    Group=tomcat
    RestartSec=10
    Restart=always

    [Install]
    WantedBy=multi-user.target
    EOF

Добавить пользователей для админки (внутрь блока `tomcat-users`)

    sudo nano /opt/tomcat/conf/tomcat-users.xml

Добавить пользователя (например `admin:password`)

    <user username="admin" password="password" roles="manager-gui,admin-gui"/>

*Опционально*: разрешить доступ в админку с любого IP

    sudo sed -i 's/allow="[^"]\+"/allow="^.*$"/' webapps/manager/META-INF/context.xml

*Опционально*: разрешить доступ в админку с конкретных IP

    sudo sed -i 's/allow="[^"]\+"/allow="127.0.0.1|10.0.0.67"/' webapps/manager/META-INF/context.xml

Настроить права доступа на каталоги Tomcat - tomcat для работы и остальным пользователям для обновления

    sudo chown -R tomcat:tomcat .

    sudo chmod o+rx logs
    sudo chmod -R o+rwx webapps

Сгенерировать сертификат для работы по https (необходимо будет ввести данные о компании, местоположении и др.)

    sudo $JAVA_HOME/bin/keytool -genkey -alias tomcat -keyalg RSA -keystore .keystore

*Опционально:* Импортировать готовый сертификат

    openssl pkcs12 -export -in <mycert>.crt -inkey <mykey>.key \
      -out <mycert>.p12 -name tomcat -CAfile <myCA>.crt -caname root -chain

Включить и настроить работу https в tomcat - открыть файл:

    sudo nano conf/server.xml

Добавить в файл следующие строки:

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

*Опционально:* Увеличить таймаут асинхронных запросов (требуется для работы c проектами Firesec, содержащими более 10 сценариев)

Добавить в connector-ы по протам 8443 и 8080 параметр asyncTimeout="300000":

    <Connector port="8080" protocol="HTTP/1.1" asyncTimeout="300000"
               connectionTimeout="20000"
               redirectPort="8443" />

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" asyncTimeout="300000"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

Настроить время сессии - открыть файл:

    sudo nano conf/web.xml

Добавить в файл следующие строки:

    <session-config>
        <session-timeout>-1</session-timeout>
    </session-config>

Настроить автозапуск и запустить Tomcat

    sudo systemctl enable tomcat.service
    sudo systemctl start tomcat.service

Выйти из каталога Tomcat

    popd

### Установить сервер очередей RabbitMQ

Установить сервер очередей и включить плагин WEB-management

    sudo apt install -y rabbitmq-server
    sudo rabbitmq-plugins enable rabbitmq_management

Добавить пользователя с правами администратора (например `admin:password`) и дать право на удаленное подключение к корневому виртуальному хосту

    sudo rabbitmqctl add_user admin password
    sudo rabbitmqctl set_user_tags admin administrator
    sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

Прописать ограничения на размер очередей и эксчейнджей - 1Gb для каждой очереди или эксчейнджа (можно регулировать в зависимости от дискового пространства сервера)

    sudo rabbitmqctl set_policy messaging_limits '^ru\.rubezh\.firesec\.nt\..*' '{"max-length-bytes":1073741824}'

### Установить драйвер Hasp

Скачать архив с bitbucket.org

    wget --user="ВАША УЧЁТНАЯ ЗАПИСЬ НА BITBUCKET" --password "ВАШ ПАРОЛЬ ОТ BITBUCKET" \
            https://bitbucket.org/satellite-soft/firesec/downloads/Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz

*Либо:* Скачать пакет с gemalto.com (в браузере)

    https://sentinelcustomer.gemalto.com/DownloadNotice.aspx?dID=8589945165

Распаковать архив и установить пакет

    tar xzf Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz
    rm Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz
    sudo dpkg -i Sentinel_LDK_Ubuntu_DEB_Run-time_Installer/aksusbd_7.81-1_amd64.deb

### Подготовка к запуску проекта (однократно)

Установить проприетарную библиотеку для работы с Hasp

    wget --user="ВАША УЧЁТНАЯ ЗАПИСЬ НА BITBUCKET" --password "ВАШ ПАРОЛЬ ОТ BITBUCKET" \
            https://bitbucket.org/satellite-soft/firesec/downloads/libHASPJava_x86_64.so
    sudo mv libHASPJava_x86_64.so /usr/lib

Добавить права для доступа к USB устройствам

    cat << EOF | sudo tee /etc/udev/rules.d/99-myhid.rules
    # RUBEZH HID
    # idVendor 0xc251 Keil Software, Inc.
    # idProduct 0x1303
    ATTRS{idProduct}=="1303", ATTRS{idVendor}=="c251", MODE="0660", GROUP="plugdev"
    EOF

Создать пользователя и группу для запуска приложений

    sudo groupadd firesec
    sudo mkdir -p /opt/firesecNT/drivers
    sudo useradd -s /bin/false -g firesec -G plugdev -d /opt/firesecNT firesec
    sudo chown -R firesec:firesec /opt/firesecNT

Прописать права всем локальным пользователям для развертывания приложений

    sudo chmod o+rwx -R /opt/firesecNT

Добавить конфигурационные файлы для автозапуска приложений

    cat << EOF | sudo tee /etc/systemd/system/firesecnt-bl.service
    [Unit]
    Description=FiresecNT business logic server
    After=network.target

    [Service]
    WorkingDirectory=/opt/firesecNT

    Environment=JAVA_HOME=/usr/lib/jvm/java-8-oracle
    # Environment=RABBITMQ_HOST=localhost
    # Environment=RABBITMQ_PORT=5672
    # Environment=RABBITMQ_API=http://localhost:15672/api/
    Environment=RABBITMQ_USERNAME=admin
    Environment=RABBITMQ_PASSWORD=password
    # Environment=SPRING_DATA_MONGODB_HOST=localhost
    # Environment=SPRING_DATA_MONGODB_PORT=27017
    # Environment=SPRING_DATA_MONGODB_DATABASE=firesec

    ExecStart=/usr/bin/java -jar /opt/firesecNT/business-logic-server.jar
    ExecStop=/bin/kill -TERM $MAINPID

    User=firesec
    Group=firesec
    RestartSec=10
    Restart=always

    [Install]
    WantedBy=multi-user.target
    EOF

    cat << EOF | sudo tee /etc/systemd/system/firesecnt-cs.service
    [Unit]
    Description=FiresecNT communication server
    After=network.target

    [Service]
    WorkingDirectory=/opt/firesecNT

    Environment=JAVA_HOME=/usr/lib/jvm/java-8-oracle
    # Environment=RABBITMQ_HOST=localhost
    # Environment=RABBITMQ_PORT=5672
    # Environment=RABBITMQ_API=http://localhost:15672/api/
    Environment=RABBITMQ_USERNAME=admin
    Environment=RABBITMQ_PASSWORD=password
    # Environment=SPRING_DATA_MONGODB_HOST=localhost
    # Environment=SPRING_DATA_MONGODB_PORT=27017
    # Environment=SPRING_DATA_MONGODB_DATABASE=firesec

    ExecStart=/usr/bin/java -jar /opt/firesecNT/communication-server.jar -d /opt/firesecNT/drivers
    ExecStop=/bin/kill -TERM $MAINPID

    User=firesec
    Group=firesec
    RestartSec=10
    Restart=always

    [Install]
    WantedBy=multi-user.target
    EOF

Настроить автозапуск приложений

    sudo systemctl enable firesecnt-bl.service
    sudo systemctl enable firesecnt-cs.service


Подготовка Ubuntu 14.04 LTS x64
-------------------------------

### Установить Java 8 от Oracle

Добавить репозиторий сторонних скриптов установки java

    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update

*Опционально*: автоматическое принятие лицензии Oracle

    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

Установка Java 8

    sudo apt-get install -y oracle-java8-installer

Автоматическая установка переменных среды

    sudo apt-get install -y oracle-java8-set-default

### Установить MongoDB 3.4

    echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    sudo apt-get update
    sudo apt-get install -y mongodb-org

*Опционально*: для подключения к MongoDB с удаленного сервера указать в файле `/etc/mongod.conf` в параметре `net.bindIp` прослушиваемый интерфейс (например: `0.0.0.0` - все интерфейсы) и перезапустить сервис

    sudo initctl restart mongod

### Установить Tomcat 8.5.24

Создать пользователя и группу

    sudo groupadd tomcat
    sudo mkdir /opt/tomcat
    sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

Скачать и установить дистрибутив (например версии `8.5.29`)

    TOMCAT_VERSION=8.5.29
    wget http://archive.apache.org/dist/tomcat/tomcat-8/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz
    sudo tar xzf apache-tomcat-$TOMCAT_VERSION.tar.gz -C /opt/tomcat --strip-components=1
    rm apache-tomcat-$TOMCAT_VERSION.tar.gz

Перейти в каталог Tomcat

    pushd /opt/tomcat/

Скопировать путь до jre и установить в переменную `JAVA_HOME`:

    update-java-alternatives -l
    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

Настроить сервис, установив в переменную `JAVA_HOME` скопированный путь

    cat << EOF | sudo tee /etc/init/tomcat.conf
    description "Tomcat Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    setuid tomcat
    setgid tomcat

    env JAVA_HOME=/usr/lib/jvm/java-8-oracle
    env CATALINA_PID=/opt/tomcat/temp/tomcat.pid
    env CATALINA_HOME=/opt/tomcat
    env CATALINA_BASE=/opt/tomcat
    env UMASK=0022

    env CATALINA_OPTS="-Xms512M -Xmx1024M -server -XX:+UseParallelGC"
    env JAVA_OPTS="-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"

    script
        chdir $CATALINA_HOME
        exec $CATALINA_HOME/bin/catalina.sh run
    end script

    # cleanup temp directory after stop
    post-stop script
        rm -rf "\$CATALINA_HOME"/temp/*
    end script
    EOF

Добавить пользователей для админки (внутрь блока `tomcat-users`)

    sudo nano /opt/tomcat/conf/tomcat-users.xml

Добавить пользователя (например `admin:password`)

    <user username="admin" password="password" roles="manager-gui,admin-gui"/>

*Опционально*: разрешить доступ в админку с любого IP

    sudo sed -i 's/allow="[^"]\+"/allow="^.*$"/' webapps/manager/META-INF/context.xml

*Опционально*: разрешить доступ в админку с конкретных IP

    sudo sed -i 's/allow="[^"]\+"/allow="127.0.0.1|10.0.0.67"/' webapps/manager/META-INF/context.xml

Настроить права доступа на каталоги Tomcat - tomcat для работы и остальным пользователям для обновления

    sudo chown -R tomcat:tomcat .

    sudo chmod o+rx logs
    sudo chmod -R o+rwx webapps

Сгенерировать сертификат для работы по https (необходимо будет ввести данные о компании, местоположении и др.)

    sudo $JAVA_HOME/bin/keytool -genkey -alias tomcat -keyalg RSA -keystore .keystore

*Опционально:* Импортировать готовый сертификат

    openssl pkcs12 -export -in <mycert>.crt -inkey <mykey>.key \
      -out <mycert>.p12 -name tomcat -CAfile <myCA>.crt -caname root -chain

Включить и настроить работу https в tomcat - открыть файл:

    sudo nano conf/server.xml

Добавить в файл следующие строки:

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

*Опционально:* Увеличить таймаут асинхронных запросов (требуется для работы c проектами Firesec, содержащими более 10 сценариев)

Добавить в connector-ы по протам 8443 и 8080 параметр asyncTimeout="300000":

    <Connector port="8080" protocol="HTTP/1.1" asyncTimeout="300000"
               connectionTimeout="20000"
               redirectPort="8443" />

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" asyncTimeout="300000"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

Настроить время сессии - открыть файл:

    sudo nano conf/web.xml

Добавить в файл следующие строки:

    <session-config>
        <session-timeout>-1</session-timeout>
    </session-config>

Настроить автозапуск и запустить Tomcat

    sudo initctl reload-configuration
    sudo initctl start tomcat

Выйти из каталога Tomcat

    popd

### Установить сервер очередей RabbitMQ

Установить сервер очередей и включить плагин WEB-management

    wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
    sudo dpkg -i erlang-solutions_1.0_all.deb
    rm erlang-solutions_1.0_all.deb

    echo "deb https://dl.bintray.com/rabbitmq/debian trusty main" | sudo tee /etc/apt/sources.list.d/bintray.rabbitmq.list
    wget -O- https://dl.bintray.com/rabbitmq/Keys/rabbitmq-release-signing-key.asc | sudo apt-key add -
    wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install -y rabbitmq-server
    sudo rabbitmq-plugins enable rabbitmq_management

Добавить пользователя с правами администратора (например `admin:password`) и дать право на удаленное подключение к корневому виртуальному хосту

    sudo rabbitmqctl add_user admin password
    sudo rabbitmqctl set_user_tags admin administrator
    sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"

Прописать ограничения на размер очередей и эксчейнджей - 1Gb для каждой очереди или эксчейнджа (можно регулировать в зависимости от дискового пространства сервера)

    sudo rabbitmqctl set_policy messaging_limits '^ru\.rubezh\.firesec\.nt\..*' '{"max-length-bytes":1073741824}'

### Подготовка к запуску проекта (однократно)

Добавить права для доступа к USB устройствам

    cat << EOF | sudo tee /etc/udev/rules.d/99-myhid.rules
    # RUBEZH HID
    # idVendor 0xc251 Keil Software, Inc.
    # idProduct 0x1303
    ATTRS{idProduct}=="1303", ATTRS{idVendor}=="c251", MODE="0660", GROUP="plugdev"
    EOF

Создать пользователя и группу для запуска приложений

    sudo groupadd firesec
    sudo mkdir -p /opt/firesecNT/drivers
    sudo useradd -s /bin/false -g firesec -G plugdev -d /opt/firesecNT firesec
    sudo chown -R firesec:firesec /opt/firesecNT

Прописать права всем локальным пользователям для развертывания приложений

    sudo chmod o+rwx -R /opt/firesecNT

Добавить конфигурационные файлы для автозапуска приложений

    cat << EOF | sudo tee /etc/init/firesecnt-bl.conf
    description "FiresecNT business logic Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    setuid firesec
    setgid firesec

    env JAVA_HOME=/usr/lib/jvm/java-8-oracle
    # env RABBITMQ_HOST=localhost
    # env RABBITMQ_PORT=5672
    # env RABBITMQ_API=http://localhost:15672/api/
    env RABBITMQ_USERNAME=admin
    env RABBITMQ_PASSWORD=password
    # env SPRING_DATA_MONGODB_HOST=localhost
    # env SPRING_DATA_MONGODB_PORT=27017
    # env SPRING_DATA_MONGODB_DATABASE=firesec

    chdir /opt/firesecNT

    exec java -jar /opt/firesecNT/business-logic-server.jar
    EOF

    cat << EOF | sudo tee  /etc/init/firesecnt-cs.conf
    description "FiresecNT communication Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    setuid firesec
    setgid firesec

    env JAVA_HOME=/usr/lib/jvm/java-8-oracle
    # env RABBITMQ_HOST=localhost
    # env RABBITMQ_PORT=5672
    # env RABBITMQ_API=http://localhost:15672/api/
    env RABBITMQ_USERNAME=admin
    env RABBITMQ_PASSWORD=password
    # env SPRING_DATA_MONGODB_HOST=localhost
    # env SPRING_DATA_MONGODB_PORT=27017
    # env SPRING_DATA_MONGODB_DATABASE=firesec

    chdir /opt/firesecNT

    exec java -jar /opt/firesecNT/communication-server.jar -d /opt/firesecNT/drivers
    EOF

Настроить автозапуск приложений

    sudo initctl reload-configuration


Подготовка Windows 10 x64
-------------------------

### Установить GIT

Скачать и установить git for windows - ссылка на [оф. сайт](https://git-for-windows.github.io)

Дальнейшие консольные команды выполнять в Git Bash из под администратора.

### Установить jdk

Скачать и установить версию 8 - ссылка на [оф. сайт](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

Установить переменные окружения, заменив <jdk_dir> путём до java jdk (например, C:/Program Files/Java/jdk1.8.0_131):

    setx JAVA_HOME <jdk_dir> -m

Для задействования нового окружения нужно перезапустить консоль, или создать локальную переменную:

    JAVA_HOME=<jdk_dir>

### Установить менеджер служб

Скачать - ссылка на [оф. сайт](http://mirror.linux-ia64.org/apache//commons/daemon/binaries/windows/)

Распаковать в корень диска `С:\`

### Установить MongoDB 3.4

Скачать и установить - ссылка на [оф. сайт](https://www.mongodb.com/download-center#community)

Во время установки выбрать тип установки Complete.

Создать директорию для базы данных:

     mkdir -p C:/data/db/

Зарегистрировать сервис (по умолчанию mongo_dir="C:\Program Files\MongoDB"):

    <mongodb_dir>\Server\3.4\bin\mongod --logpath "C:\data\mongo-log.txt" --dbpath "C:\data\db" --install

Запустить:

    net start MongoDB

### Установить Tomcat 8.5.29

Скачать и установить - ссылка на [оф. сайт](http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.29/bin/)

Сгенерировать сертификат для работы по https (необходимо будет ввести пароль, данные о компании, местоположении и др.)

    $JAVA_HOME\bin\keytool -genkey -alias tomcat -keyalg RSA -keystore <tomcat_dir>/.keystore

*Опционально:* Импортировать готовый сертификат

    openssl pkcs12 -export -in <mycert>.crt -inkey <mykey>.key \
      -out <mycert>.p12 -name tomcat -CAfile <myCA>.crt -caname root -chain

Включить и настроить работу https в tomcat - открыть файл <tomcat_dir>/conf/server.xml,
в блоке <Server ...> <Service name="Catalina"> добавить следующий код,
подставив <tomcat_dir> и установленный ранее пароль в "keystorePass":

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="<tomcat_dir>\.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

*Опционально:* Увеличить таймаут асинхронных запросов (требуется для работы c проектами Firesec, содержащими более 10 сценариев)

Добавить в connector-ы по протам 8443 и 8080 параметр asyncTimeout="300000":

    <Connector port="8080" protocol="HTTP/1.1" asyncTimeout="300000"
               connectionTimeout="20000"
               redirectPort="8443" />

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" asyncTimeout="300000"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

Настроить время сессии - открыть файл <tomcat_dir>/conf/web.xml,
в середине файла найти session-config и добавить следующие строки:

    <session-config>
        <session-timeout>-1</session-timeout>
    </session-config>

Перезапустить сервер - в трее в значке "Apache Tomcat" в контекстном меню выбрать "Start service".
Если такого нет, то запустить и в окне нажать кнопку "Start":

    <tomcat_dir>\bin\Tomcat8w.exe

Установить автозапуск:

    C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //US//Tomcat8 --Startup=auto

### Установить сервер очередей

Скачать и установить erlang - ссылка на [оф. сайт](http://www.erlang.org/downloads)

Скачать и установить rabbitMQ 3.6.10 - ссылка на [оф. сайте](https://www.rabbitmq.com/download.html)

Включить плагин WEB-management (по умолчанию rabbitmq_dir="C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.10")

    <rabbitmq_dir>\sbin\rabbitmq-plugins.bat enable rabbitmq_management

Прописать ограничения на размер очередей и эксчейнджей  - 1Gb для каждой очереди или эксчейнджа (можно регулировать в зависимости от дискового пространства сервера)

    <rabbitmq_dir>\sbin\rabbitmqctl.bat set_policy messaging_limits "^ru\.rubezh\.firesec\.nt\..*" "{""max-length-bytes"":1073741824}"


Через веб-интерфейс по адресу 'http://localhost:15672' (пользователь по умолчанию guest с паролем guest)
добавить пользователя admin с правами администратора.

### Установить драйвер Hasp

Скачать архив с bitbucket.org

    https://bitbucket.org/satellite-soft/firesec/downloads/Sentinel_LDK_Run-time_setup.zip

*Либо:* Скачать пакет с gemalto.com

    https://sentinelcustomer.gemalto.com/DownloadNotice.aspx?dID=8589947119

Распаковать архив и установить

    Распаковать 'Sentinel_LDK_Run-time_setup.zip', запустить 'HASPUserSetup.exe' и следовать указаниям установочного приложения. Если в процессе установки возникнет ошибка - повторить установку драйвера.

### Подготовка к запуску проекта (однократно)

Установить проприетарные библиотеки Hasp

    Скачать библиотеки с bitbucket.org:
        https://bitbucket.org/satellite-soft/firesec/downloads/HASPJava_x64.dll
        https://bitbucket.org/satellite-soft/firesec/downloads/hasp_windows_x64_83702.dll

    Скопировать 'HASPJava_x64.dll' и 'hasp_windows_x64_83702.dll' в 'C:\Windows\System32\'

Создать папки для проекта

    mkdir -p C:\firesecNT\drivers

Зарегистрировать сервисы

    cd C:\commons-daemon-1.1.0-bin-windows\amd64

    ./prunsrv.exe //IS//firesecnt-bl --Jvm=auto --StartMode=java
        --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ"
        --Classpath="C:\firesecNT\business-logic-server.jar"
        --StartClass="ru.rubezh.firesec.nt.BusinessLogicApplication" --StartMethod=main
        --LogPath="C:\firesecNT\logs" --LogPrefix="bl-log"
        --StdOutput="C:\firesecNT\logs\bl-stdout-log.txt"
        --StdError="C:\firesecNT\logs\bl-stderr-log.txt"

    ./prunsrv.exe //IS//firesecnt-cs --Jvm=auto --StartMode=java
        --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ"
        --Classpath="C:\firesecNT\communication-server.jar" --StartParams="-d 'C:\firesecNT\drivers'"
        --StartClass="ru.rubezh.firesec.nt.cs.CommunicationServer" --StartMethod=main
        --LogPath="C:\firesecNT\logs" --LogPrefix="cs-log"
        --StdOutput="C:\firesecNT\logs\cs-stdout-log.txt"
        --StdError="C:\firesecNT\logs\cs-stderr-log.txt"

При необходимости можно будет поменять исполняемый файл командой:

    ./prunsrv.exe //US//<service_name> --Classpath="<path_to_jar>"

Также необходимо увеличить время на автозапуск сервисов в системе -
добавить ключ через редактор реестра (regedit)
или создать и запустить файл *.reg со следующим содержимым:

    Windows Registry Editor Version 5.00

    [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control]
    "ServicePipeTimeout"=dword:0000ea60

Для полной остановки сервисов нужно подготовить .bat файл:

    cd C:\commons-daemon-1.1.0-bin-windows\amd64
    ./prunsrv.exe //SS//firesecnt-bl
    ./prunsrv.exe //SS//firesecnt-cs
    for /F "tokens=1,3 delims= " %%a in ('handle64 -nobanner driver-2OP3.jar') do taskkill /f /pid %%b

Также нужно скачать [handle64](https://download.sysinternals.com/files/Handle.zip),
распаковать и скопировать handle64.exe в "C:\windows\system32"


Компиляция проекта
------------------

### Компиляция backend модулей

Перед сборкой необходимо проверить правильность настроек модулей
(файлы конфигураций расположены в `<module_name>/src/main/resources/*.properties`).

Инструкция по сборке описана в [fireback/README.md](fireback/README.md)

### Сборка клиента

Инструкция по сборке описана в [firefront/README.md](firefront/README.md)


Разворачивание/обновление системы
---------------------------------

### Разворачивание скомпилированного frontend сервера

#### Ubuntu

Установить путь до директории с Tomcat

    tomcat_dir="/opt/tomcat"

Скопировать скомпилированную версию фронтенд-сервера в Apache Tomcat (или задеплоить через веб-интерфейс):

    cp fireback/frontend-server/target/frontend-server.war "$tomcat_dir"/webapps/ROOT.war

Скопировать папку ресурсов и представление скомпилированного клиента в frontend сервер:

    cp -r firefront/build/public "$tomcat_dir"/webapps/ROOT/
    cp firefront/build/index.html "$tomcat_dir"/webapps/ROOT/WEB-INF/views/

Установить путь до каталога приложения

    firesecNT_dir="/opt/firesecNT"

Cкопировать собранные модули и зависимости

    cp fireback/business-logic/target/business-logic-server.jar "$firesecNT_dir"
    cp fireback/communication-server/target/communication-server.jar "$firesecNT_dir"
    cp fireback/driver-2OP3/target/driver-2OP3.jar "$firesecNT_dir"/drivers

`Ubuntu 16.04`: Перезапустить запущенные приложения

    sudo systemctl restart firesecnt-bl.service
    sudo systemctl restart firesecnt-cs.service

`Ubuntu 14.04`: Перезапустить запущенные приложения

    sudo initctl restart firesecnt-bl
    sudo initctl restart firesecnt-cs


#### Windows

Установить путь до директории с Tomcat

    tomcat_dir="<tomcat_dir>"

Скопировать скомпилированную версию фронтенд-сервера в Apache Tomcat (или задеплоить через веб-интерфейс):

    cp fireback/frontend-server/target/frontend-server.war "$tomcat_dir"/webapps/ROOT.war

Скопировать папку ресурсов и представление скомпилированного клиента в frontend сервер:

    cp -r firefront/build/public "$tomcat_dir"/webapps/ROOT/
    cp firefront/build/index.html "$tomcat_dir"/webapps/ROOT/WEB-INF/views/

Установить путь до каталога приложения

    firesecNT_dir="C:\firesecNT"

Cкопировать собранные модули и зависимости

    cp fireback/business-logic/target/business-logic-server.jar "$firesecNT_dir"
    cp fireback/communication-server/target/communication-server.jar "$firesecNT_dir"
    cp fireback/driver-2OP3/target/driver-2OP3.jar "$firesecNT_dir"/drivers

Перезапустить запущенные приложения

    cd C:\commons-daemon-1.1.0-bin-windows\amd64

    ./prunsrv.exe //SS//firesecnt-bl
    ./prunsrv.exe //SS//firesecnt-cs

    ./prunsrv.exe //ES//firesecnt-bl
    ./prunsrv.exe //ES//firesecnt-cs


Чтение системного журнала
-------------------------

Журналы взаимодействия с клиентом:

    "$tomcat_dir"/logs/catalina*.log
    "$tomcat_dir"/logs/localhost_access_log*.txt

(Для Windows) Журналы приложения:

    C:\firesecNT\logs\cs-stdout*.txt
    C:\firesecNT\logs\bl-stdout*.txt

(Для Ubuntu) Журналы приложения:

    /opt/firesecNT/logs/cs.log
    /opt/firesecNT/logs/firesec.log

Чтение в реальном времени:

    tail -f <path_to_log>

    Ctrl+C

Просмотр старых логов:

    cat <path_to_log> | grep <фильтр> | less


Пример изменения конфигурации и применение в проекте
----------------------------------------------------

### Изменение настроек драйвера

Для изменения конфигурации драйвера нужно внести необходимые изменения
в файлы `driver-2OP3/src/main/resources/driverProfile.xml`
и/или `driver-2OP3/src/main/resources/driverProfileRussianView.xml`.

После чего необходимо пересобрать jar-пакет драйвера:

    mvn package -pl driver-2OP3 -am

И перезапустить коммуникационный сервер.

### Пример изменения конфигурации драйвера

  * Изменение максимального количества устройств на линии
    1. В файле `driverProfile.xml` изменить параметр `lineDeviceCount` в `DeviceProfile`.
    2. Пересобрать и перезапустить КС
    3. В клиенте при подключении устройства можно увидеть, что диапазон допустимых адресов изменился

  * Изменение типа подсистемы состояния
    1. В файле `driverProfile.xml` изменить параметр `subsystem` в `DecodableState`.
    2. Пересобрать и перезапустить КС
    3. В клиенте все подключенные в системе устройства с данным состоянием изменят свою подсистему.

  * Изменение отображаемого названия профиля устройства
    1. В файле `driverProfileRussianView.xml` изменить параметр `name` в `DriverProfileView`.
    2. Пересобрать и перезапустить КС
    3. В клиенте все подключенные в системе устройства данного типа изменят своё название.

  * Изменение отображаемого названия состояния
    1. В файле `driverProfileRussianView.xml` изменить параметр `name` элемента `stateViews` объекта `DriverProfileView`.
    2. Пересобрать и перезапустить КС
    3. В клиенте все подключенные в системе устройства с данным состоянием изменят своё название.

