import logging


class FireDetector:
    def __init__(self, limit_temperature_value, limit_temperature_gradient, temperature_init_value=0):
        logging.debug('')
        self.temperatureEvent = False
        self.gradientTemperatureEvent = False
        self.temperatureChannelFailure = False
        self.temperatureValue = temperature_init_value
        self.temperatureGradient = 0
        self.limitTemperatureValue = limit_temperature_value
        self.limitTemperatureGradient = limit_temperature_gradient

    def get_info(self):
        logging.debug('')
        return {
            'temperatureEvent': self.temperatureEvent,
            'gradientTemperatureEvent': self.gradientTemperatureEvent,
            'temperatureChannelFailure': self.temperatureChannelFailure,
            'temperatureValue': self.temperatureValue
        }

    def set_temperature_value(self, value):
        self.temperatureGradient = value - self.temperatureValue
        self.temperatureValue = value
        logging.debug('self.temperatureGradient: {}, self.temperatureValue: {}'.
                      format(self.temperatureGradient, value))

        if self.temperatureValue < self.limitTemperatureValue:
            self.temperatureEvent = False
        else:
            self.temperatureEvent = True

        if self.temperatureGradient < self.limitTemperatureGradient:
            self.gradientTemperatureEvent = False
        else:
            self.gradientTemperatureEvent = True

    def set_values(self, data):
        logging.debug('data: {0}'.format(data))
        for k, v in data.items():
            if k == 'temperatureValue':
                self.temperatureValue = v

    def deserialize(self, data):
        self.temperatureEvent = data.get('temperatureEvent', False)
        self.gradientTemperatureEvent = data.get('gradientTemperatureEvent', False)
        self.temperatureChannelFailure = data.get('temperatureChannelFailure', False)
        self.temperatureValue = data.get('temperatureValue', 0)
