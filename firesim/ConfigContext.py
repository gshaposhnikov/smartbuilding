import yaml


class ConfigContext:
    LOG_LEVELS = ['fatal', 'error', 'warn', 'info', 'debug']

    def __init__(self):
        self.logLevelId = 3
        self.logFile = None
        self.scenarioFile = None
        self.stateFile = None
        self.port = 8081
        self.connectionTimeout = 300
        self.line1 = 1
        self.line2 = 1
        self.temperatureInitValue = 0
        self.temperatureWarningValue = 50
        self.temperatureFireValue = 70
        self.temperatureWarningGradient = 5
        self.temperatureFireGradient = 10
        self.dbName = 'data/firesim.db'
        self.dbMaxEventToCommit = 20

    def parse(self, config_file_name):
        with open(config_file_name) as stream:
            try:
                config = yaml.load(stream)
                data = config.get('logLevel')
                if isinstance(data, str) and data in self.LOG_LEVELS:
                    self.logLevelId = self.LOG_LEVELS.index(data)

                data = config.get('logFile')
                if isinstance(data, str) and len(data) > 0 and data != '-':
                    self.logFile = data

                data = config.get('scenarioFile')
                if isinstance(data, str) and len(data) > 0:
                    self.scenarioFile = data

                data = config.get('serverPort')
                if isinstance(data, int) and data >= 0:
                    self.port = data

                data = config.get('connectionTimeout')
                if isinstance(data, int) and data >= 0:
                    self.connectionTimeout = data

                data = config.get('line1DevicesCount')
                if isinstance(data, int) and data >= 0:
                    self.line1 = data

                data = config.get('line2DevicesCount')
                if isinstance(data, int) and data >= 0:
                    self.line2 = data

                data = config.get('temperatureWarningValue')
                if isinstance(data, int):
                    self.temperatureWarningValue = data

                data = config.get('temperatureFireValue')
                if isinstance(data, int):
                    self.temperatureFireValue = data

                data = config.get('temperatureWarningGradient')
                if isinstance(data, int):
                    self.temperatureWarningGradient = data

                data = config.get('temperatureFireGradient')
                if isinstance(data, int):
                    self.temperatureFireGradient = data

                data = config.get('temperatureInitValue')
                if isinstance(data, int):
                    self.temperatureInitValue = data

                data = config.get('stateFile')
                if isinstance(data, str) and len(data) > 0:
                    self.stateFile = data

                data = config.get('dbName')
                if isinstance(data, str) and len(data) > 0:
                    self.dbName = data

                data = config.get('dbMaxEventToCommit')
                if isinstance(data, int) and data > 0:
                    self.dbMaxEventToCommit = data

            except yaml.YAMLError:
                pass

    def get_log_level(self):
        return self.LOG_LEVELS[self.logLevelId].upper()
