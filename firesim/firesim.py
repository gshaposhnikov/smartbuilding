#!/usr/bin/env python3

import asyncio
import logging
import logging.handlers
import argparse
import os
import json

from Panel import Panel
from Scenario import Scenario
from TcpHandlers.Handler import Handler
from ConfigContext import ConfigContext


def serialize(_state_file, panel, scenario):
    logging.debug('stateFile: {0}'.format(_state_file))
    with open(_state_file, 'wt') as stream:
        state = {
            'panel': panel.serialize()
        }
        if scenario is not None:
            state['scenario'] = scenario.serialize()
        state = json.dumps(state, indent=2)
        stream.write(state)


# return: panel, scenarioEventId
def deserialize(_config_context):
    logging.debug('stateFile: {0}'.format(_config_context.stateFile))
    panel = None
    scenario = None
    if os.path.exists(_config_context.stateFile):
        with open(_config_context.stateFile) as stream:
            data = stream.read()
            if len(data) > 0:
                try:
                    parsed_data = json.loads(data)

                    panel_state = parsed_data.get('panel', {})
                    panel = Panel(_config_context)
                    panel.deserialize(_config_context, panel_state)

                    scenario_state = parsed_data.get('scenario', {})
                    if len(scenario_state) > 0:
                        scenario = Scenario(panel, 0)
                        scenario.deserialize(scenario_state)
                except Exception as e:
                    logging.error('Deserialize error: {0}'.format(e))
    return panel, scenario


def main(_config_context):
    panel = None
    scenario = None

    if _config_context.stateFile is not None:
        panel, scenario = deserialize(_config_context)

    if panel is None:
        logging.debug('Initialization panel')
        panel = Panel(_config_context,
                      [(1, _config_context.line1),
                       (2, _config_context.line2)])
        if scenario is not None:
            scenario.panel = panel

    if _config_context.scenarioFile is not None and \
            os.path.exists(_config_context.scenarioFile):
        if scenario is None:
            scenario = Scenario(panel, 0)
        scenario.parse(_config_context.scenarioFile)

    handler = Handler(panel, _config_context.connectionTimeout)

    loop = asyncio.get_event_loop()
    server_gen = asyncio.start_server(handler.handle_connection,
                                      port=_config_context.port)
    server = loop.run_until_complete(server_gen)
    logging.info('Listening established on {0}'
                 .format(server.sockets[0].getsockname()))

    if scenario is not None:
        asyncio.async(scenario.run())

    try:
        logging.debug('Run loop')
        loop.run_forever()
    except KeyboardInterrupt:
        logging.debug('Keyboard Interrupt')
    except Exception as ex:
        logging.debug('Exception: {}'.format(ex))
    finally:
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()
        if _config_context.stateFile is not None:
            serialize(_config_context.stateFile, panel, scenario)


if __name__ == '__main__':
    CONFIG_FILE_DEFAULT = 'config.yml'

    parser = argparse.ArgumentParser(description='Simulate devices panel.')
    parser.add_argument('--config',
                        default=CONFIG_FILE_DEFAULT,
                        help='set config file, default is "{0}"'.format(CONFIG_FILE_DEFAULT))
    args = parser.parse_args()

    configFile = CONFIG_FILE_DEFAULT
    if args.config is not None:
        configFile = args.config

    configContext = ConfigContext()
    configContext.parse(configFile)
    logLevel = configContext.get_log_level()

    loggerFormat = '%(asctime)s - %(levelname)s: <%(module)s::%(funcName)s> %(message)s'
    logging.basicConfig(format=loggerFormat,
                        datefmt='%b %d %H:%M:%S',
                        level=getattr(logging, logLevel, logging.INFO))
    loggerHandler = None
    if configContext.logFile is None:
        loggerHandler = logging.handlers.SysLogHandler(address='/dev/log')
        loggerHandler.setFormatter(logging.Formatter('%(levelname)s - %(message)s'
                                                     .format(os.path.basename(__file__))))
    else:
        loggerHandler = logging.FileHandler(configContext.logFile)
        loggerHandler.setFormatter(logging.Formatter(loggerFormat))
    logging.getLogger().addHandler(loggerHandler)

    logging.getLogger("asyncio").setLevel(logging.WARNING)

    main(configContext)
