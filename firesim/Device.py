import logging

from FireDetector import FireDetector
from LogCollection import LogCollection


class Device:
    STATUSES = ['normal', 'fail', 'lost']
    STATUS_OK = 0
    STATUS_FAIL = 1
    STATUS_LOST = 2

    FIRE_STATUSES = ['normal', 'warning', 'fire']
    FIRE_STATUS_OK = 0
    FIRE_STATUS_WARNING = 1
    FIRE_STATUS_FIRE = 2

    def __init__(self, config_context, address):
        logging.debug('no {0}'.format(address))
        self.address = address
        self.dataPresence = True
        self.deviceStatusId = self.STATUS_OK
        self.fireStatusId = self.FIRE_STATUS_OK
        self.limitTemperatureFireValue = config_context.temperatureFireValue
        self.limitTemperatureFireGradient = config_context.temperatureFireGradient
        self.fireDetector = FireDetector(config_context.temperatureWarningValue,
                                         config_context.temperatureWarningGradient,
                                         config_context.temperatureInitValue)
        self.smokeEvent = False

    def get_info(self, status_id=None):
        logging.debug('no {0}, statusId: {1}'
                      .format(self.address, status_id))
        if status_id is None:
            status_id = self.deviceStatusId
        return {
            'address': self.address,
            'deviceStatus': self.STATUSES[status_id],
            'fireStatus': self.FIRE_STATUSES[self.fireStatusId],
            'dataPresence': self.dataPresence,
            'fireDetector': self.fireDetector.get_info()
        }

    def set_device_status(self, status, line_no, log_collection: LogCollection):
        logging.debug('status: {0}'.format(status))
        if status in self.STATUSES:
            device_status_id = self.STATUSES.index(status)
            if self.deviceStatusId != device_status_id:

                if device_status_id == self.STATUS_LOST:
                    log_collection.add_event(line_no, self.address, status,
                                             True, device_type='fireDetector')
                elif device_status_id == self.STATUS_FAIL:
                    log_collection.add_event(line_no, self.address, status,
                                             'detected', False, False,
                                             device_type='fireDetector')

                if self.deviceStatusId == self.STATUS_LOST:
                    log_collection.add_event(line_no, self.address, 'lost',
                                             False, device_type='fireDetector')
                elif self.deviceStatusId == self.STATUS_FAIL:
                    log_collection.add_event(line_no, self.address, 'fail',
                                             'fixedAll', False, False,
                                             device_type='fireDetector')

                self.deviceStatusId = device_status_id

    def set_values(self, data, log_collection):
        line_no = data['lineNo']
        logging.debug('no {0}, line: {1}, data: {2}'
                      .format(self.address, line_no, data))

        for k, v in data.items():
            if k == 'dataPresence':
                self.dataPresence = v
            elif k == 'deviceStatus':
                self.set_device_status(v, line_no, log_collection)
            elif k == 'smokeEvent':
                self.smokeEvent = v
            elif k == 'temperatureValue':
                self.fireDetector.set_temperature_value(v)

        self.update_fire_status(line_no, log_collection)

    def update_fire_status(self, line_no, log_collection):
        fire_event = False

        if self.fireDetector.temperatureValue >= self.limitTemperatureFireValue or \
                self.fireDetector.temperatureGradient >= self.limitTemperatureFireGradient:
            self.fireStatusId = self.FIRE_STATUS_FIRE
            fire_event = True
        elif self.fireDetector.temperatureEvent or \
                self.fireDetector.gradientTemperatureEvent:
            self.fireStatusId = self.FIRE_STATUS_WARNING
            fire_event = True
        elif self.fireStatusId != self.FIRE_STATUS_OK:
            self.fireStatusId = self.FIRE_STATUS_OK
            fire_event = True

        if fire_event:
            log_collection.add_event(line_no, self.address, 'fire',
                                     self.FIRE_STATUSES[self.fireStatusId],
                                     self.fireDetector.temperatureValue,
                                     self.fireDetector.temperatureEvent,
                                     self.fireDetector.gradientTemperatureEvent,
                                     self.smokeEvent, device_type='fireDetector')

    def serialize(self):
        state = self.__dict__
        del state['limitTemperatureFireValue']
        del state['limitTemperatureFireGradient']

        fire_detector_state = self.fireDetector.__dict__
        del fire_detector_state['temperatureGradient']
        del fire_detector_state['limitTemperatureValue']
        del fire_detector_state['limitTemperatureGradient']

        state['fireDetector'] = fire_detector_state
        return state

    def deserialize(self, data):
        self.deviceStatusId = data.get('deviceStatusId', self.STATUS_OK)
        self.fireStatusId = data.get('fireStatusId', self.FIRE_STATUS_OK)
        self.dataPresence = data.get('dataPresence', True)
        self.smokeEvent = data.get('smokeEvent', False)
        self.fireDetector.deserialize(data.get('fireDetector', {}))
