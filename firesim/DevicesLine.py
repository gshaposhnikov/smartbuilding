import logging

from Device import Device
from LogEventContexts.PanelEventStatuses import PanelEventStatuses


class DevicesLine:
    def __init__(self, line_no=0, devices=None):
        if devices is None:
            devices = []

        logging.debug('lineNo: {0}'.format(line_no))
        self.lineNo = line_no
        self.devices = devices
        self.powerStatus = PanelEventStatuses.POWER_STATUS_OK
        self.outputStatus = PanelEventStatuses.OUTPUT_STATUS_OK

    def set_power_status(self, status, panel):
        logging.debug('status: {0}'.format(status))
        if status in PanelEventStatuses.POWER_STATUSES:
            status_id = PanelEventStatuses.POWER_STATUSES.index(status)

            if status != self.powerStatus:
                panel.logCollection.add_event('powerFail', self.lineNo, status,
                                              device_type='panel')

            self.powerStatus = status_id

    def set_output_status(self, status, panel):
        logging.debug('status: {0}'.format(status))
        if status in PanelEventStatuses.OUTPUT_STATUSES:
            status_id = PanelEventStatuses.OUTPUT_STATUSES.index(status)

            if status_id != self.outputStatus:
                panel.logCollection.add_event('outputFail', self.lineNo, status,
                                              device_type='panel')

            self.outputStatus = status_id

    def get_devices_info(self, devices):
        logging.debug('devices: {0}'.format(devices))
        response = []
        status_id = None

        if self.powerStatus != PanelEventStatuses.POWER_STATUS_OK or \
                self.outputStatus != PanelEventStatuses.OUTPUT_STATUS_OK:
            status_id = 2

        devices_ = [device for device in self.devices if device.address in devices]
        for device in devices_:
            response.append(device.get_info(status_id))
        return response

    def serialize(self):
        logging.debug('lineNo: {0}'.format(self.lineNo))
        state = self.__dict__
        state['devices'] = [device.serialize() for device in self.devices]
        return state

    def deserialize(self, config_context, data):
        logging.debug('data: {0}'.format(data))
        self.lineNo = data.get('lineNo', 0)
        self.powerStatus = data.get('powerStatus', PanelEventStatuses.POWER_STATUS_OK)
        self.outputStatus = data.get('outputStatus', PanelEventStatuses.OUTPUT_STATUS_OK)

        self.devices = []
        devices = data.get('devices', [])
        for device in devices:
            d = Device(config_context, device.get('address'))
            d.deserialize(device)
            self.devices.append(d)
