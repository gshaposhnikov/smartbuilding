import logging
import sqlite3


class EventRepository:
    def __init__(self, db_name, db_max_event_to_commit):
        logging.debug('dbName = {0}, dbMaxEventToCommit = {1}'
                      .format(db_name, db_max_event_to_commit))
        self.con = sqlite3.connect(db_name)
        self.index = self.select_last_id()
        self.commitCount = 0
        self.commitCountMax = db_max_event_to_commit

    def __del__(self):
        logging.debug('')
        self.con.commit()
        self.con.close()

    def insert(self, keys, values):
        cur = self.con.cursor()
        cur.execute('INSERT INTO events {0} VALUES{1}'
                    .format(keys, values))

        if self.commitCount >= self.commitCountMax:
            self.con.commit()
            self.commitCount = 0
        else:
            self.commitCount += 1

    def select_last_id(self):
        cur = self.con.cursor()
        cur.execute('SELECT id FROM events ORDER BY id DESC LIMIT 1')
        res = cur.fetchall()

        if len(res) == 0:
            return 0
        else:
            return res[0][0]

    def select_by_id(self, _id):
        cur = self.con.cursor()
        cur.execute('SELECT time, deviceType, context FROM events WHERE id = {0}'
                    .format(_id))
        res = cur.fetchall()
        return res[0]
