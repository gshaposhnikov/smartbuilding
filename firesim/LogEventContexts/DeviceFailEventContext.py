import logging


class DeviceFailEventContext:
    def __init__(self, fail_status='', temperature_channel_failure=False, optical_channel_failure=False):
        logging.debug('failStatus: {0}, temperatureChannelFailure: {1}, '
                      'opticalChannelFailure: {2}'
                      .format(fail_status, temperature_channel_failure, optical_channel_failure))
        self.failStatus = fail_status
        self.temperatureChannelFailure = temperature_channel_failure
        self.opticalChannelFailure = optical_channel_failure

    def get_info(self):
        logging.debug('')
        response = {
            'failStatus': self.failStatus,
            'temperatureChannelFailure': self.temperatureChannelFailure,
            'opticalChannelFailure': self.opticalChannelFailure
        }
        return {'failEventContext': response}

    def deserialize(self, data):
        self.failStatus = data.get('failStatus', '')
        self.temperatureChannelFailure = data.get('temperatureChannelFailure', False)
        self.opticalChannelFailure = data.get('opticalChannelFailure', False)
