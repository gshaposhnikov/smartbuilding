import logging


class DeviceLostEventContext:
    def __init__(self, status=False):
        logging.debug('status = {0}'.format(status))
        self.status = status

    def get_info(self):
        logging.debug('')
        response = {'lost': self.status}
        return {'lostEventContext': response}

    def deserialize(self, data):
        self.status = data.get('status', False)
