import logging

from LogEventContexts.PanelEventStatuses import PanelEventStatuses


class PanelPowerFailEventContext:
    def __init__(self, _input=0, status=''):
        logging.debug('input, {0}, status = {1}'
                      .format(_input, status))
        self.input = _input
        self.status = status

    def get_info(self):
        logging.debug('')
        response = {
            'powerStatus': self.status,
            'powerInput': self.input
        }
        return {'powerFailEventContext': response}

    def deserialize(self, data):
        status = data.get('status')
        if status in PanelEventStatuses.POWER_STATUSES:
            self.status = status
            self.input = data.get('input', 0)
        else:
            logging.warning('Invalid output status: {}'.format(status))
