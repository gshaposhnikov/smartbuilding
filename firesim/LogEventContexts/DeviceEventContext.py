import logging

from LogEventContexts.DeviceFailEventContext import DeviceFailEventContext
from LogEventContexts.DeviceLostEventContext import DeviceLostEventContext
from LogEventContexts.DeviceFireEventContext import DeviceFireEventContext


class DeviceEventContext:
    def __init__(self, line_no=0, address=0, event_type=None, *args):
        logging.debug('lineNo: {0}, address: {1}, eventType: {2}'
                      .format(line_no, address, event_type))
        self.lineNo = line_no
        self.address = address
        self.eventType = event_type
        self.context = None

        if event_type == 'fail':
            self.context = DeviceFailEventContext(*args)
        elif event_type == 'lost':
            self.context = DeviceLostEventContext(*args)
        elif event_type == 'fire':
            self.context = DeviceFireEventContext(*args)

    def get_info(self):
        logging.debug('')
        response = {
            'eventType': self.eventType,
            'lineNo': self.lineNo,
            'address': self.address
        }

        if self.context is not None:
            response.update(self.context.get_info())

        return {'deviceEventContext': response}

    def serialize(self):
        state = self.__dict__

        if self.context is not None:
            state['context'] = self.context.__dict__
        else:
            state['context'] = {}

        return state

    def deserialize(self, data):
        self.lineNo = data.get('lineNo')
        self.address = data.get('address')

        if self.eventType is None:
            self.eventType = data.get('eventType')

        if self.context is None:
            if self.eventType == 'fail':
                self.context = DeviceFailEventContext()
            elif self.eventType == 'lost':
                self.context = DeviceLostEventContext()
            elif self.eventType == 'fire':
                self.context = DeviceFireEventContext()

        self.context.deserialize(data.get('context'))
