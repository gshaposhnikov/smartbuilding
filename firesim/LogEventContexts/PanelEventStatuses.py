class PanelEventStatuses:
    POWER_STATUSES = ['normal', 'redundancy', 'fail', 'failFixed']
    POWER_STATUS_OK = 0
    OUTPUT_STATUSES = ['normal', 'breaked', 'shortCircuit', 'highLoad', 'lowLoad']
    OUTPUT_STATUS_OK = 0
