import logging

from LogEventContexts.PanelEventStatuses import PanelEventStatuses


class PanelOutputFailEventContext:
    def __init__(self, output=0, status=''):
        logging.debug('output, {0}, status = {1}'
                      .format(output, status))
        self.output = output
        self.status = status

    def get_info(self):
        logging.debug('')
        response = {
            'outputStatus': self.status,
            'outputNo': self.output
        }
        return {'outputFailEventContext': response}

    def deserialize(self, data):
        status = data.get('status')

        if status in PanelEventStatuses.OUTPUT_STATUSES:
            self.status = status
            self.output = data.get('output', 0)
        else:
            logging.warning('Invalid output status: {}'.format(status))
