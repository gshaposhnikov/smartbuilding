import logging


class DeviceFireEventContext:
    def __init__(self, status='', temperature_value=0, temperature_event=False,
                 gradient_temperature_event=False, smoke_event=False):
        logging.debug('status: {0}, temperatureValue: {1}, temperatureEvent: {2}, '
                      'gradientTemperatureEvent: {3}, smokeEvent: {4}'
                      .format(status, temperature_value, temperature_event,
                              gradient_temperature_event, smoke_event))
        self.status = status
        self.temperatureValue = temperature_value
        self.temperatureEvent = temperature_event
        self.gradientTemperatureEvent = gradient_temperature_event
        self.smokeEvent = smoke_event

    def get_info(self):
        logging.debug('')
        response = {
            'fireStatus': self.status,
            'temperatureValue': self.temperatureValue,
            'temperatureEvent': self.temperatureEvent,
            'gradientTemperatureEvent': self.gradientTemperatureEvent,
            'smokeEvent': self.smokeEvent
        }
        return {'fireEventContext': response}

    def deserialize(self, data):
        self.status = data.get('status', '')
        self.temperatureValue = data.get('temperatureValue', 0)
        self.temperatureEvent = data.get('temperatureEvent', False)
        self.gradientTemperatureEvent = data.get('gradientTemperatureEvent', False)
        self.smokeEvent = data.get('smokeEvent', False)
