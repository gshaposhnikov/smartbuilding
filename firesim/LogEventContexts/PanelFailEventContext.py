import logging


class PanelFailEventContext:
    def __init__(self, status=False):
        logging.debug('status = {0}'.format(status))
        self.status = status

    def get_info(self):
        logging.debug('')
        response = {'fail': self.status}
        return {'failEventContext': response}

    def deserialize(self, data):
        self.status = data.get('status', False)
