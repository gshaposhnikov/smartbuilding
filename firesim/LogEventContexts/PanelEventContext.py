import logging

from LogEventContexts.PanelFailEventContext import PanelFailEventContext
from LogEventContexts.PanelPowerFailEventContext import PanelPowerFailEventContext
from LogEventContexts.PanelOutputFailEventContext import PanelOutputFailEventContext


class PanelEventContext:
    def __init__(self, event_type=None, *args):
        logging.debug('eventType = {0}'.format(event_type))
        self.eventType = event_type
        self.context = None

        if event_type == 'fail':
            self.context = PanelFailEventContext(*args)
        elif event_type == 'powerFail':
            self.context = PanelPowerFailEventContext(*args)
        elif event_type == 'outputFail':
            self.context = PanelOutputFailEventContext(*args)

    def get_info(self):
        logging.debug('')
        response = {'eventType': self.eventType}

        if self.context is not None:
            response.update(self.context.get_info())

        return {'panelEventContext': response}

    def serialize(self):
        state = self.__dict__

        if self.context is not None:
            state['context'] = self.context.__dict__
        else:
            state['context'] = {}

        return state

    def deserialize(self, data):
        if self.eventType is None:
            self.eventType = data.get('eventType')

        if self.context is None:
            if self.eventType == 'fail':
                self.context = PanelFailEventContext()
            elif self.eventType == 'powerFail':
                self.context = PanelPowerFailEventContext()
            elif self.eventType == 'outputFail':
                self.context = PanelOutputFailEventContext()

        self.context.deserialize(data.get('context'))
