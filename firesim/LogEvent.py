import logging
import json
from datetime import datetime

from LogEventContexts.PanelEventContext import PanelEventContext
from LogEventContexts.DeviceEventContext import DeviceEventContext


class LogEvent:
    def __init__(self, *args, device_type=None):
        logging.debug('deviceType = {0}'.format(device_type))
        self.time = int(datetime.now().timestamp())
        self.deviceType = device_type
        self.context = None

        if device_type == 'panel':
            self.context = PanelEventContext(*args)
        elif device_type == 'fireDetector':
            self.context = DeviceEventContext(*args)

    def get_info(self):
        logging.debug('')
        response = self.context.get_info()
        response['eventTime'] = self.time
        response['deviceType'] = self.deviceType
        return response

    def serialize(self):
        state = self.__dict__

        if self.context is not None:
            state['context'] = json.dumps(self.context.serialize())
        else:
            state['context'] = '{}'

        return state

    def deserialize(self, data):
        self.time = data.get('time', 0)

        if self.deviceType is None:
            self.deviceType = data.get('deviceType')

        if self.context is None:
            if self.deviceType == 'panel':
                self.context = PanelEventContext()
            elif self.deviceType == 'fireDetector':
                self.context = DeviceEventContext()

        self.context.deserialize(data.get('context'))
