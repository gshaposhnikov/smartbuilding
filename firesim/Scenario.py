import logging
import asyncio
import json


class Scenario:
    class Event:
        def __init__(self):
            logging.debug('')
            self.devicesUpdate = []
            self.panelPower = {}
            self.panelOutput = {}
            self.pause = 1

        def action(self, panel):
            logging.debug('')
            for deviceUpdate in self.devicesUpdate:
                panel.set_device_values(deviceUpdate['lineNo'],
                                        deviceUpdate['address'],
                                        deviceUpdate)

            if len(self.panelPower) > 0:
                panel.set_panel_power_status(self.panelPower['powerInput'],
                                             self.panelPower['powerStatus'])

            if len(self.panelOutput) > 0:
                panel.set_panel_output_status(self.panelOutput['outputNo'],
                                              self.panelOutput['outputStatus'])
    # end class Event

    def __init__(self, panel, event_id):
        logging.debug('eventId: {0}'.format(event_id))
        self.events = []
        self.panel = panel
        self.currentEventId = event_id

    def parse_node(self, data):
        logging.debug('data: {0}'.format(data))
        event = Scenario.Event()

        if isinstance(data.get('pause', 0), int):
            event.pause = data['pause']

        if isinstance(data.get('devices'), list):
            event.devicesUpdate = data['devices']
            for p in event.devicesUpdate:
                if not isinstance(p, dict) or \
                        not isinstance(p['address'], int) or \
                        not isinstance(p['lineNo'], int):
                    event.devicesUpdate = []
                    break

        if isinstance(data.get('panelPower'), dict):
            event.panelPower = data['panelPower']
            if not isinstance(event.panelPower.get('powerInput'), int) and \
                    not isinstance(event.panelPower.get('powerStatus'), str):
                event.panelPower = {}

        if isinstance(data.get('panelOutput'), dict):
            event.panelOutput = data['panelOutput']
            if not isinstance(event.panelOutput.get('outputNo'), int) and \
                    not isinstance(event.panelOutput.get('outputStatus'), str):
                event.panelOutput = {}

        self.events.append(event)

    def parse(self, file_name):
        logging.debug('fileName: {0}'.format(file_name))
        valid = True

        with open(file_name) as file:
            scenario = json.loads(file.read())
            if isinstance(scenario, list):
                for node in scenario:
                    if isinstance(node, dict):
                        self.parse_node(node)
                    else:
                        valid = False
                        break
            else:
                valid = False

        if not valid:
            self.events = []

    @asyncio.coroutine
    def run(self):
        logging.debug('')
        while len(self.events) > 0:
            if self.currentEventId >= len(self.events):
                self.currentEventId = 0

            event = self.events[self.currentEventId]
            yield from asyncio.sleep(event.pause)
            event.action(self.panel)
            self.currentEventId += 1

    def serialize(self):
        return {
            'eventNo': self.currentEventId
        }

    def deserialize(self, data):
        self.currentEventId = data.get('eventNo', 0)
