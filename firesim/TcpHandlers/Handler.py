import asyncio
import logging
import concurrent.futures
import json
import bson

from TcpHandlers.TcpHandler import TcpHandler
from TcpHandlers.HttpHandler import HttpHandler


class Handler:
    def __init__(self, panel, timeout):
        logging.debug('')
        self.panel = panel
        self.timeout = timeout

    @asyncio.coroutine
    def handle_connection(self, reader, writer):
        peername = writer.get_extra_info('peername')
        logging.info('Accepted connection from {}'.format(peername))

        data = b''
        while True:
            try:
                data += yield from asyncio.wait_for(reader.read(2048), self.timeout)
            except concurrent.futures.TimeoutError:
                logging.debug('Disconnection from {0}, timeout'.format(peername))
                break

            if data:
                logging.info('Connection from {0}, data length: {1}'.format(peername, len(data)))
                writer.write(self.request_handler(data))
                data = b''
            else:
                logging.debug('Disconnection from {0}, EOF'.format(peername))
                break

        writer.close()

    def request_handler(self, data):
        logging.debug('data: {}'.format(data))

        if data:
            try:
                data = bson.loads(data)
                data = TcpHandler().handle(self.panel, data)
                data = bson.dumps(data)
            except IndexError:
                data = data.decode('ascii')

                if data.startswith('{'):
                    data = json.loads(data)
                    data = TcpHandler().handle(self.panel, data)
                    data = json.dumps(data)
                else:
                    data = HttpHandler().handle(self.panel, data)

                data = data.encode('ascii')

        logging.debug('output data: {}'.format(data))
        return data
