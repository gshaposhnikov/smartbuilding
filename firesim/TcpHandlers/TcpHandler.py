import logging


class TcpHandler:
    def handle(self, panel, data):
        logging.debug('input tcp data: {}'.format(data))

        if data.get('parameter') == 'fullPanelState':
            data['fullPanelState'] = panel.get_info()

        elif data.get('parameter') == 'devicesState' and \
                isinstance(data.get('lineNo'), int) and \
                isinstance(data.get('devices'), list):
            data['devices'] = panel.get_devices_info(data['lineNo'],
                                                     data['devices'])
        elif data.get('parameter') == 'event' and \
                isinstance(data.get('recordIndex'), int):
            data = dict(panel.get_log_event_info(data['recordIndex']), **data)

        else:
            logging.error('unknown parameter: {}'.format(data.get('parameter')))

        logging.debug('output tcp data: {}'.format(data))
        return data
