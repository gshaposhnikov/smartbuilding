import logging
import json
from urllib.parse import urlparse
from datetime import datetime


class HttpHandler:
    EVENT_LEVELS = ['info', 'warning', 'error', 'critical']

    def handle(self, panel, data):
        data_ = data.splitlines()
        method, url, schema = data_[0].split()
        response_body = ''

        data_.clear()

        if method == 'GET':
            url_ = urlparse(url)
            if url_.path.startswith('/api/v1/log'):
                query = self.http_url_query_parse(url_.query)
                first_id = query.get('first_record', '0')
                count = query.get('n_records', '1')
                query_level_id = query.get('level', '')

                if query_level_id in self.EVENT_LEVELS:
                    query_level_id = self.EVENT_LEVELS.index(query_level_id)
                else:
                    query_level_id = 0

                if first_id.isdigit() and count.isdigit():
                    first_id = int(first_id)
                    count = int(count)
                    logging.debug('Get log events. id: {0}, count: {1}'
                                  .format(first_id, count))

                    response_body = []
                    for event_id in range(first_id, first_id + count):
                        event = panel.get_log_event_info(event_id)
                        level_id = self.get_event_level_id(event)
                        if level_id >= query_level_id:
                            event['level'] = self.EVENT_LEVELS[level_id]

                            time_value = datetime.utcfromtimestamp(event['eventTime'])
                            event['date'] = time_value.strftime('%Y-%m-%d')
                            event['time'] = time_value.strftime('%H:%M:%S')

                            event['recordNo'] = event_id
                            event['object'] = event['deviceType']
                            del event['deviceType']
                            response_body.append(event)

                    response_body = {'records': response_body}
                    response_body = json.dumps(response_body)

            elif url_.path.startswith('/api/v1/plans'):
                is_panel = url_.path.endswith('/devices') or \
                           url_.path.endswith('/devices/')
                if is_panel:
                    logging.debug('Get panel info')
                    response_body = panel.get_info(True)
                    del response_body['fireLogIndex']
                    response_body['deviceType'] = 'panel'
                    response_body = json.dumps(response_body)
                else:
                    line_no, address = url_.path.split('/')[-2:]
                    logging.debug('Get devices info. lineNo: {0}, address: {1}'
                                  .format(line_no, address))
                    if line_no.isdigit() and address.isdigit():
                        address = [{
                            'startAddress': int(address),
                            'count': 1
                        }]
                        response_body = panel.get_devices_info(int(line_no), address)[0]
                        logging.debug('responseBody: {0}'.format(response_body))
                        response_body['deviceType'] = 'fireDetector'
                        response_body['lineNo'] = int(line_no)
                        response_body.update(response_body.get('fireDetector', {}))
                        del response_body['fireDetector']
                        response_body = json.dumps(response_body)

        return self.generate_http_response(schema, response_body)

    def http_url_query_parse(self, data):
        query = {}
        if len(data) > 0:
            logging.debug('data: {}'.format(data))
            query = [q.split('=') for q in data.split('&')]
            query = {k: v for k, v in query}
            logging.debug('query: {}'.format(query))
        return query

    def generate_http_response(self, schema, response_body):
        data_ = [
            '{0} 200 OK'.format(schema),
            'Content-Type: application/json; charset=UTF-8',
            'Content-Length: {0}'.format(len(response_body) + 1),
            '',
            response_body + '\n'
        ]
        return '\r\n'.join(data_)

    def get_event_level_id(self, event):
        level_id = 0
        device_type = event.get('deviceType')

        if device_type == 'panel':
            event_type = event.get('panelEventContext', {}) \
                .get('eventType')
            if event_type == 'fail':
                level_id = 1
            elif event_type == 'powerFail':
                level_id = 2
            elif event_type == 'outputFail':
                level_id = 1
        elif device_type == 'fireDetector':
            event_type = event.get('deviceEventContext', {}) \
                .get('eventType')
            if event_type == 'fail':
                level_id = 1
            elif event_type == 'lost':
                level_id = 2
            elif event_type == 'fire':
                level_id = 3

        return level_id
