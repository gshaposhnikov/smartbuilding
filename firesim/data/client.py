#!/usr/bin/env python3

import socket
import bson
import json
import argparse


def socket_connect(_address, _port):
    print('About to connect: {0}:{1}'
          .format(_address, _port))
    _sock = socket.socket()
    _sock.connect((_address, _port))
    return _sock


def socket_close(_sock=None):
    if _sock is not None:
        _sock.close()


def socket_send(_sock: socket, data: dict = '') -> dict:
    print('About to request: data: {0}'
          .format(json.dumps(data)))

    _sock.send(bson.dumps(data))

    data = _sock.recv(2048)

    data_convert = bson.loads(data)
    print('Response: {0}'.format(json.dumps(data_convert)))
    print()

    return data_convert


def get_full_panel_state(_sock):
    print('Get full panel state')
    data = {
        'sequenceNo': 1,
        'cmd': 'cmd',
        'parameter': 'fullPanelState'
    }
    return socket_send(_sock, data)


def get_devices_state(_sock):
    print('Get devices state')
    data = {
        'sequenceNo': 1,
        'cmd': 'cmd',
        'parameter': 'devicesState',
        'lineNo': 1,
        'devices': [
            {'startAddress': 1, 'count': 3},
            {'startAddress': 10, 'count': 2}
        ]
    }
    return socket_send(_sock, data)


def get_event(_sock, _index):
    print('Get event, index: {0}'.format(_index))
    data = {
        'sequenceNo': 1,
        'cmd': 'cmd',
        'parameter': 'event',
        'logType': 'fire',
        'recordIndex': _index
    }
    return socket_send(_sock, data)


if __name__ == '__main__':
    address = 'localhost'
    port = 8081

    parser = argparse.ArgumentParser(description='Firesim test client.')
    parser.add_argument('--address', default=address,
                        help='set host address, default is "{0}"'.format(address))
    parser.add_argument('--port', default=port,
                        help='set host port, default is {0}'.format(port))
    args = parser.parse_args()

    address = args.address
    port = args.port

    sock = None
    try:
        sock = socket_connect(address, port)
        for i in range(0, 1):
            panelStateData = get_full_panel_state(sock)
            get_devices_state(sock)

            index = panelStateData.get('fullPanelState', {}).get('fireLogIndex', 0)
            get_event(sock, index)
        socket_close(sock)
    except Exception as e:
        print('Connection error: {0}'.format(e))
    socket_close(sock)
