import logging

from Device import Device
from LogCollection import LogCollection
from DevicesLine import DevicesLine
from LogEventContexts.PanelEventStatuses import PanelEventStatuses


class Panel:
    def __init__(self, config_context, lines=None):
        if lines is None:
            lines = []

        logging.debug('lines: {0}'.format(lines))
        self.devicesLines = []

        for line in lines:
            self.devicesLines.append(
                DevicesLine(line[0], [Device(config_context, i)
                                      for i in range(1, line[1] + 1)]))

        self.logCollection = LogCollection(config_context.dbName,
                                           config_context.dbMaxEventToCommit)
        self.deviceStatusId = Device.STATUS_OK
        self.fireStatusId = Device.FIRE_STATUS_OK

    def get_info(self, add_lines_info=False):
        logging.debug('')
        response = {
            'fireLogIndex': self.logCollection.get_last_index(),
            'deviceStatus': Device.STATUSES[self.deviceStatusId],
            'fireStatus': Device.FIRE_STATUSES[self.fireStatusId]
        }

        if add_lines_info:
            for line in self.devicesLines:
                response['power{0}Status'.format(line.lineNo)] = \
                    PanelEventStatuses.POWER_STATUSES[line.powerStatus]
                response['output{0}Status'.format(line.lineNo)] = \
                    PanelEventStatuses.OUTPUT_STATUSES[line.outputStatus]

        return response

    def get_devices_info(self, line_no, devices):
        logging.debug('lineNo: {0}, devices: {1}'.format(line_no, devices))
        devices = [
            j for i in devices
            for j in range(i['startAddress'], i['startAddress'] + i['count'])
        ]
        response = []
        devices_line = [line for line in self.devicesLines if line.lineNo == line_no]

        if len(devices_line) == 1:
            response.extend(devices_line[0].get_devices_info(devices))

        return response

    def get_log_event_info(self, index):
        return self.logCollection.get_event_info(index)

    def set_device_values(self, _line_no, _address, _data):
        for devicesLine in self.devicesLines:
            if _line_no == devicesLine.lineNo:
                for device in devicesLine.devices:
                    if device.address == _address:
                        device.set_values(_data, self.logCollection)
                break

        self.update_panel_devices_status()
        self.update_panel_fire_status()

    def update_panel_fire_status(self):
        status = Device.FIRE_STATUS_OK

        for devicesLine in self.devicesLines:
            fire_statuses = [device.fireStatusId for device in devicesLine.devices]
            if Device.FIRE_STATUS_FIRE in fire_statuses:
                status = Device.FIRE_STATUS_FIRE
                break
            elif Device.FIRE_STATUS_WARNING in fire_statuses:
                status = Device.FIRE_STATUS_WARNING

        self.fireStatusId = status

    def update_panel_devices_status(self):
        status = Device.STATUS_OK

        for devicesLine in self.devicesLines:
            if devicesLine.powerStatus != PanelEventStatuses.POWER_STATUS_OK or \
                    devicesLine.outputStatus != PanelEventStatuses.OUTPUT_STATUS_OK:
                status = Device.STATUS_FAIL
                break
            else:
                devices_statuses = [device.deviceStatusId for device in devicesLine.devices
                                    if device.deviceStatusId != Device.STATUS_OK]
                if len(devices_statuses) > 0:
                    status = Device.STATUS_FAIL

        # check new status
        if self.deviceStatusId != status:
            if self.deviceStatusId == Device.STATUS_FAIL:  # fail -> !fail
                self.logCollection.add_event('fail', False, device_type='panel')
            elif status == Device.STATUS_FAIL:
                self.logCollection.add_event('fail', True, device_type='panel')

        self.deviceStatusId = status

    def set_panel_power_status(self, _input, status):
        logging.debug('input: {0}, status: {1}'.format(_input, status))
        devices_line = [line for line in self.devicesLines if line.lineNo == _input]

        if len(devices_line) == 1:
            devices_line[0].set_power_status(status, self)

        self.update_panel_devices_status()

    def set_panel_output_status(self, output, status):
        logging.debug('output: {0}, status: {1}'.format(output, status))
        devices_line = [line for line in self.devicesLines if line.lineNo == output]

        if len(devices_line) == 1:
            devices_line[0].set_output_status(status, self)

        self.update_panel_devices_status()

    def serialize(self):
        logging.debug('')
        state = self.__dict__
        del state['logCollection']
        state['devicesLines'] = [devicesLine.serialize() for devicesLine in self.devicesLines]
        return state

    def deserialize(self, config_context, data):
        logging.debug('data: {0}'.format(data))
        self.deviceStatusId = data.get('deviceStatusId', Device.STATUS_OK)
        self.fireStatusId = data.get('fireStatusId', Device.FIRE_STATUS_OK)

        devices_lines_data = data.get('devicesLines', [])
        for devicesLine in devices_lines_data:
            line_no = devicesLine.get('lineNo', 0)
            dl = DevicesLine(line_no)
            dl.deserialize(config_context, devicesLine)
            self.devicesLines.append(dl)
            logging.debug('dl: {0}'.format(dl))
