.open firesim.db

create table events (
    id integer primary key asc,
    time integer,
    deviceType text,
    context text
);
