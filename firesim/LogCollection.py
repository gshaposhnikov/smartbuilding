import logging
import json

from LogEvent import LogEvent
from Dao.EventRepository import EventRepository


class LogCollection:
    def __init__(self, db_name, db_max_event_to_commit):
        logging.debug('')
        self.eventRepository = EventRepository(db_name, db_max_event_to_commit)
        self.index = self.eventRepository.select_last_id()

    def add_event(self, *args, device_type=None):
        logging.debug('')
        event = LogEvent(*args, device_type=device_type).serialize()
        self.eventRepository.insert(
            tuple(event.keys()),
            tuple(event.values())
        )
        self.index += 1

    def get_last_index(self):
        return self.index

    def get_event_info(self, index):
        logging.debug('index = {0}'.format(index))
        response = {}

        if index > 0:
            state = self.eventRepository.select_by_id(index)
            state = {
                'time': state[0],
                'deviceType': state[1],
                'context': json.loads(state[2])
            }
            event = LogEvent()
            event.deserialize(state)
            response = event.get_info()

        return response
