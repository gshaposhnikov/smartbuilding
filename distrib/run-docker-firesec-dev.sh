### Скрипт для запуска контейнера сборки Firesec.
#!/bin/sh

set -e
test ":$DEBUG" != :true || set -x

GIT_DIR=$(dirname $(dirname $(pwd)))

# set image
set -- docker.satellite-soft.ru:5043/firesec-dev "$@"

# use current user and its groups at host
for v in /etc/group /etc/passwd; do
  [ ! -r "$v" ] || set -- -v $v:$v:ro "$@"
done
set -- --user "`id -u`:`id -g`" "$@"
for g in `id -G`; do
  set -- --group-add "$g" "$@"
done
set -- -v "$HOME":"$HOME" "$@"
set -- -v "$GIT_DIR":"$GIT_DIR" "$@"

exec docker run -d -it --name firesec-docker-dev "$@"
