#!/bin/bash -
set -e # stop on error (some command returnet non-0 code)

# Подготовка директорий

help() {
    me=`basename "$0"`

    echo " Usage:"
    echo " $me -t <path> [ -d <path> ]"

    echo " Options:"
    echo "  -t, --tag=<string>"
    echo "       Версия для сборки. Обязательно."
    echo "  -d, --diff=<string>"
    echo "       Путь к diff-файлу для сборки без авторизации. По умолчанию 'unauthorize.diff'"
}

CURRENT_VERSION=""
UNAUTHORIZE_DIFF_FILE="unauthorize.diff"

TEMP=`getopt -o ht:d: -l help,tag:,diff: \
     -n "$(basename $0)" -q -- "$@"`
eval set -- "${TEMP}"

while true ; do
    case "$1" in
        -h|--help) help; exit 0 ;;
        -t|--tag) CURRENT_VERSION=$2; shift 2 ;;
        -d|--diff) UNAUTHORIZE_DIFF_FILE=$2; shift 2 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

DIR_UBUNTU_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_UBUNTU_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_WINDOWS_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_win10x64"
DIR_WINDOWS_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_win10x64"

bitbucket_user=""
bitbucket_password=""

echo "Создадим дирректории для сборки дистрибутивов (full)"
mkdir -p "$DIR_UBUNTU_FULL_DISTRIB"
mkdir -p "$DIR_WINDOWS_FULL_DISTRIB"

echo "Создадим дирректории для сборки дистрибутивов (update)"
mkdir -p "$DIR_UBUNTU_UPDATE_DISTRIB"
mkdir -p "$DIR_WINDOWS_UPDATE_DISTRIB"

# $1 - package url
download_package() {
    [ -z "$bitbucket_user" ] && {
        echo "Для загрузки пакетов из репозитория butbucket нужно ввести учётные данные."
        read -p "> Butbucket user:" bitbucket_user
        read -s -p "> Butbucket password:" bitbucket_password
    }

    wget --user="$bitbucket_user" --password "$bitbucket_password" "$1"
}

# $1 - package name
# $2 - package repository
check_and_download_package() {
    local package_name="$1"
    local package_repository="$2"
    local package_url="https://bitbucket.org/satellite-soft/$package_repository/downloads/$package_name"

    [ -e "$package_name.md5" ] || {
        echo "> Download $package_name.md5"
        download_package "${package_url}.md5"
    }

    md5sum -c "$package_name.md5" || {
        echo "> Download $package_name"
        download_package "$package_url"
    }
}

download_packages() {
    echo "> Загрузка пакетов"
    mkdir -p download/windows download/ubuntu

    # windows
    pushd download/windows

    check_and_download_package "jre-8u144-windows-x64.exe" "firesec"

    [ -e "commons-daemon-1.1.0-bin-windows.zip" ] || {
        wget "http://mirror.linux-ia64.org/apache//commons/daemon/binaries/windows/commons-daemon-1.1.0-bin-windows.zip"
    }

    [ -e "mongodb-win32-x86_64-2008plus-ssl-3.4.7-signed.msi" ] || {
        wget "https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.4.7-signed.msi"
    }

    [ -e "apache-tomcat-8.5.29.exe" ] || {
        wget "http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.29/bin/apache-tomcat-8.5.29.exe"
    }

    [ -e "erlang_win64_20.3.exe" ] || {
        wget "http://erlang.org/download/otp_win64_20.3.exe" --output-document "erlang_win64_20.3.exe"
    }

    [ -e "rabbitmq-server-3.6.10.exe" ] || {
        wget "https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.6.10/rabbitmq-server-3.6.10.exe"
    }

    [ -e "Handle.zip" ] || {
        wget "https://download.sysinternals.com/files/Handle.zip"
    }

    pushd hasp
    check_and_download_package "hasp_windows_x64_83702.dll" "firesec"
    check_and_download_package "HASPJava_x64.dll" "firesec"
    check_and_download_package "Sentinel_LDK_Run-time_setup.zip" "firesec"
    popd # from hasp

    popd # from download/windows


    # ubuntu
    pushd download/ubuntu

    check_and_download_package "jre-8u144-linux-x64.tar.gz" "firesec"
    check_and_download_package "ubuntu_14.04_debs.tar.gz" "firesec"

    [ -e "apache-tomcat-8.5.29.tar.gz" ] || {
        wget "http://archive.apache.org/dist/tomcat/tomcat-8/v8.5.29/bin/apache-tomcat-8.5.29.tar.gz"
    }

    pushd hasp
    check_and_download_package "libHASPJava_x86_64.so" "firesec"
    check_and_download_package "Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz" "firesec"
    popd # from hasp

    popd # from download/ubuntu
}

create_scripts() {
    echo "> Создание скриптов"
    mkdir -p scripts/windows scripts/ubuntu

    # windows
    pushd scripts/windows

    cat > set-system-variables.bat << EOF
setx JAVA_HOME "C:\Program Files\Java\jre1.8.0_144" -m
setx JRE_HOME "C:\Program Files\Java\jre1.8.0_144" -m
setx CATALINA_HOME "C:\Program Files\Apache Software Foundation\Tomcat 8.5" -m
EOF

    cat > cert-gen.bat << EOF
cd %JAVA_HOME%
bin\keytool -genkey -alias tomcat -keyalg RSA -keystore "%CATALINA_HOME%/.keystore"
EOF

    cat > tomcat-set-autostart.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //US//Tomcat8 --Startup=auto
EOF

    cat > rabbitmq-enable-web-interface.bat << EOF
"C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.10\sbin\rabbitmq-plugins.bat" enable rabbitmq_management
EOF

    cat > register-mongo-service.bat << EOF
"C:\Program Files\MongoDB\Server\3.4\bin\mongod" --logpath "C:\data\mongo-log.txt" --install
net start MongoDB
EOF

    cat > BL-service-register.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //IS//firesecnt-bl --Jvm=auto --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ" --StartMode=java --StartClass=ru.rubezh.firesec.nt.BusinessLogicApplication --StartMethod main --Classpath=C:\firesecNT\business-logic-server.jar --LogPath=C:\firesecNt\logs --LogPrefix="firesecnt-bl-log" --StdOutput=C:\firesecNT\logs\bl-stdout-log.txt --StdError=C:\firesecNT\logs\bl-stderr-log.txt
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-bl
EOF

    cat > CS-service-register.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //IS//firesecnt-cs --Jvm=auto --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ" --StartMode=java --StartClass=ru.rubezh.firesec.nt.cs.CommunicationServer --StartMethod main --Classpath=C:\firesecNT\communication-server.jar --StartParams="-d \"C:\firesecNT\drivers\"" --LogPath=C:\firesecNT\logs --LogPrefix="firesecnt-cs-log" --StdOutput=C:\firesecNT\logs\cs-stdout-log.txt --StdError=C:\firesecNT\logs\cs-stderr-log.txt
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-cs
EOF

    mkdir -p firesecNT/bin
    pushd firesecNT/bin

    cat > stop-firesecnt-services.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //SS//firesecnt-bl
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //SS//firesecnt-cs
for /F "tokens=1,3 delims= " %%a in ('handle64 -nobanner driver-2OP3.jar') do taskkill /f /pid %%b
EOF

    cat > start-firesecnt-services.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-bl
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-cs
EOF

    cp ../../../../../report.bat .

    popd # firesecNT/bin
    popd # scripts/windows
}

download_packages
create_scripts

cp -r download/windows/* "$DIR_WINDOWS_FULL_DISTRIB"
cp -r scripts/windows/* "$DIR_WINDOWS_FULL_DISTRIB"
rm "$DIR_WINDOWS_FULL_DISTRIB"/*.md5
rm "$DIR_WINDOWS_FULL_DISTRIB"/hasp/*.md5

cp -r download/ubuntu/* "$DIR_UBUNTU_FULL_DISTRIB"
rm "$DIR_UBUNTU_FULL_DISTRIB"/*.md5
rm "$DIR_UBUNTU_FULL_DISTRIB"/hasp/*.md5

cp -r scripts/windows/firesecNT "$DIR_WINDOWS_UPDATE_DISTRIB"

cp -r download/windows/hasp "$DIR_WINDOWS_UPDATE_DISTRIB"
cp -r download/ubuntu/hasp "$DIR_UBUNTU_UPDATE_DISTRIB"
rm "$DIR_WINDOWS_UPDATE_DISTRIB"/hasp/*.md5
rm "$DIR_UBUNTU_UPDATE_DISTRIB"/hasp/*.md5

# TODO: добавить работу с миграциями дистрибутивов

echo "Подготовка дирректорий дистрибутивов завершена!"
