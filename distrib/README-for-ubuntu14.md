Инструкция по разворачиванию проекта FireSec
============================================

Подготовка сервера к разворачиванию
-----------------------------------

### Установить Java 8 от Oracle

Создать дирректорию для разворачивания

    sudo mkdir -p /opt/jre

Распаковать архив

    sudo tar -zxf jre-8u144-linux-x64.tar.gz -C /opt/jre

Добавить ссылку для удобства вызова

    sudo update-alternatives --install /usr/bin/java java /opt/jre/jre1.8.0_144/bin/java 100

Установить переменные окружения

    export JAVA_HOME=/opt/jre/jre1.8.0_144
    echo "export JAVA_HOME=/opt/jre/jre1.8.0_144" >> ~/.bashrc

Проверить

    java -version

### Установить MongoDB 3.4 и сервер очередей

Установить

    tar -zxf ubuntu_14.04_debs.tar.gz
    sudo dpkg -i debs/*

Для проверки установки MongoDB запустить клиент

    mongo

Включить плагин WEB-management для сервера очередей

    sudo rabbitmq-plugins enable rabbitmq_management
    sudo service rabbitmq-server restart

Прописать ограничения на размер очередей и эксчейнджей

    sudo rabbitmqctl set_policy messaging_limits '^ru\.rubezh\.firesec\.nt\..*' '{"max-length-bytes":1073741824}'

Для проверки работы открыть в браузере 'localhost:15672', логин 'guest', пароль 'guest'.


### Установить Tomcat 8.5.15

Создать пользователя и группу

    sudo groupadd tomcat
    sudo mkdir /opt/tomcat
    sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

Установить дистрибутив

    sudo tar xvf apache-tomcat-8.5.16.tar.gz -C /opt/tomcat --strip-components=1
    sudo -i
    pushd /opt/tomcat

Опционально: Разрешить заходить в админку с не локальной машины

    mkdir -p conf/Catalina/localhost
    cat > conf/Catalina/localhost/manager.xml << EOF
    <Context privileged="true"
             antiResourceLocking="false"
             docBase="\${catalina.home}/webapps/manager">
        <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
    </Context>
    EOF

Настроить сервис

    cat > /etc/init/tomcat.conf << EOF
    description "Tomcat Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    setuid tomcat
    setgid tomcat

    env JAVA_HOME=/opt/jre/jre1.8.0_144
    env CATALINA_HOME=/opt/tomcat

    # Modify these options as needed
    env JAVA_OPTS="-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom"
    env CATALINA_OPTS="-Xms512M -Xmx1024M -server -XX:+UseParallelGC"

    exec /opt/tomcat/bin/catalina.sh run

    # cleanup temp directory after stop
    post-stop script
        rm -rf "\$CATALINA_HOME"/temp/*
    end script
    EOF

Добавить в файл '/opt/tomcat/conf/tomcat-users.xml' пользователей для админки

    <tomcat-users>
      ...
      <user username="admin" password="admin" roles="manager-gui,admin-gui"/>
    </tomcat-users>

Прописать права пользователю tomcat

    chown -R tomcat:tomcat .
    chmod g+rwx conf
    chmod g+r -R conf

Прописать права всем локальным пользователям для развертывания приложений

    chmod a+rwx webapps logs

Сгенерировать сертификат для работы по https (необходимо будет ввести данные о компании, местоположении и др.)

    /opt/jre/jre1.8.0_144/bin/keytool -genkey -alias tomcat -keyalg RSA -keystore /opt/tomcat/.keystore

*Опционально:* Импортировать готовый сертификат

    openssl pkcs12 -export -in <mycert>.crt -inkey <mykey>.key -out <mycert>.p12 -name tomcat -CAfile <myCA>.crt -caname root -chain

Включить и настроить работу https в tomcat - открыть файл <tomcat_dir>/conf/server.xml

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

*Опционально:* Увеличить таймаут асинхронных запросов (требуется для работы c проектами Firesec, содержащими более 10 сценариев)

Добавить в connector-ы по протам 8443 и 8080 параметр asyncTimeout="300000":

    <Connector port="8080" protocol="HTTP/1.1" asyncTimeout="300000"
               connectionTimeout="20000"
               redirectPort="8443" />

    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" asyncTimeout="300000"
               maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
               keystoreFile="/opt/tomcat/.keystore" keystorePass="tomcat"
               clientAuth="false" sslProtocol="TLS" />

Настроить время сессии - открыть файл:

    sudo nano conf/web.xml

Добавить в файл следующие строки:

    <session-config>
        <session-timeout>-1</session-timeout>
    </session-config>

Проинициализировать и запустить сервис

    initctl reload-configuration
    initctl start tomcat

Добавления прав для чтого, чтобы USB устроства были доступны, открываем файл

    nano /etc/udev/rules.d/99-myhid.rules

Добавляем в файл соответсвующии строки:

    # RUBEZH HID
    # idVendor 0xc251 Keil Software, Inc.
    # idProduct 0x1303
    ATTRS{idProduct}=="1303", ATTRS{idVendor}=="c251", MODE="0660", GROUP="plugdev"

Копируем библиотеку HASP

    cp libHASPJava.so /usr/lib

Распаковать архив и установить драйвер HASP

    tar xzf Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz
    dpkg -i Sentinel_LDK_Ubuntu_DEB_Run-time_Installer/aksusbd_7.81-1_amd64.deb

Отключиться от root'а

    exit

Для проверки работы открыть в браузере 'localhost:8080'.


Подготовка к запуску проекта (однократно)
-----------------------------------------

Создать папку для проекта

    sudo -i
    mkdir -p /opt/firesecNT

Прописать права всем локальным пользователям для развертывания приложений

    chmod a+rwx -R /opt/firesecNT

Добавить конфигурационные файлы для автозапуска

    cat > /etc/init/firesecnt-bl.conf << EOF
    description "FiresecNT business logic Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    env JAVA_HOME=/opt/jre/jre1.8.0_144

    chdir /opt/firesecNT

    exec java -jar /opt/firesecNT/business-logic-server.jar
    EOF

    cat > /etc/init/firesecnt-cs.conf << EOF
    description "FiresecNT communication Server"

    start on runlevel [2345]
    stop on runlevel [!2345]
    respawn
    respawn limit 10 5

    env JAVA_HOME=/opt/jre/jre1.8.0_144

    chdir /opt/firesecNT

    exec java -jar /opt/firesecNT/communication-server.jar -d /opt/firesecNT/drivers
    EOF

    initctl reload-configuration

    exit

Разворачивание/обновление системы
---------------------------------

### Разворачивание frontend сервера

Скопировать скомпилированную версию в apache tomcat (или задеплоить через веб-интерфейс):

    cp firesecNT/ROOT.war /opt/tomcat/webapps

### Обновление сервера бизнес-логики и драйверов

Остановить модули (если запущены)

    sudo initctl stop firesecnt-bl
    sudo initctl stop firesecnt-cs

Скопировать новую версию

    rm -rf /opt/firesecNT/*
    cp -r firesecNT/* /opt/firesecNT/

Запустить модули

    sudo initctl start firesecnt-bl
    sudo initctl start firesecnt-cs

Настройка оповещений в браузере
-------------------------------
### google chrome

##### Русскоязычный:

    Настройки -> Дополнительные -> Настройки контента -> Звук

В разделе **Разрешить** нажать **Добавить** и ввести:

    https://<ip-адрес хоста>:8443/

##### Англоязычный:

    Settings -> Advanced -> Content settings -> Sound

В разделе **Allow** нажать **Add** и ввести:


    https://<ip-адрес хоста>:8443/
