#!/bin/bash -
set -e # stop on error (some command returnet non-0 code)

# Подготовка конечных дистрибутивов и пакетов обновлений

help() {
    me=`basename "$0"`

    echo " Usage:"
    echo " $me [ -t <path> | -l <path> | -a | -d <path> | -f | -n ]"

    echo " Options:"
    echo "  -t, --tag=<string>"
    echo "       Версия для сборки. Обязательно."
    echo "  -l, --last_tag=<string>"
    echo "       Прошлая версия сборки. Используется для переименования дирректорий. По умолчанию равна текущей."
    echo "  -a, --authorize"
    echo "       Включить авторизацию. По умолчанию выключена"
    echo "  -d, --diff=<string>"
    echo "       Путь к diff-файлу для сборки без авторизации. По умолчанию 'unauthorize.diff'"
    echo "  -f, --full"
    echo "       Собирать не только пакет обновления, но и полноценный дистрибутив."
    echo "  -n, --no_compile"
    echo "       Собирать без компиляции."
}

LAST_VERSION=""
CURRENT_VERSION=""
ON_AUTHORIZE="false"
UNAUTHORIZE_DIFF_FILE="unauthorize.diff"
CREATE_FULL_DISTR="false"
COMPILE="true"

TEMP=`getopt -o ht:l:ad:fn -l help,tag:,last_tag:,authorize,diff:,full,no_compile \
     -n "$(basename $0)" -q -- "$@"`
eval set -- "$TEMP"

while true ; do
    case "$1" in
        -h|--help) help; exit 0 ;;
        -t|--tag) CURRENT_VERSION=$2; shift 2 ;;
        -l|--last_tag) LAST_VERSION=$2; shift 2 ;;
        -a|--authorize) ON_AUTHORIZE="true"; shift ;;
        -d|--diff) UNAUTHORIZE_DIFF_FILE=$2; shift 2 ;;
        -f|--full) CREATE_FULL_DISTR="true"; shift ;;
        -n|--no_compile) COMPILE="false"; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

DIR_UBUNTU_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_UBUNTU_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_WINDOWS_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_win10x64"
DIR_WINDOWS_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_win10x64"

# Сборка
[ "true" = "$COMPILE" ] && {
    DIR="${BASH_SOURCE%/*}"
    [[ ! -d "$DIR" ]] && DIR="$PWD"
    cmd="${DIR}/do_create.sh --tag '${CURRENT_VERSION}'"
    [ "true" = "$ON_AUTHORIZE" ] && {
        cmd="$cmd --authorize"
    } || {
        cmd="$cmd --diff '${UNAUTHORIZE_DIFF_FILE}'"
    }
    eval "$cmd"
}

[ -d "build/firesecNT" ] || { echo "Error: build/firesecNT directory not found!"; exit 1; }

# Подготовка дирректорий
mkdir -p build
pushd build

[ -n "$LAST_VERSION" ] && {
    [ -d "firesecnt_${LAST_VERSION}_for_ubuntu14.04LTSx64" -a -d "firesecnt_${LAST_VERSION}_for_win10x64" ] && {
        echo "Переименуем дирректории с прошлой версии (full)"
        mv "firesecnt_${LAST_VERSION}_for_ubuntu14.04LTSx64" "$DIR_UBUNTU_FULL_DISTRIB"
        mv "firesecnt_${LAST_VERSION}_for_win10x64" "$DIR_WINDOWS_FULL_DISTRIB"
    }
    [ -d "firesecnt_update_to_${LAST_VERSION}_for_ubuntu14.04LTSx64" -a -d "firesecnt_update_to_${LAST_VERSION}_for_win10x64" ] && {
        echo "Переименуем дирректории с прошлой версии (update)"
        mv "firesecnt_update_to_${LAST_VERSION}_for_ubuntu14.04LTSx64" "$DIR_UBUNTU_UPDATE_DISTRIB"
        mv "firesecnt_update_to_${LAST_VERSION}_for_win10x64" "$DIR_WINDOWS_UPDATE_DISTRIB"
    }
}
../default_dirs.sh --tag "$CURRENT_VERSION" --diff "$UNAUTHORIZE_DIFF_FILE"

[ "true" = "$CREATE_FULL_DISTR" ] && {
    cp ../README-for-ubuntu14.md "$DIR_UBUNTU_FULL_DISTRIB/README.md"

    lowriter --convert-to pdf --invisible ../README-for-win10.docx
    mv README-for-win10.pdf "$DIR_WINDOWS_FULL_DISTRIB/README.pdf"
}

# update distribs
cp ../README-for-ubuntu14-update.md "$DIR_UBUNTU_UPDATE_DISTRIB/README.md"

lowriter --convert-to pdf --invisible ../README-for-win10-update.docx
mv README-for-win10-update.pdf "$DIR_WINDOWS_UPDATE_DISTRIB/README.pdf"

popd # from build

echo "Начинаем копирование..."
copyTo() {
    DIR_TO_COPY="build/$1"
    echo "> Copy to $DIR_TO_COPY"
    cp -r build/firesecNT "$DIR_TO_COPY"
}

[ "true" = "$CREATE_FULL_DISTR" ] && {
    copyTo "$DIR_UBUNTU_FULL_DISTRIB"
    copyTo "$DIR_WINDOWS_FULL_DISTRIB"
}
copyTo "$DIR_UBUNTU_UPDATE_DISTRIB"
copyTo "$DIR_WINDOWS_UPDATE_DISTRIB"
echo "Копирование завершено"

echo "Упаковка..."
packingTo() {
    pushd build
    DIR_DISTRIB="$1"
    METHOD="$2"

    echo "> Packing build/$DIR_DISTRIB"
    [ "$METHOD" = "tar" ] && {
        tar -zcf "$DIR_DISTRIB.tar.gz" "$DIR_DISTRIB"
    } || {
        zip -rq "$DIR_DISTRIB.zip" "$DIR_DISTRIB"
    }
    popd # from build
}

[ "true" = "$CREATE_FULL_DISTR" ] && {
    packingTo "$DIR_UBUNTU_FULL_DISTRIB" "tar"
    packingTo "$DIR_WINDOWS_FULL_DISTRIB" "zip"
}
packingTo "$DIR_UBUNTU_UPDATE_DISTRIB" "tar"
packingTo "$DIR_WINDOWS_UPDATE_DISTRIB" "zip"
echo "Упаковка завершена!"

echo "Сборка дистрибутивов завершена!"
