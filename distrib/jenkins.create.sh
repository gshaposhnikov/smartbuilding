#!/bin/bash -
# Скрипт инициализации сборки Firesec

set -e # stop on error (some command returnet non-0 code)

# Подготовка конечных дистрибутивов и пакетов обновлений

help() {
    me=`basename "$0"`

    echo " Usage:"
    echo " $me [ -t <path> | -l <path> | -a | -d <path> | -f | -n ]"

    echo " Options:"
    echo "  -t, --tag=<string>"
    echo "       Версия для сборки. Обязательно."
    echo "  -l, --last_tag=<string>"
    echo "       Прошлая версия сборки. Используется для переименования дирректорий. По умолчанию равна текущей."
    echo "  -a, --authorize"
    echo "       Включить авторизацию. По умолчанию выключена"
    echo "  -d, --diff=<string>"
    echo "       Путь к diff-файлу для сборки без авторизации. По умолчанию 'unauthorize.diff'"
    echo "  -f, --full"
    echo "       Собирать не только пакет обновления, но и полноценный дистрибутив."
    echo "  -n, --no_compile"
    echo "       Собирать без компиляции."
}

LAST_VERSION=""
CURRENT_VERSION="$(git tag -l | sort -V | tail -1)"
ON_AUTHORIZE="false"
UNAUTHORIZE_DIFF_FILE="unauthorize.diff"
CREATE_FULL_DISTR="false"
COMPILE="true"

TEMP=`getopt -o ht:l:ad:fn -l help,tag:,last_tag:,authorize,diff:,full,no_compile \
     -n "$(basename $0)" -q -- "$@"`
eval set -- "$TEMP"

while true ; do
    case "$1" in
        -h|--help) help; exit 0 ;;
        -t|--tag) CURRENT_VERSION=$2; shift 2 ;;
        -l|--last_tag) LAST_VERSION=$2; shift 2 ;;
        -a|--authorize) ON_AUTHORIZE="true"; shift ;;
        -d|--diff) UNAUTHORIZE_DIFF_FILE=$2; shift 2 ;;
        -f|--full) CREATE_FULL_DISTR="true"; shift ;;
        -n|--no_compile) COMPILE="false"; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

DIR_UBUNTU_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_UBUNTU_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_WINDOWS_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_win10x64"
DIR_WINDOWS_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_win10x64"

# Сборка
[ "true" = "$COMPILE" ] && {
    DIR="${BASH_SOURCE%/*}"
    [[ ! -d "$DIR" ]] && DIR="$PWD/distrib"
    cmd="${DIR}/jenkins.do_create.sh --tag '${CURRENT_VERSION}'"
    [ "true" = "$ON_AUTHORIZE" ] && {
        cmd="$cmd --authorize"
    } || {
        cmd="$cmd --diff '${UNAUTHORIZE_DIFF_FILE}'"
    }
    eval "$cmd"
}

[ -d "distrib/build/firesecNT" ] || { echo "Error: distrib/build/firesecNT directory not found!"; exit 1; }

# Подготовка дирректорий
pushd ./distrib > /dev/null
mkdir -p build
pushd ./build > /dev/null

[ -n "$LAST_VERSION" ] && {
    [ -d "firesecnt_${LAST_VERSION}_for_ubuntu14.04LTSx64" -a -d "firesecnt_${LAST_VERSION}_for_win10x64" ] && {
        echo "Переименуем дирректории с прошлой версии (full)"
        mv "firesecnt_${LAST_VERSION}_for_ubuntu14.04LTSx64" "$DIR_UBUNTU_FULL_DISTRIB"
        mv "firesecnt_${LAST_VERSION}_for_win10x64" "$DIR_WINDOWS_FULL_DISTRIB"
    }
    [ -d "firesecnt_update_to_${LAST_VERSION}_for_ubuntu14.04LTSx64" -a -d "firesecnt_update_to_${LAST_VERSION}_for_win10x64" ] && {
        echo "Переименуем дирректории с прошлой версии (update)"
        mv "firesecnt_update_to_${LAST_VERSION}_for_ubuntu14.04LTSx64" "$DIR_UBUNTU_UPDATE_DISTRIB"
        mv "firesecnt_update_to_${LAST_VERSION}_for_win10x64" "$DIR_WINDOWS_UPDATE_DISTRIB"
    }
}

echo "Создадим папки по умолчанию"

DIR_UBUNTU_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_UBUNTU_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_ubuntu14.04LTSx64"
DIR_WINDOWS_FULL_DISTRIB="firesecnt_${CURRENT_VERSION}_for_win10x64"
DIR_WINDOWS_UPDATE_DISTRIB="firesecnt_update_to_${CURRENT_VERSION}_for_win10x64"

bitbucket_user="jenkins-ssl"
bitbucket_password="701e10818c"

echo "Создадим дирректории для сборки дистрибутивов (full)"
mkdir -p "$DIR_UBUNTU_FULL_DISTRIB"
mkdir -p "$DIR_WINDOWS_FULL_DISTRIB"

echo "Создадим дирректории для сборки дистрибутивов (update)"
mkdir -p "$DIR_UBUNTU_UPDATE_DISTRIB"
mkdir -p "$DIR_WINDOWS_UPDATE_DISTRIB"

# $1 - package url
download_package() {
#    [ -z "$bitbucket_user" ] && {
#        echo "Для загрузки пакетов из репозитория butbucket нужно ввести учётные данные."
#        read -p "> Butbucket user:" bitbucket_user
#        read -s -p "> Butbucket password:" bitbucket_password
#    }

    wget --user="$bitbucket_user" --password "$bitbucket_password" "$1"
}

# $1 - package name
# $2 - package repository
check_and_download_package() {
    local package_name="$1"
    local package_repository="$2"
    local package_url="https://bitbucket.org/satellite-soft/$package_repository/downloads/$package_name"

    [ -e "$package_name.md5" ] || {
        echo "> Download $package_name.md5"
        download_package "${package_url}.md5"
    }

    md5sum -c "$package_name.md5" || {
        echo "> Download $package_name"
        download_package "$package_url"
    }
}

download_packages() {
    echo "> Загрузка пакетов"
    mkdir -p download/windows download/ubuntu

    # windows
    pushd download/windows

    check_and_download_package "jre-8u144-windows-x64.exe" "firesec"

    [ -e "commons-daemon-1.1.0-bin-windows.zip" ] || {
        wget "https://mirror.linux-ia64.org/apache/commons/daemon/binaries/windows/commons-daemon-1.1.0-bin-windows.zip"
    }

    [ -e "mongodb-win32-x86_64-2008plus-ssl-3.4.7-signed.msi" ] || {
        wget "https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.4.7-signed.msi"
    }

    [ -e "apache-tomcat-8.5.29.exe" ] || {
        wget "https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.29/bin/apache-tomcat-8.5.29.exe"
    }

    # TODO: этот файл категорически не хочет качаться при сборке дженкинсом.
    # Получаем HTML файл, содержащий такие строки:
    # Authentication required
    # <!--
    # You are authenticated as: anonymous
    # Groups that you are in:
    # 
    # Permission you need to have (but didn't): hudson.model.Hudson.Read
    #  ... which is implied by: hudson.security.Permission.GenericRead
    #  ... which is implied by: hudson.model.Hudson.Administer
    # -->
    # [ -e "erlang_win64_20.3.exe" ] || {
    #     wget "http://erlang.org/download/otp_win64_20.3.exe" --output-document "erlang_win64_20.3.exe"
    # }

    # TODO: обновить версию rabbitmq или закинуть дистрибутив на своё облако
    # [ -e "rabbitmq-server-3.6.10.exe" ] || {
    #     wget "https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.6.10/rabbitmq-server-3.6.10.exe"
    # }

    # TODO: удалить совсем, если не возникнет новая необходимость
    # [ -e "Handle.zip" ] || {
    #     wget "https://download.sysinternals.com/files/Handle.zip"
    # }

    # hasp
    mkdir -p hasp
    pushd hasp
    check_and_download_package "hasp_windows_x64_83702.dll" "firesec"
    check_and_download_package "HASPJava_x64.dll" "firesec"
    check_and_download_package "Sentinel_LDK_Run-time_setup.zip" "firesec"
    popd # from hasp

    popd # from download/windows


    # ubuntu
    pushd download/ubuntu

    check_and_download_package "jre-8u144-linux-x64.tar.gz" "firesec"
    check_and_download_package "ubuntu_14.04_debs.tar.gz" "firesec"

    [ -e "apache-tomcat-8.5.29.tar.gz" ] || {
        wget "https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.29/bin/apache-tomcat-8.5.29.tar.gz"
    }

    # hasp
    mkdir -p hasp
    pushd hasp
    check_and_download_package "libHASPJava_x86_64.so" "firesec"
    check_and_download_package "Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz" "firesec"
    popd # from hasp

    popd # from download/ubuntu
}

create_scripts() {
    echo "> Создание скриптов"
    mkdir -p scripts/windows scripts/ubuntu

    # windows
    pushd scripts/windows

    cat > set-system-variables.bat << EOF
setx JAVA_HOME "C:\Program Files\Java\jre1.8.0_144" -m
setx JRE_HOME "C:\Program Files\Java\jre1.8.0_144" -m
setx CATALINA_HOME "C:\Program Files\Apache Software Foundation\Tomcat 8.5" -m
EOF

    cat > cert-gen.bat << EOF
cd %JAVA_HOME%
bin\keytool -genkey -alias tomcat -keyalg RSA -keystore "%CATALINA_HOME%/.keystore"
EOF

    cat > tomcat-set-autostart.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //US//Tomcat8 --Startup=auto
EOF

    cat > rabbitmq-enable-web-interface.bat << EOF
"C:\Program Files\RabbitMQ Server\rabbitmq_server-3.6.10\sbin\rabbitmq-plugins.bat" enable rabbitmq_management
EOF

    cat > register-mongo-service.bat << EOF
"C:\Program Files\MongoDB\Server\3.4\bin\mongod" --logpath "C:\data\mongo-log.txt" --install
net start MongoDB
EOF

    cat > BL-service-register.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //IS//firesecnt-bl --Jvm=auto --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ" --StartMode=java --StartClass=ru.rubezh.firesec.nt.BusinessLogicApplication --StartMethod main --Classpath=C:\firesecNT\business-logic-server.jar --LogPath=C:\firesecNt\logs --LogPrefix="firesecnt-bl-log" --StdOutput=C:\firesecNT\logs\bl-stdout-log.txt --StdError=C:\firesecNT\logs\bl-stderr-log.txt
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-bl
EOF

    cat > CS-service-register.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //IS//firesecnt-cs --Jvm=auto --Startup=auto ++DependsOn="MongoDB;Tomcat8;RabbitMQ" --StartMode=java --StartClass=ru.rubezh.firesec.nt.cs.CommunicationServer --StartMethod main --Classpath=C:\firesecNT\communication-server.jar --StartParams="-d \"C:\firesecNT\drivers\"" --LogPath=C:\firesecNT\logs --LogPrefix="firesecnt-cs-log" --StdOutput=C:\firesecNT\logs\cs-stdout-log.txt --StdError=C:\firesecNT\logs\cs-stderr-log.txt
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-cs
EOF

    mkdir -p firesecNT/bin
    pushd firesecNT/bin

    cat > stop-firesecnt-services.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //SS//firesecnt-bl
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //SS//firesecnt-cs
wmic PROCESS Where "CommandLine Like '%%-jar driver-2OP3.jar%%' AND NOT CommandLine Like 'wmic %%'" Call Terminate
EOF

    cat > start-firesecnt-services.bat << EOF
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-bl
C:\commons-daemon-1.1.0-bin-windows\amd64\prunsrv.exe //ES//firesecnt-cs
EOF

    cp ../../../../../report.bat .

    popd # firesecNT/bin
    popd # scripts/windows
}

download_packages
create_scripts

cp -r download/windows/* "$DIR_WINDOWS_FULL_DISTRIB"
cp -r scripts/windows/* "$DIR_WINDOWS_FULL_DISTRIB"
rm "$DIR_WINDOWS_FULL_DISTRIB"/*.md5
rm "$DIR_WINDOWS_FULL_DISTRIB"/hasp/*.md5

cp -r download/ubuntu/* "$DIR_UBUNTU_FULL_DISTRIB"
rm "$DIR_UBUNTU_FULL_DISTRIB"/*.md5
rm "$DIR_UBUNTU_FULL_DISTRIB"/hasp/*.md5

cp -r scripts/windows/firesecNT "$DIR_WINDOWS_UPDATE_DISTRIB"

cp -r download/windows/hasp "$DIR_WINDOWS_UPDATE_DISTRIB"
cp -r download/ubuntu/hasp "$DIR_UBUNTU_UPDATE_DISTRIB"
rm "$DIR_WINDOWS_UPDATE_DISTRIB"/hasp/*.md5
rm "$DIR_UBUNTU_UPDATE_DISTRIB"/hasp/*.md5

# TODO: добавить работу с миграциями дистрибутивов

echo "Подготовка дирректорий дистрибутивов завершена!"

[ "true" = "$CREATE_FULL_DISTR" ] && {
    cp ../README-for-ubuntu14.md "$DIR_UBUNTU_FULL_DISTRIB/README.md"

#export HOME=/tmp && libreoffice --headless --invisible --convert-to pdf ../README-for-win10.docx
#    mv README-for-win10.pdf "$DIR_WINDOWS_FULL_DISTRIB/README.pdf"
    cp ../README-for-win10.docx "$DIR_WINDOWS_FULL_DISTRIB/README.docx"
}

# update distribs
cp ../README-for-ubuntu14-update.md "$DIR_UBUNTU_UPDATE_DISTRIB/README.md"

#export HOME=/tmp && libreoffice --headless --invisible --convert-to pdf ../README-for-win10-update.docx
#mv README-for-win10-update.pdf "$DIR_WINDOWS_UPDATE_DISTRIB/README.pdf"
cp ../README-for-win10-update.docx "$DIR_WINDOWS_UPDATE_DISTRIB/README.docx"

popd # from build

echo "Начинаем копирование..."
copyTo() {
    DIR_TO_COPY="build/$1"
    echo "> Copy to $DIR_TO_COPY"
    cp -r build/firesecNT "$DIR_TO_COPY"
}

[ "true" = "$CREATE_FULL_DISTR" ] && {
    copyTo "$DIR_UBUNTU_FULL_DISTRIB"
    copyTo "$DIR_WINDOWS_FULL_DISTRIB"
}
copyTo "$DIR_UBUNTU_UPDATE_DISTRIB"
copyTo "$DIR_WINDOWS_UPDATE_DISTRIB"
echo "Копирование завершено"

echo "Упаковка..."
packingTo() {
    pushd build
    DIR_DISTRIB="$1"
    METHOD="$2"

    echo "> Packing build/$DIR_DISTRIB"
    [ "$METHOD" = "tar" ] && {
        tar -zcf "$DIR_DISTRIB.tar.gz" "$DIR_DISTRIB"
    } || {
        zip -rq "$DIR_DISTRIB.zip" "$DIR_DISTRIB"
    }
    popd # from build
}

[ "true" = "$CREATE_FULL_DISTR" ] && {
    packingTo "$DIR_UBUNTU_FULL_DISTRIB" "tar"
    packingTo "$DIR_WINDOWS_FULL_DISTRIB" "zip"
}
packingTo "$DIR_UBUNTU_UPDATE_DISTRIB" "tar"
packingTo "$DIR_WINDOWS_UPDATE_DISTRIB" "zip"
echo "Упаковка завершена!"
echo "Сборка дистрибутивов завершена!"

echo "Отправка в NextCloud..."
pushd build
curl -kT "$DIR_UBUNTU_FULL_DISTRIB.tar.gz"  --user jenkins:SSLjenkins https://nc.satellite-soft.ru/remote.php/webdav/firesec-builds/
curl -kT "$DIR_WINDOWS_FULL_DISTRIB.zip"  --user jenkins:SSLjenkins https://nc.satellite-soft.ru/remote.php/webdav/firesec-builds/
curl -kT "$DIR_UBUNTU_UPDATE_DISTRIB.tar.gz"  --user jenkins:SSLjenkins https://nc.satellite-soft.ru/remote.php/webdav/firesec-builds/
curl -kT "$DIR_WINDOWS_UPDATE_DISTRIB.zip"  --user jenkins:SSLjenkins https://nc.satellite-soft.ru/remote.php/webdav/firesec-builds/
echo "Отправка завершена!"

echo "Удаление из Jenkins архивов!"
rm -R "$DIR_UBUNTU_FULL_DISTRIB"
rm -R "$DIR_WINDOWS_FULL_DISTRIB"
rm -R "$DIR_UBUNTU_UPDATE_DISTRIB"
rm -R "$DIR_WINDOWS_UPDATE_DISTRIB"
rm -R "$DIR_UBUNTU_FULL_DISTRIB.tar.gz"
rm -R "$DIR_WINDOWS_FULL_DISTRIB.zip"
rm -R "$DIR_UBUNTU_UPDATE_DISTRIB.tar.gz"
rm -R "$DIR_WINDOWS_UPDATE_DISTRIB.zip"
rm -R "firesecNT"
rm -R "scripts"

echo "Готово!"
