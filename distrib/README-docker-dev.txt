Инструкция по сборке проекта в контейнере докер.
Cкачиваем исходники (здесь и далее по тексту - вместо username - свой пользователь в bitbucket или ПК)

На рабочей машине создаём папку ~/workspace/
mkdir ~/workspace/

Далее скачиваем исходники (здесь и далее по тексту - вместо username - свой пользователь в bitbucket или ПК)
git clone https://username@bitbucket.org/satellite-soft/firesec.git
cd firesec/
git remote add upstream https://username@bitbucket.org/satellite-soft/firesec.git

Конфигурируем git
git config --global user.email "vasya.pupkin@satellite-soft.ru"
git config --global user.name "Vasily Pupkin"

Даём права на папку ~/.config (необходимо для сборщика)
sudo chown -R username:username ~/.config

Переходим в папку distrib и запускаем скрипт:
username@PC:~$ ./run-docker-firesec-dev.sh
Контейнер запустится в фоновом режиме.

Подключаемся к нашему контейнеру:
docker attach firesec-docker-dev

Переходим в папку с скриптом сборки проекта
username@4ef1869aa205:/home$ cd <свой путь>/firesec/distrib/

Запускаем скрипт сборки create.sh (--tag <версия сборки> --full для полной сборки)
username@4ef1869aa205:<свой путь>/firesec/distrib$ ./create.sh --tag v0.16.2 --full
Password for 'https://username@bitbucket.org':
Начало сборки...
> Сборка frontend
yarn install v1.6.0
[1/4] Resolving packages...
success Already up-to-date.
Done in 1.64s.
yarn run v1.6.0
$ node scripts/build.js
Creating an optimized production build...
...........
~/workspace/firesec/distrib
Упаковка завершена!
Сборка дистрибутивов завершена!

После сборки если вы хотите оставить контейнер в рабочем состоянии, но выйти из него, то CTRL+P CTRL+Q.
Если хотите выключить контейнер, то CTRL+D или exit.

Сборки лежат в папке <свой путь>/firesec/distrib/build




ЕСЛИ НЕ УСТАНОВЛЕН DOCKER - инструкция ниже.

1: Установка Docker

Обновите индекс пакетов:

sudo apt-get update

Теперь можно загрузить и установить пакет Docker. Добавьте в систему GPG-ключ репозитория Docker:

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

Добавьте этот репозиторий в APT:

echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

Обновите индекс пакетов системы:

sudo apt-get update

Следующая команда позволяет переключиться из репозитория Ubuntu 16.04 в репозиторий Docker:

apt-cache policy docker-engine

Команда должна вернуть:

docker-engine:
Installed: (none)
Candidate: 1.11.1-0~xenial
Version table:
1.11.1-0~xenial 500
500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
1.11.0-0~xenial 500
500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages

Обратите внимание: пакет docker-engine пока не установлен. Версия пакета может отличаться.

Чтобы установить Docker, введите:

sudo apt-get install -y docker-engine

После этого программа Docker будет установлена; также это запустит демона и настроит автозапуск процесса. Чтобы убедиться в том, что программа работает, запросите её состояние:

sudo systemctl status docker

Команда должна вернуть:

docker.service - Docker Application Container Engine
Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
Active: active (running) since Sun 2016-05-01 06:53:52 CDT; 1 weeks 3 days ago
Docs: https://docs.docker.com
Main PID: 749 (docker)

Теперь в системе работает сервис Docker (или демон). Также у вас есть доступ к утилите командной строки docker (это клиент Docker).

2: Настройка команды docker (опционально)

По умолчанию команда docker требует привилегий root (или доступа к команде sudo). Также её можно запускать в группе docker, которая создаётся автоматически во время установки программы Docker.

Если вы попытаетесь запустить команду docker без префикса sudo и вне группы docker, вы получите ошибку:

docker: Cannot connect to the Docker daemon. Is the docker daemon running on this host?.
See 'docker run --help'.

Чтобы вам не пришлось набирать префикс sudo каждый раз когда вам нужно запустить команду docker, добавьте своего пользователя в группу docker:

sudo usermod -aG docker $(whoami)

Чтобы активировать это изменение, выйдите из системы и войдите снова.

Чтобы добавить в группу docker пользователя, который не является текущим, укажите в команде его имя:

sudo usermod -aG docker username





NOTES (если что-то пошло не так... будет дополняться):
В некоторых случаях для работы docker необходимо:

1) Gоправить файл /etc/docker/daemon.json

{
        "userns-remap": ""
}

2) И GRUB
sudo echo "GRUB_CMDLINE_LINUX_DEFAULT="quiet cgroup_enable=memory swapaccount=1"" >> /etc/default/grub
sudo update-grub
sudo reboot
