#!/bin/bash -
# Вспомогательный скрипт к jenkins create
set -e # stop on error (some command returnet non-0 code)

# Подготовка самого дистрибутива нашей системы

help() {
    me=`basename "$0"`

    echo " Usage:"
    echo " $me [ -t <path> | -a | -d <path> ]"

    echo " Options:"
    echo "  -t, --tag=<string>"
    echo "       Версия для сборки. Обязательно."
    echo "  -a, --authorize"
    echo "       Включить авторизацию. По умолчанию выключена"
    echo "  -d, --diff=<string>"
    echo "       Путь к diff-файлу для сборки без авторизации. По умолчанию 'unauthorize.diff'"
}

CURRENT_VERSION=""
ON_AUTHORIZE="false"
UNAUTHORIZE_DIFF_FILE="unauthorize.diff"

TEMP=`getopt -o ht:ad: -l help,tag:,authorize,diff: \
     -n "$(basename $0)" -q -- "$@"`
eval set -- "$TEMP"

while true ; do
    case "$1" in
        -h|--help) help; exit 0 ;;
        -t|--tag) CURRENT_VERSION=$2; shift 2 ;;
        -a|--authorize) ON_AUTHORIZE="true"; shift ;;
        -d|--diff) UNAUTHORIZE_DIFF_FILE=$2; shift 2 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

echo "Переход к версии $CURRENT_VERSION. Все несохранённые изменения помещены в stash"
git stash
#git fetch upstream > /dev/null
git checkout "$CURRENT_VERSION" > /dev/null

#[ "true" = "$ON_AUTHORIZE" ] || {
#  DIR="$(pwd)/distrib"
#  pushd .. > /dev/null
#  git apply "$DIR/$UNAUTHORIZE_DIFF_FILE"
#  popd > /dev/null
#}

echo "Начало сборки..."
echo "> Сборка frontend"
pushd ./firefront > /dev/null
yarn
yarn build
rm -rf ../fireback/frontend-server/src/main/webapp/public
cp -r build/public ../fireback/frontend-server/src/main/webapp/
cp build/index.html ../fireback/frontend-server/src/main/webapp/WEB-INF/views/index.html
popd > /dev/null

echo "> Сборка backend"
pushd ./fireback > /dev/null
mvn -q clean install
popd

echo "> Удаление временных файлов"
rm -rf ./fireback/frontend-server/src/main/webapp/public
git checkout -- ./fireback/frontend-server/src/main/webapp/ > /dev/null
git checkout -- ./firefront/config/app.js > /dev/null

echo "Сборка завершена"

echo "Копирование сборки в дирректорию build/firesecNT"
pushd ./distrib
rm -rf build/firesecNT/*
mkdir -p build/firesecNT/drivers

cp ../fireback/frontend-server/target/frontend-server.war ./build/firesecNT/ROOT.war
cp ../fireback/business-logic/target/business-logic-server.jar ./build/firesecNT/
cp ../fireback/communication-server/target/communication-server.jar ./build/firesecNT/
cp ../fireback/driver-2OP3/target/driver-2OP3.jar ./build/firesecNT/drivers/

echo "Подготовка завершена!"
