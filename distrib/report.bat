@echo off
:: UTF-8
CHCP 65001

::
:: Создание отчёта по системе
::

for /f "delims=" %%i in ('powershell get-date -format "{yyyy-MM-dd}"') do set current_date="%%i"
for /f "delims=" %%i in ('powershell get-date -format "{dd-MM-yyyy_HH.mm.ss}"') do set report_path="report-%%i"

:: Создаём директорию для хранения отчетов
mkdir ..\reports
cd ..\reports

:: Создаём временную папку
mkdir %report_path%

:: Собираем данные
:: Логи
copy "%CATALINA_HOME%\logs\firesec.txt" "%report_path%"\tomcat.txt
copy "C:\firesecNT\logs\firesecnt-bl-log.%current_date%.log" "%report_path%"\bl.txt
copy "C:\firesecNT\logs\bl-stdout-log.txt" "%report_path%"\bl-stdout.txt
copy "C:\firesecNT\logs\firesecnt-cs-log.%current_date%.log" "%report_path%"\cs.txt
copy "C:\firesecNT\logs\cs-stdout-log.txt" "%report_path%"\cs-stdout.txt
copy "C:\firesecNT\drivers\logs\rubezh2op3.log" "%report_path%"\driver-2OP3.txt

:: Дамп базы
"C:\Program Files\MongoDB\Server\3.4\bin\mongodump" --db firesec --archive="%report_path%"\mongo_dump.gz --gzip
:: Для восстановления базы:
::   mongorestore --db firesec --archive=mongo_dump.gz --gzip

:: Комментарий пользователя
set /p comment_header="Краткое описание проблемы: "
set /p comment_body="Опишите проблему подробнее (если необходимо): "
set /p comment_actions="Опишите действия, вызвавшие проблему: "

(echo Описание: %comment_header% && echo.) > "%report_path%\comment.txt"
(echo Пояснение: %comment_body% && echo.) >> "%report_path%\comment.txt"
echo Действия: %comment_actions% >> "%report_path%\comment.txt"

:: Архивируем и удаляем временную папку
powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::CreateFromDirectory('%report_path%', '%report_path%.zip'); }"
rmdir /s %report_path% /q
