Инструкция по обновению проекта FireSec
============================================

Разворачивание/обновление системы
---------------------------------

### Разворачивание frontend сервера

Скопировать скомпилированную версию в apache tomcat (или задеплоить через веб-интерфейс):

    cp firesecNT/ROOT.war /opt/tomcat/webapps

### Настроить время сессии в tomcat (при обновлении с версии v0.16 и более ранних)

Открыть файл:

    sudo nano conf/web.xml

Добавить в файл следующие строки:

    <session-config>
        <session-timeout>-1</session-timeout>
    </session-config>

### Установка драйвера Hasp (при обновлении с версии v0.17.18 и более ранних)

Распаковать архив и установить пакет

    tar xzf Sentinel_LDK_Ubuntu_DEB_Run-time_Installer.tar.gz
    sudo dpkg -i Sentinel_LDK_Ubuntu_DEB_Run-time_Installer/aksusbd_7.81-1_amd64.deb

### Обновление сервера бизнес-логики и драйверов

Остановить модули (если запущены)

    sudo initctl stop firesecnt-bl
    sudo initctl stop firesecnt-cs

Скопировать новую версию

    rm -rf /opt/firesecNT/*
    cp -r firesecNT/* /opt/firesecNT/

Скопировать библиотеку HASP (при обновлении с версии v0.17.15 и более ранних)

    cp libHASPJava.so /usr/lib

Запустить модули

    sudo initctl start firesecnt-bl
    sudo initctl start firesecnt-cs

